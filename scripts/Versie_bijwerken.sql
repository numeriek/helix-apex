-- Parameter &1: pad/naam van het spool-bestand
-- Parameter &2: versienummer van de oplevering
-- Parameter &3: soort van de oplevering (D, W, B)
-- Parameter &4: commandfile van waaruit aangeroepen (DB, WS)

set echo off
set feedback off
set verify off
set heading on

set pagesize 60
set linesize 128

spool &1

prompt Patchnummer/systeemdatum updaten in tabel KGC_GEINSTALLEERDE_VERSIES 

UPDATE KGC_GEINSTALLEERDE_VERSIES
   SET AFGEROND = 'J'
 WHERE (( '&3' = 'D' AND '&4' = 'DB' ) OR ( '&3' IN ( 'W', 'B' ) AND '&4' = 'WS' ))
   AND DATUM = (SELECT MAX(DATUM) FROM KGC_GEINSTALLEERDE_VERSIES)
/
commit
/

spool off

exit 0
