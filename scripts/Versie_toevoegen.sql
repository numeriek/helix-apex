-- Parameter &1: pad/naam van het spool-bestand
-- Parameter &2: versienummer van de oplevering
-- Parameter &3: soort van de oplevering (D, W, B)
-- Parameter &4: commandfile van waaruit aangeroepen (DB, WS)

set echo off
set feedback off
set verify off
set heading on

set pagesize 60
set linesize 128

spool &1

prompt Patchnummer/systeemdatum: &2 toevoegen aan tabel KGC_GEINSTALLEERDE_VERSIES 

INSERT INTO KGC_GEINSTALLEERDE_VERSIES
  (DATUM
  ,VERSIE
  ,USERNAME
  ,AFGEROND)
SELECT SYSDATE
     , '&2'
     , USER
     , 'N'
  FROM DUAL
 WHERE ( '&3' IN ('D', 'B') AND '&4' = 'DB' ) OR ( '&3' = 'W' AND '&4' = 'WS' )
/
commit
/

spool off

exit 0
