@echo off
rem ###############################################################
echo Running export APEX for Oracle home: %ORACLE_HOME%
SET APEX_HOME=C:\rene\apex_5.0.4\apex
SET PATH=%PATH%;%ORACLE_HOME%\jdk\bin
SET CLASSPATH=%ORACLE_HOME%\jdbc\lib\ojdbc14.jar;%APEX_HOME%\utilities
rem ----------------------------------------
SET DATABASE=umcstgen06.umcn.nl:1521:devilix
SET USER=apex_050000
SET PASS=apx165berlin
rem ----------------------------------------
rem ### SET RUNJAVA=%ORACLE_HOME%\jre\1.4.2\bin\java.exe
SET RUNJAVA="c:\Program Files (x86)\Java\jdk1.7.0_45\jre"\bin\java.exe
rem ### %runjava% -version
rem ###############################################################
IF [%1]==[] GOTO BLANK
  %RUNJAVA% oracle.apex.APEXExport -db %DATABASE% -user %USER% -password %PASS% -applicationid %1
  if ERRORLEVEL 1 (
    ECHO Error during export
    GOTO DONE
  )
  rmdir /Q /S f%1
  %RUNJAVA% oracle.apex.APEXExportSplitter f%1.sql
  del f%1.sql
  xcopy /I /Y /S f%1 apex_f%1
GOTO DONE

:BLANK
ECHO No APEX application Parameter
GOTO DONE

:DONE
