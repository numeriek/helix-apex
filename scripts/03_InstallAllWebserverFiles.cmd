@echo off
rem ###############################################################
rem Kopieer alle webserver files (froms, reports, libraries, menus)
rem van hun COMPILEDIR naar de juiste directories in APPLICATIONDIR.
rem
rem Dit script moet draaien na het genereerWebserver.cmd script.
rem
rem Als er in de BEH directory een versie van hetzelfde bestand bestaat,
rem wordt deze hernoemd naar een .bck extensie en wordt de nieuwe
rem versie geplaatst. Als je in de BEH directory een file plaatst
rem met dezelfde basename maar met extensie .txt dan blijft de oude
rem webserver file staan. 
rem
rem De installatiebrede settings staan in het bestand env.bat
rem
rem     OPLEVERDIR      de directory waar de patch staat.
rem     COMPILEDIR      de directory waar de gecompileerde webserver-objecten staan.
rem     APPLICATIONDIR  de directory waar naartoe gekopieerd wordt.
rem     RELEASE         de naam van de release die geinstalleerd wordt.

call env.bat

rem ###############################################################
SET REPORTS_PATH=%APPLICATIONDIR%\reports
SET FORMS_PATH=%APPLICATIONDIR%\forms
SET LIBS_PATH=%APPLICATIONDIR%\libs
SET REPORTS_BEH_PATH=%APPLICATIONDIR%_beh\reports
SET FORMS_BEH_PATH=%APPLICATIONDIR%_beh\forms
SET LIBS_BEH_PATH=%APPLICATIONDIR%_beh\libs

SET LOGFILE=%OPLEVERDIR%\Release\Logfiles\installAllWebserverFiles.log

echo -----------------------------                          >> %LOGFILE%
echo Installeer de forms objecten voor omgeving %OMGEVING%  >> %LOGFILE%
echo Datum               : %DATE% - %TIME%                  >> %LOGFILE%
echo Bron compiledir     : %COMPILEDIR%                     >> %LOGFILE%
echo Doel applicationdir : %APPLICATIONDIR%                 >> %LOGFILE%
echo -----------------------------                          >> %LOGFILE%

rem # ------------------------------------------------------------------------------------
rem # Zoek de pll en plx files in de COMPILEDIR en kopieer deze naar het LIBS_PATH.
rem # Als het bestand al bestaat wordt deze overschreven. 
rem # Als een bestand met dezelfde naam bestaat in het LIBS_BEH_PATH wordt het op die plek 
rem # hernoemd naar .%RELEASE%.bck.
rem # ------------------------------------------------------------------------------------
for %%f in (%COMPILEDIR%\pll\*.pll,%COMPILEDIR%\plx\*.plx) do (
  echo "Check file: %%~nf%%~xf" >> %LOGFILE%
  rem Controleer of de file bestaat in het BEH path.
  rem Als dit zo is, move de file als er geen .txt bestand bestaat.
  if exist "%LIBS_BEH_PATH%\%%~nf%%~xf" (
    if exist "%LIBS_BEH_PATH%\%%~nf.txt" (
      echo "+ Keep BEH file %%~nf%%~xf. (because file %%~nf.txt exists)"           >> %LOGFILE%
    ) else (
      echo "+ Rename BEH file %%~nf%%~xf to %%~nf%%~xf.%RELEASE%.bck"               >> %LOGFILE%
      move "%LIBS_BEH_PATH%\%%~nf%%~xf" "%LIBS_BEH_PATH%\%%~nf%%~xf.%RELEASE%.bck"  >> %LOGFILE% 2>&1
    )
  )
  rem Kopieer de file naar juist locatie
  echo "+ Install file: %LIBS_PATH%\%%~nf%%~xf" >> %LOGFILE%
  copy /Y "%%f" "%LIBS_PATH%\%%~nf%%~xf"        >> %LOGFILE% 2>&1

  if ERRORLEVEL 1 (
    echo "+ Destination locked (try renaming): %LIBS_PATH%\%%~nf%%~xf" >> %LOGFILE%
    move "%LIBS_PATH%\%%~nf%%~xf" "%LIBS_PATH%\%%~nf%%~xf.locked"      >> %LOGFILE% 2>&1
    copy /Y "%%f" "%LIBS_PATH%\%%~nf%%~xf"                             >> %LOGFILE% 2>&1

  if ERRORLEVEL 1 (
       echo "+ COPY FAILED: %LIBS_PATH%\%%~nf%%~xf" >> %LOGFILE%
    )
  )
)


rem # ------------------------------------------------------------------------------------
rem # Zoek de fmb, fmx, mmb en mmx files in de COMPILEDIR en kopieer deze naar het FORMS_PATH.
rem # Als het bestand al bestaat wordt deze overschreven. 
rem # Als een bestand met dezelfde naam bestaat in het FORMS_BEH_PATH wordt het op die plek 
rem # hernoemd naar .%RELEASE%.bck.
rem # ------------------------------------------------------------------------------------
for %%f in (%COMPILEDIR%\fmb\*.fmb,%COMPILEDIR%\fmx\*.fmx,%COMPILEDIR%\mmb\*.mmb,%COMPILEDIR%\mmx\*.mmx) do (
  echo "Check file: %%~nf%%~xf" >> %LOGFILE%
  rem Controleer of de file bestaat in het BEH path.
  rem Als dit zo is, move de file als er geen .txt bestand bestaat.
  if exist "%FORMS_BEH_PATH%\%%~nf%%~xf" (
    if exist "%FORMS_BEH_PATH%\%%~nf.txt" (
      echo "+ Keep BEH file %%~nf%%~xf. (because file %%~nf.txt exists)"             >> %LOGFILE%
    ) else (
      echo "+ Rename BEH file %%~nf%%~xf to %%~nf%%~xf.%RELEASE%.bck"                 >> %LOGFILE%
      move "%FORMS_BEH_PATH%\%%~nf%%~xf" "%FORMS_BEH_PATH%\%%~nf%%~xf.%RELEASE%.bck"  >> %LOGFILE% 2>&1
    )
  )
  rem Kopieer de file naar juist locatie
  echo "+ Install file: %FORMS_PATH%\%%~nf%%~xf"  >> %LOGFILE%
  copy /Y "%%f" "%FORMS_PATH%\%%~nf%%~xf"     >> %LOGFILE% 2>&1

  if ERRORLEVEL 1 (
    echo "+ Destination locked (try renaming): %FORMS_PATH%\%%~nf%%~xf" >> %LOGFILE%
    move "%FORMS_PATH%\%%~nf%%~xf" "%FORMS_PATH%\%%~nf%%~xf.locked"     >> %LOGFILE% 2>&1
    copy /Y "%%f" "%FORMS_PATH%\%%~nf%%~xf"                             >> %LOGFILE% 2>&1

    if ERRORLEVEL 1 (
      echo "+ COPY FAILED: %LIBS_PATH%\%%~nf%%~xf" >> %LOGFILE%
    )
  )
)


rem # ------------------------------------------------------------------------------------
rem # Zoek de rdf files in de COMPILEDIR en kopieer deze naar het REPORTS_PATH.
rem # Als het bestand al bestaat wordt deze overschreven. 
rem # Als een bestand met dezelfde naam bestaat in het REPORTS_BEH_PATH wordt het op die plek 
rem # hernoemd naar .%RELEASE%.bck.
rem # ------------------------------------------------------------------------------------
for %%f in (%COMPILEDIR%\rdf\*.rdf) do (
  echo "Check file: %%~nf%%~xf" >> %LOGFILE%
  rem Controleer of de file bestaat in het BEH path.
  rem Als dit zo is, move de file als er geen .txt bestand bestaat.
  if exist "%REPORTS_BEH_PATH%\%%~nf%%~xf" (
    if exist "%REPORTS_BEH_PATH%\%%~nf.txt" (
      echo "+ Keep BEH file %%~nf%%~xf. (because file %%~nf.txt exists)"                 >> %LOGFILE%
    ) else (
      echo "+ Rename BEH file %%~nf%%~xf to %%~nf%%~xf.%RELEASE%.bck"                     >> %LOGFILE%
      move "%REPORTS_BEH_PATH%\%%~nf%%~xf" "%REPORTS_BEH_PATH%\%%~nf%%~xf.%RELEASE%.bck"  >> %LOGFILE% 2>&1
    )
  )
  rem Kopieer de file naar juist locatie
  echo "+ Install file: %REPORTS_PATH%\%%~nf%%~xf"  >> %LOGFILE%
  copy /Y "%%f" "%REPORTS_PATH%\%%~nf%%~xf"     >> %LOGFILE% 2>&1
  
  if ERRORLEVEL 1 (
    echo "+ Destination locked (try renaming): %REPORTS_PATH%\%%~nf%%~xf" >> %LOGFILE%
    move "%REPORTS_PATH%\%%~nf%%~xf" "%REPORTS_PATH%\%%~nf%%~xf.locked"   >> %LOGFILE% 2>&1
    copy /Y "%%f" "%REPORTS_PATH%\%%~nf%%~xf"                             >> %LOGFILE% 2>&1

    if ERRORLEVEL 1 (
        echo "+ COPY FAILED: %LIBS_PATH%\%%~nf%%~xf" >> %LOGFILE%
    )
  )
)

echo -----------------------------                                      >> %LOGFILE%
echo Datum : %DATE% - %TIME%                                            >> %LOGFILE%
echo -----------------------------                                      >> %LOGFILE%
echo De FMB, FMX, MMB, MMX, RDF, PLL en PLX bestanden zijn gekopieerd.  >> %LOGFILE%
echo Kopieer nu met de hand de Java en Help bestanden.                  >> %LOGFILE%
echo -----------------------------                                      >> %LOGFILE%
