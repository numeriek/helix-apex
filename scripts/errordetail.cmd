@echo off

rem verwerken logging tbv van genereren fmx,plx,rep en mmx-bestanden
rem Parameter 1 logbestand  err...
rem Parameter 2 executable  plx,fmx,rep....


if not exist %1 goto einde
if     exist %2 goto checkFoutcode
goto Nietafgerond

:checkFoutcode
find /N /I "FRM- ORA- REP-" %1

if ERRORLEVEL 1 goto einde
if ERRORLEVEL 0 goto mogelijkfout

goto einde
:mogelijkfout

findstr /N /I "FRM- ORA- REP-" %1 >>FRMmelding.lst

move %1 %OPLEVERDIR%\Release\Logfiles\

goto einde


:Nietafgerond
find /N /I "Created form file " %1>>geenexecutable.lst
move %1 %OPLEVERDIR%\Release\Logfiles\
goto einde


:einde

