rem
rem Parameters mbt. het versienummer en de soort van de onderhavige oplevering
rem
rem VERSIE			Het Helix-versienummer van deze oplevering (V.R.P.F_YYYYMMDD)
rem SOORT			Het soort oplevering dat het betreft (alleen DB-componenten, alleen webserver-componenten of beide): 
rem					SOORT=D alleen het script InstalleerDatabaseObjecten.cmd wordt uitgevoerd
rem					SOORT=W alleen het script genereerWebserver.cmd wordt uitgevoerd
rem					SOORT=B beide scripts worden uitgevoerd

SET VERSIE=8.13.3.1_20170202
SET SOORT=B
