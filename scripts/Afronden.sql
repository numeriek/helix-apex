rem Afronden installatie
rem Valid maken specifieke objecten

set linesize 256

spool overzicht_sppl.lst

prompt recompile javascources

rem ALTER JAVA SOURCE "BlobHandler" compile;
rem ALTER JAVA SOURCE "FTPOps" compile;
rem ALTER JAVA SOURCE "HL7" compile;
rem ALTER JAVA SOURCE "Bestanden" compile;

prompt compile body van kgc_import
alter package kgc_import compile body;

prompt invalid object hercompileren;

exec t.cmp;
exec t.cmp;
exec t.cmp;

-- reconcile de omgeving
exec kgc_util_00.init;

prompt toon alle aanwezige fouten
select * from user_errors;

spool off

Prompt toon nu de afwijkingen


exit 0
