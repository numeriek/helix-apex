rem Enable constraints en triggers

set heading off
set feedback off
set echo off
set escape off

spool &1.

select 'Alter table '||table_name||' enable constraint '||constraint_name||';'
from   user_constraints ucs
where status <> 'ENABLED'
and    ucs.constraint_type='R'
and    ucs.delete_rule <> 'NO ACTION'
/

spool off

@&1.

exit 0