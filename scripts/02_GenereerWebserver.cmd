@echo off
rem Installeren Webserver-objecten HELIX

rem Het script moet op de webserver uitgevoerd worden.
rem Genereren van executables van forms, reports, menus en libraries.
rem Directory-settings worden binnen het bestand env.bat gedaan.
rem Versie-settings worden binnen het bestand versie.bat gedaan.
rem Diverse scripts maken een spoolbestand aan.
rem Graag deze bestanden naar het Serviceteam helix terugzenden.

rem Indien van toepassing eerst de database-objecten installeren door het command-bestand
rem 'InstalleerDatabaseobjecten.cmd' uit te voeren.

setlocal

rem Installatiebrede settings staan in het bestand env.bat
rem	OPLEVERDIR 	de directory van de oplevering.
rem     OWNER		de connectstring voor de database.
rem     COMPILEDIR	de directory van de te compileren webserver-objecten.

call env.bat

rem Versiegegevens van de onderhavige oplevering staan in het bestand versie.bat
rem	VERSIE		Het Helix-versienummer van deze oplevering.
rem	SOORT		Het soort oplevering (alleen DB-componenten, alleen webserver-componenten of beide).

call versie.bat

echo Uitvoeren webserver-scripts %date% %TIME% >>compile.lst

call sqlplus %OWNER% @Versie_toevoegen.sql %OPLEVERDIR%\Release\logfiles\versie_toevoegen_ws.lst %VERSIE% %SOORT% WS

rem file attributen resetten tbv de genereerbaarheid

attrib -R -A %COMPILEDIR%\pll\*.* /S /D
attrib -R -A %COMPILEDIR%\fmb\*.* /S /D
attrib -R -A %COMPILEDIR%\mmb\*.* /S /D
attrib -R -A %COMPILEDIR%\rdf\*.* /S /D

for %%f in (%COMPILEDIR%\pll\*.pll) do call DetailCompilePll %OWNER% %%f %COMPILEDIR%\plx\%%~nf.plx %%~pf%%~nf.err %FRMCAOPT%
for %%f in (%COMPILEDIR%\fmb\*.fmb) do call DetailCompileFmb %OWNER% %%f %COMPILEDIR%\fmx\%%~nf.fmx %%~pf%%~nf.err %FRMCAOPT%
for %%f in (%COMPILEDIR%\mmb\*.mmb) do call DetailCompileMmb %OWNER% %%f %COMPILEDIR%\mmx\%%~nf.mmx %%~pf%%~nf.err %FRMCAOPT%
for %%f in (%COMPILEDIR%\rdf\*.rdf) do call DetailCompileRdf %OWNER% %%f %COMPILEDIR%\rep\%%~nf.rep %%~pf%%~nf.err %RDFCAOPT%

call sqlplus %OWNER% @Versie_bijwerken.sql %OPLEVERDIR%\Release\logfiles\versie_bijwerken_ws.lst %VERSIE% %SOORT% WS

echo Klaar met genereren webserver-objecten %date% %TIME% >>compile.lst
move *.lst %OPLEVERDIR%\Release\Logfiles\
move *.log %OPLEVERDIR%\Release\Logfiles\

endlocal

exit
