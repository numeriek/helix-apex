set echo off
set feedback off
set verify off
set heading on

set pagesize 60
set linesize 128

spool &1

prompt Invalid objects 

select object_type,object_name
from   user_objects 
where status <> 'VALID'
/

prompt.
prompt Disabled Constraints 

select constraint_name, constraint_type
from   user_constraints
where  status <> 'ENABLED'
/

prompt.
prompt Disabled triggers 

select trigger_name
from   user_triggers
where  status <> 'ENABLED'
/

spool off

exit 0
/
