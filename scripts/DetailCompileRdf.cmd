Rem verwerk de rdf tot rep

echo RDF_%2 >>compile.lst

%RDFCONVERTER% source=%2 userid=%1 stype=RDFFILE dtype=REPFILE batch=YES overwrite=YES dest=%3 compile_all=%5

if exist %4 call errordetail %4 %3

del /q %4
