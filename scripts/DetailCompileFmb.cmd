Rem verwerk de fmb tot fmx

echo FMB_%2>>compile.lst

%FORMCOMPILER% module=%2 userid=%1 module_type=FORM logon=YES batch=YES output_file=%3 Window_state=Minimize compile_all=%5

if exist %4 call errordetail %4 %3

del /q %4
