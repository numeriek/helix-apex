@echo off

rem Installeren Database-objecten HELIX

rem Het script moet op de webserver uitgevoerd worden.
rem Genereren van server-side objecten zoals tables, triggers, packages etc.
rem Directory-settings worden binnen het bestand env.bat gedaan.
rem Versie-settings worden binnen het bestand versie.bat gedaan.
rem Diverse scripts maken een spoolbestand aan.
rem Graag deze bestanden naar het Serviceteam helix terugzenden.

rem Indien van toepassing hierna de webserver-objecten compileren door het command-bestand
rem 'genereerWebserver.cmd' uit te voeren.

setlocal

rem Installatiebrede settings staan in het bestand env.bat
rem	OPLEVERDIR 	de directory van de oplevering.
rem     OWNER		de connectstring voor de database.

call env.bat

rem Versiegegevens van de onderhavige oplevering staan in het bestand versie.bat
rem	VERSIE		Het Helix-versienummer van deze oplevering.
rem	SOORT		Het soort oplevering (alleen DB-componenten, alleen webserver-componenten of beide).

call versie.bat

echo Uitvoeren database-scripts %date% %TIME% >>instaldatabase.lst

call sqlplus %OWNER% @Versie_toevoegen.sql %OPLEVERDIR%\Release\logfiles\versie_toevoegen_db.lst %VERSIE% %SOORT% DB
call sqlplus %OWNER% @Afwijkingen.sql %OPLEVERDIR%\Release\logfiles\prerelease.lst

call sqlplus %OWNER% @%OPLEVERDIR%\Release\database\DDL\start_ddl.sql
call sqlplus %OWNER% @%OPLEVERDIR%\Release\database\HST\start_hst.sql
call sqlplus %OWNER% @%OPLEVERDIR%\Release\database\TAPI\start_tapi.sql

call sqlplus %OWNER% @%OPLEVERDIR%\Release\database\DML\start_dml.sql

call sqlplus %OWNER% @enableConstraints.sql %OPLEVERDIR%\Release\logfiles\reenableConstraints.sql
call sqlplus %OWNER% @afronden.sql
call sqlplus %OWNER% @Afwijkingen.sql %OPLEVERDIR%\Release\logfiles\postrelease.lst
call sqlplus %OWNER% @Versie_bijwerken.sql %OPLEVERDIR%\Release\logfiles\versie_bijwerken_db.lst %VERSIE% %SOORT% DB

echo Klaar met genereren databaseobjecten %date% %TIME% >>instaldatabase.lst
move *.lst %OPLEVERDIR%\Release\Logfiles\
move *.log %OPLEVERDIR%\Release\Logfiles\

endlocal

exit 0
