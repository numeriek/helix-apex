rem ###############################################################
rem ###############################################################
rem dit stuk moet per patch installatie gecheck/gewijzigd worden. Welke release en waar het
rem geinstalleerd moet worden.
rem ###############################################################
rem ###############################################################
SET OMGEVING=ACPT
rem SET OMGEVING=PINK
rem SET OMGEVING=PROD
rem SET OMGEVING=TEST
SET RELEASE=VUL_HIER_RELEASE_NAAM_IN
rem ###############################################################
rem ###############################################################
rem Algemene variables. Deze gelden voor alle releases en alle omgevingen. 
rem Als het goed is, hoeven ze nooit veranderd te worden.
rem ###############################################################
rem ###############################################################
SET OPLEVERDIR=E:\Helix\patches\2017\%RELEASE%
SET APPLICATIONDIR=E:\Helix\%OMGEVING%
SET APPLICATIONDIR_PROD=E:\Helix\prod
SET FORMCOMPILER=D:\oracle\middlewareForms\Oracle_FRHome1\bin\frmcmp.exe
SET RDFCONVERTER=D:\oracle\middlewareforms\Oracle_FRHome1\bin\rwconverter.exe
SET COMPILEDIR=%OPLEVERDIR%\Release\Webserver
SET NLS_LANG=AMERICAN_AMERICA.UTF8
SET FRMCAOPT=SPECIAL
SET RDFCAOPT=YES
rem ###############################################################
rem ###############################################################
rem Omgeving specifieke deel. De rest van de settings zijn omgeving afhankelijk. 
rem Bij patch installatie hoef je hier niks te wijzigen.
rem ###############################################################
rem ###############################################################
IF "%OMGEVING%"=="ACPT" GOTO ACPT
IF "%OMGEVING%"=="PINK" GOTO PINK
IF "%OMGEVING%"=="PROD" GOTO PROD
IF "%OMGEVING%"=="TEST" GOTO TEST ELSE GOTO EINDE

:ACPT
SET OWNER=helix/acc165berlin@accilix.umcn.nl
SET REPORTS_PATH=%COMPILEDIR%\rdf;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%APPLICATIONDIR_PROD%\libs;%REPORTS_PATH%;
SET FORMS_PATH=%COMPILEDIR%\fmb;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%APPLICATIONDIR_PROD%\libs;%FORMS_PATH%;
GOTO EINDE

:PINK
SET OWNER=helix/pink165berlin@pinkilix.umcn.nl
SET REPORTS_PATH=%COMPILEDIR%\rdf;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%APPLICATIONDIR_PROD%\libs;%REPORTS_PATH%;
SET FORMS_PATH=%COMPILEDIR%\fmb;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%APPLICATIONDIR_PROD%\libs;%FORMS_PATH%;
GOTO EINDE

:PROD
SET OWNER=helix/prod165berlin@prodilix.umcn.nl
SET REPORTS_PATH=%COMPILEDIR%\rdf;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%REPORTS_PATH%;
SET FORMS_PATH=%COMPILEDIR%\fmb;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%FORMS_PATH%;
GOTO EINDE

:TEST
SET OWNER=helix/test165berlin@testilix.umcn.nl
SET REPORTS_PATH=%COMPILEDIR%\rdf;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%APPLICATIONDIR_PROD%\libs;%REPORTS_PATH%;
SET FORMS_PATH=%COMPILEDIR%\fmb;%COMPILEDIR%\Pll;%COMPILEDIR%\Tpl;%APPLICATIONDIR%\libs;%APPLICATIONDIR_PROD%\libs;%FORMS_PATH%;
GOTO EINDE

:EINDE
