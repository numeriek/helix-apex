prompt GENEREER DDL SCRIPTS
prompt voor objecten like:  '&1'
prompt output naar:         '&2' 

set serveroutput ON size 999999
set pagesize 0
set trimspool on 
set headsep off
set feedback off
set echo off
set termout off
set linesize 9999
set verify off

spool %TEMP%\intermediate_file.sql

declare
  cursor c_obj is
  select decode(object_type, 'TABLE',object_type , 'TYPE',object_type , 'SEQUENCE',object_type , 'JAVA SOURCE','JAVA_SOURCE', 'LIBRARY',object_type , 'PROCEDURE',object_type 
                           , 'FUNCTION',object_type , 'PACKAGE',object_type , 'PACKAGE BODY','PACKAGE', 'VIEW',object_type, 'MATERIALIZED VIEW','MATERIALIZED_VIEW', 'TRIGGER',object_type
                           , 'INDEX',object_type, 'JOB', 'PROCOBJ' ) as type -- Het TYPE dat nodig is voor export d.m.v. DBMS_METADATA.GET_DDL.
       , object_name
       , object_type
       , lower(nvl('&2', '.')) || '\' || translate(lower(object_type), ' ', '_') as folder
       , translate(lower(object_name), '$', '_') || '.sql' as filename
       , owner
  from   all_objects
  where  owner in ('HELIX', 'PUBLIC')
  and object_type in ('TABLE', 'INDEX', 'VIEW','MATERIALIZED VIEW','TRIGGER','PACKAGE','PROCEDURE','FUNCTION','JOB','TYPE','JAVA SOURCE','LIBRARY', 'SEQUENCE')
  and object_name like upper(nvl('&1', '%'))
  and object_name not like 'BEH_MANTIS%' and object_name not like 'CG$%' and object_name not like 'TMP_%'
  and object_name not like 'TEMP_%' and object_name not like 'OUD_%' and object_name not like 'WEBUTIL%' and object_name not like 'QMS_%'
  and object_name not like '%HAMID%' and object_name not like '%MARZENA%'
  and not (object_name like '%_A_TRG' and object_type = 'TRIGGER') -- _A_TRG triggers worden genenereerd
  order by decode(object_type, 'TABLE',10,'TYPE',20,'SEQUENCE',30,'JAVA SOURCE',40,'JAVA CLASS',50,'LIBRARY',60,'PROCEDURE',70,'FUNCTION',80,'PACKAGE',90,'VIEW',110,'MATERIALIZED VIEW',120,'TRIGGER',130,'INDEX',140,'JOB', 150)
         , object_name;

  cursor c_dir is
  select distinct lower(nvl('&2', '.')) || '\' || translate(lower(object_type), ' ', '_') as folder
  from   all_objects
  where  owner in ('HELIX', 'PUBLIC')
  and object_type in ('TABLE', 'INDEX', 'VIEW','MATERIALIZED VIEW','TRIGGER','PACKAGE','PROCEDURE','FUNCTION','JOB','TYPE','JAVA SOURCE','LIBRARY', 'SEQUENCE')
  and object_name like upper(nvl('&1', '%'))
  and not (object_name like '%_A_TRG' and object_type = 'TRIGGER') -- _A_TRG triggers worden genenereerd
  and object_name not like 'BEH_MANTIS%';

begin
  dbms_output.enable(null);
  dbms_output.put_line('set pagesize 0');
  dbms_output.put_line('set trimspool on');
  dbms_output.put_line('set headsep off');
  dbms_output.put_line('set feedback off');
  dbms_output.put_line('set echo off');
  dbms_output.put_line('set termout off');
  dbms_output.put_line('set linesize 32767');
  dbms_output.put_line('set verify off');
  dbms_output.put_line('set long 1000000');
  dbms_output.put_line('column ddl format a32767');
  dbms_output.put_line('BEGIN');
  dbms_output.put_line('DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, ''SQLTERMINATOR'', true);');
  dbms_output.put_line('DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, ''PRETTY'', true);');
  dbms_output.put_line('END;');
  dbms_output.put_line('/');
  dbms_output.put_line('HOST mkdir ' || lower(nvl('&2', '.')) || ' 2>NUL');
  for r_dir in c_dir loop
    dbms_output.put_line('HOST mkdir ' || r_dir.folder || ' 2>NUL');
  end loop;
  for r_obj in c_obj loop
    dbms_output.put_line('spool ' || r_obj.folder || '\' || r_obj.filename);
    -- Het DBMS_METADATA.GET_DDL package lijkt standaard 3 characters extra voor de rest van de tekst te zetten.
    -- Deze worden er hier weer afgegooid.
    dbms_output.put_line('select substr(DBMS_METADATA.GET_DDL(''' || r_obj.type || ''', ''' || r_obj.object_name || ''', ''' || r_obj.owner || '''),4) as ddl from dual;');
    dbms_output.put_line('prompt /');
    dbms_output.put_line('prompt QUIT');
    dbms_output.put_line('spool off');
  end loop;
end;
/

spool off

prompt DDL genereren
@%TEMP%\intermediate_file.sql

QUIT

