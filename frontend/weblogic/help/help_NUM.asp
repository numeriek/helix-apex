<% @Language = "VBScript" %>
<% Response.buffer = true %>
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" Content="text-html; charset=Windows-1252">
</HEAD>
<body>

<%
Dim strModuleName, strType

strModuleName = LCase(Request.QueryString("module"))
strType = LCase(Request.QueryString("type"))
strUser = LCase(Request.QueryString("username"))
Session("username") = strUser

' Leeg? dan index.htm opstarten
if strModuleName = "" then
  Response.Redirect "gebr/index.htm"
end if
strModuleName = "ISF" & strModuleName
if strType = "" then
  ' het is een standaard-structuuromschrijving
else ' -> default: strType = "ld" then
  strModuleName = strModuleName & "_LD"
end if
strModuleName = strModuleName & ".htm"

Set Tools = Server.CreateObject("MSWC.Tools")

strStart = "gebr/index.htm"
' Bestaat de file in dezelfde directory als help.asp?
If Tools.FileExists(strModuleName) then
  strStart = strStart + "?inhoud=" + strModuleName
ElseIf Tools.FileExists("gebr/html_gen/" & strModuleName) then
  strStart = strStart + "?inhoud=html_gen/" & strModuleName
End If
Response.Redirect strStart
' voor testen: zet een ' voor bovenstaande regel
response.write("<p>parameters:")
response.write("<p>module:" + Request.QueryString("module"))
response.write("<p>type:" + Request.QueryString("type"))
response.write("<p>username:" + Request.QueryString("username") + " -sessie: " + Session.Contents("username"))
response.write("<p><A Href='" + strStart + "'>Verder...</A>")
%>
</BODY>
</HTML>
