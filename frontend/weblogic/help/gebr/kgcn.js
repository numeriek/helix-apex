var bIE = (navigator.appName.indexOf("Microsoft") != -1);
var g_strConc = "/";
var g_strHomeDir = getHomeDir();

function getHomeDir() {
  s = top.location.pathname.toString();
  n1 = s.lastIndexOf("/");
  n2 = s.lastIndexOf("\\");
  if (n2 > n1) {
    n1 = n2;
    g_strConc = "\\";
  }
  if (n1 == -1) {
    return "";
  }
  return s.substring(0, n1);
}
/* algemene functies */
/*
 * deze functie doet de parsing van comma-separated name=value argument paren
 * van de query-string van een URL. Het slaat de name=value pairs in de
 * eigenschappen van een object en geeft het object terug
*/
function getArgs(strQuery) {
  var args = new Object();
  var query = "";
  if (strQuery)
    query = strQuery;
  else
    query = location.search.substring(1); // de query-string
  var pairs = query.split(","); // opsplitsen
  for (var i = 0; i < pairs.length; i++) {
    var pos = pairs[i].indexOf('=');  // zoeken naar "name=value"
    if (pos == -1) continue;
    var argname = pairs[i].substring(0, pos);
    var value = pairs[i].substring(pos + 1);
    args[argname] = unescape(value);
  }
  return args;
}

function ExpandOnClick(hCtrl)
{
  if (!bIE) return;
  // next image & div?
  var hImg, hDiv;
  for (i=hCtrl.sourceIndex; i < hCtrl.document.all.length; i++) {
    if ((hCtrl.document.all[i].tagName == "IMG") && (!hImg))
      hImg = hCtrl.document.all[i];
    if ((hCtrl.document.all[i].tagName == "DIV") && (!hDiv))
      hDiv = hCtrl.document.all[i];
    if (hImg && hDiv)
      break;
  }
  if (!hImg && !hDiv)
    return;
  if (hImg.src.indexOf('ExpYes.gif') != -1) {
    hImg.src='ExpNo.gif';
    hDiv.style.display='inline';
  }
  else {
    hImg.src='ExpYes.gif';
    hDiv.style.display='none';
  }
}

function showISF(argProgName, argProgStartup) {
  strP1 = argProgStartup;
  if (argProgStartup == "") {
    strP1 = argProgName;
  }
  top.desCtrl.StartOraF6trx(strP1, "", "", "");
}

function showDoc(argDir, argFile, argBookMark) {
  newLoc = "";
  if (argDir != "") {
    newLoc = argDir + "/";
  }
  newLoc = newLoc + argFile;
  if (argBookMark != "") {
    newLoc = newLoc + "#" + argBookMark;
  }
  if (newLoc != "") {
    top.inhoud.location = newLoc;
  }
}

function showObj(docObj, fileHTML) {
  showObj(docObj, fileHTML, "", "");
}

function showObj(docObj, fileHTML, aName) {
  showObj(docObj, fileHTML, aName, "");
}

function showObj(docObj, fileHTML, aName, type) {
  fileHTML = g_strHomeDir + g_strConc + 'html_gen' + g_strConc + fileHTML;
  if ((!type) || type == "") {
    fileHTML = fileHTML + getShowWhat();
  }
  else {
    fileHTML = fileHTML + "_" + type.toUpperCase();
  }
  fileHTML = fileHTML + ".htm";
  if (aName) {
    if (aName != "") {
      fileHTML = fileHTML + "#" + aName;
    }
  }
  top.inhoud.location=fileHTML;
}

function showDia(docObj, fileHTML) {
  strSrc = docObj.location.toString();
  if ((strSrc.indexOf("html_gen") == -1) && (strSrc.indexOf("html%5Fgen") == -1))
    fileHTML = 'html_gen/' + fileHTML;
  fileHTML = fileHTML + getShowWhat();
  top.inhoud.location=fileHTML + ".htm";
}

function showDesc(docObj, fileHTML) {
  strSrc = docObj.location.toString();
  if ((strSrc.indexOf("html_gen") == -1) && (strSrc.indexOf("html%5Fgen") == -1))
    fileHTML = 'html_gen/' + fileHTML;
  top.inhoud.location=fileHTML;
}

/* haal op wat er getoond moet worden: structuur (geen postfix) of Tekst (_LD) */
function getShowWhat() {
  strDef = '_LD';
  if (!top.menu) return strDef;
  if (!top.menu.ShowWhat) return strDef;
  if (!top.menu.ShowWhat.rbShowWhat) return strDef;
  return getRadioSelection(top.menu.ShowWhat.rbShowWhat);
}

function getRadioSelection(theRadio) {
  var selection=null;
  for(var i=0; i < theRadio.length; i++) {
    if(theRadio[i].checked) {
      selection=theRadio[i].value;
      return selection; 
    }
  }
  return selection; 
}

/* ander type tonen indien in inhoud = ACT of ISF */
function wissel(docObj, strExt) {
  strSrc = top.inhoud.location.pathname.toString();
  nPos = strSrc.indexOf("html_gen");
  if (nPos == -1)
    nPos = strSrc.indexOf("html%5Fgen");
  if (nPos == -1)
    return; // dan geen kandidaat
  strSrc = strSrc.substring(nPos + 9); // slash er ook af!
  nPos = strSrc.indexOf("ACT");
  if (nPos != 0) // begin
    nPos = strSrc.indexOf("ISF");
  if (nPos != 0) // alleen act en isf mogen wisselen
    return;
  nPos = strSrc.indexOf(".htm");
  if (nPos == -1)
    return;
  strSrc = strSrc.substring(0, nPos);
  nLen = strSrc.length;
  nPos = strSrc.indexOf("_LD");
  if (nPos == nLen - 3) {
    strSrc = strSrc.substring(0, nPos);
  }
  showObj(docObj,strSrc);
}

function reageer(docObj) {
  strSrc = docObj.location.pathname.toString();
  nPos = strSrc.lastIndexOf("/");
  if (nPos == -1)
    return; // dan geen kandidaat
  strMod = strSrc.substring(nPos + 1); // slash er ook af!
  nPos = strMod.indexOf(".htm");
  if (nPos == -1)
    return;
  strMod = strMod.substring(0, nPos);
  strPad = ""
//  nPos = strMod.indexOf("ACT");
//  if (nPos != 0)
//    nPos = strMod.indexOf("act");
//  if (nPos != 0)
    nPos = strMod.indexOf("ISF");
  if (nPos != 0)
    nPos = strMod.indexOf("isf");
  if (nPos == 0) {
    strMod = strMod.substring(3);
    strPad = "../" // dan relatief tov html_gen!
  }

  strReageer = strPad + "hlx_reageer.asp?helpdoc=" + strSrc +  ",helpmod=" + strMod + ",helpopm=Scherm '" + strMod + "' met titel '" + docObj.title + "'";
// alert(strReageer);
  top.inhoud.location=strReageer;
}
