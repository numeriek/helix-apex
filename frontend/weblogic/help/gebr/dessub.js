// prevent isolated startup v3.6.0.1
function checkFrames(sub, target) {
  if (top.location == sub.location) {
    strRelDir = "";
    if (sub.location.pathname.indexOf("html_gen") != -1) strRelDir = "../";
    // add your own other subdirectories here!
    top.location.replace(strRelDir + "index.htm?" + target + "=" + sub.location.href);
  }
}
