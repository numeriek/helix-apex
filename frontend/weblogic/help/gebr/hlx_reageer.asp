<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns:des><HEAD><TITLE>Geef uw reaktie op de on-line help van Helix</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<META content="H Ruissen KGCN" name=Author>
<META content=KGCN name=Owner><LINK href="main.css" rel=stylesheet>
<SCRIPT language=JavaScript>
var bIE = (navigator.appName.indexOf("Microsoft") != -1);

function verwerkReactie() {
//  if (document.reageerForm.helpmod.value == "") {
//    alert("Geef naam of omschrijving Help-document!");
//    return;
//  }
//  if (document.reageerForm.helpuser.value == "") {
//    alert("Geef uw user-id!");
//    return;
//  }
  if (document.reageerForm.helpopm.value == "") {
    alert("Geef uw opmerkingen bij het helpdocument!");
    return;
  }
  if (document.reageerForm.helpopm.value.length > 2000) {
    alert("De lengte van het opmerkingenveld is beperkt tot 2000!");
    return;
  }
// submitten maar; dit is nodig zodat asp de gegevens kan verwerken!

  document.reageerForm.submit();
}
</SCRIPT>

<META content="OrgDesigner v3.4.0.1" name=GENERATOR></HEAD>
<BODY bgColor=#e7e3f1>
<h1>Geef uw reaktie op de on-line help van Helix</H1>
<HR class=grey>

<FORM name=reageerForm action="hlx_reageer_ok.asp" method="GET">
<INPUT type=hidden size=10 name=helpdoc>
<table>
<tr>
  <td>Help-document:</td>
  <td><INPUT size=60 maxlength=250 name=helpmod READONLY><td>
</tr><tr>
  <td>Uw&nbsp;Helix&nbsp;username:</td>
  <td><INPUT size=60 maxlength=250 name=helpuser><td>
</tr><tr>
  <td colspan=2>Geef hieronder uw opmerkingen. Vermeld daarbij aub zoveel mogelijk relevante details, zoals bv om welk scherm en veld het gaat (bv de <b>tekst in de titelbalk</B> van het scherm, en de tekst bij het veld, indien van toepassing).
<UL><LI>Is er iets <B>fout</b>, geef dan aan wat fout is, en hoe het wel moet zijn</LI>
<LI>Is er iets <B>onduidelijk</B>, geef dan aan wat er onduidelijk is, en eventueel hoe dat beter zou kunnen</LI>
<LI>Kunt u iets <B>niet vinden</B>, vertel dan waar u naar op zoek was (bedenk dat u ook kunt zoeken op woorden in de helpdocumentatie! Zie de linker kolom ('geef sleutelwoord(en)')</LI>
<LI>Bent u betrokken bij de goedkeuring van de <b>Help-documentatie</b>, geef dan uw opmerkingen of de tekst 'help is geaccepteerd'</LI>
</UL>
</td>
</tr><tr>
  <td valign=top>Opmerkingen:</td>
  <td><TEXTAREA rows=10 cols=60 name=helpopm></TEXTAREA><td>
</tr>
</table>
<A href="javascript:verwerkReactie();"><B>Verstuur reactie!</B></A>
</FORM>
<P><P>Uw reactie wordt verwerkt in de probleemregistratie! Dank voor uw medewerking!</P>
<SCRIPT language=JavaScript>
  var strHelpDoc = "Geef omschrijving help-document";
  var strHelpMod = "modulefout";
  var strHelpOpm = "";
  var strHelpUser = "";
  var args = top.getArgs(location.search.substring(1)); // uit kgcn.js
  if (args.helpdoc) strHelpDoc = args.helpdoc;
  if (args.helpmod) strHelpMod = args.helpmod;
  if (args.helpopm) strHelpOpm = args.helpopm;
  document.reageerForm.helpdoc.value = strHelpDoc;
  document.reageerForm.helpmod.value = strHelpMod;
  document.reageerForm.helpopm.value = strHelpOpm;
<%
  if Session.Contents("username") = "" then
    response.write("  strHelpUser = ""Geef uw username"";")
  else
    response.write("  strHelpUser = """ & Session.Contents("username") & """;")
  end if
%>
  document.reageerForm.helpuser.value = strHelpUser;
</SCRIPT>
</BODY></HTML>