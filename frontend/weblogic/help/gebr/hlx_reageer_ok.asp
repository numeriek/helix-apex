<%@ Language=VBScript LCID = 1043 %>
<!--METADATA TYPE="typelib" uuid="00000205-0000-0010-8000-00AA006D2EA4" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD>
<LINK href="main.css" rel=stylesheet>
<SCRIPT language=JavaScript>
<%
  'Create a connection object.
  Set cnn = Server.CreateObject("ADODB.Connection")
  'Open a connection using the OLE DB connection string.
  cnn.Open  "Provider=MSDAORA.1;Data Source=prod;User ID=probleem;Password=probleem"
  Set cmn= Server.CreateObject("ADODB.Command")
  Set cmn.ActiveConnection = cnn

  strHelpDoc = Request.Querystring("helpdoc")
  strHelpMod = "HELP_" & Request.Querystring("helpmod")
  strHelpUser = Request.Querystring("helpuser")
  strHelpOpm = Request.Querystring("helpopm")

  'Define SQL statement. 
  cmn.CommandText = "{Call probleemmelding(?, ?, ?)}"
'  cmn.commandtype = adCmdStoredProc - niet nodig!
'  cmn.CommandTimeout = 0 - niet nodig!
'  cmn.Prepared = false - niet nodig!

  cmn.Parameters.Append cmn.CreateParameter("P_MODULE",adVarChar,adParamInput,255, strHelpMod)
  cmn.Parameters.Append cmn.CreateParameter("P_MELDER",adVarChar,adParamInput,255, strHelpUser)
  cmn.Parameters.Append cmn.CreateParameter("P_MELDING",adVarChar,adParamInput,2000, strHelpOpm)

  cmn.Execute

  'vervolg aangeven: 
  Response.write("alert('Uw opmerkingen zijn verwerkt!');")
  Response.write("top.inhoud.location=""" & strHelpDoc & """;")
'  Server.Transfer(strHelpDoc) - ai, werkt niet goed ivm relatieve paden!

%>
</SCRIPT>
</HEAD><BODY>
</BODY></HTML>