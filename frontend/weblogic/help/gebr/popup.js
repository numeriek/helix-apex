var bIE = (navigator.appName.indexOf("Microsoft") != -1);
var bNowShowing = false;
var nMouseX = 0;
var nMouseY = 0;

// init:
if (bIE) {
  hMenu = popMenu.style;
}
else {
  hMenu = document.popMenu;
}
if (!hMenu) {
  alert('Place SPAN-element in your document (see DemoComp-example); required for popup!');
}
document.onmousemove = mouseMove;
if (!bIE) {
  document.captureEvents(Event.MOUSEMOVE);
}
hideMenu();

function mouseMove(moveEvent)
{
  if (bIE) {
    nMouseX=event.x + document.body.scrollLeft;
    nMouseY=event.y + document.body.scrollTop;
  }
  else {
    nMouseX=moveEvent.pageX;
    nMouseY=moveEvent.pageY;
  }
}

function hideMenu()
{
  if (bIE) {
    hMenu.visibility = 'hidden';
  }
  else {
    hMenu.visibility = 'hide';
  }
  bNowShowing = false;
}

function showMenu()
{
  if (bNowShowing) {
    hideMenu();
  }
  hMenu.left = nMouseX;
  hMenu.top = nMouseY;
  strTRs = "<tr><td>";
  strTRe = "</td></tr>";
  strHTML = "";
  // compose HTML for menu
  var i,p,v,obj,args=showMenu.arguments;
  for (j=0; j<8; j++) { // sort!
    for (i=0; i<(args.length); i+=2) {
      if ((j==0) && (args[i]=='desc_ld')) { // long description
        strHTML = strHTML + strTRs + args[i+1] + strTRe;
      }
      else if ((j==1) && (args[i]=='desc_in')) { // instruction
        strHTML = strHTML + strTRs + args[i+1] + strTRe;
      }
      else if ((j==2) && (args[i]=='desc_rm')) { // remarks
        strHTML = strHTML + strTRs + args[i+1] + strTRe;
      }
      else if ((j==3) && (args[i]=='desc_dn')) { // design notes
        strHTML = strHTML + strTRs + args[i+1] + strTRe;
      }
      else if ((j==4) && (args[i]=='objroots')) { // drill down
        strHTML = strHTML + strTRs + 'Drill Down to ' + args[i+1] + strTRe;
      }
//      else if ((j==5) && (args[i]=='href1')) { // showObj
//      strHTML = strHTML + strTRs + args[i+1] + 'All Details</A>' + strTRe;
//      }
      else if ((j==6) && (args[i]=='rel_actdrfref2')) { // related documents
        strHTML = strHTML + strTRs + 'Open Document ' + args[i+1] + strTRe;
      }
      else if ((j==7) && (args[i]=='rel_actisfref2')) { // supporting IS Functions
        strHTML = strHTML + strTRs + 'Start Function ' + args[i+1] + strTRe;
      }
    }
  }
  if (strHTML != "") {
    if (bIE) {
      hMenu.visibility = 'visible';
    }
    else {
      hMenu.visibility = 'show';
    }
    bNowShowing = true;
    strHTML = "<TABLE cellSpacing=0 cols=1 cellPadding=1 border=1 bgcolor=#bbbbbb>" + strHTML + "</TABLE>";
    if (bIE) {
      popMenu.innerHTML = strHTML;
    }
    else {
      document.popMenu.document.write('<HTML><HEAD>');
//      document.popMenu.document.write('<STYLE>'); // -> crash in NN4.79
//      document.popMenu.document.write('td {font-size:8pt; font-family:Geneva, Arial, Helvetica, sans-serif;}');
//      document.popMenu.document.write('</STYLE>');
      document.popMenu.document.write('</HEAD>');
      document.popMenu.document.write(strHTML);
      document.popMenu.document.write('</BODY></HTML>');
      document.popMenu.document.close();
    }
  }
}
