02-10-2012 HBOU
in verband met relatiebeheer project, is het veld "code" optioneel gemaakt. 
Deze wordt door procedure beh_gen_code.beh_gen_code_rela (via insert trigger) gevuld.
gebruikers kunnen dit veld handmatig updaten.

bij property palette van veld "code":
"Data" -> "Required" op No gezet. Dit is de enige wijziging!