##################################
##### 29-11-2012 HBOU
##### wegschrijven van client info (ip, os username) in database
##################################
in forms developper:
1 - webutil aan "Object group" toegevoegd.

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
2 - webutil.pll als libray aan helix.fmb gekoppeld

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
3 - in "Helix" nivo aan trigger: WHEN-NEW-FORM-INSTANCE
deze codes (aan het begin) toegevoegd:
Blijkbaar heeft Form wat tijd nodig om webutil te inititeren!
want zonder timer wordt het een blanco tekst (dus niks) weggeschreven.
dus eerst timer creeren:
----------------
declare 
    t_set_session_info timer;	
BEGIN
    -- start timer
    t_set_session_info := create_timer('t_set_session_info',1000, NO_REPEAT); 
end;
------------------
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
4 - trigger WHEN-TIMER-EXPIRED aangemaakt met deze codes:
to do: uitbreiden met webutil_clientinfo.get_host_name zodat je computernaam ook hebt.
----------------
declare
    v_client_info varchar2(2000);
    v_separator varchar2(1) := ';';
begin
    -- define client info
    v_client_info := webutil_clientinfo.get_ip_address ||
    							v_separator ||
    							webutil_clientinfo.get_user_name ||
    							v_separator ||						
    							webutil_clientinfo.get_operating_system							
    							;
    -- set client info
    DBMS_APPLICATION_INFO.SET_CLIENT_INFO(v_client_info);
end;

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--------------
5 - Nu zijn client ip adres en os username via deze qurey op te halen:
select client_info
from sys.v_$session