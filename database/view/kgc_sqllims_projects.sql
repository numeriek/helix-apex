CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_SQLLIMS_PROJECTS" ("KAFD_ID", "ONGR_ID", "PROJECT") AS
  SELECT to_number(null) kafd_id
,          to_number(null)   ongr_id
,          name project
from  cwebv_api_projects
 ;

/
QUIT
