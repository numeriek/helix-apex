CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ISOLATIE_PROCESSEN_VW" ("KAFD_ID", "PROC_ID", "PROC_CODE", "PROC_OMSCHRIJVING", "PROC_COMMENTAAR") AS
  SELECT proc.kafd_id
,      proc.proc_id
,      proc.code proc_code
,      proc.omschrijving proc_omschrijving
,      proc.commentaar proc_commentaar
from   kgc_processen proc
where proc.vervallen = 'N'
and    proc.procestype = 'I'
and  ( proc.mede_id = kgc_mede_00.medewerker_id
    or proc.mede_id is null
     )
 ;

/
QUIT
