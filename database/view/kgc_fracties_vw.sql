CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FRACTIES_VW" ("FRAC_ID", "MONS_ID", "KAFD_ID", "ONGR_ID", "PERS_ID", "ONGR_CODE", "ONGR_OMSCHRIJVING", "FRACTIENUMMER", "STATUS", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "DATUM_AANMELDING") AS
  SELECT frac.frac_id
,      frac.mons_id
,      mons.kafd_id
,      mons.ongr_id
,      mons.pers_id
,      ongr.code ongr_code
,      ongr.omschrijving ongr_omschrijving
,      frac.fractienummer
,      frac.status
,      mons.monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      mons.datum_aanmelding
from   kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_monsters mons
,      bas_fracties frac
where frac.mons_id = mons.mons_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mate_id = mate.mate_id
 ;

/
QUIT
