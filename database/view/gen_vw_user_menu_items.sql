CREATE OR REPLACE FORCE VIEW "HELIX"."GEN_VW_USER_MENU_ITEMS" ("ID", "PARENT_ID", "ITEM_LABEL", "ITEM_DESCRIPTION", "ITEM_TYPE", "ITEM_ICON", "PAGE_NAME", "DISPLAY_SEQUENCE", "CLEARCACHE", "PARAMETERS", "PARAMETER_VALUES", "PRINTERFRIENDLY", "PAGE_ID", "TARGET_ATTR") AS
  WITH v_session_values
        AS (SELECT LOWER(v('LANGCODE')) lang
                  ,v('G_APP_USER_ID') app_user_id
                  ,v('APP_ID') app_id
              --,v('DEBUG') dbg
              --,v('SESSION') session_id
              FROM DUAL)
       ,v_menu_items
        AS (SELECT mim.id
                  ,mim.parent_id
                  ,mil.item_label
                  ,mil.item_description
                  ,mim.item_type
                  ,mim.item_icon
                  ,mim.page_name
                  ,mim.display_sequence
                  ,mim.clearcache
                  ,mim.parameters
                  ,mim.parameter_values
                  ,mim.printerfriendly
                  ,mim.component_function
                  ,mim.target_attr
              FROM gen_menu_items mim
                   INNER JOIN gen_menu_items_lang mil
                      ON (mim.id = mil.menu_item_id)
             WHERE mil.language_code = (SELECT lang
                                          FROM v_session_values))
       ,v_auth_pages
        AS (SELECT mim.*
                  ,aap.page_id
              FROM v_menu_items mim
                   INNER JOIN apex_application_pages aap
                      ON (aap.page_alias = mim.page_name)
             WHERE aap.application_id = (SELECT app_id
                                           FROM v_session_values)
               AND (aap.authorization_scheme IS NULL
                 OR EXISTS
                       (SELECT '1'
                          FROM gen_appl_authorizations aas
                         WHERE component_page = aap.page_alias
                           AND (mim.component_function IS NULL
                             OR mim.component_function = aas.component_function)
                           AND aas.appl_role_id IN (SELECT arr.authorization_appl_role_id
                                                      FROM gen_appl_role_roles arr
                                                           INNER JOIN
                                                           gen_appl_roles arl
                                                              ON (arr.authentication_appl_role_id =
                                                                     arl.id)
                                                     WHERE arl.id IN (SELECT aur.appl_role_id
                                                                        FROM gen_appl_user_roles aur
                                                                       WHERE UPPER(
                                                                                aur.active_ind) =
                                                                                'Y'
                                                                         AND aur.appl_user_id =
                                                                                (SELECT app_user_id
                                                                                   FROM v_session_values))))))
   SELECT id
         ,parent_id
         ,item_label
         ,item_description
         ,item_type
         ,item_icon
         ,page_name
         ,display_sequence
         ,clearcache
         ,parameters
         ,parameter_values
         ,printerfriendly
         ,page_id
         ,target_attr
     FROM v_auth_pages
    WHERE page_id != 9008
   UNION ALL
   SELECT id
         ,parent_id
         ,item_label
         ,item_description
         ,item_type
         ,item_icon
         ,page_name
         ,display_sequence
         ,clearcache
         ,parameters
         ,parameter_values
         ,printerfriendly
         ,page_id
         ,target_attr
     FROM v_auth_pages mim
    WHERE page_id = 9008
      AND EXISTS
             (    SELECT '1'
                    FROM gen_menu_items mim2
                   WHERE mim2.id IN (SELECT id
                                       FROM v_auth_pages)
              CONNECT BY PRIOR id = parent_id
              START WITH parent_id = mim.id)
   UNION ALL
   SELECT mim.id
         ,mim.parent_id
         ,mim.item_label
         ,mim.item_description
         ,mim.item_type
         ,mim.item_icon
         ,mim.page_name
         ,mim.display_sequence
         ,mim.clearcache
         ,mim.parameters
         ,mim.parameter_values
         ,mim.printerfriendly
         ,NULL page_id
         ,mim.target_attr
     FROM v_menu_items mim
    WHERE mim.page_name IS NULL
      AND EXISTS
             (    SELECT '1'
                    FROM gen_menu_items mim2
                   WHERE mim2.id IN (SELECT id
                                       FROM v_auth_pages)
              CONNECT BY PRIOR id = parent_id
              START WITH parent_id = mim.id)
;

/
QUIT
