CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCUITC51_VW" ("ONDE_ID", "STGR_ID", "SORTEER", "KOLOM1", "KOLOM2", "KOLOM3", "KOLOM4", "KOLOM5", "KOLOM6", "KOLOM7") AS
  SELECT meti.onde_id
,      meti.stgr_id
,      -2 sorteer
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 1 ) kolom1
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 2 ) kolom2
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 3 ) kolom3
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 4 ) kolom4
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 5 ) kolom5
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 6 ) kolom6
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 7 ) kolom7
from   bas_metingen meti
where  onde_id = onde_id
and    stgr_id = stgr_id
and    kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 1 ) is not null
union
select meti.onde_id
,      meti.stgr_id
,      -1
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -1, -1, 1 ) kolom1
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -1, -1, 2 ) kolom2
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -1, -1, 3 ) kolom3
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -1, -1, 4 ) kolom4
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -1, -1, 5 ) kolom5
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -1, -1, 6 ) kolom6
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, -1, -1, 7 ) kolom7
from   bas_metingen meti
where  onde_id = onde_id
and    stgr_id = stgr_id
and    kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 1 ) is not null
union
select meti.onde_id
,      meti.stgr_id
,      meet.volgorde
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, meti.stgr_id, meet.stof_id, 1 ) kolom1
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, meti.stgr_id, meet.stof_id, 2 ) kolom2
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, meti.stgr_id, meet.stof_id, 3 ) kolom3
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, meti.stgr_id, meet.stof_id, 4 ) kolom4
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, meti.stgr_id, meet.stof_id, 5 ) kolom5
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, meti.stgr_id, meet.stof_id, 6 ) kolom6
,      kgc_uits_00.uitslagbrief_controle( meti.onde_id, meti.stgr_id, meet.stof_id, 7 ) kolom7
from   bas_metingen meti
,      bas_meetwaarden meet
where  meti.meti_id = meet.meti_id
and    meti.onde_id = meti.onde_id
and    meti.stgr_id = meti.stgr_id
and    kgc_uits_00.uitslagbrief_controle( meti.onde_id, -2, -2, 1 ) is not null
 ;

/
QUIT
