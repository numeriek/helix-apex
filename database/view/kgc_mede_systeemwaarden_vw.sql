CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MEDE_SYSTEEMWAARDEN_VW" ("CODE", "OMSCHRIJVING", "WAARDE") AS
  SELECT code
,      omschrijving
,      nvl( kgc_sypa_00.medewerker_waarde( sypa_id ), standaard_waarde ) waarde
from   kgc_systeem_parameters
Where publiek = 'J'
union
select 'DB-ROL'
,           'Verleende rol (ivm. menu-opties)'
,           granted_role
from   user_role_privs;

/
QUIT
