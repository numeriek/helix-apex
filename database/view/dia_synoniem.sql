CREATE OR REPLACE FORCE VIEW "HELIX"."DIA_SYNONIEM" ("CODE", "OMSCHRIJVING", "VERVALLEN", "EDITIE_INVOER", "EDITIE_WIJZIGING", "EDITIE_VERVALLEN") AS
  SELECT ziektecode code
, ziektesynoniem omschrijving
, DECODE (editie_vervallen, NULL, 'N', 'J') vervallen
, editie_invoer
, editie_wijziging
, editie_vervallen
FROM  cin_syn
 ;

/
QUIT
