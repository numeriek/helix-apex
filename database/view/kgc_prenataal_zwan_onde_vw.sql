CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_PRENATAAL_ZWAN_ONDE_VW" ("ZWAN_ID", "ONDE_ID", "PERS_ID", "FOET_ID", "KAFD_ID", "ONGR_ID", "KAFD_CODE", "ONGR_CODE", "DATUM_BINNEN", "VREUGDEBRIEF") AS
  SELECT zwan.zwan_id,
             onde.onde_id,
             onde.pers_id,
             onde.foet_id,
             onde.kafd_id,
             onde.ongr_id,
             kafd.code kafd_code,
             ongr.code ongr_code,
             onde.datum_binnen,
             zwan.vreugdebrief
      FROM kgc_kgc_afdelingen kafd,
             kgc_onderzoeksgroepen ongr,
             kgc_onderzoeken onde,
             kgc_zwangerschappen zwan
     WHERE zwan.pers_id = onde.pers_id
             AND onde.datum_binnen BETWEEN zwan.datum_aterm - 280
                                                AND zwan.datum_aterm
             AND onde.kafd_id = kafd.kafd_id
             AND onde.ongr_id = ongr.ongr_id
             AND kafd.code in ('CYTO', 'GENOOM')
             AND ongr.code LIKE 'PRE%'
 ;

/
QUIT
