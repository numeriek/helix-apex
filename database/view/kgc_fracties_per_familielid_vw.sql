CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FRACTIES_PER_FAMILIELID_VW" ("FAMILIENUMMER", "FAMILIENAAM", "PERSOON", "GEBOORTEDATUM", "GESLACHT", "FRACTIENUMMER", "ONDERZOEKNUMMER", "AFDELING", "FAMI_ID", "PERS_ID", "FRAC_ID", "MONS_ID", "ONDE_ID") AS
  SELECT DISTINCT faov.familienummer , faov.fami_naam , faov.pers_aanspreken , faov.pers_geboortedatum , faov.pers_geslacht , faov.fractienummer , faov.onderzoeknr , faov.kafd_code , faov.fami_id , faov.pers_id , faov.frac_id , faov.mons_id , faov.onde_id FROM kgc_familie_overzicht_vw faov
 ;

/
QUIT
