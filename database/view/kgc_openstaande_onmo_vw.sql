CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_OPENSTAANDE_ONMO_VW" ("KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ONDE_DATUM_BINNEN", "MONSTERNUMMER", "MATE_CODE", "INDI_CODE", "STATUS", "ONDE_ID", "MONS_ID", "PERS_ID", "KAFD_ID", "ONGR_ID") AS
  SELECT kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      onde.datum_binnen onde_datum_binnen
,      mons.monsternummer
,      mate.code mate_code
,      kgc_indi_00.indi_code( onde.onde_id ) indi_code
,      onde.status
,      onmo.onde_id
,      onmo.mons_id
,      onde.pers_id
,      onde.kafd_id
,      onde.ongr_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      kgc_onderzoek_monsters onmo
where onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.pers_id = pers.pers_id
and    onde.afgerond = 'N'
and    onde.onde_id = onmo.onde_id (+)
 and    onmo.mons_id = mons.mons_id (+)
and    mons.mate_id = mate.mate_id (+)
 ;

/
QUIT
