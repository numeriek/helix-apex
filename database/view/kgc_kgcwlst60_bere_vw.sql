CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCWLST60_BERE_VW" ("FORM_CODE", "FORM_OMSCHRIJVING", "WLST_ID", "BEWY_ID", "STGR_ID") AS
  SELECT frmu.code
,      frmu.omschrijving
,      wlit.wlst_id
,      bere.bewy_id
,      meti.stgr_id
FROM   bas_formules            frmu
,      bas_berekenings_wijzen  bewy
,      bas_berekeningen        bere
,      bas_meetwaarden         meet
,      kgc_werklijst_items     wlit
,      bas_metingen            meti
WHERE  wlit.meet_id = meet.meet_id
AND    meet.meti_id = bere.meti_id
AND    meti.meti_id = meet.meti_id
AND    bere.bewy_id = bewy.bewy_id
AND    bewy.form_id = frmu.form_id
GROUP BY frmu.code
,        frmu.omschrijving
,        wlit.wlst_id
,        bere.bewy_id
,        meti.stgr_id
 ;

/
QUIT
