CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_QUERY_BEWAREN_VW" ("BESCHRIJVING", "VIEW_NAAM") AS
  SELECT distinct beschrijving
, view_naam
from kgc_query_bewaren
group by beschrijving
, view_naam
 ;

/
QUIT
