CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCOPTE51_KOL_VW" ("ONDE_NR", "MATE_CODE", "FRAC_NR", "METI_DAT_AANM", "METI_ID", "FAMI_NR") AS
  SELECT kol.onde_nr
, kol.mate_code
, kol.frac_nr
, kol.meti_dat_aanm
, kol.meti_id
, kol.fami_nr
  FROM TABLE(kgc_opte_00.f_retourneer_omf_set) kol
  ORDER BY
  kol.onde_nr
, kol.mate_code
, kol.frac_nr
, kol.meti_dat_aanm DESC
, kol.meti_id DESC
 ;

/
QUIT
