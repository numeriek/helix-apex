CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_OPWERKLIJST_VW" ("MONS_ID", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "MONS_DATUM_AANMELDING", "PERSOON_INFO", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "MONS_COMMENTAAR") AS
  SELECT mons.mons_id
,      kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr
,      mons.monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      mons.datum_aanmelding mons_datum_aanmelding
,      kgc_pers_00.persoon_info
       ( mons.mons_id
       , onde.onde_id
       , mons.pers_id
       , 'LANG'
       ) persoon_info
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      mons.commentaar mons_commentaar
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
where onmo.onde_id = onde.onde_id
and    onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onmo.mons_id = mons.mons_id
and    mons.pers_id = pers.pers_id
and    mons.mate_id = mate.mate_id
and    mons.alleen_voor_opslag = 'N'
and  ( exists
       ( select null
         from   bas_fracties frac
         where  frac.mons_id = mons.mons_id
         and    nvl( frac.status, 'A' ) = 'A'
       )
    or not exists
       ( select null
         from   bas_fracties frac
         where  frac.mons_id = mons.mons_id
       )
     )
 ;

/
QUIT
