CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_RETE_CONTEXT_VW" ("RETE_ID", "KAFD_ID", "METI_ID", "CODE", "OMSCHRIJVING") AS
  SELECT rete.rete_id
,      rete.kafd_id
,      meti.meti_id
,      rete.code
,      rete.omschrijving
FROM   kgc_resultaat_teksten rete
,      kgc_teksten_context   teco
,      bas_metingen          meti
WHERE  rete.rete_id = teco.rete_id
AND    meti.stgr_id = teco.stgr_id
AND    rete.vervallen = 'N'
UNION ALL
SELECT rete.rete_id
,      rete.kafd_id
,      meti.meti_id
,      rete.code
,      rete.omschrijving
FROM   kgc_resultaat_teksten rete
,      bas_metingen          meti
,      bas_fracties          frac
WHERE  frac.frac_id   = meti.frac_id
AND    frac.kafd_id   = rete.kafd_id
AND    rete.vervallen = 'N'
AND    not exists
       ( SELECT null
         FROM   kgc_resultaat_teksten rete2
         ,      kgc_teksten_context   teco2
         ,      bas_metingen          meti2
         WHERE  rete2.rete_id = teco2.rete_id
         AND    meti2.stgr_id = teco2.stgr_id
         AND    rete2.vervallen = 'N'
         AND    meti2.meti_id = meti.meti_id
       );

/
QUIT
