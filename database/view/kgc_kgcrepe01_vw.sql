CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCREPE01_VW" ("REPE_ID", "FARE_ID", "PERS_ID", "PERS_ID_RELATIE", "GESLACHT", "RELATIE", "RICHTING", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE") AS
  SELECT repe_id
,      repe.fare_id
,      repe.pers_id
,      repe.pers_id_relatie
,      fare.geslacht
,      kgc_repe_00.omschrijving_relatie ('>', repe.repe_id) relatie
,      '>' richting
,     repe.created_by
,     repe.creation_date
,     repe.last_updated_by
,     repe.last_update_date
FROM   kgc_relaties_van_persoon repe
,      kgc_familie_relaties fare
WHERE  fare.fare_id = repe.fare_id
UNION
SELECT repe_id
,      repe.fare_id
,      repe.pers_id_relatie
,      repe.pers_id
,      fare.geslacht
,      kgc_repe_00.omschrijving_relatie ('<', repe.repe_id) relatie
,      '<' richting
,     repe.created_by
,     repe.creation_date
,     repe.last_updated_by
,     repe.last_update_date
FROM   kgc_relaties_van_persoon repe
,      kgc_familie_relaties fare
WHERE  fare.fare_id = repe.fare_id
 ;

/
QUIT
