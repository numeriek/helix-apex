CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FISH_FRACTIES_VW" ("FRAC_ID", "ONMO_ID", "MONS_ID", "ONDE_ID", "PERS_ID", "ONGR_ID", "KAFD_ID", "FRACTIENUMMER", "MONSTERNUMMER", "ONDERZOEKNR", "PERSOON_INFO", "AFGEROND") AS
  SELECT frac.frac_id
,      onmo.onmo_id
,      frac.mons_id
,      onmo.onde_id
,      mons.pers_id
,      mons.ongr_id
,      mons.kafd_id
,      frac.fractienummer
,      mons.monsternummer
,      onde.onderzoeknr
,      kgc_pers_00.persoon_info( onmo.mons_id, onmo.onde_id, mons.pers_id, 'LANG' ) persoon_info
,      onde.afgerond
from   kgc_monsters mons
,      kgc_onderzoeken onde
,      bas_fracties frac
,      kgc_onderzoek_monsters onmo
where onmo.onde_id = onde.onde_id
and    onmo.mons_id = mons.mons_id
and    frac.mons_id = mons.mons_id
and    nvl( frac.status, 'A' ) in ( 'A', 'O' )
 ;

/
QUIT
