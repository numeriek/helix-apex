CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_METINGEN_VW" ("METI_ID", "ONGR_ID", "KAFD_ID", "ONDE_ID", "PERS_ID", "FRAC_ID", "MONS_ID", "STGR_ID", "MEDE_ID", "DATUM_AANMELDING", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "ONDE_GEPLANDE_EINDDATUM", "ONDE_STATUS", "SPOED", "ONDE_AFGEROND", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "FRTY_CODE", "FRTY_OMSCHRIJVING", "STGR_CODE", "STGR_OMSCHRIJVING", "MEDE_CODE", "MEDE_NAAM", "METI_AFGEROND", "ONDE_DATUM_BINNEN") AS
  SELECT meti.meti_id
,      nvl(onde.ongr_id, mons.ongr_id) ongr_id
,      ongr.kafd_id
,      meti.onde_id
,      onde.pers_id
,      meti.frac_id
,      frac.mons_id
,      meti.stgr_id
,      meti.mede_id
,      meti.datum_aanmelding
,      kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr
,      onde.geplande_einddatum onde_geplande_einddatum
,      onde.status onde_status
,      decode( onde.spoed
                     , 'J', 'J'
                    , meti.prioriteit
                    ) spoed
,      onde.afgerond onde_afgerond
,      mons.monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.fractienummer
,      frty.code frty_code
,      frty.omschrijving frty_omschrijving
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      mede.code mede_code
,      mede.naam mede_naam
,      meti.afgerond meti_afgerond
,      onde.datum_binnen onde_datum_binnen
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      bas_fractie_types frty
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      kgc_medewerkers mede
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
where  meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    nvl( onde.ongr_id, mons.ongr_id ) = ongr.ongr_id
and    ongr.kafd_id = kafd.kafd_id
and    mons.mate_id = mate.mate_id
and    frac.frty_id = frty.frty_id (+)
and    meti.onde_id = onde.onde_id (+)
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.mede_id = mede.mede_id (+)
union  all-- standaardfracties
select meti.meti_id
,      stan.ongr_id
,      ongr.kafd_id
,      meti.onde_id -- null
,      to_number(null) pers_id
,      meti.frac_id
,      to_number(null) mons_id
,      meti.stgr_id
,      meti.mede_id
,      meti.datum_aanmelding
,      kafd.code kafd_code
,      ongr.code ongr_code
,      null -- onderzoeknr
,      to_date(null) -- geplande_einddatum
,      null -- onde-status
,      meti.prioriteit
,      'N' -- onde_afgerond
,      'Controle' -- monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      stan.standaardfractienummer
,      null -- frty_code
,      null -- frty_omschrijving
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      mede.code mede_code
,      mede.naam mede_naam
,      meti.afgerond meti_afgerond
,       to_date(null) -- onde_datum_binnen
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      kgc_medewerkers mede
,      bas_standaardfracties stan
,      bas_metingen meti
where  meti.stan_id = stan.stan_id
and    stan.ongr_id = ongr.ongr_id
and    ongr.kafd_id = kafd.kafd_id
and    stan.mate_id = mate.mate_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.mede_id = mede.mede_id (+);

/
QUIT
