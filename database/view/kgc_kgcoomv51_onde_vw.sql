CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCOOMV51_ONDE_VW" ("KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ONDE_DATUM_BINNEN", "ONDE_GEPLANDE_EINDDATUM", "STATUS", "ONDERZOEKSWIJZE_OMS", "ONDE_ONDERZOEKSWIJZE", "PERS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID") AS
  SELECT kafd.code                kafd_code
,      ongr.code                ongr_code
,      onde.onderzoeknr
,      pers.aanspreken          pers_aanspreken
,      pers.geboortedatum       pers_geboortedatum
,      pers.geslacht            pers_geslacht
,      onde.datum_binnen        onde_datum_binnen
,      onde.geplande_einddatum  onde_geplande_einddatum
,      onde.status
,      kgc_onwy_00.onwy_omschrijving( onde.onde_id ) onderzoekswijze_oms
,      onde.onderzoekswijze     onde_onderzoekswijze
,      onde.pers_id
,      onde.kafd_id
,      onde.ongr_id
,      onde.onde_id
FROM   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_personen pers
,      kgc_onderzoeken onde
WHERE  onde.kafd_id = kafd.kafd_id
AND    onde.ongr_id = ongr.ongr_id
AND    onde.pers_id = pers.pers_id
AND    onde.afgerond = 'N'
 ;

/
QUIT
