CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_AFGERONDE_BEREKENINGEN_VW" ("MEET_ID", "BERE_ID", "BEWY_ID", "BETY_ID", "FORM_ID", "STOF_ID", "METI_ID", "FRAC_ID", "MONS_ID", "ONMO_ID", "ONDE_ID", "ONDERZOEKNR", "MONSTERNUMMER", "FRACTIENUMMER", "STGR_CODE", "STGR_OMSCHRIJVING", "STOF_CODE", "STOF_OMSCHRIJVING", "MEET_REKEN", "BETY_CODE", "BETY_OMSCHRIJVING") AS
  SELECT /*+RULE */ -- berekeningen op meetwaarden/stoftesten
       meet.meet_id
,      bere.bere_id
,      bere.bewy_id
,      bewy.bety_id
,      bewy.form_id
,      meet.stof_id
,      meti.meti_id
,      meti.frac_id
,      frac.mons_id
,      meti.onmo_id
,      meti.onde_id
,      onde.onderzoeknr
,      mons.monsternummer
,      frac.fractienummer
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.meet_reken
,      bety.code bety_code
,      bety.omschrijving bety_omschrijving
from   bas_berekening_types bety
,      bas_berekenings_wijzen bewy
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      bas_berekeningen bere
,      bas_meetwaarden meet
where meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.onde_id = onde.onde_id
and    meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    meet.meet_id = bere.meet_id
and    bere.bety_id = bety.bety_id
and    bere.bewy_id = bewy.bewy_id
and    onde.afgerond = 'N'
and    decode( meti.afgerond
             , 'J', 'J'
             , meet.afgerond
             ) = 'J'
union  all -- directe berekeningen
select meet.meet_id
,      to_number(null) -- bere_id
,      bewy.bewy_id
,      bewy.bety_id
,      bewy.form_id
,      meet.stof_id
,      meti.meti_id
,      meti.frac_id
,      frac.mons_id
,      meti.onmo_id
,      meti.onde_id
,      onde.onderzoeknr
,      mons.monsternummer
,      frac.fractienummer
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.meet_reken
,      bety.code bety_code
,      bety.omschrijving bety_omschrijving
from   bas_berekening_types bety
,      bas_berekenings_wijzen bewy
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
where  meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.onde_id = onde.onde_id
and    meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    meet.stof_id = bewy.stof_id
and    bewy.bety_id = bety.bety_id
and    bety.direct = 'J'
and    meet.meet_reken = 'R'
and    onde.afgerond = 'N'
and    decode( meti.afgerond
             , 'J', 'J'
             , meet.afgerond
             ) = 'J'
union  all -- berekeningen op metingen/protocollen
select to_number(null) -- meet_id
,      bere.bere_id
,      bere.bewy_id
,      bewy.bety_id
,      bewy.form_id
,      to_number(null) -- stof_id
,      meti.meti_id
,      meti.frac_id
,      frac.mons_id
,      meti.onmo_id
,      meti.onde_id
,      onde.onderzoeknr
,      mons.monsternummer
,      frac.fractienummer
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      null -- stof_code
,      null -- stof_omschrijving
,      null -- meet_reken
,      bety.code bety_code
,      bety.omschrijving bety_omschrijving
from   bas_berekening_types bety
,      bas_berekenings_wijzen bewy
,      kgc_stoftestgroepen stgr
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      bas_berekeningen bere
where  meti.stgr_id = stgr.stgr_id
and    meti.onde_id = onde.onde_id
and    meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    meti.meti_id = bere.meti_id
and    bere.bety_id = bety.bety_id
and    bere.bewy_id = bewy.bewy_id
and    onde.afgerond = 'N'
and    meti.afgerond = 'J'
 ;

/
QUIT
