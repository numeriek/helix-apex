CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAGCONTROLE_WAARDEN_VW" ("PERS_ID", "STGR_ID", "ONDE_ID", "MEET_ID", "VOLGORDE", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "NORMAALWAARDEN", "MONSTERNUMMER", "DATUM_AFNAME", "MEETWAARDE", "MONSTERNUMMER_VG", "DATUM_AFNAME_VG", "MEETWAARDE_VG") AS
  SELECT mons.pers_id
,      meti.stgr_id
,      meti.onde_id
,      meet.meet_id
,      meet.volgorde
,      stof.stof_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      bas_meet_00.normaalwaarden( meet.meet_id )||' '||meet.meeteenheid normaalwaarden
,      mons.monsternummer
,      mons.datum_afname
,      meet.tonen_als meetwaarde
,      mons_vg.monsternummer monsternummer_vg
,      mons_vg.datum_afname datum_afname_vg
,      meet_vg.tonen_als meetwaarde
from   kgc_stoftesten stof
,      kgc_monsters mons_vg
,      kgc_onderzoek_monsters onmo_vg
,      bas_meetwaarden meet_vg
,      bas_metingen meti_vg
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
,      bas_meetwaarden meet
,      bas_metingen meti
where  meti_vg.onmo_id = onmo_vg.onmo_id
and    onmo_vg.mons_id = mons_vg.mons_id
and    meti_vg.meti_id = meet_vg.meti_id
and    meet_vg.stof_id = stof.stof_id
and    onmo.mons_id <> onmo_vg.mons_id
and    meti.onmo_id <> meti_vg.onmo_id
and    meti.meti_id <> meti_vg.meti_id
and    meet.meet_id <> meet_vg.meet_id
and    meti.onmo_id = onmo.onmo_id
and    onmo.mons_id = mons.mons_id
and    meti.meti_id = meet.meti_id
and    meet.stof_id = stof.stof_id
 ;

/
QUIT
