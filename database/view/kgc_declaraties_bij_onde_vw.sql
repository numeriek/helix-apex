CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_DECLARATIES_BIJ_ONDE_VW" ("ONDE_ID", "ONDERZOEKNR", "AFGEROND", "DATUM_AUTORISATIE", "DECL_ID", "ENTITEIT", "ID", "DEEN_ID", "DATUM", "KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "PERS_ID", "PERSOON_INFO", "DECLAREREN", "DEWY_ID", "DEWY_CODE", "STATUS", "DATUM_VERWERKING", "INGR_ID", "MACHTIGINGSINDICATIE", "BETREFT_TEKST", "TOELICHTING", "VERRICHTINGCODE", "CREATION_DATE", "CREATED_BY", "LAST_UPDATE_DATE", "LAST_UPDATED_BY") AS
  SELECT onde.onde_id
,      onde.onderzoeknr
,      onde.afgerond
,      onde.datum_autorisatie
,      decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.deen_id
,      decl.datum
,      decl.kafd_id
,      kafd.code kafd_code
,      decl.ongr_id
,      ongr.code ongr_code
,      decl.pers_id
,      kgc_pers_00.persoon_info( null, null, decl.pers_id, 'LANG' ) persoon_info
,      decl.declareren
,      decl.dewy_id
,      dewy.code dewy_code
,      decl.status
,      decl.datum_verwerking
,      decl.ingr_id
,      ingr.machtigingsindicatie
,      decl.betreft_tekst
,      decl.toelichting
,      decl.verrichtingcode
,      decl.creation_date
,      decl.created_by
,      decl.last_update_date
,      decl.last_updated_by
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_declaratiewijzen dewy
,      kgc_indicatie_groepen ingr
,      kgc_onderzoeken onde
,      kgc_declaraties decl
where decl.kafd_id = kafd.kafd_id
and    decl.ongr_id = ongr.ongr_id
and    decl.dewy_id = dewy.dewy_id (+)
and    decl.ingr_id = ingr.ingr_id (+)
and    decl.entiteit = 'ONDE'
and    decl.id = onde.onde_id
union all
select onde.onde_id
,      onde.onderzoeknr
,      onde.afgerond
,      onde.datum_autorisatie
,      decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.deen_id
,      decl.datum
,      decl.kafd_id
,      kafd.code kafd_code
,      decl.ongr_id
,      ongr.code ongr_code
,      decl.pers_id
,      kgc_pers_00.persoon_info( null, null, decl.pers_id, 'LANG' ) persoon_info
,      decl.declareren
,      decl.dewy_id
,      dewy.code dewy_code
,      decl.status
,      decl.datum_verwerking
,      decl.ingr_id
,      ingr.machtigingsindicatie
,      decl.betreft_tekst
,      decl.toelichting
,      decl.verrichtingcode
,      decl.creation_date
,      decl.created_by
,      decl.last_update_date
,      decl.last_updated_by
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_declaratiewijzen dewy
,      kgc_indicatie_groepen ingr
,      kgc_onderzoeken onde
,      kgc_onderzoek_betrokkenen onbe
,      kgc_declaraties decl
where  decl.kafd_id = kafd.kafd_id
and    decl.ongr_id = ongr.ongr_id
and    decl.dewy_id = dewy.dewy_id (+)
and    decl.ingr_id = ingr.ingr_id (+)
and    decl.entiteit = 'ONBE'
and    decl.id = onbe.onbe_id
and    onbe.onde_id = onde.onde_id
union all
select onde.onde_id
,      onde.onderzoeknr
,      onde.afgerond
,      onde.datum_autorisatie
,      decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.deen_id
,      decl.datum
,      decl.kafd_id
,      kafd.code kafd_code
,      decl.ongr_id
,      ongr.code ongr_code
,      decl.pers_id
,      kgc_pers_00.persoon_info( null, null, decl.pers_id, 'LANG' ) persoon_info
,      decl.declareren
,      decl.dewy_id
,      dewy.code dewy_code
,      decl.status
,      decl.datum_verwerking
,      decl.ingr_id
,      ingr.machtigingsindicatie
,      decl.betreft_tekst
,      decl.toelichting
,      decl.verrichtingcode
,      decl.creation_date
,      decl.created_by
,      decl.last_update_date
,      decl.last_updated_by
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_declaratiewijzen dewy
,      kgc_indicatie_groepen ingr
,      kgc_onderzoeken onde
,      bas_metingen meti
,      kgc_declaraties decl
where  decl.kafd_id = kafd.kafd_id
and    decl.ongr_id = ongr.ongr_id
and    decl.dewy_id = dewy.dewy_id (+)
and    decl.ingr_id = ingr.ingr_id (+)
and    decl.entiteit = 'METI'
and    decl.id = meti.meti_id
and    meti.onde_id = onde.onde_id
union all
select onde.onde_id
,      onde.onderzoeknr
,      onde.afgerond
,      onde.datum_autorisatie
,      decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.deen_id
,      decl.datum
,      decl.kafd_id
,      kafd.code kafd_code
,      decl.ongr_id
,      ongr.code ongr_code
,      decl.pers_id
,      kgc_pers_00.persoon_info( null, null, decl.pers_id, 'LANG' ) persoon_info
,      decl.declareren
,      decl.dewy_id
,      dewy.code dewy_code
,      decl.status
,      decl.datum_verwerking
,      decl.ingr_id
,      ingr.machtigingsindicatie
,      decl.betreft_tekst
,      decl.toelichting
,      decl.verrichtingcode
,      decl.creation_date
,      decl.created_by
,      decl.last_update_date
,      decl.last_updated_by
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_declaratiewijzen dewy
,      kgc_indicatie_groepen ingr
,      kgc_onderzoeken onde
,      kgc_gesprekken gesp
,      kgc_declaraties decl
where  decl.kafd_id = kafd.kafd_id
and    decl.ongr_id = ongr.ongr_id
and    decl.dewy_id = dewy.dewy_id (+)
and    decl.ingr_id = ingr.ingr_id (+)
and    decl.entiteit = 'GESP'
and    decl.id = gesp.gesp_id
and    gesp.onde_id = onde.onde_id
union all -- niet onderzoeksgebonden (monsterafnames)
select to_number(null)
,      mons.monsternummer
,      'J'
,      mons.datum_aanmelding -- ??
,      decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.deen_id
,      decl.datum
,      decl.kafd_id
,      kafd.code kafd_code
,      decl.ongr_id
,      ongr.code ongr_code
,      decl.pers_id
,      kgc_pers_00.persoon_info( null, null, decl.pers_id, 'LANG' ) persoon_info
,      decl.declareren
,      decl.dewy_id
,      dewy.code dewy_code
,      decl.status
,      decl.datum_verwerking
,      decl.ingr_id
,      ingr.machtigingsindicatie
,      decl.betreft_tekst
,      decl.toelichting
,      decl.verrichtingcode
,      decl.creation_date
,      decl.created_by
,      decl.last_update_date
,      decl.last_updated_by
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_declaratiewijzen dewy
,      kgc_indicatie_groepen ingr
,      kgc_monsters mons
,      kgc_declaraties decl
where  decl.kafd_id = kafd.kafd_id
and    decl.ongr_id = ongr.ongr_id
and    decl.dewy_id = dewy.dewy_id (+)
and    decl.ingr_id = ingr.ingr_id (+)
and    decl.entiteit = 'MONS'
and    decl.id = mons.mons_id
 ;

/
QUIT
