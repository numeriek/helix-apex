CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BASMEET53_MEET_VW" ("STOF_CODE", "STOF_OMSCHRIJVING", "VOLGORDE", "MEETWAARDE", "MEETEENHEID", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "COMMENTAAR", "AFGEROND", "METI_ID", "MEET_ID", "STOF_ID", "UITSLAG", "UITSLAGEENHEID") AS
  SELECT stof.code stof_code
, stof.omschrijving stof_omschrijving
, meet.volgorde
, meet.meetwaarde
, meet.meeteenheid
, meet.normaalwaarde_ondergrens
, meet.normaalwaarde_bovengrens
, meet.commentaar
, meet.afgerond
, meet.meti_id
, meet.meet_id
, meet.stof_id
, NVL( bas_meet_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'UITSLAG' )
     , meet.meetwaarde
     )  uitslag
, NVL( bas_meet_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'EENHEID' )
     , meet.meeteenheid
     ) uitslageenheid
FROM kgc_stoftesten stof
, bas_meetwaarden meet
  WHERE meet.stof_id = stof.stof_id
 ;

/
QUIT
