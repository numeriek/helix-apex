CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_VIEW_COLUMNS" ("VIEW_NAME", "COLUMN_NAME") AS
  SELECT col.table_name
,      col.column_name
FROM   all_tab_columns  col
,      kgc_views        vw
  WHERE col.table_name   = vw.view_name
and col.owner = kgc_sypa_00.systeem_waarde( 'APPLICATIE_EIGENAAR' )
 ;

/
QUIT
