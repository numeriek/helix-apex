CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FRACTIE_TYPES_VW" ("FRTY_ID", "ONGR_ID", "KAFD_ID", "CODE", "OMSCHRIJVING", "VERVALLEN", "ONGR_CODE", "ONGR_OMSCHRIJVING") AS
  SELECT frty.frty_id
,      frty.ongr_id
,      ongr.kafd_id
,      frty.code
,      frty.omschrijving||' ('||ongr.omschrijving||')' omschrijving
,      frty.vervallen
,      ongr.code
,      ongr.omschrijving
from   kgc_onderzoeksgroepen ongr
,      bas_fractie_types frty
  WHERE frty.ongr_id = ongr.ongr_id
 ;

/
QUIT
