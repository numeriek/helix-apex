CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_HERINNERINGSBRIEF_MG_VW" ("GEADRESSEERDE", "REFERENTIE", "BETREFT", "OPENINGSZIN", "ONDERTEKENAAR", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "EERDER_GEPRINT", "ID", "OBMG_ID", "ONBE_ID", "ONDE_ID", "RELA_ID", "PERS_ID", "MEDE_ID", "KAFD_ID", "ONGR_ID", "HERINNERING") AS
  SELECT NVL( OBMG.ADRESSERING
          , KGC_ADRES_00.RELATIE( RELA.RELA_ID, 'J' )
          )
,      MEDE.CODE||'/'||ONDE.ONDERZOEKNR
,      KGC_PERS_00.INFO( PERS.PERS_ID )
,      'Op '||TO_CHAR(kgc_brie_00.verzoekmg_geprint_op(obmg.obmg_id),'DD-MM-YYYY')||' '
            ||'stuurden wij u het verzoek om medische gegevens van '
            ||DECODE( pers.geslacht, 'M', 'mijnheer ', 'V', 'mevrouw ', NULL )
            ||pers.aanspreken
            ||', bovengenoemd.'
,      NVL(MEDE.FORMELE_NAAM,MEDE.NAAM)
,      KAFD.CODE
,      ONGR.CODE
,      ONDE.ONDERZOEKNR
,      KGC_BRIE_00.HERINNERMG_REEDS_GEPRINT( OBMG.OBMG_ID )
,      OBMG.OBMG_ID
,      OBMG.OBMG_ID
,      ONBE.ONBE_ID
,      ONDE.ONDE_ID
,      RELA.RELA_ID
,      PERS.PERS_ID
,      MEDE.MEDE_ID
,      KAFD.KAFD_ID
,      ONGR.ONGR_ID
,      OBMG.HERINNERING
FROM   KGC_KGC_AFDELINGEN        KAFD
,      KGC_ONDERZOEKSGROEPEN     ONGR
,      KGC_MEDEWERKERS           MEDE
,      KGC_PERSONEN              PERS
,      KGC_RELATIES              RELA
,      KGC_ONDERZOEKEN           ONDE
,      KGC_ONDERZOEK_BETROKKENEN ONBE
,      KGC_ONBE_MED_GEGEVENS     OBMG
WHERE  OBMG.ONBE_ID              = ONBE.ONBE_ID
AND    ONBE.ONDE_ID              = ONDE.ONDE_ID
AND    OBMG.RELA_ID              = RELA.RELA_ID (+)
AND    ONBE.PERS_ID              = PERS.PERS_ID
AND    ONDE.MEDE_ID_BEOORDELAAR  = MEDE.MEDE_ID
AND    ONDE.KAFD_ID              = KAFD.KAFD_ID
AND    ONDE.ONGR_ID              = ONGR.ONGR_ID
AND    OBMG.AFGEHANDELD          = 'N'
AND    OBMG.ONTVANGEN            IS NULL
AND    OBMG.HERINNERING          IS NOT NULL
AND    OBMG.HERINNERING          <= TRUNC(SYSDATE)
 ;

/
QUIT
