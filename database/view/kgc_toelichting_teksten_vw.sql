CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_TOELICHTING_TEKSTEN_VW" ("KAFD_ID", "ENTITEIT", "ID", "CODE", "TEKST") AS
  SELECT cote.kafd_id
,     'CONC' entiteit
,     cote.conc_id id
,     cote.code
,     cote.omschrijving tekst
from   kgc_conclusie_teksten cote
,          kgc_systeem_parameters sypa
where sypa.code = 'TOELICHTING_TEKSTEN'
and      instr( nvl( kgc_sypa_00.medewerker_waarde( sypa.sypa_id )
                             , sypa.standaard_waarde
                             )
                     , 'C'
                     ) > 0
and      cote.vervallen = 'N'
union all
select rete.kafd_id
,           'RETE'
,           rete.rete_id
,           rete.code
,           rete.omschrijving
from   kgc_resultaat_teksten rete
,           kgc_systeem_parameters sypa
where sypa.code = 'TOELICHTING_TEKSTEN'
and      instr( nvl( kgc_sypa_00.medewerker_waarde( sypa.sypa_id )
                             , sypa.standaard_waarde
                             )
                      , 'R'
                      ) > 0
and      rete.vervallen = 'N'
union all
select tote.kafd_id
,           'TOTE'
,           tote.tote_id
,           tote.code
,           tote.omschrijving
from   kgc_toelichting_teksten tote
,           kgc_systeem_parameters sypa
where sypa.code = 'TOELICHTING_TEKSTEN'
and      instr( nvl( kgc_sypa_00.medewerker_waarde( sypa.sypa_id )
                              , sypa.standaard_waarde
                              )
                       , 'T'
                       ) > 0
and     tote.vervallen = 'N'
 ;

/
QUIT
