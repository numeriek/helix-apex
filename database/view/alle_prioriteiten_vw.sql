CREATE OR REPLACE FORCE VIEW "HELIX"."ALLE_PRIORITEITEN_VW" ("BRON", "PRIO_ID", "CODE", "OMSCHRIJVING", "VERVALLEN") AS
  select 'PROD'
,      prio_id
,      code
,      omschrijving
,      vervallen
from kgc_prioriteiten
 ;

/
QUIT
