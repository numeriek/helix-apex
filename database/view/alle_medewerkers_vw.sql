CREATE OR REPLACE FORCE VIEW "HELIX"."ALLE_MEDEWERKERS_VW" ("BRON", "MEDE_ID", "CODE", "NAAM", "ORACLE_UID", "VERVALLEN") AS
  select 'PROD'
, mede_id
, code
, naam
, oracle_uid
, vervallen
from helix.kgc_medewerkers
 ;

/
QUIT
