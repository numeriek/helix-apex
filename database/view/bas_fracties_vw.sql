CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_FRACTIES_VW" ("FRAC_ID", "MONS_ID", "ONDE_ID", "ONGR_ID", "KAFD_ID", "FRACTIENUMMER", "MONSTERNUMMER", "ONDERZOEKNR", "STATUS") AS
  SELECT frac.frac_id
,      frac.mons_id
,      onmo.onde_id
,      mons.ongr_id
,      mons.kafd_id
,      frac.fractienummer
,      mons.monsternummer
,      onde.onderzoeknr
,      frac.status
from  kgc_monsters mons
,          kgc_onderzoeken onde
,          bas_fracties frac
,          kgc_onderzoek_monsters onmo
where onmo.onde_id = onde.onde_id
and      onmo.mons_id = mons.mons_id
and     frac.mons_id = mons.mons_id
 ;

/
QUIT
