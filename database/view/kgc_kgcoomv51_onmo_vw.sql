CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCOOMV51_ONMO_VW" ("MONSTERNUMMER", "MATE_CODE", "ONDE_ID", "MONS_ID", "ONMO_ID") AS
  SELECT mons.monsternummer
,      mate.code            mate_code
,      onmo.onde_id
,      onmo.mons_id
,      onmo.onmo_id
FROM   kgc_materialen mate
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
WHERE  onmo.mons_id = mons.mons_id (+)
AND    mons.mate_id = mate.mate_id (+)
 ;

/
QUIT
