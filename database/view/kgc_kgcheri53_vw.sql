CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCHERI53_VW" ("GEADRESSEERDE", "REFERENTIE", "BETREFT", "DATUM_OORSPRONKELIJKE_BRIEF", "GESLACHT", "AANSPREKEN", "ONDERTEKENAAR", "EERDER_GEPRINT", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "HERINNERING", "BRIEFDATUM", "ID", "OBMG_ID", "ONBE_ID", "ONDE_ID", "RELA_ID", "PERS_ID", "MEDE_ID", "KAFD_ID", "ONGR_ID") AS
  SELECT heri.geadresseerde
,      heri.referentie
,      betreft
,
TO_CHAR(kgc_brie_00.verzoekmg_geprint_op(heri.obmg_id),
'DD-MM-YYYY')
,      DECODE( pers.geslacht, 'M', 'mijnheer ', 'V', 'mevrouw '
, NULL )
,      pers.aanspreken
,      heri.ondertekenaar
,      kgc_brie_00.herinnermg_reeds_geprint( heri.obmg_id )
,      heri.kafd_code
,      heri.ongr_code
,      heri.onderzoeknr
,      heri.herinnering
,      TO_CHAR( SYSDATE, 'DD-MM-YYYY' )
,      heri.obmg_id
,      heri.obmg_id
,      heri.onbe_id
,      heri.onde_id
,      heri.rela_id
,      heri.pers_id
,      heri.mede_id
,      heri.kafd_id
,      heri.ongr_id
FROM   kgc_personen  pers
,      kgc_herinneringsbrief_mg_vw  heri
WHERE  heri.pers_id = pers.pers_id
 ;

/
QUIT
