CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCWLST04_VW" ("VOLGNUMMER", "ONDERZOEKNR", "MONSTERNUMMER", "FRACTIENUMMER", "LEEFTIJD", "PERS_GESLACHT", "PERSOON_INFO", "COND_CODE", "COND_OMSCHRIJVING", "INDI_CODE", "INDI_OMSCHRIJVING", "MEERDERE_PROTOCOLLEN", "PRIORITEIT", "ONDE_AFGEROND", "AFGEROND", "STOF_CODE", "STOF_OMSCHRIJVING", "STGR_CODE", "STGR_OMSCHRIJVING", "MEETWAARDE", "MEET_COMMENTAAR", "WLST_ID", "MEET_ID", "MWST_ID", "STOF_ID", "MEST_ID", "METI_ID", "ONDE_ID", "FRAC_ID", "STAN_ID", "MONS_ID", "PERS_ID", "KAFD_ID", "ONGR_ID", "RELA_ID") AS
  SELECT wliv.volgnummer
,      onde.onderzoeknr
,      mons.monsternummer
,      nvl( frac.fractienummer, stan.standaardfractienummer ) fractienummer
,      kgc_leef_00.omschrijving( mons.leeftijd, mons.mons_id ) leeftijd
,      pers.geslacht pers_geslacht
,      nvl( kgc_pers_00.persoon_info( mons.mons_id, onde.pers_id )
          , 'Standaardfractie'
          ) persoon_info
,      cond.code cond_code
,      cond.omschrijving cond_omschrijving
,      kgc_indi_00.indi_code( onde.onde_id ) indi_code
,      kgc_indi_00.indi_omschrijving( onde.onde_id ) indi_omschrijving
,      kgc_mons_00.meerdere_protocollen( mons.mons_id ) meerdere_protocollen
,      decode( onde.spoed
            , 'J', 'J'
            , decode(meti.prioriteit
                    ,'J','J'
                    ,meet.spoed
                    )
            ) prioriteit
,      onde.afgerond onde_afgerond
,      meet.afgerond afgerond
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meet.meetwaarde meetwaarde
,      meet.commentaar meet_commentaar
,      wliv.wlst_id
,      meet.meet_id
,      meet.mwst_id
,      meet.stof_id
,      meet.mest_id
,      meti.meti_id
,      meti.onde_id
,      meti.frac_id
,      meti.stan_id
,      frac.mons_id
,      mons.pers_id
,      ongr.kafd_id
,      ongr.ongr_id
,      onde.rela_id
from   kgc_onderzoeksgroepen ongr
,      kgc_condities cond
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      kgc_onderzoeken onde
,      bas_standaardfracties stan
,      bas_fracties frac
,      kgc_monsters mons
,      kgc_personen pers
,      kgc_werklijst_items wliv
,      bas_metingen meti
,      bas_meetwaarden meet
where  wliv.meet_id = meet.meet_id
and    meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.onde_id = onde.onde_id (+)
and    meti.stan_id = stan.stan_id (+)
and    meti.frac_id = frac.frac_id (+)
and    frac.mons_id = mons.mons_id (+)
and    mons.pers_id = pers.pers_id (+)
and    mons.cond_id = cond.cond_id (+)
and    nvl( mons.ongr_id, stan.ongr_id ) = ongr.ongr_id;

/
QUIT
