CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MACHTIGINGSBRIEF_BU_VW" ("GEADRESSEERDE", "BETREFT_TEKST", "IDENTIFICATIE", "VERZEKERDE", "GEBOORTEDATUM", "GESLACHT", "AANVRAAGDATUM", "DATUM_AUTORISATIE", "RELA_CODE", "VERWIJZER", "TOELICHTING", "AUTORISATOR", "ONDE_ID", "KAFD_ID", "PERS_ID", "DECL_ID", "ONGR_ID", "INGR_ID", "TAAL_ID", "RAMO_ID", "KAFD_CODE", "ONGR_CODE", "EERDER_GEPRINT", "ONDERZOEKNR", "AFGEROND", "DECL_STATUS") AS
  SELECT nvl( decl.nota_adres, mach.verwijzer ) geadresseerde
, mach.betreft_tekst
, mach.identificatie
, mach.verzekerde
, mach.geboortedatum
, mach.geslacht
, mach.aanvraagdatum
, mach.datum_autorisatie
, mach.rela_code
, mach.verwijzer
, mach.toelichting
, mach.autorisator
, mach.onde_id
, mach.kafd_id
, mach.pers_id
, mach.decl_id
, mach.ongr_id
, mach.ingr_id
, mach.taal_id
, mach.ramo_id
, mach.kafd_code
, mach.ongr_code
, mach.eerder_geprint
, mach.onderzoeknr
, mach.afgerond
, mach.decl_status
from  kgc_declaraties decl
, kgc_machtigingsbrief_vw mach
where mach.decl_id = decl.decl_id
and  mach.ramo_code in ( 'MACHT_BU', 'MACHT_BEG' )
 ;

/
QUIT
