CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_SELECT_MEET_2_VW" ("KAFD_CODE", "ONGR_CODE", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "METI_DATUM_AANMELDING", "SPOED", "STGR_CODE", "STGR_OMSCHRIJVING", "RUNNUMMER", "STOF_CODE", "STOF_OMSCHRIJVING", "MEETWAARDE", "MEETEENHEID", "MEET_COMMENTAAR", "KAFD_ID", "ONGR_ID", "METI_ID", "MEET_ID", "STOF_ID", "MWST_ID", "MEST_ID", "MEDE_ID", "MEDE_ID_CONTROLE", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_ZOEKNAAM", "PERS_ZISNR", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ONDERZOEKNR", "INDICATIE", "MONSTERNUMMER", "MONS_DATUM_AFNAME", "FRTY_CODE", "FRTY_OMSCHRIJVING", "PERS_ID", "ONDE_ID", "MONS_ID") AS
  SELECT kafd.code kafd_code
,      ongr.code ongr_code
,      stan.mate_id
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      stan.standaardfractienummer fractienummer
,      meti.datum_aanmelding meti_datum_aanmelding
,      decode( meti.prioriteit
             , 'J','J'
              , meet.spoed )  spoed
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      runs.runnummer
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.tonen_als meetwaarde
,      meet.meeteenheid meeteenheid
,      meet.commentaar meet_commentaar
,       kafd.kafd_id
,      ongr.ongr_id
,      meti.meti_id
,      meet.meet_id
,      stof.stof_id
,      meet.mwst_id
,      meet.mest_id
,      meet.mede_id
,      meet.mede_id_controle
, null pers_aanspreken
, null pers_achternaam
, null pers_zoeknaam
, null pers_zisnr
, to_date(null) pers_geboortedatum
, null pers_geslacht
, null onderzoeknr
, null indicatie
, null monsternummer
, to_date(null) mons_datum_afname
, null frty_code
, null frty_omschrijving
, to_number(null) pers_id
, to_number(null) onde_id
, to_number(null) mons_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      bas_runs runs
,      kgc_materialen mate
,      bas_standaardfracties stan
,      kgc_stoftestgroepen stgr
,      bas_metingen meti
,      kgc_stoftesten stof
,      bas_meetwaarden meet
where ongr.kafd_id = kafd.kafd_id
and      stan.ongr_id = ongr.ongr_id
and      stan.mate_id = mate.mate_id
and      stan.stan_id = meti.stan_id
and      meti.stgr_id = stgr.stgr_id (+)
and      meti.meti_id = meet.meti_id
and      meet.stof_id = stof.stof_id
and      meet.runs_id = runs.runs_id (+)
and      meti.afgerond = 'N'
and      meet.afgerond = 'N'
and      stan.vervallen = 'N'
and    meet.meet_reken <> ('R')
 ;

/
QUIT
