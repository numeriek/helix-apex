CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_VERWACHTE_MONSTERS_VW" ("MONS_ID", "MONSTERNUMMER", "PERS_ID", "PERSOON_INFO", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "HERK_ID", "HERK_CODE", "HERK_OMSCHRIJVING", "RELA_ID", "RELA_CODE", "RELA_AANSPREKEN", "DATUM_VERWACHT", "DATUM_AANMELDING", "COMMENTAAR", "ONGR_ID", "ONGR_CODE", "ONGR_OMSCHRIJVING", "KAFD_ID", "KAFD_CODE", "KAFD_NAAM") AS
  SELECT mons.mons_id
,      mons.monsternummer
,      mons.pers_id
,      kgc_pers_00.persoon_info( mons.mons_id )  persoon_info
,      mons.mate_id mate_id
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      mons.herk_id
,      herk.code herk_code
,      herk.omschrijving herk_omschrijving
,      mons.rela_id
,      rela.code rela_code
,      rela.aanspreken rela_aanspreken
,      mons.datum_verwacht
,      mons.datum_aanmelding
,      mons.commentaar
,      mons.ongr_id
,      ongr.code ongr_code
,      ongr.omschrijving ongr_omschrijving
,      mons.kafd_id
,      kafd.code kafd_code
,      kafd.naam kafd_naam
from   kgc_herkomsten herk
,      kgc_materialen mate
,      kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_relaties rela
,      kgc_monsters mons
where mons.mate_id = mate.mate_id (+)
and    mons.rela_id = rela.rela_id (+)
and    mons.herk_id = herk.herk_id (+)
and    mons.kafd_id = kafd.kafd_id
and    mons.ongr_id = ongr.ongr_id
 ;

/
QUIT
