CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_VOOROPSLAG_VW" ("KAFD_ID", "ONGR_ID", "MONS_ID", "MONSTERNUMMER", "MONS_DATUM_AANMELDING", "MATE_CODE", "MATE_OMSCHRIJVING", "PERS_ID", "PERS_ZISNR", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "VOTY_CODE", "VOTY_OMSCHRIJVING", "VOOP_VOLGNR") AS
  SELECT mons.kafd_id
,      mons.ongr_id
,      mons.mons_id
,      mons.monsternummer
,      mons.datum_aanmelding mons_datum_aanmelding
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      pers.pers_id
,      pers.zisnr pers_zisnr
,      pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      voty.code voty_code
,      voty.omschrijving voty_omschrijving
,      voop.volgnr voop_volgnr
from   kgc_vooropslag_types voty
,      kgc_materialen mate
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_vooropslag voop
where voop.mons_id = mons.mons_id
and    mons.pers_id = pers.pers_id
and    mons.mate_id = mate.mate_id
and    voop.voty_id = voty.voty_id
 ;

/
QUIT
