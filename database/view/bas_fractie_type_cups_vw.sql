CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_FRACTIE_TYPE_CUPS_VW" ("ONGR_ID", "FRTY_ID", "CUTY_ID", "FRTY_CODE", "FRTY_OMSCHRIJVING", "LETTER_FRACTIENR", "STANDAARD", "VOLGORDE", "CUTY_CODE", "CUTY_OMSCHRIJVING", "VERVALLEN", "STANDAARD_AANTAL") AS
  SELECT frty.ongr_id
,      ftct.frty_id
,      ftct.cuty_id
,      frty.code frty_code
,      frty.omschrijving frty_omschrijving
,      frty.letter_fractienr
,      frty.standaard frty_standaard
,      frty.volgorde frty_volgorde
,      cuty.code cuty_code
,      cuty.omschrijving cuty_omschrijving
,      decode( frty.vervallen
             , 'J', 'J'
             , decode( cuty.vervallen
                     , 'J', 'J'
                     , 'N'
                     )
             ) vervallen
,      ftct.standaard_aantal
from   kgc_cup_types cuty
,      bas_fractie_types frty
,      bas_frty_cuty ftct
where ftct.frty_id = frty.frty_id
and    ftct.cuty_id = cuty.cuty_id
union all
select  frty.ongr_id
,      frty.frty_id
,      to_number( null )
,      frty.code
,      frty.omschrijving
,      frty.letter_fractienr
,      frty.standaard
,      frty.volgorde
,      null
,      null
,      frty.vervallen
,      to_number( null )
from   bas_fractie_types frty
where  not exists
       ( select null
         from   bas_frty_cuty ftct
         where  ftct.frty_id = frty.frty_id
       )
 ;

/
QUIT
