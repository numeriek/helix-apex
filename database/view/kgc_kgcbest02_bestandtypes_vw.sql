CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCBEST02_BESTANDTYPES_VW" ("SYPA_CODE", "BTYP_ID", "BTYP_CODE", "ENTI_CODE", "BCAT_VERVALLEN", "KAFD_ID", "ONGR_ID", "MEDE_ID", "LIST_LABEL") AS
  select sypa_code, btyp_id, btyp_code, enti_code, bcat_vervallen, kafd_id, ongr_id, mede_id, list_label from (
    SELECT sypa.code sypa_code
    ,      btyp.btyp_id
    ,      sypa.standaard_waarde btyp_code
    ,      btyp.enti_code
    ,      bcat.vervallen bcat_vervallen
    ,      to_number('') kafd_id
    ,      to_number('') ongr_id
    ,      to_number('') mede_id
    ,      btyp.code||', '||btyp.omschrijving list_label
    FROM   kgc_systeem_parameters sypa
    ,      kgc_bestand_types btyp
    ,      kgc_bestand_categorieen bcat
    WHERE  sypa.standaard_waarde = btyp.code
    and    btyp.bcat_id = bcat.bcat_id
    AND    btyp.vervallen = 'N'
    union all
    SELECT sypa.code sypa_code
    ,      btyp.btyp_id
    ,      btyp.code btyp_code
    ,      btyp.enti_code
    ,      bcat.vervallen bcat_vervallen
    ,      spwa.kafd_id
    ,      spwa.ongr_id
    ,      spwa.mede_id
    ,      btyp.code||', '||btyp.omschrijving list_label
    FROM   kgc_systeem_par_waarden spwa
    ,      kgc_systeem_parameters sypa
    ,      kgc_bestand_types btyp
    ,      kgc_bestand_categorieen bcat
    WHERE  sypa.sypa_id = spwa.sypa_id
    and    btyp.bcat_id = bcat.bcat_id
    AND    btyp.vervallen = 'N'
    and    instr(','||spwa.waarde||',', ','||btyp.code||',') > 0
    )
    order by btyp_code;

/
QUIT
