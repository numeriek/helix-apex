CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_DECLARATIE_ITEMS_VW" ("ENTITEIT", "ID", "KAFD_ID", "ONGR_ID", "PERS_ID", "OMSCHRIJVING", "DECLAREREN") AS
  SELECT 'ONDE' entiteit
, onde.onde_id id
, onde.kafd_id
, onde.ongr_id
, onde.pers_id
, 'Onderzoek: '||onde.onderzoeknr
||chr(10)|| 'Groep: '||ongr.omschrijving
||chr(10)|| 'Datum binnen: '||to_char(onde.datum_binnen,'dd-mm-yyyy')||'  '||'Afgerond: '||decode( onde.afgerond, 'J', 'Ja', 'Nee' )
||chr(10)|| 'Indicatie: '||kgc_indi_00.onderzoeksreden( onde.onde_id)
omschrijving
, onde.declareren
from kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_onderzoeken onde
where onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
union all
select 'ONBE'
, onbe.onbe_id
, onde.kafd_id
, onde.ongr_id
, onbe.pers_id
, decode( onbe.frac_id
        , null, 'Rol: '||onbe.rol
        , 'Fractie: '||frac.fractienummer||decode( frac.controle, 'J',' (indexfractie)', null )
        )
||chr(10)|| 'Onderzoek: '||onde.onderzoeknr ||' van '||kgc_pers_00.persoon_info( null, onde.onde_id )
||chr(10)|| 'Groep: '||ongr.omschrijving
||chr(10)|| 'Datum binnen: '||to_char(onde.datum_binnen,'dd-mm-yyyy') || '  '|| 'Afgerond: '||decode( onde.afgerond, 'J', 'Ja' , 'Nee' )
||chr(10)|| 'Indicatie: '||kgc_indi_00.onderzoeksreden( onde.onde_id)
, onbe.declareren
from kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_onderzoeken onde
, bas_fracties frac
, kgc_onderzoek_betrokkenen onbe
where onbe.frac_id = frac.frac_id (+)
and onbe.onde_id = onde.onde_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
union all
select 'MONS'
, mons.mons_id
, mons.kafd_id
, mons.ongr_id
, mons.pers_id
, 'Monster: '||mons.monsternummer
||chr(10)|| 'Groep: '||ongr.omschrijving||decode( mons.herk_id, null, null, ' van  '||herk.omschrijving)
||chr(10)|| 'Datum: '||to_char(mons.datum_aanmelding,'dd-mm-yyyy')
||chr(10)|| 'Materiaal: '||mate.omschrijving
omschrijving
, mons.declareren
from kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_herkomsten herk
, kgc_materialen mate
, kgc_monsters mons
where mons.kafd_id = kafd.kafd_id
and mons.ongr_id = ongr.ongr_id
and mons.mate_id = mate.mate_id
and mons.herk_id = herk.herk_id (+)
union all
select 'METI'
, meti.meti_id
, onde.kafd_id
, onde.ongr_id
, mons.pers_id
, 'Fractie: '||frac.fractienummer||decode( frac.controle, 'J',' (indexfractie)', null )
||chr(10)|| 'Onderzoek: '||onde.onderzoeknr|| ' van '||kgc_pers_00.persoon_info( null, onde.onde_id )
||chr(10)|| 'Protocol: '||stgr.omschrijving
||chr(10)|| 'Datum: '||to_char(meti.datum_aanmelding,'dd-mm-yyyy') || '  '|| 'Afgerond: '||decode( meti.afgerond, 'J', 'Ja' , 'Nee' )
||chr(10)|| 'Groep: '||ongr.omschrijving
, meti.declareren
from kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_stoftestgroepen stgr
, kgc_onderzoeken onde
, bas_fracties frac
, kgc_monsters mons
, bas_metingen meti
where meti.frac_id = frac.frac_id
and frac.mons_id = mons.mons_id
and meti.onde_id = onde.onde_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
and meti.stgr_id = stgr.stgr_id (+)
union all
select 'METI'
, meti.meti_id
, onde.kafd_id
, onde.ongr_id
, onde.pers_id
, 'Fractie: '||frac.fractienummer||decode( frac.controle, 'J',' (indexfractie)', null )
||chr(10)|| 'Onderzoek: '||onde.onderzoeknr|| ' van '||kgc_pers_00.persoon_info( null, onde.onde_id )
||chr(10)|| 'Protocol: '||stgr.omschrijving
||chr(10)|| 'Datum: '||to_char(meti.datum_aanmelding,'dd-mm-yyyy') || '  '|| 'Afgerond: '||decode( meti.afgerond, 'J', 'Ja' , 'Nee' )
||chr(10)|| 'Groep: '||ongr.omschrijving
, meti.declareren
from kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_stoftestgroepen stgr
, kgc_onderzoeken onde
, bas_fracties frac
, kgc_monsters mons
, bas_metingen meti
where meti.frac_id = frac.frac_id
and frac.mons_id = mons.mons_id
and meti.onde_id = onde.onde_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
and meti.stgr_id = stgr.stgr_id (+)
union all
select 'GESP'
, gesp.gesp_id
, onde.kafd_id
, gesp.ongr_id
, gebe.pers_id
, 'Onderzoek: '||onde.onderzoeknr|| ' van '||kgc_pers_00.persoon_info( null, onde.onde_id )
||chr(10)|| 'Datum: '||to_char(gesp.datum,'dd-mm-yyyy')
||chr(10)|| 'Groep: '||ongr.omschrijving
, gesp.declareren
from kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_onderzoeken onde
, kgc_gesprek_betrokkenen gebe
, kgc_gesprekken gesp
where gesp.onde_id = onde.onde_id
and onde.kafd_id = kafd.kafd_id
and gesp.ongr_id = ongr.ongr_id
and gesp.gesp_id = gebe.gesp_id
and gebe.pers_id is not null
and gebe.declaratie = 'J'
union all -- declarabele gesprekken zonder betrokkenen worden toegewezen aan de onderzoekspersoon
select 'GESP'
, gesp.gesp_id
, onde.kafd_id
, gesp.ongr_id
, onde.pers_id
, 'Onderzoek: '||onde.onderzoeknr|| ' van '||kgc_pers_00.persoon_info( null, onde.onde_id )
||chr(10)|| 'Datum: '||to_char(gesp.datum,'dd-mm-yyyy')
||chr(10)|| 'Groep: '||ongr.omschrijving
, gesp.declareren
from kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_onderzoeken onde
, kgc_gesprekken gesp
where gesp.onde_id = onde.onde_id
and onde.kafd_id = kafd.kafd_id
and gesp.ongr_id = ongr.ongr_id
and not exists
    ( select null
      from kgc_gesprek_betrokkenen gebe
      where gebe.gesp_id = gesp.gesp_id
      and gebe.pers_id is not null
      and gebe.declaratie = 'J'
    );

/
QUIT
