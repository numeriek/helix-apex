CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_SONIFICATIE_CAWA_VW" ("CAWA_VOLGORDE", "CAWA_BESCHRIJVING", "UITSLAG", "NORMAALWAARDEN", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "NORMAALWAARDE_GEMIDDELDE", "EENHEID", "MEET_ID", "STOF_ID", "ONDE_ID", "ONMO_ID", "CATE_ID") AS
  SELECT -- werkelijk gebruikte categorie-waarden
       cawa.volgorde cawa_volgorde
,      nvl( kgc_vert_00.vertaling( 'CAWA'
, cawa.cawa_id, null, onde.taal_id ), cawa.beschrijving )
cawa_beschrijving
,      meet.meetwaarde uitslag
,      bas_meet_00.normaalwaarden( meet.meet_id, 'N' ) normaalwaarden
,      to_char(meet.normaalwaarde_ondergrens) normaalwaarde_ondergrens
,      to_char(meet.normaalwaarde_bovengrens) normaalwaarde_bovengrens
,      to_char(meet.normaalwaarde_gemiddelde) normaalwaarde_gemiddelde
,      meet.meeteenheid eenheid
,      meet.meet_id
,      meet.stof_id
,      meti.onde_id
,      meti.onmo_id
,      cawa.cate_id
from   kgc_stoftesten stof
,      kgc_onderzoeken onde
,      bas_metingen meti
,      bas_meetwaarden meet
,      kgc_categorie_waarden cawa
WHERE meti.onde_id = onde.onde_id
and    meti.meti_id = meet.meti_id
and    meet.stof_id = stof.stof_id
and    cawa.waarde = stof.code
and    onde.afgerond = 'N'
and    meti.afgerond = 'N'
and    meet.afgerond = 'N'
union  -- altijd getoonde categorie-waarden
SELECT cawa.volgorde
,      nvl( kgc_vert_00.vertaling( 'CAWA'
, cawa.cawa_id, null, onde.taal_id ), cawa.beschrijving )
,      '-' uitslag
,      bas_rang_00.normaalwaarden
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       )
,      bas_rang_00.laag
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       )
,      bas_rang_00.hoog
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       )
,      bas_rang_00.gemiddeld
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       )
,      stof.eenheid
,      to_number(null) meet_id
,      stof.stof_id
,      onde.onde_id
,      onmo.onmo_id
,      cawa.cate_id
from   kgc_stoftesten stof
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      kgc_onderzoek_monsters onmo
,      kgc_categorieen cate
,      kgc_categorie_waarden cawa
where  mons.pers_id = pers.pers_id
and    onmo.mons_id = mons.mons_id
and    onmo.onde_id = onde.onde_id
and    onde.kafd_id = stof.kafd_id
and    onde.afgerond = 'N'
and    cate.cate_id = cawa.cate_id
and    cawa.waarde = stof.code
and    cawa.als_in_gebruik = 'N'
and  ( onde.kafd_id = cate.kafd_id or cate.kafd_id is null )
and  ( onde.ongr_id = cate.ongr_id or cate.ongr_id is null )
and    not exists
       ( select null
         from   bas_metingen meti
         ,      bas_meetwaarden meet
         WHERE  meti.meti_id = meet.meti_id
         and    meet.stof_id = stof.stof_id
         and    meti.onde_id = onde.onde_id
       )
 ;

/
QUIT
