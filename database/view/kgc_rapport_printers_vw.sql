CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_RAPPORT_PRINTERS_VW" ("KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "TAAL_ID", "TAAL_CODE", "RAPP_ID", "RAPPORT", "MEDE_ID", "PRIN_ID", "PRINTER", "PRIN_CODE") AS
  SELECT kafd.kafd_id   kafd_id
,      kafd.code      kafd_code
,      ongr.ongr_id   ongr_id
,      ongr.code      ongr_code
,      taal.taal_id   taal_id
,      taal.code      taal_code
,      rapp.rapp_id   rapp_id
,      rapp.rapport   rapport
,      rapr.mede_id   mede_id
,      rapr.prin_id   prin_id
,      prin.naam      printer
,      prin.code      prin_code
from   kgc_printers prin
,      kgc_talen taal
,      kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_rapporten rapp
,      kgc_rapport_printers rapr
where  rapr.rapp_id = rapp.rapp_id
and    rapr.prin_id = prin.prin_id
and    rapp.kafd_id = kafd.kafd_id
and    kafd.kafd_id = ongr.kafd_id
and    ( ongr.ongr_id = rapp.ongr_id or rapp.ongr_id is null )
and    rapp.taal_id = taal.taal_id  (+)
and    (rapr.mede_id = kgc_mede_00.medewerker_id or rapr.mede_id is null)
union
select kafd.kafd_id
,      kafd.code kafd_code
,      ongr.ongr_id
,      ongr.code ongr_code
,      taal.taal_id
,      taal.code taal_code
,      rapp.rapp_id
,      rapp.rapport
,      null -- mede_id
,      prin.prin_id -- carthesisch produkt
,      prin.naam printer
,      prin.code prin_code
from   kgc_printers prin
,      kgc_talen taal
,      kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_rapporten rapp
where  rapp.kafd_id = kafd.kafd_id
and    kafd.kafd_id = ongr.kafd_id
and    ( ongr.ongr_id = rapp.ongr_id or rapp.ongr_id is null )
and    rapp.taal_id = taal.taal_id (+)
and   (nvl(taal.code,'NED') = nvl('','NED') or (rapp.taal_id is null and '' is null ))
and    not exists
       ( select null
         from   kgc_rapport_printers rapr
         where  rapr.rapp_id = rapp.rapp_id
       )
 ;

/
QUIT
