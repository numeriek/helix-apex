CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERZOEK_RESULTATEN_VW" ("ONDE_ID", "LAST_UPDATE_DATE", "RESULTAAT") AS
  SELECT meti.onde_id,
          meti.last_update_date,
          TO_CHAR (meti.conclusie) resultaat --TO_CHAR added by Anuja for MANTIS 9355 on 8/4/2016 for release 8.11.0.3
     FROM kgc_onderzoeken onde,
          kgc_monsters mons,
          bas_fracties frac,
          bas_metingen meti
    WHERE     meti.onde_id = onde.onde_id
          AND meti.frac_id = frac.frac_id
          AND frac.mons_id = mons.mons_id
          -- and    mons.pers_id = onde.pers_id -- niet enkel adviesvrager, maar alle betrokkenen
          AND to_char(meti.conclusie) IS NOT NULL -- nav mantis nr13998
          AND NVL (frac.status, 'O') <> 'M';

/
QUIT
