CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_CONC_CONTEXT_VW" ("CONC_ID", "KAFD_ID", "ONDE_ID", "CODE", "OMSCHRIJVING") AS
  SELECT conc.conc_id
, conc.kafd_id
, onde.onde_id
, conc.code
, conc.omschrijving
FROM kgc_conclusie_teksten conc
,    kgc_teksten_context teco
,    kgc_onderzoek_indicaties onin
,    kgc_indicatie_teksten indi
,    kgc_onderzoeken onde
WHERE conc.conc_id = teco.conc_id
AND   teco.indi_id = onin.indi_id
AND   onin.onde_id = onde.onde_id
AND   onin.indi_id = indi.indi_id
AND   conc.vervallen = 'N'
UNION ALL
SELECT conc.conc_id
, conc.kafd_id
, onde.onde_id
, conc.code
, conc.omschrijving
FROM kgc_conclusie_teksten conc
,    kgc_onderzoeken onde
WHERE conc.kafd_id = onde.kafd_id
AND   conc.vervallen = 'N'
AND  NOT EXISTS
  ( SELECT 1
    FROM kgc_conclusie_teksten conc2
    ,    kgc_teksten_context teco2
    ,    kgc_onderzoek_indicaties onin2
    ,    kgc_indicatie_teksten indi2
    ,    kgc_onderzoeken onde2
    WHERE conc2.conc_id = teco2.conc_id
    AND   teco2.indi_id = indi2.indi_id
    AND   onin2.onde_id = onde2.onde_id
    AND   onin2.indi_id = indi2.indi_id
    AND   conc2.vervallen = 'N'
    AND   onde2.onde_id = onde.onde_id
  )
 ;

/
QUIT
