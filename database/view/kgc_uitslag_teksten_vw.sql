CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAG_TEKSTEN_VW" ("ONDE_ID", "METI_ID", "MEET_ID", "BRON", "ID", "CONTEXT", "TEKST", "VOLGORDE", "DATUM") AS
  SELECT meti.onde_id
,      meti.meti_id
,      to_number(null) meet_id
,      'meting resultaat' bron
,      meti.meti_id id
,      stgr.omschrijving context
,      TO_CHAR(meti.conclusie)  tekst --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014)
                                       --  TO_CHAR(meti.conclusie)  changed to  meti.conclusie by sachin for reverting changes of mantis 9355 in rework release 201502 12-06-2015
                                       --  TO_CHAR added by SKA for MANTIS 9355 (31-03-2016) release 8.11.0.2
,      1 volgorde
,      meti.creation_date datum
from   kgc_stoftestgroepen stgr
,      bas_metingen meti
where  meti.stgr_id = stgr.stgr_id (+)
and    meti.conclusie is not null
union all
select meti.onde_id
,      meti.meti_id
,      meet.meet_id
,      'meetwaarde'
,      meet.meet_id
,      stof.omschrijving
,      meet.meetwaarde
,      2
,      meet.creation_date
from   kgc_stoftesten stof
,      bas_metingen meti
,      bas_meetwaarden meet
where  meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meet.meetwaarde is not null
union all
select meti.onde_id
,      meti.meti_id
,      meet.meet_id
,      'meetwaarde detail'
,      mdet.mdet_id
,      mdet.prompt||
       decode( upper( mdet.prompt )
             , upper( stof.omschrijving), ''
             , ' ('||stof.omschrijving||')'
             )
,      mdet.waarde
,      3
,      meet.creation_date
from   kgc_mwst_samenstelling mwss
,      kgc_stoftesten stof
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_meetwaarde_details mdet
where  meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meet.meet_id = mdet.meet_id
and    meet.mwst_id = mwss.mwst_id
and    mdet.volgorde = mwss.volgorde
and    mwss.in_uitslag = 'J'
and    mdet.waarde is not null
union all
select meti.onde_id
,      meti.meti_id
,      meet.meet_id
,      'meetwaarde commentaar'
,      meet.meet_id
,      stof.omschrijving
,      meet.commentaar
,      4
,      meet.creation_date
from   kgc_stoftesten stof
,      bas_metingen meti
,      bas_meetwaarden meet
where  meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meet.commentaar is not null
union all
select meti.onde_id
,      meti.meti_id
,      meet.meet_id
,      'berekening'
,      bere.bere_id
,      bety.omschrijving
,      to_char(bere.uitslag)
,      5
,      bere.creation_date
from   bas_berekening_types bety
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_berekeningen bere
where  meet.meti_id = meti.meti_id
and    meet.meet_id = bere.meet_id
and    bere.bety_id = bety.bety_id
and    bere.uitslag is not null
and    bere.in_uitslag = 'J'
union all
select coco.onde_id
,      to_number( null ) -- meti_id
,      to_number( null ) -- meet_id
,      'conclusie tekst'
,      coco.conc_id
,      coco.code
,      coco.omschrijving
,      8
,      to_date( null )
from   kgc_conc_context_vw coco
union  all
select toco.onde_id
,      to_number( null ) -- meti_id
,      to_number( null ) -- meet_id
,      'toelichting tekst'
,      toco.tote_id
,      toco.code
,      toco.omschrijving
,      9
,      to_date( null )
from   kgc_tote_context_vw toco;

/
QUIT
