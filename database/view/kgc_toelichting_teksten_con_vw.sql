CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_TOELICHTING_TEKSTEN_CON_VW" ("KAFD_ID", "ENTITEIT", "ID", "CODE", "TEKST", "ONDE_ID") AS
  SELECT coct.kafd_id
,     'CONC' entiteit
,     coct.conc_id id
,     coct.code
,     coct.omschrijving tekst
,     coct.onde_id
from   kgc_conc_context_vw coct
,      kgc_systeem_parameters sypa
where sypa.code = 'TOELICHTING_TEKSTEN'
and      instr( nvl( kgc_sypa_00.medewerker_waarde( sypa.sypa_id )
                             , sypa.standaard_waarde
                             )
                     , 'C'
                     ) > 0
union all
select rete.kafd_id
,           'RETE'
,           rete.rete_id
,           rete.code
,           rete.omschrijving
,     to_number(NULL)
from   kgc_resultaat_teksten rete
,           kgc_systeem_parameters sypa
where sypa.code = 'TOELICHTING_TEKSTEN'
and      instr( nvl( kgc_sypa_00.medewerker_waarde( sypa.sypa_id )
                             , sypa.standaard_waarde
                             )
                      , 'R'
                      ) > 0
and      rete.vervallen = 'N'
union all
select toct.kafd_id
,           'TOTE'
,           toct.tote_id
,           toct.code
,           toct.omschrijving
,     		toct.onde_id
from   kgc_tote_context_vw toct
,           kgc_systeem_parameters sypa
where sypa.code = 'TOELICHTING_TEKSTEN'
and      instr( nvl( kgc_sypa_00.medewerker_waarde( sypa.sypa_id )
                              , sypa.standaard_waarde
                              )
                       , 'T'
                       ) > 0
 ;

/
QUIT
