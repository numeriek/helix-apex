CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCINDI03_ONDE_VW" ("ONDE_ID", "PERS_ID", "ONDERZOEKNR", "PERSOON_INFO", "INDICATIES") AS
  SELECT onmo.onde_id
,      onde.pers_id
,      onde.onderzoeknr
,      kgc_pers_00.persoon_info( null, onde.onde_id )
,      kgc_indi_00.indicaties_met_opmerking( onde.onde_id )
FROM   kgc_onderzoeken         onde
,      kgc_onderzoek_monsters  onmo
  WHERE onmo.onde_id            = onde.onde_id
 ;

/
QUIT
