CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_RESULTATEN_VW" ("KAFD_ID", "KAFD_CODE", "ONGR_ID", "PERS_ID", "PERS_ZISNR", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ONDE_ID", "ONDERZOEKNR", "ONDE_AFGEROND", "ONDERZOEKSTYPE", "INDICATIE", "MONS_ID", "MONSTERNUMMER", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "MONS_TIJD_AFNAME", "MONS_HOEVEELHEID", "MEDISCHE_INDICATIE", "MONS_MEDICATIE", "MONS_VOEDING", "MONS_COMMENTAAR", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "FRAC_ID", "STAN_ID", "FRACTIENUMMER", "METI_ID", "METI_DATUM_AANMELDING", "METI_PRIORITEIT", "METI_SNELHEID", "STGR_ID", "STGR_CODE", "STGR_OMSCHRIJVING", "MEST_CODE", "MEST_OMSCHRIJVING", "METI_OPMERKING_ANALYSE", "METI_OPMERKING_ALGEMEEN", "METI_CONCLUSIE", "METI_SETNAAM", "METI_AFGEROND", "MEET_AFGEROND", "MEET_ID", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "MEETWAARDE", "NORMAALWAARDEN", "MEETEENHEID", "MEET_COMMENTAAR", "BETY_CODE", "BETY_OMSCHRIJVING", "UITSLAG", "UITSLAGEENHEID", "BERE_NORMAALWAARDEN", "MEET_DATUM_AFGEROND", "METI_DATUM_AFGEROND") AS
  SELECT -- normale fracties
      kafd.kafd_id
,     kafd.code kafd_code
,     mons.ongr_id
,     mons.pers_id
,     pers.zisnr pers_zisnr
,     pers.aanspreken pers_aanspreken
,     pers.achternaam pers_achternaam
,     pers.geboortedatum pers_geboortedatum
,     pers.geslacht pers_geslacht
,     onde.onde_id
,     onde.onderzoeknr
,     onde.afgerond onde_afgerond
,     onde.onderzoekstype
,     kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie
,     mons.mons_id
,     mons.monsternummer
,     mons.datum_aanmelding mons_datum_aanmelding
,     mons.datum_afname mons_datum_afname
,     mons.tijd_afname mons_tijd_afname
,     mons.hoeveelheid_monster mons_hoeveelheid
,     mons.medische_indicatie
,     mons.medicatie mons_medicatie
,     mons.voeding mons_voeding
,     mons.commentaar mons_commentaar
,     mons.mate_id
,     mate.code mate_code
,     mate.omschrijving mate_omschrijving
,     frac.frac_id
,     to_number( null ) stan_id
,     frac.fractienummer
,     meti.meti_id
,     meti.datum_aanmelding meti_datum_aanmelding
,     meti.prioriteit meti_prioriteit
,     meti.snelheid meti_snelheid
,     meti.stgr_id
,     stgr.code stgr_code
,     stgr.omschrijving stgr_omschrijving
,     mest.code mest_code
,     mest.omschrijving mest_omschrijving
,     meti.opmerking_analyse meti_opmerking_analyse
,     meti.opmerking_algemeen meti_opmerking_algemeen
,     to_char(meti.conclusie)  meti_conclusie -- Mantis 9355 to_char added by SKA 08-04-2016 release 8.11.0.3
,     meti.setnaam meti_setnaam
,     meti.afgerond meti_afgerond
,     meet.afgerond meet_afgerond
,     meet.meet_id
,     meet.stof_id
,     stof.code stof_code
,     stof.omschrijving stof_omschrijving
,     meet.tonen_als meetwaarde
,     bas_meet_00.normaalwaarden( meet.meet_id )  normaalwaarden
,     meet.meeteenheid
,     meet.commentaar meet_commentaar
,     bety.code bety_code
,     bety.omschrijving  bety_omschrijving
,     bere.uitslag
,     bere.uitslageenheid
,     bas_bere_00.normaalwaarden( bere.bere_id )  bere_normaalwaarden
,     meet.datum_afgerond meet_datum_afgerond
,     meti.datum_afgerond meti_datum_afgerond
from  kgc_kgc_afdelingen kafd
,     bas_berekening_types bety
,     bas_meetwaarde_statussen mest
,     kgc_stoftestgroepen stgr
,     kgc_stoftesten stof
,     kgc_monsters mons
,     kgc_materialen mate
,     kgc_onderzoeken onde
,     kgc_personen pers
,     bas_fracties frac
,     bas_metingen meti
,     bas_berekeningen bere
,     bas_meetwaarden meet
where meet.stof_id = stof.stof_id
and   meet.meti_id = meti.meti_id
and   meti.onde_id = onde.onde_id
and   meti.frac_id = frac.frac_id
and   frac.mons_id = mons.mons_id
and   mons.kafd_id = kafd.kafd_id
and   mons.pers_id = pers.pers_id
and   mons.mate_id = mate.mate_id
and   meti.stgr_id = stgr.stgr_id (+)
and   meet.mest_id = mest.mest_id (+)
and   meet.meet_id = bere.meet_id (+)
and   bere.bety_id = bety.bety_id (+)
union all
select -- standaardfracties
      ongr.kafd_id
,     kafd.code
,     stan.ongr_id
,     to_number( null )
,     null
,     null
,     null
,     to_date(null)
,     null
,     to_number( null )
,     null -- onderzoeknr
,     null
,     null
,     null
,     to_number( null )
,     null
,     to_date( null)
,     to_date( null)
,     to_date( null)
,     to_number( null )
,     null
,     null
,     null
,     null
,     to_number( null )
,     null
,     null
,     to_number( null )
,     stan.stan_id
,     stan.standaardfractienummer
,     meti.meti_id
,     meti.datum_aanmelding
,     meti.prioriteit
,     meti.snelheid
,     meti.stgr_id
,     stgr.code
,     stgr.omschrijving
,     mest.code
,     mest.omschrijving
,     meti.opmerking_analyse
,     meti.opmerking_algemeen
,    to_char( meti.conclusie) conclusie  -- Mantis 9355 to_char added by SKA 08-04-2016 release 8.11.0.3
,     meti.setnaam
,     meti.afgerond
,     meet.afgerond
,     meet.meet_id
,     meet.stof_id
,     stof.code
,     stof.omschrijving
,     meet.tonen_als meetwaarde
,     bas_meet_00.normaalwaarden( meet.meet_id )
,     meet.meeteenheid
,     meet.commentaar
,     bety.code
,     bety. omschrijving
,     bere.uitslag
,     bere.uitslageenheid
,     bas_bere_00.normaalwaarden( bere.bere_id )
,     meet.datum_afgerond
,     meti.datum_afgerond
from  kgc_kgc_afdelingen kafd
,     bas_berekening_types bety
,     kgc_onderzoeksgroepen ongr
,     bas_standaardfracties stan
,     bas_meetwaarde_statussen mest
,     kgc_stoftestgroepen stgr
,     kgc_stoftesten stof
,     bas_metingen meti
,     bas_berekeningen bere
,     bas_meetwaarden meet
where stan.ongr_id = ongr.ongr_id
and   ongr.kafd_id = kafd.kafd_id
and   meet.stof_id = stof.stof_id
and   meet.meti_id = meti.meti_id
and   meti.stan_id = stan.stan_id
and   meti.stgr_id = stgr.stgr_id (+)
and   meet.mest_id = mest.mest_id (+)
and   meet.meet_id = bere.meet_id (+)
and   bere.bety_id = bety.bety_id (+);

/
QUIT
