CREATE OR REPLACE FORCE VIEW "HELIX"."DIA_HOOFDTERM" ("CODE", "OMSCHRIJVING", "ICDBPA", "MCKUSICK", "DIAGNOSEGROEP", "VERVALLEN", "MCKUSICK_EDITIE", "EDITIE_INVOER", "EDITIE_WIJZIGING", "EDITIE_VERVALLEN", "REDEN_INVOER", "REDEN_WIJZIGING", "REDEN_VERVALLEN") AS
  SELECT ziektecode code
, ziektetekst omschrijving
, icdbpa
, mckusick
, diaggroepcode diagnosegroep
, decode(editie_vervallen,null, 'N','J') vervallen
, mckusick_editie
, editie_invoer
, editie_wijziging
, editie_vervallen
, reden_invoer
, reden_wijziging
, reden_vervallen
FROM  cin_hfd
 ;

/
QUIT
