CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_METAB_MACHTIGINGEN2_VW" ("ONDERZOEKNR", "DECL", "HERKOMST") AS
  SELECT onde.onderzoeknr, 'N' decl, herk.code
FROM     KGC_ONDERZOEKEN onde
	   , KGC_HERKOMSTEN herk
	   , KGC_KGC_AFDELINGEN kafd
	   , KGC_metab_machtigingen_vw mema
WHERE onde.onde_id = mema.onde_id (+)
AND onde.kafd_id = kafd.kafd_id
AND onde.herk_id = herk.herk_id (+)
AND UPPER(kafd.code) = UPPER('metab')
AND mema.onde_id IS NULL
AND onde.datum_binnen BETWEEN TO_DATE('01-01-2003', 'DD-MM-YYYY') AND TO_DATE('01-01-2004', 'DD-MM-YYYY')
UNION
SELECT onde.onderzoeknr, 'J' decl, herk.code
FROM     KGC_ONDERZOEKEN onde
	   , KGC_HERKOMSTEN herk
	   , KGC_KGC_AFDELINGEN kafd
	   , KGC_metab_machtigingen_vw mema
WHERE onde.onde_id = mema.onde_id (+)
AND onde.kafd_id = kafd.kafd_id
AND onde.herk_id = herk.herk_id (+)
AND UPPER(kafd.code) = UPPER('metab')
AND mema.onde_id IS NOT NULL
AND onde.datum_binnen BETWEEN TO_DATE('01-01-2003', 'DD-MM-YYYY') AND TO_DATE('01-01-2004', 'DD-MM-YYYY')

 ;

/
QUIT
