CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_MEETWAARDE_DETAILS_VW" ("MDET_ID", "VOLGORDE", "PROMPT", "WAARDE", "MEET_ID", "STOF_ID", "KAFD_ID", "STOF_CODE", "MEETWAARDE", "MWST_ID", "METI_ID", "DATUM_AANMELDING", "STGR_ID", "FRAC_ID", "STAN_ID", "FRACTIENUMMER", "ONDE_ID", "ONDERZOEKNR", "ONDE_STATUS") AS
  SELECT mdet.mdet_id
,      mdet.volgorde
,      mdet.prompt
,      mdet.waarde
,      mdet.meet_id
,      meet.stof_id
,      stof.kafd_id
,      stof.code stof_code
,      meet.tonen_als meetwaarde
,      nvl(meet.mwst_id,stof.mwst_id) mwst_id
,      meet.meti_id
,      meti.datum_aanmelding
,      meti.stgr_id
,      meti.frac_id
,      meti.stan_id
,      nvl(frac.fractienummer,stan.standaardfractienummer)  fractienummer
,      meti.onde_id
,      onde.onderzoeknr
,      onde.status onde_status
from   kgc_stoftesten stof
,      bas_standaardfracties stan
,      kgc_onderzoeken onde
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_meetwaarde_details mdet
where mdet.meet_id = meet.meet_id
and    meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meet.meet_reken in ( 'V', 'M' )
and    meet.afgerond = 'N'
and    meti.afgerond = 'N'
and    meti.frac_id = frac.frac_id (+)
and    meti.frac_id = stan.stan_id (+)
and    meti.onde_id = onde.onde_id (+)
and    nvl(onde.afgerond,'N') = 'N'
 ;

/
QUIT
