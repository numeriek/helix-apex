CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BASMEET01C_FRAC_VW" ("SOORT_MONSTER", "FRACTIENUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "ONDERZOEKNR", "ONDERZOEKSTYPE", "MONSTERNUMMER", "FAMILIENUMMER", "ONDE_DATUM_BINNEN", "STGR_CODE", "STGR_OMSCHRIJVING", "METI_DATUM_AANMELDING", "METI_CONCLUSIE", "METI_AFGEROND", "METI_HERHALEN", "ONGR_CODE", "MEGR_NUMMER", "MONS_PERS_AANSPREKEN", "MONS_PERS_GEBOORTEDATUM", "PERSOON_INFO_ONDE", "INDICATIE", "DATUM_SELECTIE_ROBOT", "METI_SPOED", "KAFD_ID", "ONGR_ID", "ONDE_ID", "MONS_ID", "PERS_ID", "FRAC_ID", "MATE_ID", "STGR_ID", "METI_ID", "MEGR_ID", "TAAL_ID", "MEET_VOLGORDE") AS
  SELECT 'O' soort_monster -- Onderzoeksmonsters
,      frac.fractienummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      onde.onderzoeknr
,      onde.onderzoekstype
,      mons.monsternummer
,      kgc_fami_00.familie_bij_onderzoek(onde.onde_id,'N') familienummer
,      onde.datum_binnen onde_datum_binnen
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.datum_aanmelding meti_datum_aanmelding
,      to_char(meti.conclusie)  meti_conclusie -- Mantis 9355 to_char added by SKA 08-04-2016 release 8.11.0.3
,      meti.afgerond meti_afgerond
,      meti.herhalen meti_herhalen
,      ongr.code ongr_code
,      megr.nummer megr_nummer
,      pers.aanspreken mons_pers_aanspreken
,      pers.geboortedatum mons_pers_geboortedatum
,      kgc_pers_00.persoon_info( null, onde.onde_id ) persoon_info_onde
,      kgc_indi_00.indi_omschrijving( onde.onde_id ) indicatie
,      meti.datum_selectie_robot
,      meti.prioriteit meti_spoed
,      mons.kafd_id
,      mons.ongr_id
,      onde.onde_id
,      mons.mons_id
,      mons.pers_id
,      frac.frac_id
,      mons.mate_id
,      meti.stgr_id
,      meti.meti_id
,      megr.megr_id
,      onde.taal_id
,      bas_meti_00.volgorde( meti.meti_id ) meet_volgorde
FROM   kgc_personen                pers
,      kgc_meting_groepen          megr
,      kgc_megr_samenstelling      mgsa
,      kgc_onderzoeksgroepen       ongr
,      kgc_materialen              mate
,      kgc_stoftestgroepen         stgr
,      kgc_monsters                mons
,      kgc_onderzoeken             onde
,      bas_fracties                frac
,      bas_metingen                meti
WHERE  frac.mons_id  = mons.mons_id
AND    mons.ongr_id  = ongr.ongr_id
AND    mons.mate_id  = mate.mate_id
AND    meti.onde_id  = onde.onde_id
AND    meti.frac_id  = frac.frac_id
AND    mons.pers_id  = pers.pers_id
AND    onde.afgerond = 'N'
AND    meti.stgr_id  = stgr.stgr_id (+)
AND    meti.meti_id  = mgsa.meti_id (+)
AND    mgsa.megr_id  = megr.megr_id (+)
UNION ALL
SELECT 'C' -- Controlemonsters
,      NULL -- frac.fractienummer
,      mate.code
,      mate.omschrijving
,      NULL -- onde.onderzoeknr
,      NULL -- onde.onderzoekstype
,      'Controle' -- mons.monsternummer
,      NULL -- kgc_fami_00.familie_bij_onderzoek(onde.onde_id,'N')
,      NULL -- onde.datum_binnen
,      stgr.code
,      stgr.omschrijving
,      meti.datum_aanmelding
,      to_char(meti.conclusie) conclusie  -- Mantis 9355 to_char added by SKA 08-04-2016 release 8.11.0.3
,      meti.afgerond
,      meti.herhalen
,      ongr.code
,      NULL -- megr.nummer
,      stan.standaardfractienummer -- pers.aanspreken
,      NULL -- pers.geboortedatum
,      NULL -- kgc_pers_00.persoon_info( null, onde.onde_id )
,      NULL -- kgc_indi_00.indi_omschrijving( onde.onde_id )
,      meti.datum_selectie_robot
,      meti.prioriteit meti_spoed
,      kafd.kafd_id -- mons.kafd_id
,      stan.ongr_id -- mons.ongr_id
,      NULL -- onde.onde_id
,      NULL -- mons.mons_id
,      NULL -- mons.pers_id
,      NULL -- frac.frac_id
,      stan.mate_id -- mons.mate_id
,      meti.stgr_id
,      meti.meti_id
,      NULL -- megr.megr_id
,      NULL -- onde.taal_id
,      bas_meti_00.volgorde( meti.meti_id ) meet_volgorde
FROM   kgc_kgc_afdelingen          kafd
,      kgc_onderzoeksgroepen       ongr
,      kgc_materialen              mate
,      bas_standaardfracties       stan
,      bas_metingen                meti
,      kgc_stoftestgroepen         stgr
WHERE  kafd.kafd_id  = ongr.kafd_id
AND    ongr.ongr_id  = stan.ongr_id
AND    mate.mate_id  = stan.mate_id
AND    meti.stan_id  = stan.stan_id
AND    meti.stgr_id  = stgr.stgr_id (+);

/
QUIT
