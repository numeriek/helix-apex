CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FAON_BETROKKENEN_VW" ("FOBE_ID", "ONDE_ID", "PERS_ID", "FAON_ID", "FAMI_ID", "FAMILIEONDERZOEKNR", "FAMILIENUMMER", "FAMI_NAAM", "PERS_ZISNR", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ADVIESVRAGER", "FOBE_ONDE_ID") AS
  SELECT fobe.fobe_id
,      fobe.onde_id
,      fobe.pers_id
,      fobe.faon_id
,      faon.fami_id
,      faon.familieonderzoeknr
,      fami.familienummer
,      fami.naam fami_naam
,      pers.zisnr pers_zisnr
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      decode( fobe2.onde_id
                   , null, 'N'
                  , 'J'
                  ) adviesvrager
,      fobe2.onde_id fobe_onde_id
from   kgc_families fami
,      kgc_familie_onderzoeken faon
,      kgc_personen pers
,      kgc_faon_betrokkenen fobe -- adviesvraag
,      kgc_faon_betrokkenen fobe2 -- alle betrokkenen
  WHERE fobe.faon_id = faon.faon_id
and    faon.fami_id = fami.fami_id
and    fobe.onde_id is not null
and    faon.faon_id = fobe2.faon_id
and    fobe2.pers_id = pers.pers_id;

/
QUIT
