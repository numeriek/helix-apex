CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KWEEKFORMULIEREN_VW" ("KAFD_CODE", "ONGR_CODE", "PERS_ID", "PERSOON_INFO", "ZWAN_DUUR", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "MONSTERNUMMER", "HERK_CODE", "HERK_OMSCHRIJVING", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "RELA_AANSPREKEN", "MONS_HOEVEELHEID", "COND_CODE_MONS", "COND_OMSCHRIJVING_MONS", "ONDE_ID", "ONDERZOEKNR", "ONDE_GEPLANDE_EINDDATUM", "INDICATIES", "ONDE_REDEN", "FRAC_ID", "FRACTIENUMMER", "FRTY_CODE", "FRTY_OMSCHRIJVING", "COND_CODE_FRAC", "COND_OMSCHRIJVING_FRAC", "SPOED") AS
  SELECT kafd.code kafd_code
,      ongr.code ongr_code
,      mons.pers_id
,      kgc_pers_00.info( mons.pers_id ) persoon
,      kgc_zwan_00.duur( onde.onde_id ) zwan_duur
,      mons.mate_id
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      mons.monsternummer
,      herk.code herk_code
,      herk.omschrijving herk_omschrijving
,      mons.datum_aanmelding mons_datum_aanmelding
,      mons.datum_afname mons_datum_afname
,      rela.aanspreken rela_aanspreken
,      mons.hoeveelheid_monster ||' '|| mate.eenheid mons_hoeveelheid
,      cond1.code cond_code_mons
,      cond1.omschrijving cond_omschrijving_mons
,      onmo.onde_id
,      onde.onderzoeknr
,      onde.geplande_einddatum onde_geplande_einddatum
,      kgc_indi_00.onderzoeksindicaties( onde.onde_id ) indicaties
,      onde.omschrijving onde_reden
,      frac.frac_id
,      frac.fractienummer
,      frty.code frty_code
,      frty.omschrijving frty_omschrijving
,      cond2.code cond_code_frac
,      cond2.omschrijving cond_omschrijving_frac
,      onde.spoed
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_herkomsten herk
,      kgc_condities cond1
,      kgc_condities cond2
,      bas_fractie_types frty
,      kgc_materialen mate
,      bas_fracties frac
,      kgc_relaties rela
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      kgc_onderzoek_monsters onmo
where onmo.onde_id = onde.onde_id
and    onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.rela_id = rela.rela_id
and    onmo.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
and    mons.herk_id = herk.herk_id (+)
and    mons.cond_id = cond1.cond_id (+)
and    mons.mons_id = frac.mons_id
and    frac.frty_id = frty.frty_id (+)
and    frac.cond_id = cond2.cond_id (+)
and   onde.afgerond = 'N'
 ;

/
QUIT
