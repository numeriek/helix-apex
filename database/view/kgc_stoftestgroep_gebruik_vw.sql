CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_STOFTESTGROEP_GEBRUIK_VW" ("STGR_ID", "KAFD_ID", "CODE", "OMSCHRIJVING", "PROTOCOL", "SNELHEID", "STANDAARD", "SCREENINGTEST", "GETAL_OPNEMEN", "VERVALLEN", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "MATE_ID", "INDI_ID", "FRTY_ID", "VOLGORDE") AS
  SELECT stgr.stgr_id
, stgr.kafd_id
, stgr.code
, stgr.omschrijving
, stgr.protocol
, stgg.snelheid
, stgg.standaard
, stgg.screeningtest
, stgg.getal_opnemen
, decode( stgg.vervallen
, 'N', stgr.vervallen
, stgg.vervallen
) vervallen
, stgg.created_by
, stgg.creation_date
, stgg.last_updated_by
, stgg.last_update_date
, stgg.mate_id
, stgg.indi_id
, stgg.frty_id
, stgg.volgorde
from kgc_stoftestgroepen stgr
, kgc_stoftestgroep_gebruik stgg
where stgr.stgr_id = stgg.stgr_id
 ;

/
QUIT
