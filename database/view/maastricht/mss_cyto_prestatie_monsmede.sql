CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_CYTO_PRESTATIE_MONSMEDE" ("MEDE_ID", "MONS_ID", "DATUM_AANMELDING") AS
  Select MONS.MEDE_ID,
        MONS.MONS_ID,
        MONS.DATUM_AANMELDING
 From  kgc_monsters MONS
 Where MONS_ID in
           (Select Distinct(MONS_ID)
            from   Kgc_Onderzoek_Monsters
            Where  ONMO_ID in
                    (Select Distinct(ONMO_ID)
                     from   Bas_metingen
                     Where  STGR_ID in
                            (Select STGR_ID
                             From Kgc_stoftestgroepen
                             where Code in
                                   ('KARYO-PR','KARYO-TU','MLPA_POST',
                                    'MLPA_PRE','FISH-KARYO',
                                    'BLOOM-SCE','FAN-ATAX',
                                    'INACTX','SNP','AFP',
                                    'ACHE','CYTOSPINXY','MD','DOOR')) ) );

/
QUIT
