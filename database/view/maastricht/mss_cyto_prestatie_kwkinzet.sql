CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_CYTO_PRESTATIE_KWKINZET" ("MEDE_ID", "DATUM_INZET") AS
  Select  Mede.Mede_Id,
        datum_inzet
from    Kgc_medewerkers MEDE,
        Kgc_Kweekfracties KFRAC,
        Bas_fracties FRAC ,
        MSS_CYTO_PRESTATIE_KWKFRACPROT MFP
Where   KFRAC.MEDE_ID_INZET = MEDE.MEDE_ID
and     FRAC.frac_id = MFP.Fractie
And     FRAC.frac_id = KFRAC.frac_id;

/
QUIT
