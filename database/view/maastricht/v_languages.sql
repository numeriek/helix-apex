CREATE OR REPLACE FORCE VIEW "HELIX"."V_LANGUAGES" ("LANGUAGE", "LANGUAGE_FULL", "SORT_ORDER") AS
  SELECT value_short language, desc_value language_full, sort_order
     FROM gen_appl_lookups
    WHERE lookup_type = 'LANGUAGE'
;

/
QUIT
