CREATE OR REPLACE FORCE VIEW "HELIX"."BEHUITS51_52LAB_ALG_VW" ("KOPIE", "LBL_LOGO51", "LBL_LOGO52", "KOPLAB", "TITELMDEPD", "GEADRESS", "LBL_LKTD", "LKTD", "LBL_NGBP", "NGBP", "LBL_BETAAN", "LBL_PAT_GEG", "LBL_NAAM_PAT", "NAAM_PAT", "LBL_AANVRDATUM", "AANVRAAGDATUM", "LBL_INDICATIES", "INDICATIES", "LBL_ZWANGER_DU", "ZWANGER_DU", "LBL_ONDE_GEG", "LBL_ON_DAT", "LBL_AFNDAT", "LBL_HERKOMST", "LBL_MONS", "LBL_FRAC", "LBL_MDET_GEG", "LBL_AFN_PL", "LBL_RESULT", "LBL_TEST", "LBL_SENSI", "LBL_MAT", "LBL_UITMAT", "LBL_MONSMAT", "LBL_RES_MD", "LBL_CONCLUSIE", "LBL_TOELICHTING", "LBL_GROET", "FORMELE_NAAM1", "FUNCTIE1", "FORMELE_NAAM2", "FUNCTIE2", "LBL_CC_ORG", "CC_ORIG", "LBL_DISCLAIMER", "LBL_EL_AUT", "VERVOLGVEL_INFO", "ONDE_ID", "ONDERZOEKNR", "KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "TAAL_ID", "RELA_ID", "UITS_ID", "BRTY_ID", "BRTY_CODE", "PERS_ID", "KOHO_ID", "TOELICHTING", "ONWY_OMSCH", "LBL_ONDE_R", "ONDE_R", "ONDE_OMSCHR") AS
  SELECT 'N'                                                                                                                                                                                         kopie
  ,      CASE
            WHEN mss_brie_enotificatie.is_interne_kg_rela(uits.rela_id) = 'FALSE'
            AND  kgc_brie_00.uitslag_eerder_geprint(uits.uits_id,uits.brty_id) = 'N'
            THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGO',onde.taal_id)
            WHEN mss_brie_enotificatie.is_interne_kg_rela(uits.rela_id) = 'FALSE'
            AND  kgc_brie_00.uitslag_eerder_geprint(uits.uits_id,uits.brty_id) = 'J'
            THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGOK',onde.taal_id)
            WHEN mss_brie_enotificatie.is_interne_kg_rela(uits.rela_id) = 'TRUE'
            AND  kgc_brie_00.uitslag_eerder_geprint(uits.uits_id,uits.brty_id) = 'N'
            THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGOI',onde.taal_id)
            ELSE mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGOIK',onde.taal_id)
         END                                                                                                                                                                                         lbl_logo51
  ,      CASE
           WHEN mss_brie_enotificatie.is_interne_kg_rela(uits.rela_id) = 'TRUE'
           THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGOI',onde.taal_id)
           ELSE mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id, NULL,'UITSLAG','LBL_LOGO',onde.taal_id)
         END                                                                                                                                                                                         lbl_logo52
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','KOPLABNW',onde.taal_id)                                                                                                    koplab
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','TITELMDEPD',onde.taal_id,onde.onderzoeknr)                                                                                 titelmdepd
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ADRES',onde.taal_id,kgc_adres_00.relatie (uits.rela_id, 'J'))                                                          geadress
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LKTD',onde.taal_id)                                                                                                    lbl_lktd
  ,      mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LKTD',onde.taal_id,TO_CHAR (pers.geboortedatum, 'dd-mm-yyyy'),kgc_pers_00.bsn_opgemaakt (pers.pers_id)
                               ,pers.zisnr,mss_algemeen.bep_familienummer (onde.onde_id),mss_algemeen.bep_uw_kenmerk(onde.onde_id),onde.onderzoeknr
                               ,TO_CHAR (SYSDATE,'dd-mm-yyyy'))                                                                                                                                      lktd
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_NGBP',onde.taal_id)                                                                                                    lbl_ngbp
  ,      mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','NGBP',onde.taal_id,pers.aanspreken || ' (' || pers.geslacht || ')',TO_CHAR (pers.geboortedatum, 'dd-mm-yyyy')
                               ,kgc_pers_00.bsn_opgemaakt(pers.pers_id),pers.zisnr)                                                                                                                  ngbp
  ,      mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_BETAAN',onde.taal_id
                               ,CASE
                                  WHEN ongr.code IN ('PM', 'PV')
                                  THEN 'postnatale genotypering.'
                                  WHEN ongr.code IN ('TM', 'TV')
                                  THEN 'Diagnostiek van hematologische maligniteit.'
                                  WHEN ongr.code = 'PR'
                                  THEN 'Prenatale genotypering.'
                                  ELSE kgc_attr_00.waarde('KGC_ONDERZOEKSWIJZEN', 'BETREFT', onwy.onwy_id) -- Voor DNA, CYTOM en CYTOV
                                END
                               ,CASE
                                  WHEN ongr.code = 'PR'
                                  THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_PR',onde.taal_id,TO_CHAR (onde.datum_binnen, 'dd-mm-yyyy'))
                                  WHEN ongr.code in ('TM', 'TV')
                                  THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_TUM',onde.taal_id)
                                  WHEN ongr.code IN ('PM', 'PV')
                                  THEN kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_PO',onde.taal_id)
                                  ELSE kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_OV',onde.taal_id)
                                END)                                                                                                                                                                 lbl_betaan
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_PAT_GE',onde.taal_id)                                                                                                  lbl_pat_geg
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_NM_PAT',onde.taal_id)                                                                                                  lbl_naam_pat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','NM_PAT',onde.taal_id,pers.aanspreken || ' (' || pers.geslacht || ')')                                                      naam_pat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AVRDAT',onde.taal_id)                                                                                                  lbl_aanvrdatum
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','AVRDAT',onde.taal_id,to_char(onde.datum_binnen,'dd-mm-yyyy'))                                                              aanvraagdatum
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_INDICA',onde.taal_id)                                                                                                  lbl_indicaties
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','INDICA_PAT',onde.taal_id,kgc_indi_00.indi_omschrijving (onde.onde_id))                                                     indicaties
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ZWG_DU',onde.taal_id)                                                                                                  lbl_zwanger_du
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','ZWG_DU',onde.taal_id,nvl(kgc_zwan_00.duur (onde.onde_id, 'LANG'),'Onbekend'))                                              zwanger_du
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ONDE_G',onde.taal_id)                                                                                                  lbl_onde_geg
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ON_DAT',onde.taal_id)                                                                                                  lbl_on_dat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AFNDAT',onde.taal_id)                                                                                                  lbl_afndat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_HERK',onde.taal_id)                                                                                                    lbl_herkomst
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MONS',onde.taal_id)                                                                                                    lbl_mons
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_FRAC',onde.taal_id)                                                                                                    lbl_frac
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MDET_G',onde.taal_id)                                                                                                  lbl_mdet_geg
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AFN_PL',onde.taal_id)                                                                                                  lbl_afn_pl
  ,      CASE
           WHEN EXISTS
                  (SELECT meti.result
                   FROM   behuits51_52lab_meti_vw meti
                   WHERE  meti.uits_id = uits.uits_id)
           THEN kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_RESULT',onde.taal_id)
           ELSE NULL
         END                                                                                                                                                                                         lbl_result
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_TEST',onde.taal_id)                                                                                                    lbl_test
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_SENSI',onde.taal_id)                                                                                                   lbl_sensi
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MAT',onde.taal_id)                                                                                                     lbl_mat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_UITMAT',onde.taal_id)                                                                                                  lbl_uitmat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MONSMA',onde.taal_id)                                                                                                  lbl_monsmat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_RES_MD',onde.taal_id)                                                                                                  lbl_res_md
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_CONCLU',onde.taal_id,uits.tekst)                                                                                       lbl_conclusie
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_TOEL',onde.taal_id,uits.toelichting)                                                                                   lbl_toelichting
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_GROET',onde.taal_id)                                                                                                   lbl_groet
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FORM_NAAM1',onde.taal_id,NVL(kgc_vert_00.vertaling('MEDE',mede1.mede_id,NVL(mede1.formele_naam,mede1.naam),onde.taal_id)
                               ,NVL (mede1.formele_naam, mede1.naam)))                                                                                                                               formele_naam1
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FUNCTIE1',onde.taal_id,mede1.func_omschr)                                                                                  functie1
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FORM_NAAM2',onde.taal_id,NVL(kgc_vert_00.vertaling('MEDE',mede2.mede_id,NVL(mede2.formele_naam,mede2.naam),onde.taal_id)
                               ,NVL (mede2.formele_naam, mede2.naam)))                                                                                                                               formele_naam2
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FUNCTIE2',onde.taal_id,mede2.func_omschr)                                                                                  functie2
  ,      CASE
           WHEN kgc_rapp_00.cc_kopiehouders(uits.uits_id, uits.rela_id) IS NULL
           THEN NULL
           ELSE kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_CC_ORG',onde.taal_id,'Cc:')
         END                                                                                                                                                                                         lbl_cc_org
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_KOPIEH',onde.taal_id,kgc_rapp_00.cc_kopiehouders(uits.uits_id,uits.rela_id))                                           cc_orig
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_DISCLA',onde.taal_id)                                                                                                  lbl_disclaimer
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_EL_AUT',onde.taal_id)                                                                                                  lbl_el_aut
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_VV_VEL',onde.taal_id)                                                                                                  vervolgvel_info
  ,      onde.onde_id                                                                                                                                                                                onde_id
  ,      onde.onderzoeknr                                                                                                                                                                            onderzoeknr
  ,      onde.kafd_id                                                                                                                                                                                kafd_id
  ,      kafd.code                                                                                                                                                                                   kafd_code
  ,      onde.ongr_id                                                                                                                                                                                ongr_id
  ,      ongr.code                                                                                                                                                                                   ongr_code
  ,      onde.taal_id                                                                                                                                                                                taal_id
  ,      uits.rela_id                                                                                                                                                                                rela_id
  ,      uits.uits_id                                                                                                                                                                                uits_id
  ,      uits.brty_id                                                                                                                                                                                brty_id
  ,      mss_algemeen.bep_brty_code (uits.brty_id)                                                                                                                                                   brty_code
  ,      onde.pers_id                                                                                                                                                                                pers_id
  ,      TO_NUMBER (NULL)                                                                                                                                                                            koho_id
  ,      uits.toelichting                                                                                                                                                                            toelichting
  ,      onwy.omschrijving                                                                                                                                                                           onwy_omsch
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ONDE_R',onde.taal_id)                                                                                                  lbl_onde_r
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','ONDE_R',onde.taal_id,onde.omschrijving)                                                                                    onde_r
  ,      onde.omschrijving                                                                                                                                                                           onde_omschr
  FROM   ( SELECT mede2.mede_id mede_id
           ,      mede2.naam naam
           ,      mede2.formele_naam formele_naam
           ,      func2.omschrijving func_omschr
           FROM   kgc_functies func2
           ,      kgc_mede_kafd meka2
           ,      kgc_medewerkers mede2
           WHERE  meka2.mede_id = mede2.mede_id
           AND    meka2.kafd_id = kgc_kafd_00.id ('LAB')
           AND    func2.func_id(+) = meka2.func_id
          )                                                      mede2
  ,       ( SELECT mede1.mede_id mede_id
            ,      mede1.naam naam
            ,      mede1.formele_naam formele_naam
            ,      func1.omschrijving func_omschr
            FROM   kgc_functies func1
            ,      kgc_mede_kafd meka1
            ,      kgc_medewerkers mede1
            WHERE  meka1.mede_id = mede1.mede_id
            AND    meka1.kafd_id = kgc_kafd_00.id ('LAB')
            AND    func1.func_id(+) = meka1.func_id
           )                                                     mede1
  ,        kgc_brieftypes                                        brty
  ,        kgc_personen                                          pers
  ,        kgc_relaties                                          aanv
  ,        kgc_kgc_afdelingen                                    kafd
  ,        kgc_onderzoekswijzen                                  onwy
  ,        kgc_onderzoeksgroepen                                 ongr
  ,        kgc_onderzoeken                                       onde
  ,        kgc_uitslagen                                         uits
  WHERE    uits.tekst IS NOT NULL
  AND      onde.onde_id = uits.onde_id
  AND      ongr.ongr_id = onde.ongr_id
  AND      kafd.kafd_id = onde.kafd_id
  AND      onwy.code (+) = onde.onderzoekswijze
  AND      aanv.rela_id = onde.rela_id
  AND      pers.pers_id = onde.pers_id
  AND      brty.brty_id = uits.brty_id
  AND      (   (ongr.code not in ('PM','PV','PR','TM','TV'))
            or (    ongr.code in ('PM','PV','PR','TM','TV')
                and brty.code IN
                      ( SELECT cawa.waarde
                        FROM   kgc_categorie_waarden cawa
                        ,      kgc_categorieen cate
                        ,      kgc_categorie_types caty
                        WHERE  caty.code = 'BRIE_CYTO'
                        AND    cate.caty_id = caty.caty_id
                        AND    cate.kafd_id = kafd.kafd_id
                        AND    cawa.cate_id = cate.cate_id
                      )
               )
           )
  AND      mede1.mede_id(+) = uits.mede_id
  AND      mede2.mede_id(+) = uits.mede_id2
  UNION ALL
  SELECT 'J'                                                                                                                                                                                         kopie
  ,      CASE
            WHEN mss_brie_enotificatie.is_interne_kg_rela(koho.rela_id) = 'FALSE'
            THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGOK',onde.taal_id)
            ELSE mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGOIK',onde.taal_id)
         END                                                                                                                                                                                         lbl_logo51
  ,      CASE
           WHEN mss_brie_enotificatie.is_interne_kg_rela(koho.rela_id) = 'FALSE'
           THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LOGOK',onde.taal_id)
           ELSE mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id, NULL,'UITSLAG','LBL_LOGOIK',onde.taal_id)
         END                                                                                                                                                                                         lbl_logo52
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','KOPLABNW',onde.taal_id)                                                                                                    koplab
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','TITELMDEPD',onde.taal_id,onde.onderzoeknr)                                                                                 titelmdepd
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ADRES',onde.taal_id,kgc_adres_00.relatie (koho.rela_id, 'J'))                                                          geadress
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_LKTD',onde.taal_id)                                                                                                    lbl_lktd
  ,      mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LKTD',onde.taal_id,TO_CHAR (pers.geboortedatum, 'dd-mm-yyyy'),kgc_pers_00.bsn_opgemaakt (pers.pers_id)
                               ,pers.zisnr,mss_algemeen.bep_familienummer (onde.onde_id),mss_algemeen.bep_uw_kenmerk(onde.onde_id),onde.onderzoeknr
                               ,TO_CHAR (SYSDATE,'dd-mm-yyyy'))                                                                                                                                      lktd
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_NGBP',onde.taal_id)                                                                                                    lbl_ngbp
  ,      mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','NGBP',onde.taal_id,pers.aanspreken || ' (' || pers.geslacht || ')',TO_CHAR (pers.geboortedatum, 'dd-mm-yyyy')
                               ,kgc_pers_00.bsn_opgemaakt(pers.pers_id),pers.zisnr)                                                                                                                  ngbp
  ,      mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_BETAAN',onde.taal_id
                               ,CASE
                                  WHEN ongr.code IN ('PM', 'PV')
                                  THEN 'postnatale genotypering.'
                                  WHEN ongr.code IN ('TM', 'TV')
                                  THEN 'Diagnostiek van hematologische maligniteit.'
                                  WHEN ongr.code = 'PR'
                                  THEN 'Prenatale genotypering.'
                                  ELSE kgc_attr_00.waarde('KGC_ONDERZOEKSWIJZEN', 'BETREFT', onwy.onwy_id) -- Voor DNA, CYTOM en CYTOV
                                END
                               ,CASE
                                  WHEN ongr.code = 'PR'
                                  THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_PR',onde.taal_id,TO_CHAR (onde.datum_binnen, 'dd-mm-yyyy'))
                                  WHEN ongr.code in ('TM', 'TV')
                                  THEN mss_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_TUM',onde.taal_id)
                                  WHEN ongr.code IN ('PM', 'PV')
                                  THEN kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_PO',onde.taal_id)
                                  ELSE kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AH_OV',onde.taal_id)
                                END)                                                                                                                                                                 lbl_betaan
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_PAT_GE',onde.taal_id)                                                                                                  lbl_pat_geg
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_NM_PAT',onde.taal_id)                                                                                                  lbl_naam_pat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','NM_PAT',onde.taal_id,pers.aanspreken || ' (' || pers.geslacht || ')')                                                      naam_pat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AVRDAT',onde.taal_id)                                                                                                  lbl_aanvrdatum
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','AVRDAT',onde.taal_id,to_char(onde.datum_binnen,'dd-mm-yyyy'))                                                              aanvraagdatum
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_INDICA',onde.taal_id)                                                                                                  lbl_indicaties
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','INDICA_PAT',onde.taal_id,kgc_indi_00.indi_omschrijving (onde.onde_id))                                                     indicaties
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ZWG_DU',onde.taal_id)                                                                                                  lbl_zwanger_du
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','ZWG_DU',onde.taal_id,nvl(kgc_zwan_00.duur (onde.onde_id, 'LANG'),'Onbekend'))                                              zwanger_du
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ONDE_G',onde.taal_id)                                                                                                  lbl_onde_geg
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ON_DAT',onde.taal_id)                                                                                                  lbl_on_dat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AFNDAT',onde.taal_id)                                                                                                  lbl_afndat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_HERK',onde.taal_id)                                                                                                    lbl_herkomst
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MONS',onde.taal_id)                                                                                                    lbl_mons
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_FRAC',onde.taal_id)                                                                                                    lbl_frac
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MDET_G',onde.taal_id)                                                                                                  lbl_mdet_geg
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_AFN_PL',onde.taal_id)                                                                                                  lbl_afn_pl
  ,      CASE
           WHEN EXISTS
                  (SELECT meti.result
                   FROM   behuits51_52lab_meti_vw meti
                   WHERE  meti.uits_id = uits.uits_id)
           THEN kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_RESULT',onde.taal_id)
           ELSE NULL
         END                                                                                                                                                                                         lbl_result
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_TEST',onde.taal_id)                                                                                                    lbl_test
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_SENSI',onde.taal_id)                                                                                                   lbl_sensi
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MAT',onde.taal_id)                                                                                                     lbl_mat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_UITMAT',onde.taal_id)                                                                                                  lbl_uitmat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_MONSMA',onde.taal_id)                                                                                                  lbl_monsmat
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_RES_MD',onde.taal_id)                                                                                                  lbl_res_md
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_CONCLU',onde.taal_id,uits.tekst)                                                                                       lbl_conclusie
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_TOEL',onde.taal_id,uits.toelichting)                                                                                   lbl_toelichting
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_GROET',onde.taal_id)                                                                                                   lbl_groet
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FORM_NAAM1',onde.taal_id,NVL(kgc_vert_00.vertaling('MEDE',mede1.mede_id,NVL(mede1.formele_naam,mede1.naam),onde.taal_id)
                               ,NVL (mede1.formele_naam, mede1.naam)))                                                                                                                               formele_naam1
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FUNCTIE1',onde.taal_id,mede1.func_omschr)                                                                                  functie1
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FORM_NAAM2',onde.taal_id,NVL(kgc_vert_00.vertaling('MEDE',mede2.mede_id,NVL(mede2.formele_naam,mede2.naam),onde.taal_id)
                               ,NVL (mede2.formele_naam, mede2.naam)))                                                                                                                               formele_naam2
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','FUNCTIE2',onde.taal_id,mede2.func_omschr)                                                                                  functie2
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_CC_ORG',onde.taal_id,'Origineel:')                                                                                     lbl_cc_org
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_KOPIEH',onde.taal_id,kgc_rapp_00.origineel(uits.rela_id))                                                              cc_orig
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_DISCLA',onde.taal_id)                                                                                                  lbl_disclaimer
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_EL_AUT',onde.taal_id)                                                                                                  lbl_el_aut
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_VV_VEL',onde.taal_id)                                                                                                  vervolgvel_info
  ,      onde.onde_id                                                                                                                                                                                onde_id
  ,      onde.onderzoeknr                                                                                                                                                                            onderzoeknr
  ,      onde.kafd_id                                                                                                                                                                                kafd_id
  ,      kafd.code                                                                                                                                                                                   kafd_code
  ,      onde.ongr_id                                                                                                                                                                                ongr_id
  ,      ongr.code                                                                                                                                                                                   ongr_code
  ,      onde.taal_id                                                                                                                                                                                taal_id
  ,      uits.rela_id                                                                                                                                                                                rela_id
  ,      uits.uits_id                                                                                                                                                                                uits_id
  ,      uits.brty_id                                                                                                                                                                                brty_id
  ,      mss_algemeen.bep_brty_code (uits.brty_id)                                                                                                                                                   brty_code
  ,      onde.pers_id                                                                                                                                                                                pers_id
  ,      koho.koho_id                                                                                                                                                                                koho_id
  ,      uits.toelichting                                                                                                                                                                            toelichting
  ,      onwy.omschrijving                                                                                                                                                                           onwy_omsch
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','LBL_ONDE_R',onde.taal_id)                                                                                                  lbl_onde_r
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','ONDE_R',onde.taal_id,onde.omschrijving)                                                                                    onde_r
  ,      onde.omschrijving                                                                                                                                                                           onde_omschr
  FROM   ( SELECT mede2.mede_id mede_id
           ,      mede2.naam naam
           ,      mede2.formele_naam formele_naam
           ,      func2.omschrijving func_omschr
           FROM   kgc_functies func2
           ,      kgc_mede_kafd meka2
           ,      kgc_medewerkers mede2
           WHERE  meka2.mede_id = mede2.mede_id
           AND    meka2.kafd_id = kgc_kafd_00.id ('LAB')
           AND    func2.func_id(+) = meka2.func_id
          )                                                      mede2
  ,       ( SELECT mede1.mede_id mede_id
            ,      mede1.naam naam
            ,      mede1.formele_naam formele_naam
            ,      func1.omschrijving func_omschr
            FROM   kgc_functies func1
            ,      kgc_mede_kafd meka1
            ,      kgc_medewerkers mede1
            WHERE  meka1.mede_id = mede1.mede_id
            AND    meka1.kafd_id = kgc_kafd_00.id ('LAB')
            AND    func1.func_id(+) = meka1.func_id
           )                                                     mede1
  ,        kgc_kopiehouders                                      koho
  ,        kgc_brieftypes                                        brty
  ,        kgc_personen                                          pers
  ,        kgc_relaties                                          aanv
  ,        kgc_kgc_afdelingen                                    kafd
  ,        kgc_onderzoekswijzen                                  onwy
  ,        kgc_onderzoeksgroepen                                 ongr
  ,        kgc_onderzoeken                                       onde
  ,        kgc_uitslagen                                         uits
  WHERE    uits.tekst IS NOT NULL
  AND      onde.onde_id = uits.onde_id
  AND      ongr.ongr_id = onde.ongr_id
  AND      kafd.kafd_id = onde.kafd_id
  AND      onwy.code (+) = onde.onderzoekswijze
  AND      aanv.rela_id = onde.rela_id
  AND      pers.pers_id = onde.pers_id
  AND      brty.brty_id = uits.brty_id
  AND      (   (ongr.code not in ('PM','PV','PR','TM','TV'))
            or (    ongr.code in ('PM','PV','PR','TM','TV')
                and brty.code IN
                      ( SELECT cawa.waarde
                        FROM   kgc_categorie_waarden cawa
                        ,      kgc_categorieen cate
                        ,      kgc_categorie_types caty
                        WHERE  caty.code = 'BRIE_CYTO'
                        AND    cate.caty_id = caty.caty_id
                        AND    cate.kafd_id = kafd.kafd_id
                        AND    cawa.cate_id = cate.cate_id
                      )
               )
           )
  AND      koho.uits_id     = uits.uits_id
  AND      mede1.mede_id(+) = uits.mede_id
  AND      mede2.mede_id(+) = uits.mede_id2;

/
QUIT
