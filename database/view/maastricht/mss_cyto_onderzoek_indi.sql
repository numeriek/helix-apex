CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_CYTO_ONDERZOEK_INDI" ("PATIENT_NR", "ONDERZOEK_NR", "GEBOORTE_DATUM", "ONDERZOEK_GROEP", "UITSLAG_CODE", "INDICATIE", "INDIOMS", "AFGEROND", "HOOFD_INDICATIE", "INDICATIE_VOLG", "DECLAREREN", "DATUM_BINNEN", "DATUM_AUTORISATIE", "ONDE_ID", "ONUI_ID", "INDI_ID", "ONGR_ID", "PERS_ID") AS
  SELECT PERS.ZISNR PATIENT_NR,
          Onde.Onderzoeknr ONDERZOEK_NR,
          Pers.Geboortedatum GEBOORTE_DATUM,
          Ongr.Code ONDERZOEK_GROEP,
          ONUI.Code UITSLAG_CODE,
          INDI.CODE INDICATIE,
          Indi.Omschrijving INDIOMS,
          Onde.Afgerond AFGEROND,
          ONin.Hoofd_Ind HOOFD_INDICATIE,
          Onin.Volgorde INDICATIE_VOLG,
          ONDE.DECLAREREN,
          ONDE.DATUM_BINNEN DATUM_BINNEN,
          ONDE.Datum_Autorisatie,
          ONDE.ONDE_ID,
          ONUI.ONUI_ID,
          INDI.INDI_ID,
          ONGR.ONGR_ID,
          PERS.PERS_ID
     FROM Kgc_personen PERS,
          Kgc_onderzoeken Onde,
          Kgc_onderzoeksgroepen ONGR,
          Kgc_Onderzoek_Uitslagcodes ONUI,
          Kgc_Onderzoek_Indicaties ONIN,
          Kgc_Indicatie_Teksten INDI
    WHERE     Onde.Ongr_Id = ONGR.ONGR_ID
          AND ONUI.Onui_Id = Onde.Onui_Id
          AND ONDE.ONDE_ID = ONIN.ONDE_ID
          AND INDI.INDI_ID = Onin.Indi_Id
          AND Pers.Pers_Id = Onde.Pers_Id
          AND ONGR.Code IN ('CYTOM', 'CYTOV');

/
QUIT
