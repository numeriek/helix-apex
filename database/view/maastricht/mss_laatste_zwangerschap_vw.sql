CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_LAATSTE_ZWANGERSCHAP_VW" ("ZWAN_ID", "PERS_ID", "NUMMER", "INFO_GESLACHT", "CONSANGUINITEIT", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "RELA_ID", "ZEKERHEID_DATUM_LM", "DATUM_LM", "BEREKENINGSWIJZE", "DATUM_ATERM", "MEDICATIE", "OPMERKINGEN", "VREUGDEBRIEF", "MOLA", "APLA", "EUG", "PARA", "ABORTUSSEN", "ABORTUS", "HOEVEELLING", "PROGENITUUR") AS
  SELECT zwan."ZWAN_ID",zwan."PERS_ID",zwan."NUMMER",zwan."INFO_GESLACHT",zwan."CONSANGUINITEIT",zwan."CREATED_BY",zwan."CREATION_DATE",zwan."LAST_UPDATED_BY",zwan."LAST_UPDATE_DATE",zwan."RELA_ID",zwan."ZEKERHEID_DATUM_LM",zwan."DATUM_LM",zwan."BEREKENINGSWIJZE",zwan."DATUM_ATERM",zwan."MEDICATIE",zwan."OPMERKINGEN",zwan."VREUGDEBRIEF",zwan."MOLA",zwan."APLA",zwan."EUG",zwan."PARA",zwan."ABORTUSSEN",zwan."ABORTUS",zwan."HOEVEELLING",zwan."PROGENITUUR"
   FROM   kgc_zwangerschappen  zwan
   where  zwan.nummer =
             ( select max(zwan2.nummer)
               from   kgc_zwangerschappen zwan2
               where  zwan2.pers_id = zwan.pers_id
             );

/
QUIT
