CREATE OR REPLACE FORCE VIEW "HELIX"."BEHUITS51_52LAB_METI_VW" ("UITS_ID", "RESULT", "DAT_METI_AFGEROND") AS
  SELECT uits.uits_id                             uits_id
  ,      kgc_rapp_00.formatteer(onde.kafd_id
                               ,onde.ongr_id
                               ,NULL
                               ,'UITSLAG'
                               ,'RESULT'
                               ,onde.taal_id
                               ,meti.conclusie
                               )                  result
  ,      meti.datum_afgerond                      dat_meti_afgerond
  FROM   kgc_stoftestgroepen     stgr
  ,      bas_metingen            meti
  ,      kgc_monsters            mons
  ,      kgc_onderzoek_monsters  onmo
  ,      kgc_onderzoeksgroepen   ongr
  ,      kgc_onderzoeken         onde
  ,      kgc_uitslagen           uits
  WHERE  onde.onde_id = uits.onde_id
  AND    ongr.ongr_id = onde.ongr_id
  AND    onmo.onde_id = onde.onde_id
  AND    mons.mons_id = onmo.mons_id
  AND    meti.onmo_id = onmo.onmo_id
  AND    meti.afgerond = 'J'
  AND    meti.conclusie IS NOT NULL
  AND    stgr.stgr_id (+) = meti.stgr_id
  AND    nvl(stgr.code,'X') not in ('FISH-AML', 'FISH-CLL','FISH_1','FISH_2','FISH_3','FISH_4','FISH_5','FISH_6')
  AND    nvl(stgr.code,'X') not like 'FISH-KLR%'
  AND    (   (ongr.code not in ('PM','PV','PR','TM','TV'))
          or (    ongr.code in ('PM','PV','PR','TM','TV')
              and (stgr.stgr_id,uits.brty_id) in
                    ( select ebpt.stgr_id
                      ,      ebpt.brty_id
                      from   epd_brieftype_protocol ebpt
                    )
             )
         )
  ORDER
  BY     meti.datum_afgerond;

/
QUIT
