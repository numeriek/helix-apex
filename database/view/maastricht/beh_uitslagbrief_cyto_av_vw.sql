CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_UITSLAGBRIEF_CYTO_AV_VW" ("BRIEFTYPE", "ONDERZOEKNR", "DATUM_BINNEN", "AFGEROND", "DATUM_UITSLAG", "DATUM_AUTORISATIE", "TEK1_CODE", "UITS_ID", "BRTY_ID", "PERS_ID", "ONDE_ID", "KAFD_ID", "ONGR_ID", "TAAL_ID", "RELA_ID", "RELA_ID_ORIGINEEL", "KAFD_CODE", "ONGR_CODE", "ZISNR", "MEDE_ID_AUTORISATOR", "MEDE_ID_ONDERTEKENAAR2") AS
  SELECT brty.omschrijving brieftype
,      onde.onderzoeknr
,      onde.datum_binnen
,      onde.afgerond
,      uits.datum_uitslag
,      onde.datum_autorisatie
,      mede.code
,      uits.uits_id
,      uits.brty_id
,      onde.pers_id
,      onde.onde_id
,      onde.kafd_id
,      onde.ongr_id
,      onde.taal_id
,      uits.rela_id
,      uits.rela_id rela_id_origineel
,      kafd.code kafd_code
,      ongr.code ongr_code
,      pers.zisnr
,      mede.mede_id
,      mede2.mede_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_brieftypes brty
,      kgc_medewerkers mede
,      kgc_medewerkers mede2
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_uitslagen uits
WHERE onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.pers_id = pers.pers_id
and    onde.onde_id = uits.onde_id
and    uits.mede_id = mede.mede_id (+)
and    uits.mede_id2 = mede2.mede_id (+)
and    uits.brty_id = brty.brty_id (+)
and    uits.tekst is not null
and    brty.code in (
'FISH', 'MLPA', 'SNP', 'VRLBRIEF')
and    kgc_brie_00.uitslag_eerder_geprint( uits.uits_id, uits.brty_id ) = 'N';

/
QUIT
