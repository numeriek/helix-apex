CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_GEM_RES_PER_INDI_VW" ("INDICATIE", "DB_WAARDE", "MONS_ID", "MONS_DATUM_AANMELDING", "CODE_ONGR") AS
  SELECT DISTINCT indi.code indicatie,
                   mwtw.db_waarde db_waarde,
                   mons.mons_id mons_id,
                   mons.datum_aanmelding mons_datum_aanmelding,
                   onde.code_ongr code_ongr
     FROM kgc_mwss_toegestane_waarden mwtw,
          bas_meetwaarde_details mdet,
          kgc_mwst_samenstelling mwss,
          kgc_indicatie_teksten indi,
          kgc_onderzoek_indicaties onin,
          kgc_meetwaardestructuur mwst,
          bas_meetwaarden meet,
          bas_metingen meti,
          mss_onde_pt_cyto_vw onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons
    WHERE     onmo.mons_id = mons.mons_id
          AND onde.onde_id = onmo.onde_id
          AND onde.code_ongr IN ('CYTOM', 'CYTOV')
          AND meti.onmo_id = onmo.onmo_id
          AND meet.meti_id = meti.meti_id
          AND mwst.mwst_id = meet.mwst_id
          AND mwst.code = 'KGCKARY01'                             -- Karyogram
          AND onin.onde_id = onde.onde_id
          AND (   onin.volgorde = 1
               OR (    onin.volgorde IS NULL
                   AND onin.onin_id = (SELECT MIN (onin2.onin_id)
                                         FROM kgc_onderzoek_indicaties onin2
                                        WHERE onin2.onde_id = onde.onde_id)))
          AND indi.indi_id = onin.indi_id
          AND mwss.mwst_id = mwst.mwst_id
          AND mwss.code = 'GEM-KWAL'
          AND mdet.meet_id = meet.meet_id
          AND mdet.volgorde = mwss.volgorde
          AND mwtw.mwss_id = mwss.mwss_id
          AND mwtw.db_waarde = mdet.waarde;

/
QUIT
