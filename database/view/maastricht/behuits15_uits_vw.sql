CREATE OR REPLACE FORCE VIEW "HELIX"."BEHUITS15_UITS_VW" ("IS_KOPIEHOUDER", "PERS_ID", "PERS_AANSPREKEN", "PERS_ZISNR", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_ADRES", "PERS_ZOEKNAAM", "PERS_POSTCODE", "PERS_TELEFOON", "PERS_TELEFAX", "PERS_EMAIL", "PERS_WOONPLAATS", "FOET_ID", "ONDE_ID", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "SPOED", "ONDERZOEKNR", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "REFERENTIE", "INDICATIE", "REDEN", "AFGEROND", "STATUS", "MEDE_ID_CONTROLE", "ONGR_ID", "ONGR_CODE", "ONGR_OMSCHRIJVING", "RELA_ID", "RELA_ID_ONDE", "RELA_CODE", "IS_KLG", "RELA_NAAM", "RELA_TYPE", "RELA_ENOTADRES", "EMAILADRES_OK", "KAFD_ID", "KAFD_CODE", "KAFD_NAAM", "TAAL_ID", "TAAL_CODE", "REEDS_GEPRINT", "DATUM_AUTORISATIE", "UITS_ID", "BRTY_ID", "BRTY_CODE", "KOHO_ID") AS
  SELECT 'N',
          onde.pers_id,
          pers.aanspreken pers_aanspreken,
          pers.zisnr pers_zisnr,
          pers.achternaam pers_achternaam,
          pers.geboortedatum pers_geboortedatum,
          pers.geslacht pers_geslacht,
          kgc_adres_00.persoon (pers.pers_id, 'N') pers_adres,
          pers.zoeknaam pers_zoeknaam,
          pers.postcode pers_postcode,
          pers.telefoon pers_telefoon,
          pers.telefax pers_telefax,
          pers.email pers_email,
          pers.woonplaats,
          onde.foet_id,
          onde.onde_id,
          onde.onderzoekstype,
          onde.onderzoekswijze,
          onde.spoed,
          onde.onderzoeknr onderzoek,
          onde.datum_binnen,
          onde.geplande_einddatum,
          onde.referentie,
          kgc_indi_00.onderzoeksreden (onde.onde_id) indicatie,
          onde.omschrijving reden,
          onde.afgerond,
          onde.status,
          onde.mede_id_controle,
          onde.ongr_id,
          ongr.code ongr_code,
          ongr.omschrijving ongr_omschrijving,
          rela.rela_id,
          onde.rela_id rela_id_onde,
          rela.code rela_code,
          decode(substr(upper(rela.code),1,3),'KLG','J','N') is_klg,
          rela.aanspreken rela_aanspreken,
          rela.relatie_type relatie_type,
          mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                               , decode( rela.rela_id
                                                       , onde.rela_id, 'J'
                                                       , 'N'
                                                       )
                                               ),
          case
            when lower(mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                                            , decode( rela.rela_id
                                                                    , onde.rela_id, 'J'
                                                                    , 'N'
                                                                    )
                                                            )
                      ) like '%@mumc.nl'
            then 'J'
            else 'N'
          end emailadres_ok,
          onde.kafd_id,
          kafd.code kafd_code,
          kafd.naam kafd_naam,
          onde.taal_id,
          taal.code taal_code,
          mss_algemeen.bbrief_reeds_geprint (uits.uits_id,
                                             brty.brty_id,
                                             rela.rela_id)
             reeds_geprint,
          onde.datum_autorisatie datum_autorisatie,
          uits.uits_id,
          brty.brty_id,
          brty.code brty_code,
          TO_NUMBER (NULL) koho_id
     FROM kgc_talen taal,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_relaties rela,
          kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_uitslagen uits,
          kgc_brieftypes brty,
          kgc_rapport_modules ramo
    WHERE     onde.pers_id = pers.pers_id
          AND uits.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onde_id = uits.onde_id
          AND uits.tekst IS NOT NULL
          AND uits.brty_id IS NOT NULL
          AND onde.taal_id = taal.taal_id(+)
          AND brty.brty_id = uits.brty_id
          AND brty.ramo_id = ramo.ramo_id(+)
          AND onde.afgerond = 'J'
   UNION ALL
   SELECT 'J',
          onde.pers_id,
          pers.aanspreken pers_aanspreken,
          pers.zisnr pers_zisnr,
          pers.achternaam pers_achternaam,
          pers.geboortedatum pers_geboortedatum,
          pers.geslacht pers_geslacht,
          kgc_adres_00.persoon (pers.pers_id, 'N') pers_adres,
          pers.zoeknaam pers_zoeknaam,
          pers.postcode pers_postcode,
          pers.telefoon pers_telefoon,
          pers.telefax pers_telefax,
          pers.email pers_email,
          pers.woonplaats,
          onde.foet_id,
          onde.onde_id,
          onde.onderzoekstype,
          onde.onderzoekswijze,
          onde.spoed,
          onde.onderzoeknr onderzoek,
          onde.datum_binnen,
          onde.geplande_einddatum,
          onde.referentie,
          kgc_indi_00.onderzoeksreden (onde.onde_id) indicatie,
          onde.omschrijving reden,
          onde.afgerond,
          onde.status,
          onde.mede_id_controle,
          onde.ongr_id,
          ongr.code ongr_code,
          ongr.omschrijving ongr_omschrijving,
          rela.rela_id,
          onde.rela_id rela_id_onde,
          rela.code rela_code,
          decode(substr(upper(rela.code),1,3),'KLG','J','N') is_klg,
          rela.aanspreken rela_aanspreken,
          rela.relatie_type relatie_type,
          mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                               , decode( rela.rela_id
                                                       , onde.rela_id, 'J'
                                                       , 'N'
                                                       )
                                               ),
          case
            when lower(mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                                            , decode( rela.rela_id
                                                                    , onde.rela_id, 'J'
                                                                    , 'N'
                                                                    )
                                                            )
                      ) like '%@mumc.nl'
            then 'J'
            else 'N'
          end emailadres_ok,
          onde.kafd_id,
          kafd.code kafd_code,
          kafd.naam kafd_naam,
          onde.taal_id,
          taal.code taal_code,
          mss_algemeen.bbrief_reeds_geprint (uits.uits_id,
                                             brty.brty_id,
                                             rela.rela_id)
             reeds_geprint,
          onde.datum_autorisatie datum_autorisatie,
          uits.uits_id,
          brty.brty_id,
          brty.code brty_code,
          koho.koho_id koho_id
     FROM kgc_kopiehouders koho,
          kgc_talen taal,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_relaties rela,
          kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_uitslagen uits,
          kgc_brieftypes brty,
          kgc_rapport_modules ramo
    WHERE     onde.pers_id = pers.pers_id
          AND koho.uits_id = uits.uits_id
          AND rela.rela_id = koho.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onde_id = uits.onde_id
          AND uits.tekst IS NOT NULL
          AND uits.brty_id IS NOT NULL
          AND onde.taal_id = taal.taal_id(+)
          AND brty.brty_id = uits.brty_id
          AND brty.ramo_id = ramo.ramo_id(+)
          AND onde.afgerond = 'J'
          AND uits.last_update_date >= TO_DATE ('01052015', 'ddmmyyyy')
   UNION ALL
   SELECT 'N',
          onde.pers_id,
          pers.aanspreken pers_aanspreken,
          pers.zisnr pers_zisnr,
          pers.achternaam pers_achternaam,
          pers.geboortedatum pers_geboortedatum,
          pers.geslacht pers_geslacht,
          kgc_adres_00.persoon (pers.pers_id, 'N') pers_adres,
          pers.zoeknaam pers_zoeknaam,
          pers.postcode pers_postcode,
          pers.telefoon pers_telefoon,
          pers.telefax pers_telefax,
          pers.email pers_email,
          pers.woonplaats,
          onde.foet_id,
          onde.onde_id,
          onde.onderzoekstype,
          onde.onderzoekswijze,
          onde.spoed,
          onde.onderzoeknr onderzoek,
          onde.datum_binnen,
          onde.geplande_einddatum,
          onde.referentie,
          kgc_indi_00.onderzoeksreden (onde.onde_id) indicatie,
          onde.omschrijving reden,
          onde.afgerond,
          onde.status,
          onde.mede_id_controle,
          onde.ongr_id,
          ongr.code ongr_code,
          ongr.omschrijving ongr_omschrijving,
          rela.rela_id,
          onde.rela_id rela_id_onde,
          rela.code rela_code,
          decode(substr(upper(rela.code),1,3),'KLG','J','N') is_klg,
          rela.aanspreken rela_aanspreken,
          rela.relatie_type relatie_type,
          mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                               , decode( rela.rela_id
                                                       , onde.rela_id, 'J'
                                                       , 'N'
                                                       )
                                               ),
          case
            when lower(mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                                            , decode( rela.rela_id
                                                                    , onde.rela_id, 'J'
                                                                    , 'N'
                                                                    )
                                                            )
                      ) like '%@mumc.nl'
            then 'J'
            else 'N'
          end emailadres_ok,
          onde.kafd_id,
          kafd.code kafd_code,
          kafd.naam kafd_naam,
          onde.taal_id,
          taal.code taal_code,
          mss_algemeen.bbrief_reeds_geprint (uits.uits_id,
                                             brty.brty_id,
                                             rela.rela_id)
             reeds_geprint,
          uits.datum_mede_id4 datum_autorisatie,
          uits.uits_id,
          brty.brty_id,
          brty.code brty_code,
          TO_NUMBER (NULL) koho_id
     FROM kgc_talen taal,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_relaties rela,
          kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_uitslagen uits,
          kgc_brieftypes brty,
          kgc_rapport_modules ramo
    WHERE     onde.pers_id = pers.pers_id
          AND uits.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onde_id = uits.onde_id
          AND uits.tekst IS NOT NULL
          AND uits.brty_id IS NOT NULL
          AND onde.taal_id = taal.taal_id(+)
          AND brty.brty_id = uits.brty_id
          AND brty.ramo_id = ramo.ramo_id(+)
          AND NVL (onde.afgerond, 'N') = 'N'
          AND NVL (ramo.code, 'xxx') = 'UITSLAG'
          AND uits.mede_id4 IS NOT NULL
   UNION ALL
   SELECT 'J',
          onde.pers_id,
          pers.aanspreken pers_aanspreken,
          pers.zisnr pers_zisnr,
          pers.achternaam pers_achternaam,
          pers.geboortedatum pers_geboortedatum,
          pers.geslacht pers_geslacht,
          kgc_adres_00.persoon (pers.pers_id, 'N') pers_adres,
          pers.zoeknaam pers_zoeknaam,
          pers.postcode pers_postcode,
          pers.telefoon pers_telefoon,
          pers.telefax pers_telefax,
          pers.email pers_email,
          pers.woonplaats,
          onde.foet_id,
          onde.onde_id,
          onde.onderzoekstype,
          onde.onderzoekswijze,
          onde.spoed,
          onde.onderzoeknr onderzoek,
          onde.datum_binnen,
          onde.geplande_einddatum,
          onde.referentie,
          kgc_indi_00.onderzoeksreden (onde.onde_id) indicatie,
          onde.omschrijving reden,
          onde.afgerond,
          onde.status,
          onde.mede_id_controle,
          onde.ongr_id,
          ongr.code ongr_code,
          ongr.omschrijving ongr_omschrijving,
          rela.rela_id,
          onde.rela_id rela_id_onde,
          rela.code rela_code,
          decode(substr(upper(rela.code),1,3),'KLG','J','N') is_klg,
          rela.aanspreken rela_aanspreken,
          rela.relatie_type relatie_type,
          mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                               , decode( rela.rela_id
                                                       , onde.rela_id, 'J'
                                                       , 'N'
                                                       )
                                               ),
          case
            when lower(mss_brie_enotificatie.bep_emailadres ( rela.rela_id
                                                            , decode( rela.rela_id
                                                                    , onde.rela_id, 'J'
                                                                    , 'N'
                                                                    )
                                                            )
                      ) like '%@mumc.nl'
            then 'J'
            else 'N'
          end emailadres_ok,
          onde.kafd_id,
          kafd.code kafd_code,
          kafd.naam kafd_naam,
          onde.taal_id,
          taal.code taal_code,
          mss_algemeen.bbrief_reeds_geprint (uits.uits_id,
                                             brty.brty_id,
                                             rela.rela_id)
             reeds_geprint,
          uits.datum_mede_id4 datum_autorisatie,
          uits.uits_id,
          brty.brty_id,
          brty.code brty_code,
          koho.koho_id koho_id
     FROM kgc_kopiehouders koho,
          kgc_talen taal,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_relaties rela,
          kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_uitslagen uits,
          kgc_brieftypes brty,
          kgc_rapport_modules ramo
    WHERE     onde.pers_id = pers.pers_id
          AND koho.uits_id = uits.uits_id
          AND rela.rela_id = koho.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onde_id = uits.onde_id
          AND uits.tekst IS NOT NULL
          AND uits.brty_id IS NOT NULL
          AND onde.taal_id = taal.taal_id(+)
          AND brty.brty_id = uits.brty_id
          AND brty.ramo_id = ramo.ramo_id(+)
          AND NVL (onde.afgerond, 'N') = 'N'
          AND NVL (ramo.code, 'xxx') = 'UITSLAG'
          AND uits.mede_id4 IS NOT NULL
          AND uits.last_update_date >= TO_DATE ('01052015', 'ddmmyyyy');

/
QUIT
