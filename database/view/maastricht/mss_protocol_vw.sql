CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_PROTOCOL_VW" ("METI_ID", "ONDE_ID", "ONMO_ID", "STGR_ID", "PROTOCOL", "SPOED", "AFGEROND", "DATUM_AANMELDING", "DATUM_AFGEROND", "DATUM_GEPLAND", "DECLAREREN", "FRAC_ID") AS
  SELECT   meti.meti_id             meti_id
  ,        meti.onde_id             onde_id
  ,        meti.onmo_id             onmo_id
  ,        stgr.stgr_id             stgr_id
  ,        stgr.code                protocol
  ,        meti.prioriteit          spoed
  ,        meti.afgerond            afgerond
  ,        meti.datum_aanmelding    datum_aanmelding
  ,        meti.datum_afgerond      datum_afgerond
  ,        meti.geplande_einddatum  datum_gepland
  ,        meti.declareren          declareren
  ,        meti.frac_id             frac_id
  FROM     kgc_stoftestgroepen      stgr
  ,        bas_metingen             meti
  WHERE    stgr.stgr_id = meti.stgr_id;

/
QUIT
