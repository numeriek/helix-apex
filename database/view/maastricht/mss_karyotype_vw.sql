CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_KARYOTYPE_VW" ("MONS_ID", "MWST_ID", "MEET_ID", "KARYOTYPE") AS
  SELECT onmo.mons_id
  ,      mwst.mwst_id
  ,      meet.meet_id
  ,      meet.meetwaarde
  FROM   kgc_meetwaardestructuur  mwst
  ,      bas_meetwaarden          meet
  ,      bas_metingen             meti
  ,      kgc_onderzoek_monsters   onmo
  WHERE  meti.onmo_id = onmo.onmo_id
  AND    meet.meti_id = meti.meti_id
  AND    mwst.mwst_id = meet.mwst_id
  AND    mwst.code = 'KGCKARY01' -- Karyogram
;

/
QUIT
