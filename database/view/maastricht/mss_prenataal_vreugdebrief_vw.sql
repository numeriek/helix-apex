CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_PRENATAAL_VREUGDEBRIEF_VW" ("KAFD_CODE", "ONGR_CODE", "VREUGDE_VOLGENS_REGELS", "VREUGDE", "REFERENTIE", "GEADRESSEERDE", "VERRICHTING", "INFO_GESLACHT", "METINGEN", "ONDERTEKENAAR", "EERDER_GEPRINT", "KAFD_ID", "ONGR_ID", "PERS_ID", "ZWAN_ID") AS
  SELECT   DISTINCT
            zwon.kafd_code,
            zwon.ongr_code,
            kgc_vreugdebrief_00.vreugde (zwon.zwan_id) vreugde_volgens_regels,
            DECODE (zwon.vreugdebrief,
                    'J', 'J',
                    kgc_vreugdebrief_00.vreugde (zwon.zwan_id))
               vreugde,
            kgc_vreugdebrief_00.referentie (zwon.zwan_id) referentie,
            kgc_adres_00.persoon (zwon.pers_id, 'J') geadresseerde,
            DECODE (kgc_vreugdebrief_00.verrichting (zwon.zwan_id),
                    'een test op vlok', 'een test op vlokkenmateriaal',
                    kgc_vreugdebrief_00.verrichting (zwon.zwan_id))
               verrichting,
            mss_vreugdebrief_00.info_geslacht (zwon.kafd_id, zwon.zwan_id)
               info_geslacht,
            kgc_vreugdebrief_00.metingen (zwon.zwan_id) metingen,
            kgc_vreugdebrief_00.ondertekenaar (zwon.zwan_id) ondertekenaar,
            kgc_brie_00.vreugdebrief_reeds_geprint (zwon.zwan_id)
               eerder_geprint,
            zwon.kafd_id,
            zwon.ongr_id,
            zwon.pers_id,
            zwon.zwan_id
     FROM   kgc_prenataal_zwan_onde_vw zwon
    WHERE   datum_binnen >= ADD_MONTHS (SYSDATE, -3)
            AND NVL (zwon.vreugdebrief, 'S') <> 'N';

/
QUIT
