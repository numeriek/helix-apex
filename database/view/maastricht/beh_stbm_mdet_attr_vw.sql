CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_STBM_MDET_ATTR_VW" ("MEET_MEET_ID", "GEN", "FRAGMENT", "FRAC_FRACTIENUMMER", "TECHNIEK", "MATERIAAL", "MUTATIE", "PATHOGENICITEIT", "ZYGOTIE", "REPEAT", "REPEATLENGTE", "RESULTAAT", "ONDE_ONDERZOEKSTYPE", "INDICATIE", "ONDE_DATUM_AUTORISATIE", "ONDE_ONDERZOEKNR", "ONDE_ONDERZOEKSWIJZE", "ONDE_AFGEROND", "ONDE_PERS_ID", "DNALAB", "FOETUS_ID", "MONS_PERS_ID", "PERS_ID_MOEDER", "EFFECT") AS
  select meet.meet_id                                                                                            meet_meet_id
  ,      case
           when
             mwst.code = 'MUTATIE'
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'GEN'
                                           , 1
                                           )
           when
             mwst.code = 'NGS_FRAG'
           then
             case
               when
                 instr(stof.omschrijving,'_',1,2) = 0
               then
                 substr(stof.omschrijving,instr(stof.omschrijving,'_',1,1)+1,length(stof.omschrijving))
               else
                 substr(stof.omschrijving,instr(stof.omschrijving,'_',1,1)+1,
                   instr(stof.omschrijving,'_',1,2)-instr(stof.omschrijving,'_',1,1)-1)
             end
           when
             (   (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
              or (    upper(stof.omschrijving) like 'AG_mtDNA%'
                  and mwst.code = 'AG_STND'
                 )
              or (mwst.code = 'RLT_DEPL2')
             )
           then
             'mt-DNA'
           else
             genv.waarde
         end                                                                                                     gen
  ,      case
           when
             mwst.code IN ('MUTATIE', 'NGS_FRAG')
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'FRAGMENT'
                                           , 1
                                           )
           else
             case
               when
                 instr(stof.omschrijving,'_',1,2) = 0
               then
                 NULL
               when
                 instr(stof.omschrijving,'_',1,3) = 0
               then
                 case
                   when
                     (   (stof.omschrijving LIKE '%MM_m.%')
                      or (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                     )
                   then
                     substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,100)
                   else
                     'exon '||substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,100)
                 end
               else
                 case
                   when
                     (   (stof.omschrijving LIKE '%MM_m.%')
                      or (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                     )
                   then
                     substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,
                       instr(stof.omschrijving,'_',1,3) - instr(stof.omschrijving,'_',1,2) - 1)
                   else
                     'exon '||substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,
                       instr(stof.omschrijving,'_',1,3) - instr(stof.omschrijving,'_',1,2) - 1)
                   end
             end
         end                                                                                                     fragment
  ,      frac.fractienummer                                                                                      frac_fractienummer
  ,      case
           when
             mwst.code = 'MUTATIE'
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'TECHNIEK'
                                           , 1
                                           )
           else
             tech.code
         end                                                                                                     techniek
  ,      mate.code                                                                                               materiaal
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             'N' -- Normaal
           else
             case
               when
                 tech.code = 'RFLP'
               then
                 substr(stof.code,instr(stof.code,'_',1,2)+1,instr(stof.code,'_',1,3)-instr(stof.code,'_',1,2)-1)
               when
                 mwst.code = 'RLT_DEPL2'
               then
                 'Depletie ' ||mss_algemeen.bep_mdet_waarde(meet.meet_id,'Depletie?')
               when
                 (   (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                  or (    upper(stof.omschrijving) like 'AG_mtDNA%'
                      and mwst.code = 'AG_STND'
                     )
                 )
               then
                 'Deletie ' ||mss_algemeen.bep_mdet_waarde(meet.meet_id,'Grootte deletie')
               else
                 beh_stamboom_00.waardebepaling( meet.meet_id
                                               , mwst.code
                                               , 'MUTATIE'
                                               , 1
                                               )
             end
         end                                                                                                     mutatie
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking
           else
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'PATHOGENICITEIT'
                                           , 1
                                           )
         end                                                                                                     pathogeniciteit
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking
           else
             case
               when
                 upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%'
               then
                 mss_algemeen.bep_mdet_waarde(meet.meet_id,'Ratio deletie')||' ratio del.'
               when
                 tech.code = 'RFLP'
               then
                 mss_algemeen.bep_mdet_waarde(meet.meet_id,'Percentage')||'%'
               else
                 beh_stamboom_00.waardebepaling( meet.meet_id
                                               , mwst.code
                                               , 'ZYGOTIE'
                                               , 1
                                               )
             end
         end                                                                                                     zygotie
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'REPEAT'
                                       , 1
                                       )                                                                         repeat
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'REPEATLENGTE'
                                       , 1
                                       )                                                                         repeatlengte
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'RESULTAAT'
                                       , 1
                                       )                                                                         resultaat
  ,      onde.onderzoekstype                                                                                     onde_onderzoekstype
  ,      indi.code                                                                                               indicatie
  ,      onde.datum_autorisatie                                                                                  onde_datum_autorisatie
  ,      onde.onderzoeknr                                                                                        onde_onderzoeknr
  ,      onde.onderzoekswijze                                                                                    onde_onderzoekswijze
  ,      onde.afgerond                                                                                           onde_afgerond
  ,      mons.pers_id                                                                                            onde_pers_id -- Was onde.pers_id. Naamgeving is nu krom.
  ,      'HELIX'                                                                                                 dnalab       -- PASS software werkt met naam onde_pers_id terwijl mons.pers_id leading is
  ,      mons.foet_id                                                                                            foetus_id
  ,      mons.pers_id                                                                                            mons_pers_id
  ,      beh_stamboom_00.pers_id_moeder(mons.pers_id)                                                            pers_id_moeder
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking (hoeft niet te checken naar stamboom omdat waarde = null)
           else
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'EFFECT'
                                           , 1
                                           )
         end                                                                                                     effect
  from   kgc_materialen                                        mate
  ,      kgc_meetwaardestructuur                               mwst
  ,      kgc_technieken                                        tech
  ,      kgc_stoftesten                                        stof
  ,      ( select atwa.id
           ,      atwa.waarde
           from   kgc_attributen        attr
           ,      kgc_attribuut_waarden atwa
           where  attr.attr_id = atwa.attr_id
           and    attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
           and    attr.code = 'GEN'
         )                                                     genv
  ,      kgc_indicatie_teksten                                 indi
  ,      kgc_onderzoek_indicaties                              onin
  ,      kgc_onderzoeken                                       onde
  ,      bas_meetwaarden                                       meet
  ,      bas_metingen                                          meti
  ,      bas_fracties                                          frac
  ,      kgc_monsters                                          mons
  where  frac.mons_id = mons.mons_id
  and    meti.frac_id = frac.frac_id
  and    meet.meti_id = meti.meti_id
  and    exists
           ( select NULL
             from   bas_mdet_ok             mdok
             ,      bas_meetwaarde_details  mdet
             where  mdet.meet_id = meet.meet_id
             and    mdok.mdet_id = mdet.mdet_id
             and    upper(mdok.naar_stamboom) = 'J'
           )
  and    (   (    mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
              and (   not exists
                        ( select NULL
                          from   beh_waardebep_stamboom  stam
                          ,      bas_mdet_ok             mdok
                          ,      bas_meetwaarde_details  mdet
                          where  mdet.meet_id = meet.meet_id
                          and    mdok.mdet_id = mdet.mdet_id
                          and    upper(mdok.naar_stamboom) = 'J'
                          and    stam.mwst_code = mwst.code
                          and    upper(stam.kolom_in_view) in (upper('Mutatie'),upper('Pathogeniciteit'),upper('Zygotie'),upper('Effect'))
                          and    mdet.prompt in (stam.samengesteld_prompt1,stam.samengesteld_prompt2,stam.samengesteld_prompt3)
                        )
                   or exists
                        ( select NULL
                          from   beh_waardebep_stamboom  stam
                          ,      bas_mdet_ok             mdok
                          ,      bas_meetwaarde_details  mdet
                          where  mdet.meet_id = meet.meet_id
                          and    mdok.mdet_id = mdet.mdet_id
                          and    upper(mdok.naar_stamboom) = 'J'
                          and    stam.mwst_code = mwst.code
                          and    upper(stam.kolom_in_view) in (upper('Mutatie'),upper('Pathogeniciteit'),upper('Zygotie'),upper('Effect'))
                          and    mdet.prompt = stam.samengesteld_prompt1
                        )
                  )
             )
          or ( mwst.code not in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             )
         )
  and    onde.onde_id = meti.onde_id
  and    onin.onde_id = onde.onde_id
  and    indi.indi_id = onin.indi_id
  and    genv.id(+) = indi.indi_id
  and    stof.stof_id = meet.stof_id
  and    tech.tech_id(+) = stof.tech_id
  and    mwst.mwst_id = meet.mwst_id
  and    mate.mate_id = mons.mate_id
  union
  select meet.meet_id                                                                                            meet_meet_id
  ,      case
           when
             mwst.code = 'MUTATIE'
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'GEN'
                                           , 2
                                           )
           when
             mwst.code = 'NGS_FRAG'
           then
             case
               when
                 instr(stof.omschrijving,'_',1,2) = 0
               then
                 substr(stof.omschrijving,instr(stof.omschrijving,'_',1,1)+1,length(stof.omschrijving))
               else
                 substr(stof.omschrijving,instr(stof.omschrijving,'_',1,1)+1,
                   instr(stof.omschrijving,'_',1,2)-instr(stof.omschrijving,'_',1,1)-1)
             end
           when
             (   (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
              or (    upper(stof.omschrijving) like 'AG_mtDNA%'
                  and mwst.code = 'AG_STND'
                 )
              or (mwst.code = 'RLT_DEPL2')
             )
           then
             'mt-DNA'
           else
             genv.waarde
         end                                                                                                     gen
  ,      case
           when
             mwst.code in ('MUTATIE', 'NGS_FRAG')
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'FRAGMENT'
                                           , 2
                                           )
           else
             case
               when
                 instr(stof.omschrijving,'_',1,2) = 0
               then
                 NULL
               when
                 instr(stof.omschrijving,'_',1,3) = 0
               then
                 case
                   when
                     (   (stof.omschrijving LIKE '%MM_m.%')
                      or (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                     )
                   then
                     substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,100)
                   else
                     'exon '||substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,100)
                 end
               else
                 case
                   when
                     (   (stof.omschrijving LIKE '%MM_m.%')
                      or (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                     )
                   then
                     substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,
                       instr(stof.omschrijving,'_',1,3) - instr(stof.omschrijving,'_',1,2) - 1)
                   else
                     'exon '||substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,
                       instr(stof.omschrijving,'_',1,3) - instr(stof.omschrijving,'_',1,2) - 1)
                   end
             end
         end                                                                                                     fragment
  ,      frac.fractienummer                                                                                      frac_fractienummer
  ,      case
           when
             mwst.code = 'MUTATIE'
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'TECHNIEK'
                                           , 2
                                           )
           else
             tech.code
         end                                                                                                     techniek
  ,      mate.code                                                                                               materiaal
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             'N' -- Normaal
           else
             case
               when
                 tech.code = 'RFLP'
               then
                 substr(stof.code,instr(stof.code,'_',1,2)+1,instr(stof.code,'_',1,3)-instr(stof.code,'_',1,2)-1)
               when
                 mwst.code = 'RLT_DEPL2'
               then
                 'Depletie ' ||mss_algemeen.bep_mdet_waarde(meet.meet_id,'Depletie?')
               when
                 (   (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                  or (    upper(stof.omschrijving) like 'AG_mtDNA%'
                      and mwst.code = 'AG_STND'
                     )
                 )
               then
                 'Deletie ' ||mss_algemeen.bep_mdet_waarde(meet.meet_id,'Grootte deletie')
               else
                 beh_stamboom_00.waardebepaling( meet.meet_id
                                               , mwst.code
                                               , 'MUTATIE'
                                               , 2
                                               )
             end
         end                                                                                                     mutatie
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking
           else
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'PATHOGENICITEIT'
                                           , 2
                                           )
         end                                                                                                     pathogeniciteit
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking
           else
             case
               when
                 upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%'
               then
                 mss_algemeen.bep_mdet_waarde(meet.meet_id,'Ratio deletie')||' ratio del.'
               when
                 tech.code = 'RFLP'
               then
                 mss_algemeen.bep_mdet_waarde(meet.meet_id,'Percentage')||'%'
               else
                 beh_stamboom_00.waardebepaling( meet.meet_id
                                               , mwst.code
                                               , 'ZYGOTIE'
                                               , 2
                                               )
             end
         end                                                                                                     zygotie
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'REPEAT'
                                       , 2
                                       )                                                                         repeat
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'REPEATLENGTE'
                                       , 2
                                       )                                                                         repeatlengte
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'RESULTAAT'
                                       , 2
                                       )                                                                         resultaat
  ,      onde.onderzoekstype                                                                                     onde_onderzoekstype
  ,      indi.code                                                                                               indicatie
  ,      onde.datum_autorisatie                                                                                  onde_datum_autorisatie
  ,      onde.onderzoeknr                                                                                        onde_onderzoeknr
  ,      onde.onderzoekswijze                                                                                    onde_onderzoekswijze
  ,      onde.afgerond                                                                                           onde_afgerond
  ,      mons.pers_id                                                                                            onde_pers_id -- Was onde.pers_id. Naamgeving is nu krom.
  ,      'HELIX'                                                                                                 dnalab       -- PASS software werkt met naam onde_pers_id terwijl mons.pers_id leading is
  ,      mons.foet_id                                                                                            foetus_id
  ,      mons.pers_id                                                                                            mons_pers_id
  ,      beh_stamboom_00.pers_id_moeder(mons.pers_id)                                                            pers_id_moeder
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking (hoeft niet te checken naar stamboom omdat waarde = null)
           else
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'EFFECT'
                                           , 2
                                           )
         end                                                                                                     effect
  from   kgc_materialen                                        mate
  ,      kgc_meetwaardestructuur                               mwst
  ,      kgc_technieken                                        tech
  ,      kgc_stoftesten                                        stof
  ,      ( select atwa.id
           ,      atwa.waarde
           from   kgc_attributen        attr
           ,      kgc_attribuut_waarden atwa
           where  attr.attr_id = atwa.attr_id
           and    attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
           and    attr.code = 'GEN'
         )                                                     genv
  ,      kgc_indicatie_teksten                                 indi
  ,      kgc_onderzoek_indicaties                              onin
  ,      kgc_onderzoeken                                       onde
  ,      bas_meetwaarden                                       meet
  ,      bas_metingen                                          meti
  ,      bas_fracties                                          frac
  ,      kgc_monsters                                          mons
  where  frac.mons_id = mons.mons_id
  and    meti.frac_id = frac.frac_id
  and    meet.meti_id = meti.meti_id
  and    exists
           ( select NULL
             from   beh_waardebep_stamboom  stam
             ,      bas_mdet_ok             mdok
             ,      bas_meetwaarde_details  mdet
             where  mdet.meet_id = meet.meet_id
             and    mdok.mdet_id = mdet.mdet_id
             and    upper(mdok.naar_stamboom) = 'J'
             and    stam.mwst_code = mwst.code
             and    upper(stam.kolom_in_view) in (upper('Mutatie'),upper('Pathogeniciteit'),upper('Zygotie'),upper('Effect'))
             and    mdet.prompt = stam.samengesteld_prompt2
           )
  and    onde.onde_id = meti.onde_id
  and    onin.onde_id = onde.onde_id
  and    indi.indi_id = onin.indi_id
  and    genv.id(+) = indi.indi_id
  and    stof.stof_id = meet.stof_id
  and    tech.tech_id(+) = stof.tech_id
  and    mwst.mwst_id = meet.mwst_id
  and    mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
  and    mate.mate_id = mons.mate_id
  union
  select meet.meet_id                                                                                            meet_meet_id
  ,      case
           when
             mwst.code = 'MUTATIE'
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'GEN'
                                           , 3
                                           )
           when
             mwst.code = 'NGS_FRAG'
           then
             case
               when
                 instr(stof.omschrijving,'_',1,2) = 0
               then
                 substr(stof.omschrijving,instr(stof.omschrijving,'_',1,1)+1,length(stof.omschrijving))
               else
                 substr(stof.omschrijving,instr(stof.omschrijving,'_',1,1)+1,
                   instr(stof.omschrijving,'_',1,2)-instr(stof.omschrijving,'_',1,1)-1)
             end
           when
             (   (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
              or (    upper(stof.omschrijving) like 'AG_mtDNA%'
                  and mwst.code = 'AG_STND'
                 )
              or (mwst.code = 'RLT_DEPL2')
             )
           then
             'mt-DNA'
           else
             genv.waarde
         end                                                                                                     gen
  ,      case
           when
             mwst.code in ('MUTATIE', 'NGS_FRAG')
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'FRAGMENT'
                                           , 3
                                           )
           else
             case
               when
                 instr(stof.omschrijving,'_',1,2) = 0
               then
                 NULL
               when
                 instr(stof.omschrijving,'_',1,3) = 0
               then
                 case
                   when
                     (   (stof.omschrijving LIKE '%MM_m.%')
                      or (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                     )
                   then
                     substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,100)
                   else
                     'exon '||substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,100)
                 end
               else
                 case
                   when
                     (   (stof.omschrijving LIKE '%MM_m.%')
                      or (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                     )
                   then
                     substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,
                       instr(stof.omschrijving,'_',1,3) - instr(stof.omschrijving,'_',1,2) - 1)
                   else
                     'exon '||substr(stof.omschrijving,instr(stof.omschrijving,'_',1,2) + 1,
                       instr(stof.omschrijving,'_',1,3) - instr(stof.omschrijving,'_',1,2) - 1)
                   end
             end
         end                                                                                                     fragment
  ,      frac.fractienummer                                                                                      frac_fractienummer
  ,      case
           when
             mwst.code = 'MUTATIE'
           then
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'TECHNIEK'
                                           , 3
                                           )
           else
             tech.code
         end                                                                                                     techniek
  ,      mate.code                                                                                               materiaal
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             'N' -- Normaal
           else
             case
               when
                 tech.code = 'RFLP'
               then
                 substr(stof.code,instr(stof.code,'_',1,2)+1,instr(stof.code,'_',1,3)-instr(stof.code,'_',1,2)-1)
               when
                 mwst.code = 'RLT_DEPL2'
               then
                 'Depletie ' ||mss_algemeen.bep_mdet_waarde(meet.meet_id,'Depletie?')
               when
                 (   (upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%')
                  or (    upper(stof.omschrijving) like 'AG_mtDNA%'
                      and mwst.code = 'AG_STND'
                     )
                 )
               then
                 'Deletie ' ||mss_algemeen.bep_mdet_waarde(meet.meet_id,'Grootte deletie')
               else
                 beh_stamboom_00.waardebepaling( meet.meet_id
                                               , mwst.code
                                               , 'MUTATIE'
                                               , 3
                                               )
             end
         end                                                                                                     mutatie
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking
           else
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'PATHOGENICITEIT'
                                           , 3
                                           )
         end                                                                                                     pathogeniciteit
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking
           else
             case
               when
                 upper(stof.omschrijving) like 'SB_MT-DNA-SOUTHERN%'
               then
                 mss_algemeen.bep_mdet_waarde(meet.meet_id,'Ratio deletie')||' ratio del.'
               when
                 tech.code = 'RFLP'
               then
                 mss_algemeen.bep_mdet_waarde(meet.meet_id,'Percentage')||'%'
               else
                 beh_stamboom_00.waardebepaling( meet.meet_id
                                               , mwst.code
                                               , 'ZYGOTIE'
                                               , 3
                                               )
             end
         end                                                                                                     zygotie
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'REPEAT'
                                       , 3
                                       )                                                                         repeat
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'REPEATLENGTE'
                                       , 3
                                       )                                                                         repeatlengte
  ,      beh_stamboom_00.waardebepaling( meet.meet_id
                                       , mwst.code
                                       , 'RESULTAAT'
                                       , 3
                                       )                                                                         resultaat
  ,      onde.onderzoekstype                                                                                     onde_onderzoekstype
  ,      indi.code                                                                                               indicatie
  ,      onde.datum_autorisatie                                                                                  onde_datum_autorisatie
  ,      onde.onderzoeknr                                                                                        onde_onderzoeknr
  ,      onde.onderzoekswijze                                                                                    onde_onderzoekswijze
  ,      onde.afgerond                                                                                           onde_afgerond
  ,      mons.pers_id                                                                                            onde_pers_id -- Was onde.pers_id. Naamgeving is nu krom.
  ,      'HELIX'                                                                                                 dnalab       -- PASS software werkt met naam onde_pers_id terwijl mons.pers_id leading is
  ,      mons.foet_id                                                                                            foetus_id
  ,      mons.pers_id                                                                                            mons_pers_id
  ,      beh_stamboom_00.pers_id_moeder(mons.pers_id)                                                            pers_id_moeder
  ,      case
           when
                 mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
             and nvl(mss_algemeen.bep_mdet_waarde(meet.meet_id,'Afwijking?'),'N') = 'N'
           then
             null -- Er is geen afwijking (hoeft niet te checken naar stamboom omdat waarde = null)
           else
             beh_stamboom_00.waardebepaling( meet.meet_id
                                           , mwst.code
                                           , 'EFFECT'
                                           , 3
                                           )
         end                                                                                                     effect
  from   kgc_materialen                                        mate
  ,      kgc_meetwaardestructuur                               mwst
  ,      kgc_technieken                                        tech
  ,      kgc_stoftesten                                        stof
  ,      ( select atwa.id
           ,      atwa.waarde
           from   kgc_attributen        attr
           ,      kgc_attribuut_waarden atwa
           where  attr.attr_id = atwa.attr_id
           and    attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
           and    attr.code = 'GEN'
         )                                                     genv
  ,      kgc_indicatie_teksten                                 indi
  ,      kgc_onderzoek_indicaties                              onin
  ,      kgc_onderzoeken                                       onde
  ,      bas_meetwaarden                                       meet
  ,      bas_metingen                                          meti
  ,      bas_fracties                                          frac
  ,      kgc_monsters                                          mons
  where  frac.mons_id = mons.mons_id
  and    meti.frac_id = frac.frac_id
  and    meet.meti_id = meti.meti_id
  and    exists
           ( select NULL
             from   beh_waardebep_stamboom  stam
             ,      bas_mdet_ok             mdok
             ,      bas_meetwaarde_details  mdet
             where  mdet.meet_id = meet.meet_id
             and    mdok.mdet_id = mdet.mdet_id
             and    upper(mdok.naar_stamboom) = 'J'
             and    stam.mwst_code = mwst.code
             and    upper(stam.kolom_in_view) in (upper('Mutatie'),upper('Pathogeniciteit'),upper('Zygotie'),upper('Effect'))
             and    mdet.prompt = stam.samengesteld_prompt3
           )
  and    onde.onde_id = meti.onde_id
  and    onin.onde_id = onde.onde_id
  and    indi.indi_id = onin.indi_id
  and    genv.id(+) = indi.indi_id
  and    stof.stof_id = meet.stof_id
  and    tech.tech_id(+) = stof.tech_id
  and    mwst.mwst_id = meet.mwst_id
  and    mwst.code in ('NGS_FRAG','SEQ_STND','SEQ_STND_O')
  and    mate.mate_id = mons.mate_id
  order
  by     gen
  ,      frac_fractienummer
  ,      techniek
  ,      meet_meet_id;

/
QUIT
