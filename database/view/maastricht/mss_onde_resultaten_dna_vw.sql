CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_ONDE_RESULTATEN_DNA_VW" ("ONDE_ID", "LAST_UPDATE_DATE", "RESULTAAT") AS
  SELECT   meti.onde_id, meti.last_update_date, meti.conclusie resultaat
     FROM   kgc_onderzoeken onde,
            kgc_monsters mons,
            bas_fracties frac,
            bas_metingen meti
    WHERE       meti.onde_id = onde.onde_id
            AND meti.frac_id = frac.frac_id
            AND frac.mons_id = mons.mons_id
            AND mons.pers_id = onde.pers_id -- niet enkel adviesvrager, maar alle betrokkenen
            AND meti.conclusie IS NOT NULL
            AND NVL (frac.status, 'O') <> 'M';

/
QUIT
