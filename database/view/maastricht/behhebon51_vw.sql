CREATE OR REPLACE FORCE VIEW "HELIX"."BEHHEBON51_VW" ("HEBO_ID", "PERS_ID", "ONDE_ID", "KAFD_ID", "ONGR_ID", "NAAM_ADRES", "LBL_PBD", "PBD", "AANHEF", "UN_PWD") AS
  select hebo.hebo_id
  ,      hebo.pers_id
  ,      hebo.onde_id
  ,      onde.kafd_id
  ,      onde.ongr_id
  ,      kgc_rapp_00.formatteer ( onde.kafd_id
                                , onde.ongr_id
                                , NULL
                                , 'HEBON'
                                , 'NAAM_ADRES'
                                , onde.taal_id
                                , kgc_adres_00.persoon (hebo.pers_id, 'J')
                                )                                naam_adres
  ,      kgc_rapp_00.formatteer ( onde.kafd_id
                                , onde.ongr_id
                                , NULL
                                , 'HEBON'
                                , 'LBL_PBD'
                                , onde.taal_id
                                )                                lbl_pbd
  ,      mss_rapp_00.formatteer ( onde.kafd_id
                                , onde.ongr_id
                                , NULL
                                , 'HEBON'
                                , 'PBD'
                                , onde.taal_id
                                , 'Hebon studie'
                                , to_char(sysdate,'dd-mm-yyyy')
                                )                                pbd
  ,      kgc_rapp_00.formatteer ( onde.kafd_id
                                , onde.ongr_id
                                , NULL
                                , 'HEBON'
                                , 'AANHEF'
                                , onde.taal_id
                                , case
                                    when pers.geslacht = 'M'
                                    then 'heer '
                                    when pers.geslacht = 'V'
                                    then 'mw. '
                                    else null
                                  end ||
                                  case
                                    when pers.voorvoegsel is not null
                                    then upper(substr(pers.voorvoegsel,1,1))||substr(pers.voorvoegsel,2,50)||' '||pers.achternaam
                                    else pers.achternaam
                                  end
                                )                                aanhef
  ,      mss_rapp_00.formatteer ( onde.kafd_id
                                , onde.ongr_id
                                , NULL
                                , 'HEBON'
                                , 'UN_PWD'
                                , onde.taal_id
                                , hebo.hebonnummer
                                , lpad(to_char(hebo.pincode),4,'0')
                                )                                un_pwd
  from   kgc_relaties     rela
  ,      kgc_onderzoeken  onde
  ,      kgc_personen     pers
  ,      beh_hebon        hebo
  where  pers.pers_id = hebo.pers_id
  and    not exists         -- Alleen openstaande brieven printen (In BRIE nog geen
           ( select null    -- registratie brief voor geg. pers_id en type HEBON)
             from   kgc_brieftypes  brty
             ,      kgc_brieven     brie
             where  brie.pers_id = hebo.pers_id
             and    brty.brty_id = brie.brty_id
             and    brty.code = 'HEBON'
           )
  and    pers.overleden = 'N'  -- Persoon mag niet overleden zijn
  and    trunc(months_between(trunc(sysdate),trunc(pers.geboortedatum))/12) >= 18 -- Onderzoekspersoon moet minimaal 18 jaar zijn
  and    onde.onde_id = hebo.onde_id
  and    onde.pers_id = pers.pers_id
  and    onde.datum_autorisatie <= trunc(sysdate) - 61  -- Vertraging van 61 dagen na autorisatie voordat de brief gegenereerd mag worden
  and    onde.onderzoekswijze in ('PG','PV')
  and    rela.rela_id = onde.rela_id
  and    mss_brie_enotificatie.is_interne_kg_rela(rela.rela_id) = 'TRUE'  -- Onderzoek is aangevraagd door interne counseler
  and    exists
           ( select null
             from   kgc_indicatie_teksten     indi
             ,      kgc_onderzoek_indicaties  onin
             where  onin.onde_id = onde.onde_id
             and    indi.indi_id = onin.indi_id
             and    indi.code in ('BC1','BC2','BC3')
           )
  and    rownum < kgc_sypa_00.systeem_waarde('HEBON_MAX_REC_BRIEF')  -- Maximaal aantal records (= aantal Hebon brieven wat rapport gegeneert) beperken
;

/
QUIT
