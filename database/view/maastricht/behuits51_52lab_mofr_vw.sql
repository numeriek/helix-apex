CREATE OR REPLACE FORCE VIEW "HELIX"."BEHUITS51_52LAB_MOFR_VW" ("UITS_ID", "ONDE_ID", "ONMO_ID", "MONS_ID", "FRAC_ID", "MONSTERNUMMER", "MONS_ON_DAT", "MONS_PERS_ID", "MONS_AFNDAT", "FRACTIENUMMER", "MATE_ID", "MATE_OMSCH", "HERKOMST") AS
  SELECT DISTINCT
         uits.uits_id                                                   uits_id
  ,      onmo.onde_id                                                   onde_id
  ,      onmo.onmo_id                                                   onmo_id
  ,      mons.mons_id                                                   mons_id
  ,      frac.frac_id                                                   frac_id
  ,      mons.monsternummer                                             monsternummer
  ,      TO_CHAR (mons.datum_aanmelding, 'dd-mm-yyyy')                  mons_on_dat
  ,      mons.pers_id                                                   mons_pers_id
  ,      NVL (TO_CHAR (mons.datum_afname, 'dd-mm-yyyy'), 'Onbekend')    mons_afndat
  ,      frac.fractienummer                                             fractienummer
  ,      mate.mate_id                                                   mate_id
  ,      CASE
           WHEN UPPER (mate.omschrijving) LIKE 'ONBEKEND%'
           THEN ( SELECT frty.omschrijving
                  FROM   bas_fractie_types frty
                  WHERE  frty.frty_id = frac.frty_id
                )
           ELSE mate.omschrijving
         END                                                            mate_omsch
  ,      NVL (herk.code, 'Onbekend')                                    herkomst
  FROM   kgc_stoftestgroepen     stgr
  ,      bas_metingen            meti
  ,      kgc_materialen          mate
  ,      kgc_herkomsten          herk
  ,      bas_fracties            frac
  ,      kgc_monsters            mons
  ,      kgc_onderzoek_monsters  onmo
  ,      kgc_onderzoeksgroepen   ongr
  ,      kgc_onderzoeken         onde
  ,      kgc_uitslagen           uits
  WHERE  onde.onde_id = uits.onde_id
  AND    ongr.ongr_id = onde.ongr_id
  AND    onmo.onde_id = onde.onde_id
  AND    mons.mons_id = onmo.mons_id
  AND    herk.herk_id(+) = mons.herk_id
  AND    mate.mate_id = mons.mate_id
  AND    frac.mons_id = mons.mons_id
  AND    NVL (frac.status, 'O') <> 'M'
  AND    meti.onmo_id = onmo.onmo_id
  AND    meti.frac_id = frac.frac_id
  AND    meti.afgerond = 'J'
  AND    stgr.stgr_id (+) = meti.stgr_id
  AND    nvl(stgr.code,'X') not in ('AFP', 'FISH-AML', 'FISH-CLL','FISH_1','FISH_2','FISH_3','FISH_4','FISH_5','FISH_6')
  AND    nvl(stgr.code,'X') not like 'FISH-KLR%'
  AND    (   (ongr.code not in ('PM','PV','PR','TM','TV'))
          or (    ongr.code in ('PM','PV','PR','TM','TV')
              and (stgr.stgr_id,uits.brty_id) in
                    ( select ebpt.stgr_id
                      ,      ebpt.brty_id
                      from   epd_brieftype_protocol ebpt
                    )
              and (   mss_algemeen.bep_brty_code (uits.brty_id) = 'MOLDIAG'
                   or (    mss_algemeen.bep_brty_code (uits.brty_id) != 'MOLDIAG'
                       and meti.conclusie IS NOT NULL
                      )
                  )
             )
         );

/
QUIT
