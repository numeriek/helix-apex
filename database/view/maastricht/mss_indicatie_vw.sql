CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_INDICATIE_VW" ("ONDE_ID", "INDI_CODE") AS
  SELECT   onin.onde_id   onde_id
  ,        min(indi.code) indi_code
  FROM     kgc_indicatie_teksten    indi
  ,        kgc_onderzoek_indicaties onin
  WHERE    onin.hoofd_ind  = 'J'
  AND      indi.indi_id = onin.indi_id
  GROUP BY onin.onde_id;

/
QUIT
