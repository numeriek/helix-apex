CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_STBM_FOETUSSEN_VW" ("FOET_ID", "PERS_ID_MOEDER", "FOET_NR", "GESLACHT", "DOOD_GEBOREN", "PERS_ID_GEBOREN_ALS") AS
  SELECT foet.foet_id foet_id,
          foet.pers_id_moeder pers_id_moeder,
          CASE
             WHEN zwan.nummer IS NULL THEN '-' || foet.volgnr
             ELSE zwan.nummer || '-' || foet.volgnr
          END
             foet_nr,
          foet.geslacht geslacht,
          foet.niet_geboren niet_geboren,
          foet.PERS_ID_GEBOREN_ALS PERS_ID_GEBOREN_ALS
     FROM kgc_zwangerschappen zwan, kgc_foetussen foet
    WHERE zwan.zwan_id(+) = foet.zwan_id;

/
QUIT
