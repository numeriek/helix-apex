CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_ANALYSEFORMULIEREN_VW" ("KAFD_CODE", "ONGR_CODE", "PERS_ID", "PERSOON_INFO", "ZWAN_DUUR", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "MONSTERNUMMER", "HERK_CODE", "HERK_OMSCHRIJVING", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "RELA_AANSPREKEN", "MONS_HOEVEELHEID", "COND_CODE_MONS", "COND_OMSCHRIJVING_MONS", "ONDE_ID", "ONDERZOEKNR", "ONDE_GEPLANDE_EINDDATUM", "INDICATIES", "ONDE_REDEN", "GERELATEERDE_ONDERZOEKEN", "FRAC_ID", "FRACTIENUMMER", "FRTY_CODE", "FRTY_OMSCHRIJVING", "COND_CODE_FRAC", "COND_OMSCHRIJVING_FRAC", "METI_ID", "DATUM_AANMELDING", "SPOED", "STGR_CODE", "STGR_OMSCHRIJVING", "WERKLIJST", "RESULTAAT", "COMMENTAAR", "AFGEROND", "DATUM_AFGEROND", "MEDE_NAAM", "ONWY_OMSCHRIJVING", "MEET_MEETWAARDE") AS
  SELECT kafd.code kafd_code,
          ongr.code ongr_code,
          mons.pers_id pers_id,
          kgc_pers_00.info (mons.pers_id) persoon,
          kgc_zwan_00.duur (onde.onde_id) zwan_duur,
          mons.mate_id mate_id,
          mate.code mate_code,
          mate.omschrijving mate_omschrijving,
          mons.monsternummer monsternummer,
          herk.code herk_code,
          herk.omschrijving herk_omschrijving,
          mons.datum_aanmelding mons_datum_aanmelding,
          mons.datum_afname mons_datum_afname,
          rela.aanspreken rela_aanspreken,
          mons.hoeveelheid_monster || ' ' || mate.eenheid mons_hoeveelheid,
          cond1.code cond_code_mons,
          cond1.omschrijving cond_omschrijving_mons,
          onmo.onde_id onde_id,
          onde.onderzoeknr onderzoeknr,
          onde.geplande_einddatum onde_geplande_einddatum,
          kgc_indi_00.onderzoeksindicaties (onde.onde_id) indicaties,
          onde.omschrijving onde_reden,
          kgc_onde_00.gerelateerde_onderzoeken (onde.onde_id)
             gerelateerde_onderzoeken,
          frac.frac_id frac_id,
          frac.fractienummer fractienummer,
          frty.code frty_code,
          frty.omschrijving frty_omschrijving,
          cond2.code cond_code_frac,
          cond2.omschrijving cond_omschrijving_frac,
          meti.meti_id meti_id,
          meti.datum_aanmelding datum_aanmelding,
          DECODE (meti.prioriteit, 'J', 'J', onde.spoed) spoed,
          stgr.code stgr_code,
          stgr.omschrijving stgr_omschrijving,
          kgc_wlst_00.werklijst_lijst (NULL, meti.meti_id) werklijst,
          TO_CHAR (meti.conclusie) resultaat --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014)
                                            --TO_CHAR(meti.conclusie)  changed to meti.conclusie by sachin for reverting mantis 9355 changes in rework release 201502 12-06-2015
                                            --  TO_CHAR added by SKA for MANTIS 9355  2016-02-16 release 8.11.0.2
          ,
          bas_meti_00.alle_meet_commentaar (meti.meti_id) commentaar,
          meti.afgerond afgerond,
          meti.datum_afgerond datum_afgerond,
          mede.naam mede_naam,
          kgc_onwy_00.onwy_omschrijving (onde.onde_id) onwy_omschrijving,
          meet.meetwaarde meet_meetwaarde
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_herkomsten herk,
          kgc_condities cond1,
          kgc_condities cond2,
          bas_fractie_types frty,
          kgc_materialen mate,
          kgc_stoftestgroepen stgr,
          kgc_medewerkers mede,
          bas_fracties frac,
          kgc_relaties rela,
          bas_metingen meti,
          kgc_monsters mons,
          kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_meetwaardestructuur mwst,
          bas_meetwaarden meet
    WHERE     onmo.onde_id = onde.onde_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.rela_id = rela.rela_id
          AND onmo.mons_id = mons.mons_id
          AND mons.mate_id = mate.mate_id
          AND mons.herk_id = herk.herk_id(+)
          AND mons.cond_id = cond1.cond_id(+)
          AND mons.mons_id = frac.mons_id
          AND frac.frty_id = frty.frty_id(+)
          AND frac.cond_id = cond2.cond_id(+)
          AND frac.frac_id = meti.frac_id
          AND onde.onde_id = meti.onde_id
          AND meti.stgr_id = stgr.stgr_id(+)
          AND meti.mede_id = mede.mede_id(+)
          AND meet.meti_id = meti.meti_id
          AND mwst.mwst_id = meet.mwst_id
          AND mwst.code = 'KGCKARY01';

/
QUIT
