CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_CYTO_PRESTATIE_KWKFRACPROT" ("FRACTIE") AS
  Select Distinct(Meti.Frac_Id) Fractie
From   Bas_metingen        Meti,
       Kgc_Stoftestgroepen Stg
Where  Stg.Stgr_Id = Meti.Stgr_Id
And    Stg.code in ('KARYO-PR','KARYO-TU','MLPA_POST','MLPA_PRE','FISH-KARYO',
                    'BLOOM-SCE','FAN-ATAX','INACTX','SNP','AFP','ACHE','CYTOSPINXY',
                    'MD','DOOR');

/
QUIT
