CREATE OR REPLACE FORCE VIEW "HELIX"."ZMSS_PATIENTHISTORIE_VW" ("KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "PERS_ID", "ONDE_ID", "ONDERZOEKNR", "VOEGTOE", "DATUM_BINNEN", "AANSPREKEN", "TEST", "AFGEROND", "OMSCHRIJVING") AS
  SELECT distinct onde.kafd_id
,      kafd.code kafd_code
,      onde.ongr_id ongr_id
,      ongr.code ongr_code
,      onbe.pers_id pers_id
,      onde.onde_id onde_id
,      onde.onderzoeknr onderzoeknr
,      decode(onbe.rol, null, null, onbe.rol||' ')||'betr. '||
       decode( onbe.foet_id, null, null, 'foetus: '||kgc_foet_00.foetus_info( onbe.foet_id, null, null, 'J' ) ) ||
       kgc_onde_00.counselor( onde.onde_id ) voegtoe
,      onde.datum_binnen
,      rela.aanspreken
,      replace( kgc_indi_00.onderzoeksindicaties( onde.onde_id ), chr(10), '; ') test
,      onde.afgerond
,      onui.omschrijving
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_onderzoek_uitslagcodes onui
,      kgc_relaties rela
,      kgc_onderzoeken onde
,      kgc_onderzoek_betrokkenen onbe
where  onde.rela_id = rela.rela_id
and    onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.onde_id = onbe.onde_id
and    onde.onui_id = onui.onui_id(+)
UNION ALL -- onderzoeks-betrokkenheden van de moeder waarbij de persoon als foetus geregistreerd staat:
SELECT distinct onde.kafd_id
,      kafd.code
,      onde.ongr_id
,      ongr.code
,      foet.pers_id_geboren_als
,      onde.onde_id
,      onde.onderzoeknr
,      decode(onbe.rol, null, null, onbe.rol||' ')||'betr. '||
       'als foetus: '||kgc_foet_00.foetus_info( onbe.foet_id, null, null, 'J' ) ||
       kgc_onde_00.counselor( onde.onde_id )
,      onde.datum_binnen
,      rela.aanspreken
,      replace( kgc_indi_00.onderzoeksindicaties( onde.onde_id ), chr(10), '; ')
,      onde.afgerond
,      onui.omschrijving
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_onderzoek_uitslagcodes onui
,      kgc_relaties rela
,      kgc_onderzoeken onde
,      kgc_onderzoek_betrokkenen onbe
,      kgc_foetussen foet
where  onde.rela_id = rela.rela_id
and    onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.onde_id = onbe.onde_id
and    onde.onui_id = onui.onui_id(+)
and    onbe.foet_id = foet.foet_id;

/
QUIT
