CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_TRENDANALYSE_DNA_VW" ("ONDE_ID", "ONDERZOEKSWIJZE", "INDICATIECODE", "DATUM_AUTORISATIE", "DATUM_BINNEN", "NOM_DOORLOOPTIJD", "LOOPTIJD_WERKELIJK") AS
  SELECT onde.onde_id onde_id,
          onwy.omschrijving onderzoekswijze,
          indi_v.indi_code indicatiecode,
          onde.datum_autorisatie datum_autorisatie,
          onde.datum_binnen datum_binnen,
            TRUNC (
               mss_nominale_looptijd (indi.indi_id, onde.onderzoekswijze) / 7)
          + 1
             nom_doorlooptijd,
          TRUNC ( (onde.datum_autorisatie - audl.datum_tijd) / 7) + 1
             looptijd_werkelijk
     FROM kgc_indicatie_teksten indi,
          mss_indicatie_vw indi_v,
          kgc_audit_log audl,
          kgc_onderzoekswijzen onwy,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeken onde
    WHERE     onde.datum_autorisatie IS NOT NULL
          AND kafd.kafd_id = onde.kafd_id
          AND kafd.code = 'LAB'
          AND onwy.code = onde.onderzoekswijze
          AND onwy.kafd_id = onde.kafd_id
          AND audl.id = onde.onde_id
          AND audl.tabel_naam = 'KGC_ONDERZOEKEN'
          AND audl.kolom_naam = 'STATUS'
          AND audl.waarde_oud IS NULL
          AND audl.waarde_nieuw = 'G'
          AND indi_v.onde_id = onde.onde_id
          AND indi.ongr_id = onde.ongr_id
          AND indi.code = indi_v.indi_code;

/
QUIT
