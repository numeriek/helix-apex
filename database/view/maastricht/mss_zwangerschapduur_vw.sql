CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_ZWANGERSCHAPDUUR_VW" ("PERS_ID", "ZWANGERSCHAPDUUR", "ZWANGERSCHAPDUUR_WKN", "INFO_GESLACHT") AS
  SELECT zwan.pers_id                                                        pers_id
   ,      max(floor((onde.datum_binnen-(zwan.datum_aterm-280))/7)||' wkn '||
             mod((onde.datum_binnen-(zwan.datum_aterm-280)),7)||' dgn')       zwangerschapduur
   ,      max(floor((onde.datum_binnen-(zwan.datum_aterm-280))/7))            zwangerschapduur_wkn
   ,      zwan.info_geslacht                                                  info_geslacht
   FROM   kgc_onderzoeken              onde
   ,      mss_laatste_zwangerschap_vw  zwan
   WHERE  onde.pers_id = zwan.pers_id
   GROUP BY zwan.pers_id
   ,        zwan.info_geslacht;

/
QUIT
