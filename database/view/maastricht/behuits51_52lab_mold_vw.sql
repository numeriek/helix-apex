CREATE OR REPLACE FORCE VIEW "HELIX"."BEHUITS51_52LAB_MOLD_VW" ("UITS_ID", "ONMO_ID", "MONS_ID", "MATE_OMSCH", "FRAC_ID", "METI_ID", "MEET_ID", "NAAM", "SENSITIVITEIT", "UITG_MATERIAAL", "RES_MD") AS
  SELECT uits.uits_id                                                                                         uits_id
  ,      onmo.onmo_id                                                                                         onmo_id
  ,      mons.mons_id                                                                                         mons_id
  ,      mate.omschrijving                                                                                    mate_omsch
  ,      meti.frac_id                                                                                         frac_id
  ,      meti.meti_id                                                                                         meti_id
  ,      meet.meet_id                                                                                         meet_id
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','NAAM',onde.taal_id,
           ( SELECT mdet.waarde
             FROM   bas_meetwaarde_details mdet
             WHERE  mdet.meet_id = meet.meet_id
             AND    UPPER(mdet.prompt) = 'NAAM')
           )                                                                                                  naam
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','SENSI',onde.taal_id,
           ( SELECT mdet.waarde
             FROM   bas_meetwaarde_details mdet
             WHERE  mdet.meet_id = meet.meet_id
             AND    UPPER (mdet.prompt) = 'SENSITIVITEIT')
           )                                                                                                  sensitiviteit
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','UITG_MAT',onde.taal_id,frty.code)   uitg_materiaal
  ,      kgc_rapp_00.formatteer(onde.kafd_id,onde.ongr_id,NULL,'UITSLAG','RES_MD',onde.taal_id,
           ( SELECT mdet.waarde
             FROM   bas_meetwaarde_details mdet
             WHERE  mdet.meet_id = meet.meet_id
             AND UPPER (mdet.prompt) = 'RESULTAAT')
           )                                                                                                  res_md
  FROM   kgc_stoftesten          stof
  ,      bas_meetwaarden         meet
  ,      kgc_stoftestgroepen     stgr
  ,      bas_metingen            meti
  ,      bas_fractie_types       frty
  ,      bas_fracties            frac
  ,      kgc_materialen          mate
  ,      kgc_monsters            mons
  ,      kgc_onderzoek_monsters  onmo
  ,      kgc_onderzoeksgroepen   ongr
  ,      kgc_onderzoeken         onde
  ,      kgc_uitslagen           uits
  WHERE  onde.onde_id = uits.onde_id
  AND    ongr.ongr_id = onde.ongr_id
  AND    onmo.onde_id = onde.onde_id
  AND    mons.mons_id = onmo.mons_id
  AND    mate.mate_id = mons.mate_id
  AND    frac.mons_id = mons.mons_id
  AND    frac.frac_id = -- De resultaten van de metingen met de laagste meti_id per fractietype dienen getoond te worden
           ( SELECT frac2.frac_id
             FROM   bas_metingen meti2
             ,      bas_fracties frac2
             WHERE  frac2.mons_id = mons.mons_id
             AND    frac2.frty_id = frac.frty_id
             AND    meti2.frac_id = frac2.frac_id
             AND    meti2.afgerond = 'J'
             AND    meti2.meti_id =
                      ( SELECT min(meti3.meti_id)
                        FROM   bas_metingen meti3
                        ,      bas_fracties frac3
                        WHERE  frac3.mons_id = mons.mons_id
                        AND    frac3.frty_id = frac2.frty_id
                        AND    meti3.frac_id = frac3.frac_id
                        AND    meti3.afgerond = 'J'
                      )
           )
  AND    frty.frty_id = frac.frty_id
  AND    meti.onmo_id = onmo.onmo_id
  AND    meti.frac_id = frac.frac_id
  AND    meti.afgerond = 'J'
  AND    stgr.stgr_id = meti.stgr_id
  AND    meet.meti_id = meti.meti_id
  AND    meet.meet_id = -- Indien stoftest meerdere malen is aangemeld, neem die met hoogste id
           ( SELECT max(meet2.meet_id)
             FROM   kgc_stoftesten  stof2
             ,      bas_meetwaarden meet2
             WHERE  meet2.meti_id = meti.meti_id
             AND    stof2.stof_id = meet2.stof_id
             AND    stof2.stof_id = stof.stof_id
           )
  AND    stof.stof_id = meet.stof_id
  AND    (   (    ongr.code in ('DNA','CYTOM','CYTOV')
              and onde.onderzoekswijze = 'TU'
             )
          or (    ongr.code in ('TM','TV')
              and (stgr.stgr_id,uits.brty_id) in
                    ( select ebpt.stgr_id
                      ,      ebpt.brty_id
                      from   epd_brieftype_protocol ebpt
                    )
             )
         );

/
QUIT
