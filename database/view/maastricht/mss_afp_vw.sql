CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_AFP_VW" ("MEET_ID", "METI_ID", "ONMO_ID", "MEETWAARDE") AS
  SELECT   meet.meet_id      meet_id
  ,        prot.meti_id      meti_id
  ,        prot.onmo_id      onmo_id
  ,        meet.meetwaarde   meetwaarde
  FROM     bas_meetwaarden   meet
  ,        mss_protocol_vw   prot
  WHERE    prot.protocol = 'AFP'
  AND      meet.meti_id = prot.meti_id;

/
QUIT
