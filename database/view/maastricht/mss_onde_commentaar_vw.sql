CREATE OR REPLACE FORCE VIEW "HELIX"."MSS_ONDE_COMMENTAAR_VW" ("ID", "WAARDE") AS
  SELECT atwa.id                id
  ,      atwa.waarde            waarde
  FROM   kgc_attributen         attr
  ,      kgc_attribuut_waarden  atwa
  WHERE  attr.tabel_naam  = 'KGC_ONDERZOEKEN'
  AND    attr.code        = 'COMMENTAAR'
  AND    atwa.attr_id     = attr.attr_id;

/
QUIT
