CREATE OR REPLACE FORCE VIEW "HELIX"."V_GEN_AUR_AUS" ("ID", "USER_ID", "USER_ABBR", "USER_NAME", "EMAIL", "ACTIVE_IND", "PROD_USER", "OBI_ROLE", "VISITOR_ALLOWED_IND", "PASSWORD", "LDAP_DOMAIN", "USER_ROLES", "CREATED_ON", "CREATED_BY", "MODIFIED_ON", "MODIFIED_BY") AS
  SELECT aus.id
         ,aus.user_id
         ,aus.user_abbr
         ,aus.user_name
         ,aus.email
         ,aus.active_ind
         ,aus.prod_user
         ,aus.obi_role
         ,aus.visitor_allowed_ind
         ,aus.password
         ,aus.ldap_domain
         ,(SELECT LISTAGG(aur.appl_role_id
                         ,':')
                  WITHIN GROUP (ORDER BY aur.appl_user_id)
             FROM gen_appl_user_roles aur
            WHERE aur.appl_user_id = aus.id)
             user_roles
         ,created_on
         ,created_by
         ,modified_on
         ,modified_by
     FROM gen_appl_users aus
;

/
QUIT
