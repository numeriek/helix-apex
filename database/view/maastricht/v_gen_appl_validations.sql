CREATE OR REPLACE FORCE VIEW "HELIX"."V_GEN_APPL_VALIDATIONS" ("ID", "APPLICATION_ID", "DOMAIN_NAME", "DOMAIN_DATATYPE", "DOMAIN_VALUE_LOW", "DOMAIN_VALUE_HIGH", "LOW_VALUE_INCLUDED_IND", "HIGH_VALUE_INCLUDED_IND", "ACTIVE_IND", "CREATED_ON", "CREATED_BY", "MODIFIED_ON", "MODIFIED_BY") AS
  SELECT avn.id,
          avn.application_id,
          avn.domain_name,
          avn.domain_datatype,
          CASE avn.domain_datatype
             WHEN 'N' THEN TO_CHAR (avn.domain_value_low_n)
             WHEN 'C' THEN avn.domain_value_low_c
             WHEN 'D' THEN TO_CHAR (avn.domain_value_low_d, 'DD-MM-YYYY')
          END
             AS domain_value_low,
          CASE avn.domain_datatype
             WHEN 'N' THEN TO_CHAR (avn.domain_value_high_n)
             WHEN 'C' THEN avn.domain_value_high_c
             WHEN 'D' THEN TO_CHAR (avn.domain_value_high_d, 'DD-MM-YYYY')
          END
             AS domain_value_high,
          avn.low_value_included_ind,
          avn.high_value_included_ind,
          avn.active_ind,
          avn.created_on,
          avn.created_by,
          avn.modified_on,
          avn.modified_by
     FROM gen_appl_validations avn
;

/
QUIT
