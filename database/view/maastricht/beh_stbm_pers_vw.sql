CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_STBM_PERS_VW" ("PERS_PERS_ID", "PERS_VOORLETTERS", "PERS_VOORVOEGSEL", "PERS_ACHTERNAAM", "PERS_ACHTERNAAM_PARTNER", "PERS_VOORVOEGSEL_PARTNER", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_OVERLIJDENSDATUM", "PERS_OVERLEDEN", "PERS_POSTCODE", "PERS_ADRES", "ONDERZOEKNRS", "FAMILIENUMMERS", "PERS_ZISNR", "PERS_BSN", "FRACTIENUMMERS") AS
  SELECT pers.pers_id pers_pers_id,
          pers.voorletters pers_voorletters,
          pers.voorvoegsel pers_voorvoegsel,
          pers.achternaam pers_achternaam,
          pers.achternaam_partner pers_achternaam_partner,
          pers.voorvoegsel_partner pers_voorvoegsel_partner,
          pers.geboortedatum pers_geboortedatum,
          pers.geslacht pers_geslacht,
          pers.overlijdensdatum pers_overlijdensdatum,
          pers.overleden pers_overleden,
          pers.postcode pers_postcode,
          pers.adres pers_adres,
          beh_stamboom_00.haal_alle_onde_op (pers.pers_id) onderzoeknrs,
          beh_stamboom_00.haal_alle_fami_op (pers.pers_id) familienummers,
          pers.zisnr pers_zisnr,
          pers.bsn pers_bsn,
          mss_samenvoegen.fractienr_bij_pers_id(pers.pers_id) fractienummers
     FROM kgc_personen pers;

/
QUIT
