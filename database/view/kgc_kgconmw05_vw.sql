CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCONMW05_VW" ("SOORT", "METI_ID", "ONDE_ID", "ONMO_ID", "MEET_ID", "MEST_ID", "FRACTIENUMMER", "STGR_ID", "STGR_CODE", "STGR_OMSCHRIJVING", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "WAARDE", "EENHEID", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "NORMAALWAARDEN", "AFGEROND", "VOLGORDE") AS
  SELECT 'MEET' soort
,      meti.meti_id
,      meti.onde_id
,      meti.onmo_id
,      meet.meet_id
,      meet.mest_id
,      nvl( frac.fractienummer, stan.standaardfractienummer ) fractienummer
,      stgr.stgr_id
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meet.stof_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.meetwaarde waarde
,      meet.meeteenheid eenheid
,      meet.normaalwaarde_ondergrens
,      meet.normaalwaarde_bovengrens
,      bas_meet_00.normaalwaarden( meet.meet_id, 'N' ) normaalwaarden
,      decode( meti.afgerond
             , 'J', 'J'
             , meet.afgerond
             ) afgerond
,      meet.volgorde
from  bas_standaardfracties stan
,      bas_fracties frac
,      bas_meetwaarde_statussen mest
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      bas_metingen meti
,      bas_meetwaarden meet
where  meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.frac_id = frac.frac_id (+)
and    meti.stan_id = stan.stan_id (+)
and    meti.stgr_id = stgr.stgr_id (+)
and    meet.mest_id = mest.mest_id (+)
and  ( mest.in_uitslag = 'J' or meet.mest_id is null )
union all -- berekeningen
select  'BERE'
,      meti.meti_id
,      meti.onde_id
,      meti.onmo_id
,      meet.meet_id
,      meet.mest_id
,      nvl( frac.fractienummer, stan.standaardfractienummer )
,      stgr.stgr_id
,      stgr.code
,      stgr.omschrijving
,      meet.stof_id
,      stof.code
,      '('||stof.code||') '||bety.omschrijving
,      to_char(bere.uitslag)
,      bere.uitslageenheid
,      bere.normaalwaarde_ondergrens
,      bere.normaalwaarde_bovengrens
,      bas_bere_00.normaalwaarden( bere.bere_id )
,      decode( meti.afgerond
             , 'J', 'J'
             , meet.afgerond
             )
,      meet.volgorde
from  bas_standaardfracties stan
,      bas_fracties frac
,      bas_berekening_types bety
,      bas_meetwaarde_statussen mest
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_berekeningen bere
where  meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.frac_id = frac.frac_id (+)
and    meti.stan_id = stan.stan_id (+)
and    meti.stgr_id = stgr.stgr_id (+)
and    meet.mest_id = mest.mest_id (+)
and    meet.meet_id = bere.meet_id
and    bere.bety_id = bety.bety_id
and    bere.in_uitslag = 'J'
and  ( mest.in_uitslag = 'J' or meet.mest_id is null )
union all -- tbv. categorieen / alle stoftesten
SELECT 'STOF'
,      to_number(null) -- meti_id
,      bstv.onde_id
,      bstv.onmo_id
,      to_number(null) -- meet_id
,      to_number(null) -- mest_id
,      null -- fractienummer
,      to_number(null) -- stgr_id
,      null -- stgr_code
,      null -- stgr_code
,      stof.stof_id
,      stof.code
,      stof.omschrijving
,      null -- meetwaarde
,      stof.eenheid
,      to_number(null) -- ondergrens
,      to_number(null) -- bovengrens
,      bas_rang_00.normaalwaarden
       ( bstv.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       ) normaalwaarden
,      'N' -- afgerond
,     to_number(null) -- volgorde
from   kgc_stoftesten stof
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_beschikbare_stoftesten_vw bstv
where  mons.pers_id = pers.pers_id
and    bstv.mons_id = mons.mons_id
and    bstv.stof_id = stof.stof_id
and    stof.vervallen = 'N'
and    not exists
       ( select null
         from   bas_metingen meti
         ,      bas_meetwaarden meet
         WHERE  meti.meti_id = meet.meti_id
         and    meet.stof_id = bstv.stof_id
         and    meti.onde_id = bstv.onde_id
       )
 ;

/
QUIT
