CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MEDE_NAAM_VW" ("MEDE_ID", "CODE", "NAAM", "VERVALLEN") AS
  SELECT mede.mede_id mede_id
,      mede.code code
,      nvl( mede.formele_naam, mede.naam ) naam
,      mede.vervallen vervallen
from   kgc_medewerkers mede
 ;

/
QUIT
