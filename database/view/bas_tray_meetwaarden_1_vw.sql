CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_TRAY_MEETWAARDEN_1_VW" ("MEET_ID", "METI_ID", "STOF_ID", "STGR_ID", "FRAC_ID", "STAN_ID", "MONS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "TECH_ID", "DATUM_AANMELDING", "PRIORITEIT", "STOF_CODE", "STOF_OMSCHRIJVING", "FRACTIENUMMER", "FRAC_STATUS", "FRAC_CONTROLE", "MONSTERNUMMER", "ONDERZOEKNR", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "SPOED", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "MEET_REKEN", "VOORTEST_OK", "VOORTEST_OMSCHRIJVING") AS
  SELECT meet.meet_id
,            meet.meti_id
,            meet.stof_id
,            meti.stgr_id
,            meti.frac_id
,            meti.stan_id
,            frac.mons_id
,            mons.kafd_id
,            mons.ongr_id
,            meti.onde_id
,            stof.tech_id
,            meti.datum_aanmelding
,            meti.prioriteit
,            stof.code stof_code
,            stof.omschrijving stof_omschrijving
,            frac.fractienummer
,            frac.status frac_status
,            frac.controle frac_controle
,            mons.monsternummer
,            onde.onderzoeknr
,            onde.onderzoekstype
,            onde.onderzoekswijze
,            onde.spoed
,            onde.datum_binnen
,            onde.geplande_einddatum
,            meet.meet_reken
,            bas_meet_00.voortest_ok( meet.meti_id, meet.stof_id )  voortest_ok
,            stof_voor.omschrijving
from         kgc_monsters mons
,            bas_fracties frac
,            bas_metingen meti
,            kgc_stoftesten stof_voor
,            kgc_stoftesten stof
,            bas_meetwaarden meet
,            kgc_onderzoeken onde
where meet.stof_id = stof.stof_id
and      stof.stof_id_voor = stof_voor.stof_id(+)
and      meet.meti_id = meti.meti_id
and      meti.onde_id = onde.onde_id
and      meti.frac_id = frac.frac_id
and      frac.mons_id = mons.mons_id
and      meet.meet_reken <> 'R'
and      meet.meet_id_super IS NULL   -- geen substoftesten
and      meet.afgerond = 'N'
and      meti.afgerond = 'N'
and      onde.afgerond = 'N'
and      onde.status = 'G'
and      nvl( frac.status, 'O' ) not in ( 'M', 'V' )
 ;

/
QUIT
