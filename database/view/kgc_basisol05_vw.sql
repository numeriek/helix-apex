CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BASISOL05_VW" ("FRACTIENUMMER", "MONSTERNUMMER", "JN_USER", "JN_DATE_TIME", "JN_OPERATION", "VERWIJDERD", "ISOLATIELIJST_NUMMER", "ORACLE_UID", "VOLGNUMMER", "DATUM", "STAP", "FRAC_ID", "MONS_ID", "ONGR_ID", "KAFD_ID", "BUISNR", "SAMENGEVOEGD", "PROC_ID", "COMMENTAAR", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE") AS
  SELECT frac.fractienummer         fractienummer
      ,mons.monsternummer         monsternummer
      ,isolj.jn_user              jn_user
      ,isolj.jn_date_time         jn_date_time
      ,isolj.jn_operation         jn_operation
      ,'J'                        verwijderd
      ,isolj.isolatielijst_nummer isolatielijst_nummer
      ,isolj.oracle_uid           oracle_uid
      ,isolj.volgnummer           volgnummer
      ,isolj.datum                datum
      ,isolj.stap                 stap
      ,isolj.frac_id              frac_id
      ,isolj.mons_id              mons_id
      ,isolj.ongr_id              ongr_id
      ,isolj.kafd_id              kafd_id
      ,isolj.buisnr               buisnr
      ,isolj.samengevoegd         samengevoegd
      ,isolj.proc_id              proc_id
      ,isolj.commentaar           commentaar
      ,isolj.created_by           created_by
      ,isolj.creation_date        creation_date
      ,isolj.last_updated_by      last_updated_by
      ,isolj.last_update_date     last_update_date
  FROM bas_isolatielijsten_jn isolj
      ,bas_fracties           frac
      ,kgc_monsters           mons
  WHERE mons.mons_id = isolj.mons_id
AND frac.frac_id = isolj.frac_id
AND isolj.jn_operation = 'DEL'
UNION ALL
SELECT frac.fractienummer        fractienummer
      ,mons.monsternummer        monsternummer
      ,null                      jn_user
      ,null                      jn_date_time
      ,null                      jn_operation
      ,'N'                       verwijderd
      ,isol.isolatielijst_nummer isolatielijst_nummer
      ,isol.oracle_uid           oracle_uid
      ,isol.volgnummer           volgnummer
      ,isol.datum                datum
      ,isol.stap                 stap
      ,isol.frac_id              frac_id
      ,isol.mons_id              mons_id
      ,isol.ongr_id              ongr_id
      ,isol.kafd_id              kafd_id
      ,isol.buisnr               buisnr
      ,isol.samengevoegd         samengevoegd
      ,isol.proc_id              proc_id
      ,isol.commentaar           commentaar
      ,isol.created_by           created_by
      ,isol.creation_date        creation_date
      ,isol.last_updated_by      last_updated_by
      ,isol.last_update_date     last_update_date
  FROM bas_isolatielijsten isol
      ,bas_fracties        frac
      ,kgc_monsters        mons
 WHERE mons.mons_id = isol.mons_id
   AND frac.frac_id = isol.frac_id
 ;

/
QUIT
