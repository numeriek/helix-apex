CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_INDICATIES_VW" ("INDI_ID", "KAFD_ID", "ONGR_ID", "INGR_ID", "CODE", "OMSCHRIJVING", "ENTITEIT", "VERVALLEN", "OMSCHRIJVING_REDEN", "MACHTIGINGSINDICATIE", "ONGR_CODE", "ONGR_OMSCHRIJVING", "INGR_CODE") AS
  SELECT indi.indi_id
,      indi.kafd_id
,      indi.ongr_id
,      indi.ingr_id
,      indi.code
,      indi.omschrijving
,      indi.entiteit
,      decode( indi.vervallen
             , 'J', 'J'
             , ingr.vervallen
             ) vervallen
,      indi.omschrijving_reden
,      ingr.machtigingsindicatie
,      ongr.code ongr_code
,      ongr.omschrijving ongr_omschrijving
,      ingr.code ingr_code
from   kgc_onderzoeksgroepen ongr
,      kgc_indicatie_groepen ingr
,      kgc_indicatie_teksten indi
where indi.ingr_id = ingr.ingr_id
and    ingr.ongr_id = ongr.ongr_id;

/
QUIT
