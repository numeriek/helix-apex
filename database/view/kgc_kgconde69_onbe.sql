CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCONDE69_ONBE" ("ROL", "BETROKKENE", "ONDE_ID", "ONBE_ID") AS
  SELECT onbe.rol
,      kgc_pers_00.info( onbe.pers_id )
,      onde.onde_id
,      onbe.onbe_id
FROM   kgc_onderzoek_betrokkenen  onbe
,      kgc_onderzoeken            onde
WHERE  onde.onde_id = onbe.onde_id
 ;

/
QUIT
