CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_METAB_MACHTIGINGEN_VW" ("ONDE_ID", "DATUM_PRINT") AS
  SELECT brie.onde_id onde_id, TO_CHAR(brie.datum_print, 'YYYY') datum_print
FROM KGC_ONDERZOEKEN onde, KGC_BRIEVEN brie, KGC_BRIEFTYPES brty, KGC_KGC_AFDELINGEN kafd
WHERE onde.onde_id = brie.onde_id (+)
AND onde.KAFD_ID = kafd.KAFD_ID
AND brie.brty_id = brty.brty_id
AND UPPER(kafd.code) = UPPER('METAB')
AND UPPER(brty.code) = UPPER('MACHTIGING')

 ;

/
QUIT
