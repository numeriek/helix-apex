CREATE OR REPLACE FORCE VIEW "HELIX"."DIA_CODEBOEK" ("CODE", "OMSCHRIJVING", "EIGENCODE", "MCKUSICK", "ICDBPA", "DIAGNOSEGROEP", "VERVALLEN", "MCKUSICK_EDITIE", "EDITIE_INVOER", "EDITIE_WIJZIGING", "EDITIE_VERVALLEN", "REDEN_INVOER", "REDEN_WIJZIGING", "REDEN_VERVALLEN", "DIAGNOSETYPE", "ZOEKOMSCHRIJVING") AS
  SELECT ziektecode code,
       ziektetekst omschrijving,
       NULL eigencode,
       mckusick,
       icdbpa,
       diaggroepcode diagnosegroep,
       DECODE (editie_vervallen, NULL, 'N', 'J') vervallen,
       mckusick_editie,
       editie_invoer,
       editie_wijziging,
       editie_vervallen,
       reden_invoer,
       reden_wijziging,
       reden_vervallen,
       'H' diagnosetype,
       LOWER (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE
             (REPLACE (REPLACE (ziektetekst, ' ', NULL), '-', NULL)
             , '*', NULL), ',', NULL),'.', NULL), '(', NULL)
            ,')',NULL)) zoekomschrijving
        FROM cin_hfd
UNION
      SELECT ziektecode code,
             ziektesynoniem omschrijving,
             null eigencode,
             NULL mckusick,
             NULL icdbpa,
             NULL diagnosegroep,
             DECODE (editie_vervallen, NULL, 'N', 'J') vervallen,
             TO_NUMBER (NULL) mkusick_editie,
             editie_invoer,
             editie_wijziging,
             editie_vervallen,
             NULL reden_invoer,
             NULL reden_wijziging,
             NULL reden_vervallen,
             'S',
             LOWER (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE
                  (REPLACE (REPLACE (ziektesynoniem, ' ', NULL), '-'
, NULL)
                   ,'*', NULL), ',', NULL),'.', NULL), '(', NULL)
                   ,')', NULL)) zoekomschrijving
        FROM cin_syn
 ;

/
QUIT
