CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_INDEXFRACTIES_VW" ("PERS_ID", "MONSTERNUMMER", "PERS_ZISNR", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "MONS_ID", "FRAC_ID", "FRACTIENUMMER", "INDI_ID", "INDI_CODE") AS
  SELECT distinct mons.pers_id
,      mons.monsternummer
,      pers.zisnr pers_zisnr
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      frac.mons_id
,      frac.frac_id
,      frac.fractienummer
,      onin.indi_id
,      indi.code indi_code
from   kgc_indicatie_teksten indi
,      kgc_onderzoek_indicaties onin
,      kgc_personen pers
,      kgc_monsters mons
,      bas_metingen meti
,      bas_fracties frac
where frac.mons_id = mons.mons_id
and    mons.pers_id = pers.pers_id
and    frac.frac_id = meti.frac_id
and    meti.onde_id = onin.onde_id
and    onin.indi_id = indi.indi_id
and    onin.indi_id is not null
and    frac.controle = 'J';

/
QUIT
