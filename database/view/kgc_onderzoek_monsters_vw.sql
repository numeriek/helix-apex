CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERZOEK_MONSTERS_VW" ("ONMO_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "ONDERZOEKNR", "AFGEROND", "MONS_ID", "MONSTERNUMMER", "PERS_ID", "MATE_ID", "COND_ID", "COMMENTAAR", "LEEFTIJD", "PERSOON_INFO", "MATE_CODE", "MATE_OMSCHRIJVING", "COND_CODE", "COND_OMSCHRIJVING", "OPWERKDATUM") AS
  SELECT onmo.onmo_id
,      onde.kafd_id
,      onde.ongr_id
,      onde.onde_id
,      onde.onderzoeknr
,      onde.afgerond
,      mons.mons_id
,      mons.monsternummer
,      mons.pers_id
,      mons.mate_id
,      mons.cond_id
,      mons.commentaar
,      kgc_leef_00.omschrijving( mons.leeftijd, onmo.mons_id ) leeftijd
,      kgc_pers_00.persoon_info( onmo.mons_id, onmo.onde_id, mons.pers_id, 'LANG' )  persoon_info
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      cond.code cond_code
,      cond.omschrijving cond_omschrijving
,      nvl(min(opwe.opwerkdatum), min(frac.creation_date) ) opwerkdatum
from   kgc_condities cond
,      kgc_materialen mate
,      bas_opwerken opwe
,      bas_fracties frac
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
where onmo.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id (+)
and    mons.cond_id = cond.cond_id (+)
and    mons.mons_id = opwe.mons_id (+)
and    mons.mons_id = frac.mons_id (+)
and    onmo.onde_id = onde.onde_id
group by onmo.onmo_id
,      onde.kafd_id
,      onde.ongr_id
,      onde.onde_id
,      onde.onderzoeknr
,      onde.afgerond
,      mons.mons_id
,      mons.monsternummer
,      mons.pers_id
,      mons.mate_id
,      mons.cond_id
,      mons.commentaar
,      mons.leeftijd
,      onmo.mons_id
,      onmo.onde_id
,      mons.pers_id
,      mate.code
,      mate.omschrijving
,      cond.code
,      cond.omschrijving
 ;

/
QUIT
