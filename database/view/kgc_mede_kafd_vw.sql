CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MEDE_KAFD_VW" ("MEDE_ID", "MEDE_CODE", "MEDE_NAAM", "MEDE_VERVALLEN", "KAFD_ID", "KAFD_CODE", "KAFD_NAAM", "FUNC_ID", "FUNC_CODE", "FUNC_OMSCHRIJVING", "ONDERTEKENAAR", "AUTORISATOR") AS
  SELECT mede.mede_id
,  mede.code       mede_code
,  mede.naam       mede_naam
,  mede.vervallen mede_vervallen
,  kafd.kafd_id
,  kafd.code       afd_code
,  kafd.naam       afd_naam
,  func.func_id
,  func.code       func_code
,  func.omschrijving  func_omschrijving
,  meka.ondertekenaar
,  meka.autorisator
from
   kgc_kgc_afdelingen kafd
,  kgc_functies       func
,  kgc_medewerkers    mede
,  kgc_mede_kafd      meka
where
      kafd.kafd_id  = meka.kafd_id
and   mede.mede_id  = meka.mede_id
and   func.func_id (+) = meka.func_id
union all
select
   mede.mede_id
,  mede.code
,  mede.naam
,  mede.vervallen
,  to_number(null)
,  null
,  null
,  to_number(null)
,  null
,  null
, 'N'
, 'N'
from
   kgc_medewerkers    mede
 ;

/
QUIT
