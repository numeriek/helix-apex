CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_INCOMPLETE_MACHTIGINGEN_VW" ("KAFD_CODE", "ONGR_CODE", "ENTITEIT", "ID", "IDENTIFICATIE", "PERSOON", "VERZEKERAAR", "VERZEKERINGSNR", "INGR_CODE", "ONDE_ID", "AFGEROND", "AANVRAAGDATUM", "DATUM_AUTORISATIE") AS
  SELECT kafd.code kafd_code
,      ongr.code ongr_code
,      decl.entiteit
,      decl.id
,      decl.identificatie
,      kgc_pers_00.info( decl.pers_id ) persoon
,      verz.naam verz_naam
,      decl.verzekeringsnr
,      ingr.code ingr_code
,      decl.onde_id
,      decl.afgerond
,      decl.datum_aanvraag
,      decl.datum_autorisatie
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_verzekeraars verz
,      kgc_indicatie_groepen ingr
,      kgc_declaraties_vw decl
where decl.kafd_id = kafd.kafd_id
and    decl.ongr_id = ongr.ongr_id
and    decl.dewy_id = kgc_decl_00.binnenland( decl.kafd_id )
and    decl.ingr_id = ingr.ingr_id (+)
and    decl.verz_id = verz.verz_id (+)
and    kgc_brie_00.machtiging_reeds_geprint( decl.decl_id ) = 'N'
and    decl.datum_aanvraag >= add_months( sysdate, -2*12 ) -- slecht 2 jaar declarabel
and ( ( decl.verz_id is null and decl.verzekeringsnr is null )
     or decl.ingr_id is null
    )
 ;

/
QUIT
