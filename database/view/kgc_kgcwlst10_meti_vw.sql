CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCWLST10_METI_VW" ("ONDERZOEKNR", "ONDERZOEKSTYPE", "MONSTERNUMMER", "AANSPREKEN", "GEBOORTEDATUM", "HOEVEELHEID", "OPMERKING", "METI_SPOED", "METI_AFGEROND", "WLST_ID", "ONDE_ID", "MONS_ID", "METI_ID", "PERS_ID", "STGR_ID", "WLIT_VOLGNUMMER") AS
  SELECT onde.onderzoeknr
,      onde.onderzoekstype
,      nvl( mons.monsternummer, 'Controle' ) monsternummer
,      decode( mons.monsternummer, null, stan.standaardfractienummer, pers.aanspreken ) aanspreken
,      pers.geboortedatum
,      mons.hoeveelheid_monster hoeveelheid
,      meti.opmerking_algemeen opmerking
,      meti.prioriteit meti_spoed
,      meti.afgerond meti_afgerond
,      wlit.wlst_id
,      onde.onde_id
,      mons.mons_id
,      meti.meti_id
,      pers.pers_id
,      meti.stgr_id
,      min( wlit.volgnummer ) wlit_volgnummer
FROM   kgc_personen  pers
,      kgc_onderzoeken  onde
,      bas_metingen  meti
,      kgc_monsters  mons
,      kgc_werklijst_items  wlit
,      bas_standaardfracties  stan
WHERE  wlit.meti_id = meti.meti_id
AND    meti.onde_id = onde.onde_id (+)
AND    wlit.mons_id = mons.mons_id (+)
AND    mons.pers_id = pers.pers_id (+)
AND    meti.stan_id = stan.stan_id (+)
group by onde.onderzoeknr
,        onde.onderzoekstype
,        mons.monsternummer
,        stan.standaardfractienummer
,        pers.aanspreken
,        pers.geboortedatum
,        mons.hoeveelheid_monster
,        meti.opmerking_algemeen
,        meti.prioriteit
,        meti.afgerond
,        wlit.wlst_id
,        onde.onde_id
,        mons.mons_id
,        meti.meti_id
,        pers.pers_id
,        meti.stgr_id
 ;

/
QUIT
