CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_GEGENEREERDE_NUMMERS_VW" ("TABELNAAM", "KOLOMNAAM", "NUMMER", "ID", "TABEL_OMSCHRIJVING", "RECORD_OMSCHRIJVING") AS
  SELECT 'BAS_FRACTIES' tabelnaam
,	'FRACTIENUMMER' kolomnaam
,	frac.fractienummer nummer
,	frac.frac_id id
,	'Fractie' tabel_omschrijving
,	kgc_pers_00.persoon_info(mons_id,null) record_omschrijving
from bas_fracties frac
union all
select  'BAS_ISOLATIELIJSTEN'
,	'ISOLATIELIJST_NUMMER'
,	isol.isolatielijst_nummer
,	0
,	'Isolatielijst'
,	kgc_pers_00.persoon_info(mons_id,null)
from bas_isolatielijsten isol
union all
select  'KGC_MONSTERS'
,	'MONSTERNUMMER'
,	mons.monsternummer
,	mons.mons_id
,	'Monster'
,	kgc_pers_00.persoon_info(mons_id, null)
from kgc_monsters mons
union all
select 	'KGC_ONDERZOEKEN'
,	'ONDERZOEKNR'
,	ond.onderzoeknr
,	ond.onde_id
,	'Onderzoek'
,	kgc_pers_00.persoon_info(null, onde_id) record_omschrijving
from kgc_onderzoeken ond
union all
select	'KGC_FAMILIES'
,	'FAMILIENUMMER'
,	fami.familienummer
,	fami.fami_id
,	'Familie'
,	naam
from kgc_families fami
union all
select 	'KGC_FAMILIE_ONDERZOEKEN'
,	'FAMILIEONDERZOEKNR'
,	faon.familieonderzoeknr
,	faon.faon_id
,	'Familieonderzoek'
,	fami.naam
from kgc_familie_onderzoeken faon
     , kgc_families fami
where faon.fami_id = fami.fami_id
union all
select 	'KGC_WERKLIJSTEN'
,	'WERKLIJST_NUMMER'
,	wlst.werklijst_nummer
,	wlst.wlst_id
,	'Werklijst'
,	null
from kgc_werklijsten wlst
union all
select 	'KGC_METING_GROEPEN'
,	'NUMMER'
,	megr.nummer
,	megr.megr_id
,	'metinggroepen'
,	null
from kgc_meting_groepen megr
union all
select       'BAS_STANDAARDFRACTIES'
,	'STANDAARDFRACTIENUMMER'
,	stan.standaardfractienummer
,	stan.stan_id
,	'Standaardfractie'
,	stan.commentaar
from bas_standaardfracties stan
 ;

/
QUIT
