CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_TRAY_MEETWAARDEN_2_VW" ("MEET_ID", "METI_ID", "STOF_ID", "STGR_ID", "FRAC_ID", "STAN_ID", "MONS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "TECH_ID", "DATUM_AANMELDING", "PRIORITEIT", "STOF_CODE", "STOF_OMSCHRIJVING", "FRACTIENUMMER", "FRAC_STATUS", "FRAC_CONTROLE", "MONSTERNUMMER", "ONDERZOEKNR", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "SPOED", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "MEET_REKEN", "VOORTEST_OK", "VOORTEST_OMSCHRIJVING") AS
  SELECT meet.meet_id
,            meet.meti_id
,            meet.stof_id
,            meti.stgr_id
,            meti.frac_id
,            meti.stan_id
,            to_number(null)
,            ongr.kafd_id
,            stan.ongr_id
,            meti.onde_id
,            stof.tech_id
,            meti.datum_aanmelding
,            meti.prioriteit
,            stof.code
,            stof.omschrijving
,            stan.standaardfractienummer
,            null
,            'J' -- controle
,            null
,            'CONTROLE'
,            null
,            null
,            'N' -- spoed
,            to_date(null)
,            to_date(null)
,            meet.meet_reken
,            'J' -- hoeft niet te worden bepaald; bas_meet_00.voortest_ok( meet.meti_id, meet.stof_id )
,            stof_voor.omschrijving
from    kgc_onderzoeksgroepen ongr
,            bas_metingen meti
,            kgc_stoftesten stof_voor
,            kgc_stoftesten stof
,            bas_meetwaarden meet
,            bas_standaardfracties stan
where stan.ongr_id = ongr.ongr_id
and      stof.stof_id_voor = stof_voor.stof_id(+)and      meet.stof_id = stof.stof_id
and      meet.meti_id = meti.meti_id
and      meti.stan_id = stan.stan_id
and      meet.meet_reken <> 'R'
and      meet.meet_id_super IS NULL   -- geen substoftesten
and      meet.afgerond = 'N'
and      meti.afgerond = 'N'
and      stan.vervallen = 'N'
 ;

/
QUIT
