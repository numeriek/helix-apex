CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KEUZE_RAPPORTEN_VW" ("KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONDE_ID", "ONDERZOEKNR", "BSN_OPGEMAAKT", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ADRES_AANVRAGER", "INDICATIE", "MONS_ID", "MONS_DATUM_AANMELDING", "REFERENTIE") AS
  SELECT kafd.kafd_id
,      kafd.code kafd_code
,      onde.ongr_id
,      onde.onde_id
,      onde.onderzoeknr
,      kgc_pers_00.bsn_opgemaakt(pers.pers_id) bsn_opgemaakt
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      kgc_adres_00.relatie( onde.rela_id, 'J' ) adres_aanvrager
,      kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie
,      mons.mons_id
,      mons.datum_aanmelding mons_datum_aanmelding
,      kgc_uits_00.referentie( kafd.code, onde.onde_id, onde.onderzoeknr ) referentie
from   kgc_kgc_afdelingen kafd
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
where onde.kafd_id = kafd.kafd_id
and    onde.pers_id = pers.pers_id
and    onmo.onde_id = onde.onde_id
and    onmo.mons_id = mons.mons_id
 ;

/
QUIT
