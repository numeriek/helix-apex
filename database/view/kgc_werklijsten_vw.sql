CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_WERKLIJSTEN_VW" ("WLST_ID", "WERKLIJST_NUMMER", "KAFD_ID", "KAFD_CODE", "MEDE_NAAM", "DATUM", "DATUM_TIJD_CHAR", "ARCHIEF", "GESORTEERD", "CREATION_DATE", "AANTAL") AS
  SELECT wlst.wlst_id
,      wlst.werklijst_nummer
,      kafd.kafd_id
,      kafd.code kafd_code
,      kgc_mede_00.medewerker_naam( wlst.oracle_uid ) mede_naam
,      wlst.datum
,      TO_CHAR( wlst.datum, 'DD-MM-YYYY HH24:MI:SS' )
,      wlst.archief
,      wlst.gesorteerd
,      wlst.creation_date
,      count(*) aantal
from   kgc_kgc_afdelingen kafd
,      kgc_werklijst_items_archief wlia
,      kgc_werklijst_items wlit
,      kgc_werklijsten wlst
where wlst.kafd_id = kafd.kafd_id
and    wlst.wlst_id = wlit.wlst_id (+)
and    wlst.wlst_id = wlia.wlst_id (+)
group by wlst.wlst_id
,      wlst.werklijst_nummer
,      kafd.kafd_id
,      kafd.code
,      wlst.oracle_uid
,      wlst.datum
,      wlst.archief
,      wlst.gesorteerd
,      wlst.creation_date
 ;

/
QUIT
