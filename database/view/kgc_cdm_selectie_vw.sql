CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_CDM_SELECTIE_VW" ("FRACTIENUMMER", "ONDERZOEKNR", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "FAMILIENUMMER", "MATE_CODE", "RELA_AANSPREKEN", "INDICATIES", "NOTITIE", "FRAC_ID", "ONDE_ID", "PERS_ID", "ONGR_ID", "KAFD_ID") AS
  SELECT distinct frac.fractienummer
,      onde.onderzoeknr
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      kgc_fami_00.familie_bij_onderzoek( onde.onde_id, 'N' ) familienummer
,      mate.code mate_code
,      rela.aanspreken rela_aanspreken
,      kgc_indi_00.onde_indicatie_codes( onde.onde_id ) indicaties
,      kgc_cdm_00.cdm_notitie( meti.frac_id, meti.onde_id ) notitie
,      frac.frac_id
,      onde.onde_id
,      pers.pers_id
,      onde.ongr_id
,      onde.kafd_id
from   kgc_materialen mate
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      bas_fracties frac
,      bas_metingen meti
,      kgc_stoftestgroepen stgr
where  meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
and    mons.pers_id = pers.pers_id
and    meti.onde_id = onde.onde_id
and    meti.stgr_id = stgr.stgr_id
and    onde.rela_id = rela.rela_id
and    onde.status = 'G'
and    onde.afgerond = 'N'
and    meti.afgerond = 'N'
-- and    kgc_cdm_00.cdm_techniek( meti.frac_id, meti.onde_id ) = 'J'
and    kgc_cdm_00.cdm_protocol(stgr.code,onde.kafd_id,onde.ongr_id) = 'J';

/
QUIT
