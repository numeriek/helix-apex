CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_METINGEN_VW" ("AANSPREKEN", "GEBOORTEDATUM", "PERSOON_INFO", "LEEF_OMSCHRIJVING", "STGR_CODE", "STGR_OMSCHRIJVING", "PRIORITEIT", "ONDERZOEKNR", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "DATUM_AFNAME", "HOEVEELHEID_MONSTER", "BEWAREN", "MONS_MEDISCHE_INDICATIE", "MONS_MEDICATIE", "MONS_VOEDING", "MONS_COMMENTAAR", "DATUM_AANMELDING", "HERHALEN", "DECLAREREN", "AFGEROND", "OPMERKING_ALGEMEEN", "OPMERKING_ANALYSE", "ANALIST", "AUTORISATOR", "SCREENINGTEST", "CONCLUSIE", "METI_ID", "ONDE_ID", "STGR_ID", "PERS_ID", "MEDE_ID_ANALIST", "MEDE_ID_AUTORISATOR", "ONMO_ID", "LEEF_ID", "KAFD_ID", "ONGR_ID", "FRAC_ID", "MONS_ID", "ONUI_ID", "ONUI_CODE", "ONDE_AFGEROND", "FRTY_CODE", "FRTY_OMSCHRIJVING", "FRACTIENUMMER", "FRACTIESTATUS", "ONDERZOEKSTATUS", "STAN_ID", "ANALYSENR", "TAAL_ID", "DATUM_AFGEROND", "METI_GEPLANDE_EINDDATUM", "GESLACHT", "SPOED", "ONDERZOEKSTYPE") AS
  SELECT pers.aanspreken aanspreken
,      pers.geboortedatum geboortedatum
,      kgc_pers_00.persoon_info( null, meti.onde_id, onde.pers_id, 'LANG' ) persoon_info
,      leef.omschrijving leef_omschrijving
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.prioriteit prioriteit
,      onde.onderzoeknr onderzoeknr
,      mons.monsternummer monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      mons.datum_afname datum_afname
,      mons.hoeveelheid_monster hoeveelheid_monster
,      mons.bewaren bewaren
,      mons.medische_indicatie
,      mons.medicatie
,      mons.voeding
,      mons.commentaar
,      meti.datum_aanmelding datum_aanmelding
,      meti.herhalen herhalen
,      meti.declareren declareren
,      meti.afgerond afgerond
,      meti.opmerking_algemeen opmerking_algemeen
,      meti.opmerking_analyse opmerking_analyse
,      bas_meti_00.mede_code_analist(meti.meti_id) analist
,      mede1.code autorisator
,      'N' screeningtest
,      to_char(meti.conclusie) conclusie -- Mantis 9355 to_char added by SKA 28-03-2016 release 8.11.0.2
,      meti.meti_id
,      meti.onde_id
,      meti.stgr_id
,      onde.pers_id
,      bas_meti_00.mede_id_analist(meti.meti_id) mede_id_analist
,      mede1.mede_id mede_id_autorisator
,      onmo.onmo_id
,      leef.leef_id
,      onde.kafd_id
,      onde.ongr_id
,      meti.frac_id
,      mons.mons_id
,      onde.onui_id
,      onui.code
,      onde.afgerond onde_afgerond
,      frty.code frty_code
,      frty.omschrijving frty_omschrijving
,      frac.fractienummer fractienummer
,      frac.status fractiestatus
,      onde.status onderzoekstatus
,      meti.stan_id
,      meti.analysenr
,      onde.taal_id
,      meti.datum_afgerond
,      meti.geplande_einddatum
,      pers.geslacht
,      onde.spoed
,      onde.onderzoekstype
FROM   bas_fractie_types frty
,      kgc_onderzoek_uitslagcodes onui
,      kgc_medewerkers mede1
,      kgc_medewerkers mede2
,      kgc_leeftijden leef
,      kgc_materialen mate
,      kgc_personen pers
,      bas_fracties frac
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
,      kgc_onderzoeken onde
,      kgc_stoftestgroepen stgr
,      bas_metingen meti
WHERE  meti.onde_id = onde.onde_id (+)
AND    meti.stgr_id = stgr.stgr_id (+)
AND    meti.frac_id = frac.frac_id (+)
AND    frac.frty_id = frty.frty_id (+)
AND    meti.onmo_id = onmo.onmo_id (+)
AND    meti.mede_id = mede1.mede_id (+)
AND    onde.pers_id = pers.pers_id (+)
AND    onde.mede_id_autorisator = mede2.mede_id (+)
AND    onde.onui_id = onui.onui_id (+)
AND    mons.leef_id = leef.leef_id (+)
AND    mons.mate_id = mate.mate_id (+)
AND    onmo.mons_id = mons.mons_id (+);

/
QUIT
