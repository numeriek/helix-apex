CREATE OR REPLACE FORCE VIEW "HELIX"."IDEF_OPENSTAANDE_TESTEN_VW" ("PERS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "MONS_ID", "MATE_ID", "FRAC_ID", "STGR_ID", "METI_ID", "MONS_ONGR_CODE", "ONDE_ONGR_CODE", "ONDERZOEKNR", "ONDE_DATUM_BINNEN", "ONDE_GEPLANDE_EINDDATUM", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "ONDE_STATUS", "ONUI_CODE", "ONDE_AFGEROND", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "FAMILIENUMMER", "STGR_CODE", "STGR_OMSCHRIJVING", "METI_DATUM_AANMELDING", "MEDE_CODE", "METI_AFGEROND", "SPOED", "INDI_CODE") AS
  SELECT mons.pers_id
, mons.kafd_id
, mons.ongr_id
, onde.onde_id
, mons.mons_id
, mons.mate_id
, frac.frac_id
, meti.stgr_id
, meti.meti_id
, mons_ongr.code mons_ongr_code
, onde_ongr.code onde_ongr_code
, onde.onderzoeknr
, onde.datum_binnen onde_datum_binnen
, onde.geplande_einddatum onde_geplande_einddatum
, onde.onderzoekstype
, onde.onderzoekswijze
, onde.status onde_status
, onui.code onui_code
, onde.afgerond onde_afgerond
, mons.monsternummer
, mate.code mate_code
, mate.omschrijving mate_omschrijving
, frac.fractienummer
, kgc_fami_00.familie_bij_onderzoek(onde.onde_id,'N') familienummer
, stgr.code stgr_code
, stgr.omschrijving stgr_omschrijving
, meti.datum_aanmelding meti_datum_aanmelding
, mede.code mede_code
, meti.afgerond meti_afgerond
, decode( onde.spoed
, 'J', 'J'
, meti.prioriteit
) spoed
, kgc_indi_00.indi_code(onde.onde_id) indi_code
from kgc_onderzoeksgroepen mons_ongr
, kgc_onderzoeksgroepen onde_ongr
, kgc_onderzoek_uitslagcodes onui
, kgc_materialen mate
, kgc_stoftestgroepen stgr
, kgc_monsters mons
, kgc_onderzoeken onde
, bas_fracties frac
, kgc_medewerkers mede
, bas_metingen meti
where frac.mons_id = mons.mons_id
and mons.ongr_id = mons_ongr.ongr_id
and onde.ongr_id = onde_ongr.ongr_id
and mons.mate_id = mate.mate_id
and meti.onde_id = onde.onde_id
and meti.frac_id = frac.frac_id
and mede.oracle_uid = meti.last_updated_by
and meti.stgr_id = stgr.stgr_id (+)
and onde.onui_id = onui.onui_id (+)
 ;

/
QUIT
