CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_XML_ONDERZOEKEN_VW" ("ONDE_ID", "ONGR_ID", "KAFD_ID", "PERS_ID", "ONDERZOEKSTYPE", "ONDERZOEKNR", "RELA_ID", "SPOED", "DECLAREREN", "AFGEROND", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "FOET_ID", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "HERK_ID", "INST_ID", "ONDERZOEKSWIJZE", "STATUS", "MEDE_ID_BEOORDELAAR", "MEDE_ID_AUTORISATOR", "MEDE_ID_CONTROLE", "MEDE_ID_DIA_AUTORISATOR", "ONUI_ID", "COTY_ID", "DIAGNOSE_CODE", "ERMO_ID", "DIST_ID", "HERK_ID_DIA", "TAAL_ID", "VERZ_ID", "PROJ_ID", "FARE_ID", "DEWY_ID", "ZEKERHEID_DIAGNOSE", "ZEKERHEID_STATUS", "ZEKERHEID_ERFMODUS", "DATUM_AUTORISATIE", "KARIOTYPERING", "REFERENTIE", "NOTA_ADRES", "OMSCHRIJVING", "ONRE_ID", "RELA_ID_OORSPRONG") AS
  SELECT ONDE_ID, ONGR_ID, KAFD_ID,
   PERS_ID, ONDERZOEKSTYPE, ONDERZOEKNR,
   RELA_ID, SPOED, DECLAREREN,
   AFGEROND, CREATED_BY, CREATION_DATE,
   LAST_UPDATED_BY, LAST_UPDATE_DATE, FOET_ID,
   to_char(DATUM_BINNEN, 'DD-MM-RRRR') datum_binnen, GEPLANDE_EINDDATUM, HERK_ID,
   INST_ID, ONDERZOEKSWIJZE, STATUS,
   MEDE_ID_BEOORDELAAR, MEDE_ID_AUTORISATOR, MEDE_ID_CONTROLE,
   MEDE_ID_DIA_AUTORISATOR, ONUI_ID, COTY_ID,
   DIAGNOSE_CODE, ERMO_ID, DIST_ID,
   HERK_ID_DIA, TAAL_ID, VERZ_ID,
   PROJ_ID, FARE_ID, DEWY_ID,
   ZEKERHEID_DIAGNOSE, ZEKERHEID_STATUS, ZEKERHEID_ERFMODUS,
   DATUM_AUTORISATIE, KARIOTYPERING, REFERENTIE,
   NOTA_ADRES, OMSCHRIJVING, ONRE_ID,
   RELA_ID_OORSPRONG
FROM KGC_ONDERZOEKEN
 ;

/
QUIT
