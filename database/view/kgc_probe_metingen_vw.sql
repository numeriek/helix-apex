CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_PROBE_METINGEN_VW" ("METI_ID", "ONMO_ID", "ONDE_ID", "FRAC_ID", "METI_AFGEROND", "DATUM_AANMELDING", "STGR_ID", "STGR_CODE", "STGR_OMSCHRIJVING", "DATUM_ANALYSE1", "MEDE_ID_ANALYSE1", "MEDE_CODE_ANALYSE1", "MEDE_NAAM_ANALYSE1", "DATUM_ANALYSE2", "MEDE_ID_ANALYSE2", "MEDE_CODE_ANALYSE2", "MEDE_NAAM_ANALYSE2", "MEDE_ID_CONTROLE", "MEDE_CODE_CONTROLE", "MEDE_NAAM_CONTROLE", "METI_RESULTAAT", "METI_COMMENTAAR", "MEET_ID_1", "MEET_ID_2", "MEET_ID_3", "MEET_ID_4", "METI_PRIORITEIT", "METI_GEPLANDE_EINDDATUM") AS
  SELECT meti.meti_id
,      meti.onmo_id
,      meti.onde_id
,      meti.frac_id
,      meti.afgerond meti_afgerond
,      meti.datum_aanmelding
,      meti.stgr_id
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.datum_analyse datum_analyse1
,      meti.mede_id mede_id_analyse1
,      mede.code mede_code_analyse1
,      mede.naam mede_naam_analyse1
,      meti.datum_analyse2
,      meti.mede_id_analyse2
,      mede2.code mede_code_analyse2
,      mede2.naam mede_naam_analyse2
,      meti.mede_id_controle
,      mede3.code mede_code_controle
,      mede3.naam mede_naam_controle
,      to_char(meti.conclusie) meti_resultaat  --TO_CHAR added by Anuja for MANTIS 9355 on 8/4/2016 for release 8.11.0.3
,      meti.opmerking_algemeen meti_commentaar
,      kgc_pros_00.aangemelde_probe( meti.meti_id, 1 ) meet_id_1
,      kgc_pros_00.aangemelde_probe( meti.meti_id, 2 ) meet_id_2
,      kgc_pros_00.aangemelde_probe( meti.meti_id, 3 ) meet_id_3
,      kgc_pros_00.aangemelde_probe( meti.meti_id, 4 ) meet_id_4
,      meti.prioriteit meti_prioriteit
,      meti.geplande_einddatum meti_geplande_einddatum
from   kgc_stoftestgroepen stgr
,      kgc_medewerkers mede
,      kgc_medewerkers mede2
,      kgc_medewerkers mede3
,      bas_metingen meti
where meti.stgr_id = stgr.stgr_id (+)
and    meti.mede_id = mede.mede_id (+)
and    meti.mede_id_analyse2 = mede2.mede_id (+)
and    meti.mede_id_controle = mede3.mede_id (+)
and    meti.onmo_id is not null
and    meti.frac_id is not null
and ( exists
           ( select prmw.meti_id
               from kgc_probe_meetwaarden_vw prmw
               where prmw.meti_id = meti.meti_id
           )
      or not exists
          ( select null
            from   bas_meetwaarden meet
            where meet.meti_id = meti.meti_id
          )
   );

/
QUIT
