CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_CAWA_VW" ("REFERENTIE_TABEL", "KAFD_ID", "ONGR_ID", "WAARDE", "BESCHRIJVING") AS
  SELECT 'KGC_STOFTESTEN' referentie_tabel
, kafd_id kafd_id
, to_number(null) ongr_id
, code waarde
, omschrijving beschrijving
from kgc_stoftesten
where vervallen = 'N'
union all
select 'KGC_BEREKENING_TYPES'
, to_number(null)
, to_number(null)
, code
, omschrijving
from bas_berekening_types
where vervallen = 'N'
union all
select 'KGC_POSTCODES'
, to_number(null)
, to_number(null)
, postcode
, plaats
from kgc_postcodes
 ;

/
QUIT
