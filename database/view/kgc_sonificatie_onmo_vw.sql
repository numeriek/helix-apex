CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_SONIFICATIE_ONMO_VW" ("KAFD_ID", "ONGR_ID", "ONMO_ID", "PERS_ID", "MONS_ID", "MATE_ID", "ONDE_ID", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "ONDE_DATUM_BINNEN", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "MONS_DATUM_AANMELDING", "AANTAL_MEET") AS
  SELECT onde.kafd_id
,      onde.ongr_id
,      onmo.onmo_id
,      mons.pers_id
,      onmo.mons_id
,      mons.mate_id
,      onmo.onde_id
,      kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr
,      onde.datum_binnen onde_datum_binnen
,      mons.monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      mons.datum_aanmelding mons_datum_aanmelding
,      count( meet.meet_id ) aantal_meet
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      bas_metingen meti
,      bas_meetwaarden meet
,      kgc_onderzoek_monsters onmo
where onmo.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
and    onmo.onde_id = onde.onde_id
and    onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.afgerond = 'N'
and    meti.afgerond = 'N'
and    meet.afgerond = 'N'
and    onmo.onde_id = meti.onde_id
and    meti.meti_id = meet.meti_id
and    exists
       ( select null
         from   kgc_onmo_cate_vw omca
         ,      kgc_sonificatie_cawa_vw socw
         where  socw.onmo_id = onmo.onmo_id
         and    socw.stof_id = meet.stof_id
         and    socw.onmo_id = omca.onmo_id
         and    socw.cate_id = omca.cate_id
         and    omca.cate_id_parent = kgc_cate_00.categorie( 'CATEGORIE_SONIFICATIE'
                                                           , onde.kafd_id
                                                           , onde.ongr_id
                                                           , 'KGC_MATERIALEN'
                                                           , mons.mate_id
                                                           )
       )
and    not exists
       ( select null
         from   kgc_werklijst_items_vw wliv
         where  wliv.meet_id = meet.meet_id
         and    wliv.werklijst_nummer like 'SON____'
       )
group by onde.kafd_id
,        onde.ongr_id
,        onmo.onmo_id
,        mons.pers_id
,        onmo.mons_id
,        mons.mate_id
,        onmo.onde_id
,        kafd.code
,        ongr.code
,        onde.onderzoeknr
,        onde.datum_binnen
,        mons.monsternummer
,        mate.code
,        mate.omschrijving
,        mons.datum_aanmelding
 ;

/
QUIT
