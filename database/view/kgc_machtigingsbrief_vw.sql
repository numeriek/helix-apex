CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MACHTIGINGSBRIEF_VW" ("DECL_ID", "GEADRESSEERDE", "BETREFT_TEKST", "IDENTIFICATIE", "VERZEKERDE", "GESLACHT", "GEBOORTEDATUM", "ADRES_VERZEKERDE", "ZISNR", "ZIS_CODE", "VERZEKERAAR", "VERZEKERINGSNR", "AANVRAAGDATUM", "DATUM_AUTORISATIE", "AFGEROND", "RELA_CODE", "VERWIJZER", "RELATIE", "TOELICHTING", "AUTORISATOR", "ENTITEIT", "ID", "KAFD_ID", "ONGR_ID", "PERS_ID", "INGR_ID", "ONDE_ID", "TAAL_ID", "DEWY_ID", "VERZ_ID", "RAMO_ID", "DEWY_CODE", "RAMO_CODE", "KAFD_CODE", "ONGR_CODE", "EERDER_GEPRINT", "INDICATIE", "ONDERZOEKNR", "DECL_STATUS") AS
  SELECT decl.decl_id
, decl.nota_adres geadresseerde
, nvl( decl.betreft_tekst
     , decode( kafd.code
             , 'CYTO', decode( substr( ongr.code, 1, 3 )
                             , 'PRE', 'Prenatale cytogenetische diagnostiek'
                             , 'POS', 'Postnatale cytogenetische diagnostiek'
                             , 'TUM', 'Postnatale cytogenetische diagnostiek'
                             , dewy.betreft_tekst
                             )
             , 'OBS',  decode( onde.onderzoekswijze
                             , 'U1', 'Ultrageluidonderzoek type I'
                             , 'U2', 'Ultrageluidonderzoek type II'
                             , dewy.betreft_tekst
                             )
             , dewy.betreft_tekst
             )
     ) betreft_tekst
, kgc_decl_00.decl_identificatie( decl.decl_id ) identificatie
, pers.aanspreken verzekerde
, pers.geslacht geslacht
, pers.geboortedatum
, kgc_adres_00.persoon( pers.pers_id, 'N' ) adres_verzekerde
, pers.zisnr
, verz.zis_code zis_code
, nvl( verz.naam, 'Onbekend' ) verzekeraar
, decl.verzekeringsnr || ' ('||decl.verzekeringswijze||')' verzekeringsnr
--, onde.datum_binnen aanvraagdatum
, decl.datum aanvraagdatum
, onde.datum_autorisatie
, onde.afgerond
, rela.code rela_code
, kgc_adres_00.relatie( rela.rela_id,'J') verwijzer
, nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
, decl.toelichting
, mede.naam autorisator
, decl.entiteit
, decl.id
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.ingr_id
, onde.onde_id
, onde.taal_id
, decl.dewy_id
, decl.verz_id
, dewy.ramo_id
, dewy.code dewy_code
, ramo.code ramo_code
, kafd.code kafd_code
, ongr.code ongr_code
, kgc_brie_00.machtiging_reeds_geprint( decl.decl_id ) eerder_geprint
, nvl( kgc_decl_00.machtigingsindicatie( decl.decl_id )
     , kgc_indi_00.machtigingsindicatie( onde.onde_id )
     ) indicatie
, onde.onderzoeknr
, decl.status decl_status
from kgc_familie_relaties fare
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_rapport_modules ramo
, kgc_declaratiewijzen dewy
, kgc_medewerkers mede
, kgc_verzekeraars verz
, kgc_relaties rela
, kgc_personen pers
, kgc_onderzoeken onde
, kgc_declaraties decl
where decl.kafd_id = kafd.kafd_id
and decl.ongr_id = ongr.ongr_id
and decl.pers_id = pers.pers_id
and decl.entiteit = 'ONDE'
and decl.id = onde.onde_id
and rela.rela_id = nvl( onde.rela_id_oorsprong, onde.rela_id )
and onde.mede_id_autorisator = mede.mede_id (+)
and decl.verz_id = verz.verz_id (+)
and decl.fare_id = fare.fare_id (+)
and decl.dewy_id = dewy.dewy_id
and dewy.ramo_id = ramo.ramo_id
and decl.declareren = 'J'
and nvl( decl.status, 'H' ) in ('H','V')
and kgc_decl_00.machtigingsbrief_nodig( decl.decl_id ) = 'J'
union all SELECT decl.decl_id
, decl.nota_adres
, nvl( decl.betreft_tekst
     , decode( kafd.code
             , 'CYTO', decode( substr( ongr.code, 1, 3 )
                             , 'PRE', 'Prenatale cytogenetische diagnostiek'
                             , 'POS', 'Postnatale cytogenetische diagnostiek'
                             , 'TUM', 'Postnatale cytogenetische diagnostiek'
                             , dewy.betreft_tekst
                             )
             , 'OBS',  decode( onde.onderzoekswijze
                             , 'U1', 'Ultrageluidonderzoek type I'
                             , 'U2', 'Ultrageluidonderzoek type II'
                             , dewy.betreft_tekst
                             )
             , dewy.betreft_tekst
             )
     ) betreft_tekst
, kgc_decl_00.decl_identificatie( decl.decl_id )
, pers.aanspreken
, pers.geslacht
, pers.geboortedatum
, kgc_adres_00.persoon( pers.pers_id, 'N')
, pers.zisnr
, verz.zis_code
, nvl( verz.naam, 'Onbekend' )
, decl.verzekeringsnr || ' ('||decl.verzekeringswijze||')'
--, onde.datum_binnen
, decl.datum aanvraagdatum
, onde.datum_autorisatie
, onde.afgerond
, rela.code
, kgc_adres_00.relatie( rela.rela_id, 'J' )
, nvl( fare.omschrijving
     , decode ( decl.pers_id
              , onde.pers_id, 'Verzekerde is index persoon'
              , 'Verzekerde is betrokkene bij het onderzoek'
              )
     )
, decl.toelichting
, mede.naam
, decl.entiteit
, decl.id
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.ingr_id
, onde.onde_id
, onde.taal_id
, decl.dewy_id
, decl.verz_id
, dewy.ramo_id
, dewy.code
, ramo.code
, kafd.code
, ongr.code
, kgc_brie_00.machtiging_reeds_geprint( decl.decl_id )
, nvl( kgc_decl_00.machtigingsindicatie( decl.decl_id )
     , kgc_indi_00.machtigingsindicatie( onde.onde_id )
     )
, onde.onderzoeknr
, decl.status decl_status
from kgc_familie_relaties fare
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_rapport_modules ramo
, kgc_declaratiewijzen dewy
, kgc_medewerkers mede
, kgc_verzekeraars verz
, kgc_relaties rela
, kgc_personen pers
, kgc_onderzoeken onde
, kgc_onderzoek_betrokkenen onbe
, kgc_declaraties decl
where decl.kafd_id = kafd.kafd_id
and decl.ongr_id = ongr.ongr_id
and decl.entiteit = 'ONBE'
and decl.id = onbe.onbe_id
and onbe.pers_id = pers.pers_id
and onbe.onde_id = onde.onde_id
and rela.rela_id = nvl( onde.rela_id_oorsprong, onde.rela_id )
and onde.mede_id_autorisator = mede.mede_id (+)
and decl.verz_id = verz.verz_id (+)
and decl.fare_id = fare.fare_id (+)
and decl.dewy_id = dewy.dewy_id
and dewy.ramo_id = ramo.ramo_id
and decl.declareren = 'J'
and nvl( decl.status, 'H' ) in ('H','V')
and onde.declareren = 'J'
and kgc_decl_00.machtigingsbrief_nodig( decl.decl_id ) = 'J'
union all SELECT decl.decl_id
, decl.nota_adres
, nvl( decl.betreft_tekst
     , decode( kafd.code
             , 'CYTO', decode( substr( ongr.code, 1, 3 )
                             , 'PRE', 'Prenatale cytogenetische diagnostiek'
                             , 'POS', 'Postnatale cytogenetische diagnostiek'
                             , 'TUM', 'Postnatale cytogenetische diagnostiek'
                             , dewy.betreft_tekst
                             )
             , 'OBS',  decode( onde.onderzoekswijze
                             , 'U1', 'Ultrageluidonderzoek type I'
                             , 'U2', 'Ultrageluidonderzoek type II'
                             , dewy.betreft_tekst
                             )
             , dewy.betreft_tekst
             )
     ) betreft_tekst
, kgc_decl_00.decl_identificatie( decl.decl_id )
, pers.aanspreken
, pers.geslacht geslacht
, pers.geboortedatum
, kgc_adres_00.persoon( pers.pers_id, 'N' )
, pers.zisnr
, verz.zis_code
, nvl( verz.naam, 'Onbekend' )
, decl.verzekeringsnr || ' ('||decl.verzekeringswijze||')'
--, onde.datum_binnen
, decl.datum aanvraagdatum
, nvl( onde.datum_autorisatie
     , brie.datum_print
     )
, onde.afgerond
, rela.code
, kgc_adres_00.relatie( rela.rela_id, 'J' )
, nvl( fare.omschrijving, 'Verzekerde is index persoon' )
, decl.toelichting
, mede.naam
, decl.entiteit
, decl.id
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.ingr_id
, onde.onde_id
, onde.taal_id
, decl.dewy_id
, decl.verz_id
, dewy.ramo_id
, dewy.code
, ramo.code
, kafd.code
, ongr.code
, kgc_brie_00.machtiging_reeds_geprint( decl.decl_id )
, nvl( kgc_decl_00.machtigingsindicatie( decl.decl_id )
     , kgc_indi_00.machtigingsindicatie( onde.onde_id )
     )
, onde.onderzoeknr
, decl.status decl_status
from kgc_familie_relaties fare
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_rapport_modules ramo
, kgc_declaratiewijzen dewy
, kgc_medewerkers mede
, kgc_verzekeraars verz
, kgc_relaties rela
, kgc_brieftypes brty
, kgc_brieven brie
, kgc_uitslagen uits
, kgc_personen pers
, kgc_onderzoeken onde
, kgc_onderzoek_betrokkenen onbe
, kgc_declaraties decl
where decl.kafd_id = kafd.kafd_id
and decl.ongr_id = ongr.ongr_id
and decl.entiteit = 'ONBE'
and decl.id = onbe.onbe_id
and onbe.pers_id = pers.pers_id
and onbe.onde_id = onde.onde_id
and onde.onde_id = uits.onde_id
and uits.mede_id = mede.mede_id (+)
and rela.rela_id = nvl( onde.rela_id_oorsprong, onde.rela_id )
and decl.verz_id = verz.verz_id (+)
and decl.fare_id = fare.fare_id (+)
and decl.dewy_id = dewy.dewy_id
and dewy.ramo_id = ramo.ramo_id
and decl.declareren = 'J'
and nvl( decl.status, 'H' ) in ('H','V')
and onde.afgerond = 'N'
and onde.onde_id = brie.onde_id
and brie.kopie = 'N'
and brie.brty_id = brty.brty_id
and brty.code = 'UITSLAG_T'
and kgc_decl_00.machtigingsbrief_nodig( decl.decl_id ) = 'J'
union all SELECT decl.decl_id
, decl.nota_adres
, nvl( decl.betreft_tekst
     , dewy.betreft_tekst
     )
, kgc_decl_00.decl_identificatie( decl.decl_id )
, pers.aanspreken verzekerde
, pers.geslacht geslacht
, pers.geboortedatum
, kgc_adres_00.persoon( pers.pers_id, 'N' ) adres_verzekerde
, pers.zisnr
, verz.zis_code
, nvl( verz.naam, 'Onbekend' ) verzekeraar
, decl.verzekeringsnr || ' ('||decl.verzekeringswijze||')' verzekeringsnr
--, mons.datum_aanmelding
, decl.datum aanvraagdatum
, mons.datum_aanmelding
, 'J'
, rela.code
, kgc_adres_00.relatie( rela.rela_id,'J') verwijzer
, nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
, decl.toelichting
, null
, decl.entiteit
, decl.id
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.ingr_id
, to_number( null )
, to_number( null )
, decl.dewy_id
, decl.verz_id
, dewy.ramo_id
, dewy.code
, ramo.code
, kafd.code
, ongr.code
, kgc_brie_00.machtiging_reeds_geprint( decl.decl_id )
, kgc_decl_00.machtigingsindicatie( decl.decl_id )
, NULL
, decl.status decl_status
from kgc_familie_relaties fare
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_rapport_modules ramo
, kgc_declaratiewijzen dewy
, kgc_verzekeraars verz
, kgc_relaties rela
, kgc_personen pers
, kgc_monsters mons
, kgc_declaraties decl
where decl.kafd_id = kafd.kafd_id
and decl.ongr_id = ongr.ongr_id
and decl.pers_id = pers.pers_id
and decl.entiteit = 'MONS'
and decl.id = mons.mons_id
and mons.rela_id = rela.rela_id
and decl.verz_id = verz.verz_id (+)
and decl.fare_id = fare.fare_id (+)
and decl.dewy_id = dewy.dewy_id
and dewy.ramo_id = ramo.ramo_id
and decl.declareren = 'J'
and nvl( decl.status, 'H' ) in ('H','V')
and kgc_decl_00.machtigingsbrief_nodig( decl.decl_id ) = 'J'
union all SELECT decl.decl_id
, decl.nota_adres
, nvl( decl.betreft_tekst
     , decode( kafd.code
             , 'CYTO', decode( substr( ongr.code, 1, 3 )
                             , 'PRE', 'Prenatale cytogenetische diagnostiek'
                             , 'POS', 'Postnatale cytogenetische diagnostiek'
                             , 'TUM', 'Postnatale cytogenetische diagnostiek'
                             , dewy.betreft_tekst
                             )
             , 'OBS',  decode( onde.onderzoekswijze
                             , 'U1', 'Ultrageluidonderzoek type I'
                             , 'U2', 'Ultrageluidonderzoek type II'
                             , dewy.betreft_tekst
                             )
             , dewy.betreft_tekst
             )
     ) betreft_tekst
, kgc_decl_00.decl_identificatie( decl.decl_id )
, pers.aanspreken
, pers.geslacht
, pers.geboortedatum
, kgc_adres_00.persoon( pers.pers_id, 'N' )
, pers.zisnr
, verz.zis_code
, nvl( verz.naam, 'Onbekend' ) verzekeraar
, decl.verzekeringsnr || ' ('||decl.verzekeringswijze||')' verzekeringsnr
--, onde.datum_binnen aanvraagdatum
, decl.datum aanvraagdatum
, onde.datum_autorisatie
, onde.afgerond
, rela.code
, kgc_adres_00.relatie( rela.rela_id,'J') verwijzer
, nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
, decl.toelichting
, mede.naam autorisator
, decl.entiteit
, decl.id
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.ingr_id
, onde.onde_id
, onde.taal_id
, decl.dewy_id
, decl.verz_id
, dewy.ramo_id
, dewy.code
, ramo.code
, kafd.code
, ongr.code
, kgc_brie_00.machtiging_reeds_geprint( decl.decl_id )
, nvl( kgc_decl_00.machtigingsindicatie( decl.decl_id )
     , kgc_indi_00.machtigingsindicatie( onde.onde_id )
     ) indicatie
, onde.onderzoeknr
, decl.status decl_status
from kgc_familie_relaties fare
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_rapport_modules ramo
, kgc_declaratiewijzen dewy
, kgc_medewerkers mede
, kgc_verzekeraars verz
, kgc_relaties rela
, kgc_personen pers
, kgc_onderzoeken onde
, bas_metingen meti
, kgc_declaraties decl
where decl.kafd_id = kafd.kafd_id
and decl.ongr_id = ongr.ongr_id
and decl.pers_id = pers.pers_id
and decl.entiteit = 'METI'
and decl.id = meti.meti_id
and meti.onde_id = onde.onde_id
and rela.rela_id = nvl( onde.rela_id_oorsprong, onde.rela_id )
and onde.mede_id_autorisator = mede.mede_id (+)
and decl.verz_id = verz.verz_id (+)
and decl.fare_id = fare.fare_id (+)
and decl.dewy_id = dewy.dewy_id
and dewy.ramo_id = ramo.ramo_id
and decl.declareren = 'J'
and nvl( decl.status, 'H' ) in ('H','V')
and meti.afgerond = 'J'
and kgc_decl_00.machtigingsbrief_nodig( decl.decl_id ) = 'J'
union all SELECT decl.decl_id
, decl.nota_adres
, nvl( decl.betreft_tekst
     , decode( kafd.code
             , 'CYTO', decode( substr( ongr.code, 1, 3 )
                             , 'PRE', 'Prenatale cytogenetische diagnostiek'
                             , 'POS', 'Postnatale cytogenetische diagnostiek'
                             , 'TUM', 'Postnatale cytogenetische diagnostiek'
                             , dewy.betreft_tekst
                             )
             , 'OBS',  decode( onde.onderzoekswijze
                             , 'U1', 'Ultrageluidonderzoek type I'
                             , 'U2', 'Ultrageluidonderzoek type II'
                             , dewy.betreft_tekst
                             )
             , dewy.betreft_tekst
             )
     ) betreft_tekst
, kgc_decl_00.decl_identificatie( decl.decl_id )
, pers.aanspreken
, pers.geslacht
, pers.geboortedatum
, kgc_adres_00.persoon( pers.pers_id, 'N' )
, pers.zisnr
, verz.zis_code
, nvl( verz.naam, 'Onbekend' ) verzekeraar
, decl.verzekeringsnr || ' ('||decl.verzekeringswijze||')' verzekeringsnr
, gesp.datum aanvraagdatum
, onde.datum_autorisatie
, onde.afgerond
, rela.code
, kgc_adres_00.relatie( rela.rela_id,'J') verwijzer
, nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
, decl.toelichting
, mede.naam autorisator
, decl.entiteit
, decl.id
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.ingr_id
, onde.onde_id
, onde.taal_id
, decl.dewy_id
, decl.verz_id
, dewy.ramo_id
, dewy.code
, ramo.code
, kafd.code
, ongr.code
, kgc_brie_00.machtiging_reeds_geprint( decl.decl_id )
, onre.omschrijving ||' ('
                    || nvl( kgc_decl_00.machtigingsindicatie( decl.decl_id )
                          , kgc_indi_00.machtigingsindicatie( onde.onde_id )
                          )
                    || ')'
, onde.onderzoeknr
, decl.status decl_status
from kgc_familie_relaties fare
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_onderzoeksredenen onre
, kgc_rapport_modules ramo
, kgc_declaratiewijzen dewy
, kgc_medewerkers mede
, kgc_verzekeraars verz
, kgc_relaties rela
, kgc_personen pers
, kgc_onderzoeken onde
, kgc_gesprekken gesp
, kgc_declaraties decl
where decl.kafd_id = kafd.kafd_id
and decl.ongr_id = ongr.ongr_id
and decl.pers_id = pers.pers_id
and decl.entiteit = 'GESP'
and decl.id = gesp.gesp_id
and gesp.onde_id = onde.onde_id
and rela.rela_id = nvl( onde.rela_id_oorsprong, onde.rela_id )
and onde.onre_id = onre.onre_id (+)
and onde.mede_id_autorisator = mede.mede_id (+)
and decl.verz_id = verz.verz_id (+)
and decl.fare_id = fare.fare_id (+)
and decl.dewy_id = dewy.dewy_id
and dewy.ramo_id = ramo.ramo_id
and decl.declareren = 'J'
and nvl( decl.status, 'H' ) in ('H','V')
and kgc_decl_00.machtigingsbrief_nodig( decl.decl_id ) = 'J'
 ;

/
QUIT
