CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_RAPPORT_GROEPEN_VW" ("RAGR_ID", "CODE", "OMSCHRIJVING", "VOLGORDE") AS
  SELECT ragr.ragr_id
,      ragr.code
,      ragr.omschrijving
,      ragr.volgorde
from   kgc_rapport_groepen ragr
,      kgc_rapportgroep_medewerkers rgme
where rgme.ragr_id = ragr.ragr_id
and    rgme.mede_id = kgc_mede_00.medewerker_id
and   ragr.vervallen = 'N'
union
select ragr.ragr_id
,      ragr.code
,      ragr.omschrijving
,      ragr.volgorde
from   kgc_rapport_groepen ragr
where  ragr.vervallen = 'N'
and  not exists
       ( select null
         from   kgc_rapportgroep_medewerkers rgme
         where  rgme.ragr_id = ragr.ragr_id
       )
union -- de eigenaar mag alles
select ragr.ragr_id
,      ragr.code
,      ragr.omschrijving
,      ragr.volgorde
from   kgc_rapport_groepen ragr
where  user in ( 'KGCN', 'HELIX' )
 ;

/
QUIT
