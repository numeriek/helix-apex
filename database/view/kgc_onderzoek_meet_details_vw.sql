CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERZOEK_MEET_DETAILS_VW" ("KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "FRACTIENUMMER", "FAMILIENUMMER", "INDICATIE", "STOF_CODE", "ONDE_AFGEROND", "METI_AFGEROND", "MEET_AFGEROND", "VOLGORDE", "WAARDE", "FAMI_ID", "ONDE_ID", "PERS_ID", "STOF_ID", "MEET_ID", "MDET_ID") AS
  SELECT kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr
,      frac.fractienummer
,      fami.familienummer
,      kgc_indi_00.onderzoeksreden(onde.onde_id) indicatie
,      stof.code stof_code
,      onde.afgerond onde_afgerond
,      meti.afgerond meti_afgerond
,      meet.afgerond meet_afgerond
,      mdet.volgorde
,      mdet.waarde
,      fami.fami_id
,      onde.onde_id
,      onde.pers_id
,      stof.stof_id
,      meet.meet_id
,      mdet.mdet_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_stoftesten stof
,      kgc_families fami
,      kgc_familie_onderzoeken faon
,      kgc_faon_betrokkenen fobe
,      kgc_onderzoeken onde
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_meetwaarde_details mdet
where meet.meet_id = mdet.meet_id
and    meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.frac_id = frac.frac_id
and    meti.onde_id = onde.onde_id (+)
and    onde.onde_id = fobe.onde_id (+)
and    fobe.faon_id = faon.faon_id (+)
and    faon.fami_id = fami.fami_id (+)
and    onde.ongr_id = ongr.ongr_id
and    ongr.kafd_id = kafd.kafd_id
union all
select kafd.code
,      ongr.code
,      null
,      stan.standaardfractienummer
,      null
,      'Controlefractie'
,      stof.code
,      'N'
,      meti.afgerond
,      meet.afgerond
,      mdet.volgorde
,      mdet.waarde
,      to_number(null)
,      to_number(null)
,      to_number(null)
,      stof.stof_id
,      meet.meet_id
,      mdet.mdet_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_stoftesten stof
,      bas_standaardfracties stan
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_meetwaarde_details mdet
where  meet.meet_id = mdet.meet_id
and    meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.stan_id = stan.stan_id
and    stan.ongr_id = ongr.ongr_id
and    ongr.kafd_id = kafd.kafd_id
 ;

/
QUIT
