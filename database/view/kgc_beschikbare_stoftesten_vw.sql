CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BESCHIKBARE_STOFTESTEN_VW" ("ENTITEIT", "STGR_ID", "STOF_ID", "KAFD_ID", "CODE", "OMSCHRIJVING", "ONMO_ID", "MONS_ID", "ONDE_ID") AS
  SELECT distinct "ENTITEIT","STGR_ID","STOF_ID","KAFD_ID","CODE","OMSCHRIJVING","ONMO_ID","MONS_ID","ONDE_ID"
from (
select 'STGR' entiteit -- stoftestgroepen
,      stgr.stgr_id
,      to_number(null) stof_id
,      stgr.kafd_id
,      stgr.code
,      stgr.omschrijving
,      onmo.onmo_id
,      onmo.mons_id
,      onmo.onde_id
from   kgc_stoftestgroepen stgr
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
where  stgr.vervallen = 'N'
and    onmo.mons_id = mons.mons_id
and    stgr.kafd_id = mons.kafd_id
and exists
(   SELECT null
    FROM   kgc_stoftestgroep_gebruik stgg
    ,      kgc_protocol_stoftesten prst
    WHERE  stgg.stgr_id = stgr.stgr_id
    AND    stgg.stgr_id = prst.stgr_id
    AND    stgg.vervallen = 'N'
    AND  ( stgg.mate_id IS NULL
        OR mons.mate_id = stgg.mate_id
         )
    AND  ( stgg.frty_id IS NULL
        OR exists
           ( SELECT null
             FROM   bas_fracties frac
             WHERE  frac.mons_id = onmo.mons_id
			 and    frac.frty_id = stgg.frty_id
           )
         )
    AND  ( stgg.indi_id IS NULL
        OR exists
           ( SELECT null
             FROM   kgc_onderzoek_indicaties onin
             WHERE  onin.onde_id = onmo.onde_id
			 and    onin.indi_id = stgg.indi_id
             UNION ALL
             SELECT null
             FROM   kgc_monster_indicaties moin
             WHERE  moin.mons_id = onmo.mons_id
			 and    moin.indi_id = stgg.indi_id
           )
         )
)
union all
select 'STOF' -- stoftesten
,      to_number(null) stgr_id
,      prst.stof_id
,      stof.kafd_id
,      stof.code
,      stof.omschrijving
,      onmo.onmo_id
,      onmo.mons_id
,      onmo.onde_id
from   kgc_protocol_stoftesten prst
,      kgc_stoftesten stof
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
where  onmo.mons_id = mons.mons_id
and    stof.kafd_id = mons.kafd_id
and    prst.stof_id = stof.stof_id
and exists
(   SELECT stgg.stgr_id
    FROM   kgc_stoftestgroep_gebruik stgg
    WHERE  stgg.stgr_id = prst.stgr_id
    AND    stgg.vervallen = 'N'
    AND  ( stgg.mate_id IS NULL
        OR mons.mate_id = stgg.mate_id
         )
    AND  ( stgg.frty_id IS NULL
        OR exists
           ( SELECT null
             FROM   bas_fracties frac
             WHERE  frac.mons_id = onmo.mons_id
			 AND    frac.frty_id = stgg.frty_id
           )
         )
    AND  ( stgg.indi_id IS NULL
        OR exists
           ( SELECT onin.indi_id
             FROM   kgc_onderzoek_indicaties onin
             WHERE  onin.onde_id = onmo.onde_id
			 AND    onin.indi_id = stgg.indi_id
             UNION ALL
             SELECT moin.indi_id
             FROM   kgc_monster_indicaties moin
             WHERE  moin.mons_id = onmo.mons_id
			 AND    moin.indi_id = stgg.indi_id
           )
         )
)
union all -- werkelijk aangemelde protocollen
select  'STGR' entiteit
,      meti.stgr_id
,      to_number(null) stof_id
,      stgr.kafd_id
,      stgr.code
,      stgr.omschrijving
,      meti.onmo_id
,      onmo.mons_id
,      meti.onde_id
from   kgc_stoftestgroepen stgr
,      bas_metingen meti
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
where  meti.onmo_id = onmo.onmo_id
and    onmo.mons_id = mons.mons_id
and    stgr.kafd_id = mons.kafd_id
and    meti.stgr_id = stgr.stgr_id
union all -- werkelijk aangemelde stoftesten
select 'STOF' -- stoftesten
,      to_number(null) stgr_id
,      meet.stof_id
,      stof.kafd_id
,      stof.code
,      stof.omschrijving
,      meti.onmo_id
,      onmo.mons_id
,      meti.onde_id
from   kgc_stoftesten stof
,      bas_metingen meti
,      bas_meetwaarden meet
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
where  meet.meti_id = meti.meti_id
and    meti.onmo_id = onmo.onmo_id
and    onmo.mons_id = mons.mons_id
and    meet.stof_id = stof.stof_id
and    stof.kafd_id = mons.kafd_id
);

/
QUIT
