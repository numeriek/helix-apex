CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BASMETI53_VW" ("MONSTERNUMMER", "WERKLIJST_NUMMER", "STGR_CODE", "STGR_OMSCHRIJVING", "ONDERZOEKNR", "ONDE_SPOED", "MATE_CODE", "MONS_ID", "MONS_DATUM_AFNAME", "MONS_VOLUME", "MONS_COMMENTAAR", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "KAFD_ID", "KAFD_CODE", "ONGR_CODE") AS
  SELECT mons.monsternummer
,      wlst.werklijst_nummer
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      onde.onderzoeknr
,      decode( meti.prioriteit
             , 'J', 'J'
             , onde.spoed
             ) onde_spoed
,      mate.code mate_code
,      mons.mons_id
,      mons.datum_afname mons_datum_afname
,      decode( mons.hoeveelheid_monster
             , null, null
             , mons.hoeveelheid_monster
               || decode( mate.eenheid
                        , null, null
                        , ' '||mate.eenheid
                        )
             ) mons_volume
,      mons.commentaar mons_commentaar
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      kafd.kafd_id
,      kafd.code kafd_code
,      ongr.code ongr_code
from   kgc_werklijsten       wlst
,      kgc_werklijst_items   wlit
,      bas_meetwaarden       meet
,      bas_metingen          meti
,      kgc_stoftestgroepen   stgr
,      bas_fracties          frac
,      kgc_monsters          mons
,      kgc_onderzoeken       onde
,      kgc_materialen        mate
,      kgc_personen          pers
,      kgc_kgc_afdelingen    kafd
,      kgc_onderzoeksgroepen ongr
where  wlit.wlst_id = wlst.wlst_id -- (+)
and    meet.meet_id = wlit.meet_id -- (+)
and    meti.meti_id = meet.meti_id -- (+)
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.frac_id = frac.frac_id -- (+)
and    frac.mons_id = mons.mons_id -- (+)
and    meti.onde_id = onde.onde_id
and    mons.mate_id = mate.mate_id
and    pers.pers_id = onde.pers_id
and    kafd.kafd_id = mons.kafd_id
and    ongr.ongr_id = mons.ongr_id
 ;

/
QUIT
