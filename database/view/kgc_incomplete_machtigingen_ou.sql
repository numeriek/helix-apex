CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_INCOMPLETE_MACHTIGINGEN_OU" ("KAFD_CODE", "ONDERZOEKNR", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "VERZ_NAAM", "PERS_VERZEKERINGSNR", "INGR_CODE", "ONDE_ID", "ONDE_AFGEROND", "ONDE_DATUM_BINNEN", "ONDE_DATUM_AUTORISATIE") AS
  SELECT
  kafd.code kafd_code ,
  onde.onderzoeknr ,
  pers.aanspreken pers_aanspreken ,
  pers.geboortedatum pers_geboortedatum ,
  pers.geslacht pers_geslacht ,
  nvl(verz_o.naam,
  verz_p.naam) verz_naam ,
  pers.verzekeringsnr pers_verzekeringsnr ,
  kgc_indi_00.ingr_code( onde.onde_id ) ingr_code ,
  onde.onde_id ,
  onde.afgerond onde_afgerond ,
  onde.datum_binnen onde_datum_binnen ,
  onde.datum_autorisatie onde_datum_autorisatie
FROM
  kgc_kgc_afdelingen kafd ,
  kgc_declaratiewijzen dewy ,
  kgc_verzekeraars verz_p ,
  kgc_verzekeraars verz_o ,
  kgc_personen pers ,
  kgc_onderzoeken onde
WHERE
  onde.kafd_id = kafd.kafd_id and
  onde.pers_id = pers.pers_id and
  onde.verz_id = verz_o.verz_id (+) and
  pers.verz_id = verz_p.verz_id (+) and
  onde.dewy_id = dewy.dewy_id and
  dewy.code = 'BI' and
  onde.declareren = 'J' and
  onde.onderzoekstype <> 'R' and
  kgc_brie_00.machtiging_reeds_geprint( onde.onde_id ) = 'N' and
  onde.datum_binnen >= add_months( sysdate, -2*12 ) and
  ( ( pers.verz_id is null and  onde.verz_id is null )
   or ( pers.verzekeringsnr is null and  pers.verz_id is not null )
   or ( kgc_indi_00.ingr_code( onde.onde_id ) is null ) )
 ;

/
QUIT
