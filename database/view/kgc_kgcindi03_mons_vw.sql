CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCINDI03_MONS_VW" ("ONDE_ID", "MONS_ID", "MONSTERNUMMER", "MEDISCHE_INDICATIE", "MEDICATIE", "VOEDING", "MONS_COMMENTAAR", "MATE_CODE", "MATE_OMSCHRIJVING") AS
  SELECT onmo.onde_id
,      mons.mons_id
,      mons.monsternummer
,      mons.medische_indicatie
,      mons.medicatie
,      mons.voeding
,      mons.commentaar mons_commentaar
,      mate.code
,      mate.omschrijving
FROM   kgc_materialen          mate
,      kgc_monsters            mons
,      kgc_onderzoek_monsters  onmo
WHERE  onmo.mons_id            = mons.mons_id
AND    mons.mate_id            = mate.mate_id
 ;

/
QUIT
