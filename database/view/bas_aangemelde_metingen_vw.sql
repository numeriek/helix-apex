CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_AANGEMELDE_METINGEN_VW" ("MEET_ID", "METI_ID", "STOF_ID", "MEDE_ID", "FRAC_ID", "STGR_ID", "ONMO_ID", "ONDE_ID", "MONS_ID", "FRACTIENUMMER", "DATUM_AANMELDING", "SNELHEID", "STGR_CODE", "STGR_OMSCHRIJVING", "STOF_CODE", "STOF_OMSCHRIJVING", "AFGEROND", "MEET_REKEN", "MEETWAARDE", "PRIORITEIT", "GEPLANDE_EINDDATUM") AS
  SELECT meet.meet_id
,      meet.meti_id
,      meet.stof_id
,      meet.mede_id
,      meti.frac_id
,      meti.stgr_id
,      meti.onmo_id
,      meti.onde_id
,      frac.mons_id
,      frac.fractienummer
,      meti.datum_aanmelding
,      meti.snelheid
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      decode( meti.afgerond
                     , 'J', 'J'
                     , meet.afgerond
                     ) afgerond
,      meet.meet_reken
,      meet.tonen_als
,      meti.prioriteit spoed
,      meti.geplande_einddatum
from   kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
where meet.meti_id = meti.meti_id
and    meti.frac_id = frac.frac_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meet.stof_id = stof.stof_id
 ;

/
QUIT
