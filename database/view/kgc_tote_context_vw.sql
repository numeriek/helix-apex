CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_TOTE_CONTEXT_VW" ("TOTE_ID", "KAFD_ID", "ONDE_ID", "CODE", "OMSCHRIJVING") AS
  SELECT tote.tote_id
, tote.kafd_id
, onde.onde_id
, tote.code
, tote.omschrijving
FROM kgc_toelichting_teksten tote
,     kgc_teksten_context teco
,     kgc_indicatie_teksten indi
,     kgc_onderzoek_indicaties onin
,     kgc_onderzoeken onde
WHERE tote.tote_id = teco.tote_id
AND   teco.indi_id = indi.indi_id
AND   indi.indi_id = onin.indi_id
AND   onde.onde_id = onin.onde_id
AND   tote.vervallen = 'N'
UNION
SELECT tote.tote_id
, tote.kafd_id
, onde.onde_id
, tote.code
, tote.omschrijving
FROM  kgc_toelichting_teksten tote
,     kgc_onderzoeken onde
WHERE tote.kafd_id = onde.kafd_id
AND   tote.vervallen = 'N'
AND NOT EXISTS
  ( SELECT null
    FROM  kgc_toelichting_teksten tote2
    ,     kgc_teksten_context teco2
    ,     kgc_indicatie_teksten indi2
    ,     kgc_onderzoek_indicaties onin2
    ,     kgc_onderzoeken onde2
    WHERE tote2.tote_id = teco2.tote_id
    AND   teco2.indi_id = indi2.indi_id
    AND   indi2.indi_id = onin2.indi_id
    AND   onde2.onde_id = onin2.onde_id
    AND   tote2.vervallen = 'N'
    AND   onde2.onde_id = onde.onde_id
  )
 ;

/
QUIT
