CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_NOTITIES_VW" ("NOTI_ID", "ENTITEIT", "ID", "ENTITEIT_OMSCHRIJVING", "OMSCHRIJVING", "CODE", "COMMENTAAR", "GEREED", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "PERS_ID", "AFDELING", "ONGR_CODE") AS
  select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, pers.pers_id
, to_char('') afdeling
, to_char('') ongr_code
from kgc_notities noti
, kgc_personen pers
where noti.entiteit = 'PERS'
and noti.id = pers.pers_id
union all
-- MONS
select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, mons.pers_id
, kafd.code afdeling
, ongr.code ongr_code
from kgc_notities noti
, kgc_monsters mons
, kgc_onderzoeksgroepen ongr
, kgc_kgc_afdelingen kafd
where noti.entiteit = 'MONS'
and noti.id = mons.mons_id
and mons.ongr_id = ongr.ongr_id
and mons.kafd_id = kafd.kafd_id
union all
-- ONDE
select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, onde.pers_id
, kafd.code afdeling
, ongr.code ongr_code
from kgc_notities noti
, kgc_onderzoeken onde
, kgc_onderzoeksgroepen ongr
, kgc_kgc_afdelingen kafd
where noti.entiteit = 'ONDE'
and noti.id = onde.onde_id
and onde.ongr_id = ongr.ongr_id
and onde.kafd_id = kafd.kafd_id
union all
-- FRAC
select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, mons.pers_id
, kafd.code afdeling
, ongr.code ongr_code
from kgc_notities noti
, bas_fracties frac
, kgc_monsters mons
, kgc_onderzoeksgroepen ongr
, kgc_kgc_afdelingen kafd
where noti.entiteit = 'FRAC'
and noti.id = frac.frac_id
and frac.mons_id = mons.mons_id
and mons.ongr_id = ongr.ongr_id
and mons.kafd_id = kafd.kafd_id
union all
-- MEET
select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, onde.pers_id
, kafd.code afdeling
, ongr.code ongr_code
from kgc_notities noti
, bas_meetwaarden meet
, bas_metingen meti
, kgc_onderzoeken onde
, kgc_onderzoeksgroepen ongr
, kgc_kgc_afdelingen kafd
where noti.entiteit = 'MEET'
and noti.id = meet.meet_id
and meet.meti_id = meti.meti_id
and meti.onde_id = onde.onde_id
and onde.ongr_id = ongr.ongr_id
and onde.kafd_id = kafd.kafd_id
union all
-- UITS
select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, onde.pers_id
, kafd.code afdeling
, ongr.code ongr_code
from kgc_notities noti
, kgc_uitslagen uits
, kgc_onderzoeken onde
, kgc_onderzoeksgroepen ongr
, kgc_kgc_afdelingen kafd
where noti.entiteit = 'UITS'
and noti.id = uits.uits_id
and uits.onde_id = onde.onde_id
and onde.ongr_id = ongr.ongr_id
and onde.kafd_id = kafd.kafd_id
union all
-- BRIE
select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, brie.pers_id
, kafd.code afdeling
, to_char('') ongr_code
from kgc_notities noti
, kgc_brieven brie
, kgc_kgc_afdelingen kafd
where noti.entiteit = 'BRIE'
and noti.id = brie.brie_id
and brie.kafd_id = kafd.kafd_id
union all
-- FAMI
select noti.noti_id
, noti.entiteit
, noti.id
, kgc_noti_00.basis_omschrijving( noti.entiteit,noti.id) entiteit_omschrijving
, noti.omschrijving
, noti.code
, noti.commentaar
, noti.gereed
, noti.created_by
, noti.creation_date
, noti.last_updated_by
, noti.last_update_date
, fale.pers_id pers_id
, to_char('') afdeling
, to_char('') ongr_code
from kgc_notities noti
, kgc_families fami
, kgc_familie_leden fale
where noti.entiteit = 'FAMI'
and noti.id = fami.fami_id
and fami.fami_id = fale.fami_id
;

/
QUIT
