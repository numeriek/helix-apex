CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCWLST60_METI_VW" ("ONDERZOEKNUMMER", "ONDERZOEKSTYPE", "MONSTERNUMMER", "AANSPREKEN", "GEBOORTEDATUM", "METI_SPOED", "MEETWAARDE_REFERENTIE_STOFTEST", "HOEVEELHEID", "WLST_ID", "ONDE_ID", "MONS_ID", "METI_ID", "PERS_ID", "STGR_ID", "WLIT_VOLGNUMMER") AS
  SELECT wlst.onderzoeknr
      ,wlst.onderzoekstype
      ,wlst.monsternummer
      ,wlst.aanspreken
      ,wlst.geboortedatum
      ,decode(wlst.meti_spoed
             ,'J','!'
             ,null) meti_spoed
      ,kgc_mons_00.meetwaarde_referentie_stoftest(wlst.mons_id
                                                 ,wlst.onde_id)
      ,wlst.hoeveelheid
      ,wlst.wlst_id
      ,wlst.onde_id
      ,wlst.mons_id
      ,wlst.meti_id
      ,wlst.pers_id
      ,wlst.stgr_id
      ,wlst.wlit_volgnummer
  from kgc_kgcwlst10_meti_vw wlst
 ;

/
QUIT
