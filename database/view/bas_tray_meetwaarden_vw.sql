CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_TRAY_MEETWAARDEN_VW" ("MEET_ID", "METI_ID", "STOF_ID", "STGR_ID", "FRAC_ID", "STAN_ID", "MONS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "TECH_ID", "DATUM_AANMELDING", "PRIORITEIT", "STOF_CODE", "STOF_OMSCHRIJVING", "FRACTIENUMMER", "FRAC_STATUS", "FRAC_CONTROLE", "MONSTERNUMMER", "ONDERZOEKNR", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "SPOED", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "MEET_REKEN", "VOORTEST_OK", "VOORTEST_OMSCHRIJVING") AS
  SELECT meet_id
, meti_id
, stof_id
, stgr_id
, frac_id
, stan_id
, mons_id
, kafd_id
, ongr_id
, onde_id
, tech_id
, datum_aanmelding
, prioriteit
, stof_code
, stof_omschrijving
, fractienummer
, frac_status
, frac_controle
, monsternummer
, onderzoeknr
, onderzoekstype
, onderzoekswijze
, spoed
, datum_binnen
, geplande_einddatum
, meet_reken
, voortest_ok
, voortest_omschrijving
from bas_tray_meetwaarden_1_vw
union all
SELECT
meet_id
, meti_id
, stof_id
, stgr_id
, frac_id
, stan_id
, mons_id
, kafd_id
, ongr_id
, onde_id
, tech_id
, datum_aanmelding
, prioriteit
, stof_code
, stof_omschrijving
, fractienummer
, frac_status
, frac_controle
, monsternummer
, onderzoeknr
, onderzoekstype
, onderzoekswijze
, spoed
, datum_binnen
, geplande_einddatum
, meet_reken
, voortest_ok
, voortest_omschrijving
from bas_tray_meetwaarden_2_vw;

/
QUIT
