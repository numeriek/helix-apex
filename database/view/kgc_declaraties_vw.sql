CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_DECLARATIES_VW" ("DECL_ID", "ENTITEIT", "ID", "NOTA_ADRES", "IDENTIFICATIE", "VERZEKERINGSNR", "VERZEKERINGSWIJZE", "RELATIE", "KAFD_ID", "ONGR_ID", "PERS_ID", "DEWY_ID", "INGR_ID", "VERZ_ID", "FARE_ID", "ONDE_ID", "ONBE_ID", "MONS_ID", "METI_ID", "GESP_ID", "DATUM_AANVRAAG", "ONDERZOEKSTYPE", "RELA_ID", "AFGEROND", "MEDE_ID", "DATUM_AUTORISATIE") AS
  SELECT -- onderzoeken
       decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.nota_adres
,      kgc_decl_00.decl_identificatie( decl.decl_id ) identificatie
,      decl.verzekeringsnr
,      decl.verzekeringswijze
,      nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
,      decl.kafd_id
,      decl.ongr_id
,      decl.pers_id
,      decl.dewy_id
,      decl.ingr_id
,      decl.verz_id
,      decl.fare_id
,      id onde_id
,      to_number(null) onbe_id
,      to_number(null) mons_id
,      to_number(null) meti_id
,      to_number(null) gesp_id
,      onde.datum_binnen datum_aanvraag
,      onde.onderzoekstype
,      onde.rela_id
,      onde.afgerond
,      onde.mede_id_autorisator mede_id
,      onde.datum_autorisatie datum_autorisatie
from kgc_familie_relaties fare
,        kgc_onderzoeken onde
,         kgc_declaraties decl
where decl.entiteit = 'ONDE'
and    decl.id = onde.onde_id
and    decl.fare_id = fare.fare_id (+)
and    decl.declareren = 'J'
and    decl.status is null
union all -- betrokkenen
SELECT decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.nota_adres
,      kgc_decl_00.decl_identificatie( decl.decl_id )
,      decl.verzekeringsnr
,      decl.verzekeringswijze
,      nvl( fare.omschrijving
             ,  decode ( decl.pers_id
                               , onde.pers_id, 'Verzekerde is index persoon'
                               , 'Verzekerde is betrokkene bij het onderzoek'
                               )
            ) relatie
,      decl.kafd_id
,      decl.ongr_id
,      decl.pers_id
,      decl.dewy_id
,      decl.ingr_id
,      decl.verz_id
,      decl.fare_id
,      onbe.onde_id
,      id onbe_id
,      to_number(null) mons_id
,      to_number(null) meti_id
,      to_number(null) gesp_id
,      onde.datum_binnen datum_aanvraag
,      onde.onderzoekstype
,      onde.rela_id
,      onde.afgerond
,      onde.mede_id_autorisator
,      onde.datum_autorisatie datum_autorisatie
from  kgc_familie_relaties fare
,     kgc_onderzoeken onde
,     kgc_onderzoek_betrokkenen onbe
,     kgc_declaraties decl
where decl.entiteit = 'ONBE'
and    decl.id = onbe.onbe_id
and   onbe.onde_id = onde.onde_id
and  decl.fare_id = fare.fare_id (+)
and   onde.declareren = 'J'
and    decl.declareren = 'J'
and    decl.status is null
union all -- monsterafnames
SELECT decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.nota_adres
,      kgc_decl_00.decl_identificatie( decl.decl_id )
,      decl.verzekeringsnr
,      decl.verzekeringswijze
,      nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
,      decl.kafd_id
,      decl.ongr_id
,      decl.pers_id
,      decl.dewy_id
,      decl.ingr_id
,      decl.verz_id
,      decl.fare_id
,      to_number(null)
,      to_number(null)
,      id mons_id
,      to_number(null) meti_id
,      to_number(null) gesp_id
,      mons.datum_aanmelding
,      'D' -- onderzoekstype
,      mons.rela_id
,      'J'
,      to_number(null) -- mede_id_autorisator
,      to_date(null) --datum_autorisatie
from kgc_familie_relaties fare
,         kgc_monsters mons
,         kgc_declaraties decl
where decl.entiteit = 'MONS'
and decl.id = mons.mons_id
and    decl.fare_id = fare.fare_id (+)
and    decl.declareren = 'J'
and    decl.status is null
union all -- metingen (deelonderzoeken)
SELECT decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.nota_adres
,      kgc_decl_00.decl_identificatie( decl.decl_id )
,      decl.verzekeringsnr
,      decl.verzekeringswijze
,      nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
,      decl.kafd_id
,      decl.ongr_id
,      decl.pers_id
,      decl.dewy_id
,      decl.ingr_id
,      decl.verz_id
,      decl.fare_id
,      meti.onde_id
,      to_number(null)
,      to_number(null)
,      id meti_id
,      to_number(null) gesp_id
,      onde.datum_binnen datum_aanvraag
,      onde.onderzoekstype
,      onde.rela_id
,      onde.afgerond
,      onde.mede_id_autorisator
,      onde.datum_autorisatie datum_autorisatie
from kgc_familie_relaties fare
,      kgc_onderzoeken onde
,      bas_metingen meti
,      kgc_declaraties decl
where decl.entiteit = 'METI'
and    decl.id = meti.meti_id
and    meti.onde_id = onde.onde_id
and    decl.fare_id = fare.fare_id (+)
and    decl.declareren = 'J'
and    decl.status is null
union all -- gesprekken (counseling)
SELECT decl.decl_id
,      decl.entiteit
,      decl.id
,      decl.nota_adres
,      kgc_decl_00.decl_identificatie( decl.decl_id )
,      decl.verzekeringsnr
,      decl.verzekeringswijze
,      nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
,      decl.kafd_id
,      decl.ongr_id
,      decl.pers_id
,      decl.dewy_id
,      decl.ingr_id
,      decl.verz_id
,      decl.fare_id
,      gesp.onde_id
,      to_number(null)
,      to_number(null)
,      to_number(null)
,      gesp.gesp_id
,      onde.datum_binnen datum_aanvraag
,      onde.onderzoekstype
,      onde.rela_id
,      onde.afgerond
,      onde.mede_id_autorisator
,      onde.datum_autorisatie datum_autorisatie
from kgc_familie_relaties fare
,      kgc_onderzoeken onde
,      kgc_gesprekken gesp
,      kgc_declaraties decl
where decl.entiteit = 'GESP'
and    decl.id = gesp.gesp_id
and    gesp.onde_id = onde.onde_id
and    decl.fare_id = fare.fare_id (+)
and    decl.declareren = 'J'
and    decl.status is null
 ;

/
QUIT
