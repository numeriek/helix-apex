CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_PERSOON_STOFTESTEN_VW" ("PERS_ID", "PERS_ZISNR", "PERS_ACHTERNAAM", "PERS_AANSPREKEN", "PERS_ZOEKNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "KAFD_ID", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING") AS
  SELECT pers_id
,      zisnr pers_zisnr
,      achternaam pers_achternaam
,      aanspreken pers_aanspreken
,      zoeknaam pers_zoeknaam
,      geboortedatum pers_geboortedatum
,      geslacht pers_geslacht
,      to_number( null ) kafd_id
,      to_number( null ) stof_id
,      null stof_code
,      null stof_omschrijving
from   kgc_personen
union all
select  to_number(null) -- pers_id
,      null -- pers_zisnr
,      null -- pers_achternaam
,      null -- pers_aanspreken
,      null -- pers_zoeknaam
,      to_date(null) -- pers_geboortedatum
,      null -- pers_geslacht
,      kafd_id
,      stof_id
,      code stof_code
,      omschrijving  stof_omschrijving
from kgc_stoftesten;

/
QUIT
