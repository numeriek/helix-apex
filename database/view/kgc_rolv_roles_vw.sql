CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ROLV_ROLES_VW" ("GRANTED_ROLE") AS
  SELECT distinct rolp.granted_role
from   kgc_role_privs_vw rolp
,           kgc_systeem_parameters sypa
where rolp.grantee = sypa.standaard_waarde
and      sypa.code = 'APPLICATIE_EIGENAAR'
and      rolp.granted_role not in ( 'CONNECT', 'RESOURCE', 'DBA' )
 ;

/
QUIT
