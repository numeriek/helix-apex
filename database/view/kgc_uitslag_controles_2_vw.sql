CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAG_CONTROLES_2_VW" ("KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "BRTY_CODE", "KOPIE", "ONDE_ID", "ONDERZOEKNR", "ONDE_DATUM_BINNEN", "UW_REFERENTIE", "TAAL_ID", "RELA_ID", "RELA_AANSPREKEN", "GEADRESSEERDE", "PERS_ID", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_ZISNR", "UITS_ID", "KOHO_ID", "UITS_TEKST", "UITS_TOELICHTING", "DATUM_AUTORISATIE", "AUTORISATOR", "ONDERTEKENAAR2", "STGR_ID", "STGR_CODE", "STGR_OMSCHRIJVING", "BRTY_ID", "BSN_OPGEMAAKT") AS
  SELECT onde.kafd_id kafd_id
,      kafd.code kafd_code
,      onde.ongr_id ongr_id
,      ongr.code ongr_code
,      brty.code brty_code
,      'J' kopie
,      onde.onde_id onde_id
,      onde.onderzoeknr onderzoeknr
,      onde.datum_binnen onde_datum_binnen
,      onde.referentie uw_referentie
,      onde.taal_id taal_id
--,      onde.rela_id rela_id
,      koho.rela_id rela_id
,      rela.aanspreken rela_aanspreken
,      kgc_adres_00.relatie( koho.rela_id, 'J' ) geadresseerde
,      onde.pers_id pers_id
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      pers.zisnr pers_zisnr
,      uits.uits_id uits_id
,      koho.koho_id koho_id
,      uits.tekst uits_tekst
,      uits.toelichting uits_toelichting
,      onde.datum_autorisatie datum_autorisatie
,      nvl( mede.formele_naam, mede.naam ) autorisator
,      nvl( mede2.formele_naam, mede2.naam ) ondertekenaar2
,      meti.stgr_id stgr_id
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      uits.brty_id brty_id
,      kgc_pers_00.bsn_opgemaakt(pers.pers_id) bsn_opgemaakt
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_brieftypes brty
,      kgc_medewerkers mede
,      kgc_medewerkers mede2
,      kgc_stoftestgroepen stgr
,      bas_metingen meti
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_kopiehouders koho
,      kgc_uitslagen uits
where  uits.onde_id = onde.onde_id
and    uits.uits_id = koho.uits_id
and    onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.rela_id = rela.rela_id
and    onde.pers_id = pers.pers_id
and    uits.brty_id = brty.brty_id (+)
and    uits.mede_id = mede.mede_id (+)
and    uits.mede_id2 = mede2.mede_id (+)
and    onde.onde_id = meti.onde_id
and    meti.stgr_id = stgr.stgr_id;

/
QUIT
