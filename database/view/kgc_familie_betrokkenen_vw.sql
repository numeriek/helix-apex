CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FAMILIE_BETROKKENEN_VW" ("FAMI_NAAM", "FAMILIENUMMER", "FAMI_ARCHIEF", "FRACTIENUMMER", "FRACTIESTATUS", "MATE_CODE", "ONDERZOEKNR", "ONDE_AFGEROND", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "FAMILIEONDERZOEKNR", "KAFD_CODE", "FAMI_ID", "PERS_ID", "FRAC_ID") AS
  SELECT fami.naam fami_naam
,      fami.familienummer
,      fami.archief fami_archief
,      frac.fractienummer
,      frac.status fractiestatus
,      mate.code mate_code
,      onde.onderzoeknr
,      onde.afgerond onde_afgerond
,      pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      faon.familieonderzoeknr
,      kafd.code kafd_code
,      fami.fami_id
,      pers.pers_id
,      frac.frac_id
from   kgc_kgc_afdelingen kafd
,     kgc_materialen mate
,      kgc_families fami
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      kgc_familie_onderzoeken faon
,      kgc_faon_betrokkenen fobe
,      bas_metingen meti
where fobe.faon_id = faon.faon_id
and    faon.fami_id = fami.fami_id
and    fobe.onde_id = meti.onde_id
and    meti.onde_id = onde.onde_id
and    meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    mons.kafd_id = kafd.kafd_id
and    mons.pers_id = pers.pers_id
and    mons.mate_id = mate.mate_id
 ;

/
QUIT
