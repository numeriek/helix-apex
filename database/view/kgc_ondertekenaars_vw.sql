CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERTEKENAARS_VW" ("KAFD_ID", "MEDE_ID", "CODE", "NAAM", "VERVALLEN") AS
  SELECT meka.kafd_id
,      meka.mede_id
,      mede.code mede_code
,      nvl( mede.formele_naam, mede.naam ) mede_naam
,      mede.vervallen mede_vervallen
from   kgc_medewerkers mede
,      kgc_mede_kafd meka
where  meka.mede_id = mede.mede_id
and    ( meka.autorisator = 'J' or  meka.ondertekenaar = 'J' )
 ;

/
QUIT
