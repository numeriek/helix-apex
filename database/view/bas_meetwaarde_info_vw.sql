CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_MEETWAARDE_INFO_VW" ("MEET_ID", "METI_ID", "STOF_ID", "STGR_ID", "FRAC_ID", "STAN_ID", "MONS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "PERS_ID", "MEST_ID", "FRACTIENUMMER", "FRACTIESTATUS", "MONSTERNUMMER", "DATUM_AFNAME", "TIJD_AFNAME", "MATE_CODE", "MATE_OMSCHRIJVING", "PERSOON_INFO_MONS", "MONS_COMMENTAAR", "ONDERZOEKNR", "ONDERZOEKSTATUS", "ONDE_SPOED", "ONDE_AFGEROND", "PERSOON_INFO_ONDE", "INDICATIE", "STGR_CODE", "STGR_OMSCHRIJVING", "METI_DATUM_AANMELDING", "METI_PRIORITEIT", "METI_SNELHEID", "METI_AFGEROND", "METI_CONCLUSIE", "STOF_CODE", "STOF_OMSCHRIJVING", "DATUM_METING", "MEET_AFGEROND", "MEET_REKEN", "MWST_CODE", "MWST_OMSCHRIJVING", "MEDE_NAAM", "MEDE_NAAM_CONTROLE", "MEETWAARDE", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "EENHEID", "MEST_CODE", "MEST_OMSCHRIJVING", "COMMENTAAR", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE") AS
  SELECT -- normale fracties
       meet.meet_id
,      meet.meti_id
,      meet.stof_id
,      meti.stgr_id
,      meti.frac_id
,      meti.stan_id
,      frac.mons_id
,      mons.kafd_id
,      mons.ongr_id
,      meti.onde_id
,      mons.pers_id
,      meet.mest_id
,      frac.fractienummer
,      frac.status fractiestatus
,      mons.monsternummer
,      mons.datum_afname
,      mons.tijd_afname
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      kgc_pers_00.persoon_info( mons.mons_id, null )
persoon_info_mons
,      mons.commentaar mons_commentaar
,      onde.onderzoeknr
,      onde.status onderzoekstatus
,      onde.spoed onde_spoed
,      onde.afgerond onde_afgerond
,      kgc_pers_00.persoon_info( null, onde.onde_id )
persoon_info_onde
,      kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.datum_aanmelding meti_datum_aanmelding
,      meti.prioriteit meti_prioriteit
,      meti.snelheid meti_snelheid
,      meti.afgerond meti_afgerond
,      to_char(meti.conclusie) meti_conclusie -- Mantis 9355 to_char added by SKA 08-04-2016 release 8.11.0.3
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.datum_meting
,      meet.afgerond meet_afgerond
,      meet.meet_reken
,      mwst.code mwst_code
,      mwst.omschrijving mwst_omschrijving
,      mede.naam mede_naam
,      mede2.naam mede_naam_controle
,      meet.tonen_als meetwaarde
,      meet.normaalwaarde_ondergrens
,      meet.normaalwaarde_bovengrens
,      meet.meeteenheid eenheid
,      mest.code mest_code
,      mest.omschrijving mest_omschrijving
,      meet.commentaar
,      meet.created_by
,      meet.creation_date
,      meet.last_updated_by
,      meet.last_update_date
from   bas_meetwaarde_statussen mest
,      kgc_meetwaardestructuur mwst
,      kgc_medewerkers mede
,      kgc_medewerkers mede2
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      kgc_stoftesten stof
,      bas_meetwaarden meet
where meet.stof_id = stof.stof_id
and    meet.mede_id = mede.mede_id (+)
and    meet.mede_id_controle = mede2.mede_id (+)
and    meet.mwst_id = mwst.mwst_id (+)
and    meet.mest_id = mest.mest_id (+)
and    meet.meti_id = meti.meti_id
and    meti.onde_id = onde.onde_id (+)
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
union all
select meet.meet_id
,      meet.meti_id
,      meet.stof_id
,      meti.stgr_id
,      meti.frac_id
,      meti.stan_id
,      to_number(null) mons_id
,      ongr.kafd_id
,      stan.ongr_id
,      meti.onde_id
,      to_number( null ) pers_id
,      meet.mest_id
,      stan.standaardfractienummer
,      null fractiestatus
,      null monsternummer
,      to_date( null ) datum_afname
,      to_date( null ) tijd_afname
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      'Controlemonster' persoon_info_mons
,      null mons_commentaar
,      null onderzoeknr
,      'G' onderzoekstatus
,      'N' onde_spoed
,      'N' onde_afgerond
,      null persoon_info_onde
,      null indicatie
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.datum_aanmelding meti_datum_aanmelding
,      meti.prioriteit meti_prioriteit
,      meti.snelheid meti_snelheid
,      meti.afgerond meti_afgerond
,      to_char(meti.conclusie) meti_conclusie -- Mantis 9355 to_char added by SKA 08-04-2016 release 8.11.0.3
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.datum_meting
,      meet.afgerond meet_afgerond
,      meet.meet_reken
,      mwst.code mwst_code
,      mwst.omschrijving mwst_omschrijving
,      mede.naam mede_naam
,      mede2.naam mede_naam_controle
,      meet.tonen_als meetwaarde
,      meet.normaalwaarde_ondergrens
,      meet.normaalwaarde_bovengrens
,      meet.meeteenheid eenheid
,      mest.code mest_code
,      mest.omschrijving mest_omschrijving
,      meet.commentaar
,      meet.created_by
,      meet.creation_date
,      meet.last_updated_by
,      meet.last_update_date
from   kgc_onderzoeksgroepen ongr
,      bas_meetwaarde_statussen mest
,      kgc_meetwaardestructuur mwst
,      kgc_medewerkers mede
,      kgc_medewerkers mede2
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      bas_standaardfracties stan
,      bas_metingen meti
,      kgc_stoftesten stof
,      bas_meetwaarden meet
where  stan.ongr_id = ongr.ongr_id
and    stan.mate_id = mate.mate_id
and    stan.stan_id = meti.stan_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.meti_id = meet.meti_id
and    meet.stof_id = stof.stof_id
and    meet.mwst_id = mwst.mwst_id (+)
and    meet.mest_id = mest.mest_id (+)
and    meet.mede_id = mede.mede_id (+)
and    meet.mede_id_controle = mede2.mede_id (+);

/
QUIT
