CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_PERSONEN_FAMILIE_VW" ("PERS_ID", "ZISNR", "BSN", "BSN_GEVERIFIEERD", "AANSPREKEN", "GEBOORTEDATUM", "GESLACHT", "ADRES", "POSTCODE", "WOONPLAATS", "ACHTERNAAM", "ZOEKNAAM", "FAMILIES", "ZOEKFAMILIE") AS
  SELECT pers.pers_id
,      pers.zisnr
,      pers.bsn
,      pers.bsn_geverifieerd
,      pers.aanspreken
,      pers.geboortedatum
,      pers.geslacht
,      pers.adres
,      pers.postcode
,      pers.woonplaats
,      pers.achternaam
,      pers.zoeknaam
,      kgc_pers_00.families( pers.pers_id, 'K' ) families
,      pers.zoekfamilie
from   kgc_personen pers
 ;

/
QUIT
