CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_CDM_CASES_VW" ("ONDERZOEKNR", "MONSTERNUMMER", "FRACTIENUMMER", "FAMILIENUMMER", "PERS_AANSPREKEN", "GEBOORTEDATUM", "GESLACHT", "MATE_OMSCHRIJVING", "RELA_AANSPREKEN", "INDICATIES", "DATUM_MONSTER", "MEDE_NAAM_ANALIST", "KWEEK_KWALITEIT", "KLEUR_KWALITEIT", "METHODE", "STANDAARD_ONDERZOEK", "TOTAAL_AANTAL", "TOTAAL_GETELD", "TOTAAL_GEANALYSEERD", "TOTAAL_GEKARYOTYPEERD", "TOTAAL_OP_FOTO", "METI_RESULTAAT", "ONDE_ID", "ONMO_ID", "FRAC_ID", "METI_ID", "MEET_ID") AS
  SELECT onde.onderzoeknr
      ,mons.monsternummer
      ,frac.fractienummer
      ,kgc_fami_00.familie(pers.pers_id
                          ,'N') familienummer
      ,pers.aanspreken pers_aanspreken
      ,pers.geboortedatum
      ,pers.geslacht
      ,mate.omschrijving mate_omschrijving
      ,rela.aanspreken rela_aanspreken
      ,kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicaties
      --,nvl( mons.datum_afname, mons.datum_aanmelding ) datum_monster
      ,cdmh.datum_overgezet datum_monster
      ,mede.naam mede_naam_analist
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'KWEEK_KWALITEIT') kweek_kwaliteit
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'KLEUR_KWALITEIT') kleur_kwaliteit
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'METHODE') methode
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'STANDAARD_ONDERZOEK') standaard_onderzoek
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'TOTAAL_AANTAL') totaal_aantal
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'TOTAAL_GETELD') totaal_geteld
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'TOTAAL_GEANALYSEERD') totaal_geanalyseerd
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'TOTAAL_GEKARYOTYPEERD') totaal_gekaryotypeerd
      ,kgc_mwst_00.detailwaarde_meting(meti.meti_id
                                      ,'TOTAAL_OP_FOTO') totaal_op_foto
      ,to_char(meti.conclusie) meti_resultaat    --TO_CHAR added by Anuja for Mantis 9355 on 28-03-2016 for Release 8.11.0.2
      ,meti.onde_id
      ,meti.onmo_id
      ,meti.frac_id
      ,meti.meti_id
      --,kgc_cdm_00.meet_id( frac.frac_id, onde.onde_id, 'N' ) meet_id
      ,meet.meet_id -- 1712
  FROM kgc_medewerkers           mede
      ,kgc_relaties              rela
      ,kgc_materialen            mate
      ,kgc_personen              pers
      ,kgc_onderzoeken           onde
      ,kgc_monsters              mons
      ,bas_fracties              frac
      ,kgc_cdm_helix             cdmh
      ,bas_metingen              meti
      ,bas_meetwaarden           meet -- 1712
      ,kgc_cdm_selectie_cdm01_vw cdm1 -- 1712
      ,kgc_stoftesten            stof -- 1712
 WHERE meti.onde_id = onde.onde_id
   AND onde.rela_id = rela.rela_id
   AND meti.frac_id = frac.frac_id
   AND frac.mons_id = mons.mons_id
   AND mons.pers_id = pers.pers_id
   AND mons.mate_id = mate.mate_id
   AND meti.mede_id = mede.mede_id(+)
   AND meti.onde_id = cdmh.onde_id(+)
   AND meti.frac_id = cdmh.frac_id(+)
   AND stof.code = kgc_sypa_00.standaard_waarde('REFERENTIE_STOFTEST' -- p_parameter_code
                                               ,onde.kafd_id
                                               ,onde.ongr_id) -- 1712
   AND meet.stof_id = stof.stof_id
   AND meti.frac_id = cdm1.frac_id
   AND meti.onde_id = cdm1.onde_id
   AND meti.meti_id = meet.meti_id;

/
QUIT
