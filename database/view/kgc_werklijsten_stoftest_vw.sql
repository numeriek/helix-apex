CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_WERKLIJSTEN_STOFTEST_VW" ("KAFD_ID", "ONDERZOEKNR", "ONDE_ID", "INDICATIE", "MONSTERNUMMER", "MONS_ID", "PERSOON_INFO_MONS", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "FRAC_ID", "WLST_ID", "WERKLIJST_NUMMER", "WLIT_VOLGNUMMER", "STGR_CODE", "STGR_OMSCHRIJVING", "STGR_ID", "METI_ID", "MEET_ID", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING") AS
  SELECT onde.kafd_id
,      onde.onderzoeknr
,      onde.onde_id
,      kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie
,      mons.monsternummer
,      mons.mons_id
,      kgc_pers_00.persoon_info(mons.mons_id, null )  persoon_info_mons
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.fractienummer
,      frac.frac_id
,      wlst.wlst_id
,      wlst.werklijst_nummer
,      wlit.VOLGNUMMER     wlit_volgnummer
,      stgr.code           stgr_code
,      stgr.omschrijving   stgr_omschrijving
,      stgr.stgr_id
,      meti.meti_id
,      meet.meet_id
,      stof.stof_id
,      stof.CODE           stof_code
,      stof.OMSCHRIJVING   stof_omschrijving
FROM kgc_onderzoeken onde
,    bas_metingen    meti
,    bas_fracties    frac
,    kgc_monsters    mons
,    bas_meetwaarden meet
,    kgc_werklijsten wlst
,    kgc_werklijst_items wlit
,    kgc_stoftestgroepen stgr
,    kgc_stoftesten  stof
,    kgc_materialen mate
WHERE meti.ONDE_ID = onde.ONDE_ID
AND   meti.FRAC_ID = frac.FRAC_ID
AND   frac.MONS_ID = mons.MONS_ID
AND   mons.mate_id = mate.mate_id
AND   meet.METI_ID = meti.METI_ID
AND   wlit.MEET_ID = meet.MEET_ID
AND   wlit.WLST_ID = wlst.WLST_ID
AND   meti.STGR_ID = stgr.STGR_ID
AND   meet.STOF_ID = stof.STOF_ID
AND   meet.AFGEROND = 'N'
AND   meti.AFGEROND = 'N'
AND   onde.AFGEROND = 'N'
 ;

/
QUIT
