CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ANFO_MEETWAARDEN_VW" ("MEET_ID", "METI_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "MEDE_NAAM", "MEDE_NAAM_CONTROLE", "MEST_OMSCHRIJVING", "MDET_PROMPT", "MDET_WAARDE", "MDET_VOLGORDE") AS
  SELECT meet.meet_id
,      meet.meti_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      mede.naam mede_naam
,      mede2.naam mede_naam_controle
,      mest.omschrijving mest_omschrijving
,      mdet.prompt mdet_prompt
,      mdet.waarde mdet_waarde
,      mdet.volgorde mdet_volgorde
from   kgc_meetwaardestructuur mwst
,      bas_meetwaarde_statussen mest
,      kgc_medewerkers mede2
,      kgc_medewerkers mede
,      kgc_stoftesten stof
,      bas_meetwaarden meet
,      bas_meetwaarde_details mdet
where  meet.stof_id = stof.stof_id
and    stof.mwst_id = mwst.mwst_id  (+)
and    nvl(mwst.code,'kleine letters') <> 'KGCKARY01'
and   meet.meet_id = mdet.meet_id (+)
and    meet.mede_id = mede.mede_id (+)
and    meet.mede_id_controle = mede2.mede_id (+)
and    meet.mest_id = mest.mest_id (+)
 ;

/
QUIT
