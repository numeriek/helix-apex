CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_DEIT_ONDE_EN_MONS_VW" ("ENTITEIT", "ID", "KAFD_ID", "ONGR_ID", "KAFD_CODE", "ONGR_CODE", "IDENTIFICATIE", "DATUM_BINNEN", "PERSOON_INFO", "ONDE_ID", "MONS_ID", "PERS_ID", "AANTAL_DECO") AS
  SELECT 'ONDE' entiteit
,      onde.onde_id id
,      onde.kafd_id
,      onde.ongr_id
,      kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr identificatie
,      onde.datum_binnen
,      kgc_pers_00.persoon_info( null, onde.onde_id, null, 'LANG' ) persoon_info
,      onde.onde_id
,      to_number(null) mons_id
,      onde.pers_id
,      count(deco.decl_id) aantal_deco
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_declaraties_bij_onde_vw deco
,      kgc_onderzoeken onde
  WHERE onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.onde_id = deco.onde_id (+)
and  (  nvl( deco.status, '-' ) in ( '-', 'T', 'R' ) or deco.onde_id is null )
and    onde.afgerond = 'N'
group by onde.onde_id
,        onde.kafd_id
,        onde.ongr_id
,        kafd.code
,        ongr.code
,        onde.onderzoeknr
,        onde.datum_binnen
,        onde.pers_id
union all
select 'MONS'
,      mons.mons_id
,      mons.kafd_id
,      mons.ongr_id
,      kafd.code kafd_code
,      ongr.code ongr_code
,      mons.monsternummer
,      mons.datum_aanmelding
,      kgc_pers_00.persoon_info( mons.mons_id, null, null, 'LANG' )
,      to_number(null)
,      mons.mons_id
,      mons.pers_id
,      count(decl.decl_id)
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_declaraties decl
,      kgc_monsters mons
where  mons.kafd_id = kafd.kafd_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mons_id = decl.id (+)
and  ( ( nvl( decl.entiteit, 'MONS' ) = 'MONS'
     and nvl( decl.status, '-' ) in ( '-', 'T', 'R' )
       )
    or decl.decl_id is null
     )
group by mons.mons_id
,        mons.kafd_id
,        mons.ongr_id
,        kafd.code
,        ongr.code
,        mons.monsternummer
,        mons.datum_aanmelding
,        mons.pers_id
 ;

/
QUIT
