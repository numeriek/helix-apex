CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_LAATSTE_MEETWAARDEN_VW" ("METI_ID", "FRAC_ID", "ONDE_ID", "STOF_ID", "MEET_ID") AS
  SELECT meti.meti_id
,      meti.frac_id
,      meti.onde_id
,      meet.stof_id
,      max(meet.meet_id) meet_id
from   bas_metingen meti
,      bas_meetwaarden meet
where meet.meti_id = meti.meti_id
group by meti.meti_id
,        meti.frac_id
,        meti.onde_id
,        meet.stof_id
 ;

/
QUIT
