CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_PRENATAAL_VREUGDEBRIEF_VW" ("KAFD_CODE", "ONGR_CODE", "VREUGDE_VOLGENS_REGELS", "VREUGDE", "REFERENTIE", "GEADRESSEERDE", "VERRICHTING", "INFO_GESLACHT", "METINGEN", "ONDERTEKENAAR", "EERDER_GEPRINT", "KAFD_ID", "ONGR_ID", "PERS_ID", "ZWAN_ID") AS
  SELECT distinct zwon.kafd_code
,      zwon.ongr_code
,      kgc_vreugdebrief_00.vreugde( zwon.zwan_id ) vreugde_volgens_regels
,      decode( zwon.vreugdebrief
             , 'J', 'J'
             , kgc_vreugdebrief_00.vreugde( zwon.zwan_id )
             ) vreugde
,      kgc_vreugdebrief_00.referentie( zwon.zwan_id ) referentie
,      kgc_adres_00.persoon( zwon.pers_id, 'J' ) geadresseerde
,      kgc_vreugdebrief_00.verrichting( zwon.zwan_id ) verrichting
,      kgc_vreugdebrief_00.info_geslacht( zwon.zwan_id ) info_geslacht
,      kgc_vreugdebrief_00.metingen( zwon.zwan_id ) metingen
,      kgc_vreugdebrief_00.ondertekenaar( zwon.zwan_id ) ondertekenaar
,      kgc_brie_00.vreugdebrief_reeds_geprint( zwon.zwan_id ) eerder_geprint
,      zwon.kafd_id
,      zwon.ongr_id
,      zwon.pers_id
,      zwon.zwan_id
from   kgc_prenataal_zwan_onde_vw zwon
where  datum_binnen >= add_months( sysdate, -3 )
and    nvl( zwon.vreugdebrief, 'S' ) <> 'N'
and NVL(kgc_attr_00.waarde('KGC_ZWANGERSCHAPPEN', 'SMS_VERZENDEN', zwon.zwan_id), 'J') = 'N'   -- per sms?;;
 ;

/
QUIT
