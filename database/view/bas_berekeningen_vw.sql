CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_BEREKENINGEN_VW" ("MEET_ID", "BERE_ID", "BETY_CODE", "BETY_OMSCHRIJVING", "UITSLAG", "UITSLAGEENHEID", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS") AS
  SELECT bere.meet_id
,      bere.bere_id
,      bety.code bety_code
,      bety.omschrijving bety_omschrijving
,      bere.uitslag
,      bere.uitslageenheid
,      bere.normaalwaarde_ondergrens
,      bere.normaalwaarde_bovengrens
from   bas_berekening_types bety
,      bas_berekeningen bere
where bere.bety_id = bety.bety_id
 ;

/
QUIT
