CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCONDE69_ONDE" ("KAFD_CODE", "KAFD_NAAM", "MEDE_CODE", "MEDE_NAAM", "ONDERZOEKNR", "DATUM_BINNEN", "AANMAAK_DATUM", "INDI_CODE", "INDI_OMSCHRIJVING", "KAFD_ID", "ONDE_ID", "ONIN_ID", "INDI_ID", "MEDE_ID") AS
  SELECT kafd.code
,      kafd.naam
,      mede.code
,      mede.naam
,      onde.onderzoeknr
,      onde.datum_binnen
,      onde.creation_date
,      indi.code
,      indi.omschrijving
,      kafd.kafd_id
,      onde.onde_id
,      onin.onin_id
,      indi.indi_id
,      mede.mede_id
FROM   kgc_kgc_afdelingen        kafd
,      kgc_medewerkers           mede
,      kgc_onderzoeken           onde
,      kgc_onderzoek_indicaties  onin
,      kgc_indicatie_teksten     indi
WHERE  kafd.kafd_id  = onde.kafd_id
AND    onde.onde_id  = onin.onde_id
AND    onin.indi_id  = indi.indi_id
AND    onde.mede_id_beoordelaar = mede.mede_id (+)
AND    onde.afgerond = 'N'
 ;

/
QUIT
