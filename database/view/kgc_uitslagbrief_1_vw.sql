CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAGBRIEF_1_VW" ("KOPIE", "BRTY_CODE", "BRIEFTYPE", "GEADRESSEERDE", "ONDERZOEKNR", "PERSOON", "GESLACHT", "GEBOORTEDATUM", "AANVRAAGDATUM", "AANV_CODE", "AANVRAGER", "AANV_TOEVOEGING", "INDICATIE", "ONDE_OMSCHRIJVING", "ONDE_REFERENTIE", "AFGEROND", "DATUM_UITSLAG", "DATUM_AUTORISATIE", "AUTO_CODE", "AUTORISATOR", "ONDERTEKENAAR1", "ONDERTEKENAAR2", "ONDERTEKENAAR3", "UITSLAG_TEKST", "TOELICHTING", "UITS_ID", "BRTY_ID", "KOHO_ID", "PERS_ID", "ONDE_ID", "KAFD_ID", "ONGR_ID", "TAAL_ID", "RELA_ID", "RELA_ID_ORIGINEEL", "INDI_ID", "KAFD_CODE", "ONGR_CODE", "ZISNR", "MEDE_ID_AUTORISATOR", "MEDE_ID_ONDERTEKENAAR1", "MEDE_ID_ONDERTEKENAAR2", "MEDE_ID_ONDERTEKENAAR3", "ONDE_STATUS", "ONDERZOEKSTYPE", "BSN_OPGEMAAKT", "DATUM_BRIEF") AS
  SELECT 'N' kopie,
             brty.code brty_code,
             brty.omschrijving brieftype,
             kgc_adres_00.relatie (uits.rela_id, 'J') geadresseerde,
             onde.onderzoeknr onderzoeknr,
             pers.aanspreken persoon,
             kgc_vert_00.
              vertaling ('PERS_GESL', NULL, pers.geslacht, onde.taal_id)
                 geslacht,
             pers.geboortedatum geboortedatum,
             onde.datum_binnen aanvraagdatum,
             aanv.code aanv_code,
             aanv2.aanspreken aanvrager,
             kgc_rela_00.toevoeging (aanv2.rela_id) aanv_toevoeging,
             kgc_indi_00.onderzoeksreden (onde.onde_id) indicatie,
             onde.omschrijving onde_omschrijving,
             onde.referentie onde_referentie,
             onde.afgerond afgerond,
             uits.datum_uitslag datum_uitslag,
             onde.datum_autorisatie datum_autorisatie,
             DECODE (onde.afgerond, 'J', mede_onde.code, mede_uits.code)
                 auto_code,
             DECODE (onde.afgerond,
                        'J', NVL (mede_onde.formele_naam, mede_onde.naam),
                        NVL (mede_uits.formele_naam, mede_uits.naam)
                      )
                 autorisator,
             NVL (mede1.formele_naam, mede1.naam) ondertekenaar1,
             NVL (mede2.formele_naam, mede2.naam) ondertekenaar2,
             NVL (mede3.formele_naam, mede3.naam) ondertekenaar3,
             uits.tekst uitslag_tekst,
             uits.toelichting toelichting,
             uits.uits_id uits_id,
             uits.brty_id brty_id,
             TO_NUMBER (NULL) koho_id,
             onde.pers_id pers_id,
             onde.onde_id onde_id,
             onde.kafd_id kafd_id,
             onde.ongr_id ongr_id,
             onde.taal_id taal_id,
             uits.rela_id rela_id,
             uits.rela_id rela_id_origineel,
             kgc_indi_00.indi_id (onde.onde_id) indi_id,
             kafd.code kafd_code,
             ongr.code ongr_code,
             pers.zisnr zisnr,
             DECODE (onde.afgerond, 'J', mede_onde.mede_id, mede_uits.mede_id)
                 mede_id_autorisator,
             mede1.mede_id mede_id_ondertekenaar1,
             mede2.mede_id mede_id_ondertekenaar2,
             mede3.mede_id mede_id_ondertekenaar3,
             onde.status onde_status,
             onde.onderzoekstype onderzoekstype,
             kgc_pers_00.bsn_opgemaakt (pers.pers_id) bsn_opgemaakt,
             kgc_brie_00.datum_huidige_brief (uits.uits_id) datum_brief
      FROM kgc_kgc_afdelingen kafd,
             kgc_onderzoeksgroepen ongr,
             kgc_brieftypes brty,
             kgc_relaties aanv,
             kgc_relaties aanv2,
             kgc_medewerkers mede_onde,
             kgc_medewerkers mede_uits,
             kgc_medewerkers mede1,
             kgc_medewerkers mede2,
             kgc_medewerkers mede3,
             kgc_personen pers,
             kgc_onderzoeken onde,
             kgc_uitslagen uits
     WHERE      onde.kafd_id = kafd.kafd_id
             AND onde.ongr_id = ongr.ongr_id
             AND onde.pers_id = pers.pers_id
             AND onde.onde_id = uits.onde_id
             AND onde.rela_id = aanv.rela_id
             AND NVL (onde.rela_id_oorsprong, onde.rela_id) = aanv2.rela_id
             AND onde.mede_id_autorisator = mede_onde.mede_id(+)
             AND uits.mede_id4 = mede_uits.mede_id(+)
             AND uits.mede_id = mede1.mede_id(+)
             AND uits.mede_id2 = mede2.mede_id(+)
             AND uits.mede_id3 = mede3.mede_id(+)
             AND uits.brty_id = brty.brty_id(+)
             AND uits.tekst IS NOT NULL
 ;

/
QUIT
