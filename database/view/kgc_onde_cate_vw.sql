CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDE_CATE_VW" ("ONDE_ID", "CATE_ID", "CATE_ID_PARENT", "CATE_VOLGORDE", "CATE_OMSCHRIJVING") AS
  SELECT onde.onde_id
,      cate.cate_id
,      cate.cate_id_parent
,      cate.volgorde cate_volgorde
,      nvl( kgc_vert_00.vertaling( 'CATE', cate.cate_id, null, onde.taal_id ), cate.omschrijving ) cate_omschrijving
from   kgc_categorieen cate
, kgc_onderzoeken onde
  WHERE ( onde.kafd_id = cate.kafd_id or cate.kafd_id is null )
and  ( onde.ongr_id = cate.ongr_id or cate.ongr_id is null )
 ;

/
QUIT
