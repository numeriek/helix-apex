CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCOPTE51_RIJ_VW" ("STOF_ID", "STOF_OMS") AS
  SELECT rij.stof_id
, rij.stof_oms
  FROM TABLE(kgc_opte_00.f_retourneer_obj) rij
  WHERE trim(rij.stof_oms) NOT IN ('Onderzoek', 'Materiaal', 'Fractie', '#METI_ID#')
  ORDER BY
  rij.stof_oms
 ;

/
QUIT
