CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCBEST02_DOCUMENTEN_VW" ("BEST_ID", "ENTITEIT_CODE", "ENTITEIT_PK", "ENTITEIT_OMSCHRIJVING", "BTYP_ID", "BESTAND_SPECIFICATIE", "BESTAND_AANGEMAAKT_DOOR", "BESTAND_DATUM_AANGEMAAKT", "BESTAND_VOLGORDE", "BESTAND_LOCATIE", "BESTANDSNAAM", "BESTAND_CATEGORIE", "PERS_ID", "ONGR_CODE", "ONGR_OMSCHRIJVING", "INDICATIE", "ONDE_ID", "ONDE_AFGEROND", "AFDELING", "ONDERZOEKSNUMMER", "MONSTERNUMMER", "WERKLIJST_NUMMER", "BRIEFTYPE", "BESTANDSTYPE", "ONUI_CODE", "KOPIE", "RELATEERBAAR", "BCAT_ID", "BTYP_VERWIJDERBAAR", "BTYP_VERVALBAAR", "BEST_VERVALLEN", "BEST_VERWIJDERD") AS
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    to_number('') pers_id ,
    TO_CHAR('') ongr_code ,
    TO_CHAR('') ongr_omschrijving ,
    TO_CHAR('') indicatie ,
    to_number('') onde_id ,
    TO_CHAR('') onde_afgerond ,
    TO_CHAR('') Afdeling ,
    TO_CHAR('') Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    TO_CHAR('') onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen best_vervallen,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_families fami ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = fami.fami_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'FAMI'
  UNION ALL
  -- FRAC (Fracties)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id pers_id ,
    TO_CHAR('') ongr_code ,
    TO_CHAR('') ongr_omschrijving ,
    kgc_indi_00.mons_indicatie_codes(mons.mons_id) indicatie ,
    to_number('') onde_id ,
    TO_CHAR('') onde_afgerond ,
    kafd.code Afdeling ,
    TO_CHAR('') Onderzoeksnummer ,
    mons.monsternummer Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    TO_CHAR('') onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    bas_fracties frac ,
    kgc_monsters mons ,
    kgc_personen pers ,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = frac.frac_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND frac.mons_id       = mons.mons_id
  AND mons.pers_id       = pers.pers_id
  AND frac.kafd_id       = kafd.kafd_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'FRAC'
  UNION ALL
  -- GEBE (Gesprek betrokkenen)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id pers_id ,
    ongr.code ongr_code ,
    ongr.omschrijving ongr_omschrijving ,
    kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicatie ,
    onde.onde_id onde_id ,
    onde.afgerond onde_afgerond,
    kafd.code Afdeling ,
    onde.onderzoeknr Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    decode(kgc_moto_00.toegang_jn('KGCBEST02',null,null,null,null,null,onde.onde_id,'ONDE',onde.onde_id,kafd.kafd_id,'P'),'J',decode(onde.afgerond,'J',onui.code,null),null) onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_gesprek_betrokkenen gebe ,
    kgc_gesprekken gesp ,
    kgc_onderzoeken onde ,
    kgc_onderzoek_uitslagcodes onui,
    kgc_onderzoeksgroepen ongr ,
    kgc_personen pers ,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = gebe.gebe_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND gebe.pers_id       = pers.pers_id
  AND gebe.gesp_id       = gesp.gesp_id
  AND gesp.onde_id       = onde.onde_id
  AND gesp.ongr_id       = ongr.ongr_id
  AND onde.kafd_id       = kafd.kafd_id
  AND onde.onui_id       = onui.onui_id(+)
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'GEBE'
  UNION ALL
  -- GESP (Gesprekken)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    ongr.code ongr_code ,
    ongr.omschrijving ongr_omschrijving ,
    kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicatie ,
    onde.onde_id ,
    onde.afgerond onde_afgerond,
    kafd.code Afdeling ,
    onde.onderzoeknr Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    decode(kgc_moto_00.toegang_jn('KGCBEST02',null,null,null,null,null,onde.onde_id,'ONDE',onde.onde_id,kafd.kafd_id,'P'),'J',decode(onde.afgerond,'J',onui.code,null),null) onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_gesprek_betrokkenen gebe ,
    kgc_gesprekken gesp ,
    kgc_onderzoeken onde ,
    kgc_onderzoek_uitslagcodes onui,
    kgc_onderzoeksgroepen ongr ,
    kgc_personen pers ,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = gesp.gesp_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND gebe.pers_id       = pers.pers_id
  AND gebe.gesp_id       = gesp.gesp_id
  AND gesp.onde_id       = onde.onde_id
  AND gesp.ongr_id       = ongr.ongr_id
  AND onde.kafd_id       = kafd.kafd_id
  AND onde.onui_id       = onui.onui_id(+)
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'GESP'
  UNION ALL
  -- IMFI (Import Files)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    TO_CHAR('') ongr_code ,
    TO_CHAR('') ongr_omschrijving ,
    TO_CHAR('') indicatie ,
    to_number('') onde_id ,
    TO_CHAR('') onde_afgerond ,
    TO_CHAR('') Afdeling ,
    TO_CHAR('') Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    TO_CHAR('') onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_import_files imfi ,
    kgc_im_personen impe ,
    kgc_personen pers ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = imfi.imfi_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND imfi.imfi_id       = impe.imfi_id
  AND impe.kgc_pers_id   = pers.pers_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'IMFI'
  UNION ALL
  -- MEET (Meetwaarden)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    ongr.code ongr_code ,
    ongr.omschrijving ongr_omschrijving ,
    kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicatie ,
    onde.onde_id ,
    onde.afgerond onde_afgerond,
    kafd.code Afdeling ,
    onde.onderzoeknr Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    decode(kgc_moto_00.toegang_jn('KGCBEST02',null,null,null,null,null,onde.onde_id,'ONDE',onde.onde_id,kafd.kafd_id,'P'),'J',decode(onde.afgerond,'J',onui.code,null),null) onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    bas_meetwaarden meet ,
    bas_metingen meti ,
    kgc_onderzoeken onde ,
    kgc_onderzoeksgroepen ongr ,
    kgc_onderzoek_uitslagcodes onui,
    kgc_personen pers ,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = meet.meet_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND meet.meti_id       = meti.meti_id
  AND meti.onde_id       = onde.onde_id
  AND onde.pers_id       = pers.pers_id
  AND onde.ongr_id       = ongr.ongr_id
  AND onde.onui_id       = onui.onui_id(+)
  AND onde.kafd_id       = kafd.kafd_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'MEET'
  UNION ALL
  -- METI (Metingen)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    ongr.code ongr_code ,
    ongr.omschrijving ongr_omschrijving ,
    kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicatie ,
    onde.onde_id ,
    onde.afgerond onde_afgerond,
    kafd.code Afdeling ,
    onde.onderzoeknr Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    decode(kgc_moto_00.toegang_jn('KGCBEST02',null,null,null,null,null,onde.onde_id,'ONDE',onde.onde_id,kafd.kafd_id,'P'),'J',decode(onde.afgerond,'J',onui.code,null),null) onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    bas_metingen meti ,
    kgc_onderzoeken onde ,
    kgc_onderzoeksgroepen ongr ,
    kgc_onderzoek_uitslagcodes onui,
    kgc_personen pers ,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = meti.meti_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND meti.onde_id       = onde.onde_id
  AND onde.pers_id       = pers.pers_id
  AND onde.ongr_id       = ongr.ongr_id
  AND onde.kafd_id       = kafd.kafd_id
  AND onde.onui_id       = onui.onui_id(+)
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'METI'
  UNION ALL
  -- MONS (Monsters)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    TO_CHAR('') ongr_code ,
    TO_CHAR('') ongr_omschrijving ,
    kgc_indi_00.mons_indicatie_codes(mons.mons_id) indicatie ,
    to_number('') onde_id ,
    TO_CHAR('') onde_afgerond ,
    kafd.code Afdeling ,
    TO_CHAR('') Onderzoeksnummer ,
    mons.monsternummer Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    TO_CHAR('') onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_monsters mons ,
    kgc_personen pers ,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = mons.mons_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND mons.pers_id       = pers.pers_id
  AND mons.kafd_id       = kafd.kafd_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'MONS'
  UNION ALL
  -- ONDE (Onderzoeken)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    ongr.code ongr_code ,
    ongr.omschrijving ongr_omschrijving ,
    kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicatie ,
    onde.onde_id ,
    onde.afgerond onde_afgerond,
    kafd.code Afdeling ,
    onde.onderzoeknr Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    decode(kgc_moto_00.toegang_jn('KGCBEST02',null,null,null,null,null,onde.onde_id,'ONDE',onde.onde_id,kafd.kafd_id,'P'),'J',decode(onde.afgerond,'J',onui.code,null),null) onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_onderzoeken onde ,
    kgc_onderzoeksgroepen ongr ,
    kgc_onderzoek_uitslagcodes onui,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat ,
    kgc_personen pers,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = onde.onde_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND onde.pers_id       = pers.pers_id
  AND onde.ongr_id       = ongr.ongr_id(+)
  AND onde.onui_id       = onui.onui_id(+)
  AND onde.kafd_id       = kafd.kafd_id
  AND best.entiteit_code = 'ONDE'
  UNION ALL
  -- PERS (Personen)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    TO_CHAR('') ongr_code ,
    TO_CHAR('') ongr_omschrijving ,
    TO_CHAR('') indicatie ,
    to_number('') onde_id ,
    TO_CHAR('') onde_afgerond ,
    TO_CHAR('') Afdeling ,
    TO_CHAR('') Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    TO_CHAR('') onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_personen pers ,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti
  WHERE best.entiteit_pk = pers.pers_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'PERS'
  UNION ALL
  -- UITS (Uitslagen)
  SELECT best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id ,
    ongr.code ongr_code ,
    ongr.omschrijving ongr_omschrijving ,
    kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicatie ,
    onde.onde_id ,
    onde.afgerond onde_afgerond,
    kafd.code Afdeling ,
    onde.onderzoeknr Onderzoeksnummer ,
    TO_CHAR('') Monsternummer ,
    TO_CHAR('') werklijst_nummer,
    decode(brie.best_id,null,null,brty.code) Brieftype ,
    btyp.code Bestandstype,
    decode(kgc_moto_00.toegang_jn('KGCBEST02',null,null,null,null,null,onde.onde_id,'ONDE',onde.onde_id,kafd.kafd_id,'P'),'J',decode(onde.afgerond,'J',onui.code,null),null) onui_code,
    nvl(brie.kopie,'N') kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best ,
    kgc_bestand_types btyp ,
    kgc_uitslagen uits ,
    kgc_onderzoeken onde ,
    kgc_onderzoek_uitslagcodes onui,
    kgc_onderzoeksgroepen ongr ,
    kgc_kgc_afdelingen kafd ,
    kgc_bestand_categorieen bcat ,
    kgc_personen pers,
    kgc_brieftypes brty,
    kgc_entiteiten enti,
    kgc_brieven   brie
  WHERE best.entiteit_pk = uits.uits_id
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND uits.onde_id       = onde.onde_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND onde.pers_id       = pers.pers_id
  AND onde.ongr_id       = ongr.ongr_id(+)
  AND onde.onui_id       = onui.onui_id(+)
  AND uits.kafd_id       = kafd.kafd_id
  AND brie.brty_id       = brty.brty_id(+)
  AND brie.best_id(+)    = best.best_id
  AND best.entiteit_code = 'UITS'
  UNION ALL
  -- WLST (Werklijstenlijsten)
  SELECT distinct best.best_id ,
    best.entiteit_code ,
    best.entiteit_pk ,
    enti.omschrijving entiteit_omschrijving,
    best.btyp_id ,
    best.bestand_specificatie ,
    best.created_by bestand_aangemaakt_door ,
    best.creation_date bestand_datum_aangemaakt ,
    best.volgorde bestand_volgorde ,
    best.locatie bestand_locatie ,
    best.bestandsnaam ,
    bcat.code||decode(bcat.vervallen,'J','(V)') bestand_categorie ,
    pers.pers_id pers_id,
    ongr.code ongr_code ,
    ongr.omschrijving ongr_omschrijving ,
    kgc_indi_00.onde_indicatie_codes(onde.onde_id) indicatie ,
    onde.onde_id ,
    onde.afgerond onde_afgerond,
    kafd.code Afdeling ,
    onde.onderzoeknr Onderzoeksnummer ,
    TO_CHAR('') Monsternummer,
    wlst.werklijst_nummer,
    TO_CHAR('') Brieftype ,
    btyp.code Bestandstype,
    decode(kgc_moto_00.toegang_jn('KGCBEST02',null,null,null,null,null,onde.onde_id,'ONDE',onde.onde_id,kafd.kafd_id,'P'),'J',decode(onde.afgerond,'J',onui.code,null),null) onui_code,
    'N' kopie,
    btyp.relateerbaar,
    bcat.bcat_id,
    btyp.verwijderbaar btyp_verwijderbaar,
    btyp.vervalbaar btyp_vervalbaar,
    best.vervallen BEST_VERVALLEN,
    best.verwijderd best_verwijderd
  FROM kgc_bestanden best,
    kgc_bestand_types btyp,
    kgc_werklijsten wlst,
    kgc_kgc_afdelingen kafd,
    kgc_bestand_categorieen bcat,
    kgc_entiteiten enti,
    kgc_personen pers,
    (select distinct  wlst_id, meti_id from kgc_werklijst_items) wlit,
   kgc_onderzoeken onde ,
    kgc_onderzoeksgroepen ongr,
    kgc_onderzoek_uitslagcodes onui,
    bas_metingen meti
  WHERE best.entiteit_pk = wlst.wlst_id
  and wlst.wlst_id = wlit.wlst_id
  and wlit.meti_id = meti.meti_id
  and meti.onde_id = onde.onde_id
  and onde.pers_id = pers.pers_id
  AND onde.ongr_id = ongr.ongr_id(+)
  AND onde.onui_id = onui.onui_id(+)
  AND best.entiteit_code = enti.code
  AND best.btyp_id       = btyp.btyp_id
  AND wlst.kafd_id       = kafd.kafd_id
  AND best.bcat_id       = bcat.bcat_id(+)
  AND best.entiteit_code = 'WLST';

/
QUIT
