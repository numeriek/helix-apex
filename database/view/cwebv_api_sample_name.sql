CREATE OR REPLACE FORCE VIEW "HELIX"."CWEBV_API_SAMPLE_NAME" ("HELIX_NAME", "LIMS_NAME", "COMPLETE") AS
  SELECT distinct
         naam helix_name
  ,      '000_'||naam lims_name
  ,      'N' complete
  from   kgc_samples
 ;

/
QUIT
