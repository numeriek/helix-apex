CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCOOMV51_METI_VW" ("ONMO_ID", "STGR_CODE", "MEDE_CODE", "AFGEROND") AS
  SELECT meti.onmo_id
,      stgr.code            stgr_code
,      mede.code            mede_code
,      meti.afgerond
FROM   bas_metingen meti
,      kgc_stoftestgroepen stgr
,      kgc_medewerkers mede
WHERE  meti.stgr_id = stgr.stgr_id (+)
AND    meti.mede_id = mede.mede_id(+)
 ;

/
QUIT
