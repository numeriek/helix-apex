CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FOETUSSEN_VW" ("ZWAN_NUMMER", "FOET_VOLGNR", "POSI_CODE", "POSI_OMSCHRIJVING", "DATUM_ATERM", "GEBOREN", "IDENTIFICATIE", "FOET_ID", "PERS_ID_MOEDER", "PERS_ID_GEBOREN_ALS", "ZWAN_ID", "POSI_ID", "GESLACHT", "POSI_OMSCHRIJVING1", "COMMENTAAR", "DOODGEBOREN") AS
  SELECT zwan.nummer zwan_nummer
,      foet.volgnr foet_volgnr
,      posi.code posi_code
,      nvl(posi.omschrijving,commentaar) posi_omschrijving    --  old column used in KGCSECR13 and  KGCONBE01B
,      nvl( zwan.datum_aterm, foet.geplande_geboortedatum ) datum_aterm
,      decode( foet.pers_id_geboren_als
             , null, 'N'
             , 'J'
             ) geboren
,      to_char(zwan.nummer)||'-'||to_char(foet.volgnr) identificatie
,      foet.foet_id
,      foet.pers_id_moeder
,      foet.pers_id_geboren_als
,      foet.zwan_id
,      foet.posi_id
,      foet.geslacht
,      posi.omschrijving posi_omschrijving1  -- new column used in KGCPERS01 ,  KGCONDE01  for   mantis 2881
,      foet.commentaar  -- -- mantis 2881
,      foet.NIET_GEBOREN Doodgeboren  -- mantis 2881
from   kgc_posities posi
,      kgc_zwangerschappen zwan
,      kgc_foetussen foet
where foet.zwan_id = zwan.zwan_id (+)
and    foet.posi_id = posi.posi_id (+);

/
QUIT
