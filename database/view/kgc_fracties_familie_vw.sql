CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FRACTIES_FAMILIE_VW" ("FAMI_ID", "FAMILIENAAM", "FALE_ID", "PERS_ID", "AANSPREKEN", "GEBOORTEDATUM", "GESLACHT", "KAFD_ID", "NAAM", "ONGR_ID", "OMSCHRIJVING", "MONS_ID", "MONSTERNUMMER", "DATUM_AANMELDING", "FRAC_ID", "FRAC_CREATION_DATE", "FRACTIENUMMER", "STATUS", "CONTROLE", "OPWERKDATUM", "MATE_CODE", "MATE_OMSCHRIJVING", "FOETUS") AS
  SELECT fami.fami_id
,    fami.naam familienaam
,    fale.fale_id
,    fale.pers_id
,   pers.aanspreken
,   pers.geboortedatum
,   pers.geslacht
,   mons.kafd_id
,   kafd.naam
,   mons.ongr_id
,   ongr.omschrijving
,   mons.mons_id
,   mons.monsternummer
,   mons.datum_aanmelding
,   frac.frac_id
,   frac.creation_date frac_creation_date
,   frac.fractienummer
,   frac.status
,   frac.controle
,   opwe.opwerkdatum
,   mate.code mate_code
,   mate.omschrijving mate_omschrijving
,   kgc_foet_00.foetus_info( mons.foet_id, null, null, 'J' ) foetus
from   kgc_onderzoeksgroepen ongr
,   kgc_kgc_afdelingen  kafd
,   kgc_materialen mate
,   bas_opwerken  opwe
,   kgc_personen   pers
,   bas_fracties   frac
,   kgc_monsters   mons
,   kgc_familie_leden  fale
,   kgc_families   fami
where ongr.ongr_id = mons.ongr_id
and   kafd.kafd_id = mons.kafd_id
and mons.mate_id = mate.mate_id
 and   frac.opwe_id = opwe.opwe_id (+)
 and   pers.pers_id = mons.pers_id
 and   frac.mons_id = mons.mons_id
 and   mons.pers_id = fale.pers_id
 and   fale.fami_id = fami.fami_id
and  nvl(frac.status,'O') in ( 'A', 'O','V','X','M' )--Statuses added for Mantis 4513;

/
QUIT
