CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCWLST10_STGR_VW" ("KAFD_CODE", "KAFD_NAAM", "WERKLIJST_NUMMER", "ANALIST", "PROTOCOL", "WLST_ID", "KAFD_ID", "STGR_ID") AS
  SELECT DISTINCT
       kafd.code
,      kafd.naam
,      wlst.werklijst_nummer
,      kgc_mede_00.medewerker_naam( oracle_uid )
,      stgr.code
,      wlst.wlst_id
,      kafd.kafd_id
,      stgr.stgr_id
FROM   kgc_kgc_afdelingen   kafd
,      kgc_stoftestgroepen  stgr
,      bas_metingen         meti
,      bas_meetwaarden      meet
,      kgc_werklijst_items  wlit
,      kgc_werklijsten      wlst
WHERE  wlst.kafd_id = kafd.kafd_id
AND    wlit.meet_id = meet.meet_id
AND    meet.meti_id = meti.meti_id
AND    meti.stgr_id = stgr.stgr_id (+)
AND    wlit.wlst_id = wlst.wlst_id;

/
QUIT
