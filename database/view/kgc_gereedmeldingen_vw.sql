CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_GEREEDMELDINGEN_VW" ("GERE_ID", "KAFD_CODE", "KAFD_ID", "ONGR_ID", "ONDE_ID", "TAAL_ID", "INST_ID", "RELA_ID", "INDICATIE", "PERSOON", "AANVRAGER", "GEADRESSEERDE", "ONDERZOEKNR", "DATUM_AUTORISATIE", "ONDE_REFERENTIE") AS
  SELECT gere.gere_id
,      kafd.code kafd_code
,      onde.kafd_id
,      onde.ongr_id
,      gere.onde_id
,      gere.taal_id
,      gere.inst_id
,      gere.rela_id
,      kgc_indi_00.indi_omschrijving( gere.onde_id ) indicatie
,      kgc_pers_00.persoon_info( null, gere.onde_id, null, 'BRIEF' ) persoon
,      kgc_adres_00.relatie( onde.rela_id, 'J' ) aanvrager
,      decode( gere.rela_id
                     , null, kgc_adres_00.instelling( gere.inst_id, 'J' )
                     , kgc_adres_00.relatie( gere.rela_id, 'J' )
                     ) geadresseerde
,      onde.onderzoeknr
,      onde.datum_autorisatie
,      onde.referentie
from kgc_kgc_afdelingen kafd
,      kgc_onderzoeken onde
,      kgc_gereedmeldingen gere
where gere.onde_id = onde.onde_id
and    onde.kafd_id = kafd.kafd_id
and    onde.afgerond = 'J'
and    gere.datum_gereedgemeld is null
 ;

/
QUIT
