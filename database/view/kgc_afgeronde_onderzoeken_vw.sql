CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_AFGERONDE_ONDERZOEKEN_VW" ("ONDE_ID", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ONDERZOEKNR", "FAMILIENUMMER", "RELA_AANSPREKEN", "INDI_CODE", "UITS_TEKST", "ONDE_DATUM_AUTORISATIE", "KAFD_CODE", "ONGR_CODE") AS
  SELECT onde.onde_id
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      onde.onderzoeknr
,      kgc_fami_00.familie_bij_onderzoek( onde.onde_id ) familienummer
,      rela.aanspreken rela_aanspreken
,      kgc_indi_00.indi_code( onde.onde_id ) indi_code
,      uits.tekst uits_tekst
,      onde.datum_autorisatie onde_datum_autorisatie
,      kafd.code kafd_code
,      ongr.code ongr_code
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_personen pers
,      kgc_relaties rela
,      kgc_uitslagen uits
,      kgc_onderzoeken onde
where onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.pers_id = pers.pers_id
and    onde.rela_id = rela.rela_id
and    onde.onde_id = uits.onde_id (+)
and    onde.afgerond = 'J'
and    onde.datum_autorisatie is not null
 ;

/
QUIT
