CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MODULES_VW" ("ID", "SHORT_NAME", "NAME", "TITLE", "MTYPE", "PURPOSE") AS
  SELECT id
, short_name
, name
, title
, mtype
, purpose
from qms_modules
 ;

/
QUIT
