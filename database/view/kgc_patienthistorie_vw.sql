CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_PATIENTHISTORIE_VW" ("SOORT_GEGEVENS", "KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "PERS_ID", "ONDE_ID", "ONDERZOEKNR", "TOEVOEGING", "DATUM_BINNEN", "RELA_AANSPREKEN", "REDEN", "AFGEROND", "ONUI_OMSCHRIJVING") AS
  SELECT                                       -- onderzoeken van de persoon:
         'O' soort_gegevens,
          onde.kafd_id,
          kafd.code kafd_code,
          onde.ongr_id,
          ongr.code ongr_code,
          onde.pers_id,
          onde.onde_id,
          onde.onderzoeknr,
          DECODE (onde.foet_id,
                  NULL, NULL,
                  'foetus: '
                  || kgc_foet_00.foetus_info (onde.foet_id,
                                              NULL,
                                              NULL,
                                              'J'))
          || kgc_onde_00.counselor (onde.onde_id)
             toevoeging,
          onde.datum_binnen,
          rela.aanspreken rela_aanspreken,
          REPLACE (kgc_indi_00.onderzoeksindicaties (onde.onde_id),
                   CHR (10),
                   '; ')
             reden,
          onde.afgerond,
          onui.omschrijving onui_omschrijving
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_onderzoek_uitslagcodes onui,
          kgc_relaties rela,
          kgc_onderzoeken onde
    WHERE     onde.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onui_id = onui.onui_id(+)
   UNION ALL -- onderzoeken van de moeder waarbij de persoon als foetus geregistreerd staat:
   SELECT 'O',
          onde.kafd_id,
          kafd.code kafd_code,
          onde.ongr_id,
          ongr.code ongr_code,
          foet.pers_id_geboren_als,
          onde.onde_id,
          onde.onderzoeknr,
             'als foetus: '
          || kgc_foet_00.foetus_info (onde.foet_id,
                                      NULL,
                                      NULL,
                                      'J')
          || kgc_onde_00.counselor (onde.onde_id)
             toevoeging,
          onde.datum_binnen,
          rela.aanspreken rela_aanspreken,
          REPLACE (kgc_indi_00.onderzoeksindicaties (onde.onde_id),
                   CHR (10),
                   '; ')
             reden,
          onde.afgerond,
          onui.omschrijving onui_omschrijving
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_onderzoek_uitslagcodes onui,
          kgc_relaties rela,
          kgc_onderzoeken onde,
          kgc_foetussen foet
    WHERE     onde.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onui_id = onui.onui_id(+)
          AND onde.foet_id = foet.foet_id
   UNION ALL                      -- onderzoeks-betrokkenheden van de persoon:
   SELECT DISTINCT
          'B',
          onde.kafd_id,
          kafd.code,
          onde.ongr_id,
          ongr.code,
          onbe.pers_id,
          onde.onde_id,
          onde.onderzoeknr,
             DECODE (onbe.rol, NULL, NULL, onbe.rol || ' ')
          || 'betr. '
          || DECODE (onbe.foet_id,
                     NULL, NULL,
                     'foetus: '
                     || kgc_foet_00.foetus_info (onbe.foet_id,
                                                 NULL,
                                                 NULL,
                                                 'J'))
          || kgc_onde_00.counselor (onde.onde_id),
          onde.datum_binnen,
          rela.aanspreken,
          REPLACE (kgc_indi_00.onderzoeksindicaties (onde.onde_id),
                   CHR (10),
                   '; '),
          onde.afgerond,
          onui.omschrijving
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_onderzoek_uitslagcodes onui,
          kgc_relaties rela,
          kgc_onderzoeken onde,
          kgc_onderzoek_betrokkenen onbe
    WHERE     onde.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onde_id = onbe.onde_id
          AND NOT (onbe.pers_id = onde.pers_id
                   AND NVL (onbe.foet_id, -9) = NVL (onde.foet_id, -9)) -- alleen betrokkenen <> aan de onderzoekspersoon/foetus zelf
          AND onde.onui_id = onui.onui_id(+)
   UNION ALL -- onderzoeks-betrokkenheden van de moeder waarbij de persoon als foetus geregistreerd staat:
   SELECT DISTINCT
          'B',
          onde.kafd_id,
          kafd.code,
          onde.ongr_id,
          ongr.code,
          foet.pers_id_geboren_als,
          onde.onde_id,
          onde.onderzoeknr,
             DECODE (onbe.rol, NULL, NULL, onbe.rol || ' ')
          || 'betr. '
          || 'als foetus: '
          || kgc_foet_00.foetus_info (onbe.foet_id,
                                      NULL,
                                      NULL,
                                      'J')
          || kgc_onde_00.counselor (onde.onde_id),
          onde.datum_binnen,
          rela.aanspreken,
          REPLACE (kgc_indi_00.onderzoeksindicaties (onde.onde_id),
                   CHR (10),
                   '; '),
          onde.afgerond,
          onui.omschrijving
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_onderzoek_uitslagcodes onui,
          kgc_relaties rela,
          kgc_onderzoeken onde,
          kgc_onderzoek_betrokkenen onbe,
          kgc_foetussen foet
    WHERE     onde.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onde_id = onbe.onde_id
          AND NOT (onbe.pers_id = onde.pers_id
                   AND NVL (onbe.foet_id, -9) = NVL (onde.foet_id, -9)) -- alleen betrokkenen <> aan de onderzoekspersoon/foetus zelf
          AND onde.onui_id = onui.onui_id(+)
          AND onbe.foet_id = foet.foet_id
   UNION ALL                                 -- losse monsters van de persoon:
     SELECT 'M',
            mons.kafd_id,
            kafd.code kafd_code,
            mons.ongr_id,
            ongr.code ongr_code,
            mons.pers_id,
            TO_NUMBER (NULL)                                        -- onde_id
                            ,
            NULL                                                -- onderzoeknr
                ,
            /* MGOL: 07-04-2013 gewijzigd ivm samenwerking Nijmegen-Maastricht, onderstaande code is vervangen door de code eronder
            'monsterafname'
            || DECODE (mons.foet_id,
                       NULL, NULL,
                       ', foetus: '
                       || kgc_foet_00.foetus_info (mons.foet_id,
                                                   NULL,
                                                   NULL,
                                                   'J')),
            */
            -- MGOL: 07-04-2013 begin wijziging
            SUBSTR (
               DECODE (mons.foet_id,
                       NULL, NULL,
                          'foetus: '
                       || kgc_foet_00.foetus_info (mons.foet_id,
                                                   NULL,
                                                   NULL,
                                                   'J')
                       || ' | ')
               || listagg (INTE.CODE, ';')
                     WITHIN GROUP (ORDER BY MOIN.CREATION_DATE DESC),
               1,
               100),
            --MGOL: 07-04-2013 einde wijziging
            mons.datum_aanmelding,
            rela.aanspreken rela_aanspreken,
            'Monster ' || mons.monsternummer,
            'J',
            NULL                                          -- onui_omschrijving
       FROM kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_relaties rela,
            kgc_monsters mons,
            --MGOL: 07-04-2013 begin wijziging
            kgc_monster_indicaties moin,
            kgc_indicatie_teksten inte
      --MGOL: 07-04-2013 einde wijziging
      WHERE     mons.rela_id = rela.rela_id(+)
            AND mons.kafd_id = kafd.kafd_id
            AND mons.ongr_id = ongr.ongr_id
            --MGOL: 07-04-2013 begin wijziging
            AND mons.mons_id = moin.mons_id(+)
            AND moin.indi_id = inte.indi_id(+)
            --MGOL: 07-04-2013 einde wijziging
            AND (NOT EXISTS
                    (SELECT NULL
                       FROM kgc_onderzoek_monsters onmo
                      WHERE onmo.mons_id = mons.mons_id)
                 -- MGOL: 07-04-2013 begin wijziging
                 -- dit stuk toegevoegd om ook monsters die al aan onderzoek gekoppeld zijn te kunnen zien (alleen monsters met de indicatie)
                 OR mons.MONS_ID IN (SELECT moin.mons_id
                                       FROM kgc_monster_indicaties moin
                                      WHERE moin.mons_id = MONS.MONS_ID))
   GROUP BY MOIN.MONS_ID,
            mons.kafd_id,
            kafd.code,
            mons.ongr_id,
            ongr.code,
            mons.pers_id,
            mons.foet_id,
            mons.datum_aanmelding,
            rela.aanspreken,
            mons.monsternummer
   --MGOL: 07-04-2013 einde wijziging
   UNION ALL -- losse monsters van de moeder waarbij de persoon als foetus geregistreerd staat:
     SELECT 'M',
            mons.kafd_id,
            kafd.code kafd_code,
            mons.ongr_id,
            ongr.code ongr_code,
            foet.pers_id_geboren_als,
            TO_NUMBER (NULL)                                        -- onde_id
                            ,
            NULL                                                -- onderzoeknr
                ,
            /* MGOL: 07-04-2013 gewijzigd ivm samenwerking Nijmegen-Maastricht, onderstaande code is vervangen door de code eronder
              'monsterafname'
            || ', als foetus: '
            || kgc_foet_00.foetus_info (mons.foet_id,
                                        NULL,
                                        NULL,
                                        'J'),
            */
            -- MGOL: 07-04-2013 begin wijziging
            SUBSTR (
                  'Afname'
               || ', als foetus: '
               || kgc_foet_00.foetus_info (mons.foet_id,
                                           NULL,
                                           NULL,
                                           'J' || ' | ')
               || listagg (INTE.CODE, ';')
                     WITHIN GROUP (ORDER BY MOIN.CREATION_DATE DESC),
               1,
               100),
            --MGOL: 07-04-2013 einde wijziging
            mons.datum_aanmelding,
            rela.aanspreken rela_aanspreken,
            'Monster ' || mons.monsternummer,
            'J',
            NULL                                          -- onui_omschrijving
       FROM kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_relaties rela,
            kgc_monsters mons,
            kgc_foetussen foet,
            --MGOL: 07-04-2013 begin wijziging
            kgc_monster_indicaties moin,
            kgc_indicatie_teksten inte
      --MGOL: 07-04-2013 einde wijziging
      WHERE     mons.rela_id = rela.rela_id(+)
            AND mons.kafd_id = kafd.kafd_id
            AND mons.ongr_id = ongr.ongr_id
            --MGOL: 07-04-2013 begin wijziging
            AND mons.mons_id = moin.mons_id(+)
            AND moin.indi_id = inte.indi_id(+)
            --MGOL: 07-04-2013 einde wijziging
            AND (NOT EXISTS
                    (SELECT NULL
                       FROM kgc_onderzoek_monsters onmo
                      WHERE onmo.mons_id = mons.mons_id)
                 -- MGOL: 07-04-2013 begin wijziging
                 -- dit stuk toegevoegd om ook monsters die al aan onderzoek gekoppeld zijn te kunnen zien (alleen monsters met de indicatie)
                 OR mons.MONS_ID IN (SELECT moin.mons_id
                                       FROM kgc_monster_indicaties moin
                                      WHERE moin.mons_id = MONS.MONS_ID))
            --MGOL: 07-04-2013 einde wijziging
            AND mons.foet_id = foet.foet_id
   -- MGOL: 07-04-2013 begin wijziging
   GROUP BY MOIN.MONS_ID,
            mons.kafd_id,
            kafd.code,
            mons.ongr_id,
            ongr.code,
            mons.pers_id,
            mons.foet_id,
            mons.datum_aanmelding,
            rela.aanspreken,
            mons.monsternummer,
            foet.pers_id_geboren_als;

/
QUIT
