CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAG_BIJLAGE_UTMETAB_VW" ("ONDERZOEKNR", "MONSTERNUMMER", "MATE_OMSCHRIJVING", "DATUM_AANMELDING", "DATUM_AFNAME", "PERS_AANSPREKEN", "PERS_GESLACHT", "PERS_GEBOORTEDATUM", "STGR_OMSCHRIJVING", "CONCLUSIE", "STOF_OMSCHRIJVING", "STOFTEST_VOLGORDE", "MEETWAARDE", "MEETWAARDE_VOOR_DECIMAAL", "MEETWAARDE_NA_DECIMAAL", "NORMAALWAARDE_ONDERGRENS", "NORM_ONDERGRENS_VOOR_DECIMAAL", "NORM_ONDERGRENS_NA_DECIMAAL", "NORMAALWAARDE_BOVENGRENS", "NORM_BOVENGRENS_VOOR_DECIMAAL", "NORM_BOVENGRENS_NA_DECIMAAL", "MEETEENHEID", "MEET_UITSLAG", "MEET_UITSLAG_VOOR_DECIMAAL", "MEET_UITSLAG_NA_DECIMAAL", "MEET_UITSLAGEENHEID", "MEET_ID", "ONDE_ID", "KAFD_ID", "ONGR_ID", "TAAL_ID", "MONS_ID", "STGR_ID", "METI_ID") AS
  SELECT onde.onderzoeknr
, mons.monsternummer
, mate.omschrijving mate_omschrijving
, mons.datum_aanmelding
, mons.datum_afname
, pers.aanspreken pers_aanspreken
, pers.geslacht pers_geslacht
, pers.geboortedatum pers_geboortedatum
, stgr.omschrijving stgr_omschrijving
, to_char(meti.conclusie) --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014) --TO_CHAR(meti.conclusie)  changed to meti.conclusie by sachin for reverting changes of mantis 9355 in rework release 201502 12-06-2015 --Mantis 9355 to_char added to conclusie by SKA 29-03-2016 release 8.11.0.2
, stof.omschrijving stof_omschrijving
, kgc_sorteren.stoftest_volgorde( meti.stgr_id, stof.stof_id ) stoftest_volgorde
, meet.meetwaarde
, kgc_util_00.voor_deci_punt( meet.meetwaarde) meetwaarde_voor_decimaal
, kgc_util_00.na_deci_punt( meet.meetwaarde) meetwaarde_na_decimaal
, meet.normaalwaarde_ondergrens
, kgc_util_00.voor_deci_punt( meet.normaalwaarde_ondergrens ) norm_ondergrens_voor_decimaal
, kgc_util_00.na_deci_punt( meet.normaalwaarde_ondergrens ) norm_ondergrens_na_decimaal
, meet.normaalwaarde_bovengrens
, kgc_util_00.voor_deci_punt( meet.normaalwaarde_bovengrens ) norm_bovengrens_voor_decimaal
, kgc_util_00.na_deci_punt( meet.normaalwaarde_bovengrens ) norm_bovengrens_na_decimaal
, decode(meet.meeteenheid,NULL,NULL,'('||meet.meeteenheid||')') meeteenheid
, BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'UITSLAG' ) meet_uitslag
, kgc_util_00.voor_deci_punt( BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'UITSLAG' ) ) meet_uitslag_voor_decimaal
, kgc_util_00.na_deci_punt( BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'UITSLAG' ) ) meet_uitslag_na_decimaal
, decode( BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'EENHEID')
        , NULL, NULL
        ,'('||BAS_MEET_00.bepaal_uitslag_of_eenheid(meet.meet_id, 'EENHEID')||')'
        ) MEET_UITSLAGEENHEID
, meet.meet_id
, onmo.onde_id
, onde.kafd_id
, onde.ongr_id
, onde.taal_id
, mons.mons_id
, stgr.stgr_id
, meti.meti_id
from bas_meetwaarde_statussen mest
, kgc_materialen mate
, kgc_stoftesten stof
, kgc_stoftestgroepen stgr
, kgc_personen pers
, kgc_onderzoeken onde
, kgc_monsters mons
, bas_fracties frac
, bas_metingen meti
, bas_meetwaarden meet
, kgc_onderzoek_monsters onmo
where onmo.mons_id = mons.mons_id
and onmo.onde_id = onde.onde_id
and mons.pers_id = pers.pers_id
and mons.mate_id = mate.mate_id
and mons.mons_id = frac.mons_id
and frac.frac_id = meti.frac_id
and onde.onde_id = meti.onde_id
and meti.stgr_id = stgr.stgr_id (+)
and meti.meti_id = meet.meti_id (+)
and meet.stof_id = stof.stof_id (+)
and meet.mest_id = mest.mest_id (+)
and nvl( mest.in_uitslag, 'J' ) = 'J'
and bas_meet_00.afgerond( meet.meet_id) = 'J'
and exists
    ( select null
      from   kgc_stoftestgroep_gebruik stgg
      where  stgg.stgr_id = meti.stgr_id
      and    stgg.mate_id = mons.mate_id
      and    stgg.getal_opnemen = 'J'
    )
union
SELECT onde.onderzoeknr
, mons.monsternummer
, mate.omschrijving mate_omschrijving
, mons.datum_aanmelding
, mons.datum_afname
, pers.aanspreken pers_aanspreken
, pers.geslacht pers_geslacht
, pers.geboortedatum pers_geboortedatum
, stgr.omschrijving stgr_omschrijving
, to_char(meti.conclusie) --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014) --TO_CHAR(meti.conclusie)  changed to meti.conclusie by sachin for reverting changes of mantis 9355 in rework release 201502 16-06-2015 --Mantis 9355 to_char added to conclusie by SKA 29-03-2016 release 8.11.0.2
, null -- stof.omschrijving stof_omschrijving
, null -- kgc_sorteren.stoftest_volgorde( meti.stgr_id, stof.stof_id ) stoftest_volgorde
, null -- meet.meetwaarde
, null -- kgc_util_00.voor_deci_punt( meet.meetwaarde) meetwaarde_voor_decimaal
, null -- kgc_util_00.na_deci_punt( meet.meetwaarde) meetwaarde_na_decimaal
, null -- meet.normaalwaarde_ondergrens
, null -- kgc_util_00.voor_deci_punt( meet.normaalwaarde_ondergrens ) norm_ondergrens_voor_decimaal
, null -- kgc_util_00.na_deci_punt( meet.normaalwaarde_ondergrens ) norm_ondergrens_na_decimaal
, null -- meet.normaalwaarde_bovengrens
, null -- kgc_util_00.voor_deci_punt( meet.normaalwaarde_bovengrens ) norm_bovengrens_voor_decimaal
, null -- kgc_util_00.na_deci_punt( meet.normaalwaarde_bovengrens ) norm_bovengrens_na_decimaal
, null -- decode(meet.meeteenheid,NULL,NULL,'('||meet.meeteenheid||')') meeteenheid
, null -- BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'UITSLAG' ) meet_uitslag
, null -- kgc_util_00.voor_deci_punt( BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'UITSLAG' ) ) meet_uitslag_voor_decimaal
, null -- kgc_util_00.na_deci_punt( BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'UITSLAG' ) ) meet_uitslag_na_decimaal
, null -- decode( BAS_MEET_00.bepaal_uitslag_of_eenheid( meet.meet_id, 'EENHEID')
       -- , NULL, NULL
       -- ,'('||BAS_MEET_00.bepaal_uitslag_of_eenheid(meet.meet_id, 'EENHEID')||')'
       -- ) MEET_UITSLAGEENHEID
, to_number(null) -- meet.meet_id
, onmo.onde_id
, onde.kafd_id
, onde.ongr_id
, onde.taal_id
, mons.mons_id
, stgr.stgr_id
, meti.meti_id
from kgc_materialen mate
, kgc_stoftestgroepen stgr
, kgc_personen pers
, kgc_onderzoeken onde
, kgc_monsters mons
, bas_fracties frac
, bas_metingen meti
, kgc_onderzoek_monsters onmo
where onmo.mons_id = mons.mons_id
and onmo.onde_id = onde.onde_id
and mons.pers_id = pers.pers_id
and mons.mate_id = mate.mate_id
and mons.mons_id = frac.mons_id
and frac.frac_id = meti.frac_id
and onde.onde_id = meti.onde_id
and meti.stgr_id = stgr.stgr_id (+)
and meti.afgerond = 'J'
and ( exists
      ( select null
        from   kgc_stoftestgroep_gebruik stgg
        where  stgg.stgr_id = meti.stgr_id
        and    stgg.mate_id = mons.mate_id
        and    stgg.getal_opnemen = 'N'
      )
   or not exists
      ( select null
        from   bas_meetwaarden meet
        where  meet.meti_id = meti.meti_id
        and    meet.meetwaarde is not null
      )
    );

/
QUIT
