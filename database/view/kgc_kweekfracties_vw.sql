CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KWEEKFRACTIES_VW" ("KWFR_ID", "FRAC_ID", "CODE", "VERVALLEN", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "KWTY_ID", "KWEEKDUUR", "MEDE_ID_INZET", "DATUM_INZET", "MEDE_ID_UITHAAL", "DATUM_UITHAAL", "KWFR_ID_PARENT", "OMSCHRIJVING", "MEDE_ID_CONTROLE", "DATUM_CONTROLE", "MEDE_ID_OPSLAAN", "DATUM_OPSLAAN", "OPSLAG_ELDERS", "DATUM_UIT_TE_HALEN", "KWFR_SEQ_NUM") AS
  SELECT kwfr.kwfr_id kwfr_id
, kwfr.frac_id frac_id
, kwfr.code code
, kwfr.vervallen vervallen
, kwfr.created_by created_by
, kwfr.creation_date creation_date
, kwfr.last_updated_by last_updated_by
, kwfr.last_update_date last_update_date
, kwfr.kwty_id kwty_id
, kwfr.kweekduur kweekduur
, kwfr.mede_id_inzet mede_id_inzet
, kwfr.datum_inzet datum_inzet
, kwfr.mede_id_uithaal mede_id_uithaal
, kwfr.datum_uithaal datum_uithaal
, kwfr.kwfr_id_parent kwfr_id_parent
, kwfr.omschrijving omschrijving
, kwfr.mede_id_controle mede_id_controle
, kwfr.datum_controle datum_controle
, kwfr.mede_id_opslaan mede_id_opslaan
, kwfr.datum_opslaan datum_opslaan
, kwfr.opslag_elders opslag_elders
, kwfr.datum_uit_te_halen
, kwes.kwfr_seq_num kwfr_seq_num
FROM kgc_kweekfracties kwfr
, kgc_tmp_kweekfracties kwes
WHERE kwfr.kwfr_id = kwes.kwfr_id(+);

/
QUIT
