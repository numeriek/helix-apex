CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAG_ONDERZOEKEN_VW" ("PERS_ID", "PERS_AANSPREKEN", "PERS_ZISNR", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_ADRES", "PERS_ZOEKNAAM", "PERS_POSTCODE", "PERS_TELEFOON", "PERS_TELEFAX", "PERS_EMAIL", "PERS_WOONPLAATS", "FOET_ID", "ONDE_ID", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "SPOED", "ONDERZOEKNR", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "REFERENTIE", "INDICATIE", "REDEN", "AFGEROND", "STATUS", "MEDE_ID_CONTROLE", "ONGR_ID", "ONGR_CODE", "ONGR_OMSCHRIJVING", "RELA_ID", "RELA_CODE", "RELA_NAAM", "KAFD_ID", "KAFD_CODE", "KAFD_NAAM", "TAAL_ID", "TAAL_CODE", "REEDS_GEPRINT", "DATUM_AUTORISATIE", "UITS_ID", "BRTY_ID", "BRTY_CODE") AS
  SELECT onde.pers_id ,
pers.aanspreken pers_aanspreken ,
pers.zisnr pers_zisnr ,
pers.achternaam pers_achternaam ,
pers.geboortedatum pers_geboortedatum ,
pers.geslacht pers_geslacht ,
kgc_adres_00.persoon ( pers.pers_id, 'N' ) pers_adres ,
pers.zoeknaam pers_zoeknaam ,
pers.postcode pers_postcode ,
pers.telefoon pers_telefoon ,
pers.telefax pers_telefax ,
pers.email pers_email ,
pers.woonplaats ,
onde.foet_id ,
onde.onde_id ,
onde.onderzoekstype ,
onde.onderzoekswijze ,
onde.spoed ,
onde.onderzoeknr onderzoek ,
onde.datum_binnen ,
onde.geplande_einddatum ,
onde.referentie ,
kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie ,
onde.omschrijving reden ,
onde.afgerond ,
onde.status ,
onde.mede_id_controle ,
onde.ongr_id ,
ongr.code ongr_code ,
ongr.omschrijving ongr_omschrijving ,
onde.rela_id ,
rela.code rela_code ,
rela.aanspreken rela_aanspreken ,
onde.kafd_id ,
kafd.code kafd_code ,
kafd.naam kafd_naam ,
onde.taal_id ,
taal.code taal_code,
kgc_brie_00.uitslag_eerder_geprint( uits.uits_id, uits.brty_id ) reeds_geprint,
onde.datum_autorisatie datum_autorisatie,
uits.uits_id,
brty.brty_id,
brty.code brty_code
FROM
kgc_talen taal,
kgc_kgc_afdelingen kafd ,
kgc_onderzoeksgroepen ongr ,
kgc_relaties rela ,
kgc_personen pers ,
kgc_onderzoeken onde,
kgc_uitslagen uits,
kgc_brieftypes brty,
kgc_rapport_modules ramo
where onde.pers_id = pers.pers_id
and onde.rela_id = rela.rela_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
and onde.onde_id = uits.onde_id
and uits.tekst IS NOT NULL
and uits.brty_id IS NOT NULL
and onde.taal_id = taal.taal_id (+)
and brty.brty_id = uits.brty_id
and brty.ramo_id = ramo.ramo_id(+)
and onde.afgerond = 'J'
UNION ALL
SELECT onde.pers_id ,
pers.aanspreken pers_aanspreken ,
pers.zisnr pers_zisnr ,
pers.achternaam pers_achternaam ,
pers.geboortedatum pers_geboortedatum ,
pers.geslacht pers_geslacht ,
kgc_adres_00.persoon ( pers.pers_id, 'N' ) pers_adres ,
pers.zoeknaam pers_zoeknaam ,
pers.postcode pers_postcode ,
pers.telefoon pers_telefoon ,
pers.telefax pers_telefax ,
pers.email pers_email ,
pers.woonplaats ,
onde.foet_id ,
onde.onde_id ,
onde.onderzoekstype ,
onde.onderzoekswijze ,
onde.spoed ,
onde.onderzoeknr onderzoek ,
onde.datum_binnen ,
onde.geplande_einddatum ,
onde.referentie ,
kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie ,
onde.omschrijving reden ,
onde.afgerond ,
onde.status ,
onde.mede_id_controle ,
onde.ongr_id ,
ongr.code ongr_code ,
ongr.omschrijving ongr_omschrijving ,
onde.rela_id ,
rela.code rela_code ,
rela.aanspreken rela_aanspreken ,
onde.kafd_id ,
kafd.code kafd_code ,
kafd.naam kafd_naam ,
onde.taal_id ,
taal.code taal_code,
kgc_brie_00.uitslag_eerder_geprint( uits.uits_id, uits.brty_id ) reeds_geprint,
uits.datum_mede_id4 datum_autorisatie,
uits.uits_id,
brty.brty_id,
brty.code brty_code
FROM
kgc_talen taal,
kgc_kgc_afdelingen kafd ,
kgc_onderzoeksgroepen ongr ,
kgc_relaties rela ,
kgc_personen pers ,
kgc_onderzoeken onde,
kgc_uitslagen uits,
kgc_brieftypes brty,
kgc_rapport_modules ramo
where onde.pers_id = pers.pers_id
and onde.rela_id = rela.rela_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
and onde.onde_id = uits.onde_id
and uits.tekst IS NOT NULL
and uits.brty_id IS NOT NULL
and onde.taal_id = taal.taal_id (+)
and brty.brty_id = uits.brty_id
and brty.ramo_id = ramo.ramo_id(+)
and nvl(onde.afgerond,'N') = 'N'
and nvl(ramo.code,'xxx') = 'UITSLAG'
and uits.mede_id4 is not null
 ;

/
QUIT
