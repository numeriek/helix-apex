CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_VERTRAGINGSBRIEF_VW" ("PERS_ID", "PERS_AANSPREKEN", "PERS_ZISNR", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "RELA_ADRES", "ONDE_ID", "ONDERZOEKNR", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "ONDE_REFERENTIE", "INDI_ID", "INDICATIE", "RELA_ID", "KAFD_ID", "KAFD_CODE", "KAFD_NAAM", "ONGR_ID", "TAAL_ID", "BSN_OPGEMAAKT", "ONDERZOEKSTYPE") AS
  SELECT onde.pers_id pers_id
,      pers.aanspreken pers_aanspreken
,      pers.zisnr pers_zisnr
,      pers.achternaam pers_achternaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      kgc_adres_00.relatie( onde.rela_id, 'J') rela_adres
,      onde.onde_id onde_id
,      onde.onderzoeknr onderzoeknr
,      onde.datum_binnen datum_binnen
,      onde.geplande_einddatum geplande_einddatum
,      onde.referentie onde_referentie
,      kgc_indi_00.indi_id( onde.onde_id ) indi_id
,      kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie
,      onde.rela_id rela_id
,      onde.kafd_id kafd_id
,      kafd.code   kafd_code
,      kafd.naam kafd_naam
,      onde.ongr_id ongr_id
,      onde.taal_id taal_id
,      kgc_pers_00.bsn_opgemaakt(pers.pers_id) bsn_opgemaakt
,      onde.onderzoekstype onderzoekstype
from   kgc_kgc_afdelingen kafd
,      kgc_personen pers
,      kgc_onderzoeken onde
where onde.pers_id = pers.pers_id
and    onde.kafd_id = kafd.kafd_id
and    onde.afgerond = 'N'
 ;

/
QUIT
