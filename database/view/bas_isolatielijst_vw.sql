CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_ISOLATIELIJST_VW" ("PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_ZOEKNAAM", "PERS_ZISNR", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "FAMILIE", "KAFD_CODE", "ONGR_CODE", "MONSTERNUMMER", "DATUM_AANMELDING", "DATUM_AFNAME", "MONS_VOLUME", "MONS_AANTAL_BUIZEN", "SPOED", "MONS_COMMENTAAR", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "GOEDGEKEURD", "PERS_ID", "KAFD_ID", "ONGR_ID", "MONS_ID", "MATE_ID", "FRAC_ID", "FRAC_ID_PARENT", "TYPE_AFLEIDING", "FRTY_CODE", "FRAC_SEQ_NUM", "CONTROLEREN") AS
  SELECT pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.zoeknaam pers_zoeknaam
,      pers.zisnr pers_zisnr
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      bas_isol_00.familie( mons.pers_id, mons.mons_id ) familie
,      kafd.code kafd_code
,      ongr.code ongr_code
,      nvl( frac2.fractienummer, mons.monsternummer ) monsternummer
,      mons.datum_aanmelding datum_aanmelding
,      mons.datum_afname datum_afname
,      mons.hoeveelheid_monster mons_volume
,      decode(frac.frac_id_parent,null,mons.aantal_buizen,1) mons_aantal_buizen
,      bas_isol_00.spoed( mons.mons_id ) spoed
,      mons.commentaar mons_commentaar
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.fractienummer fractienummer
,      bas_isol_00.goedgekeurd( frac.frac_id) goedgekeurd
,      pers.pers_id pers_id
,      kafd.kafd_id kafd_id
,      ongr.ongr_id ongr_id
,      mons.mons_id mons_id
,      mate.mate_id mate_id
,      frac.frac_id frac_id
,      frac.frac_id_parent frac_id_parent
,      frac.type_afleiding type_afleiding
,      frty.code frty_code
,      isot.frac_seq_num frac_seq_num -- Added by Athar Shaikh for Mantis-3760 on 04-04-2016
,      isot.controleren controleren   -- Added by Athar Shaikh for Mantis-3760 on 04-04-2016
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_materialen mate
,      bas_fracties frac
,      bas_fractie_types frty
,      bas_fracties frac2
,      kgc_tmp_isolatielijst isot -- Added by Athar Shaikh for Mantis-3760 on 04-04-2016
where  kafd.kafd_id = mons.kafd_id
and    ongr.ongr_id = mons.ongr_id
and    pers.pers_id = mons.pers_id
and    mons.mons_id = frac.mons_id
and    mate.mate_id = mons.mate_id
and    frac.status = 'A'
and    frty.frty_id(+) = frac.frty_id
and    frac2.frac_id(+) = frac.frac_id_parent
AND    frac.frac_id = isot.frac_id(+)  -- Added by Athar Shaikh for Mantis-3760 on 04-04-2016;

/
QUIT
