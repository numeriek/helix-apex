CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCMETI10_ONMO_VW" ("ONGR_CODE", "ONDERZOEKNR", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "ONDE_STATUS", "FAMILIENUMMER", "INDI_CODE", "INDI_OMSCHRIJVING", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "FRAC_STATUS", "STGR_CODE", "SPOED", "STOF_CODE", "STOF_OMSCHRIJVING", "MEDE_CODE", "AFGEROND", "MEET_ID", "STOF_ID", "METI_ID", "ONMO_ID", "ONDE_ID", "FRAC_ID", "STAN_ID", "STGR_ID", "MONS_ID", "MATE_ID", "PERS_ID", "KAFD_ID", "ONGR_ID") AS
  SELECT ongr.code ongr_code
,      onde.onderzoeknr
,      onde.onderzoekstype
,      onde.onderzoekswijze
,      onde.status onde_status
,      kgc_fami_00.familie_bij_onderzoek(onde.onde_id, 'N') familienummer
,      kgc_indi_00.indi_code( onde.onde_id ) indi_code
,      kgc_indi_00.indi_omschrijving( onde.onde_id ) indi_omschrijving
,      mons.monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.fractienummer
,      frac.status frac_status
,      null  stgr_code
,      onde.spoed
,      null stof_code
,      null stof_omschrijving
,      null mede_code
,      'N' afgerond
,      to_number(null) meet_id
,      to_number(null) stof_id
,      to_number(null) meti_id
,      onmo.onmo_id
,      onde.onde_id
,      frac.frac_id
,      to_number(null) stan_id
,      to_number(null) stgr_id
,      frac.mons_id
,      mons.mate_id
,      mons.pers_id
,      mons.kafd_id
,      mons.ongr_id
from   kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      kgc_onderzoek_monsters onmo
where onmo.onde_id = onde.onde_id
and    onmo.mons_id = mons.mons_id
and    frac.mons_id = mons.mons_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mate_id = mate.mate_id
and    nvl(onde.afgerond,'N') = 'N'
 ;

/
QUIT
