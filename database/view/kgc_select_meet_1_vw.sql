CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_SELECT_MEET_1_VW" ("PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_ZOEKNAAM", "PERS_ZISNR", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "INDICATIE", "MONSTERNUMMER", "MONS_DATUM_AFNAME", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "FRTY_CODE", "FRTY_OMSCHRIJVING", "STGR_CODE", "STGR_OMSCHRIJVING", "METI_DATUM_AANMELDING", "SPOED", "RUNNUMMER", "STOF_CODE", "STOF_OMSCHRIJVING", "MEETWAARDE", "MEETEENHEID", "MEET_COMMENTAAR", "PERS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "MONS_ID", "METI_ID", "MEET_ID", "STOF_ID", "MWST_ID", "MEST_ID", "MEDE_ID", "MEDE_ID_CONTROLE") AS
  SELECT pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.zoeknaam pers_zoeknaam
,      pers.zisnr pers_zisnr
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr
,      kgc_indi_00.onderzoeksreden(onde.onde_id ) indicatie
,      mons.monsternummer
,      mons.datum_afname mons_datum_afname
,      mons.mate_id
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.fractienummer
,      frty.code frty_code
,      frty.omschrijving frty_omschrijving
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.datum_aanmelding meti_datum_aanmelding
,      decode( onde.spoed
             , 'J', 'J'
             , decode(meti.prioriteit
                     ,'J', 'J'
                     ,meet.spoed )
              ) spoed
,      runs.runnummer
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.tonen_als meetwaarde
,      meet.meeteenheid meeteenheid
,      meet.commentaar meet_commentaar
,      pers.pers_id
,      kafd.kafd_id
,      ongr.ongr_id
,      onde.onde_id
,      mons.mons_id
,      meti.meti_id
,      meet.meet_id
,      stof.stof_id
,      meet.mwst_id
,      meet.mest_id
,      meet.mede_id
,      meet.mede_id_controle
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      bas_fractie_types frty
,      bas_runs runs
,      kgc_materialen mate
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      kgc_stoftestgroepen stgr
,      bas_metingen meti
,      kgc_stoftesten stof
,      bas_meetwaarden meet
where mons.kafd_id = kafd.kafd_id
and    mons.ongr_id = ongr.ongr_id
and    mons.pers_id = pers.pers_id
and    mons.mate_id = mate.mate_id
and    mons.mons_id = frac.mons_id
and    frac.frty_id = frty.frty_id (+)
and    frac.frac_id = meti.frac_id
and    nvl( frac.status, 'O' ) not in ( 'M', 'V' )
and    meti.onde_id = onde.onde_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.meti_id = meet.meti_id
and    meet.stof_id = stof.stof_id
and    meet.runs_id = runs.runs_id (+)
and    onde.afgerond = 'N'
and    meti.afgerond = 'N'
and    meet.afgerond = 'N'
and    meet.meet_reken <> 'R'
 ;

/
QUIT
