CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_AANVRAGERS_VW" ("RELA_ID", "CODE", "AANSPREKEN", "RELATIE_TYPE", "ADRES", "POSTCODE", "WOONPLAATS", "LAND", "TELEFOON", "TELEFAX", "EMAIL", "VERVALLEN", "SPEC_ID", "SPEC_CODE", "SPEC_OMSCHRIJVING", "INST_ID", "INST_CODE", "INST_NAAM", "AFDG_ID", "AFDG_CODE", "AFDG_OMSCHRIJVING") AS
  SELECT rela.rela_id
  ,      rela.code
  ,      rela.aanspreken
  ,      rela.relatie_type
  ,      rela.adres
  ,      rela.postcode
  ,      rela.woonplaats
  ,      rela.land
  ,      rela.telefoon
  ,      rela.telefax
  ,      rela.email
  ,      rela.vervallen
  ,      rela.spec_id
  ,      spec.code spec_code
  ,      spec.omschrijving spec_omschrijving
  ,      rela.inst_id
  ,      inst.code inst_code
  ,      inst.naam inst_naam
  ,      rela.afdg_id
  ,      afdg.code afdg_code
  ,      afdg.omschrijving afdg_omschrijving
  from   kgc_specialismen spec
  ,      kgc_instellingen inst
  ,      kgc_afdelingen afdg
  ,      kgc_relaties rela
where rela.inst_id = inst.inst_id (+)
  and    rela.afdg_id = afdg.afdg_id (+)
  and    rela.spec_id = spec.spec_id (+)
 ;

/
QUIT
