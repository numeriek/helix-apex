CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MONSTERLIJST_VW" ("MONS_ID", "MONSTERNUMMER", "FRACTIENUMMER", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "MATE_CODE", "MATE_OMSCHRIJVING", "ONDERZOEKNR", "ONDE_SPOED", "FAMILIENUMMER", "INDI_CODE", "INDI_OMSCHRIJVING", "KAFD_CODE", "ONGR_CODE", "MONS_DATUM_AANMELDING", "ALLEEN_VOOR_OPSLAG") AS
  SELECT mons.mons_id
,      mons.monsternummer
,      frac.fractienummer
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      onde.onderzoeknr
,      onde.spoed onde_spoed
,      kgc_fami_00.familie_bij_onderzoek( onde.onde_id ) familienummer
,      kgc_indi_00.indi_code( onde.onde_id ) indi_code
,      kgc_indi_00.onderzoeksreden( onde.onde_id ) indi_omschrijving
,      kafd.code kafd_code
,      ongr.code ongr_code
,      mons.datum_aanmelding mons_datum_aanmelding
,      mons.alleen_voor_opslag
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_personen pers
,      bas_fracties frac
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      kgc_onderzoek_monsters onmo
where mons.kafd_id = kafd.kafd_id
and    mons.ongr_id = ongr.ongr_id
and    mons.pers_id = pers.pers_id
and    mons.mate_id = mate.mate_id
and    mons.mons_id = frac.mons_id (+)
and    mons.mons_id = onmo.mons_id (+)
and    onmo.onde_id = onde.onde_id (+)
 ;

/
QUIT
