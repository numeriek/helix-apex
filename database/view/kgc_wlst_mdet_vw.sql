CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_WLST_MDET_VW" ("WLST_ID", "VOLGNUMMER", "MEET_ID", "FRACTIENUMMER", "ONDE_ID", "ONDERZOEKNR", "MONS_ID", "MONSTERNUMMER", "PERSOON_INFO", "STGR_CODE", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "AFGEROND", "SPOED", "COMMENTAAR", "MEETWAARDE", "MDET_VOLGORDE", "MDET_PROMPT", "MDET_WAARDE", "MDET_WAARDE_GEVULD_JN", "GEPLANDE_EINDDATUM", "ONDERZOEKSINDICATIES", "ONDERZOEKINDICATIE_CODES", "ZWANGERSCHAPSDUUR") AS
  SELECT distinct  wlit.wlst_id
,      wlit.volgnummer
,      wlit.meet_id
,      wimw.fractienummer
,      wimw.onde_id
,      wimw.onderzoeknr
,      wlit.mons_id
,      wimw.monsternummer
,      kgc_pers_00.persoon_info( wlit.mons_id, null, null, 'LANG' ) persoon_info
,      wimw.stgr_code
,      wlit.stof_id
,      wimw.stof_code
,      wimw.stof_omschrijving
,      wimw.afgerond
,      wimw.prioriteit
,      wimw.commentaar
,      wimw.meetwaarde
,      mdet.volgorde
,      mdet.prompt
,     decode( mdet.waarde
                    , null, null
                    , nvl( bas_mdet_00.detail_waarde( wlit.meet_id, mdet.volgorde ), mdet.waarde )
                    ) mdet_waarde
-- Mantis 711, definitie mdet_waarde_gevuld_jn aangepast
--,     decode( mdet.waarde
--                    , null, 'N'
--                    , 'J'
--                    ) mdet_waarde_gevuld_jn
,      bas_mdet_00.prompt_gevuld_jn( wlit.wlst_id, mdet.volgorde) mdet_waarde_gevuld_jn
,      wimw.geplande_einddatum
,      wimw.onderzoeksindicaties
,      kgc_indi_00.onde_indicatie_codes( wimw.onde_id )
,      kgc_zwan_00.duur(wimw.onde_id) zwangerschapsduur
from bas_meetwaarde_details mdet
,      kgc_werklijst_meetwaarden_vw wimw
,     kgc_werklijst_items wlit
where wlit.meet_id = wimw.meet_id
and     wlit.meet_id = mdet.meet_id (+)
 ;

/
QUIT
