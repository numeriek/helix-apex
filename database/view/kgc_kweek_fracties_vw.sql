CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KWEEK_FRACTIES_VW" ("KAFD_ID", "ONGR_ID", "MONS_ID", "KAFD_CODE", "ONGR_CODE", "MONSTERNUMMER", "PERSOON", "DATUM_AANMELDING", "MATE_CODE", "MATE_OMSCHRIJVING", "FRAC_ID", "FRACTIENUMMER", "FRTY_ID", "FRTY_CODE", "FRTY_OMSCHRIJVING", "LETTER_FRACTIENR", "FRAC_STATUS", "AANTAL_KWEEKFRACTIES", "AANTAL_INGEZET", "AANTAL_UITGEHAALD", "HOOGST_DATUM_UITHAAL", "EMBR_ID", "EMBRYONR") AS
  SELECT mons.kafd_id  kafd_id
,      mons.ongr_id ongr_id
,      mons.mons_id mons_id
,      kafd.code kafd_code
,      ongr.code ongr_code
,      mons.monsternummer monsternummer
,      kgc_pers_00.persoon_info( mons.mons_id, null ) persoon
,      mons.datum_aanmelding datum_aanmelding
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.frac_id frac_id
,      frac.fractienummer fractienummer
,      frac.frty_id frty_id
,      frty.code frty_code
,      frty.omschrijving frty_omschrijving
,      frty.letter_fractienr letter_fractienr
,      frac.status frac_status
,      count( kwfr.kwfr_id ) aantal_kweekfracties
,      count( decode( kwfr.datum_inzet
                    , null, null
					, kwfr.kwfr_id
					)
	        ) aantal_kwfr_ingezet
,      count( decode( kwfr.datum_uithaal
                    , null, null
					, kwfr.kwfr_id
					)
	        ) aantal_kwfr_uitgehaald
,      max( kwfr.datum_uithaal ) hoogste_datum_uithaal
,    embr.embr_id embr_id
,   embr.embryonr embryonr
from   bas_fractie_types frty
,      kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_monsters mons
,      kgc_kweekfracties kwfr
,      bas_fracties frac
,     kgc_embryos embr
where mons.kafd_id = kafd.kafd_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mate_id = mate.mate_id
and    mons.mons_id = frac.mons_id (+)
and    frac.frty_id = frty.frty_id (+)
and    nvl( frac.status, 'O' ) in ( 'A', 'O' )
and    frac.frac_id = kwfr.frac_id (+)
and    nvl( kwfr.vervallen, 'N' ) = 'N'
and   embr.embr_id(+)=mons.embr_id
group by mons.kafd_id
,      mons.ongr_id
,      mons.mons_id
,      kafd.code
,      ongr.code
,      mons.monsternummer
,      mons.datum_aanmelding
,      mate.code
,      mate.omschrijving
,      frac.frac_id
,      frac.fractienummer
,      frac.frty_id
,      frty.code
,      frty.omschrijving
,      frty.letter_fractienr
,      frac.status
,     embr.embr_id
,    embr.embryonr;

/
QUIT
