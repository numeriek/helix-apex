CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAG_BIJLAGE_VW" ("ONDERZOEKNR", "MONSTERNUMMER", "MATE_OMSCHRIJVING", "DATUM_AANMELDING", "DATUM_AFNAME", "PERS_AANSPREKEN", "PERS_GESLACHT", "PERS_GEBOORTEDATUM", "STGR_OMSCHRIJVING", "CONCLUSIE", "STOF_OMSCHRIJVING", "STOFTEST_VOLGORDE", "MEETWAARDE", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "MEETEENHEID", "MEET_ID", "ONDE_ID", "KAFD_ID", "ONGR_ID", "TAAL_ID") AS
  SELECT onde.onderzoeknr
, mons.monsternummer
, mate.omschrijving mate_omschrijving
, mons.datum_aanmelding
, mons.datum_afname
, pers.aanspreken pers_aanspreken
, pers.geslacht pers_geslacht
, pers.geboortedatum pers_geboortedatum
, stgr.omschrijving stgr_omschrijving
, to_char(meti.conclusie) --Mantis 9355 to_char added to conclusie by SKA 29-03-2016 release 8.11.0.2
, stof.omschrijving stof_omschrijving
, kgc_sorteren.stoftest_volgorde( meti.stgr_id, stof.stof_id ) stoftest_volgorde
, bas_meet_00.toon_meetwaarde( meet.meet_id, null, 'WAARDE' ) meetwaarde
, meet.normaalwaarde_ondergrens
, meet.normaalwaarde_bovengrens
, bas_meet_00.toon_meetwaarde( meet.meet_id, null, 'EENHEID' ) meeteenheid
, meet.meet_id
, onmo.onde_id
, onde.kafd_id
, onde.ongr_id
, onde.taal_id
from bas_meetwaarde_statussen mest
, kgc_materialen mate
, kgc_stoftesten stof
, kgc_stoftestgroepen stgr
, kgc_personen pers
, kgc_onderzoeken onde
, kgc_monsters mons
, bas_fracties frac
, bas_metingen meti
, bas_meetwaarden meet
, kgc_onderzoek_monsters onmo
where onmo.mons_id = mons.mons_id
and onmo.onde_id = onde.onde_id
and mons.pers_id = pers.pers_id
and mons.mate_id = mate.mate_id
and mons.mons_id = frac.mons_id
and frac.frac_id = meti.frac_id
and onde.onde_id = meti.onde_id
and meti.stgr_id = stgr.stgr_id (+)
and meti.meti_id = meet.meti_id
and meet.stof_id = stof.stof_id
and meet.mest_id = mest.mest_id (+)
and nvl( mest.in_uitslag, 'J' ) = 'J'
and bas_meet_00.afgerond( meet.meet_id) = 'J';

/
QUIT
