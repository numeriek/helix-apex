CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAGBRIEF_UTMETAB_1_VW" ("KOPIE", "BRIEFTYPE", "GEADRESSEERDE", "ONDERZOEKNR", "PERSOON", "GESLACHT", "GEBOORTEDATUM", "ONDE_REFERENTIE", "AFGEROND", "DATUM_UITSLAG", "DATUM_AUTORISATIE", "AUTORISATOR", "ONDERTEKENAAR1", "ONDERTEKENAAR2", "ONDERTEKENAAR3", "UITSLAG_TEKST", "TOELICHTING", "UITS_ID", "BRTY_ID", "KOHO_ID", "PERS_ID", "ONDE_ID", "KAFD_ID", "ONGR_ID", "TAAL_ID", "RELA_ID", "INDI_ID", "KAFD_CODE", "ONGR_CODE", "MEDE_ID_AUTORISATOR", "MEDE_ID_ONDERTEKENAAR1", "MEDE_ID_ONDERTEKENAAR2", "MEDE_ID_ONDERTEKENAAR3", "MATERIAAL", "AFNAME_DATUM", "BSN_OPGEMAAKT", "DATUM_BRIEF") AS
  SELECT 'N' kopie
,     brty.omschrijving brieftype
,     kgc_adres_00.relatie( uits.rela_id, 'J' ) geadresseerde
,     onde.onderzoeknr
,     pers.aanspreken  persoon
,     kgc_vert_00.vertaling('PERS_GESL',null,pers.geslacht,onde.taal_id) geslacht
,     pers.geboortedatum
,     onde.referentie onde_referentie
,     onde.afgerond
,     uits.datum_uitslag
,     onde.datum_autorisatie
,     decode(onde.afgerond,'J', nvl( mede_onde.formele_naam, mede_onde.naam ), nvl( mede_uits.formele_naam, mede_uits.naam ) ) autorisator
,     nvl( mede1.formele_naam, mede1.naam ) ondertekenaar1
,     nvl( mede2.formele_naam, mede2.naam ) ondertekenaar2
,     nvl( mede3.formele_naam, mede3.naam ) ondertekenaar3
,     decode( substr(ltrim(uits.tekst,' '),1,1)
            , upper(substr(ltrim(uits.tekst,' '),1,1)), uits.tekst
            , pers.aanspreken||' '||uits.tekst
            ) uitslag_tekst
,     uits.toelichting
,     uits.uits_id
,     uits.brty_id
,     to_number(null) koho_id
,     onde.pers_id
,     onde.onde_id
,     onde.kafd_id
,     onde.ongr_id
,     onde.taal_id
,     uits.rela_id
,     kgc_indi_00.indi_id(onde.onde_id) indi_id
,     kafd.code kafd_code
,     ongr.code ongr_code
,     decode(onde.afgerond,'J' , mede_onde.mede_id, mede_uits.mede_id) mede_id_autorisator
,     mede1.mede_id mede_id_ondertekenaar1
,     mede2.mede_id mede_id_ondertekenaar2
,     mede3.mede_id mede_id_ondertekenaar3
,     mate.omschrijving materiaal
,     mons.datum_afname afname_datum
,     kgc_pers_00.bsn_opgemaakt(pers.pers_id) bsn_opgemaakt
,     kgc_brie_00.datum_huidige_brief(uits.uits_id) datum_brief
FROM  kgc_kgc_afdelingen kafd
,     kgc_onderzoeksgroepen ongr
,     kgc_brieftypes brty
,     kgc_relaties aanv
,     kgc_medewerkers mede_onde
,     kgc_medewerkers mede_uits
,     kgc_medewerkers mede1
,     kgc_medewerkers mede2
,     kgc_medewerkers mede3
,     kgc_personen pers
,     kgc_onderzoeken onde
,     kgc_uitslagen uits
,     kgc_onderzoek_monsters onmo
,     kgc_monsters mons
,     kgc_materialen mate
where onde.kafd_id = kafd.kafd_id
and   onde.ongr_id = ongr.ongr_id
and   onde.pers_id = pers.pers_id
and   onde.onde_id = uits.onde_id
and   onde.rela_id = aanv.rela_id
and   onde.mede_id_autorisator = mede_onde.mede_id (+)
and   uits.mede_id4 = mede_uits.mede_id (+)
and   uits.mede_id  = mede1.mede_id (+)
and   uits.mede_id2 = mede2.mede_id (+)
and   uits.mede_id3 = mede3.mede_id (+)
and   uits.onde_id = onmo.onde_id (+)
and   onmo.mons_id = mons.mons_id (+)
and   onmo.mons_id = (select min(onmo2.mons_id)
                      from kgc_onderzoek_monsters onmo2
                      where uits.onde_id =onmo2.onde_id
                     )
and mons.mate_id = mate.mate_id (+)
and    uits.brty_id = brty.brty_id (+)
-- and    uits.tekst is not null : mk 21-07-2005 lege uitslagen mogen ook van Remko
 ;

/
QUIT
