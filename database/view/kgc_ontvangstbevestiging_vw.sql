CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONTVANGSTBEVESTIGING_VW" ("KAFD_CODE", "ONDERZOEKNR", "RELA_CODE", "GEADRESSEERDE", "TAAL_CODE", "REFERENTIE", "DATUM_BINNEN", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "INDICATIE", "PRINT_DATUM", "KAFD_ID", "ONGR_ID", "ONDE_ID", "PERS_ID", "RELA_ID", "TAAL_ID") AS
  SELECT kafd.code kafd_code
,      onde.onderzoeknr
,      rela.code rela_code
,      kgc_adres_00.relatie( onde.rela_id, 'J' ) geadresseerde
,      taal.code taal_code
,      nvl( kgc_fami_00.familie_bij_onderzoek( onde.onde_id, 'N' )
          , onde.onderzoeknr
          ) referentie
,      onde.datum_binnen
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      kgc_vert_00.vertaling( 'PERS_GESL'
                            , null
                            , pers.geslacht
                            , onde.taal_id
                            ) pers_geslacht
,      kgc_vert_00.vertaling( 'INDI'
                            , nvl( kgc_indi_00.indi_id( onde.onde_id ), -9999999999 )
                            , onde.taal_id
                            ) indicatie
,      kgc_brie_00.ontvangstbevest_geprint_op( onde.onde_id ) print_datum
,      onde.kafd_id
,      onde.ongr_id
,      onde.onde_id
,      onde.pers_id
,      onde.rela_id
,      onde.taal_id
from   kgc_talen taal
,      kgc_kgc_afdelingen kafd
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
  WHERE onde.kafd_id = kafd.kafd_id
and    onde.pers_id = pers.pers_id
and    onde.rela_id = rela.rela_id
and    onde.taal_id = taal.taal_id (+)
and   onde.afgerond = 'N'
 ;

/
QUIT
