CREATE OR REPLACE FORCE VIEW "HELIX"."ENZ_UITSLAG_OVZ_SUP_VW" ("MONS_ID", "MONSTERNUMMER", "FRAC_ID", "FRACTIENUMMER", "MEET_ID", "STOF_ID", "CODE", "OMSCHRIJVING", "UITSLAG", "MEETEENHEID", "RANGE", "COX", "COX_EENHEID", "COX_ONDER", "COX_BOVEN", "CS", "CS_EENHEID", "CS_ONDER", "CS_BOVEN", "EIWIT", "EIWIT_EENHEID", "EIWIT_ONDER", "EIWIT_BOVEN") AS
  SELECT frac.mons_id,
       mons.monsternummer,
       frac.frac_id,
       frac.fractienummer,
       meet.meet_id,
       meet.stof_id,
       stof.code,
       stof.omschrijving,
       meet.meetwaarde uitslag,
       meet.meeteenheid,
       meet.normaalwaarde_ondergrens||' - '
||normaalwaarde_bovengrens range,
       bas_bere_00.uitslag(meet_id,'COX','UITSLAG')   cox,
       bas_bere_00.uitslag(meet_id,'COX','EENHEID')   cox_eenheid,
       bas_bere_00.uitslag(meet_id,'COX','ONDER')     cox_onder,
       bas_bere_00.uitslag(meet_id,'COX','BOVEN')     cox_boven,
       bas_bere_00.uitslag(meet_id,'CS','UITSLAG')    cs,
       bas_bere_00.uitslag(meet_id,'CS','EENHEID')    cs_eenheid,
       bas_bere_00.uitslag(meet_id,'CS','ONDER')      cs_onder,
       bas_bere_00.uitslag(meet_id,'CS','BOVEN')      cs_boven,
       bas_bere_00.uitslag(meet_id,'EIWIT','UITSLAG') eiwit,
       bas_bere_00.uitslag(meet_id,'EIWIT','EENHEID') eiwit_eenheid,
       bas_bere_00.uitslag(meet_id,'EIWIT','ONDER')   eiwit_onder,
       bas_bere_00.uitslag(meet_id,'EIWIT','BOVEN')   eiwit_boven
from
       kgc_stoftesten  stof,
       kgc_monsters    mons,
       bas_fracties    frac,
       bas_metingen    meti,
       bas_meetwaarden meet
  WHERE stof.stof_id   =  meet.stof_id        and
       mons.mons_id   =  frac.mons_id        and
       substr(frac.fractienummer,1,1) = 'S'  and
       meti.frac_id   =  frac.frac_id        and
       meet.meti_id   =  meti.meti_id
 ;

/
QUIT
