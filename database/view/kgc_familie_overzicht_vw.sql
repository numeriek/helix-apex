CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FAMILIE_OVERZICHT_VW" ("FAMI_NAAM", "FAMILIENUMMER", "FAMI_ARCHIEF", "FRACTIENUMMER", "FRACTIESTATUS", "MATE_CODE", "ONDERZOEKNR", "ONDE_AFGEROND", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "FAMILIEONDERZOEKNR", "KAFD_CODE", "FAMI_ID", "PERS_ID", "FRAC_ID", "BETROKKEN", "OPENSTAAND_ONDERZOEK", "MONS_ID", "ONDE_ID") AS
  SELECT /* betrokkenen */
       fbev.fami_naam
,      fbev.familienummer
,      fbev.fami_archief
,      fbev.fractienummer
,      fbev.fractiestatus
,      fbev.mate_code
,      fbev.onderzoeknr
,      fbev.onde_afgerond
,      fbev.pers_aanspreken
,      fbev.pers_achternaam
,      fbev.pers_geboortedatum
,      fbev.pers_geslacht
,      fbev.familieonderzoeknr
,      fbev.kafd_code
,      fbev.fami_id
,      fbev.pers_id
,      fbev.frac_id
,      'J'
,     kgc_fami_00.openstaand_onderzoek( fbev.fami_id,  fbev.pers_id ) openstaand_onderzoek
,      to_number( null )
,      to_number( null )
from   kgc_familie_betrokkenen_vw fbev
union /* wel onderzoek, geen familieonderzoek */
select fami.naam
,      fami.familienummer
,      fami.archief
,      frac.fractienummer
,      frac.status
,      mate.code
,      onde.onderzoeknr
,      onde.afgerond
,      pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      null
,      kafd.code
,      fami.fami_id
,      pers.pers_id
,      frac.frac_id
,     'N'
,     kgc_fami_00.openstaand_onderzoek( fami.fami_id,  pers.pers_id )
,      mons.mons_id
,      onde.onde_id
from kgc_kgc_afdelingen kafd
,    kgc_materialen mate
,     kgc_families fami
,     kgc_personen pers
,     kgc_familie_leden fale
,     kgc_onderzoeken onde
,     kgc_monsters mons
,     bas_metingen meti
,     bas_fracties frac
where fale.fami_id = fami.fami_id
and   fale.pers_id = pers.pers_id
and   pers.pers_id = onde.pers_id
and   pers.pers_id = mons.pers_id
and   meti.onde_id = onde.onde_id
and   meti.frac_id = frac.frac_id
and   frac.mons_id = mons.mons_id
and   mons.kafd_id = kafd.kafd_id
and   mons.mate_id = mate.mate_id
and   not exists
      ( select null
        from   kgc_familie_onderzoeken faon
        ,      kgc_faon_betrokkenen fobe
        where  faon.faon_id = fobe.faon_id
        and    faon.fami_id = fami.fami_id
        and    fobe.onde_id = onde.onde_id
      )
union /* wel onderzoek, geen gebruikte fractie */
select fami.naam
,      fami.familienummer
,      fami.archief
,      null
,      null
,      null
,      onde.onderzoeknr
,      onde.afgerond
,      pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      null
,      kafd.code
,      fami.fami_id
,      pers.pers_id
,      to_number(null)
,     'N'
,     kgc_fami_00.openstaand_onderzoek( fami.fami_id,  pers.pers_id )
,      to_number( null )
,      onde.onde_id
from kgc_kgc_afdelingen kafd
,     kgc_families fami
,     kgc_personen pers
,     kgc_familie_leden fale
,     kgc_onderzoeken onde
where fale.fami_id = fami.fami_id
and   fale.pers_id = pers.pers_id
and   pers.pers_id = onde.pers_id
and   onde.kafd_id = kafd.kafd_id
and   not exists
      ( select null
        from   bas_metingen meti
        ,      bas_fracties frac
        ,      kgc_monsters mons
        where  meti.onde_id = onde.onde_id
        and    meti.frac_id = frac.frac_id
        and    frac.mons_id = mons.mons_id
        and mons.pers_id = pers.pers_id
      )
union /* wel fractie, geen onderzoek */
select fami.naam
,      fami.familienummer
,      fami.archief
,      frac.fractienummer
,      frac.status
,      mate.code
,      null
,      null
,      pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      null
,      kafd.code
,      fami.fami_id
,      pers.pers_id
,      frac.frac_id
,     'N'
,     kgc_fami_00.openstaand_onderzoek( fami.fami_id,  pers.pers_id )
,      mons.mons_id
,      to_number( null )
from kgc_kgc_afdelingen kafd
,     kgc_materialen mate
,     kgc_families fami
,     kgc_personen pers
,     kgc_familie_leden fale
,     kgc_monsters mons
,     bas_fracties frac
where fale.fami_id = fami.fami_id
and   fale.pers_id = pers.pers_id
and   pers.pers_id = mons.pers_id
and   mons.mons_id = frac.mons_id
and   mons.kafd_id = kafd.kafd_id
and   mons.mate_id = mate.mate_id
and   not exists
      ( select null
        from   bas_metingen meti
        ,           kgc_onderzoeken onde
        where meti.onde_id = onde.onde_id
       and       onde.pers_id = pers.pers_id
       and       meti.frac_id = frac.frac_id
      )
union /* geen fractie */
select fami.naam
,      fami.familienummer
,      fami.archief
,      null
,      null
,      null
,      null
,      null
,      pers.aanspreken pers_aanspreken
,      pers.achternaam pers_achternaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      null
,      kafd.code
,      fami.fami_id
,      pers.pers_id
,      to_number(null)
,     'N'
,     kgc_fami_00.openstaand_onderzoek( fami.fami_id,  pers.pers_id )
,      to_number( null )
,      to_number( null )
from kgc_kgc_afdelingen kafd
,     kgc_families fami
,     kgc_personen pers
,     kgc_familie_leden fale
where fale.fami_id = fami.fami_id
and   fale.pers_id = pers.pers_id
and   not exists
      ( select null
        from   kgc_monsters mons
       ,       bas_fracties frac
        where  frac.mons_id = mons.mons_id
        and    mons.pers_id = pers.pers_id
        and    mons.kafd_id = kafd.kafd_id
      )
and   not exists
      ( select null
        from  kgc_onderzoeken onde
        where  onde.pers_id = pers.pers_id
        and    onde.kafd_id = kafd.kafd_id
      )
 ;

/
QUIT
