CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCOPTE51_CEL_VW" ("STOF_ID", "ONDE_NR", "MATE_CODE", "FRAC_NR", "METI_DAT_AANM", "METI_ID", "MEET_RESULT") AS
  SELECT cel.stof_id
, cel.onde_nr
, cel.mate_code
, cel.frac_nr
, cel.meti_dat_aanm
, cel.meti_id
, cel.meet_result
  FROM TABLE(kgc_opte_00.f_retourneer_matrix) cel
 ;

/
QUIT
