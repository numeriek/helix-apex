CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_AANGEMELDE_STOFTESTEN_VW" ("PERS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "MONS_ID", "MATE_ID", "FRAC_ID", "FRTY_ID", "STGR_ID", "METI_ID", "MEET_ID", "STOF_ID", "PERS_ZISNR", "PERS_ACHTERNAAM", "PERS_AANSPREKEN", "PERS_ZOEKNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "ONDERZOEKNR", "ONDE_DATUM_BINNEN", "ONDE_AFGEROND", "MONSTERNUMMER", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "MONS_LEEFTIJD", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "STGR_CODE", "STGR_OMSCHRIJVING", "METI_DATUM_AANMELDING", "METI_AFGEROND", "METI_CONCLUSIE", "STOF_CODE", "STOF_OMSCHRIJVING", "MEETWAARDE", "MEETEENHEID", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "NORMAALWAARDE_GEMIDDELDE", "MEET_DATUM_METING", "MEET_AFGEROND") AS
  SELECT mons.pers_id
,      mons.kafd_id
,      mons.ongr_id
,      onde.onde_id
,      mons.mons_id
,      mons.mate_id
,      frac.frac_id
,      frac.frty_id
,      meti.stgr_id
,      meti.meti_id
,      meet.meet_id
,      stof.stof_id
,      pers.zisnr pers_zisnr
,      pers.achternaam pers_achternaam
,      pers.aanspreken pers_aanspreken
,      pers.zoeknaam pers_zoeknaam
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      onde.onderzoeknr
,      onde.datum_binnen onde_datum_binnen
,      onde.afgerond onde_afgerond
,      mons.monsternummer
,      mons.datum_aanmelding mons_datum_aanmelding
,      mons.datum_afname mons_datum_afname
,      mons.leeftijd mons_leeftijd
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.fractienummer
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.datum_aanmelding meti_datum_aanmelding
,      meti.afgerond meti_afgerond
,      TO_CHAR(meti.conclusie) meti_conclusie --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014)
                                               -- TO_CHAR removed by sachin for rverting changes of 9355 in rework release 201502 14-06-2015
                                               --  TO_CHAR added by SKA for MANTIS 9355  2016-02-16 release 8.11.0.2
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.tonen_als meetwaarde
,      meet.meeteenheid
,      meet.normaalwaarde_ondergrens
,      meet.normaalwaarde_bovengrens
,      meet.normaalwaarde_gemiddelde
,      meet.datum_meting meet_datum_meting
,      meet.afgerond meet_afgerond
from  kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      kgc_onderzoek_monsters onmo
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
where onmo.mons_id = mons.mons_id
and    mons.pers_id = pers.pers_id
and    mons.mate_id = mate.mate_id
and    onmo.onde_id = onde.onde_id
and    onmo.onmo_id = meti.onmo_id
and    meti.frac_id = frac.frac_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.meti_id = meet.meti_id
and    meet.stof_id = stof.stof_id
union
select mons.pers_id
,      mons.kafd_id
,      mons.ongr_id
,      onde.onde_id
,      mons.mons_id
,      mons.mate_id
,      frac.frac_id
,      frac.frty_id
,      to_number(null) -- stgr_id
,      to_number(null) -- meti_id
,      to_number(null) -- meet_id
,      stof.stof_id
,      pers.zisnr
,      pers.achternaam
,      pers.aanspreken
,      pers.zoeknaam
,      pers.geboortedatum
,      pers.geslacht
,      onde.onderzoeknr
,      onde.datum_binnen
,      onde.afgerond
,      mons.monsternummer
,      mons.datum_aanmelding
,      mons.datum_afname
,      mons.leeftijd mons_leeftijd
,      mate.code
,      mate.omschrijving
,      frac.fractienummer
,      null -- stgr_code
,      null -- stgr_omschrijving
,      to_date(null) -- meti_datum_aanmelding
,      null -- meti_afgerond
,      null -- meti_conclusie
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      null -- meetwaarde
,      null -- meeteenheid
,      to_number(null) -- normaalwaarde_ondergrens
,      to_number(null) -- normaalwaarde_bovengrens
,      to_number(null) -- normaalwaarde_gemiddelde
,      to_date(null) -- meet_datum_meting
,      null -- meet_afgerond
from  kgc_materialen mate
,      kgc_stoftesten stof
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      kgc_onderzoek_monsters onmo
,      bas_fracties frac
where  onmo.mons_id = mons.mons_id
and    mons.pers_id = pers.pers_id
and    mons.mate_id = mate.mate_id
and    onmo.onde_id = onde.onde_id
and    mons.mons_id = frac.mons_id
and    mons.kafd_id = stof.kafd_id
and    not exists
       ( select null
         from   kgc_onderzoek_monsters onmo
         ,      bas_metingen meti
         ,      bas_meetwaarden meet
         where  meet.meti_id = meti.meti_id
         and    meti.frac_id = frac.frac_id
         and    meti.onmo_id = onmo.onmo_id
         and    onmo.mons_id = mons.mons_id
         and    meet.stof_id = stof.stof_id
       );

/
QUIT
