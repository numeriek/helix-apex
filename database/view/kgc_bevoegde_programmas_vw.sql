CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BEVOEGDE_PROGRAMMAS_VW" ("PROG_ID", "PROGRAMMA", "OMSCHRIJVING", "UITVOERLOCATIE", "UITVOERBESTAND", "SCRIPT", "OPMERKINGEN") AS
  SELECT prog.prog_id
,      prog.programma
,      prog.omschrijving
,      prog.uitvoerlocatie
,      prog.uitvoerbestand
,      prog.script
,      prog.opmerkingen
from   kgc_programmas prog
,      kgc_prog_bevoegdheid prgb
where prog.prog_id = prgb.prog_id
and    prgb.mede_id = kgc_mede_00.medewerker_id
union -- geen expliciete bevoegdheid betekent voor iedereen
select prog.prog_id
,      prog.programma
,      prog.omschrijving
,      prog.uitvoerlocatie
,      prog.uitvoerbestand
,      prog.script
,      prog.opmerkingen
from   kgc_programmas prog
where  not exists
             ( select  null
                from    kgc_prog_bevoegdheid prgb
               where prgb.prog_id = prog.prog_id
            )
union -- applicatiebeheerdser mag alles
select prog.prog_id
,      prog.programma
,      prog.omschrijving
,      prog.uitvoerlocatie
,      prog.uitvoerbestand
,      prog.script
,      prog.opmerkingen
from   kgc_programmas prog
where  user in ( 'KGCN', 'HELIX' )
 ;

/
QUIT
