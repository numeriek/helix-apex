CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BASFRAC01_KWTY_VW" ("AANTAL_KWEKEN", "CODE", "KWEEKDUUR", "KWTY_ID", "MATE_ID", "OMSCHRIJVING", "PREFIX", "STANDAARD", "VOLGORDE", "KAFD_ID", "FRTY_CODE", "MATE_CODE", "MONS_MONSTERNUMMER") AS
  SELECT DISTINCT mons.monsternummer
               ,frty.code
               ,mate.code
               ,mons.mons_id
               ,kwty.kwty_id
               ,kwty.mate_id
               ,kwty.code
               ,kwty.omschrijving
               ,kwty.standaard
               ,kwty.volgorde
               ,kwty.aantal_kweken
               ,kwty.kweekduur
               ,kwty.prefix
FROM   bas_fractie_types        frty
      ,bas_fracties             frac
      ,kgc_monsters             mons
      ,kgc_materialen           mate
      ,kgc_onderzoek_monsters   onmo
      ,kgc_onderzoeken          onde
      ,kgc_onderzoek_indicaties onin
      ,kgc_frty_kwty            ftkt
      ,kgc_kweektypes           kwty
  WHERE onin.indi_id = nvl(kwty.indi_id, onin.indi_id)
AND    onin.onde_id = onde.onde_id
AND    onde.onde_id = onmo.onde_id
AND    mons.mons_id = frac.mons_id(+)
AND    onmo.mons_id = mons.mons_id(+)
AND    mons.mate_id = mate.mate_id
AND    frac.frty_id = frty.frty_id(+)
AND    ftkt.frty_id = frty.frty_id
AND    ftkt.kwty_id = kwty.kwty_id
AND    kwty.vervallen = 'N'
 ;

/
QUIT
