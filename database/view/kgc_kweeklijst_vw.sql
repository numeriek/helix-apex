CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KWEEKLIJST_VW" ("KAFD_ID", "ONGR_ID", "MONS_ID", "KAFD_CODE", "ONGR_CODE", "MONSTERNUMMER", "PERSOON", "DATUM_AANMELDING", "MATE_CODE", "MATE_OMSCHRIJVING", "FRAC_ID", "FRACTIENUMMER", "FRTY_ID", "FRTY_CODE", "FRTY_OMSCHRIJVING", "FRAC_STATUS", "COMMENTAAR", "KWTY_ID", "KWTY_CODE", "KWTY_OMSCHRIJVING", "KWFR_ID", "KWFR_CODE", "KWFR_VERVALLEN", "DATUM_INZET", "MEDE_ID_INZET", "MEDE_CODE_INZET", "MEDE_NAAM_INZET", "UITHAALDATUM", "DATUM_UITHAAL", "MEDE_ID_UITHAAL", "MEDE_CODE_UITHAAL", "MEDE_NAAM_UITHAAL", "KWFR2_CODE") AS
  SELECT mons.kafd_id
,      mons.ongr_id
,      mons.mons_id
,      kafd.code kafd_code
,      ongr.code ongr_code
,      mons.monsternummer
,      kgc_pers_00.info( mons.pers_id ) persoon
,      mons.datum_aanmelding
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.frac_id
,      frac.fractienummer
,      frac.frty_id
,      frty.code frty_code
,      frty.omschrijving frty_omschrijving
,      frac.status  frac_status
,      frac.commentaar       ||decode( frac.commentaar            , null, mons.commentaar      , chr(10)|| mons.commentaar      ) commentaar
,      kwfr.kwty_id
,      kwty.code kwty_code
,      kwty.omschrijving kwty_omschrijving
,      kwfr.kwfr_id
,      kwfr.code
,      kwfr.vervallen
,      kwfr.datum_inzet
,      kwfr.mede_id_inzet
,      mede1.code mede_code_inzet
,      mede1.naam mede_naam_inzet
,      kwfr.datum_inzet + kwfr.kweekduur uithaaldatum
,      kwfr.datum_uithaal
,      kwfr.mede_id_uithaal
,      mede2.code mede_code_uithaal
,      mede2.naam mede_naam_uithaal
,      kwfr2.code
from   bas_fractie_types frty
,      kgc_kweektypes kwty
,      kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_medewerkers mede2
,      kgc_medewerkers mede1
,      kgc_materialen mate
,      kgc_monsters mons
,      bas_fracties frac
,      kgc_kweekfracties kwfr
,      kgc_kweekfracties kwfr2
  WHERE mons.kafd_id = kafd.kafd_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mate_id = mate.mate_id
and    mons.mons_id = frac.mons_id (+)
and    frac.frty_id = frty.frty_id (+)
and    frac.frac_id = kwfr.frac_id
and    kwfr.mede_id_inzet = mede1.mede_id (+)
and    kwfr.mede_id_uithaal = mede2.mede_id (+)
and    kwfr.kwty_id = kwty.kwty_id (+)
and    kwfr.kwfr_id_parent = kwfr2.kwfr_id (+)
and   kwty.vervallen = 'N'
and    nvl( frac.status, 'O' ) in ( 'A', 'O' )
 ;

/
QUIT
