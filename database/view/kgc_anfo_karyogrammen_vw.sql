CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ANFO_KARYOGRAMMEN_VW" ("MEET_ID", "METI_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "MEDE_NAAM", "MEDE_NAAM_CONTROLE", "MEST_OMSCHRIJVING", "CEL", "PREPARAAT", "NONIUS", "KWALITEIT", "AANTAL", "MICROSCOPISCHE_ANALYSE") AS
  SELECT kary.meet_id
,      meet.meti_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      mede.naam mede_naam
,      mede2.naam mede_naam_controle
,      mest.omschrijving mest_omschrijving
,      kary.cel
,      kwfr.code preparaat
,      kary.nonius
,      kary.kwaliteit
,      kary.aantal
,      kgc_kary_00.chromosomen( kary.meet_id, kary.cel ) microscopische_analyse
from   kgc_meetwaardestructuur mwst
,      bas_meetwaarde_statussen mest
,      kgc_medewerkers mede2
,      kgc_medewerkers mede
,      kgc_stoftesten stof
,      kgc_kweekfracties kwfr
,      kgc_karyogrammen kary
,      bas_meetwaarden meet
where meet.meet_id = kary.meet_id
and    kary.kwfr_id = kwfr.kwfr_id
and    meet.stof_id = stof.stof_id
and    stof.mwst_id = mwst.mwst_id
and    mwst.code = 'KGCKARY01'
and    meet.mede_id = mede.mede_id (+)
and    meet.mede_id_controle = mede2.mede_id (+)
and    meet.mest_id = mest.mest_id (+)
union all
select meet.meet_id
,      meet.meti_id
,      stof.code
,      stof.omschrijving
,      mede.naam
,      mede2.naam
,      mest.omschrijving
,      dummy.nr
,      null -- preparaat
,      null -- nonius
,      null -- kwaliteit
,      null -- aantal
,      null -- microscopische_analyse
from   kgc_meetwaardestructuur mwst
,      bas_meetwaarde_statussen mest
,      kgc_medewerkers mede2
,      kgc_medewerkers mede
,      kgc_stoftesten stof
,      kgc_onderzoeksgroepen ongr
,      kgc_onderzoeken onde
,      bas_metingen meti
,      bas_meetwaarden meet
,      dummy_aantal dummy
where  meet.meti_id = meti.meti_id
and    meti.onde_id = onde.onde_id
and    onde.ongr_id = ongr.ongr_id
and    ( ( ongr.code like '%TUM%' and dummy.nr <= 20 )
        or dummy.nr <= 10
          )
and    meet.stof_id = stof.stof_id
and    stof.mwst_id = mwst.mwst_id
and    meet.mede_id = mede.mede_id (+)
and    meet.mede_id_controle = mede2.mede_id (+)
and    meet.mest_id = mest.mest_id (+)
and    mwst.code = 'KGCKARY01'
and    not exists
       ( select null
         from   kgc_karyogrammen kary
         where  kary.meet_id = meet.meet_id
         and    kary.cel = dummy.nr
       )
 ;

/
QUIT
