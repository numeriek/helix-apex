CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_AANGEMELD_VOOR_RUN_VW" ("KAFD_ID", "ONGR_ID", "MEET_ID", "STOF_ID", "RUNS_ID", "METI_ID", "ONDE_ID", "ONDERZOEKNR", "FRAC_ID", "STAN_ID", "FRACTIENUMMER", "MONS_ID", "MONSTERNUMMER", "MONS_DATUM_AFNAME", "PERS_ID", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "STGR_ID", "STGR_CODE", "STGR_OMSCHRIJVING", "VOLGNUMMER", "KAFD_CODE", "KAFD_NAAM", "SORTEERWAARDE") AS
  SELECT mons.kafd_id
,      mons.ongr_id
,      meet.meet_id
,      meet.stof_id
,      meet.runs_id
,      meti.meti_id
,      meti.onde_id
,      onde.onderzoeknr
,      frac.frac_id
,      to_number(null) stan_id
,      frac.fractienummer
,      mons.mons_id
,      mons.monsternummer
,      mons.datum_afname mons_datum_afname
,      pers.pers_id
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      mate.mate_id
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      stgr.stgr_id
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      wlit.volgnummer volgnummer
,      kafd.code kafd_code
,      kafd.naam kafd_naam
,      to_number( kgc_sorteren.sorteerwaarde( meet.meet_id, 'WERKLIJST' ) ) sorteerwaarde
from   kgc_onderzoeken onde
,      kgc_kgc_afdelingen kafd
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      kgc_personen pers
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
,      (select runnummer from bas_runs union select 'a' runnummer from dual) runs
,      kgc_werklijsten wlst
,      kgc_werklijst_items wlit
where  kafd.kafd_id = mons.kafd_id
and    mate.mate_id = mons.mate_id
and    pers.pers_id = mons.pers_id
and    mons.mons_id = frac.mons_id
and    frac.frac_id = meti.frac_id
and    onde.afgerond = 'N'
and    onde.onde_id = meti.onde_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.meti_id = meet.meti_id
and    meti.afgerond = 'N'
and    meet.afgerond = 'N'
and    meet.meet_id  = wlit.meet_id(+)
and    wlit.wlst_id = wlst.wlst_id (+)
and (  wlst.werklijst_nummer = runs.runnummer or meet.runs_id is null )
union all
select kafd.kafd_id
,      stan.ongr_id
,      meet.meet_id
,      meet.stof_id
,      meet.runs_id
,      meti.meti_id
,      to_number (null)
,      null
,      to_number (null)
,      stan.stan_id
,      stan.standaardfractienummer
,      to_number(null)
,      null
,      to_date(null)
,      to_number(null)
,      'Controlemonster'
,      to_date(null)
,      null
,      mate.mate_id
,      mate.code
,      mate.omschrijving
,      stgr.stgr_id
,      stgr.code
,      stgr.omschrijving
,      wlit.volgnummer volgnummer
,      kafd.code
,      kafd.naam
,      to_number( kgc_sorteren.sorteerwaarde( meet.meet_id, 'WERKLIJST' ) )
from   kgc_onderzoeksgroepen ongr
,      kgc_kgc_afdelingen kafd
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      bas_standaardfracties stan
,      bas_metingen meti
,      bas_meetwaarden meet
,      (select runnummer from bas_runs union select 'a' runnummer from dual) runs
,      kgc_werklijsten wlst
,      kgc_werklijst_items wlit
where  kafd.kafd_id = ongr.kafd_id
and    ongr.ongr_id = stan.ongr_id
and    mate.mate_id = stan.mate_id
and    stan.stan_id = meti.stan_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.meti_id = meet.meti_id
and    meet.meet_id = wlit.meet_id (+)
and    meti.afgerond = 'N'
and    meet.afgerond = 'N'
and    wlit.wlst_id = wlst.wlst_id (+)
and (  wlst.werklijst_nummer = runs.runnummer or meet.runs_id is null );

/
QUIT
