CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_RESERVERINGEN_VW" ("RESE_ID", "DATUM_RESERVERING", "MEET_ID", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "METI_ID", "STGR_ID", "STGR_CODE", "STGR_OMSCHRIJVING", "ONMO_ID", "ONDE_ID", "FRAC_ID", "FRACTIENUMMER", "MONS_ID", "CUTY_ID", "CUTY_CODE", "CUTY_OMSCHRIJVING", "HOEVEELHEID", "EENHEID") AS
  SELECT rese.rese_id
,      rese.datum_reservering
,      rese.meet_id
,      meet.stof_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meet.meti_id
,      meti.stgr_id
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.onmo_id
,      meti.onde_id
,      meti.frac_id
,      frac.fractienummer
,      frac.mons_id
,      rese.cuty_id
,      cuty.code cuty_code
,      cuty.omschrijving cuty_omschrijving
,      rese.hoeveelheid
,      cuty.eenheid
from   kgc_cup_types cuty
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_reserveringen rese
  WHERE rese.cuty_id = cuty.cuty_id
and    rese.meet_id = meet.meet_id
and    meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.frac_id = frac.frac_id;

/
QUIT
