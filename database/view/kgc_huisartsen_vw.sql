CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_HUISARTSEN_VW" ("RELA_ID", "CODE", "AANSPREKEN", "VOORLETTERS", "VOORVOEGSEL", "ACHTERNAAM", "ADRES", "POSTCODE", "WOONPLAATS", "LAND", "TELEFOON", "TELEFAX", "EMAIL", "VERVALLEN") AS
  SELECT rela.rela_id
,      rela.code
,      rela.aanspreken
,      rela.voorletters
,      rela.voorvoegsel
,      rela.achternaam
,      rela.adres
,      rela.postcode
,      rela.woonplaats
,      rela.land
,      rela.telefoon
,      rela.telefax
,      rela.email
,      rela.vervallen
from   kgc_relaties rela
  WHERE rela.relatie_type = 'HA'
 ;

/
QUIT
