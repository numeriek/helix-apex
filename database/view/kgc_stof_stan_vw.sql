CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_STOF_STAN_VW" ("STAN_ID", "STANDAARDFRACTIENUMMER", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "METI_AFGEROND", "MEET_AFGEROND", "KAFD_ID", "ONGR_ID") AS
  SELECT distinct stan.stan_id
,      stan.standaardfractienummer
,      stof.stof_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      meti.afgerond meti_afgerond
,      meet.afgerond meet_afgerond
,      stof.kafd_id
,      stan.ongr_id
from   bas_standaardfracties stan
,      kgc_stoftesten stof
,      bas_meetwaarden meet
,      bas_metingen meti
  WHERE meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.stan_id = stan.stan_id
and    stan.vervallen = 'N'
and    stof.vervallen = 'N'
 ;

/
QUIT
