CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_TRAY_VULLING_VW" ("TRVU_ID", "MEET_ID", "TRGE_ID", "TRTY_ID", "WERKLIJST_NUMMER", "X_POSITIE", "Y_POSITIE", "Z_POSITIE", "FRAC_ID", "FRACTIENUMMER", "STOF_OMSCHRIJVING", "PERS_ACHTERNAAM", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "INHOUD", "MEDE_ID") AS
  SELECT trvu.trvu_id
,      trvu.meet_id
,      trvu.trge_id
,      trge.trty_id
,      trge.werklijst_nummer
,      trvu.x_positie
,      trvu.y_positie
,      trvu.z_positie
,      frac.frac_id
,      nvl(frac.fractienummer,stan.standaardfractienummer)  fractienummer
,      stof.omschrijving stof_omschrijving
,      pers.achternaam pers_achternaam
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      nvl(frac.fractienummer,stan.standaardfractienummer)
       ||chr(10)||substr(pers.aanspreken,1,20)
       ||chr(10)||stof.omschrijving inhoud
,      trvu.mede_id
from   kgc_stoftesten stof
,      bas_standaardfracties stan
,      kgc_monsters mons
,      bas_fracties frac
,      kgc_personen pers
,      bas_metingen meti
,      bas_meetwaarden meet
,      kgc_tray_gebruik trge
,      kgc_tray_vulling trvu
where trvu.trge_id = trge.trge_id
and    trvu.meet_id = meet.meet_id
and    meet.meti_id = meti.meti_id
and    meet.stof_id = stof.stof_id
and    meti.frac_id = frac.frac_id (+)
and    meti.stan_id = stan.stan_id (+)
and    frac.mons_id = mons.mons_id (+)
and    mons.pers_id = pers.pers_id (+)
 ;

/
QUIT
