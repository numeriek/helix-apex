CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KWFO_REGELS_VW" ("NR", "FRAC_ID", "KOP", "KOLOM1", "KOLOM2", "KOLOM3", "KOLOM4", "KOLOM5", "KOLOM6", "KOLOM7") AS
  SELECT dummy.nr
,      frac.frac_id
,      'INZETTEN' kop
,      decode( dummy.nr
             , 1, 'Datum'
             ,    ' '
             ) kolom1
,      decode( dummy.nr
             , 1, decode( mate.code
                        , 'C', '      Analist STC:                   Analist LTC:'
                        , 'A', '      Analist Amniochrome:           Analist a-mem:'
                        ,      '      Analist:'
                        )
             , 2, decode( mate.code
                        , 'A', ' 2x Amniochrome'
                        , null
                        )
             , 3, decode( mate.code
                        , 'A', ' 2x a-mem'
                        , null
                        )
             , 4, decode( mate.code
                        , 'C', ' STC'
                        , null
                        )
             , 5, decode( mate.code
                        , 'A', '  x Amniochrome'
                        , 'C', ' Trypsine-collagenase-kweek (LTC) aantal gl:'
                        , null
                        )
             , 6, decode( mate.code
                        , 'A', '  x a-mem'
                        , 'C', ' Proefkweek (LTC):'
                        , null
                        )
             , 7, decode( mate.code
                        , 'A', ' 1x proefmedium'
                        , null
                        )
             , null
             ) kolom2
,      decode( dummy.nr
             , 3, decode( mate.code
                        , 'A', 'gl 1'
                        , null
                        )
             , 4, decode( mate.code
                        , 'A', 'gl 2'
                        , 'C', 'RPMI-FKS / RPMI'
                        , null
                        )
             , 5, decode( mate.code
                        , 'A', 'gl 3'
                        , 'C', 'Chang'
                        , null
                        )
             , 6, decode( mate.code
                        , 'A', 'gl 4'
                        , null
                        )
             , null
             ) kolom3
,      decode( dummy.nr
             , 2, decode( mate.code
                        , 'A', 'dag 6'
                        , null
                        )
             ) kolom4
,      decode( dummy.nr
             , 2, decode( mate.code
                        , 'A', 'dag 7'
                        , null
                        )
             ) kolom5
,      decode( dummy.nr
             , 2, decode( mate.code
                        , 'A', 'dag 8'
                        , null
                        )
             ) kolom6
,      decode( dummy.nr
             , 2, decode( mate.code
                        , 'A', 'dag 9'
                        , null
                        )
             ) kolom7
from   kgc_materialen mate
,      kgc_monsters mons
,      bas_fracties frac
,      dummy_aantal dummy
where frac.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
and    dummy.nr <= 7
union all
select dummy.nr
,      frac.frac_id
,      'KWEKEN' kop
,      decode( dummy.nr
             , 1, 'Datum'
             , null
             ) kolom1
,      null kolom2
,      decode( dummy.nr
             , 17, 'Invriezen: Ja / Nee'
             , null
             ) kolom3
,      null kolom4
,      null kolom5
,      null kolom6
,      null kolom7
from   bas_fracties frac
,      dummy_aantal dummy
where  dummy.nr <= 17
 ;

/
QUIT
