CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_DEADLINE_ONDERZOEKEN_VW" ("KAFD_CODE", "ONGR_CODE", "ONDE_GEPLANDE_EINDDATUM", "ONDERZOEKNR", "ONDE_DATUM_BINNEN", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERSOON_INFO", "INDI_CODE", "ONDE_OMSCHRIJVING", "ZWANGERSCHAPSDUUR", "UITS_DATUM_UITSLAG", "ONWY_OMSCHRIJVING", "KAFD_ID", "ONGR_ID", "ONDE_ID", "PERS_ID", "FOET_ID", "UITS_ID") AS
  SELECT kafd.code kafd_code
,             ongr.code ongr_code
,             onde.geplande_einddatum onde_geplande_einddatum
,             onde.onderzoeknr
,             onde.datum_binnen onde_datum_binnen
,             pers.aanspreken pers_aanspreken
,             pers.geboortedatum pers_geboortedatum
,             kgc_pers_00.persoon_info( null,  onde.onde_id ) persoon_info
,             kgc_indi_00.indi_code( onde.onde_id ) indi_code
,             onde.omschrijving onde_omschrijving
,             kgc_zwan_00.duur( onde.onde_id ) zwangerschapsduur
,             uits.datum_uitslag uits_datum_uitslag
,             kgc_onwy_00.onwy_omschrijving(onde.onde_id) onwy_omschrijving
,             onde.kafd_id
,             onde.ongr_id
,             onde.onde_id
,             onde.pers_id
,             onde.foet_id
,             uits.uits_id
FROM   kgc_kgc_afdelingen kafd
,             kgc_onderzoeksgroepen ongr
,             kgc_personen pers
,             kgc_uitslagen uits
,             kgc_onderzoeken onde
where  onde.kafd_id = kafd.kafd_id
and       onde.ongr_id = ongr.ongr_id
and       onde.pers_id = pers.pers_id
and       onde.onde_id = uits.onde_id (+)
and       onde.afgerond = 'N'
and       onde.geplande_einddatum is not null
 ;

/
QUIT
