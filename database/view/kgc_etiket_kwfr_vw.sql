CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ETIKET_KWFR_VW" ("TYPE_ETIKET", "SORTERING", "KAFD_CODE", "ONGR_CODE", "FRACTIENUMMER", "FRAC_ID_PARENT", "TYPE_AFLEIDING", "FRTY_CODE", "FRTY_OMSCHRIJVING", "MONSTERNUMMER", "MONS_COMMENTAAR", "MONS_DATUM_AANMELDING", "ONDERZOEKNR", "ONDERZOEKSWIJZE", "ONDERZOEKSTYPE", "ONDE_INDICATIE", "KWFR_CODE", "DATUM", "MATE_CODE", "MATE_OMSCHRIJVING", "KWTY_CODE", "KWTY_OMSCHRIJVING", "KWFR_KWFR_ID_PARENT", "KWFR_CODE_PARENT", "VOTY_CODE", "VOTY_OMSCHRIJVING", "PERS_ZISNR", "PERS_BSN", "BSN_GEVERIFIEERD", "PERS_ACHTERNAAM", "PERS_VOORVOEGSEL", "PERS_VOORLETTERS", "PERS_AANSPREKEN", "PERS_GESLACHT", "PERS_GEBOORTEDATUM", "PERS_TELEFOON", "PERS_ADRES", "PERS_POSTCODE", "PERS_WOONPLAATS", "VERZEKERAAR_CODE", "VERZEKERAAR_NAAM", "VERZEKERINGSNR", "PERSOON_INFO", "PERSOON_INFO_KORT", "FAMILIENUMMER", "FAMI_NAAM", "HUISARTS_ACHTERNAAM", "HUISARTS_AANSPREKEN", "HUISARTS_WOONPLAATS", "HUISARTS_TELEFOON", "BETROKKENE_A1", "BETROKKENE_A2", "MONS_DATUM_AFNAME", "MONS_TIJD_AFNAME", "HOEVEELHEID_MONSTER", "FRAC_ID", "MONS_ID", "OPSLAGPOSITIE", "STAN_ID", "ONDE_ID", "KWFR_ID", "ONBE_ID", "VOOP_ID", "BEST_ID", "OPAT_ID", "MEGR_ID", "STGR_ID", "WLST_ID", "VOOR_ID", "CUTY_ID", "CUPTYPE", "OPSLAG_VOLGNR", "TIJDSTEMPEL", "OPSLAG_TYPE", "FOETUS_INFO", "MONS_ETIKET_TOEVOEGING", "MEGR_NUMMER", "WERKLIJST_NUMMER", "AANGEMAAKT_DOOR", "STGR_CODE", "STGR_OMSCHRIJVING", "DATUM_AANMAAK", "ONDE_AANVRAGER", "RELA_INSTELLING", "RELA_INSTELLING_CODE", "RELA_AFDELING", "ONTSTAAN_UIT", "MONS_INDICATIE") AS
  SELECT 'KWFR' type_etiket,
             LPAD (frac.fractienummer, 20, '!')
          || LPAD (TO_CHAR (kwfr.kwty_id), 10, '!')
          || LPAD (TO_CHAR (kwfr.kwfr_id), 10, '!')
             sortering,
          kafd.code kafd_code,
          ongr.code ongr_code,
          frac.fractienummer fractienummer,
          frac.frac_id_parent frac_id_parent,
          frac.type_afleiding type_afleiding,
          frty.code frty_code,
          frty.omschrijving frty_omschrijving,
          mons.monsternummer monsternummer,
          mons.commentaar mons_commentaar --mons_commentaar added by Anuja against Mantis 6672-9401 on 23/06/15
                                         ,
          mons.datum_aanmelding mons_datum_aanmelding --mons_datum_aanmelding added by Anuja against Mantis 6672-9401 on 23/06/15
                                                     ,
          NULL onderzoeknr,
          NULL onderzoekswijze,
          NULL onderzoekstype,
          NULL onde_indicatie,
          kwfr.code kwfr_code,
          kwfr.datum_inzet datum,
          mate.code mate_code,
          mate.omschrijving mate_omschrijving,
          kwty.code kwty_code,
          NVL (kwty.omschrijving, kwty.code) kwty_omschrijving,
          CASE
             WHEN KWFR2.KWFR_ID IS NULL THEN KWFR.KWFR_ID
             ELSE KWFR2.KWFR_ID
          END
             AS kwfr_kwfr_id_parent, -- MG 29-07-16 - mantis 13264 -  toegevoegd voor nieuwe spatrobot etiketten
          kwfr2.code kwfr_code_parent,
          NULL voty_code,
          NULL voty_omschrijving,
          pers.zisnr pers_zisnr,
          pers.bsn pers_bsn --pers_bsn added by Anuja against Mantis 6672-9401 on 25/06/15
                           ,
          pers.bsn_geverifieerd bsn_geverifieerd --bsn_geverifieerd added by Anuja against Mantis 6672-9401 on 25/06/15
                                                ,
          pers.achternaam pers_achternaam,
          pers.voorvoegsel pers_voorvoegsel,
          pers.voorletters pers_voorletters,
          pers.aanspreken pers_aanspreken,
          pers.geslacht pers_geslacht,
          pers.geboortedatum pers_geboortedatum,
          pers.telefoon pers_telefoon,
          pers.adres pers_adres,
          pers.postcode pers_postcode,
          pers.woonplaats pers_woonplaats,
          verz.code verzekeraar_code,
          verz.naam verzekeraar_naam,
          pers.verzekeringsnr verzekeringsnr,
          kgc_pers_00.persoon_info (mons.mons_id, NULL) persoon_info,
          kgc_pers_00.persoon_info (mons.mons_id,
                                    NULL,
                                    NULL,
                                    'KORT')
             Persoon_info_kort,
          NULL familienummer,
          NULL fami_naam,
          rela.achternaam huisarts_achternaam,
          rela.aanspreken huisarts_aanspreken,
          rela.woonplaats huisarts_woonplaats,
          rela.telefoon huisarts_telefoon,
          NULL betrokkene_a1,
          NULL betrokkene_a2,
          mons.datum_afname mons_datum_afname,
          TO_CHAR (mons.tijd_afname, 'HH24:MI') mons_tijd_afname,
          mons.hoeveelheid_monster hoeveelheid_monster,
          frac.frac_id frac_id,
          frac.mons_id mons_id,
          kgc_opslag_00.opslagpositie ('FRAC', frac.frac_id) opslagpositie,
          TO_NUMBER (NULL) stan_id,
          TO_NUMBER (NULL) onde_id,
          kwfr.kwfr_id kwfr_id,
          TO_NUMBER (NULL) onbe_id,
          TO_NUMBER (NULL) voop_id,
          TO_NUMBER (NULL) best_id,
          TO_NUMBER (NULL) opat_id,
          TO_NUMBER (NULL) megr_id,
          TO_NUMBER (NULL) stgr_id,
          TO_NUMBER (NULL) wlst_id,
          TO_NUMBER (NULL) voor_id,
          TO_NUMBER (NULL) cuty_id,
          NULL cuptype,
          TO_NUMBER (NULL) opslag_volgnr,
          TO_DATE (NULL) tijdstempel,
          TO_CHAR (NULL) opslag_type,
          kgc_foet_00.foetus_info (NULL,
                                   NULL,
                                   mons.mons_id,
                                   'J')
             foetus_info,
          mons.etiket_toevoeging mons_etiket_toevoeging,
          TO_CHAR (NULL) megr_nummer,
          TO_CHAR (NULL) werklijst_nummer,
          kgc_mede_00.medewerker_naam (kwfr.created_by) aangemaakt_door,
          TO_CHAR (NULL) stgr_code,
          TO_CHAR (NULL) stgr_omschrijving,
          TO_DATE (NULL) datum_aanmaak,
          TO_CHAR (NULL) onde_aanvrager,
          TO_CHAR (NULL) rela_instelling,
          TO_CHAR (NULL) rela_instelling_code,
          TO_CHAR (NULL) rela_afdeling,
          NULL ontstaan_uit                           -- Added for Mantis 4273
                           ,
          NULL mons_indicatie                         -- Added for Mantis 4273
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_kweektypes kwty,
          kgc_materialen mate,
          kgc_relaties rela,
          kgc_verzekeraars verz,
          kgc_personen pers,
          kgc_monsters mons,
          bas_fracties frac,
          kgc_kweekfracties kwfr2                                    -- parent
                                 ,
          kgc_kweekfracties kwfr,
          bas_fractie_types frty
    WHERE     mons.kafd_id = kafd.kafd_id
          AND mons.ongr_id = ongr.ongr_id
          AND mons.pers_id = pers.pers_id
          AND mons.mate_id = mate.mate_id
          AND mons.mons_id = frac.mons_id
          AND frac.frac_id = kwfr.frac_id
          AND frac.frty_id = frty.frty_id(+)
          AND kwfr.kwty_id = kwty.kwty_id(+)
          AND kwfr.kwfr_id_parent = kwfr2.kwfr_id(+)
          AND pers.verz_id = verz.verz_id(+)
          AND pers.rela_id = rela.rela_id(+);

/
QUIT
