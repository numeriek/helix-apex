CREATE OR REPLACE FORCE VIEW "HELIX"."LOGGER_LOGS_TERSE" ("ID", "LOGGER_LEVEL", "TIME_AGO", "TEXT") AS
  select id, logger_level,
        substr(logger.date_text_format(time_stamp),1,20) time_ago,
        substr(text,1,200) text
   from logger_logs
  where time_stamp > systimestamp - (5/1440)
  order by id asc
;

/
QUIT
