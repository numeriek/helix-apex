CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAGBRIEF_UTDNA_VW" ("KOPIE", "BRIEFTYPE", "GEADRESSEERDE", "ONDERZOEKNR", "PERSOON", "GESLACHT", "GEBOORTEDATUM", "AANVRAAGDATUM", "AANV_CODE", "AANVRAGER", "AANV_TOEVOEGING", "INDICATIE", "ONDE_OMSCHRIJVING", "ONDE_REFERENTIE", "AFGEROND", "DATUM_UITSLAG", "DATUM_AUTORISATIE", "AUTO_CODE", "AUTORISATOR", "ONDERTEKENAAR1", "ONDERTEKENAAR2", "ONDERTEKENAAR3", "UITSLAG_TEKST", "TOELICHTING", "UITS_ID", "BRTY_ID", "KOHO_ID", "PERS_ID", "ONDE_ID", "KAFD_ID", "ONGR_ID", "TAAL_ID", "RELA_ID", "RELA_ID_ORIGINEEL", "INDI_ID", "KAFD_CODE", "ONGR_CODE", "ZISNR", "MEDE_ID_AUTORISATOR", "MEDE_ID_ONDERTEKENAAR1", "MEDE_ID_ONDERTEKENAAR2", "MEDE_ID_ONDERTEKENAAR3", "AUTO_TELEFOON", "AUTO_TELEFAX", "AUTO_INTERNE_POST", "AUTO_EMAIL", "AUTO_FUNCTIE_OMSCHR", "ONDERTEKENAAR1_FUNCTIE_OMSCHR", "ONDERTEKENAAR2_FUNCTIE_OMSCHR", "ONDERTEKENAAR3_FUNCTIE_OMSCHR", "FAMILIENUMMER", "FAMILIEONDERZOEKNR", "MATERIAAL", "LOCUS", "GENDEFECT", "BSN_OPGEMAAKT", "DATUM_BRIEF", "MATE_ID", "FUNC_ID_ONDERTEKENAAR1", "FUNC_ID_ONDERTEKENAAR2", "FUNC_ID_ONDERTEKENAAR3") AS
  SELECT 'N' kopie,
          brty.omschrijving brieftype,
          kgc_adres_00.relatie (uits.rela_id, 'J') geadresseerde,
          onde.onderzoeknr,
          pers.aanspreken persoon,
          kgc_vert_00.vertaling ('PERS_GESL',
                                 NULL,
                                 pers.geslacht,
                                 onde.taal_id)
             geslacht,
          pers.geboortedatum,
          onde.datum_binnen aanvraagdatum,
          aanv.code aanv_code,
          aanv2.aanspreken aanvrager,
          kgc_rela_00.toevoeging (aanv2.rela_id) aanv_toevoeging,
          kgc_indi_00.onderzoeksreden (onde.onde_id) indicatie,
          onde.omschrijving onde_omschrijving,
          onde.referentie onde_referentie,
          onde.afgerond,
          uits.datum_uitslag,
          onde.datum_autorisatie,
          DECODE(mede2.mede_id,
                 NULL, mede1.code, mede2.code)
             auto_code,
          DECODE(mede2.mede_id,
                 NULL, NVL(mede1.formele_naam, mede1.naam),
                 NVL(mede2.formele_naam, mede2.naam))
             autorisator,
          NVL (mede1.formele_naam, mede1.naam) ondertekenaar1,
          NVL (mede2.formele_naam, mede2.naam) ondertekenaar2,
          NVL (mede3.formele_naam, mede3.naam) ondertekenaar3,
          DECODE (SUBSTR (LTRIM (uits.tekst, ' '), 1, 1),
                  UPPER (SUBSTR (LTRIM (uits.tekst, ' '), 1, 1)), uits.tekst,
                  pers.aanspreken || ' ' || uits.tekst)
             uitslag_tekst,
          uits.toelichting,
          uits.uits_id,
          uits.brty_id,
          TO_NUMBER (NULL) koho_id,
          onde.pers_id,
          onde.onde_id,
          onde.kafd_id,
          onde.ongr_id,
          onde.taal_id,
          uits.rela_id,
          uits.rela_id rela_id_origineel,
          kgc_indi_00.indi_id (onde.onde_id) indi_id,
          kafd.code kafd_code,
          ongr.code ongr_code,
          pers.zisnr,
          DECODE(mede2.mede_id,
                 NULL, mede1.mede_id,
                 mede2.mede_id)
             mede_id_autorisator,
          mede1.mede_id mede_id_ondertekenaar1,
          mede2.mede_id mede_id_ondertekenaar2,
          mede3.mede_id mede_id_ondertekenaar3,
          DECODE(mede2.mede_id,
                 NULL, mede1.telefoon,
                 mede2.telefoon)
             auto_telefoon,
          DECODE(mede2.mede_id,
                 NULL, mede1.telefax,
                 mede2.telefax)
             auto_telefax,
          DECODE(mede2.mede_id,
                 NULL, mede1.interne_post,
                 mede2.interne_post)
             auto_interne_post,
          DECODE(mede2.mede_id,
                 NULL, mede1.email,
                 mede2.email)
             auto_email,
          DECODE(mede2.mede_id,
                 NULL, meka1.func_omschrijving,
                 meka2.func_omschrijving)
             auto_functie_omschr,
          meka1.func_omschrijving ondertekenaar1_functie_omschr,
          meka2.func_omschrijving ondertekenaar2_functie_omschr,
          meka3.func_omschrijving ondertekenaar3_functie_omschr,
          fami.familienummer,
          faon.familieonderzoeknr,
          mate.omschrijving materiaal,
          kgc_indi_00.locus (onde.onde_id) locus,
          faon.omschrijving gendefect,
          kgc_pers_00.bsn_opgemaakt (pers.pers_id) bsn_opgemaakt,
          kgc_brie_00.datum_huidige_brief (uits.uits_id) datum_brief,
          mate.mate_id,--Added for Mantis 7565,ASI as given by NUMereik
          meka1.func_id func_id_ondertekenaar1,--Added for Mantis 7565,ASI as given by NUMereik
          meka2.func_id func_id_ondertekenaar2,--Added for Mantis 7565,ASI as given by NUMereik
          meka3.func_id func_id_ondertekenaar3--Added for Mantis 7565,ASI as given by NUMereik
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_brieftypes brty,
          kgc_relaties aanv,
          kgc_relaties aanv2,                   -- (oorspronkelijke) aanvrager
          kgc_medewerkers mede1,
          kgc_mede_kafd_vw MEKA1,
          kgc_medewerkers mede2,
          kgc_mede_kafd_vw MEKA2,
          kgc_medewerkers mede3,
          kgc_mede_kafd_vw MEKA3,
          kgc_medewerkers mede_onde,
          kgc_mede_kafd_vw MEKA_onde,
          kgc_medewerkers mede_uits,
          kgc_mede_kafd_vw MEKA_uits,
          kgc_faon_betrokkenen FOBE,
          kgc_familie_onderzoeken FAON,
          kgc_families FAMI,
          kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_uitslagen uits,
          kgc_onderzoek_monsters ONMO,
          kgc_monsters MONS,
          kgc_materialen MATE
    WHERE     onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.pers_id = pers.pers_id
          AND onde.onde_id = uits.onde_id
          AND onde.rela_id = aanv.rela_id
          AND NVL (onde.rela_id_oorsprong, onde.rela_id) = aanv2.rela_id
          AND uits.mede_id = mede1.mede_id(+)
          AND uits.mede_id = meka1.mede_id(+)
          AND uits.kafd_id = meka1.kafd_id(+)
          AND uits.mede_id2 = mede2.mede_id(+)
          AND uits.mede_id2 = meka2.mede_id(+)
          AND uits.kafd_id = meka2.kafd_id(+)
          AND uits.mede_id3 = mede3.mede_id(+)
          AND uits.mede_id3 = meka3.mede_id(+)
          AND uits.kafd_id = meka3.kafd_id(+)
          AND onde.mede_id_autorisator = mede_onde.mede_id(+)
          AND onde.mede_id_autorisator = meka_onde.mede_id(+)
          AND onde.kafd_id = meka_onde.kafd_id(+)
          AND uits.mede_id4 = mede_uits.mede_id(+)
          AND uits.mede_id4 = meka_uits.mede_id(+)
          AND uits.kafd_id = meka_uits.kafd_id(+)
          AND uits.onde_id = fobe.onde_id(+)
          AND fobe.faon_id = faon.faon_id(+)
          AND faon.fami_id = fami.fami_id(+)
          AND uits.onde_id = onmo.onde_id(+)
          AND onmo.mons_id = mons.mons_id(+)
          AND onmo.mons_id = (SELECT MAX (mons_id)
                                FROM kgc_onderzoek_monsters onmo2
                               WHERE uits.onde_id = onmo2.onde_id)
          AND mons.mate_id = mate.mate_id(+)
          AND uits.brty_id = brty.brty_id(+)
          AND uits.tekst IS NOT NULL
   UNION ALL
   SELECT 'J',
          brty.omschrijving brieftype,
          kgc_adres_00.relatie (koho.rela_id, 'J'),
          onde.onderzoeknr,
          pers.aanspreken,
          kgc_vert_00.vertaling ('PERS_GESL',
                                 NULL,
                                 pers.geslacht,
                                 onde.taal_id),
          pers.geboortedatum,
          onde.datum_binnen,
          aanv.code,
          aanv2.aanspreken,
          kgc_rela_00.toevoeging (aanv2.rela_id),
          kgc_indi_00.onderzoeksreden (onde.onde_id),
          onde.omschrijving,
          onde.referentie,
          onde.afgerond,
          uits.datum_uitslag,
          onde.datum_autorisatie,
          DECODE(mede2.mede_id,
                 NULL, mede1.code, mede2.code)
             auto_code,
          DECODE(mede2.mede_id,
                 NULL, NVL(mede1.formele_naam, mede1.naam),
                 NVL(mede2.formele_naam, mede2.naam))
             autorisator,
          NVL (mede1.formele_naam, mede1.naam),
          NVL (mede2.formele_naam, mede2.naam),
          NVL (mede3.formele_naam, mede3.naam),
          DECODE (SUBSTR (LTRIM (uits.tekst, ' '), 1, 1),
                  UPPER (SUBSTR (LTRIM (uits.tekst, ' '), 1, 1)), uits.tekst,
                  pers.aanspreken || ' ' || uits.tekst),
          uits.toelichting,
          uits.uits_id,
          uits.brty_id,
          koho.koho_id,
          onde.pers_id,
          onde.onde_id,
          onde.kafd_id,
          onde.ongr_id,
          onde.taal_id,
          koho.rela_id,
          uits.rela_id,
          kgc_indi_00.indi_id (onde.onde_id),
          kafd.code,
          ongr.code,
          pers.zisnr,
          DECODE(mede2.mede_id,
                 NULL, mede1.mede_id,
                 mede2.mede_id)
             mede_id_autorisator,
          mede1.mede_id,
          mede2.mede_id,
          mede3.mede_id,
          DECODE(mede2.mede_id,
                 NULL, mede1.telefoon,
                 mede2.telefoon)
             auto_telefoon,
          DECODE(mede2.mede_id,
                 NULL, mede1.telefax,
                 mede2.telefax)
             auto_telefax,
          DECODE(mede2.mede_id,
                 NULL, mede1.interne_post,
                 mede2.interne_post)
             auto_interne_post,
          DECODE(mede2.mede_id,
                 NULL, mede1.email,
                 mede2.email)
             auto_email,
          DECODE(mede2.mede_id,
                 NULL, meka1.func_omschrijving,
                 meka2.func_omschrijving)
             auto_functie_omschr,
          meka1.func_omschrijving ondertekenaar1_functie_omschr,
          meka2.func_omschrijving ondertekenaar2_functie_omschr,
          meka3.func_omschrijving ondertekenaar3_functie_omschr,
          fami.familienummer,
          faon.familieonderzoeknr,
          mate.omschrijving materiaal,
          kgc_indi_00.locus (onde.onde_id) locus,
          faon.omschrijving gendefect,
          kgc_pers_00.bsn_opgemaakt (pers.pers_id) bsn_opgemaakt,
          kgc_brie_00.datum_huidige_brief (uits.uits_id) datum_brief,
          mate.mate_id,--Added for Mantis 7565,ASI as given by NUMereik
          meka1.func_id func_id_ondertekenaar1,--Added for Mantis 7565,ASI as given by NUMereik
          meka2.func_id func_id_ondertekenaar2,--Added for Mantis 7565,ASI as given by NUMereik
          meka3.func_id func_id_ondertekenaar3--Added for Mantis 7565,ASI as given by NUMereik
     FROM kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_brieftypes brty,
          kgc_relaties aanv,
          kgc_relaties aanv2,
          kgc_medewerkers mede1,
          kgc_mede_kafd_vw MEKA1,
          kgc_medewerkers mede2,
          kgc_mede_kafd_vw MEKA2,
          kgc_medewerkers mede3,
          kgc_mede_kafd_vw MEKA3,
          kgc_medewerkers mede_onde,
          kgc_mede_kafd_vw MEKA_onde,
          kgc_medewerkers mede_uits,
          kgc_mede_kafd_vw MEKA_uits,
          kgc_faon_betrokkenen FOBE,
          kgc_familie_onderzoeken FAON,
          kgc_families FAMI,
          kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_uitslagen uits,
          kgc_kopiehouders koho,
          kgc_onderzoek_monsters onmo,
          kgc_monsters MONS,
          kgc_materialen MATE
    WHERE     onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.pers_id = pers.pers_id
          AND onde.rela_id = aanv.rela_id
          AND NVL (onde.rela_id_oorsprong, onde.rela_id) = aanv2.rela_id
          AND onde.onde_id = uits.onde_id
          AND uits.uits_id = koho.uits_id
          AND uits.mede_id = mede1.mede_id(+)
          AND uits.mede_id = meka1.mede_id(+)
          AND uits.kafd_id = meka1.kafd_id(+)
          AND uits.mede_id2 = mede2.mede_id(+)
          AND uits.mede_id2 = meka2.mede_id(+)
          AND uits.kafd_id = meka2.kafd_id(+)
          AND uits.mede_id3 = mede3.mede_id(+)
          AND uits.mede_id3 = meka3.mede_id(+)
          AND uits.kafd_id = meka3.kafd_id(+)
          AND onde.mede_id_autorisator = mede_onde.mede_id(+)
          AND onde.mede_id_autorisator = meka_onde.mede_id(+)
          AND onde.kafd_id = meka_onde.kafd_id(+)
          AND uits.mede_id4 = mede_uits.mede_id(+)
          AND uits.mede_id4 = meka_uits.mede_id(+)
          AND uits.kafd_id = meka_uits.kafd_id(+)
          AND uits.onde_id = fobe.onde_id(+)
          AND fobe.faon_id = faon.faon_id(+)
          AND faon.fami_id = fami.fami_id(+)
          AND uits.onde_id = onmo.onde_id(+)
          AND onmo.mons_id = mons.mons_id(+)
          AND onmo.mons_id = (SELECT MAX (mons_id)
                                FROM kgc_onderzoek_monsters onmo2
                               WHERE uits.onde_id = onmo2.onde_id)
          AND mons.mate_id = mate.mate_id(+)
          AND uits.brty_id = brty.brty_id(+)
          AND uits.tekst IS NOT NULL;

/
QUIT
