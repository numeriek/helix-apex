CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MACHTIGINGSBRIEF_OUD" ("GEADRESSEERDE", "BETREFT_TEKST", "ONDERZOEKNR", "VERZEKERDE", "GESLACHT", "GEBOORTEDATUM", "ADRES_VERZEKERDE", "ZIS_NUMMER", "VERZEKERAAR", "VERZEKERINGSNR", "AANVRAAGDATUM", "DATUM_AUTORISATIE", "RELA_CODE", "VERWIJZER", "RELATIE", "AUTORISATOR", "ONDE_ID", "KAFD_ID", "ONGR_ID", "TAAL_ID", "DEWY_ID", "RAMO_ID", "DEWY_CODE", "RAMO_CODE", "KAFD_CODE", "EERDER_GEPRINT") AS
  SELECT nvl( onde.nota_adres
            ,  kgc_adres_00.verzekeraar( nvl( onde.verz_id, pers.verz_id ), 'J')
            ) geadresseerde
,      dewy.betreft_tekst
,      onde.onderzoeknr
,      pers.aanspreken  verzekerde
,      pers.geslacht geslacht
,      pers.geboortedatum
,      kgc_adres_00.persoon( pers.pers_id, 'N'  ) adres_verzekerde
,      verz2.zis_code zis_nummer
,      nvl( verz2.naam, 'Onbekend' ) verzekeraar
,      pers.verzekeringsnr || ' ('||pers.verzekeringswijze||')'  verzekeringsnr
,      onde.datum_binnen aanvraagdatum
,      onde.datum_autorisatie
,      rela.code rela_code
,      kgc_adres_00.relatie( rela.rela_id,'J') verwijzer
,      nvl( fare.omschrijving, 'Verzekerde is index persoon' ) relatie
,      mede.naam autorisator
,      onde.onde_id
,      onde.kafd_id
,      onde.ongr_id
,      onde.taal_id
,      onde.dewy_id
,      dewy.ramo_id
,      dewy.code dewy_code
,      ramo.code ramo_code
,      kafd.code  kafd_code
,      kgc_brie_00.machtiging_reeds_geprint( onde.onde_id ) eerder_geprint
from   kgc_familie_relaties fare
,      kgc_kgc_afdelingen kafd
,      kgc_rapport_modules ramo
,      kgc_declaratiewijzen dewy
,      kgc_medewerkers mede
,      kgc_verzekeraars verz2
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
where onde.kafd_id = kafd.kafd_id
and    onde.pers_id = pers.pers_id
and    onde.rela_id = rela.rela_id
and    onde.mede_id_autorisator = mede.mede_id
and    verz2.verz_id (+) = pers.verz_id
and    onde.fare_id = fare.fare_id (+)
and    onde.dewy_id = dewy.dewy_id
and    dewy.ramo_id = ramo.ramo_id
and    onde.declareren = 'J'
and    onde.afgerond = 'J'
and    onde.onderzoekstype = 'D'
-- als betrokkene bestaat, kijk dan daar!
and    not exists
       ( select null from kgc_onderzoek_betrokkenen onbe
         where onbe.onde_id = onde.onde_id
         and     onbe.pers_id = onde.pers_id
      )
union -- na tussentijdse uitslagbrief voor niet-afgeronde onderzoeken (voorlopige autorisator in staat in uitslag)
SELECT nvl( onde.nota_adres
            ,  kgc_adres_00.verzekeraar( nvl( onde.verz_id, pers.verz_id ), 'J')
            )
,      dewy.betreft_tekst
,      onde.onderzoeknr
,      pers.aanspreken
,      pers.geslacht geslacht
,      pers.geboortedatum
,      kgc_adres_00.persoon( pers.pers_id, 'N'  )
,      verz2.zis_code
,      nvl( verz2.naam, 'Onbekend' )
,      pers.verzekeringsnr || ' ('||pers.verzekeringswijze||')'
,      onde.datum_binnen
,      nvl( onde.datum_autorisatie
            , brie.datum_print
            )
,      rela.code rela_code
,      kgc_adres_00.relatie( rela.rela_id, 'J' )
,      nvl( fare.omschrijving, 'Verzekerde is index persoon' )
,      mede.naam
,      onde.onde_id
,      onde.kafd_id
,      onde.ongr_id
,      onde.taal_id
,      onde.dewy_id
,      dewy.ramo_id
,      dewy.code dewy_code
,      ramo.code ramo_code
,      kafd.code  kafd_code
,      kgc_brie_00.machtiging_reeds_geprint( onde.onde_id ) eerder_geprint
from   kgc_familie_relaties fare
,      kgc_kgc_afdelingen kafd
,      kgc_rapport_modules ramo
,      kgc_declaratiewijzen dewy
,      kgc_medewerkers mede
,      kgc_verzekeraars verz2
,      kgc_relaties rela
,      kgc_brieftypes brty
,      kgc_brieven brie
,      kgc_uitslagen uits
,      kgc_personen pers
,      kgc_onderzoeken onde
where  onde.kafd_id = kafd.kafd_id
and    onde.pers_id = pers.pers_id
and    onde.rela_id = rela.rela_id
and    onde.onde_id = uits.onde_id
and    uits.mede_id = mede.mede_id
and    verz2.verz_id (+) = pers.verz_id
and    onde.fare_id = fare.fare_id (+)
and    onde.dewy_id = dewy.dewy_id
and    dewy.ramo_id = ramo.ramo_id
and    onde.declareren = 'J'
and    onde.afgerond = 'N'
and    onde.onde_id = brie.onde_id
and    brie.brty_id = brty.brty_id
and    brty.code = 'UITSLAG_T'
and    onde.onderzoekstype = 'D'
union -- betrokkenen (blijkt de hoofdquery, ook de adviesvrager staat hierin)
SELECT nvl( onde.nota_adres
                     ,  kgc_adres_00.verzekeraar( nvl( onde.verz_id, pers.verz_id ), 'J')
                     )
,      dewy.betreft_tekst
,      onde.onderzoeknr
,      pers.aanspreken
,      pers.geslacht
,      pers.geboortedatum
,      kgc_adres_00.persoon( pers.pers_id, 'N')
,      verz2.zis_code
,      nvl( verz2.naam, 'Onbekend' )
,      pers.verzekeringsnr || ' ('||pers.verzekeringswijze||')'
,      onde.datum_binnen
,      onde.datum_autorisatie
,      rela.code
,      kgc_adres_00.relatie( rela.rela_id )
,      decode ( onbe.pers_id
              , onde.pers_id, 'Verzekerde is index persoon'
              , 'Verzekerde is betrokkene bij het onderzoek'
              )
,      mede.naam
,      onde.onde_id
,      onde.kafd_id
,      onde.ongr_id
,      onde.taal_id
,      onde.dewy_id
,      dewy.ramo_id
,      dewy.code
,      ramo.code
,      kafd.code
,      kgc_brie_00.machtiging_reeds_geprint( onde.onde_id ) eerder_geprint
from   kgc_kgc_afdelingen kafd
,      kgc_rapport_modules ramo
,      kgc_declaratiewijzen dewy
,      kgc_medewerkers mede
,      kgc_verzekeraars verz2
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoek_betrokkenen onbe
,      kgc_onderzoeken onde
where onde.kafd_id = kafd.kafd_id
and    onde.onde_id = onbe.onde_id
and    onbe.pers_id = pers.pers_id
and    onde.rela_id = rela.rela_id
and    onde.mede_id_autorisator = mede.mede_id
and    verz2.verz_id (+) = pers.verz_id
and    onde.dewy_id = dewy.dewy_id
and    dewy.ramo_id = ramo.ramo_id
and    onde.declareren = 'J'
and    onbe.declareren = 'J'
and    onde.afgerond = 'J'
and    onde.onderzoekstype = 'D'
 ;

/
QUIT
