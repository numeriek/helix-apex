CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DECL_GENOOM_AFG_ONDE_J_VW" ("NAAR_HELIX", "ONDERZOEKNR", "GROEP", "NOTITIE", "NOTITIE_RELA", "DECL_CHECK", "PERS_CHECK", "RELA_CHECK", "INST_CHECK", "RELA_CODE", "RELA_AANSPREKEN", "ZNR", "SPEC_CODE", "ZISNR", "OVERLEDEN", "OVERLIJDENSDATUM", "BEHEER_IN_ZIS", "DECL_ID", "DECL_DATUM", "VERRICHTINGCODE", "SERVICE_PROVIDER", "ONDE_TYPE", "AANVRAGER_TYPE", "DECL_TYPE", "SER_ID", "BEVOEGD", "DEWY_CODE", "ICODE", "INST_CODE", "VERBORGEN_NOTI_ID", "VERBORGEN_NOTI_ENTITEIT", "VERBORGEN_KLEUR", "VERBORGEN_SORTERING") AS
  SELECT decl.pers_id naar_helix,
          decl.onderzoeknr,
          decl.ongr_code groep,
          noti.omschrijving AS notitie,
          notirela.omschrijving AS notitierela,
          decl.decl_check,
          decl.pers_check,
          decl.rela_check,
          decl.inst_check,
          decl.rela_code,
          decl.rela_aanspreken,
          decl.znr,
          decl.spec_code,
          decl.zisnr,
          decl.overleden,
          decl.overlijdensdatum,
          decl.beheer_in_zis,
          decl.decl_id,
          decl.decl_datum,
          decl.verrichtingcode,
          decl.service_provider,
          DECODE (decl.onderzoekstype,
                  'R', 'Research',
                  'D', 'Diagnostiek',
                  '')
             AS ONDE_TYPE,
          decl.aanvrager_type,
          decl.decl_type,
          decl.ser_id,
          decl.bevoegd_declareren bevoegd,
          decl.dewy_code,
          decl.icode,
          decl.inst_code,
          noti.noti_id verborgen_noti_id,
          'DECL' verborgen_noti_entiteit,
          '#79A8A8' verborgen_kleur,
          decl.decl_datum verborgen_sortering
     FROM beh_decl_check_vw decl,
          kgc_notities noti,
          kgc_notities notirela,
          kgc_relaties rela
    WHERE     decl.decl_id = noti.id(+)
          AND noti.entiteit(+) = 'DECL'
          AND decl.rela_code = rela.code
          AND rela.rela_id = notirela.id(+)
          AND notirela.entiteit(+) = 'RELA'
          AND notirela.gereed(+) = 'N'
          AND decl.kafd_code = 'GENOOM'
          AND decl.onde_afgerond = 'J'
          AND (   decl.decl_check != 'OK'
               OR decl.pers_check != 'OK'
               OR decl.rela_check != 'OK'
               OR decl.inst_check != 'OK')

--   SELECT decl.pers_id naar_helix,
--          decl.decl_id,
--          decl.decl_datum,
--          decl.verrichtingcode,
--          decl.service_provider,
--          decl.zisnr,
--          decl.overleden,
--          decl.overlijdensdatum,
--          decl.beheer_in_zis,
--          decl.onderzoeknr,
--          DECODE (decl.onderzoekstype,
--                  'R', 'Research',
--                  'D', 'Diagnostiek',
--                  '')
--             AS ONDE_TYPE,
--          decl.ongr_code groep,
--          decl.rela_code,
--          decl.znr,
--          decl.rela_aanspreken,
--          decl.aanvrager_type,
--          decl.decl_type,
--          decl.ser_id,
--          decl.spec_code,
--          decl.bevoegd_declareren bevoegd,
--          decl.dewy_code,
--          decl.icode,
--          decl.inst_code,
--          decl.decl_check,
--          decl.pers_check,
--          decl.rela_check,
--          decl.inst_check,
--          noti.omschrijving AS notitie,
--          noti.noti_id verborgen_noti_id,
--          'DECL' verborgen_noti_entiteit,
--          '#FFFFFF' verborgen_kleur,
--          decl.decl_datum verborgen_sortering
--     FROM beh_decl_check_vw decl, kgc_notities noti
--    WHERE     decl.decl_id = noti.id(+)
--          AND noti.entiteit(+) = 'DECL'
--          AND decl.kafd_code = 'GENOOM'
--          AND decl.onde_afgerond = 'J'
--          AND (   decl.decl_check != 'OK'
--               OR decl.pers_check != 'OK'
--               OR decl.rela_check != 'OK'
--               OR decl.inst_check != 'OK');;

/
QUIT
