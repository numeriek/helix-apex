CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_NGS_EXA_S_MDET_VW" ("MEET_ID", "ONDE_ID", "ONDERZOEKNR", "DATA_GEDEELD", "INTERPRETATIE_KLAAR", "BEVESTIGEN", "AFGEROND") AS
  SELECT "MEET_ID",
          "ONDE_ID",
          "ONDERZOEKNR",
          "'Data gedeeld'",
          "'1e interpretatie klaar'",
          CASE
             WHEN    UPPER ("'Bevestigen'") IS NULL
                  OR UPPER ("'Bevestigen'") = ''
                  OR UPPER ("'Bevestigen'") = 'N'
                  OR UPPER ("'Bevestigen'") = 'NEE'
             THEN
                'N'
             ELSE
                'J'
          END
             AS Bevestigen,
        AFGEROND
     FROM (SELECT mdet.meet_id,
                  ONDE.ONDE_ID,
                  ONDE.ONDERZOEKNR,
                  PROMPT,
                  WAARDE,
                  MEET.AFGEROND
             FROM kgc_onderzoeken onde,
                  bas_metingen meti,
                  bas_meetwaarden meet,
                  bas_meetwaarde_details mdet,
                  kgc_onderzoeksgroepen ongr,
                  kgc_meetwaardestructuur mwst
            WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                  AND METI.METI_ID = MEET.METI_ID
                  AND MEET.MEET_ID = MDET.MEET_ID
                  AND ONDE.ONGR_ID = ONGR.ONGR_ID
                  AND MEET.MWST_ID = MWST.MWST_ID
                  AND ONDE.AFGEROND = 'N'
                  AND METI.AFGEROND = 'N'
                  AND MEET.MEET_REKEN = 'S'
                  AND MWST.CODE = 'EXA_S'
                  AND ONGR.CODE = 'NGS') PIVOT (MAX (WAARDE)
                                         FOR (PROMPT)
                                         IN  ('Data gedeeld',
                                             '1e interpretatie klaar',
                                             'Bevestigen'));

/
QUIT
