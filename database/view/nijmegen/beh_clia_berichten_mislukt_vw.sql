CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_CLIA_BERICHTEN_MISLUKT_VW" ("VOLGNR", "MEET_ID", "CREATION_DATE_EERSTE_KEER", "CREATION_DATE_LAATSTE_KEER", "VERZONDEN_EERSTE_KEER", "VERZONDEN_LAATSTE_KEER", "LOG_TEKST_EERSTE_KEER", "LOG_TEKST_LAATSTE_KEER") AS
  SELECT "VOLGNR",
          "MEET_ID",
          "CREATION_DATE_EERSTE_KEER",
          "CREATION_DATE_LAATSTE_KEER",
          "VERZONDEN_EERSTE_KEER",
          "VERZONDEN_LAATSTE_KEER",
          "LOG_TEKST_EERSTE_KEER",
          "LOG_TEKST_LAATSTE_KEER"
     FROM (  SELECT ROW_NUMBER ()
                       OVER (PARTITION BY MEET_ID ORDER BY CREATION_DATE)
                       VOLGNR,
                    MEET_ID,
                    MIN (CREATION_DATE)
                       KEEP (DENSE_RANK FIRST ORDER BY CREATION_DATE)
                       OVER (PARTITION BY MEET_ID)
                       CREATION_DATE_EERSTE_KEER,
                    MAX (creation_date)
                       KEEP (DENSE_RANK LAST ORDER BY CREATION_DATE)
                       OVER (PARTITION BY MEET_ID)
                       creation_date_laatste_KEER,
                    MIN (VERZONDEN)
                       KEEP (DENSE_RANK FIRST ORDER BY CREATION_DATE)
                       OVER (PARTITION BY MEET_ID)
                       VERZONDEN_EERSTE_KEER,
                    MAX (VERZONDEN)
                       KEEP (DENSE_RANK LAST ORDER BY CREATION_DATE)
                       OVER (PARTITION BY MEET_ID)
                       VERZONDEN_LAATSTE_KEER,
                    MIN (LOG_TEKST)
                       KEEP (DENSE_RANK FIRST ORDER BY CREATION_DATE)
                       OVER (PARTITION BY MEET_ID)
                       LOG_TEKST_EERSTE_KEER,
                    MAX (log_tekst)
                       KEEP (DENSE_RANK LAST ORDER BY CREATION_DATE)
                       OVER (PARTITION BY MEET_ID)
                       LOG_TEKST_LAATSTE_KEER
               FROM BEH_CLIA_HL7_BERICHTEN
               where bericht_type = 'ORM'
           ORDER BY MEET_ID, CREATION_DATE)
    WHERE     VolgNR = 1
          AND VERZONDEN_EERSTE_KEER = 'N'
          AND VERZONDEN_LAATSTE_KEER = 'N'
          AND creation_date_EERSTE_KEER > (SYSDATE - 7)
 ;

/
QUIT
