CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST6_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "BI_BU", "INT_EXT", "TAAL", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "STATUS", "NOTITIE", "DAGEN_OVER", "FRACTIENR", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT CASE                                    -- Openstaande Onderzoeken
               WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) > 0
               THEN
                  '#5CAB5C'                                           -- groen
               ELSE
                  CASE
                     WHEN TRUNC (ONDE.GEPLANDE_EINDDATUM) = '11-11-1111'
                     THEN
                        '#0B0B61'
                     ELSE
                        CASE
                           WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) <= 0
                           THEN
                              '#CD5B5B'                                -- rood
                           ELSE
                              CASE
                                 WHEN ONDE.GEPLANDE_EINDDATUM IS NULL
                                 THEN
                                    '#79A8D6'                         -- blauw
                              END
                        END
                  END
            END
               AS VERBORGEN_KLEUR,
            CASE
               WHEN TRUNC (ONDE.GEPLANDE_EINDDATUM) = '11-11-1111'
               THEN
                  3
               ELSE
                  CASE
                     WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) <= 0
                     THEN
                        1                                              -- rood
                     ELSE
                        CASE
                           WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) > 0
                           THEN
                              2                                       -- groen
                           ELSE
                              CASE
                                 WHEN ONDE.GEPLANDE_EINDDATUM IS NULL THEN 4 -- blauw
                              END
                        END
                  END
            END
               AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            DECODE (beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
                    'J', 'INTERN',
                    'N', '')
               AS INT_EXT,
            TAAL.CODE TAAL,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS DAT_BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            NOTI.OMSCHRIJVING AS NOTITIE,
            ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) AS DAGEN_OVER,
            LISTAGG (FRAC.FRACTIENUMMER, ', ')
               WITHIN GROUP (ORDER BY FRAC.FRACTIENUMMER)
               AS FRACTIENR,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_familie_leden fale,
            kgc_families fami,
            bas_fracties frac,
            kgc_onderzoek_betrokkenen onbe,
            BEH_FLOW_FAVORIETEN FLFA,
            KGC_TALEN TAAL
      WHERE     ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED(+) = 'N'
            AND onde.kafd_id = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND onde.ongr_id = ongr.ongr_id
            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND onde.afgerond = 'N'
            AND ONDE.KAFD_ID = 1
            AND ONDE.ONDE_ID = ONBE.ONDE_ID(+)
            AND ONBE.FRAC_ID = FRAC.FRAC_ID(+)
            --AND ONGR.CODE <> 'ARR'
            AND ONGR.CODE NOT LIKE 'PRE%'
            AND ONGR.CODE NOT LIKE 'POST%'
            AND ONGR.CODE NOT LIKE 'TUM%'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY ONDE.GEPLANDE_EINDDATUM,
            ONDE.ONDERZOEKNR,
            ONDE.PERS_ID,
            ONGR.CODE,
            FAMI.FAMILIENUMMER,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU'),
            DECODE (beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
                    'J', 'INTERN',
                    'N', ''),
            TAAL.CODE,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            ONDE.ONDERZOEKSTYPE,
            ONDE.STATUS,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.MEDE_ID,
            FLFA.FLOW_ID
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING ASC,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
