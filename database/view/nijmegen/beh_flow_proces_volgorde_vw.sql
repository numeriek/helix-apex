CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_PROCES_VOLGORDE_VW" ("VOORGAAND_PROCES_STAP", "VOLGEND_PROCES_STAP", "MAXDOORLOOPTIJD", "IN_BEREKENING") AS
  SELECT                     --- MAXDoorlooptijd komt van VOLGEND_PROCES_STAP
         FLPV.FLDE_ID_VOORG_STAP,
          FLPV.FLDE_ID_VOLG_STAP,
          CASE WHEN IN_BEREKENING = 'J' THEN MAXDOORLOOPTIJD ELSE 0 END
             AS MAXDOORLOOPTIJD,
          FLDE.IN_BEREKENING
     FROM beh_flow_proces_volgorde flpv, beh_flow_details flde
    WHERE FLPV.FLDE_ID_VOLG_STAP = FLDE.FLDE_ID;

/
QUIT
