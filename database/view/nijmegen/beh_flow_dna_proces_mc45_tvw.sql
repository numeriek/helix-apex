CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC45_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "SEQUENCING", "FACILITEIT", "DAGEN_BIJ_FAC", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht NGS - data is gestuurd naar BGI --> Sequencing is gevuld
           '#79A8A8' AS VERBORGEN_KLEUR,
            '' AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            PVT.SEQUENCING,
            PVT.FACILITEIT,
            ROUND (SYSDATE - TO_DATE (PVT.SEQUENCING, 'dd-mm-rrrr hh24:mi:ss'))
               AS DAGEN_BIJ_FAC,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            beh_flow_help_fami_vw fami,
            kgc_onderzoekswijzen onwy,
            beh_flow_favorieten flfa,
            kgc_stoftestgroepen stgr,
            (SELECT exal.meet_id, exal.SEQUENCING, EXAL.FACILITEIT
               FROM BEH_FLOW_NGS_EXA_LIB_MDET_VW exal
              WHERE     exal.SEQUENCING IS NOT NULL
                    AND STATUS IS NULL
                    AND DATA_BESCHIKBAAR IS NULL
                    AND DATA_OPGEHAALD IS NULL
                    AND DATA_GEBACK_UPT IS NULL) pvt
      WHERE     ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FAMI.PERS_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND meti.stgr_id = stgr.stgr_id
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND MEET.MEET_ID = PVT.MEET_ID
            AND MEET.AFGEROND = 'N'
            AND ONDE.AFGEROND = 'N'
            AND STGR.CODE = 'NGS_ST_1'                       -- mantisnr 11373
   ORDER BY ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDE_ID ASC, FRAC.FRAC_ID ASC;

/
QUIT
