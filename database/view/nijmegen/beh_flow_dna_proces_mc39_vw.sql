CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC39_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "STOF_CODE", "STOF_OMSCHRIJVING", "STOF_AFGEROND", "STOF_STATUS", "MEETDETAILS", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT             -- Overzicht "Samenstelling stoftesten" -- -- NGS view
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            --LISTAGG ((FAMI.FAMILIENUMMER, ', ') WITHIN GROUP (ORDER BY FAMI.FAMILIENUMMER)) AS FAMILIENR,
            to_char(wm_concat (distinct FAMI.FAMILIENUMMER)) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            STOF.CODE,
            STOF.OMSCHRIJVING,
            MEET.AFGEROND,
            MEST.CODE AS STATUS,
            to_char(LISTAGG (MDET.PROMPT || ': ' || MDET.WAARDE || '|') WITHIN GROUP (ORDER BY MDET.MDET_ID)) AS MeetDetails,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            BAS_MEETWAARDE_DETAILS mdet,
            BAS_MEETWAARDE_STATUSSEN mest,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_technieken tech,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC39_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND STOF.TECH_ID = TECH.TECH_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            --AND MEET.AFGEROND = 'N'
            AND MEET.MEET_REKEN = 'S'
            --AND STGR.CODE NOT LIKE '%!_R' ESCAPE '!'
            --AND UPPER (STGR.OMSCHRIJVING) NOT LIKE '%ROBOT%'
            --AND NVL (TECH.CODE, 'GEEN') NOT IN ('SEQ', 'GS')
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND MDET.MEET_ID = MEET.MEET_ID
            AND ONGR.CODE = 'NGS'
            AND MEET.MEST_ID = MEST.MEST_ID(+)
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            FRAC.FRACTIENUMMER,
            WLST.WERKLIJST_NUMMER,
            FLFA.FLOW_ID,
            STOF.CODE,
            STOF.OMSCHRIJVING,
            MEET.AFGEROND,
            MEST.CODE,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC,
            FRAC.FRACTIENUMMER ASC
/*
     SELECT             -- Overzicht "Samenstelling stoftesten" -- -- NGS view
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID,
            STOF.CODE,
            STOF.OMSCHRIJVING,
            MEET.AFGEROND,
            MEST.CODE AS "GOED-FOUT",
            mdetails."Status",
            mdetails."BC-Tag",
            mdetails."RunID",
            mdetails."FLOWCELL",
            mdetails."Exp. OK",
            mdetails."Backup OK",
            mdetails."Map.Start",
            mdetails."Map.OK",
            mdetails."Annot.Start",
            mdetails."Annot.OK",
            mdetails."Trio OK",
            mdetails."Data Shared"
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            BAS_MEETWAARDE_STATUSSEN mest,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_technieken tech,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa,
            (  SELECT meet_id,
                      MAX (DECODE (UPPER (prompt), 'STATUS', waarde, NULL))
                         AS "Status",
                      MAX (DECODE (UPPER (prompt), 'BC-TAG', waarde, NULL))
                         AS "BC-Tag",
                      MAX (DECODE (UPPER (prompt), 'RUNID', waarde, NULL))
                         AS "RunID",
                      MAX (DECODE (UPPER (prompt), 'FLOWCELL', waarde, NULL))
                         AS "FLOWCELL",
                      MAX (DECODE (UPPER (prompt), 'EXP. OK', waarde, NULL))
                         AS "Exp. OK",
                      MAX (DECODE (UPPER (prompt), 'BACKUP OK', waarde, NULL))
                         AS "Backup OK",
                      MAX (DECODE (UPPER (prompt), 'MAP.START', waarde, NULL))
                         AS "Map.Start",
                      MAX (DECODE (UPPER (prompt), 'MAP.OK', waarde, NULL))
                         AS "Map.OK",
                      MAX (DECODE (UPPER (prompt), 'ANNOT.START', waarde, NULL))
                         AS "Annot.Start",
                      MAX (DECODE (UPPER (prompt), 'ANNOT.OK', waarde, NULL))
                         AS "Annot.OK",
                      MAX (DECODE (UPPER (prompt), 'TRIO OK', waarde, NULL))
                         AS "Trio OK",
                      MAX (DECODE (UPPER (prompt), 'DATA SHARED', waarde, NULL))
                         AS "Data Shared"
                 FROM (SELECT prompt, meet_id, waarde FROM BAS_MEETWAARDE_DETAILS)
             GROUP BY meet_id) mdetails
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC39_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND STOF.TECH_ID = TECH.TECH_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            --AND MEET.AFGEROND = 'N'
            AND MEET.MEET_REKEN = 'S'
            --AND STGR.CODE NOT LIKE '%!_R' ESCAPE '!'
            --AND UPPER (STGR.OMSCHRIJVING) NOT LIKE '%ROBOT%'
            --AND NVL (TECH.CODE, 'GEEN') NOT IN ('SEQ', 'GS')
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND MDETAILS.MEET_ID = MEET.MEET_ID
            AND ONGR.CODE = 'NGS'
            AND MEET.MEST_ID = MEST.MEST_ID(+)
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC,
            FRAC.FRACTIENUMMER ASC;
*/;

/
QUIT
