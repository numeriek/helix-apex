CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_HERSTEL_NIPT_DECL_VW" ("DECL_ID", "DATUM", "VERRICHTINGCODE", "JAAR", "VERRICHTINGCODE_GOED") AS
  select "DECL_ID","DATUM","VERRICHTINGCODE","JAAR","VERRICHTINGCODE_GOED"
from
(
select  decl.decl_id
,       decl.datum
,       upper(decl.verrichtingcode) verrichtingcode
,       extract(year from decl.datum) jaar
,       case
          when extract(year from decl.datum) > 2014 then
            '377881C'
          else
            'NIPT'
        end verrichtingcode_goed
from kgc_declaraties decl
where decl.deen_id = 709
and   decl.status is null
and   decl.declareren = 'J'
and   trunc(decl.datum) > sysdate-(365*2)
)
where verrichtingcode != verrichtingcode_goed;

/
QUIT
