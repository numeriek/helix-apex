CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST22_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "GROEP", "ONDERZOEKNR", "ONDE_TYPE", "WIJZE", "UITSLAGCODE", "AANMAAK_DATUM", "DATUM_GOEDKEURING", "DT_TOT_GOEDGEKEURD", "EERSTE_WERKLIJST", "DT_TOT_EERSTE_WERKLIJST", "LAATSTE_MEET_AFGEROND", "DT_TOT_LAATSTE_MEET_AFGEROND", "AANMAK_UITSLAG", "DT_TOT_EERST_UITSLAG", "DAT_ONDERTEKENING1", "DT_TOT_EERST_ONDERTEKENING", "DAT_ONDERTEKENING2", "DT_TOT_TWEEDE_ONDERTEKENING", "DATUM_AUTORISATIE", "DT_TOT_AUTORISATIE", "TOTALE_DOORLOOPTIJD") AS
  SELECT '#FFFFFF',
            1,
            '' AS SORTERING      --TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM')
                           ,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            ONWY.OMSCHRIJVING AS WIJZE,
            ONUI.CODE AS UITSLAGCODE,
            ONDE.CREATION_DATE,
            ONGO.DATUM_GOEDKEURING,
            ROUND (ONGO.DATUM_GOEDKEURING - ONDE.CREATION_DATE, 4)
               AS DT_TOT_GOEDGEKEURD,
            ONWK.CREATION_DATE AS EERSTE_WERKLIJST,
            CASE
               WHEN ONGO.DATUM_GOEDKEURING IS NULL
               THEN
                  ROUND (ONWK.CREATION_DATE - ONDE.CREATION_DATE, 4)
               ELSE
                  CASE
                     WHEN ONWK.CREATION_DATE - ONGO.DATUM_GOEDKEURING < 0
                     THEN
                        0
                     ELSE
                        ROUND (ONWK.CREATION_DATE - ONGO.DATUM_GOEDKEURING, 4)
                  END
            END
               AS DT_TOT_EERSTE_WERKLIJST,
            MEAF.DATUM_AFGEROND AS LAATSTE_MEET_AFGEROND,
            CASE
               WHEN ONWK.CREATION_DATE - ONGO.DATUM_GOEDKEURING < 0
               THEN
                  ROUND (MEAF.DATUM_AFGEROND - ONGO.DATUM_GOEDKEURING, 4)
               ELSE
                  CASE
                     WHEN MEAF.DATUM_AFGEROND - ONWK.CREATION_DATE < 0 THEN 0
                     ELSE ROUND (MEAF.DATUM_AFGEROND - ONWK.CREATION_DATE, 4)
                  END
            END
               AS DT_TOT_LAATSTE_MEET_AFGEROND --, CASE WHEN MEAF.DATUM_AFGEROND - ONWK.CREATION_DATE < 0 THEN 0 ELSE ROUND (MEAF.DATUM_AFGEROND - ONWK.CREATION_DATE, 4) END AS DT_TOT_LAATSTE_MEET_AFGEROND
                                              ,
            UITSA.AANMAK_UITSLAG AS AANMAK_UITSLAG,
            CASE
               WHEN     MEAF.DATUM_AFGEROND IS NULL
                    AND ONWK.CREATION_DATE IS NULL
                    AND ONGO.DATUM_GOEDKEURING IS NULL
               THEN
                  ROUND (uitsA.aanmak_uitslag - ONDE.CREATION_DATE, 4)
               ELSE
                  CASE
                     WHEN MEAF.DATUM_AFGEROND IS NULL
                          AND ONWK.CREATION_DATE IS NULL
                     THEN
                        ROUND (uitsA.aanmak_uitslag - ONGO.DATUM_GOEDKEURING,
                               4)
                     ELSE
                        CASE
                           WHEN MEAF.DATUM_AFGEROND IS NULL
                           THEN
                              ROUND (uitsA.aanmak_uitslag - ONWK.CREATION_DATE,
                                     4)
                           ELSE
                              ROUND (
                                 uitsA.aanmak_uitslag - MEAF.DATUM_AFGEROND,
                                 4)
                        END
                  END
            END
               AS DT_TOT_EERST_UITSLAG,
            uits1.datum_onder1 AS DAT_ONDERTEKENING1,
            ROUND (uits1.datum_onder1 - uitsA.aanmak_uitslag, 4)
               AS DT_TOT_EERST_ONDERTEKENING,
            uits2.datum_onder2 AS DAT_ONDERTEKENING2,
            ROUND (uits2.datum_onder2 - uits1.datum_onder1, 4)
               AS DT_TOT_TWEEDE_ONDERTEKENING --, ONDE.DATUM_AUTORISATIE as onde_aut
                                             ,
            ONAU.DATUM_AUTORISATIE,
            CASE
               WHEN ONAU.DATUM_AUTORISATIE IS NOT NULL
                    AND UITS2.DATUM_ONDER2 IS NOT NULL
               THEN
                  ROUND (ONAU.DATUM_AUTORISATIE - uits2.datum_onder2, 4)
               ELSE
                  CASE
                     WHEN     ONAU.DATUM_AUTORISATIE IS NOT NULL
                          AND UITS2.DATUM_ONDER2 IS NULL
                          AND UITS1.DATUM_ONDER1 IS NOT NULL
                     THEN
                        ROUND (ONAU.DATUM_AUTORISATIE - uits1.datum_onder1, 4)
                     ELSE
                        CASE
                           WHEN     ONAU.DATUM_AUTORISATIE IS NOT NULL
                                AND UITS1.DATUM_ONDER1 IS NULL
                                AND UITSA.AANMAK_UITSLAG IS NOT NULL
                           THEN
                              ROUND (
                                 ONAU.DATUM_AUTORISATIE - UITSA.AANMAK_UITSLAG,
                                 4)
                           ELSE
                              CASE
                                 WHEN     ONAU.DATUM_AUTORISATIE IS NOT NULL
                                      AND UITSA.AANMAK_UITSLAG IS NULL
                                      AND MEAF.DATUM_AFGEROND IS NOT NULL
                                 THEN
                                    ROUND (
                                       ONAU.DATUM_AUTORISATIE
                                       - MEAF.DATUM_AFGEROND,
                                       4)
                                 ELSE
                                    CASE
                                       WHEN ONAU.DATUM_AUTORISATIE IS NOT NULL
                                            AND UITSA.AANMAK_UITSLAG IS NULL
                                            AND MEAF.DATUM_AFGEROND IS NULL
                                            AND ONWK.CREATION_DATE IS NOT NULL
                                       THEN
                                          ROUND (
                                             ONAU.DATUM_AUTORISATIE
                                             - ONWK.CREATION_DATE,
                                             4)
                                       ELSE
                                          CASE
                                             WHEN ONAU.DATUM_AUTORISATIE
                                                     IS NOT NULL
                                                  AND UITSA.AANMAK_UITSLAG
                                                         IS NULL
                                                  AND MEAF.DATUM_AFGEROND
                                                         IS NULL
                                                  AND ONWK.CREATION_DATE
                                                         IS NULL
                                                  AND ONGO.DATUM_GOEDKEURING
                                                         IS NOT NULL
                                             THEN
                                                ROUND (
                                                   ONAU.DATUM_AUTORISATIE
                                                   - ONGO.DATUM_GOEDKEURING,
                                                   4)
                                             ELSE
                                                CASE
                                                   WHEN ONAU.DATUM_AUTORISATIE
                                                           IS NOT NULL
                                                        AND UITSA.AANMAK_UITSLAG
                                                               IS NULL
                                                        AND MEAF.DATUM_AFGEROND
                                                               IS NULL
                                                        AND ONWK.CREATION_DATE
                                                               IS NULL
                                                        AND ONGO.DATUM_GOEDKEURING
                                                               IS NULL
                                                   THEN
                                                      ROUND (
                                                         ONAU.DATUM_AUTORISATIE
                                                         - ONDE.CREATION_DATE,
                                                         4)
                                                   ELSE
                                                      CASE
                                                         WHEN ONAU.DATUM_AUTORISATIE
                                                                 IS NULL
                                                              AND UITS2.DATUM_ONDER2
                                                                     IS NOT NULL
                                                         THEN
                                                            ROUND (
                                                               ONDE.DATUM_AUTORISATIE
                                                               - uits2.datum_onder2,
                                                               4)
                                                         ELSE
                                                            CASE
                                                               WHEN ONAU.DATUM_AUTORISATIE
                                                                       IS NULL
                                                                    AND UITS2.DATUM_ONDER2
                                                                           IS NULL
                                                                    AND UITS1.DATUM_ONDER1
                                                                           IS NOT NULL
                                                               THEN
                                                                  ROUND (
                                                                     ONDE.DATUM_AUTORISATIE
                                                                     - uits1.datum_onder1,
                                                                     4)
                                                               ELSE
                                                                  CASE
                                                                     WHEN ONAU.DATUM_AUTORISATIE
                                                                             IS NULL
                                                                          AND UITS1.DATUM_ONDER1
                                                                                 IS NULL
                                                                          AND UITSA.AANMAK_UITSLAG
                                                                                 IS NOT NULL
                                                                     THEN
                                                                        ROUND (
                                                                           ONDE.DATUM_AUTORISATIE
                                                                           - UITSA.AANMAK_UITSLAG,
                                                                           4)
                                                                     ELSE
                                                                        CASE
                                                                           WHEN ONAU.DATUM_AUTORISATIE
                                                                                   IS NULL
                                                                                AND UITSA.AANMAK_UITSLAG
                                                                                       IS NULL
                                                                                AND MEAF.DATUM_AFGEROND
                                                                                       IS NOT NULL
                                                                           THEN
                                                                              ROUND (
                                                                                 ONDE.DATUM_AUTORISATIE
                                                                                 - MEAF.DATUM_AFGEROND,
                                                                                 4)
                                                                           ELSE
                                                                              CASE
                                                                                 WHEN ONAU.DATUM_AUTORISATIE
                                                                                         IS NULL
                                                                                      AND UITSA.AANMAK_UITSLAG
                                                                                             IS NULL
                                                                                      AND MEAF.DATUM_AFGEROND
                                                                                             IS NULL
                                                                                      AND ONWK.CREATION_DATE
                                                                                             IS NOT NULL
                                                                                 THEN
                                                                                    ROUND (
                                                                                       ONDE.DATUM_AUTORISATIE
                                                                                       - ONWK.CREATION_DATE,
                                                                                       4)
                                                                                 ELSE
                                                                                    CASE
                                                                                       WHEN ONAU.DATUM_AUTORISATIE
                                                                                               IS NULL
                                                                                            AND UITSA.AANMAK_UITSLAG
                                                                                                   IS NULL
                                                                                            AND MEAF.DATUM_AFGEROND
                                                                                                   IS NULL
                                                                                            AND ONWK.CREATION_DATE
                                                                                                   IS NULL
                                                                                            AND ONGO.DATUM_GOEDKEURING
                                                                                                   IS NOT NULL
                                                                                       THEN
                                                                                          ROUND (
                                                                                             ONDE.DATUM_AUTORISATIE
                                                                                             - ONGO.DATUM_GOEDKEURING,
                                                                                             4)
                                                                                       ELSE
                                                                                          CASE
                                                                                             WHEN ONAU.DATUM_AUTORISATIE
                                                                                                     IS NULL
                                                                                                  AND UITSA.AANMAK_UITSLAG
                                                                                                         IS NULL
                                                                                                  AND MEAF.DATUM_AFGEROND
                                                                                                         IS NULL
                                                                                                  AND ONWK.CREATION_DATE
                                                                                                         IS NULL
                                                                                                  AND ONGO.DATUM_GOEDKEURING
                                                                                                         IS NULL
                                                                                             THEN
                                                                                                ROUND (
                                                                                                   ONDE.DATUM_AUTORISATIE
                                                                                                   - ONDE.CREATION_DATE,
                                                                                                   4)
                                                                                          END
                                                                                    END
                                                                              END
                                                                        END
                                                                  END
                                                            END
                                                      END
                                                END
                                          END
                                    END
                              END
                        END
                  END
            END
               AS DT_TOT_AUTORISATIE,
            CASE
               WHEN ONAU.DATUM_AUTORISATIE IS NOT NULL
               THEN
                  ROUND (ONAU.DATUM_AUTORISATIE - ONDE.CREATION_DATE, 4)
               ELSE
                  ROUND (ONDE.DATUM_AUTORISATIE - ONDE.CREATION_DATE, 4)
            END
               AS TOTALE_DOORLOOPTIJD
       FROM kgc_onderzoeken onde,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_uitslagcodes onui,
            (  SELECT DISTINCT
                      ONDE.ONDE_ID, MIN (WLST.CREATION_DATE) AS CREATION_DATE
                 FROM kgc_onderzoeken onde,
                      kgc_werklijst_items wlit,
                      kgc_werklijsten wlst,
                      bas_metingen meti
                WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                      AND METI.METI_ID = WLIT.METI_ID
                      AND WLIT.WLST_ID = WLST.WLST_ID
             GROUP BY ONDE.ONDE_ID) ONWK,
            (  SELECT DISTINCT
                      ONDE.ONDE_ID, MAX (MEET.LAST_UPDATE_DATE) AS DATUM_AFGEROND
                 FROM kgc_onderzoeken onde,
                      kgc_werklijst_items wlit,
                      kgc_werklijsten wlst,
                      bas_meetwaarden meet,
                      bas_metingen meti
                WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                      AND METI.METI_ID = WLIT.METI_ID
                      AND MEET.MEET_ID = WLIT.MEET_ID
                      AND WLIT.WLST_ID = WLST.WLST_ID
                      AND ONDE.AFGEROND = 'J'
                      AND MEET.AFGEROND = 'J'
             GROUP BY ONDE.ONDE_ID) MEAF,
            (  SELECT DISTINCT
                      uits.ONDE_ID, MIN (UITS.CREATION_DATE) AS aanmak_uitslag
                 FROM kgc_uitslagen uits
             GROUP BY uits.ONDE_ID) uitsA,
            (  SELECT DISTINCT
                      uits.ONDE_ID, MIN (UITS.DATUM_MEDE_ID) AS datum_onder1
                 FROM kgc_uitslagen uits
             GROUP BY uits.ONDE_ID) uits1,
            (  SELECT DISTINCT
                      uits.ONDE_ID, MIN (UITS.DATUM_MEDE_ID2) AS datum_onder2
                 FROM kgc_uitslagen uits
             GROUP BY uits.ONDE_ID) uits2,
            (  SELECT DISTINCT
                      ONDE.ONDE_ID, MAX (ONAH.CREATION_DATE) AS DATUM_AUTORISATIE
                 FROM kgc_onderzoeken onde, KGC_ONDE_AUTORISATIE_HISTORIE onah
                WHERE ONDE.ONDE_ID = ONAH.ONDE_ID
                      AND ONDE.ONDE_ID NOT IN
                             (SELECT ONAH.ONDE_ID
                                FROM KGC_ONDE_AUTORISATIE_HISTORIE onah,
                                     kgc_redenen_de_autorisatie rnda
                               WHERE     ONAH.RNDA_ID = RNDA.RNDA_ID
                                     AND ONAH.ACTIE = 'D'
                                     AND RNDA.SCHONEN_AUTORISATIEDATUM = 'N'
                                     AND RNDA.CODE IN
                                            ('01_BRIEF', '04_AANVR', '06_TAAL'))
                      AND ONAH.ACTIE = 'A'
             GROUP BY ONDE.ONDE_ID) ONAU,
            (  SELECT DISTINCT
                      ONDE.ONDE_ID, MIN (AULO.DATUM_TIJD) AS DATUM_GOEDKEURING
                 FROM kgc_onderzoeken onde, kgc_audit_log aulo
                WHERE     aulo.id = ONDE.ONDE_ID
                      AND AULO.KOLOM_NAAM = 'STATUS'
                      AND AULO.WAARDE_NIEUW = 'G'
             GROUP BY ONDE.ONDE_ID) ONGO
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID(+)
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE(+)
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONUI_ID = ONUI.ONUI_ID
            --AND ONDE.ONDE_ID = ONIN.ONDE_ID
            --AND ONIN.INDI_ID = INTE.INDI_ID
            --AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.ONDE_ID = ONGO.onde_id(+)
            AND ONDE.ONDE_ID = ONWK.onde_id(+)
            AND ONDE.ONDE_ID = MEAF.ONDE_ID(+)
            AND ONDE.ONDE_ID = uitsA.onde_id(+)
            AND ONDE.ONDE_ID = uits1.onde_id(+)
            AND ONDE.ONDE_ID = uits2.onde_id(+)
            AND ONDE.ONDE_ID = onau.onde_id(+)
            AND ONDE.AFGEROND = 'J'
            AND TO_CHAR (ONDE.CREATION_DATE, 'YYYY') >= ('2010')
            AND ONGR.CODE IN ('ARR', 'LTG', 'NGS', 'P', 'PR', 'RFD', 'TML')
   --AND AULO.KOLOM_NAAM = 'STATUS'
   --AND AULO.WAARDE_NIEUW = 'G'
   ORDER BY ONDE.ONDE_ID DESC;

/
QUIT
