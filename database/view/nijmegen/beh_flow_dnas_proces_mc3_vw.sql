CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNAS_PROCES_MC3_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "SPOED", "ISOLATIELIJSTNR", "VOLGNR", "FRACTIENR", "MONSTERNR", "MATERIAAL", "BUISNR", "PROCES_CODE", "STAP_GEDAAN", "COMMENTAAR") AS
  SELECT CASE    -- Overzicht "Fracties om te isoleren - stap 1 uitgevoerd"
               WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
               THEN
                  '#04B404'                                           -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                     THEN
                        '#AEB404'                              --  licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - FRAC.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              '#FF8000'                              -- oranje
                           ELSE
                              '#FF0000'                                -- rood
                        END
                  END
            END
               AS verborgen_kleur,
            CASE
               WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
               THEN
                  4                                                   -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                     THEN
                        3                                       -- licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - FRAC.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              2                                      -- oranje
                           ELSE
                              1                                        -- rood
                        END
                  END
            END
               AS verborgen_sortering,
            FRAC.FRACTIENUMMER AS VERBORGEN_TELLING,
            MONS.PERS_ID AS verborgen_pers_id,
            DECODE (bas_isol_00.spoed (ISOL.MONS_ID), 'J', 'JA', NULL) SPOED,
            ISOL.ISOLATIELIJST_NUMMER,
            --ISOL.ORACLE_UID ,
            ISOL.VOLGNUMMER,
            FRAC.FRACTIENUMMER,
            --decode(FRAC.STATUS, 'A', 'Aangemeld', 'O', 'Opgewerkt', 'M', 'Mislukt', 'V', 'Vervallen', 'X', 'Verbruikt', decode( FRAC.CONTROLE, 'J', 'Indexfractie', null)) FRAC_STATUS,
            NVL (FRAC2.FRACTIENUMMER, MONS.MONSTERNUMMER) MONS_MONSTERNUMMER,
            MATE.CODE,
            --kgc_pers_00.persoon_info( ISOL.MONS_ID, NULL ) PERSOON_INFO,
            --bas_isol_00.familie( ISOL.MONS_ID, 'N') FAMILIENUMMER,
            ISOL.BUISNR,
            --ISOL.DATUM,
            --ISOL.FRAC_ID FRAC_ID,
            --ISOL.ONGR_ID ONGR_ID,
            --ISOL.KAFD_ID KAFD_ID,
            --ISOL.MONS_ID MONS_ID,
            PROC.CODE,
            ISOL.STAP,
            MONS.COMMENTAAR
       FROM KGC_MATERIALEN MATE,
            BAS_FRACTIES FRAC2,
            BAS_FRACTIES FRAC,
            KGC_MONSTERS MONS,
            BAS_ISOLATIELIJSTEN ISOL,
            KGC_KGC_AFDELINGEN KAFD,
            KGC_PROCESSEN PROC,
            BEH_FLOW_PRIORITEIT_VW flpr
      WHERE               ---ISOL.ISOLATIELIJST_NUMMER = :p_isolatielijst  AND
           FLPR .VIEW_NAME = 'BEH_FLOW_DNAS_PROCES_MC3_VW'
            AND flpr.kafd_id = ISOL.KAFD_ID
            AND ISOL.MONS_ID = MONS.MONS_ID
            AND ISOL.FRAC_ID = FRAC.FRAC_ID
            AND FRAC.FRAC_ID_PARENT = FRAC2.FRAC_ID(+)
            AND MONS.MATE_ID = MATE.MATE_ID
            AND KAFD.KAFD_ID = ISOL.KAFD_ID
            AND ISOL.PROC_ID = PROC.PROC_ID(+)
            AND FRAC.STATUS = 'A'
            AND ISOL.STAP <> 0
   ORDER BY verborgen_sortering,
            spoed,
            ISOL.ISOLATIELIJST_NUMMER,
            ISOL.VOLGNUMMER;

/
QUIT
