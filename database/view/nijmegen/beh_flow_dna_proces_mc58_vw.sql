CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC58_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "TAAL", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "TOT_AANG", "TOT_WLST", "TOT_WLST_MPOOL", "MEGA_POOL_PLATE_ID", "STATUS", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            FAVORIET,
            NAAR_HELIX,
            GROEP,
            FAMILIENR,
            ONDERZOEKNR,
            taal,
            SPOED,
            EINDDATUM,
            INDICODE,
            INGRCODE,
            WIJZE,
            ONDE_TYPE,
            FRACTIENR,
            TOTAAL_AANGEMELD,
            TOTAAL_WERKLIJST,
            TOTAAL_WERKLIJST_MEGA_POOL,
            MEGA_POOL_PLATE_ID,
            CASE
               WHEN     TOTAAL_AANGEMELD = TOTAAL_WERKLIJST
                    AND TOTAAL_WERKLIJST = TOTAAL_WERKLIJST_MEGA_POOL
                    AND UPPER (STATUS_ROBOT) = 'COMPLETE'
               THEN
                  'ALLES KLAAR'
               ELSE
                  ''
            END
               AS STATUS,
            NOTITIE,
            VERBORGEN_NOTI_ID,
            VERBORGEN_FLOW_ID
       FROM (  SELECT FLPR.VERBORGEN_KLEUR,
                      FLPR.VERBORGEN_SORTERING,
                      ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
                      FLFA.MEDE_ID AS FAVORIET,
                      ONDE.PERS_ID AS NAAR_HELIX,
                      ONGR.CODE AS GROEP,
                      beh_haal_familienr (ONDE.PERS_ID) AS FAMILIENR,
                      ONDE.ONDERZOEKNR,
                      TAAL.CODE TAAL,
                      ONDE.SPOED,
                      ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
                      INTE.CODE AS INDICODE,
                      INGR.CODE AS INGRCODE,
                      ONWY.OMSCHRIJVING AS WIJZE,
                      DECODE (ONDE.ONDERZOEKSTYPE,
                              'R', 'Research',
                              'D', 'Diagnostiek',
                              '')
                         AS ONDE_TYPE,
                      BEH_HAAL_FRACTIENR_MATE (onde.onde_id) AS FRACTIENR,
                      (SELECT COUNT (DISTINCT meet1.meet_id)
                         FROM kgc_onderzoeken onde1,
                              bas_metingen meti1,
                              bas_meetwaarden meet1,
                              kgc_stoftesten stof1,
                              kgc_technieken tech1,
                              kgc_stoftestgroepen stgr1,
                              kgc_protocol_stoftesten prst1
                        WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                              AND METI1.METI_ID = MEET1.METI_ID
                              AND MEET1.STOF_ID = STOF1.STOF_ID
                              AND STOF1.TECH_ID = TECH1.TECH_ID
                              AND STGR1.STGR_ID = PRST1.STGR_ID
                              AND PRST1.STOF_ID = STOF1.STOF_ID
                              AND METI1.STGR_ID = STGR1.STGR_ID
                              AND TECH1.CODE = 'PCR'
                              AND ONDE.ONDE_ID = ONDE1.ONDE_ID
                              AND UPPER (STOF1.OMSCHRIJVING) LIKE 'PCR%(MIP%)'
                              AND UPPER (STGR1.OMSCHRIJVING) LIKE '%ROBOT%MIP%' --AND METI1.METI_ID = METI.METI_ID
                                                                               )
                         AS TOTAAL_AANGEMELD,
                      (SELECT COUNT (DISTINCT meet1.meet_id)
                         FROM kgc_onderzoeken onde1,
                              bas_metingen meti1,
                              bas_meetwaarden meet1,
                              kgc_stoftesten stof1,
                              kgc_technieken tech1,
                              kgc_stoftestgroepen stgr1,
                              kgc_protocol_stoftesten prst1,
                              kgc_werklijst_items wlit1
                        WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                              AND METI1.METI_ID = MEET1.METI_ID
                              AND MEET1.MEET_ID = WLIT1.MEET_ID
                              AND MEET1.STOF_ID = STOF1.STOF_ID
                              AND STOF1.TECH_ID = TECH1.TECH_ID
                              AND STGR1.STGR_ID = PRST1.STGR_ID
                              AND PRST1.STOF_ID = STOF1.STOF_ID
                              AND METI1.STGR_ID = STGR1.STGR_ID
                              AND TECH1.CODE = 'PCR'
                              AND UPPER (STOF1.OMSCHRIJVING) LIKE 'PCR%(MIP%)'
                              AND UPPER (STGR1.OMSCHRIJVING) LIKE '%ROBOT%MIP%'
                              AND ONDE.ONDE_ID = ONDE1.ONDE_ID ---AND METI1.METI_ID = METI.METI_ID
                                                              )
                         AS TOTAAL_WERKLIJST,
                      (SELECT COUNT (DISTINCT meet1.meet_id)
                         FROM kgc_onderzoeken onde1,
                              bas_metingen meti1,
                              bas_meetwaarden meet1,
                              kgc_stoftesten stof1,
                              kgc_technieken tech1,
                              kgc_werklijst_items wlit1,
                              kgc_stoftestgroepen stgr1,
                              kgc_protocol_stoftesten prst1,
                              BEH_FLOW_DNA_POSTROBOT POST1,
                              BEH_FLOW_DNA_SEQUENCEFACILITY seq1
                        WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                              AND METI1.METI_ID = MEET1.METI_ID
                              AND MEET1.MEET_ID = WLIT1.MEET_ID
                              AND MEET1.STOF_ID = STOF1.STOF_ID
                              AND STOF1.TECH_ID = TECH1.TECH_ID
                              AND MEET1.MEET_ID = POST1.MEETID(+)
                              AND STGR1.STGR_ID = PRST1.STGR_ID
                              AND PRST1.STOF_ID = STOF1.STOF_ID
                              AND METI1.STGR_ID = STGR1.STGR_ID
                              AND POST1.ELUTION_BARCODE = seq1.ELUTION_BARCODE
                              AND TECH1.CODE = 'PCR'
                              AND UPPER (STOF1.OMSCHRIJVING) LIKE 'PCR%(MIP%)'
                              AND UPPER (STGR1.OMSCHRIJVING) LIKE '%ROBOT%MIP%'
                              AND ONDE.ONDE_ID = ONDE1.ONDE_ID --AND METI1.METI_ID = METI.METI_ID
                                                              )
                         AS TOTAAL_WERKLIJST_MEGA_POOL,
                      TO_CHAR (wm_concat (DISTINCT POST.STATE))
                         AS STATUS_ROBOT,
                      TO_CHAR (
                         wm_concat (
                            DISTINCT SEQ.SEQUENCEPLATEID || '-' || SEQ.SEQ_STATE))
                         AS MEGA_POOL_PLATE_ID,
                      NOTI.OMSCHRIJVING AS NOTITIE,
                      NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
                      FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
                 FROM kgc_onderzoeken onde,
                      kgc_onderzoek_indicaties onin,
                      bas_metingen meti,
                      bas_meetwaarden meet,
                      kgc_onderzoeksgroepen ongr,
                      kgc_onderzoekswijzen onwy,
                      kgc_notities noti,
                      kgc_indicatie_groepen ingr,
                      kgc_indicatie_teksten inte,
                      kgc_stoftestgroepen stgr,
                      kgc_protocol_stoftesten prst,
                      kgc_stoftesten stof,
                      kgc_technieken tech,
                      BEH_FLOW_DNA_POSTROBOT POST,
                      BEH_FLOW_DNA_SEQUENCEFACILITY seq,
                      BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
                      beh_flow_favorieten flfa,
                      KGC_TALEN TAAL
                WHERE     ONDE.ONDE_ID = ONIN.ONDE_ID(+)
                      AND METI.METI_ID = MEET.METI_ID
                      AND METI.ONDE_ID = ONDE.ONDE_ID
                      AND ONDE.ONGR_ID = ONGR.ONGR_ID
                      AND ONDE.KAFD_ID = ONWY.KAFD_ID
                      AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
                      AND ONDE.KAFD_ID = INGR.KAFD_ID
                      AND ONIN.INDI_ID = INTE.INDI_ID(+)
                      AND ONDE.ONDE_ID = NOTI.ID(+)
                      AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
                      AND NOTI.ENTITEIT(+) = 'ONDE'
                      AND NOTI.GEREED(+) = 'N'
                      AND INTE.INGR_ID = INGR.INGR_ID(+)
                      AND MEET.STOF_ID = STOF.STOF_ID
                      AND STGR.STGR_ID = PRST.STGR_ID
                      AND PRST.STOF_ID = STOF.STOF_ID
                      AND STOF.TECH_ID = TECH.TECH_ID
                      AND METI.STGR_ID = STGR.STGR_ID
                      AND ONDE.KAFD_ID = FLPR.KAFD_ID
                      AND ONDE.ONDE_ID = FLPR.ONDE_ID
                      AND ONDE.ONDE_ID = FLFA.ID(+)
                      AND MEET.MEET_ID = POST.MEETID(+)
                      AND POST.ELUTION_BARCODE = seq.ELUTION_BARCODE(+)
                      AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC40_VW'
                      AND ONDE.AFGEROND = 'N'
                      AND TECH.CODE = 'PCR'
                      AND UPPER (STGR.OMSCHRIJVING) LIKE '%ROBOT%MIP%'
                      AND UPPER (STOF.OMSCHRIJVING) LIKE 'PCR%(MIP%)'
                      AND ONDE.STATUS = 'G'
                      AND ONDE.ONDE_ID NOT IN
                             (SELECT ONDE1.ONDE_ID
                                FROM kgc_onderzoeken onde1,
                                     bas_metingen meti1,
                                     bas_meetwaarden meet1,
                                     kgc_stoftesten stof1,
                                     kgc_technieken tech1,
                                     kgc_stoftestgroepen stgr1,
                                     kgc_protocol_stoftesten prst1
                               WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                                     AND METI1.METI_ID = MEET1.METI_ID
                                     AND MEET1.STOF_ID = STOF1.STOF_ID
                                     AND STOF1.TECH_ID = TECH1.TECH_ID
                                     AND STGR1.STGR_ID = PRST1.STGR_ID
                                     AND PRST1.STOF_ID = STOF1.STOF_ID
                                     AND METI1.STGR_ID = STGR1.STGR_ID
                                     AND TECH1.CODE = 'MIP'
                                     AND STOF1.MEET_REKEN = 'S'
                                     AND MEET1.AFGEROND = 'J'
                                     AND ONDE.ONDE_ID = ONDE1.ONDE_ID
                                     AND METI1.METI_ID = METI.METI_ID)
             GROUP BY ONDE.PERS_ID,
                      ONGR.CODE,
                      ONDE.ONDE_ID,
                      ONDE.ONDERZOEKNR,
                      TAAL.CODE,
                      ONDE.SPOED,
                      ONDE.GEPLANDE_EINDDATUM,
                      ONDE.ONDERZOEKSTYPE,
                      INTE.CODE,
                      INGR.CODE,
                      ONWY.OMSCHRIJVING,
                      FLPR.VERBORGEN_KLEUR,
                      FLPR.VERBORGEN_SORTERING,
                      FLFA.MEDE_ID,
                      NOTI.OMSCHRIJVING,
                      --POST.ELUTION_BARCODE,
                      --SEQ.SEQ_STATE,
                      --SEQ.SEQUENCEPLATEID,
                      NOTI.NOTI_ID,
                      FLFA.FLOW_ID) testje
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            EINDDATUM ASC,
            ONDERZOEKNR ASC;

/
QUIT
