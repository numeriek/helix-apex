CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_DD_MC7_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "STATUS", "FRACTIENR", "FRACTIE_STATUS", "VERBORGEN_FLOW_ID") AS
  SELECT                        -- goedgekeurde onderzoeken zonder metingen
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            DECODE (FRAC.STATUS,
                    'A', 'Aangemeld',
                    'O', 'Opgewerkt',
                    'V', 'Vervallen',
                    'M', 'Mislukt',
                    'X', 'Verbruikt',
                    '')
               AS FRACTIE_STATUS,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_monsters onmo,
            bas_fracties frac,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_METAB_DD_MC7_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND onde.onde_id = flfa.id(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND ONDE.ONDE_ID NOT IN (SELECT METI.ONDE_ID
                                       FROM bas_metingen meti)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
