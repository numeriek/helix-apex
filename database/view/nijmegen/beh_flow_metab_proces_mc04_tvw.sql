CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC04_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "VERBORGEN_IMFI_ID", "DRAGON_ID", "IMPORT_DATUM", "AFGEHANDELD", "ZISNR", "AANSPREKEN", "AANVRAAG_TYPE", "RELATIE_CODE", "RELATIENAAM", "INDICODE", "MATERIAAL", "AANVRAAG", "OPMERKINGEN") AS
  select case
           when WM_CONCAT (IMPW3.EXTERNE_ID) is null or to_char (WM_CONCAT (IMPW3.EXTERNE_ID)) = 'DNA' -- Materiaal Reeds Aanwezig + DNA (mogelijk ook aanwezig!)
           then
             '#0B3861' -- navy
           else
             case -- Openstaande Dragon aanvragen
               when (sysdate - IMFI.IMPORT_DATUM) < FLPR.ORANJE then
                 '#5CAB5C' -- groen     --'#04B404'
               else
                 case
                   when ( (sysdate - IMFI.IMPORT_DATUM) >= FLPR.ORANJE and (sysdate - IMFI.IMPORT_DATUM) < FLPR.LICHTGROEN) then
                     '#AEB404' --  licht groen
                   else
                     case
                       when ( (sysdate - IMFI.IMPORT_DATUM) < FLPR.GROEN and (sysdate - IMFI.IMPORT_DATUM) >= FLPR.LICHTGROEN) then
                         '#ED8B1C' -- oranje     --'#FF8000'
                       else
                         '#CD5B5B' -- rood       --'#EE5252'
                     end
                 end
             end
         end as VERBORGEN_KLEUR
       , case
           when to_char (WM_CONCAT (IMPW3.EXTERNE_ID)) is null or to_char (WM_CONCAT (IMPW3.EXTERNE_ID)) = 'DNA' -- Materiaal Reeds Aanwezig + DNA (mogelijk ook aanwezig!)
           then
             0 --- navy
           else
             case
               when (sysdate - IMFI.IMPORT_DATUM) < FLPR.ORANJE then
                 4 -- groen
               else
                 case
                   when ( (sysdate - IMFI.IMPORT_DATUM) >= FLPR.ORANJE and (sysdate - IMFI.IMPORT_DATUM) < FLPR.LICHTGROEN) then
                     3 -- licht groen
                   else
                     case
                       when ( (sysdate - IMFI.IMPORT_DATUM) < FLPR.GROEN and (sysdate - IMFI.IMPORT_DATUM) >= FLPR.LICHTGROEN) then
                         2 -- oranje
                       else
                         1 -- rood
                     end
                 end
             end
         end as VERBORGEN_SORTERING
       , substr (IMFI.FILENAAM
               , 1
               , instr (IMFI.FILENAAM
                      ,' ') - 1)
           as VERBORGEN_TELLING
       , IMFI.IMFI_ID as VERBORGEN_IMFI_ID
       , substr (IMFI.FILENAAM
               , 1
               , instr (IMFI.FILENAAM
                      ,' ') - 1)
           as DRAGON_ID
       , IMFI.IMPORT_DATUM
       , IMFI.AFGEHANDELD
       , IMPE.ZISNR
       , IMPE.AANSPREKEN
       , IMPW.EXTERNE_ID as AANVRAAG_TYPE
       , RELA.CODE as RELATIE_CODE
       , RELA.AANSPREKEN as RELATIENAAM
       , to_char (WM_CONCAT (INTE.CODE || '-' || INTE.OMSCHRIJVING || ' ')) as INDICODE
       , case
           when WM_CONCAT (IMPW3.EXTERNE_ID) is null then 'MATERIAAL REEDS AANWEZIG'
           else to_char (WM_CONCAT (distinct IMPW3.EXTERNE_ID))
         end
       , case
           when BEST.BESTAND_SPECIFICATIE is null then ''
           else '<a href="' || BEST.BESTAND_SPECIFICATIE || '" target="_blank">Open</a>'
         end as AANVRAAG
       , IMFI.OPMERKINGEN
  from   KGC_IMPORT_LOCATIES imlo
       , kgc_import_files imfi
       , kgc_im_personen impe
       , kgc_im_onderzoeken imon
       , kgc_import_param_waarden impw
       , kgc_import_param_waarden impw1
       , kgc_relaties rela
       , kgc_im_onderzoek_indicaties imin
       , kgc_import_param_waarden impw2
       , kgc_indicatie_teksten inte
       , kgc_im_monsters immo
       , kgc_import_param_waarden impw3
       , kgc_bestanden best
       , BEH_FLOW_PRIORITEIT_VW flpr
  where  FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC3_VW'
     and IMLO.IMLO_ID = IMFI.IMLO_ID
     and IMPE.IMFI_ID = IMFI.IMFI_ID
     and IMON.IMFI_ID = IMFI.IMFI_ID
     and IMFI.IMFI_ID = IMMO.IMFI_ID
     and IMMO.IMPW_ID_MATE = IMPW3.IMPW_ID(+)
     and IMPW.IMPW_ID = IMON.IMPW_ID_ONDERZOEKSWIJZE
     and IMPW1.IMPW_ID = IMON.IMPW_ID_RELA(+)
     and IMPW1.HELIX_WAARDE = RELA.RELA_ID(+)
     and BEST.ENTITEIT_PK = IMFI.IMFI_ID
     and BEST.ENTITEIT_CODE = 'IMFI'
     and IMPW1.HELIX_TABLE_DOMAIN = 'KGC_RELATIES'
     and IMLO.DIRECTORYNAAM = 'Dragon'
     and IMIN.IMFI_ID = IMFI.IMFI_ID
     and IMIN.IMON_ID = IMON.IMON_ID
     and IMPW2.IMPW_ID = IMIN.IMPW_ID_INDI
     and IMPW2.HELIX_WAARDE = INTE.INDI_ID
     and IMPW2.HELIX_TABLE_DOMAIN = 'KGC_INDICATIE_TEKSTEN'
     and IMFI.FILENAAM like '%a=6%'
     and IMMO.IMPW_ID_KAFD = 29596
  group by IMFI.IMPORT_DATUM, FLPR.ORANJE, FLPR.GROEN, flpr.LICHTGROEN, IMFI.FILENAAM, IMFI.IMFI_ID, IMFI.IMPORT_DATUM
         , IMPE.ZISNR, IMPE.AANSPREKEN, IMPW.EXTERNE_ID, RELA.CODE, RELA.AANSPREKEN, IMFI.AFGEHANDELD, BEST.BESTAND_SPECIFICATIE
         , IMFI.OPMERKINGEN
  union
  select case
           when (sysdate - IMFI.IMPORT_DATUM) < FLPR.ORANJE then
             '#5CAB5C' -- groen     --'#04B404'
           else
             case
               when ( (sysdate - IMFI.IMPORT_DATUM) >= FLPR.ORANJE and (sysdate - IMFI.IMPORT_DATUM) < FLPR.LICHTGROEN) then
                 '#AEB404' --  licht groen
               else
                 case
                   when ( (sysdate - IMFI.IMPORT_DATUM) < FLPR.GROEN and (sysdate - IMFI.IMPORT_DATUM) >= FLPR.LICHTGROEN) then
                     '#ED8B1C' -- oranje     --'#FF8000'
                   else
                     '#CD5B5B' -- rood       --'#EE5252'
                 end
             end
         end as VERBORGEN_KLEUR
       , case
           when (sysdate - IMFI.IMPORT_DATUM) < FLPR.ORANJE then
             4 -- groen
           else
             case
               when ( (sysdate - IMFI.IMPORT_DATUM) >= FLPR.ORANJE and (sysdate - IMFI.IMPORT_DATUM) < FLPR.LICHTGROEN) then
                 3 -- licht groen
               else
                 case
                   when ( (sysdate - IMFI.IMPORT_DATUM) < FLPR.GROEN and (sysdate - IMFI.IMPORT_DATUM) >= FLPR.LICHTGROEN) then
                     2 -- oranje
                   else
                     1 -- rood
                 end
             end
         end as VERBORGEN_SORTERING
       , substr (IMFI.FILENAAM
               , 1
               , instr (IMFI.FILENAAM
                      ,' ') - 1) as VERBORGEN_TELLING
       , IMFI.IMFI_ID as VERBORGEN_IMFI_ID
       , substr (IMFI.FILENAAM
               , 1
               , instr (IMFI.FILENAAM
                      ,' ') - 1)
           as DRAGON_ID
       , IMFI.IMPORT_DATUM
       , IMFI.AFGEHANDELD
       , IMPE.ZISNR
       , IMPE.AANSPREKEN
       , 'Alleen opslag'
       , ''
       , ''
       , ''
       , IMPW.EXTERNE_ID
       , case
           when BEST.BESTAND_SPECIFICATIE is null then ''
           else '<a href="' || BEST.BESTAND_SPECIFICATIE || '" target="_blank">Open</a>'
         end as AANVRAAG
       , IMFI.OPMERKINGEN
  from   KGC_IMPORT_LOCATIES imlo
       , kgc_import_files imfi
       , kgc_im_personen impe
       , kgc_im_monsters immo
       , kgc_import_param_waarden impw
       , kgc_bestanden best
       , BEH_FLOW_PRIORITEIT_VW flpr
  where  FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC3_VW'
     and IMLO.IMLO_ID = IMFI.IMLO_ID
     and IMPE.IMFI_ID = IMFI.IMFI_ID
     and IMMO.IMFI_ID = IMFI.IMFI_ID
     and IMMO.IMPW_ID_MATE = IMPW.IMPW_ID
     and BEST.ENTITEIT_PK = IMFI.IMFI_ID
     and BEST.ENTITEIT_CODE = 'IMFI'
     and IMFI.IMFI_ID not in (select IMON.IMFI_ID
                              from   kgc_im_onderzoeken imon)
     and IMLO.DIRECTORYNAAM = 'Dragon'
     and IMFI.FILENAAM like '%a=6%'
     and IMMO.IMPW_ID_KAFD = 29596
  union all
  select -- als een fake record toegevoegd, anders onstaan er problemen bij het exporteren van data vanuit Flow naar excel
        ''
       , 0
       , ''
       , 0
       , ''
       , null
       , ''
       , ''
       , ''
       , ''
       , ''
       , ''
       , ''
       , ''
       , ''
       , ''
  from   dual
  order by AFGEHANDELD desc, VERBORGEN_SORTERING asc, IMPORT_DATUM asc, DRAGON_ID asc;

/
QUIT
