CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_OV3A_VW" ("KLEUR", "AANTAL", "PCRPLATEBARCODE") AS
  SELECT CASE
               WHEN FLPR.VERBORGEN_SORTERING = 1
               THEN
                  'rood: ' || COUNT (DISTINCT (ONDE.ONDERZOEKNR))
               ELSE
                  CASE
                     WHEN FLPR.VERBORGEN_SORTERING = 2
                     THEN
                        'oranje: ' || COUNT (DISTINCT (ONDE.ONDERZOEKNR))
                     ELSE
                        CASE
                           WHEN FLPR.VERBORGEN_SORTERING = 3
                           THEN
                              'licht_groen: '
                              || COUNT (DISTINCT (ONDE.ONDERZOEKNR))
                           ELSE
                              CASE
                                 WHEN FLPR.VERBORGEN_SORTERING = 4
                                 THEN
                                    'groen: '
                                    || COUNT (DISTINCT (ONDE.ONDERZOEKNR))
                                 ELSE
                                    CASE
                                       WHEN FLPR.VERBORGEN_SORTERING = 5
                                       THEN
                                          'blauw: '
                                          || COUNT (
                                                DISTINCT (ONDE.ONDERZOEKNR))
                                       ELSE
                                          CASE
                                             WHEN FLPR.VERBORGEN_SORTERING = 6
                                             THEN
                                                'navy: '
                                                || COUNT (
                                                      DISTINCT (ONDE.
                                                                 ONDERZOEKNR))
                                          END
                                    END
                              END
                        END
                  END
            END
               AS kleur,
            COUNT (DISTINCT (ONDE.ONDERZOEKNR)) AS aantal,
            POST.PCRPLATEBARCODE
       FROM beh_flow_dna_postrobot POST,
            kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            BEH_FLOW_PRIORITEIT_NEW_VW flpr
      WHERE     POST.INVESTIGATION_ID = ONDE.ONDERZOEKNR
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC17_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND ONDE.ONDERZOEKSTYPE = 'D'
            AND UPPER (POST.STATE) LIKE 'PRESETUPCOMPLETE%'
   GROUP BY FLPR.VERBORGEN_SORTERING, POST.PCRPLATEBARCODE
   ORDER BY PCRPLATEBARCODE,
            VERBORGEN_SORTERING,
            COUNT (DISTINCT (ONDE.ONDERZOEKNR)) DESC;

/
QUIT
