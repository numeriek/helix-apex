CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_AANVRAGEN_VW" ("DRAGON_ID", "CREATION_DATE") AS
  SELECT DISTINCT
          CASE
             WHEN SUBSTR (IMFI.FILENAAM, 1, INSTR (IMFI.FILENAAM, '-') - 1)
                     IS NULL
             THEN
                SUBSTR (IMFI.FILENAAM, 1, INSTR (IMFI.FILENAAM, ' ') - 1)
             ELSE
                SUBSTR (IMFI.FILENAAM, 1, INSTR (IMFI.FILENAAM, '-') - 1)
          END
             AS DRAGON_ID,
          IMFI.CREATION_DATE
     FROM KGC_IMPORT_LOCATIES imlo, kgc_import_files imfi
    WHERE     IMLO.IMLO_ID = IMFI.IMLO_ID
          AND IMLO.DIRECTORYNAAM = 'Dragon'
          AND IMFI.CREATION_DATE > SYSDATE - 180;

/
QUIT
