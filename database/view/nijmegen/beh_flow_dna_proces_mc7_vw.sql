CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC7_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "INFO", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT            -- Overzicht "Goedgekeurde Onderzoeken zonder METINGEN"
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            LISTAGG (FAMI.FAMILIENUMMER, ', ')
               WITHIN GROUP (ORDER BY FAMI.FAMILIENUMMER)
               AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            /*DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,*/
            LISTAGG (FRAC.FRACTIENUMMER, ', ')
               WITHIN GROUP (ORDER BY FRAC.FRACTIENUMMER)
               AS FRACTIENR,
            --'GEEN METING - onderzoek goedgekeurd!' AS INFO,
            CASE
               WHEN ONDE.ONDE_ID IN
                       (SELECT ONDE1.ONDE_ID
                          FROM BAS_MEETWAARDEN MEET,
                               BAS_METINGEN METI,
                               KGC_ONDERZOEKEN ONDE1
                         WHERE     ONDE1.ONDE_ID = METI.ONDE_ID
                               AND METI.STGR_ID IS NULL
                               AND METI.METI_ID = MEET.METI_ID)
               THEN
                  'LEGE METING met Stoftesten!'
               ELSE
                  CASE
                     WHEN ONDE.ONDE_ID NOT IN
                             (SELECT ONDE1.ONDE_ID
                                FROM BAS_MEETWAARDEN MEET,
                                     BAS_METINGEN METI,
                                     KGC_ONDERZOEKEN ONDE1
                               WHERE ONDE1.ONDE_ID = METI.ONDE_ID
                                     AND METI.METI_ID = MEET.METI_ID)
                     THEN
                        'LEGE METING zonder Stoftesten!'
                  END
            END
               AS INFO,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_onderzoek_monsters onmo,
            bas_fracties frac,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC2_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.AFGEROND = 'N'
            AND (ONDE.ONDE_ID NOT IN
                    (SELECT ONDE1.ONDE_ID
                       FROM BAS_MEETWAARDEN MEET,
                            BAS_METINGEN METI,
                            KGC_ONDERZOEKEN ONDE1
                      WHERE ONDE1.ONDE_ID = METI.ONDE_ID
                            AND METI.METI_ID = MEET.METI_ID)
                 OR ONDE.ONDE_ID IN
                       (SELECT ONDE1.ONDE_ID
                          FROM BAS_MEETWAARDEN MEET,
                               BAS_METINGEN METI,
                               KGC_ONDERZOEKEN ONDE1
                         WHERE     ONDE1.ONDE_ID = METI.ONDE_ID
                               AND METI.STGR_ID IS NULL
                               AND METI.METI_ID = MEET.METI_ID))
            AND ONDE.STATUS = 'G'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.ONDE_ID,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
