CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_EAS_KLINIEK_INDICATIES_VW" ("AFDELING", "ONDERZOEKSGROEP", "KLINIEKINDICATIE_GROEP_CODE", "KLINIEKINDICATIE_GROEP", "KLINIEKINDICATIE_CODE", "KLINIEKINDICATIE", "CODE_ENG") AS
  SELECT kafd.code afdeling,
          ongr.code onderzoeksgroep,
          ingr.code ingr_code,
          ingr.omschrijving ingr_omschrijving,
          indi.code indi_code,
          DECODE (
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id),
             '*', indi.code,
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id))
             dragon_code,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGON_CODE_ENG',
                              indi.indi_id)
             dragon_code_eng
     FROM kgc_indicatie_groepen ingr,
          kgc_indicatie_teksten indi,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd
    WHERE     ingr.ingr_id = indi.ingr_id
          AND ingr.ongr_id = ongr.ongr_id
          AND ingr.kafd_id = kafd.kafd_id
          AND kafd.code = 'METAB'
          AND ( (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                     'DRAGON_CODE',
                                     indi.indi_id)
                    IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_OMSCHRIJVING',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_CODE_ENG',
                                       indi.indi_id)
                      IS NOT NULL))
          AND indi.vervallen = 'N'
          AND ingr.vervallen = 'N'
 union
   SELECT decode(kafd.code, 'METAB', 'GENOOM', null)  afdeling,
          decode(ongr.code, 'MITO','P') onderzoeksgroep,
          ingr.code ingr_code,
          ingr.omschrijving ingr_omschrijving,
          indi.code indi_code,
          DECODE (
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id),
             '*', indi.code,
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id))
             dragon_code,
          nvl(kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGON_CODE_ENG',
                              indi.indi_id), kgc_vert_00.vertaling('INDI', indi.indi_id, 3))
             dragon_code_eng
     FROM kgc_indicatie_groepen ingr,
          kgc_indicatie_teksten indi,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd
    WHERE     ingr.ingr_id = indi.ingr_id
          AND ingr.ongr_id = ongr.ongr_id
          AND ingr.kafd_id = kafd.kafd_id
          AND kafd.code = 'METAB'
          and ongr.code = 'MITO'
          AND ( (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                     'DRAGON_CODE',
                                     indi.indi_id)
                    IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_OMSCHRIJVING',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_CODE_ENG',
                                       indi.indi_id)
                      IS NOT NULL))
          AND indi.vervallen = 'N'
          AND ingr.vervallen = 'N';

/
QUIT
