CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC07_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "MONSTERNR", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "TOTAAL", "NIET_GECONTROLEERD", "GECONTROLEERD", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT                                         -- controleren meetwaarden
           '#79A8A8',                                  --FLPR.VERBORGEN_KLEUR,
            '',                                    --FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            BEH_HAAL_MONSTERNR (onde.onde_id) AS MONSTERNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            OMM.TOTAAL_MEET,
            OMM.NCONT,
            OMM.WCONT,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_notities noti,
            beh_flow_favorieten flfa,
            BEH_FLOW_ONDE_MEET_METI_MATVW omm
      WHERE                  --FLPR.VIEW_NAME = 'BEH_FLOW_CYTO_PROCES_MC01_VW'
                --AND
                ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND ONDE.ONDE_ID = OMM.ONDE_ID
            AND OMM.MEET_NIET_AF = 0
            AND OMM.NCONT <> 0
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY                                             --VERBORGEN_SORTERING,
           ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
