CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_VERHUIS_UITS_FILES_VW" ("BEST_ID", "JAARTAL", "KWARTAAL", "BESTAND_SPECIFICATIE", "BESTAND_SPECIFICATIE_NEW", "LOCATIE", "LOCATIE_NEW") AS
  select "BEST_ID","JAARTAL","KWARTAAL","BESTAND_SPECIFICATIE","BESTAND_SPECIFICATIE_NEW","LOCATIE","LOCATIE_NEW"
from
(
select best.best_id
, to_char(best.creation_date, 'YYYY') jaartal
, to_char(best.creation_date, 'Q') kwartaal
, best.bestand_specificatie
, '\\UMCMS005\GEN_STG$\HELIX\'||
substr( SYS_CONTEXT('USERENV','DB_NAME'), 1, 4)
||'\UITSLAGEN\' || to_char(best.creation_date, 'YYYY') || '\Q' || to_char(best.creation_date, 'Q') || '\'|| best.bestandsnaam bestand_specificatie_new
, best.locatie
, '\\UMCMS005\GEN_STG$\HELIX\'||
substr(SYS_CONTEXT('USERENV','DB_NAME'), 1, 4)
||'\UITSLAGEN\' || to_char(best.creation_date, 'YYYY') || '\Q' || to_char(best.creation_date, 'Q') || '\' locatie_new
from kgc_bestanden best
where best.entiteit_code = 'UITS'
and best.btyp_id != (select btyp_id from kgc_bestand_types where code = 'BIJLAGE')
and best.bestandsnaam is not null
and not exists
(
select null
from kgc_bestanden_log
where best_id = best.best_id
)
order by best.creation_date
)
where rownum <= 20000 -- per run verwerken.;

/
QUIT
