CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_KLINGEN_ONDERZOEKEN_VW" ("PERS_ID", "ONDE_ID", "ONDERZOEKNR") AS
  SELECT
onde.pers_id, onde.onde_id, onde.onderzoeknr
FROM KGC_ONDERZOEKEN onde, KGC_KGC_AFDELINGEN kafd
WHERE onde.kafd_id = kafd.kafd_id
AND UPPER(kafd.code) = UPPER('KLINGEN')

 ;

/
QUIT
