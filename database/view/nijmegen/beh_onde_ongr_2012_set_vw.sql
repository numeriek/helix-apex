CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ONDE_ONGR_2012_SET_VW" ("PERS_ID", "ARR", "BASIS", "DD", "GLYCOS", "HOM", "KWEEK", "LTG", "LYSO", "MITO", "NGS", "OVERIG", "P", "POST_NIJM", "PR", "PRE_NIJM", "RFD", "TUM_NIJM") AS
  select onde.pers_id
, decode(ongr.code, 'ARR', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) ARR
, decode(ongr.code, 'BASIS', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) "BASIS"
, decode(ongr.code, 'DD', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) DD
, decode(ongr.code, 'GLYCOS', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) GLYCOS
, decode(ongr.code, 'HOM', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) HOM
, decode(ongr.code, 'KWEEK', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) KWEEK
, decode(ongr.code, 'LTG', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) LTG
, decode(ongr.code, 'LYSO', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) LYSO
, decode(ongr.code, 'MITO', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) MITO
, decode(ongr.code, 'NGS', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) NGS
, decode(ongr.code, 'OVERIG', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) OVERIG
, decode(ongr.code, 'P', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) "P"
, decode(ongr.code, 'POST_NIJM', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) POST_NIJM
, decode(ongr.code, 'PR', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) PR
, decode(ongr.code, 'PRE_NIJM', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) PRE_NIJM
, decode(ongr.code, 'RFD', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) RFD
, decode(ongr.code, 'TUM_NIJM', to_char(onde.datum_binnen, 'dd-mm-yyyy'), 0) TUM_NIJM
from kgc_onderzoeken onde
, kgc_onderzoeksgroepen ongr
where onde.ongr_id = ongr.ongr_id
and onde.kafd_id in (1, 6)
and onderzoekstype <>'R'
and onde.datum_binnen > to_date('01-01-2011','dd-mm-yyyy')
group by pers_id
, ongr.code
, onde.datum_binnen;

/
QUIT
