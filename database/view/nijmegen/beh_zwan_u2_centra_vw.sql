CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ZWAN_U2_CENTRA_VW" ("ZWAN_ID") AS
  select foet.zwan_id
from kgc_foetussen foet
, kgc_onderzoeken onde
where onde.foet_id = foet.foet_id
and onde.onderzoekswijze = 'U2'
and exists ( select *
             from kgc_onderzoeken onde2
             , kgc_foetussen  foet2
             where onde2.foet_id = foet2.foet_id
             and foet2.zwan_id = foet.zwan_id
             and onde2.ongr_id <> onde.ongr_id
             and onde2.onderzoekswijze = 'U2'
           )
group by foet.zwan_id
 ;

/
QUIT
