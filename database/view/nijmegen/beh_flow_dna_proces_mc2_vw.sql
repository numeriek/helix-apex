CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC2_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ARCHIEF", "ONDERZOEKNR", "BI_BU", "INT_EXT", "AANVRAAG", "SPOED", "BINNEN", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "CYTOMAT", "NOTI_CODE", "NOTITIE", "NOTI_COMMENTAAR", "NOTI_GEREED", "FAMILIE_NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_BESTANDEN
        AS (SELECT BEST.ENTITEIT_PK,
                   BEST.ENTITEIT_CODE,
                   beh_bestanden_to_string (
                      CAST (
                         COLLECT (
                               '<a href="'
                            || BEST.BESTAND_SPECIFICATIE
                            || '" target="_blank">Open</a>' ORDER                                                       BY BEST.BEST_ID desc ) as beh_bestanden_link)) as bestanden
                FROM KGC_BESTANDEN BEST, KGC_BESTAND_CATEGORIEEN BCAT
                WHERE BEST.BCAT_ID = BCAT.BCAT_ID
                    AND BEST.ENTITEIT_CODE  = 'ONDE'
                    AND BCAT.CODE <> ('DUMMY')
                    AND BEST.VERVALLEN = 'N'
                    AND BEST.VERWIJDERD = 'N'
                group by best.ENTITEIT_PK, BEST.ENTITEIT_CODE),
--   WITH w_kgc_bestanden
--        AS (SELECT *
--              FROM (SELECT best.*,
--                           LISTAGG (
--                              CASE
--                                 WHEN best.bestand_specificatie IS NULL
--                                 THEN
--                                    ''
--                                 ELSE
--                                       '<a href="'
--                                    || best.bestand_specificatie
--                                    || '" target="_blank">Open</a>'
--                              END,
--                              ' ')
--                           WITHIN GROUP (ORDER BY best.entiteit_pk)
--                           OVER (PARTITION BY best.entiteit_pk)
--                              aanvraag,
--                           ROW_NUMBER ()
--                           OVER (PARTITION BY best.entiteit_pk
--                                 ORDER BY best.entiteit_pk)
--                              AS row_nr
--                      FROM kgc_bestanden best, kgc_bestand_categorieen bcat
--                     WHERE     BEST.BCAT_ID = BCAT.BCAT_ID
--                           AND best.entiteit_code = 'ONDE'
--                           AND BCAT.CODE <> 'DUMMY')
--             WHERE row_nr = 1),
        w_bas_fracties
        AS (SELECT *
              FROM (SELECT frac.*,
                           LISTAGG (
                              ' ' || FRAC.FRACTIENUMMER || ' - '
                              || CASE
                                    WHEN FLDS.AVAILABLE = 'NIL'
                                    THEN
                                       'REMP'
                                    ELSE
                                       CASE
                                          WHEN FLDS.AVAILABLE IS NULL
                                          THEN
                                             'GEEN'
                                          ELSE
                                             FLDS.AVAILABLE
                                       END
                                 END,
                              ',')
                           WITHIN GROUP (ORDER BY frac.mons_id)
                           OVER (PARTITION BY frac.mons_id)
                              CYTOMAT,
                           ROW_NUMBER ()
                           OVER (PARTITION BY frac.mons_id
                                 ORDER BY frac.frac_id)
                              AS row_nr
                      FROM BEH_FLOW_DNA_STORAGE FLDS, bas_fracties frac
                     WHERE FRAC.FRACTIENUMMER = FLDS.ID(+))
             WHERE row_nr = 1)
     SELECT                                     -- Onderzoeken ter goedkeuring
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            FAMI.ARCHIEF,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            DECODE (beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
                    'J', 'INTERN',
                    'N', '')
               AS INT_EXT,
            --            TAAL.CODE TAAL,
            BEST.BESTANDEN,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.CYTOMAT,
            NOTI.CODE AS NOTICODE,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.COMMENTAAR AS NOTICOMMENTAAR,
            NOTI.GEREED AS NOTI_GEREED,
            NOTIFAMI.FAMILIE_NOTITIE AS FAMILIE_NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_onderzoek_monsters onmo,
            kgc_monsters mons,
            w_bas_fracties frac,
            (  SELECT id,
                      LISTAGG (
                         CASE
                            WHEN OMSCHRIJVING IS NOT NULL
                            THEN
                                  ' '
                               || LAST_UPDATE_DATE
                               || ' - '
                               || CODE
                               || ' - '
                               || OMSCHRIJVING
                               || ' ~~~~ '
                            ELSE
                               ''
                         END,
                         ',')
                      WITHIN GROUP (ORDER BY LAST_UPDATE_DATE DESC)
                         AS FAMILIE_NOTITIE
                 FROM kgc_notities
                WHERE ENTITEIT = 'FAMI'
                    AND GEREED = 'N'
             GROUP BY ID) notifami,
            w_kgc_bestanden best,
            BEH_FLOW_PRIORITEIT_NEW_VW flpr,
            beh_flow_favorieten flfa                                       --,
      --            kgc_talen taal
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC2_VW'
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND FAMI.FAMI_ID = NOTIFAMI.ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND FLPR.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            --            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = ONMO.ONDE_ID(+)
            AND ONMO.MONS_ID = MONS.MONS_ID(+)
            AND MONS.MONS_ID = FRAC.MONS_ID(+)
            AND ONDE.AFGEROND = 'N'
            AND NVL (ONDE.STATUS, 'onb') NOT IN ('G', 'V')
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
