CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_AANV_KOHO_VW" ("UITS_ID", "RELA_ID", "KOHO") AS
  SELECT                                                              -- aanvrager onderzoek
            uits.uits_id, onde.rela_id, 'N'
      FROM kgc_onderzoeken onde, kgc_uitslagen uits
     WHERE uits.onde_id = onde.onde_id
    UNION
    SELECT                                                                    -- relatie uitslag
            uits.uits_id, uits.rela_id, 'N'
      FROM kgc_uitslagen uits
    UNION
    SELECT                                                             -- kopiehouders uitslag
            uits.uits_id, koho.rela_id, 'J'
      FROM kgc_uitslagen uits, kgc_kopiehouders koho
     WHERE uits.uits_id = koho.uits_id
 ;

/
QUIT
