CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_TE_HERSTELLEN_DECL_RELA_VW" ("RELA_CODE", "AANSPREKEN", "SPECIALISME", "AGB", "ACTIEHOUDER") AS
  select  distinct(rela.code) rela_code
,       rela.aanspreken
,       (case
        when rela.spec_id is null then 'Specialisme ontbreekt.'
        else 'OK (' || spec.code || ')'
        end) specialisme
,       (case
        when trim(kgc_attr_00.waarde('KGC_RELATIES', 'AGB_CODE', decl.rela_id)) is null then 'AGB-code ontbreekt.'
        else 'OK (' || trim(kgc_attr_00.waarde('KGC_RELATIES', 'AGB_CODE', decl.rela_id)) || ')'
        end) agb
 ,      acha.kafd_code actieHouder
from kgc_declaraties_vw decl, kgc_relaties rela, kgc_kgc_afdelingen kafd,
kgc_specialismen spec
,(
select  rela.code rela_code
,       kafd.code kafd_code
,       count(*) aantal_decl
,       ROW_NUMBER() OVER (PARTITION BY rela.code ORDER BY rela.code) row_num
from kgc_declaraties_vw decl, kgc_relaties rela, kgc_kgc_afdelingen kafd
where decl.rela_id = rela.rela_id
and   decl.kafd_id = kafd.kafd_id
and   datum_aanvraag > (sysdate - 735)
and (
  (trim(kgc_attr_00.waarde('KGC_RELATIES', 'AGB_CODE', decl.rela_id)) is null) or
  (rela.spec_id is null)
  )
and nvl(upper(rela.land), 'NEDERLAND') = 'NEDERLAND'
group by rela.code, kafd.code
order by 1, 3 desc
) acha
where decl.rela_id = rela.rela_id
and   decl.kafd_id = kafd.kafd_id
and   rela.spec_id = spec.spec_id(+)
and   rela.code = acha.rela_code
and   datum_aanvraag > (sysdate - 735)
and (
  (trim(kgc_attr_00.waarde('KGC_RELATIES', 'AGB_CODE', decl.rela_id)) is null) or
  (rela.spec_id is null)
  )
and nvl(upper(rela.land), 'NEDERLAND') = 'NEDERLAND'
and acha.row_num = 1;

/
QUIT
