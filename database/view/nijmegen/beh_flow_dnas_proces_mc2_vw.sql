CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNAS_PROCES_MC2_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "SPOED", "ISOLATIELIJSTNR", "VOLGNR", "FRACTIENR", "MONSTERNR", "MATERIAAL", "BUISNR", "PROCES_CODE", "STAP_GEDAAN", "COMMENTAAR") AS
  SELECT CASE                               -- fracties op de isolatielijst
               WHEN (SYSDATE - L_FRAC.CREATION_DATE) < FLPR.ORANJE
               THEN
                  '#04B404'                                           -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - L_FRAC.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - L_FRAC.CREATION_DATE) <
                                  FLPR.LICHTGROEN)
                     THEN
                        '#AEB404'                              --  licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - L_FRAC.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - L_FRAC.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              '#FF8000'                              -- oranje
                           ELSE
                              '#FF0000'                                -- rood
                        END
                  END
            END
               AS verborgen_kleur,
            CASE
               WHEN (SYSDATE - L_FRAC.CREATION_DATE) < FLPR.ORANJE
               THEN
                  4                                                   -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - L_FRAC.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - L_FRAC.CREATION_DATE) <
                                  FLPR.LICHTGROEN)
                     THEN
                        3                                       -- licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - L_FRAC.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - L_FRAC.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              2                                      -- oranje
                           ELSE
                              1                                        -- rood
                        END
                  END
            END
               AS verborgen_sortering,
            L_FRAC.FRACTIENUMMER AS VERBORGEN_TELLING,
            L_MONS.PERS_ID AS VERBORGEN_PERS_ID,
            DECODE (bas_isol_00.spoed (ISOL.MONS_ID), 'J', 'J', NULL) SPOED,
            ISOL.ISOLATIELIJST_NUMMER ISOLATIELIJSTNR,
            ISOL.VOLGNUMMER VOLGNR,
            L_FRAC.FRACTIENUMMER L_FRAC_FRACTIENUMMER,
            NVL (L_FRAC2.FRACTIENUMMER, L_MONS.MONSTERNUMMER)
               L_MONS_MONSTERNUMMER,
            L_MATE.CODE MATERIAAL,
            ISOL.BUISNR BUISNR,
            PROC.CODE,
            ISOL.STAP STAP_GEDAAN,
            L_MONS.COMMENTAAR COMMENTAAR
       FROM KGC_MATERIALEN L_MATE,
            BAS_FRACTIES L_FRAC2,
            BAS_FRACTIES L_FRAC,
            KGC_MONSTERS L_MONS,
            BAS_ISOLATIELIJSTEN ISOL,
            KGC_PROCESSEN PROC,
            BEH_FLOW_PRIORITEIT_VW flpr
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNAS_PROCES_MC2_VW'
            AND ISOL.KAFD_ID = flpr.kafd_id
            AND ISOL.MONS_ID = L_MONS.MONS_ID
            AND ISOL.FRAC_ID = L_FRAC.FRAC_ID
            AND L_FRAC.FRAC_ID_PARENT = L_FRAC2.FRAC_ID(+)
            AND L_MONS.MATE_ID = L_MATE.MATE_ID
            AND ISOL.PROC_ID = PROC.PROC_ID(+)
            AND L_FRAC.STATUS = 'A'
            AND ISOL.STAP = 0
   ORDER BY verborgen_sortering,
            spoed,
            ISOL.ISOLATIELIJST_NUMMER,
            ISOL.VOLGNUMMER;

/
QUIT
