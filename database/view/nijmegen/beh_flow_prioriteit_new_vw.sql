CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_PRIORITEIT_NEW_VW" ("KAFD_ID", "FLOW_ID", "ONDE_ID", "FLDE_ID", "VIEW_NAME", "DLT_BENODIGDE", "DLT_HERIJKT_DAGEN", "ROOD", "ORANJE", "LICHTGROEN", "GROEN", "DLT_HERIJKT_DAGEN_CUM", "GEPLANDE_EINDDATUM", "VERBORGEN_KLEUR", "VERBORGEN_SORTERING") AS
  SELECT FLOW.KAFD_ID,
          FLOW.FLOW_ID,
          ONDA.ONDE_ID,
          ONDA.FLDE_ID,
          FLDE.VIEW_NAME,
          FLDE.DLT_BENODIGDE,
          ONDA.DLT_HERIJKT_DAGEN,
          FLDE.DLT_BENODIGDE AS ROOD,
          (ONDA.DLT_HERIJKT_DAGEN - FLDE.DLT_BENODIGDE) / 3
          + FLDE.DLT_BENODIGDE
             AS ORANJE,
          ( (ONDA.DLT_HERIJKT_DAGEN - FLDE.DLT_BENODIGDE) / 3) * 2
          + FLDE.DLT_BENODIGDE
             AS LICHTGROEN,
          ONDA.DLT_HERIJKT_DAGEN AS GROEN,
          ONDA.DLT_HERIJKT_DAGEN_CUM,
          onde.geplande_einddatum,
          CASE
             WHEN ONDA.PARAM_01_CHECK = 'KLEINER'
             THEN
                '#0B0B61'
             ELSE
                CASE
                   WHEN ONDA.PARAM_01_CHECK = 'LEEG'
                   THEN
                      '#79A8D6'
                   ELSE
                      CASE
                         WHEN (onde.geplande_einddatum - SYSDATE - ONDA.DLT_HERIJKT_DAGEN_CUM) >
                                 ( ( (ONDA.DLT_HERIJKT_DAGEN
                                      - FLDE.DLT_BENODIGDE)
                                    / 3)
                                  * 2
                                  + FLDE.DLT_BENODIGDE)
                         THEN
                            '#5CAB5C'                 -- groen     --'#04B404'
                         ELSE
                            CASE
                               WHEN (onde.geplande_einddatum - SYSDATE - ONDA.DLT_HERIJKT_DAGEN_CUM) >
                                       ( (ONDA.DLT_HERIJKT_DAGEN
                                          - FLDE.DLT_BENODIGDE)
                                        / 3
                                        + FLDE.DLT_BENODIGDE)
                                    AND (  onde.geplande_einddatum
                                         - SYSDATE
                                         - ONDA.DLT_HERIJKT_DAGEN_CUM) <=
                                           ( ( (ONDA.DLT_HERIJKT_DAGEN
                                                - FLDE.DLT_BENODIGDE)
                                              / 3)
                                            * 2
                                            + FLDE.DLT_BENODIGDE)
                               THEN
                                  '#AEB404'                    --  licht groen
                               ELSE
                                  CASE
                                     WHEN (  onde.geplande_einddatum
                                           - SYSDATE
                                           - ONDA.DLT_HERIJKT_DAGEN_CUM) >
                                             FLDE.DLT_BENODIGDE
                                          AND (  onde.geplande_einddatum
                                               - SYSDATE
                                               - ONDA.DLT_HERIJKT_DAGEN_CUM) <=
                                                 ( (ONDA.DLT_HERIJKT_DAGEN
                                                    - FLDE.DLT_BENODIGDE)
                                                  / 3
                                                  + FLDE.DLT_BENODIGDE)
                                     THEN
                                        '#ED8B1C'      -- oranje     '#FF8000'
                                     ELSE
                                        '#CD5B5B'      -- rood       '#CD5B5B'
                                  END
                            END
                      END
                END
          END
             AS VERBORGEN_KLEUR,
          CASE
             WHEN ONDA.PARAM_01_CHECK = 'KLEINER'
             THEN
                6
             ELSE
                CASE
                   WHEN ONDA.PARAM_01_CHECK = 'LEEG'
                   THEN
                      5
                   ELSE
                      CASE
                         WHEN (onde.geplande_einddatum - SYSDATE - ONDA.DLT_HERIJKT_DAGEN_CUM) >
                                 ( ( (ONDA.DLT_HERIJKT_DAGEN
                                      - FLDE.DLT_BENODIGDE)
                                    / 3)
                                  * 2
                                  + FLDE.DLT_BENODIGDE)
                         THEN
                            4                         -- groen     --'#04B404'
                         ELSE
                            CASE
                               WHEN (onde.geplande_einddatum - SYSDATE - ONDA.DLT_HERIJKT_DAGEN_CUM) >
                                       ( (ONDA.DLT_HERIJKT_DAGEN
                                          - FLDE.DLT_BENODIGDE)
                                        / 3
                                        + FLDE.DLT_BENODIGDE)
                                    AND (  onde.geplande_einddatum
                                         - SYSDATE
                                         - ONDA.DLT_HERIJKT_DAGEN_CUM) <=
                                           ( ( (ONDA.DLT_HERIJKT_DAGEN
                                                - FLDE.DLT_BENODIGDE)
                                              / 3)
                                            * 2
                                            + FLDE.DLT_BENODIGDE)
                               THEN
                                  3                            --  licht groen
                               ELSE
                                  CASE
                                     WHEN (  onde.geplande_einddatum
                                           - SYSDATE
                                           - ONDA.DLT_HERIJKT_DAGEN_CUM) >
                                             FLDE.DLT_BENODIGDE
                                          AND (  onde.geplande_einddatum
                                               - SYSDATE
                                               - ONDA.DLT_HERIJKT_DAGEN_CUM) <=
                                                 ( (ONDA.DLT_HERIJKT_DAGEN
                                                    - FLDE.DLT_BENODIGDE)
                                                  / 3
                                                  + FLDE.DLT_BENODIGDE)
                                     THEN
                                        2              -- oranje     '#FF8000'
                                     ELSE
                                        1              -- rood       '#CD5B5B'
                                  END
                            END
                      END
                END
          END
             AS VERBORGEN_SORTERING
     FROM beh_flow_details flde,
          beh_flow_onde_data onda,
          beh_flows flow,
          kgc_onderzoeken onde
    WHERE
    --FLDE.TOON_MODE IS NULL
          FLOW.FLOW_TYPE='FLOW'
          AND FLDE.FLOW_ID = FLOW.FLOW_ID
          AND ONDA.FLDE_ID = FLDE.FLDE_ID
          AND ONDA.ONDE_ID = ONDE.ONDE_ID;

/
QUIT
