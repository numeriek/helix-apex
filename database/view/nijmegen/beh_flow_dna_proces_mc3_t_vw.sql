CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC3_T_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "DRAGON_ID", "STATUS", "DATUM_VERZONDEN", "DATUM_AANVRAAG", "OPMERKINGEN") AS
  SELECT '#0B3861' AS VERBORGEN_KLEUR,
          bdrg."DRAGON_ID" AS VERBORGEN_SORTERING,
          bdrg."DRAGON_ID" AS VERBORGEN_TELLING,
          bdrg."DRAGON_ID",
          bdrg."STATUS",
          bdrg."DATUM_STATUS",
          bdrg."DATUM_AANMAAK_AANVRAAG",
          'aanvraag in Dragon maar niet in Helix'
    FROM BEH_DRAGON_VERZONDEN_AANVRAGE bdrg
    WHERE BDRG.DRAGON_ID not in (SELECT SUBSTR (IMFI.FILENAAM,1,INSTR (IMFI.FILENAAM, ' ') - 1)
                                                    FROM kgc_import_files imfi
                                                    where IMFI.IMLO_ID = 84
                                                    and SUBSTR (IMFI.FILENAAM,1,INSTR (IMFI.FILENAAM, ' ') - 1) is not null)
    order by dragon_id;

/
QUIT
