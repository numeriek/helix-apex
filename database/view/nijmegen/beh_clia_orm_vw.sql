CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_CLIA_ORM_VW" ("MEET_ID", "PEILDATUM", "ZISNR", "ACHTERNAAM", "GEPLANDE_EINDDATUM", "MONSTERNUMMER", "ONGR_CODE", "STGR_CODE") AS
  SELECT meet.meet_id,
          meet.creation_date peildatum,
          UPPER (pers.zisnr) zisnr,
          pers.achternaam,
          onde.geplande_einddatum,
          UPPER (mons.monsternummer) || ' (' || meet.meet_id || ')',
          UPPER (ongr.code) ongr_code,
          UPPER (stgr.code) stgr_code
     FROM kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
          bas_metingen meti,
          bas_meetwaarden meet,
          kgc_stoftestgroepen stgr,
          kgc_stoftesten stof,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr
    WHERE     pers.pers_id = onde.pers_id
          AND onde.onde_id = onmo.onde_id
          AND onmo.mons_id = mons.mons_id
          AND mons.mons_id = frac.mons_id
          AND frac.frac_id = meti.frac_id
          AND meti.meti_id = meet.meti_id
          AND meti.onde_id = onde.onde_id(+)
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND meti.stgr_id = stgr.stgr_id
          AND meet.stof_id = stof.stof_id
          AND UPPER (onde.afgerond) = UPPER ('N')
          AND UPPER (meti.afgerond) = UPPER ('N')
          AND UPPER (meet.afgerond) = UPPER ('N')
          AND UPPER (kafd.code) IN (UPPER ('GENOOM'), UPPER ('CYTO'))
          AND UPPER (stgr.code) LIKE UPPER ('%KAR%')
          AND UPPER (stof.code) LIKE UPPER ('%KAR%')
          AND NOT EXISTS
                     (SELECT NULL
                        FROM beh_clia_hl7_berichten
                       WHERE     meet_id = meet.meet_id
                             AND verzonden = 'J'
                             AND bericht_type = 'ORM')
          AND NOT EXISTS
                     (SELECT NULL
                        FROM beh_clia_hl7_berichten2
                       WHERE     meet_id = meet.meet_id
                             AND verzonden = 'J'
                             AND bericht_type = 'ORM');

/
QUIT
