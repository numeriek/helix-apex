CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_LOGI_VW" ("LOGI_ID", "K_SESSION_USER", "K_OS_USER", "K_SESSIONID", "K_HOST", "K_IP_ADDRESS", "K_MODULE", "TOEGESTAAN_TEGENGEHOUDEN", "CREATION_DATE", "CREATED_BY", "LAST_UPDATE_DATE", "LAST_UPDATED_BY") AS
  SELECT logi_id,
			 k_session_user,
			 k_os_user,
			 k_sessionid,
			 k_host,
			 k_ip_address,
			 k_module,
			 toegestaan_tegengehouden,
			 creation_date,
			 created_by,
			 last_update_date,
			 last_updated_by
	  FROM beh_log_in_gegevens;

/
QUIT
