CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC1_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "GROEP", "MONSTERNR", "MATERIAAL", "SPOED", "FRACTIENR", "AANMELD_DATUM", "STATUS", "ZISNR", "AANSPREKEN", "GESLACHT") AS
  SELECT CASE                                      -- Fracties om te isoleren
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                '#5CAB5C'                               -- groen     '#04B404'
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      '#AEB404'                                --  licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            '#ED8B1C'                  -- oranje     '#FF8000'
                         ELSE
                            '#CD5B5B'                  -- rood       '#FF0000'
                      END
                END
          END
             AS VERBORGEN_KLEUR,
          CASE
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                4                                                     -- groen
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      3                                         -- licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            2                                        -- oranje
                         ELSE
                            1                                          -- rood
                      END
                END
          END
             AS VERBORGEN_SORTERING,
          ISOV.FRACTIENUMMER AS VERBORGEN_TELLING,
          ISOV.PERS_ID AS VERBORGEN_PERS_ID,
          ISOV.ONGR_CODE,
          ISOV.MONSTERNUMMER,
          ISOV.MATE_CODE AS MATERIAAL,
          ISOV.SPOED,
          ISOV.FRACTIENUMMER,
          FRAC.CREATION_DATE,
          DECODE (FRAC.STATUS,
                  'A', 'Aangemeld',
                  'O', 'Opgewerkt',
                  'V', 'Vervallen',
                  'M', 'Mislukt',
                  'X', 'Verbruikt',
                  '')
             AS STATUS,
          ISOV.PERS_ZISNR,
          ISOV.PERS_AANSPREKEN,
          ISOV.PERS_GESLACHT
     --NOTI.OMSCHRIJVING AS NOTITIE,
     --NOTI.NOTI_ID AS VERBORGEN_NOTI_ID
     FROM bas_isolatielijst_vw isov,
          bas_fracties frac,
          --kgc_notities noti,
          BEH_FLOW_PRIORITEIT_VW flpr
    WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC1_VW'
          AND flpr.kafd_id = ISOV.KAFD_ID
          AND ISOV.FRAC_ID = FRAC.FRAC_ID
          AND (isov.fractienummer NOT LIKE 'PAT%'
               AND isov.fractienummer NOT LIKE 'PAN%')
          AND (isov.frac_id_parent IS NOT NULL
               OR bas_isol_00.aantal_buizen (isov.mons_id) > 0)
          AND (NOT EXISTS
                  (SELECT NULL
                     FROM bas_isolatielijsten isol1
                    WHERE isol1.frac_id = isov.frac_id))
          AND (isov.frac_id_parent IS NULL
               OR NVL (isov.type_afleiding, 'X') != 'D')
   --AND FRAC.MONS_ID = NOTI.ID(+)
   --AND NOTI.ENTITEIT (+) = 'MONS'
   UNION ALL
   SELECT CASE                                      -- Fracties om te isoleren
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                '#5CAB5C'                               -- groen     '#04B404'
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      '#AEB404'                                --  licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            '#ED8B1C'                  -- oranje     '#FF8000'
                         ELSE
                            '#CD5B5B'                  -- rood       '#FF0000'
                      END
                END
          END
             AS VERBORGEN_KLEUR,
          CASE
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                4                                                     -- groen
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      3                                         -- licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            2                                        -- oranje
                         ELSE
                            1                                          -- rood
                      END
                END
          END
             AS VERBORGEN_SORTERING,
          FRAC.FRACTIENUMMER AS VERBORGEN_TELLING,
          MONS.PERS_ID AS VERBORGEN_PERS_ID,
          ONGR.CODE,
          MONS.MONSTERNUMMER,
          MATE.CODE AS MATERIAAL,
          'N' AS SPOED,
          FRAC.FRACTIENUMMER,
          FRAC.CREATION_DATE,
          DECODE (FRAC.STATUS,
                  'A', 'Aangemeld',
                  'O', 'Opgewerkt',
                  'V', 'Vervallen',
                  'M', 'Mislukt',
                  'X', 'Verbruikt',
                  '')
             AS STATUS,
          PERS.ZISNR,
          PERS.AANSPREKEN,
          PERS.GESLACHT
     -- NOTI.OMSCHRIJVING AS NOTITIE,
     --NOTI.NOTI_ID AS VERBORGEN_NOTI_ID
     FROM bas_fracties frac,
          kgc_monsters mons,
          kgc_personen pers,
          kgc_materialen mate,
          kgc_onderzoeksgroepen ongr,
          --kgc_notities noti,
          BEH_FLOW_PRIORITEIT_VW flpr
    WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC1_VW'
          AND mons.KAFD_ID = flpr.KAFD_ID
          AND MONS.ONGR_ID = ONGR.ONGR_ID
          AND MONS.MONS_ID = FRAC.MONS_ID
          AND MONS.PERS_ID = PERS.PERS_ID
          AND MONS.MATE_ID = MATE.MATE_ID
          AND (NOT EXISTS
                  (SELECT NULL
                     FROM bas_isolatielijsten isol1
                    WHERE isol1.frac_id = frac.frac_id))
          AND FRAC.FRACTIENUMMER LIKE '%U%'
          AND FRAC.STATUS = 'O'
   --AND FRAC.MONS_ID = NOTI.ID(+)
   --AND NOTI.ENTITEIT (+) = 'MONS'
   ORDER BY VERBORGEN_SORTERING, SPOED;

/
QUIT
