CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_MEERLING_ZWAN_VW" ("PERS_ID", "ZWAN_ID", "DATUM_ATERM", "HOEVEELLING") AS
  SELECT zwan.pers_id,
    zwan.zwan_id,
    zwan.datum_aterm,
    zwan.hoeveelling
FROM kgc_foetussen foet
, kgc_zwangerschappen zwan
WHERE foet.zwan_id = ZWAN.ZWAN_ID
GROUP BY zwan.pers_id,
    zwan.zwan_id,
    zwan.datum_aterm,
    zwan.hoeveelling
having Count(*) > 1
 ;

/
QUIT
