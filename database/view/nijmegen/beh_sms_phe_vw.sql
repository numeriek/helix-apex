CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_SMS_PHE_VW" ("PERS_ID", "SMS_TEKST", "KAFD_ID", "ONGR_ID", "KAFD_CODE", "ONGR_CODE", "TAAL_CODE", "AFGEROND", "ZISNR", "BSN", "TELEFOON", "TELEFOON2", "MONSTERNUMMER", "MATE_CODE", "GROEP2_CODE", "DATUM_AFNAME", "ONDERZOEKNR", "ONDERZOEKSTYPE", "ONDE_INDICATIES", "INDI_1000_J_N", "METI_ID", "MEETWAARDE", "TONEN_ALS", "EENHEID", "VOORLETTERS", "GEBOORTEJAAR") AS
  SELECT DISTINCT
			 onde.pers_id,
			 DECODE (
				 beh_phe_00.meetwaarde_doorgeven (meet.tonen_als),
				 'N', (beh_phe_00.
						  bepaal_sms_vervangende_tekst (
							 'BEH_SMS_PHE_VERVANGENDE_TEKST',
							 beh_phe_00.bepaal_voorletters (pers.voorletters),
							 TO_CHAR (pers.geboortedatum, 'yyyy'),
							 mons.datum_afname)),
				 (beh_phe_00.
					bepaal_sms_tekst (
					  'BEH_SMS_PHE_TEKST',
					  beh_phe_00.bepaal_voorletters (pers.voorletters),
					  TO_CHAR (pers.geboortedatum, 'yyyy'),
					  mons.datum_afname,
					  meet.tonen_als,
					  REPLACE (meet.eenheid, '�', 'micro'))))
				 sms_tekst,
			 onde.kafd_id,
			 onde.ongr_id,
			 kafd.code kafd_code,
			 ongr.code ongr_code,
			 NVL (taal.code, 'NED') taal_code,
			 onde.afgerond,
			 pers.zisnr,
			 pers.bsn,
			 pers.telefoon,
			 pers.telefoon2,
			 mons.monsternummer,
			 mate.code mate_code,
			 ongr2.code groep2_code,
			 mons.datum_afname,
			 --  nvl(mons.datum_afname,'onbekend') datum_afname,
			 onde.onderzoeknr,
			 onde.onderzoekstype,
			 beh_onderzoek_indicaties (onde.onde_id) onde_indicaties,
			 INSTR (beh_onderzoek_indicaties (onde.onde_id), '1000')
				 indi_1000_j_n,
			 meti.meti_id,
			 meet.meetwaarde,
			 meet.tonen_als,
			 meet.eenheid,
			 pers.voorletters,
			 TO_CHAR (pers.geboortedatum, 'yyyy') geboortejaar
	  FROM kgc_kgc_afdelingen kafd,
			 kgc_onderzoeksgroepen ongr,
			 kgc_onderzoeksgroepen ongr2,
			 kgc_onderzoeken onde,
			 kgc_personen pers,
			 kgc_talen taal,
			 kgc_monsters mons,
			 kgc_onderzoek_monsters onmo,
			 bas_metingen meti,
			 kgc_stoftestgroepen stgr,
			 bas_meetwaarden meet,
			 kgc_stoftesten stof,
			 kgc_materialen mate
	 WHERE	  onde.datum_binnen >= ADD_MONTHS (SYSDATE, -1) -- het onderzoek is niet ouder dan een maand vanaf nu
			 AND onde.kafd_id = kafd.kafd_id
			 AND onde.ongr_id = ongr.ongr_id
			 AND onde.taal_id = taal.taal_id(+)
			 AND mons.pers_id = pers.pers_id
			 AND mons.mons_id = onmo.mons_id
			 AND onde.onde_id = onmo.onde_id
			 AND mons.mate_id = mate.mate_id
			 AND mons.ongr_id = ongr2.ongr_id
			 AND onmo.onmo_id = meti.onmo_id
			 AND meti.stgr_id = stgr.stgr_id
			 AND meet.meti_id = meti.meti_id
			 AND meet.stof_id = stof.stof_id
			 AND mate.code IN ('SPOT', 'P/S')
			 AND stgr.code IN ('SAZPHE_P/S')
			 AND stof.code IN ('PHE')
			 AND kafd.code IN ('METAB')
			 AND ongr.code IN ('BASIS')
			 AND ongr2.code IN ('BASIS')
			 AND beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) = 'J'
			 AND meti.afgerond = 'J'
			 AND onde.onderzoekstype <> 'R'
			 AND pers.telefoon IS NOT NULL
			 AND (SUBSTR (pers.telefoon, 1, 2) = '06'
					OR SUBSTR (pers.telefoon, 1, 5) = '+49 1')
			 AND INSTR (beh_onderzoek_indicaties (onde.onde_id), '1000') > 0
			 AND EXISTS ---- alleen als mensen hebben aangegeven dat ze toestemming geven
					  (SELECT *
						  FROM kgc_attributen attr, kgc_attribuut_waarden atwa
						 WHERE	  attr.attr_id = atwa.attr_id
								 AND atwa.id = onde.pers_id
								 AND attr.tabel_naam = 'KGC_PERSONEN'
								 AND attr.code = 'SMS_PHE_TOESTEMMING'
								 AND UPPER (atwa.waarde) = 'J'
								 AND onde.creation_date > atwa.creation_date);

/
QUIT
