CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_SIGNAAL_ONDERZOEKEN_VW" ("ONDE_ID", "KAFD_CODE", "PERS_ACHTERNAAM", "PERS_AANSPREKEN", "ONDERZOEKNR", "SPOED", "STATUS", "DATUM_BINNEN", "ONDERZOEKSWIJZE", "INDI_CODE", "INDICATIE", "GEPLANDE_EINDDATUM", "DATUM_SIGNAAL") AS
  select onde.onde_id
     , kafd.code kafd_code
     , pers.achternaam pers_achternaam
     , pers.aanspreken pers_aanspreken
     , onde.onderzoeknr
     , onde.spoed
     , onde.status
     , onde.datum_binnen
     , onde.onderzoekswijze
     , kgc_indi_00.indi_code(onde.onde_id) indi_code
     , kgc_indi_00.onderzoeksreden(onde.onde_id) indicatie
     , onde.geplande_einddatum
     , min(beh_loop_00.signaleringsdatum(onde.onde_id, onin.indi_id)) as datum_signaal
from   kgc_onderzoeken               onde
  join kgc_kgc_afdelingen            kafd on kafd.kafd_id = onde.kafd_id
  join kgc_personen                  pers on pers.pers_id = onde.pers_id
  left join kgc_onderzoek_indicaties onin on onin.onde_id = onde.onde_id
where  onde.afgerond = 'N'
group by onde.onde_id, kafd.code, pers.achternaam, pers.aanspreken, onde.onderzoeknr, onde.spoed, onde.status, onde.datum_binnen, onde.onderzoekswijze, onde.geplande_einddatum;

/
QUIT
