CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_DD_MC33_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "GROEP", "MONSTERNR", "AANMELD_DATUM", "OPSLAG") AS
  SELECT CASE                                -- Monsters zonder onderzoeken
               WHEN (SYSDATE - MONS.CREATION_DATE) < FLPR.ORANJE
               THEN
                  '#5CAB5C'                             -- groen     '#04B404'
               ELSE
                  CASE
                     WHEN ( (SYSDATE - MONS.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - MONS.CREATION_DATE) < FLPR.LICHTGROEN)
                     THEN
                        '#AEB404'                              --  licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - MONS.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - MONS.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              '#ED8B1C'                -- oranje     '#FF8000'
                           ELSE
                              '#CD5B5B'                -- rood       '#FF0000'
                        END
                  END
            END
               AS verborgen_kleur,
            CASE
               WHEN (SYSDATE - MONS.CREATION_DATE) < FLPR.ORANJE
               THEN
                  4                                                   -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - MONS.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - MONS.CREATION_DATE) < FLPR.LICHTGROEN)
                     THEN
                        3                                       -- licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - MONS.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - MONS.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              2                                      -- oranje
                           ELSE
                              1                                        -- rood
                        END
                  END
            END
               AS verborgen_sortering,
            MONS.MONSTERNUMMER AS verborgen_telling,
            MONS.PERS_ID,
            ONGR.CODE,
            MONS.MONSTERNUMMER,
            MONS.CREATION_DATE,
            MONS.ALLEEN_VOOR_OPSLAG
       FROM kgc_monsters mons,
            kgc_onderzoeksgroepen ongr,
            BEH_FLOW_PRIORITEIT_VW flpr
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_METAB_DD_MC33_VW'
            AND MONS.KAFD_ID = flpr.kafd_id
            AND ONGR.ONGR_ID = MONS.ONGR_ID
            AND MONS.MONS_ID NOT IN (SELECT onmo.mons_id
                                       FROM kgc_onderzoek_monsters onmo)
            AND ONGR.ONGR_ID IN (SELECT FLKO.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR flko
                                  WHERE FLKO.FLOW_ID = FLPR.FLOW_ID)
            AND MONS.ALLEEN_VOOR_OPSLAG = 'N'
            AND MONS.CREATION_DATE > '01-04-2011' --tijdedelijke selectiecriterium tot ALLEEN_VOOR_OPSLAG wordt op J gezet voor oude monsters
   ORDER BY verborgen_sortering, MONS.CREATION_DATE;

/
QUIT
