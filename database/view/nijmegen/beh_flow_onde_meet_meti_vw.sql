CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_ONDE_MEET_METI_VW" ("ONDE_ID", "TOTAAL_METI", "METI_AF", "METI_NIET_AF", "TOTAAL_MEET", "MEET_AF", "MEET_NIET_AF", "NCONT", "WCONT", "NCONT_V", "NCONT_S", "NCONT_M", "WCONT_V", "WCONT_S", "WCONT_M") AS
  WITH with_kgc_onderzoeken AS (SELECT onde_id
                                   FROM kgc_onderzoeken
                                  WHERE afgerond = 'N'),
        with_bas_metingen
           AS (SELECT onde.onde_id, meti.meti_id, meti.afgerond
                 FROM with_kgc_onderzoeken onde, bas_metingen meti
                WHERE onde.onde_id = meti.onde_id),
        with_bas_meetwaarden
           AS (SELECT meti.onde_id,
                      meet.meet_id,
                      meti.afgerond meti_afgerond,
                      meet.afgerond meet_afgerond,
                      meet.meet_reken,
                      meet.mest_id
                 FROM with_bas_metingen meti, bas_meetwaarden meet
                WHERE meti.meti_id = meet.meti_id),
        with_bas_meetwaarden_1
           AS (SELECT onde_id,
                      meet_id,
                      CASE
                         WHEN METI_AFGEROND = 'J' THEN 'J'
                         ELSE MEET_AFGEROND
                      END
                         AFGEROND
                 FROM with_bas_meetwaarden),
        with_bas_meetwaarden_2
           AS (SELECT onde_id,
                      meet_id,
                      CASE
                         WHEN METI_AFGEROND = 'J'
                         THEN
                            'WCONT'
                         ELSE
                            CASE
                               WHEN MEET_REKEN = 'V' AND MEST_ID IS NOT NULL
                               THEN
                                  'WCONT_V'
                               ELSE
                                  CASE
                                     WHEN MEET_REKEN = 'S'
                                          AND MEST_ID IS NOT NULL
                                     THEN
                                        'WCONT_S'
                                     ELSE
                                        CASE
                                           WHEN MEET_REKEN = 'M'
                                                AND MEST_ID IS NOT NULL
                                           THEN
                                              'WCONT_M'
                                           ELSE
                                              CASE
                                                 WHEN MEET_REKEN = 'V'
                                                      AND MEST_ID IS NULL
                                                 THEN
                                                    'NCONT_V'
                                                 ELSE
                                                    CASE
                                                       WHEN MEET_REKEN = 'S'
                                                            AND MEST_ID
                                                                   IS NULL
                                                       THEN
                                                          'NCONT_S'
                                                       ELSE
                                                          CASE
                                                             WHEN MEET_REKEN =
                                                                     'M'
                                                                  AND MEST_ID
                                                                         IS NULL
                                                             THEN
                                                                'NCONT_M'
                                                          END
                                                    END
                                              END
                                        END
                                  END
                            END
                      END
                         AS CONT
                 FROM with_bas_meetwaarden)
   SELECT SQLMETI.ONDE_ID,
          SQLMETI.METI_AF + SQLMETI.METI_NIET_AF AS TOTAAL_METI,
          SQLMETI.METI_AF,
          SQLMETI.METI_NIET_AF,
          SQLMEET.MEET_AF + SQLMEET.MEET_NIET_AF AS TOTAAL_MEET,
          SQLMEET.MEET_AF,
          SQLMEET.MEET_NIET_AF,
          SQLCONT.NCONT_V + SQLCONT.NCONT_S + SQLCONT.NCONT_M AS NCONT,
          SQLCONT.WCONT + SQLCONT.WCONT_V + SQLCONT.WCONT_S + SQLCONT.WCONT_M
             AS WCONT,
          SQLCONT.NCONT_V,
          SQLCONT.NCONT_S,
          SQLCONT.NCONT_M,
          SQLCONT.WCONT_V,
          SQLCONT.WCONT_S,
          SQLCONT.WCONT_M
     FROM (SELECT *
             FROM with_bas_metingen PIVOT (COUNT (meti_id)
                                    FOR AFGEROND
                                    IN ('J' AS METI_AF, 'N' AS METI_NIET_AF))) SQLMETI,
          (SELECT *
             FROM with_bas_meetwaarden_1 PIVOT (COUNT (meet_id)
                                         FOR AFGEROND
                                         IN  ('J' AS MEET_AF,
                                             'N' AS MEET_NIET_AF))) SQLMEET,
          (SELECT *
             FROM with_bas_meetwaarden_2 PIVOT (COUNT (meet_id)
                                         FOR CONT
                                         IN  ('NCONT_V' AS NCONT_V,
                                             'NCONT_S' AS NCONT_S,
                                             'NCONT_M' AS NCONT_M,
                                             'WCONT' AS WCONT,
                                             'WCONT_V' AS WCONT_V,
                                             'WCONT_S' AS WCONT_S,
                                             'WCONT_M' AS WCONT_M))) SQLCONT
    WHERE SQLMETI.onde_id = SQLMEET.onde_id
          AND SQLMETI.onde_id = SQLCONT.onde_id
/* onderstaande code is vervangen door de code die Hamid heeft geschreven om de query sneller te maken
SELECT SQLMETI.ONDE_ID,
          SQLMETI.METI_AF + SQLMETI.METI_NIET_AF AS TOTAAL_METI,
          SQLMETI.METI_AF,
          SQLMETI.METI_NIET_AF,
          SQLMEET.MEET_AF + SQLMEET.MEET_NIET_AF AS TOTAAL_MEET,
          SQLMEET.MEET_AF,
          SQLMEET.MEET_NIET_AF,
          SQLCONT.NCONT_V + SQLCONT.NCONT_S + SQLCONT.NCONT_M AS NCONT,
          SQLCONT.WCONT + SQLCONT.WCONT_V + SQLCONT.WCONT_S + SQLCONT.WCONT_M AS WCONT,
          SQLCONT.NCONT_V,
          SQLCONT.NCONT_S,
          SQLCONT.NCONT_M,
          SQLCONT.WCONT_V,
          SQLCONT.WCONT_S,
          SQLCONT.WCONT_M
     FROM (SELECT *
             FROM (SELECT ONDE.ONDE_ID, METI.AFGEROND, METI.METI_ID
                     FROM kgc_onderzoeken onde, bas_metingen meti          --,
                    WHERE ONDE.ONDE_ID = METI.ONDE_ID AND ONDE.AFGEROND = 'N' --                            and ONDE.ONDE_ID IN (1208045, 1206395, 1207616)
                                                                             ) PIVOT (COUNT (
                                                                                         meti_id)
                                                                               FOR AFGEROND
                                                                               IN  ('J' AS METI_AF,
                                                                                   'N' AS METI_NIET_AF))) SQLMETI,
          (SELECT *
             FROM (SELECT ONDE.ONDE_ID,
                          CASE
                             WHEN METI.AFGEROND = 'J' THEN 'J'
                             ELSE MEET.AFGEROND
                          END
                             AFGEROND,
                          MEET.MEET_ID
                     FROM kgc_onderzoeken onde,
                          bas_metingen meti,
                          bas_meetwaarden meet
                    WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                          AND METI.METI_ID = MEET.METI_ID
                          AND ONDE.AFGEROND = 'N' --                            and ONDE.ONDE_ID IN (1208045, 1206395, 1207616)
                                                 ) PIVOT (COUNT (meet_id)
                                                   FOR AFGEROND
                                                   IN  ('J' AS MEET_AF,
                                                       'N' AS MEET_NIET_AF))) SQLMEET,
          (SELECT *
             FROM (SELECT ONDE.ONDE_ID,
                          CASE
                             WHEN METI.AFGEROND = 'J'
                             THEN
                                'WCONT'
                          ELSE
                                CASE
                                    WHEN MEET.MEET_REKEN = 'V' AND MEET.MEST_ID IS NOT NULL THEN 'WCONT_V'
                                    ELSE CASE
                                        WHEN MEET.MEET_REKEN = 'S' AND MEET.MEST_ID IS NOT NULL THEN 'WCONT_S'
                                        ELSE CASE
                                            WHEN MEET.MEET_REKEN = 'M' AND MEET.MEST_ID IS NOT NULL THEN 'WCONT_M'

                             ELSE
                                CASE
                                    WHEN MEET.MEET_REKEN = 'V' AND MEET.MEST_ID IS NULL THEN 'NCONT_V'
                                    ELSE CASE
                                        WHEN MEET.MEET_REKEN = 'S' AND MEET.MEST_ID IS NULL THEN 'NCONT_S'
                                        ELSE CASE
                                            WHEN MEET.MEET_REKEN = 'M' AND MEET.MEST_ID IS NULL THEN 'NCONT_M'
                                        END
                                    END
                                END
                                        END
                                    END
                                END
                          END
                             AS CONT,
                          MEET.MEET_ID
                     FROM kgc_onderzoeken onde,
                          bas_metingen meti,
                          bas_meetwaarden meet
                    WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                          AND METI.METI_ID = MEET.METI_ID
                          AND ONDE.AFGEROND = 'N' --                            and ONDE.ONDE_ID IN (1208045, 1206395, 1207616)
                                                 ) PIVOT (COUNT (meet_id)
                                                   FOR CONT
                                                   IN  ('NCONT_V' AS NCONT_V,
                                                        'NCONT_S' AS NCONT_S,
                                                        'NCONT_M' AS NCONT_M,
                                                        'WCONT' AS WCONT,
                                                        'WCONT_V' AS WCONT_V,
                                                        'WCONT_S' AS WCONT_S,
                                                        'WCONT_M' AS WCONT_M))) SQLCONT
    WHERE SQLMETI.onde_id = SQLMEET.onde_id
          AND SQLMETI.onde_id = SQLCONT.onde_id

*/;

/
QUIT
