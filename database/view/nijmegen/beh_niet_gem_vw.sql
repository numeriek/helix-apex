CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_NIET_GEM_VW" ("ZWAN_ID", "FOET_ID", "AANTAL") AS
  select zwan.zwan_id
, foet.foet_id
, count(*) aantal
from kgc_zwangerschappen zwan
, kgc_foetussen foet
, kgc_onderzoeken onde
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
where zwan.zwan_id = foet.zwan_id
and foet.foet_id = onde.foet_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
and kafd.code = 'CYTO'
and ongr.code in ('PRE_NIJM','PRE_ENS')
and onde.datum_binnen between to_date('01-01-2008','dd-mm-yyyy') and to_date('30-09-2008','dd-mm-yyyy')
group by zwan.zwan_id, foet.foet_id
having count(*) > 1

 ;

/
QUIT
