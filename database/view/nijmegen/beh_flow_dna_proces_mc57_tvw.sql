CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC57_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "FRAC_STATUS", "ISOLATIELIJST_NUMMER", "CREATION_DATE", "LAST_UPDATE_DATE", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT -- Overzicht NGS - monster moet gestuurd worden naar de faciliteit (bv BGI)
            '#79A8A8' AS VERBORGEN_KLEUR,
            '' AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            DECODE (FRAC.STATUS,
                    'A', 'Aangemeld',
                    'O', 'Opgewerkt',
                    'M', 'Mislukt',
                    'V', 'Vervallen',
                    'X', 'Verbruikt')
               AS FRAC_STATUS,
            ISOL.ISOLATIELIJST_NUMMER,
            TO_CHAR (ISOL.CREATION_DATE, 'rrrr-mm-dd hh24:mm:ss'),
            TO_CHAR (ISOL.LAST_UPDATE_DATE, 'rrrr-mm-dd hh24:mm:ss'),
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            bas_metingen meti,
            kgc_stoftestgroepen stgr,
            bas_fracties frac,
            beh_flow_help_fami_vw fami,
            BAS_ISOLATIELIJSTEN isol,
            beh_flow_favorieten flfa,
            (SELECT DISTINCT EXAL.ONDE_ID
               FROM BEH_FLOW_NGS_EXA_LIB_MDET_VW exal
              WHERE exal.SEQUENCING IS NULL) pvt
      WHERE     ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FAMI.PERS_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.FRAC_ID = FRAC.FRAC_ID
            AND meti.stgr_id = stgr.stgr_id
            AND FRAC.FRAC_ID = ISOL.FRAC_ID(+)
            AND ONDE.ONDE_ID = PVT.ONDE_ID
            AND ONDE.STATUS = 'G'
            -- alleen fracties die nog niet op werklijst staan (mantisnr 11188)
            AND NOT EXISTS
                   (SELECT NULL
                      FROM kgc_werklijst_items
                     WHERE meti_id = meti.meti_id)
            AND ONDE.AFGEROND = 'N'
            AND STGR.CODE = 'NGS_ST_1'                       -- mantisnr 11373
   ORDER BY ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
