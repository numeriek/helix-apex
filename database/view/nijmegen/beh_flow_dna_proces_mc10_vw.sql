CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC10_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "SPOED", "ISOLATIELIJSTNR", "VOLGNR", "GROEP", "FRACTIENR", "MONSTERNR", "MATERIAAL", "BUISNR", "PROCES_CODE", "STAP_GEDAAN", "COMMENTAAR") AS
  SELECT CASE    -- Overzicht "Fracties om te isoleren - stap 1 uitgevoerd"
               WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
               THEN
                  '#5CAB5C'                             -- groen     '#04B404'
               ELSE
                  CASE
                     WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                     THEN
                        '#AEB404'                              --  licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - FRAC.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              '#ED8B1C'                -- oranje     '#FF8000'
                           ELSE
                              '#CD5B5B'                -- rood       '#FF0000'
                        END
                  END
            END
               AS verborgen_kleur,
            CASE
               WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
               THEN
                  4                                                   -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                           AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                     THEN
                        3                                       -- licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                                 AND (SYSDATE - FRAC.CREATION_DATE) >=
                                        FLPR.LICHTGROEN)
                           THEN
                              2                                      -- oranje
                           ELSE
                              1                                        -- rood
                        END
                  END
            END
               AS verborgen_sortering,
            FRAC.FRACTIENUMMER AS VERBORGEN_TELLING,
            MONS.PERS_ID AS verborgen_pers_id,
            DECODE (bas_isol_00.spoed (ISOL.MONS_ID), 'J', 'JA', NULL) SPOED,
            ISOL.ISOLATIELIJST_NUMMER,
            ISOL.VOLGNUMMER,
            ONGR.CODE,
            FRAC.FRACTIENUMMER,
            NVL (FRAC2.FRACTIENUMMER, MONS.MONSTERNUMMER) MONS_MONSTERNUMMER,
            MATE.CODE,
            ISOL.BUISNR,
            PROC.CODE,
            ISOL.STAP,
            MONS.COMMENTAAR
       FROM KGC_MATERIALEN MATE,
            BAS_FRACTIES FRAC2,
            BAS_FRACTIES FRAC,
            KGC_MONSTERS MONS,
            BAS_ISOLATIELIJSTEN ISOL,
            KGC_KGC_AFDELINGEN KAFD,
            KGC_PROCESSEN PROC,
            KGC_ONDERZOEKSGROEPEN ONGR,
            BEH_FLOW_PRIORITEIT_VW flpr
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC10_VW'
            AND flpr.kafd_id = ISOL.KAFD_ID
            AND ISOL.MONS_ID = MONS.MONS_ID
            AND ISOL.FRAC_ID = FRAC.FRAC_ID
            AND FRAC.FRAC_ID_PARENT = FRAC2.FRAC_ID(+)
            AND MONS.MATE_ID = MATE.MATE_ID
            AND KAFD.KAFD_ID = ISOL.KAFD_ID
            AND ISOL.PROC_ID = PROC.PROC_ID(+)
            AND ISOL.ONGR_ID = ONGR.ONGR_ID
            AND FRAC.STATUS = 'A'
            AND ISOL.STAP <> 0
   ORDER BY verborgen_sortering,
            spoed,
            ISOL.ISOLATIELIJST_NUMMER,
            ISOL.VOLGNUMMER;

/
QUIT
