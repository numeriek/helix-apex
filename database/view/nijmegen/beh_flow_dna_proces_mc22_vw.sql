CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC22_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "WERKLIJSTNR", "ELUTION_BC", "POSITIE", "FRACTIENR", "STOFTESTCODE", "MACHINE", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht PCRPlaten (werklijsten) met stoftesten status = POST_NORMALISATION_FINISHED -- AUTOMATISCH PROCES
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            POST.PCRPLATEBARCODE AS WERKLIJSTNR,
            POST.ELUTION_BARCODE AS ELUTION_BC,
            POST.DNAPOSITION AS POSITIE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            POST.PRIMER_ID AS STOFTESTCODE,
            MACH.CODE AS MACHINE,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            bas_fracties frac,
            bas_metingen meti,
            bas_meetwaarden meet,
            beh_flow_dna_postrobot POST,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoeksgroepen ongr,
            beh_flow_dna_machine_storage mast,
            beh_flow_dna_machines mach,
            kgc_notities noti,
            beh_flow_prioriteit_new_vw flpr,
            beh_flow_favorieten flfa
      WHERE     ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.FRAC_ID = FRAC.FRAC_ID
            AND METI.METI_ID = MEET.METI_ID
            AND MEET.MEET_ID = POST.MEETID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND POST.POST_ID = MAST.POST_ID
            AND MACH.MACH_ID = MAST.MACH_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.AFGEROND = 'N'
            AND MEET.AFGEROND = 'N'
            AND (UPPER (MAST.STATE) = 'POST_NORMALISATION_FINISHED')
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC22_VW'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
