CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_UITSLAG_ALERT_VW2" ("UITS_ID", "CREATION_DATE", "LAST_UPDATE_DATE", "ONDE_ID", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "PERS_ID") AS
  SELECT uits.uits_id,
             uits.creation_date,
             uits.last_update_date,
             uits.onde_id,
             kafd.code kafd_code,
             ongr.code ongr_code,
             onde.onderzoeknr,
             onde.pers_id
      FROM kgc_uitslagen uits,
             kgc_onderzoeken onde,
             kgc_kgc_afdelingen kafd,
             kgc_onderzoeksgroepen ongr
     WHERE      uits.onde_id = onde.onde_id
             AND onde.kafd_id = kafd.kafd_id
             AND onde.ongr_id = ongr.ongr_id
             AND kafd.code IN
                      ('CYTO', 'DNA-A', 'DNA-C', 'DNA-M', 'GENOOM', 'METAB')
             AND ongr.code IN
                      ('POST_ENS',
                        'POST_NIJM',
                        'PRE_ARN',
                        'PRE_NIJM',
                        'PRE_ENS',
                        'TUM_ENS',
                        'TUM_NIJM',
                        'P',
                        'PR',
                        'LTG',
                        'ARR',
                        'RFD',
                        'NGS',
                        'BASIS',
                        'HOM',
                        'MITO',
                        'LYSO',
                        'DD',
                        'GLYCOS',
                        'OVERIG')
             AND onde.datum_autorisatie > (SYSDATE - 3)
             AND beh_epd_00.onderzoek_epd_afgerond (uits.onde_id) = 'J';

/
QUIT
