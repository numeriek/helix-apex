CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ANALYSEFORMULIEREN_VW" ("KAFD_CODE", "ONGR_CODE", "PERS_ID", "ZISNR", "PERSOON_INFO", "ZWAN_DUUR", "MATE_ID", "MATE_CODE", "MATE_OMSCHRIJVING", "MONSTERNUMMER", "HERK_CODE", "HERK_OMSCHRIJVING", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "RELA_AANSPREKEN", "MONS_HOEVEELHEID", "COND_CODE_MONS", "COND_OMSCHRIJVING_MONS", "ONDE_ID", "ONDERZOEKNR", "ONDE_GEPLANDE_EINDDATUM", "ONDE_AFGEROND", "INDICATIES", "ONDE_REDEN", "GERELATEERDE_ONDERZOEKEN", "FRAC_ID", "FRACTIENUMMER", "FRTY_CODE", "FRTY_OMSCHRIJVING", "COND_CODE_FRAC", "COND_OMSCHRIJVING_FRAC", "METI_ID", "DATUM_AANMELDING", "SPOED", "STGR_CODE", "STGR_OMSCHRIJVING", "WERKLIJST", "RESULTAAT", "COMMENTAAR", "AFGEROND", "DATUM_AFGEROND", "MEDE_NAAM", "ONWY_OMSCHRIJVING", "FAMILIES") AS
  select kafd.code kafd_code
     , ongr.code ongr_code
     , mons.pers_id
     , PERS.ZISNR -- toevoeging MGOL tvb IPN CA
     , kgc_pers_00.info (mons.pers_id) persoon
     , kgc_zwan_00.duur (onde.onde_id) zwan_duur
     , mons.mate_id
     , mate.code mate_code
     , mate.omschrijving mate_omschrijving
     , mons.monsternummer
     , herk.code herk_code
     , herk.omschrijving herk_omschrijving
     , mons.datum_aanmelding mons_datum_aanmelding
     , mons.datum_afname mons_datum_afname
     , rela.aanspreken rela_aanspreken
     , mons.hoeveelheid_monster || ' ' || mate.eenheid mons_hoeveelheid
     , cond1.code cond_code_mons
     , cond1.omschrijving cond_omschrijving_mons
     , onmo.onde_id
     , onde.onderzoeknr
     , onde.geplande_einddatum onde_geplande_einddatum
     , onde.afgerond as onde_afgerond
     , kgc_indi_00.onderzoeksindicaties (onde.onde_id) indicaties
     , onde.omschrijving onde_reden
     , kgc_onde_00.gerelateerde_onderzoeken (onde.onde_id) gerelateerde_onderzoeken
     , frac.frac_id
     , frac.fractienummer
     , frty.code frty_code
     , frty.omschrijving frty_omschrijving
     , cond2.code cond_code_frac
     , cond2.omschrijving cond_omschrijving_frac
     , meti.meti_id
     , meti.datum_aanmelding
     , decode (meti.prioriteit, 'J', 'J', onde.spoed) spoed
     , stgr.code stgr_code
     , stgr.omschrijving stgr_omschrijving
     , kgc_wlst_00.werklijst_lijst (null, meti.meti_id) werklijst
     , to_char(meti.conclusie) resultaat --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014)
     , bas_meti_00.alle_meet_commentaar (meti_id) commentaar
     , meti.afgerond
     , meti.datum_afgerond
     , mede.naam mede_naam
     , kgc_onwy_00.onwy_omschrijving (onde.onde_id) onwy_omschrijving
     , wm_concat (FAMI.FAMILIENUMMER) familes
from   kgc_kgc_afdelingen kafd
     , kgc_onderzoeksgroepen ongr
     , kgc_herkomsten herk
     , kgc_condities cond1
     , kgc_condities cond2
     , bas_fractie_types frty
     , kgc_materialen mate
     , kgc_stoftestgroepen stgr
     , kgc_medewerkers mede
     , bas_fracties frac
     , kgc_relaties rela
     , bas_metingen meti
     , kgc_monsters mons
     , kgc_onderzoeken onde
     , kgc_onderzoek_monsters onmo
     , kgc_personen pers -- toevoeging MGOL tvb IPN CA
     , kgc_families fami -- toevoeging MGOL tvb IPN nieuwe werkwijze (wens 5712)
     , kgc_familie_leden fale -- toevoeging MGOL tvb IPN nieuwe werkwijze (wens 5712)
where  onmo.onde_id = onde.onde_id
   and MONS.PERS_ID = PERS.PERS_ID -- toevoeging MGOL tvb IPN CA
   and onde.kafd_id = kafd.kafd_id
   and onde.ongr_id = ongr.ongr_id
   and onde.rela_id = rela.rela_id
   and onmo.mons_id = mons.mons_id
   and mons.mate_id = mate.mate_id
   and mons.herk_id = herk.herk_id(+)
   and mons.cond_id = cond1.cond_id(+)
   and mons.mons_id = frac.mons_id
   and frac.frty_id = frty.frty_id(+)
   and frac.cond_id = cond2.cond_id(+)
   and frac.frac_id = meti.frac_id
   and onde.onde_id = meti.onde_id
   and meti.stgr_id = stgr.stgr_id(+)
   and meti.mede_id = mede.mede_id(+)
   and PERS.PERS_ID = FALE.PERS_ID(+) -- toevoeging MGOL tvb IPN nieuwe werkwijze (wens 5712)
   and FALE.FAMI_ID = FAMI.FAMI_ID(+) -- toevoeging MGOL tvb IPN nieuwe werkwijze (wens 5712)
   -- Rene: BEH_ANALYSEFORMULIEREN_VW aangemaakt als kopie van KGC_ANALYSEFORMULIEREN_VW met als verschil het onderstaande filter op afgerond=N.
   -- In Nijmegen moet het analyseformulier report ook afgesloten onderzoeken tonen i.v.m. het digitaal dossier.
   -- *** and onde.afgerond = 'N' ***
group by kafd.code -- Gehele GROUP BY toegevoegd imv wm_concat functie
     , ongr.code, mons.pers_id, PERS.ZISNR -- toevoeging MGOL tvb IPN CA
     , kgc_pers_00.info (mons.pers_id), kgc_zwan_00.duur(onde.onde_id)
     , mons.mate_id, mate.code, mate.omschrijving, mons.monsternummer, herk.code, herk.omschrijving, mons.datum_aanmelding
     , mons.datum_afname, rela.aanspreken, mons.hoeveelheid_monster || ' ' || mate.eenheid, cond1.code, cond1.omschrijving
     , onmo.onde_id, onde.onderzoeknr, onde.geplande_einddatum, onde.afgerond, kgc_indi_00.onderzoeksindicaties (onde.onde_id)
     , onde.omschrijving, kgc_onde_00.gerelateerde_onderzoeken (onde.onde_id), frac.frac_id, frac.fractienummer, frty.code
     , frty.omschrijving, cond2.code, cond2.omschrijving, meti.meti_id, meti.datum_aanmelding
     , decode (meti.prioriteit, 'J', 'J', onde.spoed), stgr.code, stgr.omschrijving
     , kgc_wlst_00.werklijst_lijst (null, meti.meti_id), to_char(meti.conclusie), bas_meti_00.alle_meet_commentaar (meti_id), meti.afgerond
     , meti.datum_afgerond, mede.naam, kgc_onwy_00.onwy_omschrijving (onde.onde_id);

/
QUIT
