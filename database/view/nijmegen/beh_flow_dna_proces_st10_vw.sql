CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST10_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND", "WEEK", "INDIGROEP", "ONDE_TYPE", "WIJZE", "TOT_30_DAGEN", "VAN_30_TOT_90_DAGEN", "VAN_90_TOT_365_DAGEN", "VAN_365_DAGEN", "TOTAAL") AS
  SELECT '#FFFFFF',
            1,
            '',
            JAAR_MAAND,
            WEEK,
            INDIGROEP,
            ONDE_TYPE,
            WIJZE,
            SUM ("<=30 dagen"),
            SUM (">30 & <=90 dagen"),
            SUM (">90 & <=365 dagen"),
            SUM (">365 dagen"),
            SUM (
                 "<=30 dagen"
               + ">30 & <=90 dagen"
               + ">90 & <=365 dagen"
               + ">365 dagen")
       FROM BEH_FLOW_DNA_STAT_OHW
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            WEEK,
            INDIGROEP,
            ONDE_TYPE,
            WIJZE
   ORDER BY JAAR_MAAND DESC, WEEK DESC;

/
QUIT
