CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC11_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "DATUM_AUTORISATIE", "DEAUTORISATIEDATUM", "DEAUTORISATOR", "REDENCODE", "OMSCHRIJVING", "TOELICHTING", "ONDE_TYPE", "NOTI_CODE", "NOTITIE", "NOTI_COMMENTAAR", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT    VERBORGEN_KLEUR,
   VERBORGEN_SORTERING,
   VERBORGEN_TELLING,
   FAVORIET,
   NAAR_HELIX,
   GROEP,
   ONDERZOEKNR,
   SPOED,
   BINNEN,
   EINDDATUM,
   DATUM_AUTORISATIE,
   DEAUTORISATIEDATUM,
   DEAUTORISATOR,
   REDENCODE,
   OMSCHRIJVING,
   TOELICHTING,
   ONDE_TYPE,
   NOTI_CODE,
   NOTITIE,
   NOTI_COMMENTAAR,
   VERBORGEN_NOTI_ID,
   VERBORGEN_FLOW_ID
FROM    (SELECT                        -- Overzicht 'Ge-dautoriseerde Onderzoeken'
                   '#79A8A8' as VERBORGEN_KLEUR,                                  --FLPR.VERBORGEN_KLEUR,
                    '' as VERBORGEN_SORTERING,                                    --FLPR.VERBORGEN_SORTERING,
                    ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
                    FLFA.MEDE_ID AS FAVORIET,
                    ONDE.PERS_ID AS NAAR_HELIX,
                    ONGR.CODE AS GROEP,
                    ONDE.ONDERZOEKNR,
                    ONDE.SPOED,
                    ONDE.DATUM_BINNEN AS BINNEN,
                    ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
                    ONAH.DATUM_AUTORISATIE,
                    DENSE_RANK() OVER (PARTITION BY ONDE.ONDE_ID ORDER BY ONAH.CREATION_DATE desc ) as RANKING,
                    (ONAH.CREATION_DATE) AS DEAUTORISATIEDATUM,
                    ONAH.CREATED_BY AS DEAUTORISATOR,
                    RNDA.CODE AS REDENCODE,
                    RNDA.OMSCHRIJVING,
                    ONAH.TOELICHTING,
                    DECODE (ONDE.ONDERZOEKSTYPE,
                            'R', 'Research',
                            'D', 'Diagnostiek',
                            'C', 'Controle',
                            '')
                       AS ONDE_TYPE,
                    NOTI.CODE AS NOTI_CODE,
                    NOTI.OMSCHRIJVING AS NOTITIE,
                    NOTI.COMMENTAAR AS NOTI_COMMENTAAR,
                    NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
                    FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
               FROM kgc_onderzoeken onde,
                    KGC_ONDE_AUTORISATIE_HISTORIE onah,
                    KGC_REDENEN_DE_AUTORISATIE rnda,
                    kgc_notities noti,
                    kgc_kgc_afdelingen kafd,
                    kgc_onderzoeksgroepen ongr,
                    BEH_FLOW_FAVORIETEN FLFA
              WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                           FROM BEH_FLOW_KAFD_ONGR fkon
                                          WHERE FKON.FLOW_ID = 3) -- MG: dit moet nog gewijzigd worden!!!
                    AND ONDE.ONDE_ID = NOTI.ID(+)
                    AND NOTI.ENTITEIT(+) = 'ONDE'
                    AND KAFD.KAFD_ID = ONDE.KAFD_ID
                    AND ONGR.ONGR_ID = ONDE.ONGR_ID
                    AND ONAH.ONDE_ID = ONDE.ONDE_ID
                    AND ONAH.RNDA_ID = RNDA.RNDA_ID
                    AND ONAH.ACTIE = 'D'
                    AND ONDE.AFGEROND = 'N'
                    AND ONDE.ONDE_ID = FLFA.ID(+)
                    AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
                    ) vw
   WHERE vw.ranking=1
   ORDER BY DEAUTORISATIEDATUM DESC,
            GROEP,
            SPOED ASC,
            ONDERZOEKNR ASC;

/
QUIT
