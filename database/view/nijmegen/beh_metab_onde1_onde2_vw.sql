CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_METAB_ONDE1_ONDE2_VW" ("PERS_ID", "AANSPREKEN", "GEBOORTEDATUM", "GESLACHT", "ONDE_ID_1", "ONDERZOEKNR_1", "DATUM_BINNEN_1", "AFGEROND_1", "DECLAREREN_1", "ONDERZOEKSTYPE_1", "DATUM_AUTORISATIE_1", "ONDE_ID_2", "ONDERZOEKNR_2", "DATUM_BINNEN_2", "AFGEROND_2", "DECLAREREN_2", "ONDERZOEKSTYPE_2", "DATUM_AUTORISATIE_2", "ONGR_CODE_1", "ONGR_CODE_2") AS
  select pers1.pers_id pers_id
	   , pers1.aanspreken aanspreken
	   , pers1.geboortedatum geboortedatum
   	   , pers1.geslacht geslacht
	   , onde1.onde_id onde_id_1
	   , onde1.onderzoeknr onderzoeknr_1
	   , onde1.datum_binnen datum_binnen_1
	   , onde1.afgerond afgerond_1
	   , onde1.declareren declareren_1
	   , onde1.onderzoekstype onderzoekstype_1
	   , onde1.datum_autorisatie datum_autorisatie_1
  	   , null onde_id_2
	   , null onderzoeknr_2
	   , null datum_binnen_2
	   , null afgerond_2
	   , null declareren_2
	   , null onderzoekstype_2
	   , null datum_autorisatie_2
	   , ongr1.code ongr_code_1
   	   , null ongr_code_2
from   kgc_personen pers1
	 , kgc_onderzoeken onde1
	 , kgc_kgc_afdelingen kafd1
	 , kgc_onderzoeksgroepen ongr1
where pers1.pers_id = onde1.pers_id
and   onde1.kafd_id = kafd1.kafd_id
and   onde1.ongr_id = ongr1.ongr_id
and   UPPER(kafd1.code) = UPPER('METAB')
and exists( select *
from   kgc_personen pers3
	 , kgc_onderzoeken onde3
	 , kgc_kgc_afdelingen kafd3
	 , kgc_onderzoeksgroepen ongr3
where pers3.pers_id = onde3.pers_id
and   onde3.kafd_id = kafd3.kafd_id
and   onde3.ongr_id = ongr3.ongr_id
and   UPPER(kafd3.code) = UPPER('METAB')
and   onde3.pers_id = onde1.pers_id
group by onde3.pers_id
having count(onde3.pers_id) < 2 )
union
select pers1.pers_id pers_id
	   , pers1.aanspreken aanspreken
	   , pers1.geboortedatum geboortedatum
   	   , pers1.geslacht geslacht
	   , onde1.onde_id onde_id_1
	   , onde1.onderzoeknr onderzoeknr_1
	   , onde1.datum_binnen datum_binnen_1
	   , onde1.afgerond afgerond_1
	   , onde1.declareren declareren_1
	   , onde1.onderzoekstype onderzoekstype_1
	   , onde1.datum_autorisatie datum_autorisatie_1
   	   , onde2.onde_id onde_id_2
	   , onde2.onderzoeknr onderzoeknr_2
	   , onde2.datum_binnen datum_binnen_2
	   , onde2.afgerond afgerond_2
	   , onde2.declareren declareren_2
	   , onde2.onderzoekstype onderzoekstype_2
	   , onde2.datum_autorisatie datum_autorisatie_2
	   , ongr1.code ongr_code_1
   	   , ongr2.code ongr_code_2
from   kgc_personen pers1
	 , kgc_onderzoeken onde1
	 , kgc_onderzoeken onde2
	 , kgc_kgc_afdelingen kafd1
	 , kgc_onderzoeksgroepen ongr1
	 , kgc_kgc_afdelingen kafd2
	 , kgc_onderzoeksgroepen ongr2
where pers1.pers_id = onde1.pers_id
and   pers1.pers_id = onde2.pers_id
and   onde1.onde_id <> onde2.onde_id
and   onde1.kafd_id = kafd1.kafd_id
and   onde2.kafd_id = kafd2.kafd_id
and   onde1.ongr_id = ongr1.ongr_id
and   onde2.ongr_id = ongr2.ongr_id
and   UPPER(kafd1.code) = UPPER('METAB')
and   UPPER(kafd2.code) = UPPER('METAB')
 ;

/
QUIT
