CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC46_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "STATUS", "FACILITEIT", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT                   -- Overzicht NGS - Status is mislukt of in wacht
           '#79A8A8' VERBORGEN_KLEUR,
            '' VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            TO_CHAR (wm_concat (DISTINCT FAMI.FAMILIENUMMER)) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            PVT.STATUS,
            PVT.FACILITEIT,
            --            STOF.CODE,
            --            STOF.OMSCHRIJVING,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            --            kgc_stoftestgroepen stgr,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_onderzoekswijzen onwy,
            --            kgc_stoftesten stof,
            --            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa,
            (SELECT exal.meet_id, EXAL.STATUS, EXAL.FACILITEIT
               FROM BEH_FLOW_NGS_EXA_LIB_MDET_VW exal
              WHERE STATUS IS NOT NULL --                    AND DATA_BESCHIKBAAR IS NULL
                                      --                    AND DATA_OPGEHAALD IS NULL
                                      --                    AND DATA_GEBACK_UPT IS NULL
            ) pvt
      WHERE                 --  FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC46_VW'
                --            AND ONDE.KAFD_ID = FLPR.KAFD_ID
                --            AND ONDE.ONDE_ID = FLPR.ONDE_ID
                --            AND
                ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            --            AND meti.stgr_id = stgr.stgr_id
            AND FRAC.FRAC_ID = METI.FRAC_ID
            --            AND STOF.STOF_ID = MEET.STOF_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND MEET.MEET_ID = PVT.MEET_ID
            AND MEET.AFGEROND = 'N'
            AND ONDE.AFGEROND = 'N'
   --            AND STGR.CODE = 'NGS_ST_1'                       -- mantisnr 11373
   GROUP BY                                            --FLPR.VERBORGEN_KLEUR,
            --            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            FRAC.FRACTIENUMMER,
            WLST.WERKLIJST_NUMMER,
            FLFA.FLOW_ID,
            PVT.STATUS,
            PVT.FACILITEIT,
            --            STOF.CODE,
            --            STOF.OMSCHRIJVING,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID
   ORDER BY                                             --VERBORGEN_SORTERING,
           SPOED ASC,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC,
            FRAC.FRACTIENUMMER ASC;

/
QUIT
