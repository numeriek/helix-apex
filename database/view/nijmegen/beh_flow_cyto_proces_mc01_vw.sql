CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC01_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "AANVRAAG", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "NOTITIE", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_BESTANDEN
        AS (SELECT BEST.ENTITEIT_PK,
                   BEST.ENTITEIT_CODE,
                   beh_bestanden_to_string (
                      CAST (
                         COLLECT (
                               '<a href="'
                            || BEST.BESTAND_SPECIFICATIE
                            || '" target="_blank">Open</a>' ORDER                   BY BEST.BEST_ID desc ) as beh_bestanden_link)) as bestanden
                FROM KGC_BESTANDEN BEST, KGC_BESTAND_CATEGORIEEN BCAT
                WHERE BEST.BCAT_ID = BCAT.BCAT_ID
                    AND BEST.ENTITEIT_CODE  = 'ONDE'
                    AND BCAT.CODE <> ('DUMMY')
                    AND BEST.VERVALLEN = 'N'
                    AND BEST.VERWIJDERD = 'N'
                group by best.ENTITEIT_PK, BEST.ENTITEIT_CODE),
   W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED (+) = 'N'
                    AND NOTI.ENTITEIT = 'ONDE')
     SELECT                                     -- Onderzoeken ter goedkeuring
           '#79A8A8',                                  --FLPR.VERBORGEN_KLEUR,
            '',                                    --FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            BEST.BESTANDEN,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED as NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            w_kgc_bestanden best,
            W_KGC_NOTITIES noti,
            --BEH_FLOW_PRIORITEIT_NEW_VW flpr,
            beh_flow_favorieten flfa
      WHERE                  --FLPR.VIEW_NAME = 'BEH_FLOW_CYTO_PROCES_MC01_VW'
                --AND
                ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK(+)
            AND BEST.ENTITEIT_CODE(+) = 'ONDE'
            --AND ONDE.KAFD_ID = FLPR.KAFD_ID
            --AND FLPR.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND NVL (ONDE.STATUS, 'onb') NOT IN ('G', 'V')
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY                                             --VERBORGEN_SORTERING,
           ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
