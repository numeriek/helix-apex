CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DECL_METAB_AFG_ONDE_J_VW" ("NAAR_HELIX", "DECL_ID", "DECL_DATUM", "VERRICHTINGCODE", "SERVICE_PROVIDER", "ZISNR", "OVERLEDEN", "OVERLIJDENSDATUM", "BEHEER_IN_ZIS", "ONDERZOEKNR", "ONDE_TYPE", "GROEP", "RELA_CODE", "ZNR", "RELA_AANSPREKEN", "AANVRAGER_TYPE", "DECL_TYPE", "SER_ID", "SPEC_CODE", "BEVOEGD", "DEWY_CODE", "ICODE", "INST_CODE", "DECL_CHECK", "PERS_CHECK", "RELA_CHECK", "INST_CHECK", "VERBORGEN_KLEUR", "VERBORGEN_SORTERING") AS
  SELECT pers_id naar_helix,
          decl_id,
          decl_datum,
          verrichtingcode,
          service_provider,
          zisnr,
          overleden,
          overlijdensdatum,
          beheer_in_zis,
          onderzoeknr,
          DECODE (ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    'C', 'Controle',
                    '')
               AS ONDE_TYPE,
          ongr_code groep,
          rela_code,
          znr,
          rela_aanspreken,
          aanvrager_type,
          decl_type,
          ser_id,
          spec_code,
          bevoegd_declareren bevoegd,
          dewy_code,
          icode,
          inst_code,
          decl_check,
          pers_check,
          rela_check,
          inst_check,
          '#FFFFFF' verborgen_kleur,
          decl_datum verborgen_sortering
     FROM beh_decl_check_vw
    WHERE kafd_code = 'METAB'
    and onde_afgerond = 'J';

/
QUIT
