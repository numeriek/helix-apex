CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC44_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "STOF_CODE", "STOF_OMSCHRIJVING", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht "Monsters die naar BGI zijn gestuurd en waarbij data teruggekomen  en geshared is -- STATUS = DATA ONTVANGEN en DATA SHARED = 'J'
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            TO_CHAR (wm_concat (DISTINCT FAMI.FAMILIENUMMER)) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            STOF.CODE,
            STOF.OMSCHRIJVING,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            BAS_MEETWAARDE_DETAILS mdet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_technieken tech,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC44_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND STOF.TECH_ID = TECH.TECH_ID(+)
            AND MDET.MEET_ID = MEET.MEET_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND MEET.MEET_REKEN = 'S'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND (UPPER (MDET.PROMPT) = 'STATUS'
                 AND UPPER (MDET.WAARDE) = '1STE ANALYSE KLAAR')
            AND ONGR.CODE = 'NGS'
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            FRAC.FRACTIENUMMER,
            WLST.WERKLIJST_NUMMER,
            FLFA.FLOW_ID,
            STOF.CODE,
            STOF.OMSCHRIJVING,
            MEET.AFGEROND,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            MDET.WAARDE
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC,
            FRAC.FRACTIENUMMER ASC;

/
QUIT
