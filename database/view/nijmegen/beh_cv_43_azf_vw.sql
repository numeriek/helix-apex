CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_CV_43_AZF_VW" ("ZISNR", "AANSPREKEN", "ONDERZOEKSGROEP_43", "ONDERZOEKNR_43", "INDICATIES_43", "UITSLAGCODE_43", "ONDERZOEKSGROEP_AZF", "ONDERZOEKNR_AZF", "INDICATIES_AZF", "UITSLAGCODE_AZF") AS
  select --count(*)  aantal
PERS.ZISNR
, pers.aanspreken
, ongr.code onderzoeksgroep_43
, onde.onderzoeknr onderzoeknr_43
, BEH_ONDERZOEK_INDICATIES(onde.onde_id) indicaties_43
, nvl(onui.code, ' ') uitslagcode_43
, ongr2.code onderzoeksgroep_AZF
, onde2.onderzoeknr onderzoeknr_AZF
, BEH_ONDERZOEK_INDICATIES(onde2.onde_id) indicaties_AZF
, nvl(onui2.code, ' ') uitslagcode_AZF
from KGC_PERSONEN pers
, KGC_ONDERZOEKEN onde
, kgc_onderzoeksgroepen ongr
, KGC_ONDERZOEK_UITSLAGCODES onui
, kgc_onderzoek_indicaties onin
, kgc_indicatie_teksten indi
, KGC_ONDERZOEKEN onde2
, kgc_onderzoeksgroepen ongr2
, KGC_ONDERZOEK_UITSLAGCODES onui2
, kgc_onderzoek_indicaties onin2
, kgc_indicatie_teksten indi2
where pers.pers_id = ONDE.PERS_ID
and onde.onui_id = onui.onui_id(+)
and onde.ongr_id = ongr.ongr_id
and ONDE.ONDE_ID = onin.onde_id
and onin.indi_id = indi.indi_id
and indi.code = '43'
and pers.pers_id = ONDE2.PERS_ID
and onde2.onui_id = onui2.onui_id(+)
and onde2.ongr_id = ongr2.ongr_id
and ONDE2.ONDE_ID = onin2.onde_id
and onin2.indi_id = indi2.indi_id
and indi2.code = 'AZF'
order by PERS.ZISNR, pers.aanspreken;

/
QUIT
