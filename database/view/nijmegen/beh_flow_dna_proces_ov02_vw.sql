CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_OV02_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "AANVRAAG", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "CYTOMAT", "NOTITIE", "FAMILIE_NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT              -- Goedgekeurde onderzoeken zonder Geplande Einddatum
           '#79A8D6' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            CASE
               WHEN BEST.BESTAND_SPECIFICATIE IS NULL
               THEN
                  ''
               ELSE
                     '<a href="'
                  || BEST.BESTAND_SPECIFICATIE
                  || '" target="_blank">Open</a>'
            END
               AS AANVRAAG,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            LISTAGG (
               ' ' || FRAC.FRACTIENUMMER || ' - '
               || CASE
                     WHEN FLDS.AVAILABLE = 'NIL'
                     THEN
                        'REMP'
                     ELSE
                        CASE
                           WHEN FLDS.AVAILABLE IS NULL THEN 'GEEN'
                           ELSE FLDS.AVAILABLE
                        END
                  END,
               ',')
            WITHIN GROUP (ORDER BY FRAC.FRACTIENUMMER)
               AS CYTOMAT,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTIFAMI.OMSCHRIJVING AS FAMILIE_NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_onderzoek_monsters onmo,
            kgc_monsters mons,
            BEH_FLOW_DNA_STORAGE FLDS,
            bas_fracties frac,
            kgc_notities notifami,
            kgc_bestanden best,
            beh_flows flow,
            beh_flow_details flde,
            beh_flow_favorieten flfa
      WHERE     FLDE.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_OV02_VW'
            AND ONDE.KAFD_ID = FLOW.KAFD_ID
            AND FLOW.FLOW_ID = FLDE.FLOW_ID
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND FAMI.FAMI_ID = NOTIFAMI.ID(+)
            AND NOTIFAMI.ENTITEIT(+) = 'FAMI'
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK(+)
            AND BEST.ENTITEIT_CODE(+) = 'ONDE'
            AND BEST.VERVALLEN = 'N'
            AND BEST.VERWIJDERD = 'N'
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = ONMO.ONDE_ID(+)
            AND ONMO.MONS_ID = MONS.MONS_ID(+)
            AND MONS.MONS_ID = FRAC.MONS_ID(+)
            AND FRAC.FRACTIENUMMER = FLDS.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND NVL (ONDE.STATUS, 'onb') = 'G'
            AND ONDE.GEPLANDE_EINDDATUM IS NULL
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY ONDE.CREATION_DATE,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            FAMI.FAMILIENUMMER,
            NOTIFAMI.OMSCHRIJVING,
            ONDE.ONDERZOEKNR,
            BEST.BESTAND_SPECIFICATIE,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY ONDE.CREATION_DATE ASC,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
