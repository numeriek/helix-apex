CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_VERHUIS_DRAGON_FILES_VW" ("BEST_ID", "JAARTAL", "BESTAND_SPECIFICATIE", "BESTAND_SPECIFICATIE_NEW", "LOCATIE", "LOCATIE_NEW") AS
  select "BEST_ID","JAARTAL","BESTAND_SPECIFICATIE","BESTAND_SPECIFICATIE_NEW","LOCATIE","LOCATIE_NEW"
from
(
select best.best_id
, to_char(best.creation_date, 'YYYY') jaartal
, best.bestand_specificatie
, '\\UMCMS005\GEN_STG$\DRAGON\'
|| substr( SYS_CONTEXT('USERENV','DB_NAME'), 1, 4)
|| '\'
|| substr(best.bestand_specificatie, instr(upper(best.bestand_specificatie), 'AANVRAGEN'), (instr(best.bestand_specificatie, '\', -1) - instr(upper(best.bestand_specificatie), 'AANVRAGEN'))+1 )
|| to_char(best.creation_date, 'YYYY')
|| substr(best.bestand_specificatie, instr(best.bestand_specificatie, '\', -1))
bestand_specificatie_new
,best.locatie
,case
  when best.locatie is null then
    null
  else
    '\\UMCMS005\GEN_STG$\DRAGON\'||
    substr( SYS_CONTEXT('USERENV','DB_NAME'), 1, 4) || '\' ||
    substr(best.locatie, instr(upper(best.locatie), 'AANVRAGEN')) ||
    to_char(best.creation_date, 'YYYY') ||
    '\'
end locatie_new
from kgc_bestanden best, kgc_bestand_types btyp
where best.btyp_id = btyp.btyp_id
and btyp.code in ('IMFI_DOC', 'IMFI_MONS', 'IMFI_ONDE')
and not exists
(
select null
from kgc_bestanden_log
where best_id = best.best_id
)
order by best.creation_date
)
where rownum <= 20000;

/
QUIT
