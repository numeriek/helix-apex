CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_EAS_TESTEN_VW" ("AFDELING", "INDI_INDI_ID", "GEN", "ONDERZOEKSGROEP", "OMSCHRIJVING", "DRAGON_OMSCHRIJVING", "URL", "AANBIEDEN_INTL", "AANBIEDEN_NATL", "OMSCHRIJVING_ENG", "PRIJS", "OMIMNR", "TYPE") AS
  SELECT 'GENOOM',
          indi.indi_id indi_indi_id,
          indi.code gen,
          ongr.code,
          DECODE (
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id),
             '*', indi.code,
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id))
             dragon_code,
          NVL (
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_OMSCHRIJVING',
                                 indi.indi_id),
             indi.omschrijving)
             dragon_omschrijving,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGON_URL',
                              indi.indi_id)
             dragon_url,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGONW_AANB_INTL',
                              indi.indi_id)
             dragonw_aanb_intl,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGONW_AANB_NATL',
                              indi.indi_id)
             dragonw_aanb_natl,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGON_CODE_ENG',
                              indi.indi_id)
             dragon_code_eng,
          REPLACE (
             REPLACE (
                REPLACE (
                   kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGONW_PRIJS',
                                       indi.indi_id),
                   '�',
                   ''),
                ' ',
                ''),
             '.',
             '')
             dragonw_prijs,
          beh_eas_00.bepaal_omimnr (
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_URL',
                                 indi.indi_id))
             omimnr,
          'GEN' TYPE
     FROM kgc_indicatie_groepen ingr,
          kgc_indicatie_teksten indi,
          kgc_onderzoeksgroepen ongr
    WHERE     ingr.ingr_id = indi.ingr_id
          AND ingr.ongr_id = ongr.ongr_id
          AND ingr.kafd_id = 1
          AND ( (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                     'DRAGON_CODE',
                                     indi.indi_id)
                    IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_OMSCHRIJVING',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_URL',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGONW_AANB_INTL',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGONW_AANB_NATL',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_CODE_ENG',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGONW_PRIJS',
                                       indi.indi_id)
                      IS NOT NULL))
          AND indi.vervallen = 'N'
          AND ingr.vervallen = 'N'
          AND BEH_ALFU_00.IS_NUMMER (
                 TO_NUMBER (
                    REPLACE (
                       REPLACE (
                          REPLACE (
                             REPLACE (
                                REPLACE (
                                   kgc_attr_00.waarde (
                                      'KGC_INDICATIE_TEKSTEN',
                                      'DRAGONW_PRIJS',
                                      indi.indi_id),
                                   '�',
                                   ''),
                                ' ',
                                ''),
                             ',',
                             ''),
                          '-',
                          ''),
                       '.',
                       ''))) = 'J'
          AND INSTR (
                 UPPER (
                    kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                        'DRAGON_CODE',
                                        indi.indi_id)),
                 'PANEL') < 1
   UNION                                                      -- ook de panels
   SELECT 'GENOOM',
          indi_indi_id,
          omschrijving ongr_code,
          omschrijving,
          omschrijving_engels,                                         --NULL,
          omschrijving_engels,                                         --NULL,
          NULL,
          NULL,
          NULL,
          NULL,
          NULL,
          prijs,
          'PANEL' TYPE
     FROM beh_eas_producten prod
    WHERE     NVL (gepubliceerd, 'O') <> 'N'
          AND product_type = 'PANEL'
          AND indi_indi_id IS NOT NULL
   UNION                                                 -- ook de exoompanels
   SELECT 'GENOOM',
          indi_indi_id,
          omschrijving_engels,
          'NGS',
          omschrijving,                                                --NULL,
          omschrijving_engels,                                         --NULL,
          NULL,
          NULL,
          NULL,
          NULL,
          '1800',
          NULL,
          'EXOOM_PAKKET' TYPE
     FROM beh_eas_producten prod
    WHERE     gepubliceerd = 'J'
          AND product_type = 'EXOOM_PAKKET'
          AND indi_indi_id IS NOT NULL;

/
QUIT
