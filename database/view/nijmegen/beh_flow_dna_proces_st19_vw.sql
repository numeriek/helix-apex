CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST19_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND", "GROEP", "ONDE_TYPE", "WIJZE", "G01", "G02", "G03", "G04", "G05", "G06", "G07", "G08", "G09", "G00", "ONRE", "PERC_AFGEROND_TOTAAL") AS
  SELECT '#FFFFFF' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            '' AS VERBORGEN_TELLING,
            STAG.JAAR_MAAND,
            STAG.GROEP,
            STAG.ONDE_TYPE,
            STAG.WIJZE,
            G01,
            G02,
            G03,
            G04,
            G05,
            G06,
            G07,
            G08,
            G09,
            G00,
            ONRE,
            ROUND (PERCENTILE_CONT (0.95) WITHIN GROUP (ORDER BY GEM_TAT), 2)
               AS PERC_AFGEROND_TOTAAL
       FROM BEH_FLOW_DNA_STAT_AFG STAG,
            ( -- in deze subquery (SUB1) wordt een pivot table gecreerd op basis van de gegevens die in SUB2 query verzameld worden
             SELECT   JAAR_MAAND,
                      GROEP,
                      ONDE_TYPE,
                      WIJZE,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G01'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G01,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G02'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G02,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G03'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G03,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G04'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G04,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G05'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G05,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G06'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G06,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G07'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G07,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G08'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G08,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G09'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G09,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'G00'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS G00,
                      ROUND (
                         AVG (
                            CASE
                               WHEN SUB2.INDIGROEP = 'ONRE'
                               THEN
                                  SUB2.PERC_AFGEROND
                               ELSE
                                  NULL
                            END),
                         0)
                         AS ONRE
                 FROM ( -- In deze subquery (SUB 2) worden de gegevens geselecteerd en de gewogen tat berekend
                       SELECT   JAAR_MAAND,
                                GROEP,
                                INDIGROEP,
                                ONDE_TYPE,
                                WIJZE,
                                ROUND (
                                   PERCENTILE_CONT (0.95)
                                      WITHIN GROUP (ORDER BY GEM_TAT),
                                   2)
                                   AS PERC_AFGEROND
                           FROM BEH_FLOW_DNA_STAT_AFG
                       GROUP BY JAAR_MAAND,
                                GROEP,
                                INDIGROEP,
                                ONDE_TYPE,
                                WIJZE) SUB2
             GROUP BY JAAR_MAAND,
                      GROEP,
                      ONDE_TYPE,
                      WIJZE) SUB1
      WHERE     SUB1.JAAR_MAAND = STAG.JAAR_MAAND
            AND SUB1.ONDE_TYPE = STAG.ONDE_TYPE
            AND SUB1.GROEP = STAG.GROEP
            AND SUB1.WIJZE = STAG.WIJZE
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            STAG.JAAR_MAAND,
            STAG.GROEP,
            STAG.ONDE_TYPE,
            STAG.WIJZE,
            G01,
            G02,
            G03,
            G04,
            G05,
            G06,
            G07,
            G08,
            G09,
            G00,
            ONRE
   ORDER BY STAG.JAAR_MAAND DESC, STAG.GROEP, STAG.ONDE_TYPE ASC;

/
QUIT
