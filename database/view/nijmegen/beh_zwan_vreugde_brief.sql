CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ZWAN_VREUGDE_BRIEF" ("ZWAN_ID", "CREATION_DATE") AS
  select
 brie.zwan_id
, brie.creation_date
from kgc_brieven brie
, kgc_brieftypes brty
where brie.brty_id = brty.brty_id
and brie.zwan_id is not null
and brty.code = 'VREUGDE'
 ;

/
QUIT
