CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DECLARATIES_VW" ("ONDE_ID", "ONBE_ID", "METI_ID", "DECL_ID", "ENTITEIT", "ID", "DECLAREREN", "KAFD_ID", "ONGR_ID", "PERS_ID", "DEWY_ID", "INGR_ID", "VERZ_ID", "FARE_ID", "RNDE_ID", "STATUS", "DATUM_VERWERKING", "NOTA_ADRES", "VERZEKERINGSWIJZE", "VERZEKERINGSNR", "TOELICHTING", "BETREFT_TEKST", "DEEN_ID", "DATUM", "VERRICHTINGCODE", "VERWERKING", "RELA_ID") AS
  select -- onderzoeken
  onde.onde_id
, null onbe_id
, null meti_id
, decl.decl_id
, decl.entiteit
, decl.id
, decl.declareren
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.dewy_id
, decl.ingr_id
, decl.verz_id
, decl.fare_id
, decl.rnde_id
, decl.status
, decl.datum_verwerking
, decl.nota_adres
, decl.verzekeringswijze
, decl.verzekeringsnr
, decl.toelichting
, decl.betreft_tekst
, decl.deen_id
, decl.datum
, decl.verrichtingcode
, decl.verwerking
, case onde.rela_id_oorsprong
  when null then onde.rela_id
  else onde.rela_id_oorsprong
  end rela_id
from kgc_declaraties decl
,    kgc_onderzoeken onde
where decl.entiteit = 'ONDE'
and   decl.id       = onde.onde_id
union all
select -- betrokkenen
  onde.onde_id
, onbe.onbe_id
, null meti_id
, decl.decl_id
, decl.entiteit
, decl.id
, decl.declareren
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.dewy_id
, decl.ingr_id
, decl.verz_id
, decl.fare_id
, decl.rnde_id
, decl.status
, decl.datum_verwerking
, decl.nota_adres
, decl.verzekeringswijze
, decl.verzekeringsnr
, decl.toelichting
, decl.betreft_tekst
, decl.deen_id
, decl.datum
, decl.verrichtingcode
, decl.verwerking
--, onde.rela_id
, case onde.rela_id_oorsprong
  when null then onde.rela_id
  else onde.rela_id_oorsprong
  end rela_id
from kgc_declaraties decl
,    kgc_onderzoeken onde
,    kgc_onderzoek_betrokkenen onbe
where decl.entiteit = 'ONBE'
and   decl.id       = onbe.onbe_id
and   onbe.onde_id  = onde.onde_id
union all
select -- metingen
  onde.onde_id
, null onbe_id
, meti.meti_id
, decl.decl_id
, decl.entiteit
, decl.id
, decl.declareren
, decl.kafd_id
, decl.ongr_id
, decl.pers_id
, decl.dewy_id
, decl.ingr_id
, decl.verz_id
, decl.fare_id
, decl.rnde_id
, decl.status
, decl.datum_verwerking
, decl.nota_adres
, decl.verzekeringswijze
, decl.verzekeringsnr
, decl.toelichting
, decl.betreft_tekst
, decl.deen_id
, decl.datum
, decl.verrichtingcode
, decl.verwerking
--, onde.rela_id
, case onde.rela_id_oorsprong
  when null then onde.rela_id
  else onde.rela_id_oorsprong
  end rela_id
from kgc_declaraties decl
,    kgc_onderzoeken onde
,    bas_metingen meti
where decl.entiteit = 'METI'
and   decl.id       = meti.meti_id
and   meti.onde_id  = onde.onde_id;

/
QUIT
