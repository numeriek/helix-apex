CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC03_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "ONDE_TYPE", "STATUS", "PROTOCOL", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT                        -- Overzicht 'Onderzoeken zonder werklijst'
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    'C', 'Controle',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            --            LISTAGG (STGR.CODE, ',') WITHIN GROUP (ORDER BY STGR.CODE) AS PROTOCOL,
            STGR.CODE AS PROTOCOL,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_kgc_afdelingen kafd,
            bas_metingen meti,
            kgc_stoftestgroepen stgr,
            kgc_onderzoeksgroepen ongr,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 3) -- MG: dit moet nog gewijzigd worden!!!
            AND FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC03_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.AFGEROND = 'N'
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.STGR_ID = STGR.STGR_ID(+)
            AND METI.AFGEROND = 'N'
            AND NOT EXISTS
                   (SELECT 1
                      FROM KGC_WERKLIJST_ITEMS WLIT
                     WHERE WLIT.METI_ID = METI.METI_ID)
            AND EXISTS
                   (SELECT 1
                      FROM BAS_MEETWAARDEN MEET1
                     WHERE METI.METI_ID = MEET1.METI_ID)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
