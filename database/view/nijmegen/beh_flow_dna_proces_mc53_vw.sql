CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC53_VW" ("VERBORGEN_KLEUR", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "BI_BU", "INT_EXT", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "AFGEROND", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT               -- Overzicht NGS - testen dienen bevestigd te worden
           FLPR.VERBORGEN_KLEUR,
            --            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            beh_haal_familienr (ONDE.PERS_ID) AS FAMILIENR,
            --TO_CHAR (wm_concat (DISTINCT FAMI.FAMILIENUMMER)) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            DECODE (
               beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
               'J', 'INTERN',
               'N', '')
               AS INT_EXT,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            --            PVT.BEVESTIGEN,
            PVT.AFGEROND,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            kgc_stoftestgroepen stgr,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            --            kgc_familie_leden fale,
            --            kgc_families fami,
            kgc_onderzoekswijzen onwy,
            BEH_FLOW_PRIORITEIT_NEW_VW flpr,
            beh_flow_favorieten flfa,
            (SELECT meet_id, BEVESTIGEN, AFGEROND
               FROM BEH_FLOW_NGS_EXA_S_MDET_VW
              WHERE BEVESTIGEN = 'J') pvt
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC53_VW'
            AND ONDE.ONDE_ID = NOTI.ID(+)
            --            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            --            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED(+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND FLPR.ONDE_ID = ONDE.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID(+)
            AND WLIT.WLST_ID = WLST.WLST_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND meti.stgr_id = stgr.stgr_id
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND MEET.MEET_ID = PVT.MEET_ID
            AND ONDE.AFGEROND = 'N'
            --            AND (STGR.CODE = 'NGS_RESEAR' OR STGR.CODE LIKE 'NGS_%_D') -- mantisnr 11373 -- MGOL 09-11-15 Volgens mij is dit toch een foute voorwaarde, ik haal het nu weg en ik zal contact opnemen met Wendy Buijsman hierover.
            AND NOT EXISTS
                       (SELECT 1
                          FROM bas_metingen meti1,
                               bas_meetwaarden meet1,
                               kgc_stoftesten stof1,
                               kgc_technieken tech1
                         WHERE     METI1.ONDE_ID = ONDE.ONDE_ID
                               AND METI1.METI_ID = MEET1.METI_ID
                               AND MEET1.STOF_ID = STOF1.STOF_ID
                               AND STOF1.TECH_ID = TECH1.TECH_ID
                               AND TECH1.CODE IN ('SEQ', 'GS'))
   --   GROUP BY VERBORGEN_KLEUR,
   --            VERBORGEN_SORTERING,
   --            ONDE.ONDERZOEKNR,
   --            FLFA.MEDE_ID,
   --            ONDE.PERS_ID,
   --            ONGR.CODE,
   --            ONDE.ONDERZOEKNR,
   --            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
   --                    'NEDERLAND', '',
   --                    'BUITENLAND', 'BU'),
   --            DECODE (
   --               beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
   --               'J', 'INTERN',
   --               'N', ''),
   --            ONDE.SPOED,
   --            ONDE.GEPLANDE_EINDDATUM,
   --            INTE.CODE,
   --            INGR.CODE,
   --            ONWY.OMSCHRIJVING,
   --            DECODE (ONDE.ONDERZOEKSTYPE,
   --                    'R', 'Research',
   --                    'D', 'Diagnostiek',
   --                    ''),
   --            FRAC.FRACTIENUMMER,
   --            WLST.WERKLIJST_NUMMER,
   --            FLFA.FLOW_ID,
   --            PVT.BEVESTIGEN,
   --            PVT.AFGEROND,
   --            NOTI.OMSCHRIJVING,
   --            NOTI.NOTI_ID
   ORDER BY SPOED ASC,                      --            VERBORGEN_SORTERING,
                      ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
