CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DLT_PROT_KORT_LANG_VW" ("SNELHEID", "WAARDE", "ONGR_ID", "GROEP") AS
  select -- doorlooptijden van kort- en langdurend protocol vastgelegd per onderzoeksgroep bij extra attribuut waarde
         -- dit wordt gebruikt voor het berekenen van doorlooptijden voor METAB in FLOW
    substr(ATTR.CODE, 10) as SNELHEID, ATWA.WAARDE, ONGR.ONGR_ID, ONGR.CODE as GROEP
from KGC_ATTRIBUUT_WAARDEN atwa, kgc_onderzoeksgroepen ongr, KGC_ATTRIBUTEN attr
where ATTR.ATTR_ID = ATWA.ATTR_ID
    and ATWA.ID = ONGR.ONGR_ID
    and ATTR.CODE like '%DLT%';

/
QUIT
