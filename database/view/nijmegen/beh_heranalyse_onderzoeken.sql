CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_HERANALYSE_ONDERZOEKEN" ("FAMI_ID", "FAON_ID", "FAMILIEONDERZOEKNR", "ONDE_ID", "PERS_ID", "GESLACHT", "FOETUS_FAON_ID", "ONDERZOEKNR", "ONDE_STATUS", "FRACTIENUMMER", "INDICATIE", "AANTAL_ONDE", "RELATIE") AS 
WITH HERANALYSE_DATA
      AS (SELECT DISTINCT
                 FAON.FAMI_ID,
                 FAON.FAON_ID,
                 FAON.FAMILIEONDERZOEKNR,
                 FOBE.ONDE_ID,
                 MONS.PERS_ID, -- pers id van betrokken fractie, niet van het onderzoekspersoon!!!
                 PERS.GESLACHT, -- geslacht van persoon van betrokken fractie, niet van het onderzoekspersoon!!!
                 CASE
                    WHEN ONDE.FOET_ID IS NOT NULL THEN FAON.FAON_ID
                    ELSE NULL
                 END
                    AS FOETUS, --om te bepalen dat het om prenatale TRIO onderzoeken gaat
                 ONDE.ONDERZOEKNR,
                 ONDE.STATUS,
                 FRAC.FRACTIENUMMER,
                 INDI.CODE,
                 COUNT (DISTINCT FOBE.ONDE_ID)
                    OVER (PARTITION BY FAON.FAON_ID)
                    AS Aantal_onde
            FROM kgc_familie_onderzoeken faon,
                 KGC_FAON_BETROKKENEN fobe,
                 kgc_onderzoeken onde,
                 kgc_indicatie_teksten indi,
                 bas_metingen meti,
                 bas_fracties frac,
                 kgc_monsters mons,
                 kgc_personen pers
           WHERE     FAON.FAON_ID = FOBE.FAON_ID
                 AND FAON.INDI_ID = INDI.INDI_ID
                 AND FOBE.ONDE_ID = METI.ONDE_ID
                 AND FOBE.ONDE_ID = ONDE.ONDE_ID
                 AND FRAC.FRAC_ID = METI.FRAC_ID
                 AND FOBE.ONDE_ID = METI.ONDE_ID
                 AND FRAC.MONS_ID = MONS.MONS_ID
                 AND PERS.PERS_ID = MONS.PERS_ID
                 AND NOT EXISTS
                            (SELECT 1
                               FROM kgc_onderzoeken onde1,
                                    KGC_FAON_BETROKKENEN fobe1
                              WHERE     NVL (ONDE1.STATUS, 'LEEG') <> 'G'
                                    AND ONDE1.ONDE_ID = FOBE1.ONDE_ID
                                    AND FOBE1.FAON_ID = FAON.FAON_ID) -- uitsluitend goedgekeurde onderzoeken
                 AND NOT EXISTS
                            (SELECT BERI.ENTITEIT_ID
                               FROM BEH_BERICHTEN beri,
                                    BEH_BERICHT_TYPES bety
                              WHERE     BERI.BETY_ID = BETY.BETY_ID
                                    AND BERI.ENTITEIT_ID = FAON.FAON_ID
                                    AND BERI.ENTITEIT = 'FAON'
                                    AND (BERI.VERZONDEN = 'J'
                                         OR UPPER (
                                               SUBSTR (BERI.LOG_TEKST,
                                                       1,
                                                       4)) = 'TVF:')) -- uitsluitend onderzoeken ophalen die nog niet verstuurd zijn OF die Te Veel Fracties bevatten
                 AND ONDE.AFGEROND = 'N'
                 AND UPPER (INDI.CODE) LIKE '%HERANALYSE%')
 -- Gegevens van TRIO onderzoeken indien meerdere onderzoeken binnen een familie_onderzoek, echter GEEN prenatale onderzoeken
 SELECT DISTINCT heda."FAMI_ID",
                 heda."FAON_ID",
                 heda."FAMILIEONDERZOEKNR",
                 heda."ONDE_ID",
                 heda."PERS_ID",
                 heda."GESLACHT",
                 heda."FOETUS",
                 heda."ONDERZOEKNR",
                 heda."STATUS",
                 heda."FRACTIENUMMER",
                 heda."CODE",
                 heda."AANTAL_ONDE",
                 fare.relatie
   FROM HERANALYSE_DATA HEDA, BEH_FAMILIE_RELATIES FARE
  WHERE HEDA.PERS_ID = fare.pers_id AND heda.Aantal_onde > 1
        AND (fare.pers_id_relatie IN
                (SELECT fare1.pers_id_relatie
                   FROM BEH_FAMILIE_RELATIES fare1,
                        KGC_FAON_BETROKKENEN fobe1
                  WHERE fare1.pers_id_relatie = FOBE1.PERS_ID
                        AND FOBE1.FAON_ID = HEDA.FAON_ID)) -- uitsluitend releaties van familieleden binnen de desbetreffende familieonderzoek ophalen
 UNION ALL
 -- Gegevens van onderzoek indien uitsluitend een onderzoek binnen een familie_onderzoek
 SELECT DISTINCT heda."FAMI_ID",
                 heda."FAON_ID",
                 heda."FAMILIEONDERZOEKNR",
                 heda."ONDE_ID",
                 heda."PERS_ID",
                 heda."GESLACHT",
                 heda."FOETUS",
                 heda."ONDERZOEKNR",
                 heda."STATUS",
                 heda."FRACTIENUMMER",
                 heda."CODE",
                 heda."AANTAL_ONDE",
                 'KIND'
   FROM HERANALYSE_DATA HEDA
  WHERE heda.Aantal_onde = 1
 UNION ALL
 -- Gegevens van prenatale TRIO onderzoeken (tenminste een onderzoek binnen de desbetreffende familieonderzoek is een foetus onderzoek
 SELECT DISTINCT
        heda."FAMI_ID",
        heda."FAON_ID",
        heda."FAMILIEONDERZOEKNR",
        heda."ONDE_ID",
        heda."PERS_ID",
        heda."GESLACHT",
        heda."FOETUS",
        heda."ONDERZOEKNR",
        heda."STATUS",
        heda."FRACTIENUMMER",
        heda."CODE",
        heda."AANTAL_ONDE",
        CASE
           WHEN heda.foetus IS NOT NULL THEN 'KIND'
           WHEN heda.geslacht = 'V' THEN 'MOEDER'
           WHEN heda.geslacht = 'M' THEN 'VADER'
           ELSE 'ONBEKEND'
        END
           AS RELATIE
   FROM HERANALYSE_DATA HEDA
  WHERE heda.Aantal_onde > 1
        AND EXISTS
               (SELECT 1
                  FROM HERANALYSE_DATA HEDA1
                 WHERE heda1.foetus = heda.faon_id);
/

begin
  kgc_util_00.reconcile( p_object => 'BEH_HERANALYSE_ONDERZOEKEN' );
end;
/

