CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC52_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "BI_BU", "INT_EXT", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "AANVRAAG", "FRACTIENR", "WERKLIJSTNR", "GECONTROLEERD", "INTERPRETATIE_KLAAR", "BEVESTIGEN", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH w_kgc_bestanden
        AS (SELECT *
              FROM (SELECT best.*,
                           LISTAGG (
                              CASE
                                 WHEN best.bestand_specificatie IS NULL
                                 THEN
                                    ''
                                 ELSE
                                       '<a href="'
                                    || best.bestand_specificatie
                                    || '" target="_blank">Open</a>'
                              END,
                              ' ')
                           WITHIN GROUP (ORDER BY best.entiteit_pk)
                           OVER (PARTITION BY best.entiteit_pk)
                              aanvraag,
                           ROW_NUMBER ()
                           OVER (PARTITION BY best.entiteit_pk
                                 ORDER BY best.entiteit_pk)
                              AS row_nr
                      FROM kgc_bestanden best, kgc_bestand_categorieen bcat
                     WHERE     BEST.BCAT_ID = BCAT.BCAT_ID
                           AND best.entiteit_code = 'ONDE'
                           AND BCAT.CODE = 'AANVRAAG'
                           AND BEST.VERVALLEN = 'N'
                           AND BEST.VERWIJDERD = 'N')
             WHERE row_nr = 1)
     SELECT                     -- Overzicht NGS - 1ste interpretatie is klaar
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            TO_CHAR (wm_concat (DISTINCT FAMI.FAMILIENUMMER)) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            DECODE (
               beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
               'J', 'INTERN',
               'N', '')
               AS INT_EXT,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            BEST.AANVRAAG,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            MEST.CODE AS GECONTROLEERD,
            PVT.INTERPRETATIE_KLAAR,
            PVT.BEVESTIGEN,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            kgc_stoftestgroepen stgr,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_onderzoekswijzen onwy,
            w_kgc_bestanden best,
            --            kgc_stoftesten stof,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa,
            BAS_MEETWAARDE_STATUSSEN mest,
            (SELECT meet_id, INTERPRETATIE_KLAAR, BEVESTIGEN
               FROM BEH_FLOW_NGS_EXA_S_MDET_VW
              WHERE INTERPRETATIE_KLAAR IS NOT NULL   --AND (BEVESTIGEN = 'N')
                                                   ) pvt
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC52_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED(+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID(+)
            AND WLIT.WLST_ID = WLST.WLST_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND meti.stgr_id = stgr.stgr_id
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            --            AND STOF.STOF_ID = MEET.STOF_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK(+)
            AND MEET.MEET_ID = PVT.MEET_ID
            AND MEET.MEST_ID = MEST.MEST_ID(+)
            AND MEET.AFGEROND = 'N'
            AND ONDE.AFGEROND = 'N'
            AND NOT EXISTS
                       (SELECT 1
                          FROM bas_metingen meti1,
                               bas_meetwaarden meet1,
                               kgc_stoftesten stof1,
                               kgc_technieken tech1
                         WHERE     METI1.ONDE_ID = ONDE.ONDE_ID
                               AND METI1.METI_ID = MEET1.METI_ID
                               AND MEET1.STOF_ID = STOF1.STOF_ID
                               AND STOF1.TECH_ID = TECH1.TECH_ID
                               AND TECH1.CODE IN ('SEQ', 'GS'))
   --            AND (STGR.CODE = 'NGS_RESEAR' OR STGR.CODE LIKE 'NGS_%_D') -- mantisnr 11373-- MGOL 09-11-15 Volgens mij is dit toch een foute voorwaarde, ik haal het nu weg en ik zal contact opnemen met Wendy Buijsman hierover.
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU'),
            DECODE (
               beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
               'J', 'INTERN',
               'N', ''),
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            BEST.AANVRAAG,
            FRAC.FRACTIENUMMER,
            WLST.WERKLIJST_NUMMER,
            MEST.CODE,
            FLFA.FLOW_ID,
            --            STOF.CODE,
            --            STOF.OMSCHRIJVING,
            PVT.INTERPRETATIE_KLAAR,
            PVT.BEVESTIGEN,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID
   ORDER BY SPOED ASC,
            PVT.BEVESTIGEN ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
