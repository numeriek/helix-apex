CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_RELATIES_VW" ("RELA_ID", "ZNUMMER", "ACHTERNAAM", "AANSPREKEN", "AANPSREKEN_1", "INSTELLING", "ADRES", "WOONPLAATS", "LAND", "VOLLEDIG_ADRES", "SPECIALISME", "AFDELING", "ZIPCODE", "CITY", "COUNTRY") AS
  SELECT    rela.rela_id,
                rela.liszcode,
                rela.achternaam,
                rela.aanspreken,
                    rela.aanspreken
                || ',  Specialisme: '
                || spec.omschrijving
                || ', Afdeling: '
                || afdg.omschrijving
                    aanspreken_1,
                NVL (inst.naam, '') instelling,
                NVL (inst.adres, rela.adres) adres,
                NVL (inst.woonplaats, rela.woonplaats) woonplaats,
                NVL (inst.land, rela.land) land,
                kgc_rapp_00.volledig_adres_rela (rela_id) volledig_adres,
                spec.omschrijving specialisme,
                afdg.omschrijving afdeling,
                rela.postcode zipcode,
                rela.woonplaats city,
                rela.land country
         FROM kgc_relaties rela,
                kgc_specialismen spec,
                kgc_afdelingen afdg,
                kgc_instellingen inst
        WHERE      rela.spec_id = spec.spec_id(+)
                AND rela.afdg_id = afdg.afdg_id(+)
                AND rela.inst_id = inst.inst_id(+)
                AND rela.vervallen = 'N'
    ORDER BY achternaam;

/
QUIT
