CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC5_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "BI_BU", "INT_EXT", "TAAL", "WIJZE", "ONDE_TYPE", "AANVRAAG", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "BRIEFTYPE", "AANMAAK_DATUM", "FRACTIENR", "NOTITIE", "NOTI_GEREED", "UITS_COMMENTAAR", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH w_kgc_bestanden AS (SELECT *
                              FROM (SELECT best.*,
                                           LISTAGG (
                                              CASE
                                                 WHEN best.
                                                       bestand_specificatie
                                                         IS NULL
                                                 THEN
                                                    ''
                                                 ELSE
                                                    '<a href="'
                                                    || best.
                                                        bestand_specificatie
                                                    || '" target="_blank">Open</a>'
                                              END,
                                              ' ')
                                           WITHIN GROUP (ORDER BY
                                                            best.entiteit_pk)
                                           OVER (
                                              PARTITION BY best.entiteit_pk)
                                              aanvraag,
                                           ROW_NUMBER ()
                                           OVER (
                                              PARTITION BY best.entiteit_pk
                                              ORDER BY best.entiteit_pk)
                                              AS row_nr
                                      FROM kgc_bestanden best,
                                           kgc_bestand_categorieen bcat
                                     WHERE     BEST.BCAT_ID = BCAT.BCAT_ID
                                           AND best.entiteit_code = 'ONDE'
                                           AND BCAT.CODE = 'AANVRAAG'
                                           AND BEST.VERVALLEN = 'N'
                                           AND BEST.VERWIJDERD = 'N'
                                           )
                             WHERE row_nr = 1)
SELECT                                      --- Te ondertekenen uitslagen
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            flfa.mede_id AS favoriet,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            beh_haal_familienr(ONDE.PERS_ID) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            DECODE (beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
                    'J', 'INTERN',
                    'N', '')
               AS INT_EXT,
            TAAL.CODE TAAL,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            BEST.AANVRAAG,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            BRTY.CODE AS BRIEFTYPE,
            UITS.CREATION_DATE AS AANMAAK_DATUM,
            beh_haal_fractienr(onde.onde_id) AS FRACTIENR,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            UITS.COMMENTAAR AS UITS_COMMENTAAR,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_uitslagen uits,
            kgc_brieftypes brty,
            kgc_onderzoekswijzen onwy,
            w_kgc_bestanden best,
            kgc_notities noti,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
--            BEH_FLOW_ONDE_MEET_METI_VW omm,
            beh_flow_favorieten flfa,
            KGC_TALEN TAAL
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC5_VW'
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND UITS.ONDE_ID = ONDE.ONDE_ID
            AND UITS.BRTY_ID = BRTY.BRTY_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.AFGEROND = 'N'
            AND UITS.MEDE_ID IS NULL
            AND UITS.MEDE_ID2 IS NULL
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')

--            AND ONDE.ONDE_ID = OMM.ONDE_ID
--            AND OMM.NCONT_M + OMM.NCONT_S = 0

--            AND (exists (select 1
--                            from BEH_FLOW_ONDE_MEET_METI_VW omm
--                            where OMM.ONDE_ID = ONDE.ONDE_ID
--                                and OMM.NCONT_M + OMM.NCONT_S = 0)
            AND (exists (select 1
                            from BEH_FLOW_ONDE_MEET_METI_MATVW omm
                            where OMM.ONDE_ID = ONDE.ONDE_ID
                                and omm.NIET_OP_WLST_NIET_AF=0
                                and omm.WLST_AF_NCONT_S=0
                                and omm.WLST_AF_NCONT_M=0
                                and OMM.WLST_NIET_AF= 0
                                )
                  --OR NOTI.CODE = 'GEREED_ONDERTEKENING'  -- MG2016-06-20: vervangen door onderstaande code ivm dat niet alle notities bij de onderzoeken met gereed_ondertekening zichtbaar waren
                  OR exists (select 1
                                from kgc_notities noti
                                where NOTI.CODE = 'GEREED_ONDERTEKENING'
                                    and NOTI.ID= ONDE.ONDE_ID
                                    and NOTI.ENTITEIT='ONDE')
                  )

--            AND ONDE.ONDE_ID in (select TREAT (BEH_FLOW_ONDE_METI_MEET_AANTAL(ONDE.ONDE_ID) AS BEH_FLOW_ONDE_METI_MEET).ONDE_ID
--                                                            from dual
--                                                            where        TREAT (BEH_FLOW_ONDE_METI_MEET_AANTAL(ONDE.ONDE_ID) AS BEH_FLOW_ONDE_METI_MEET).ncont_m
--                                                                    + TREAT (BEH_FLOW_ONDE_METI_MEET_AANTAL(ONDE.ONDE_ID) AS BEH_FLOW_ONDE_METI_MEET).ncont_s =0)

   ORDER BY SPOED ASC, VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC


/*

     SELECT                                      --- Te ondertekenen uitslagen
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            flfa.mede_id AS favoriet,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            TO_CHAR (wm_concat (DISTINCT FAMI.FAMILIENUMMER)) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            BRTY.CODE AS BRIEFTYPE,
            UITS.CREATION_DATE AS AANMAAK_DATUM,
            --            MEDE.CODE AS ONDTKNR1,
            --            MEDE2.CODE AS ONDTKNR2,
            TO_CHAR (wm_concat (DISTINCT FRAC.FRACTIENUMMER)) AS FRACTIENR,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_uitslagen uits,
            kgc_brieftypes brty,
            --            kgc_medewerkers mede,
            --            kgc_medewerkers mede2,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_onderzoekswijzen onwy,
            bas_metingen meti,
            bas_fracties frac,
            kgc_notities noti,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC5_VW'
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND UITS.ONDE_ID = ONDE.ONDE_ID
            AND UITS.BRTY_ID = BRTY.BRTY_ID
            --            AND UITS.MEDE_ID = MEDE.MEDE_ID(+)
            --            AND UITS.MEDE_ID2 = MEDE2.MEDE_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.FRAC_ID = FRAC.FRAC_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND (NOTI.ENTITEIT = 'ONDE' OR NOTI.ENTITEIT IS NULL)
            AND ONDE.AFGEROND = 'N'
            AND UITS.MEDE_ID IS NULL
            AND UITS.MEDE_ID2 IS NULL
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            flfa.mede_id,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            BRTY.CODE,
            UITS.CREATION_DATE,
            --            MEDE.CODE,
            --            MEDE2.CODE,
            ONWY.OMSCHRIJVING,
            ONDE.ONDERZOEKSTYPE,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC
*/;

/
QUIT
