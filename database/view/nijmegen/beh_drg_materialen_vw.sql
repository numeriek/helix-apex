CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_MATERIALEN_VW" ("MATE_ID", "KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "MATE_CODE", "OMSCHRIJVING", "DRAGON_MATERIAAL", "CONDITIES_VERZENDING", "CONDITIES_VERZENDING_SPECIFIEK", "MATERIAAL_INFO", "MATERIAAL_SORTEER_VOLGORDE") AS
  SELECT mate.mate_id,
          kafd.kafd_id,
          kafd.code,
          ongr.ongr_id,
          ongr.code,
          mate.code,
          mate.omschrijving,
          kgc_attr_00.
           waarde ('KGC_MATERIALEN', 'DRAGON_MATERIAAL', mate.mate_id)
             dragon_materiaal,
          kgc_sypa_00.standaard_waarde ('BEH_DRG_CONDITIES_VERZENDING',
                                        kafd.kafd_id,
                                        ongr.ongr_id,
                                        NULL)
             condities_verzending, -- Algemene verzend condities per afdeling, per groep
          -- toevoeging marzena om verzend condities per materiaal te kunnen vastleggen en bepalen --> Alleen condities die per materiaal gelden
          CASE
             WHEN kgc_attr_00.
                   waarde ('KGC_MATERIALEN',
                           'DRAGON_MATERIAAL_VERZEND_COND',
                           mate.mate_id)
                     IS NOT NULL
             THEN
                kgc_attr_00.
                 waarde ('KGC_MATERIALEN', 'DRAGON_MATERIAAL', mate.mate_id)
                || ': '
                || kgc_attr_00.
                    waarde ('KGC_MATERIALEN',
                            'DRAGON_MATERIAAL_VERZEND_COND',
                            mate.mate_id)
             ELSE
                ''
          END
             condities_verzending_specifiek,
          -- toevoeging marzena om materiaal info per materiaal te kunnen vastleggen en bepalen
          kgc_attr_00.
           waarde ('KGC_MATERIALEN', 'DRAGON_MATERIAAL_INFO', mate.mate_id)
             materiaal_info,
          TO_NUMBER (
             kgc_attr_00.
              waarde ('KGC_MATERIALEN',
                      'DRAGON_MATERIAAL_VOLGORDE',
                      mate.mate_id))
             dragon_materiaal_volgorde
     FROM kgc_materialen mate,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr
    WHERE mate.kafd_id = kafd.kafd_id AND kafd.kafd_id = ongr.kafd_id
          AND kgc_attr_00.
               waarde ('KGC_ONDERZOEKSGROEPEN',
                       'DRAGON_ONDE_SOORT',
                       ongr.ongr_id)
                 IS NOT NULL
          AND kgc_attr_00.
               waarde ('KGC_MATERIALEN', 'DRAGON_MATERIAAL', mate_id)
                 IS NOT NULL
          -- toevoeging marzena om materialen per groep te kunnen vastleggen en bepalen
          AND (mate.mate_id, ongr.code) IN
                 (SELECT mid, (COLUMN_VALUE).getstringval () groep
                    FROM (SELECT mate2.mate_id mid,
                                 kgc_attr_00.
                                  waarde ('KGC_MATERIALEN',
                                          'DRAGON_MATERIAAL_GROEP',
                                          mate2.mate_id)
                                    groep
                            FROM kgc_materialen mate2
                           WHERE kgc_attr_00.
                                  waarde ('KGC_MATERIALEN',
                                          'DRAGON_MATERIAAL_GROEP',
                                          mate_id)
                                    IS NOT NULL),
                         XMLTABLE (groep))

 order by
 ongr_id,
 TO_NUMBER(kgc_attr_00.waarde ('KGC_MATERIALEN', 'DRAGON_MATERIAAL_VOLGORDE', mate.mate_id));

/
QUIT
