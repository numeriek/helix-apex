CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_NIPT_WLST_BESTAND_VW" ("TEKST", "WLST_ID", "WERKLIJST_NUMMER", "CREATION_DATE") AS
  SELECT "TEKST",
          "WLST_ID",
          "WERKLIJST_NUMMER",
          "CREATION_DATE"
     FROM (WITH NIPT
     AS (
SELECT        -- voorselectie van de juiste werklijsten
                              -- selecteer EXA werklijsten en de daarbij behorende LIIB werklijsten
                              -- op LIB werklijst staan voortesten van EXA stoftesten
                              -- op EXA werklijst staan samenstelling stoftesten
                              WLST.WLST_ID AS WLST_ID_LIB,
                              WLST.WERKLIJST_NUMMER AS WLST_LIB,
                              WLST1.WLST_ID AS WLST_ID_EXA,
                              WLST1.WERKLIJST_NUMMER AS WLST_EXA,
                              WLST1.CREATION_DATE AS CREATION_DATE_EXA,
                              MEET1.MEET_ID AS MEET_ID_EXA,
                              METI.FRAC_ID AS FRAC_ID,
                              sasi.SASI_BC as SASI_BC
                         FROM bas_metingen meti,                -- voor meting
                              bas_meetwaarden meet, -- voor stoftest van LIB voortest
                              kgc_stoftesten stof, -- voor stoftest gegevens van LIB voortest
                              bas_meetwaarde_details mdet, -- voor meetwaarde details van LIB voortest
                              kgc_werklijsten wlst, -- voor werklijst van LIB voorstesten
                              kgc_werklijst_items wlit, -- voor werklijstitems van LIB voortesten
                              bas_meetwaarden meet1, -- voor stoftest van EXA samenstelling test
                              kgc_werklijsten wlst1, -- voor werklijst van EXA samenstellingen testen
                              kgc_werklijst_items wlit1, -- voor werklijstitems van EXA samenstelling testen
                              kgc_stoftesten stof1, -- voor stoftest gegevens van EXA samenstelling stoftest
                              kgc_tray_gebruik trge1, -- voor tray gegevens van EXA werklijsten
                              (select WLST2.WLST_ID, FRAC2.FRAC_ID, MDET2.WAARDE as SASI_BC -- selectie voor SASI-SEQ werklijsten
                              from bas_fracties frac2,
                                        bas_metingen meti2,                           -- voor SASI-SEQ meting
                                        bas_meetwaarden meet2,     -- voor stoftest van SASI-SEQ stoftest
                                        kgc_stoftesten stof2, -- voor stoftest gegevens van SASI-SEQ stoftest
                                        bas_meetwaarde_details mdet2, -- voor meetwaarde details van SASI-SEQ stoftest
                                        kgc_werklijsten wlst2, -- voor werklijst van SASI-SEQ stoftesten
                                        kgc_werklijst_items wlit2 -- voor werklijstitems van SASI-SEQ stoftesten
                                where   FRAC2.FRAC_ID = METI2.FRAC_ID
                                    AND METI2.METI_ID = MEET2.METI_ID
                                    AND MEET2.STOF_ID = STOF2.STOF_ID
                                    AND MEET2.MEET_ID =  MDET2.MEET_ID
                                    AND MEET2.MEET_ID = WLIT2.MEET_ID
                                    AND WLIT2.WLST_ID = WLST2.WLST_ID
                                    AND MEET2.AFGEROND = 'J'
                                    AND STOF2.CODE = 'SASI_SEQ'
                                    AND NOT EXISTS (SELECT 1 FROM BAS_MEETWAARDEN meet3, kgc_werklijsten wlst3, kgc_werklijst_items wlit3 where WLST3.WLST_ID = WLST2.WLST_ID AND WLIT3.WLST_ID = WLST3.WLST_ID AND MEET3.MEET_ID = WLIT3.MEET_ID and MEET3.AFGEROND = 'N'  )
                                    )  SASI
                        WHERE           -- selectie/joins voor LIB werklijsten
                             METI .METI_ID = MEET.METI_ID
                              AND MEET.STOF_ID = STOF.STOF_ID
                              AND MEET.MEET_ID = MDET.MEET_ID
                              AND MEET.MEET_ID = WLIT.MEET_ID
                              AND WLIT.WLST_ID = WLST.WLST_ID
                              AND STOF.MEET_REKEN = 'V'
                              AND UPPER (MDET.PROMPT) = 'BC-TAG'
                              -- selectie/joins voor EXA werklijsten
                              AND METI.METI_ID = MEET1.METI_ID
                              AND MEET1.MEET_ID = WLIT1.MEET_ID
                              AND WLIT1.WLST_ID = WLST1.WLST_ID
                              AND MEET1.STOF_ID = STOF1.STOF_ID
                              AND TRGE1.WERKLIJST_NUMMER =
                                     WLST1.WERKLIJST_NUMMER
                              AND TRGE1.GEFIXEERD = 'J'
                              AND STOF1.OMSCHRIJVING LIKE 'EXA_NIPT%'
                              AND STOF1.MEET_REKEN = 'S'
                              -- selectie voor SASI_barcode
                              AND SASI.FRAC_ID = METI.FRAC_ID
                              -- selectie van EXA werklijsten waarvoor geen werklijst file aangemaakt is en verstuurd
                              AND WLST1.WLST_ID NOT IN
                                     (SELECT BERI.ENTITEIT_ID
                                        FROM beh_berichten beri, beh_bericht_types bety
                                        WHERE  BERI.BETY_ID = BETY.BETY_ID
                                             AND BETY.CODE = 'NIPT_WLST'
                                             AND BERI.VERZONDEN = 'J')
                              -- select dat er daadwerkelijk bij elke EXA _S stoftest een waarde bij LIB stoftest geregistreerd en afgerond is
                              AND NOT EXISTS
                                         (SELECT 1
                                            FROM (SELECT WLIT2.WLST_ID,
                                                         MEET2.METI_ID
                                                    FROM kgc_werklijst_items wlit2,
                                                         bas_meetwaarden meet2
                                                   WHERE MEET2.MEET_REKEN ='S'
                                                         AND WLIT2.MEET_ID =MEET2.MEET_ID) WEXA,
                                                 (SELECT WLIT2.WLST_ID,
                                                         MEET2.METI_ID
                                                    FROM kgc_werklijst_items wlit2,
                                                         bas_meetwaarden meet2,
                                                         bas_meetwaarde_details mdet2
                                                   WHERE MEET2.MEET_ID = MDET2.MEET_ID
                                                         AND UPPER (MDET2.PROMPT) ='BC-TAG'
                                                         AND (MEET2.AFGEROND ='N' OR MDET2.WAARDE IS NULL)
                                                         AND WLIT2.MEET_ID = MEET2.MEET_ID) WLIB
                                           WHERE WEXA.METI_ID = WLIB.METI_ID
                                                 AND wexa.wlst_id = wlst.wlst_id)
           )
           -- Daadwerkelijke selectie van de gegevens
           SELECT *
             FROM (SELECT -- selectie van testen van standaard fracties en de daarbij behorende meetwaarde details
                         DISTINCT
                             MEET.MEET_ID
                          || ','
                          || REPLACE (STAN.STANDAARDFRACTIENUMMER, '-')
                          || ','
                          || ''
                          || ','
                          || ''
                          || ','
                          || MDET.WAARDE
                          || ','
                          || ''
                          || ','
                          || NIPT.WLST_EXA
                          ||','
                            AS TEKST,
                          NIPT.WLST_ID_EXA AS WLST_ID,
                          NIPT.WLST_EXA WERKLIJST_NUMMER,
                          NIPT.CREATION_DATE_EXA AS CREATION_DATE
                     FROM bas_metingen meti,                    -- voor meting
                          bas_meetwaarden meet, -- voor stoftest van LIB voortest van standaard fractie
                          kgc_stoftesten stof, -- voor stoftest gegevens van LIB voortest van standaard fractie
                          bas_meetwaarde_details mdet, -- voor meetwaarde details van LIB voortest van standaard fractie
                          bas_standaardfracties stan, -- voor standaard fractie gegevens
                          kgc_werklijsten wlst, -- voor werklijst van LIB voortesten
                          kgc_werklijst_items wlit, -- voor werklijstitems van LIB voortesten
                          NIPT NIPT       -- voor werklijst van LIB voortesten
                    WHERE     METI.METI_ID = MEET.METI_ID
                          AND MEET.STOF_ID = stof.stof_id
                          AND MEET.MEET_ID = MDET.MEET_ID
                          AND MEET.MEET_ID = WLIT.MEET_ID
                          AND WLIT.WLST_ID = WLST.WLST_ID
                          AND WLST.WLST_ID = NIPT.WLST_ID_LIB
                          AND METI.STAN_ID = STAN.STAN_ID
                          AND METI.STAN_ID IN
                                 (SELECT metist.stan_id
                                    FROM bas_metingen metist,
                                         kgc_werklijst_items wlitst
                                   WHERE WLITST.METI_ID = METIST.METI_ID
                                         AND WLITST.WLST_ID =
                                                NIPT.WLST_ID_EXA)
                          AND STOF.MEET_REKEN = 'V'
                          AND UPPER(MDET.PROMPT) = 'BC-TAG')
           UNION ALL
           (SELECT -- selectie van EXA testen en de daarbij behorende meetwaarde details van LIB voortesten
                  WLIT1.MEET_ID
                   || ','
                   || REPLACE (FRAC.FRACTIENUMMER, '-')
                   || REPLACE (ONDE.ONDERZOEKNR, '-')
                   || ','
                   || ''
                   || ','
                   || ''
                   || ','
                   || MDET.WAARDE
                   || ','
                   || ''
                   || ','
                   || WLST1.WERKLIJST_NUMMER
                   ||','
                   || NIPT.SASI_BC--SASI.SASI_BC
                      AS TEKST,
                   WLST1.WLST_ID AS WLST_ID,
                   WLST1.WERKLIJST_NUMMER AS WERKLIJST_NUMMER,
                   WLST1.CREATION_DATE AS CREATION_DATE
              FROM bas_metingen meti,                           -- voor meting
                   bas_meetwaarden meet,     -- voor stoftest van LIB voortest
                   kgc_stoftesten stof, -- voor stoftest gegevens van LIB voortest
                   bas_meetwaarde_details mdet, -- voor meetwaarde details van LIB voortest
                   kgc_werklijsten wlst, -- voor werklijst van LIB voorstesten
                   kgc_werklijst_items wlit, -- voor werklijstitems van LIB voortesten
                   bas_meetwaarden meet1, -- voor stoftest van EXA samenstelling test
                   kgc_werklijsten wlst1, -- voor werklijst van EXA samenstellingen testen
                   kgc_werklijst_items wlit1, -- voor werklijstitems van EXA samenstelling testen
                   bas_fracties frac,                 -- voor fractie gegevens
                   kgc_onderzoeken onde,            -- voor onderzoek gegevens
                   nipt nipt                    -- voor EXA werklijst gegevens
             WHERE                       -- selectie/joins voor LIB sotftesten
                  METI .METI_ID = MEET.METI_ID
                   AND MEET.MEET_ID = MDET.MEET_ID
                   AND UPPER (MDET.PROMPT) = ('BC-TAG')
                   AND MEET.STOF_ID = STOF.STOF_ID
                   AND STOF.MEET_REKEN = 'V'
                   -- selectie/joins voor LIB werklijsten
                   AND MEET.MEET_ID = WLIT.MEET_ID
                   AND WLIT.WLST_ID = WLST.WLST_ID
                   -- selectie/joins voor EXA werklijsten
                   AND METI.METI_ID = MEET1.METI_ID
                   AND MEET1.MEET_ID = WLIT1.MEET_ID
                   AND WLIT1.WLST_ID = WLST1.WLST_ID
                   AND WLST1.WLST_ID = NIPT.WLST_ID_EXA
                   AND WLIT1.MEET_ID = NIPT.MEET_ID_EXA
                   -- selectie/joins voor fractie en onderzoek gegevens
                   AND METI.FRAC_ID = FRAC.FRAC_ID
                   AND METI.ONDE_ID = ONDE.ONDE_ID
))


/*
   SELECT "TEKST",
          "WLST_ID",
          "WERKLIJST_NUMMER",
          "CREATION_DATE",
          "LOG_TEKST"
     FROM (WITH NIPT
                   AS (SELECT        -- voorselectie van de juiste werklijsten
                              -- selecteer EXA werklijsten en de daarbij behorende LIIB werklijsten
                              -- op LIB werklijst staan voortesten van EXA stoftesten
                              -- op EXA werklijst staan samenstelling stoftesten
                              WLST.WLST_ID AS WLST_ID_LIB,
                              WLST.WERKLIJST_NUMMER AS WLST_LIB,
                              WLST1.WLST_ID AS WLST_ID_EXA,
                              WLST1.WERKLIJST_NUMMER AS WLST_EXA,
                              WLST1.CREATION_DATE AS CREATION_DATE_EXA,
                              MEET1.MEET_ID AS MEET_ID_EXA
                         FROM bas_metingen meti,                -- voor meting
                              bas_meetwaarden meet, -- voor stoftest van LIB voortest
                              kgc_stoftesten stof, -- voor stoftest gegevens van LIB voortest
                              bas_meetwaarde_details mdet, -- voor meetwaarde details van LIB voortest
                              kgc_werklijsten wlst, -- voor werklijst van LIB voorstesten
                              kgc_werklijst_items wlit, -- voor werklijstitems van LIB voortesten
                              bas_meetwaarden meet1, -- voor stoftest van EXA samenstelling test
                              kgc_werklijsten wlst1, -- voor werklijst van EXA samenstellingen testen
                              kgc_werklijst_items wlit1, -- voor werklijstitems van EXA samenstelling testen
                              kgc_stoftesten stof1, -- voor stoftest gegevens van EXA samenstelling stoftest
                              kgc_tray_gebruik trge1 -- voor tray gegevens van EXA werklijsten
                        WHERE           -- selectie/joins voor LIB werklijsten
                             METI .METI_ID = MEET.METI_ID
                              AND MEET.STOF_ID = STOF.STOF_ID
                              AND MEET.MEET_ID = MDET.MEET_ID
                              AND MEET.MEET_ID = WLIT.MEET_ID
                              AND WLIT.WLST_ID = WLST.WLST_ID
                              AND STOF.MEET_REKEN = 'V'
                              AND UPPER (MDET.PROMPT) = 'BC-TAG'
                              -- selectie/joins voor EXA werklijsten
                              AND METI.METI_ID = MEET1.METI_ID
                              AND MEET1.MEET_ID = WLIT1.MEET_ID
                              AND WLIT1.WLST_ID = WLST1.WLST_ID
                              AND MEET1.STOF_ID = STOF1.STOF_ID
                              AND TRGE1.WERKLIJST_NUMMER =
                                     WLST1.WERKLIJST_NUMMER
                              AND TRGE1.GEFIXEERD = 'J'
                              AND STOF1.OMSCHRIJVING LIKE 'EXA_NIPT%'
                              AND STOF1.MEET_REKEN = 'S'
                              -- selectie van EXA werklijsten waarvoor geen werklijst file aangemaakt is en verstuurd
                              AND WLST1.WLST_ID NOT IN
                                     (SELECT BERI.ENTITEIT_ID
                                        FROM beh_berichten beri,
                                             beh_bericht_types bety
                                       WHERE     BERI.BETY_ID = BETY.BETY_ID
                                             AND BETY.CODE = 'NIPT_WLST'
                                             AND BERI.VERZONDEN = 'J')
                              -- select dat er daadwerkelijk bij elke EXA _S stoftest een waarde bij LIB stoftest geregistreerd en afgerond is
                              AND NOT EXISTS
                                         (SELECT 1
                                            FROM (SELECT WLIT2.WLST_ID,
                                                         MEET2.METI_ID
                                                    FROM kgc_werklijst_items wlit2,
                                                         bas_meetwaarden meet2
                                                   WHERE MEET2.MEET_REKEN =
                                                            'S'
                                                         AND WLIT2.MEET_ID =
                                                                MEET2.MEET_ID) WEXA,
                                                 (SELECT WLIT2.WLST_ID,
                                                         MEET2.METI_ID
                                                    FROM kgc_werklijst_items wlit2,
                                                         bas_meetwaarden meet2,
                                                         bas_meetwaarde_details mdet2
                                                   WHERE MEET2.MEET_ID =
                                                            MDET2.MEET_ID
                                                         AND UPPER (
                                                                MDET2.PROMPT) =
                                                                'BC-TAG'
                                                         AND (MEET2.AFGEROND =
                                                                 'N'
                                                              OR MDET2.WAARDE
                                                                    IS NULL)
                                                         AND WLIT2.MEET_ID =
                                                                MEET2.MEET_ID) WLIB
                                           WHERE WEXA.METI_ID = WLIB.METI_ID
                                                 AND wexa.wlst_id =
                                                        wlst.wlst_id))
           -- Daadwerkelijke selectie van de gegevens
           SELECT *
             FROM (SELECT -- selectie van testen van standaard fracties en de daarbij behorende meetwaarde details
                         DISTINCT
                             MEET.MEET_ID
                          || ','
                          || REPLACE (STAN.STANDAARDFRACTIENUMMER, '-')
                          || ','
                          || ''
                          || ','
                          || ''
                          || ','
                          || MDET.WAARDE
                          || ','
                          || ''
                          || ','
                          || NIPT.WLST_EXA
                             AS TEKST,
                          NIPT.WLST_ID_EXA AS WLST_ID,
                          NIPT.WLST_EXA WERKLIJST_NUMMER,
                          NIPT.CREATION_DATE_EXA AS CREATION_DATE,
                          '.' AS LOG_TEKST
                     FROM bas_metingen meti,                    -- voor meting
                          bas_meetwaarden meet, -- voor stoftest van LIB voortest van standaard fractie
                          kgc_stoftesten stof, -- voor stoftest gegevens van LIB voortest van standaard fractie
                          bas_meetwaarde_details mdet, -- voor meetwaarde details van LIB voortest van standaard fractie
                          bas_standaardfracties stan, -- voor standaard fractie gegevens
                          kgc_werklijsten wlst, -- voor werklijst van LIB voortesten
                          kgc_werklijst_items wlit, -- voor werklijstitems van LIB voortesten
                          NIPT NIPT       -- voor werklijst van LIB voortesten
                    WHERE     METI.METI_ID = MEET.METI_ID
                          AND MEET.STOF_ID = stof.stof_id
                          AND MEET.MEET_ID = MDET.MEET_ID
                          AND MEET.MEET_ID = WLIT.MEET_ID
                          AND WLIT.WLST_ID = WLST.WLST_ID
                          AND WLST.WLST_ID = NIPT.WLST_ID_LIB
                          AND METI.STAN_ID = STAN.STAN_ID
                          AND METI.STAN_ID IN
                                 (SELECT metist.stan_id
                                    FROM bas_metingen metist,
                                         kgc_werklijst_items wlitst
                                   WHERE WLITST.METI_ID = METIST.METI_ID
                                         AND WLITST.WLST_ID =
                                                NIPT.WLST_ID_EXA)
                          AND STOF.MEET_REKEN = 'V'
                          AND MDET.PROMPT = 'BC-Tag')
           UNION ALL
           (SELECT -- selectie van EXA testen en de daarbij behorende meetwaarde details van LIB voortesten
                  WLIT1.MEET_ID
                   || ','
                   || REPLACE (FRAC.FRACTIENUMMER, '-')
                   || REPLACE (ONDE.ONDERZOEKNR, '-')
                   || ','
                   || ''
                   || ','
                   || ''
                   || ','
                   || MDET.WAARDE
                   || ','
                   || ''
                   || ','
                   || WLST1.WERKLIJST_NUMMER
                      AS TEKST,
                   WLST1.WLST_ID AS WLST_ID,
                   WLST1.WERKLIJST_NUMMER AS WERKLIJST_NUMMER,
                   WLST1.CREATION_DATE AS CREATION_DATE,
                   '.' AS LOG_TEKST
              FROM bas_metingen meti,                           -- voor meting
                   bas_meetwaarden meet,     -- voor stoftest van LIB voortest
                   kgc_stoftesten stof, -- voor stoftest gegevens van LIB voortest
                   bas_meetwaarde_details mdet, -- voor meetwaarde details van LIB voortest
                   kgc_werklijsten wlst, -- voor werklijst van LIB voorstesten
                   kgc_werklijst_items wlit, -- voor werklijstitems van LIB voortesten
                   bas_meetwaarden meet1, -- voor stoftest van EXA samenstelling test
                   kgc_werklijsten wlst1, -- voor werklijst van EXA samenstellingen testen
                   kgc_werklijst_items wlit1, -- voor werklijstitems van EXA samenstelling testen
                   bas_fracties frac,                 -- voor fractie gegevens
                   kgc_onderzoeken onde,            -- voor onderzoek gegevens
                   nipt nipt                    -- voor EXA werklijst gegevens
             WHERE                       -- selectie/joins voor LIB sotftesten
                  METI .METI_ID = MEET.METI_ID
                   AND MEET.MEET_ID = MDET.MEET_ID
                   AND UPPER (MDET.PROMPT) = ('BC-TAG')
                   AND MEET.STOF_ID = STOF.STOF_ID
                   AND STOF.MEET_REKEN = 'V'
                   -- selectie/joins voor LIB werklijsten
                   AND MEET.MEET_ID = WLIT.MEET_ID
                   AND WLIT.WLST_ID = WLST.WLST_ID
                   -- selectie/joins voor EXA werklijsten
                   AND METI.METI_ID = MEET1.METI_ID
                   AND MEET1.MEET_ID = WLIT1.MEET_ID
                   AND WLIT1.WLST_ID = WLST1.WLST_ID
                   AND WLST1.WLST_ID = NIPT.WLST_ID_EXA
                   AND WLIT1.MEET_ID = NIPT.MEET_ID_EXA
                   -- selectie/joins voor fractie en onderzoek gegevens
                   AND METI.FRAC_ID = FRAC.FRAC_ID
                   AND METI.ONDE_ID = ONDE.ONDE_ID))
*/;

/
QUIT
