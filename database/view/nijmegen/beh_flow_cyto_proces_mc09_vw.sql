CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC09_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "AUTORISATIEDATUM", "UITSLAG_CODE", "OMSCHRIJVING", "WIJZE", "ONDE_TYPE", "VERBORGEN_FLOW_ID") AS
  SELECT                                  -- Overzicht "Te printen brieven"
           '#79A8A8' VERBORGEN_KLEUR,
            '' VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            ONDE.DATUM_AUTORISATIE AS AUTORISATIEDATUM,
            ONUI.CODE AS UITSLAG_CODE,
            ONUI.OMSCHRIJVING,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_uitslagcodes onui,
            kgc_uitslagen uits,
            kgc_onderzoekswijzen onwy,
            --            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE                    --FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC8_VW'
                --            AND ONDE.KAFD_ID = flpr.kafd_id
                --            AND ONDE.ONDE_ID = FLPR.ONDE_ID
                ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND UITS.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONUI_ID = ONUI.ONUI_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE(+)
            AND ONDE.AFGEROND = 'J'
            AND ONUI.CODE NOT IN ('04', 'N04', '09')
            AND UITS.UITS_ID IN
                   (SELECT BEST.ENTITEIT_PK
                      FROM kgc_bestanden best,
                           KGC_BESTAND_TYPES btyp,
                           kgc_relaties rela --, kgc_attribuut_waarden atwa, KGC_ATTRIBUTEN attr
                     WHERE (BEST.RELA_ID, BEST.ENTITEIT_PK) NOT IN
                              (SELECT BRIE.RELA_ID, BRIE.UITS_ID
                                 FROM kgc_brieven brie
                                WHERE BRIE.UITS_ID = BEST.ENTITEIT_PK)
                           AND BEST.RELA_ID IS NOT NULL
                           AND BEST.ENTITEIT_CODE = 'UITS'
                           AND BEST.BTYP_ID = BTYP.BTYP_ID
                           AND BTYP.IND_REGISTREREN_ALS_BRIEF = 'J'
                           AND BEST.RELA_ID = RELA.RELA_ID
                           AND UPPER (RELA.RELATIE_TYPE) IN ('EX', 'HA')
                           --and BEST.RELA_ID not in (select ATWA.ID from kgc_attribuut_waarden atwa, KGC_ATTRIBUTEN attr where ATWA.ATTR_ID = ATTR.ATTR_ID and ATTR.CODE = 'ZORGMAIL')
                           AND NOT EXISTS
                                      (SELECT 1
                                         FROM kgc_attribuut_waarden atwa,
                                              KGC_ATTRIBUTEN attr
                                        WHERE     ATWA.ATTR_ID = ATTR.ATTR_ID
                                              AND ATTR.CODE = 'ZORGMAIL'
                                              AND ATWA.ID = BEST.RELA_ID))
   ORDER BY ONDE.SPOED ASC,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
