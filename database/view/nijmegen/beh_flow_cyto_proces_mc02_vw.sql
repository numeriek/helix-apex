CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC02_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "MONSTERNUMMER", "NOTITIE", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED (+) = 'N'
                    AND NOTI.ENTITEIT = 'ONDE')
     SELECT -- Onderzoeken met monsters zonder kweekfracties of met fracties bij BB_CLL onderzoeken die nog niet Opgewerkt zijn
           '#79A8A8' VERBORGEN_KLEUR,
            '' VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (ONDE.ONDE_ID),
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            MONS.MONSTERNUMMER,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_monsters onmo,
            kgc_monsters mons,
            kgc_onderzoeksgroepen ongr,
            W_KGC_NOTITIES noti,
            beh_flow_favorieten flfa
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.ONDE_ID = NOTI.ID(+)
--            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND MONS.MONS_ID = ONMO.MONS_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND NOT EXISTS
                       (SELECT FRAC.MONS_ID -- voor onderzoeken met BB_CCL indicatie mag geen fracties status O hebben
                          FROM bas_fracties frac,
                               kgc_indicatie_teksten inte,
                               kgc_onderzoek_indicaties onin
                         WHERE     FRAC.STATUS = 'O'
                               AND ONDE.ONDE_ID = ONMO.ONDE_ID
                               AND ONMO.MONS_ID = MONS.MONS_ID
                               AND MONS.MONS_ID = FRAC.MONS_ID
                               AND ONIN.ONDE_ID = ONDE.ONDE_ID
                               AND ONIN.INDI_ID = INTE.INDI_ID
                               AND INTE.CODE = 'BB_CLL')
            AND NOT EXISTS
                       (SELECT FRAC.MONS_ID
                          FROM KGC_KWEEKFRACTIES KWFR, BAS_FRACTIES FRAC
                         WHERE KWFR.FRAC_ID = FRAC.FRAC_ID
                               AND FRAC.MONS_ID = ONMO.MONS_ID)
   GROUP BY ONDE.ONDERZOEKNR,
            ONDE.ONDE_ID,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            MONS.MONSTERNUMMER,
            NOTI.OMSCHRIJVING,
            NOTI.GEREED,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
