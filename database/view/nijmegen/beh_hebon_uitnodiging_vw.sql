CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_HEBON_UITNODIGING_VW" ("KAFD_ID", "KAFD_CODE", "PERS_ID", "PERS_AANSPREKEN", "PERS_ADRES", "PERS_POSTCODE", "PERS_WOONPLAATS", "PERS_GESLACHT", "ONDE_ID", "RELA_ID", "ZISNR", "ONDERZOEKNR", "DATUM_BINNEN", "ONDERZOEKSTYPE", "RELA_AANSPREKEN", "LISZCODE", "AFGEROND", "HEBON_UN", "HEBON_PW", "VANDAAG") AS
  SELECT													-- onderzoeken van de persoon:
			kafd.kafd_id,
			 kafd.code,
			 pers.pers_id,
				 DECODE (pers.geslacht,  'V', 'Mevrouw',  'M', 'Meneer',  NULL)
			 || ' '
			 || pers.aanspreken
				 aanspreken,
			 pers.adres,
			 pers.postcode,
			 pers.woonplaats,
			 pers.geslacht,
			 onde.onde_id,
			 rela.rela_id,
			 pers.zisnr,
			 onde.onderzoeknr,
			 onde.datum_binnen,
			 onde.onderzoekstype,
			 rela.aanspreken,
			 rela.liszcode,
			 onde.afgerond,
			 hewa.hebon_un,
			 hewa.hebon_pw,
			 TO_CHAR (SYSDATE, 'dd month yyyy')
	  FROM kgc_kgc_afdelingen kafd,
			 kgc_onderzoeksgroepen ongr,
			 kgc_relaties rela,
			 kgc_personen pers,
			 kgc_onderzoeken onde,
			 beh_hebon_waarden hewa
	 WHERE	  onde.rela_id = rela.rela_id
			 AND onde.kafd_id = kafd.kafd_id
			 AND onde.ongr_id = ongr.ongr_id
			 AND onde.pers_id = pers.pers_id
			 AND onde.pers_id = hewa.pers_id
			 AND onde.onderzoekstype IN ('D', 'C')
			 --  AND onde.onderzoeknr = 'R14-05621'
			 AND onde.onderzoeknr IN ('R15-01187');

/
QUIT
