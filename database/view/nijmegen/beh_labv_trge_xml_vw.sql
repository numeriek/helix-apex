CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_LABV_TRGE_XML_VW" ("TRGE_ID", "TRGE_XML") AS
  SELECT trge.trge_id, XMLElement("kgc_tray_gebruik",
                  XMLElement("trge", XMLAttributes(trge.trge_id AS "id"),
                            XMLElement("worklist_number", trge.werklijst_nummer),
                            XMLElement("trty_code", trty.code),
                            XMLElement("trty_description", trty.omschrijving),
                            XMLElement("znr",mede.vms_username),
                            XMLElement("tech_code",tech.code),
                            XMLElement("tech_description",tech.omschrijving),
                            XMLElement("kgc_tray_vulling",
                            (select (
                                XMLAgg(XMLElement("trvu", XMLAttributes(trvu.verborgen_trvu_id AS "id"),
                                  XMLElement("sample_name", trvu.sample_name),
                                  XMLElement("fractionnr", trvu.fractionnr),
                                  XMLElement("test", trvu.test),
                                  XMLElement("sample_nummer", trvu.sample_nummer),
                                  XMLElement("investigation_id", trvu.investigation_id),
                                  XMLElement("measure_id", trvu.measure_id),
                                  XMLElement("position_x", trvu.position_x),
                                  XMLElement("position_y", trvu.position_y),
--                                  XMLElement("indication", trvu.indication),
                                  XMLElement("indication", ''),
                                  XMLElement("znr", trvu.znr)
                                )
                                  order by trvu.position_x, trvu.position_y
                                )
                            ) from beh_labv_trvu_vw trvu
                                where trge.trge_id = trvu.verborgen_trge_id(+)
                            )),

                            XMLElement("kgc_tray_interface_data",
                                (select
                                XMLAgg(XMLElement("tifd", XMLAttributes(tifd.tifd_id AS "id"),
                                  XMLElement("meta_type", tifd.meta_type),
                                  XMLElement("meta_type_value", tifd.waarde)
                                )
                                )
                                from  kgc_tray_interface_data tifd
                                where trge.trge_id = tifd.trge_id(+)
                                )
                                )

                  )
       )
   trge_xml
    FROM kgc_tray_gebruik trge,
          kgc_tray_types trty,
          kgc_medewerkers mede,
          kgc_technieken tech
    WHERE     trge.trty_id = trty.trty_id
          AND trge.mede_id = mede.mede_id
          AND trge.tech_id = tech.tech_id(+);

/
QUIT
