CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC08_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "ONDE_TYPE", "STATUS", "PROTOCOL", "SNELHEID", "WERKLIJST", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT -- Overzicht 'Onderzoeken met openstaande protocollen (met of niet werklijst)'
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    'C', 'Controle',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            STGR.CODE AS PROTOCOL,
            DECODE (STGE.SNELHEID,  'L', 'LANGDUREND',  'K', 'KORTDUREND',  '')
               AS SNELHEID, -- deze kan alleen gebruikt worden indien de snelheid per vastgestelde gebruik hetzelfde is. Het is nu wel zo, dus het ophalen van snelheid gebeurt niet zoals het echt zou moeten, wel levert het voor nu juiste resultaten.
            CASE
               WHEN WLIT.METI_ID IS NULL THEN 'GEEN'
               ELSE TO_CHAR (wm_concat (DISTINCT (WLST.WERKLIJST_NUMMER)))
            END
               AS WERKLIJST,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            BAS_METINGEN METI,
            KGC_STOFTESTGROEPEN STGR,
            KGC_STOFTESTGROEP_GEBRUIK STGE, -- deze kan alleen gebruikt worden indien de snelheid per vastgestelde gebruik hetzelfde is. Het is nu wel zo, dus het ophalen van snelheid gebeurt niet zoals het echt zou moeten, wel levert het voor nu juiste resultaten.
            KGC_WERKLIJSTEN WLST,
            KGC_WERKLIJST_ITEMS WLIT,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 3) -- MG: dit moet nog gewijzigd worden!!!
            AND FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC08_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.STGR_ID = STGR.STGR_ID
            AND STGR.STGR_ID = STGE.STGR_ID -- deze kan alleen gebruikt worden indien de snelheid per vastgestelde gebruik hetzelfde is. Het is nu wel zo, dus het ophalen van snelheid gebeurt niet zoals het echt zou moeten, wel levert het voor nu juiste resultaten.
            AND METI.METI_ID = WLIT.METI_ID(+)
            AND WLIT.WLST_ID = WLST.WLST_ID(+)
            AND ONDE.AFGEROND = 'N'
            AND METI.AFGEROND = 'N'
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND EXISTS
                   (SELECT 1
                      FROM bas_meetwaarden meet
                     WHERE MEET.AFGEROND = 'N' AND MEET.METI_ID = METI.METI_ID)
   GROUP BY
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            ONDE.PERS_ID,
            FLFA.MEDE_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM,
            WLIT.METI_ID,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    'C', 'Controle',
                    ''),
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    ''),
            STGR.CODE,
            DECODE (STGE.SNELHEID,
                    'L', 'LANGDUREND',
                    'K', 'KORTDUREND',
                    ''),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
