CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC33_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "GROEP", "MONSTERNR", "MATERIAAL", "AANMELD_DATUM", "OPSLAG", "FRACTIENR", "ZISNR", "AANSPREKEN", "GESLACHT", "NOTITIE", "LAST_UPDATE_OP_DOOR", "VERBORGEN_NOTI_ID", "VERBORGEN_NOTI_ENTITEIT") AS
  SELECT CASE                                -- Monsters zonder onderzoeken
               WHEN (SYSDATE - mons.creation_date) < flpr.oranje
               THEN
                  '#5CAB5C'                             -- groen     '#04B404'
               ELSE
                  CASE
                     WHEN ( (SYSDATE - mons.creation_date) >= flpr.oranje
                           AND (SYSDATE - mons.creation_date) < flpr.lichtgroen)
                     THEN
                        '#AEB404'                              --  licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - mons.creation_date) < flpr.groen
                                 AND (SYSDATE - mons.creation_date) >=
                                        flpr.lichtgroen)
                           THEN
                              '#ED8B1C'                -- oranje     '#FF8000'
                           ELSE
                              '#CD5B5B'                -- rood       '#FF0000'
                        END
                  END
            END
               AS verborgen_kleur,
            CASE
               WHEN (SYSDATE - mons.creation_date) < flpr.oranje
               THEN
                  4                                                   -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - mons.creation_date) >= flpr.oranje
                           AND (SYSDATE - mons.creation_date) < flpr.lichtgroen)
                     THEN
                        3                                       -- licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - mons.creation_date) < flpr.groen
                                 AND (SYSDATE - mons.creation_date) >=
                                        flpr.lichtgroen)
                           THEN
                              2                                      -- oranje
                           ELSE
                              1                                        -- rood
                        END
                  END
            END
               AS verborgen_sortering,
            mons.monsternummer AS verborgen_telling,
            mons.pers_id AS naar_helix,
            ongr.code AS groep,
            mons.monsternummer AS monsternr,
            mate.code AS materiaal,
            mons.creation_date AS aanmeld_datum,
            mons.alleen_voor_opslag AS opslag,
            listagg (frac.fractienummer, ', ')
               WITHIN GROUP (ORDER BY frac.fractienummer)
               AS fractienr,
            pers.zisnr,
            pers.aanspreken AS aanspreken,
            pers.geslacht,
            notimons.omschrijving AS notitie,
            CASE
               WHEN notimons.last_update_date IS NOT NULL
               THEN
                     notimons.last_update_date
                  || ' door '
                  || notimons.last_updated_by
               ELSE
                  ''
            END
               AS last_update_op_door,
            notimons.noti_id AS verborgen_noti_id,
            'MONS' AS verborgen_noti_entiteit
       FROM kgc_monsters mons,
            kgc_onderzoeksgroepen ongr,
            kgc_personen pers,
            bas_fracties frac,
            kgc_materialen mate,
            kgc_notities notimons,
            beh_flow_prioriteit_vw flpr
      WHERE     flpr.view_name = 'BEH_FLOW_DNA_PROCES_MC33_VW'
            AND mons.kafd_id = flpr.kafd_id
            AND ongr.ongr_id = mons.ongr_id
            AND mons.pers_id = pers.pers_id
            AND mons.mate_id = mate.mate_id
            AND mons.mons_id = frac.mons_id(+)
            AND mons.mons_id NOT IN (SELECT onmo.mons_id
                                       FROM kgc_onderzoek_monsters onmo)
            AND mons.alleen_voor_opslag = 'N'
            AND mons.mons_id = notimons.id(+)
            AND notimons.entiteit(+) = 'MONS'
            AND notimons.GEREED (+) = 'N'
            AND mons.creation_date > TO_DATE ('13-03-2015', 'dd-mm-yyyy') --CV, 13-03-2015,  zie mantis0011056: zsm aanpassing van de flow module monsters zonder onderzoeken zodat het alleen gaat om monsters vanaf vandaag
   --AND MONS.CREATION_DATE > '01-04-2011' --tijdedelijke selectiecriterium tot ALLEEN_VOOR_OPSLAG wordt op J gezet voor oude monsters
   GROUP BY CASE                                -- Monsters zonder onderzoeken
               WHEN (SYSDATE - mons.creation_date) < flpr.oranje
               THEN
                  '#5CAB5C'                             -- groen     '#04B404'
               ELSE
                  CASE
                     WHEN ( (SYSDATE - mons.creation_date) >= flpr.oranje
                           AND (SYSDATE - mons.creation_date) <
                                  flpr.lichtgroen)
                     THEN
                        '#AEB404'                              --  licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - mons.creation_date) < flpr.groen
                                 AND (SYSDATE - mons.creation_date) >=
                                        flpr.lichtgroen)
                           THEN
                              '#ED8B1C'                -- oranje     '#FF8000'
                           ELSE
                              '#CD5B5B'                -- rood       '#FF0000'
                        END
                  END
            END,
            CASE
               WHEN (SYSDATE - mons.creation_date) < flpr.oranje
               THEN
                  4                                                   -- groen
               ELSE
                  CASE
                     WHEN ( (SYSDATE - mons.creation_date) >= flpr.oranje
                           AND (SYSDATE - mons.creation_date) <
                                  flpr.lichtgroen)
                     THEN
                        3                                       -- licht groen
                     ELSE
                        CASE
                           WHEN ( (SYSDATE - mons.creation_date) < flpr.groen
                                 AND (SYSDATE - mons.creation_date) >=
                                        flpr.lichtgroen)
                           THEN
                              2                                      -- oranje
                           ELSE
                              1                                        -- rood
                        END
                  END
            END,
            mons.monsternummer,
            mons.pers_id,
            ongr.code,
            mons.monsternummer,
            mate.code,
            mons.creation_date,
            mons.alleen_voor_opslag,
            pers.zisnr,
            pers.aanspreken,
            pers.geslacht,
            notimons.omschrijving,
            CASE
               WHEN notimons.last_update_date IS NOT NULL
               THEN
                     notimons.last_update_date
                  || ' door '
                  || notimons.last_updated_by
               ELSE
                  ''
            END,
            notimons.noti_id,
            'MONS'
   ORDER BY verborgen_sortering, mons.creation_date;

/
QUIT
