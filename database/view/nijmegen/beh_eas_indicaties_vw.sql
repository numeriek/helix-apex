CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_EAS_INDICATIES_VW" ("INDI_KAFD_ID", "INDI_INDI_ID", "INDI_CODE", "ONGR_ID", "ONGR_CODE", "DRAGON_CODE", "DRAGON_OMSCHRIJVING", "DRAGON_URL", "DRAGONW_AANB_INTL", "DRAGONW_AANB_NATL", "DRAGON_CODE_ENG", "VERRICHTINGCODE", "DRAGONW_PRIJS") AS
  SELECT indi.kafd_id indi_kafd_id,
          indi.indi_id indi_indi_id,
          indi.code indi_code,
          ingr.ongr_id,
          ongr.code,
          DECODE (
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id),
             '*', indi.code,
             kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                 'DRAGON_CODE',
                                 indi.indi_id))
             dragon_code,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGON_OMSCHRIJVING',
                              indi.indi_id)
             dragon_omschrijving,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGON_URL',
                              indi.indi_id)
             dragon_url,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGONW_AANB_INTL',
                              indi.indi_id)
             dragonw_aanb_intl,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGONW_AANB_NATL',
                              indi.indi_id)
             dragonw_aanb_natl,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGON_CODE_ENG',
                              indi.indi_id)
             dragon_code_eng,
          beh_dragonw_00.haal_ver_code_op (indi.code) verrichtingcode,
          kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                              'DRAGONW_PRIJS',
                              indi.indi_id)
             dragonw_prijs
     FROM kgc_indicatie_groepen ingr
     , kgc_indicatie_teksten indi
    , kgc_onderzoeksgroepen ongr
    WHERE ingr.ingr_id = indi.ingr_id
    and ingr.ongr_id = ongr.ongr_id
    and ingr.kafd_id = 1
          AND ( (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                     'DRAGON_CODE',
                                     indi.indi_id)
                    IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_OMSCHRIJVING',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_URL',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGONW_AANB_INTL',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGONW_AANB_NATL',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGON_CODE_ENG',
                                       indi.indi_id)
                      IS NOT NULL)
               OR (kgc_attr_00.waarde ('KGC_INDICATIE_TEKSTEN',
                                       'DRAGONW_PRIJS',
                                       indi.indi_id)
                      IS NOT NULL))
          AND indi.vervallen = 'N'
          AND ingr.vervallen = 'N';

/
QUIT
