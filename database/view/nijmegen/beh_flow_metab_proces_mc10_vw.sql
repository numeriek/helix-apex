CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC10_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "AUTORISATIEDATUM", "UITSLAG_CODE", "OMSCHRIJVING", "ONDE_TYPE", "STATUS", "VERBORGEN_FLOW_ID") AS
  SELECT                                  -- Overzicht 'Te printen brieven'
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            ONDE.DATUM_AUTORISATIE AS AUTORISATIEDATUM,
            ONUI.CODE AS UITSLAG_CODE,
            ONUI.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_uitslagcodes onui,
            kgc_uitslagen uits,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND UITS.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONUI_ID = ONUI.ONUI_ID
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND FLPR.ONDE_ID = ONDE.ONDE_ID
            AND onde.onde_id = flfa.id(+)
            AND FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC10_VW'
            AND ONDE.AFGEROND = 'J'
            AND ONUI.CODE <> '04'
            AND KAFD.CODE = 'METAB'
            AND ONDE.ONDE_ID NOT IN
                   (SELECT UITS1.ONDE_ID
                      FROM KGC_UITSLAGEN UITS1
                     WHERE UITS1.UITS_ID IN
                              (SELECT BRIE.UITS_ID
                                 FROM KGC_BRIEVEN BRIE, KGC_BRIEFTYPES BRTY
                                WHERE BRIE.BRTY_ID = BRTY.BRTY_ID
                                      AND BRTY.CODE <> 'UITSLAG_T'))
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONGR.CODE,
            ONDE.SPOED ASC,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
