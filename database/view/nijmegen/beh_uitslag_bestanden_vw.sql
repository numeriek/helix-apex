CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_UITSLAG_BESTANDEN_VW" ("PERS_ID", "PERS_AANSPREKEN", "PERS_ZISNR", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_ADRES", "PERS_ZOEKNAAM", "PERS_POSTCODE", "PERS_TELEFOON", "PERS_TELEFAX", "PERS_EMAIL", "PERS_WOONPLAATS", "FOET_ID", "ONDE_ID", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "SPOED", "ONDERZOEKNR", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "REFERENTIE", "INDICATIE", "REDEN", "AFGEROND", "STATUS", "MEDE_ID_CONTROLE", "ONGR_ID", "ONGR_CODE", "ONGR_OMSCHRIJVING", "RELA_ID", "RELA_CODE", "RELA_NAAM", "KAFD_ID", "KAFD_CODE", "KAFD_NAAM", "TAAL_ID", "TAAL_CODE", "REEDS_GEPRINT", "DATUM_AUTORISATIE", "UITS_ID", "BRTY_ID", "BRTY_CODE", "BEST_ID", "LOCATIE", "BESTANDSNAAM", "BEST_COMMENTAAR", "BRIE_ID", "AANMAAKWIJZE", "KOPIE", "BESTAND_SPECIFICATIE", "BESTANDSTYPE", "BESTANDSTYPE_OMSCHRIJVING", "STANDAARD_EXTENTIE", "RAMO_CODE", "ONUI_CODE", "ONUI_OMSCHRIJVING", "RELATIE_IS_INTERN", "RELATIE_HEEFT_ZORGMAIL", "MEDIATYPE") AS
  SELECT onde.pers_id,
          pers.aanspreken pers_aanspreken,
          pers.zisnr pers_zisnr,
          pers.achternaam pers_achternaam,
          pers.geboortedatum pers_geboortedatum,
          pers.geslacht pers_geslacht,
          kgc_adres_00.persoon (pers.pers_id, 'N') pers_adres,
          pers.zoeknaam pers_zoeknaam,
          pers.postcode pers_postcode,
          pers.telefoon pers_telefoon,
          pers.telefax pers_telefax,
          pers.email pers_email,
          pers.woonplaats,
          onde.foet_id,
          onde.onde_id,
          onde.onderzoekstype,
          onde.onderzoekswijze,
          onde.spoed,
          onde.onderzoeknr onderzoek,
          onde.datum_binnen,
          onde.geplande_einddatum,
          onde.referentie,
          kgc_indi_00.onderzoeksreden (onde.onde_id) indicatie,
          onde.omschrijving reden,
          onde.afgerond,
          onde.status,
          onde.mede_id_controle,
          onde.ongr_id,
          ongr.code ongr_code,
          ongr.omschrijving ongr_omschrijving,
          best.rela_id rela_id,
          rela.code rela_code,
          rela.aanspreken rela_aanspreken,
          onde.kafd_id,
          kafd.code kafd_code,
          kafd.naam kafd_naam,
          onde.taal_id,
          taal.code taal_code,
          beh_brie_00.uitslag_eerder_geprint (uits.uits_id,
                                              uits.brty_id,
                                              best.rela_id)
             reeds_geprint,
          NVL (onde.datum_autorisatie, uits.datum_mede_id4) datum_autorisatie,
          uits.uits_id,
          brty.brty_id,
          brty.code brty_code,
          best.best_id,
          best.locatie,
          best.bestandsnaam,
          best.commentaar,
          brie.brie_id,
          brie.aanmaakwijze aanmaakwijze,
          brie.kopie,
          best.bestand_specificatie,
          btyp.code bestandstype,
          btyp.omschrijving bestandstype_oms,
          btyp.standaard_extentie,
          ramo.code,
          onui.code onui_code,
          onui.omschrijving onui_omschrijving,
          beh_uits_00.rela_is_intern (rela.rela_id) rela_is_intern,
          beh_zoma_00.rela_heeft_zorgmail (rela.rela_id) rela_heeft_zorgmail,
          beh_uits_00.bepaal_mediatype (best.best_id) mediatype
     FROM kgc_talen taal,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_relaties rela,
          kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_onderzoek_uitslagcodes onui,
          kgc_uitslagen uits,
          kgc_brieftypes brty,
          kgc_rapport_modules ramo,
          kgc_bestanden best,
          kgc_brieven brie,
          kgc_bestand_types btyp
    WHERE     onde.pers_id = pers.pers_id
          AND best.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.onde_id = uits.onde_id
          AND uits.tekst IS NOT NULL
          AND uits.brty_id IS NOT NULL
          AND onde.taal_id = taal.taal_id(+)
          AND brty.brty_id = uits.brty_id
          AND brty.ramo_id = ramo.ramo_id(+)
          AND (NVL (onde.afgerond, 'N') = 'J'
               OR (NVL (onde.afgerond, 'N') = 'N'
                   AND ( (NVL (ramo.code, 'xxx') = 'UITSLAG'
                          AND uits.mede_id4 IS NOT NULL)
                        OR NVL (ramo.code, 'xxx') <> 'UITSLAG')))
          AND best.best_id = brie.best_id(+)
          AND best.btyp_id = btyp.btyp_id
          AND best.entiteit_code = 'UITS'
          AND best.vervallen = 'N'
          AND best.verwijderd = 'N'
          AND btyp.ind_registreren_als_brief = 'J'
          AND best.entiteit_pk = uits.uits_id
          AND onde.onui_id = onui.onui_id(+)
          AND beh_uits_00.check_ramo_code (kgc_rapp_00.rapport (
                                              beh_epd_00.zoek_uitslag_ramo_code (
                                                 uits.uits_id),
                                              NULL,
                                              NULL,
                                              NULL,
                                              'ONDE',
                                              onde.onde_id),
                                           kafd.kafd_id,
                                           ongr.ongr_id,
                                           NVL (onde.taal_id,
                                                (SELECT taal_id
                                                   FROM kgc_talen
                                                  WHERE code = 'NED'))) = 'J'
          AND ( (beh_uits_00.rela_is_intern (rela.rela_id) = 'N')
               OR (beh_uits_00.rela_is_intern (rela.rela_id) = 'J'
                   AND INSTR (beh_epd_00.ingr_code (onde.onde_id),
                              '04_G10_ND') = 0));

/
QUIT
