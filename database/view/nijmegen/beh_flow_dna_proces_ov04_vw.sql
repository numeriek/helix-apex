CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_OV04_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "AANVRAAG", "SPOED", "DAT_BINNEN", "EINDDATUM", "WIJZE", "ONDE_TYPE", "NOTITIE", "VERBORGEN_NOTI_ID") AS
  SELECT -- gemaakt voor Sec Genoom ivm ScanToPDF probleem --> soms werden gescande paginas van aanvraagformulieren weggegooid terwijl ze niet blank waren
           '#FFFFFF' AS VERBORGEN_KLEUR,
            2 AS VERBORGEN_SORTERING,
            --ONDE.ONDERZOEKNR
            BEST.BESTAND_SPECIFICATIE AS VERBORGEN_TELLING,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
               '<a href="'
            || BEST.BESTAND_SPECIFICATIE
            || '" target="_blank">Open</a>'
               AS BESTAND,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_bestanden best
      WHERE     ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK
            AND BEST.ENTITEIT_CODE = 'ONDE'
            AND BEST.VERVALLEN = 'N'
            AND BEST.VERWIJDERD = 'N'
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = 1
            AND ONGR.CODE = 'NGS'
   ORDER BY VERBORGEN_SORTERING ASC, GROEP ASC, ONDERZOEKNR ASC;

/
QUIT
