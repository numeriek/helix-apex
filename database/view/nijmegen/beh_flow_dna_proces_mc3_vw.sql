CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC3_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "VERBORGEN_IMFI_ID", "DRAGON_ID", "IMPORT_DATUM", "ZISNR", "AANSPREKEN", "AANVRAAG_TYPE", "RELATIE_CODE", "RELATIENAAM", "ONDERZOEK", "INDICODE", "OMSCHRIJVING", "MATERIAAL", "AANVRAAG", "OPMERKINGEN", "AFGEHANDELD", "LAST_UPDATE_OP_DOOR") AS
  SELECT CASE
             WHEN IMPW3.EXTERNE_ID IS NULL OR IMPW3.EXTERNE_ID = 'DNA' -- Materiaal Reeds Aanwezig + DNA (mogelijk ook aanwezig!)
             THEN
                '#0B3861'                                              -- navy
             ELSE
                CASE                           -- Openstaande Dragon aanvragen
                   WHEN (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.ORANJE
                   THEN
                      '#5CAB5C'                       -- groen     --'#04B404'
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) >= FLPR.ORANJE
                               AND (SYSDATE - IMFI.IMPORT_DATUM) <
                                      FLPR.LICHTGROEN)
                         THEN
                            '#AEB404'                          --  licht groen
                         ELSE
                            CASE
                               WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) <
                                        FLPR.GROEN
                                     AND (SYSDATE - IMFI.IMPORT_DATUM) >=
                                            FLPR.LICHTGROEN)
                               THEN
                                  '#ED8B1C'          -- oranje     --'#FF8000'
                               ELSE
                                  '#CD5B5B'          -- rood       --'#EE5252'
                            END
                      END
                END
          END
             AS VERBORGEN_KLEUR,
          CASE
             WHEN IMPW3.EXTERNE_ID IS NULL OR IMPW3.EXTERNE_ID = 'DNA' -- Materiaal Reeds Aanwezig + DNA (mogelijk ook aanwezig!)
             THEN
                0                                                     --- navy
             ELSE
                CASE
                   WHEN (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.ORANJE
                   THEN
                      4                                               -- groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) >= FLPR.ORANJE
                               AND (SYSDATE - IMFI.IMPORT_DATUM) <
                                      FLPR.LICHTGROEN)
                         THEN
                            3                                   -- licht groen
                         ELSE
                            CASE
                               WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) <
                                        FLPR.GROEN
                                     AND (SYSDATE - IMFI.IMPORT_DATUM) >=
                                            FLPR.LICHTGROEN)
                               THEN
                                  2                                  -- oranje
                               ELSE
                                  1                                    -- rood
                            END
                      END
                END
          END
             AS VERBORGEN_SORTERING,
          SUBSTR (IMFI.FILENAAM, 1, INSTR (IMFI.FILENAAM, ' ') - 1)
             AS VERBORGEN_TELLING,
          IMFI.IMFI_ID AS VERBORGEN_IMFI_ID,
          SUBSTR (IMFI.FILENAAM, 1, INSTR (IMFI.FILENAAM, ' ') - 1)
             AS DRAGON_ID,
          IMFI.IMPORT_DATUM,
          IMPE.ZISNR,
          IMPE.AANSPREKEN,
          IMPW.EXTERNE_ID AS AANVRAAG_TYPE,
          RELA.CODE AS RELATIE_CODE,
          RELA.AANSPREKEN AS RELATIENAAM,
          IMIN.IMON_ID AS ONDERZOEK,
          INTE.CODE AS INDICODE,
          INTE.OMSCHRIJVING,
          CASE
             WHEN IMPW3.EXTERNE_ID IS NULL THEN 'MATERIAAL REEDS AANWEZIG'
             ELSE IMPW3.EXTERNE_ID
          END
             CASE,
          CASE
             WHEN BEST.BESTAND_SPECIFICATIE IS NULL
             THEN
                ''
             ELSE
                   '<a href="'
                || BEST.BESTAND_SPECIFICATIE
                || '" target="_blank">Open</a>'
          END
             AS AANVRAAG,
          IMFI.OPMERKINGEN,
          IMFI.AFGEHANDELD,
          IMFI.LAST_UPDATE_DATE || ' door ' || IMFI.LAST_UPDATED_BY
     FROM KGC_IMPORT_LOCATIES imlo,
          kgc_import_files imfi,
          kgc_im_personen impe,
          kgc_im_onderzoeken imon,
          kgc_import_param_waarden impw,
          kgc_import_param_waarden impw1,
          kgc_relaties rela,
          kgc_im_onderzoek_indicaties imin,
          kgc_import_param_waarden impw2,
          kgc_indicatie_teksten inte,
          kgc_im_monsters immo,
          kgc_import_param_waarden impw3,
          kgc_bestanden best,
          BEH_FLOW_PRIORITEIT_VW flpr
    WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC3_VW'
          AND IMLO.IMLO_ID = IMFI.IMLO_ID
          AND IMPE.IMFI_ID = IMFI.IMFI_ID
          AND IMON.IMFI_ID = IMFI.IMFI_ID
          AND IMFI.IMFI_ID = IMMO.IMFI_ID
          AND IMMO.IMPW_ID_MATE = IMPW3.IMPW_ID(+)
          AND IMPW.IMPW_ID = IMON.IMPW_ID_ONDERZOEKSWIJZE
          AND IMPW1.IMPW_ID = IMON.IMPW_ID_RELA(+)
          AND IMPW1.HELIX_WAARDE = RELA.RELA_ID(+)
          AND BEST.ENTITEIT_PK = IMFI.IMFI_ID
          AND BEST.ENTITEIT_CODE = 'IMFI'
          AND IMPW1.HELIX_TABLE_DOMAIN = 'KGC_RELATIES'
          AND IMLO.DIRECTORYNAAM = 'Dragon'
          AND IMFI.AFGEHANDELD = 'N'
          AND IMIN.IMFI_ID = IMFI.IMFI_ID
          AND IMIN.IMON_ID = IMON.IMON_ID
          AND IMPW2.IMPW_ID = IMIN.IMPW_ID_INDI
          AND IMPW2.HELIX_WAARDE = INTE.INDI_ID
          AND IMPW2.HELIX_TABLE_DOMAIN = 'KGC_INDICATIE_TEKSTEN'
          AND IMFI.FILENAAM NOT LIKE '%a=6%'
          AND IMMO.IMPW_ID_KAFD <> 29596
   UNION
   SELECT CASE
             WHEN (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.ORANJE
             THEN
                '#5CAB5C'                             -- groen     --'#04B404'
             ELSE
                CASE
                   WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) >= FLPR.ORANJE
                         AND (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.LICHTGROEN)
                   THEN
                      '#AEB404'                                --  licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.GROEN
                               AND (SYSDATE - IMFI.IMPORT_DATUM) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            '#ED8B1C'                -- oranje     --'#FF8000'
                         ELSE
                            '#CD5B5B'                -- rood       --'#EE5252'
                      END
                END
          END
             AS VERBORGEN_KLEUR,
          CASE
             WHEN (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.ORANJE
             THEN
                4                                                     -- groen
             ELSE
                CASE
                   WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) >= FLPR.ORANJE
                         AND (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.LICHTGROEN)
                   THEN
                      3                                         -- licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - IMFI.IMPORT_DATUM) < FLPR.GROEN
                               AND (SYSDATE - IMFI.IMPORT_DATUM) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            2                                        -- oranje
                         ELSE
                            1                                          -- rood
                      END
                END
          END
             AS VERBORGEN_SORTERING,
          SUBSTR (IMFI.FILENAAM, 1, INSTR (IMFI.FILENAAM, ' ') - 1)
             AS VERBORGEN_TELLING,
          IMFI.IMFI_ID AS VERBORGEN_IMFI_ID,
          SUBSTR (IMFI.FILENAAM, 1, INSTR (IMFI.FILENAAM, ' ') - 1)
             AS DRAGON_ID,
          IMFI.IMPORT_DATUM,
          IMPE.ZISNR,
          IMPE.AANSPREKEN,
          'Alleen opslag',
          '',
          '',
          '',
          '',
          '',
          IMPW.EXTERNE_ID,
          CASE
             WHEN BEST.BESTAND_SPECIFICATIE IS NULL
             THEN
                ''
             ELSE
                   '<a href="'
                || BEST.BESTAND_SPECIFICATIE
                || '" target="_blank">Open</a>'
          END
             AS AANVRAAG,
          IMFI.OPMERKINGEN,
          IMFI.AFGEHANDELD,
          IMFI.LAST_UPDATE_DATE || ' door ' || IMFI.LAST_UPDATED_BY
     FROM KGC_IMPORT_LOCATIES imlo,
          kgc_import_files imfi,
          kgc_im_personen impe,
          kgc_im_monsters immo,
          kgc_import_param_waarden impw,
          kgc_bestanden best,
          BEH_FLOW_PRIORITEIT_VW flpr
    WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC3_VW'
          AND IMLO.IMLO_ID = IMFI.IMLO_ID
          AND IMPE.IMFI_ID = IMFI.IMFI_ID
          AND IMMO.IMFI_ID = IMFI.IMFI_ID
          AND IMMO.IMPW_ID_MATE = IMPW.IMPW_ID
          AND BEST.ENTITEIT_PK = IMFI.IMFI_ID
          AND BEST.ENTITEIT_CODE = 'IMFI'
          AND IMFI.IMFI_ID NOT IN (SELECT IMON.IMFI_ID
                                     FROM kgc_im_onderzoeken imon)
          AND IMLO.DIRECTORYNAAM = 'Dragon'
          AND IMFI.AFGEHANDELD = 'N'
          AND IMFI.FILENAAM NOT LIKE '%a=6%'
          AND IMMO.IMPW_ID_KAFD <> 29596
   --   UNION ALL
   --   SELECT -- als een fake record toegevoegd, anders onstaan er problemen bij het exporteren van data vanuit Flow naar excel
   --         '',
   --          0,
   --          '',
   --          -1,
   --          '',
   --          NULL,
   --          '',
   --          '',
   --          '',
   --          '',
   --          '',
   --          '',
   --          '',
   --          '',
   --          '',
   --          '',
   --          ''
   --     FROM DUAL
   ORDER BY VERBORGEN_SORTERING ASC, IMPORT_DATUM ASC, DRAGON_ID ASC;

/
QUIT
