CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_OPDRACHTEN_VW" ("VERBORGEN_NOTI_ID", "VERBORGEN_NOTI_ENTITEIT", "ID", "NOTICODE", "NOTITIE", "NOTICOMMENTAAR", "NOTIGEREED", "CREATION_DATE", "CREATED_BY", "LAST_UPDATE_DATE", "LAST_UPDATED_BY", "NAAR_HELIX", "FAMILIENR", "ARCHIEF", "FAMI_BESTANDEN", "AFDELING", "GROEP", "ONDERZOEKNR", "EINDDATUM", "BI_BU", "ONDE_BESTANDEN", "ZISNR", "ENTITEIT_OMSCHRIJVING") AS
  WITH W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
                   NOTI.ENTITEIT AS VERBORGEN_NOTI_ENTITEIT,
                   NOTI.ID AS ID,
                   NOTI.CODE AS NOTICODE,
                   NOTI.OMSCHRIJVING AS NOTITIE,
                   NOTI.COMMENTAAR AS NOTICOMMENTAAR,
                   NOTI.GEREED AS NOTIGEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED <> 'J'
                   AND (   NOTI.CODE LIKE 'SEC%'
                        OR NOTI.CODE LIKE 'SVD%'
                        OR NOTI.CODE LIKE 'PL%'
                        OR NOTI.CODE LIKE 'CGAL%'
                        OR NOTI.CODE LIKE 'FZ%'
                        OR NOTI.CODE LIKE 'LTG%'
                        OR NOTI.CODE LIKE 'MENS%'
                        OR NOTI.CODE LIKE 'MM%'
                        OR NOTI.CODE LIKE 'TML%'
                        OR NOTI.CODE LIKE 'MDL%')),
        W_KGC_BESTANDEN
        AS (SELECT BEST.ENTITEIT_PK,
                   BEST.ENTITEIT_CODE,
                   beh_bestanden_to_string (
                      CAST (
                         COLLECT (
                               '<a href="'
                            || BEST.BESTAND_SPECIFICATIE
                            || '" target="_blank">Open</a>' ORDER                                                       BY BEST.BEST_ID desc ) as beh_bestanden_link)) as bestanden
                FROM KGC_BESTANDEN BEST, KGC_BESTAND_CATEGORIEEN BCAT
                WHERE BEST.BCAT_ID = BCAT.BCAT_ID
                    AND BEST.ENTITEIT_CODE IN ('FAMI', 'ONDE')
                    AND BCAT.CODE <> ('DUMMY')
                    AND BEST.VERVALLEN = 'N'
                    AND BEST.VERWIJDERD = 'N'
                group by best.ENTITEIT_PK, BEST.ENTITEIT_CODE)
   -- NOTITIES BIJ FAMILIE
   SELECT noti."VERBORGEN_NOTI_ID",
          noti."VERBORGEN_NOTI_ENTITEIT",
          noti."ID",
          noti."NOTICODE",
          noti."NOTITIE",
          noti."NOTICOMMENTAAR",
          noti."NOTIGEREED",
          noti."CREATION_DATE",
          noti."CREATED_BY",
          noti."LAST_UPDATE_DATE",
          noti."LAST_UPDATED_BY",
          FALE.PERS_ID AS NAAR_HELIX,
          FAMI.FAMILIENUMMER,
          FAMI.ARCHIEF,
          BESTF.BESTANDEN AS FAMI_BESTANDEN,
          '' AS AFDELING,
          '' AS GROEP,
          '' AS ONDERZOEKNR,
          NULL AS EINDDATUM,
          '' AS BI_BU,
          '' AS ONDE_BESTANDEN,
          '' AS ZISNR,
          '' AS ENTITEIT_OMSCHRIJVING
     FROM kgc_families fami,
          kgc_familie_leden fale,
          W_KGC_NOTITIES noti,
          W_KGC_BESTANDEN bestf
    WHERE     FAMI.FAMI_ID = NOTI.ID
          AND NOTI.VERBORGEN_NOTI_ENTITEIT = 'FAMI'
          AND FAMI.FAMI_ID = BESTF.ENTITEIT_PK(+)
          AND BESTF.ENTITEIT_CODE(+) = 'FAMI'
          AND FAMI.FAMI_ID = FALE.FAMI_ID
          AND FALE.PERS_ID =
                 (SELECT FALE1.PERS_ID
                    FROM KGC_FAMILIE_LEDEN FALE1
                   WHERE FALE1.FAMI_ID = FAMI.FAMI_ID AND ROWNUM = 1)
   -- NOTITIES BIJ ONDERZOEKEN
   UNION ALL
   SELECT noti."VERBORGEN_NOTI_ID",
          noti."VERBORGEN_NOTI_ENTITEIT",
          noti."ID",
          noti."NOTICODE",
          noti."NOTITIE",
          noti."NOTICOMMENTAAR",
          noti."NOTIGEREED",
          noti."CREATION_DATE",
          noti."CREATED_BY",
          noti."LAST_UPDATE_DATE",
          noti."LAST_UPDATED_BY",
          PERS.PERS_ID AS NAAR_HELIX,
          FAMI.FAMILIENUMMER,
          FAMI.ARCHIEF,
          BESTF.BESTANDEN AS FAMI_BESTANDEN,
          KAFD.CODE AS AFDELING,
          ONGR.CODE AS GROEP,
          ONDE.ONDERZOEKNR,
          ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
          DECODE (beh_onde_bu (ONDE.ONDE_ID),
                  'NEDERLAND', '',
                  'BUITENLAND', 'BU')
             AS BI_BU,
          besto.BESTANDEN AS ONDE_BESTANDEN,
          PERS.ZISNR,
          '' AS ENTITEIT_OMSCHRIJVING
     FROM kgc_onderzoeken onde,
          kgc_personen pers,
          kgc_familie_leden fale,
          kgc_families fami,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd,
          W_KGC_NOTITIES noti,
          W_KGC_BESTANDEN bestf,
          W_KGC_BESTANDEN besto
    WHERE     PERS.PERS_ID = ONDE.PERS_ID
          AND ONDE.KAFD_ID = KAFD.KAFD_ID
          AND ONDE.ONGR_ID = ONGR.ONGR_ID
          AND PERS.PERS_ID = FALE.PERS_ID(+)
          AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
          AND noti.ID = ONDE.ONDE_ID
          AND noti.verborgen_noti_entiteit = 'ONDE'
          AND ONDE.ONDE_ID = besto.ENTITEIT_PK(+)
          AND besto.ENTITEIT_CODE(+) = 'ONDE'
          AND FAMI.FAMI_ID = bestf.ENTITEIT_PK(+)
          AND bestf.ENTITEIT_CODE(+) = 'FAMI'
   -- NOTITIES BIJ PERSOON
   UNION ALL
   SELECT noti."VERBORGEN_NOTI_ID",
          noti."VERBORGEN_NOTI_ENTITEIT",
          noti."ID",
          noti."NOTICODE",
          noti."NOTITIE",
          noti."NOTICOMMENTAAR",
          noti."NOTIGEREED",
          noti."CREATION_DATE",
          noti."CREATED_BY",
          noti."LAST_UPDATE_DATE",
          noti."LAST_UPDATED_BY",
          PERS.PERS_ID AS NAAR_HELIX,
          FAMI.FAMILIENUMMER,
          FAMI.ARCHIEF,
          BESTF.BESTANDEN AS FAMI_BESTANDEN,
          '' AS AFDELING,
          '' AS GROEP,
          '' AS ONDERZOEKNR,
          NULL AS EINDDATUM,
          '' AS BI_BU,
          '' AS ONDE_BESTANDEN,
          PERS.ZISNR,
          '' AS ENTITEIT_OMSCHRIJVING
     FROM kgc_personen pers,
          kgc_familie_leden fale,
          kgc_families fami,
          W_KGC_NOTITIES noti,
          W_KGC_BESTANDEN bestf
    WHERE     PERS.PERS_ID = NOTI.ID
          AND NOTI.VERBORGEN_NOTI_ENTITEIT = 'PERS'
          AND PERS.PERS_ID = FALE.PERS_ID(+)
          AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
          AND FAMI.FAMI_ID = BESTF.ENTITEIT_PK(+)
          AND BESTF.ENTITEIT_CODE(+) = 'FAMI'
   -- NOTITIES BIJ ALLE ANDERE ENTITEITEN
   UNION ALL
   SELECT noti."VERBORGEN_NOTI_ID",
          noti."VERBORGEN_NOTI_ENTITEIT",
          noti."ID",
          noti."NOTICODE",
          noti."NOTITIE",
          noti."NOTICOMMENTAAR",
          noti."NOTIGEREED",
          noti."CREATION_DATE",
          noti."CREATED_BY",
          noti."LAST_UPDATE_DATE",
          noti."LAST_UPDATED_BY",
          NULL AS NAAR_HELIX,
          '' AS FAMILIENUMMER,
          '' AS ARCHIEF,
          '' AS FAMI_BESTANDEN,
          '' AS AFDELING,
          '' AS GROEP,
          '' AS ONDERZOEKNR,
          NULL AS EINDDATUM,
          '' AS BI_BU,
          '' AS ONDE_BESTANDEN,
          '' AS ZISNR,
          kgc_noti_00.basis_omschrijving (noti.verborgen_noti_entiteit,
                                          noti.id)
             AS ENTITEIT_OMSCHRIJVING
     FROM W_KGC_NOTITIES noti
    WHERE noti.verborgen_noti_entiteit NOT IN ('ONDE', 'FAMI', 'PERS');

/
QUIT
