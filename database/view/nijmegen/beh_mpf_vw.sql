CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_MPF_VW" ("ONDERZOEKNR", "MONSTERNR", "ZISNR", "ACHTERNAAM", "AANSPREKEN", "GEBOORTEDATUM", "MATE_CODE", "DATUM_BINNEN", "MEET_ID", "FRAC_ID", "KWFR_ID", "KWFR_CODE", "KWFR_ID_PARENT", "PEILDATUM", "GEPLANDE_EINDDATUM", "OMSCHRIJVING", "ONGR_CODE", "STGR_CODE", "MPF_CLASSIFIER") AS
  SELECT onde.onderzoeknr,
          MONS.MONSTERNUMMER AS monsternr,
          UPPER (pers.zisnr) zisnr,
          pers.achternaam,
          pers.aanspreken,
          TO_CHAR (pers.geboortedatum, 'dd-mm-yyyy') geboortedatum,
          mate.code mate_code,
          TO_CHAR (onde.datum_binnen, 'dd-mm-yyyy') datum_binnen,
          meet.meet_id,
          frac.frac_id,
          kwfr.kwfr_id,
          kwfr.code kwfr_code,
          KWFR.KWFR_ID_PARENT,
          CASE
             WHEN KWFR.CREATION_DATE > MEET.CREATION_DATE
             THEN
                KWFR.CREATION_DATE
             ELSE
                MEET.CREATION_DATE
          END
             peildatum,
          onde.geplande_einddatum,
          onde.omschrijving,
          UPPER (ongr.code) ongr_code,
          UPPER (stgr.code) stgr_code,
          atwa.waarde AS mpf_classifier
     FROM kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
          kgc_kweekfracties kwfr,
          bas_metingen meti,
          bas_meetwaarden meet,
          kgc_materialen mate,
          kgc_stoftestgroepen stgr,
          kgc_stoftesten stof,
          kgc_kgc_afdelingen kafd,
          kgc_attribuut_waarden atwa,
          kgc_attributen attr,
          kgc_onderzoeksgroepen ongr
    WHERE     pers.pers_id = onde.pers_id
          AND onde.onde_id = onmo.onde_id
          AND onmo.mons_id = mons.mons_id
          AND mons.mons_id = frac.mons_id
          AND frac.frac_id = kwfr.frac_id
          AND frac.frac_id = meti.frac_id
          AND meti.meti_id = meet.meti_id
          AND meti.onde_id = onde.onde_id(+)
          AND mons.mate_id = mate.mate_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND meti.stgr_id = stgr.stgr_id
          AND meet.stof_id = stof.stof_id
          AND atwa.id = mate.mate_id
          AND attr.attr_id = atwa.attr_id
          AND UPPER (onde.afgerond) = UPPER ('N')
          AND UPPER (meti.afgerond) = UPPER ('N')
          AND UPPER (meet.afgerond) = UPPER ('N')
          AND UPPER (kafd.code) IN (UPPER ('GENOOM'), UPPER ('CYTO'))
          AND UPPER (stgr.code) LIKE UPPER ('%KAR%')
          AND UPPER (stof.code) LIKE UPPER ('%KAR%')
          AND kwfr.vervallen = 'N'
          AND attr.code = 'MPF_CLASSIFIER';

/
QUIT
