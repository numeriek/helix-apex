CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_KAFD_ONGR_TVW" ("FLOW_ID", "KAFD_ID", "ONGROEPEN") AS
  select flow_id
       , kafd_id
       , to_char (WM_CONCAT (ongr_id)) as ongroepen
  from   BEH_FLOW_KAFD_ONGR
  group by flow_id, kafd_id;

/
QUIT
