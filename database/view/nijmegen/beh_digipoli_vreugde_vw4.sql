CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DIGIPOLI_VREUGDE_VW4" ("ZWAN_ID", "INFO_GESLACHT", "SMS_TEKST", "PERS_ID", "KAFD_ID", "ONGR_ID", "KAFD_CODE", "ONGR_CODE", "TAAL_CODE", "ONDE_AFGEROND", "ZISNR", "BSN", "TELEFOON", "TELEFOON2") AS
  SELECT DISTINCT
			 zwan.zwan_id,
			 kgc_vreugdebrief_00.info_geslacht (zwan.zwan_id),
			 beh_info_geslacht (zwan.zwan_id),
			 onde.pers_id,
			 onde.kafd_id,
			 onde.ongr_id,
			 kafd.code kafd_code,
			 ongr.code ongr_code,
			 NVL (taal.code, 'NED') taal_code,
			 onde.afgerond,
			 pers.zisnr,
			 pers.bsn,
			 pers.telefoon,
			 pers.telefoon2
	  FROM kgc_kgc_afdelingen kafd,
			 kgc_onderzoeksgroepen ongr,
			 kgc_onderzoeken onde,
			 kgc_personen pers,
			 kgc_zwangerschappen zwan,
			 kgc_talen taal
	 WHERE zwan.pers_id = onde.pers_id
			 AND onde.datum_binnen BETWEEN zwan.datum_aterm - 280 -- het onderzoek moet vallen binnen de periode van de zwangerschap
												AND zwan.datum_aterm
			 AND onde.datum_binnen >= ADD_MONTHS (SYSDATE, -6) -- het onderzoek is niet ouder dan zes maanden vanaf nu
			 AND onde.kafd_id = kafd.kafd_id
			 AND onde.ongr_id = ongr.ongr_id
			 AND onde.taal_id = taal.taal_id(+)
			 AND onde.pers_id = pers.pers_id
			 AND kafd.code IN ('CYTO', 'GENOOM')
			 AND ongr.code LIKE 'PRE%'
             and exists ( select *
                          from kgc_foetussen foet3
                          where zwan.zwan_id = foet3.zwan_id
                          and onde.foet_id = foet3.foet_id  )
			 AND ( (NVL (zwan.vreugdebrief, 'S') = 'S' -- het mag volgens de criteria
					  AND kgc_vreugdebrief_00.vreugde (zwan_id, NULL) = 'J')
					OR zwan.vreugdebrief = 'J' -- het mag door het systeem te negeren
													  )
			 AND NOT EXISTS
							(SELECT * -- er zijn geen niet afgeronde onderzoeken bij Prenataal bij deze zwangerschap
								FROM kgc_onderzoeken onde2,
									  kgc_foetussen foet,
									  kgc_onderzoeksgroepen ongr2
							  WHERE		onde2.ongr_id = ongr2.ongr_id
									  AND ongr2.code LIKE 'PRE%'
									  AND onde2.foet_id = foet.foet_id
									  AND foet.zwan_id = zwan.zwan_id
									  AND onde2.afgerond = 'N')
			 AND NOT EXISTS
							(SELECT * -- de onderzoeken moeten langer dan een uur geleden zijn afgerond,net als bij het epd
								FROM kgc_onderzoeken onde2,
									  kgc_foetussen foet,
									  kgc_onderzoeksgroepen ongr2
							  WHERE		onde2.ongr_id = ongr2.ongr_id
									  AND ongr2.code LIKE 'PRE%'
									  AND onde2.foet_id = foet.foet_id
									  AND foet.zwan_id = zwan.zwan_id
									  AND beh_onderzoek_sms_afgerond (onde2.onde_id) =
												'N')
			 --  AND NOT EXISTS -- check of er al vreugdebrief bestaat
			 --  (SELECT *
			 --  FROM kgc_brieven brie, kgc_brieftypes brty
			 --  WHERE brie.zwan_id = zwan.zwan_id
			 --  AND brie.brty_id = brty.brty_id
			 --  AND brty.code = 'VREUGDE')
			 AND NOT EXISTS ---- alleen als mensen hebben aangegeven dat ze geen bezwaar hebben
							(SELECT *
								FROM kgc_attributen attr, kgc_attribuut_waarden atwa
							  WHERE		attr.attr_id = atwa.attr_id
									  AND atwa.id = zwan.zwan_id
									  AND attr.tabel_naam = 'KGC_ZWANGERSCHAPPEN'
									  AND attr.code = 'SMS_VERZENDEN'
									  AND UPPER (atwa.waarde) = 'N')
			 AND zwan.datum_aterm = -- alleen de meest recente zwangerschap  tekt mee
										  (SELECT MAX (zwan2.datum_aterm)
											  FROM kgc_zwangerschappen zwan2
											 WHERE zwan2.pers_id = zwan.pers_id)
			 AND TRUNC (onde.datum_autorisatie) >
					  TO_DATE ('09-02-2012', 'dd-mm-yyyy');

/
QUIT
