CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNAS_PROCES_MC1_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "MONSTERNR", "SPOED", "FRACTIENR", "AANMELD_DATUM", "STATUS") AS
  SELECT CASE                                      -- Fracties om te isoleren
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                '#04B404'                                             -- groen
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      '#AEB404'                                --  licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            '#FF8000'                                -- oranje
                         ELSE
                            '#FF0000'                                  -- rood
                      END
                END
          END
             AS verborgen_kleur,
          CASE
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                4                                                     -- groen
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      3                                         -- licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            2                                        -- oranje
                         ELSE
                            1                                          -- rood
                      END
                END
          END
             AS verborgen_sortering,
          isov.fractienummer AS verborgen_telling,
          ISOV.PERS_ID AS verborgen_pers_id,
          --isov.ongr_code,
          isov.monsternummer,
          isov.spoed,
          isov.fractienummer,
          FRAC.CREATION_DATE,
          DECODE (FRAC.STATUS,
                  'A', 'Aangemeld',
                  'O', 'Opgewerkt',
                  'V', 'Vervallen',
                  'M', 'Mislukt',
                  'X', 'Verbruikt',
                  '')
             AS Status
     FROM bas_isolatielijst_vw isov,
          bas_fracties frac,
          BEH_FLOW_PRIORITEIT_VW flpr
    WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNAS_PROCES_MC1_VW'
          AND flpr.kafd_id = ISOV.KAFD_ID
          AND ISOV.FRAC_ID = FRAC.FRAC_ID
          AND (isov.fractienummer NOT LIKE 'PAT%'
               AND isov.fractienummer NOT LIKE 'PAN%')
          AND (isov.frac_id_parent IS NOT NULL
               OR bas_isol_00.aantal_buizen (isov.mons_id) > 0)
          AND (NOT EXISTS
                  (SELECT NULL
                     FROM bas_isolatielijsten isol1
                    WHERE isol1.frac_id = isov.frac_id))
          AND (isov.frac_id_parent IS NULL
               OR NVL (isov.type_afleiding, 'X') != 'D')
   UNION
   SELECT CASE                                      -- Fracties om te isoleren
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                '#04B404'                                             -- groen
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      '#AEB404'                                --  licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            '#FF8000'                                -- oranje
                         ELSE
                            '#FF0000'                                  -- rood
                      END
                END
          END
             AS verborgen_kleur,
          CASE
             WHEN (SYSDATE - FRAC.CREATION_DATE) < FLPR.ORANJE
             THEN
                4                                                     -- groen
             ELSE
                CASE
                   WHEN ( (SYSDATE - FRAC.CREATION_DATE) >= FLPR.ORANJE
                         AND (SYSDATE - FRAC.CREATION_DATE) < FLPR.LICHTGROEN)
                   THEN
                      3                                         -- licht groen
                   ELSE
                      CASE
                         WHEN ( (SYSDATE - FRAC.CREATION_DATE) < FLPR.GROEN
                               AND (SYSDATE - FRAC.CREATION_DATE) >=
                                      FLPR.LICHTGROEN)
                         THEN
                            2                                        -- oranje
                         ELSE
                            1                                          -- rood
                      END
                END
          END
             AS verborgen_sortering,
          frac.fractienummer AS verborgen_telling,
          mons.PERS_ID AS verborgen_pers_id,
          --ONGR.CODE,
          mons.monsternummer,
          'N' AS Spoed,
          frac.fractienummer,
          FRAC.CREATION_DATE,
          DECODE (FRAC.STATUS,
                  'A', 'Aangemeld',
                  'O', 'Opgewerkt',
                  'V', 'Vervallen',
                  'M', 'Mislukt',
                  'X', 'Verbruikt',
                  '')
             AS Status
     FROM bas_fracties frac,
          kgc_monsters mons,
          kgc_onderzoeksgroepen ongr,
          BEH_FLOW_PRIORITEIT_VW flpr
    WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNAS_PROCES_MC1_VW'
          AND mons.KAFD_ID = flpr.KAFD_ID
          AND MONS.ONGR_ID = ONGR.ONGR_ID
          AND MONS.MONS_ID = FRAC.MONS_ID
          AND (NOT EXISTS
                  (SELECT NULL
                     FROM bas_isolatielijsten isol1
                    WHERE isol1.frac_id = frac.frac_id))
          AND FRAC.FRACTIENUMMER LIKE '%U%'
          AND FRAC.STATUS = 'O'
   ORDER BY verborgen_sortering, spoed;

/
QUIT
