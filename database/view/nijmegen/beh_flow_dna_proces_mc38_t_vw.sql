CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC38_T_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "WERKLIJSTNR", "SAMENSTELLING", "MACHINE") AS
  SELECT '#0B3861' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            WERKLIJSTNR AS VERBORGEN_TELLING,
            WERKLIJSTNR,
            LTRIM (
               LISTAGG (' ' || SAMENSTELLING, ', ')
                  WITHIN GROUP (ORDER BY SAMENSTELLING),
               ' ')
               AS SAMENSTELLING,
            MACHINE
       FROM (  SELECT WERKLIJSTNR,
                      MACHINE,
                      CASE
                         WHEN VERBORGEN_SORTERING = 1
                         THEN
                            'Rood: ' || COUNT (DISTINCT (ONDERZOEKNR))
                      END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 2
                            THEN
                               'Oranje: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 3
                            THEN
                               'Licht_groen: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 4
                            THEN
                               'Groen: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 5
                            THEN
                               'Blauw: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 6
                            THEN
                               'Navy: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                         AS SAMENSTELLING
                 FROM (SELECT -- Overzicht PCRPlaten (werklijsten) met stoftesten status = PRE_AVAILABLE% -- AUTOMATISCH PROCES
                             FLPR.VERBORGEN_KLEUR,
                              FLPR.VERBORGEN_SORTERING,
                              ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
                              FLFA.MEDE_ID AS FAVORIET,
                              ONDE.PERS_ID AS NAAR_HELIX,
                              ONGR.CODE AS GROEP,
                              ONDE.ONDERZOEKNR,
                              ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
                              INTE.CODE AS INDICODE,
                              INGR.CODE AS INGRCODE,
                              ONWY.OMSCHRIJVING AS WIJZE,
                              DECODE (ONDE.ONDERZOEKSTYPE,
                                      'R', 'Research',
                                      'D', 'Diagnostiek',
                                      '')
                                 AS ONDE_TYPE,
                              POST.PCRPLATEBARCODE AS WERKLIJSTNR,
                              POST.DNA_ID AS FRACTIENR,
                              POST.PRIMER_ID AS STOFTESTCODE,
                              MACH.CODE AS MACHINE,
                              FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
                         FROM beh_flow_dna_postrobot POST,
                              kgc_onderzoeken onde,
                              kgc_onderzoeksgroepen ongr,
                              kgc_onderzoekswijzen onwy,
                              kgc_onderzoek_indicaties onin,
                              kgc_indicatie_teksten inte,
                              kgc_indicatie_groepen ingr,
                              beh_flow_dna_machine_storage mast,
                              beh_flow_dna_machines mach,
                              BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
                              beh_flow_favorieten flfa
                        WHERE     POST.INVESTIGATION_ID = ONDE.ONDERZOEKNR
                              AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
                              AND ONIN.INDI_ID = INTE.INDI_ID(+)
                              AND INTE.INGR_ID = INGR.INGR_ID(+)
                              AND ONDE.ONGR_ID = ONGR.ONGR_ID
                              AND ONDE.KAFD_ID = ONWY.KAFD_ID
                              AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
                              AND POST.POST_ID = MAST.POST_ID
                              AND MACH.MACH_ID = MAST.MACH_ID
                              AND FLPR.VIEW_NAME =
                                     'BEH_FLOW_DNA_PROCES_MC38_T_VW'
                              AND ONDE.KAFD_ID = FLPR.KAFD_ID
                              AND ONDE.ONDE_ID = FLPR.ONDE_ID
                              AND ONDE.ONDE_ID = FLFA.ID(+)
                              AND ONDE.AFGEROND = 'N'
                              AND UPPER (MAST.STATE) LIKE 'PRE_ERROR%'
                              AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE'))
             GROUP BY WERKLIJSTNR, MACHINE, VERBORGEN_SORTERING)
   GROUP BY WERKLIJSTNR, MACHINE
   ORDER BY WERKLIJSTNR ASC;

/
QUIT
