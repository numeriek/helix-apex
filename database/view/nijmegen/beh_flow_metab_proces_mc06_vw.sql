CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC06_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "ONDE_TYPE", "STATUS", "BEOORDELAAR", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT -- Overzicht 'Onderzoeken met afgeronde protocollen zonder uitslagen'
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    'C', 'Controle',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            MEDE.CODE,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            BAS_METINGEN METI,
            KGC_MEDEWERKERS mede,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 3) -- MG: dit moet nog gewijzigd worden!!!
            AND FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC06_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND ONDE.MEDE_ID_BEOORDELAAR = MEDE.MEDE_ID(+)
            AND NOT EXISTS
                   (SELECT 1
                      FROM KGC_UITSLAGEN uits
                     WHERE UITS.ONDE_ID = ONDE.ONDE_ID)
            AND ONDE.AFGEROND = 'N'
            AND METI.AFGEROND = 'J'
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
