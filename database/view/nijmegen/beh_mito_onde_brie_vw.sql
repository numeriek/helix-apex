CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_MITO_ONDE_BRIE_VW" ("ONDE_ID", "AFDELING", "GROEP", "BRTY_CODE", "DATUM_VERZONDEN") AS
  select  onde.onde_id,
   kafd.code afdeling,
   ongr.code groep,
   'Leeg' brty_code,
   'Leeg' datum_verzonden
 from kgc_onderzoeken onde,
    kgc_brieven brie,
    kgc_kgc_afdelingen kafd,
    kgc_onderzoeksgroepen ongr
    where onde.onde_id = brie.onde_id(+)
    and   onde.kafd_id = kafd.kafd_id
 and   onde.ongr_id = ongr.ongr_id
 and   brie.onde_id is null
 and   UPPER(kafd.code) = 'METAB'
 and   UPPER(ongr.code) = 'MITO'
 union
 select onde.onde_id,
   kafd.code afdeling,
   ongr.code groep,
   brty.code brty_code,
     to_char(brie.datum_verzonden, 'DD-MM-YYYY') datum_verzonden
 from kgc_onderzoeken onde,
   kgc_brieven brie,
     kgc_brieftypes brty,
   kgc_kgc_afdelingen kafd,
   kgc_onderzoeksgroepen ongr
 where onde.onde_id = brie.onde_id
 and   brie.brty_id = brty.brty_id
 and   onde.kafd_id = kafd.kafd_id
 and   onde.ongr_id = ongr.ongr_id
 and   UPPER(kafd.code) = 'METAB'
 and   UPPER(ongr.code) = 'MITO'
 and   not exists (select brie2.onde_id
            from kgc_brieven brie2, kgc_brieftypes brty2
            where brie2.brty_id = brty2.brty_id
        and  brty2.code = 'UITSLAG_T')
 union
 select onde.onde_id,
   kafd.code afdeling,
   ongr.code groep,
     brty.code brty_code,
     to_char(min(brie.datum_verzonden), 'DD-MM-YYYY') datum_verzonden
 from kgc_onderzoeken onde,
   kgc_brieven brie,
     kgc_brieftypes brty,
   kgc_kgc_afdelingen kafd,
   kgc_onderzoeksgroepen ongr
 where onde.onde_id = brie.onde_id
 and   brie.brty_id = brty.brty_id
 and   onde.kafd_id = kafd.kafd_id
 and   onde.ongr_id = ongr.ongr_id
 and   UPPER(kafd.code) = 'METAB'
 and   UPPER(ongr.code) = 'MITO'
 and   brty.code = 'UITSLAG_T'
 and   brie.kopie = 'N'
 and   brie.datum_verzonden is not null
 group by kafd.code, ongr.code, brty.code, onde.onde_id
 order by 1
 ;

/
QUIT
