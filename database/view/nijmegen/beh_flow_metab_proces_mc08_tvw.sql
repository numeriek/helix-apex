CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC08_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "ONDE_TYPE", "STATUS", "PROTOCOL", "SNELHEID", "WERKLIJST", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  select distinct -- Overzicht 'Onderzoeken met openstaande protocollen (met of niet werklijst)'
                  '#79A8A8'
                , ''
                , ONDE.ONDERZOEKNR as VERBORGEN_TELLING
                , FLFA.MEDE_ID as FAVORIET
                , ONDE.PERS_ID as NAAR_HELIX
                , ONGR.CODE as GROEP
                , ONDE.ONDERZOEKNR
                , ONDE.SPOED
                , ONDE.DATUM_BINNEN as BINNEN
                , ONDE.GEPLANDE_EINDDATUM as EINDDATUM
                , decode (ONDE.ONDERZOEKSTYPE, 'R', 'Research', 'D', 'Diagnostiek', 'C', 'Controle', '') as ONDE_TYPE
                , decode (ONDE.STATUS, 'V', 'Vervallen', 'G', 'Goedgekeurd', 'B', 'Beoordelen', '') as STATUS
                , STGR.CODE as PROTOCOL
                , decode (STGE.SNELHEID, 'L', 'LANGDUREND', 'K', 'KORTDUREND', '') as SNELHEID
                , -- deze kan alleen gebruikt worden indien de snelheid per vastgestelde gebruik hetzelfde is. Het is nu wel zo, dus het ophalen van snelheid gebeurt niet zoals het echt zou moeten, wel levert het voor nu juiste resultaten.
                  case when WLIT.METI_ID is null then 'GEEN' else to_char (wm_concat (distinct (WLST.WERKLIJST_NUMMER))) end as WERKLIJST
                , NOTI.OMSCHRIJVING as NOTITIE
                , NOTI.NOTI_ID as VERBORGEN_NOTI_ID
                , FLFA.FLOW_ID as VERBORGEN_FLOW_ID
  from   kgc_onderzoeken onde
       , kgc_notities noti
       , kgc_kgc_afdelingen kafd
       , kgc_onderzoeksgroepen ongr
       , BAS_METINGEN METI
       , KGC_STOFTESTGROEPEN STGR
       , KGC_STOFTESTGROEP_GEBRUIK STGE
       , -- deze kan alleen gebruikt worden indien de snelheid per vastgestelde gebruik hetzelfde is. Het is nu wel zo, dus het ophalen van snelheid gebeurt niet zoals het echt zou moeten, wel levert het voor nu juiste resultaten.
         KGC_WERKLIJSTEN WLST
       , KGC_WERKLIJST_ITEMS WLIT
       , BEH_FLOW_FAVORIETEN FLFA
  where  ONDE.ONGR_ID in (select FKON.ONGR_ID
                          from   BEH_FLOW_KAFD_ONGR fkon
                          where  FKON.FLOW_ID = 3) -- MG: dit moet nog gewijzigd worden!!!
     and ONDE.ONDE_ID = NOTI.ID(+)
     and NOTI.ENTITEIT(+) = 'ONDE'
     and KAFD.KAFD_ID = ONDE.KAFD_ID
     and ONGR.ONGR_ID = ONDE.ONGR_ID
     and ONDE.ONDE_ID = METI.ONDE_ID
     and METI.STGR_ID = STGR.STGR_ID
     and STGR.STGR_ID = STGE.STGR_ID -- deze kan alleen gebruikt worden indien de snelheid per vastgestelde gebruik hetzelfde is. Het is nu wel zo, dus het ophalen van snelheid gebeurt niet zoals het echt zou moeten, wel levert het voor nu juiste resultaten.
     and METI.METI_ID = WLIT.METI_ID(+)
     and WLIT.WLST_ID = WLST.WLST_ID(+)
     and ONDE.AFGEROND = 'N'
     and METI.AFGEROND = 'N'
     and ONDE.ONDE_ID = FLFA.ID(+)
     and upper (FLFA.ENTITEIT(+)) = upper ('ONDE')
     and exists (select 1
                 from   bas_meetwaarden meet
                 where  MEET.AFGEROND = 'N' and MEET.METI_ID = METI.METI_ID)
  group by '#79A8A8', '', ONDE.ONDERZOEKNR, ONDE.PERS_ID, FLFA.MEDE_ID, ONGR.CODE, ONDE.ONDERZOEKNR, ONDE.SPOED
         , ONDE.DATUM_BINNEN, ONDE.GEPLANDE_EINDDATUM, WLIT.METI_ID
         , decode (ONDE.ONDERZOEKSTYPE, 'R', 'Research', 'D', 'Diagnostiek', 'C', 'Controle', '')
         , decode (ONDE.STATUS, 'V', 'Vervallen', 'G', 'Goedgekeurd', 'B', 'Beoordelen', ''), STGR.CODE
         , decode (STGE.SNELHEID, 'L', 'LANGDUREND', 'K', 'KORTDUREND', ''), NOTI.OMSCHRIJVING, NOTI.NOTI_ID, FLFA.FLOW_ID
  order by ONGR.CODE, ONDE.SPOED asc, ONDE.ONDERZOEKNR asc;

/
QUIT
