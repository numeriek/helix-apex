CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_HEBON_ONDE_UITN_VW" ("KAFD_ID", "PERS_ID", "ONDE_ID", "RELA_ID") AS
  SELECT													-- onderzoeken van de persoon:
			kafd.kafd_id, onde.pers_id, onde.onde_id, onde.rela_id
	  FROM kgc_kgc_afdelingen kafd, kgc_onderzoeken onde
	 WHERE	  onde.kafd_id = kafd.kafd_id
			 AND kafd.code = 'GENOOM'
			 AND onde.onderzoekstype = 'D'
			 AND onde.onderzoeknr IN ('R15-01187')
			 AND onde.datum_autorisatie BETWEEN (SYSDATE - 77)
													  AND (SYSDATE - (1 / 24)) --pas na een uur en nietlanger dan een week terug
			 AND EXISTS
					  (SELECT * --alleen aanvragers die bij klinische genetica in het radboud werken
						  FROM kgc_relaties rela,
								 kgc_afdelingen afdg,
								 kgc_specialismen spec
						 WHERE	  rela.rela_id = onde.rela_id
								 AND rela.spec_id = spec.spec_id(+)
								 AND rela.afdg_id = afdg.afdg_id
								 --and rela.vervallen = 'N'
								 AND UPPER (adres) LIKE '%ALHIER%'
								 AND UPPER (afdg.omschrijving) LIKE
										  '%KLINISCHE GENETICA')
			 AND EXISTS
					  (SELECT * 			  --aleen onderzoeken die in het epd komen
						  FROM beh_epd_vw epvw
						 WHERE onde.onde_id = epvw.onde_id)
			 AND EXISTS
					  (SELECT * 			 -- alleen onderzoeken met indicatie BRCA%
						  FROM kgc_onderzoek_indicaties onin,
								 kgc_indicatie_teksten indi
						 WHERE	  onin.onde_id = onde.onde_id
								 AND onin.indi_id = indi.indi_id
								 AND UPPER (indi.code) LIKE 'BRCA%')
			 AND NOT EXISTS
							(SELECT * -- alleen personen die nog geen uitnodigingsbrief hebben
								FROM beh_hebon_waarden hewa
							  WHERE onde.pers_id = hewa.pers_id
									  AND hewa.uitnodiging_gelukt = 'J');

/
QUIT
