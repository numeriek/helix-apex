CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_BRIE_BEST_VERKEERD_VW" ("UITS_ID", "BEST_ID") AS
  select brie.uits_id
, brie.best_id
from kgc_brieven brie
, kgc_uitslagen uits
where BRIE.UITS_ID = uits.uits_id
and brie.uits_id is not null
and brie.best_id is not null
and uits.creation_date > to_date('01-01-2010','dd-mm-yyyy')
minus
select best.entiteit_pk
, BEST.BEST_ID
from kgc_bestanden best
where entiteit_code = 'UITS'
and best.creation_date > to_date('01-01-2010','dd-mm-yyyy');

/
QUIT
