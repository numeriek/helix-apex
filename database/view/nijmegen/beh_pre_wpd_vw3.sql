CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_PRE_WPD_VW3" ("PERSOON", "MONSTERNUMMER", "MATERIAAL", "HERKOMST", "ONDE_ID", "ONDERZOEKNR", "DATUM_BINNEN", "INDICATIES", "REDEN", "WPD_LEEFTIJD", "WPD_GROEP", "UITSLAG_CODE", "UITSLAG", "NORMAAL") AS
  SELECT kgc_pers_00.info (onde.pers_id) persoon,
			 mons.monsternummer,
			 mate.code materiaal,
			 DECODE (herk.code,
						'OBST_ARN', 'ARNHEM',
						'OBST_BOS', 'DEN BOS',
						'OBST_EDE', 'EDE',
						'OBST_ENS', 'ENSCHEDE',
						'OBST_TILB', 'TILBURG',
						'NIJMEGEN'
					  )
				 herkomst,
			 onde.onde_id,
			 onde.onderzoeknr,
			 onde.datum_binnen,
			 beh_onderzoek_indicaties (onde.onde_id) indicaties,
			 onde.omschrijving reden,
			 beh_onde_wpd_lft (onde.onde_id) wpd_leeftijd,
			 beh_onde_wpd_grp (onde.onde_id) wpd_groep,
			 onui.code uitslag_code,
			 onui.omschrijving uitslag,
			 DECODE (
				 NVL (
					 kgc_attr_00.
					  waarde ('KGC_ONDERZOEK_UITSLAGCODES',
								 'WPD_NORMAAL',
								 onui.onui_id
								),
					 'N'),
				 'Normaal', 'Normaal',
                 'Hangt af van indicatie', 'Hangt af van indicatie',
				 'N', 'Afwijkend')
				 normaal
	  FROM kgc_onderzoeken onde,
			 kgc_personen pers,
			 kgc_onderzoek_monsters onmo,
			 kgc_monsters mons,
			 kgc_materialen mate,
			 kgc_kgc_afdelingen kafd,
			 kgc_onderzoeksgroepen ongr,
			 kgc_onderzoek_uitslagcodes onui,
			 kgc_herkomsten herk
	 WHERE	  mons.pers_id = pers.pers_id
			 AND mons.herk_id = herk.herk_id(+)
			 AND mons.mate_id = mate.mate_id
			 AND mons.mons_id = onmo.mons_id
			 AND onde.onde_id = onmo.onde_id
			 AND kafd.kafd_id = onde.kafd_id
			 AND ongr.ongr_id = onde.ongr_id
			 AND onde.onui_id = onui.onui_id
			 --and pers.postcode = post.postcode(+)
			 AND mons.foet_id IS NOT NULL
			 AND kafd.code = 'GENOOM'
			 AND ongr.code IN ('PRE_NIJM')
-- 				AND onde.datum_binnen BETWEEN TO_DATE ('01-01-2012', 'dd-mm-yyyy')
-- 															  AND TO_DATE ('01-01-2013', 'dd-mm-yyyy');

/
QUIT
