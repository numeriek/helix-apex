CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DIAGNOSE_VW" ("ONDERZOEKNR", "PERS_ID", "AANSPREKEN", "CODE_AUTORISATOR_ONDERZOEK", "NAAM_AUTORISATOR_ONDERZOEK", "CODE_AUTORISATOR_DIAGNOSE", "NAAM_AUTORISATOR_DIAGNOSE", "CINEAS_CODE", "CINEAS_TEKST", "ZEKERHEID_DIAGNOSE", "ZEKERHEID_STATUS", "ZEKERHEID_ERFMODUS", "DIAGNOSE_CODE", "DIAGNOSE_OMSCHRIJVING", "AFGEROND") AS
  SELECT                 -- wordt in iedere geval gebruikt voor Disco (Jayne)
         onde.onderzoeknr,
          onde.pers_id AS pers_id,
          pers.aanspreken,
          mede_o.code AS code_autorisator_onderzoek,
          mede_o.naam AS naam_autorisator_onderzoek,
          mede_d.code AS code_autorisator_diagnose,
          mede_d.naam AS naam_autorisator_diagnose,
          diag.diagnose_code AS cineas_code,
          chfd.ziektetekst AS cineas_tekst,
          DECODE (diag.zekerhed_diagnose,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_diagnose,
          DECODE (diag.zekerheid_status,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_status,
          DECODE (diag.zekerheid_erfmodus,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_erfmodus,
          dist.code AS diagnose_code,
          dist.omschrijving AS diagnose_omschrijving,
          ONDE.AFGEROND
     FROM kgc_onderzoeken onde,
          kgc_diagnoses diag,
          kgc_diagnose_statussen dist,
          cin_hfd chfd,
          kgc_medewerkers mede_o,
          kgc_medewerkers mede_d,
          kgc_personen pers
    WHERE     diag.onbe_id IS NULL                 -- ONBE_ID MOET LEEG   ZIJN
          AND diag.pers_id = pers.pers_id -- PERS_ID MOET GEVULD ZIJN, DIT IS DE PERSOON VAN DE DIAGNOSE
          AND diag.onde_id = onde.onde_id -- ONDE_ID MOET GEVULD ZIJN, DIT IS HET ODNEROZEK DAT ER BIJ HOORT
          AND diag.dist_id = dist.dist_id(+)
          AND diag.diagnose_code = chfd.ziektecode(+)
          AND onde.mede_id_autorisator = mede_o.mede_id(+)
          AND diag.mede_id_autorisator = mede_d.mede_id(+)
   --AND onde.afgerond = 'J' -- uitgezet om ook niet afgeronde onderzoeken te zien op 07-12-2011
   UNION ALL
   -- 2 diagnose waarbij alleen het onderzoek bekend is, de onderzoeks persoon is dan de diagnose persoon
   SELECT onde.onderzoeknr,
          onde.pers_id AS pers_id,
          pers.aanspreken,
          mede_o.code AS code_autorisator_onderzoek,
          mede_o.naam AS naam_autorisator_onderzoek,
          mede_d.code AS code_autorisator_diagnose,
          mede_d.naam AS naam_autorisator_diagnose,
          diag.diagnose_code AS cineas_code,
          chfd.ziektetekst AS cineas_tekst,
          DECODE (diag.zekerhed_diagnose,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_diagnose,
          DECODE (diag.zekerheid_status,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_status,
          DECODE (diag.zekerheid_erfmodus,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_erfmodus,
          dist.code AS diagnose_code,
          dist.omschrijving AS diagnose_omschrijving,
          ONDE.AFGEROND
     FROM kgc_onderzoeken onde,
          kgc_diagnoses diag,
          kgc_diagnose_statussen dist,
          cin_hfd chfd,
          kgc_medewerkers mede_o,
          kgc_medewerkers mede_d,
          kgc_personen pers
    WHERE     diag.onbe_id IS NULL
          AND diag.onde_id = onde.onde_id
          AND onde.pers_id = pers.pers_id
          AND diag.pers_id IS NULL
          AND diag.dist_id = dist.dist_id(+)
          AND diag.diagnose_code = chfd.ziektecode(+)
          AND onde.mede_id_autorisator = mede_o.mede_id(+)
          AND diag.mede_id_autorisator = mede_d.mede_id(+)
   --AND onde.afgerond = 'J' -- uitgezet om ook niet afgeronde onderzoeken te zien op 07-12-2011
   UNION ALL
   -- 3 diagnose in de oude situatie dwz  tot en met halverwege 2006
   SELECT onde.onderzoeknr,
          betr.pers_id AS pers_id,
          pers.aanspreken,
          mede_o.code AS code_autorisator_onderzoek,
          mede_o.naam AS naam_autorisator_onderzoek,
          mede_d.code AS code_autorisator_diagnose,
          mede_d.naam AS naam_autorisator_diagnose,
          diag.diagnose_code AS cineas_code,
          chfd.ziektetekst AS cineas_tekst,
          DECODE (diag.zekerhed_diagnose,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_diagnose,
          DECODE (diag.zekerheid_status,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_status,
          DECODE (diag.zekerheid_erfmodus,
                  '0', 'Uitgesloten',
                  '1', 'Onwaarschijnlijk',
                  '2', 'Mogelijk',
                  '3', 'Waarschijnlijk',
                  '4', 'Zeker')
             AS zekerheid_erfmodus,
          dist.code AS diagnose_code,
          dist.omschrijving AS diagnose_omschrijving,
          ONDE.AFGEROND
     FROM kgc_onderzoeken onde,
          kgc_diagnoses diag,
          kgc_diagnose_statussen dist,
          cin_hfd chfd,
          kgc_medewerkers mede_o,
          kgc_medewerkers mede_d,
          kgc_onderzoek_betrokkenen betr,
          kgc_personen pers
    WHERE     diag.onde_id = onde.onde_id
          AND diag.onbe_id = betr.onbe_id              -- betrokkenen diagnose
          AND diag.pers_id IS NULL
          AND betr.pers_id = pers.pers_id                      -- naam ophalen
          AND diag.dist_id = dist.dist_id(+)
          AND diag.diagnose_code = chfd.ziektecode(+)
          AND onde.mede_id_autorisator = mede_o.mede_id(+)
          AND diag.mede_id_autorisator = mede_d.mede_id(+)
   --AND onde.afgerond = 'J' -- uitgezet om ook niet afgeronde onderzoeken te zien op 07-12-2011
   UNION ALL
   -- 4 ook alle onderzoeken waarbij geen diagnose geregistreerd is - toegevoegd op verzoek van Jayne 07-12-2011
   SELECT                 -- wordt in iedere geval gebruikt voor Disco (Jayne)
         onde.onderzoeknr,
          onde.pers_id AS pers_id,
          pers.aanspreken,
          mede_o.code AS code_autorisator_onderzoek,
          mede_o.naam AS naam_autorisator_onderzoek,
          '',
          '',
          'Geen diagnose',
          '',
          '',
          '',
          '',
          '',
          '',
          ONDE.AFGEROND
     FROM kgc_onderzoeken onde, kgc_medewerkers mede_o, kgc_personen pers, kgc_kgc_afdelingen kafd
    WHERE     onde.pers_id = pers.pers_id
          AND onde.mede_id_autorisator = mede_o.mede_id(+)
          --AND onde.afgerond = 'J' -- uitgezet om ook niet afgeronde onderzoeken te zien op 07-12-2011
          AND ONDE.ONDE_ID NOT IN (SELECT NVL (DIAG.ONDE_ID, 0)
                                     FROM kgc_diagnoses diag) -- alleen onderzoeken waarbij geen diagnose is geregistreerd;
         AND ONDE.KAFD_ID = KAFD.KAFD_ID
         AND KAFD.CODE = 'KLINGEN'
 ;

/
QUIT
