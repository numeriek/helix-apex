CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_HELP_FAMI_VW" ("PERS_ID", "FAMILIENUMMER") AS
  select  distinct fale.pers_id
  ,       LISTAGG (fami.familienummer, ',') WITHIN GROUP (ORDER BY fami.familienummer) OVER (PARTITION BY fale.pers_id) AS familienummer
    from kgc_families fami, kgc_familie_leden fale
  where fami.fami_id = fale.fami_id;

/
QUIT
