CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_NGS_EXA_LIB_MDET_VW" ("MEET_ID", "ONDE_ID", "ONDERZOEKNR", "SEQUENCING", "FACILITEIT", "STATUS", "DATA_BESCHIKBAAR", "DATA_OPGEHAALD", "DATA_GEBACK_UPT") AS
  SELECT "MEET_ID",
            "ONDE_ID",
            "ONDERZOEKNR",
            "'Sequencing'",
            DECODE ("'Faciliteit'",  'HUIS', 'In huis',  'BGI', 'BGI',  ''),
            DECODE ("'Status'",  'M', 'Mislukt',  'W', 'In de wacht',  ''),
            "'Data beschikbaar'",
            "'Data opgehaald'",
            "'Data geback-upt'"
       FROM (SELECT mdet.meet_id,
                    PROMPT,
                    WAARDE,
                    ONDE.ONDERZOEKNR,
                    ONDE.ONDE_ID
               FROM kgc_onderzoeken onde,
                    bas_metingen meti,
                    bas_meetwaarden meet,
                    bas_meetwaarde_details mdet,
                    kgc_onderzoeksgroepen ongr,
                    kgc_meetwaardestructuur mwst
              WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                    AND METI.METI_ID = MEET.METI_ID
                    AND MEET.MEET_ID = MDET.MEET_ID
                    AND ONDE.ONGR_ID = ONGR.ONGR_ID
                    AND MEET.MWST_ID = MWST.MWST_ID
                    AND ONDE.AFGEROND = 'N'
                    AND METI.AFGEROND = 'N'
                    AND MEET.AFGEROND = 'N'
                    AND ONGR.CODE = 'NGS'
                    AND MWST.CODE = 'LIB_ST2') PIVOT (MAX (WAARDE)
                                               FOR (PROMPT)
                                               IN  ('Sequencing',
                                                   'Faciliteit',
                                                   'Status',
                                                   'Data beschikbaar',
                                                   'Data opgehaald',
                                                   'Data geback-upt'))
   ORDER BY onde_id, meet_id;

/
QUIT
