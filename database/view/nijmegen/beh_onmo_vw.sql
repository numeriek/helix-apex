CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ONMO_VW" ("MONS_ID", "ONDE_ID") AS
  select onmo.mons_id
, min(onmo.onde_id) onde_id
from kgc_onderzoek_monsters onmo
, kgc_monsters mons
where mons.mons_Id = onmo.mons_id
and mons.kafd_id in (1,28,30,32, 3, 52, 31)
group by onmo.mons_id
 ;

/
QUIT
