CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC15_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "WIJZE", "ONDE_TYPE", "INDICODE", "INGRCODE", "FRACTIENR", "WERKLIJSTNR", "TECHNIEK", "EXTERNE_ID", "GEFIXEERD", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT -- stoftesten op werklijst nog niet in POSTROBOT tabel - via semi-automatisch proces gestuurd naar de robot
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            TECH.CODE,
            TRGE.EXTERNE_ID,
            TRGE.GEFIXEERD,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            kgc_onderzoek_monsters onmo,
            bas_fracties frac,
            kgc_tray_gebruik trge,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_technieken tech,
            BEH_FLOW_PRIORITEIT_NEW_VW flpr,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC15_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND TRGE.WERKLIJST_NUMMER = WLST.WERKLIJST_NUMMER
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND STOF.TECH_ID = TECH.TECH_ID
            AND ONDE.AFGEROND = 'N'
            AND MEET.AFGEROND = 'N'
            AND (TRGE.GEFIXEERD = 'J' OR WLST.WERKLIJST_NUMMER LIKE '%-ROBOT')
            AND NOT EXISTS
                   (SELECT *
                      FROM BEH_FLOW_DNA_POSTROBOT POST
                     WHERE POST.PCRPLATEBARCODE = WLST.WERKLIJST_NUMMER)
            AND (   UPPER (STOF.OMSCHRIJVING) LIKE '%(SEQ)'
                 OR UPPER (STOF.OMSCHRIJVING) LIKE '%(GS)'
                 OR UPPER (STOF.OMSCHRIJVING) LIKE '%(ION)')
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            EINDDATUM ASC,
            ONDERZOEKNR ASC,
            FRACTIENR ASC;

/
QUIT
