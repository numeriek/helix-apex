CREATE OR REPLACE FORCE VIEW "HELIX"."DFT_KOPPELING_VW" ("DECL_ID", "KAFD_CODE", "ODBNR", "BONNUMMER", "VERRICHTINGCODE", "SYSTEEMDEEL", "PROD_AFDELING", "ZISNR", "VERRICHTINGDAT", "AANVR_AFDELING", "PROD_SPEC", "AANVR_SPEC", "ABW", "ABI_VERZCD", "ABI_VERZNR", "TEKST", "AANTAL", "NAAM", "GEBDAT", "ADRES", "WPLTS", "PSTCD", "LAND", "STATUS", "STAT_OMS") AS
  SELECT decl.decl_id
,      kafd.code kafd_code
,      onde.onde_id odbnr
,      to_char(decl.decl_id) bonnummer
,      'POST' verrichtingcode
,      'OR_DMG' systeemdeel
,      'W530' prod_afdeling
,      nvl( replace( pers.zisnr, '.', null ), '0000000' ) zisnr
,      to_char( onde.datum_autorisatie, 'YYYYMMDD' ) verrichtingdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'ARTS'
             ,            'W530'
             ) aanvr_afdeling
,      'KLG' prod_spec
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'EXT'
             ,            'KLG'
             ) aanvr_spec
,      decode( decl.verzekeringswijze
             , 'Z',  decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'Z1'
                           , 'ZF'
                           )
             ,       decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'N1'
                           , 'NZ'
                           )
             ) abw
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', decode( decl.verzekeringswijze
                                , 'Z', verz.code
                                , 'EIGENR'
                                )
             , null
             ) abi_verzcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr( replace( pers.verzekeringsnr, '.', null ), 1, 10 )
             , null
             ) abi_verznr
,      substr( kgc_decl_00.decl_identificatie( decl.decl_id )||' '||rela.aanspreken||' '||afdg.omschrijving,1,44 ) tekst
,      to_char(1) aantal
,      substr(pers.aanspreken,1,48) naam
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', to_char( pers.geboortedatum, 'YYYYMMDD' )
             , null
             ) gebdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.adres,1,61)
             , null
             ) adres
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.woonplaats,1,30)
             , null
             ) wplts
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(replace(pers.postcode,' ',null),1,6)
             , null
             ) pstcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.land,1,15)
             , null
             ) land
,      null status
,      null stat_oms
from   kgc_kgc_afdelingen kafd
,      kgc_afdelingen afdg
,      kgc_verzekeraars verz
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_onderzoek_betrokkenen onbe
,      kgc_declaraties decl
where  decl.entiteit = 'ONBE'
and    decl.id = onbe.onbe_id
and    nvl( decl.status, 'H' ) = 'H'
and    decl.declareren = 'J'
and    onbe.onde_id = onde.onde_id
and    onde.rela_id = rela.rela_id
and    rela.afdg_id = afdg.afdg_id (+)
and    onde.afgerond = 'J'
and    onde.kafd_id = kafd.kafd_id
and    decl.pers_id = pers.pers_id
and    decl.verz_id = verz.verz_id (+)
and    kafd.code = 'DNA'
union -- DNA foetusonderzoek
SELECT decl.decl_id
,      kafd.code kafd_code
,      onde.onde_id odbnr
,      to_char(decl.decl_id) bonnummer
,      'PRE' verrichtingcode
,      'OR_DMG' systeemdeel
,      'W530' prod_afdeling
,      nvl( replace( pers.zisnr, '.', null ), '0000000' ) zisnr
,      to_char( onde.datum_autorisatie, 'YYYYMMDD' ) verrichtingdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'ARTS'
             ,            'W530'
             ) aanvr_afdeling
,      'KLG' prod_spec
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'EXT'
             ,            'KLG'
             ) aanvr_spec
,      decode( decl.verzekeringswijze
             , 'Z',  decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'Z1'
                           , 'ZF'
                           )
             ,       decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'N1'
                           , 'NZ'
                           )
             ) abw
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', decode( decl.verzekeringswijze
                                , 'Z', verz.code
                                , 'EIGENR'
                                )
             , null
             ) abi_verzcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr( replace( pers.verzekeringsnr, '.', null ), 1, 10 )
             , null
             ) abi_verznr
,      substr( kgc_decl_00.decl_identificatie( decl.decl_id )||' '||rela.aanspreken||' '||afdg.omschrijving,1,44 ) tekst
,      to_char(1) aantal
,      substr(pers.aanspreken,1,48) naam
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', to_char( pers.geboortedatum, 'YYYYMMDD' )
             , null
             ) gebdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.adres,1,61)
             , null
             ) adres
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.woonplaats,1,30)
             , null
             ) wplts
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(replace(pers.postcode,' ',null),1,6)
             , null
             ) pstcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.land,1,15)
             , null
             ) land
,      null status
,      null stat_oms
from   kgc_kgc_afdelingen kafd
,      kgc_afdelingen afdg
,      kgc_verzekeraars verz
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_declaraties decl
where  decl.entiteit = 'ONDE'
and    decl.id = onde.onde_id
and    nvl( decl.status, 'H' ) = 'H'
and    decl.declareren = 'J'
and    onde.rela_id = rela.rela_id
and    rela.afdg_id = afdg.afdg_id (+)
and    onde.afgerond = 'J'
and    onde.kafd_id = kafd.kafd_id
and    decl.pers_id = pers.pers_id
and    decl.verz_id = verz.verz_id (+)
and    kafd.code = 'DNA'
and    instr( upper(onde.omschrijving), 'PRENATA' ) > 0
union -- DNA maternale conterminatie (profilermeting)
SELECT decl.decl_id
,      kafd.code kafd_code
,      onde.onde_id odbnr
,      to_char(decl.decl_id) bonnummer
,      'POST' verrichtingcode
,      'OR_DMG' systeemdeel
,      'W530' prod_afdeling
,      nvl( replace( pers.zisnr, '.', null ), '0000000' ) zisnr
,      to_char( onde.datum_autorisatie, 'YYYYMMDD' ) verrichtingdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'ARTS'
             ,            'W530'
             ) aanvr_afdeling
,      'KLG' prod_spec
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'EXT'
             ,            'KLG'
             ) aanvr_spec
,      decode( decl.verzekeringswijze
             , 'Z',  decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'Z1'
                           , 'ZF'
                           )
             ,       decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'N1'
                           , 'NZ'
                           )
             ) abw
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', decode( decl.verzekeringswijze
                                , 'Z', verz.code
                                , 'EIGENR'
                                )
             , null
             ) abi_verzcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr( replace( pers.verzekeringsnr, '.', null ), 1, 10 )
             , null
             ) abi_verznr
,      substr( kgc_decl_00.decl_identificatie( decl.decl_id )||' '||rela.aanspreken||' '||afdg.omschrijving,1,44 ) tekst
,      to_char(1) aantal
,      substr(pers.aanspreken,1,48) naam
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', to_char( pers.geboortedatum, 'YYYYMMDD' )
             , null
             ) gebdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.adres,1,61)
             , null
             ) adres
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.woonplaats,1,30)
             , null
             ) wplts
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(replace(pers.postcode,' ',null),1,6)
             , null
             ) pstcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.land,1,15)
             , null
             ) land
,      null status
,      null stat_oms
from   kgc_kgc_afdelingen kafd
,      kgc_afdelingen afdg
,      kgc_verzekeraars verz
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
,      bas_metingen meti
,      kgc_declaraties decl
where  decl.entiteit = 'METI'
and    decl.id = meti.meti_id
and    nvl( decl.status, 'H' ) = 'H'
and    decl.declareren = 'J'
and    meti.onde_id = onde.onde_id
and    onde.rela_id = rela.rela_id
and    rela.afdg_id = afdg.afdg_id (+)
and    onde.afgerond = 'J'
and    onde.kafd_id = kafd.kafd_id
and    decl.pers_id = pers.pers_id
and    decl.verz_id = verz.verz_id (+)
and    kafd.code = 'DNA'
union -- metab (onderzoek)
SELECT decl.decl_id
,      kafd.code kafd_code
,      onde.onde_id odbnr
,      to_char(decl.decl_id) bonnummer
,      decode( onde.onderzoekswijze
             , 'B1', '377884'
             , 'B2', '377884'
             , 'B3', '377884'
             , 'EN', '377885'
             , null
             ) verrichtingcode -- = straks tariefcode
,      'OR_METAB' systeemdeel
,      'W590' prod_afdeling
,      nvl( replace( pers.zisnr, '.', null ), '0000000' ) zisnr
,      to_char( onde.datum_autorisatie, 'YYYYMMDD' ) verrichtingdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'ARTS'
             ,            'W590'
             ) aanvr_afdeling
,      'LAB' prod_spec
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'EXT'
             ,            'LAB'
             ) aanvr_spec
,      decode( decl.verzekeringswijze
             , 'Z',  decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'Z1'
                           , 'ZF'
                           )
             ,       decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'N1'
                           , 'NZ'
                           )
             ) abw
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', decode( decl.verzekeringswijze
                                , 'Z', verz.code
                                , 'EIGENR'
                                )
             , null
             ) abi_verzcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr( replace( pers.verzekeringsnr, '.', null ), 1, 10 )
             , null
             ) abi_verznr
,      substr( kgc_decl_00.decl_identificatie( decl.decl_id )||' '||rela.aanspreken||' '||afdg.omschrijving,1,44 ) tekst
,      to_char(1) aantal
,      substr(pers.aanspreken,1,48) naam
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', to_char( pers.geboortedatum, 'YYYYMMDD' )
             , null
             ) gebdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.adres,1,61)
             , null
             ) adres
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.woonplaats,1,30)
             , null
             ) wplts
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(replace(pers.postcode,' ',null),1,6)
             , null
             ) pstcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.land,1,15)
             , null
             ) land
,      null status
,      null stat_oms
from   kgc_kgc_afdelingen kafd
,      kgc_afdelingen afdg
,      kgc_verzekeraars verz
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_declaraties decl
where  decl.entiteit = 'ONDE'
and    decl.id = onde.onde_id
and    nvl( decl.status, 'H' ) = 'H'
and    decl.declareren = 'J'
and    onde.rela_id = rela.rela_id
and    rela.afdg_id = afdg.afdg_id (+)
and    onde.afgerond = 'J'
and    onde.kafd_id = kafd.kafd_id
and    decl.pers_id = pers.pers_id
and    decl.verz_id = verz.verz_id (+)
and    kafd.code = 'MET'
union -- metab (protocol)
SELECT decl.decl_id
,      kafd.code kafd_code
,      onde.onde_id odbnr
,      to_char(decl.decl_id) bonnummer
,      kgc_attr_00.waarde( 'KGC_STOFTESTGROEPEN', 'TARIEFCODE', meti.stgr_id) verrichtingcode
,      'OR_METAB' systeemdeel
,      'W590' prod_afdeling
,      nvl( replace( pers.zisnr, '.', null ), '0000000' ) zisnr
,      to_char( onde.datum_autorisatie, 'YYYYMMDD' ) verrichtingdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', decode( rela.inst_id
                                , null, 'ARTS'
                                , kgc_attr_00.waarde( 'KGC_INSTELLINGEN', 'ACCODE', rela.inst_id)
                                )
             ,            'W590'
             ) aanvr_afdeling
,      'LAB' prod_spec
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', 'EXT'
             ,            'LAB'
             ) aanvr_spec
,      decode( decl.verzekeringswijze
             , 'Z',  decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'Z1'
                           , 'ZF'
                           )
             ,       decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
                           , '0000000', 'N1'
                           , 'NZ'
                           )
             ) abw
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', decode( decl.verzekeringswijze
                                , 'Z', verz.code
                                , 'EIGENR'
                                )
             , null
             ) abi_verzcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr( replace( pers.verzekeringsnr, '.', null ), 1, 10 )
             , null
             ) abi_verznr
,      substr( kgc_decl_00.decl_identificatie( decl.decl_id )||' '||rela.aanspreken||' '||afdg.omschrijving,1,44 ) tekst
,      to_char(1) aantal
,      substr(pers.aanspreken,1,48) naam
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', to_char( pers.geboortedatum, 'YYYYMMDD' )
             , null
             ) gebdat
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.adres,1,61)
             , null
             ) adres
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.woonplaats,1,30)
             , null
             ) wplts
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(replace(pers.postcode,' ',null),1,6)
             , null
             ) pstcd
,      decode( nvl( replace( pers.zisnr, '.', null ), '0000000' )
             , '0000000', substr(pers.land,1,15)
             , null
             ) land
,      null status
,      null stat_oms
from   kgc_kgc_afdelingen kafd
,      kgc_afdelingen afdg
,      kgc_verzekeraars verz
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
,      bas_metingen meti
,      kgc_declaraties decl
where  decl.entiteit = 'METI'
and    decl.id = meti.meti_id
and    nvl( decl.status, 'H' ) = 'H'
and    decl.declareren = 'J'
and    meti.onde_id = onde.onde_id
and    onde.rela_id = rela.rela_id
and    rela.afdg_id = afdg.afdg_id (+)
and    onde.afgerond = 'J'
and    onde.kafd_id = kafd.kafd_id
and    decl.pers_id = pers.pers_id
and    decl.verz_id = verz.verz_id (+)
and    kafd.code = 'MET'
 ;

/
QUIT
