CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC51_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "BI_BU", "INT_EXT", "TAAL", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "AANVRAAG", "FRACTIENR", "WERKLIJSTNR", "DATA_GEDEELD", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_BESTANDEN
        AS (SELECT BEST.ENTITEIT_PK,
                   BEST.ENTITEIT_CODE,
                   beh_bestanden_to_string (
                      CAST (
                         COLLECT (
                               '<a href="'
                            || BEST.BESTAND_SPECIFICATIE
                            || '" target="_blank">Open</a>' ORDER                                                       BY BEST.BEST_ID desc ) as beh_bestanden_link)) as bestanden
                FROM KGC_BESTANDEN BEST, KGC_BESTAND_CATEGORIEEN BCAT
                WHERE BEST.BCAT_ID = BCAT.BCAT_ID
                    AND BEST.ENTITEIT_CODE  = 'ONDE'
                    AND BCAT.CODE = 'AANVRAAG'
                    AND BEST.VERVALLEN = 'N'
                    AND BEST.VERWIJDERD = 'N'
                group by best.ENTITEIT_PK, BEST.ENTITEIT_CODE)
     SELECT                                 -- Overzicht NGS - Data is gedeeld
           DISTINCT
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            DECODE (beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
                    'J', 'INTERN',
                    'N', '')
               AS INT_EXT,
            TAAL.CODE TAAL,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            BEST.BESTANDEN,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            PVT.DATA_GEDEELD,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            beh_flow_help_fami_vw fami,
            kgc_onderzoekswijzen onwy,
            w_kgc_bestanden best,
            KGC_TALEN TAAL,
            beh_flow_favorieten flfa,
            BEH_FLOW_PRIORITEIT_NEW_VW flpr,
            (SELECT meet_id, DATA_GEDEELD
               FROM BEH_FLOW_NGS_EXA_S_MDET_VW
              WHERE     DATA_GEDEELD IS NOT NULL
                    AND INTERPRETATIE_KLAAR IS NULL
                    AND (BEVESTIGEN = 'N')) pvt
      WHERE   FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC51_VW'
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FAMI.PERS_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND FLPR.ONDE_ID = ONDE.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID(+)
            AND WLIT.WLST_ID = WLST.WLST_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK(+)
            AND MEET.MEET_ID = PVT.MEET_ID
            AND MEET.AFGEROND = 'N'
            AND ONDE.AFGEROND = 'N'
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
