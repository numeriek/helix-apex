CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_RELA_MET_DECL_ZONDER_SER" ("SER_ID", "RELA_ID", "RELA_CODE", "GESLACHT", "TITULATUUR", "RELA_VOORLETTERS", "RELA_VOORVOEGSEL", "RELA_ACHTERNAAM", "RELA_AANSPREKEN", "RELA_SPECIALISME", "RELA_ADRES", "RELA_WOONPLAATS", "RELA_POSTCODE", "RELA_LAND", "RELA_TELEFOON", "RELA_SEIN", "RELA_TELEFAX", "RELA_EMAIL", "RELA_INTERNE_POST", "RELA_AGB_CODE", "INST_ID", "INST_CODE", "INST_NAAM", "INST_ADRES", "INST_POSTCODE", "INST_WOONPLAATS", "INST_LAND_ISO_CODE", "INST_TELEFOON", "INST_TELEFAX", "INST_EMAIL", "INST_ICODE", "INST_AGB_CODE", "AANTAL_DECL") AS
  select
null ser_id,
rela.rela_id,
rela.code rela_code,
case
  when substr(upper(replace(rela.briefaanhef, ' ')), 0, 3) = 'MW.' then 'V'
  when substr(upper(replace(rela.briefaanhef, ' ')), 0, 5) = 'MEVR.' then 'V'
  when substr(upper(replace(rela.briefaanhef, ' ')), 0, 4) = 'DHR.' then 'M'
  else 'O'
end geslacht,
case
  when substr(upper(replace(rela.briefaanhef, ' ')), 0, 3) = 'MW.' and
    lower(substr(replace(rela.briefaanhef, ' '), 4)) in
    (select titulatuur from beh_epic_titulaturen) then
    (select code from beh_epic_titulaturen where titulatuur = lower(substr(replace(rela.briefaanhef, ' '), 4)) )

  when substr(upper(replace(rela.briefaanhef, ' ')), 0, 4) = 'DHR.' and
    lower(substr(replace(rela.briefaanhef, ' '), 5)) in
    (select titulatuur from beh_epic_titulaturen) then
    (select code from beh_epic_titulaturen where titulatuur = lower(substr(replace(rela.briefaanhef, ' '), 5)) )

  when lower(replace(rela.briefaanhef, ' ')) in
    (select titulatuur from beh_epic_titulaturen) then
    (select code from beh_epic_titulaturen where titulatuur = lower(replace(rela.briefaanhef, ' ')) )
  else null
end titulatuur,
rela.voorletters rela_voorletters,
rela.voorvoegsel rela_voorvoegsel,
rela.achternaam rela_achternaam,
rela.aanspreken rela_aanspreken,
nvl(spec.epic_cate_number, 9999) rela_specialisme,
case -- enters weghalen
  when trim(replace(rela.adres, chr(10))) is not null
  then trim(replace(rela.adres, chr(10), ', '))
  else null
end rela_adres,
rela.woonplaats rela_woonplaats,
rela.postcode rela_postcode,
upper(kgc_attr_00.waarde('KGC_LANDEN', 'ISO_CODE_3_LETTERIG', (select land_id from kgc_landen where upper(naam) = upper(nvl(rela.land, 'NEDERLAND'))))) rela_land,
case -- goede formaat = 024-1234567 en bu nummers = #0040-123456789
  when instr(rela.telefoon, '-') > 0 then
    case
      when upper(nvl(rela.land, 'NEDERLAND')) = 'NEDERLAND' then
        trim(substr(replace(rela.telefoon, ' '), 0, 11))
      else
        '#'||trim(replace(rela.telefoon, ' '))
    end
  else null
end rela_telefoon,
case
  when instr(rela.telefoon, '*') > 0 then trim(substr(rela.telefoon, instr(rela.telefoon, '*')+1))
  else null
end rela_sein,
case -- goede formaat = 024-1234567 en bu nummers = #0040-123456789
  when instr(rela.telefax, '-') > 0 then
    case
      when upper(nvl(rela.land, 'NEDERLAND')) = 'NEDERLAND' then
        trim(substr(replace(rela.telefax, ' '), 0, 11))
      else
        '#'||trim(replace(rela.telefax, ' '))
    end
  else null
end rela_telefax,
case
 when instr(rela.email, '@') > 0 then rela.email
 else null
end rela_email,
rela.interne_post rela_interne_post,
case
  when kgc_attr_00.waarde('KGC_RELATIES', 'AGB_CODE', rela.rela_id) like '06%' then null
  else kgc_attr_00.waarde('KGC_RELATIES', 'AGB_CODE', rela.rela_id)
  end rela_agb_code,
inst.inst_id inst_id,
inst.code inst_code,
inst.naam inst_naam,
case -- enters weghalen
  when trim(replace(inst.adres, chr(10))) is not null
  then trim(replace(inst.adres, chr(10), ', '))
  else null
end inst_adres,
inst.postcode inst_postcode,
upper(inst.woonplaats) inst_woonplaats,
upper(kgc_attr_00.waarde('KGC_LANDEN', 'ISO_CODE_3_LETTERIG', (select land_id from kgc_landen where upper(naam) = upper(nvl(inst.land, 'NEDERLAND'))))) inst_land_iso_code,
case -- goede formaat = 024-1234567 en bu nummers = #0040-123456789
  when instr(inst.telefoon, '-') > 0 then
    case
      when upper(nvl(inst.land, 'NEDERLAND')) = 'NEDERLAND' then
        trim(substr(replace(inst.telefoon, ' '), 0, 11))
      else
        '#'||trim(replace(inst.telefoon, ' '))
    end
  else null
end inst_telefoon,
case -- goede formaat = 024-1234567 en bu nummers = #0040-123456789
  when instr(inst.telefax, '-') > 0 then
    case
      when upper(nvl(inst.land, 'NEDERLAND')) = 'NEDERLAND' then
        trim(substr(replace(inst.telefax, ' '), 0, 11))
      else
        '#'||trim(replace(inst.telefax, ' '))
    end
  else null
end inst_telefax,
case  -- een geldige email adres heeft een @
 when instr(inst.email, '@') > 0 then inst.email
 else null
end inst_email,
upper(kgc_attr_00.waarde('KGC_INSTELLINGEN', 'FEZ_ICODE', inst.inst_id)) inst_icode,
upper(kgc_attr_00.waarde('KGC_INSTELLINGEN', 'AGB_CODE', inst.inst_id)) inst_agb_code,
(select count(*) from beh_decl_check_vw decl where decl.rela_id = rela.rela_id) aantal_decl
from kgc_relaties rela, kgc_instellingen inst,
(select spec.*, epic.epic_cate_number
from kgc_specialismen spec, beh_epic_helix_spec epic
where spec.spec_id = epic.spec_id(+)) spec
where rela.inst_id = inst.inst_id(+)
and rela.spec_id = spec.spec_id(+)
and nvl(upper(kgc_attr_00.waarde('KGC_RELATIES', 'GECHECKT_IN_EPIC', rela.rela_id)), 'N') = 'J'
and exists
(
select null
from beh_decl_check_vw
where rela.rela_id = rela_id
and decl_check = 'OK'
and pers_check = 'OK'
and rela_check = 'GEEN_SER_ID'
and inst_check = 'OK'
and aanvrager_type != 'PRAKTIJK'
and onde_afgerond = 'J'
);

/
QUIT
