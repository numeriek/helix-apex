CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_OV01_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "ONDE_TYPE", "STATUS", "NOTITIE", "DAGEN_OVER", "FRACTIENR", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  select case -- Openstaande Onderzoeken
           when round (ONDE.GEPLANDE_EINDDATUM - sysdate) > 0 then
             '#5CAB5C' -- groen
           else
             case
               when round(ONDE.GEPLANDE_EINDDATUM - sysdate) <= 0 then '#CD5B5B' -- rood
               else case when ONDE.GEPLANDE_EINDDATUM is null then '#79A8D6' -- blauw
                                                                            end
             end
         end
           as VERBORGEN_KLEUR
       , case
           when round (ONDE.GEPLANDE_EINDDATUM - sysdate) > 0 then
             2 -- groen
           else
             case
               when round(ONDE.GEPLANDE_EINDDATUM - sysdate) <= 0 then 1 -- rood
               else case when ONDE.GEPLANDE_EINDDATUM is null then 3 -- blauw
                                                                    end
             end
         end
           as VERBORGEN_SORTERING
       , ONDE.ONDERZOEKNR as VERBORGEN_TELLING
       , FLFA.MEDE_ID as FAVORIET
       , ONDE.PERS_ID as VERBORGEN_PERS_ID
       , ONGR.CODE as GROEP
       , FAMI.FAMILIENUMMER as FAMILIENR
       , ONDE.ONDERZOEKNR
       , ONDE.SPOED
       , ONDE.DATUM_BINNEN as DAT_BINNEN
       , ONDE.GEPLANDE_EINDDATUM as EINDDATUM
       , to_char (WM_CONCAT (INTE.CODE)) as INDICODE
       , decode (ONDE.ONDERZOEKSTYPE, 'R', 'Research', 'D', 'Diagnostiek', 'C', 'Controle', '') as ONDE_TYPE
       , decode (ONDE.STATUS, 'V', 'Vervallen', 'G', 'Goedgekeurd', 'B', 'Beoordelen', '') as STATUS
       , NOTI.OMSCHRIJVING as NOTITIE
       , round (ONDE.GEPLANDE_EINDDATUM - sysdate) as DAGEN_OVER
       , LISTAGG (FRAC.FRACTIENUMMER, ', ') within group (order by FRAC.FRACTIENUMMER) as FRACTIENR
       , NOTI.NOTI_ID as VERBORGEN_NOTI_ID
       , FLFA.FLOW_ID as VERBORGEN_FLOW_ID
  from   kgc_onderzoeken onde
       , kgc_onderzoeksgroepen ongr
       , kgc_onderzoek_indicaties onin
       , kgc_indicatie_teksten inte
       , kgc_indicatie_groepen ingr
       , kgc_notities noti
       , kgc_familie_leden fale
       , kgc_families fami
       , bas_fracties frac
       , kgc_onderzoek_betrokkenen onbe
       , BEH_FLOW_FAVORIETEN FLFA
  where  ONDE.PERS_ID = FALE.PERS_ID(+)
     and FALE.FAMI_ID = FAMI.FAMI_ID(+)
     and ONDE.ONDE_ID = NOTI.ID(+)
     and (NOTI.ENTITEIT = 'ONDE' or NOTI.ENTITEIT is null)
     and onde.ongr_id = ongr.ongr_id
     and ONDE.ONDE_ID = ONIN.ONDE_ID(+)
     and ONIN.INDI_ID = INTE.INDI_ID(+)
     and INTE.INGR_ID = INGR.INGR_ID(+)
     and ONDE.ONDE_ID = FLFA.ID(+)
     and onde.afgerond = 'N'
     and ONDE.KAFD_ID = 6
     and ONDE.ONDE_ID = ONBE.ONDE_ID(+)
     and ONBE.FRAC_ID = FRAC.FRAC_ID(+)
     and upper (FLFA.ENTITEIT(+)) = upper ('ONDE')
  group by ONDE.GEPLANDE_EINDDATUM, ONDE.ONDERZOEKNR, ONDE.PERS_ID, ONGR.CODE, FAMI.FAMILIENUMMER, ONDE.ONDERZOEKNR, ONDE.SPOED
         , ONDE.DATUM_BINNEN, ONDE.ONDERZOEKSTYPE, ONDE.STATUS, NOTI.OMSCHRIJVING, NOTI.NOTI_ID, FLFA.MEDE_ID, FLFA.FLOW_ID
  order by VERBORGEN_SORTERING asc, ONDE.GEPLANDE_EINDDATUM asc, ONDE.ONDERZOEKNR asc;

/
QUIT
