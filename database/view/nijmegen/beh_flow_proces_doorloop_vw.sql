CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_PROCES_DOORLOOP_VW" ("PATH", "TOTALE_DOORLOOPTIJD") AS
  SELECT                                       -- path is achteruit berekend!
         PATH, TOTALE_DOORLOOPTIJD
     FROM (    SELECT (SELECT DISTINCT (VOLGEND_PROCES_STAP)
                         FROM BEH_FLOW_PROCES_VOLGORDE_VW
                        WHERE VOLGEND_PROCES_STAP NOT IN
                                 (SELECT VOORGAAND_PROCES_STAP
                                    FROM BEH_FLOW_PROCES_VOLGORDE_VW
                                   WHERE VOORGAAND_PROCES_STAP IS NOT NULL))
                      || SYS_CONNECT_BY_PATH (VOORGAAND_PROCES_STAP, '-')
                         PATH,
                      VOORGAAND_PROCES_STAP,
                      LENGTH (
                         REPLACE (
                            SYS_CONNECT_BY_PATH (RPAD (' ', MAXDOORLOOPTIJD),
                                                 '-'),
                            '-'))
                         TOTALE_DOORLOOPTIJD
                 FROM BEH_FLOW_PROCES_VOLGORDE_VW
           START WITH VOLGEND_PROCES_STAP IN
                         (SELECT DISTINCT (VOLGEND_PROCES_STAP)
                            FROM BEH_FLOW_PROCES_VOLGORDE_VW
                           WHERE VOLGEND_PROCES_STAP NOT IN
                                    (SELECT VOORGAAND_PROCES_STAP
                                       FROM BEH_FLOW_PROCES_VOLGORDE_VW
                                      WHERE VOORGAAND_PROCES_STAP IS NOT NULL))
           CONNECT BY NOCYCLE PRIOR VOORGAAND_PROCES_STAP =
                                 VOLGEND_PROCES_STAP)
--WHERE VOORGAAND_PROCES_STAP IS NULL;

/
QUIT
