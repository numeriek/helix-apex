CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_PADEN_DLT_VW" ("FLOW_ID", "PADNR", "STAPNR", "FLDE_ID_VOORG_STAP", "FLDE_ID_VOLG_STAP", "PADNR_PLUS_FLDE_ID_VOORG_STAP", "PADNR_PLUS_FLDE_ID_VOLG_STAP", "VIEW_NAME", "IN_BEREKENING", "DLT_BENODIGDE", "DLT_TOEGESTANE", "DLT_TOTALE", "ONDE_ID", "ONDERZOEKNR", "GEPLANDE_EINDDATUM_CHECK", "DLT_ONDE_DAGEN", "DLT_VERBRUIKT_PERC", "DLT_VERBRUIKT_DAGEN", "DLT_OVERGEBLEVEN_DAGEN", "DLT_HERIJKT_PERC", "DLT_HERIJKT_DAGEN", "DLT_HERIJKT_DAGEN_PAD", "DLT_HERIJKT_DAGEN_CUM", "FLDE_ID_VOORG_STAP_PAD") AS
  SELECT flow_id,
                     padnr,
                     LEVEL stapnr,
                     flde_id_voorg_stap,
                     flde_id_volg_stap,
                     padnr_plus_flde_id_voorg_stap,
                     padnr_plus_flde_id_volg_stap,
                     view_name,
                     in_berekening,
                     dlt_benodigde,
                     dlt_toegestane,
                     dlt_totale,
                     onde_id,
                     onderzoeknr,
                     geplande_einddatum_check,
                     dlt_onde_dagen,
                     dlt_verbruikt_perc,
                     dlt_verbruikt_dagen,
                     dlt_overgebleven_dagen,
                     dlt_herijkt_perc,
                     dlt_herijkt_dagen,
                     SYS_CONNECT_BY_PATH (
                        (DECODE (in_berekening, 'N', 0, dlt_herijkt_dagen)),
                        '+')
                        dlt_herijkt_dagen_pad,
                     ROUND (
                        beh_alfu_00.
                         calc_string (
                           REPLACE (
                              SYS_CONNECT_BY_PATH (
                                 (DECODE (in_berekening, 'N', 0, dlt_herijkt_dagen)),
                                 '+'),
                              ',',
                              '.')),
                        2)
                        dlt_herijkt_dagen_cum,
                     SYS_CONNECT_BY_PATH (flde_id_voorg_stap, '>')
                        flde_id_voorg_stap_pad
                FROM (  SELECT flow_id,
                               padnr,
                               flde_id_voorg_stap,
                               flde_id_volg_stap,
                               padnr_plus_flde_id_voorg_stap,
                               padnr_plus_flde_id_volg_stap,
                               view_name,
                               in_berekening,
                               dlt_benodigde,
                               dlt_toegestane,
                               dlt_totale,
                               onde_id,
                               onderzoeknr,
                               geplande_einddatum_check,
                               dlt_onde_dagen,
                               SUM (dlt_verbrukt_perc) OVER (PARTITION BY padnr)
                                  dlt_verbruikt_perc,
                               SUM (dlt_toeg_of_perc2) OVER (PARTITION BY padnr)
                                  dlt_verbruikt_dagen,
                               (dlt_onde_dagen
                                - SUM (dlt_toeg_of_perc2) OVER (PARTITION BY padnr))
                                  dlt_overgebleven_dagen,
                               ROUND (
                                  dlt_totale
                                  / (SUM (dlt_overgebleven_perc) OVER (PARTITION BY padnr))
                                  * 100,
                                  20)
                                  dlt_herijkt_perc,
                               CASE
                                  WHEN dlt_toeg_of_perc2 > 0
                                  THEN
                                     dlt_toeg_of_perc2
                                  ELSE
                                     ROUND (
                                        ( (dlt_totale
                                           / (SUM (dlt_overgebleven_perc)
                                                 OVER (PARTITION BY padnr))
                                           * 100)
                                         / 100)
                                        * (dlt_onde_dagen
                                           - SUM (dlt_toeg_of_perc2)
                                                OVER (PARTITION BY padnr)),
                                        20)
                               END
                                  dlt_herijkt_dagen
                          FROM (SELECT flow.flow_id,
                                       flpv.padnr,
                                       flpv.flde_id_voorg_stap,
                                       flpv.flde_id_volg_stap,
                                       flpv.padnr_plus_flde_id_voorg_stap,
                                       flpv.padnr_plus_flde_id_volg_stap,
                                       flde.view_name,
                                       flde.in_berekening,
                                       flde.dlt_benodigde,
                                       flde.dlt_toegestane,
                                       flde.dlt_totale,
                                       onde.onde_id,
                                       onde.onderzoeknr,
                                       CASE
                                          WHEN onde.geplande_einddatum IS NOT NULL
                                          THEN
                                             CASE
                                                WHEN TRUNC (onde.geplande_einddatum) <
                                                        TRUNC (onde.datum_binnen)
                                                THEN
                                                   'KLEINER' -- kleinerdan datum binnen
                                                ELSE
                                                   'GOED'
                                             END
                                          ELSE
                                             'LEEG'
                                       END
                                          geplande_einddatum_check,
                                       (TRUNC (onde.geplande_einddatum)
                                        - TRUNC (onde.datum_binnen))
                                          dlt_onde_dagen,
                                       ROUND (
                                          ( (TRUNC (onde.geplande_einddatum)
                                             - TRUNC (onde.datum_binnen))
                                           / 100)
                                          * flde.dlt_totale,
                                          20)
                                          dlt_percentuele,
                                       CASE
                                          WHEN ROUND (
                                                  ( (TRUNC (onde.geplande_einddatum)
                                                     - TRUNC (onde.datum_binnen))
                                                   / 100)
                                                  * flde.dlt_totale,
                                                  20) > flde.dlt_toegestane
                                          THEN
                                             flde.dlt_toegestane
                                          ELSE
                                             ROUND (
                                                ( (TRUNC (onde.geplande_einddatum)
                                                   - TRUNC (onde.datum_binnen))
                                                 / 100)
                                                * flde.dlt_totale,
                                                20)
                                       END
                                          dlt_toeg_of_perc1,
                                       CASE
                                          WHEN flde.dlt_toegestane IS NOT NULL
                                          THEN
                                             CASE
                                                WHEN ROUND (
                                                        ( (TRUNC (onde.geplande_einddatum)
                                                           - TRUNC (onde.datum_binnen))
                                                         / 100)
                                                        * flde.dlt_totale,
                                                        20) > flde.dlt_toegestane
                                                THEN
                                                   flde.dlt_toegestane
                                                ELSE
                                                   ROUND (
                                                      ( (TRUNC (onde.geplande_einddatum)
                                                         - TRUNC (onde.datum_binnen))
                                                       / 100)
                                                      * flde.dlt_totale,
                                                      20)
                                             END
                                          ELSE
                                             0
                                       END
                                          dlt_toeg_of_perc2,
                                       CASE
                                          WHEN flde.dlt_toegestane IS NOT NULL
                                          THEN
                                             dlt_totale
                                          ELSE
                                             0
                                       END
                                          dlt_verbrukt_perc,
                                       CASE
                                          WHEN flde.dlt_toegestane IS NULL THEN dlt_totale
                                          ELSE 0
                                       END
                                          dlt_overgebleven_perc
                                  FROM kgc_onderzoeken onde,
                                       beh_flows flow,
                                       (SELECT flow_id,
                                               flde_id,
                                               view_name,
                                               in_berekening,
                                               DECODE (in_berekening,
                                                       'N', 0,
                                                       dlt_benodigde)
                                                  dlt_benodigde,
                                               DECODE (in_berekening,
                                                       'N', 0,
                                                       dlt_toegestane)
                                                  dlt_toegestane,
                                               DECODE (in_berekening, 'N', 0, dlt_totale)
                                                  dlt_totale,
                                               vervallen
                                          FROM beh_flow_details) flde,
                                       beh_flow_proces_volgorde_vw2 flpv,
                                       beh_flow_kafd_ongr flko
                                 WHERE     flow.flow_id = flde.flow_id
                                       AND flde.flde_id = flpv.flde_id_voorg_stap
                                       AND flow.flow_id = flko.flow_id
                                       AND onde.kafd_id = flko.kafd_id
                                       AND onde.ongr_id = flko.ongr_id
                                       AND flow.vervallen = 'N'
                                       AND flde.vervallen = 'N'
                                       AND flko.vervallen = 'N'
                                       AND onde.onde_id =
                                              TO_NUMBER (
                                                 SYS_CONTEXT ('BEH_FLOW_CONTEXT',
                                                              'ONDE_ID'))
                                       AND flow.flow_id =
                                              TO_NUMBER (
                                                 SYS_CONTEXT ('BEH_FLOW_CONTEXT',
                                                              'FLOW_ID')))
                      ORDER BY flde_id_voorg_stap)
          START WITH flde_id_volg_stap IS NULL
          CONNECT BY PRIOR padnr_plus_flde_id_voorg_stap =
                        padnr_plus_flde_id_volg_stap
   ORDER SIBLINGS BY flde_id_volg_stap;

/
QUIT
