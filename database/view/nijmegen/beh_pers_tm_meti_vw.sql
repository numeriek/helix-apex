CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_PERS_TM_METI_VW" ("PERS_PERS_ID", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GESLACHT", "PERS_GEBOORTEDATUM", "ONDE_ONDE_ID", "ONDE_ONDERZOEKNR", "ONDE_ONDERZOEKSTYPE", "ONDE_DATUM_BINNEN", "ONDE_AFGEROND", "ONDE_DATUM_AUTORISATIE", "ONDE_DECLAREREN", "KAFD_CODE", "ONGR_CODE", "MONS_MONS_ID", "MONS_MONSTERNUMMER", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "MONS_LEEFTIJD", "MATE_MATE_ID", "MATE_CODE", "FRAC_FRAC_ID", "FRAC_FRACTIENUMMER", "METI_METI_ID", "STGR_CODE", "METI_DATUM_AANMELDING", "METI_DECLAREREN", "METI_AFGEROND") AS
  select pers.pers_id,
	   pers.aanspreken,
	   pers.achternaam,
	   pers.geslacht,
	   pers.geboortedatum,
	   onde.onde_id,
	   onde.onderzoeknr,
	   onde.onderzoekstype,
	   onde.datum_binnen,
	   onde.afgerond,
	   onde.datum_autorisatie,
	   onde.declareren,
	   kafd.code,
	   ongr.code,
	   mons.mons_id,
	   mons.monsternummer,
	   mons.datum_aanmelding,
	   mons.datum_afname,
	   mons.leeftijd,
	   mate.mate_id,
	   mate.code,
	   frac.frac_id,
	   frac.fractienummer,
	   meti.meti_id,
	   stgr.code,
	   meti.datum_aanmelding,
	   meti.declareren,
	   meti.afgerond
from kgc_personen pers,
kgc_onderzoeken onde,
kgc_onderzoek_monsters onmo,
kgc_monsters mons,
kgc_materialen mate,
bas_fracties frac,
bas_metingen meti,
kgc_kgc_afdelingen kafd,
kgc_onderzoeksgroepen ongr,
kgc_stoftestgroepen stgr
where pers.pers_id = onde.pers_id
and   onde.onde_id = onmo.onde_id
and   onmo.mons_id = mons.mons_id
and   mons.mate_id = mate.mate_id
and   mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   onde.onde_id = meti.onde_id(+)
and   onde.kafd_id = kafd.kafd_id
and   onde.ongr_id = ongr.ongr_id
and   meti.stgr_id = stgr.stgr_id(+)
 ;

/
QUIT
