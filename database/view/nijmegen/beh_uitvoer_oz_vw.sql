CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_UITVOER_OZ_VW" ("RECO") AS
  select
    '"' || replace(onde.onderzoeknr, '/', '_') || '",' ||
    '"' || replace(mons.monsternummer, '/', '_') || '$' || stgr.code || '",' ||
    '"' || pers.aanspreken || '"'
    reco
from kgc_personen pers
,    kgc_onderzoeken onde
,    kgc_onderzoek_monsters onmo
,    kgc_monsters mons
,    bas_fracties frac
,    bas_metingen meti
,    kgc_kgc_afdelingen kafd
,    kgc_onderzoeksgroepen ongr
,    kgc_stoftestgroepen stgr
,    kgc_materialen mate
where pers.pers_id = onde.pers_id
and   onde.onde_id = onmo.onde_id
and   onmo.mons_id = mons.mons_id
and   mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   onde.onde_id = meti.onde_id
and   onde.kafd_id =  kafd.kafd_id
and   onde.ongr_id = ongr.ongr_id
and   mons.mate_id = mate.mate_id
and   meti.stgr_id = stgr.stgr_id
and   upper(kafd.code) = upper('METAB')
and   upper(ongr.code) = upper('BASIS')
and   upper(stgr.code) = upper('UOZK_UP')
and   upper(mate.code) = upper('UP')
and   upper(onde.afgerond) = upper('N')
order by onde.onderzoeknr
 ;

/
QUIT
