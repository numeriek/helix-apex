CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC8_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING") AS
  SELECT
        '#79A8A8',                                  --FLPR.VERBORGEN_KLEUR,
        ''                                    --FLPR.VERBORGEN_SORTERING,

    FROM dual

--   VERBORGEN_TELLING,
--   FAVORIET,
--   NAAR_HELIX,
--   GROEP,
--   ONDERZOEKNR,
--   SPOED,
--   EINDDATUM,
--   AUTORISATIEDATUM,
--   UITSLAG_CODE,
--   OMSCHRIJVING,
--   WIJZE,
--   ONDE_TYPE,
--   STATUS,
--   VERBORGEN_FLOW_ID

--     SELECT                                  -- Overzicht "Te printen brieven"
--           FLPR.VERBORGEN_KLEUR,
--            FLPR.VERBORGEN_SORTERING,
--            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
--            FLFA.MEDE_ID AS FAVORIET,
--            ONDE.PERS_ID AS NAAR_HELIX,
--            ONGR.CODE AS GROEP,
--            ONDE.ONDERZOEKNR,
--            ONDE.SPOED,
--            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
--            ONDE.DATUM_AUTORISATIE AS AUTORISATIEDATUM,
--            ONUI.CODE AS UITSLAG_CODE,
--            ONUI.OMSCHRIJVING,
--            ONWY.OMSCHRIJVING AS WIJZE,
--            DECODE (ONDE.ONDERZOEKSTYPE,
--                    'R', 'Research',
--                    'D', 'Diagnostiek',
--                    '')
--               AS ONDE_TYPE,
--            DECODE (ONDE.STATUS,
--                    'V', 'Vervallen',
--                    'G', 'Goedgekeurd',
--                    'B', 'Beoordelen',
--                    '')
--               AS STATUS,
--            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
--       FROM kgc_onderzoeken onde,
--            kgc_onderzoeksgroepen ongr,
--            kgc_onderzoek_uitslagcodes onui,
--            kgc_uitslagen uits,
--            kgc_onderzoekswijzen onwy,
--            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
--            beh_flow_favorieten flfa
--      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC8_VW'
--            AND ONDE.KAFD_ID = flpr.kafd_id
--            AND ONDE.ONDE_ID = FLPR.ONDE_ID
--            AND ONGR.ONGR_ID = ONDE.ONGR_ID
--            AND UITS.ONDE_ID = ONDE.ONDE_ID
--            AND ONDE.ONUI_ID = ONUI.ONUI_ID
--            AND ONDE.KAFD_ID = ONWY.KAFD_ID
--            AND ONDE.ONDE_ID = FLFA.ID(+)
--            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
--            AND ONDE.AFGEROND = 'J'
--            AND ONUI.CODE <> '04'
--            AND ONDE.ONDE_ID NOT IN
--                   (SELECT uits1.onde_id
--                      FROM kgc_uitslagen uits1
--                     WHERE uits1.uits_id IN
--                              (SELECT BRIE.UITS_ID
--                                 FROM kgc_brieven brie, kgc_brieftypes brty
--                                WHERE BRIE.BRTY_ID = BRTY.BRTY_ID
--                                      AND BRTY.CODE <> 'UITSLAG_T'))
--            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
--   ORDER BY VERBORGEN_SORTERING,
--            ONDE.GEPLANDE_EINDDATUM ASC,
--            ONDE.ONDERZOEKNR ASC;
--
--;

/
QUIT
