CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DIPO_BERICHTEN_MISLUKT_VW" ("VOLGNR", "ENTITEIT_ID", "CREATION_DATE_EERSTE_KEER", "CREATION_DATE_LAATSTE_KEER", "VERZONDEN_EERSTE_KEER", "VERZONDEN_LAATSTE_KEER", "LOG_TEKST_EERSTE_KEER", "LOG_TEKST_LAATSTE_KEER") AS
  SELECT VOLGNR,
          entiteit_id,
          CREATION_DATE_EERSTE_KEER,
          CREATION_DATE_LAATSTE_KEER,
          VERZONDEN_EERSTE_KEER,
          VERZONDEN_LAATSTE_KEER,
          LOG_TEKST_EERSTE_KEER,
          LOG_TEKST_LAATSTE_KEER
     FROM (  SELECT ROW_NUMBER ()
                    OVER (PARTITION BY beri.entiteit_id
                          ORDER BY beri.creation_date)
                       volgnr,
                    beri.entiteit_id entiteit_id,
                    MIN (beri.creation_date)
                       KEEP (DENSE_RANK FIRST ORDER BY beri.creation_date)
                       OVER (PARTITION BY beri.entiteit_id)
                       creation_date_eerste_keer,
                    MAX (beri.creation_date)
                       KEEP (DENSE_RANK LAST ORDER BY beri.creation_date)
                       OVER (PARTITION BY beri.entiteit_id)
                       creation_date_laatste_keer,
                    MIN (beri.verzonden)
                       KEEP (DENSE_RANK FIRST ORDER BY beri.creation_date)
                       OVER (PARTITION BY beri.entiteit_id)
                       verzonden_eerste_keer,
                    MAX (beri.verzonden)
                       KEEP (DENSE_RANK LAST ORDER BY beri.creation_date)
                       OVER (PARTITION BY beri.entiteit_id)
                       verzonden_laatste_keer,
                    MIN (beri.log_tekst)
                       KEEP (DENSE_RANK FIRST ORDER BY beri.creation_date)
                       OVER (PARTITION BY beri.entiteit_id)
                       log_tekst_eerste_keer,
                    MAX (beri.log_tekst)
                       KEEP (DENSE_RANK LAST ORDER BY beri.creation_date)
                       OVER (PARTITION BY beri.entiteit_id)
                       log_tekst_laatste_keer
               FROM beh_berichten beri, beh_bericht_types bety
              WHERE     beri.bety_id = bety.bety_id
                    AND UPPER (beri.entiteit) = UPPER ('ZWAN')
                    AND UPPER (beri.ontvanger_type) = UPPER ('PERS')
                    AND bety.code = UPPER ('SMS')
           ORDER BY beri.beri_id, beri.creation_date)
    WHERE     VolgNR = 1
          AND VERZONDEN_EERSTE_KEER = 'N'
          AND VERZONDEN_LAATSTE_KEER = 'N'
          AND creation_date_EERSTE_KEER >
                 (SYSDATE
                  - TO_NUMBER (
                       kgc_sypa_00.
                        systeem_waarde ('BEH_TERMIJN_DIPO_BERI_MISLUKT')))
 ;

/
QUIT
