CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST15_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND", "ONDE_TYPE", "WIJZE", "BINNENGEKOMEN") AS
  SELECT '#FFFFFF',
            1,
            '',
            TO_CHAR (ONDE.DATUM_BINNEN, 'YYYY-MM'),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONWY.OMSCHRIJVING,
            COUNT (DISTINCT onde.onderzoeknr)
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND TO_CHAR (ONDE.DATUM_BINNEN, 'YYYY') >= ('2008')
            AND ONGR.CODE IN ('ARR', 'LTG', 'NGS', 'P', 'PR', 'RFD', 'TML')
   GROUP BY TO_CHAR (ONDE.DATUM_BINNEN, 'YYYY-MM'),
            ONDE.ONDERZOEKSTYPE,
            ONWY.OMSCHRIJVING
   ORDER BY TO_CHAR (ONDE.DATUM_BINNEN, 'YYYY-MM') DESC;

/
QUIT
