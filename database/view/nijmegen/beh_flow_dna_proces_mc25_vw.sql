CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC25_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "PROTOCOL", "ST_NIET_WLST_NIET_AFG", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            FAVORIET,
            NAAR_HELIX,
            GROEP,
            ONDERZOEKNR,
            SPOED,
            EINDDATUM,
            INDICODE,
            INGRCODE,
            WIJZE,
            ONDE_TYPE,
            FRACTIENR,
            PROTOCOL,
            LISTAGG (ST_NIET_WLST_NIET_AFG, ', ')
               WITHIN GROUP (ORDER BY ST_NIET_WLST_NIET_AFG)
               AS ST_NIET_WLST_NIET_AFG,
            NOTITIE,
            VERBORGEN_NOTI_ID,
            VERBORGEN_FLOW_ID
       FROM (  SELECT FLPR.VERBORGEN_KLEUR,
                      FLPR.VERBORGEN_SORTERING,
                      ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
                      FLFA.MEDE_ID AS FAVORIET,
                      ONDE.PERS_ID AS NAAR_HELIX,
                      ONGR.CODE AS GROEP,
                      ONDE.ONDERZOEKNR,
                      ONDE.SPOED,
                      ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
                      INTE.CODE AS INDICODE,
                      INGR.CODE AS INGRCODE,
                      ONWY.OMSCHRIJVING AS WIJZE,
                      DECODE (ONDE.ONDERZOEKSTYPE,
                              'R', 'Research',
                              'D', 'Diagnostiek',
                              '')
                         AS ONDE_TYPE,
                      FRAC.FRACTIENUMMER AS FRACTIENR,
                      STGR.CODE AS PROTOCOL,
                      CASE
                         WHEN TECH.CODE = 'SEQ' THEN 'SEQ: ' || COUNT (*)
                         ELSE 'GS: ' || COUNT (*)
                      END
                         AS ST_NIET_WLST_NIET_AFG,
                      NOTI.OMSCHRIJVING AS NOTITIE,
                      NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
                      FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
                 FROM kgc_onderzoeken onde,
                      kgc_onderzoeksgroepen ongr,
                      kgc_onderzoek_indicaties onin,
                      kgc_indicatie_teksten inte,
                      kgc_indicatie_groepen ingr,
                      bas_metingen meti,
                      bas_meetwaarden meet,
                      bas_fracties frac,
                      kgc_stoftesten stof,
                      kgc_stoftestgroepen stgr,
                      kgc_onderzoekswijzen onwy,
                      kgc_notities noti,
                      kgc_technieken tech,
                      BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
                      beh_flow_favorieten flfa
                WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC25_VW'
                      AND ONDE.KAFD_ID = FLPR.KAFD_ID
                      AND ONDE.ONDE_ID = FLPR.ONDE_ID
                      AND ONGR.ONGR_ID = ONDE.ONGR_ID
                      AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
                      AND ONDE.KAFD_ID = ONWY.KAFD_ID
                      AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
                      AND ONIN.INDI_ID = INTE.INDI_ID(+)
                      AND INTE.INGR_ID = INGR.INGR_ID(+)
                      AND ONDE.ONDE_ID = METI.ONDE_ID
                      AND METI.METI_ID = MEET.METI_ID
                      AND FRAC.FRAC_ID = METI.FRAC_ID
                      AND STOF.STOF_ID = MEET.STOF_ID
                      AND STGR.STGR_ID = METI.STGR_ID
                      AND STOF.TECH_ID = TECH.TECH_ID
                      AND ONDE.ONDE_ID = FLFA.ID(+)
                      AND ONDE.ONDE_ID = NOTI.ID(+)
                      AND NOTI.ENTITEIT(+) = 'ONDE'
                      AND NOTI.GEREED(+) = 'N'
                      AND ONDE.AFGEROND = 'N'
                      AND ONDE.STATUS = 'G'
                      AND MEET.AFGEROND = 'N'
                      AND MEET.MEET_REKEN = 'M'
                      AND STGR.CODE NOT LIKE '%!_R' ESCAPE '!'
                      AND UPPER (STGR.OMSCHRIJVING) NOT LIKE '%ROBOT%'
                      AND TECH.CODE IN ('SEQ', 'GS')
                      AND MEET.MEET_ID NOT IN (SELECT WLIT1.MEET_ID
                                                 FROM KGC_WERKLIJST_ITEMS WLIT1)
                      AND STOF.STOF_ID_VOOR NOT IN
                             (SELECT MEETV.STOF_ID
                                FROM BAS_MEETWAARDEN MEETV
                               WHERE     MEETV.METI_ID = METI.METI_ID
                                     AND STOF.STOF_ID_VOOR = MEETV.STOF_ID
                                     AND MEETV.AFGEROND = 'N')
                      AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
             GROUP BY FLPR.VERBORGEN_KLEUR,
                      FLPR.VERBORGEN_SORTERING,
                      ONDE.ONDERZOEKNR,
                      FLFA.MEDE_ID,
                      ONDE.PERS_ID,
                      ONGR.CODE,
                      ONDE.ONDERZOEKNR,
                      ONDE.SPOED,
                      ONDE.GEPLANDE_EINDDATUM,
                      INTE.CODE,
                      INGR.CODE,
                      ONWY.OMSCHRIJVING,
                      DECODE (ONDE.ONDERZOEKSTYPE,
                              'R', 'Research',
                              'D', 'Diagnostiek',
                              ''),
                      FRAC.FRACTIENUMMER,
                      STGR.CODE,
                      TECH.CODE,
                      NOTI.OMSCHRIJVING,
                      NOTI.NOTI_ID,
                      FLFA.FLOW_ID
             ORDER BY ST_NIET_WLST_NIET_AFG DESC) VW
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            FAVORIET,
            NAAR_HELIX,
            GROEP,
            ONDERZOEKNR,
            SPOED,
            EINDDATUM,
            INDICODE,
            INGRCODE,
            WIJZE,
            ONDE_TYPE,
            FRACTIENR,
            PROTOCOL,
            NOTITIE,
            VERBORGEN_NOTI_ID,
            VERBORGEN_FLOW_ID
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            EINDDATUM ASC,
            ONDERZOEKNR ASC,
            FRACTIENR ASC;

/
QUIT
