CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_HELP_TECH_ONDE_WLSTS" ("ONDE_ID", "TECH_ID", "WERKLIJSTNRS") AS
  with dist_tech_onde_wlsts as
( select distinct meti.onde_id
       , stof.tech_id
       , wlst.werklijst_nummer
  from   kgc_werklijsten wlst
    join kgc_werklijst_items wlit on wlit.wlst_id = wlst.wlst_id
    join bas_metingen        meti on meti.meti_id = wlit.meti_id
    join kgc_stoftesten      stof on stof.stof_id = wlit.stof_id
  where stof.code not like '%KASP%'
    and stof.meet_reken in ('M', 'S')
    and not exists
    ( select 1
      from bas_meetwaarden meet
      where meet.afgerond  = 'N'
      and meet.meti_id     = wlit.meti_id
      and meet.meet_reken in('S', 'M')
    )
    and exists
    ( select 1
      from bas_meetwaarden meet
      where meet.mest_id  is null
      and meet.meti_id     = wlit.meti_id
      and meet.meet_reken in('S', 'M')
    )
)
select onde_id
     , tech_id
     , listagg(werklijst_nummer, ',') WITHIN GROUP (ORDER BY werklijst_nummer)
from   dist_tech_onde_wlsts
group by onde_id, tech_id;

/
QUIT
