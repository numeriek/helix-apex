CREATE OR REPLACE FORCE VIEW "HELIX"."VLOKKKLDAN10" ("ONDERZOEKNR", "GROEP", "DATUM_BINNEN", "DATUM_AUTORISATIE", "VERSCHIL", "INDICATIE", "MATERIAAL") AS
  select    onde.onderzoeknr
       ,  decode(onde.ongr_id, 16,'PRE_ARN',17,'PRE_ENS', 18,'PRE_NIJM') groep
       ,  onde.datum_binnen
	   ,  onde.datum_autorisatie
       , (onde.datum_autorisatie - onde.datum_binnen) verschil
       ,  max(indi.code) indicatie	
       ,  max(mate.code) materiaal
from kgc_onderzoek_monsters onmo,
	kgc_onderzoeken onde,
	kgc_monsters mons,
	kgc_onderzoek_indicaties onin,
	kgc_indicatie_teksten indi,
	kgc_materialen mate
where onmo.onde_id = onde.onde_id
and   onmo.mons_id = mons.mons_id
and   onde.onde_id = onin.onde_id
and   onin.indi_id = indi.indi_id
and   mons.mate_id = mate.mate_id
and   onde.kafd_id = 3 --cyto
and   onde.ongr_id in (16,17,18) --PRE_ARN, PRE_ENS, PRE_NIJM
and   (onde.datum_autorisatie - mons.datum_aanmelding) <= 10
and   to_char(datum_aanmelding, 'YYYY') = 2003
and   mate.code in ('C')
group by onde.onderzoeknr
	  , decode(onde.ongr_id, 16,'PRE_ARN',17,'PRE_ENS', 18,'PRE_NIJM')
	  , onde.datum_binnen
	  , onde.datum_autorisatie
	  , (onde.datum_autorisatie - onde.datum_binnen)

 ;

/
QUIT
