CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST4_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND", "GROEP", "INDI_GROEP", "ONDE_TYPE", "WIJZE", "GEREALISEERDE_DOORLOOPTIJD", "AANTAL") AS
  SELECT '#FFFFFF',
            1,
            '',
            TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM') AS JAAR_MAAND,
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONWY.OMSCHRIJVING,
            'OK' AS Gerealiseerde_Doorlooptijd,
            COUNT (DISTINCT ONDE.ONDERZOEKNR)
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.AFGEROND = 'J'
            AND ONDE.DATUM_AUTORISATIE <= ONDE.GEPLANDE_EINDDATUM
            AND TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY') >= 2010
            AND ONGR.CODE IN ('LTG', 'NGS', 'P', 'PR', 'RFD', 'TML')
   GROUP BY TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM'),
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            ONDE.ONDERZOEKSTYPE,
            ONWY.OMSCHRIJVING
   UNION
     SELECT '#FFFFFF',
            1,
            '',
            TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM') AS JAAR_MAAND,
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONWY.OMSCHRIJVING,
            'TE LAAT' AS Effe,
            COUNT (DISTINCT ONDE.ONDERZOEKNR)
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.AFGEROND = 'J'
            AND ONDE.DATUM_AUTORISATIE > ONDE.GEPLANDE_EINDDATUM
            AND TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY') >= 2010
            AND ONGR.CODE IN ('LTG', 'NGS', 'P', 'PR', 'RFD', 'TML')
   GROUP BY TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM'),
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            ONDE.ONDERZOEKSTYPE,
            ONWY.OMSCHRIJVING
   UNION
     SELECT '#FFFFFF',
            1,
            '',
            TO_CHAR (UITS.LAST_UPDATE_DATE, 'YYYY-MM') AS JAAR_MAAND,
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONWY.OMSCHRIJVING,
            'OK' AS Gerealiseerde_Doorlooptijd,
            COUNT (DISTINCT ONDE.ONDERZOEKNR)
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_uitslagen uits
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.ONDE_ID = UITS.ONDE_ID(+)
            AND ONDE.AFGEROND = 'J'
            AND TO_DATE (UITS.LAST_UPDATE_DATE, 'dd-mm-rrrr') <=
                   ONDE.GEPLANDE_EINDDATUM
            AND TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY') >= 2010
            AND ONGR.CODE = 'ARR'
            AND (ONDE.ONDE_ID, UITS.LAST_UPDATE_DATE) IN
                   (  SELECT uits.onde_id, MIN (UITS.LAST_UPDATE_DATE)
                        FROM kgc_uitslagen uits
                    GROUP BY UITS.ONDE_ID)
   GROUP BY TO_CHAR (UITS.LAST_UPDATE_DATE, 'YYYY-MM'),
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            ONDE.ONDERZOEKSTYPE,
            ONWY.OMSCHRIJVING
   UNION
     SELECT '#FFFFFF',
            1,
            '',
            TO_CHAR (UITS.LAST_UPDATE_DATE, 'YYYY-MM') AS JAAR_MAAND,
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONWY.OMSCHRIJVING,
            'TE LAAT' AS Effe,
            COUNT (DISTINCT ONDE.ONDERZOEKNR)
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_uitslagen uits
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.ONDE_ID = UITS.ONDE_ID(+)
            AND ONDE.AFGEROND = 'J'
            AND TO_DATE (UITS.LAST_UPDATE_DATE, 'dd-mm-rrrr') >
                   ONDE.GEPLANDE_EINDDATUM
            AND TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY') >= 2010
            AND ONGR.CODE = 'ARR'
            AND (ONDE.ONDE_ID, UITS.LAST_UPDATE_DATE) IN
                   (  SELECT uits.onde_id, MIN (UITS.LAST_UPDATE_DATE)
                        FROM kgc_uitslagen uits
                    GROUP BY UITS.ONDE_ID)
   GROUP BY TO_CHAR (UITS.LAST_UPDATE_DATE, 'YYYY-MM'),
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            ONDE.ONDERZOEKSTYPE,
            ONWY.OMSCHRIJVING
   ORDER BY JAAR_MAAND DESC;

/
QUIT
