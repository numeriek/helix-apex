CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC03_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "MONSTERNR", "FRACTIENUMMER", "KWEEKTYPE", "DATUM_INZET", "DATUM_UIT_TE_HALEN", "NOTITIE", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED (+) = 'N'
                    AND NOTI.ENTITEIT = 'ONDE')
     SELECT                                      -- uit te halen kweekfracties
           '#79A8A8',                                  --FLPR.VERBORGEN_KLEUR,
            '',                                    --FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            MONS.MONSTERNUMMER,
            FRAC.FRACTIENUMMER,
            KWTY.CODE,
            KWFR.DATUM_INZET,
            KWFR.DATUM_UIT_TE_HALEN,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_monsters onmo,
            kgc_monsters mons,
            kgc_kweekfracties kwfr,
            kgc_kweektypes kwty,
            bas_fracties frac,
            kgc_onderzoeksgroepen ongr,
            W_KGC_NOTITIES noti,
            --BEH_FLOW_PRIORITEIT_NEW_VW flpr,
            beh_flow_favorieten flfa
      WHERE                  --FLPR.VIEW_NAME = 'BEH_FLOW_CYTO_PROCES_MC01_VW'
                --AND
                ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND MONS.MONS_ID = ONMO.MONS_ID
            AND KWFR.FRAC_ID = FRAC.FRAC_ID
            AND FRAC.MONS_ID = MONS.MONS_ID
            AND KWFR.KWTY_ID = KWTY.KWTY_ID
            --AND ONDE.KAFD_ID = FLPR.KAFD_ID
            --AND FLPR.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND KWFR.DATUM_UITHAAL IS NULL
            AND KWFR.VERVALLEN <> 'J'
   GROUP BY                                            --FLPR.VERBORGEN_KLEUR,
            --FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            ONDE.ONDE_ID,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            MONS.MONSTERNUMMER,
            FRAC.FRACTIENUMMER,
            KWTY.CODE,
            KWFR.DATUM_INZET,
            KWFR.DATUM_UIT_TE_HALEN,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            NOTI.GEREED,
            FLFA.FLOW_ID
   ORDER BY                                             --VERBORGEN_SORTERING,
           ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
