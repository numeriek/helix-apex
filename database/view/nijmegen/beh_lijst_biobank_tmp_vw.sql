CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_LIJST_BIOBANK_TMP_VW" ("ONDERZOEKNR", "INDICATIES", "PERSOON", "ZISNR", "GEBOORTEDATUM", "FAMILIE", "AANVRAGER", "MONSTERNUMMER") AS
  select-- count(*)
 onde.onderzoeknr
, beh_onderzoek_indicaties(onde.onde_id) indicaties
, pers.aanspreken persoon
, PERS.ZISNR
, pers.geboortedatum
, kgc_pers_00.families(pers.pers_id, null) familienummer
, rela.aanspreken aanvrager
, mons.monsternummer
from KGC_ONDERZOEKEN onde
, kgc_onderzoek_monsters onmo
, kgc_personen pers
, kgc_relaties rela
, kgc_monsters mons
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
where onde.pers_id = pers.pers_id
  and onde.rela_id = rela.rela_id
  and onde.kafd_id = kafd.kafd_id
  and onde.ongr_id = ongr.ongr_id
  and onde.onde_id = onmo.onde_id
  and onmo.mons_id = mons.mons_id
  and kafd.code = 'GENOOM'
  and ongr.code = 'NGS'
  and ONDE.DATUM_BINNEN > to_date('01-01-2014','dd-mm-yyyy')
  and exists (select *
              from kgc_onderzoek_indicaties onin
              , kgc_indicatie_teksten indi
              where onde.onde_id = onin.onde_id
              and onin.indi_id = indi.indi_id
              and indi.code in ( 'CFA', 'CFA_03', 'CFA_C'
                            , 'DSD', 'DSD_03', 'DSD_C'
                             , 'HH' , 'HH_03' , 'HH_C'
                             , 'MCA', 'MCA_03', 'MCA_C'
                             , 'MIM_MM', 'MIM_M' || chr(38) || 'M_03', 'MIM_M' || chr(38) || 'M_2C', 'MIM_M' || chr(38) || 'M_C'
                             , 'MR', 'MR_01', 'MR_03', 'MR_C'
                             , 'MUL_M' || chr(38) || 'M', 'MUL_M' || chr(38) || 'M_02', 'MUL_M' || chr(38) || 'M_03', 'MUL_M' || chr(38) || 'M_2C', 'MUL_M' || chr(38) || 'M_C')
           );

/
QUIT
