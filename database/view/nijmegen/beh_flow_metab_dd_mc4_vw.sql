CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_DD_MC4_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "BRIEFTYPE", "ONDERTEKENAAR1", "ONDERTEKEND_OP1", "ONDERTEKENAAR2", "ONDERTEKEND_OP2", "UITSLAG_CODE", "OMSCHRIJVING", "WIJZE", "ONDE_TYPE", "STATUS", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT                                      -- te autoriseren onderzoeken
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            BRTY.CODE AS BRIEFTYPE,
            MEDE.CODE AS ONDERTEKENAAR1,
            UITS.DATUM_MEDE_ID AS ONDERTEKEND_OP1,
            MEDE2.CODE AS ONDERTEKENAAR2,
            UITS.DATUM_MEDE_ID2 AS ONDERTEKEND_OP2,
            ONUI.CODE AS UITSLAG_CODE,
            ONUI.OMSCHRIJVING,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_uitslagen uits,
            kgc_brieftypes brty,
            kgc_medewerkers mede,
            kgc_medewerkers mede2,
            kgc_onderzoek_uitslagcodes onui,
            kgc_onderzoekswijzen onwy,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_notities noti,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_METAB_DD_MC4_VW'
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND UITS.ONDE_ID = ONDE.ONDE_ID
            AND UITS.BRTY_ID = BRTY.BRTY_ID
            AND UITS.MEDE_ID = MEDE.MEDE_ID(+)
            AND UITS.MEDE_ID2 = MEDE2.MEDE_ID(+)
            AND ONDE.ONUI_ID = ONUI.ONUI_ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND (NOTI.ENTITEIT = 'ONDE' OR NOTI.ENTITEIT IS NULL)
            AND ONDE.AFGEROND = 'N'
            AND (UITS.MEDE_ID IS NOT NULL OR UITS.MEDE_ID2 IS NOT NULL)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
