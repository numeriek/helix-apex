CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC53_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "BEVESTIGEN", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT               -- Overzicht NGS - testen dienen bevestigd te worden
           '#79A8A8' AS VERBORGEN_KLEUR,
            '' AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            PVT.BEVESTIGEN,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            kgc_stoftestgroepen stgr,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            beh_flow_help_fami_vw fami,
            kgc_onderzoekswijzen onwy,
            beh_flow_favorieten flfa,
            (SELECT meet_id, BEVESTIGEN
               FROM BEH_FLOW_NGS_EXA_S_MDET_VW
              WHERE BEVESTIGEN = 'J') pvt
      WHERE     ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FAMI.PERS_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID(+)
            AND WLIT.WLST_ID = WLST.WLST_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND meti.stgr_id = stgr.stgr_id
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND MEET.MEET_ID = PVT.MEET_ID
            AND ONDE.AFGEROND = 'N'
            AND (STGR.CODE = 'NGS_RESEAR' OR STGR.CODE LIKE 'NGS_%_D') -- mantisnr 11373
            AND NOT EXISTS
                       (SELECT 1                              -- METI1.ONDE_ID
                          FROM bas_metingen meti1,
                               bas_meetwaarden meet1,
                               kgc_stoftesten stof1,
                               kgc_technieken tech1
                         WHERE     METI1.ONDE_ID = ONDE.ONDE_ID
                               AND METI1.METI_ID = MEET1.METI_ID
                               AND MEET1.STOF_ID = STOF1.STOF_ID
                               AND STOF1.TECH_ID = TECH1.TECH_ID
                               AND TECH1.CODE = 'SEQ')
   GROUP BY ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            FAMI.FAMILIENUMMER,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            FRAC.FRACTIENUMMER,
            WLST.WERKLIJST_NUMMER,
            FLFA.FLOW_ID,
            PVT.BEVESTIGEN,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID
   ORDER BY ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC,
            FRAC.FRACTIENUMMER ASC;

/
QUIT
