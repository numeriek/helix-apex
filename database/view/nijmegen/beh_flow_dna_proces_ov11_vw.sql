CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_OV11_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "BI_BU", "WIJZE", "ONDE_TYPE", "AANVRAAG", "SPOED", "EINDDATUM", "DATUM_AUTORISATIE", "AUTORISATOR", "INDICODE", "NOTI_CODE", "NOTITIE", "ONDTKNR1", "ONDTKND_OP1", "ONDTKNR2", "ONDTKND_OP2", "UITSLAG_CODE", "OMSCHRIJVING", "UITS_COMMENTAAR", "INGRCODE", "BRIEFTYPE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_BESTANDEN
        AS (SELECT BEST.ENTITEIT_PK,
                   BEST.ENTITEIT_CODE,
                   beh_bestanden_to_string (
                      CAST (
                         COLLECT (
                               '<a href="'
                            || BEST.BESTAND_SPECIFICATIE
                            || '" target="_blank">Open</a>' ORDER                            BY BEST.BEST_ID desc ) as beh_bestanden_link)) as bestanden
                FROM KGC_BESTANDEN BEST, KGC_BESTAND_CATEGORIEEN BCAT
                WHERE BEST.BCAT_ID = BCAT.BCAT_ID
                    AND BEST.ENTITEIT_CODE  = 'ONDE'
                    AND BCAT.CODE <> ('DUMMY')
                    AND BCAT.CODE = 'AANVRAAG'
                    AND best.vervallen ='N'
                    AND best.verwijderd = 'N'
                group by best.ENTITEIT_PK, BEST.ENTITEIT_CODE)
     SELECT                                      -- geautoriseerde onderzoeken in afgelopen 2 weken
           '#79A8A8' VERBORGEN_KLEUR,
            '' VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            --            TAAL.CODE TAAL,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            BEST.BESTANDEN,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            ONDE.DATUM_AUTORISATIE,
            MEDE3.CODE as AUTORISATOR,
            INTE.CODE AS INDICODE,
            NOTI.CODE AS NOTICODE,
            NOTI.OMSCHRIJVING AS NOTITIE,
--            NOTI.GEREED AS NOTI_GEREED,
            MEDE.CODE AS ONDTKNR1,
            UITS.DATUM_MEDE_ID AS ONDTKND_OP1,
            MEDE2.CODE AS ONDTKNR2,
            UITS.DATUM_MEDE_ID2 AS ONDTKND_OP2,
            ONUI.CODE AS UITSLAG_CODE,
            ONUI.OMSCHRIJVING,
            UITS.COMMENTAAR AS UITS_COMMENTAAR,
            INGR.CODE AS INGRCODE,
            BRTY.CODE AS BRIEFTYPE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_uitslagen uits,
            kgc_brieftypes brty,
            kgc_medewerkers mede,
            kgc_medewerkers mede2,
            kgc_medewerkers mede3,
            kgc_onderzoek_uitslagcodes onui,
            kgc_onderzoekswijzen onwy,
            kgc_familie_leden fale,
            kgc_families fami,
            kgc_notities noti,
            w_kgc_bestanden best,
--            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     --FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_OV11_VW'
            --AND
            ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 1) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
--            AND ONDE.KAFD_ID = FLPR.KAFD_ID
--            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = UITS.ONDE_ID(+)
            AND UITS.BRTY_ID = BRTY.BRTY_ID(+)
            AND UITS.MEDE_ID = MEDE.MEDE_ID(+)
            AND UITS.MEDE_ID2 = MEDE2.MEDE_ID(+)
            AND ONDE.ONUI_ID = ONUI.ONUI_ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.MEDE_ID_AUTORISATOR = MEDE3.MEDE_ID
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.ONDE_ID = BEST.ENTITEIT_PK(+)
            AND ONDE.AFGEROND = 'J'
            AND ONDE.DATUM_AUTORISATIE > sysdate - 14
--            AND (   UITS.MEDE_ID IS NOT NULL
--                 OR UITS.MEDE_ID2 IS NOT NULL
--                 OR (ONGR.CODE = 'ARR' AND UITS.ONDE_ID IS NOT NULL)
--                 OR (ONGR.CODE = 'ARR' AND UITS.ONDE_ID IS NULL AND NOTI.CODE = 'GEREED_AUTORISATIE')
--                 OR NOTI.CODE IN ('GEREED_AUTORISATIE', 'STAF_ONDE_VERV')
--                 OR INTE.CODE = 'OPSLAG')
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
--            AND NOT EXISTS
--                       (SELECT 1
--                          FROM kgc_uitslagen uits1--, kgc_notities noti1
--                         WHERE     UITS1.ONDE_ID = ONDE.ONDE_ID
--                               AND UITS1.MEDE_ID IS NULL
--                               AND UITS1.MEDE_ID2 IS NULL
--                               AND ONDE.ONGR_ID = ONGR.ONGR_ID -- MG 2016-07-08: toegevoegd om array uitslagen zonder ondertekenaar toch meenemen --> in de lijn met de eerdere criteria hierboven - uitslagen van array hoeven niet ondertekend te worden
--                               and ONGR.CODE <> 'ARR' -- MG 2016-07-08: toegevoegd om array uitslagen zonder ondertekenaar toch meenemen --> in de lijn met de eerdere criteria hierboven - uitslagen van array hoeven niet ondertekend te worden
--                               AND not exists (select 1 from kgc_notities noti1 where NOTI1.ID = UITS1.ONDE_ID and NOTI1.CODE IN ('GEREED_AUTORISATIE', 'STAF_ONDE_VERV') ) -- MG 2016-07-08: criterium tav notities gewijzigd omdat onderzoeken met deze notities toch niet meegenomen werden
--                               ) --toegevoegd nav aanvullende uitslagen die nog niet ondertekend waren
--            AND NOT EXISTS
--                   (SELECT 1
--                      FROM kgc_brieven brie1
--                     WHERE BRIE1.UITS_ID = UITS.UITS_ID) -- toegevoegd nav aanvullende uitslagen -- nog even met Annet overleggen over geopende onderzoeken maar geen aanvullend brief en geen correctie in brief!!!, deze blijven door dit buiten beschouwing, maar wel nodig voor aanvullende brieven.
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.DATUM_AUTORISATIE DESC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
