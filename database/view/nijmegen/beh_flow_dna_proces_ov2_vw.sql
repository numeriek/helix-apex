CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_OV2_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "GROEP", "ONDERZOEKNR", "GEPLANDE_EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "PCRPLATEBARCODE", "DNA_ID", "PRIMER_ID", "ELUTION_BARCODE", "STATE", "SEQ_PLATE_ID", "FAVORIET", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT                                   -- Overzicht Boven kant
            '#FFFFFF' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            '' AS VERBORGEN_TELLING,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            POST.PCRPLATEBARCODE,
            POST.DNA_ID,
            POST.PRIMER_ID,
            POST.ELUTION_BARCODE,
            POST.STATE,
            SEQ.SEQUENCEPLATEID,
            FLFA.MEDE_ID AS FAVORIET,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM beh_flow_dna_postrobot POST,
            kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            BEH_FLOW_DNA_SEQUENCEFACILITY SEQ,
            beh_flow_favorieten flfa
      WHERE     POST.INVESTIGATION_ID = ONDE.ONDERZOEKNR
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND POST.ELUTION_BARCODE = SEQ.ELUTION_BARCODE(+)
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY POST.STATE,
            POST.ELUTION_BARCODE,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
