CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_OV02_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "STATUS", "MONSTERNR", "FRACTIENUMMER", "KWEEKTYPE", "KWEEKCODE", "ENTITEIT", "NOTI_CODE", "NOTITIE", "NOTI_COMMENTAAR", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH w_noti
        AS (SELECT *
              FROM kgc_notities noti
             WHERE NOTI.CODE IN
                      ('VERS_GEN-ENS', 'VERS_GEN-NIJM', 'MAT_KWEEK')
                   AND NOTI.GEREED = 'N')
   SELECT                              --verstuurde/te versturen kweekfracties
         '#79A8A8',
          '',
          ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
          FLFA.MEDE_ID AS FAVORIET,
          ONDE.PERS_ID AS NAAR_HELIX,
          ONGR.CODE AS GROEP,
          ONDE.ONDERZOEKNR,
          ONDE.SPOED,
          ONDE.DATUM_BINNEN AS BINNEN,
          ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
          BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
          BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
          DECODE (ONDE.ONDERZOEKSTYPE,
                  'R', 'Research',
                  'D', 'Diagnostiek',
                  '')
             AS ONDE_TYPE,
          DECODE (ONDE.STATUS,
                  'V', 'Vervallen',
                  'G', 'Goedgekeurd',
                  'B', 'Beoordelen',
                  '')
             AS STATUS,
          MONS.MONSTERNUMMER,
          FRAC.FRACTIENUMMER,
          KWTY.CODE,
          KWFR.CODE,
          NOTI.ENTITEIT AS ENTITEIT,
          NOTI.CODE AS NOTI_CODE,
          NOTI.OMSCHRIJVING AS NOTITIE,
          NOTI.COMMENTAAR AS NOTI_COMMENTAAR,
          NOTI.GEREED AS NOTI_GEREED,
          NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
          FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
     FROM kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          kgc_kweekfracties kwfr,
          kgc_kweektypes kwty,
          bas_fracties frac,
          kgc_onderzoeksgroepen ongr,
          w_noti noti,
          beh_flow_favorieten flfa
    WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                 FROM BEH_FLOW_KAFD_ONGR fkon
                                WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
          AND ONDE.ONDE_ID = ONMO.ONDE_ID
          AND MONS.MONS_ID = ONMO.MONS_ID
          AND KWFR.FRAC_ID = FRAC.FRAC_ID
          AND FRAC.MONS_ID = MONS.MONS_ID
          AND KWFR.KWTY_ID = KWTY.KWTY_ID
          AND ONDE.ONGR_ID = ONGR.ONGR_ID
          AND ONDE.ONDE_ID = FLFA.ID(+)
          --            AND ONDE.AFGEROND = 'N'
          AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
          AND KWFR.KWFR_ID = NOTI.ID
          AND NOTI.ENTITEIT = 'KWFR'
   UNION ALL
   SELECT                              --verstuurde/te versturen kweekfracties
         '#79A8A8',
          '',
          ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
          FLFA.MEDE_ID AS FAVORIET,
          ONDE.PERS_ID AS NAAR_HELIX,
          ONGR.CODE AS GROEP,
          ONDE.ONDERZOEKNR,
          ONDE.SPOED,
          ONDE.DATUM_BINNEN AS BINNEN,
          ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
          BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
          BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
          DECODE (ONDE.ONDERZOEKSTYPE,
                  'R', 'Research',
                  'D', 'Diagnostiek',
                  '')
             AS ONDE_TYPE,
          DECODE (ONDE.STATUS,
                  'V', 'Vervallen',
                  'G', 'Goedgekeurd',
                  'B', 'Beoordelen',
                  '')
             AS STATUS,
          MONS.MONSTERNUMMER,
          FRAC.FRACTIENUMMER,
          '',                                                     --KWTY.CODE,
          '',                                                     --KWFR.CODE,
          NOTI.ENTITEIT AS NOTI_ENTITEIT,
          NOTI.CODE AS NOTI_CODE,
          NOTI.OMSCHRIJVING AS NOTITIE,
          NOTI.COMMENTAAR AS NOTI_COMMENTAAR,
          NOTI.GEREED AS NOTI_GEREED,
          NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
          FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
     FROM kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
          kgc_onderzoeksgroepen ongr,
          w_noti noti,
          beh_flow_favorieten flfa
    WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                 FROM BEH_FLOW_KAFD_ONGR fkon
                                WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
          AND ONDE.ONDE_ID = ONMO.ONDE_ID
          AND MONS.MONS_ID = ONMO.MONS_ID
          AND MONS.MONS_ID = FRAC.MONS_ID
          AND ONDE.ONGR_ID = ONGR.ONGR_ID
          AND ONDE.ONDE_ID = FLFA.ID(+)
          --            AND ONDE.AFGEROND = 'N'
          AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
          AND FRAC.FRAC_ID = NOTI.ID
          AND NOTI.ENTITEIT = 'FRAC'
   UNION ALL
   SELECT                              --verstuurde/te versturen kweekfracties
         '#79A8A8',
          '',
          ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
          FLFA.MEDE_ID AS FAVORIET,
          ONDE.PERS_ID AS NAAR_HELIX,
          ONGR.CODE AS GROEP,
          ONDE.ONDERZOEKNR,
          ONDE.SPOED,
          ONDE.DATUM_BINNEN AS BINNEN,
          ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
          BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
          BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
          DECODE (ONDE.ONDERZOEKSTYPE,
                  'R', 'Research',
                  'D', 'Diagnostiek',
                  '')
             AS ONDE_TYPE,
          DECODE (ONDE.STATUS,
                  'V', 'Vervallen',
                  'G', 'Goedgekeurd',
                  'B', 'Beoordelen',
                  '')
             AS STATUS,
          MONS.MONSTERNUMMER,
          '',                                            --FRAC.FRACTIENUMMER,
          '',                                                     --KWTY.CODE,
          '',                                                     --KWFR.CODE,
          NOTI.ENTITEIT AS NOTI_ENTITEIT,
          NOTI.CODE AS NOTI_CODE,
          NOTI.OMSCHRIJVING AS NOTITIE,
          NOTI.COMMENTAAR AS NOTI_COMMENTAAR,
          NOTI.GEREED AS NOTI_GEREED,
          NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
          FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
     FROM kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          kgc_onderzoeksgroepen ongr,
          w_noti noti,
          beh_flow_favorieten flfa
    WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                 FROM BEH_FLOW_KAFD_ONGR fkon
                                WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
          AND ONDE.ONDE_ID = ONMO.ONDE_ID
          AND MONS.MONS_ID = ONMO.MONS_ID
          AND ONDE.ONGR_ID = ONGR.ONGR_ID
          AND ONDE.ONDE_ID = FLFA.ID(+)
          --            AND ONDE.AFGEROND = 'N'
          AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
          AND MONS.MONS_ID = NOTI.ID
          AND NOTI.ENTITEIT = 'MONS'
   UNION ALL
   SELECT                              --verstuurde/te versturen kweekfracties
         '#79A8A8',
          '',
          ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
          FLFA.MEDE_ID AS FAVORIET,
          ONDE.PERS_ID AS NAAR_HELIX,
          ONGR.CODE AS GROEP,
          ONDE.ONDERZOEKNR,
          ONDE.SPOED,
          ONDE.DATUM_BINNEN AS BINNEN,
          ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
          BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
          BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
          DECODE (ONDE.ONDERZOEKSTYPE,
                  'R', 'Research',
                  'D', 'Diagnostiek',
                  '')
             AS ONDE_TYPE,
          DECODE (ONDE.STATUS,
                  'V', 'Vervallen',
                  'G', 'Goedgekeurd',
                  'B', 'Beoordelen',
                  '')
             AS STATUS,
          '',                                            --MONS.MONSTERNUMMER,
          '',                                            --FRAC.FRACTIENUMMER,
          '',                                                     --KWTY.CODE,
          '',                                                     --KWFR.CODE,
          NOTI.ENTITEIT AS NOTI_ENTITEIT,
          NOTI.CODE AS NOTI_CODE,
          NOTI.OMSCHRIJVING AS NOTITIE,
          NOTI.COMMENTAAR AS NOTI_COMMENTAAR,
          NOTI.GEREED AS NOTI_GEREED,
          NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
          FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
     FROM kgc_onderzoeken onde,
          kgc_onderzoeksgroepen ongr,
          w_noti noti,
          beh_flow_favorieten flfa
    WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                 FROM BEH_FLOW_KAFD_ONGR fkon
                                WHERE FKON.FLOW_ID IN (8, 1)) -- MG: dit moet nog gewijzigd worden!!!
          AND ONDE.ONGR_ID = ONGR.ONGR_ID
          AND ONDE.ONDE_ID = FLFA.ID(+)
          --            AND ONDE.AFGEROND = 'N'
          AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
          AND ONDE.ONDE_ID = NOTI.ID
          AND NOTI.ENTITEIT = 'ONDE'
   ORDER BY SPOED ASC, EINDDATUM ASC, ONDERZOEKNR ASC;

/
QUIT
