CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_NOG_GEEN_VREUGDE_SMS_VW" ("ONDERZOEKNR", "ONDE_ID", "DATUM_BINNEN", "DATUM_AUTORISATIE", "FOET_ID", "UITSLAGCODE", "ONDERZOEKSINDICATIES", "PERSOON", "TELEFOON", "GEBOORTEDATUM", "ZISNR", "BSN") AS
  select onderzoeknr
, onde.onde_id
, onde.datum_binnen
, onde.datum_autorisatie
, onde.foet_id
, onui.code
, beh_onderzoek_indicaties(onde.onde_id) indicaties
, PERS.AANSPREKEN
, pers.telefoon
, pers.geboortedatum
, pers.zisnr
, pers.bsn
from kgc_onderzoeken onde
,  kgc_personen pers
, kgc_onderzoek_uitslagcodes onui
where onde.pers_id = pers.pers_id
and ONDE.ONUI_ID = onui.onui_id
and onde.kafd_id = 1
and onde.ongr_id = 271
and nvl(to_char(onde.datum_autorisatie, 'dd-mm-yyyy'), '00-00-0000') <> '00-00-0000'
and onde.datum_autorisatie > to_date('01-01-2014','dd-mm-yyyy')
and onui.code  in ('01', 'A01', 'A04', 'A09')
and not exists (select *
                from  kgc_onderzoek_indicaties onin
                , kgc_indicatie_teksten indi
                where onde.onde_id = onin.onde_id
                and onin.indi_id = indi.indi_id
                and (upper(indi.omschrijving) like '%ECHO%'
                     or indi.afwijking = 'J'
                     )
                )
and (sysdate - onde.datum_autorisatie) < 200
and (sysdate - onde.datum_autorisatie) > 1
and  not Exists ( select *
                from beh_berichten beri
                , beh_bericht_types bety
                , kgc_foetussen foet
                , kgc_zwangerschappen zwan
                where bety.bety_id = beri.bety_id
                and onde.foet_id = foet.foet_id
                and foet.zwan_id = zwan.zwan_id
                and bety.code = 'SMS'
                and beri.entiteit = 'ZWAN'
                and beri.entiteit_id = zwan.zwan_id
                and beri.verzonden = 'J' );

/
QUIT
