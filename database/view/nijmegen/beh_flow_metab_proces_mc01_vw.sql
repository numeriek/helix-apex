CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC01_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "INGRCODE", "INDICODE", "ONDE_TYPE", "STATUS", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT        -- Overzicht 'Onderzoeken zonder protocollen of stoftesten'
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INGR.CODE AS INGRCODE,
            INTE.CODE AS INDICODE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    'C', 'Controle',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 3) -- MG: dit moet nog gewijzigd worden!!!
            AND FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC01_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND ONDE.KAFD_ID = KAFD.KAFD_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND NOT EXISTS
                   (SELECT 1
                      FROM BAS_METINGEN METI1
                     WHERE METI1.ONDE_ID = ONDE.ONDE_ID)
            AND NOT EXISTS
                       (SELECT 1
                          FROM BAS_METINGEN METI1, BAS_MEETWAARDEN MEET1
                         WHERE METI1.METI_ID = MEET1.METI_ID
                               AND METI1.ONDE_ID = ONDE.ONDE_ID)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
