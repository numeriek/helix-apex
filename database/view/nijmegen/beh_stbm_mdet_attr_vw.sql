CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_STBM_MDET_ATTR_VW" ("ONDE_PERS_ID", "MEET_MEET_ID", "SLEUTEL", "GEN", "INDICATIES", "ONDE_ONDERZOEKSWIJZE", "ONDE_ONDERZOEKSTYPE", "ONDE_DATUM_AUTORISATIE", "DNALAB", "FRAC_FRACTIENUMMER", "ONDE_ONDERZOEKNR", "PATHOGENE_MUTATIE", "UVIII", "UVII", "OVERIGE_RESULTATEN", "ONDE_AFGEROND", "ONUI_CODE", "ONUI_OMSCH", "FAMILIENUMMERS", "FOETUS_ID", "MONS_PERS_ID", "PERS_ID_MOEDER", "EFFECT") AS
  SELECT onde.pers_id onde_pers_id,
          frac.frac_id meet_meet_id,
             'P'
          || TO_CHAR (onde.pers_id)
          || 'O'
          || TO_CHAR (onde.onde_id)
          || 'F'
          || TO_CHAR (frac.frac_id)
             sleutel,
          beh_stbm_00.haal_alle_indi_op (onde.onde_id) gen,
          beh_stbm_00.haal_alle_indi_omsch_op (onde.onde_id) indicaties,
          onwy.omschrijving onde_onderzoekswijze,
          DECODE (UPPER (onde.onderzoekstype),
                  'C', 'Controle',
                  'D', 'Diagnostiek',
                  'R', 'Research',
                  onde.onderzoekstype)
             onde_onderzoekstype,
          onde.datum_autorisatie onde_datum_autorisatie,
          'HELIX' dnalab,
          frac.fractienummer frac_fractienummer,
          onde.onderzoeknr onde_onderzoeknr,
          beh_stbm_00.
           haal_alle_mdet_op (onde.onde_id,
                              frac.frac_id,
                              'Pathogene mutatie')
             pathogene_mutatie,
          beh_stbm_00.haal_alle_mdet_op (onde.onde_id, frac.frac_id, 'UVIII')
             uviii,
          beh_stbm_00.haal_alle_mdet_op (onde.onde_id, frac.frac_id, 'UVII')
             uvii,
          beh_stbm_00.haal_alle_mdet_op (onde.onde_id,
                                         frac.frac_id,
                                         'Overige resultaten',
                                         'J')
             overige_resultaten,
          onde.afgerond onde_afgerond,
          CASE
             WHEN onde.datum_autorisatie <
                     TO_DATE ('14-03-2002', 'dd-mm-yyyy')
             THEN
                'Controleer uitslag in Helix'
             ELSE
                onui.code
          END
             onui_code,
          CASE
             WHEN onde.datum_autorisatie <
                     TO_DATE ('14-03-2002', 'dd-mm-yyyy')
             THEN
                'Controleer uitslag in Helix'
             ELSE
                onui.omschrijving
          END
             onui_omsch,
          beh_stbm_00.haal_alle_fami_op (onde.pers_id) familienummers,
		  foet.foet_id foetus_id,
		  mons.pers_id mons_pers_id,
		  foet.pers_id_moeder,
		  null effect
     FROM kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoekswijzen onwy,
          kgc_onderzoek_uitslagcodes onui,
          kgc_foetussen foet
    WHERE     onde.onde_id = onmo.onde_id
          AND onmo.mons_id = mons.mons_id
          AND mons.mons_id = frac.mons_id
          AND mons.pers_id = onde.pers_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.kafd_id = onwy.kafd_id
          AND onde.onui_id = onui.onui_id(+)
--		      AND onde.pers_id = foet.pers_id_moeder(+)
          AND onde.foet_id = foet.foet_id(+)
          AND UPPER (onde.onderzoekswijze) = UPPER (onwy.code)
          AND UPPER (kafd.code) = 'GENOOM'
          AND UPPER (SUBSTR (frac.fractienummer, -1, 1)) <> 'U'
         and user != 'PEDIGREE_RESEARCH';

/
QUIT
