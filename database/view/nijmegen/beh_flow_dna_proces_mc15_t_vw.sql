CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC15_T_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "WERKLIJSTNR", "TECHNIEK", "SAMENSTELLING") AS
  SELECT '#0B3861' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            WERKLIJSTNR AS VERBORGEN_TELLING,
            WERKLIJSTNR,
            TECHNIEK,
            LTRIM (
               LISTAGG (' ' || SAMENSTELLING, ', ')
                  WITHIN GROUP (ORDER BY SAMENSTELLING),
               ' ')
       FROM (  SELECT WERKLIJSTNR, TECHNIEK,
                      CASE
                         WHEN VERBORGEN_SORTERING = 1
                         THEN
                            'Rood: ' || COUNT (DISTINCT (ONDERZOEKNR))
                      END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 2
                            THEN
                               'Oranje: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 3
                            THEN
                               'Licht_groen: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 4
                            THEN
                               'Groen: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 5
                            THEN
                               'Blauw: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 6
                            THEN
                               'Navy: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                         AS SAMENSTELLING
                 FROM BEH_FLOW_DNA_PROCES_MC15_VW
             GROUP BY WERKLIJSTNR, TECHNIEK, VERBORGEN_SORTERING)
   GROUP BY WERKLIJSTNR, TECHNIEK
   ORDER BY WERKLIJSTNR ASC;

/
QUIT
