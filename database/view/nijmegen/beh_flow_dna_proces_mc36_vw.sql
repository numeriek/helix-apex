CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC36_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "TAAL", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "TOT_AANG", "LIMS_COMPLETE", "LABV_COMPLETE", "STATUS_LIMS", "STATUS_LABV", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht SEQFACPlaten/Labvantage (werklijsten) die naar Sequence Pilot zijn gestuurd
 /*
 1e select haald de gegevens uit sqllims
 2e select, haalt gegevens uit labvantage
 1e select weghalen als sqllims uit de lucht is
 */
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            beh_haal_familienr (ONDE.PERS_ID) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            TAAL.CODE TAAL,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            BEH_HAAL_FRACTIENR_MATE (onde.onde_id) AS FRACTIENR,
            (SELECT COUNT (DISTINCT meet1.meet_id)
               FROM kgc_onderzoeken onde1,
                    bas_metingen meti1,
                    bas_meetwaarden meet1,
                    kgc_stoftesten stof1,
                    kgc_technieken tech1
              WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                    AND METI1.METI_ID = MEET1.METI_ID
                    AND MEET1.STOF_ID = STOF1.STOF_ID
                    AND STOF1.TECH_ID = TECH1.TECH_ID
                    AND TECH1.CODE = 'SEQ'
                    AND MEET1.AFGEROND = 'N'
                    AND ONDE.ONDE_ID = ONDE1.ONDE_ID)
               AS TOTAAL_AANGEMELD,
            COUNT (DISTINCT meet.meet_id) AS LIMS_COMPLETE,
            NULL LABV_COMPLETE,
            (CASE
                WHEN COUNT (DISTINCT meet.meet_id) =
                        (SELECT COUNT (DISTINCT meet1.meet_id)
                           FROM kgc_onderzoeken onde1,
                                bas_metingen meti1,
                                bas_meetwaarden meet1,
                                kgc_stoftesten stof1,
                                kgc_technieken tech1
                          WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                                AND METI1.METI_ID = MEET1.METI_ID
                                AND MEET1.STOF_ID = STOF1.STOF_ID
                                AND STOF1.TECH_ID = TECH1.TECH_ID
                                AND TECH1.CODE = 'SEQ'
                                AND MEET1.AFGEROND = 'N'
                                AND ONDE.ONDE_ID = ONDE1.ONDE_ID)
                THEN
                   'Alles klaar'
             END)
               AS STATUS_LIMS,
            NULL AS STATUS_LABV,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_indicatie_groepen ingr,
            kgc_indicatie_teksten inte,
            kgc_stoftesten stof,
            kgc_technieken tech,
            BEH_FLOW_DNA_NAI_SAMPLES nasa,
            BEH_FLOW_DNA_NAI_SUBMISSIONS nasu,
            BEH_FLOW_DNA_NAI_STATUS_ACTION STAC,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa,
            KGC_TALEN TAAL
      WHERE     ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND METI.METI_ID = MEET.METI_ID
            AND METI.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.KAFD_ID = INGR.KAFD_ID
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.STOF_ID = STOF.STOF_ID
            AND STOF.TECH_ID = TECH.TECH_ID
            AND NASA.MEASURE_ID = MEET.MEET_ID
            AND NASA.SUBMISSION_ID = NASU.SUBMISSION_ID
            AND NASU.SUBMISSION_ID = STAC.ID
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC36_VW'
            AND ONDE.AFGEROND = 'N'
            AND UPPER (STAC.STATUS) = 'FIRED'
            AND TECH.CODE = 'SEQ'
            AND MEET.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDE_ID,
            ONDE.ONDERZOEKNR,
            TAAL.CODE,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            ONDE.ONDERZOEKSTYPE,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            FLFA.MEDE_ID,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   UNION ALL
     SELECT FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            beh_haal_familienr (ONDE.PERS_ID) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            TAAL.CODE TAAL,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            BEH_HAAL_FRACTIENR_MATE (onde.onde_id) AS FRACTIENR,
            (SELECT COUNT (DISTINCT meet1.meet_id)
               FROM kgc_onderzoeken onde1,
                    bas_metingen meti1,
                    bas_meetwaarden meet1,
                    kgc_stoftesten stof1,
                    kgc_technieken tech1
              WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                    AND METI1.METI_ID = MEET1.METI_ID
                    AND MEET1.STOF_ID = STOF1.STOF_ID
                    AND STOF1.TECH_ID = TECH1.TECH_ID
                    AND TECH1.CODE = 'SEQ'
                    AND MEET1.AFGEROND = 'N'
                    AND ONDE.ONDE_ID = ONDE1.ONDE_ID)
               AS TOTAAL_AANGEMELD,
            NULL LIMS_COMPLETE,
            COUNT (DISTINCT meet.meet_id) AS LABV_COMPLETE,
            NULL STATUS_LIMS,
            (CASE
                WHEN COUNT (DISTINCT meet.meet_id) =
                        (SELECT COUNT (DISTINCT meet1.meet_id)
                           FROM kgc_onderzoeken onde1,
                                bas_metingen meti1,
                                bas_meetwaarden meet1,
                                kgc_stoftesten stof1,
                                kgc_technieken tech1
                          WHERE     ONDE1.ONDE_ID = METI1.ONDE_ID
                                AND METI1.METI_ID = MEET1.METI_ID
                                AND MEET1.STOF_ID = STOF1.STOF_ID
                                AND STOF1.TECH_ID = TECH1.TECH_ID
                                AND TECH1.CODE = 'SEQ'
                                AND MEET1.AFGEROND = 'N'
                                AND ONDE.ONDE_ID = ONDE1.ONDE_ID)
                THEN
                   'Alles klaar'
             END)
               AS STATUS_LABV,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_indicatie_groepen ingr,
            kgc_indicatie_teksten inte,
            kgc_stoftesten stof,
            kgc_technieken tech,
            beh_labv_s_sample_vw labv,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa,
            KGC_TALEN TAAL
      WHERE     ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND METI.METI_ID = MEET.METI_ID
            AND METI.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.KAFD_ID = INGR.KAFD_ID
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.STOF_ID = STOF.STOF_ID
            AND STOF.TECH_ID = TECH.TECH_ID
            AND labv."u_measureid" = MEET.MEET_ID
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC36_VW'
            AND ONDE.AFGEROND = 'N'
            AND labv."u_completeflag" = 'Y'
            AND TECH.CODE = 'SEQ'
            AND MEET.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDE_ID,
            ONDE.ONDERZOEKNR,
            TAAL.CODE,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            ONDE.ONDERZOEKSTYPE,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            FLFA.MEDE_ID,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            EINDDATUM ASC,
            ONDERZOEKNR ASC;

/
QUIT
