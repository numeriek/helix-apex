CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST21_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND", "WEEK", "GROEP", "ONDE_TYPE", "RANGE", "SCANNING", "SCREENING", "TOTAAL") AS
  WITH step1 AS (SELECT VERBORGEN_KLEUR,
                         VERBORGEN_SORTERING,
                         VERBORGEN_TELLING,
                         JAAR_MAAND,
                         WEEK,
                         GROEP,
                         ONDE_TYPE,
                         WIJZE,
                         TOT_30_DAGEN,
                         VAN_30_TOT_90_DAGEN,
                         VAN_90_TOT_365_DAGEN,
                         VAN_365_DAGEN
                    FROM BEH_FLOW_DNA_PROCES_ST12_VW),
        step2
           AS (SELECT step1.*,
                      ROW_NUMBER ()
                      OVER (
                         PARTITION BY JAAR_MAAND,
                                      ONDE_TYPE,
                                      WEEK,
                                      GROEP,
                                      WIJZE
                         ORDER BY WIJZE)
                         AS rn
                 FROM step1)
     SELECT VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            WEEK,
            GROEP,
            ONDE_TYPE,
            '1 - TOT_30_DAGEN' AS RANGE,
            MAX (DECODE (WIJZE, 'Scanning', TOT_30_DAGEN, NULL)) AS Scanning,
            MAX (DECODE (WIJZE, 'Screening', TOT_30_DAGEN, NULL)) AS Screening,
            SUM (TOT_30_DAGEN) AS totaal
       FROM step2
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            ONDE_TYPE,
            WEEK,
            GROEP,
            rn
   UNION ALL
     SELECT VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            WEEK,
            GROEP,
            ONDE_TYPE,
            '2 - VAN_30_TOT_90_DAGEN' AS RANGE,
            MAX (DECODE (WIJZE, 'Scanning', VAN_30_TOT_90_DAGEN, NULL))
               AS Scanning,
            MAX (DECODE (WIJZE, 'Screening', VAN_30_TOT_90_DAGEN, NULL))
               AS Screening,
            SUM (VAN_30_TOT_90_DAGEN) AS totaal
       FROM step2
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            ONDE_TYPE,
            WEEK,
            GROEP,
            rn
   UNION ALL
     SELECT VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            WEEK,
            GROEP,
            ONDE_TYPE,
            '3 - VAN_90_TOT_365_DAGEN' AS RANGE,
            MAX (DECODE (WIJZE, 'Scanning', VAN_90_TOT_365_DAGEN, NULL))
               AS Scanning,
            MAX (DECODE (WIJZE, 'Screening', VAN_90_TOT_365_DAGEN, NULL))
               AS Screening,
            SUM (VAN_90_TOT_365_DAGEN) AS totaal
       FROM step2
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            ONDE_TYPE,
            WEEK,
            GROEP,
            rn
   UNION ALL
     SELECT VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            WEEK,
            GROEP,
            ONDE_TYPE,
            '4 - VAN_365_DAGEN' AS RANGE,
            MAX (DECODE (WIJZE, 'Scanning', VAN_365_DAGEN, NULL)) AS Scanning,
            MAX (DECODE (WIJZE, 'Screening', VAN_365_DAGEN, NULL)) AS Screening,
            SUM (VAN_365_DAGEN) AS totaal
       FROM step2
   GROUP BY VERBORGEN_KLEUR,
            VERBORGEN_SORTERING,
            VERBORGEN_TELLING,
            JAAR_MAAND,
            ONDE_TYPE,
            WEEK,
            GROEP,
            rn
   ORDER BY JAAR_MAAND DESC,
            WEEK DESC,
            GROEP,
            ONDE_TYPE,
            RANGE;

/
QUIT
