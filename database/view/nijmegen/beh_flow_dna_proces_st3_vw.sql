CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST3_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "GROEP", "INDI_GROEP", "ONDE_TYPE", "WIJZE", "AANTAL") AS
  SELECT '#FFFFFF',
            1,
            '',
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONWY.OMSCHRIJVING,
            COUNT (DISTINCT ONDE.ONDERZOEKNR)
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.AFGEROND = 'N'
   GROUP BY ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            ONDE.ONDERZOEKSTYPE,
            ONWY.OMSCHRIJVING
   ORDER BY ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            ONDE.ONDERZOEKSTYPE,
            ONWY.OMSCHRIJVING;

/
QUIT
