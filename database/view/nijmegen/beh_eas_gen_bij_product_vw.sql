CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_EAS_GEN_BIJ_PRODUCT_VW" ("INDI_INDI_ID_PRODUCT", "OMSCHRIJVING_ENGELS", "CURRENT_RELEASE", "INDI_INDI_ID_GEN", "GENE_NAAM", "MEDIAN_COVERAGE", "PERCENTAGE_COVERED_GREATER_10X", "PERCENTAGE_COVERED_GREATER_20X", "OMIM_PHENOTYPE_ID") AS
  SELECT prod.indi_indi_id indi_indi_id_product,
          prod.omschrijving_engels,
          prod.current_release,
          gene.indi_indi_id indi_indi_id_gen,
          gepr.gene_naam,
          gepr.median_coverage,
          gepr.percentage_covered_greater_10x,
          gepr.percentage_covered_greater_20x,
          gepr.omim_phenotype_ID
     FROM beh_eas_producten prod
     , beh_eas_gen_bij_product gepr
     , beh_eas_genen gene
    WHERE gepr.prod_id = prod.prod_id
    and gene.gene_id = gepr.gene_id
    AND prod.gepubliceerd = 'J';

/
QUIT
