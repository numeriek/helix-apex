CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_OV08_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_NOTI_ID", "VERBORGEN_NOTI_ENTITEIT", "NAAR_HELIX", "FAMILIENR", "ARCHIEF", "FAMI_BESTANDEN", "OGROEP", "ONDERZOEKNR", "EINDDATUM", "BI_BU", "ONDE_BESTANDEN", "ZISNR", "ENTI_OMSCH", "ENTITEIT", "NOTI_CODE", "NOTITIE", "NOTICOMMENTAAR", "NOTIGEREED", "CREATION_DATE", "CREATED_BY", "LAST_UPDATE_DATE", "LAST_UPDATED_BY") AS
  SELECT CASE
               WHEN FLOP.CREATION_DATE < ADD_MONTHS (SYSDATE, -2)
               THEN
                  '#CD5B5B'                                             --rood
               ELSE
                  CASE
                     WHEN FLOP.CREATION_DATE < ADD_MONTHS (SYSDATE, -1)
                     THEN
                        '#ED8B1C'                                     --oranje
                     ELSE
                        '#5CAB5C'                                      --groen
                  END
            END
               AS VERBORGEN_KLEUR,
            FLOP.LAST_UPDATE_DATE AS VERBORGEN_SORTERING,
            FLOP.VERBORGEN_NOTI_ID,
            FLOP.VERBORGEN_NOTI_ENTITEIT,
            FLOP.NAAR_HELIX,
            FLOP.FAMILIENR,
            FLOP.ARCHIEF,
            FLOP.FAMI_BESTANDEN,
            FLOP.GROEP,
            FLOP.ONDERZOEKNR,
            FLOP.EINDDATUM,
            FLOP.BI_BU,
            FLOP.ONDE_BESTANDEN,
            FLOP.ZISNR,
            FLOP.ENTITEIT_OMSCHRIJVING,
            FLOP.VERBORGEN_NOTI_ENTITEIT AS ENTITEIT,
            FLOP.NOTICODE,
            FLOP.NOTITIE,
            FLOP.NOTICOMMENTAAR,
            FLOP.NOTIGEREED,
            FLOP.CREATION_DATE,
            FLOP.CREATED_BY,
            FLOP.LAST_UPDATE_DATE,
            FLOP.LAST_UPDATED_BY
       FROM BEH_FLOW_OPDRACHTEN_VW FLOP
      WHERE FLOP.NOTICODE LIKE 'SVD%'
   --            AND (FLOP.GROEP IS NULL
   --                 OR FLOP.GROEP IN
   --                       (SELECT ONGR.CODE
   --                          FROM BEH_FLOW_KAFD_ONGR fkon,
   --                               kgc_onderzoeksgroepen ongr
   --                         WHERE FKON.FLOW_ID = 1 AND FKON.ONGR_ID = ONGR.ONGR_ID))
   ORDER BY DECODE (VERBORGEN_KLEUR,  '#CD5B5B', 1,  '#ED8B1C', 2,  3) ASC,
            VERBORGEN_SORTERING ASC;

/
QUIT
