CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_OV02_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "GROEP", "ONDERZOEKNR", "DAT_BINNEN", "MEDECODE", "AANSPREKEN", "GESLACHT", "GEBOORTEDATUM", "PROTOCOL", "AFGEROND", "DATUM_AFGEROND") AS
  SELECT '#79A8A8',                                 --FLPR.VERBORGEN_KLEUR,
            '',                                    --FLPR.VERBORGEN_SORTERING,
            ongr.code,
            onde.onderzoeknr,
            onde.datum_binnen,
            mede.code,
            pers.aanspreken,
            pers.geslacht,
            TO_CHAR (pers.geboortedatum, 'dd-mm-yyyy'),
            NVL (stgr.code, 'Onbekend'),
            NVL (meti.afgerond, '-'),
            NVL (TO_CHAR (meti.datum_afgerond, 'dd-mm-yyyy'), '-')
       FROM kgc_onderzoeken onde,
            kgc_personen pers,
            bas_metingen meti,
            kgc_stoftestgroepen stgr,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_medewerkers mede
      WHERE     onde.pers_id = pers.pers_id
            AND meti.onde_id(+) = onde.onde_id
            AND meti.stgr_id = stgr.stgr_id(+)
            AND onde.mede_id_beoordelaar = mede.mede_id(+)
            AND onde.kafd_id = kafd.kafd_id
            AND onde.ongr_id = ongr.ongr_id
            AND ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 3)
            --and onderzoeknr = 'B04/1253'
            --and onde.onderzoeknr = 'M07/0754'
            AND onde.afgerond <> 'J'
   --AND onderzoekstype in ('C', 'D')
   ORDER BY ongr.code, onde.onderzoeknr, stgr.code;

/
QUIT
