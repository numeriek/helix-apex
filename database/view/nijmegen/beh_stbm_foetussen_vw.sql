CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_STBM_FOETUSSEN_VW" ("FOET_ID", "PERS_ID_MOEDER", "FOET_NR", "GESLACHT", "DOOD_GEBOREN", "PERS_ID_GEBOREN_ALS") AS
  SELECT foet.foet_id foet_id
     , foet.pers_id_moeder pers_id_moeder
	 , TO_CHAR(zwan.nummer) || '-' ||TO_CHAR(foet.volgnr) foet_nr
	 , foet.geslacht geslacht
	 , foet.niet_geboren dood_geboren
	 , foet.pers_id_geboren_als
FROM   kgc_foetussen foet
     , kgc_zwangerschappen zwan
WHERE  foet.zwan_id = zwan.zwan_id
and user != 'PEDIGREE_RESEARCH'
ORDER BY foet.foet_id;

/
QUIT
