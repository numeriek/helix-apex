CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC04_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "MONSTERNR", "SPOED", "BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "PROTOCOL", "MEDEWERKER", "TOTAAL_MEET", "MEET_NIET_AF", "MEET_AF", "NOTITIE", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED (+) = 'N'
                    AND NOTI.ENTITEIT = 'ONDE')
   SELECT                              -- beoordelen meetwaarden en afronden
           '#79A8A8' VERBORGEN_KLEUR,
            '' VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            MONS.MONSTERNUMMER MONSTERNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            stgr.code PROTOCOL,
            MEDE.CODE MEDEWERKER,
            OMM.TOTAAL_MEET,
            OMM.MEET_NIET_AF,
            OMM.MEET_AF,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_monsters mons,
            kgc_onderzoek_monsters onmo,
            kgc_onderzoeksgroepen ongr,
            W_KGC_NOTITIES noti,
            beh_flow_favorieten flfa,
            BEH_FLOW_ONDE_MEET_METI_MATVW omm,
            bas_metingen meti,
            kgc_medewerkers mede,
            kgc_stoftestgroepen stgr
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND onde.onde_id = onmo.onde_id
            AND onmo.mons_id = mons.mons_id
            AND onde.onde_id = meti.onde_id
            AND meti.stgr_id = stgr.stgr_id
            AND ONDE.ONDE_ID = OMM.ONDE_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND meti.mede_id = mede.mede_id
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND OMM.MEET_NIET_AF <> 0
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND METI.AFGEROND = 'N'
            AND METI.MEDE_ID IS NOT NULL
            AND UPPER (FLFA.ENTITEIT(+)) = 'ONDE'
   ORDER BY ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
