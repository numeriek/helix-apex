CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DIGIDOS_ENTITEITEN_VW" ("BTYP_ID", "BTYP_CODE", "ENTITEIT", "ENTITEIT_ID", "ENTITEIT_NR", "CREATIONYEAR", "DEF") AS
  select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       fami.fami_id entiteit_id
,       fami.familienummer entiteit_nr
,       to_char(fami.creation_date, 'YYYY') creationyear
,       pad.def
from    kgc_families fami
, 		beh_digidos_paden_vw pad
WHERE   pad.enti_code = 'FAMI'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       frac.frac_id entiteit_id
,       frac.fractienummer entiteit_nr
,       to_char(frac.creation_date, 'YYYY') creationyear
,       pad.def
from    bas_fracties frac
, 		beh_digidos_paden_vw pad
where   frac.kafd_id = nvl(pad.kafd_id, frac.kafd_id)
and     pad.enti_code = 'FRAC'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       meet.meet_id entiteit_id
,       to_char(meet.meet_id) entiteit_nr
,       to_char(meet.creation_date, 'YYYY') creationyear
,       pad.def
from    bas_meetwaarden meet
,       bas_metingen meti
,       bas_fracties frac
,       beh_digidos_paden_vw pad
where   meet.meti_id = meti.meti_id
and     meti.frac_id = frac.frac_id
and     frac.kafd_id = nvl(pad.kafd_id, frac.kafd_id)
and     pad.enti_code = 'MEET'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       meti.meti_id entiteit_id
,       to_char(meti.meti_id) entiteit_nr
,       to_char(meti.creation_date, 'YYYY') creationyear
,       pad.def
from    bas_metingen meti
,       bas_fracties frac
,       beh_digidos_paden_vw pad
where   meti.frac_id = frac.frac_id
and     frac.kafd_id = nvl(pad.kafd_id, frac.kafd_id)
and     pad.enti_code = 'METI'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       mons.mons_id entiteit_id
,       mons.monsternummer entiteit_nr
,       to_char(mons.creation_date, 'YYYY') creationyear
,       pad.def
from    kgc_monsters mons
, 		beh_digidos_paden_vw pad
where   mons.kafd_id = nvl(pad.kafd_id, mons.kafd_id)
and     pad.enti_code = 'MONS'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       onde.onde_id entiteit_id
,       onde.onderzoeknr entiteit_nr
,       to_char(onde.creation_date, 'YYYY') creationyear
,       pad.def
from    kgc_onderzoeken onde
,       beh_digidos_paden_vw pad
where   onde.kafd_id = nvl(pad.kafd_id, onde.kafd_id)
and     pad.enti_code = 'ONDE'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       pers.pers_id entiteit_id
,       pers.zisnr entiteit_nr
,       to_char(pers.creation_date, 'YYYY') creationyear
,       pad.def
from    kgc_personen pers
, 		  beh_digidos_paden_vw pad
where   pad.enti_code = 'PERS'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       uits.uits_id entiteit_id
,       to_char(uits.uits_id) entiteit_nr
,       to_char(uits.creation_date, 'YYYY') creationyear
,       pad.def
from    kgc_uitslagen uits
, 		  beh_digidos_paden_vw pad
where   uits.kafd_id = nvl(pad.kafd_id, uits.kafd_id)
and     pad.enti_code = 'UITS'
union all
select  pad.btyp_id
,		pad.btyp_code
,       pad.enti_code entiteit
,       wlst.wlst_id entiteit_id
,       wlst.werklijst_nummer entiteit_nr
,       to_char(wlst.creation_date, 'YYYY') creationyear
,       pad.def
from    kgc_werklijsten wlst
, 		  beh_digidos_paden_vw pad
where   wlst.kafd_id = nvl(pad.kafd_id, wlst.kafd_id)
and     pad.enti_code = 'WLST';

/
QUIT
