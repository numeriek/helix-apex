CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST5_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND", "GROEP", "INDI_GROEP", "ONDE_TYPE", "WIJZE", "AANTAL_ONDE", "GEM_DOORLOOPTIJD") AS
  SELECT '#FFFFFF',
            1,
            '',
            TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM') AS JAAR_MAAND,
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONWY.OMSCHRIJVING,
            COUNT (DISTINCT ONDE.ONDERZOEKNR),
            ROUND (AVG (DISTINCT (ONDE.DATUM_AUTORISATIE - ONDE.DATUM_BINNEN)))
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr
      WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.AFGEROND = 'J'
            AND TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY') > 2010
   GROUP BY TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM'),
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
            ONWY.OMSCHRIJVING,
            ONDE.ONDERZOEKSTYPE
   ORDER BY JAAR_MAAND ASC,
            ONGR.CODE,
            SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3);

/
QUIT
