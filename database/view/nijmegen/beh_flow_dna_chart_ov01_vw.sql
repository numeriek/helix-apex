CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_CHART_OV01_VW" ("ONDER", "LIJN", "AANTAL") AS
  WITH A AS (SELECT *
                FROM BEH_FLOW_DNA_STAT_OHW
               WHERE INDIGROEP = 'G00' AND ONDE_TYPE = 'Diagnostiek')
     SELECT A.JAAR_MAAND || '-' || A.WEEK AS ONDER,
            A.WIJZE || ' <=30 dagen' AS LIJN,
            SUM (A."<=30 dagen") AS AANTAL
       FROM A
   GROUP BY A.INDIGROEP,
            A.ONDE_TYPE,
            A.WIJZE,
            A.JAAR_MAAND || '-' || A.WEEK
   UNION ALL
     SELECT A.JAAR_MAAND || '-' || A.WEEK AS ONDER,
            A.WIJZE || ' >30 & <=90 dagen' AS LIJN,
            SUM (A.">30 & <=90 dagen") AS AANTAL
       FROM A
   GROUP BY A.INDIGROEP,
            A.ONDE_TYPE,
            A.WIJZE,
            A.JAAR_MAAND || '-' || A.WEEK
   UNION ALL
     SELECT A.JAAR_MAAND || '-' || A.WEEK AS ONDER,
            A.WIJZE || ' >90 & <=365 dagen' AS LIJN,
            SUM (A.">90 & <=365 dagen") AS AANTAL
       FROM A
   GROUP BY A.INDIGROEP,
            A.ONDE_TYPE,
            A.WIJZE,
            A.JAAR_MAAND || '-' || A.WEEK
   UNION ALL
     SELECT A.JAAR_MAAND || '-' || A.WEEK AS ONDER,
            A.WIJZE || ' >365 dagen' AS LIJN,
            SUM (A.">365 dagen") AS AANTAL
       FROM A
   GROUP BY A.INDIGROEP,
            A.ONDE_TYPE,
            A.WIJZE,
            A.JAAR_MAAND || '-' || A.WEEK
   UNION ALL
     SELECT A.JAAR_MAAND || '-' || A.WEEK AS ONDER,
            A.WIJZE || '-Totaal' AS LIJN,
              SUM (A."<=30 dagen")
            + SUM (A.">30 & <=90 dagen")
            + SUM (A.">90 & <=365 dagen")
            + SUM (A.">365 dagen")
               AS AANTAL
       FROM A
   GROUP BY A.INDIGROEP,
            A.ONDE_TYPE,
            A.WIJZE,
            A.JAAR_MAAND || '-' || A.WEEK
   ORDER BY ONDER DESC, LIJN ASC;

/
QUIT
