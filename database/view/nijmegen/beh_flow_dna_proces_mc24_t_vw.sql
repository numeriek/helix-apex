CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC24_T_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "WERKLIJSTNR", "SAMENSTELLING", "STATUS", "MACHINE") AS
  SELECT '#0B3861' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            WERKLIJSTNR AS VERBORGEN_TELLING,
            WERKLIJSTNR,
            LTRIM (
               LISTAGG (' ' || SAMENSTELLING, ', ')
                  WITHIN GROUP (ORDER BY SAMENSTELLING),
               ' ')
               AS SAMENSTELLING,
            STATUS,
            MACHINE
       FROM (  SELECT WERKLIJSTNR,
                      STATUS,
                      MACHINE,
                      CASE
                         WHEN VERBORGEN_SORTERING = 1
                         THEN
                            'Rood: ' || COUNT (DISTINCT (ONDERZOEKNR))
                      END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 2
                            THEN
                               'Oranje: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 3
                            THEN
                               'Licht_groen: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 4
                            THEN
                               'Groen: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 5
                            THEN
                               'Blauw: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                      || CASE
                            WHEN VERBORGEN_SORTERING = 6
                            THEN
                               'Navy: ' || COUNT (DISTINCT (ONDERZOEKNR))
                         END
                         AS Samenstelling
                 FROM BEH_FLOW_DNA_PROCES_MC24_VW
             GROUP BY WERKLIJSTNR,
                      STATUS,
                      MACHINE,
                      VERBORGEN_SORTERING)
   GROUP BY WERKLIJSTNR, STATUS, MACHINE
   ORDER BY WERKLIJSTNR ASC;

/
QUIT
