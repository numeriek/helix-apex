CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC05_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "TOTAAL_P", "OPEN_P", "AFGEROND_P", "ANALIST", "NOTITIE", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH w_meco
        AS (SELECT *
              FROM (SELECT onde_id,
                           LISTAGG (code, ',')
                              WITHIN GROUP (ORDER BY code)
                              OVER (PARTITION BY onde_id)
                              AS meco,
                           ROW_NUMBER ()
                              OVER (PARTITION BY onde_id ORDER BY onde_id)
                              AS row_nr
                      FROM (SELECT UNIQUE meti.onde_id, mede.code
                              FROM bas_metingen meti,
                                   bas_meetwaarden meet,
                                   kgc_medewerkers mede
                             WHERE METI.METI_ID = MEET.METI_ID
                                   AND MEET.MEDE_ID = MEDE.MEDE_ID))
             WHERE row_nr = 1),
    W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED (+) = 'N'
                    AND NOTI.ENTITEIT = 'ONDE')
     SELECT '#79A8A8',
            '',
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            omm.TOTAAL_METI,
            omm.METI_NIET_AF,
            omm.METI_AF,
            meco.meco AS analist,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            W_KGC_NOTITIES noti,
            beh_flow_favorieten flfa,
            w_meco meco,
            BEH_FLOW_ONDE_MEET_METI_MATVW omm
      WHERE     ONDE.ONGR_ID IN (SELECT ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR
                                  WHERE FLOW_ID = 8)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND FLFA.ENTITEIT(+) = 'ONDE'
            AND ONDE.ONDE_ID = MECO.ONDE_ID(+)
            AND ONDE.ONDE_ID = OMM.ONDE_ID
            AND ONDE.AFGEROND = 'N'
            AND ( ( (OMM.TOTAAL_METI <> OMM.METI_AF
                     OR NOT EXISTS
                           (SELECT NULL
                              FROM KGC_UITSLAGEN UITS
                             WHERE UITS.ONDE_ID = ONDE.ONDE_ID))
                   AND OMM.MEET_NIET_AF = 0)
                 OR NOTI.CODE = 'GEREED_UITSLAG'
                 OR EXISTS
                       (SELECT 1
                          FROM KGC_UITSLAGEN UITS
                         WHERE     UITS.ONDE_ID = ONDE.ONDE_ID
                               AND UITS.MEDE_ID IS NULL
                               AND UITS.MEDE_ID2 IS NULL))
   ORDER BY ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
