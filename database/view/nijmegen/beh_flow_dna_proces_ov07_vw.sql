CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_OV07_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht "Goedgekeurde NGS Onderzoeken zonder KASP meting (alleen voor onde_type=D, alleen voor kind, dus niet voor ouders -> indicatie <> _C)"
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            LISTAGG (FRAC.FRACTIENUMMER, ', ')
               WITHIN GROUP (ORDER BY FRAC.FRACTIENUMMER)
               AS FRACTIENR,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            beh_flow_help_fami_vw FAMI,
            bas_fracties frac,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_onderzoek_monsters onmo,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC2_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.PERS_ID = FAMI.PERS_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND ONDE.ONDERZOEKSTYPE = 'D'
            AND ONGR.CODE = 'NGS'
            AND FRAC.FRACTIENUMMER NOT LIKE '%B'
            AND INTE.CODE NOT LIKE '%!_C' ESCAPE '!'
            AND INTE.CODE NOT LIKE '%!_02' ESCAPE '!'
            AND INTE.CODE NOT LIKE '%!_02C' ESCAPE '!'
            AND INTE.CODE NOT LIKE '%!_2' ESCAPE '!'
            AND INTE.CODE NOT LIKE '%!_2C' ESCAPE '!'
            AND INTE.CODE NOT LIKE '%!_3C' ESCAPE '!'
            AND INTE.CODE NOT LIKE '%!_03C' ESCAPE '!'
            AND INTE.CODE <> 'CNV'
            AND INTE.CODE <> 'RES'
            AND INTE.CODE <> 'TOEVAL'
            AND NOT EXISTS
                       (SELECT 1
                          FROM bas_metingen meti, kgc_stoftestgroepen stgr
                         WHERE METI.ONDE_ID = ONDE.ONDE_ID
                               AND METI.STGR_ID = STGR.STGR_ID
                               AND (STGR.CODE LIKE '%KASP%'
                                    OR STGR.CODE LIKE '%SNP_S%'))
            AND EXISTS
                   (SELECT 1
                      FROM bas_metingen meti, kgc_stoftestgroepen stgr
                     WHERE METI.ONDE_ID = ONDE.ONDE_ID
                           AND METI.STGR_ID = STGR.STGR_ID
                           AND (STGR.CODE = 'NGS_ST_1'
                                OR STGR.CODE = 'NGS_ST_2'))
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.ONDE_ID,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            FAMI.FAMILIENUMMER,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
