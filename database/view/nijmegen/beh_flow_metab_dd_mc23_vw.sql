CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_DD_MC23_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "VT_NIET_WLST_NIET_AFG", "VERBORGEN_FLOW_ID") AS
  SELECT --DISTINCT -- Overzicht "Inplannen voortesten" - voortesten niet op de werklijst, niet afgerond  -- SEMI AUTOMATISCH PROCES
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            COUNT (*) AS VT_NIET_WLST_NIET_AFG, --aantal1.voortesten_niet_op_wl_niet_afg
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_onderzoek_monsters onmo,
            bas_fracties frac,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_METAB_DD_MC23_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND MEET.AFGEROND = 'N'
            AND MEET.MEET_REKEN = 'V'
            AND STGR.CODE NOT LIKE '%!_R' ESCAPE '!'
            AND UPPER (STGR.OMSCHRIJVING) NOT LIKE '%ROBOT%'
            AND (UPPER (STOF.OMSCHRIJVING) LIKE '%(SEQ)'
                 OR UPPER (STOF.OMSCHRIJVING) LIKE '%(GS)')
            AND MEET.MEET_ID NOT IN (SELECT WLIT1.MEET_ID
                                       FROM KGC_WERKLIJST_ITEMS WLIT1)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            flfa.mede_id,
            ONDE.PERS_ID,
            ongr.code,
            onde.onderzoeknr,
            onde.spoed,
            onde.geplande_einddatum,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            FRAC.FRACTIENUMMER,
            FLFA.FLOW_ID
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC,
            FRAC.FRACTIENUMMER ASC;

/
QUIT
