CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DECL_CHECK_VW" ("DECL_ID", "ENTITEIT", "ENTITEIT_ID", "DECL_DATUM", "DATUM_VERWERKING", "VERRICHTINGCODE", "SERVICE_PROVIDER", "PERS_ID", "ZISNR", "PERS_AANSPREKEN", "OVERLEDEN", "OVERLIJDENSDATUM", "BEHEER_IN_ZIS", "ONDE_ID", "ONDERZOEKNR", "DATUM_BINNEN", "ONDERZOEKSTYPE", "ONDE_AFGEROND", "KAFD_CODE", "ONGR_CODE", "RELA_ID", "RELA_CODE", "ZNR", "RELA_AANSPREKEN", "RELA_ADRES", "AANVRAGER_TYPE", "DECL_TYPE", "SER_ID", "SPEC_CODE", "BEVOEGD_DECLAREREN", "DEWY_CODE", "ICODE", "INST_CODE", "DECL_STATUS", "UITV_AFDELING", "DECL_CHECK", "PERS_CHECK", "RELA_CHECK", "INST_CHECK") AS
  select decl."DECL_ID",decl."ENTITEIT",decl."ENTITEIT_ID",decl."DECL_DATUM",decl."DATUM_VERWERKING",decl."VERRICHTINGCODE",decl."SERVICE_PROVIDER",decl."PERS_ID",decl."ZISNR",decl."PERS_AANSPREKEN",decl."OVERLEDEN",decl."OVERLIJDENSDATUM",decl."BEHEER_IN_ZIS",decl."ONDE_ID",decl."ONDERZOEKNR",decl."DATUM_BINNEN",decl."ONDERZOEKSTYPE",decl."ONDE_AFGEROND",decl."KAFD_CODE",decl."ONGR_CODE",decl."RELA_ID",decl."RELA_CODE",decl."ZNR",decl."RELA_AANSPREKEN",decl."RELA_ADRES",decl."AANVRAGER_TYPE",decl."DECL_TYPE",decl."SER_ID",decl."SPEC_CODE",decl."BEVOEGD_DECLAREREN",decl."DEWY_CODE",decl."ICODE",decl."INST_CODE",decl."DECL_STATUS",decl."UITV_AFDELING",
case
	when decl.service_provider is null then
		'GEEN_SERVICE_PROVIDER'
	else
		'OK'	
end decl_check,
case
  when zisnr is not null then
    case
      when beheer_in_zis = 'J' then
        case
          when overleden = 'J' then
            case
              when overlijdensdatum < datum_binnen then 'PATIENT_OVERLEDEN'	
              else 'OK'
              end
          else 'OK'
          end
      else 'NIET_BEHEERD_IN_ZIS'
    end
else 'GEEN_ZIS_NR'
end pers_check,
case decl_type
	when 'EXTERN' then
		case
			when nvl(ser_id, znr) is null then
				case
					when spec_code is null then 'GEEN_SPECIALISME'
					else
						case bevoegd_declareren
							when 'J' then
								case when rela_voorletters is null then
									'GEEN_VOORLETTERS'									
								else
									'GEEN_SER_ID'
								end		
							when 'N' then 'ONBEVOEGD'
							else '?'
						end	
				end
			else
				case
					when spec_code is null then 'GEEN_SPECIALISME'
					else
						case bevoegd_declareren
							when 'J' then 'OK'
							when 'N' then 'ONBEVOEGD'
							else '?'
						end	
				end
		end
	when 'INTERN' then
		case
			when nvl(znr, ser_id) is null then
				case
					when spec_code is null then 'GEEN_SPECIALISME'
					else
						case bevoegd_declareren
							when 'J' then
								case when rela_voorletters is null then
									'GEEN_VOORLETTERS'									
								else
									'GEEN_ZNR'
								end		
							when 'N' then 'ONBEVOEGD'
							else '?'
						end	
				end
			else
				case
					when spec_code is null then 'GEEN_SPECIALISME'
					else
						case bevoegd_declareren
							when 'J' then 'OK'
							when 'N' then 'ONBEVOEGD'
							else '?'
						end	
				end
		end
	else '?'
end rela_check,
case
    when inst_code is null then
        case
            when dewy_code in ('WDS', 'BU') then
                'GEEN_INSTELLING'
            else
                case
                    when nvl(ser_id, znr) is null then
                        'GEEN_INSTELLING'
                    else
                        'OK'
                end
        end
    else
        case
            when dewy_code in ('WDS', 'BU') then
                case
                    when icode is null then
                        'GEEN_ICODE'
                    else
                        'OK'
                end
            else
                'OK'
        end
end inst_check
from beh_decl_alles_vw decl
where decl.decl_status is null
--and    decl.onde_afgerond = 'J'
and    trunc(decl.decl_datum) > sysdate-(365*2);

/
QUIT
