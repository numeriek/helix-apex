CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ROBOT_INVOER_H2O_VW" ("ONDERZOEKNR", "ONDERZOEKSTYPE", "MONSTERNUMMER", "DATUM_AFNAME", "PROTOCOL_CODE", "PRIORITEIT", "PATIENTNAAM", "PERS_ACHTERNAAM", "GEBOORTEDATUM", "MEETWAARDE", "BINNENKOMST_DATUM", "SESSIE", "METI_ID", "VOLGORDE", "AANTAL") AS
  select rinv.onderzoeknr
       , rinv.onderzoekstype
       , rinv.monsternummer
       , rinv.datum_afname
       , rinv.protocol_code
       , rinv.prioriteit
       , rinv.patientnaam
       , rinv.pers_achternaam
       , rinv.geboortedatum
       , rinv.meetwaarde
       , rinv.binnenkomst_datum
       , rinv.sessie
       , rinv.meti_id
       , rinv.volgorde * 10
       , rinv.aantal
  from   bas_robot_invoer rinv
  where  rinv.sessie is not null and rinv.volgorde is not null
  union all
  select 'H20' -- Genereer 1x H2O op de 10e positie als de output gesorteerd is op het volgorde veld.
       , null
       , 'Water ter controle'
       , null
       , null
       , null
       , null
       , null
       , null
       , null
       , null
       , rinv.sessie
       , null
       , 81
       , null
  from   bas_robot_invoer rinv
  where  rinv.sessie is not null and rinv.volgorde is not null
  group by rinv.sessie
  having count ( * ) >= 9 ;

/
QUIT
