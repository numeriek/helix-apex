CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_LABV_TRVU_VW" ("SAMPLE_NAME", "FRACTIONNR", "TEST", "SAMPLE_NUMMER", "INVESTIGATION_ID", "MEASURE_ID", "POSITION_X", "POSITION_Y", "INDICATION", "ZNR", "VERBORGEN_TRGE_ID", "VERBORGEN_TRVU_ID") AS
  SELECT stan.standaardfractienummer || '_' || stof.omschrijving sample_name,
          stan.standaardfractienummer fractionnr,
          stof.omschrijving test,
          '' sample_nummer,
          '' investigation_id,
          trvu.meet_id measure_id,
          trvu.x_positie position_x,
          trvu.y_positie position_y,
          '' indication,
          mede.vms_username znr,
          trge.trge_id verborgen_trge_id,
          trvu.trvu_id verborgen_trvu_id
     FROM kgc_tray_gebruik trge,
          kgc_tray_vulling trvu,
          bas_metingen meti,
          bas_meetwaarden meet,
          kgc_stoftesten stof,
          kgc_medewerkers mede,
          bas_standaardfracties stan
    WHERE     trge.mede_id = mede.mede_id
          AND trge.trge_id = trvu.trge_id
          AND meti.meti_id = meet.meti_id
          AND meti.stan_id = stan.stan_id
          AND trvu.meet_id = meet.meet_id
          AND meet.stof_id = stof.stof_id
          AND trvu.z_positie = '0'
   UNION
   SELECT frac.fractienummer || '_' || replace(onde.onderzoeknr, '/', '-') || '_' || stof.omschrijving sample_name,
        --frac.fractienummer || '_' || stof.omschrijving sample_name,
          frac.fractienummer fractionnr,
          stof.omschrijving test,
          mons.monsternummer sample_nummer,
--          onde.onderzoeknr investigation_id,
          replace(onde.onderzoeknr, '/', '-') investigation_id,
          trvu.meet_id measure_id,
          trvu.x_positie position_x,
          trvu.y_positie position_y,
--          indi.code indication, -- dubbele records bij onde met meerdere indicaties.
          kgc_indi_00.indi_code(onde.onde_id) indication,
          mede.vms_username znr,
          trge.trge_id verborgen_trge_id,
          trvu.trvu_id verborgen_trvu_id
     FROM kgc_tray_gebruik trge,
          kgc_tray_vulling trvu,
          bas_metingen meti,
          bas_meetwaarden meet,
          kgc_stoftesten stof,
          kgc_medewerkers mede,
          kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
/*          kgc_onderzoek_indicaties onin,
          kgc_indicatie_teksten indi,*/
          kgc_kgc_afdelingen kafd
    WHERE     trge.mede_id = mede.mede_id
          AND trge.trge_id = trvu.trge_id
          AND trvu.meet_id = meet.meet_id
          AND meti.meti_id = meet.meti_id
          AND meet.stof_id = stof.stof_id
          AND onde.onde_id = meti.onde_id(+)
          AND onde.onde_id = onmo.onde_id
          AND onmo.mons_id = mons.mons_id
          AND mons.mons_id = frac.mons_id
          AND frac.frac_id = meti.frac_id
/*          AND onde.onde_id = onin.onde_id
          AND onin.indi_id = indi.indi_id*/
          AND onde.kafd_id = kafd.kafd_id
          AND trvu.z_positie = '0'
          AND kafd.code IN ('GENOOM', 'DNA', 'METAB');

/
QUIT
