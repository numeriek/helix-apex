CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC32_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "FRACTIENR", "CYTOMAT", "TECHNIEK", "VT_NIET_WLST_NIET_AFG", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT -- Overzicht "Inplannen voortesten" - voortesten niet op de werklijst, niet afgerond  -- AUTOMATISCH PROCES
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            CASE
               WHEN FLDS.AVAILABLE = 'NIL' THEN 'REMP'
               ELSE FLDS.AVAILABLE
            END
               AS CYTOMAT,
            UPPER (SUBSTR (STOF.OMSCHRIJVING, -4, 3)) AS TECHNIEK,
            COUNT (*) AS VT_NIET_WLST_NIET_AFG, --aantal1.voortesten_niet_op_wl_niet_afg
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_onderzoek_monsters onmo,
            bas_fracties frac,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            BEH_FLOW_DNA_STORAGE flds,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC32_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED(+) = 'N'
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND FRAC.FRACTIENUMMER = FLDS.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND MEET.AFGEROND = 'N'
            AND (MEET.MEET_REKEN = 'V' OR STGR.CODE LIKE '%KASP_R%')
            AND (   STGR.CODE LIKE '%!_R' ESCAPE '!'
                 OR STGR.CODE LIKE '%!_I' ESCAPE '!'
                 OR STGR.CODE LIKE '%!_S' ESCAPE '!'
                 )         --mantisnr 11341
            AND UPPER (STGR.OMSCHRIJVING) LIKE '%ROBOT%'
            AND MEET.MEET_ID NOT IN (SELECT WLIT1.MEET_ID
                                       FROM KGC_WERKLIJST_ITEMS WLIT1)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            flfa.mede_id,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            FRAC.FRACTIENUMMER,
            CASE
               WHEN FLDS.AVAILABLE = 'NIL' THEN 'REMP'
               ELSE FLDS.AVAILABLE
            END,
            SUBSTR (STOF.OMSCHRIJVING, -4, 3),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
UNION ALL -- MGOL:16-12-2016: met Alwin afgesproken om query uit te breiden met andere onderzoeksgropen die in principe niet in DNA flow horen (daarom extra union)
                -- in union worden alle onderzoeken van onderzoeksgroepen van Genoom afdeling genomen die niet in DNA flow horen (die uiteraard voldoen aan de overige selectie voorwaarden)
                -- kleur/sortering/prioritering wordt niet bepaald aan de hand van de uitgerekende paden omdat er geen pad voor die onderzoeken binnen DNA flow uitgerekend wordt
                -- zie mantis 13871
SELECT DISTINCT -- Overzicht "Inplannen voortesten" - voortesten niet op de werklijst, niet afgerond  -- AUTOMATISCH PROCES
            '#79A8A8',--FLPR.VERBORGEN_KLEUR,
            0,--FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            CASE
               WHEN FLDS.AVAILABLE = 'NIL' THEN 'REMP'
               ELSE FLDS.AVAILABLE
            END
               AS CYTOMAT,
            UPPER (SUBSTR (STOF.OMSCHRIJVING, -4, 3)) AS TECHNIEK,
            COUNT (*) AS VT_NIET_WLST_NIET_AFG, --aantal1.voortesten_niet_op_wl_niet_afg
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_onderzoek_monsters onmo,
            bas_fracties frac,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            BEH_FLOW_DNA_STORAGE flds,
--            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     --FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC32_VW'
--            AND ONDE.KAFD_ID = FLPR.KAFD_ID
--            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            --AND
            ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.KAFD_ID=1
            AND ONDE.ONGR_ID NOT IN (Select FLKO.ONGR_ID from BEH_FLOW_KAFD_ONGR flko where FLKO.KAFD_ID = ONDE.KAFD_ID AND FLKO.FLOW_ID = 1)
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED(+) = 'N'
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND FRAC.FRACTIENUMMER = FLDS.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND MEET.AFGEROND = 'N'
            AND (MEET.MEET_REKEN = 'V' OR STGR.CODE LIKE '%KASP_R%')
            AND (   STGR.CODE LIKE '%!_R' ESCAPE '!'
                 OR STGR.CODE LIKE '%!_I' ESCAPE '!'
                 OR STGR.CODE LIKE '%!_S' ESCAPE '!'
                 )         --mantisnr 11341
            AND UPPER (STGR.OMSCHRIJVING) LIKE '%ROBOT%'
            AND MEET.MEET_ID NOT IN (SELECT WLIT1.MEET_ID
                                       FROM KGC_WERKLIJST_ITEMS WLIT1)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY --FLPR.VERBORGEN_KLEUR,
            --FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            flfa.mede_id,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            FRAC.FRACTIENUMMER,
            CASE
               WHEN FLDS.AVAILABLE = 'NIL' THEN 'REMP'
               ELSE FLDS.AVAILABLE
            END,
            SUBSTR (STOF.OMSCHRIJVING, -4, 3),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY SPOED,
            VERBORGEN_SORTERING,
            EINDDATUM ASC,
            ONDERZOEKNR ASC,
            FRACTIENR ASC;

/
QUIT
