CREATE OR REPLACE FORCE VIEW HELIX.BEH_FLOW_CYTO_PROCES_MC08_VW
(
   VERBORGEN_KLEUR,
   VERBORGEN_SORTERING,
   VERBORGEN_TELLING,
   FAVORIET,
   NAAR_HELIX,
   ONDERZOEKNR,
   MONSTERNR,
   PROTOCOL,
   INDICODE,
   WIJZE,
   NOTITIE,
   NOTIKWEEK,
   EINDDATUM,
   SPOED,
   GROEP,
   BINNEN,
   ONDE_TYPE,
   NOTI_GEREED,
   VERBORGEN_NOTI_ID,
   VERBORGEN_FLOW_ID
)
AS
   WITH W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED(+) = 'N' AND NOTI.ENTITEIT = 'ONDE'),
        W_KGC_NOTITIES_KWFR
        AS (  SELECT LISTAGG (KWFR.CODE || '-' || 'Inzetten voor FISH', ', ') --NOTI.OMSCHRIJVING, ',')
                        WITHIN GROUP (ORDER BY NOTI.ID DESC)
                        AS NOTIKWEEK,
                     KWFR.FRAC_ID
                FROM kgc_notities noti, KGC_KWEEKFRACTIES kwfr
               WHERE     NOTI.GEREED(+) = 'N'
                     AND NOTI.ENTITEIT = 'KWFR'
                     AND KWFR.KWFR_ID = NOTI.ID
                     AND UPPER (NOTI.CODE) LIKE 'VERS_GEN%'
            GROUP BY KWFR.FRAC_ID)
     SELECT                        -- Metingen die toegewezen dienen te worden
           '#79A8A8' VERBORGEN_KLEUR,
            '' VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.ONDERZOEKNR,
            MONS.MONSTERNUMMER MONSTERNR,
            STGR.CODE AS PROTOCOL,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTIKW.NOTIKWEEK,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            ONDE.SPOED,
            ONGR.CODE AS GROEP,
            ONDE.DATUM_BINNEN AS BINNEN,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            NOTI.GEREED AS NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_monsters mons,
            kgc_onderzoek_monsters onmo,
            kgc_onderzoeksgroepen ongr,
            W_KGC_NOTITIES noti,
            bas_metingen meti,
            W_KGC_NOTITIES_KWFR notikw,
            kgc_stoftestgroepen stgr,
            beh_flow_favorieten flfa
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND onde.onde_id = onmo.onde_id
            AND onmo.mons_id = mons.mons_id
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.FRAC_ID = NOTIKW.FRAC_ID(+)
            AND METI.STGR_ID = STGR.STGR_ID
            AND METI.AFGEROND = 'N'
            AND METI.MEDE_ID IS NULL
            AND ONDE.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY '#79A8A8',
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            MONS.MONSTERNUMMER,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN,
            ONDE.GEPLANDE_EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id),
            BEH_HAAL_ONWY_CODE (onde.onde_id),
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            STGR.CODE,
            NOTIKW.NOTIKWEEK,
            NOTI.OMSCHRIJVING,
            NOTI.GEREED,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID
   ORDER BY ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
