CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_STBM_ALLE_MDET_VW" ("ONDE_ID", "FRAC_ID", "MEET_ID", "KOLOM_PASS", "PROMPT", "DB_WAARDE", "WAARDE", "SORTERING") AS
  select onde.onde_id
       , frac.frac_id
       , meet.meet_id
       , decode (mdet1.waarde,
                 null, 'Overige resultaten',
                 'Polymorfisme', 'Overige resultaten',
                 'Mutatie', 'Pathogene mutatie',
                 'class 1', 'Overige resultaten',
                 'class 2', 'UVII',
                 'class 3', 'UVIII',
                 'class 4', 'UVIII',
                 'class 5', 'Pathogene mutatie',
                 mdet1.waarde)
           kolom_pass
       , decode (mdet1.waarde, null, 'Afwijking', 'Mutatie', 'Pathogeen', mdet1.waarde) prompt
       , mdet2.waarde db_waarde
       , (select mwtw.waarde
          from   kgc_mwst_samenstelling mwss
               , kgc_mwss_toegestane_waarden mwtw
          where  mwss.mwss_id = mwtw.mwss_id and mwss.mwst_id = mwst.mwst_id and upper (mwtw.db_waarde) = upper (mdet2.waarde))
           waarde
       , decode (upper (mdet2.prompt),
                 upper ('Naam mutatie'), '1',
                 upper ('Naam afwijking'), '2',
                 upper ('Aminozuur'), '3',
                 upper ('Zygotie'), '4',
                 '9')
           sortering
  from   kgc_onderzoeken onde
       , kgc_monsters mons
       , kgc_onderzoek_monsters onmo
       , bas_fracties frac
       , bas_metingen meti
       , bas_meetwaarden meet
       , bas_meetwaarde_details mdet1
       , bas_meetwaarde_details mdet2
       , kgc_stoftesten stof
       , kgc_meetwaardestructuur mwst
       , bas_meetwaarde_statussen mest
       , (select *
          from   bas_mdet_ok
          where  naar_stamboom = 'J') mdetok
  where  onde.onde_id = onmo.onde_id
     and onmo.mons_id = mons.mons_id
     and mons.mons_id = frac.mons_id
     and frac.frac_id = meti.frac_id
     and onde.onde_id = meti.onde_id(+)
     and meti.meti_id = meet.meti_id
     and meet.meet_id = mdet1.meet_id
     and meet.meet_id = mdet2.meet_id
     and meet.mwst_id = mwst.mwst_id
     and meet.stof_id = stof.stof_id
     and mdet2.mdet_id = mdetok.mdet_id
     and meet.mest_id = mest.mest_id
     and upper (mdet1.prompt) = upper ('Afwijking')
     and (upper (mdet1.waarde) <> 'N' or mdet1.waarde is null)
     and upper (mdet2.prompt) in (upper ('Naam mutatie'), upper ('Naam afwijking'), upper ('Zygotie'), upper ('Aminozuur'))
     and mdet2.waarde is not null
     and upper (meet.afgerond) = 'J'
     and upper (mest.code) = 'GOED'
     and upper (mwst.code) in ('SEQ_ST2', 'SEQ_ST3', 'MIP_ST')
     and user != 'PEDIGREE_RESEARCH' ;

/
QUIT
