CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_PERS_TM_MEET_VW" ("PERS_PERS_ID", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GESLACHT", "PERS_GEBOORTEDATUM", "ONDE_ONDE_ID", "ONDE_ONDERZOEKNR", "ONDE_ONDERZOEKSTYPE", "ONDE_DATUM_BINNEN", "ONDE_AFGEROND", "ONDE_DATUM_AUTORISATIE", "ONDE_DECLAREREN", "KAFD_CODE", "ONGR_CODE", "ONMO_ONMO_ID", "MONS_MONS_ID", "MONS_MONSTERNUMMER", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "MONS_LEEFTIJD", "MATE_MATE_ID", "MATE_CODE", "FRAC_FRAC_ID", "FRAC_FRACTIENUMMER", "METI_METI_ID", "STGR_CODE", "METI_DATUM_AANMELDING", "METI_DECLAREREN", "METI_AFGEROND", "MEET_MEET_ID", "MEET_MEETWAARDE", "MEET_MEETWAARDE_CHAR", "MEET_MEETWAARDE_NUM", "MEET_NORMAALWAARDE_ONDERGRENS", "MEET_NORMAALWAARDE_BOVENGRENS", "MEET_AFGEROND", "MEET_DATUM_AFGEROND", "STOF_STOF_ID", "STOF_CODE") AS
  SELECT pers.pers_id, pers.aanspreken, pers.achternaam, pers.geslacht,
          pers.geboortedatum, onde.onde_id, onde.onderzoeknr,
          onde.onderzoekstype, onde.datum_binnen, onde.afgerond,
          onde.datum_autorisatie, onde.declareren, kafd.code, ongr.code,
          onmo.onmo_id, mons.mons_id, mons.monsternummer,
          mons.datum_aanmelding, mons.datum_afname, mons.leeftijd,
          mate.mate_id, mate.code,
          frac.frac_id, frac.fractienummer, meti.meti_id, stgr.code,
          meti.datum_aanmelding, meti.declareren, meti.afgerond, meet.meet_id,
          meet.meetwaarde,
          SUBSTR (TO_CHAR (meet.meetwaarde), 1, 10) meetwaarde_char,
/*
          DECODE
                ((REPLACE (TRANSLATE (TRIM (meet.meetwaarde),
                                      '.0123456789',
                                      '00000000000'
                                     ),
                           '0',
                           NULL
                          )
                 ),
                 NULL, (TO_NUMBER (TRIM (meet.meetwaarde), '999,999999.99'))
                ) meetwaarde_num,
*/
     case beh_alfu_00.is_nummer(meet.meetwaarde)
        when 'J' then meet.meetwaarde
        else null
       end meetwaarde_num,
          meet.normaalwaarde_ondergrens, meet.normaalwaarde_bovengrens,
          meet.afgerond, meet.datum_afgerond, stof.stof_id, stof.code
     FROM kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          kgc_materialen mate,
          bas_fracties frac,
          bas_metingen meti,
          bas_meetwaarden meet,
          kgc_stoftesten stof,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_stoftestgroepen stgr
    WHERE pers.pers_id = onde.pers_id
      AND onde.onde_id = onmo.onde_id
      AND onmo.mons_id = mons.mons_id
      AND mons.mate_id = mate.mate_id
      AND mons.mons_id = frac.mons_id
      AND frac.frac_id = meti.frac_id
      AND meti.meti_id = meet.meti_id
      AND meet.stof_id = stof.stof_id
      AND onde.kafd_id = kafd.kafd_id
      AND onde.ongr_id = ongr.ongr_id
      AND meti.stgr_id = stgr.stgr_id(+)
      AND onde.onde_id = meti.onde_id(+)
 ;

/
QUIT
