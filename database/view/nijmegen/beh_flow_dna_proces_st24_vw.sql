CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST24_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND_WEEK", "ONDE_TYPE", "FACILITEIT", "TOTAAL", "NOG_NIET_TERUG", "TOT_20_DAGEN", "VAN_21_TOT_30_DAGEN", "VAN_31_TOT_40_DAGEN", "VAN_41_TOT_50_DAGEN", "VAN_51_TOT_60_DAGEN", "VAN_61_DAGEN") AS
  SELECT '#FFFFFF' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            '' AS VERBORGEN_TELLING,
            TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss') - 3,'YYYY-MM') ||'-' ||TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss'), 'IW') AS JAAR_MAAND_WEEK,
            ONDE_TYPE,
            DECODE ("'Faciliteit'",  'HUIS', 'In huis',  'BGI', 'BGI',  '') AS FACILITEIT,
            SUM (CASE WHEN "'Sequencing'" IS NOT NULL THEN 1 ELSE NULL END) AS "TOTAAL",
            SUM (CASE WHEN "'Data beschikbaar'" IS NULL THEN 1 ELSE NULL END) AS "NOG NIET TERUG",
            SUM (CASE WHEN WIDTH_BUCKET (ROUND (TRUNC (TO_DATE ("'Data beschikbaar'", 'dd-mm-rrrr hh24:mi:ss')) - TRUNC (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss')), 0),0,20,1) = 1 THEN 1
                    ELSE 0
                 END)AS "<=20 dagen",
            SUM (CASE WHEN WIDTH_BUCKET (ROUND (TRUNC (TO_DATE ("'Data beschikbaar'", 'dd-mm-rrrr hh24:mi:ss')) - TRUNC (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss')),0),21,30,1) = 1 THEN 1
                    ELSE 0
                 END)AS ">20 en <=30 dagen",
            SUM (CASE
                    WHEN WIDTH_BUCKET (ROUND (TRUNC (TO_DATE ("'Data beschikbaar'",'dd-mm-rrrr hh24:mi:ss')) - TRUNC (TO_DATE ("'Sequencing'",'dd-mm-rrrr hh24:mi:ss')),0),31,40,1) = 1 THEN 1
                    ELSE 0
                 END) AS ">30 en <=40 dagen",
            SUM (CASE
                    WHEN WIDTH_BUCKET (ROUND (TRUNC (TO_DATE ("'Data beschikbaar'", 'dd-mm-rrrr hh24:mi:ss')) - TRUNC (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss')),0),41,50.5,1) = 1 THEN 1
                    ELSE 0
                 END) AS ">40 en <=50 dagen",
            SUM (CASE WHEN WIDTH_BUCKET (ROUND (TRUNC (TO_DATE ("'Data beschikbaar'", 'dd-mm-rrrr hh24:mi:ss')) - TRUNC (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss')),0),51,60,1) = 1 THEN 1
                    ELSE 0
                 END) AS ">50 en <=60 dagen",
            SUM (CASE WHEN WIDTH_BUCKET (ROUND (TRUNC (TO_DATE ("'Data beschikbaar'", 'dd-mm-rrrr hh24:mi:ss')) - TRUNC (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss')),0),61,100000000,1) = 1 THEN 1
                    ELSE 0
                 END) AS ">60 dagen"
       FROM (SELECT mdet.meet_id,
                    PROMPT,
                    WAARDE,
                    ONDE.ONDERZOEKNR,
                    DECODE (ONDE.ONDERZOEKSTYPE, 'R', 'Research', 'D', 'Diagnostiek','') AS ONDE_TYPE,
                    ONDE.DATUM_BINNEN
               FROM kgc_onderzoeken onde,
                    bas_metingen meti,
                    bas_meetwaarden meet,
                    bas_meetwaarde_details mdet,
                    kgc_onderzoeksgroepen ongr,
                    kgc_meetwaardestructuur mwst
              WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                    AND METI.METI_ID = MEET.METI_ID
                    AND MEET.MEET_ID = MDET.MEET_ID
                    AND ONDE.ONGR_ID = ONGR.ONGR_ID
                    AND MEET.MWST_ID = MWST.MWST_ID
                    AND MWST.CODE = 'LIB_ST2') PIVOT (MAX (WAARDE)
                                               FOR (PROMPT)
                                               IN  ('Sequencing',
                                                   'Faciliteit',
                                                   'Status',
                                                   'Data beschikbaar',
                                                   'Data opgehaald',
                                                   'Data geback-upt'))
   WHERE "'Sequencing'" IS NOT NULL
   GROUP BY "'Faciliteit'",
            TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss') - 3,'YYYY-MM') ||'-' ||TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss'), 'IW'),
            '#FFFFFF',
            1,
            '',
            ONDE_TYPE
   ORDER BY JAAR_MAAND_WEEK DESC,
            "'Faciliteit'"





/* oud
     SELECT '#FFFFFF' AS VERBORGEN_KLEUR,
            1 AS VERBORGEN_SORTERING,
            '' AS VERBORGEN_TELLING,
            TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss') - 3,
                     'YYYY-MM')
               AS JAAR_MAAND_NAAR_FACILITEIT,
            TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss'), 'IW')
               AS WEEK_NAAR_FACILITEIT,
            DECODE ("'Faciliteit'",  'HUIS', 'In huis',  'BGI', 'BGI',  '')
               AS FACILITEIT,
            SUM (CASE WHEN "'Data beschikbaar'" IS NULL THEN 1 ELSE NULL END)
            || CASE
                  WHEN "'Data beschikbaar'" IS NULL
                  THEN
                     ' onderzoek(en) '
                     || ROUND (
                           SYSDATE
                           - TRUNC (
                                TO_DATE ("'Sequencing'",
                                         'dd-mm-rrrr hh24:mi:ss'))) --  'NOG NIET TERUG'
                     || ' dagen bij facilitieit'
                  ELSE
                     'TERUG'
               END
               AS STATUS,
            SUM (CASE
                    WHEN WIDTH_BUCKET (
                            ROUND (
                               TRUNC (
                                  TO_DATE ("'Data beschikbaar'",
                                           'dd-mm-rrrr hh24:mi:ss'))
                               - TRUNC (
                                    TO_DATE ("'Sequencing'",
                                             'dd-mm-rrrr hh24:mi:ss'))),
                            0,
                            15,
                            1) = 1 THEN 1
                    ELSE 0
                 END)
               AS "<=15 dagen",
            SUM (CASE
                    WHEN WIDTH_BUCKET (
                            ROUND (
                               TRUNC (
                                  TO_DATE ("'Data beschikbaar'",
                                           'dd-mm-rrrr hh24:mi:ss'))
                               - TRUNC (
                                    TO_DATE ("'Sequencing'",
                                             'dd-mm-rrrr hh24:mi:ss'))),
                            16,
                            30,
                            1) = 1 THEN 1
                    ELSE 0
                 END)
               AS ">15 en <=30 dagen",
            SUM (CASE
                    WHEN WIDTH_BUCKET (
                            ROUND (
                               TRUNC (
                                  TO_DATE ("'Data beschikbaar'",
                                           'dd-mm-rrrr hh24:mi:ss'))
                               - TRUNC (
                                    TO_DATE ("'Sequencing'",
                                             'dd-mm-rrrr hh24:mi:ss'))),
                            31,
                            45,
                            1) = 1 THEN 1
                    ELSE 0
                 END)
               AS ">30 en <=45 dagen",
            SUM (CASE
                    WHEN WIDTH_BUCKET (
                            ROUND (
                               TRUNC (
                                  TO_DATE ("'Data beschikbaar'",
                                           'dd-mm-rrrr hh24:mi:ss'))
                               - TRUNC (
                                    TO_DATE ("'Sequencing'",
                                             'dd-mm-rrrr hh24:mi:ss'))),
                            45,
                            100000000,
                            1) = 1 THEN 1
                    ELSE 0
                 END)
               AS ">45 dagen"
       FROM (SELECT mdet.meet_id,
                    PROMPT,
                    WAARDE,
                    ONDE.ONDERZOEKNR,
                    ONDE.DATUM_BINNEN
               FROM kgc_onderzoeken onde,
                    bas_metingen meti,
                    bas_meetwaarden meet,
                    bas_meetwaarde_details mdet,
                    kgc_onderzoeksgroepen ongr,
                    kgc_meetwaardestructuur mwst
              WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                    AND METI.METI_ID = MEET.METI_ID
                    AND MEET.MEET_ID = MDET.MEET_ID
                    AND ONDE.ONGR_ID = ONGR.ONGR_ID
                    AND MEET.MWST_ID = MWST.MWST_ID
                    AND MWST.CODE = 'LIB_ST2') PIVOT (MAX (WAARDE)
                                               FOR (PROMPT)
                                               IN  ('Sequencing',
                                                   'Faciliteit',
                                                   'Status',
                                                   'Data beschikbaar',
                                                   'Data opgehaald',
                                                   'Data geback-upt'))
      WHERE "'Sequencing'" IS NOT NULL
   GROUP BY "'Faciliteit'",
            TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss') - 3,
                     'YYYY-MM'),
            TO_CHAR (TO_DATE ("'Sequencing'", 'dd-mm-rrrr hh24:mi:ss'), 'IW'),
            CASE
               WHEN "'Data beschikbaar'" IS NULL
               THEN
                  ' onderzoek(en) '
                  || ROUND (
                        SYSDATE
                        - TRUNC (
                             TO_DATE ("'Sequencing'",
                                      'dd-mm-rrrr hh24:mi:ss'))) --  'NOG NIET TERUG'
                  || ' dagen bij facilitieit'
               ELSE
                  'TERUG'
            END,
            '#FFFFFF',
            1,
            ''
   ORDER BY JAAR_MAAND_NAAR_FACILITEIT DESC,
            week_NAAR_FACILITEIT DESC,
            "'Faciliteit'";

*/;

/
QUIT
