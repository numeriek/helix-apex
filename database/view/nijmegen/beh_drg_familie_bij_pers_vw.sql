CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_FAMILIE_BIJ_PERS_VW" ("FAMI_ID", "FAMILIE_NAAM", "FAMILIE_NUMMER", "PERS_ID") AS
  SELECT fale.fami_id, fami.naam, fami.familienummer, fale.pers_id
      FROM kgc_familie_leden fale, kgc_families fami
     WHERE fami.fami_id = fale.fami_id;

/
QUIT
