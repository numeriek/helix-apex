CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_MC06_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "BRIEFTYPE", "ONDTKNR1", "ONDTKND_OP1", "ONDTKNR2", "ONDTKND_OP2", "UITSLAG_CODE", "WIJZE", "ONDE_TYPE", "TOTAAL", "OPEN", "AFGEROND", "STATUS", "NOTITIE", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED (+) = 'N'
                    AND NOTI.ENTITEIT = 'ONDE')
    SELECT                                       -- autoriseren van onderzoek
           '#79A8A8',                                  --FLPR.VERBORGEN_KLEUR,
            '',                                    --FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BRTY.CODE AS BRIEFTYPE,
            MEDE.CODE AS ONDTKNR1,
            UITS.DATUM_MEDE_ID AS ONDTKND_OP1,
            MEDE2.CODE AS ONDTKNR2,
            UITS.DATUM_MEDE_ID2 AS ONDTKND_OP2,
            ONUI.CODE AS UITSLAG_CODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            omm.TOTAAL_METI,
            omm.METI_NIET_AF,
            omm.METI_AF,
            CASE
               WHEN omm.TOTAAL_METI = omm.METI_AF THEN 'ALLES KLAAR'
               ELSE ''
            END
               CASE,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_uitslagen uits,
            kgc_brieftypes brty,
            kgc_medewerkers mede,
            kgc_medewerkers mede2,
            kgc_onderzoek_uitslagcodes onui,
            W_KGC_NOTITIES noti,
            beh_flow_favorieten flfa,
            BEH_FLOW_ONDE_MEET_METI_MATVW omm
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND UITS.ONDE_ID = ONDE.ONDE_ID
            AND UITS.BRTY_ID = BRTY.BRTY_ID
            AND UITS.MEDE_ID = MEDE.MEDE_ID(+)
            AND UITS.MEDE_ID2 = MEDE2.MEDE_ID(+)
            AND ONDE.ONUI_ID = ONUI.ONUI_ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = omm.onde_id(+)
            AND ONDE.AFGEROND = 'N'
            AND (   UITS.MEDE_ID IS NOT NULL
                 OR UITS.MEDE_ID2 IS NOT NULL
                 OR NOTI.CODE IN ('GEREED_AUTORISATIE', 'STAF_ONDE_VERV'))
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY ONDE.SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
