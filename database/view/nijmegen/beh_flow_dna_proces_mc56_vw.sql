CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC56_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "BI_BU", "INT_EXT", "TAAL", "WIJZE", "ONDE_TYPE", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "FRACTIENR", "WERKLIJSTNR", "CODE", "TOTAAL", "NCONT", "WCONT", "OPEN_TECHNIEK", "NOTITIE", "NOTI_GEREED", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT                                         -- controleren meetwaarden
           CASE
               WHEN ONDE.GEPLANDE_EINDDATUM - TRUNC (SYSDATE) <= 2
               THEN
                  '#CD5B5B'                                             --rood
               ELSE
                  CASE
                     WHEN ONDE.GEPLANDE_EINDDATUM - TRUNC (SYSDATE) > 2
                          AND ONDE.GEPLANDE_EINDDATUM - TRUNC (SYSDATE) <= 5
                     THEN
                        '#ED8B1C'                                     --oranje
                     ELSE
                        '#5CAB5C'                                      --groen
                  END
            END
               AS VERBORGEN_KLEUR,
            '' VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            beh_haal_familienr (ONDE.PERS_ID) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU')
               AS BI_BU,
            DECODE (beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
                    'J', 'INTERN',
                    'N', '')
               AS INT_EXT,
            TAAL.CODE TAAL,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            BEH_HAAL_FRACTIENR_MATE (onde.onde_id) AS FRACTIENR,
            TO_CHAR (WM_CONCAT (DISTINCT WLST.WERKLIJST_NUMMER)) AS WERKLIJSTNR,
            TECH.CODE,
            COUNT (MEET.MEET_ID) AS totaal,
            CASE WHEN MEET.MEST_ID IS NULL THEN COUNT (MEET.MEET_ID) ELSE 0 END
               AS NCONT,
            CASE
               WHEN MEET.MEST_ID IS NOT NULL THEN COUNT (MEET.MEET_ID)
               ELSE 0
            END
               AS WCONT,
            LOTE.CODE AS OPEN_TECHNIEK,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            kgc_uitslagen uits,
            kgc_notities noti,
            beh_flow_favorieten flfa,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_stoftesten stof,
            kgc_technieken tech,
            kgc_werklijsten wlst,
            kgc_werklijst_items wlit,
            KGC_TALEN TAAL,
            (  SELECT onde_id,
                      listagg (CODE, ',') WITHIN GROUP (ORDER BY CODE) AS code
                 FROM (  SELECT ONDE.ONDE_ID, TECH.CODE
                           FROM kgc_onderzoeken onde,
                                bas_metingen meti,
                                bas_meetwaarden meet,
                                kgc_stoftesten stof,
                                kgc_technieken tech
                          WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                                AND METI.METI_ID = MEET.METI_ID
                                AND MEET.STOF_ID = STOF.STOF_ID
                                AND STOF.TECH_ID = TECH.TECH_ID
                                AND MEET.AFGEROND = 'N'
                                AND ONDE.AFGEROND = 'N'
                       GROUP BY ONDE.ONDE_ID, TECH.CODE)
             GROUP BY ONDE_ID) LOTE
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 1) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED(+) = 'N'
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = UITS.ONDE_ID
            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND MEET.STOF_ID = STOF.STOF_ID
            AND STOF.TECH_ID = TECH.TECH_ID
            AND WLIT.WLST_ID = WLST.WLST_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID(+)
            AND STOF.MEET_REKEN IN ('M', 'S')
            AND STOF.CODE NOT LIKE '%KASP%' -- MG 05-01-16 toegevoegd om KASP testen uit te sluiten
            AND STOF.CODE NOT LIKE '%!_SNP' ESCAPE '!'
            AND TECH.CODE <> 'EXA' -- MG 09-02-16  tijdelijk toegevoegd om NGS testen uit te sluiten - deze voorwaarde mag weg op het moment dat via Bioinformatica stoftesten ook goedgekeurd worden
            AND ONDE.ONDE_ID = LOTE.ONDE_ID(+)
            AND NOT EXISTS
                       (SELECT 1
                          FROM bas_meetwaarden meet
                         WHERE     MEET.AFGEROND = 'N'
                               AND MEET.METI_ID = METI.METI_ID
                               AND MEET.MEET_REKEN IN ('S', 'M'))
            AND EXISTS
                   (SELECT 1
                      FROM bas_meetwaarden meet,
                           bas_metingen meti,
                           kgc_technieken tech,
                           kgc_stoftesten stof
                     WHERE     MEET.MEST_ID IS NULL
                           AND MEET.METI_ID = METI.METI_ID
                           AND METI.ONDE_ID = ONDE.ONDE_ID
                           AND MEET.STOF_ID = STOF.STOF_ID
                           AND STOF.TECH_ID = TECH.TECH_ID
                           AND MEET.MEET_REKEN IN ('S', 'M')
                           AND TECH.CODE <> 'EXA'
                           AND STOF.CODE NOT LIKE '%KASP%'
                           AND STOF.CODE NOT LIKE '%!_SNP%' ESCAPE '!')
   GROUP BY '#79A8A8',
            '',
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            beh_haal_familienr (ONDE.PERS_ID),
            ONDE.ONDERZOEKNR,
            DECODE (beh_onde_buitenland_00.beh_onde_bu (ONDE.ONDE_ID),
                    'NEDERLAND', '',
                    'BUITENLAND', 'BU'),
            DECODE (beh_onde_buitenland_00.beh_onde_rela_is_intern (ONDE.ONDE_ID),
                    'J', 'INTERN',
                    'N', ''),
            TAAL.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            TECH.CODE,
            BEH_HAAL_FRACTIENR_MATE (onde.onde_id),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            NOTI.GEREED,
            FLFA.FLOW_ID,
            LOTE.CODE,
            MEET.MEST_ID
      ORDER BY SPOED ASC, ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
