CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_ST08_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "GROEP", "ONDERZOEKNR", "INDICODE", "ONDE_TYPE", "PROTOCOL", "MONSTERNR", "DATUM_AFNAME", "UITSLAG", "CREATION_DATE", "BINNEN_OP", "PROT_DATUM_AANMELDING", "GOEDGEKEURD_OP", "WERKLIJST_OP", "STOF_DATUM_AFGEROND", "PROT_DATUM_AFGEROND", "UITSLAG_DAT", "ONDERTEKEN_DAT", "AUTORISATIE_DAT", "TOT_UITSLAG", "TOT_ONDERTEKENING", "TOT_AUTORISATIE", "TOTAAL") AS
  SELECT VERBORGEN_KLEUR,
       VERBORGEN_SORTERING,
       VERBORGEN_TELLING,
       GROEP,
       ONDERZOEKNR,
       INDICODE,
       ONDE_TYPE,
       PROTOCOL,
       MONSTERNUMMER,
       DATUM_AFNAME,
       UITSLAG,
       CREATION_DATE,
       BINNEN_OP,
       PROT_DATUM_AANMELDING,
       GOEDGEKEURD_OP,
       WERKLIJST_OP,
       STOF_DATUM_AFGEROND,
       PROT_DATUM_AFGEROND,
       UITSLAG_DAT,
       ONDERTEKEN_DAT,
       AUTORISATIE_DAT,
       TOT_UITSLAG,
       TOT_ONDERTEKENING,
       TOT_AUTORISATIE,
       TOTAAL
  FROM BEH_FLOW_METAB_DTIJDEN

--     SELECT '#FFFFFF' VERBORGEN_KLEUR,
--            1 VERBORGEN_SORTERING,
--            '' VERBORGEN_TELLING,
--            ONGR.CODE GROEP,
--            ONDE.ONDERZOEKNR,
--            BEH_HAAL_INDI_CODE (ONDE.ONDE_ID) AS INDICODE,
--            DECODE (ONDE.ONDERZOEKSTYPE,
--                    'R', 'Research',
--                    'D', 'Diagnostiek',
--                    'C', 'Controle',
--                    '')
--               AS ONDE_TYPE,
--            STGR.CODE PROTOCOL,
--            MONS.MONSTERNUMMER,
--            MONS.DATUM_AFNAME,
--            ONUI.CODE || '-' || ONUI.OMSCHRIJVING AS UITSLAG,
--            TO_CHAR (ONDE.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS')
--               AS CREATION_DATE,
--            TO_CHAR (DATBIN.BINNEN_OP, 'dd-mm-yyyy HH24:MI:SS') AS BINNEN_OP,
--            TO_CHAR (METI.DATUM_AANMELDING, 'dd-mm-yyyy HH24:MI:SS')
--               AS PROT_DATUM_AANMELDING,
--            TO_CHAR (AULO.DATUM_TIJD, 'dd-mm-yyyy HH24:MI:SS')
--               AS GOEDGEKEURD_OP,
--            TO_CHAR (WLSTDAT.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS')
--               AS WERKLIJST_OP,
--            TO_CHAR (MEET.STOF_DATUM_AFGEROND, 'dd-mm-yyyy HH24:MI:SS')
--               AS STOF_DATUM_AFGEROND,
--            TO_CHAR (METI.DATUM_AFGEROND, 'dd-mm-yyyy HH24:MI:SS')
--               AS PROT_DATUM_AFGEROND,
--            TO_CHAR (UITSDAT.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS')
--               AS UITSLAG_DAT,
--            TO_CHAR (UITSDAT.DATUM_MEDE_ID, 'dd-mm-yyyy HH24:MI:SS')
--               AS ONDERTEKEN_DAT,
--            TO_CHAR (OAHIDAT.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS')
--               AS AUTORISATIE_DAT,
--            SUBSTR (
--               SUBSTR (
--                  NUMTODSINTERVAL (
--                     (UITSDAT.CREATION_DATE - WLSTDAT.CREATION_DATE),
--                     'day'),
--                  6),
--               1,
--               14)
--               AS TOT_UITSLAG,
--            SUBSTR (
--               SUBSTR (
--                  NUMTODSINTERVAL (
--                     (UITSDAT.DATUM_MEDE_ID - UITSDAT.CREATION_DATE),
--                     'day'),
--                  6),
--               1,
--               14)
--               AS TOT_ONDERTEKENING,
--            SUBSTR (
--               SUBSTR (
--                  NUMTODSINTERVAL (
--                     (OAHIDAT.CREATION_DATE - UITSDAT.DATUM_MEDE_ID),
--                     'day'),
--                  6),
--               1,
--               14)
--               AS TOT_AUTORISATIE,
--            SUBSTR (
--               SUBSTR (
--                  NUMTODSINTERVAL ( (OAHIDAT.CREATION_DATE - DATBIN.BINNEN_OP),
--                                   'day'),
--                  6),
--               1,
--               14)
--               AS TOTAAL
--       FROM (SELECT ONDE_ID,
--                    CASE
--                       WHEN TRUNC (CREATION_DATE) <= TRUNC (DATUM_BINNEN)
--                       THEN
--                          CREATION_DATE
--                       ELSE
--                          DATUM_BINNEN
--                    END
--                       AS BINNEN_OP
--               FROM kgc_onderzoeken) DATBIN,
--            (  SELECT ID AS ONDE_ID, MIN (DATUM_TIJD) AS DATUM_TIJD
--                 FROM kgc_audit_log
--                WHERE KOLOM_NAAM = 'STATUS' AND WAARDE_NIEUW = 'G'
--             GROUP BY id) AULO,
--            (  SELECT MIN (WLST.CREATION_DATE) AS CREATION_DATE, METI.METI_ID
--                 FROM bas_metingen meti,
--                      kgc_werklijst_items wlit,
--                      kgc_werklijsten wlst
--                WHERE     METI.METI_ID = WLIT.METI_ID
--                      AND WLIT.WLST_ID = WLST.WLST_ID
--                      AND METI.CREATION_DATE <= WLST.CREATION_DATE
--             GROUP BY METI.METI_ID) WLSTDAT,
--            (  SELECT MIN (CREATION_DATE) AS CREATION_DATE,
--                      ONDE_ID,
--                      MIN (DATUM_MEDE_ID) AS DATUM_MEDE_ID
--                 FROM kgc_uitslagen
--             GROUP BY ONDE_ID) UITSDAT,
--            (  SELECT MIN (CREATION_DATE) AS CREATION_DATE, ONDE_ID
--                 FROM kgc_onde_autorisatie_historie
--             GROUP BY ONDE_ID) OAHIDAT,
--            (  SELECT MAX (MEET.DATUM_AFGEROND) AS STOF_DATUM_AFGEROND,
--                      MEET.METI_ID
--                 FROM bas_meetwaarden meet
--             GROUP BY MEET.METI_ID) MEET,
--            kgc_onderzoeken onde,
--            kgc_onderzoeksgroepen ongr,
--            kgc_onderzoekswijzen onwy,
--            kgc_onderzoek_uitslagcodes onui,
--            bas_metingen meti,
--            bas_fracties frac,
--            kgc_monsters mons,
--            kgc_stoftestgroepen stgr
--      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
--                                   FROM BEH_FLOW_KAFD_ONGR fkon
--                                  WHERE FKON.FLOW_ID IN (2, 3))
--            AND ONDE.ONGR_ID = ONGR.ONGR_ID
--            AND ONDE.KAFD_ID = ONWY.KAFD_ID(+)
--            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE(+)
--            AND ONDE.ONDE_ID = DATBIN.ONDE_ID
--            AND ONDE.ONDE_ID = AULO.ONDE_ID(+)
--            AND ONDE.ONDE_ID = UITSDAT.ONDE_ID(+)
--            AND ONDE.ONDE_ID = OAHIDAT.ONDE_ID
--            AND ONDE.ONUI_ID = ONUI.ONUI_ID(+)
--            AND ONDE.ONDE_ID = METI.ONDE_ID(+)
--            AND METI.STGR_ID = STGR.STGR_ID(+)
--            AND METI.METI_ID = WLSTDAT.METI_ID(+)
--            AND ONDE.AFGEROND = 'J'
--            AND METI.FRAC_ID = FRAC.FRAC_ID
--            AND FRAC.MONS_ID = MONS.MONS_ID
--            AND METI.METI_ID = MEET.METI_ID(+)
--            AND TO_CHAR (ONDE.CREATION_DATE, 'rrrr') >= '2011'
--   ORDER BY ONDE.ONDE_ID DESC;

/
QUIT
