CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_NOTITIES_VW" ("NOTI_ID", "ENTITEIT", "ID", "ENTITEIT_OMSCHRIJVING", "ENTITEIT_OMSCHRIJVING_KORT", "BESTANDEN", "BI_BU", "EINDDATUM", "FAMILIENUMMER", "ARCHIEF", "OMSCHRIJVING", "CODE", "COMMENTAAR", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "PERS_ID", "AFDELING", "ONGR_CODE") AS
  WITH W_KGC_BESTANDEN AS (SELECT *
                              FROM (SELECT BEST.*,
                                           LISTAGG (
                                              CASE
                                                 WHEN BEST.
                                                       BESTAND_SPECIFICATIE
                                                         IS NULL
                                                 THEN
                                                    ''
                                                 ELSE
                                                    '<a href="'
                                                    || BEST.
                                                        BESTAND_SPECIFICATIE
                                                    || '" target="_blank">Open</a>'
                                              END,
                                              ' ')
                                           WITHIN GROUP (ORDER BY
                                                            BEST.ENTITEIT_PK)
                                           OVER (
                                              PARTITION BY BEST.ENTITEIT_PK)
                                              BESTANDEN,
                                           ROW_NUMBER ()
                                           OVER (
                                              PARTITION BY BEST.ENTITEIT_PK
                                              ORDER BY BEST.ENTITEIT_PK)
                                              AS ROW_NR
                                      FROM KGC_BESTANDEN BEST,
                                           KGC_BESTAND_CATEGORIEEN BCAT
                                     WHERE BEST.BCAT_ID = BCAT.BCAT_ID
                                           AND BEST.ENTITEIT_CODE IN
                                                  ('FAMI', 'ONDE')
                                           AND BCAT.CODE <> 'DUMMY')
                             WHERE ROW_NR = 1)
   SELECT -- PERS
          NOTI.NOTI_ID,
          NOTI.ENTITEIT,
          NOTI.ID,
          KGC_NOTI_00.BASIS_OMSCHRIJVING (NOTI.ENTITEIT, NOTI.ID) AS ENTITEIT_OMSCHRIJVING,
          PERS.ZISNR AS ENTITEIT_OMSCHRIJVING_KORT, -- selectie nodig voor het wijzigen van notitie in FLOW
          '' AS BESTANDEN,
          '' AS BI_BU,
          NULL AS EINDDATUM,
          BEH_HAAL_FAMILIENR(pers.pers_id) AS FAMILIENUMMER,
          '' AS ARCHIEF,
          NOTI.OMSCHRIJVING,
          NOTI.CODE,
          NOTI.COMMENTAAR,
          NOTI.CREATED_BY,
          NOTI.CREATION_DATE,
          NOTI.LAST_UPDATED_BY,
          NOTI.LAST_UPDATE_DATE,
          PERS.PERS_ID,
          TO_CHAR ('') AFDELING,
          TO_CHAR ('') ONGR_CODE
     FROM KGC_NOTITIES NOTI, KGC_PERSONEN PERS
    WHERE NOTI.ENTITEIT = 'PERS' AND NOTI.ID = PERS.PERS_ID
   UNION ALL
   SELECT                                                          --  Familie
         noti.noti_id,
          noti.entiteit,
          noti.id,
          kgc_noti_00.basis_omschrijving (noti.entiteit, noti.id) AS ENTITEIT_OMSCHRIJVING,
          FAMI.FAMILIENUMMER, -- selectie nodig voor het wijzigen van notitie in FLOW
          best.bestanden AS bestanden,
          '' AS BI_BU,
          NULL AS EINDDATUM,
          BEH_HAAL_FAMILIENR(fale.pers_id) AS FAMILIENUMMER,
          FAMI.ARCHIEF AS ARCHIEF,
          noti.omschrijving,
          noti.code,
          noti.commentaar,
          noti.created_by,
          noti.creation_date,
          noti.last_updated_by,
          noti.last_update_date,
          fale.pers_id,
          TO_CHAR ('') afdeling,
          TO_CHAR ('') ongr_code
     FROM kgc_notities noti,
          kgc_familie_leden fale,
          kgc_families fami,
          w_kgc_bestanden best
    WHERE     noti.entiteit = 'FAMI'
          AND NOTI.ID = BEST.ENTITEIT_PK(+)
          AND noti.id = fale.fami_id
          AND FALE.FAMI_ID = FAMI.FAMI_ID
          AND fale.pers_id = (SELECT fale1.pers_id
                                FROM kgc_familie_leden fale1
                               WHERE fale1.fami_id = noti.id AND ROWNUM = 1)
   UNION ALL
   -- MONS
   SELECT noti.noti_id,
          noti.entiteit,
          noti.id,
          kgc_noti_00.basis_omschrijving (noti.entiteit, noti.id) as ENTITEIT_OMSCHRIJVING,
          MONS.MONSTERNUMMER, -- selectie nodig voor het wijzigen van notitie in FLOW
          '',
          '' AS BI_BU,
          NULL AS EINDDATUM,
          BEH_HAAL_FAMILIENR(mons.pers_id) AS FAMILIENUMMER,
          '' AS ARCHIEF,
          noti.omschrijving,
          noti.code,
          noti.commentaar,
          noti.created_by,
          noti.creation_date,
          noti.last_updated_by,
          noti.last_update_date,
          mons.pers_id,
          kafd.code afdeling,
          ongr.code ongr_code
     FROM kgc_notities noti,
          kgc_monsters mons,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd
    WHERE     noti.entiteit = 'MONS'
          AND noti.id = mons.mons_id
          AND mons.ongr_id = ongr.ongr_id
          AND mons.kafd_id = kafd.kafd_id
   UNION ALL
   -- ONDE
   SELECT noti.noti_id,
          noti.entiteit,
          noti.id,
          kgc_noti_00.basis_omschrijving (noti.entiteit, noti.id) as ENTITEIT_OMSCHRIJVING,
          ONDE.ONDERZOEKNR, -- selectie nodig voor het wijzigen van notitie in FLOW
          best.bestanden AS bestanden,
          beh_onde_bu (ONDE.ONDE_ID) AS BI_BU,
          ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
          BEH_HAAL_FAMILIENR(onde.pers_id) AS FAMILIENUMMER,
          '' AS ARCHIEF,
          noti.omschrijving,
          noti.code,
          noti.commentaar,
          noti.created_by,
          noti.creation_date,
          noti.last_updated_by,
          noti.last_update_date,
          onde.pers_id,
          kafd.code afdeling,
          ongr.code ongr_code
     FROM kgc_notities noti,
          kgc_onderzoeken onde,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd,
          w_kgc_bestanden best
    WHERE     noti.entiteit = 'ONDE'
          AND noti.id = onde.onde_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.kafd_id = kafd.kafd_id
          AND NOTI.ID = BEST.ENTITEIT_PK(+)
   UNION ALL
   -- FRAC
   SELECT noti.noti_id,
          noti.entiteit,
          noti.id,
          kgc_noti_00.basis_omschrijving (noti.entiteit, noti.id) as ENTITEIT_OMSCHRIJVING,
          FRAC.FRACTIENUMMER, -- selectie nodig voor het wijzigen van notitie in FLOW
          '',
          '' AS BI_BU,
          NULL AS EINDDATUM,
          BEH_HAAL_FAMILIENR(mons.pers_id) AS FAMILIENUMMER,
          '' AS ARCHIEF,
          noti.omschrijving,
          noti.code,
          noti.commentaar,
          noti.created_by,
          noti.creation_date,
          noti.last_updated_by,
          noti.last_update_date,
          mons.pers_id,
          kafd.code afdeling,
          ongr.code ongr_code
     FROM kgc_notities noti,
          bas_fracties frac,
          kgc_monsters mons,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd
    WHERE     noti.entiteit = 'FRAC'
          AND noti.id = frac.frac_id
          AND frac.mons_id = mons.mons_id
          AND mons.ongr_id = ongr.ongr_id
          AND mons.kafd_id = kafd.kafd_id
   UNION ALL
   -- MEET
   SELECT noti.noti_id,
          noti.entiteit,
          noti.id,
          kgc_noti_00.basis_omschrijving (noti.entiteit, noti.id)
             entiteit_omschrijving,
          '',
          '',
          '' AS BI_BU,
          NULL AS EINDDATUM,
          BEH_HAAL_FAMILIENR(onde.pers_id) AS FAMILIENUMMER,
          '' AS ARCHIEF,
          noti.omschrijving,
          noti.code,
          noti.commentaar,
          noti.created_by,
          noti.creation_date,
          noti.last_updated_by,
          noti.last_update_date,
          onde.pers_id,
          kafd.code afdeling,
          ongr.code ongr_code
     FROM kgc_notities noti,
          bas_meetwaarden meet,
          bas_metingen meti,
          kgc_onderzoeken onde,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd
    WHERE     noti.entiteit = 'MEET'
          AND noti.id = meet.meet_id
          AND meet.meti_id = meti.meti_id
          AND meti.onde_id = onde.onde_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.kafd_id = kafd.kafd_id
   UNION ALL
   -- UITS
   SELECT noti.noti_id,
          noti.entiteit,
          noti.id,
          kgc_noti_00.basis_omschrijving (noti.entiteit, noti.id)
             entiteit_omschrijving,
          '',
          '',
          beh_onde_bu (ONDE.ONDE_ID) AS BI_BU,
          NULL AS EINDDATUM,
          BEH_HAAL_FAMILIENR(onde.pers_id) AS FAMILIENUMMER,
          '' AS ARCHIEF,
          noti.omschrijving,
          noti.code,
          noti.commentaar,
          noti.created_by,
          noti.creation_date,
          noti.last_updated_by,
          noti.last_update_date,
          onde.pers_id,
          kafd.code afdeling,
          ongr.code ongr_code
     FROM kgc_notities noti,
          kgc_uitslagen uits,
          kgc_onderzoeken onde,
          kgc_onderzoeksgroepen ongr,
          kgc_kgc_afdelingen kafd
    WHERE     noti.entiteit = 'UITS'
          AND noti.id = uits.uits_id
          AND uits.onde_id = onde.onde_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.kafd_id = kafd.kafd_id
   UNION ALL
   -- BRIE
   SELECT noti.noti_id,
          noti.entiteit,
          noti.id,
          kgc_noti_00.basis_omschrijving (noti.entiteit, noti.id) as ENTITEIT_OMSCHRIJVING,
          '',
          '',
          '' AS BI_BU,
          NULL AS EINDDATUM,
          '' AS FAMILIENUMMER,
          '' AS ARCHIEF,
          noti.omschrijving,
          noti.code,
          noti.commentaar,
          noti.created_by,
          noti.creation_date,
          noti.last_updated_by,
          noti.last_update_date,
          brie.pers_id,
          kafd.code afdeling,
          TO_CHAR ('') ongr_code
     FROM kgc_notities noti, kgc_brieven brie, kgc_kgc_afdelingen kafd
    WHERE     noti.entiteit = 'BRIE'
          AND noti.id = brie.brie_id
          AND brie.kafd_id = kafd.kafd_id;

/
QUIT
