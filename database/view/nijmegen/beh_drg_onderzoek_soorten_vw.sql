CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_ONDERZOEK_SOORTEN_VW" ("ONGR_ID", "ONGR_CODE", "SOORT_ONDERZOEK", "KAFD_ID", "KGC_AFDELING") AS
  SELECT ongr.ongr_id,
             ongr.code ongr_code,
             kgc_attr_00.
              waarde ('KGC_ONDERZOEKSGROEPEN',
                         'DRAGON_ONDE_SOORT',
                         ongr.ongr_id
                        )
                 soort_onderzoek,
             ongr.kafd_id,
             kafd.code kgc_afdeling
      FROM kgc_onderzoeksgroepen ongr, kgc_kgc_afdelingen kafd
     WHERE ongr.kafd_id = kafd.kafd_id AND kafd.code IN ('GENOOM', 'METAB')
             AND ongr.code IN
                      ('P',
                        'LTG',
                        'ARR',
                        'NGS',
                        'POST_NIJM',
                        'TUM_NIJM',
                        'BASIS',
                        'HOM',
                        'MITO',
                        'LYSO',
                        'GLYCOS',
                        'DD',
                        'KWEEK');

/
QUIT
