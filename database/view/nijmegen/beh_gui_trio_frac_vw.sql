CREATE OR REPLACE FORCE VIEW HELIX.BEH_GUI_TRIO_FRAC_VW
(
   PERS_ID,
   ONDE_ID,
   FAMILIENUMMER,
   FAMILIEONDERZOEKNR,
   AANSPREKEN,
   GEBOORTEDATUM,
   GESLACHT,
   ONDERZOEKNR,
   AANTAL_ONDERZOEKEN,
   FAMI_ROL,
   FRACTIES,
   AANTAL_FRACTIES
)
AS
-- MGOL: aangepast voor PRENTALE NGS onderzoeken, oude deel is uitgecommentariseerd
       SELECT pers.pers_id,
          onde.onde_id,
          fami.familienummer,
          faon.familieonderzoeknr,
          pers.aanspreken,
          pers.geboortedatum,
          pers.geslacht,
          onde.onderzoeknr,
          COUNT (*) OVER (PARTITION BY faon.familieonderzoeknr) AS aantal_onderzoeken,
          CASE WHEN FOET.FOBE_ID IS NOT NULL THEN FOET.FAM_RELATIE ELSE
          CASE WHEN (pers.geboortedatum = MAX (pers.geboortedatum) OVER (PARTITION BY fami.familienummer, faon.familieonderzoeknr)) 
                            AND (COUNT (*) OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr) = 3) 
                            AND (COUNT (*) OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr,pers.pers_id) = 1)
             THEN 'KIND'
                   WHEN (pers.geboortedatum !=MAX (pers.geboortedatum)OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr))
                             AND (COUNT (*) OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr) = 3)
                             AND (COUNT (*) OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr,pers.pers_id) = 1)
                             AND (pers.geslacht = 'M')
             THEN 'VADER'
                   WHEN (pers.geboortedatum !=MAX (pers.geboortedatum) OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr))
                            AND (COUNT (*) OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr) = 3)
                            AND (COUNT (*) OVER (PARTITION BY fami.familienummer,faon.familieonderzoeknr,pers.pers_id) = 1)
                            AND (pers.geslacht = 'V')
             THEN 'MOEDER'
             ELSE '?'
          END
          END
             fami_rol,
          frac.fracties,
          frac.aantal_fracties
     FROM kgc_families fami,
          kgc_familie_onderzoeken faon,
          kgc_faon_betrokkenen fobe,
          kgc_onderzoeken onde,
          kgc_personen pers,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          (SELECT DISTINCT
                  pers_id,
                  onde_id,
                  mons_id,
                  LISTAGG (fractienummer, ',')
                     WITHIN GROUP (ORDER BY pers_id, onde_id)
                     OVER (PARTITION BY pers_id, onde_id)
                     fracties,
                  aantal_fracties
             FROM (SELECT UNIQUE
                          pers.pers_id,
                          onde.onde_id,
                          mons.mons_id,
                          frac.fractienummer,
                          COUNT (*)
                             OVER (PARTITION BY pers.pers_id, onde.onde_id)
                             aantal_fracties
                     FROM kgc_personen pers,
                          kgc_onderzoeken onde,
                          kgc_monsters mons,
                          kgc_onderzoek_monsters onmo,
                          bas_fracties frac,
                          kgc_kgc_afdelingen kafd,
                          kgc_onderzoeksgroepen ongr,
                          bas_metingen meti,
                          kgc_stoftestgroepen stgr
                    WHERE     pers.pers_id = onde.pers_id
                          AND onde.onde_id = onmo.onde_id
                          AND onmo.mons_id = mons.mons_id
                          AND mons.mons_id = frac.mons_id
                          AND frac.frac_id = meti.frac_id
                          AND meti.stgr_id = stgr.stgr_id
                          AND meti.onde_id = onde.onde_id
                          AND onde.kafd_id = kafd.kafd_id
                          AND onde.ongr_id = ongr.ongr_id
                          AND stgr.code LIKE 'NGS%')) frac,
        (select FAON.FAMILIEONDERZOEKNR, 
                ONDE.ONDERZOEKNR, 
                PERS.AANSPREKEN, 
                PERS.GESLACHT, 
                MONS.FOET_ID, 
                FOBE.FOBE_ID,
                case when  MONS.FOET_ID is not null THEN 'KIND' 
                       ELSE CASE WHEN PERS.GESLACHT= 'V' then 'MOEDER' ELSE 'VADER'  
                       END 
                END As Fam_relatie 
        from kgc_familie_onderzoeken faon, 
                kgc_faon_betrokkenen fobe, 
                kgc_onderzoeken onde, 
                kgc_personen pers, 
                kgc_monsters mons, 
                kgc_onderzoek_monsters onmo
        where FAON.FAON_ID = FOBE.FAON_ID
            and FOBE.ONDE_ID = ONDE.ONDE_ID
            and ONDE.PERS_ID = PERS.PERS_ID
            and ONDE.ONDE_ID = ONMO.ONDE_ID
            and ONMO.MONS_ID = MONS.MONS_ID  
            and PERS.PERS_ID= MONS.PERS_ID
            and exists (select 1 
                            from kgc_faon_betrokkenen fobe1, 
                                    kgc_onderzoeken onde1, 
                                    kgc_monsters mons1, 
                                    kgc_onderzoek_monsters onmo1 
                            where FAON.FAON_ID = FOBE1.FAON_ID 
                                and FOBE1.ONDE_ID = ONDE1.ONDE_ID  
                                and ONDE1.ONDE_ID = ONMO1.ONDE_ID 
                                and ONMO1.MONS_ID = MONS1.MONS_ID 
                                and MONS1.FOET_ID is not null
                           )
                   ) foet                          
    WHERE     fami.fami_id = faon.fami_id
          AND faon.faon_id = fobe.faon_id
          AND fobe.pers_id = pers.pers_id
          AND fobe.onde_id = onde.onde_id(+)
          AND FOBE.FOBE_ID = FOET.FOBE_ID (+)
          AND onde.onde_id = frac.onde_id
          AND pers.pers_id = frac.pers_id
          AND faon.kafd_id = kafd.kafd_id
          AND faon.ongr_id = ongr.ongr_id
          AND kafd.code = 'GENOOM'
          AND ongr.code = 'NGS'
          AND TRUNC (faon.creation_date) > TO_DATE ('01-07-2013', 'dd-mm-yyyy')
          ;
/
QUIT          
