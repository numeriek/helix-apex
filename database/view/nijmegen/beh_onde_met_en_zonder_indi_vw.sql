CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ONDE_MET_EN_ZONDER_INDI_VW" ("ONDE_ONDE_ID", "ONDE_ONDERZOEKNR", "ONDE_DATUM_BINNEN", "ONDE_AFGEROND", "ONDE_KAFD_ID", "ONDE_ONGR_ID", "ONDE_ONUI_ID", "INDI_INDI_ID", "INDI_CODE", "KAFD_CODE", "ONGR_CODE") AS
  select
  onde.onde_id onde_onde_id
, onde.onderzoeknr onde_onderzoeknr
, onde.datum_binnen onde_datum_binnen
, onde.afgerond onde_afgerond
, onde.kafd_id onde_kafd_id
, onde.ongr_id onde_ongr_id
, onde.onui_id onde_onui_id
, indi.indi_id indi_indi_id
, indi.code indi_code
, kafd.code kafd_code
, ongr.code ongr_code
from kgc_onderzoeken onde
, kgc_onderzoek_indicaties onin
, kgc_indicatie_teksten indi
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
where onde.onde_id = onin.onde_id
and onin.indi_id = indi.indi_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
union
select
    onde.onde_id onde_onde_id
, onde.onderzoeknr onde_onderzoeknr
, onde.datum_binnen onde_datum_binnen
, onde.afgerond onde_afgerond
, onde.kafd_id onde_kafd_id
, onde.ongr_id onde_ongr_id
, onde.onui_id onde_onui_id
, 0 indi_indi_id
, 'geen_indicatie' indi_code
, kafd.code kafd_code
, ongr.code ongr_code
from kgc_onderzoeken onde
, kgc_onderzoek_indicaties onin
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
where onde.onde_id = onin.onde_id(+)
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
and onin.onde_id is null
 ;

/
QUIT
