CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_PROCES_MC9_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "BRIEFTYPE", "MEDEWERKER", "UITSLAG_CODE", "OMSCHRIJVING", "ONDE_TYPE", "STATUS", "VERBORGEN_FLOW_ID") AS
  SELECT                          -- Overzicht 'Te autoriseren onderzoeken'
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BRTY.CODE AS BRIEFTYPE,
            MEDE.CODE AS MEDEWERKER,
            ONUI.CODE AS UITSLAG_CODE,
            ONUI.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_uitslagen uits,
            kgc_brieftypes brty,
            kgc_medewerkers mede,
            kgc_onderzoek_uitslagcodes onui,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND UITS.ONDE_ID = ONDE.ONDE_ID
            AND UITS.BRTY_ID = BRTY.BRTY_ID
            AND UITS.MEDE_ID = MEDE.MEDE_ID
            AND ONDE.ONUI_ID = ONUI.ONUI_ID
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND FLPR.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND FLPR.VIEW_NAME = 'BEH_FLOW_METAB_PROCES_MC9_VW'
            AND ONDE.AFGEROND = 'N'
            AND KAFD.CODE = 'METAB'
            AND UITS.MEDE_ID IS NOT NULL
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONGR.CODE,
            ONDE.SPOED ASC,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
