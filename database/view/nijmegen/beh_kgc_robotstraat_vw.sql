CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_KGC_ROBOTSTRAAT_VW" ("TRVU_ID", "MEET_ID", "TRGE_ID", "TRTY_ID", "WERKLIJST_NUMMER", "X_POSITIE", "Y_POSITIE", "Z_POSITIE", "WELL_NUMMER", "WELL_NUMMER_RESHUFFLED_3730", "FRACTIENUMMER", "STOF_CODE", "STOF_OMSCHRIJVING", "SAMPLENAAM", "YX_POSITIE", "TRTY_CODE", "ONDERZOEKSNUMMER", "ONDERZOEKSWIJZE", "EXTERNE_ID") AS
  SELECT trvu.trvu_id, trvu.meet_id, trvu.trge_id, trge.trty_id,
          trge.werklijst_nummer, trvu.x_positie, trvu.y_positie,
          trvu.z_positie, trvu.x_positie || trvu.y_positie well_nummer,
          reshuffle_3730 (trvu.trvu_id) well_nummer_reshuffle_3730,
          NVL (frac.fractienummer, stan.standaardfractienummer) fractienummer,
          stof.code stof_code, stof.omschrijving stof_omschrijving,
          samp.naam samp_naam, CONCAT (trvu.y_positie, trvu.x_positie),
          trty.code trty_code, onde.onderzoeknr, onde.onderzoekswijze,trge.externe_id
--,      kgc_attr_00.waarde('KGC_STOFTESTEN','BANDSIZE',stof.stof_id) bandsize VERVALLEN
   FROM   kgc_stoftesten stof,
          bas_standaardfracties stan,
          kgc_monsters mons,
          bas_fracties frac,
          kgc_samples samp,
          bas_metingen meti,
          bas_meetwaarden meet,
          kgc_tray_gebruik trge,
          kgc_tray_vulling trvu,
          kgc_tray_types trty,
          kgc_onderzoeken onde
    WHERE trvu.trge_id = trge.trge_id
      AND trvu.meet_id = meet.meet_id
      AND meet.stof_id = stof.stof_id
      AND meet.meet_id = samp.meet_id(+)
      AND meet.meti_id = meti.meti_id
      AND meti.frac_id = frac.frac_id(+)
      AND frac.mons_id = mons.mons_id(+)
      AND meti.stan_id = stan.stan_id(+)
      AND trge.trty_id = trty.trty_id
      AND meti.onde_id = onde.onde_id(+);

/
QUIT
