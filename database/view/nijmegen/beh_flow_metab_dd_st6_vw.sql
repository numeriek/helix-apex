CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_DD_ST6_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "BINNEN", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "STATUS", "NOTITIE", "VERBORGEN_NOTI_ID", "DAGEN_OVER", "VERBORGEN_FLOW_ID") AS
  SELECT CASE                                    -- Openstaande Onderzoeken
               WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) > 0
               THEN
                  '#5CAB5C'                                           -- groen
               ELSE
                  CASE
                     WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) <= 0
                     THEN
                        '#CD5B5B'                                      -- rood
                     ELSE
                        CASE
                           WHEN ONDE.GEPLANDE_EINDDATUM IS NULL THEN '#79A8D6' -- blauw
                        END
                  END
            END
               AS verborgen_kleur,
            CASE
               WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) > 0
               THEN
                  2                                                   -- groen
               ELSE
                  CASE
                     WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) <= 0 THEN 1 -- rood
                     ELSE CASE WHEN ONDE.GEPLANDE_EINDDATUM IS NULL THEN 3 -- blauw
                                                                          END
                  END
            END
               AS verborgen_sortering,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            flfa.mede_id favoriet,
            ONDE.PERS_ID AS verborgen_pers_id,
            ongr.code,
            FAMI.FAMILIENUMMER,
            onde.onderzoeknr,
            onde.spoed,
            ONDE.DATUM_BINNEN,
            onde.geplande_einddatum,
            INTE.CODE AS IndiCode,
            INGR.CODE AS InGrCode,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS Status,
            NOTI.OMSCHRIJVING,
            noti.noti_id verborgen_noti_id,
            ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) AS DAGEN_OVER,
            FLFA.FLOW_ID VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            kgc_notities noti,
            kgc_familie_leden fale,
            KGC_FAMILIES FAMI,
            beh_flow_favorieten flfa
      WHERE     ONDE.PERS_ID = FALE.PERS_ID(+)
            AND FALE.FAMI_ID = FAMI.FAMI_ID(+)
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND (NOTI.ENTITEIT = 'ONDE' OR NOTI.ENTITEIT IS NULL)
            AND onde.kafd_id = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND onde.ongr_id = ongr.ongr_id
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND onde.onde_id = flfa.id(+)
            AND onde.afgerond = 'N'
            --AND NVL (onde.status, 'onb') <> 'G'
            --AND KAFD.CODE = 'METAB'
            AND ONGR.CODE = 'DD'
   ORDER BY verborgen_sortering ASC,
            onde.geplande_einddatum ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
