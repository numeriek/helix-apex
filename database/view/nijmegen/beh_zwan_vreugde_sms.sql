CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ZWAN_VREUGDE_SMS" ("ZWAN_ID", "DATUM_VERZONDEN") AS
  select
entiteit_id zwan_id
, datum_verzonden
from beh_berichten beri
, beh_bericht_types bety
where beri.bety_id = bety.bety_id
and beri.entiteit = 'ZWAN'
and beri.verzonden = 'J'
and bety.code = 'SMS'
 ;

/
QUIT
