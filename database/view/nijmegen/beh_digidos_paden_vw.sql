CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DIGIDOS_PADEN_VW" ("BTYP_ID", "BTYP_CODE", "KAFD_ID", "ENTI_CODE", "TEMP", "DEF", "FOUT") AS
  SELECT distinct btyp.btyp_id, btyp.code btyp_code, mobe.kafd_id, btyp.enti_code,
          SUBSTR (
             kgc_sypa_00.systeem_waarde ('BEH_DIGIDOS_PADEN'),
             0,
             INSTR (kgc_sypa_00.systeem_waarde ('BEH_DIGIDOS_PADEN'), '|')
             - 1)
             temp,
          NVL (mobe.standaard_pad, btyp.standaard_pad) def,
          SUBSTR (
             kgc_sypa_00.systeem_waarde ('BEH_DIGIDOS_PADEN'),
             INSTR (kgc_sypa_00.systeem_waarde ('BEH_DIGIDOS_PADEN'), '|')+1)
             fout
     FROM KGC_BESTAND_types btyp, kgc_model_bestanden mobe
    WHERE     btyp.btyp_id = mobe.btyp_id(+)
          AND btyp.enti_code IS NOT NULL
          AND btyp.vervallen = 'N'
          AND mobe.vervallen (+) = 'N'
          AND NVL (mobe.standaard_pad, btyp.standaard_pad) IS NOT NULL;

/
QUIT
