CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_MONS_AANWEZIG_VW" ("PERS_ID", "MONS_ID") AS
  SELECT mons.pers_id, mons.mons_id
     FROM kgc_monsters mons,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_materialen mate
    WHERE mons.kafd_id = kafd.kafd_id
          AND MONS.ONGR_ID = ONGR.ONGR_ID
          AND MONS.MATE_ID = MATE.MATE_ID
          AND EXISTS
                 (SELECT *
                   FROM bas_fracties frac, bas_fractie_types frty
                   WHERE     frac.mons_id = mons.mons_id
                         AND frac.frty_id = frty.frty_id
                         AND frty.code = 'DNA'
                         AND frac.status IN ('A', 'O')
                         )
          AND kafd.code IN ('GENOOM', 'METAB')
          AND UPPER (ONGR.CODE) NOT IN ('AGORA', 'MOLGEN', 'MULTIFACT')
          AND MATE.CODE in ('EDTA', 'DNA', 'BL');

/
QUIT
