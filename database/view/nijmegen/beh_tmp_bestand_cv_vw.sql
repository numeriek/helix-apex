CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_TMP_BESTAND_CV_VW" ("BESTAND_SPECIFICATIE", "ENTITEIT_PK") AS
  SELECT	bestand_specificatie, entiteit_pk
		 FROM kgc_bestanden
		WHERE entiteit_code = 'UITS'
	GROUP BY bestand_specificatie, entiteit_pk
	  HAVING COUNT (*) > 1;

/
QUIT
