CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_FAMILIE_ONDERZOEKEN_VW" ("FAON_ID", "FAMI_ID", "ONGR_ID", "RELA_ID", "INDI_ID", "FAMILIEONDERZOEKNR") AS
  SELECT faon_id, fami_id, ongr_id, rela_id, indi_id, familieonderzoeknr
      FROM kgc_familie_onderzoeken faon;

/
QUIT
