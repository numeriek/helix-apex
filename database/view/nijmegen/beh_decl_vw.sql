CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DECL_VW" ("DECL_ID", "ENTITEIT", "ID", "DECLAREREN", "KAFD_ID", "ONGR_ID", "PERS_ID", "DEWY_ID", "STATUS", "DATUM_VERWERKING", "DEEN_ID", "DATUM", "VERRICHTINGCODE", "VERWERKING", "PERS_ID_ALT_DECL", "ONDE_ID") AS
  select
	decl.decl_id
,	decl.entiteit
,	decl.id
,	decl.declareren
,	decl.kafd_id
,	decl.ongr_id
,	decl.pers_id
,	decl.dewy_id
,	decl.status
,	decl.datum_verwerking
,	decl.deen_id
,	decl.datum
,	decl.verrichtingcode
,	decl.verwerking
,	decl.pers_id_alt_decl
,	onde.onde_id	
from kgc_declaraties decl
,	 kgc_onderzoeken onde
where decl.entiteit = 'ONDE'
and    decl.id = onde.onde_id
and   decl.declareren = 'J'
union all
select
	decl.decl_id
,	decl.entiteit
,	decl.id
,	decl.declareren
,	decl.kafd_id
,	decl.ongr_id
,	decl.pers_id
,	decl.dewy_id
,	decl.status
,	decl.datum_verwerking
,	decl.deen_id
,	decl.datum
,	decl.verrichtingcode
,	decl.verwerking
,	decl.pers_id_alt_decl
,	onde.onde_id	
from 	kgc_declaraties decl
,		kgc_onderzoeken onde
,     	kgc_onderzoek_betrokkenen onbe
where decl.entiteit = 'ONBE'
and    decl.id = onbe.onbe_id
and    onbe.onde_id = onde.onde_id
and   decl.declareren = 'J'
union all
select
	decl.decl_id
,	decl.entiteit
,	decl.id
,	decl.declareren
,	decl.kafd_id
,	decl.ongr_id
,	decl.pers_id
,	decl.dewy_id
,	decl.status
,	decl.datum_verwerking
,	decl.deen_id
,	decl.datum
,	decl.verrichtingcode
,	decl.verwerking
,	decl.pers_id_alt_decl
,	onde.onde_id	
from 	kgc_onderzoeken onde
,      bas_metingen meti
,      kgc_declaraties decl
where decl.entiteit = 'METI'
and    decl.id = meti.meti_id
and    meti.onde_id = onde.onde_id
and   decl.declareren = 'J';

/
QUIT
