CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC24_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "EXTERNE_ID", "GEFIXEERD", "STATUS", "MACHINE", "VT_WEL_WLST_NIET_AFG", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht "Niet afgeronde SEQ en GS Voortesten op een werklijst "  -- SEMI AUTOMATISCH PROCES
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            TRGE.EXTERNE_ID,
            TRGE.GEFIXEERD,
            '' AS STATUS,
            '' AS MACHINE,
            COUNT (*) AS VT_WEL_WLST_NIET_AFG,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            kgc_tray_gebruik trge,
            --            kgc_onderzoek_monsters onmo,
            bas_fracties frac,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            beh_flow_prioriteit_new_vw flpr,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC24_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            --AND ONDE.ONDE_ID = ONMO.ONDE_ID
            --AND ONMO.MONS_ID = FRAC.MONS_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.STATUS = 'G'
            AND ONDE.AFGEROND = 'N'
            AND TRGE.GEFIXEERD = 'N'
            AND MEET.AFGEROND = 'N'
            AND MEET.MEET_REKEN = 'V'
            AND STGR.CODE NOT LIKE '%!_R' ESCAPE '!'
            AND TRGE.WERKLIJST_NUMMER = WLST.WERKLIJST_NUMMER
            AND UPPER (STGR.OMSCHRIJVING) NOT LIKE '%ROBOT%'
            AND (UPPER (STOF.OMSCHRIJVING) LIKE '%(SEQ)'
                 OR UPPER (STOF.OMSCHRIJVING) LIKE '%(GS)')
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            ONDE.ONDERZOEKSTYPE,
            FRAC.FRACTIENUMMER,
            WLST.WERKLIJST_NUMMER,
            TRGE.EXTERNE_ID,
            TRGE.GEFIXEERD,
            '',
            '',
            FLFA.FLOW_ID
   UNION ALL
     SELECT -- Overzicht PCRPlaten (werklijsten) met stoftesten status = 'MANUAL_CLEANUP' of 'MANUAL_AVAILABLE' -- VAN AUTOMATISCH PROCES naar SEMI-AUTOMATISCH PROCES gegaan
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            POST.DNA_ID AS FRACTIENR,
            POST.PCRPLATEBARCODE AS WERKLIJSTNR,
            '' AS EXTERNE_ID,
            '' AS GEFIXEERD,
            MAST.STATE AS STATUS,
            MACH.CODE AS MACHINE,
            COUNT (*) AS VT_WEL_WLST_NIET_AFG,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            bas_fracties frac,
            bas_metingen meti,
            bas_meetwaarden meet,
            beh_flow_dna_postrobot POST,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoeksgroepen ongr,
            beh_flow_dna_machine_storage mast,
            beh_flow_dna_machines mach,
            beh_flow_prioriteit_new_vw flpr,
            beh_flow_favorieten flfa
      WHERE     ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.FRAC_ID = FRAC.FRAC_ID
            AND METI.METI_ID = MEET.METI_ID
            AND MEET.MEET_ID = POST.MEETID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND POST.POST_ID = MAST.POST_ID
            AND MACH.MACH_ID = MAST.MACH_ID
            AND ONDE.STATUS = 'G'
            AND ONDE.AFGEROND = 'N'
            AND MEET.AFGEROND = 'N'
            AND (UPPER (MAST.STATE) = 'MANUAL_CLEANUP'
                 OR UPPER (MAST.STATE) = 'MANUAL_AVAILABLE')
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC24_VW'
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            ONDE.ONDERZOEKSTYPE,
            POST.DNA_ID,
            POST.PCRPLATEBARCODE,
            '',
            MAST.STATE,
            MACH.CODE,
            FLFA.FLOW_ID
   ORDER BY SPOED ASC, VERBORGEN_SORTERING, EINDDATUM ASC, ONDERZOEKNR ASC;

/
QUIT
