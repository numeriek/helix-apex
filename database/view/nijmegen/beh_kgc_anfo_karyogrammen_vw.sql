CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_KGC_ANFO_KARYOGRAMMEN_VW" ("MEET_ID", "METI_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "MEDE_NAAM", "MEDE_NAAM_CONTROLE", "MEST_OMSCHRIJVING", "CEL", "PREPARAAT", "NONIUS", "KWALITEIT", "AANTAL", "MICROSCOPISCHE_ANALYSE") AS
  SELECT kary.meet_id,
          meet.meti_id,
          stof.code stof_code,
          stof.omschrijving stof_omschrijving,
          mede.naam mede_naam,
          mede2.naam mede_naam_controle,
          mest.omschrijving mest_omschrijving,
          kary.cel,
          kwfr.code preparaat,
          kary.nonius,
          kary.kwaliteit,
          kary.aantal,
          beh_kgc_kary_00.chromosomen (kary.meet_id, kary.cel, kary.kwfr_id )
             microscopische_analyse
     FROM kgc_meetwaardestructuur mwst,
          bas_meetwaarde_statussen mest,
          kgc_medewerkers mede2,
          kgc_medewerkers mede,
          kgc_stoftesten stof,
          kgc_kweekfracties kwfr,
          kgc_karyogrammen kary,
          bas_meetwaarden meet
    WHERE     meet.meet_id = kary.meet_id
          AND kary.kwfr_id = kwfr.kwfr_id
          AND meet.stof_id = stof.stof_id
          AND stof.mwst_id = mwst.mwst_id
          AND mwst.code = 'KGCKARY01'
          AND meet.mede_id = mede.mede_id(+)
          AND meet.mede_id_controle = mede2.mede_id(+)
          AND meet.mest_id = mest.mest_id(+)
   UNION ALL
   SELECT meet.meet_id,
          meet.meti_id,
          stof.code,
          stof.omschrijving,
          mede.naam,
          mede2.naam,
          mest.omschrijving,
          dummy.nr,
          NULL                                                    -- preparaat
              ,
          NULL                                                       -- nonius
              ,
          NULL                                                    -- kwaliteit
              ,
          NULL                                                       -- aantal
              ,
          NULL                                       -- microscopische_analyse
     FROM kgc_meetwaardestructuur mwst,
          bas_meetwaarde_statussen mest,
          kgc_medewerkers mede2,
          kgc_medewerkers mede,
          kgc_stoftesten stof,
          kgc_onderzoeksgroepen ongr,
          kgc_onderzoeken onde,
          bas_metingen meti,
          bas_meetwaarden meet,
          dummy_aantal dummy
    WHERE     meet.meti_id = meti.meti_id
          AND meti.onde_id = onde.onde_id
          AND onde.ongr_id = ongr.ongr_id
          AND ( (ongr.code LIKE '%TUM%' AND dummy.nr <= 20) OR dummy.nr <= 10)
          AND meet.stof_id = stof.stof_id
          AND stof.mwst_id = mwst.mwst_id
          AND meet.mede_id = mede.mede_id(+)
          AND meet.mede_id_controle = mede2.mede_id(+)
          AND meet.mest_id = mest.mest_id(+)
          AND mwst.code = 'KGCKARY01'
          AND NOT EXISTS
                 (SELECT NULL
                    FROM kgc_karyogrammen kary
                   WHERE kary.meet_id = meet.meet_id) -- AND kary.cel = dummy.nr);
 ;

/
QUIT
