CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC48_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "VERBORGEN_PERS_ID", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "DATA_OPGEHAALD", "FACILITEIT", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT                               -- Overzicht NGS - Data is opgehaald
           '#79A8A8' AS VERBORGEN_KLEUR,
            '' AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            PVT.DATA_OPGEHAALD,
            PVT.FACILITEIT,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_notities noti,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            kgc_stoftestgroepen stgr,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            beh_flow_help_fami_vw fami,
            kgc_onderzoekswijzen onwy,
            beh_flow_favorieten flfa,
            (SELECT exal.meet_id, EXAL.FACILITEIT, EXAL.DATA_OPGEHAALD
               FROM BEH_FLOW_NGS_EXA_LIB_MDET_VW exal
              WHERE DATA_OPGEHAALD IS NOT NULL AND DATA_GEBACK_UPT IS NULL) pvt
      WHERE     ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FAMI.PERS_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND meti.stgr_id = stgr.stgr_id
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND MEET.MEET_ID = PVT.MEET_ID
            AND MEET.AFGEROND = 'N'
            AND ONDE.AFGEROND = 'N'
            AND STGR.CODE = 'NGS_ST_1'                       -- mantisnr 11373
   GROUP BY ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            FAMI.FAMILIENUMMER,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            FRAC.FRACTIENUMMER,
            WLST.WERKLIJST_NUMMER,
            FLFA.FLOW_ID,
            PVT.DATA_OPGEHAALD,
            PVT.FACILITEIT,
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID
   ORDER BY ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC,
            FRAC.FRACTIENUMMER ASC;

/
QUIT
