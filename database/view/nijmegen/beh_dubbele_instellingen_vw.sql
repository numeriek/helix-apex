CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DUBBELE_INSTELLINGEN_VW" ("INST_VOLGNR", "INST_ALLE_INST", "INST_POSTCODE", "INST_ADRES") AS
  SELECT ROW_NUMBER () OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
           inst.code) inst_volgnr,
             inst.code
          || CASE
                WHEN (LEAD (inst.code, 1) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 1) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 2) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 2) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 3) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 3) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 4) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 4) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 5) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 5) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 6) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 6) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 7) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 7) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 8) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 8) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 9) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 9) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END
          || CASE
                WHEN (LEAD (inst.code, 10) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                       inst.code)
                     ) IS NULL
                   THEN NULL
                ELSE    ', '
                     || LEAD (inst.code, 10) OVER (PARTITION BY inst.postcode, inst.adres ORDER BY inst.creation_date,
                         inst.code)
             END alle_inst,
          inst.postcode, inst.adres
     FROM kgc_instellingen inst
    WHERE UPPER (inst.land) = 'NEDERLAND' AND UPPER (inst.vervallen) = 'N' and postcode is not null
 ;

/
QUIT
