CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_KAFD_ONGR_VW" ("FLOW_ID", "KAFD_ID", "ONGROEPEN") AS
  SELECT flow_id,
            kafd_id,
            TO_CHAR(WM_CONCAT(ongr_id)) AS ongroepen
       FROM BEH_FLOW_KAFD_ONGR
   GROUP BY flow_id, kafd_id;

/
QUIT
