CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_SER_VW" ("SER_ID", "ACTIVE_STATUS", "VERWIJDERD", "RELA_ID", "RELA_CODE", "GESLACHT", "TITULATUUR", "CLINICIAN_TITLE", "RELA_BRIEFAANHEF", "RELA_VOORLETTERS", "RELA_VOORVOEGSEL", "RELA_ACHTERNAAM", "RELA_AANSPREKEN", "RELA_SPECIALISME", "RELA_ADRES", "RELA_WOONPLAATS", "RELA_POSTCODE", "RELA_LAND", "RELA_TELEFOON", "RELA_AGB_CODE", "INST_ID", "INST_CODE", "INST_NAAM", "INST_ADRES", "INST_POSTCODE", "INST_WOONPLAATS", "INST_LAND", "INST_LAND_ISO_CODE2", "INST_TELEFOON", "INST_ICODE", "INST_AGB_CODE", "EAF_ID") AS
  select
trim(KOLOM01) ser_id,
trim(KOLOM02) active_status,
trim(KOLOM03) verwijderd,
trim(KOLOM04) rela_id,
trim(KOLOM05) rela_code,
trim(KOLOM06) geslacht,
trim(KOLOM07) titulatuur,
trim(KOLOM08) clinician_title,
trim(KOLOM09) rela_briefaanhef,
trim(KOLOM10) rela_voorletters,
trim(KOLOM11) rela_voorvoegsel,
trim(KOLOM12) rela_achternaam,
trim(KOLOM13) rela_aanspreken,
trim(KOLOM14) rela_specialisme,
trim(KOLOM15) rela_adres,
trim(KOLOM16) rela_woonplaats,
trim(KOLOM17) rela_postcode,
trim(KOLOM18) rela_land,
trim(KOLOM19) rela_telefoon,
trim(KOLOM20) rela_agb_code,
trim(KOLOM21) inst_id,
trim(KOLOM22) inst_code,
trim(KOLOM23) inst_naam,
trim(KOLOM24) inst_adres,
trim(KOLOM25) inst_postcode,
trim(KOLOM26) inst_woonplaats,
trim(KOLOM27) inst_land,
trim(KOLOM28) inst_land_iso_code2,
trim(KOLOM29) inst_telefoon,
trim(KOLOM30) inst_icode,
trim(KOLOM31) inst_agb_code,
trim(KOLOM32) eaf_id
from beh_ser_external
where upper(trim(KOLOM01)) != 'SER_ID';

/
QUIT
