CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC21_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "WERKLIJSTNR", "ELUTION_BC", "POSITIE", "FRACTIENR", "STOFTESTCODE", "STATUS", "MACHINE", "VERWERKT_POSTROBOT", "AFGEROND_HELIX", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht PCRPlaten (werklijsten) met stoftesten status = QUANTIFICATION FAILED -- AUTOMATISCH PROCES
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            POST.PCRPLATEBARCODE AS WERKLIJSTNR,
            POST.ELUTION_BARCODE AS ELUTION_BC,
            POST.DNAPOSITION AS POSITIE,
            POST.DNA_ID AS FRACTIENR,
            POST.PRIMER_ID AS STOFTESTCODE,
            MAST.STATE AS STATUS,
            MACH.CODE AS MACHINE,
            POST.HELIX_VERWERKT AS VERWERKT_POSTROBOT,
            MEET.AFGEROND AS AFGEROND_HELIX,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM beh_flow_dna_postrobot POST,
            kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            beh_flow_dna_machine_storage mast,
            beh_flow_dna_machines mach,
            bas_meetwaarden meet,
            bas_meetwaarde_details mdet,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     POST.INVESTIGATION_ID = ONDE.ONDERZOEKNR
            AND POST.MEETID = MEET.MEET_ID
            AND MDET.MEET_ID = MEET.MEET_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND POST.POST_ID = MAST.POST_ID
            AND MACH.MACH_ID = MAST.MACH_ID
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC21_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND MEET.AFGEROND = 'N'
            AND ( (UPPER (MAST.STATE) LIKE 'POST_QUANTIFICATION_FAILED')
                 OR (UPPER (MAST.STATE) LIKE 'POST_QUANTIFICATION_PASSED'))
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
