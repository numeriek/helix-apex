CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_NGS_EXA_PIPE_MDET_VW" ("MEET_ID", "ONDERZOEKNR", "START_DATA_ANALYSE", "STATUS", "EINDE_DATA_ANALYSE") AS
  SELECT "MEET_ID",
            "ONDERZOEKNR",
            "'Start data analyse'",
            DECODE ("'Status'",  'M', 'Mislukt',  'W', 'In de wacht',  ''),
            "'Einde data analyse'"
       FROM (SELECT mdet.meet_id,
                    PROMPT,
                    WAARDE,
                    ONDE.ONDERZOEKNR
               FROM kgc_onderzoeken onde,
                    bas_metingen meti,
                    bas_meetwaarden meet,
                    bas_meetwaarde_details mdet,
                    kgc_onderzoeksgroepen ongr,
                    kgc_meetwaardestructuur mwst
              WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                    AND METI.METI_ID = MEET.METI_ID
                    AND MEET.MEET_ID = MDET.MEET_ID
                    AND ONDE.ONGR_ID = ONGR.ONGR_ID
                    AND MEET.MWST_ID = MWST.MWST_ID
                    AND ONDE.AFGEROND = 'N'
                    AND METI.AFGEROND = 'N'
                    AND MEET.AFGEROND = 'N'
                    AND ONGR.CODE = 'NGS'
                    AND MWST.CODE = 'EXA_PIPE') PIVOT (MAX (WAARDE)
                                                FOR (PROMPT)
                                                IN  ('Start data analyse',
                                                    'Status',
                                                    'Einde data analyse'))
   ORDER BY ONDERZOEKNR, meet_id;

/
QUIT
