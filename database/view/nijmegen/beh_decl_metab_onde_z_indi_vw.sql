CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DECL_METAB_ONDE_Z_INDI_VW" ("NAAR_HELIX", "ONDERZOEKNR", "DATUM_BINNEN", "GROEP", "ONDE_TYPE", "AFGEROND", "DECL_ID", "VERRICHTINGCODE", "ZISNR", "AANSPREKEN", "VERBORGEN_KLEUR", "VERBORGEN_SORTERING") AS
  SELECT ONDE.PERS_ID,
          ONDE.ONDERZOEKNR,
          ONDE.DATUM_BINNEN,
          DECH.ONGR_CODE,
          DECODE (DECH.ONDERZOEKSTYPE,
                  'R', 'Research',
                  'D', 'Diagnostiek',
                  'C', 'Controle',
                  '')
             AS ONDE_TYPE,
          ONDE.AFGEROND,
          DECH.DECL_ID,
          DECH.VERRICHTINGCODE,
          DECH.ZISNR,
          DECH.PERS_AANSPREKEN,
          '#FFFFFF' verborgen_kleur,
          ONDE.ONDE_ID verborgen_sortering
     FROM kgc_onderzoeken onde, BEH_DECL_CHECK_VW dech
    WHERE     ONDE.ONDE_ID = DECH.ONDE_ID
          AND dech.KAFD_CODE = 'METAB'
          AND NOT EXISTS
                 (SELECT 1
                    FROM KGC_ONDERZOEK_INDICATIES onin
                   WHERE ONDE.ONDE_ID = ONIN.ONDE_ID);

/
QUIT
