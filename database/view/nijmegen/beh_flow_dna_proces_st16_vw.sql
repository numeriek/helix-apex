CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST16_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR_MAAND", "ONDE_TYPE", "WIJZE", "G01", "G02", "G03", "G04", "G05", "G06", "G07", "G08", "G09", "G00", "ON") AS
  SELECT '#FFFFFF',
          1,
          '',
          JAAR_MAAND,
          ONDE_TYPE,
          WIJZE,
          G01,
          G02,
          G03,
          G04,
          G05,
          G06,
          G07,
          G08,
          G09,
          G00,
          ONRE
     FROM (  SELECT ST7.JAAR_MAAND,
                    ST7.ONDE_TYPE,
                    ST7.WIJZE,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G01' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G01,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G02' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G02,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G03' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G03,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G04' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G04,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G05' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G05,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G06' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G06,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G07' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G07,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G08' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G08,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G09' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G09,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'G00' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       G00,
                    ROUND (
                       AVG (
                          CASE
                             WHEN ST7.INDIGROEP = 'ON' THEN ST7.GEWOGEN_TAT
                             ELSE NULL
                          END),
                       0)
                       ONRE
               FROM BEH_FLOW_DNA_PROCES_ST7_VW ST7
           GROUP BY ST7.JAAR_MAAND, ST7.ONDE_TYPE, ST7.WIJZE
           ORDER BY ST7.JAAR_MAAND DESC);

/
QUIT
