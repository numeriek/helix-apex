CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ONDERZOEK_PLUS_CV_VW" ("KGC_AFDELING", "MDN", "ONDE_ID", "ONDERZOEKNR", "ONDERZOEKSWIJZE", "FRACTIES", "INDICATIES", "FAMILIENUMMER", "AANVRAGER", "DATUM_AANVRAAG", "DATUM_UITSLAG", "UITSLAG_CODE", "RESULTAAT", "TEKST", "TOELICHTING") AS
  select kafd.code
, pers.zisnr MDN
, onde.onde_id
, onde.onderzoeknr ONDERZOEKNR
, onderzoekswijze onderzoekswijze
, beh_haal_fractienr(onde.onde_id) fracties
, beh_onderzoek_indicaties(onde.onde_id)
, kgc_fami_00.familie_bij_onderzoek(onde.ondE_id , 'N') familienummer
, replace( kgc_rapp_00.volledig_adres_rela(onde.rela_id), chr(10), ',') aanvrager
, onde.datum_binnen datum_aanvraag
, uits.datum_uitslag datum_uitslag
, onui.code uitslagocde
, replace( kgc_rapp_00.resultaat_tekst( onde.ONDE_ID), chr(10), ',') RESULTAAT
, replace( UITs.TEKST , chr(10), ',') TEKST
, replace( UITs.TOELICHTING, chr(10), ',') TOELICHTING
from kgc_onderzoeken onde
, kgc_uitslagen uits
, kgc_onderzoek_uitslagcodes onui
, kgc_personen pers
, kgc_kgc_afdelingen kafd
where onde.onde_id = uits.onde_id
and onde.onui_id = onui.onui_id
and onde.pers_id = pers.pers_id
and onde.kafd_id = kafd.kafd_id
and kafd.code = 'GENOOM'
and onde.onderzoekswijze = 'S'
and (   beh_onderzoek_indicaties(onde.onde_id) like ('%BRCA1%')
       or beh_onderzoek_indicaties(onde.onde_id) like ('%BRCA2%')
       )
 and to_char(onde.datum_binnen, 'yyyy') =  '2010';

/
QUIT
