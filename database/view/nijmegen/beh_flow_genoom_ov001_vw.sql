CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_GENOOM_OV001_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "NAAR_HELIX", "AANMAAK_DATUM_MONS", "MONSTERNUMMER", "COMMENTAAR", "AANVRAAG") AS
  SELECT '#FFFFFF' AS VERBORGEN_KLEUR,
            MONS.MONS_ID AS VERBORGEN_SORTERING,
            MONS.MONSTERNUMMER AS VERBORGEN_TELLING,
            MONS.PERS_ID AS NAAR_HELIX,
            MONS.CREATION_DATE,
            MONS.MONSTERNUMMER,
            MONS.COMMENTAAR,
               '<a href="'
            || BEST.BESTAND_SPECIFICATIE
            || '" target="_blank">Open</a>'
               AS BESTAND
       FROM kgc_monsters mons,
            kgc_monster_indicaties moin,
            kgc_indicatie_teksten indi,
            kgc_bestanden best
      WHERE     MONS.MONS_ID = MOIN.MONS_ID
            AND MOIN.INDI_ID = INDI.INDI_ID
            AND MONS.MONS_ID = BEST.ENTITEIT_PK
            AND BEST.ENTITEIT_CODE = 'MONS'
            AND INDI.CODE LIKE 'MUMC%'
--      UNION ALL
--      SELECT -- als een fake record toegevoegd, anders onstaan er problemen bij het exporteren van data vanuit Flow naar excel
--            '',
--            0,
--            '',
--            0,
--            null,
--            '',
--            '',
--            ''
--       FROM DUAL
   ORDER BY VERBORGEN_SORTERING DESC;

/
QUIT
