CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DECL_ALLES_VW" ("DECL_ID", "ENTITEIT", "ENTITEIT_ID", "DECL_DATUM", "DATUM_VERWERKING", "VERRICHTINGCODE", "SERVICE_PROVIDER", "PERS_ID", "ZISNR", "PERS_AANSPREKEN", "OVERLEDEN", "OVERLIJDENSDATUM", "BEHEER_IN_ZIS", "ONDE_ID", "ONDERZOEKNR", "DATUM_BINNEN", "ONDERZOEKSTYPE", "ONDE_AFGEROND", "KAFD_CODE", "ONGR_CODE", "RELA_ID", "RELA_CODE", "ZNR", "RELA_VOORLETTERS", "RELA_AANSPREKEN", "RELA_ADRES", "AANVRAGER_TYPE", "DECL_TYPE", "SER_ID", "SPEC_CODE", "BEVOEGD_DECLAREREN", "DEWY_CODE", "ICODE", "INST_CODE", "DECL_STATUS", "UITV_AFDELING") AS
  SELECT
       decl.decl_id
,      decl.entiteit
,      decl.id entiteit_id
,	   decl.datum decl_datum
,     decl.datum_verwerking
,	   decl.verrichtingcode
,	   kgc_attr_00.waarde('KGC_DECL_ENTITEITEN', 'DFT_SERVICE_PROVIDER', decl.deen_id) service_provider
,	   pers.pers_id
,	   pers.zisnr
,	   pers.aanspreken pers_aanspreken
,	   pers.overleden
,	   pers.overlijdensdatum
,	   pers.beheer_in_zis
,	   onde.onde_id
,	   onde.onderzoeknr
,      onde.datum_binnen
,      onde.onderzoekstype
,	   onde.afgerond onde_afgerond
,	   kafd.code kafd_code
,	   ongr.code ongr_code
, 	   rela.rela_id
,	   rela.code rela_code
,	   rela.liszcode znr
,	   rela.voorletters rela_voorletters
,	   rela.aanspreken rela_aanspreken
,	   rela.adres rela_adres
,		case
			when regexp_like(nvl(upper(rela.briefaanhef), 'GEEN'), '(CENTRUM|KCHL|LAB|METAB|ONBEKEND|PARELTJES|PRAKTIJK|PRENATAAL|VERLOSK|VLK |ZIEKENHUIS)') or
				 regexp_like(nvl(upper(rela.aanspreken), 'GEEN'), '(CENTRUM|KCHL|LAB|METAB|ONBEKEND|PARELTJES|PRAKTIJK|PRENATAAL|VERLOSK|VLK |ZIEKENHUIS|CLINICAL)') or
				 regexp_like(nvl(upper(rela.achternaam), 'GEEN'), '(CENTRUM|KCHL|LAB|METAB|ONBEKEND|PARELTJES|PRAKTIJK|PRENATAAL|VERLOSK|VLK |ZIEKENHUIS|MEDLON|LOCATIE |Z.G.T.|LIBERIS |OBSTETRIE|C.W.Z.|GYNAECOLOGEN)')
				 then 'PRAKTIJK'
			else 'PERSOON'
		end aanvrager_type
,		case
			when nvl(upper(rela.adres), 'GEEN_ADRES') like '%ALHIER%' then 'INTERN'
			else 'EXTERN'
		end decl_type
,   kgc_attr_00.waarde('KGC_RELATIES', 'SER_ID', rela.rela_id) ser_id
,		case nvl(dewy.code, 'BI')
			when 'BU' then 'BU_SPEC'
			else spec.code
		end spec_code	
,		nvl(kgc_attr_00.waarde('KGC_SPECIALISMEN', 'BEVOEGD_DECLAREREN', spec.spec_id), 'J') bevoegd_declareren		
,	   nvl(dewy.code, 'BI') dewy_code
,	   nvl(kgc_attr_00.waarde('KGC_INSTELLINGEN', 'FEZ_ICODE', (select inst_id from kgc_relaties where rela_id = onde.rela_id)),-- niet naar rela_id_oorsprong kijken voor icodes
		   kgc_attr_00.waarde('KGC_ONDERZOEKEN', 'FEZ_ALT_ICODE', onde.onde_id) ) icode
,   inst.code inst_code
,		decl.status decl_status
,     nvl(
      upper(kgc_attr_00.waarde('KGC_ONDERZOEKSGROEPEN', 'DFT_UITVOERENDE_AFDELING', ongr.ongr_id)),
      upper(kgc_attr_00.waarde('KGC_INSTELLINGEN', 'DFT_UITVOERENDE_AFDELING', onde.inst_id))
      ) uitv_afdeling
from beh_decl_vw decl
,		kgc_onderzoeken onde
,		kgc_kgc_afdelingen kafd
,	    kgc_onderzoeksgroepen ongr
,		kgc_declaratiewijzen dewy
,		kgc_relaties rela
,		kgc_specialismen spec
,		kgc_instellingen inst
,		kgc_personen pers
where decl.onde_id = onde.onde_id
and   decl.kafd_id = kafd.kafd_id
and    decl.ongr_id = ongr.ongr_id
and    decl.dewy_id = dewy.dewy_id(+)
and    nvl(onde.rela_id_oorsprong, onde.rela_id) = rela.rela_id
and    nvl(decl.pers_id_alt_decl, decl.pers_id) = pers.pers_id
and    rela.inst_id = inst.inst_id(+)
and    rela.spec_id = spec.spec_id(+)
--and nvl(decl.verrichtingcode, 'GEEN') not in ('GEEN', 'HOMWDS', 'PRJ', 'IBD_BU', 'HANDINVOER', 'NIPT','PBMESIE', 'G36', 'POGDNA')
and nvl(decl.verrichtingcode, 'GEEN') in
(
select cawa.waarde
from kgc_categorie_waarden cawa, kgc_categorieen cate, kgc_categorie_types caty
where cate.cate_id = cawa.cate_id
and cate.caty_id = caty.caty_id
and  caty.code = 'VIC'
)
and   (ongr.code != 'HOM' or (ongr.code = 'HOM' and nvl(dewy.code, 'BI') = 'WDS') );

/
QUIT
