CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC28_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT          -- Overzicht Stoftesten status AVAILABLE IN LIMS
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            bas_fracties frac,
            BEH_FLOW_DNA_NAI_SAMPLES nasa,
            BEH_FLOW_DNA_NAI_SUBMISSIONS NASU,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND NASA.MEASURE_ID = MEET.MEET_ID
            AND NASA.SUBMISSION_ID = NASU.SUBMISSION_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC28_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND NASU.STUDY_DATAGROUP LIKE '%_VW'
            AND UPPER (NASA.SAMPLE_STATE) = 'LOGGED'
            AND UPPER (NASA.SAMPLE_CONDITION) = 'ONLINE'
            AND NASA.HAS_CHILD = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
