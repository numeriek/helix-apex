CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_GUI_VW" ("HIDDEN_PERS_ID", "HIDDEN_ONDE_ID", "ZISNR", "MONSTERNUMMER", "ONDE_INDICATIES", "ALLE_FAMILIENRS", "GEBOORTEDATUM", "GESLACHT", "ONDE_FRACTIENUMMERS", "FRACTIENUMMER", "METI_ID", "PROTOCOL", "MEET_ID", "MEET_COMMENTAAR", "MEET_AFGEROND", "MEET_DATUM_METING", "MEET_STATUS", "STOF_CODE", "STOF_OMSCH", "STOF_MEET_REKEN", "MDET_ID", "MDET_PROMPT", "MDET_WAARDE", "FAMI_FAMILIENUMMER", "FAON_FAMILIEONDERZOEKNR", "FAON_INDICATIE", "REF_NR_SENGENICS", "FRACTIES_FAMILIE_LEDEN") AS
  SELECT pers.pers_id hidden_pers_id,
		  onde.onde_id hidden_onde_id,
          pers.zisnr,
          mons.monsternummer,
          beh_soap_001.haal_alle_indi_op (onde.onde_id) onde_indicaties,
          beh_stbm_00.haal_alle_fami_op (pers.pers_id) alle_familienrs,
          pers.geboortedatum,
          pers.geslacht,
          beh_soap_001.haal_alle_frac_op (onde.onde_id, frac.fractienummer)
          onde_fractienummers,
          frac.fractienummer,
          meti.meti_id,
          stgr.code protocol,
          meet.meet_id,
          meet.commentaar meet_commentaar,
          meet.afgerond meet_afgerond,
          TO_CHAR (meet.datum_meting, 'dd-mm-yyyy') meet_datum_meting,
          mest.code meet_status,
          stof.code stof_code,
          stof.omschrijving stof_omsch,
          stof.meet_reken stof_meet_reken,
          mdet.mdet_id,
          mdet.prompt mdet_prompt,
          mdet.waarde mdet_waarde,
          fami.familienummer fami_familienummer,
          fami.familieonderzoeknr faon_familieonderzoeknr,
          fami.indicatie faon_indicatie,
          kgc_attr_00.
           waarde ('BAS_FRACTIES', 'REF_NR_SENGENICS', frac.frac_id)
             ref_nr_sengenics,
          (select distinct fami_rol ||':'|| fracties
          from beh_gui_trio_frac_vw
          where familienummer = trio.familienummer
          and   familieonderzoeknr = trio.familieonderzoeknr
          and fami_rol = 'KIND'
          ) || ';' ||
          (select distinct fami_rol ||':'|| fracties
          from beh_gui_trio_frac_vw
          where familienummer = trio.familienummer
          and   familieonderzoeknr = trio.familieonderzoeknr
          and fami_rol = 'VADER'
          ) || ';' ||
          (select distinct fami_rol ||':'|| fracties
          from beh_gui_trio_frac_vw
          where familienummer = trio.familienummer
          and   familieonderzoeknr = trio.familieonderzoeknr
          and fami_rol = 'MOEDER'
          ) fracties_familie_leden
     FROM kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
          bas_metingen meti,
          bas_meetwaarden meet,
          bas_meetwaarde_details mdet,
          bas_meetwaarde_statussen mest,
          kgc_stoftesten stof,
          kgc_stoftestgroepen stgr,
          kgc_relaties rela,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          beh_gui_trio_frac_vw trio,
          (SELECT DISTINCT frac.frac_id,
                           fami.familienummer,
                           faon.familieonderzoeknr,
                           indi.code indicatie
             FROM kgc_families fami,
                  kgc_familie_onderzoeken faon,
                  kgc_faon_betrokkenen fobe,
                  kgc_onderzoeken onde,
                  kgc_onderzoek_monsters onmo,
                  kgc_monsters mons,
                  bas_fracties frac,
                  bas_metingen meti,
                  kgc_indicatie_teksten indi
            WHERE     fami.fami_id = faon.fami_id
                  AND faon.faon_id = fobe.faon_id
                  AND fobe.onde_id = onde.onde_id
                  AND onde.onde_id = onmo.onde_id
                  AND onmo.mons_id = mons.mons_id
                  AND mons.mons_id = frac.mons_id
                  AND frac.frac_id = meti.frac_id
                  AND onmo.onmo_id = meti.onmo_id
                  AND faon.indi_id = indi.indi_id) fami
    WHERE     pers.pers_id = onde.pers_id
          AND onde.onde_id = onmo.onde_id
          AND onmo.mons_id = mons.mons_id
          AND mons.mons_id = frac.mons_id
          AND frac.frac_id = meti.frac_id
          AND meti.meti_id = meet.meti_id
          AND meet.meet_id = mdet.meet_id
          AND meet.mest_id = mest.mest_id(+)
          AND meti.stgr_id = stgr.stgr_id(+)
          AND meet.stof_id = stof.stof_id
          AND onde.onde_id = meti.onde_id(+)
          AND onde.rela_id = rela.rela_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND frac.frac_id = fami.frac_id
          and onde.onde_id = trio.onde_id(+)
          and trio.aantal_onderzoeken(+) = 3
          AND UPPER (kafd.code) = 'GENOOM'
          AND UPPER (ongr.code) = 'NGS'
          AND ONDE.AFGEROND = 'N';

/
QUIT
