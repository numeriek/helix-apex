CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_STBM_PERS_VW" ("PERS_PERS_ID", "PERS_VOORLETTERS", "PERS_VOORVOEGSEL", "PERS_ACHTERNAAM", "PERS_ACHTERNAAM_PARTNER", "PERS_VOORVOEGSEL_PARTNER", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_OVERLIJDENSDATUM", "PERS_OVERLEDEN", "PERS_POSTCODE", "PERS_ADRES", "ONDERZOEKNRS", "FAMILIENUMMERS", "PERS_ZISNR", "PERS_BSN", "FRACTIENUMMERS", "P_NUMMERS") AS
  SELECT pers.pers_id pers_pers_id,
          pers.voorletters pers_voorletters,
          pers.voorvoegsel pers_voorvoegsel,
          pers.achternaam pers_achternaam,
          pers.achternaam_partner pers_achternaam_partner,
          pers.voorvoegsel_partner pers_voorvoegsel_partner,
          pers.geboortedatum pers_geboortedatum,
          pers.geslacht pers_geslacht,
          case when pers.overlijdensdatum<to_date('01-01-1753', 'DD-MM-YYYY') THEN NULL ELSE pers.overlijdensdatum END pers_overlijdensdatum,
          pers.overleden pers_overleden,
          pers.postcode pers_postcode,
          pers.adres pers_adres,
          beh_stbm_00.haal_alle_onde_op (pers.pers_id) onderzoeknrs,
          beh_stbm_00.haal_alle_fami_op (pers.pers_id) familienummers,
          pers.zisnr pers_zisnr,
          pers.bsn pers_bsn,
          beh_stbm_00.haal_alle_frac_op (pers.pers_id) fractienummers,
          beh_stbm_00.haal_alle_klingen_onde_op (pers.pers_id) p_nummers
   FROM kgc_personen pers;

/
QUIT
