CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_METAB_DD_MC6_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "STATUS", "FRACTIENR", "VERBORGEN_FLOW_ID") AS
  SELECT                               -- Overzicht "Te aanmaken uitslagen"
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_monsters onmo,
            BAS_FRACTIES FRAC,
            BEH_FLOW_PRIORITEIT_NEW_VW flpr,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_METAB_DD_MC6_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND ONDE.ONDE_ID = ONMO.ONDE_ID
            AND ONMO.MONS_ID = FRAC.MONS_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND ONDE.ONDE_ID NOT IN (SELECT UITS.ONDE_ID
                                       FROM KGC_UITSLAGEN UITS)
            AND ONDE.ONDE_ID NOT IN
                   (SELECT ONDE1.ONDE_ID
                      FROM BAS_MEETWAARDEN MEET,
                           BAS_METINGEN METI,
                           KGC_ONDERZOEKEN ONDE1
                     WHERE     ONDE1.ONDE_ID = METI.ONDE_ID
                           AND METI.METI_ID = MEET.METI_ID
                           AND MEET.AFGEROND = 'N'
                           AND (MEET.MEET_REKEN = 'M' OR MEET.MEET_REKEN = 'S'))
            AND ONDE.ONDE_ID IN
                   (SELECT ONDE1.ONDE_ID
                      FROM BAS_MEETWAARDEN MEET,
                           BAS_METINGEN METI,
                           KGC_ONDERZOEKEN ONDE1
                     WHERE ONDE1.ONDE_ID = METI.ONDE_ID
                           AND METI.METI_ID = MEET.METI_ID)
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
