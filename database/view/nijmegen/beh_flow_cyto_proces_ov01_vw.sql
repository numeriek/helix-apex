CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_CYTO_PROCES_OV01_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "SPOED", "DAT_BINNEN", "EINDDATUM", "INDICODE", "WIJZE", "ONDE_TYPE", "STATUS", "NOTITIE", "NOTI_GEREED", "DAGEN_OVER", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  WITH W_KGC_NOTITIES
        AS (SELECT NOTI.NOTI_ID,
                   NOTI.ENTITEIT,
                   NOTI.ID,
                   NOTI.CODE,
                   NOTI.OMSCHRIJVING,
                   NOTI.COMMENTAAR,
                   NOTI.GEREED,
                   NOTI.CREATION_DATE,
                   NOTI.CREATED_BY,
                   NOTI.LAST_UPDATE_DATE,
                   NOTI.LAST_UPDATED_BY
              FROM kgc_notities noti
             WHERE NOTI.GEREED (+) = 'N'
                    AND NOTI.ENTITEIT = 'ONDE')
     SELECT CASE                                    -- Openstaande Onderzoeken
               WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) > 0
               THEN
                  '#5CAB5C'                                           -- groen
               ELSE
                  CASE
                     WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) <= 0
                     THEN
                        '#CD5B5B'                                      -- rood
                     ELSE
                        CASE
                           WHEN ONDE.GEPLANDE_EINDDATUM IS NULL THEN '#79A8D6' -- blauw
                        END
                  END
            END
               AS VERBORGEN_KLEUR,
            CASE
               WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) > 0
               THEN
                  2                                                   -- groen
               ELSE
                  CASE
                     WHEN ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) <= 0 THEN 1 -- rood
                     ELSE CASE WHEN ONDE.GEPLANDE_EINDDATUM IS NULL THEN 3 -- blauw
                                                                          END
                  END
            END
               AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS VERBORGEN_PERS_ID,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.DATUM_BINNEN AS DAT_BINNEN,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            BEH_HAAL_INDI_CODE (onde.onde_id) AS INDICODE,
            BEH_HAAL_ONWY_CODE (onde.onde_id) AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            DECODE (ONDE.STATUS,
                    'V', 'Vervallen',
                    'G', 'Goedgekeurd',
                    'B', 'Beoordelen',
                    '')
               AS STATUS,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.GEREED AS NOTI_GEREED,
            ROUND (ONDE.GEPLANDE_EINDDATUM - SYSDATE) AS DAGEN_OVER,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            W_KGC_NOTITIES noti,
            BEH_FLOW_FAVORIETEN FLFA
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 8) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY ONDE.SPOED ASC,
            VERBORGEN_SORTERING ASC,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
