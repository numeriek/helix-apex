CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_ST23_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "GROEP", "ONDERZOEKNR", "INGRCODE", "ONDE_TYPE", "WIJZE", "UITSLAG", "CREATION_DATE", "BINNEN_OP", "GOEDGEKEURD_OP", "WERKLIJST_OP", "UITSLAG_DAT", "ONDERTEKEN_DAT", "AUTORISATIE_DAT", "TOT_GOEDKEURING", "TOT_WERKLIJST", "TOT_UITSLAG", "TOT_ONDERTEKENING", "TOT_AUTORISATIE", "TOTAAL") AS
  SELECT '#FFFFFF',
            1,
            '',
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            INGR.CODE AS INGRCODE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            ONWY.OMSCHRIJVING AS WIJZE,
            ONUI.CODE || '-'|| ONUI.OMSCHRIJVING AS UITSLAG,
            TO_CHAR (ONDE.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS') AS CREATION_DATE,
            TO_CHAR (DATBIN.BINNEN_OP, 'dd-mm-yyyy HH24:MI:SS') AS BINNEN_OP,
            TO_CHAR (AULO.DATUM_TIJD, 'dd-mm-yyyy HH24:MI:SS')
               AS GOEDGEKEURD_OP,
            TO_CHAR (WLSTDAT.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS')
               AS WERKLIJST_OP,
            TO_CHAR (UITSDAT.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS')
               AS UITSLAG_DAT,
            TO_CHAR (UITSDAT.DATUM_MEDE_ID, 'dd-mm-yyyy HH24:MI:SS')
               AS ONDERTEKEN_DAT,
            TO_CHAR (OAHIDAT.CREATION_DATE, 'dd-mm-yyyy HH24:MI:SS')
               AS AUTORISATIE_DAT,
            --            OAHIDAT.CREATION_DATE as AFROND_DAT,
            SUBSTR (
               SUBSTR (
                  NUMTODSINTERVAL ( (AULO.DATUM_TIJD - DATBIN.BINNEN_OP),
                                   'day'),
                  6),
               1,
               14)
               AS "Tot Goedkeuring DDD HH:MM:SS",
            SUBSTR (
               SUBSTR (
                  NUMTODSINTERVAL ( (WLSTDAT.CREATION_DATE - AULO.DATUM_TIJD),
                                   'day'),
                  6),
               1,
               14)
               AS "Tot Werklijst DDD HH:MM:SS",
            SUBSTR (
               SUBSTR (
                  NUMTODSINTERVAL (
                     (UITSDAT.CREATION_DATE - WLSTDAT.CREATION_DATE),
                     'day'),
                  6),
               1,
               14)
               AS "Tot Utislag DDD HH:MM:SS",
            SUBSTR (
               SUBSTR (
                  NUMTODSINTERVAL (
                     (UITSDAT.DATUM_MEDE_ID - UITSDAT.CREATION_DATE),
                     'day'),
                  6),
               1,
               14)
               AS "Tot Ondertekening DDD HH:MM:SS",
            SUBSTR (
               SUBSTR (
                  NUMTODSINTERVAL (
                     (OAHIDAT.CREATION_DATE - UITSDAT.DATUM_MEDE_ID),
                     'day'),
                  6),
               1,
               14)
               AS "Tot Autorisatie DDD HH:MM:SS",
            SUBSTR (
               SUBSTR (
                  NUMTODSINTERVAL ( (OAHIDAT.CREATION_DATE - DATBIN.BINNEN_OP),
                                   'day'),
                  6),
               1,
               14)
               AS "Totale DDD HH:MM:SS"
       --            MAX (SUBSTR (SUBSTR (NUMTODSINTERVAL ( (AULO.DATUM_TIJD - DATBIN.BINNEN_OP),'day'),6),1,14)) AS "Tot Goedkeuring DDD HH:MM:SS",
       --            MAX (SUBSTR (SUBSTR (NUMTODSINTERVAL ( (AULO.DATUM_TIJD - DATBIN.BINNEN_OP),'day'),6),1,14)) AS "Tot Goedkeuring DDD HH:MM:SS",
       --            MIN (SUBSTR (SUBSTR (NUMTODSINTERVAL ( (WLSTDAT.CREATION_DATE - AULO.DATUM_TIJD),'day'),6),1,14)) AS "Tot Werklijst DDD HH:MM:SS",
       --            MAX (SUBSTR (SUBSTR (NUMTODSINTERVAL ((UITSDAT.CREATION_DATE - WLSTDAT.CREATION_DATE),'day'),6),1,14)) AS "Tot Utislag DDD HH:MM:SS",
       --            MIN (SUBSTR (SUBSTR (NUMTODSINTERVAL ((UITSDAT.DATUM_MEDE_ID - UITSDAT.CREATION_DATE),'day'),6),1,14)) AS "Tot Ondertekening DDD HH:MM:SS",
       --            MIN (SUBSTR (SUBSTR (NUMTODSINTERVAL ((OAHIDAT.CREATION_DATE - UITSDAT.DATUM_MEDE_ID),'day'),6),1,14))AS "Tot Autorisatie DDD HH:MM:SS",
       --            MIN (SUBSTR (SUBSTR (NUMTODSINTERVAL ((OAHIDAT.CREATION_DATE - DATBIN.BINNEN_OP),'day'),6),1,14)) AS  "Totale DDD HH:MM:SS"
       FROM (SELECT ONDE.ONDE_ID,
                    CASE
                       WHEN TRUNC(ONDE.CREATION_DATE) <= TRUNC(ONDE.DATUM_BINNEN)
                       THEN
                          ONDE.CREATION_DATE
                       ELSE
                          ONDE.DATUM_BINNEN
                    END
                       AS BINNEN_OP
               FROM kgc_onderzoeken onde) DATBIN,
            (  SELECT AULO.ID AS ONDE_ID, MIN (AULO.DATUM_TIJD) AS DATUM_TIJD
                 FROM kgc_audit_log aulo
                WHERE AULO.KOLOM_NAAM = 'STATUS' AND AULO.WAARDE_NIEUW = 'G'
             GROUP BY aulo.id) AULO,
            (  SELECT MIN (WLST.CREATION_DATE) AS CREATION_DATE, METI.ONDE_ID
                 FROM bas_metingen meti,
                      kgc_werklijst_items wlit,
                      kgc_werklijsten wlst
                WHERE METI.METI_ID = WLIT.METI_ID AND WLIT.WLST_ID = WLST.WLST_ID
                AND METI.CREATION_DATE <= WLST.CREATION_DATE
             GROUP BY METI.ONDE_ID) WLSTDAT,
            (  SELECT MIN (UITS.CREATION_DATE) AS CREATION_DATE,
                      UITS.ONDE_ID,
                      MIN (UITS.DATUM_MEDE_ID) AS DATUM_MEDE_ID
                 FROM kgc_uitslagen uits
             GROUP BY UITS.ONDE_ID) UITSDAT,
            (  SELECT MIN (OAHI.CREATION_DATE) AS CREATION_DATE, OAHI.ONDE_ID
                 FROM kgc_onde_autorisatie_historie oahi
             GROUP BY OAHI.ONDE_ID) OAHIDAT,
            kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoek_uitslagcodes onui
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 1)
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONDE_ID = DATBIN.ONDE_ID
            AND ONDE.ONDE_ID = AULO.ONDE_ID(+)
            AND ONDE.ONDE_ID = WLSTDAT.ONDE_ID(+)
            AND ONDE.ONDE_ID = UITSDAT.ONDE_ID(+)
            AND ONDE.ONDE_ID = OAHIDAT.ONDE_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.ONUI_ID = ONUI.ONUI_ID (+)
            AND ONDE.AFGEROND = 'J'
            AND TO_CHAR (ONDE.CREATION_DATE, 'rrrr') >= '2011'

            --and ONDE.ONDERZOEKNR = 'R14-15208'
   --   GROUP BY ONDE.CREATION_DATE,
   --            ONDE.PERS_ID,
   --            ONDE.ONDERZOEKNR,
   --            ONDE.ONDERZOEKSTYPE,
   --            ONWY.OMSCHRIJVING,
   --            ONGR.CODE,
   --            INGR.CODE,
   --            AULO.DATUM_TIJD,
   --            ONDE.ONDE_ID,
   --            DATBIN.BINNEN_OP,
   --            WLSTDAT.CREATION_DATE,
   --            UITSDAT.CREATION_DATE,
   --            UITSDAT.DATUM_MEDE_ID,
   --            OAHIDAT.CREATION_DATE
   ORDER BY ONDE.ONDE_ID DESC


-- OUDE SCRIPT
--     SELECT '#FFFFFF',
--            1,
--            '',
--            --ONDE.PERS_ID,
--            ONGR.CODE,
--            ONDE.ONDERZOEKNR,
--            INGR.CODE AS INGRCODE,
--            DECODE (ONDE.ONDERZOEKSTYPE,
--                    'R', 'Research',
--                    'D', 'Diagnostiek',
--                    '')
--               AS ONDE_TYPE,
--            ONWY.OMSCHRIJVING AS WIJZE,
--            ONDE.CREATION_DATE,
--            MAX (
--               SUBSTR (
--                  SUBSTR (
--                     NUMTODSINTERVAL ( (AULO.DATUM_TIJD - ONDE.CREATION_DATE),
--                                      'day'),
--                     6),
--                  1,
--                  14))
--               AS "Tot Goedkeuring DDD HH:MM:SS",
--            MIN (
--               SUBSTR (
--                  SUBSTR (
--                     NUMTODSINTERVAL ( (WLST.CREATION_DATE - AULO.DATUM_TIJD),
--                                      'day'),
--                     6),
--                  1,
--                  14))
--               AS "Tot Werklijst DDD HH:MM:SS",
--            MIN (
--               SUBSTR (
--                  SUBSTR (
--                     NUMTODSINTERVAL (
--                        (UITS.CREATION_DATE - WLST.CREATION_DATE),
--                        'day'),
--                     6),
--                  1,
--                  14))
--               AS "Tot Utislag DDD HH:MM:SS",
--            MIN (
--               SUBSTR (
--                  SUBSTR (
--                     NUMTODSINTERVAL (
--                        (UITS.DATUM_MEDE_ID - UITS.CREATION_DATE),
--                        'day'),
--                     6),
--                  1,
--                  14))
--               AS "Tot Ondertekening DDD HH:MM:SS",
--            MIN (
--               SUBSTR (
--                  SUBSTR (
--                     NUMTODSINTERVAL (
--                        (OAHI.CREATION_DATE - UITS.DATUM_MEDE_ID),
--                        'day'),
--                     6),
--                  1,
--                  14))
--               AS "Tot Autorisatie DDD HH:MM:SS",
--            MIN (
--               SUBSTR (
--                  SUBSTR (
--                     NUMTODSINTERVAL (
--                        (OAHI.CREATION_DATE - ONDE.CREATION_DATE),
--                        'day'),
--                     6),
--                  1,
--                  14))
--               AS "Totale DDD HH:MM:SS"
--       FROM kgc_onderzoeken onde,
--            bas_metingen meti,
--            kgc_werklijst_items wlit,
--            kgc_werklijsten wlst,
--            kgc_audit_log aulo,
--            kgc_uitslagen uits,
--            kgc_onde_autorisatie_historie oahi,
--            kgc_onderzoeksgroepen ongr,
--            kgc_onderzoekswijzen onwy,
--            kgc_onderzoek_indicaties onin,
--            kgc_indicatie_teksten inte,
--            kgc_indicatie_groepen ingr
--      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
--                                   FROM BEH_FLOW_KAFD_ONGR fkon
--                                  WHERE FKON.FLOW_ID = 1)
--            AND ONDE.ONGR_ID = ONGR.ONGR_ID
--            AND ONDE.ONDE_ID = METI.ONDE_ID
--            AND METI.METI_ID = WLIT.METI_ID
--            AND WLIT.WLST_ID = WLST.WLST_ID
--            AND ONDE.ONDE_ID = AULO.ID
--            AND ONDE.ONDE_ID = UITS.ONDE_ID(+)
--            AND ONDE.ONDE_ID = OAHI.ONDE_ID
--            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
--            AND ONIN.INDI_ID = INTE.INDI_ID(+)
--            AND INTE.INGR_ID = INGR.INGR_ID(+)
--            AND AULO.KOLOM_NAAM = 'STATUS'
--            AND AULO.WAARDE_NIEUW = 'G'
--            AND ONDE.AFGEROND = 'J'
--            AND ONDE.KAFD_ID = ONWY.KAFD_ID
--            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
--            AND TO_CHAR (ONDE.CREATION_DATE, 'rrrr') >= '2011'
--   --and ONDE.ONDERZOEKNR = 'D12-05546'
--   GROUP BY ONDE.CREATION_DATE,
--            ONDE.PERS_ID,
--            ONDE.ONDERZOEKNR,
--            ONDE.ONDERZOEKSTYPE,
--            ONWY.OMSCHRIJVING,
--            ONGR.CODE,
--            INGR.CODE,
--            ONDE.ONDE_ID
--   ORDER BY ONDE.ONDE_ID DESC;;

/
QUIT
