CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_WPD_ONDE_GROEP_PLUS_VW" ("ONDERZOEKNR", "INDICATIES", "INDI_CODE", "WPD_GROEP") AS
  select  distinct onde.onderzoeknr
, beh_onderzoek_indicaties(onde.onde_id) indicaties
, indi.code indi_code
, nvl(kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN', 'WPD_CAT_JV',onin.indi_id),'Geen') WPD_GROEP
from kgc_onderzoeken onde
, KGC_ONDERZOEK_INDICATIES onin
, kgc_indicatie_teksten indi
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
where onde.onde_id = onin.onde_id
and onin.indi_id = indi.indi_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
and kafd.code = 'GENOOM'
and ongr.code = 'PRE_NIJM'
and onde.datum_binnen between to_date('01-01-2011','dd-mm-yyyy') and to_date('01-01-2012','dd-mm-yyyy')
and  not exists (select *
                from kgc_onderzoeken onde2
                , kgc_kgc_afdelingen kafd2
                , kgc_onderzoeksgroepen ongr2
                where onde.pers_id = onde2.pers_id
                and onde.foet_id = onde2.foet_id
                and onde2.kafd_id = kafd2.kafd_id
                and ongr2.ongr_id = ongr2.ongr_id
                and ongr2.code = 'PRE_NIJM'
                and kafd2.code = 'GENOOM'
                and onde.datum_binnen < onde2.datum_binnen)
and exists ( select *
             from KGC_ONDERZOEK_INDICATIES onin2
             where onin.onde_id = onin2.onde_id
             and onin.indi_id <> onin2.indi_id
             and nvl(kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN', 'WPD_CAT_JV',onin.indi_id),'Geen')  <> nvl(kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN', 'WPD_CAT_JV',onin2.indi_id),'Geen')
            )
and onde.onderzoeknr in ('PRN11-2662', 'PRN11-2561', 'PRN11-2757')
and nvl(kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN', 'WPD_CAT_JV',onin.indi_id), 'Geen') <> 'LEEFTIJD';

/
QUIT
