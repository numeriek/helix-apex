CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_LABV_TRGE_MANUEEL_VW" ("TRGE_ID", "WERKLIJST_NUMMER", "TRTY_CODE", "TRTY_OMSCH", "ZNR", "TECH_CODE", "TECH_OMSCH") AS
  SELECT trge.trge_id,
          trge.werklijst_nummer,
          trty.code trty_code,
          trty.omschrijving trty_omsch,
          mede.vms_username znr,
          tech.code tech_code,
          tech.omschrijving tech_omsch
     FROM kgc_tray_gebruik trge,
          kgc_tray_types trty,
          kgc_medewerkers mede,
          kgc_technieken tech
    WHERE     trge.trty_id = trty.trty_id
          AND trge.mede_id = mede.mede_id
          AND trge.tech_id = tech.tech_id(+)
          AND UPPER (
                 kgc_attr_00.
                  waarde ('KGC_TRAY_GEBRUIK', 'NAAR_LABVANTAGE', trge_id)) =
                 'J'
          AND kgc_attr_00.
               waarde ('KGC_TRAY_GEBRUIK', 'LABV_XML_FILE', trge_id)
                 IS NULL;

/
QUIT
