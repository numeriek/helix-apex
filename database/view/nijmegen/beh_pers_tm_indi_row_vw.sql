CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_PERS_TM_INDI_ROW_VW" ("PERS_ID", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GESLACHT", "PERS_GEBOORTEDATUM", "ONDE_ONDE_ID", "ONDE_ONDERZOEKNR", "ONDE_DATUM_BINNEN", "ONDE_AFGEROND", "ONDE_KAFD_ID", "ONDE_ONGR_ID", "ONDE_ONUI_ID", "INDI_INDI_ID", "INDI_VOLGNR", "INDICATIE1", "INDICATIE2", "INDICATIE3", "INDICATIE4", "INDICATIE5", "INDICATIE6", "INDICATIE7", "INDICATIE8", "INDICATIE9", "INDICATIE10", "INDICATIE11", "INDICATIE12", "INDICATIE13", "INDICATIE14", "INDICATIE15", "INDICATIE16", "INDICATIE17", "INDICATIE18", "INDICATIE19", "INDICATIE20", "ALLE_INDICATIES", "KAFD_CODE", "ONGR_CODE") AS
  select pers.pers_id pers_id
, pers.aanspreken pers_aanspreken
, pers.achternaam pers_achternaam
, pers.geslacht pers_geslacht
, pers.geboortedatum pers_geboortedatum
, onde.onde_id onde_onde_id
, onde.onderzoeknr onde_onderzoeknr
, onde.datum_binnen onde_datum_binnen
, onde.afgerond onde_afgerond
, onde.kafd_id onde_kafd_id
, onde.ongr_id onde_ongr_id
, onde.onui_id onde_onui_id
, indi.indi_id indi_indi_id
, row_number() over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indi_volgnr
, indi.code indicatie1
, lead(indi.code, 1) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie2
, lead(indi.code, 2) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie3
, lead(indi.code, 3) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie4
, lead(indi.code, 4) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie5
, lead(indi.code, 5) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie6
, lead(indi.code, 6) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie7
, lead(indi.code, 7) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie8
, lead(indi.code, 8) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie9
, lead(indi.code, 9) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie10
, lead(indi.code, 10) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie11
, lead(indi.code, 11) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie12
, lead(indi.code, 12) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie13
, lead(indi.code, 13) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie14
, lead(indi.code, 14) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie15
, lead(indi.code, 15) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie16
, lead(indi.code, 16) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie17
, lead(indi.code, 17) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie18
, lead(indi.code, 18) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie19
, lead(indi.code, 19) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) indicatie20
, indi.code ||
  case when (lead(indi.code, 1) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 1) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 2) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 2) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 3) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 3) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 4) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 4) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 5) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 5) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 6) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 6) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 7) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 7) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 8) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 8) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 9) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 9) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 10) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 10) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 11) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 11) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 12) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 12) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 13) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 13) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 14) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 14) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 15) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 15) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 16) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 16) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 17) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 17) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 18) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 18) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 19) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
	   else ', ' || lead(indi.code, 19) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end ||
  case when (lead(indi.code, 20) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code)) is null
	   then null
       else ', ' || lead(indi.code, 20) over (partition by onde.onderzoeknr order by indi.creation_date, indi.code) end alle_indicaties
, kafd.code kafd_code
, ongr.code ongr_code
from kgc_personen pers
, kgc_onderzoeken onde
, kgc_onderzoek_indicaties onin
, kgc_indicatie_teksten indi
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
where pers.pers_id = onde.pers_id
and onde.onde_id = onin.onde_id
and onin.indi_id = indi.indi_id
and onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
 ;

/
QUIT
