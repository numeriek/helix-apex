CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC56_TVW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "TAAL", "WIJZE", "ONDE_TYPE", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "FRACTIENR", "WERKLIJSTNR", "TECHNIEK", "TOTAAL_MEET", "NCONT", "WCONT", "NOTITIE", "VERBORGEN_NOTI_ID", "VERBORGEN_FLOW_ID") AS
  SELECT                                         -- controleren meetwaarden
           CASE
               WHEN ONDE.GEPLANDE_EINDDATUM - TRUNC (SYSDATE) <= 2
               THEN
                  '#CD5B5B'                                             --rood
               ELSE
                  CASE
                     WHEN ONDE.GEPLANDE_EINDDATUM - TRUNC (SYSDATE) > 2
                          AND ONDE.GEPLANDE_EINDDATUM - TRUNC (SYSDATE) <= 5
                     THEN
                        '#ED8B1C'                                     --oranje
                     ELSE
                        '#5CAB5C'                                      --groen
                  END
            END
               AS VERBORGEN_KLEUR,
            '' AS VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            FAMI.FAMILIENUMMER AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            TAAL.CODE TAAL,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            beh_haal_fractienr (onde.onde_id) AS FRACTIENR,
            TO_CHAR (wm_concat (DISTINCT WLST.WERKLIJST_NUMMER)) AS WERKLIJSTNR,
            TECH.CODE,
            COUNT (MEET.MEET_ID) AS totaal,
            CASE WHEN MEET.MEST_ID IS NULL THEN COUNT (MEET.MEET_ID) ELSE 0 END
               AS NCONT,
            CASE
               WHEN MEET.MEST_ID IS NOT NULL THEN COUNT (MEET.MEET_ID)
               ELSE 0
            END
               AS WCONT,
            NOTI.OMSCHRIJVING AS NOTITIE,
            NOTI.NOTI_ID AS VERBORGEN_NOTI_ID,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            kgc_onderzoekswijzen onwy,
            kgc_uitslagen uits,
            kgc_notities noti,
            beh_flow_favorieten flfa,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_stoftesten stof,
            kgc_technieken tech,
            kgc_werklijsten wlst,
            kgc_werklijst_items wlit,
            beh_flow_help_fami_vw fami,
            KGC_TALEN TAAL
      WHERE     ONDE.ONGR_ID IN (SELECT FKON.ONGR_ID
                                   FROM BEH_FLOW_KAFD_ONGR fkon
                                  WHERE FKON.FLOW_ID = 1) -- MG: dit moet nog gewijzigd worden!!!
            AND ONDE.ONDE_ID = NOTI.ID(+)
            AND ONDE.PERS_ID = FAMI.PERS_ID(+)
            AND NOTI.ENTITEIT(+) = 'ONDE'
            AND NOTI.GEREED (+) = 'N'
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.ONDE_ID = UITS.ONDE_ID
            AND ONDE.TAAL_ID = TAAL.TAAL_ID(+)
            AND ONDE.AFGEROND = 'N'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND MEET.STOF_ID = STOF.STOF_ID
            AND STOF.TECH_ID = TECH.TECH_ID
            AND WLIT.WLST_ID = WLST.WLST_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID(+)
            AND STOF.MEET_REKEN IN ('M', 'S')
            AND NOT EXISTS
                       (SELECT 1
                          FROM bas_meetwaarden meet
                         WHERE     MEET.AFGEROND = 'N'
                               AND MEET.METI_ID = METI.METI_ID
                               AND MEET.MEET_REKEN IN ('S', 'M'))
            AND EXISTS
                   (SELECT 1
                      FROM bas_meetwaarden meet
                     WHERE     MEET.MEST_ID IS NULL
                           AND MEET.METI_ID = METI.METI_ID
                           AND MEET.MEET_REKEN IN ('S', 'M'))
   GROUP BY '#79A8A8',
            '',
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            FAMI.FAMILIENUMMER,
            ONGR.CODE,
            beh_haal_familienr (ONDE.PERS_ID),
            ONDE.ONDERZOEKNR,
            TAAL.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            TECH.CODE,
            beh_haal_fractienr (onde.onde_id),
            NOTI.OMSCHRIJVING,
            NOTI.NOTI_ID,
            FLFA.FLOW_ID,
            MEET.MEST_ID
   ORDER BY ONDE.GEPLANDE_EINDDATUM ASC, ONDE.ONDERZOEKNR ASC;

/
QUIT
