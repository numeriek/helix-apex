CREATE OR REPLACE FORCE VIEW "HELIX"."METAB_MACHTIGINGEN_VW" ("ONDE_ID", "ONDERZOEKNR", "ONDERZOEKSTYPE", "DATUM_BINNEN", "AFDELING", "GROEP", "DATUM_PRINT") AS
  SELECT  onde.onde_id onde_id
	   , onde.onderzoeknr onderzoeknr
	   , onde.onderzoekstype onderzoekstype
	   , TO_CHAR(onde.datum_binnen, 'YYYY') datum_binnen
	   , kafd.code afdeling
	   , ongr.code groep
	   , TO_CHAR(brie.datum_print, 'YYYY') datum_print
FROM KGC_ONDERZOEKEN onde
	 , KGC_BRIEVEN brie
	 , KGC_BRIEFTYPES brty
	 , KGC_KGC_AFDELINGEN kafd
 	 , KGC_ONDERZOEKSGROEPEN ongr
WHERE onde.onde_id = brie.onde_id
AND onde.KAFD_ID = kafd.KAFD_ID
AND onde.ongr_id = ongr.ongr_id
AND brie.brty_id = brty.brty_id
AND UPPER(kafd.code) = UPPER('METAB')
AND UPPER(ongr.code) = UPPER('BASIS')
AND UPPER(brty.code) = UPPER('MACHTIGING')
UNION
SELECT onde.onde_id onde_id
	   , onde.onderzoeknr onderzoeknr
	   , onde.onderzoekstype onderzoekstype
	   , TO_CHAR(onde.datum_binnen, 'YYYY') datum_binnen
	   , kafd.code afdeling
	   , ongr.code groep
	   , 'NULL' datum_print
FROM KGC_ONDERZOEKEN  onde
	 , KGC_KGC_AFDELINGEN kafd
	 , KGC_ONDERZOEKSGROEPEN ongr
WHERE onde.kafd_id = kafd.kafd_id
AND  onde.ongr_id = ongr.ongr_id
AND UPPER(kafd.code) = UPPER('METAB')
AND UPPER(ongr.code) = UPPER('BASIS')
AND NOT EXISTS ( SELECT brie.onde_id
	  			   	   		  	FROM KGC_BRIEVEN brie
								, KGC_BRIEFTYPES brty
								, KGC_KGC_AFDELINGEN kafd2
								WHERE onde.onde_id = brie.onde_id
								AND brie.brty_id = brty.brty_id
								AND  brie.kafd_id = kafd2.kafd_id
								AND UPPER(kafd2.code) = UPPER('METAB')
								AND UPPER(brty.code) =

UPPER('MACHTIGING'))

 ;

/
QUIT
