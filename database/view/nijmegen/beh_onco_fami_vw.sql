CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ONCO_FAMI_VW" ("VW_FAMILIENUMMERS", "FAMI_LEDEN_AANTAL", "MANNELIJK_AANTAL", "VROUWELIJK_AANTAL", "JONGSTE_LEEFTIJD", "OUDSTE_LEEFTIJD", "BORSTKANKER", "BORST_AANTAL_MANNEN", "BORST_AANTAL_VROUWEN", "BORST_LEEFTIJD_JONGSTE", "BORST_LEEFTIJD_OUDSTE", "DARMKANKER", "DARM_AANTAL_MANNEN", "DARM_AANTAL_VROUWEN", "DARM_LEEFTIJD_JONGSTE", "DARM_LEEFTIJD_OUDSTE", "EIERSTOKKANKER", "EIER_AANTAL_MANNEN", "EIER_AANTAL_VROUWEN", "EIER_LEEFTIJD_JONGSTE", "EIER_LEEFTIJD_OUDSTE", "MAAGKANKER", "MAAG_AANTAL_MANNEN", "MAAG_AANTAL_VROUWEN", "MAAG_LEEFTIJD_JONGSTE", "MAAG_LEEFTIJD_OUDSTE", "OVERIGEKANKER", "OVERIGE_AANTAL_MANNEN", "OVERIGE_AANTAL_VROUWEN", "OVERIGE_LEEFTIJD_JONGSTE", "OVERIGE_LEEFTIJD_OUDSTE") AS
  select familienummers vw_familienummers
,      max(fami_leden) fami_leden_aantal
,      max(mannelijk) mannelijk_aantal
,      max(vrouwelijk) vrouwelijk_aantal
,      max(jongste) jongste_leeftijd
,      max(oudste) oudste_leeftijd

,      min(borstkanker) borstkanker
,      max(borst_aantal_mannen) borst_aantal_mannen
,      max(borst_aantal_vrouwen) borst_aantal_vrouwen
,      max(borst_leeftijd_jongste) borst_leeftijd_jongste
,      max(borst_leeftijd_oudste) borst_leeftijd_oudste

,      min(darmkanker) darmkanker
,      max(darm_aantal_mannen) darm_aantal_mannen
,      max(darm_aantal_vrouwen) darm_aantal_vrouwen
,      max(darm_leeftijd_jongste) darm_leeftijd_jongste
,      max(darm_leeftijd_oudste) darm_leeftijd_oudste

,      min(eierstokkanker) eierstokkanker
,      max(eier_aantal_mannen) eier_aantal_mannen
,      max(eier_aantal_vrouwen) eier_aantal_vrouwen
,      max(eier_leeftijd_jongste) eier_leeftijd_jongste
,      max(eier_leeftijd_oudste) eier_leeftijd_oudste

,      min(maagkanker) maagkanker
,      max(maag_aantal_mannen) maag_aantal_mannen
,      max(maag_aantal_vrouwen) maag_aantal_vrouwen
,      max(maag_leeftijd_jongste) maag_leeftijd_jongste
,      max(maag_leeftijd_oudste) maag_leeftijd_oudste

,      min(overigekanker) overigekanker
,      max(overige_aantal_mannen) overige_aantal_mannen
,      max(overige_aantal_vrouwen) overige_aantal_vrouwen
,      max(overige_leeftijd_jongste) overige_leeftijd_jongste
,      max(overige_leeftijd_oudste) overige_leeftijd_oudste

from
(
select subq.*,
case
  when subq.borstkanker = 'J' and cust_gender = 'M' then
  count(*) over (partition by familienummers, cust_gender, borstkanker order by familienummers, cust_gender asc)
  else 0
  end borst_aantal_mannen,
case
  when subq.borstkanker = 'J' and cust_gender = 'V' then
  count(*) over (partition by familienummers, cust_gender, borstkanker order by familienummers, cust_gender desc)
  else 0
  end borst_aantal_vrouwen,
case
  when subq.borstkanker = 'J' then
  min(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, borstkanker)
  else 0
  end borst_leeftijd_jongste,
case
  when subq.borstkanker = 'J' then
  max(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, borstkanker)
  else 0
  end borst_leeftijd_oudste,
case
  when subq.darmkanker = 'J' and cust_gender = 'M' then
  count(*) over (partition by familienummers, cust_gender, darmkanker order by familienummers, cust_gender asc)
  else 0
  end darm_aantal_mannen,
case
  when subq.darmkanker = 'J' and cust_gender = 'V' then
  count(*) over (partition by familienummers, cust_gender, darmkanker order by familienummers, cust_gender desc)
  else 0
  end darm_aantal_vrouwen,
case
  when subq.darmkanker = 'J' then
  min(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, darmkanker)
  else 0
  end darm_leeftijd_jongste,
case
  when subq.darmkanker = 'J' then
  max(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, darmkanker)
  else 0
  end darm_leeftijd_oudste,
case
  when subq.eierstokkanker = 'J' and cust_gender = 'M' then
  count(*) over (partition by familienummers, cust_gender, eierstokkanker order by familienummers, cust_gender asc)
  else 0
  end eier_aantal_mannen,
case
  when subq.eierstokkanker = 'J' and cust_gender = 'V' then
  count(*) over (partition by familienummers, cust_gender, eierstokkanker order by familienummers, cust_gender desc)
  else 0
  end eier_aantal_vrouwen,
case
  when subq.eierstokkanker = 'J' then
  min(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, eierstokkanker)
  else 0
  end eier_leeftijd_jongste,
case
  when subq.eierstokkanker = 'J' then
  max(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, eierstokkanker)
  else 0
  end eier_leeftijd_oudste,

case
  when subq.maagkanker = 'J' and cust_gender = 'M' then
  count(*) over (partition by familienummers, cust_gender, maagkanker order by familienummers, cust_gender asc)
  else 0
  end maag_aantal_mannen,
case
  when subq.maagkanker = 'J' and cust_gender = 'V' then
  count(*) over (partition by familienummers, cust_gender, maagkanker order by familienummers, cust_gender desc)
  else 0
  end maag_aantal_vrouwen,
case
  when subq.maagkanker = 'J' then
  min(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, maagkanker)
  else 0
  end maag_leeftijd_jongste,
case
  when subq.maagkanker = 'J' then
  max(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, maagkanker)
  else 0
  end maag_leeftijd_oudste,

case
  when subq.overigekanker = 'J' and cust_gender = 'M' then
  count(*) over (partition by familienummers, cust_gender, overigekanker order by familienummers, cust_gender asc)
  else 0
  end overige_aantal_mannen,
case
  when subq.overigekanker = 'J' and cust_gender = 'V' then
  count(*) over (partition by familienummers, cust_gender, overigekanker order by familienummers, cust_gender desc)
  else 0
  end overige_aantal_vrouwen,
case
  when subq.overigekanker = 'J' then
  min(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, overigekanker)
  else 0
  end overige_leeftijd_jongste,
case
  when subq.overigekanker = 'J' then
  max(round(MONTHS_BETWEEN(sysdate, cust_birthdate)/12)) over (partition by familienummers, overigekanker)
  else 0
  end overige_leeftijd_oudste

from
(
select onco.familienummers, onco.cust_gender, trunc(onco.cust_birthdate) cust_birthdate,
    count(*) over (partition by onco.familienummers) fami_leden,
case onco.cust_gender
  when 'M' then
    count(*) over (partition by onco.familienummers, onco.cust_gender order by onco.familienummers,onco.cust_gender asc)
    else 0
    end  mannelijk,
case onco.cust_gender
  when 'V' then
    count(*) over (partition by onco.familienummers, onco.cust_gender order by onco.familienummers,onco.cust_gender desc)
    else 0
    end  vrouwelijk,
min(round(MONTHS_BETWEEN(sysdate, onco.cust_birthdate)/12))  over (partition by onco.familienummers) jongste,
max(round(MONTHS_BETWEEN(sysdate, onco.cust_birthdate)/12))  over (partition by onco.familienummers) oudste,
case
  when  upper(t_1_dici_diagdesc) = 'BORSTKANKER' or
        upper(t_2_dici_diagdesc) = 'BORSTKANKER' or
        upper(t_3_dici_diagdesc) = 'BORSTKANKER' or
        upper(t_4_dici_diagdesc) = 'BORSTKANKER' or
        upper(t_5_dici_diagdesc) = 'BORSTKANKER' then
        'J'
  else 'N'
  end borstkanker,
case
  when  upper(t_1_dici_diagdesc) = 'DIKKE DARMKANKER' or
        upper(t_2_dici_diagdesc) = 'DIKKE DARMKANKER' or
        upper(t_3_dici_diagdesc) = 'DIKKE DARMKANKER' or
        upper(t_4_dici_diagdesc) = 'DIKKE DARMKANKER' or
        upper(t_5_dici_diagdesc) = 'DIKKE DARMKANKER' then
        'J'
  else 'N'
end darmkanker,
case
  when  upper(t_1_dici_diagdesc) = 'EIERSTOKKANKER' or
        upper(t_2_dici_diagdesc) = 'EIERSTOKKANKER' or
        upper(t_3_dici_diagdesc) = 'EIERSTOKKANKER' or
        upper(t_4_dici_diagdesc) = 'EIERSTOKKANKER' or
        upper(t_5_dici_diagdesc) = 'EIERSTOKKANKER' then
        'J'
  else 'N'
end eierstokkanker,
case
  when  upper(t_1_dici_diagdesc) = 'MAAGKANKER' or
        upper(t_2_dici_diagdesc) = 'MAAGKANKER' or
        upper(t_3_dici_diagdesc) = 'MAAGKANKER' or
        upper(t_4_dici_diagdesc) = 'MAAGKANKER' or
        upper(t_5_dici_diagdesc) = 'MAAGKANKER' then
        'J'
  else 'N'
  end maagkanker,
case
  when  upper(t_1_dici_diagdesc) not in('MAAGKANKER', 'BORSTKANKER', 'DIKKE DARMKANKER', 'EIERSTOKKANKER') and
        upper(t_2_dici_diagdesc) not in('MAAGKANKER', 'BORSTKANKER', 'DIKKE DARMKANKER', 'EIERSTOKKANKER') and
        upper(t_3_dici_diagdesc) not in('MAAGKANKER', 'BORSTKANKER', 'DIKKE DARMKANKER', 'EIERSTOKKANKER') and
        upper(t_4_dici_diagdesc) not in('MAAGKANKER', 'BORSTKANKER', 'DIKKE DARMKANKER', 'EIERSTOKKANKER') and
        upper(t_5_dici_diagdesc) not in('MAAGKANKER', 'BORSTKANKER', 'DIKKE DARMKANKER', 'EIERSTOKKANKER') then
        'J'
  else 'N'
end overigekanker
from beh_onco onco
--where onco.familienummers in ('13-01542', '12-07795')
where onco.familienummers is not null
) subq
)
group by familienummers;

/
QUIT
