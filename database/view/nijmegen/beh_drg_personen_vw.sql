CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_DRG_PERSONEN_VW" ("PERS_ID", "ZISNR", "BSN", "AANSPREKEN", "GEBOORTEDATUM", "GESLACHT", "OVERLEDEN", "OVERLIJDENSDATUM") AS
  SELECT pers_id,
          zisnr,
          bsn,
          aanspreken,
          geboortedatum,
          geslacht,
          overleden,
          overlijdensdatum
     FROM kgc_personen;

/
QUIT
