CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ONDE_EINDBRIEF_JN_VW" ("ONDE_ONDE_ID", "EINDBRIEF_JN") AS
  select distinct(onde.onde_id) onde_id, decode(nvl(brie.brty_id, 0), 0, 'N', 'J') eindbrief_jn
from kgc_onderzoeken onde, kgc_brieven brie
where onde.onde_id = brie.onde_id(+)
and nvl(brie.brty_id, 0) in (1, 0)
 ;

/
QUIT
