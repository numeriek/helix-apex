CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_POEMA_METI" ("RELA_RELA_ID", "RELA_LISZCODE", "RELA_AANSPREKEN", "RELA_INTERNE_POST", "RELA_EMAIL", "RELA_TELEFOON", "RELA_ADRES", "RELA_POSTCODE", "RELA_WOONPLAATS", "RELA_LAND", "PERS_PERS_ID", "PERS_ACHTERNAAM", "PERS_ZISNR", "PERS_VOORLETTERS", "PERS_VOORVOEGSEL", "PERS_AANSPREKEN", "PERS_GESLACHT", "PERS_OVERLEDEN", "PERS_GEBOORTEDATUM", "PERS_ZOEKFAMILIE", "ONDE_ONDE_ID", "ONDE_ONDERZOEKNR", "ONDE_GEPLANDE_EINDDATUM", "ONDE_SPOED", "INDI_CODE", "FRAC_FRACTIENUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "ONGR_CODE", "STGR_CODE") AS
  SELECT rela.rela_id rela_rela_id,
          rela.liszcode rela_liszcode,
          rela.aanspreken rela_aanspreken,
          rela.interne_post rela_interne_post,
          rela.email rela_email,
          rela.telefoon rela_telefoon,
          rela.adres rela_adres,
          rela.postcode rela_postcode,
          rela.woonplaats rela_woonplaats,
          rela.land rela_land,
          pers.pers_id pers_pers_id,
          pers.achternaam pers_achternaam,
          pers.zisnr pers_zisnr,
          pers.voorletters pers_voorletters,
          pers.voorvoegsel pers_voorvoegsel,
          pers.aanspreken pers_aanspreken,
          pers.geslacht pers_geslacht,
          pers.overleden pers_overleden,
          pers.geboortedatum pers_geboortedatum,
          pers.zoekfamilie pers_zoekfamilie,
          onde.onde_id onde_onde_id,
          onde.onderzoeknr onde_onderzoeknr,
          onde.geplande_einddatum onde_geplande_einddatum,
          onde.spoed onde_spoed,
          indi.code indi_code,
          frac.fractienummer frac_fractienummer,
          mate.code mate_code,
          mate.omschrijving mate_omschrijving,
          ongr.code ongr_code,
          stgr.code stgr_code
     FROM kgc_personen pers,
          kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
          bas_metingen meti,
          kgc_stoftestgroepen stgr,
          kgc_onderzoek_indicaties onin,
          kgc_indicatie_teksten indi,
          kgc_kgc_afdelingen kafd,
          kgc_onderzoeksgroepen ongr,
          kgc_relaties rela,
          kgc_materialen mate
    WHERE     pers.pers_id = onde.pers_id
          AND onde.onde_id = onmo.onde_id
          AND onmo.mons_id = mons.mons_id
          AND mons.mons_id = frac.mons_id
          AND frac.frac_id = meti.frac_id
          AND meti.stgr_id = stgr.stgr_id
          AND onde.onde_id = onin.onde_id
          AND onin.indi_id = indi.indi_id
          AND onde.kafd_id = kafd.kafd_id
          AND onde.ongr_id = ongr.ongr_id
          AND onde.rela_id = rela.rela_id
          AND mons.mate_id = mate.mate_id
          AND UPPER (stgr.code) IN
                 (SELECT *
                    FROM TABLE (
                            CAST (
                               beh_alfu_00.
                                explode (
                                  ',',
                                  UPPER (
                                     TRIM (
                                        SYS_CONTEXT (
                                           'BEH_POEMA_SELECTIE_CRITERIA',
                                           'STGR_CODE')))) AS beh_listtable))) -- protocol codes
          AND UPPER (indi.code) IN
                 (SELECT *
                    FROM TABLE (
                            CAST (
                               beh_alfu_00.
                                explode (
                                  ',',
                                  UPPER (
                                     TRIM (
                                        SYS_CONTEXT (
                                           'BEH_POEMA_SELECTIE_CRITERIA',
                                           'INDI_CODE')))) AS beh_listtable))) -- indicatie codes
          AND UPPER (onde.status) = 'G'                          --Goeggekeurd
          AND (UPPER (kafd.code) = 'CYTO' OR UPPER (kafd.code) LIKE 'DNA%' OR UPPER (kafd.code) = 'GENOOM')
          AND UPPER (onde.afgerond) = 'N'
          AND UPPER (meti.afgerond) = 'N'
 ;

/
QUIT
