CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_SOAP_001_VW" ("AANVRAGER", "GEPLANDE_EINDDATUM", "ONDE_STATUS", "ONDERZOEKSTYPE", "ONDE_INDICATIES", "ALLE_FAMILIENRS", "GEBOORTEDATUM", "GESLACHT", "ONDE_FRACTIENUMMERS", "FRACTIENUMMER") AS
  SELECT rela.aanspreken aanvrager,
             to_char(onde.geplande_einddatum, 'dd-mm-yyyy') geplande_einddatum,
             onde.status onde_status,
             onde.onderzoekstype,
             beh_soap_001.haal_alle_indi_op (onde.onde_id) onde_indicaties,
             beh_stbm_00.haal_alle_fami_op (pers.pers_id) alle_familienrs,
             pers.geboortedatum,
             pers.geslacht,
              beh_soap_001.haal_alle_frac_op (onde.onde_id, frac.fractienummer) onde_fractienummers,
             frac.fractienummer
      FROM kgc_personen pers,
             kgc_onderzoeken onde,
             kgc_onderzoek_monsters onmo,
             kgc_monsters mons,
             bas_fracties frac,
             kgc_relaties rela,
             kgc_kgc_afdelingen kafd,
             kgc_onderzoeksgroepen ongr
     WHERE      pers.pers_id = onde.pers_id
             AND onde.onde_id = onmo.onde_id
             AND onmo.mons_id = mons.mons_id
             AND mons.mons_id = frac.mons_id
             AND onde.rela_id = rela.rela_id
             AND onde.kafd_id = kafd.kafd_id
             AND onde.ongr_id = ongr.ongr_id
             AND UPPER (kafd.code) = UPPER ('GENOOM')
             AND UPPER (ongr.code) = UPPER ('NGS')
 ;

/
QUIT
