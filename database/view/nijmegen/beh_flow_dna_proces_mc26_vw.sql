CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC26_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "FAMILIENR", "ONDERZOEKNR", "SPOED", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "FRACTIENR", "WERKLIJSTNR", "ST_WEL_WLST_NIET_AFG", "VERBORGEN_FLOW_ID") AS
  SELECT -- Overzicht "Niet afgeronden stoftesten op werklijst"  -- SEMI AUTOMATISCH PROCES
           FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            beh_haal_familienr (ONDE.PERS_ID) AS FAMILIENR,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            BEH_HAAL_FRACTIENR_MATE (onde.onde_id) AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            COUNT (*) AS ST_WEL_WLST_NIET_AFG, --aantal2.metingen_wel_op_wl_niet_afg
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_technieken tech,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC26_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND STOF.TECH_ID = TECH.TECH_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND ONDE.ONDERZOEKSTYPE = 'D'
            AND MEET.AFGEROND = 'N'
            AND MEET.MEET_REKEN = 'M'
            AND STGR.CODE NOT LIKE '%_R'
            AND UPPER (STGR.OMSCHRIJVING) NOT LIKE '%ROBOT%'
            AND TECH.CODE IN ('GS')
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   GROUP BY FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR,
            FLFA.MEDE_ID,
            ONDE.PERS_ID,
            ONDE.ONDE_ID,
            ONGR.CODE,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM,
            INTE.CODE,
            INGR.CODE,
            ONWY.OMSCHRIJVING,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    ''),
            WLST.WERKLIJST_NUMMER,
            FLFA.FLOW_ID
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
