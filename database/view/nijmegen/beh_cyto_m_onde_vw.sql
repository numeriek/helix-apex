CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_CYTO_M_ONDE_VW" ("PERS_ID", "ONDE_ID", "ONDERZOEKNR", "DATUM_BINNEN", "AFGEROND", "INDI_ID", "INDI_CODE", "KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "ONUI_ID") AS
  select onde.pers_id, onde.onde_id, onde.onderzoeknr, onde.datum_binnen, onde.afgerond, indi.indi_id, indi.code, onde.kafd_id,  kafd.code, onde.ongr_id, ongr.code, onui_id
from kgc_onderzoeken onde
, kgc_kgc_afdelingen kafd
, kgc_onderzoeksgroepen ongr
, kgc_onderzoek_indicaties onin
, kgc_indicatie_teksten indi
where onde.kafd_id = kafd.kafd_id
and  onde.ongr_id = ongr.ongr_id
and onde.onde_id = onin.onde_id
and onin.indi_id = indi.indi_id
and kafd.code = 'CYTO'
and exists(
SELECT onin_vw.onde_id
from kgc_onderzoek_indicaties_vw onin_vw
where onde.onde_id = onin_vw.onde_id
AND   onin_vw.indi_code LIKE 'M%')
 ;

/
QUIT
