CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_HELP_ONDE_WLSTS" ("ONDE_ID", "WERKLIJST_NUMMER") AS
  SELECT ONDE.ONDE_ID, WLST.WERKLIJST_NUMMER
       FROM kgc_onderzoeken onde,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_werklijsten wlst,
            kgc_werklijst_items wlit
      WHERE     ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND MEET.MEET_ID =  WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.AFGEROND = 'N'
   GROUP BY ONDE.ONDE_ID, WLST.WERKLIJST_NUMMER
--
--   WITH dist_onde_wlsts
--           AS (SELECT DISTINCT meti.onde_id, wlst.werklijst_nummer
--                 FROM kgc_werklijsten wlst
--                      JOIN kgc_werklijst_items wlit
--                         ON wlit.wlst_id = wlst.wlst_id
--                      JOIN bas_metingen meti
--                         ON meti.meti_id = wlit.meti_id
--                      JOIN kgc_stoftesten stof
--                         ON stof.stof_id = wlit.stof_id
--                WHERE stof.code NOT LIKE '%KASP%'
--                      AND stof.meet_reken IN ('M', 'S'))
--     SELECT onde_id,
--            listagg (werklijst_nummer, ',')
--               WITHIN GROUP (ORDER BY werklijst_nummer)
--       FROM dist_onde_wlsts
--   GROUP BY onde_id;

/
QUIT
