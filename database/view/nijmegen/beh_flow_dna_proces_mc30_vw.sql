CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC30_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "NAAR_HELIX", "GROEP", "ONDERZOEKNR", "EINDDATUM", "INDICODE", "INGRCODE", "WIJZE", "ONDE_TYPE", "STATUS_LIMS", "FRACTIENR", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT -- Overzicht SEQFACPlaten (werklijsten) met stoftesten status = ACTIVE of COMPLETE-- AUTOMATISCH PROCES
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            NASU.STATUS,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            bas_fracties frac,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoekswijzen onwy,
            kgc_indicatie_groepen ingr,
            kgc_indicatie_teksten inte,
            BEH_FLOW_DNA_NAI_SAMPLES nasa,
            BEH_FLOW_DNA_NAI_SUBMISSIONS nasu,
            BEH_FLOW_DNA_NAI_STATUS_ACTION STAC,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND METI.METI_ID = MEET.METI_ID
            AND METI.ONDE_ID = ONDE.ONDE_ID
            AND ONDE.ONGR_ID = ONGR.ONGR_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND NASA.MEASURE_ID = MEET.MEET_ID
            AND NASA.SUBMISSION_ID = NASU.SUBMISSION_ID
            AND NASU.SUBMISSION_ID = STAC.ID
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC30_VW'
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND ONDE.ONDERZOEKSTYPE = 'D'
            AND UPPER (NASU.STATUS) IN ('ACTIVE', 'COMPLETE')
            AND UPPER (STAC.STATUS) <> 'FIRED'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
