CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_ONDE_ONGR_2012_VW" ("PERS_ID", "ARR", "BASIS", "DD", "GLYCOS", "HOM", "KWEEK", "LTG", "LYSO", "MITO", "NGS", "OVERIG", "P", "POST_NIJM", "PR", "PRE_NIJM", "RFD", "TUM_NIJM", "AANTAL") AS
  select onde.pers_id
, sum(decode(ongr.code, 'ARR', 1, 0)) ARR
, sum(decode(ongr.code, 'BASIS', 1, 0)) "BASIS"
, sum(decode(ongr.code, 'DD', 1, 0)) DD
, sum(decode(ongr.code, 'GLYCOS', 1, 0)) GLYCOS
, sum(decode(ongr.code, 'HOM', 1, 0)) HOM
, sum(decode(ongr.code, 'KWEEK', 1, 0)) KWEEK
, sum(decode(ongr.code, 'LTG', 1, 0)) LTG
, sum(decode(ongr.code, 'LYSO', 1, 0)) LYSO
, sum(decode(ongr.code, 'MITO', 1, 0)) MITO
, sum(decode(ongr.code, 'NGS', 1, 0)) NGS
, sum(decode(ongr.code, 'OVERIG', 1, 0)) OVERIG
, sum(decode(ongr.code, 'P', 1, 0)) "P"
, sum(decode(ongr.code, 'POST_NIJM', 1, 0)) POST_NIJM
, sum(decode(ongr.code, 'PR', 1, 0)) PR
, sum(decode(ongr.code, 'PRE_NIJM', 1, 0)) PRE_NIJM
, sum(decode(ongr.code, 'RFD', 1, 0)) RFD
, sum(decode(ongr.code, 'TUM_NIJM', 1, 0)) TUM_NIJM
, count(*) aantal
from kgc_onderzoeken onde
, kgc_onderzoeksgroepen ongr
where onde.ongr_id = ongr.ongr_id
and onde.kafd_id in (1, 6)
and onderzoekstype <>'R'
and onde.datum_binnen > to_date('01-01-2011','dd-mm-yyyy')
group by pers_id;

/
QUIT
