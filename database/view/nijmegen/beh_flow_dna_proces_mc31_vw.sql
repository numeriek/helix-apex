CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FLOW_DNA_PROCES_MC31_VW" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "FAVORIET", "VERBORGEN_PERS_ID", "GROEP", "ONDERZOEKNR", "SPOED", "EINDDATUM", "WIJZE", "ONDE_TYPE", "INDICODE", "INGRCODE", "FRACTIENR", "WERKLIJSTNR", "EXTERNE_ID", "VERBORGEN_FLOW_ID") AS
  SELECT DISTINCT -- SEQ/GS Stoftesten gestuurd naar SQLLIMS, maar nog niet geaccepteerd  -- SEMI AUTOMATISCH PROCES
            FLPR.VERBORGEN_KLEUR,
            FLPR.VERBORGEN_SORTERING,
            ONDE.ONDERZOEKNR AS VERBORGEN_TELLING,
            FLFA.MEDE_ID AS FAVORIET,
            ONDE.PERS_ID AS NAAR_HELIX,
            ONGR.CODE AS GROEP,
            ONDE.ONDERZOEKNR,
            ONDE.SPOED,
            ONDE.GEPLANDE_EINDDATUM AS EINDDATUM,
            ONWY.OMSCHRIJVING AS WIJZE,
            DECODE (ONDE.ONDERZOEKSTYPE,
                    'R', 'Research',
                    'D', 'Diagnostiek',
                    '')
               AS ONDE_TYPE,
            INTE.CODE AS INDICODE,
            INGR.CODE AS INGRCODE,
            FRAC.FRACTIENUMMER AS FRACTIENR,
            WLST.WERKLIJST_NUMMER AS WERKLIJSTNR,
            TRGE.EXTERNE_ID,
            --TRGE.GEFIXEERD,
            FLFA.FLOW_ID AS VERBORGEN_FLOW_ID
       FROM kgc_onderzoeken onde,
            kgc_onderzoeksgroepen ongr,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr,
            bas_metingen meti,
            bas_meetwaarden meet,
            kgc_werklijst_items wlit,
            kgc_werklijsten wlst,
            bas_fracties frac,
            kgc_tray_gebruik trge,
            kgc_stoftesten stof,
            kgc_stoftestgroepen stgr,
            kgc_onderzoekswijzen onwy,
            kgc_technieken tech,
            BEH_FLOW_DNA_NAI_SAMPLES nasa,
            BEH_FLOW_DNA_NAI_SUBMISSIONS NASU,
            BEH_FLOW_PRIORITEIT_NEW_VW FLPR,
            beh_flow_favorieten flfa
      WHERE     FLPR.VIEW_NAME = 'BEH_FLOW_DNA_PROCES_MC31_VW'
            AND ONDE.KAFD_ID = FLPR.KAFD_ID
            AND ONDE.ONDE_ID = FLPR.ONDE_ID
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID(+)
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONIN.INDI_ID = INTE.INDI_ID(+)
            AND INTE.INGR_ID = INGR.INGR_ID(+)
            AND MEET.MEET_ID = WLIT.MEET_ID
            AND WLIT.WLST_ID = WLST.WLST_ID
            AND ONDE.ONDE_ID = METI.ONDE_ID
            AND METI.METI_ID = MEET.METI_ID
            AND FRAC.FRAC_ID = METI.FRAC_ID
            AND TRGE.WERKLIJST_NUMMER = WLST.WERKLIJST_NUMMER
            AND STOF.STOF_ID = MEET.STOF_ID
            AND STGR.STGR_ID = METI.STGR_ID
            AND STOF.TECH_ID = TECH.TECH_ID
            AND NASA.MEASURE_ID = MEET.MEET_ID
            AND NASA.SUBMISSION_ID = NASU.SUBMISSION_ID
            AND ONDE.ONDE_ID = FLFA.ID(+)
            AND ONDE.AFGEROND = 'N'
            AND ONDE.STATUS = 'G'
            AND MEET.AFGEROND = 'N'
            AND TRGE.EXTERNE_ID IS NOT NULL
            AND STGR.CODE NOT LIKE '%!_R' ESCAPE '!'
            AND UPPER (STGR.OMSCHRIJVING) NOT LIKE '%ROBOT%'
            AND TECH.CODE IN ('SEQ', 'GS')
            AND NASU.STUDY_DATAGROUP NOT LIKE '%_VW'
            AND NASU.CONDITION <> 'REJECTED'
            AND UPPER (FLFA.ENTITEIT(+)) = UPPER ('ONDE')
   ORDER BY SPOED ASC,
            VERBORGEN_SORTERING,
            ONDE.GEPLANDE_EINDDATUM ASC,
            ONDE.ONDERZOEKNR ASC;

/
QUIT
