CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_FAMILIE_RELATIES" ("PERS_ID", "AANSPREKEN", "GESLACHT", "GEBOORTEDATUM", "RELATIE", "PERS_ID_RELATIE", "AANSPREKEN_RELATIE", "GESLACHT_RELATIE", "GEBOORTEDATUM_RELATIE") AS 
SELECT PERS.PERS_ID,
        PERS.AANSPREKEN,
        PERS.GESLACHT,
        PERS.GEBOORTEDATUM,
        CASE
           WHEN REPE.RICHTING = '>' THEN 'KIND'
           WHEN REPE.RICHTING = '<' AND PERS.GESLACHT = 'V' THEN 'MOEDER'
           WHEN REPE.RICHTING = '<' AND PERS.GESLACHT = 'M' THEN 'VADER'
           WHEN REPE.RICHTING = '<' AND OUDER.GESLACHT = 'O' THEN 'ONBEKEND'
           ELSE ''
        END
           AS RELATIE,
        REPE.PERS_ID_RELATIE,
        OUDER.AANSPREKEN,
        OUDER.GESLACHT,
        OUDER.GEBOORTEDATUM
   FROM KGC_KGCREPE01_VW repe,
        kgc_familie_leden fale,
        kgc_families fami,
        kgc_personen pers,
        kgc_personen ouder,
        kgc_familie_relaties fare
  WHERE     FAMI.FAMI_ID = FALE.FAMI_ID
        AND PERS.PERS_ID = FALE.PERS_ID
        AND FALE.PERS_ID = REPE.PERS_ID
        AND OUDER.PERS_ID = REPE.PERS_ID_RELATIE
        AND REPE.FARE_ID = FARE.FARE_ID
        AND FARE.CODE IN ('DOC', 'ZOO');
/

begin
  kgc_util_00.reconcile( p_object => 'BEH_FAMILIE_RELATIES' );
end;
/
