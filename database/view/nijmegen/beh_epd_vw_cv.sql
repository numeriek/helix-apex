CREATE OR REPLACE FORCE VIEW "HELIX"."BEH_EPD_VW_CV" ("KAFD_ID", "KAFD_CODE", "DIAGNOSTIEK", "VRAAGSTELLING_KORT", "VRAAGSTELLING_LANG", "ONGR_ID", "ONGR_CODE_REPORT", "PERS_ID", "ONDE_ID", "RELA_ID", "ZISNR", "ONDERZOEKNR", "TOEVOEGING", "DATUM_BINNEN", "ONDERZOEKSTYPE", "RELA_AANSPREKEN", "LISZCODE", "AFGEROND", "ONUI_CODE", "ONUI_OMSCHRIJVING", "VERVANGENDE_TEKST", "TAAL_CODE", "DATUM_BESCHIKBAAR_VANAF", "BESCHIKBAAR", "ONEP_ID", "UITSLAG1", "DATUM_UITSLAG1", "UITSLAG2", "DATUM_UITSLAG2", "UITSLAG3", "DATUM_UITSLAG3", "UITSLAG4", "DATUM_UITSLAG4", "UITSLAG5", "DATUM_UITSLAG5") AS
  SELECT													-- onderzoeken van de persoon:
			onde.kafd_id,
			 kafd.code kafd_code,
			 CONVERT (beh_epd_00.kafd_epd_tonen (onde.ongr_id), 'US7ASCII')
				 diagnostiek,
			 CONVERT (
				 SUBSTR (beh_epd_00.ongr_epd_tonen (onde.ongr_id, onde.onde_id),
							1,
							80
						  ),
				 'US7ASCII')
				 vraagstelling_kort,
			 CONVERT (beh_epd_00.ongr_epd_tonen (onde.ongr_id, onde.onde_id),
						 'US7ASCII'
						)
				 vraagstelling_lang,
			 onde.ongr_id,
			 ongr.code ongr_code_report,
			 onde.pers_id,
			 onde.onde_id,
			 onde.rela_id,
			 pers.zisnr,
			 onde.onderzoeknr,
			 DECODE (
				 onde.foet_id,
				 NULL, NULL,
				 'foetus: '
				 || kgc_foet_00.foetus_info (onde.foet_id, NULL, NULL, 'J'))
			 || kgc_onde_00.counselor (onde.onde_id)
				 toevoeging,
			 TO_CHAR (onde.datum_binnen, 'dd-mm-yyyy') datum_binnen,
			 onde.onderzoekstype,
			 CONVERT (rela.aanspreken, 'US7ASCII') rela_aanspreken,
			 rela.liszcode,
			 beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) afgerond,
			 onui.code onui_code,
			 onui.omschrijving onui_omschrijving,
			 kgc_sypa_00.
			  standaard_waarde ('BEH_EPD_VERVANGENDE_TEKST',
									  kafd.code,
									  ongr.code,
									  NULL
									 )
				 vervangende_tekst,
			 NVL (taal.code, 'NED') taal_code,
			 TO_CHAR (onep.datum_beschikbaar_vanaf, 'dd-mm-yyyy')
				 datum_beschikbaar_vanaf,
			 DECODE (
				 beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
				 'N', 'N',
				 (DECODE (
					  SIGN (MONTHS_BETWEEN (onep.datum_beschikbaar_vanaf, SYSDATE)
							 ),
					  NULL, 'J',
					  0, 'J',
					  -1, 'J',
					  1, 'N')))
				 beschikbaar,
			 onep.onep_id,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 1)
					  )
				 uitslag1,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 1)
					  )
				 datum_uitslag1,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 2)
					  )
				 uitslag2,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 2)
					  )
				 datum_uitslag2,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 3)
					  )
				 uitslag3,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 3)
					  )
				 datum_uitslag3,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 4)
					  )
				 uitslag4,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 4)
					  )
				 datum_uitslag4,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 5)
					  )
				 uitslag5,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 5)
					  )
				 datum_uitslag5
	  FROM kgc_kgc_afdelingen kafd,
			 kgc_onderzoeksgroepen ongr,
			 kgc_onderzoek_uitslagcodes onui,
			 kgc_relaties rela,
			 kgc_personen pers,
			 kgc_onderzoeken onde,
			 kgc_talen taal,
			 beh_kgc_onderzoeken_epd onep
	 WHERE	  onde.rela_id = rela.rela_id
			 AND onde.kafd_id = kafd.kafd_id
			 AND onde.ongr_id = ongr.ongr_id
			 AND onde.pers_id = pers.pers_id
			 AND onde.taal_id = taal.taal_id(+)
			 AND onde.onui_id = onui.onui_id(+)
			 AND onde.onderzoekstype IN ('D', 'C')
			 AND onep.uits_id(+) = beh_epd_00.zoek_uitslag (onde.onde_id, 1)
			 AND NVL (
					  INSTR (
						  kgc_sypa_00.
							standaard_waarde ('BEH_EPD_UITSLCODES_UITSLUITEN',
													kafd.code,
													ongr.code,
													NULL
												  ),
						  NVL (onui.code, 'NVL')),
					  0) = 0
			 --  AND onde.afgerond = 'J'
			 AND ( (kafd.code = 'METAB'
					  AND ( -- als het onderzoek is afgerond moeten er verzonden brieven zijn
							 -- dit geldt alleen voor onderzoeken van voor 02 okt 2010
							 (onde.afgerond = 'J'
							  AND onde.datum_autorisatie <
										TO_DATE ('02-10-2010', 'dd-mm-yyyy')
							  AND EXISTS
										(SELECT *
											FROM kgc_brieven brie
										  WHERE onde.onde_id = brie.onde_id
												  AND brie.datum_verzonden IS NOT NULL))
							 OR (onde.afgerond = 'J'
								  AND onde.datum_autorisatie >=
											TO_DATE ('02-10-2010', 'dd-mm-yyyy'))
							 OR (onde.afgerond = 'N'))
					  AND (			  -- critieria per (set van) onderzoeksgroep(en)
							  (ongr.code IN ('BASIS', 'HOM')
								AND onde.datum_binnen >
										 TO_DATE ('02-05-2004', 'dd-mm-yyyy')
								AND ( (	  onde.onderzoekstype = 'C'
										 AND INSTR (
												  beh_onderzoek_indicaties (onde.onde_id),
												  '1000') > 0
										 AND onde.datum_binnen > (SYSDATE - 365))
									  OR (onde.onderzoekstype = 'C'
											AND INSTR (
													 beh_onderzoek_indicaties (
														 onde.onde_id),
													 '1000') = 0)
									  OR onde.onderzoekstype <> 'C'))
							 OR (ongr.code IN
									  ('LYSO', 'GLYCOS', 'MITO', 'KWEEK', 'OVERIG')
								  AND onde.datum_binnen >
											TO_DATE ('12-12-2004', 'dd-mm-yyyy'))
							 OR (ongr.code = 'DD' AND onde.status IN ('G')
								  AND onde.datum_binnen >
											TO_DATE ('12-12-2004', 'dd-mm-yyyy'))))
					OR (kafd.code = 'CYTO'
						 AND ( (ongr.code NOT LIKE 'PRE%'
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR (	  ongr.code LIKE 'PRE%'
									 AND onde.status IN ('G')
									 AND beh_epd_00.
											onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J')
						 AND onde.datum_binnen >
								  TO_DATE ('19-05-2003', 'dd-mm-yyyy'))
					OR (kafd.code = 'GENOOM'
						 AND ( ( 						  -- alles van oorspronkelijk DNA
								  beh_epd_00.buiten_cons (onde.onde_id) = 'N'
								  AND onde.datum_binnen >
											TO_DATE ('10-03-2002', 'dd-mm-yyyy')
								  AND (ongr.code IN ('P', 'PR', 'RFD', 'NGS', 'ARR')
										 OR (ongr.code = 'LTG'
											  AND (INSTR (
														 beh_epd_00.
														  ingr_code (onde.onde_id),
														 '04_G10_ND') = 0)))
								  AND ( (onde.status IN ('G')
											AND beh_epd_00.
												  onderzoek_epd_afgerond (onde.onde_id) =
													 'N')
										 OR beh_epd_00.
											  onderzoek_epd_afgerond (onde.onde_id) =
												 'J'))
								OR 			-- en alles wat er van cyto is bij gekomen
									(ongr.code IN
										 ('PRE_NIJM',
										  'POST_NIJM',
										  'POST_ENS',
										  'TUM_NIJM',
										  'TUM_ENS')
									 AND ( (ongr.code <> 'PRE_NIJM'
											  AND beh_epd_00.
													 onderzoek_epd_afgerond (onde.onde_id) =
														'N')
											OR (	  ongr.code = 'PRE_NIJM'
												 AND onde.status IN ('G')
												 AND beh_epd_00.
														onderzoek_epd_afgerond (
														  onde.onde_id) = 'N')
											OR beh_epd_00.
												 onderzoek_epd_afgerond (onde.onde_id) =
													'J'))))
					OR (kafd.code = 'DNA-A' AND ongr.code IN ('P')
						 AND ( (onde.status IN ('G')
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J'))
					OR (kafd.code = 'DNA-C' AND ongr.code IN ('P')
						 AND ( (onde.status IN ('G')
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J'))
					OR (kafd.code = 'DNA-M' AND ongr.code IN ('P')
						 AND ( (onde.status IN ('G')
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J')))
    and pers.zisnr = '7929620'
	UNION ALL -- onderzoeken van de moeder waarbij de persoon als foetus geregistreerd staat:
	SELECT onde.kafd_id,
			 kafd.code kafd_code,
			 CONVERT (beh_epd_00.kafd_epd_tonen (onde.ongr_id), 'US7ASCII')
				 diagnostiek,
			 CONVERT (
				 SUBSTR (beh_epd_00.ongr_epd_tonen (onde.ongr_id, onde.onde_id),
							1,
							80
						  ),
				 'US7ASCII')
				 vraagstelling_kort,
			 CONVERT (beh_epd_00.ongr_epd_tonen (onde.ongr_id, onde.onde_id),
						 'US7ASCII'
						)
				 vraagstelling_lang,
			 onde.ongr_id,
			 ongr.code ongr_code_report,
			 foet.pers_id_geboren_als,
			 onde.onde_id,
			 onde.rela_id,
			 pers.zisnr,
			 onde.onderzoeknr,
				 'als foetus: '
			 || kgc_foet_00.foetus_info (onde.foet_id, NULL, NULL, 'J')
			 || kgc_onde_00.counselor (onde.onde_id)
				 toevoeging,
			 TO_CHAR (onde.datum_binnen, 'dd-mm-yyyy') datum_binnen,
			 onde.onderzoekstype,
			 CONVERT (rela.aanspreken, 'US7ASCII') rela_aanspreken,
			 rela.liszcode,
			 beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) afgerond,
			 onui.code onui_code,
			 onui.omschrijving onui_omschrijving,
			 kgc_sypa_00.
			  standaard_waarde ('BEH_EPD_VERVANGENDE_TEKST',
									  kafd.code,
									  ongr.code,
									  NULL
									 )
				 vervangende_tekst,
			 NVL (taal.code, 'NED') taal_code,
			 TO_CHAR (onep.datum_beschikbaar_vanaf, 'dd-mm-yyyy')
				 datum_beschikbaar_vanaf,
			 DECODE (
				 beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
				 'N', 'N',
				 (DECODE (
					  SIGN (MONTHS_BETWEEN (onep.datum_beschikbaar_vanaf, SYSDATE)
							 ),
					  NULL, 'J',
					  0, 'J',
					  -1, 'J',
					  1, 'N')))
				 beschikbaar,
			 onep.onep_id,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 1)
					  )
				 uitslag1,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 1)
					  )
				 datum_uitslag1,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 2)
					  )
				 uitslag2,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 2)
					  )
				 datum_uitslag2,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 3)
					  )
				 uitslag3,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 3)
					  )
				 datum_uitslag3,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 4)
					  )
				 uitslag4,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 4)
					  )
				 datum_uitslag4,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag (onde.onde_id, 5)
					  )
				 uitslag5,
			 DECODE (beh_epd_00.onderzoek_epd_afgerond (onde.onde_id),
						'N', NULL,
						beh_epd_00.zoek_uitslag_datum (onde.onde_id, 5)
					  )
				 datum_uitslag5
	  FROM kgc_kgc_afdelingen kafd,
			 kgc_onderzoeksgroepen ongr,
			 kgc_onderzoek_uitslagcodes onui,
			 kgc_relaties rela,
			 kgc_personen pers,
			 kgc_onderzoeken onde,
			 kgc_foetussen foet,
			 kgc_talen taal,
			 beh_kgc_onderzoeken_epd onep
	 WHERE	  onde.rela_id = rela.rela_id
			 AND onde.kafd_id = kafd.kafd_id
			 AND onde.ongr_id = ongr.ongr_id
			 AND foet.pers_id_geboren_als = pers.pers_id
			 AND onde.taal_id = taal.taal_id(+)
			 AND onde.onui_id = onui.onui_id(+)
			 AND onde.onderzoekstype IN ('D', 'C')
			 AND onep.uits_id(+) = beh_epd_00.zoek_uitslag (onde.onde_id, 1)
			 AND NVL (
					  INSTR (
						  kgc_sypa_00.
							standaard_waarde ('BEH_EPD_UITSLCODES_UITSLUITEN',
													kafd.code,
													ongr.code,
													NULL
												  ),
						  NVL (onui.code, 'NVL')),
					  0) = 0
			 -- AND onde.afgerond = 'J'
			 AND ( (kafd.code = 'METAB'
					  AND ( -- als het onderzoek is afgerond moeten er verzonden brieven zijn
							 -- dit geldt alleen voor onderzoeken van voor 02 okt 2010
							 (onde.afgerond = 'J'
							  AND onde.datum_autorisatie <
										TO_DATE ('02-10-2010', 'dd-mm-yyyy')
							  AND EXISTS
										(SELECT *
											FROM kgc_brieven brie
										  WHERE onde.onde_id = brie.onde_id
												  AND brie.datum_verzonden IS NOT NULL))
							 OR (onde.afgerond = 'J'
								  AND onde.datum_autorisatie >=
											TO_DATE ('02-10-2010', 'dd-mm-yyyy'))
							 OR (onde.afgerond = 'N'))
					  AND (			  -- critieria per (set van) onderzoeksgroep(en)
							  (ongr.code IN ('BASIS', 'HOM')
								AND onde.datum_binnen >
										 TO_DATE ('02-05-2004', 'dd-mm-yyyy')
								AND ( (onde.onderzoekstype = 'C'
										 AND onde.datum_binnen > (SYSDATE - 365))
									  OR onde.onderzoekstype <> 'C'))
							 OR (ongr.code IN
									  ('LYSO', 'GLYCOS', 'MITO', 'KWEEK', 'OVERIG')
								  AND onde.datum_binnen >
											TO_DATE ('12-12-2004', 'dd-mm-yyyy'))
							 OR (ongr.code = 'DD' AND onde.status IN ('G')
								  AND onde.datum_binnen >
											TO_DATE ('12-12-2004', 'dd-mm-yyyy'))))
					OR (kafd.code = 'CYTO'
						 AND ( (ongr.code NOT LIKE 'PRE%'
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR (	  ongr.code LIKE 'PRE%'
									 AND onde.status IN ('G')
									 AND beh_epd_00.
											onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J')
						 AND onde.datum_binnen >
								  TO_DATE ('19-05-2003', 'dd-mm-yyyy'))
					OR (kafd.code = 'GENOOM'
						 AND ( ( 						  -- alles van oorspronkelijk DNA
								  beh_epd_00.buiten_cons (onde.onde_id) = 'N'
								  AND onde.datum_binnen >
											TO_DATE ('10-03-2002', 'dd-mm-yyyy')
								  AND (ongr.code IN ('P', 'PR', 'RFD', 'NGS', 'ARR')
										 OR (ongr.code = 'LTG'
											  AND (INSTR (
														 beh_epd_00.
														  ingr_code (onde.onde_id),
														 '04_G10_ND') = 0)))
								  AND ( (onde.status IN ('G')
											AND beh_epd_00.
												  onderzoek_epd_afgerond (onde.onde_id) =
													 'N')
										 OR beh_epd_00.
											  onderzoek_epd_afgerond (onde.onde_id) =
												 'J'))
								OR 			-- en alles wat er van cyto is bij gekomen
									(ongr.code IN
										 ('PRE_NIJM',
										  'POST_NIJM',
										  'POST_ENS',
										  'TUM_NIJM',
										  'TUM_ENS')
									 AND ( (ongr.code <> 'PRE_NIJM'
											  AND beh_epd_00.
													 onderzoek_epd_afgerond (onde.onde_id) =
														'N')
											OR (	  ongr.code = 'PRE_NIJM'
												 AND onde.status IN ('G')
												 AND beh_epd_00.
														onderzoek_epd_afgerond (
														  onde.onde_id) = 'N')
											OR beh_epd_00.
												 onderzoek_epd_afgerond (onde.onde_id) =
													'J'))))
					OR (kafd.code = 'DNA-A' AND ongr.code IN ('P')
						 AND ( (onde.status IN ('G')
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J'))
					OR (kafd.code = 'DNA-C' AND ongr.code IN ('P')
						 AND ( (onde.status IN ('G')
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J'))
					OR (kafd.code = 'DNA-M' AND ongr.code IN ('P')
						 AND ( (onde.status IN ('G')
								  AND beh_epd_00.
										 onderzoek_epd_afgerond (onde.onde_id) = 'N')
								OR beh_epd_00.onderzoek_epd_afgerond (onde.onde_id) =
										'J')))
			 AND onde.foet_id = foet.foet_id
                 and pers.zisnr = '7929620';

/
QUIT
