CREATE OR REPLACE FORCE VIEW "HELIX"."DNA_PLANLIJST_VW" ("KAFD_CODE", "FAMILIENUMMER", "NAAM", "OPMERKINGEN") AS
  SELECT kafd.code kafd_code,
             fami.familienummer,
             fami.naam familie,
             fami.opmerkingen
      FROM kgc_kgc_afdelingen kafd, kgc_families fami
     WHERE fami.opmerkingen IS NOT NULL AND kafd.code = 'GENOOM'
 ;

/
QUIT
