CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERZOEK_BETROKKENEN_VW" ("KAFD_CODE", "ONDERZOEKNR", "ONDE_AFGEROND", "ONDE_DATUM_BINNEN", "FRACTIENUMMER", "MONSTERNUMMER", "MATE_OMSCHRIJVING", "PERS_AANSPREKEN", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_ZISNR", "FAMILIENUMMER", "METI_DATUM_AANMELDING", "ONDE_ID", "FRAC_ID", "FOETUS") AS
  SELECT kafd.code kafd_code
,      onde.onderzoeknr
,      onde.afgerond onde_afgerond
,      onde.datum_binnen onde_datum_binnen
,      frac.fractienummer
,      mons.monsternummer
,      mate.omschrijving mate_omschrijving
,      pers.aanspreken pers_aanspreken
,      pers.geboortedatum pers_geboortedatum
,      pers.geslacht pers_geslacht
,      pers.zisnr pers_zisnr
,      kgc_fami_00.familie_bij_onderzoek( onde.onde_id ) familienummer
,      meti.datum_aanmelding meti_datum_aanmelding
,      meti.onde_id
,      meti.frac_id
,      foet.identificatie--Added for Mantis 6717
from   kgc_kgc_afdelingen kafd
,      kgc_materialen mate
,      kgc_personen pers
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      kgc_onderzoeken onde
,      kgc_foetussen_vw foet--Added for Mantis 6717
where meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    mons.pers_id = pers.pers_id
and    mons.kafd_id = kafd.kafd_id
and    mons.mate_id = mate.mate_id
and    meti.onde_id = onde.onde_id
AND   onde.foet_id=foet.foet_id(+) /*Added for Mantis 6717*/;

/
QUIT
