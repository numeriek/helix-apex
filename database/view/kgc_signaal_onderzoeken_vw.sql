CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_SIGNAAL_ONDERZOEKEN_VW" ("ONDE_ID", "KAFD_CODE", "PERS_ACHTERNAAM", "PERS_AANSPREKEN", "ONDERZOEKNR", "SPOED", "STATUS", "DATUM_BINNEN", "ONDERZOEKSWIJZE", "INDI_CODE", "INDICATIE", "GEPLANDE_EINDDATUM", "DATUM_SIGNAAL") AS
  SELECT onde.onde_id
,      kafd.code kafd_code
,      pers.achternaam pers_achternaam
,      pers.aanspreken pers_aanspreken
,      onde.onderzoeknr
,      onde.spoed
,      onde.status
,      onde.datum_binnen
,      onde.onderzoekswijze
,      kgc_indi_00.indi_code(onde.onde_id) indi_code
,      kgc_indi_00.onderzoeksreden(onde.onde_id) indicatie
,      onde.geplande_einddatum
,      kgc_loop_00.signaleringsdatum
       ( onde.datum_binnen
       , kgc_indi_00.indi_id(onde.onde_id)
       , onde.onderzoekswijze
       )  datum_signaal
from kgc_kgc_afdelingen  kafd
,     kgc_personen pers
,      kgc_onderzoeken onde
where onde.kafd_id = kafd.kafd_id
and    onde.pers_id = pers.pers_id
and    onde.afgerond = 'N'
 ;

/
QUIT
