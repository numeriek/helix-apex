CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_PROBE_MEETWAARDEN_VW" ("MEET_ID", "METI_ID", "VOLGORDE", "STOF_ID", "PROS_CODE", "PROS_OMSCHRIJVING", "CHROMOSOOM", "LOCUS", "MB_POSITIE_VA", "MB_POSITIE_TM", "GEN") AS
  SELECT meet.meet_id
,      meet.meti_id
,      meet.volgorde
,      meet.stof_id
,      nvl( pros.code, stof.code ) pros_code
,      pros.omschrijving pros_omschrijving
,      pros.chromosoom
,      pros.locus
,      pros.mb_positie_va
,      pros.mb_positie_tm
,      pros.gen
from   kgc_stoftesten stof
,      kgc_probes pros
,      bas_meetwaarden meet
where  meet.stof_id = pros.stof_id
and    pros.stof_id = stof.stof_id
 ;

/
QUIT
