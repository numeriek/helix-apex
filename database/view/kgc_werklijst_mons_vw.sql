CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_WERKLIJST_MONS_VW" ("WLST_ID", "MONS_ID", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "DATUM_AFNAME", "PERS_ZISNR", "PERS_AANSPREKEN", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT") AS
  SELECT distinct
     wlit.wlst_id
  ,  wlit.mons_id
  ,  mons.monsternummer
  ,  mate.code mate_code
  ,  mate.omschrijving mate_omschrijving
  ,  mons.datum_afname
  ,  pers.zisnr pers_zisnr
  ,  pers.aanspreken pers_aanspreken
  ,  pers.achternaam pers_achternaam
  ,  pers.geboortedatum pers_geboortedatum
  ,  pers.geslacht pers_geslacht
from kgc_materialen mate
,    kgc_personen pers
,    kgc_monsters mons
,    kgc_werklijst_items wlit
where wlit.mons_id = mons.mons_id
and   mons.pers_id = pers.pers_id
and   mons.mate_id = mate.mate_id
 ;

/
QUIT
