CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BINNENGEKOMEN_VW" ("ONDERZOEKNR", "DATUM_BINNEN", "ONDE_EINDDATUM", "MONSTERNUMMER", "MATE_CODE", "PERSOON_INFO", "FOET_VOLGNR", "ZWANGERSCHAPSDUUR", "KAFD_CODE", "ONGR_CODE", "SPOED", "ONWY_OMSCHRIJVING", "ONMO_ID", "ONDE_ID", "MONS_ID", "PERS_ID", "FOET_ID", "ZWAN_ID") AS
  SELECT /*+RULE */
       onde.onderzoeknr
,      onde.datum_binnen datum_binnen
,      onde.geplande_einddatum onde_einddatum
,      mons.monsternummer
,      mate.code mate_code
,      kgc_pers_00.info( mons.pers_id ) persoon_info
,      foet.volgnr foet_volgnr
,      kgc_zwan_00.duur( onde.onde_id ) zwangerschapsduur
,      kafd.code kafd_code
,      ongr.code ongr_code
,      onde.spoed
,      kgc_onwy_00.onwy_omschrijving(onde.onde_id) onwy_omschrijving
,      onmo.onmo_id
,      onde.onde_id
,      mons.mons_id
,      mons.pers_id
,      mons.foet_id
,      foet.zwan_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_foetussen foet
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
,      kgc_onderzoeken onde
where onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.onde_id = onmo.onde_id
and    onmo.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
and    mons.foet_id = foet.foet_id (+)
-- and    onde.afgerond = 'N'
union -- onderzoeken zonder monster
select onde.onderzoeknr
,      onde.datum_binnen
,      onde.geplande_einddatum
,      null -- monsternummer
,      null -- mate_code
,      kgc_pers_00.info( onde.pers_id )
,      foet.volgnr foet_volgnr
,      kgc_zwan_00.duur( onde.onde_id )
,      kafd.code
,      ongr.code
,      onde.spoed
,      kgc_onwy_00.onwy_omschrijving(onde.onde_id) onwy_omschrijving
,      to_number(null) -- onmo_id
,      onde.onde_id
,      to_number(null) -- mons_id
,      onde.pers_id
,      onde.foet_id
,      foet.zwan_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_foetussen foet
,      kgc_onderzoeken onde
where onde.kafd_id = kafd.kafd_id
and    onde.ongr_id = ongr.ongr_id
and    onde.foet_id = foet.foet_id (+)
-- and    onde.afgerond = 'N'
and    not exists
           ( select null
             from    kgc_onderzoek_monsters onmo
             where onmo.onde_id = onde.onde_id
          )
union  -- monsters zonder onderzoek
select null -- onderzoeknr
,      mons.datum_aanmelding -- datum_binnen
,      to_date(null) -- onde_einddatum
,      mons.monsternummer
,      mate.code mate_code
,      kgc_pers_00.info( mons.pers_id ) persoon_info
,      foet.volgnr foet_volgnr
,      kgc_zwan_00.duur( null ) zwangerschapsduur
,      kafd.code kafd_code
,      ongr.code ongr_code
,      'N'
,      null
,      to_number(null) -- onmo_id
,      to_number(null) --onde_id
,      mons.mons_id
,      mons.pers_id
,      mons.foet_id
,      foet.zwan_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_foetussen foet
,      kgc_monsters mons
where mons.kafd_id = kafd.kafd_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mate_id = mate.mate_id
and    mons.foet_id = foet.foet_id (+)
and    not exists
           ( select null
             from    kgc_onderzoek_monsters onmo
             where onmo.mons_id = mons.mons_id
          )
 ;

/
QUIT
