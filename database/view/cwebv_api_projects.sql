CREATE OR REPLACE FORCE VIEW "HELIX"."CWEBV_API_PROJECTS" ("NAME", "DATAGROUP", "APPLICATION") AS
  SELECT name
 , datagroup
 ,application
 from  cwebv_api_projects@sqllims
 ;

/
QUIT
