CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BEVESTIGINGSBRIEF_VEMO_VW" ("MONS_ID", "MONSTERNUMMER", "KAFD_ID", "KAFD_CODE", "ONGR_ID", "ONGR_CODE", "DATUM_AANMELDING", "DATUM_VERWACHT", "RELA_ID", "GEADRESSEERDE", "PERS_ID", "PERSOON_INFO", "MATE_OMSCHRIJVING") AS
  SELECT mons_id
,      monsternummer
,      kafd_id
,      kafd_code
,      ongr_id
,      ongr_code
,      datum_aanmelding
,      datum_verwacht
,      rela_id
,      kgc_adres_00.relatie( rela_id, 'J' ) geadresseerde
,      pers_id
,      kgc_pers_00.persoon_info( null, null, pers_id, 'BRIEF'  ) persoon_info
,      lower(mate_omschrijving) mate_omschrijving
from   kgc_verwachte_monsters_vw
 ;

/
QUIT
