CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_SCREENINGTESTS_VW" ("STGR_CODE", "STGR_OMSCHRIJVING", "STOF_CODE", "STOF_OMSCHRIJVING", "UITSLAG", "EENHEID", "ONMO_ID") AS
  SELECT DISTINCT stgr.code stgr_code
,      stgr.omschrijving stgr_onmschrijving
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      bas_meet_00.toon_meetwaarde (meet.meet_id, 'MEET', 'WAARDE' ) uitslag
,      bas_meet_00.toon_meetwaarde (meet.meet_id, 'MEET', 'EENHEID' ) eenheid
,      meti.onmo_id
FROM   kgc_stoftestgroepen stgr
,      kgc_stoftestgroep_gebruik stgg
,      kgc_stoftesten stof
,      bas_metingen meti
,      bas_meetwaarden meet
WHERE  meti.stgr_id = stgr.stgr_id
AND    stgr.stgr_id = stgg.stgr_id
AND    stgg.screeningtest = 'J'
AND    meet.meti_id = meti.meti_id
AND    meet.stof_id = stof.stof_id
 ;

/
QUIT
