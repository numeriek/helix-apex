CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_FRAC_STOF_MEETWAARDE_VW" ("KAFD_ID", "KAFD_CODE", "FRACTIE", "STOF_CODE", "MEETWAARDE", "DATUM_METING", "ONDE_ID") AS
  SELECT kafd.kafd_id
   ,      kafd.code kafd_code
   ,      frac.fractienummer || ' (' || onde.onderzoeknr || ')' fractie
   ,      stof.code stof_code
   ,      DECODE( COUNT (meet.meet_id)
                , 1, NULL
                , '[' || TO_CHAR (COUNT (meet.meet_id)) || 'x] '
                )
       || bas_meet_00.meetwaarde( meet.stof_id, meti.onde_id, frac.mons_id, meti.frac_id ) meetwaarde
   ,      MAX (meet.datum_meting) datum_meting
   ,      meti.onde_id
   FROM   kgc_kgc_afdelingen kafd
   ,      kgc_stoftesten stof
   ,      kgc_onderzoeken onde
   ,      kgc_monsters mons
   ,      bas_fracties frac
   ,      bas_metingen meti
   ,      bas_meetwaarden meet
   WHERE  frac.mons_id = mons.mons_id
   AND    mons.kafd_id = kafd.kafd_id
   AND    frac.frac_id = meti.frac_id
   AND    meti.onde_id = onde.onde_id (+)
   AND    meti.meti_id = meet.meti_id
   AND    meet.stof_id = stof.stof_id
   GROUP BY kafd.kafd_id
   ,        kafd.code
   ,        frac.fractienummer
   ,        onde.onderzoeknr
   ,        stof.code
   ,        meet.stof_id
   ,        meti.onde_id
   ,        frac.mons_id
   ,        meti.frac_id
 ;

/
QUIT
