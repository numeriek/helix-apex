CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_WERKLIJST_MEETWAARDEN_VW" ("MEET_ID", "MWST_ID", "STOF_ID", "RUNS_ID", "METI_ID", "ONDE_ID", "ONMO_ID", "STGR_ID", "FRAC_ID", "STAN_ID", "MONS_ID", "PRIORITEIT", "MEET_REKEN", "STOF_CODE", "STOF_OMSCHRIJVING", "FRACTIENUMMER", "MONSTERNUMMER", "ONDERZOEKNR", "AFGEROND", "STGR_CODE", "STGR_OMSCHRIJVING", "COMMENTAAR", "MEETWAARDE", "GEPLANDE_EINDDATUM", "ONDERZOEKSINDICATIES", "FRTY_CODE", "FRTY_OMSCHRIJVING") AS
  SELECT meet.meet_id
,            meet.mwst_id
,            meet.stof_id
,            meet.runs_id
,            meet.meti_id
,            meti.onde_id
,            meti.onmo_id
,            meti.stgr_id
,            meti.frac_id
,            meti.stan_id
,            frac.mons_id
,            decode( onde.spoed
                   , 'J', 'J'
                   , decode(meti.prioriteit
                           ,'J','J'
                           ,meet.spoed)
                   ) spoed
,            meet.meet_reken
,            stof.code stof_code
,            stof.omschrijving stof_omschrijving
,            nvl(frac.fractienummer,stan.standaardfractienummer) fractienummer
,            nvl(mons.monsternummer, 'Controle') monsternummer
,            onde.onderzoeknr
,            decode( meet.afgerond
                   , 'J', 'J'
                   , meti.afgerond
                   ) afgerond
,            stgr.code stgr_code
,            stgr.omschrijving stgr_omschrijving
,            meet.commentaar
,            meet.meetwaarde
,            onde.geplande_einddatum
,            kgc_indi_00.onderzoeksindicaties( onde.onde_id )
,            frty.code frty_code
,            frty.omschrijving frty_omschrijving
from         bas_fractie_types frty
,            bas_standaardfracties stan
,            kgc_monsters mons
,            kgc_onderzoeken onde
,            bas_fracties frac
,            kgc_stoftestgroepen stgr
,            bas_metingen meti
,            kgc_stoftesten stof
,            bas_meetwaarden meet
where    meet.stof_id = stof.stof_id
and      meet.meti_id = meti.meti_id
and      meti.frac_id = frac.frac_id (+)
and      frac.frty_id = frty.frty_id (+)
and      frac.mons_id = mons.mons_id (+)
and      meti.stan_id = stan.stan_id (+)
and      meti.onde_id = onde.onde_id (+)
and      meti.stgr_id = stgr.stgr_id (+)
 ;

/
QUIT
