CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERZOEK_INDICATIES_VW" ("ONIN_ID", "ONDE_ID", "VOLGORDE", "INDI_CODE", "INDI_OMSCHRIJVING", "INGR_CODE", "INGR_OMSCHRIJVING", "MACHTIGINGSINDICATIE", "OPMERKING") AS
  SELECT onin.onin_id
,      onin.onde_id
,      onin.volgorde
,      indi.code indi_code
,      indi.omschrijving indi_omschrijving
,      ingr.code ingr_code
,      ingr.omschrijving ingr_omschrijving
,      ingr.machtigingsindicatie
,      onin.opmerking
from   kgc_indicatie_groepen ingr
,      kgc_indicatie_teksten indi
,      kgc_onderzoek_indicaties onin
WHERE  onin.indi_id = indi.indi_id (+)
AND    indi.ingr_id = ingr.ingr_id (+)
UNION
SELECT onin.onin_id
,      onin.onde_id
,      onin.volgorde
,      null indi_code
,      null indi_omschrijving
,      ingr.code ingr_code
,      ingr.omschrijving ingr_omschrijving
,      ingr.machtigingsindicatie
,      onin.opmerking
from   kgc_indicatie_groepen ingr
,      kgc_indicatie_teksten indi
,      kgc_onderzoek_indicaties onin
where  onin.ingr_id = ingr.ingr_id
 ;

/
QUIT
