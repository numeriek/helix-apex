CREATE OR REPLACE FORCE VIEW "HELIX"."DNA_RESULTATENLIJST_VW" ("ONDE_ID", "FRAC_ID", "MEET_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "MEET_AFGEROND", "MEET_COMMENTAAR", "TRGE_EXTERNE_ID", "WERKLIJST_NUMMER", "MEST_GOED_FOUT", "MDET_VOLGORDE", "MDET_WAARDE") AS
  SELECT meti.onde_id
,      meti.frac_id
,      meet.meet_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      decode( meti.afgerond
             ,  'J' ,  'J'
             ,  meet.afgerond
             ) meet_afgerond
,      meet.commentaar meet_commentaar
,      trge.externe_id trge_externe_id
,      kgc_wlst_00.werklijst_lijst( meet.meet_id ) werklijst_nummer
,      mest.default_goed_fout mest_goed_fout
,      mdet.volgorde  mdet_volgorde
,      mdet.prompt||': '||mdet.waarde  mdet_waarde
FROM   kgc_stoftesten stof
,      bas_meetwaarde_statussen mest
,      kgc_tray_gebruik trge
,      kgc_tray_vulling trvu
,      bas_meetwaarde_details mdet
,      bas_meetwaarden meet
,      bas_metingen meti
WHERE  meet.meti_id = meti.meti_id
and    meet.stof_id = stof.stof_id
and    stof.meet_reken <> 'V'
and    meet.mest_id = mest.mest_id (+)
and    meet.meet_id = trvu.meet_id (+)
and    trvu.trge_id = trge.trge_id (+)
and    meet.meet_id = mdet.meet_id
union all
SELECT meti.onde_id
,      meti.frac_id
,      meet.meet_id
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      decode( meti.afgerond
             ,  'J' ,  'J'
             ,  meet.afgerond
             ) meet_afgerond
,      meet.commentaar meet_commentaar
,      trge.externe_id trge_externe_id
,      kgc_wlst_00.werklijst_lijst( meet.meet_id ) werklijst_nummer
,      mest.default_goed_fout mest_goed_fout
,      1
,      null
FROM   kgc_stoftesten stof
,      bas_meetwaarde_statussen mest
,      kgc_tray_gebruik trge
,      kgc_tray_vulling trvu
,      bas_meetwaarden meet
,      bas_metingen meti
WHERE  meet.meti_id = meti.meti_id
and    meet.stof_id = stof.stof_id
and    stof.meet_reken <> 'V'
and    meet.mest_id = mest.mest_id (+)
and    meet.meet_id = trvu.meet_id (+)
and    trvu.trge_id = trge.trge_id (+)
and    not exists
       ( select null
         from bas_meetwaarde_details mdet
         where mdet.meet_id = meet.meet_id
       );

/
QUIT
