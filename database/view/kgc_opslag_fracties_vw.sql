CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_OPSLAG_FRACTIES_VW" ("TRGE_ID", "WERKLIJST_NUMMER", "MEDE_ID", "FRAC_ID", "FRACTIENUMMER", "ACHTERNAAM", "AANSPREKEN", "GEBOORTEDATUM", "GESLACHT", "OPIN_ID", "OPPO_CODE", "OPEL_CODE", "OPAT_ID", "VOLGNR") AS
  SELECT DISTINCT tvuv.trge_id
,      tvuv.werklijst_nummer
,      tvuv.mede_id
,      tvuv.frac_id
,      tvuv.fractienummer
,      tvuv.pers_achternaam
,      tvuv.pers_aanspreken
,      tvuv.pers_geboortedatum
,      tvuv.pers_geslacht
,      opin.opin_id
,      kgc_opin_00.get_oppo_code(opin.oppo_id) oppo_code
,      kgc_opin_00.get_opel_code(opin.oppo_id) opel_code
,      opat.opat_id
,      opat.volgnr
FROM kgc_tray_vulling_vw   tvuv
,    kgc_opslag_inhoud     opin
,    kgc_opslag_attributen opat
WHERE opat.opat_id = opin.opat_id(+)
AND   tvuv.frac_id = opat.helix_id(+)
 ;

/
QUIT
