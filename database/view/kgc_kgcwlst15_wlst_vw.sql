CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCWLST15_WLST_VW" ("WLST_ID", "KAFD_ID", "WERKLIJST_NUMMER", "TYPE_WERKLIJST", "ORACLE_UID", "DATUM", "ARCHIEF", "SPOED_BOVENAAN", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "GESORTEERD", "ALLES_AFGEROND") AS
  SELECT wlst.wlst_id
,wlst.kafd_id
,wlst.werklijst_nummer
,wlst.type_werklijst
,wlst.oracle_uid
,wlst.datum
,wlst.archief
,wlst.spoed_bovenaan
,wlst.created_by
,wlst.creation_date
,wlst.last_updated_by
,wlst.last_update_date
,wlst.gesorteerd
-- mantis 3381 ,kgc_wlst_00.alles_afgerond(wlst.wlst_id) alles_afgerond
, NVL( (SELECT 'N' -- 1 of meer WLIT-rijen/onde.afgerond=N
        FROM DUAL
        WHERE EXISTS
          (SELECT onde.onde_id
           FROM   kgc_onderzoeken onde
           ,      bas_metingen meti
           ,      bas_meetwaarden meet
           ,      kgc_werklijst_items wlit      -- WLIT: werklijsten die NIET gearchiveerd zijn.
         --,      kgc_werklijst_items_archief wlia WLIA: werklijsten die WEL gearchiveerd zijn.
           WHERE  wlit.meet_id = meet.meet_id
           AND    meet.meti_id = meti.meti_id
           AND    meti.onde_id = onde.onde_id
           AND    onde.afgerond = 'N'
           AND    wlit.wlst_id = wlst.wlst_id
           AND    wlst.archief = 'N')
       ), 'J' -- alle WLIT-rijen/onde.afgerond=J en alle WLIA-rijen (worden beschouwd te zijn afgerond)
     ) alles_afgerond
FROM kgc_werklijsten wlst
 ;

/
QUIT
