CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCVERZ53_VW" ("GEADRESSEERDE", "REFERENTIE", "BETREFT", "ONDERTEKENAAR", "VERZOEKTEKST", "KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "KAFD_ID", "ONGR_ID", "OBMG_ID", "EERDER_GEPRINT", "GEPRINT_OP", "BRIEFDATUM") AS
  SELECT geadresseerde
,      referentie
,      betreft
,      ondertekenaar
,      verzoektekst
,      kafd_code
,      ongr_code
,      onderzoeknr
,      kafd_id
,      ongr_id
,      obmg_id
,      kgc_brie_00.verzoekmg_reeds_geprint( verz.obmg_id )
,      kgc_brie_00.verzoekmg_geprint_op( verz.obmg_id )
,      TO_CHAR( SYSDATE, 'DD-MM-YYYY' )
FROM   kgc_verzoekbrief_mg_vw  verz
 ;

/
QUIT
