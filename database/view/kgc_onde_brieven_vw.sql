CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDE_BRIEVEN_VW" ("ONDE_ID", "BRIE_ID", "KAFD_ID", "PERS_ID", "DATUM_PRINT", "KOPIE", "BRTY_ID", "RELA_ID", "UITS_ID", "ZWAN_ID", "DATUM_VERZONDEN", "GEADRESSEERDE", "DECL_ID", "AANMAAKWIJZE", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE") AS
  SELECT brie.onde_id,
          brie.brie_id,
          brie.kafd_id,
          brie.pers_id,
          brie.datum_print,
          brie.kopie,
          brie.brty_id,
          brie.rela_id,
          brie.uits_id,
          brie.zwan_id,
          brie.datum_verzonden,
          brie.geadresseerde,
          brie.decl_id,
          brie.aanmaakwijze,
          brie.created_by,
          brie.creation_date,
          brie.last_updated_by,
          brie.last_update_date
     FROM kgc_brieven brie
    WHERE brie.onde_id IS NOT NULL AND brie.pers_id IS NOT NULL
   UNION ALL
   SELECT zwan.onde_id,
          brie.brie_id,
          brie.kafd_id,
          brie.pers_id,
          brie.datum_print,
          brie.kopie,
          brie.brty_id,
          brie.rela_id,
          brie.uits_id,
          brie.zwan_id,
          brie.datum_verzonden,
          brie.geadresseerde,
          brie.decl_id,
          brie.aanmaakwijze,
          brie.created_by,
          brie.creation_date,
          brie.last_updated_by,
          brie.last_update_date
     FROM kgc_brieven brie, kgc_prenataal_zwan_onde_vw zwan
    WHERE     brie.onde_id IS NULL
          AND brie.pers_id IS NOT NULL
          AND zwan.pers_id = brie.pers_id
          AND zwan.onde_id IS NOT NULL;

/
QUIT
