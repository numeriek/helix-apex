CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_ROBOT_METINGEN_VW" ("METI_ID", "ONMO_ID", "FRAC_ID", "MONS_ID", "ONDE_ID", "PROJ_ID", "MATE_ID", "STGR_ID", "PERS_ID", "KAFD_ID", "DATUM_SELECTIE_ROBOT", "TRUNC_DATUM_SELECTIE_ROBOT", "PRIORITEIT", "MONSTERNUMMER", "MONS_DATUM_AFNAME", "ONDERZOEKNR", "ONDERZOEKSTYPE", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_AANSPREKEN", "GESELECTEERD", "STGR_CODE") AS
  SELECT meti.meti_id
,      meti.onmo_id
,      meti.frac_id
,      onmo.mons_id
,      onmo.onde_id
,      onde.proj_id
,      mons.mate_id
,      meti.stgr_id
,      mons.pers_id
,      mons.kafd_id
,      meti.datum_selectie_robot
,      trunc(meti.datum_selectie_robot)
,      meti.prioriteit
,      mons.monsternummer
,      mons.datum_afname
,      onde.onderzoeknr
,      onde.onderzoekstype
,      pers.achternaam
,      pers.geboortedatum
,      pers.aanspreken
,      DECODE( meti.datum_selectie_robot, NULL, 'N', 'J' ) geselecteerd
,      stgr.code stgr_code
from   kgc_personen pers
,      kgc_stoftestgroepen stgr
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
,      bas_fracties frac
,      bas_metingen meti
where  meti.onmo_id = onmo.onmo_id
and    meti.frac_id = frac.frac_id
and    onmo.mons_id = mons.mons_id
and    onmo.onde_id = onde.onde_id
and    mons.pers_id = pers.pers_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.afgerond = 'N'
and    onde.afgerond = 'N'
AND    EXISTS
       ( SELECT '1'
         FROM bas_meetwaarden meet
         WHERE meet.meti_id = meti.meti_id
         AND meet.afgerond = 'N'
       );

/
QUIT
