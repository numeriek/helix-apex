CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCONDE20_MEETWAARDEN_VW" ("MEET_ID", "METI_ID", "STOF_ID", "STGR_ID", "FRAC_ID", "STAN_ID", "MONS_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "PERS_ID", "MEST_ID", "DATUM_AANMELDING", "METI_CONCLUSIE", "MEET_REKEN", "DATUM_METING", "STOF_CODE", "STOF_OMSCHRIJVING", "MEETWAARDE", "EENHEID", "FRACTIENUMMER", "FRACTIESTATUS", "MONSTERNUMMER", "ONDERZOEKNR", "ONDERZOEKSTATUS", "AFGEROND", "COMMENTAAR", "MEST_CODE", "MEST_OMSCHRIJVING", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "DATUM_AFNAME", "MATE_CODE", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "WERKELIJKE_MEETWAARDE", "WERKLIJST_NUMMER") AS
  SELECT meet.meet_id ,
  meet.meti_id ,
  meet.stof_id ,
  meti.stgr_id ,
  meti.frac_id ,
  meti.stan_id ,
  frac.mons_id ,
  mons.kafd_id ,
  mons.ongr_id ,
  meti.onde_id ,
  mons.pers_id ,
  meet.mest_id ,
  meti.datum_aanmelding ,
  to_char(meti.conclusie) meti_conclusie ,  --TO_CHAR added by Anuja for MANTIS 9355 on 8/4/2016 for release 8.11.0.3
  meet.meet_reken ,
  meet.datum_meting ,
  stof.code stof_code ,
  stof.omschrijving stof_omschrijving ,
  meet.tonen_als  meetwaarde,
  meet.meeteenheid eenheid ,
  frac.fractienummer ,
  frac.status fractiestatus ,
  mons.monsternummer ,
  onde.onderzoeknr ,
  onde.status onderzoekstatus ,
  decode( meet.afgerond
        , 'J',    'J'
        , meti.afgerond
        ) afgerond ,
  meet.commentaar ,
  mest.code mest_code ,
  mest.omschrijving mest_omschrijving ,
  meet.created_by ,
  meet.creation_date ,
  meet.last_updated_by ,
  meet.last_update_date ,
  mons.datum_afname  datum_afname,
  mate.code  mate_code,
  meet.normaalwaarde_ondergrens  normaalwaarde_ondergrens,
  meet.normaalwaarde_bovengrens  normaalwaarde_bovengrens,
  meet.meetwaarde werkelijke_meetwaarde,
 werk.werklijst_nummer  werklijst_nummer
FROM
  kgc_materialen mate ,
  kgc_monsters mons ,
  kgc_onderzoeken onde ,
  bas_fracties frac ,
  bas_meetwaarde_statussen mest ,
  bas_metingen meti ,
  kgc_stoftesten stof ,
  bas_meetwaarden meet ,
  kgc_werklijsten werk,
  kgc_werklijst_items werkl
WHERE
  meet.stof_id = stof.stof_id and
  meet.meti_id = meti.meti_id and
  meti.onde_id = onde.onde_id (+) and
  meti.frac_id = frac.frac_id and
  frac.mons_id = mons.mons_id and
  meet.mest_id = mest.mest_id (+) and
  mons.mate_id = mate.mate_id and
  meet.meet_id = werkl.meet_id (+) and
  werk.wlst_id(+)  = werkl.wlst_id
union  all
SELECT -- meerdere fracties bij de meting
  meet.meet_id ,
  meet.meti_id ,
  meet.stof_id ,
  meti.stgr_id ,
  meti.frac_id ,
  meti.stan_id ,
  onmo.mons_id ,
  mons.kafd_id ,
  mons.ongr_id ,
  meti.onde_id ,
  mons.pers_id ,
  meet.mest_id ,
  meti.datum_aanmelding ,
  to_char(meti.conclusie) meti_conclusie ,   --TO_CHAR added by Anuja for MANTIS 9355 on 8/4/2016 for release 8.11.0.3
  meet.meet_reken ,
  meet.datum_meting ,
  stof.code stof_code ,
  stof.omschrijving stof_omschrijving ,
  meet.tonen_als meetwaarde ,
  meet.meeteenheid eenheid ,
  'FRACTIES' , -- fractienummmer
  null , -- fractiestatus
  mons.monsternummer ,
  onde.onderzoeknr ,
  onde.status onderzoekstatus ,
  decode( meet.afgerond ,
  'J',
  'J' ,
  meti.afgerond ) afgerond ,
  meet.commentaar ,
  mest.code mest_code ,
  mest.omschrijving mest_omschrijving ,
  meet.created_by ,
  meet.creation_date ,
  meet.last_updated_by ,
  meet.last_update_date ,
  mons.datum_afname  datum_afname,
  mate.code  mate_code,
  meet.normaalwaarde_ondergrens  normaalwaarde_ondergrens,
  meet.normaalwaarde_bovengrens  normaalwaarde_bovengrens,
  meet.meetwaarde werkelijke_meetwaarde,
 werk.werklijst_nummer werklijst_nummer
FROM
  kgc_materialen mate ,
  kgc_monsters mons ,
  kgc_onderzoeken onde ,
  kgc_onderzoek_monsters onmo ,
  bas_meetwaarde_statussen mest ,
  bas_metingen meti ,
  kgc_stoftesten stof ,
  bas_meetwaarden meet  ,
  kgc_werklijsten werk,
   kgc_werklijst_items werkl
WHERE
  meet.stof_id = stof.stof_id and
  meet.meti_id = meti.meti_id and
  meti.onmo_id = onmo.onmo_id and
  onmo.onde_id = onde.onde_id and
  onmo.mons_id = mons.mons_id and
  meet.mest_id = mest.mest_id (+) and
  meti.frac_id is null   and
  mons.mate_id = mate.mate_id and
 meet.meet_id = werkl.meet_id (+) and
  werk.wlst_id(+)  = werkl.wlst_id
union all
SELECT
  meet.meet_id ,
  meet.meti_id ,
  meet.stof_id ,
  meti.stgr_id ,
  meti.frac_id ,
  meti.stan_id ,
  to_number(null) ,
  ongr.kafd_id ,
  stan.ongr_id ,
  meti.onde_id ,
  to_number(null) ,
  meet.mest_id ,
  meti.datum_aanmelding ,
 to_char(meti.conclusie) meti_conclusie ,   --TO_CHAR added by Anuja for MANTIS 9355 on 8/4/2016 for release 8.11.0.3
  meet.meet_reken ,
  meet.datum_meting ,
  stof.code ,
  stof.omschrijving ,
  meet.tonen_als  meetwaarde ,
  meet.meeteenheid ,
  stan.standaardfractienummer ,
  'O' ,
  null ,
  null ,
  'G' ,
  decode( meet.afgerond ,
  'J',
  'J' ,
  meti.afgerond ) afgerond ,
  meet.commentaar ,
  mest.code ,
  mest.omschrijving ,
  meet.created_by ,
  meet.creation_date ,
  meet.last_updated_by ,
  meet.last_update_date ,
  TO_DATE(NULL) datum_afname,
  mate.code  mate_code,
  meet.normaalwaarde_ondergrens  normaalwaarde_ondergrens,
  meet.normaalwaarde_bovengrens  normaalwaarde_bovengrens,
  meet.meetwaarde werkelijke_meetwaarde,
 werk.werklijst_nummer werklijst_nummer
FROM
  kgc_materialen mate ,
  kgc_onderzoeksgroepen ongr ,
  bas_standaardfracties stan ,
  bas_meetwaarde_statussen mest ,
  bas_metingen meti ,
  kgc_stoftesten stof ,
  bas_meetwaarden meet  ,
  kgc_werklijsten werk,
  kgc_werklijst_items werkl
WHERE
  stan.ongr_id = ongr.ongr_id and
  meet.stof_id = stof.stof_id and
  meet.meti_id = meti.meti_id and
  meti.stan_id = stan.stan_id and
  meet.mest_id = mest.mest_id (+) and
  stan.mate_id = mate.mate_id and
  meet.meet_id = werkl.meet_id (+) and
  werk.wlst_id (+) = werkl.wlst_id;

/
QUIT
