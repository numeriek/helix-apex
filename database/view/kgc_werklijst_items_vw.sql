CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_WERKLIJST_ITEMS_VW" ("WLST_ID", "WERKLIJST_NUMMER", "VOLGNUMMER", "MEET_ID", "AFGEROND", "STOF_ID", "ONGR_ID", "MONS_ID", "MEDE_ID_CONTROLE", "MEST_ID", "MEETWAARDE", "COMMENTAAR") AS
  SELECT wlit.wlst_id
,wlst.werklijst_nummer
,wlit.volgnummer
,wlit.meet_id
,wlit.afgerond
,wlit.stof_id
,wlit.ongr_id
,wlit.mons_id
,wlit.mede_id_controle
,wlit.mest_id
,wlit.meetwaarde
,wlit.commentaar
from kgc_werklijsten wlst
, kgc_werklijst_items wlit
where wlit.wlst_id = wlst.wlst_id
union all
select
 wlia.wlst_id
, wlst.werklijst_nummer
,wlia.volgnummer
,wlia.meet_id
,wlia.afgerond
,wlia.stof_id
,wlia.ongr_id
,wlia.mons_id
,wlia.mede_id_controle
,wlia.mest_id
,wlia.meetwaarde
,wlia.commentaar
from kgc_werklijsten wlst
, kgc_werklijst_items_archief wlia
where wlia.wlst_id = wlst.wlst_id
 ;

/
QUIT
