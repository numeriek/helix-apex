CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_FAMILIE_LEDEN_VW" ("FALE_ID", "FAMI_ID", "PERS_ID", "FAMILIENUMMER", "FAMILIENAAM", "PERSOON", "ACHTERNAAM", "GEBOORTEDATUM", "GESLACHT", "ZISNR", "ADRES", "POSTCODE", "WOONPLAATS", "LAND") AS
  SELECT fale.fale_id
  ,    fale.fami_id
,      fale.pers_id
,      fami.familienummer
,      fami.naam familienaam
,      pers.aanspreken persoon
,      pers.achternaam
,      pers.geboortedatum
,      pers.geslacht
,      pers.zisnr
,      pers.adres
,      pers.postcode
,      pers.woonplaats
,      pers.land
from   kgc_families fami
,      kgc_personen pers
,      kgc_familie_leden fale
where fale.fami_id = fami.fami_id
and    fale.pers_id = pers.pers_id;

/
QUIT
