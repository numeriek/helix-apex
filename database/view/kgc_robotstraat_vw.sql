CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ROBOTSTRAAT_VW" ("TRVU_ID", "MEET_ID", "TRGE_ID", "TRTY_ID", "WERKLIJST_NUMMER", "X_POSITIE", "Y_POSITIE", "Z_POSITIE", "WELL_NUMMER", "WELL_NUMMER_RESHUFFLED_3730", "FRACTIENUMMER", "STOF_CODE", "STOF_OMSCHRIJVING", "SAMPLENAAM") AS
  SELECT trvu.trvu_id
,      trvu.meet_id
,      trvu.trge_id
,      trge.trty_id
,      trge.werklijst_nummer
,      trvu.x_positie
,      trvu.y_positie
,      trvu.z_positie
,      trvu.x_positie||trvu.y_positie well_nummer
,      reshuffle_3730( trvu.trvu_id) well_nummer_reshuffle_3730
,      nvl(frac.fractienummer,stan.standaardfractienummer)  fractienummer
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      samp.naam samp_naam
--,      kgc_attr_00.waarde('KGC_STOFTESTEN','BANDSIZE',stof.stof_id) bandsize VERVALLEN
from   kgc_stoftesten stof
,      bas_standaardfracties stan
,      kgc_monsters mons
,      bas_fracties frac
,      kgc_samples samp
,      bas_metingen meti
,      bas_meetwaarden meet
,      kgc_tray_gebruik trge
,      kgc_tray_vulling trvu
where  trvu.trge_id = trge.trge_id
and    trvu.meet_id = meet.meet_id
and    meet.stof_id = stof.stof_id
and    meet.meet_id = samp.meet_id (+)
and    meet.meti_id = meti.meti_id
and    meti.frac_id = frac.frac_id (+)
and    frac.mons_id = mons.mons_id (+)
and    meti.stan_id = stan.stan_id (+)
 ;

/
QUIT
