CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_CATEGORIE_VOLGORDE_VW" ("CATE_LEVEL", "OMSCHRIJVING", "CATE_ID", "VOLGORDE") AS
  SELECT level
, omschrijving
, cate_id
, volgorde
FROM   kgc_categorieen
START WITH  cate_id = kgc_variabelen.get_var_number( 'pv_cate_id' )
CONNECT BY PRIOR cate_id = cate_id_parent
ORDER SIBLINGS BY volgorde
 ;

/
QUIT
