CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_IM_PERSOON_LOCATIES_VW" ("IMPE_ID", "ACHTERNAAM", "GEBOORTEDATUM", "ZISNR", "ADRES", "POSTCODE", "WOONPLAATS", "IMFI_ID", "IMLO_ID", "DIRECTORYNAAM", "OMSCHRIJVING", "ZISNR_OVERNEMEN", "SELECTIE_GROEP", "FILENAAM", "IMPORT_DATUM", "AFGEHANDELD") AS
  SELECT impe.impe_id
,      impe.achternaam
,      impe.geboortedatum
,      impe.zisnr
,      impe.adres
,      impe.postcode
,      impe.woonplaats
,      impe.imfi_id
,      imfi.imlo_id
,      imlo.directorynaam
,      imlo.omschrijving
,      imlo.zisnr_overnemen
,      imlo.selectie_groep
,      imfi.filenaam
,      imfi.import_datum
,      imfi.afgehandeld
from   kgc_import_locaties imlo
,      kgc_import_files imfi
,      kgc_im_personen impe
where impe.imfi_id = imfi.imfi_id
and    imfi.imlo_id = imlo.imlo_id
 ;

/
QUIT
