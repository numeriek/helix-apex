CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_IUF_VW" ("KAFD_CODE", "ONGR_CODE", "ONDERZOEKNR", "ONDE_AFGEROND", "RELA_AANSPREKEN", "PERS_AANSPREKEN", "PERS_GESLACHT", "PERS_GEBOORTEDATUM", "INDI_CODE_ONDE", "INDI_CODE_MONS", "MONSTERNUMMER", "MATE_OMSCHRIJVING", "COND_OMSCHRIJVING", "MONS_DATUM_AANMELDING", "MONS_DATUM_AFNAME", "MONS_TIJD_AFNAME", "STGR_OMSCHRIJVING", "STOF_OMSCHRIJVING", "MEETWAARDE", "NORMAALWAARDEN", "METI_RESULTAAT", "WERKLIJST_LIJST", "ONDE_ID", "MONS_ID", "PERS_ID", "ONGR_ID", "KAFD_ID", "MEET_ID") AS
  SELECT kafd.code kafd_code
,      ongr.code ongr_code
,      onde.onderzoeknr
,      onde.afgerond onde_afgerond
,      rela.aanspreken rela_aanspreken
,      pers.aanspreken pers_aanspreken
,      pers.geslacht pers_geslacht
,      pers.geboortedatum pers_geboortedatum
,      kgc_indi_00.indi_code( onde.onde_id ) indi_code_onde
,      kgc_indi_00.mons_indi_code( mons.mons_id ) indi_code_mons
,      mons.monsternummer
,      mate.omschrijving mate_omschrijving
,      cond.omschrijving cond_omschrijving
,      mons.datum_aanmelding mons_datum_aanmelding
,      mons.datum_afname mons_datum_afname
,      to_char(mons.tijd_afname,'hh24:mi') mons_tijd_afname
,      stgr.omschrijving stgr_omschrijving
,      stof.omschrijving stof_omschrijving
,      meet.tonen_als||decode( meet.meeteenheid, null, null, ' '||meet.meeteenheid ) meetwaarde
,      bas_meet_00.normaalwaarden( meet.meet_id ) normaalwaarden
,      to_char(meti.conclusie) meti_resultaat  --TO_CHAR added by Anuja for MANTIS 9355 on 30-03-2016 for Release 8.11.0.2
,      kgc_wlst_00.werklijst_lijst( meet.meet_id, meet.meti_id ) werklijst_lijst
,      meti.onde_id
,      mons.mons_id
,      mons.pers_id
,      onde.ongr_id
,      ongr.kafd_id
,      meet.meet_id
from   kgc_kgc_afdelingen kafd
,      kgc_onderzoeksgroepen ongr
,      kgc_condities cond
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      kgc_relaties rela
,      kgc_personen pers
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
where meet.stof_id = stof.stof_id
and    meet.meti_id = meti.meti_id
and    meti.onde_id = onde.onde_id
and    meti.stgr_id = stgr.stgr_id (+)
and    onde.rela_id = rela.rela_id
and    meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
and    mons.cond_id = cond.cond_id (+)
and    mons.pers_id = pers.pers_id
and    onde.ongr_id = ongr.ongr_id
and    ongr.kafd_id = kafd.kafd_id;

/
QUIT
