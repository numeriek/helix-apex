CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_MEET_VOOR_VERVOLG_VW" ("ONGR_CODE", "ONDERZOEKNR", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "ONDE_STATUS", "FAMILIENUMMER", "INDI_CODE", "INDI_OMSCHRIJVING", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "FRACTIENUMMER", "FRAC_STATUS", "STGR_CODE", "SPOED", "STOF_CODE", "STOF_OMSCHRIJVING", "MEDE_CODE", "AFGEROND", "MEET_ID", "STOF_ID", "METI_ID", "ONMO_ID", "ONDE_ID", "FRAC_ID", "STAN_ID", "STGR_ID", "MONS_ID", "MATE_ID", "PERS_ID", "KAFD_ID", "ONGR_ID") AS
  SELECT ongr.code ongr_code
,      onde.onderzoeknr
,      onde.onderzoekstype
,      onde.onderzoekswijze
,      onde.status onde_status
,      kgc_fami_00.familie_bij_onderzoek(onde.onde_id, 'N') familienummer
,      kgc_indi_00.indi_code( onde.onde_id ) indi_code
,      kgc_indi_00.indi_omschrijving( onde.onde_id ) indi_omschrijving
,      mons.monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      frac.fractienummer
,      frac.status frac_status
,      stgr.code stgr_code
,      decode( meti.prioriteit
             , 'J', 'J'
             , onde.spoed
             ) spoed
,      stof.code stof_code
,      stof.omschrijving stof_omschrijving
,      mede.code mede_code
,      decode( meet.afgerond
             , 'J', 'J'
             , meti.afgerond
             ) afgerond
,      meet.meet_id
,      meet.stof_id
,      meet.meti_id
,      meti.onmo_id
,      meti.onde_id
,      meti.frac_id
,      meti.stan_id
,      meti.stgr_id
,      frac.mons_id
,      mons.mate_id
,      mons.pers_id
,      mons.kafd_id
,      mons.ongr_id
from   kgc_onderzoeksgroepen ongr
,      kgc_medewerkers mede
,      kgc_materialen mate
,      kgc_stoftestgroepen stgr
,      kgc_stoftesten stof
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,      bas_meetwaarden meet
where meet.stof_id = stof.stof_id
and    meet.mede_id = mede.mede_id (+)
and    meet.meti_id = meti.meti_id
and    meti.stgr_id = stgr.stgr_id (+)
and    meti.onde_id = onde.onde_id (+)
and    meti.frac_id = frac.frac_id
and    frac.mons_id = mons.mons_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mate_id = mate.mate_id
and    nvl(onde.afgerond,'N') = 'N'
union all -- fracties, zonder aangemelde stoftesten
select  ongr.code ongr_code
,      onde.onderzoeknr
,      onde.onderzoekstype
,      onde.onderzoekswijze
,      onde.status onde_status
,      kgc_fami_00.familie_bij_onderzoek(onde.onde_id, 'N')
,      kgc_indi_00.indi_code( onde.onde_id )
,      kgc_indi_00.indi_omschrijving( onde.onde_id )
,      mons.monsternummer
,      mate.code
,      mate.omschrijving
,      frac.fractienummer
,      frac.status
,      null  stgr_code
,      onde.spoed
,      null stof_code
,      null --  '<nog niets aangemeld>'
,      null mede_code
,      'N' afgerond
,      to_number(null) meet_id
,      to_number(null) stof_id
,      to_number(null) meti_id
,      onmo.onmo_id
,      onde.onde_id
,      frac.frac_id
,      to_number(null) stan_id
,      to_number(null) stgr_id
,      frac.mons_id
,      mons.mate_id
,      mons.pers_id
,      mons.kafd_id
,      mons.ongr_id
from   kgc_onderzoeksgroepen ongr
,      kgc_materialen mate
,      kgc_onderzoeken onde
,      kgc_monsters mons
,      bas_fracties frac
,      kgc_onderzoek_monsters onmo
where onmo.onde_id = onde.onde_id
and    onmo.mons_id = mons.mons_id
and    frac.mons_id = mons.mons_id
and    mons.ongr_id = ongr.ongr_id
and    mons.mate_id = mate.mate_id
and    nvl(onde.afgerond,'N') = 'N'
and    not exists
           ( select null
              from   bas_meetwaarden meet
             ,            bas_metingen meti
             where meti.meti_id = meet.meti_id (+)
             and     meti.frac_id = frac.frac_id
             and     meti.onde_id = onde.onde_id
           );

/
QUIT
