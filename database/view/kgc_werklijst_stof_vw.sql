CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_WERKLIJST_STOF_VW" ("WLST_ID", "STOF_ID", "STOF_CODE", "STOF_OMSCHRIJVING", "STOF_TYPE") AS
  SELECT distinct
    wlit.wlst_id
  , wlit.stof_id
  , stof.code stof_code
  , stof.omschrijving stof_omschrijving
  , stof.meet_reken stof_type
from kgc_stoftesten stof
,    kgc_werklijst_items wlit
where wlit.stof_id = stof.stof_id;

/
QUIT
