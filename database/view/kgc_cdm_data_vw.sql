CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_CDM_DATA_VW" ("CEL", "KWEEKFRACTIE", "TEKST", "METI_ID", "MEET_ID") AS
  SELECT kary.cel
,      kwfr.code kweekfractie
,      nvl( kary.tekst_chromosomen, meet.commentaar ) tekst
,      meet.meti_id
,      meet.meet_id
from   kgc_kweekfracties kwfr
,      kgc_karyogrammen kary
,      bas_meetwaarden meet
where  meet.meet_id = kary.meet_id
and    kary.kwfr_id = kwfr.kwfr_id
 ;

/
QUIT
