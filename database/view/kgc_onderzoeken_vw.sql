CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERZOEKEN_VW" ("PERS_ID", "PERS_AANSPREKEN", "PERS_ZISNR", "PERS_ACHTERNAAM", "PERS_GEBOORTEDATUM", "PERS_GESLACHT", "PERS_ADRES", "PERS_ZOEKNAAM", "PERS_POSTCODE", "PERS_TELEFOON", "PERS_TELEFAX", "PERS_EMAIL", "PERS_WOONPLAATS", "PERS_OVERLEDEN", "PERS_OVERLIJDENSDATUM", "FOET_ID", "ONDE_ID", "ONDERZOEKSTYPE", "ONDERZOEKSWIJZE", "SPOED", "ONDERZOEKNR", "DATUM_BINNEN", "GEPLANDE_EINDDATUM", "REFERENTIE", "INDI_CODE", "INDICATIE", "REDEN", "KARIOTYPERING", "AFGEROND", "DATUM_AUTORISATIE", "AUTORISATOR_CODE", "AUTORISATOR_NAAM", "STATUS", "MEDE_ID_CONTROLE", "ONGR_ID", "ONGR_CODE", "ONGR_OMSCHRIJVING", "RELA_ID", "RELA_CODE", "RELA_NAAM", "KAFD_ID", "KAFD_CODE", "KAFD_NAAM", "TAAL_ID", "TAAL_CODE", "RELA_CODE_OORSPRONG", "RELA_NAAM_OORSPRONG", "BSN_OPGEMAAKT", "BSN_GEVERIFIEERD") AS
  SELECT onde.pers_id ,
  pers.aanspreken pers_aanspreken ,
  pers.zisnr pers_zisnr ,
  pers.achternaam pers_achternaam ,
  pers.geboortedatum pers_geboortedatum ,
  pers.geslacht pers_geslacht ,
  kgc_adres_00.persoon ( pers.pers_id, 'N' ) pers_adres ,
  pers.zoeknaam pers_zoeknaam ,
  pers.postcode pers_postcode ,
  pers.telefoon pers_telefoon ,
  pers.telefax pers_telefax ,
  pers.email pers_email ,
  pers.woonplaats ,
  pers.overleden,--Added for Mantis 4740 ,ASI
  pers.overlijdensdatum,--Added for Mantis 4740 ,ASI
  onde.foet_id ,
  onde.onde_id ,
  onde.onderzoekstype ,
  onde.onderzoekswijze ,
  onde.spoed ,
  onde.onderzoeknr onderzoek ,
  onde.datum_binnen ,
  onde.geplande_einddatum ,
  onde.referentie ,
  kgc_indi_00.indi_code( onde.onde_id ) indi_code ,
  kgc_indi_00.onderzoeksreden( onde.onde_id ) indicatie ,
  onde.omschrijving reden ,
  onde.kariotypering ,
  onde.afgerond,
  onde.datum_autorisatie,
  decode( onde.mede_id_autorisator, NULL , NULL ,kgc_mede_00.medewerker_code( onde.mede_id_autorisator )) autorisator_code,
  decode( onde.mede_id_autorisator, NULL , NULL ,kgc_mede_00.medewerker_naam( onde.mede_id_autorisator )) autorisator_naam,
  onde.status ,
  onde.mede_id_controle ,
  onde.ongr_id ,
  ongr.code ongr_code ,
  ongr.omschrijving ongr_omschrijving ,
  onde.rela_id ,
  rela.code rela_code ,
  rela.aanspreken rela_aanspreken ,
  onde.kafd_id ,
  kafd.code kafd_code ,
  kafd.naam kafd_naam ,
  onde.taal_id ,
  taal.code taal_code,
  rela2.code rela_code_oorsprong,
  rela2.aanspreken rela_naam_oorsprong,
  to_char(pers.bsn, 'FM099999999' ) bsn_opgemaakt,
  pers.bsn_geverifieerd bsn_geverifieerd
FROM
  kgc_talen taal,
  kgc_kgc_afdelingen kafd ,
  kgc_onderzoeksgroepen ongr ,
  kgc_relaties rela ,
  kgc_personen pers ,
  kgc_onderzoeken onde,
  kgc_relaties rela2
where onde.pers_id = pers.pers_id
and      onde.rela_id = rela.rela_id
and      onde.kafd_id = kafd.kafd_id
and      onde.ongr_id = ongr.ongr_id
and      onde.taal_id = taal.taal_id (+)
and      onde.rela_id_oorsprong = rela2.rela_id (+);

/
QUIT
