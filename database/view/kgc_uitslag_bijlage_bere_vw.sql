CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_UITSLAG_BIJLAGE_BERE_VW" ("CAWA_VOLGORDE", "CAWA_BESCHRIJVING", "UITSLAG", "NORMAALWAARDEN", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "NORMAALWAARDE_GEMIDDELDE", "EENHEID", "MEET_ID", "STOF_ID", "ONDE_ID", "ONMO_ID", "CATE_ID") AS
  SELECT -- werkelijk gebruikte categorie-waarden
       cawa.volgorde cawa_volgorde
,      nvl( kgc_vert_00.vertaling( 'CAWA', cawa.cawa_id, null, onde.taal_id ), cawa.beschrijving ) cawa_beschrijving
,      decode( bere.meet_id
             , null, bas_meet_00.formatteer(bere.meet_id,meet.meetwaarde)
             , to_char(bas_bere_00.formatteer(bere.bere_id, bere.uitslag) )
             )  uitslag
,      decode( bere.meet_id
             , null, bas_meet_00.normaalwaarden( meet.meet_id, 'N', stof.aantal_decimalen )  -- stof.aantal_decimalen Added by Athar Shaikh for Mantis-3195 on 06-04-2016
             , bas_bere_00.normaalwaarden( bere.bere_id, stof.aantal_decimalen )  -- stof.aantal_decimalen Added by Athar Shaikh for Mantis-3195 on 06-04-2016
             ) normaalwaarden
,      decode( bere.meet_id
             , null, to_char(meet.normaalwaarde_ondergrens)
             , to_char(bere.normaalwaarde_ondergrens)
             ) normaalwaarde_ondergrens
,      decode( bere.meet_id
             , null, to_char(meet.normaalwaarde_bovengrens)
             , to_char(bere.normaalwaarde_bovengrens)
             ) normaalwaarde_bovengrens
,      decode( bere.meet_id
             , null, to_char(meet.normaalwaarde_gemiddelde)
             , to_char(bere.normaalwaarde_gemiddelde)
             ) normaalwaarde_gemiddelde
,      decode( bere.meet_id
             , null, meet.meeteenheid
             , bere.uitslageenheid
             ) eenheid
,      meet.meet_id
,      meet.stof_id
,      meti.onde_id
,      meti.onmo_id
,      cawa.cate_id
from   bas_meetwaarde_statussen mest
,      kgc_stoftesten stof
,      kgc_onderzoeken onde
,      bas_metingen meti
,      bas_meetwaarden meet
,      bas_berekeningen bere
,      kgc_categorie_waarden cawa
WHERE  meti.onde_id = onde.onde_id
and    meti.meti_id = meet.meti_id
and    meet.stof_id = stof.stof_id
and    meet.meet_id = bere.meet_id (+)
and    nvl( bere.in_uitslag, 'J' ) = 'J'
and    meet.mest_id = mest.mest_id (+)
and    nvl( mest.in_uitslag, 'J' ) = 'J'
and    cawa.waarde = stof.code
union all -- altijd getoonde categorie-waarden
SELECT cawa.volgorde
,      nvl( kgc_vert_00.vertaling( 'CAWA', cawa.cawa_id, null, onde.taal_id ), cawa.beschrijving )
,      '-' uitslag
,      bas_rang_00.normaalwaarden
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       , stof.aantal_decimalen -- stof.aantal_decimalen Added by Athar Shaikh for Mantis-3195 on 06-04-2016
       )
,      bas_rang_00.laag
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       )
,      bas_rang_00.hoog
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       )
,      bas_rang_00.gemiddeld
       ( onde.kafd_id
       , mons.mate_id
       , nvl( mons.leeftijd, mons.datum_afname - pers.geboortedatum )
       , stof.stof_id
       , null -- p_stgr_id
       , null -- p_frty_id
       , null -- p_bety_id
       , pers.geslacht
       )
,      stof.eenheid
,      to_number(null) meet_id
,      stof.stof_id
,      onde.onde_id
,      onmo.onmo_id
,      cawa.cate_id
from   kgc_stoftesten stof
,      kgc_personen pers
,      kgc_monsters mons
,      kgc_onderzoeken onde
,      kgc_onderzoek_monsters onmo
,      kgc_categorieen cate
,      kgc_categorie_waarden cawa
where  mons.pers_id = pers.pers_id
and    onmo.mons_id = mons.mons_id
and    onmo.onde_id = onde.onde_id
and    onde.kafd_id = stof.kafd_id
and    cate.cate_id = cawa.cate_id
and    cawa.waarde = stof.code
and    cawa.als_in_gebruik = 'N'
and  ( onde.kafd_id = cate.kafd_id or cate.kafd_id is null )
and  ( onde.ongr_id = cate.ongr_id or cate.ongr_id is null )
and    not exists
       ( select null
         from   bas_meetwaarde_statussen mest
         ,      bas_metingen meti
         ,      bas_meetwaarden meet
         ,      bas_berekeningen bere
         WHERE  meti.meti_id = meet.meti_id
         and    meet.meet_id = bere.meet_id (+)
         and    nvl( bere.in_uitslag, 'J' ) = 'J'
         and    meet.mest_id = mest.mest_id (+)
         and    nvl( mest.in_uitslag, 'J' ) = 'J'
         and    meet.stof_id = stof.stof_id
         and    meti.onde_id = onde.onde_id
       );

/
QUIT
