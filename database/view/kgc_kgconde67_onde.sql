CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_KGCONDE67_ONDE" ("KAFD_CODE", "KAFD_NAAM", "ONDERZOEKNR", "AANMELDDATUM", "AFGEROND", "AFMELDDATUM", "AANVRAGER", "WOONPLAATS_AANVRAGER", "SPECIALISME", "COUNSELOR", "INDI_CODE", "INDI_OMSCHRIJVING", "KAFD_ID", "ONDE_ID", "ONIN_ID", "INDI_ID", "RELA_ID", "SPEC_ID", "MEDE_ID") AS
  SELECT kafd.code
,      kafd.naam
,      onde.onderzoeknr
,      onde.datum_binnen
,      onde.afgerond
,      onde.geplande_einddatum
,      rela.aanspreken
,      rela.woonplaats
,      spec.omschrijving
,      mede.naam
,      indi.code
,      indi.omschrijving
,      kafd.kafd_id
,      onde.onde_id
,      onin.onin_id
,      indi.indi_id
,      rela.rela_id
,      spec.spec_id
,      mede.mede_id
FROM   kgc_kgc_afdelingen        kafd
,      kgc_specialismen          spec
,      kgc_medewerkers           mede
,      kgc_relaties              rela
,      kgc_onderzoeken           onde
,      kgc_onderzoek_indicaties  onin
,      kgc_indicatie_teksten     indi
WHERE  kafd.kafd_id = onde.kafd_id
AND    onde.onde_id = onin.onde_id
AND    onin.indi_id = indi.indi_id
AND    onde.rela_id = rela.rela_id
AND    rela.spec_id = spec.spec_id (+)
AND    onde.mede_id_beoordelaar = mede.mede_id (+)
 ;

/
QUIT
