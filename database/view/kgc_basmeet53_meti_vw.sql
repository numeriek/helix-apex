CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_BASMEET53_METI_VW" ("KAFD_CODE", "KAFD_NAAM", "ONGR_CODE", "ONGR_OMSCHRIJVING", "AANSPREKEN", "GEBOORTEDATUM", "LEEFTIJDCATEGORIE", "PROTOCOL", "PRIORITEIT", "ONDERZOEKNR", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "DATUM_AFNAME", "HOEVEELHEID_MONSTER", "MONSTER_BEWAREN", "MONS_MEDISCHE_INDICATIE", "MONS_MEDICATIE", "MONS_VOEDING", "MONS_COMMENTAAR", "CONCLUSIE", "OPMERKING", "HERHALEN", "AFGEROND", "ANALIST", "AUTORISATOR", "ANALYSENR", "ONDE_AFGEROND", "METI_ID", "ONDE_ID", "STGR_ID", "PERS_ID", "MEDE_ID_ANALIST", "MEDE_ID_AUTORISATOR", "ONMO_ID", "LEEF_ID", "ONGR_ID", "FRAC_ID", "MONS_ID", "ONUI_ID", "GESLACHT", "SPOED", "ONDERZOEKSTYPE") AS
  SELECT kafd.code                kafd_code
,      kafd.naam                kafd_naam
,      ongr.code                ongr_code
,      ongr.omSchrijving        ongr_omschrijving
,      pers.aanspreken          aanspreken
,      pers.geboortedatum       geboortedatum
,      meti.leef_omschrijving   leeftijdcategorie
,      meti.stgr_code           protocol
,      meti.prioriteit          prioriteit
,      meti.onderzoeknr         onderzoeknr
,      meti.monsternummer       monsternummer
,      meti.mate_code		mate_code
,      meti.mate_omschrijving   mate_omschrijving
,      meti.datum_afname        datum_afname
,      meti.hoeveelheid_monster hoeveelheid_monster
,      meti.bewaren             monster_bewaren
,      meti.mons_medische_indicatie mons_medische_indicatie
,      meti.mons_medicatie      mons_medicatie
,      meti.mons_voeding        mons_voeding
,      meti.mons_commentaar     mons_commentaar
,      to_char(meti.conclusie)  conclusie -- Mantis 9355 to_char added to conclusie by SKA 29-03-2016 release 8.11.0.2
,      meti.opmerking_algemeen  opmerking
,      meti.herhalen            herhalen
,      meti.afgerond            afgerond
,      meti.analist             analist
,      meti.autorisator         autorisator
,      meti.analysenr           analysenr
,      meti.onde_afgerond       onde_afgerond
,      meti.meti_id
,      meti.onde_id
,      meti.stgr_id
,      meti.pers_id
,      meti.mede_id_analist
,      meti.mede_id_autorisator
,      meti.onmo_id
,      meti.leef_id
,      meti.ongr_id
,      meti.frac_id
,      meti.mons_id
,      meti.onui_id
,      pers.geslacht
,      meti.spoed
,      meti.onderzoekstype
FROM   kgc_kgc_afdelingen       kafd
,      kgc_personen             pers
,      kgc_onderzoeksgroepen    ongr
,      bas_metingen_vw          meti
WHERE  meti.ongr_id     = ongr.ongr_id
AND    ongr.kafd_id     = kafd.kafd_id
AND    meti.pers_id     = pers.pers_id;

/
QUIT
