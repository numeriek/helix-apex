CREATE OR REPLACE FORCE VIEW "HELIX"."BAS_MONS_METINGEN_VW" ("DATUM_AANMELDING", "MONSTERNUMMER", "MATE_CODE", "MATE_OMSCHRIJVING", "BINNEN_DATUM", "MONS_MEDISCHE_INDICATIE", "MONS_MEDICATIE", "MONS_VOEDING", "MONS_COMMENTAAR", "STGR_CODE", "STGR_OMSCHRIJVING", "HERHALEN", "AFGEROND", "CONCLUSIE", "ONDE_ID") AS
  SELECT meti.datum_aanmelding
,      mons.monsternummer
,      mate.code mate_code
,      mate.omschrijving mate_omschrijving
,      mons.datum_aanmelding binnen_datum
,      mons.medische_indicatie
,      mons.medicatie
,      mons.voeding
,      mons.commentaar
,      stgr.code stgr_code
,      stgr.omschrijving stgr_omschrijving
,      meti.herhalen
,      meti.afgerond
,      to_char(meti.conclusie) --Mantis 9355 to_char added to conclusie by SKA 29-03-2016 release 8.11.0.2
,      onmo.onde_id
from   kgc_stoftestgroepen stgr
,      kgc_materialen mate
,      kgc_monsters mons
,      kgc_onderzoek_monsters onmo
,      bas_metingen meti
where  meti.onmo_id = onmo.onmo_id
and    onmo.mons_id = mons.mons_id
and    mons.mate_id = mate.mate_id
and    meti.stgr_id = stgr.stgr_id (+);

/
QUIT
