CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_VERVALLEN_DOOR_DUPLO_VW" ("MEET_ID") AS
  SELECT meet.meet_id
from bas_meetwaarden meet
, bas_meetwaarde_statussen mest
where meet.mest_id = mest.mest_id
and  mest.default_goed_fout = 'F'
and meet.afgerond = 'J'
and not exists
( select null
  from bas_mdet_ok mdok
  where mdok.meet_id = meet.meet_id
  and mdok.ok = 'J'
)
and not exists
( select null
  from bas_berekeningen bere
  where bere.meet_id = meet.meet_id
  and bere.in_uitslag = 'J'
)
 ;

/
QUIT
