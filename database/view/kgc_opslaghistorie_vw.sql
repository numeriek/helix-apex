CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_OPSLAGHISTORIE_VW" ("OPIN_ID", "OPAT_ID", "JN_USER", "JN_DATE_TIME", "JN_OPERATION", "OPPO_ID", "MEDE_ID_GEPLAATST", "MEDE_ID_UITGEHAALD", "CREATED_BY", "CREATION_DATE", "LAST_UPDATED_BY", "LAST_UPDATE_DATE", "OPMERKINGEN", "CODE_GEPLAATST", "NAAM_GEPLAATST", "CODE_UITGEHAALD", "NAAM_UITGEHAALD", "OPPO_OPEL_ID", "OPPO_CODE", "OPPO_X_POSITIE", "OPPO_Y_POSITIE", "OPEL_CODE", "OPEL_OMSCHRIJVING", "OPAT_VOLGNR", "OPAT_HELIX_ID", "OPAT_HELIX_CODE", "OPAT_PASSAGE", "OPAT_UITGROEI", "OPEN_CODE", "OPEN_OMSCHRIJVING", "OPMERKINGEN_HISTORIE") AS
  SELECT opinj.opin_id
, opinj.opat_id
, opinj.jn_user
, opinj.jn_date_time
, opinj.jn_operation
, opinj.oppo_id
, opinj.mede_id_geplaatst
, opinj.mede_id_uitgehaald
, opinj.created_by
, opinj.creation_date
, opinj.last_updated_by
, opinj.last_update_date
, opinj.opmerkingen
, mede1.code code_geplaatst
, mede1.naam naam_geplaatst
, mede2.code code_uitgehaald
, mede2.naam naam_uitgehaald
, oppo.opel_id
, oppo.code oppo_code
, oppo.x_positie oppo_x_positie
, oppo.y_positie oppo_y_positie
, opel.code opel_code
, opel.omschrijving opel_omschrijving
, opinj.volgnr opat_volgnr
, opinj.helix_id opat_helix_id
, kgc_opslag_00.helix_identificatie(opinj.helix_id, open.code) opat_helix_code
, opinj.passage opat_passage
, opinj.uitgroei opat_uitgroei
, open.code open_code
, open.omschrijving open_omschrijving
, kgc_opslag_00.opmerkingen( opinj.opat_id ) opmerkingen_historie
FROM kgc_opslag_inhoud_jn opinj
, kgc_medewerkers mede1
, kgc_medewerkers mede2
, kgc_opslag_posities oppo
, kgc_opslag_elementen opel
, kgc_opslag_entiteiten open
WHERE opinj.open_id = open.open_id(+)
AND opinj.mede_id_geplaatst = mede1.mede_id(+)
AND opinj.mede_id_uitgehaald = mede2.mede_id(+)
AND opinj.oppo_id = oppo.oppo_id(+)
AND oppo.opel_id = opel.opel_id(+)
 ;

/
QUIT
