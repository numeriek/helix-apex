CREATE OR REPLACE FORCE VIEW "HELIX"."KGC_ONDERZOEK_MEETWAARDEN_VW" ("MEET_ID", "MWST_ID", "STOF_ID", "METI_ID", "KAFD_ID", "ONGR_ID", "ONDE_ID", "ONMO_ID", "STGR_ID", "FRAC_ID", "MONS_ID", "PRIORITEIT", "SNELHEID", "DATUM_AANMELDING", "METI_AFGEROND", "MEET_REKEN", "DATUM_METING", "STOF_CODE", "STOF_OMSCHRIJVING", "MEETWAARDE", "EENHEID", "NORMAALWAARDE_ONDERGRENS", "NORMAALWAARDE_BOVENGRENS", "MEET_AFGEROND", "FRACTIENUMMER", "MONSTERNUMMER", "DATUM_AFNAME", "MATE_CODE", "MATE_OMSCHRIJVING", "ONDERZOEKNR", "ONDE_AFGEROND", "STGR_CODE", "STGR_OMSCHRIJVING", "COMMENTAAR", "MEST_CODE", "MEST_OMSCHRIJVING", "PERS_ZISNR", "PERS_AANSPREKEN", "PERS_GESLACHT", "PERS_GEBOORTEDATUM") AS
  SELECT meet.meet_id
,            meet.mwst_id
,            meet.stof_id
,            meet.meti_id
,            onde.kafd_id
,            onde.ongr_id
,            meti.onde_id
,            meti.onmo_id
,            meti.stgr_id
,            meti.frac_id
,            frac.mons_id
,            meti.prioriteit
,            meti.snelheid
,            meti.datum_aanmelding
,            meti.afgerond meti_afgerond
,            meet.meet_reken
,            meet.datum_meting
,            stof.code stof_code
,            stof.omschrijving stof_omschrijving
,            meet.tonen_als meetwaarde
,            meet.meeteenheid eenheid
,            meet.normaalwaarde_ondergrens
,            meet.normaalwaarde_bovengrens
,            meet.afgerond meet_afgerond
,            frac.fractienummer
,            mons.monsternummer
,            mons.datum_afname
,            mate.code mate_code
,            mate.omschrijving mate_omschrijving
,            onde.onderzoeknr
,            onde.afgerond onde_afgerond
,            stgr.code stgr_code
,            stgr.omschrijving stgr_omschrijving
,            meet.commentaar
,            mest.code mest_code
,            mest.omschrijving mest_omschrijving
,            pers.zisnr pers_zisnr
,            pers.aanspreken pers_aanspreken
,            pers.geslacht pers_geslacht
,            pers.geboortedatum pers_geboortedatum
from    kgc_monsters mons
,            kgc_materialen mate
,            kgc_onderzoeken onde
,            kgc_personen pers
,            bas_fracties frac
,            bas_meetwaarde_statussen mest
,            kgc_stoftestgroepen stgr
,            bas_metingen meti
,            kgc_stoftesten stof
,            bas_meetwaarden meet
where meet.stof_id = stof.stof_id
and      meet.meti_id = meti.meti_id
and      meti.onde_id = onde.onde_id (+)
and      meti.frac_id = frac.frac_id
and      meti.stgr_id = stgr.stgr_id (+)
and      frac.mons_id = mons.mons_id
and      mons.mate_id = mate.mate_id
and      mons.pers_id = pers.pers_id
and      meet.mest_id = mest.mest_id (+)
 ;

/
QUIT
