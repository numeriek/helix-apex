De database directory bevat de scripts die nodig zijn om de database te upgraden van de vorige versie naar de huidige versie.
NB: Zolang we Oracle designer gebruiken is het niet nodig om de volledige DDL en TAPI code hier op te slaan.

- 01_patch_ddl:  Directory bevat de (door Designer gegenereerde) DDL voor de wijzigingen aan de DB objecten.
                 Initieel script 00_patch_ddl.sql roept de door designer gegenereerde code (meestal XXXX) aan en
                 vanaf hier worden ook de script voor gewijzigde packages, views etc. aangeroepen die niet uit
                 Designer komen maar wel gewijzigd zijn in deze patch.

                 DDL die specifiek is voor 1 van de centra staat in de directory voor dat centrum. Die directory
                 heeft een eigen initieel script wat alles voor dat centrum uitvoerd (inclusief aanmaken van specifieke )
              
- 02_patch_tapi: Directory bevat de door Designer gegenereerde TAPI voor de gewijzigde tabellen.
                 De TAPI objecten zijn ook packages en triggers maar deze staan appart 

- 03 patch_dml:  Directory bevat script voor data manipulatie die nodig zijn voor deze patch.
                 Initieel script: 00_patch_dml.sql roept alle onderliggende script aan in de juiste volgorde!
