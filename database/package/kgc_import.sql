set define off
CREATE OR REPLACE PACKAGE "HELIX"."KGC_IMPORT" IS
   -- PACKAGE SPEC kgc_import
   /*
   Wijzigingshistorie:
   ===============

   Bevindingnr	   Wie	   Wanneer
   Wat
   ---------------------------------------------------------------------------------------------------------
   Mantis 3068     APL     30-05-2016
   Reverting changes for Release 8.11.4.7
   Mantis 3068     ATS     17-12-2015
   Mantis 4388/5085: vertaal_naar_helix (doorlevering)
                   JKO     2011-01-26
   Rework Mantis 3184: Interface voor Dragon
                   RDE     2009-11-25
   Mantis 3184: Interface voor Dragon
                   RDE     2009-09-15
   - vertaling tbv familie + familieonderzoek toegevoegd
   - kleine wijzigingen inlezen onderzoek en monster
   - koppel faon-onde
   Mantis 740 AT rework
                   NtB     2009-06-16
   - Procedure vertaal_naar_helix aangepast: zetten van bsn_geverifieerd en
     beheer_in_zis verbeterd bij opvoeren nieuw persoon;
     wordt gezet afh. van MOSOS of ASTRAIA.
   - Bsn_geverifieerd is geen identificerend gegeven.
     Daarom voegtoeaansql(bsn_geverifieerd) verplaatst.

   Mantis 740 AT rework
                   NtB     2009-06-12
   - Procedure vertaal_naar_helix aangepast: parameter beheer_in_zis toegevoegd;
     wordt gezet afh. van MOSOS of ASTRAIA.

   Mantis 283 AT rework
                   NtB     2009-06-02
   - Procedure vertaal_naar_helix aangepast: controleer_bsn via andere procedure.

   Mantis 283      NtB     2009-05-11
   - Procedure vertaal_naar_helix aangepast: parameter toegevoegd.

   Mantis 283      NtB     2009-04-29
   - Procedure vertaal_naar_helix aangepast: controle op en opvoeren van BSN toegevoegd.

   Mantis 740      NtB     2009-03-18
   - Bepaling of onderzoek-relatie-combinatie reeds voorkomt in kgc_onderzoek_kopiehouders aangepast.

   Mantis 740      NtB     2009-02-06
   - Koppel_onderzoek_monster toegevoegd.
   - Toevoegen kgc_onderzoek_kopiehouders verplaatst naar onderzoeksverwerking.

   Mantis 740      NtB     2009-02-02
   - Ontvlechting Astraia.

   Mantis 740	NtB	2008-03-31
   - Code verbeterd nav. geconstateerde fouten/verbeteringen door R. Debets.

   ...		TTi	2007-10-24
   Aanlevering door Numeriek (Gewijzigde)

   Mantis 1363	TTi	2007-10-24
   - Optionele textvelden worden niet gekopieerd
   - Timestamp-velden worden niet gekopieerd
   - Adresverwerking loopt stuk door recycling van variabelen.

   Mantis  529 	TTi	2007-10-24
   Aanpassingen ivm wijziging tabeldefinitie KGC_IM_ZWANGERSCAPPEN

   Mantis  740 	TTi	2007-10-24
   Aanpassingen ivm MOSOSkoppeling

   ...		TTi	2007-03-31
   Aanlevering door Numeriek


      Ver       Date        	 Author          Description
      --------  ----------  	 ---------------  ------------------------------------
      7          04-06-2007  RDE              Mantis 752; bij de gegevens van uit OBS enschede staat er in het land van de patient NED.
                                                          dit geeft de fout:error in vertaaladres
                                                          igv <> Nijm: partnernaam vullen met patient
      6          06-03-2007  RDE              Mantis 676; igv Astraia - Amnion - aantal buizen altijd op 1
      5          27-02-2007  RDE              getData aangepast - "" is gelijk aan leeg!
      4          09-02-2007  RDE              Mantis 591 - toevoegen wat mogelijk is
                                              (vertaal_naar_helix: niet wachten op controle compleet ok)
                                              Mantis 557 - kwootje in naam -> SQL error
      3          13-10-2006  RDE              Aanpassen zisnr: 10 lang!
                                              Combinatie van ulg+punctie goed verwerken (mantis 339)
                                              Bij Astraiamonster ook een afname-onderzoek toevoegen
                                              Astraia-fout ondervangen: verz-id = e-macht. pfff.
                                              Other_names: indien voornaam, dan afkorten
      2          30-05-2006  MK               HLX-HELIX-11326 Postcode check aanpassen
      1         <30-05-2006  div              Nog geen versie beschikbaar
   ***************************************************************************** */

   -- NtB, 02-02-2009: public globals geworden en uitgebreid tbv. MOSOS >>>
   g_cimpw_dom_berekeningswijze  VARCHAR2(30) := 'ATERM_BEREKENINGEN';
   g_cimpw_dom_geslacht          VARCHAR2(30) := 'GESLACHT';
   g_cimpw_dom_koho              VARCHAR2(30) := 'KGC_ONDERZOEK_KOPIEHOUDERS';
   g_cimpw_dom_verzekeringswijze VARCHAR2(30) := 'VERZEKERINGSWIJZE';
   g_cimpw_dom_zekerheid         VARCHAR2(30) := 'ZEKERHEID';
   g_cimpw_dummie                VARCHAR2(10) := 'ALL';
   g_cimpw_tab_cond              VARCHAR2(30) := 'KGC_CONDITIES';
   g_cimpw_tab_herk              VARCHAR2(30) := 'KGC_HERKOMSTEN';
   g_cimpw_tab_indi              VARCHAR2(30) := 'KGC_INDICATIE_TEKSTEN';
   g_cimpw_tab_inst              VARCHAR2(30) := 'KGC_INSTELLINGEN';
   g_cimpw_tab_kafd              VARCHAR2(30) := 'KGC_KGC_AFDELINGEN';
   g_cimpw_tab_mate              VARCHAR2(30) := 'KGC_MATERIALEN';
   g_cimpw_tab_mede              VARCHAR2(30) := 'KGC_MEDEWERKERS';
   g_cimpw_tab_ongr              VARCHAR2(30) := 'KGC_ONDERZOEKSGROEPEN';
   g_cimpw_tab_onwy              VARCHAR2(30) := 'KGC_ONDERZOEKSWIJZEN';
   g_cimpw_tab_rela              VARCHAR2(30) := 'KGC_RELATIES';
   g_cimpw_tab_verz              VARCHAR2(30) := 'KGC_VERZEKERAARS';
   -- mantis 3184: Interface voor Dragon: onderstaande zijn nieuw
   g_cimpw_tab_posi              VARCHAR2(30) := 'POSITIE';
   g_cimpw_dom_onderzoekstypen   VARCHAR2(30) := 'ONDERZOEKSTYPEN';
   g_cimpw_tab_ingr              VARCHAR2(30) := 'KGC_INDICATIE_GROEPEN';
   g_cimpw_tab_leef              VARCHAR2(30) := 'KGC_LEEFTIJDEN';
   g_cimpw_tab_dewy              VARCHAR2(30) := 'KGC_DECLARATIEWIJZEN';
   g_cimpw_tab_taal              VARCHAR2(30) := 'KGC_TALEN';
   g_cimpw_tab_onui              VARCHAR2(30) := 'KGC_ONDERZOEK_UITSLAGCODES';
   -- <<<

   -- format masker tbv de timestampvariabelen
   g_tijdstip_masker VARCHAR2(24) := 'HH24MISS';

   -- bron-formaat kan varieren!! ( bij inlezen zo mogelijk omzetten naar standaardformaat!)
   -- NtB, 02-02-2009: public global van gemaakt
   g_datumformaatbron VARCHAR2(30) := 'YYYY-MM-DD';

   -- NtB: routines public gemaakt tbv. kgc_interface_astraia >>>
   FUNCTION datumformaat(p_datumstring IN VARCHAR2) RETURN VARCHAR2;

   PROCEDURE show_output(p_tekst IN VARCHAR2);

   PROCEDURE addtext -- plak text eraan tot maximum
   (x_text       IN OUT VARCHAR2
   ,p_max        IN NUMBER
   ,p_extra_text IN VARCHAR2
   ,p_separator  IN VARCHAR2 := chr(10) -- '; ' --
    );
   -- <<<

   -- NtB: functie public gemaakt tbv. kgc_interface_mosos
   FUNCTION getimpwid(p_imlo_id            IN NUMBER
                     ,p_helix_table_domain IN VARCHAR2
                     ,p_externe_id         IN VARCHAR2
                     ,p_hint               IN VARCHAR2
                     ,p_toevoegen          IN BOOLEAN
                     ,p_helix_waarde       IN VARCHAR2 DEFAULT NULL -- NtB: nieuwe parameter
                      ) RETURN NUMBER;

   PROCEDURE vertaal_naar_helix(p_imfi_id       IN NUMBER
                               ,p_online        IN BOOLEAN  := TRUE
                               ,p_beheer_in_zis IN VARCHAR2 := 'N');

   FUNCTION toegestane_kafd(p_kafd_id IN NUMBER) RETURN VARCHAR2; -- J/N

   FUNCTION toegestane_ongr(p_ongr_id IN NUMBER) RETURN VARCHAR2; -- J/N

   -- retourneer de helix waarde bij een externe id
   FUNCTION helix_id(p_imlo_id    IN NUMBER
                    ,p_entiteit   IN VARCHAR2
                    ,p_externe_id IN VARCHAR2
                    ,p_hint       IN VARCHAR2) RETURN VARCHAR2;

   -- vul kgc_import_parameters met initiele rijen
   PROCEDURE init_impa;

   -- retourneer tekst voor dynamische LOV in forms
   FUNCTION parameter_lov(p_impa_id IN NUMBER
                         ,p_imlo_id IN NUMBER := NULL) RETURN VARCHAR2;

   -- retourneer lookup-waarde
   FUNCTION parameter_lookup(p_impa_id      IN NUMBER
                            ,p_helix_waarde IN VARCHAR2
                            ,p_imlo_id      IN NUMBER := NULL) RETURN VARCHAR2;

   PROCEDURE lees_in_astraia(p_imlo_id  IN NUMBER
                            ,p_filenaam IN VARCHAR2);

   -- verwerk de binnengekomen bestanden
   PROCEDURE verwerk_bestanden;
END KGC_IMPORT;
/
CREATE OR REPLACE PACKAGE BODY HELIX.KGC_IMPORT IS
   -- PACKAGE BODY kgc_import

   -- bron-formaat kan varieren!! ( bij inlezen zo mogelijk omzetten naar standaardformaat!)
   -- v_datumformaatbron     VARCHAR2(30) := 'YYYY-MM-DD'; -- NtB, 02-02-2009: public global geworden
   v_datumformaatdoel     VARCHAR2(30) := 'DD-MM-YYYY';
   v_datumtijdformaatdoel VARCHAR2(30) := 'DD-MM-YYYY HH24:MI:SS'; -- RDE 20-02-2008
   v_max_len              NUMBER := 4000; -- lengte foutmelding / opmerking / verslag

   -- NtB: nav. aanpassingen RDE, 26-02-2008:
   -- NtB, 02-02-2009: verplaatst naar kgc_interface_astraia
   -- v_hersteljob BOOLEAN := FALSE; -- zie gebuik; file niet opnieuw inlezen!

   FUNCTION datumformaat(p_datumstring IN VARCHAR2) RETURN VARCHAR2 IS
      v_return VARCHAR2(10);
      --    v_datum  DATE;
      -- PL/SQL Block
   BEGIN
      BEGIN
         v_return := to_char(to_date(p_datumstring
                                    ,g_datumformaatbron) -- NtB: was v_datum...
                            ,v_datumformaatdoel);
      EXCEPTION
         WHEN OTHERS THEN
            v_return := p_datumstring;
      END;
      RETURN(v_return);
   EXCEPTION
      WHEN OTHERS THEN
         RETURN(p_datumstring);
   END datumformaat;

   -- zet datumstring om in een DATE, probeer verschillende formaten
   FUNCTION datum(p_datumstring IN VARCHAR2
                 ,p_formaat     IN VARCHAR2 := NULL) RETURN DATE IS
      v_return DATE := NULL;
   BEGIN
      IF (p_datumstring IS NULL)
      THEN
         RETURN(NULL);
      END IF;
      IF (v_return IS NULL)
      THEN
         IF (p_formaat IS NOT NULL)
         THEN
            BEGIN
               v_return := to_date(p_datumstring
                                  ,p_formaat);
            EXCEPTION
               WHEN OTHERS THEN
                  v_return := NULL;
            END;
         END IF;
      END IF;
      IF (v_return IS NULL)
      THEN
         IF (v_datumformaatdoel IS NOT NULL)
         THEN
            BEGIN
               v_return := to_date(p_datumstring
                                  ,v_datumformaatdoel);
            EXCEPTION
               WHEN OTHERS THEN
                  v_return := NULL;
            END;
         END IF;
      END IF;
      IF (v_return IS NULL)
      THEN
         IF (g_datumformaatbron IS NOT NULL) -- NtB: was v_datum...
         THEN
            BEGIN
               v_return := to_date(p_datumstring
                                  ,g_datumformaatbron); -- NtB: was v_datum...
            EXCEPTION
               WHEN OTHERS THEN
                  v_return := NULL;
            END;
         END IF;
      END IF;
      IF (v_return IS NULL)
      THEN
         BEGIN
            v_return := to_date(p_datumstring
                               ,'dd-mm-yyyy');
         EXCEPTION
            WHEN OTHERS THEN
               v_return := NULL;
         END;
      END IF;
      RETURN(v_return);
   EXCEPTION
      WHEN OTHERS THEN
         RETURN(p_datumstring);
   END datum;
   PROCEDURE show_output(p_tekst IN VARCHAR2) IS
   BEGIN
      dbms_output.put_line(substr(p_tekst
                                 ,1
                                 ,255));
      IF length(p_tekst) > 255
      THEN
         dbms_output.put_line(substr(p_tekst
                                    ,256
                                    ,255));
         IF length(p_tekst) > 510
         THEN
            dbms_output.put_line(substr(p_tekst
                                       ,511
                                       ,255));
            IF length(p_tekst) > 765
            THEN
               dbms_output.put_line(substr(p_tekst
                                          ,766
                                          ,255));
               IF length(p_tekst) > 1120
               THEN
                  dbms_output.put_line(' >>');
               END IF;
            END IF;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('Geen output');
   END show_output;

   PROCEDURE addtext -- plak text eraan tot maximum
   (x_text       IN OUT VARCHAR2
   ,p_max        IN NUMBER := NULL
   ,p_extra_text IN VARCHAR2
   ,p_separator  IN VARCHAR2 := chr(10) -- '; ' --
    ) IS
      v_len     NUMBER;
      v_len_add NUMBER;
      v_sep     VARCHAR2(10) := p_separator;
   BEGIN
      IF nvl(length(p_extra_text),0) = 0
      THEN
         RETURN; -- niks toe te voegen; RDE 17-03-2010 Mantis 5715
      END IF;
      v_len     := nvl(length(x_text)
                      ,0);
      v_len_add := NVL(p_max, v_max_len) - v_len; -- beschikbare ruimte
      IF v_len_add > 0
      THEN
         IF (x_text IS NULL)
         THEN
            x_text := substr(p_extra_text
                            ,1
                            ,v_len_add);
         ELSE
            x_text := x_text || substr(v_sep || p_extra_text
                                      ,1
                                      ,v_len_add);
         END IF;
      END IF;
   END addtext;
   -- haal impw_id op adhv div. parameters
   FUNCTION getimpwid(p_imlo_id            IN NUMBER
                     ,p_helix_table_domain IN VARCHAR2
                     ,p_externe_id         IN VARCHAR2
                     ,p_hint               IN VARCHAR2
                     ,p_toevoegen          IN BOOLEAN
                     ,p_helix_waarde       IN VARCHAR2 DEFAULT NULL -- NtB: nieuwe parameter
                      ) RETURN NUMBER IS

      CURSOR c_impa IS
         SELECT impa.impa_id
           FROM kgc_import_parameters impa
          WHERE impa.helix_table_domain = p_helix_table_domain;

      CURSOR c_impw_zoek(p_impa_id IN NUMBER) IS
         SELECT impw.externe_id
           FROM kgc_import_param_waarden impw
          WHERE impw.impa_id = p_impa_id
            AND impw.externe_id like '#EXT-ID=HELIX-%#'
            AND impw.imlo_id = p_imlo_id;
      r_impw_zoek c_impw_zoek%ROWTYPE;

      CURSOR c_impw(p_impa_id IN NUMBER) IS
         SELECT *
           FROM kgc_import_param_waarden impw
          WHERE impw.impa_id = p_impa_id
            AND impw.externe_id = p_externe_id
            AND impw.imlo_id = p_imlo_id;
      r_impw         c_impw%ROWTYPE;

      v_impa_id      kgc_import_parameters.impa_id%TYPE;
      v_count        NUMBER;
   BEGIN

      IF (p_externe_id IS NULL)
      THEN
         RETURN NULL; -- bron blijkbaar leeg.
      END IF;

      OPEN c_impa;
      FETCH c_impa
         INTO v_impa_id;
      IF c_impa%NOTFOUND
      THEN
         IF p_toevoegen = FALSE
         THEN
            CLOSE c_impa;
            RETURN NULL;
         END IF;
         -- nieuw id
         SELECT kgc_impa_seq.NEXTVAL
           INTO v_impa_id
           FROM dual;
         INSERT INTO kgc_import_parameters
            (impa_id
            ,helix_table_domain)
         VALUES
            (v_impa_id
            ,p_helix_table_domain);
      END IF;
      CLOSE c_impa;

      OPEN c_impw(v_impa_id);
      FETCH c_impw INTO r_impw;
      CLOSE c_impw;

      -- mantis 3184: Interface voor Dragon
      -- rde 15-09-2009 moet er geprobeerd worden direct een koppeling te maken?
      -- dan moet er voor de parameter een waarde aanwezig zijn, bv #EXT-ID=HELIX-<uk>#
      -- onderscheid table / domain!

      IF r_impw.helix_waarde IS NULL
      THEN
         IF p_helix_waarde IS NOT NULL
         THEN
            r_impw.helix_waarde := p_helix_waarde;
         ELSE
            -- deze kan een bekende waarde hebben, dan is p_externe_id bv '#EXT-ID=MEDE.CODE:RDE'
            IF p_externe_id LIKE '#EXT-ID=%'
            THEN
               DECLARE
                  v_statement VARCHAR2(4000);
                  v_alias VARCHAR2(30) := kgc_util_00.tabel_alias(p_helix_table_domain);
                  v_pk_col VARCHAR2(30) := v_alias || '_ID';
                  v_uk_col VARCHAR2(30);
                  v_pos NUMBER;
                  v_waarde kgc_import_param_waarden.helix_waarde%TYPE;
               BEGIN
                  v_uk_col := REPLACE(p_externe_id, '#EXT-ID=' || v_alias || '.', '');
                  v_pos := INSTR(v_uk_col, ':');
                  IF v_pos > 0 THEN
                     v_waarde := SUBSTR(v_uk_col, v_pos + 1);
                     v_uk_col := SUBSTR(v_uk_col, 1, v_pos - 1); -- bv MEDE_ID of CODE
                     IF UPPER(v_pk_col) = UPPER(v_uk_col) THEN -- al klaar
                        r_impw.helix_waarde := v_waarde;
                     ELSE
                        v_statement := 'SELECT ' || v_pk_col || ' FROM ' || p_helix_table_domain ||
                                       ' WHERE ' || v_uk_col || ' = ''' || v_waarde || '''';
                        r_impw.helix_waarde := kgc_util_00.dyn_exec( v_statement ) ;
                        -- nu meteen de juiste koppeling gelegd.
                        -- moet er nog iets gebeuren met #EXT-ID=MEDE.CODE? (dit is een beetje vreemde waarde in de koppeltabel)
                        -- Antwoord: NEE! Deze waarde moet blijven, anders is er bv geen onderscheid tussen de maastrichtse RD en de nijmeegse RD...
                        -- rekening mee houden bij aanroep van kgc_import.get_externe_id_via_impw_id
                     END IF;
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN NULL; -- dan geen koppeling
               END;
            END IF;

            IF r_impw.helix_waarde IS NULL
            THEN -- nog niets gevonden!
               -- dan kan het nog zijn dat er een automatische waarde van toepassing is (bv "#EXT-ID=HELIX-CODE")
               OPEN c_impw_zoek(v_impa_id);
               FETCH c_impw_zoek
                  INTO r_impw_zoek;
               IF c_impw_zoek%FOUND
               THEN -- bepaal of het om een table of domain gaat
                  SELECT COUNT(*)
                    INTO v_count
                    FROM all_tables
                   WHERE owner = kgc_util_00.appl_owner
                     AND table_name = p_helix_table_domain;
                  IF v_count = 0
                  THEN -- het is een domain... dan moet de waarde overgenomen worden!
                     r_impw.helix_waarde := p_externe_id;
                  ELSE -- het is een table... zoek het id adhv de opgegeven kolom
                     DECLARE
                        v_statement VARCHAR2(4000);
                        v_pk_col VARCHAR2(30) := kgc_util_00.tabel_alias(p_helix_table_domain) || '_ID';
                        v_uk_col VARCHAR2(30);
                     BEGIN
                        v_uk_col := REPLACE(r_impw_zoek.externe_id, '#EXT-ID=HELIX-', '');
                        v_uk_col := REPLACE(v_uk_col, '#', '');
                        IF UPPER(v_pk_col) = UPPER(v_uk_col) THEN -- al klaar
                           r_impw.helix_waarde := p_externe_id;
                        ELSE
                           v_statement := 'SELECT ' || v_pk_col || ' FROM ' || p_helix_table_domain ||
                                          ' WHERE ' || v_uk_col || ' = ''' || p_externe_id || '''';
                           r_impw.helix_waarde := kgc_util_00.dyn_exec( v_statement ) ;
                        END IF;
                     EXCEPTION
                        WHEN OTHERS
                        THEN NULL; -- dan geen koppeling
                     END;
                  END IF;
               END IF;
               CLOSE c_impw_zoek;
            END IF;
         END IF;
      END IF;

      IF r_impw.impw_id IS NULL
      THEN
         IF p_toevoegen = FALSE
         THEN
            RETURN NULL;
         END IF;

         -- nieuw id
         SELECT kgc_impw_seq.NEXTVAL
           INTO r_impw.impw_id
           FROM dual;

         INSERT INTO kgc_import_param_waarden
            (impw_id
            ,impa_id
            ,externe_id
            ,imlo_id
            ,helix_waarde -- NtB: nieuw
            ,hint)
         VALUES
            (r_impw.impw_id
            ,v_impa_id
            ,p_externe_id
            ,p_imlo_id
            ,r_impw.helix_waarde -- NtB: nieuw
            ,p_hint);

      ELSE
         -- wellicht update hint en waarde
         IF (p_hint IS NOT NULL)
         THEN
            r_impw.hint := p_hint;
         END IF;
         UPDATE kgc_import_param_waarden impw
            SET hint = r_impw.hint -- indien opgegeven: altijd overnemen
              , helix_waarde = NVL(helix_waarde, r_impw.helix_waarde) -- indien aanwezig: niet bijwerken!
          WHERE impw.impw_id = r_impw.impw_id
            AND (NVL(hint, chr(2)) != NVL(r_impw.hint, chr(2)) OR helix_waarde IS NULL);
      END IF;

      RETURN r_impw.impw_id;

   END getimpwid;
   -- PUBLIC
   FUNCTION toegestane_kafd(p_kafd_id IN NUMBER) RETURN VARCHAR2 IS
      v_return VARCHAR2(1) := 'N';
      CURSOR meka_cur IS
         SELECT 'J'
           FROM kgc_mede_kafd meka
          WHERE meka.kafd_id = p_kafd_id
            AND meka.mede_id = kgc_mede_00.medewerker_id;
   BEGIN
      OPEN meka_cur;
      FETCH meka_cur
         INTO v_return;
      CLOSE meka_cur;
      RETURN(v_return);
   END toegestane_kafd;
   FUNCTION toegestane_ongr(p_ongr_id IN NUMBER) RETURN VARCHAR2 IS
      v_return VARCHAR2(1) := 'N';
      CURSOR meka_cur IS
         SELECT 'J'
           FROM kgc_onderzoeksgroepen ongr
          WHERE toegestane_kafd(ongr.kafd_id) = 'J'
            AND ongr.ongr_id = p_ongr_id;
   BEGIN
      OPEN meka_cur;
      FETCH meka_cur
         INTO v_return;
      CLOSE meka_cur;
      RETURN(v_return);
   END toegestane_ongr;
   PROCEDURE init_impa IS
      v_lov_query    VARCHAR2(2000);
      v_lookup_query VARCHAR2(2000);
      PROCEDURE ins_impa(p_code         IN VARCHAR2
                        ,p_omschrijving IN VARCHAR2) IS
         CURSOR c IS
            SELECT impa_id
              FROM kgc_import_parameters
             WHERE helix_table_domain = upper(p_code);
         v_impa_id kgc_import_parameters.impa_id%TYPE;
      BEGIN
         OPEN c;
         FETCH c
            INTO v_impa_id;
         CLOSE c;
         IF (v_impa_id IS NOT NULL)
         THEN
            UPDATE kgc_import_parameters
               SET omschrijving = p_omschrijving
                  ,lov_query    = nvl(lov_query
                                     ,v_lov_query)
                  ,lookup_query = nvl(lookup_query
                                     ,v_lookup_query)
             WHERE impa_id = v_impa_id;
         ELSE
            INSERT INTO kgc_import_parameters
               (helix_table_domain
               ,omschrijving
               ,lov_query
               ,lookup_query)
            VALUES
               (upper(p_code)
               ,p_omschrijving
               ,v_lov_query
               ,v_lookup_query);
         END IF;
         v_lov_query    := NULL;
         v_lookup_query := NULL;
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END ins_impa;
      PROCEDURE ins_impw(p_code         IN VARCHAR2
                        ,p_omschrijving IN VARCHAR2) IS
         CURSOR imlo_cur IS
            SELECT imlo_id
              FROM kgc_import_locaties;
         CURSOR c IS
            SELECT impw_id
              FROM kgc_import_param_waarden
             WHERE helix_table_domain = upper(p_code)
               AND upper(externe_id) = upper(p_omschrijving)
            --      and    upper( hint ) like '%STANDAARDWAARDE%' --
            ;
         v_impw_id kgc_import_param_waarden.impw_id%TYPE;
      BEGIN
         -- voor elke locatie
         FOR r IN imlo_cur
         LOOP
            OPEN c;
            FETCH c
               INTO v_impw_id;
            CLOSE c;
            IF (v_impw_id IS NOT NULL)
            THEN
               NULL; -- geen updates
            ELSE
               INSERT INTO kgc_import_param_waarden
                  (impa_id
                  ,imlo_id
                  ,helix_table_domain
                  ,externe_id
                  ,hint)
                  SELECT impa_id
                        ,r.imlo_id
                        ,upper(p_code)
                        ,p_omschrijving
                        ,'Standaardwaarde'
                    FROM kgc_import_parameters
                   WHERE helix_table_domain = upper(p_code);
            END IF;
         END LOOP;
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END ins_impw;
   BEGIN
      -- <ID> wordt bij gebruik van lookup vervangen door de PK-waarde
      v_lov_query    := 'select to_char(kafd_id) code, code||'': ''||naam description, kafd_id pk_seq_nr' || chr(10) ||
                        'from kgc_kgc_afdelingen' || chr(10) || 'where kgc_import.toegestane_kafd( kafd_id ) = ''J''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'' ''||naam' || chr(10) || 'from kgc_kgc_afdelingen' || chr(10) ||
                        'where kafd_id = <ID>';
      ins_impa(g_cimpw_tab_kafd
              ,'KGC-afdeling');
      ins_impw(g_cimpw_tab_kafd
              ,'Onderzoek');
      ins_impw(g_cimpw_tab_kafd
              ,'Monsterafname');
      ------------------------------------------------------
      v_lov_query    := 'select TO_CHAR(ongr_id) code, code||'': ''||omschrijving description, ongr_id pk_seq_nr' ||
                        chr(10) || 'from kgc_onderzoeksgroepen' || chr(10) ||
                        'where kgc_import.toegestane_ongr( ongr_id ) = ''J''' || chr(10) || 'and vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_onderzoeksgroepen' || chr(10) ||
                        'where ongr_id = <ID>';
      ins_impa(g_cimpw_tab_ongr
              ,'Onderzoeksgroep: deel van de KGC-afdeling');
      ins_impw(g_cimpw_tab_ongr
              ,'Onderzoek');
      ins_impw(g_cimpw_tab_ongr
              ,'Monsterafname');
      ------------------------------------------------------
      -- niet
      --  v_lov_query := 'select to_char(rela_id) code, code||'': ''||aanspreken description, rela_id pk_seq_nr' --
      --     ||CHR(10)|| 'from kgc_relaties' --
      --     ||CHR(10)|| 'where vervallen = ''N''' --
      --     ||CHR(10)|| 'order by 2' --
      --               ;
      -- maar...
      v_lov_query    := '<MODULE>KGCRELA05</MODULE>';
      v_lookup_query := 'select code||'': ''||aanspreken' || chr(10) || 'from kgc_relaties' || chr(10) ||
                        'where rela_id = <ID>';
      ins_impa(g_cimpw_tab_rela
              ,'Relatie; huisarts, verwijzer');
      ------------------------------------------------------
      -- niet
      --  v_lov_query := 'select to_char(verz_id) code, code||'': ''||naam description, verz_id pk_seq_nr' --
      --     ||CHR(10)|| 'from kgc_verzekeraars' --
      --     ||CHR(10)|| 'where vervallen = ''N''' --
      --     ||CHR(10)|| 'order by 2' --
      --               ;
      --maar ...
      v_lov_query    := '<MODULE>KGCVERZ05</MODULE>';
      v_lookup_query := 'select code||'': ''||naam' || chr(10) || 'from kgc_verzekeraars' || chr(10) ||
                        'where verz_id = <ID>';
      ins_impa(g_cimpw_tab_verz
              ,'Verzekeraar');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(inst_id) code, code||'': ''||naam description, inst_id pk_seq_nr' || chr(10) ||
                        'from kgc_instellingen' || chr(10) || 'where vervallen = ''N''' || chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||naam' || chr(10) || 'from kgc_instellingen' || chr(10) ||
                        'where inst_id = <ID>';
      ins_impa(g_cimpw_tab_inst
              ,'Instelling, ziekenhuis');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(herk_id) code, code||'': ''||omschrijving description, herk_id pk_seq_nr' ||
                        chr(10) || 'from kgc_herkomsten' || chr(10) || 'where vervallen = ''N''' || chr(10) ||
                        'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_herkomsten' || chr(10) ||
                        'where herk_id = <ID>';
      ins_impa(g_cimpw_tab_herk
              ,'Herkomst van een monster of onderzoek');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(mate_id) code, code||'': ''||omschrijving description, mate_id pk_seq_nr' ||
                        chr(10) || 'from kgc_materialen' || chr(10) ||
                        'where kgc_import.toegestane_kafd( kafd_id ) = ''J''' || chr(10) || 'and vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_materialen' || chr(10) ||
                        'where mate_id = <ID>';
      ins_impa(g_cimpw_tab_mate
              ,'Monstermateriaal: vruchtwater, vlokken, navelstrengbloed');
      ------------------------------------------------------
      v_lov_query    := 'select distinct to_char(mede.mede_id) code, mede.code||'': ''||mede.naam description, mede.mede_id pk_seq_nr' ||
                        chr(10) || 'from kgc_medewerkers mede, kgc_mede_kafd meka' || chr(10) ||
                        'where ( kgc_import.toegestane_kafd( meka.kafd_id ) = ''J'' or meka.kafd_id is null)' ||
                        chr(10) || 'and mede.mede_id = meka.mede_id (+)' || chr(10) || 'and mede.vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||naam' || chr(10) || 'from kgc_medewerkers' || chr(10) ||
                        'where mede_id = <ID>';
      ins_impa(g_cimpw_tab_mede
              ,'Medewerker van de KGC-afdeling');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(onwy_id) code, code||'': ''||omschrijving description, onwy_id pk_seq_nr' ||
                        chr(10) || 'from kgc_onderzoekswijzen' || chr(10) || 'where vervallen = ''N''' || chr(10) ||
                        'and kgc_import.toegestane_kafd( kafd_id ) = ''J''' || chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_onderzoekswijzen' || chr(10) ||
                        'where onwy_id = <ID>';
      ins_impa(g_cimpw_tab_onwy
              ,'Onderzoekswijze');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(cond_id) code, code||'': ''||omschrijving description, cond_id pk_seq_nr' ||
                        chr(10) || 'from kgc_condities' || chr(10) || 'where vervallen = ''N''' || chr(10) ||
                        'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_condities' || chr(10) ||
                        'where cond_id = <ID>';
      ins_impa(g_cimpw_tab_cond
              ,'Monsterconditie');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(indi.indi_id) code, indi.code||'' - ''||indi.omschrijving || '' (indgrp: '' || ingr.code || '' - '' || ingr.omschrijving || '')'' description, indi_id pk_seq_nr' ||
                        chr(10) ||
                        'from kgc_indicatie_groepen ingr, kgc_indicatie_teksten indi, kgc_import_param_waarden impw' ||
                        chr(10) || 'where indi.ingr_id = ingr.ingr_id' || chr(10) ||
                        'and kgc_import.toegestane_ongr( indi.ongr_id ) = ''J''' || chr(10) ||
                        'and indi.vervallen = ''N''' || chr(10) || 'and ingr.vervallen = ''N''' || chr(10) ||
                        'and impw.impw_id = :imon.impw_id_ongr' || chr(10) || 'and impw.helix_waarde = indi.ongr_id' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_indicatie_teksten' || chr(10) ||
                        'where indi_id = <ID>';
      ins_impa(g_cimpw_tab_indi
              ,'Indicatieteksten');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(ingr_id) code, code||'': ''||omschrijving description, ingr_id pk_seq_nr' ||
                        chr(10) || 'from kgc_indicatie_groepen' || chr(10) ||
                        'where kgc_import.toegestane_ongr( ongr_id ) = ''J''' || chr(10) || 'and vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_indicatie_groepen' || chr(10) ||
                        'where ingr_id = <ID>';
      ins_impa(g_cimpw_tab_ingr
              ,'Indicatiegroepen, machtigingsindicatie');
      ------------------------------------------------------
      v_lov_query    := 'select ''LM'' code, ''Laatste menstruatie'' description, 1 pk_seq_nr from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''ECHO'', ''Echoscopie'', 2 from dual' || chr(10) || 'union' ||
                        chr(10) || 'select ''IVF'', ''In vitro fertilisatie'', 3 from dual' || chr(10) || 'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''LM'',''Laatste menstruatie''' || chr(10) ||
                        ',''ECHO'',''Echoscopie''' || chr(10) || ',''IVF'',''In vitro fertilisatie''' || chr(10) ||
                        ') from dual';
      ins_impa(g_cimpw_dom_berekeningswijze
              ,'Berekeningswijze aterme datum');
      ------------------------------------------------------
      v_lov_query    := 'select ''0'' code, ''Uitgesloten'' description, 0 pk_seq_nr from dual' || chr(10) || 'union' ||
                        chr(10) || 'select ''1'', ''Onwaarschijnlijk'', 1 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''2'', ''Mogelijk'', 2 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''3'', ''Waarschijnlijk'', 3 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''4'', ''Zeker'', 4 from dual' || chr(10) || 'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''0'',''Uitgesloten''' || chr(10) ||
                        ',''1'',''Onwaarschijnlijk''' || chr(10) || ',''2'',''Mogelijk''' || chr(10) ||
                        ',''3'',''Waarschijnlijk''' || chr(10) || ',''4'',''Zeker''' || chr(10) || ') from dual';
      ins_impa(g_cimpw_dom_zekerheid
              ,'Mate van zekerheid');
      ------------------------------------------------------
      v_lov_query    := 'select ''M'' code, ''Mannelijk'' description, 1 pk_seq_nr from dual' || chr(10) || 'union' ||
                        chr(10) || 'select ''V'', ''Vrouwelijk'', 2 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''O'', ''Onbekend'', 3 from dual' || chr(10) || 'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''M'',''Mannelijk''' || chr(10) ||
                        ',''V'',''Vrouwelijk''' || chr(10) || ',''O'',''Onbekend''' || chr(10) || ') from dual';
      ins_impa(g_cimpw_dom_geslacht
              ,'Geslacht van een persoon of foetus');
      ------------------------------------------------------
      v_lov_query    := 'select ''P'' code, ''Particulier'' description, 1 pk_seq_nr from dual' || chr(10) || 'union' ||
                        chr(10) || 'select ''Z'', ''Ziekenfonds'', 2 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''O'', ''Onbekend'', 3 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''N'', ''Niet declarabel'', 4 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''B'', ''Basisverzekering'', 0 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''G'', ''Geen in NL'', 5 from dual' || chr(10) || 'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''P'',''Particulier''' || chr(10) ||
                        ',''Z'',''Ziekenfonds''' || chr(10) || ',''O'',''Onbekend''' || chr(10) ||
                        ',''N'',''Niet declarabel''' || chr(10) || ',''B'',''Basisverzekering''' || chr(10) ||
                        ',''G'',''Geen in NL''' || chr(10) || ') from dual';
      ins_impa(g_cimpw_dom_verzekeringswijze
              ,'Verzekeringswijze patient');
      ------------------------------------------------------
      v_lov_query    := 'select ''GR'' code, ''Groningen'' description, 1 pk_seq_nr from dual' || chr(10) || 'union' ||
                        chr(10) || 'select ''FR'', ''Friesland'', 2 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''DR'', ''Drente'', 3 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''OV'', ''Overijssel'', 4 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''GL'', ''Gelderland'', 5 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''UT'', ''Utrecht'', 6 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''NH'', ''Noord-Holland'', 7 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''ZH'', ''Zuid-Holland'', 8 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''ZL'', ''Zeeland'', 9 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''NB'', ''Noord-Brabant'', 10 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''LB'', ''Limburg'', 11 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''FL'', ''Flevoland'', 12 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''00'', ''Buitenland'', 98 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''99'', ''Onbekend'', 99 from dual' || chr(10) || 'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''GR'',''Groningen''' || chr(10) ||
                        ',''FR'',''Friesland''' || chr(10) || ',''DR'',''Drente''' || chr(10) ||
                        ',''OV'',''Overijssel''' || chr(10) || ',''GL'',''Gelderland''' || chr(10) ||
                        ',''UT'',''Utrecht''' || chr(10) || ',''NH'',''Noord-Holland''' || chr(10) ||
                        ',''ZH'',''Zuid-Holland''' || chr(10) || ',''ZL'',''Zeeland''' || chr(10) ||
                        ',''NB'',''Noord-Brabant''' || chr(10) || ',''LB'',''Limburg''' || chr(10) ||
                        ',''FL'',''Flevoland''' || chr(10) || ') from dual';
      ins_impa(g_cimpw_dom_provincie
              ,'Provincie in Nederland');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(dewy_id) code, code||'': ''||omschrijving description, dewy_id pk_seq_nr' || chr(10) ||
                        'from kgc_declaratiewijzen dewy' || chr(10) ||
                        'where vervallen = ''N''' || chr(10) ||
                        'and kgc_import.toegestane_kafd( dewy.kafd_id ) = ''J''' || chr(10) ||
                        'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_declaratiewijzen' || chr(10) ||
                        'where dewy_id = <ID>';
      ins_impa(g_cimpw_tab_dewy
              ,'Declaratiewijzen');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(onui_id) code, code||'': ''||omschrijving description, onui_id pk_seq_nr' || chr(10) ||
                        'from kgc_onderzoek_uitslagcodes onui' || chr(10) ||
                        'where vervallen = ''N''' || chr(10) ||
                        'and kgc_import.toegestane_kafd( onui.kafd_id ) = ''J''' || chr(10) ||
                        'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_onderzoek_uitslagcodes' || chr(10) ||
                        'where onui_id = <ID>';
      ins_impa(g_cimpw_tab_onui
              ,'Onderzoek uitslagcodes');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(taal_id) code, code||'': ''||omschrijving description, taal_id pk_seq_nr' || chr(10) ||
                        'from kgc_talen' || chr(10) ||
                        'where vervallen = ''N''' || chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_talen' || chr(10) ||
                        'where taal_id = <ID>';
      ins_impa(g_cimpw_tab_taal
              ,'Talen');
      ------------------------------------------------------
      v_lov_query    := 'select ''C'' code, ''Controle'' description, 1 pk_seq_nr from dual' || chr(10) || 'union' ||
                        chr(10) || 'select ''R'', ''Research'', 2 from dual' || chr(10) || 'union' || chr(10) ||
                        'select ''D'', ''Diagnostiek'', 99 from dual' || chr(10) || 'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''C'',''Controle''' || chr(10) ||
                        ',''R'',''Research''' || chr(10) || ',''D'',''Diagnostiek''' || chr(10) || ') from dual';
      ins_impa(g_cimpw_dom_onderzoekstypen
              ,'Onderzoekstypen');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(onre.onre_id) code, onre.code||'' - ''||onre.omschrijving description, onre_id pk_seq_nr' ||
                        chr(10) || 'from kgc_onderzoeksredenen onre' ||
                        chr(10) ||'where kgc_import.toegestane_ongr( onre.ongr_id ) = ''J''' || chr(10) || 'and onre.vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from kgc_onderzoeksredenen' ||
                        chr(10) ||'where onre_id = <ID>';
      ins_impa(g_cimpw_tab_onre
              ,'Onderzoeksredenen');
      ------------------------------------------------------
      v_lov_query    := 'select ''A'' code, ''Aangemeld'' description, 1 pk_seq_nr from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''O'', ''Opgewerkt'', 2 from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''V'', ''Vervallen'', 3 from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''M'', ''Mislukt'', 4 from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''X'', ''Verbruikt'', 5 from dual' || chr(10) ||
                        'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''A'',''Aangemeld''' || chr(10) ||
                        ',''O'',''Opgewerkt''' || chr(10) || ',''V'',''Vervallen''' || chr(10) ||
                        ',''M'',''Mislukt''' || chr(10) || ',''X'',''Verbruikt''' || chr(10) ||
                        ') from dual';
      ins_impa( kgc_import.g_cimpw_dom_fractiestatus
              ,'Fractiestatus');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(frty.frty_id) code, frty.code||'' - ''||frty.omschrijving description, frty_id pk_seq_nr' ||
                        chr(10) || 'from bas_fractie_types frty' ||
                        chr(10) ||'where kgc_import.toegestane_ongr( frty.ongr_id ) = ''J''' || chr(10) || 'and frty.vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select code||'': ''||omschrijving' || chr(10) || 'from bas_fractie_types' ||
                        chr(10) ||'where frty_id = <ID>';
      ins_impa( kgc_import.g_cimpw_tab_frty
              , 'Fractietypes');
-- ongr
      ------------------------------------------------------
      v_lov_query    := 'select to_char(eenh.eenh_id) code, eenh.eenheid description, eenh_id pk_seq_nr' ||
                        chr(10) || 'from kgc_eenheden eenh' ||
                        chr(10) ||'where eenh.vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select eenheid' || chr(10) || 'from kgc_eenheden' ||
                        chr(10) ||'where eenh_id = <ID>';
      ins_impa( kgc_import.g_cimpw_tab_eenh
              , 'Eenheden');
      ------------------------------------------------------
      v_lov_query    := 'select ''O'' code, ''Opgewerkt (uit een kweekfractie)'' description, 1 pk_seq_nr from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''Z'', ''Zuiveren'', 2 from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''D'', ''Dochter'', 3 from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''S'', ''Standaard'', 4 from dual' || chr(10) ||
                        'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''O'',''Opgewerkt (uit een kweekfractie)''' || chr(10) ||
                        ',''Z'',''Zuiveren''' || chr(10) || ',''D'',''Dochter''' || chr(10) ||
                        ',''S'',''Standaard''' || chr(10) ||
                        ') from dual';
      ins_impa( kgc_import.g_cimpw_dom_type_afleiding
              ,'Type afleiding');
      ------------------------------------------------------
      v_lov_query    := 'select ''K'' code, ''Kortdurend'' description, 1 pk_seq_nr from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''L'', ''Langdurend'', 2 from dual' || chr(10) ||
                        'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''K'',''Kortdurend''' || chr(10) ||
                        ',''L'',''Langdurend''' || chr(10) ||
                        ') from dual';
      ins_impa( kgc_import.g_cimpw_dom_meti_snelheid
              ,'Metingsnelheid');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(brty.brty_id) code, brty.code || '' - '' || brty.omschrijving description, brty_id pk_seq_nr' ||
                        chr(10) || 'from kgc_brieftypes brty' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select omschrijving' || chr(10) || 'from kgc_brieftypes' ||
                        chr(10) ||'where brty_id = <ID>';
      ins_impa( kgc_import.g_cimpw_tab_brty
              ,'Brieftypes');
      ------------------------------------------------------
      v_lov_query    := 'select to_char(stgr.stgr_id) code, stgr.code || '' - '' || stgr.omschrijving description, stgr_id pk_seq_nr' ||
                        chr(10) || 'from kgc_stoftestgroepen stgr' ||
                        chr(10) || 'where stgr.vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select omschrijving' || chr(10) || 'from kgc_stoftestgroepen' ||
                        chr(10) ||'where stgr_id = <ID>';
      ins_impa( kgc_import.g_cimpw_tab_stgr
              ,'Stoftestgroepen');
-- kafd_id
      ------------------------------------------------------
      -- bestand_types
      v_lov_query    := 'select to_char(btyp.btyp_id) code, btyp.code || '' - '' || btyp.omschrijving description, btyp_id pk_seq_nr' ||
                        chr(10) || 'from kgc_bestand_types btyp' ||
                        chr(10) || 'where btyp.vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select omschrijving' || chr(10) || 'from kgc_bestand_types' ||
                        chr(10) ||'where btyp_id = <ID>';
      ins_impa( kgc_import.g_cimpw_tab_btyp
              ,'Bestandtypes');
      ------------------------------------------------------
      -- onderzoekstatus
      v_lov_query    := 'select ''G'' code, ''Goedgekeurd'' description, 1 pk_seq_nr from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''V'', ''Vervallen'', 2 from dual' || chr(10) ||
                        'union' || chr(10) || 'select ''B'', ''Beoordelen'', 3 from dual' || chr(10) ||
                        'order by 3';
      v_lookup_query := 'select decode( ''<ID>''' || chr(10) || ',''G'',''Goedgekeurd''' || chr(10) ||
                        ',''V'',''Vervallen''' || chr(10) || ',''B'',''Beoordelen''' || chr(10) ||
                        ') from dual';
      ins_impa( kgc_import.g_cimpw_dom_onde_status
              ,'Onderzoekstatussen');

      ------------------------------------------------------
      -- posities
      v_lov_query    := 'select to_char(posi.posi_id) code, posi.code || '' - '' || posi.omschrijving description, posi_id pk_seq_nr' ||
                        chr(10) || 'from kgc_posities posi' ||
                        chr(10) || 'where posi.vervallen = ''N''' ||
                        chr(10) || 'order by 2';
      v_lookup_query := 'select omschrijving' || chr(10) || 'from kgc_posities' ||
                        chr(10) ||'where posi_id = <ID>';
      ins_impa( kgc_import.g_cimpw_tab_posi
              ,'Posities');

      COMMIT;
   END init_impa;
   -- retourneer tekst voor dynamische LOV in forms
   FUNCTION parameter_lov(p_impa_id IN NUMBER
                         ,p_imlo_id IN NUMBER := NULL) RETURN VARCHAR2 IS
      v_return VARCHAR2(2000);
      CURSOR impa_cur IS
         SELECT lov_query
           FROM kgc_import_parameters
          WHERE impa_id = p_impa_id;
   BEGIN
      OPEN impa_cur;
      FETCH impa_cur
         INTO v_return;
      CLOSE impa_cur;
      RETURN(v_return);
   END parameter_lov;
   -- retourneer lookup-waarde
   FUNCTION parameter_lookup(p_impa_id      IN NUMBER
                            ,p_helix_waarde IN VARCHAR2
                            ,p_imlo_id      IN NUMBER := NULL) RETURN VARCHAR2 IS
      v_return VARCHAR2(2000);
      CURSOR impa_cur IS
         SELECT lookup_query
           FROM kgc_import_parameters
          WHERE impa_id = p_impa_id;
      v_statement VARCHAR2(2000);
   BEGIN
      IF (p_helix_waarde IS NOT NULL)
      THEN
         OPEN impa_cur;
         FETCH impa_cur
            INTO v_statement;
         CLOSE impa_cur;
         v_statement := REPLACE(v_statement
                               ,'<ID>'
                               ,p_helix_waarde);
         IF (v_statement IS NOT NULL)
         THEN
            BEGIN
               v_return := kgc_util_00.dyn_exec(v_statement);
            EXCEPTION
               WHEN OTHERS THEN
                  v_return := NULL;
            END;
         END IF;
      ELSE
         v_return := NULL;
      END IF;
      RETURN(v_return);
   END parameter_lookup;
   FUNCTION helix_id(p_imlo_id    IN NUMBER
                    ,p_entiteit   IN VARCHAR2
                    ,p_externe_id IN VARCHAR2
                    ,p_hint       IN VARCHAR2) RETURN VARCHAR2 IS
      v_return     VARCHAR2(2000);
      v_entiteit   VARCHAR2(30);
      v_externe_id VARCHAR2(2000);
      v_impw_id    NUMBER;
      CURSOR impw_cur(b_impw_id IN NUMBER) IS
         SELECT helix_waarde
           FROM kgc_import_param_waarden
          WHERE impw_id = b_impw_id;
      CURSOR dflt_cur(b_entiteit IN VARCHAR2) IS
         SELECT helix_waarde
           FROM kgc_import_param_waarden
          WHERE imlo_id = p_imlo_id
            AND helix_table_domain = upper(b_entiteit)
            AND upper(hint) LIKE '%STANDAARD%WAARDE%';
   BEGIN
      IF (p_entiteit = 'KAFD_ONDE')
      THEN
         v_entiteit   := 'KGC_KGC_AFDELINGEN';
         v_externe_id := nvl(p_externe_id
                            ,'Onderzoek');
      ELSIF (p_entiteit = 'KAFD_MONS')
      THEN
         v_entiteit   := 'KGC_KGC_AFDELINGEN';
         v_externe_id := nvl(p_externe_id
                            ,'Monsterafname');
      ELSIF (p_entiteit = 'ONGR_ONDE')
      THEN
         v_entiteit   := 'KGC_ONDERZOEKSGROEPEN';
         v_externe_id := nvl(p_externe_id
                            ,'Onderzoek');
      ELSIF (p_entiteit = 'ONGR_MONS')
      THEN
         v_entiteit   := 'KGC_ONDERZOEKSGROEPEN';
         v_externe_id := nvl(p_externe_id
                            ,'Monsterafname');
      ELSE
         v_entiteit   := upper(p_entiteit);
         v_externe_id := p_externe_id;
      END IF;
      v_impw_id := getimpwid(p_imlo_id            => p_imlo_id
                            ,p_helix_table_domain => v_entiteit
                            ,p_externe_id         => v_externe_id
                            ,p_hint               => p_hint
                            ,p_toevoegen          => FALSE);
      IF (v_impw_id IS NOT NULL)
      THEN
         OPEN impw_cur(v_impw_id);
         FETCH impw_cur
            INTO v_return;
         CLOSE impw_cur;
      ELSE
         OPEN dflt_cur(v_entiteit);
         FETCH dflt_cur
            INTO v_return;
         CLOSE dflt_cur;
      END IF;
      RETURN(v_return);
   END helix_id;

   PROCEDURE koppel_faon_pers_onde(p_imfi_id IN     NUMBER
                                  ,p_pers_id IN     NUMBER
                                  ,p_faon_id IN     NUMBER := NULL
                                  ,p_onde_id IN     NUMBER := NULL
                                  ,x_verslag IN OUT VARCHAR2
                                  ) IS
   /**************************************************************************************
   ** RDE, 15-09-2009:
   ** Koppel alleen indien alle 3 de onderdelen aanwezig zij, en er is nog geen faon
   ** mogelijk moet onderzoek bij faon gezocht worden of anderom
   **************************************************************************************/
      CURSOR c_faon_zoek IS
         SELECT imfz.kgc_faon_id
           FROM kgc_im_familie_onderzoeken imfz
              , kgc_im_onderzoeken imon
              , kgc_im_onderzoek_indicaties imoi
          WHERE imfz.imfi_id = p_imfi_id
            AND imon.imfi_id = p_imfi_id
            AND imon.kgc_onde_id = p_onde_id
            AND imoi.imfi_id = p_imfi_id
            AND imoi.imon_id = imon.imon_id
            AND imoi.impw_id_indi = imfz.impw_id_indi
          ;
      CURSOR c_onde_zoek IS
         SELECT imon.kgc_onde_id
           FROM kgc_im_familie_onderzoeken imfz
              , kgc_im_onderzoeken imon
              , kgc_im_onderzoek_indicaties imoi
          WHERE imfz.imfi_id = p_imfi_id
            AND imfz.kgc_faon_id = p_faon_id
            AND imon.imfi_id = p_imfi_id
            AND imoi.imfi_id = p_imfi_id
            AND imoi.imon_id = imon.imon_id
            AND imoi.impw_id_indi = imfz.impw_id_indi
          ;
      v_faon_id NUMBER := p_faon_id;
      v_onde_id NUMBER := p_onde_id;
      v_count   NUMBER;
   BEGIN
      IF v_faon_id IS NULL
      THEN
         OPEN c_faon_zoek;
         FETCH c_faon_zoek INTO v_faon_id;
         CLOSE c_faon_zoek;
      ELSIF v_onde_id IS NULL
      THEN
         OPEN c_onde_zoek;
         FETCH c_onde_zoek INTO v_onde_id;
         CLOSE c_onde_zoek;
      END IF;
      IF v_faon_id IS NULL OR
         v_onde_id IS NULL OR
         p_pers_id IS NULL
      THEN
         RETURN;
      END IF;
      SELECT COUNT(*)
        INTO v_count
        FROM kgc_faon_betrokkenen fobe
       WHERE fobe.pers_id = p_pers_id
         AND fobe.onde_id = v_onde_id
         AND fobe.faon_id = v_faon_id;
      IF v_count = 0
      THEN
         INSERT INTO kgc_faon_betrokkenen (pers_id, onde_id, faon_id)
         VALUES (p_pers_id, v_onde_id, v_faon_id);
         addtext(x_verslag
                ,v_max_len
                ,'Bij familieonderzoek ' || kgc_util_00.pk2uk('FAON', v_faon_id, 'FAMILIEONDERZOEKNR') ||
                 ' is ' || kgc_pers_00.persoon_info(p_pers_id => p_pers_id, p_lengte => 'LANG') ||
                 ' betrokken.');
      END IF;
   END koppel_faon_pers_onde;

   PROCEDURE koppel_onderzoek_monster(p_imfi_id IN kgc_import_files.imfi_id%TYPE
                                     ,p_immo_id IN kgc_im_monsters.immo_id%TYPE := NULL
                                     ,p_imon_id IN kgc_im_onderzoeken.imon_id%TYPE := NULL
                                     ,x_fracties_betrekken IN OUT VARCHAR2
                                     ,x_verslag IN OUT VARCHAR2
                                     ) IS
      /*******************************************************************************
      * RDE 15-09-2009: Mantis 3184: Interface voor Dragon                           *
      *                 - nu niet voor 1 onderzoek maar evt voor alle!               *
      *                 - aanroep na onderzoek-toevoegen en monster-toevoegen        *
      *                 - parameters omon_id en verslag toegevoegd                   *
      * NtB, 06-02-2009, 740: verplaatst uit mosos-package, is nu generieke          *
      *                       procedure.                                             *
      *                       We proberen nu (nog) binnen het bericht een onderzoek  *
      *                       aan het monster te koppelen, indien er een relatie is  *
      *                       opgegeven tussen im_onderzoek en im_monster en er      *
      *                       nog geen koppeling bestond binnen helix.               *
      * RDE 20-03-2008: hier moet nog een nieuwe faciliteit voor komen (die ook      *
      *                 voldoet voor Astraia);                                       *
      *                 een tijdelijke workaround: het onderzoek en monster moeten   *
      *                 in hetzelfde imfi zitten (maar dit is niet voldoende voor    *
      *                 Astraia)                                                     *
      *******************************************************************************/
      -- selecteer alle onderzoeken die in aanmerking komen
      CURSOR c_imon
      (b_imfi_id kgc_import_files.imfi_id%TYPE
      ,b_imon_id kgc_im_onderzoeken.imon_id%TYPE
      ,b_immo_id kgc_im_monsters.immo_id%TYPE
      ) IS
         SELECT imon.kgc_onde_id
              , imon.immo_id
              , onde.onderzoeknr
              , onde.kafd_id
              , onde.ongr_id
           FROM kgc_im_onderzoeken imon
              , kgc_onderzoeken onde
          WHERE imon.imfi_id = b_imfi_id
            AND imon.kgc_onde_id = onde.onde_id
            AND (imon.imon_id = b_imon_id OR b_imon_id IS NULL)
            AND imon.immo_id IS NOT NULL
            AND (imon.immo_id = b_immo_id OR b_immo_id IS NULL);

      CURSOR c_immo
      (b_imfi_id kgc_import_files.imfi_id%TYPE
      ,b_immo_id kgc_im_monsters.immo_id%TYPE
      ) IS
         SELECT immo.kgc_mons_id
              , mons.monsternummer
           FROM kgc_im_monsters immo
              , kgc_monsters mons
          WHERE immo.imfi_id = b_imfi_id
            AND immo.kgc_mons_id = mons.mons_id
            AND immo.immo_id = b_immo_id;
      r_immo c_immo%ROWTYPE;

      CURSOR c_onmo
      (b_onde_id kgc_onderzoek_monsters.onde_id%TYPE -- NtB:
      , b_mons_id kgc_onderzoek_monsters.mons_id%TYPE -- NtB:
      ) IS
         SELECT onmo_id
           FROM kgc_onderzoek_monsters onmo
          WHERE onmo.onde_id = b_onde_id
            AND onmo.mons_id = b_mons_id;
      r_onmo c_onmo%ROWTYPE;

      CURSOR frac_cur(p_mons_id NUMBER)
      IS
        SELECT frac.frac_id
        FROM   bas_fracties frac
        WHERE  frac.mons_id = p_mons_id
        AND    NVL( frac.status, 'O' ) IN ( 'A', 'O' );
      frac_rec1 frac_cur%ROWTYPE;
      frac_rec2 frac_cur%ROWTYPE;

      CURSOR betr_cur(p_onde_id NUMBER, p_frac_id NUMBER) IS
         SELECT NULL
           FROM bas_metingen meti
          WHERE meti.onde_id = p_onde_id
            AND meti.frac_id = NVL(p_frac_id, meti.frac_id);
      betr_rec          betr_cur%ROWTYPE;
      v_frac_betr_maken VARCHAR2(1);
   BEGIN
      -- Init

      -- Moet er wel gekoppeld worden; eerst zoeken naar gekoppeld im_onderzoek
      -- (nieuw of bijgewerkt) en evt. kgc_onde_id ophalen
      FOR r_imon IN c_imon(b_imfi_id => p_imfi_id
                          ,b_imon_id => p_imon_id
                          ,b_immo_id => p_immo_id)
      LOOP
         v_frac_betr_maken := UPPER( SUBSTR( kgc_sypa_00.systeem_waarde( 'FRACTIE_ONDERZOEK_BETROKKEN', r_imon.kafd_id, r_imon.ongr_id ), 1, 1 ) );
         -- Kgc_mons_id er bij halen (nieuw of bijgewerkt)
         OPEN c_immo(b_imfi_id => p_imfi_id
                    ,b_immo_id => r_imon.immo_id); -- NtB, 05-02-2009: extra voorwaarde
         FETCH c_immo
            INTO r_immo;
         IF c_immo%FOUND
         THEN -- alleen dan een poging doen
            -- NtB: nav. rde 20-03-2008 verbetering:
            -- bestaat er al een koppeling in helix?
            OPEN c_onmo(r_imon.kgc_onde_id
                       ,r_immo.kgc_mons_id);
            FETCH c_onmo
               INTO r_onmo;
            IF c_onmo%NOTFOUND
            THEN
               -- nu koppelen via insert into kgc_onderzoek_monsters
               INSERT INTO kgc_onderzoek_monsters
                  (onde_id
                  ,mons_id)
               VALUES
                  (r_imon.kgc_onde_id
                  ,r_immo.kgc_mons_id)
               RETURNING onmo_id INTO r_onmo.onmo_id;
               addtext(x_verslag
                      ,v_max_len
                      ,'Monster ' || r_immo.monsternummer || ' is gekoppeld aan onderzoek ' || r_imon.onderzoeknr || '.');
            END IF;
            CLOSE c_onmo;
            -- daarna eventueel een fractie betrokken maken - maar alleen als p_immo_id gevuld is... anders komt deze melding meerdere keren!
            IF v_frac_betr_maken = 'J' AND p_immo_id IS NOT NULL
            THEN
               -- alleen betrekken als er 1 fractie is...
               OPEN frac_cur(p_mons_id => r_immo.kgc_mons_id);
               FETCH frac_cur INTO frac_rec1;
               IF frac_cur%FOUND THEN
                  FETCH frac_cur INTO frac_rec2;
                  IF frac_cur%NOTFOUND
                  THEN -- dan is er maar 1; alleen betrekken indien nog niet betrokken
                     OPEN betr_cur(p_onde_id => r_imon.kgc_onde_id
                                  ,p_frac_id => frac_rec1.frac_id
                                  );
                     FETCH betr_cur INTO betr_rec;
                     IF betr_cur%NOTFOUND
                     THEN
                         INSERT INTO bas_metingen
                         ( frac_id
                         , onmo_id
                         , onde_id
                         )
                         VALUES
                         ( frac_rec1.frac_id
                         , r_onmo.onmo_id
                         , r_imon.kgc_onde_id
                         );
                     END IF;
                     CLOSE betr_cur;
                  END IF;
               END IF;
               CLOSE frac_cur;
               -- check: is er een fractie betrokken bij het onderzoek?
               OPEN betr_cur(p_onde_id => r_imon.kgc_onde_id
                            ,p_frac_id => NULL
                            );
               FETCH betr_cur INTO betr_rec;
               IF betr_cur%NOTFOUND
               THEN
                  addtext(x_verslag
                         ,v_max_len
                         ,'Bij onderzoek ' || r_imon.onderzoeknr || ' kon geen fractie betrokken gemaakt worden; maak er zelf 1 betrokken.');
                  x_fracties_betrekken := 'J';
               END IF;
               CLOSE betr_cur;
            END IF;
         END IF;
         CLOSE c_immo;
      END LOOP; -- c_imon
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
         -- NtB: moet dit nog anders????

   END koppel_onderzoek_monster;

   PROCEDURE vertaal_naar_helix(p_imfi_id       IN NUMBER
                               ,p_online        IN BOOLEAN  := TRUE
                               ,p_beheer_in_zis IN VARCHAR2 := 'N') IS

      TYPE t_tab_ids IS TABLE OF BOOLEAN INDEX BY VARCHAR2(41); -- sleutel = id's van rij uit tabel IM_ zonder IMFI (want deze is parameter en constant per toe te voegen set)

      v_tab_impe_err             t_tab_ids;
      v_tab_imzw_err             t_tab_ids;
      v_tab_imfo_err             t_tab_ids;
      v_tab_immo_err             t_tab_ids;
      v_tab_imfr_err             t_tab_ids;
      v_tab_imon_err             t_tab_ids;
      v_tab_imoi_err             t_tab_ids;
      v_tab_imok_err             t_tab_ids;
      v_tab_imme_err             t_tab_ids;
      v_tab_imui_err             t_tab_ids;
      v_tab_imob_err             t_tab_ids;
      v_tab_imfa_err             t_tab_ids;
      v_tab_imfz_err             t_tab_ids;
      v_count                    NUMBER;
      v_count_best               NUMBER;
      v_nrofonin                 NUMBER;
      v_nrofonkh                 NUMBER;
      v_nroffrac                 NUMBER;
      v_nrofuits                 NUMBER;
      v_nrofmeti                 NUMBER;
      v_nrofonbe                 NUMBER;
      v_nrofonmo                 NUMBER;
      v_sep                      VARCHAR2(10);
      v_sypa_waarde              VARCHAR2(100);
      v_dummie                   VARCHAR2(2000);
      v_bdummie                  BOOLEAN;
      v_imfi_opmerkingen         VARCHAR2(32000); -- foutmeldingen
      v_imfi_opmerkingen_enti    VARCHAR2(32000); -- foutmeldingen per entiteit
      v_error_enti               BOOLEAN; -- foutmelding per entiteit
      v_cg_errmsg                VARCHAR2(32767);
      v_imfi                     cg$kgc_import_files.cg$row_type;
      -- verslag van de verwerking:
      v_imfi_verslag             VARCHAR2(32000); -- := 'Start verwerking ' || TO_CHAR(SYSDATE, 'DD-MM-YYYY HH24:MI:SS') || '; Wijzigingen (was->wordt).';
      v_imfi_verslag_tmp         VARCHAR2(32000);
      v_pers_id_verschillen      VARCHAR2(32000);
      v_auth_pers_fout           VARCHAR2(2000);
      v_bpersiddatawijzigen      BOOLEAN := FALSE;
      v_verwerkingstatus         VARCHAR2(1) := 'D'; -- Doorgaan; mogelijke waarden: E=Error; K=Klaar; D=Doorgaan
      v_verwerkingswijze         VARCHAR2(10);
      v_bverplicht               BOOLEAN := FALSE;
      v_bloskoppelen             BOOLEAN := FALSE;
      v_best_verplaatsen         BOOLEAN := FALSE;

      v_db_waarde                VARCHAR2(4000);
      v_pers_old_anoniem         VARCHAR2(1);
      v_impe                     cg$kgc_im_personen.cg$row_type;
      v_impe_null                cg$kgc_im_personen.cg$row_type;
      v_pers_old                 cg$kgc_personen.cg$row_type;
      v_pers_new                 cg$kgc_personen.cg$row_type;
      v_pers_null                cg$kgc_personen.cg$row_type;
      v_pers_ind                 cg$kgc_personen.cg$ind_type;
      v_pers_ind_false           cg$kgc_personen.cg$ind_type;

      v_zwan_old                 cg$kgc_zwangerschappen.cg$row_type;
      v_zwan_new                 cg$kgc_zwangerschappen.cg$row_type;
      v_zwan_null                cg$kgc_zwangerschappen.cg$row_type;
      v_zwan_ind                 cg$kgc_zwangerschappen.cg$ind_type;
      v_zwan_ind_false           cg$kgc_zwangerschappen.cg$ind_type;

      v_foet_old                 cg$kgc_foetussen.cg$row_type;
      v_foet_new                 cg$kgc_foetussen.cg$row_type;
      v_foet_null                cg$kgc_foetussen.cg$row_type;
      v_foet_ind                 cg$kgc_foetussen.cg$ind_type;
      v_foet_ind_false           cg$kgc_foetussen.cg$ind_type;

      v_onde_old                 cg$kgc_onderzoeken.cg$row_type;
      v_onde_new                 cg$kgc_onderzoeken.cg$row_type;
      v_onde_null                cg$kgc_onderzoeken.cg$row_type;
      v_onde_ind                 cg$kgc_onderzoeken.cg$ind_type;
      v_onde_ind_false           cg$kgc_onderzoeken.cg$ind_type;

      v_onin_old                 cg$kgc_onderzoek_indicaties.cg$row_type;
      v_onin_new                 cg$kgc_onderzoek_indicaties.cg$row_type;
      v_onin_null                cg$kgc_onderzoek_indicaties.cg$row_type;
      v_onin_ind                 cg$kgc_onderzoek_indicaties.cg$ind_type;
      v_onin_ind_false           cg$kgc_onderzoek_indicaties.cg$ind_type;

      v_onkh_old                 cg$kgc_onderzoek_kopiehouders.cg$row_type;
      v_onkh_new                 cg$kgc_onderzoek_kopiehouders.cg$row_type;
      v_onkh_null                cg$kgc_onderzoek_kopiehouders.cg$row_type;
      v_onkh_ind                 cg$kgc_onderzoek_kopiehouders.cg$ind_type;
      v_onkh_ind_false           cg$kgc_onderzoek_kopiehouders.cg$ind_type;

      v_uits_old                 cg$kgc_uitslagen.cg$row_type;
      v_uits_new                 cg$kgc_uitslagen.cg$row_type;
      v_uits_null                cg$kgc_uitslagen.cg$row_type;
      v_uits_ind                 cg$kgc_uitslagen.cg$ind_type;
      v_uits_ind_false           cg$kgc_uitslagen.cg$ind_type;

      v_mons_old                 cg$kgc_monsters.cg$row_type;
      v_mons_new                 cg$kgc_monsters.cg$row_type;
      v_mons_null                cg$kgc_monsters.cg$row_type;
      v_mons_ind                 cg$kgc_monsters.cg$ind_type;
      v_mons_ind_false           cg$kgc_monsters.cg$ind_type;

      v_frac_old                 cg$bas_fracties.cg$row_type;
      v_frac_new                 cg$bas_fracties.cg$row_type;
      v_frac_null                cg$bas_fracties.cg$row_type;
      v_frac_ind                 cg$bas_fracties.cg$ind_type;
      v_frac_ind_false           cg$bas_fracties.cg$ind_type;

      v_onbe_old                 cg$kgc_onderzoek_betrokkenen.cg$row_type;
      v_onbe_new                 cg$kgc_onderzoek_betrokkenen.cg$row_type;
      v_onbe_null                cg$kgc_onderzoek_betrokkenen.cg$row_type;
      v_onbe_ind                 cg$kgc_onderzoek_betrokkenen.cg$ind_type;
      v_onbe_ind_false           cg$kgc_onderzoek_betrokkenen.cg$ind_type;

      v_onmo_old                 cg$kgc_onderzoek_monsters.cg$row_type;
      v_onmo_new                 cg$kgc_onderzoek_monsters.cg$row_type;
      v_onmo_null                cg$kgc_onderzoek_monsters.cg$row_type;
      v_onmo_ind                 cg$kgc_onderzoek_monsters.cg$ind_type;
      v_onmo_ind_false           cg$kgc_onderzoek_monsters.cg$ind_type;

      v_meti_old                 cg$bas_metingen.cg$row_type;
      v_meti_new                 cg$bas_metingen.cg$row_type;
      v_meti_null                cg$bas_metingen.cg$row_type;
      v_meti_ind                 cg$bas_metingen.cg$ind_type;
      v_meti_ind_false           cg$bas_metingen.cg$ind_type;

      v_fami_old                 cg$kgc_families.cg$row_type;
      v_fami_new                 cg$kgc_families.cg$row_type;
      v_fami_null                cg$kgc_families.cg$row_type;
      v_fami_ind                 cg$kgc_families.cg$ind_type;
      v_fami_ind_false           cg$kgc_families.cg$ind_type;

      v_faon_old                 cg$kgc_familie_onderzoeken.cg$row_type;
      v_faon_new                 cg$kgc_familie_onderzoeken.cg$row_type;
      v_faon_null                cg$kgc_familie_onderzoeken.cg$row_type;
      v_faon_ind                 cg$kgc_familie_onderzoeken.cg$ind_type;
      v_faon_ind_false           cg$kgc_familie_onderzoeken.cg$ind_type;

      v_fale_count               NUMBER;
      v_bdezefoetuswijzigen      BOOLEAN;
      v_imfi_afgehandeld         VARCHAR2(1);
      v_mons_koppel              BOOLEAN := FALSE;
      v_err_id                   VARCHAR2(200);
      v_opm_tab_en_id            VARCHAR2(200);
      v_opm_tab_en_id_pers       VARCHAR2(200);
      v_datumformaatdoel         VARCHAR2(30) := 'DD-MM-YYYY';
      v_fracties_betrekken       VARCHAR2(1) := 'N'; -- rework Mantis 3184: Interface voor Dragon: moeten er nog fracties betrokken worden? Zo ja: niet afgehandeld!
      v_impe_count               NUMBER;
      v_c_checking               CONSTANT NUMBER := 1;
      v_c_updating               CONSTANT NUMBER := 2;
      v_pers_id_fami             NUMBER;
      v_datum_brie_print         DATE;
      v_waarde                   kgc_attribuut_waarden.waarde%TYPE;
      v_attr_code                kgc_attributen.code%TYPE;
      v_logregel                 VARCHAR2(4000);

      CURSOR c_imfi IS
         SELECT imfi.imlo_id
               ,imfi.opmerkingen
               ,imfi.afgehandeld
               ,imfi.filenaam
               ,imfi.specifieke_verwerking
               ,imfi.last_update_date
               ,imfi.creation_date
               ,imlo.zisnr_overnemen
               ,imlo.gegroepeerde_verwerking
               ,imlo.onde_mons_toevoegen
               ,imlo.interface_type
               ,imlo.standaard_logica
               ,imlo.directorynaam
           FROM kgc_import_files    imfi
               ,kgc_import_locaties imlo
          WHERE imfi.imfi_id = p_imfi_id
            AND imfi.imlo_id = imlo.imlo_id;
      --
      r_imfi c_imfi%ROWTYPE;

      -- let op: een generieke 'importfile' wordt geidentificeerd door het eerste stuk voor de spatie!
      CURSOR c_imfi_fami_zoek(p_imlo_id NUMBER, p_filenaam VARCHAR2) IS
         SELECT imfa.imfa_id
              , imfi.filenaam
              , imfa.kgc_fami_id
              , fami.familienummer
           FROM kgc_import_files    imfi
               ,kgc_im_families imfa
               ,kgc_families fami
          WHERE imfi.imlo_id = p_imlo_id
            AND imfi.filenaam like p_filenaam || ' %'
            AND imfi.imfi_id = imfa.imfi_id (+)
            AND imfa.kgc_fami_id = fami.fami_id (+);
      r_imfi_fami_zoek c_imfi_fami_zoek%ROWTYPE;

      CURSOR c_impe(p_imfi_id NUMBER) IS
         SELECT *
           FROM kgc_im_personen impe
          WHERE impe.imfi_id = p_imfi_id
          ORDER BY DECODE(impe.anoniem, 'N', 1, 2);
         --    FOR UPDATE nowait

      -- aanpassing Mantis 5715: rekening houden met tekenvervanging
      CURSOR c_pers1(p_zoeknaam VARCHAR2, p_geboortedatum DATE, p_geslacht VARCHAR2) IS
         SELECT pers.pers_id
           FROM kgc_personen pers
          WHERE pers.geslacht = p_geslacht
            AND pers.zoeknaam like p_zoeknaam || '%' -- tbv index
            AND kgc_pers_00.formatteer_zoeknaam(pers.achternaam) = p_zoeknaam
            AND pers.geboortedatum = p_geboortedatum;
      r_pers1 c_pers1%ROWTYPE;

      CURSOR c_perszis(p_zisnr VARCHAR2) IS
         SELECT pers.pers_id
           FROM kgc_personen pers
          WHERE zisnr = p_zisnr;

      -- nummer aanvullen met voorloopnullen. mag max 2 zijn, dus indien langer: valt bij controle door de mand, want deze komt dan als eerste!
      CURSOR c_imzw_laatste(p_imfi_id NUMBER, p_impe_id VARCHAR2) IS
         SELECT imzw.*
           FROM kgc_im_zwangerschappen imzw
          WHERE imzw.imfi_id = p_imfi_id
            AND imzw.impe_id = p_impe_id
          ORDER BY decode(nvl(length(nummer)
                          ,0)
                      ,1
                      ,'00' || nummer
                      ,2
                      ,'0' || nummer
                      ,1) DESC;

      CURSOR c_imzw_zoek(p_imfi_id NUMBER, p_imzw_id VARCHAR2) IS
         SELECT kgc_zwan_id
           FROM kgc_im_zwangerschappen imzw
          WHERE imzw.imfi_id = p_imfi_id
            AND imzw.imzw_id = p_imzw_id;

      CURSOR c_zwan_zoek(p_pers_id NUMBER, p_nummer NUMBER) IS
         SELECT zwan_id
               ,datum_aterm
           FROM kgc_zwangerschappen zwan
          WHERE zwan.pers_id = p_pers_id
            AND zwan.nummer = p_nummer;
      r_zwan_zoek c_zwan_zoek%ROWTYPE;

      CURSOR c_imfo(p_imfi_id NUMBER, p_impe_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_foetussen imfo
          WHERE imfo.imfi_id = p_imfi_id
            AND imfo.impe_id_moeder = p_impe_id;

      CURSOR c_imfo_laatst(p_imfi_id NUMBER, p_imzw_id VARCHAR2, p_volgnr VARCHAR2) IS
         SELECT imfo_id
           FROM kgc_im_foetussen imfo
          WHERE imfo.imfi_id = p_imfi_id
            AND imfo.imzw_id = p_imzw_id
            AND imfo.volgnr = p_volgnr
          ORDER BY imfo.creation_date DESC -- laatst ingevoerde eerst
         ;
      r_imfo_laatst c_imfo_laatst%ROWTYPE;

      CURSOR c_foet_zoek(p_pers_id NUMBER, p_volgnr NUMBER, p_zwan_id NUMBER) IS
         SELECT foet_id
           FROM kgc_foetussen foet
          WHERE foet.pers_id_moeder = p_pers_id
            AND foet.volgnr = p_volgnr
            AND nvl(foet.zwan_id
                   ,0) = nvl(p_zwan_id
                            ,0);

      CURSOR c_imok(p_imfi_id NUMBER, p_imon_id VARCHAR2) IS
         SELECT imok.*
           FROM kgc_im_onde_kopie imok
          WHERE imok.imfi_id = p_imfi_id
            AND imok.imon_id = p_imon_id;

      CURSOR c_imon(p_imfi_id NUMBER, p_impe_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_onderzoeken imon
          WHERE imon.imfi_id = p_imfi_id
            AND imon.impe_id = p_impe_id;

      CURSOR c_onde_zoek(p_pers_id NUMBER, p_datum DATE, p_ongr_id NUMBER, p_foet_id NUMBER, p_onderzoekswijze VARCHAR2, p_referentie VARCHAR2) IS
         SELECT onde.onde_id
              , onde.onderzoeknr
           FROM kgc_onderzoeken onde
          WHERE onde.pers_id = p_pers_id
            AND onde.datum_binnen = p_datum
            AND onde.ongr_id = p_ongr_id
            AND (p_foet_id IS NULL OR onde.foet_id = p_foet_id)
            AND (p_onderzoekswijze IS NULL OR onde.onderzoekswijze = p_onderzoekswijze)
            AND (p_referentie IS NULL OR onde.referentie = p_referentie);
      r_onde_zoek c_onde_zoek%ROWTYPE;

      CURSOR c_imoi(p_imfi_id NUMBER, p_imon_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_onderzoek_indicaties imoi
          WHERE imoi.imfi_id = p_imfi_id
            AND imoi.imon_id = p_imon_id;

      CURSOR c_imui(p_imfi_id NUMBER, p_imon_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_uitslagen imui
          WHERE imui.imfi_id = p_imfi_id
            AND imui.imon_id = p_imon_id;

      -- waarde1 ophalen - letter_monsternr!
      CURSOR c_mate(p_mate_id NUMBER) IS
         SELECT *
           FROM kgc_materialen mate
          WHERE mate.mate_id = p_mate_id;
      r_mate      c_mate%ROWTYPE;
      r_mate_null c_mate%ROWTYPE;

      CURSOR c_immo(p_imfi_id NUMBER, p_impe_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_monsters immo
          WHERE immo.imfi_id = p_imfi_id
            AND immo.impe_id = p_impe_id;

      CURSOR c_immo1(p_imfi_id NUMBER, p_immo_id VARCHAR2, p_onde_id NUMBER) IS
         SELECT mons.mons_id
              , mons.monsternummer
              , onmo.onmo_id
           FROM kgc_im_monsters immo
              , kgc_monsters mons
              , kgc_onderzoek_monsters onmo
          WHERE immo.imfi_id = p_imfi_id
            AND immo.immo_id = p_immo_id
            AND immo.kgc_mons_id = mons.mons_id(+)
            AND mons.mons_id = onmo.mons_id(+)
            AND p_onde_id = onmo.onde_id;
      r_immo1 c_immo1%ROWTYPE;

      CURSOR c_imfr(p_imfi_id NUMBER, p_immo_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_fracties imfr
          WHERE imfr.imfi_id = p_imfi_id
            AND imfr.immo_id = p_immo_id;

      CURSOR c_imfr1(p_imfi_id NUMBER, p_immo_id VARCHAR2, p_imfr_id VARCHAR2) IS
         SELECT frac.frac_id
              , frac.fractienummer
           FROM kgc_im_fracties imfr
              , bas_fracties frac
          WHERE imfr.imfi_id = p_imfi_id
            AND imfr.immo_id = p_immo_id
            AND imfr.imfr_id = p_imfr_id
            AND imfr.kgc_frac_id = frac.frac_id(+);
      r_imfr1 c_imfr1%ROWTYPE;

      CURSOR c_frac_zoek(p_fractienummer VARCHAR2) IS
         SELECT frac.frac_id
           FROM bas_fracties frac
          WHERE frac.fractienummer = p_fractienummer;
      r_frac_zoek c_frac_zoek%ROWTYPE;

      CURSOR c_imob(p_imfi_id NUMBER, p_imon_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_onderzoek_betrokkenen imob
          WHERE imob.imfi_id = p_imfi_id
            AND imob.imon_id = p_imon_id;

      CURSOR c_imme(p_imfi_id NUMBER, p_imon_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_metingen imme
          WHERE imme.imfi_id = p_imfi_id
            AND imme.imon_id = p_imon_id;

      CURSOR c_kafd(p_kafd_id NUMBER) IS
         SELECT kafd.code
           FROM kgc_kgc_afdelingen kafd
          WHERE kafd.kafd_id = p_kafd_id;
      r_kafd c_kafd%ROWTYPE;

      CURSOR c_imfa(p_imfi_id NUMBER) IS
         SELECT *
           FROM kgc_im_families imfa
          WHERE imfa.imfi_id = p_imfi_id;

      CURSOR c_imfz(p_imfi_id NUMBER
                   ,p_imfa_id VARCHAR2
                   ) IS
         SELECT *
           FROM kgc_im_familie_onderzoeken imfz
          WHERE imfz.imfi_id = p_imfi_id
            AND imfz.imfa_id = p_imfa_id;

      CURSOR c_fami_zoek(p_familienummer VARCHAR2) IS
         SELECT fami_id
           FROM kgc_families fami
          WHERE fami.familienummer = p_familienummer;
      r_fami_zoek c_fami_zoek%ROWTYPE;

      CURSOR c_fami_zoek2(p_pers_id NUMBER) IS
         SELECT fami.fami_id
               ,fami.familienummer
           FROM kgc_families fami
              , kgc_familie_leden fale
          WHERE fale.pers_id = p_pers_id
            AND fale.fami_id = fami.fami_id;
      r_fami_zoek2 c_fami_zoek2%ROWTYPE;

      CURSOR c_fale_zoek(p_pers_id NUMBER) IS
         SELECT fami_id
           FROM kgc_familie_leden fale
          WHERE fale.pers_id = p_pers_id;
      r_fale_zoek c_fale_zoek%ROWTYPE;

      CURSOR c_faon_zoek_alg(p_fami_id NUMBER
                            ,p_ongr_id NUMBER
                            ,p_indi_id NUMBER) IS
         SELECT faon_id
           FROM kgc_familie_onderzoeken faon
          WHERE fami_id = p_fami_id
            AND ongr_id = p_ongr_id
            AND indi_id = p_indi_id;

      CURSOR c_faon_zoek_spec(p_fami_id NUMBER
                             ,p_ongr_id NUMBER
                             ,p_indi_id NUMBER -- toegevoegd agv rework 3184: Interface voor Dragon
                             ,p_datum   DATE
                             ,p_rela_id NUMBER) IS
         SELECT faon_id
           FROM kgc_familie_onderzoeken faon
          WHERE fami_id = p_fami_id
            AND ongr_id = p_ongr_id
            AND indi_id = p_indi_id
            AND datum = p_datum
            AND rela_id = p_rela_id;

      PROCEDURE voegtoeaansql(p_waarde     IN VARCHAR2
                             ,p_waarde_oud IN VARCHAR2
                             ,p_type       IN VARCHAR2
                              -- D (datum) of anders
                             ,p_kolomnaam    IN VARCHAR2
                             ,p_quotestoev   IN BOOLEAN
                             ,p_badd         IN BOOLEAN -- quotes om waarde heen?
                             ,x_sql          IN OUT VARCHAR2
                             ,x_sql2         IN OUT VARCHAR2
                             ,x_sep          IN OUT VARCHAR2
                             ,x_imfi_verslag IN OUT VARCHAR2) IS
         v_waarde     VARCHAR2(4000) := p_waarde;
         v_waarde_new VARCHAR2(4000);
         v_waarde_oud VARCHAR2(4000) := p_waarde_oud;
      BEGIN
         IF p_type = 'T' -- timestamp!
         THEN
            v_waarde_new := 'to_timestamp(''' || p_waarde || ''', ''' || g_tijdstip_masker || ''')';

         ELSIF p_type = 'D' -- datum!
         THEN
            v_waarde_new := 'TO_DATE(''' || p_waarde || ''', ''' || v_datumformaatdoel || ''')';

            -- NtB: nav. aanpassingen RDE, 26-02-2008:
         ELSIF p_type = 'DT' -- datumtijd! RDE 20-02-2008
         THEN
            v_waarde_new := 'TO_DATE(''' || p_waarde || ''', ''' || v_datumtijdformaatdoel || ''')';

         ELSIF p_quotestoev
         THEN
            -- rde oud 2008-01-19 - quotes vervangen      v_waarde_new := '''' || p_waarde || '''';
            v_waarde_new := '''' || REPLACE(p_waarde
                                           ,''''
                                           ,'''''') || '''';

         ELSE
            -- rde oud 2008-01-19 - quotes vervangen      v_waarde_new := p_waarde;
            v_waarde_new := REPLACE(p_waarde
                                   ,''''
                                   ,'''''');

         END IF;

         IF p_waarde IS NOT NULL
         THEN
            IF p_badd
            THEN
               x_sql  := x_sql || ', ' || p_kolomnaam || ' ';
               x_sql2 := x_sql2 || ', ' || v_waarde_new;
            ELSE
               -- wijzigen indien iets gewijzigd!
               -- voor een aantal kolommen geldt: casewijziging: niet doorvoeren, evenals puntje verschil (in bv vltrs: RLF = R.L.F)
               IF p_kolomnaam IN
                  ('achternaam', 'aanspreken', 'voorletters', 'voorvoegsel', 'achternaam_partner', 'voorvoegsel_partner')
                  AND upper(REPLACE(p_waarde
                                   ,'.'
                                   ,'')) = upper(REPLACE(p_waarde_oud
                                                            ,'.'
                                                            ,''))
               THEN
                  NULL;
               ELSIF (p_waarde <> p_waarde_oud)
                     OR p_waarde_oud IS NULL
               THEN
                  x_sql := x_sql || x_sep || p_kolomnaam || ' = ' || v_waarde_new;
                  x_sep := ', ';
                  -- tbv verslag: 00:00:00 van datums halen:
                  IF substr(v_waarde_oud
                           ,11
                           ,9) = ' 00:00:00'
                  THEN
                     v_waarde_oud := substr(v_waarde_oud
                                           ,1
                                           ,10);
                  END IF;
                  IF substr(v_waarde
                           ,11
                           ,9) = ' 00:00:00'
                  THEN
                     v_waarde := substr(v_waarde
                                       ,1
                                       ,10);
                  END IF;
                  addtext(x_imfi_verslag
                         ,v_max_len
                         ,p_kolomnaam || '(' || nvl(v_waarde_oud
                                                   ,' ') || '->' || v_waarde || ')'
                         ,'; ');
               END IF;
            END IF;
         END IF;
      EXCEPTION
         WHEN OTHERS THEN
            addtext(x_imfi_verslag
                   ,v_max_len
                   ,'Error OTHERS: VTAS ' || p_kolomnaam || ' = ' || v_waarde_new);
      END voegtoeaansql;
      FUNCTION imxx_error -- staan er fouten in v_tab_im.._err?
      (p_enti VARCHAR2 := '#PERS#ZWAN#FOET#ONDE#ONIN#MONS#ONKH#FAMI#FAON#FRAC#UITS#METI#ONBE#'
       -- rde, mantis 11620: was '#PERS#ZWAN#FOET#ONDE#ONIN#MONS#ONKH#FAMI#FAON#'
       -- lijst enti-codes (sep=#); deze zijn relevant voor foutcontrole
       ) RETURN BOOLEAN IS
         v_id VARCHAR2(100);
      BEGIN
         v_id := v_tab_impe_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#PERS#') > 0
         LOOP
            IF v_tab_impe_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - pers ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_impe_err.NEXT(v_id);
         END LOOP;
         v_id := v_tab_imzw_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#ZWAN#') > 0
         LOOP
            IF v_tab_imzw_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - zwan ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imzw_err.NEXT(v_id);
         END LOOP;
         v_id := v_tab_imfo_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#FOET#') > 0
         LOOP
            IF v_tab_imfo_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - foet ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imfo_err.NEXT(v_id);
         END LOOP;
         v_id := v_tab_imon_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#ONDE#') > 0
         LOOP
            IF v_tab_imon_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - onde ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imon_err.NEXT(v_id);
         END LOOP;
         v_id := v_tab_imoi_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#ONIN#') > 0 -- Mantis 5715: fout hersteld. Was INDI.
         LOOP
            IF v_tab_imoi_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - onin ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imoi_err.NEXT(v_id);
         END LOOP;

         v_id := v_tab_imok_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#ONKH#') > 0
         LOOP
            IF v_tab_imok_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - onkh ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imok_err.NEXT(v_id);
         END LOOP;

         v_id := v_tab_immo_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#MONS#') > 0
         LOOP
            IF v_tab_immo_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - mons ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_immo_err.NEXT(v_id);
         END LOOP;

         v_id := v_tab_imfa_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#FAMI#') > 0
         LOOP
            IF v_tab_imfa_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - fami ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imfa_err.NEXT(v_id);
         END LOOP;

         v_id := v_tab_imfz_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#FAON#') > 0
         LOOP
            IF v_tab_imfz_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - faon ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imfz_err.NEXT(v_id);
         END LOOP;

         -- mantis 11620
         v_id := v_tab_imfr_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#FRAC#') > 0
         LOOP
            IF v_tab_imfr_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - frac ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imfr_err.NEXT(v_id);
         END LOOP;

         v_id := v_tab_imui_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#UITS#') > 0
         LOOP
            IF v_tab_imui_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - uits ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imui_err.NEXT(v_id);
         END LOOP;

         v_id := v_tab_imme_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#METI#') > 0
         LOOP
            IF v_tab_imme_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - meti ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imme_err.NEXT(v_id);
         END LOOP;

         v_id := v_tab_imob_err.FIRST;
         WHILE v_id IS NOT NULL
               AND instr(p_enti
                        ,'#ONBE#') > 0
         LOOP
            IF v_tab_imob_err(v_id)
            THEN
--dbms_output.put_line('imxx_error - onbe ' || v_id);
               RETURN TRUE;
            END IF;
            v_id := v_tab_imob_err.NEXT(v_id);
         END LOOP;

         RETURN FALSE;
      END imxx_error;
      FUNCTION aantal_fracties(p_mons_id NUMBER) RETURN VARCHAR2 IS
         v_count_frac NUMBER := 0;
      BEGIN
         SELECT COUNT(*)
           INTO v_count_frac
           FROM bas_fracties frac
          WHERE frac.mons_id = p_mons_id;
         RETURN '(' || v_count_frac || ' fracties aanwezig)'; -- Rework Mantis 3184: Interface voor Dragon
      END aantal_fracties;
      PROCEDURE copy_notities(p_imfi_id NUMBER, p_entiteit VARCHAR2, p_id NUMBER) IS
         -- kopieer notities van IMFI naar p_entiteit
         -- TENZIJ de notitie bij de entiteit al bestaat (identieke code + omschrijving + commentaar)
         CURSOR c_imfi_noti IS
            SELECT noti_id
            FROM   kgc_notities noti
            WHERE  noti.entiteit = 'IMFI'
            AND    noti.id = p_imfi_id
            AND    noti.code like p_entiteit || '%'
            AND    NOT EXISTS (SELECT 1
                               FROM   kgc_notities noti2
                               WHERE  noti2.entiteit = p_entiteit
                               AND    noti2.id = p_id
                               AND    noti2.code = noti.code
                               AND    noti2.omschrijving = noti.omschrijving
                               AND    NVL(noti2.commentaar, CHR(2)) = NVL(noti.commentaar, CHR(2))
                              )
            ;
         v_noti_trow            cg$kgc_notities.cg$row_type;
         v_noti_trow_null       cg$kgc_notities.cg$row_type;
      BEGIN
         FOR r_imfi_noti IN c_imfi_noti LOOP
            v_noti_trow := v_noti_trow_null;
            v_noti_trow.noti_id := r_imfi_noti.noti_id;
            cg$kgc_notities.slct(v_noti_trow);
            v_noti_trow.the_rowid := NULL;
            v_noti_trow.noti_id := NULL;
            v_noti_trow.entiteit := p_entiteit;
            v_noti_trow.id := p_id;
            cg$kgc_notities.ins(cg$rec => v_noti_trow
                               ,cg$ind => cg$kgc_notities.cg$ind_true);
         END LOOP;
      END copy_notities;
      FUNCTION bepaal_foetus_volgnr(p_pers_id NUMBER
                                   ,x_imfo_rec IN OUT kgc_im_foetussen%ROWTYPE
                                   ,x_errormessage IN OUT VARCHAR2
                                   ) RETURN BOOLEAN IS
         CURSOR c_foet_per (p_pers_id NUMBER
                           ,p_geplande_geboortedatum DATE
                           ,p_commentaar VARCHAR2) IS
            SELECT foet.foet_id
            FROM   kgc_foetussen foet
            WHERE  foet.pers_id_moeder = p_pers_id
            AND    p_geplande_geboortedatum BETWEEN ADD_MONTHS(foet.geplande_geboortedatum, -9) AND foet.geplande_geboortedatum + 14
            AND    (p_commentaar IS NULL OR foet.commentaar IS NULL OR UPPER(REPLACE(p_commentaar, ' ')) = UPPER(REPLACE(foet.commentaar, ' ')))
            ;
         r_foet_per c_foet_per%ROWTYPE;
         r_foet_per2 c_foet_per%ROWTYPE;
         v_geplande_geboortedatum DATE;
         v_count   NUMBER := 0;
         v_return  BOOLEAN := FALSE;
      BEGIN
         IF p_pers_id IS NULL THEN -- moeder kon niet bepaald worden - dit wordt een nieuwe persoon, OF de foutmelding komt van elders!
            x_imfo_rec.volgnr := 1;
            RETURN TRUE;
         END IF;
         -- selecteer foetus in periode 9 mnd voor tm 2 weken na

         v_geplande_geboortedatum := to_date(x_imfo_rec.geplande_geboortedatum
                                            ,v_datumformaatdoel); -- evt exception; is ok
         OPEN c_foet_per(p_pers_id
                        ,v_geplande_geboortedatum
                        ,NULL);
         FETCH c_foet_per INTO r_foet_per;
         IF c_foet_per%NOTFOUND THEN -- nieuw volgnr uitgeven!
            CLOSE c_foet_per;
            SELECT NVL(MAX(foet.volgnr), 0) + 1
            INTO   x_imfo_rec.volgnr
            FROM   kgc_foetussen foet
            WHERE  foet.pers_id_moeder = p_pers_id;
            v_return := TRUE;
         ELSE -- alle doorlopen, nu met evt selectie op commentaar
            CLOSE c_foet_per;
            FOR r_foet_per IN c_foet_per(p_pers_id
                                        ,v_geplande_geboortedatum
                                        ,x_imfo_rec.commentaar)
            LOOP
               v_count := v_count + 1;
               r_foet_per2 := r_foet_per;
            END LOOP;
            IF v_count = 0 THEN
               addtext(x_errormessage
                      ,v_max_len
                      ,'Bepaling foetus.volgnr: wel 1 of meerdere foetussen gevonden, maar geen met overeenkomstig commentaar; koppel handmatig!');
            ELSIF v_count = 1 THEN -- gewenste gevonden!
               x_imfo_rec.kgc_foet_id := r_foet_per2.foet_id;
               v_return := TRUE;
            ELSIF v_count > 1 THEN
               addtext(x_errormessage
                      ,v_max_len
                      ,'Bepaling foetus.volgnr: ' || v_count || ' foetussen gevonden die voldoen; koppel handmatig!');
            END IF;
         END IF;
         RETURN v_return;
      EXCEPTION
         WHEN OTHERS THEN
            RETURN FALSE;
      END bepaal_foetus_volgnr;

   BEGIN -- PROCEDURE vertaal_naar_helix
      reset_im2hlx;
      -- import opzoeken
      OPEN c_imfi;
      FETCH c_imfi
         INTO r_imfi;
      IF c_imfi%NOTFOUND
      THEN
         CLOSE c_imfi;
         raise_application_error(-20000
                                ,'Astraia import met id ' || to_char(p_imfi_id) || ' / naam ' || r_imfi.filenaam ||
                                 ' is niet gevonden!');
      END IF;
      CLOSE c_imfi;
      v_imfi.imfi_id := p_imfi_id;
      cg$kgc_import_files.slct(v_imfi);
      IF r_imfi.afgehandeld = 'J'
      THEN
         -- geen aktie; ook geen melding, want mogelijk staat er al een melding dat
         -- er niets te verwerken valt omdat er geen punctie of declarabel onderzoek gevonden is!
         RETURN;
      END IF;
      cg$errors.clear();
      -- Er zijn de volgende werkwijzen mogelijk voor de verwerking:
      -- AON = AllesOfNiets - indien er een fout is opgetreden, dan niets toevoegen/wijzigen
      -- WM = WatMogelijk - indien er een monster of onderzoek OK is, dan PZF toevoegen/wijzigen + de correcte monsters/onderzoeken
      v_verwerkingswijze := 'WM';
      -- Mantis 3184: Interface voor Dragon: flixibele verwerking:
      IF r_imfi.gegroepeerde_verwerking = 'N'
      THEN
         v_verwerkingswijze := 'AON'; -- alles of niets (standaard is WM - Wat Mogelijk)
      END IF;
      -- specifieke verwerking; rde 27-10-2015 mantis 11620
      v_verwerkingstatus := 'D'; -- Doorgaan
      IF r_imfi.specifieke_verwerking IS NOT NULL
      THEN
         CASE kgc_sypa_00.systeem_waarde('ZIS_LOCATIE')
         WHEN 'UTRECHT' THEN
            v_verwerkingstatus := kgc_import_utrecht.specifieke_verwerking(p_imfi => v_imfi
                                                                          ,p_plekvanaanroep => 'VOOR'
                                                                          ,x_imfi_verslag => v_imfi_verslag
                                                                          );
         WHEN 'NIJMEGEN' THEN
            v_verwerkingstatus := kgc_import_nijmegen.specifieke_verwerking(p_imfi => v_imfi
                                                                           ,p_plekvanaanroep => 'VOOR'
                                                                           ,x_imfi_verslag => v_imfi_verslag
                                                                           );
         WHEN 'MAASTRICHT' THEN
            v_verwerkingstatus := kgc_import_maastricht.specifieke_verwerking(p_imfi => v_imfi
                                                                             ,p_plekvanaanroep => 'VOOR'
                                                                             ,x_imfi_verslag => v_imfi_verslag
                                                                             );
         ELSE
            v_verwerkingstatus := 'D'; -- Doorgaan
         END CASE;
      END IF;
-- mogelijke uitkomsten: E=Error; K=Klaar; D=Doorgaan
-- uitzoeken: waarmee wordt einde verwerking gestuurd?
-- einde: afhandeling fouten in pers: geen uitzondering laten zijn, doen via normale foutafhandeling
      IF v_verwerkingstatus = 'D' -- doorgaan
      THEN
         -- hoeveel personen zijn er aanwezig? Dit is bepalend voor de meldingen
         SELECT COUNT(*)
           INTO v_impe_count
           FROM kgc_im_personen impe
          WHERE impe.imfi_id = p_imfi_id;

         -- moeten de gegevens uit ZIS overgehaald worden? (voor iedere verwerking van Mosos/Generiek!)
         IF r_imfi.zisnr_overnemen = 'J' AND r_imfi.interface_type IN ('M', 'G')
         THEN
            FOR r_impe IN c_impe(p_imfi_id)
            LOOP
               v_imfi_verslag_tmp := NULL; -- tbv foutenverslag
               v_impe := v_impe_null;
               v_impe.imfi_id := r_impe.imfi_id;
               v_impe.impe_id := r_impe.impe_id;
               cg$kgc_im_personen.slct(v_impe); -- bestaande gegevens ophalen
               -- gegevens uit ZIS in IMPE zetten
               IF NOT kgc_interface_generiek.persoon_im_in_zis(p_imlo_id   => r_imfi.imlo_id
                                                              ,x_impe_trow => v_impe
                                                              ,x_msg       => v_imfi_verslag_tmp)
               THEN
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,v_imfi_verslag_tmp);
                  v_verwerkingstatus := 'E';
               ELSIF NVL(LENGTH(v_imfi_verslag_tmp), 0) > 0 -- Rework Mantis 3184: Interface voor Dragon
               THEN
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,v_imfi_verslag_tmp);
                  v_verwerkingstatus := 'E';
               END IF;
            END LOOP; -- c_impe
         END IF;
      END IF;

      BEGIN -- tbv fout-afhandeling
         FOR i_fase IN 1 .. 2
         -- 1 = v_c_checking
         -- 2 = v_c_updating
         LOOP -- 1=CHCK 2=UPD; alle velden moeten langsgelopen worden...
            show_output('Start Fase ' || i_fase);
            IF v_verwerkingstatus = 'E'
            THEN
               show_output('Exit door verwerkingstatus ' || v_verwerkingstatus);
               EXIT;
            END IF;
            IF i_fase = v_c_updating
            THEN
               IF (v_verwerkingswijze = 'WM' AND imxx_error('#PERS#ZWAN#FOET#')) -- Wat Mogelijk
                  OR (v_verwerkingswijze = 'AON' AND imxx_error)                 -- Alles Of Niets
               THEN -- er valt niets te doen
                  show_output('Exit door fouten in verwerking; Verwerkingswijze=' || v_verwerkingswijze);
                  EXIT;
               END IF;
               -- aangeven hoe de standaard verwerking in elkaar zit
               g_standaard_logica := r_imfi.standaard_logica;
               -- reset meldingen + id's
               v_imfi_opmerkingen := NULL;
               v_imfi_verslag := NULL;
               reset_im2hlx;
            END IF;
            -- nu kunnen in ieder geval de pers-zwan-foetus verwerkt worden.Daarna moet er weer gecontroleerd worden op wat er fout is.

            FOR r_impe IN c_impe(p_imfi_id)
            LOOP
               show_output('Start controle persoon ' || c_impe%ROWCOUNT || '=' || r_impe.impe_id);
               v_pers_old := v_pers_null;
               v_pers_new := v_pers_null;

               v_bpersiddatawijzigen := FALSE;

               IF i_fase = v_c_checking THEN -- init
                  v_tab_impe_err(r_impe.impe_id) := FALSE;
               END IF;
               -- bepaal verwijzing:
               v_opm_tab_en_id := 'PERSOON';
               IF v_impe_count > 1
               THEN
                  v_opm_tab_en_id := v_opm_tab_en_id || '(' || r_impe.impe_id || ')';
                  v_opm_tab_en_id_pers := 'PERS-' || r_impe.impe_id || '; ';
               END IF;
               show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(impe_id=' || r_impe.impe_id || ', kgc_pers_id=' || r_impe.kgc_pers_id || ', anoniem=' || r_impe.anoniem || ')');

               -- controleer koppeling
               -- pers_id leeg of moet bestaan
               v_pers_old.pers_id := NVL(r_impe.kgc_pers_id, TO_NUMBER(SUBSTR(get_im2hlx('IMPE' || r_impe.impe_id), 5)));
               IF v_pers_old.pers_id IS NOT NULL -- is deze patient al eerder geweest?
               THEN
                  BEGIN
                     cg$kgc_personen.slct(v_pers_old);
                     v_pers_old_anoniem := isPersoonAnoniem(v_pers_old);
                     v_pers_new.pers_id := v_pers_old.pers_id;
                     cg$kgc_personen.slct(v_pers_new);
                     -- oud is de basis voor nieuw (bevat bv het van een externe aanlevering afwijkend zisnr)
                     -- indien zisnr_overnemen = N, dan wordt deze waarde niet meer aangepast
                  EXCEPTION
                     WHEN OTHERS THEN -- no data found
                        cg$errors.clear;
                        addtext(v_imfi_opmerkingen
                               ,v_max_len
                               ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_pers_old.pers_id) ||
                                ') verwijst niet naar een bestaande persoon. Maak het Helix-id leeg (persoon wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de persoonsgegevens.');
                        v_verwerkingstatus := 'E';
                        v_tab_impe_err(r_impe.impe_id) := TRUE;
                        EXIT; -- klaar met controleren personen
                  END;
               END IF;

               -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
               v_imfi_opmerkingen_enti := NULL;
               v_error_enti := FALSE;
               IF NOT im2pers( p_impe => r_impe
                             , p_id => v_opm_tab_en_id
                             , p_zisnr_overnemen => r_imfi.zisnr_overnemen
                             , p_beheer_in_zis => p_beheer_in_zis
                             , p_pers_old => v_pers_old
                             , x_pers => v_pers_new
                             , x_opmerkingen => v_imfi_opmerkingen_enti)
               THEN
                  v_error_enti := TRUE;
               END IF;

               IF v_pers_new.pers_id IS NULL
               THEN  -- KOPPEL: probeer te koppelen obv eigen ZIS + match op ZISnr?
                  IF r_imfi.zisnr_overnemen = 'J'
                     AND r_impe.zisnr IS NOT NULL
                  THEN
                     v_logregel := 'koppel pers obv zisnr=' || r_impe.zisnr;
                     -- zoeken naar match op basis van ZISnr
                     OPEN c_perszis(r_impe.zisnr);
                     FETCH c_perszis
                        INTO v_pers_new.pers_id;
                     IF c_perszis%FOUND
                     THEN
                        v_logregel := v_logregel || '... gevonden: pers_id=' || v_pers_new.pers_id;
                        addtext(v_imfi_verslag -- rde 8.13.2.1 was v_imfi_opmerkingen, waardoor daarna vinkje afgehandeld niet werd gezet
                               ,v_max_len
                               ,v_opm_tab_en_id || ' - Externe persoon gekoppeld aan Helix-persoon obv ZISnummer.');
                     ELSE
                        v_logregel := v_logregel || '... NIET gevonden';
                     END IF;
                     CLOSE c_perszis;
                     show_output(v_logregel);
                  END IF;
               END IF;

               IF v_pers_new.pers_id IS NULL AND
                  r_impe.bsn IS NOT NULL
               THEN  -- KOPPEL: probeer het via bsn
                  v_logregel := 'koppel pers obv BSN';
                  DECLARE
                     CURSOR c_pers_zoek_via_bsn (p_bsn NUMBER) IS
                        SELECT pers.pers_id
                          FROM kgc_personen pers
                         WHERE pers.bsn = p_bsn;
                  BEGIN
                     FOR r_pers_zoek_via_bsn IN c_pers_zoek_via_bsn(p_bsn => v_pers_new.bsn) LOOP
                        IF c_pers_zoek_via_bsn%ROWCOUNT = 1 THEN
                           v_pers_new.pers_id := r_pers_zoek_via_bsn.pers_id;
                           v_logregel := v_logregel || '; Persoon ' || v_pers_new.pers_id || 'gevonden!';
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Persoon gekoppeld aan Helix-persoon obv bsn.');
                        ELSE -- ERROR, maar niet nodig om deze te melden: dit doet kgc_pers_00.controleer_bsn (aangeroepen in im2pers)
                           v_logregel := v_logregel || '; Extra persoon met bsn gevonden (' || r_pers_zoek_via_bsn.pers_id || ')!';
                        END IF;
                     END LOOP;
                     IF v_pers_new.pers_id IS NULL THEN
                        v_logregel := v_logregel || '; Geen persoon gevonden.';
                     END IF;
                  END;
                  show_output(v_logregel);
               END IF;

               IF v_pers_new.pers_id IS NULL AND
                  NVL(r_impe.anoniem, 'N') = 'N'
               THEN  -- KOPPEL: probeer het via extra attribuut
                  v_logregel := 'koppel pers obv PERS_EXTRA1';
                  DECLARE
                     CURSOR c_pers_zoek_via_attr(p_attr_code VARCHAR2, p_waarde VARCHAR2) IS
                        SELECT pers.pers_id
                          FROM kgc_personen pers
                             , kgc_attributen attr
                             , kgc_attribuut_waarden atwa
                         WHERE attr.code = p_attr_code
                           AND attr.attr_id = atwa.attr_id
                           AND atwa.waarde = p_waarde
                           AND atwa.id = pers.pers_id;
                     r_pers_zoek_via_attr c_pers_zoek_via_attr%ROWTYPE;
                  BEGIN
                     -- mogelijk: koppelen op externe id
                     IF r_impe.extra1 IS NULL
                     THEN
                        v_logregel := v_logregel || '... gaat niet: leeg.';
                     ELSE -- zoek op extra attribuut uit imfi
                        v_logregel := v_logregel || ' (=' || r_impe.extra1 || '). ';
                        v_waarde := kgc_attr_00.waarde('KGC_IMPORT_FILES', 'PERS_EXTRA1', p_imfi_id);
                        IF v_waarde IS NULL
                        THEN
                           v_logregel := v_logregel || 'Echter: geen PERS_EXTRA1 bij imfi gevonden.';
                        ELSE -- bv ATTR_ZISNR_MUMC
                           v_logregel := v_logregel || '. imfi.PERS_EXTRA1=' || v_waarde;
                           IF SUBSTR(v_waarde, 1, 5) = 'ATTR_'
                           THEN
                              v_attr_code := SUBSTR(v_waarde, 6);
                              OPEN c_pers_zoek_via_attr(p_attr_code => v_attr_code, p_waarde => r_impe.extra1);
                              FETCH c_pers_zoek_via_attr INTO r_pers_zoek_via_attr;
                              IF c_pers_zoek_via_attr%FOUND THEN
                                 v_logregel := v_logregel || 'Gevonden: pers_id=' || r_pers_zoek_via_attr.pers_id;
                                 show_output('koppel obv PERS_EXTRA1/attr=' || v_attr_code || '; pers_id=' || r_pers_zoek_via_attr.pers_id);
                                 v_pers_new.pers_id := r_pers_zoek_via_attr.pers_id;
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - Persoon gekoppeld aan Helix-persoon obv externe-id (extra1-' || r_impe.extra1 || ').');
                              ELSE -- niet fout!
                                 v_logregel := v_logregel || 'Geen pers_id gevonden.';
                              END IF;
                              CLOSE c_pers_zoek_via_attr;
                           END IF;
                        END IF;
                     END IF;
                  END;
                  show_output(v_logregel);
               END IF;

               IF v_pers_new.pers_id IS NULL
               THEN  -- KOPPEL: probeer te koppelen obv onderzoek
                  -- dit kan bv doordat:
                  -- 1. bij een persoon wordt een goedgekeurd of afgerond onderzoek teruggekoppeld
                  -- dit kan echter alleen indien er een onderzoek aangeleverd is dat te koppelen is
                  v_logregel := 'koppel pers obv Onderzoeknummer';
                  DECLARE
                     CURSOR c_onde_loc(p_onderzoeknr VARCHAR2) IS
                        SELECT pers_id
                             , onderzoeknr
                          FROM kgc_onderzoeken
                         WHERE onderzoeknr = p_onderzoeknr;
                     r_onde_loc c_onde_loc%ROWTYPE;
                  BEGIN
                     FOR r_imon IN c_imon(p_imfi_id
                                         ,r_impe.impe_id)
                     LOOP
                        v_imfi_verslag_tmp := NULL;
                        v_mons_new := v_mons_null;
                        IF r_imon.onderzoeknr IS NOT NULL
                        THEN
                           OPEN c_onde_loc(r_imon.onderzoeknr);
                           FETCH c_onde_loc INTO r_onde_loc;
                           IF c_onde_loc%FOUND
                           THEN
                              v_pers_new.pers_id := r_onde_loc.pers_id;
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Persoon gekoppeld aan Helix-persoon obv bestaand onderzoek (' || r_onde_loc.onderzoeknr || ').');
                              v_logregel := v_logregel || ': ' || r_onde_loc.onderzoeknr || '; gelukt; pers_id=' || v_pers_new.pers_id;
                           END IF;
                           CLOSE c_onde_loc;
                        END IF;
                        IF v_pers_new.pers_id IS NOT NULL THEN
                           EXIT;
                        END IF;
                     END LOOP;
                  END;
                  IF v_pers_new.pers_id IS NULL
                  THEN
                     v_logregel := v_logregel || ' is niet gelukt: geen onderzoek gevonden.';
                  END IF;
                  show_output(v_logregel);
               END IF;

               IF v_pers_new.pers_id IS NULL
               THEN  -- KOPPEL: probeer te koppelen obv monsters
                  -- dit kan bv doordat:
                  -- 1. een persoon eerder is aangeleverd als anonieme persoon+fractie
                  -- 2. een anonieme persoon eerder is aangeleverd als persoon
                  -- dit kan echter alleen indien er een monster aangeleverd is dat te koppelen is
                  v_logregel := 'koppel pers obv Monsternummer';
                  DECLARE
                     CURSOR c_mons_loc(p_mons_id NUMBER) IS
                        SELECT pers_id
                             , monsternummer
                          FROM kgc_monsters
                         WHERE mons_id = p_mons_id;
                     r_mons_loc c_mons_loc%ROWTYPE;
                  BEGIN
                     FOR r_immo IN c_immo(p_imfi_id
                                         ,r_impe.impe_id)
                     LOOP
                        IF r_immo.monsternummer = '*KOPPEL*' THEN -- 8.13.2.1 - niet gebruiken voor koppel persoon
                           NULL;
                        ELSE
                           v_imfi_verslag_tmp := NULL;
                           v_mons_new := v_mons_null;
                           IF im2mons( p_immo => r_immo
                                     , p_id => 'dm'
                                     , p_mons_old => v_mons_new
                                     , p_type => 'KP' -- alleen tbv koppeling!
                                     , x_mons => v_mons_new
                                     , x_opmerkingen => v_imfi_verslag_tmp)
                           THEN
                              IF koppel_mons( x_mons => v_mons_new
                                            , x_opmerkingen => v_imfi_verslag_tmp
                                            , p_onde_mons_toevoegen => r_imfi.onde_mons_toevoegen)
                              THEN
                                 OPEN c_mons_loc(v_mons_new.mons_id);
                                 FETCH c_mons_loc INTO r_mons_loc;
                                 IF c_mons_loc%FOUND
                                 THEN
                                    v_pers_new.pers_id := r_mons_loc.pers_id;
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' - Persoon gekoppeld aan Helix-persoon obv bestaand monster (' || r_mons_loc.monsternummer || ').');
                                    v_logregel := v_logregel || ': ' || r_mons_loc.monsternummer || '; gelukt; pers_id=' || v_pers_new.pers_id;
                                 END IF;
                                 CLOSE c_mons_loc;
                              END IF;
                           END IF;
                           IF v_pers_new.pers_id IS NOT NULL THEN
                              EXIT;
                           END IF;
                        END IF;
                     END LOOP;
                  END;
                  IF v_pers_new.pers_id IS NULL
                  THEN
                     v_logregel := v_logregel || ' is niet gelukt: geen monster gevonden.';
                  END IF;
                  show_output(v_logregel);
               END IF;

               IF v_pers_new.pers_id IS NOT NULL
               THEN -- evt nieuw bepaalde gegevens ophalen
                  -- sla op in globale tabel
                  set_im2hlx(p_im => 'IMPE' || r_impe.impe_id, p_hlx => 'PERS' || v_pers_new.pers_id);

                  IF v_pers_old.the_rowid IS NULL
                  THEN
                     v_pers_old.pers_id := v_pers_new.pers_id;
                     cg$kgc_personen.slct(v_pers_old);
                  END IF;

                  IF v_pers_new.the_rowid IS NULL
                  THEN
                     cg$kgc_personen.slct(v_pers_new); -- oud is de basis voor nieuw
                     -- opnieuw controleren...
                     -- oude foutmeldingen (bv met bestaand bsn bij gekoppeld zisnr) niet meer relevant
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2pers( p_impe => r_impe
                                   , p_id => v_opm_tab_en_id
                                   , p_zisnr_overnemen => r_imfi.zisnr_overnemen
                                   , p_beheer_in_zis => p_beheer_in_zis
                                   , p_pers_old => v_pers_old
                                   , x_pers => v_pers_new
                                   , x_opmerkingen => v_imfi_opmerkingen_enti)
                     THEN
                        v_error_enti := TRUE;
                     END IF;
                  END IF;
               END IF;
               -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
               addtext(v_imfi_opmerkingen
                      ,v_max_len
                      ,v_imfi_opmerkingen_enti);
               v_tab_impe_err(r_impe.impe_id) := (v_tab_impe_err(r_impe.impe_id) OR v_error_enti);


               -- controleer te- of ge-koppelde persoon
               -- bepaal te koppelen persoon in familie
               IF NVL(r_impe.anoniem, 'N') = 'N'
               THEN
                  v_pers_id_fami := v_pers_new.pers_id;
               END IF;

               IF (v_pers_new.pers_id IS NULL)
               THEN
                  show_output('### Persoon nieuw; fase ' || i_fase || '(' || r_impe.impe_id || ')!');
               ELSE
                  show_output('### Persoon bestaand; fase ' || i_fase || '(' || r_impe.impe_id || ', pers_id=' || v_pers_new.pers_id || '!');
               END IF;

               v_pers_id_verschillen := NULL;
               IF v_pers_new.pers_id IS NOT NULL
               THEN
                  -- controle op verschillen pers-identificerend!
                  -- update zet een kolom nooit op null, dus als het doel null is: ok - geen wijziging
                  -- vergelijking mbv upper en zonder punten (vltrs): deze wijzigingen worden ook niet doorgevoerd!
                  v_pers_ind := v_pers_ind_false;
                  v_pers_id_verschillen := verschil_pers( p_o => v_pers_old
                                                        , p_n => v_pers_new
                                                        , p_i => v_pers_ind
                                                        , p_type => 'PI'
                                                        , p_zisnr_overnemen => r_imfi.zisnr_overnemen
                                                        );
                  IF v_pers_id_verschillen IS NOT NULL
                  THEN
                     -- dit is fout indien automatisch of geen toegang
                     IF NOT p_online
                     THEN
                        addtext(v_imfi_opmerkingen
                               ,v_max_len
                               ,'Persoonsidentificerende gegevens worden niet automatisch gewijzigd: dit moet on-line door een bevoegde persoon! (' ||
                                v_pers_id_verschillen || ')');
                        v_tab_impe_err(r_impe.impe_id) := TRUE;
                     ELSIF r_imfi.opmerkingen IS NULL OR (r_imfi.last_update_date - r_imfi.creation_date) * (24*60*60) < 15 THEN
                        -- aanpassing voor Mantis 11620, overeenkomstig Mantis 5715: igv 1e verwerking is het niet ok om de persoonsidentificerende te wijzigen
                        -- dit is echter lastig te achterhalen."r_imfi.opmerkingen IS NULL" - klopt niet altijd: soms staat hier al "De aangeleverde persoonsgegevens met ZISnr 1.182.829 wijken af van de ZIS-gegevens"
                        -- dus ook check op last-update - creatie; indien minder dan 15 seconden, dan nog niet verwerkt
                        addtext(v_imfi_opmerkingen
                               ,v_max_len
                               ,'Persoonsidentificerende gegevens kunnen niet bij de eerste verwerking gewijzigd worden: pas eventueel de gegevens aan en verwerk opnieuw! (' ||
                                v_pers_id_verschillen || ')');
                        v_tab_impe_err(r_impe.impe_id) := TRUE;
                     ELSE
                        IF (NOT v_tab_impe_err(r_impe.impe_id))
                           AND (NOT kgc_pers_00.bevoegd)
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,'U bent niet bevoegd voor het wijzigen van Persoonsidentificerende gegevens: ' ||
                                   v_pers_id_verschillen);
                           v_tab_impe_err(r_impe.impe_id) := TRUE;
                        END IF;
                     END IF;
                  END IF;
               END IF;

               IF v_pers_new.pers_id IS NULL
                  AND ((NOT p_online) OR r_imfi.opmerkingen IS NULL OR (r_imfi.last_update_date - r_imfi.creation_date) * (24*60*60) < 15)
                  -- aanpassing Mantis 5715: niet alleen in batch-mode controleren, maar ook igv 1e verwerking
                  -- dit is echter lastig te achterhalen."r_imfi.opmerkingen IS NULL" - klopt niet altijd: soms staat hier al "De aangeleverde persoonsgegevens met ZISnr 1.182.829 wijken af van de ZIS-gegevens"
                  -- dus ook check op last-update - creatie; indien minder dan 15 seconden, dan nog niet verwerkt
               THEN
                  -- indien nog geen match en automatische verwerking:
                  -- zoeken naar match, op basis van achternaam, geboortedatum en geslacht
                  -- (en anders: altijd nieuwe toevoegen; dit wordt online gecontroleerd, en de gebruiker heeft een melding gehad!)
                  -- aanpassing Mantis 5715: rekening houden met tekenvervanging
                  OPEN c_pers1(kgc_pers_00.formatteer_zoeknaam(v_pers_new.achternaam)
                              ,v_pers_new.geboortedatum
                              ,v_pers_new.geslacht);
                  FETCH c_pers1
                     INTO r_pers1;
                  IF c_pers1%NOTFOUND
                  THEN
                     -- OK: nieuwe toevoegen
                     NULL;
                  ELSE
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_opm_tab_en_id || ' - 1 of meerdere matches in Helix op naam, voorletters, geboortedatum; Koppel handmatig!');
                     v_tab_impe_err(r_impe.impe_id) := TRUE;
                  END IF;
                  CLOSE c_pers1; -- fout ontdekt tijdens Mantis 5715.
               END IF;

               -- check op autorisatie
               v_auth_pers_fout := NULL; -- melding effe opslaan: mogelijk is er niets gewijzigd!
               IF v_verwerkingstatus != 'E'
               THEN
                  IF v_pers_new.pers_id IS NULL -- zie verderop: toevoegen persoon
                  THEN
                     BEGIN
                        kgc_tato_00.toegang('KGC_PERSONEN'
                                           ,'I');
                     EXCEPTION
                        WHEN OTHERS THEN
                           v_auth_pers_fout := 'U bent niet bevoegd voor het toevoegen van Persoonsgegevens!';
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_auth_pers_fout);
                           v_verwerkingstatus := 'E'; -- direct stoppen: geen verdere toelichting meer nodig!
                     END;
                  ELSE
                     BEGIN
                        kgc_tato_00.toegang('KGC_PERSONEN'
                                           ,'U');
                     EXCEPTION
                        WHEN OTHERS THEN
                           v_auth_pers_fout := 'U bent niet bevoegd voor het wijzigen van Persoonsgegevens!';
                           -- nog niet stoppen: aangeven wat er gewijzigd zou worden!
                     END;
                  END IF;
               END IF;

               IF i_fase = v_c_updating
               THEN
                  show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                  -- in fase 1 is reeds gecontroleerd op fouten, maar het kan zijn dat door wijzigende omstandingheden toch
                  -- nog een fout geconstateerd is. Bv controle 2 personen is OK, na insert 1e persoon levert 2e een fout
                  IF v_verwerkingstatus = 'E'
                     OR (v_verwerkingswijze = 'WM' AND imxx_error('#PERS#ZWAN#FOET#'))
                     OR (v_verwerkingswijze = 'AON' AND imxx_error)
                  THEN
                     -- een blokkerende fout
                     NULL;
                  ELSE
                     -- ****** proberen correcte gegevens over te zetten naar Helix
                     BEGIN
                        -- welke persoonsgegevens moeten er toegevoegd worden? Alle!
                        IF v_pers_new.the_rowid IS NULL
                        THEN
                           show_output('Persoon nieuw toevoegen (' || r_impe.impe_id || ')!');
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Nieuwe persoon toegevoegd: ' || v_pers_new.zisnr || ' ' || v_pers_new.aanspreken || ', ' ||
                                   v_pers_new.geslacht || ', ' || to_char(v_pers_new.geboortedatum
                                                                         ,v_datumformaatdoel));
                           v_pers_ind := cg$kgc_personen.cg$ind_true;
                           IF v_pers_new.gehuwd IS NULL  THEN v_pers_ind.gehuwd := FALSE; END IF;
                           IF v_pers_new.geslacht IS NULL  THEN v_pers_ind.geslacht := FALSE; END IF;
                           IF v_pers_new.bloedverwant IS NULL  THEN v_pers_ind.bloedverwant := FALSE; END IF;
                           IF v_pers_new.meerling IS NULL  THEN v_pers_ind.meerling := FALSE; END IF;
                           IF v_pers_new.overleden IS NULL  THEN v_pers_ind.overleden := FALSE; END IF;
                           IF v_pers_new.default_aanspreken IS NULL  THEN v_pers_ind.default_aanspreken := FALSE; END IF;
                           IF v_pers_new.beheer_in_zis IS NULL  THEN v_pers_ind.beheer_in_zis := FALSE; END IF;
                           IF v_pers_new.land IS NULL  THEN v_pers_ind.land := FALSE; END IF;
                           IF v_pers_new.bsn_geverifieerd IS NULL  THEN v_pers_ind.bsn_geverifieerd := FALSE; END IF;

                           cg$kgc_personen.ins(cg$rec => v_pers_new
                                              ,cg$ind => v_pers_ind);
                           set_im2hlx(p_im => 'IMPE' || r_impe.impe_id, p_hlx => 'PERS' || v_pers_new.pers_id);
                           copy_notities(p_imfi_id, 'PERS', v_pers_new.pers_id); -- mantis 5715: copy notities
                           -- bepaal te koppelen persoon in familie
                           IF NVL(r_impe.anoniem, 'N') = 'N'
                           THEN
                              v_pers_id_fami := v_pers_new.pers_id;
                           END IF;
                           set_atwa(p_atwa_waarde => r_impe.extra1
                                   ,p_spec_waarde => kgc_attr_00.waarde('KGC_IMPORT_FILES', 'PERS_EXTRA1', p_imfi_id)
                                   ,p_tabel_naam  => 'KGC_PERSONEN'
                                   ,p_id          => v_pers_new.pers_id);
                        ELSE
                           -- welke persoonsgegevens moeten er bijgewerkt worden?
                           -- indien NULL aangeleverd -> de 'doel'-column wordt NIET op NULL gezet bv igv ZISnr + p_zisnr_overnemen = N
                           -- persoon-identificerend: achternaam, aanspreken, geslacht, ZISNR, voorletters, voorvoegsels, geboortedatum
                           --
                           -- anonieme personen: mogelijk worden deze gekoppeld.
                           -- beide anoniem: NIET bijwerken.
                           -- nieuw=anoniem, oud=niet-anoniem: NIET bijwerken!
                           -- igv beide niet anoniem: evt bijwerken
                           -- nieuw=niet-anoniem, oud=anoniem: WEL bijwerken!
                           IF r_impe.anoniem = 'J' AND isPersoonAnoniem(v_pers_new) = 'J'
                           THEN
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Anonieme persoon ' || r_impe.impe_id || ' staat ook in Helix als anoniem; geen wijzigingen!');
                           ELSIF r_impe.anoniem = 'J' AND isPersoonAnoniem(v_pers_new) = 'N'
                           THEN
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Anonieme persoon ' || r_impe.impe_id || ' staat in Helix als niet-anoniem (zisnr=' || v_pers_new.zisnr || '); geen wijzigingen!');
                           ELSE -- bijwerken!
                              v_pers_ind := v_pers_ind_false;
                              v_imfi_verslag_tmp := verschil_pers( p_o => v_pers_old -- tbv wijzigingsverslag
                                                                 , p_n => v_pers_new
                                                                 , p_i => v_pers_ind
                                                                 , p_type => NULL
                                                                 , p_zisnr_overnemen => r_imfi.zisnr_overnemen
                                                                 );
                              show_output('Persoon bestaand bijwerken (' || r_impe.impe_id || ', pers_id=' || v_pers_new.pers_id || '!');
                              IF v_imfi_verslag_tmp IS NULL
                              THEN -- geen te wijzigen kolommen
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Persoon ' || v_pers_old.zisnr || ' ' || v_pers_old.aanspreken || ', ' || v_pers_old.geslacht || ', ' ||
                                         to_char(v_pers_old.geboortedatum
                                                ,v_datumformaatdoel) || ' is niet gewijzigd');
                              ELSE
                                 -- wel gegevens gewijzigd
                                 IF v_auth_pers_fout IS NOT NULL
                                 THEN
                                    addtext(v_imfi_opmerkingen
                                           ,v_max_len
                                           ,v_auth_pers_fout || ' (wijzigingen: ' || v_imfi_verslag_tmp || ')');
                                    v_verwerkingstatus := 'E';
                                 ELSE
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,'Persoon ' || v_pers_old.zisnr || ' ' || v_pers_old.aanspreken || ', ' || v_pers_old.geslacht || ', ' ||
                                            to_char(v_pers_old.geboortedatum
                                                   ,v_datumformaatdoel) || ' is gewijzigd: ' || v_imfi_verslag_tmp || ')');
                                    cg$kgc_personen.upd(cg$rec => v_pers_new
                                                          ,cg$ind => v_pers_ind);
                                    set_atwa(p_atwa_waarde => r_impe.extra1
                                            ,p_spec_waarde => kgc_attr_00.waarde('KGC_IMPORT_FILES', 'PERS_EXTRA1', p_imfi_id)
                                            ,p_tabel_naam  => 'KGC_PERSONEN'
                                            ,p_id          => v_pers_new.pers_id);
                                 END IF;
                              END IF;
                           END IF;
                        END IF;

                        IF r_impe.kgc_pers_id IS NULL
                           AND v_verwerkingstatus != 'E'
                        THEN
                           -- pers_id aanbrengen in interface
                           show_output('Persoon-id in interface bijwerken!');
                           UPDATE kgc_im_personen impe
                              SET impe.kgc_pers_id = v_pers_new.pers_id
                            WHERE impe.imfi_id = p_imfi_id
                              AND impe.impe_id = r_impe.impe_id;
                        END IF;
                     EXCEPTION
                        WHEN OTHERS THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,'Error PERS ' || substr(SQLERRM
                                                     ,1
                                                     ,1800));
                           v_cg_errmsg := SUBSTR(cg$errors.Geterrors, 1, 32767);
                           IF v_cg_errmsg IS NOT NULL THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,'Error PERS ' || substr(v_cg_errmsg
                                                        ,1
                                                        ,1800));
                           END IF;
                           v_verwerkingstatus := 'E';
                     END;

                  END IF;
               END IF;

               -- ****** controleer en vertaal gegevens ZWANGERSCHAP
               FOR r_imzw IN c_imzw_laatste(p_imfi_id
                                           ,r_impe.impe_id)
               LOOP
                  v_zwan_old := v_zwan_null;
                  v_zwan_new := v_zwan_null;
                  v_tab_imzw_err(r_imzw.imzw_id) := FALSE;
                  -- bepaal verwijzing:
                  v_opm_tab_en_id := 'ZWANGERSCHAP(' || v_opm_tab_en_id_pers || r_imzw.imzw_id || ')';
                  show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_zwan_id=' || r_imzw.kgc_zwan_id || ')');

                  -- controleer koppeling
                  -- zwan_id leeg of moet bestaan
                  v_zwan_old.zwan_id := NVL(r_imzw.kgc_zwan_id, TO_NUMBER(SUBSTR(get_im2hlx('IMZW' || r_imzw.imzw_id), 5)));
                  IF v_zwan_old.zwan_id IS NOT NULL -- is deze al eerder geweest?
                  THEN
                     BEGIN
                        cg$kgc_zwangerschappen.slct(v_zwan_old);
                        v_zwan_new.zwan_id := v_zwan_old.zwan_id;
                        cg$kgc_zwangerschappen.slct(v_zwan_new);
                        -- oud is de basis voor nieuw
                     EXCEPTION
                        WHEN OTHERS THEN -- no data found
                           cg$errors.clear;
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_zwan_old.zwan_id) ||
                                   ') verwijst niet naar een bestaande zwangerschap. Maak het Helix-id leeg (zwangerschap wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de zwangerschapgegevens.');
                           v_verwerkingstatus := 'E';
                           v_tab_imzw_err(r_imzw.imzw_id) := TRUE;
                           EXIT; -- klaar met controleren
                     END;
                  END IF;

                  -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                  v_imfi_opmerkingen_enti := NULL;
                  v_error_enti := FALSE;
                  IF NOT im2zwan( p_imzw => r_imzw
                                , p_id => v_opm_tab_en_id
                                , p_zwan_old => v_zwan_old
                                , x_zwan => v_zwan_new
                                , x_opmerkingen => v_imfi_opmerkingen_enti)
                  THEN
                     v_error_enti := TRUE;
                  END IF;

                  IF v_zwan_old.zwan_id IS NOT NULL
                  THEN
                     v_bloskoppelen := FALSE;
                     IF v_zwan_new.nummer != v_zwan_old.nummer
                     THEN
                        -- dan wordt er kennelijk een nieuwe id (=nummer) aangeleverd. nieuwe poging toestaan tot koppelen.
                        v_bloskoppelen := TRUE;
                     END IF;
                     IF v_bloskoppelen
                     THEN
                        UPDATE kgc_im_zwangerschappen imzw
                           SET imzw.kgc_zwan_id = NULL
                         WHERE imzw.imfi_id = p_imfi_id
                           AND imzw.imzw_id = r_imzw.imzw_id;
                        v_zwan_new.zwan_id := NULL;
                        v_zwan_old := v_zwan_null;
                        show_output('imzw losgekoppeld van helix-id: nummer oud=' || v_zwan_old.nummer || '; nr nieuw=' || v_zwan_new.nummer);
                     END IF;
                  END IF;

                  IF v_zwan_new.zwan_id IS NULL
                  THEN
                     -- probeer te koppelen + controle (het volgnummer wordt regelmatig fout ingevoerd in Astraia)
                     IF v_zwan_new.nummer IS NOT NULL
                        AND v_zwan_new.pers_id IS NOT NULL
                     THEN
                        v_logregel := 'koppel zwan obv nr=' || v_zwan_new.nummer || '; dt-aterm=' || TO_CHAR(v_zwan_new.datum_aterm, v_datumformaatdoel) || '; pers_id=' || v_zwan_new.pers_id;
                        -- mogelijk bestaat er al een zwangerschap:
                        OPEN c_zwan_zoek(v_zwan_new.pers_id
                                        ,v_zwan_new.nummer);
                        FETCH c_zwan_zoek
                           INTO r_zwan_zoek;
                        IF c_zwan_zoek%NOTFOUND
                        THEN
                           NULL; -- ok, nieuw
                           v_logregel := v_logregel || '... niet gevonden';
                        ELSE
                           v_logregel := v_logregel || '... gevonden; dt-aterm=' || TO_CHAR(r_zwan_zoek.datum_aterm, v_datumformaatdoel);
                           -- er is er 1 aanwezig; deze mag met niet meer dan 1 maand aangepast worden!
                           IF r_zwan_zoek.datum_aterm < add_months(v_zwan_new.datum_aterm
                                                             ,-1)
                              OR r_zwan_zoek.datum_aterm > add_months(v_zwan_new.datum_aterm
                                                                    ,1)
                              OR (r_zwan_zoek.datum_aterm IS NULL AND v_zwan_new.datum_aterm IS NOT NULL)
                              OR (r_zwan_zoek.datum_aterm IS NOT NULL AND v_zwan_new.datum_aterm IS NULL)
                           THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - hoeveelste zwangerschap (' || v_zwan_new.nummer || ', aterm ' ||
                                      NVL(to_char(r_zwan_zoek.datum_aterm,v_datumformaatdoel), '<leeg>') || ') krijgt nieuwe aterm-datum (' ||
                                      NVL(to_char(v_zwan_new.datum_aterm,v_datumformaatdoel), '<leeg>') ||
                                      '). Dit is niet toegestaan! Is bij de aangeboden zwangerschap wel de juiste ''Hoeveelste'' opgegeven?');
                              v_tab_imzw_err(r_imzw.imzw_id) := TRUE;
                              v_logregel := v_logregel || '; error - datum niet OK';
                           ELSE  -- ok, binnen degrenzen of beide NULL
                              v_zwan_new.zwan_id := r_zwan_zoek.zwan_id; -- gebruiken voor update
                              v_logregel := v_logregel || '; OK, zwan_id=' || v_zwan_new.zwan_id;
                           END IF;
                        END IF;
                        CLOSE c_zwan_zoek;
                        show_output(v_logregel);
                     END IF;
                  END IF;

                  -- evt nieuw bepaalde gegevens ophalen
-- todo: eigenlijk is het controleren klaar. onderstaande kan naar binnen de update en geen fout
-- evt muv het opslaan van het id in de tabel
                  IF v_zwan_new.zwan_id IS NOT NULL
                  THEN
                     -- sla op in globale tabel
                     set_im2hlx(p_im => 'IMZW' || r_imzw.imzw_id, p_hlx => 'ZWAN' || v_zwan_new.zwan_id);

                     IF v_zwan_old.the_rowid IS NULL
                     THEN
                        v_zwan_old.zwan_id := v_zwan_new.zwan_id;
                        cg$kgc_zwangerschappen.slct(v_zwan_old);
                     END IF;
                     IF v_zwan_new.the_rowid IS NULL
                     THEN
                        cg$kgc_zwangerschappen.slct(v_zwan_new); -- oud is de basis voor nieuw
                        -- ****** controleer en vertaal gegevens (obv evt nieuw opgehaalde gegevens)
                        v_imfi_opmerkingen_enti := NULL;
                        v_error_enti := FALSE;
                        IF NOT im2zwan( p_imzw => r_imzw
                                      , p_id => v_opm_tab_en_id
                                      , p_zwan_old => v_zwan_old
                                      , x_zwan => v_zwan_new
                                      , x_opmerkingen => v_imfi_opmerkingen_enti)
                        THEN
                           v_error_enti := TRUE;
                        END IF;
                     END IF;
                  END IF;
                  -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,v_imfi_opmerkingen_enti);
                  v_tab_imzw_err(r_imzw.imzw_id) := (v_tab_imzw_err(r_imzw.imzw_id) OR v_error_enti);

                  IF i_fase = v_c_updating
                  THEN
                     show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                     IF v_verwerkingstatus = 'E'
                        OR (v_verwerkingswijze = 'WM' AND imxx_error('#PERS#ZWAN#FOET#'))
                        OR (v_verwerkingswijze = 'AON' AND imxx_error)
                     THEN
                        NULL;
                     ELSE
                        -- nog steeds geen blokkerende fout: zwan toevoegen
                        IF v_zwan_new.zwan_id IS NULL
                        THEN
                           show_output('Zwan nieuw toevoegen (' || r_imzw.imzw_id || ')!');
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Nieuwe zwangerschap toegevoegd (' || v_zwan_new.nummer || ')');

                           -- tbv default-zetten:
                           v_zwan_ind := cg$kgc_zwangerschappen.cg$ind_true;
                           IF v_zwan_new.info_geslacht IS NULL  THEN v_zwan_ind.info_geslacht := FALSE; END IF;
                           IF v_zwan_new.consanguiniteit IS NULL  THEN v_zwan_ind.consanguiniteit := FALSE; END IF;
                           IF v_zwan_new.berekeningswijze IS NULL  THEN v_zwan_ind.berekeningswijze := FALSE; END IF;
                           IF v_zwan_new.abortus IS NULL  THEN v_zwan_ind.abortus := FALSE; END IF;

                           cg$kgc_zwangerschappen.ins(cg$rec => v_zwan_new
                                                     ,cg$ind => v_zwan_ind);
                           set_im2hlx(p_im => 'IMZW' || r_imzw.imzw_id, p_hlx => 'ZWAN' || v_zwan_new.zwan_id);
                           -- mantis 5715: copy notities
                           copy_notities(p_imfi_id, 'ZWAN', v_zwan_new.zwan_id);
                        ELSE
                           v_zwan_ind := v_zwan_ind_false;
                           v_imfi_verslag_tmp := verschil_zwan( p_o => v_zwan_old -- tbv wijzigingsverslag
                                                              , p_n => v_zwan_new
                                                              , p_i => v_zwan_ind
                                                              );
                           IF v_imfi_verslag_tmp IS NULL
                           THEN
                              -- geen te wijzigen kolommen
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Zwangerschap ' || v_zwan_new.nummer || ' is ongewijzigd');
                           ELSE
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Zwangerschap gewijzigd (' || v_zwan_new.nummer || ': ' || v_imfi_verslag_tmp || ')');
                              cg$kgc_zwangerschappen.upd(cg$rec => v_zwan_new
                                                        ,cg$ind => v_zwan_ind);
                           END IF;
                        END IF;
                        IF r_imzw.kgc_zwan_id IS NULL
                        THEN
                           -- zwan_id aanbrengen in interface
                           show_output('Zwangerschaps-id in interface bijwerken!');
                           UPDATE kgc_im_zwangerschappen imzw
                              SET imzw.kgc_zwan_id = v_zwan_new.zwan_id
                            WHERE imzw.imfi_id = p_imfi_id
                              AND imzw.imzw_id = r_imzw.imzw_id;
                        END IF;
                     END IF;
                  END IF;
               END LOOP; -- c_imzw_laatste

               -- ****** controleer en vertaal gegevens FOETUSSEN bij persoon (en eventueel bij zwangerschap)
               FOR r_imfo IN c_imfo(p_imfi_id
                                   ,r_impe.impe_id)
               LOOP
                  v_foet_old := v_foet_null;
                  v_foet_new := v_foet_null;
                  v_tab_imfo_err(r_imfo.imfo_id) := FALSE;

                  -- bepaal verwijzing:
                  v_opm_tab_en_id := 'FOETUS(' || v_opm_tab_en_id_pers || 'ZWAN-' || r_imfo.imzw_id || '; ' || r_imfo.imfo_id || ')';
                  show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_foet_id=' || r_imfo.kgc_foet_id || ')');

                  -- controleer koppeling
                  -- foet_id leeg of moet bestaan
                  v_foet_old.foet_id := NVL(r_imfo.kgc_foet_id, TO_NUMBER(SUBSTR(get_im2hlx('IMFO' || r_imfo.imfo_id), 5)));

                  IF v_foet_old.foet_id IS NOT NULL -- is deze al eerder geweest?
                  THEN
                     BEGIN
                        cg$kgc_foetussen.slct(v_foet_old);
                        v_foet_new.foet_id := v_foet_old.foet_id;
                        cg$kgc_foetussen.slct(v_foet_new);
                        -- oud is de basis voor nieuw
                     EXCEPTION
                        WHEN OTHERS THEN -- no data found
                           cg$errors.clear;
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_foet_old.foet_id) ||
                                   ') verwijst niet naar een bestaande foetus. Maak het Helix-id leeg (foetus wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de foetusgegevens.');
                           v_tab_imfo_err(r_imfo.imfo_id) := TRUE;
                           v_verwerkingstatus := 'E';
                           EXIT; -- klaar met controleren
                     END;
                  END IF;

                  -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                  v_imfi_opmerkingen_enti := NULL;
                  v_error_enti := FALSE;
                  IF NOT im2foet( p_imfo => r_imfo
                                , p_id => v_opm_tab_en_id
                                , p_foet_old => v_foet_old
                                , x_foet => v_foet_new
                                , x_opmerkingen => v_imfi_opmerkingen_enti)
                  THEN
                     v_error_enti := TRUE;
                  END IF;

                  IF v_foet_new.foet_id IS NULL
                  THEN
                     v_logregel := 'koppel obv moeder';
                     IF v_foet_new.pers_id_moeder IS NULL
                     THEN
                        v_logregel := v_logregel || ' gaat niet: moeder niet gekoppeld';
                     ELSE -- koppel via moeder + volgnr
                        v_logregel := v_logregel || ', pers_id=' || v_foet_new.pers_id_moeder || '; volgnr=' || v_foet_new.volgnr || '; zwan_id=' || v_foet_new.zwan_id;
                        OPEN c_foet_zoek(v_foet_new.pers_id_moeder
                                        ,v_foet_new.volgnr
                                        ,v_foet_new.zwan_id);
                        FETCH c_foet_zoek
                           INTO v_foet_new.foet_id;
                        IF c_foet_zoek%FOUND
                        THEN
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Foetus gekoppeld obv moeder, zwangerschap en volgnr');
                        ELSE
                           v_logregel := v_logregel || '; niet gevonden!';
                           show_output(v_logregel);
                        END IF;
                        CLOSE c_foet_zoek;
                     END IF;
                  END IF;

                  IF v_foet_new.foet_id IS NOT NULL
                  THEN -- evt nieuwe gegevens ophalen
                     IF v_foet_old.the_rowid IS NULL
                     THEN
                        v_foet_old.foet_id := v_foet_new.foet_id;
                        cg$kgc_foetussen.slct(v_foet_old);
                     END IF;
                     IF v_foet_new.the_rowid IS NULL
                     THEN
                        cg$kgc_foetussen.slct(v_foet_new);
                        -- de nieuwe gegevens nog een keer ophalen
                        v_imfi_opmerkingen_enti := NULL;
                        v_error_enti := FALSE;
                        IF NOT im2foet( p_imfo => r_imfo
                                      , p_id => v_opm_tab_en_id
                                      , p_foet_old => v_foet_old
                                      , x_foet => v_foet_new
                                      , x_opmerkingen => v_imfi_opmerkingen_enti)
                        THEN
                           v_error_enti := TRUE;
                        END IF;
                     END IF;
                     set_im2hlx(p_im => 'IMFO' || r_imfo.imfo_id, p_hlx => 'FOET' || v_foet_new.foet_id);
                  END IF;
                  -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,v_imfi_opmerkingen_enti);
                  v_tab_imfo_err(r_imfo.imfo_id) := (v_tab_imfo_err(r_imfo.imfo_id) OR v_error_enti);

                  IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                  THEN
                     show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                     IF v_verwerkingstatus = 'E'
                        OR (v_verwerkingswijze = 'WM' AND imxx_error('#PERS#ZWAN#FOET#'))
                        OR (v_verwerkingswijze = 'AON' AND imxx_error)
                     THEN
                        NULL;
                     ELSE
                        IF v_foet_new.foet_id IS NULL
                        THEN
                           show_output('Foet nieuw toevoegen (' || r_imfo.imfo_id || ')!');
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Nieuwe foetus toegevoegd (' || v_foet_new.volgnr || ')');
                           -- tbv default-zetten:
                           v_foet_ind := cg$kgc_foetussen.cg$ind_true;
                           IF v_foet_new.niet_geboren IS NULL  THEN v_foet_ind.niet_geboren := FALSE; END IF;
                           IF v_foet_new.geslacht IS NULL THEN v_foet_ind.geslacht := FALSE; END IF;

                           cg$kgc_foetussen.ins(cg$rec => v_foet_new
                                               ,cg$ind => v_foet_ind);
                           set_im2hlx(p_im => 'IMFO' || r_imfo.imfo_id, p_hlx => 'FOET' || v_foet_new.foet_id);
                           -- mantis 5715: copy notities
                           copy_notities(p_imfi_id, 'FOET', v_foet_new.foet_id);
                        ELSE
                           -- update
                           show_output('Foetus bijwerken (' || r_imfo.imfo_id || ')!');
                           v_bdezefoetuswijzigen := TRUE;
                           -- nooit bijwerken: ... zie hieronder!
                           -- is dit de laatst toegevoegde im_foetus met dit volgnr?
                           -- Mantis 5715: indien niet gevonden (agv afwezige volgnr: toch wijzigen!
                           OPEN c_imfo_laatst(p_imfi_id
                                             ,r_imfo.imzw_id
                                             ,r_imfo.volgnr);
                           FETCH c_imfo_laatst
                              INTO r_imfo_laatst;
                           IF c_imfo_laatst%NOTFOUND THEN
                              v_bdezefoetuswijzigen := TRUE; -- Mantis 5715:
                           ELSE
                              v_bdezefoetuswijzigen := (r_imfo_laatst.imfo_id = r_imfo.imfo_id);
                           END IF;
                           CLOSE c_imfo_laatst;
                           IF v_bdezefoetuswijzigen
                           THEN
                              v_foet_ind := v_foet_ind_false;
                              v_imfi_verslag_tmp := verschil_foet( p_o => v_foet_old
                                                                 , p_n => v_foet_new
                                                                 , p_i => v_foet_ind);
                              show_output('Wijzigen ' || v_err_id || ': ' || v_imfi_verslag_tmp);
                              IF v_imfi_verslag_tmp IS NULL
                              THEN -- geen te wijzigen kolommen
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Foetus ' || to_char(v_foet_new.volgnr) || ' is ongewijzgd');
                              ELSE
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Foetus gewijzigd (' || to_char(v_foet_new.volgnr) || ': ' ||
                                         v_imfi_verslag_tmp || ')');
                                 cg$kgc_foetussen.upd(cg$rec => v_foet_new
                                                     ,cg$ind => v_foet_ind);
                              END IF;
                           ELSE
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Foetus ' || to_char(v_foet_new.volgnr) || ' wordt niet gewijzigd: dit is niet de laatst toegevoegd foetus met volgnr ' || v_foet_new.volgnr);
                           END IF;
                        END IF;
                        IF r_imfo.kgc_foet_id IS NULL
                        THEN
                           -- foet_id aanbrengen in interface
                           show_output('Foetus-id in interface bijwerken!');
                           UPDATE kgc_im_foetussen imfo
                              SET imfo.kgc_foet_id = v_foet_new.foet_id
--                                , imfo.volgnr      = v_foet_new.volgnr
                            WHERE imfo.imfi_id = p_imfi_id
                              AND imfo.imfo_id = r_imfo.imfo_id;
                        END IF;
                     END IF;
                  END IF;

               END LOOP; -- c_imfo

               -- ****** controleer en vertaal gegevens ONDERZOEK bij persoon
               FOR r_imon IN c_imon(p_imfi_id
                                   ,r_impe.impe_id)
               LOOP
                  v_onde_old := v_onde_null;
                  v_onde_new := v_onde_null;
                  v_tab_imon_err(r_imon.imon_id) := FALSE;

                  -- bepaal verwijzing:
                  v_opm_tab_en_id := 'ONDERZOEK(' || v_opm_tab_en_id_pers || r_imon.imon_id || ')';
                  show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_onde_id=' || r_imon.kgc_onde_id || ')');

                  -- controleer koppeling
                  -- onde_id leeg of moet bestaan
                  v_onde_old.onde_id := NVL(r_imon.kgc_onde_id, TO_NUMBER(SUBSTR(get_im2hlx('IMON' || r_imon.imon_id), 5)));
                  IF v_onde_old.onde_id IS NOT NULL -- is deze al eerder geweest?
                  THEN
                     BEGIN
                        cg$kgc_onderzoeken.slct(v_onde_old);
                        v_onde_new.onde_id := v_onde_old.onde_id;
                        cg$kgc_onderzoeken.slct(v_onde_new);
                        -- oud is de basis voor nieuw
                     EXCEPTION
                        WHEN OTHERS THEN -- no data found
                           cg$errors.clear;
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_onde_old.onde_id) ||
                                   ') verwijst niet naar een bestaand onderzoek. Maak het Helix-id leeg (onderzoek wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de onderzoeksgegevens.');
                           v_verwerkingstatus := 'E';
                           v_tab_imon_err(r_imon.imon_id) := TRUE;
                           EXIT; -- klaar met controleren
                     END;
                  END IF;

                  -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                  v_imfi_opmerkingen_enti := NULL;
                  v_error_enti := FALSE;
                  IF NOT im2onde( p_imon => r_imon
                                , p_id => v_opm_tab_en_id
                                , p_onde_old => v_onde_old
                                , x_onde => v_onde_new
                                , x_opmerkingen => v_imfi_opmerkingen_enti)
                  THEN
                     v_error_enti := TRUE;
                  END IF;

                  --*** zoek te koppelen onderzoek
                  IF v_onde_new.onde_id IS NULL
                  THEN
                     DECLARE
                        CURSOR c_onde_zoek_via_attr(p_attr_code VARCHAR2, p_waarde VARCHAR2) IS
                           SELECT onde.onde_id
                                , onde.onderzoeknr
                             FROM kgc_onderzoeken onde
                                , kgc_attributen attr
                                , kgc_attribuut_waarden atwa
                            WHERE attr.code = p_attr_code
                              AND attr.attr_id = atwa.attr_id
                              AND atwa.waarde = p_waarde
                              AND atwa.id = onde.onde_id;
                        r_onde_zoek_via_attr c_onde_zoek_via_attr%ROWTYPE;
                     BEGIN
                        -- mogelijk: koppelen op externe id
                        IF r_imon.extra1 IS NOT NULL THEN -- zoek op extra attribuut uit imfi
                           v_waarde := kgc_attr_00.waarde('KGC_IMPORT_FILES', 'ONDE_EXTRA1', p_imfi_id);
                           IF v_waarde IS NOT NULL
                           THEN -- bv ATTR_ONDERZOEKNR_MUMC
                              show_output('koppel obv ONDE_EXTRA1=' || v_waarde);
                              IF SUBSTR(v_waarde, 1, 5) = 'ATTR_'
                              THEN
                                 v_attr_code := SUBSTR(v_waarde, 6);
                                 OPEN c_onde_zoek_via_attr(p_attr_code => v_attr_code, p_waarde => r_imon.extra1);
                                 FETCH c_onde_zoek_via_attr INTO r_onde_zoek_via_attr;
                                 IF c_onde_zoek_via_attr%FOUND THEN
                                    v_onde_new.onde_id := r_onde_zoek_via_attr.onde_id;
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' - Onderzoek gekoppeld aan Helix-onderzoek ' || r_onde_zoek_via_attr.onderzoeknr || ' obv externe-id (extra1-' || r_imon.extra1 || ').');
                                 -- ELSE ... geen fout... indien 1e keer: gebruiken om extra attr te vullen!
                                 END IF;
                                 CLOSE c_onde_zoek_via_attr;
                              END IF;
                           END IF;
                        END IF;
                     END;
                  END IF;

                  IF v_onde_new.onde_id IS NULL
                  THEN
                     DECLARE
                        CURSOR c_onde_zoek_via_onderzoeknr(p_onderzoeknr VARCHAR2) IS
                           SELECT onde.onde_id
                                , onde.onderzoeknr
                             FROM kgc_onderzoeken onde
                            WHERE onde.onderzoeknr = p_onderzoeknr;
                        r_onde_zoek_via_onderzoeknr c_onde_zoek_via_onderzoeknr%ROWTYPE;
                     BEGIN
                        -- mogelijk: koppelen op onderzoeknr
                        IF r_imon.onderzoeknr IS NOT NULL THEN
                           show_output('koppel obv onderzoeknr=' || r_imon.onderzoeknr);
                           OPEN c_onde_zoek_via_onderzoeknr(p_onderzoeknr => r_imon.onderzoeknr);
                           FETCH c_onde_zoek_via_onderzoeknr INTO r_onde_zoek_via_onderzoeknr;
                           IF c_onde_zoek_via_onderzoeknr%FOUND THEN
                              v_onde_new.onde_id := r_onde_zoek_via_onderzoeknr.onde_id;
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Onderzoek gekoppeld aan Helix-onderzoek ' || r_onde_zoek_via_onderzoeknr.onderzoeknr || ' obv onderzoeknr.');
                           END IF;
                           CLOSE c_onde_zoek_via_onderzoeknr;
                        END IF;
                     END;
                  END IF;

                  IF v_onde_new.onde_id IS NULL
                  THEN -- mogelijk: koppelen via bestaande waarden
                     IF v_onde_new.pers_id IS NOT NULL
                        AND v_onde_new.datum_binnen IS NOT NULL
                        AND r_imfi.onde_mons_toevoegen = 'N' -- rde 15-09-2009 Mantis 3184: Interface voor Dragon;
                                                             -- indien niet altijd onde/mons toevoegen: check bestaand
                     THEN
                        -- bestaat het onderzoek al?
                        OPEN c_onde_zoek(p_pers_id         => v_onde_new.pers_id
                                        ,p_datum           => v_onde_new.datum_binnen
                                        ,p_ongr_id         => v_onde_new.ongr_id
                                        ,p_foet_id         => v_onde_new.foet_id
                                        ,p_onderzoekswijze => v_onde_new.onderzoekswijze
                                        ,p_referentie      => v_onde_new.referentie);
                        FETCH c_onde_zoek
                           INTO r_onde_zoek;
                        IF c_onde_zoek%FOUND
                        THEN
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Onderzoek gekoppeld aan Helix-onderzoek ' || r_onde_zoek.onderzoeknr || ' obv attributen (datum-binnen, onderzoeksgroep, foetus, wijze, referentie).');
                           v_onde_new.onde_id := r_onde_zoek.onde_id;
                        ELSE
                           v_onde_new.onde_id := NULL;
                        END IF;
                        CLOSE c_onde_zoek;
                     END IF;
                  END IF;

                  --*** controleer te-koppelen of gekoppelde onderzoek
                  IF v_onde_new.onde_id IS NOT NULL
                  THEN
                     DECLARE
                        CURSOR c_onde_loc IS
                           SELECT onde.pers_id
                                , onde.onderzoeknr
                                , onde.ongr_id
                                , ongr.code ongr_code
                             FROM kgc_onderzoeken onde
                                , kgc_onderzoeksgroepen ongr
                            WHERE onde.onde_id = v_onde_new.onde_id
                              AND onde.ongr_id = ongr.ongr_id;
                           r_onde_loc c_onde_loc%ROWTYPE;
                     BEGIN
                        OPEN c_onde_loc;
                        FETCH c_onde_loc INTO r_onde_loc; -- bestaan is al gecontroleerd
                        -- Controleer afhankelijkheden
                        IF r_onde_loc.pers_id != v_onde_new.pers_id
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_onde_new.onde_id) ||
                                   ') verwijst in de importtabellen naar een andere persoon dan in Helix. Maak het Helix-id leeg (onderzoek wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of koppel het aan het juiste onderzoek.');
                           v_tab_imon_err(r_imon.imon_id) := TRUE;
                        END IF;
                        IF v_onde_new.onderzoeknr IS NOT NULL
                        THEN -- mag niet wijzigen
                           IF r_onde_loc.onderzoeknr != r_imon.onderzoeknr
                           THEN
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - onderzoek ' || v_onde_new.onderzoeknr  || ' heeft in Helix aan een ander onderzoeknummer (' || r_onde_loc.onderzoeknr || ')!');
                           v_tab_imon_err(r_imon.imon_id) := TRUE;
                           END IF;
                        END IF;
/* deze controles waren onterecht ingevoerd bij release 8.13. in 8.13.3 weer verwijderd!
                        IF r_onde_loc.ongr_id != v_onde_new.ongr_id
                        THEN
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - onderzoek ' || r_imon.onderzoeknr  || ' is in Helix aan een andere onderzoeksgroep (' || r_onde_loc.ongr_code || ') gekoppeld!');
                        v_tab_imon_err(r_imon.imon_id) := TRUE;
                        END IF;
                        CLOSE c_onde_loc;
*/
                     END;
                  END IF;

                  -- evt nieuw bepaalde gegevens ophalen
                  IF v_onde_new.onde_id IS NOT NULL
                  THEN
                     -- sla op in globale tabel
                     set_im2hlx(p_im => 'IMON' || r_imon.imon_id, p_hlx => 'ONDE' || v_onde_new.onde_id);

                     IF v_onde_old.the_rowid IS NULL
                     THEN
                        v_onde_old.onde_id := v_onde_new.onde_id;
                        cg$kgc_onderzoeken.slct(v_onde_old);
                     END IF;
                     IF v_onde_new.the_rowid IS NULL
                     THEN
                        cg$kgc_onderzoeken.slct(v_onde_new); -- oud is de basis voor nieuw
                        -- ****** controleer en vertaal gegevens (obv evt nieuw opgehaalde gegevens)
                        v_imfi_opmerkingen_enti := NULL;
                        v_error_enti := FALSE;
                        IF NOT im2onde( p_imon => r_imon
                                      , p_id => v_opm_tab_en_id
                                      , p_onde_old => v_onde_old
                                      , x_onde => v_onde_new
                                      , x_opmerkingen => v_imfi_opmerkingen_enti)
                        THEN
                           v_error_enti := TRUE;
                        END IF;
                     END IF;
                  END IF;
                  -- v_imfi_opmerkingen_enti toevoegen aan opm
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,v_imfi_opmerkingen_enti);
                  v_tab_imon_err(r_imon.imon_id) := (v_tab_imon_err(r_imon.imon_id) OR v_error_enti);

                  IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                  THEN
                     show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                     IF v_verwerkingstatus = 'E'
                        OR (v_verwerkingswijze = 'WM' AND v_tab_imon_err(r_imon.imon_id)) -- dit onderzoek geeft een fout
--niet ok. onderzoek is een groep!                        OR (v_verwerkingswijze = 'WM' AND imxx_error('#ONIN#')) -- fout in indicaties
                        OR (v_verwerkingswijze = 'AON' AND imxx_error)
                     THEN
                        NULL;
                     ELSE
                        -- nog steeds geen blokkerende fout: toevoegen
                        IF v_onde_new.onde_id IS NULL
                        THEN
                           show_output('Onde nieuw toevoegen (' || r_imon.imon_id || ')!');
                           IF v_onde_new.onderzoeknr IS NULL
                           THEN
                              v_onde_new.onderzoeknr := kgc_nust_00.genereer_nr(p_nummertype => 'ONDE'
                                                                               ,p_ongr_id    => v_onde_new.ongr_id);
                           END IF;
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Nieuw onderzoek (' || v_onde_new.onderzoeknr || ') toegevoegd.');

                           -- tbv default-zetten:
                           v_onde_ind := cg$kgc_onderzoeken.cg$ind_true;
                           IF v_onde_new.onderzoekstype IS NULL  THEN v_onde_ind.onderzoekstype := FALSE; END IF;
                           IF v_onde_new.spoed IS NULL  THEN v_onde_ind.spoed := FALSE; END IF;
                           IF v_onde_new.declareren IS NULL  THEN v_onde_ind.declareren := FALSE; END IF;
                           IF v_onde_new.datum_binnen IS NULL  THEN v_onde_ind.datum_binnen := FALSE; END IF;
                           -- op dit moment het onderzoek nog niet afronden
                           v_onde_ind.afgerond := FALSE; -- dit wordt na toevoeging van alle gerelateerde zaken gedaan

                           cg$kgc_onderzoeken.ins(cg$rec => v_onde_new
                                                 ,cg$ind => v_onde_ind);
                           set_im2hlx(p_im => 'IMON' || r_imon.imon_id, p_hlx => 'ONDE' || v_onde_new.onde_id);
                           -- mantis 5715: copy notities
                           copy_notities(p_imfi_id, 'ONDE', v_onde_new.onde_id);
                           set_atwa(p_atwa_waarde => r_imon.extra1
                                   ,p_spec_waarde => kgc_attr_00.waarde('KGC_IMPORT_FILES', 'ONDE_EXTRA1', p_imfi_id)
                                   ,p_tabel_naam  => 'KGC_ONDERZOEKEN'
                                   ,p_id          => v_onde_new.onde_id);
                        ELSE
                           -- update: eventueel! (afhankelijk van AFGEROND; zie afsluiting)
                           v_onde_ind := v_onde_ind_false;
                           v_imfi_verslag_tmp := verschil_onde( p_o => v_onde_old -- tbv wijzigingsverslag
                                                              , p_n => v_onde_new
                                                              , p_i => v_onde_ind
                                                              );
                           IF v_imfi_verslag_tmp IS NULL
                           THEN
                              -- geen te wijzigen kolommen
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Onderzoek ' || v_onde_old.onderzoeknr || ' is ongewijzigd.');
                           ELSIF r_imfi.onde_mons_toevoegen = 'J' -- rde 15-09-2009 Mantis 3184: Interface voor Dragon;
                           THEN -- indien alleen maar toevoegen: ook geen update!
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Onderzoek ' || v_onde_old.onderzoeknr || ' wordt niet gewijzigd.');
                           ELSIF v_onde_old.afgerond = 'J'
                           THEN
                              -- geen aktie
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Onderzoek ' || v_onde_old.onderzoeknr ||
                                      ' wordt niet gewijzigd: het is reeds afgerond!');
                           ELSE
                              -- op dit moment het onderzoek nog niet afronden
                              v_onde_ind.afgerond := FALSE; -- dit wordt na toevoeging van alle gerelateerde zaken gedaan
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Onderzoek ' || v_onde_old.onderzoeknr || ' gewijzigd: ' || v_imfi_verslag_tmp);
                              cg$kgc_onderzoeken.upd(cg$rec => v_onde_new
                                                    ,cg$ind => v_onde_ind);
                              set_atwa(p_atwa_waarde => r_imon.extra1
                                      ,p_spec_waarde => kgc_attr_00.waarde('KGC_IMPORT_FILES', 'ONDE_EXTRA1', p_imfi_id)
                                      ,p_tabel_naam  => 'KGC_ONDERZOEKEN'
                                      ,p_id          => v_onde_new.onde_id);
                           END IF;
                        END IF;
                        IF r_imon.kgc_onde_id IS NULL
                        THEN
                           -- onde_id aanbrengen in interface
                           show_output('Onderzoeks-id in interface bijwerken!');
                           UPDATE kgc_im_onderzoeken imon
                              SET imon.kgc_onde_id = v_onde_new.onde_id
                            WHERE imon.imfi_id = p_imfi_id
                              AND imon.imon_id = r_imon.imon_id;
                        END IF;
                        -- Als laatste stap proberen we nu binnen het bericht een onderzoek aan het
                        -- monster te koppelen
                        IF r_imfi.standaard_logica = 'J'
                        THEN -- mantis 11620
                           koppel_onderzoek_monster(p_imfi_id            => p_imfi_id
                                                   ,p_imon_id            => r_imon.imon_id
                                                   ,x_fracties_betrekken => v_fracties_betrekken
                                                   ,x_verslag            => v_imfi_verslag);
                        END IF;
                     END IF;
                  END IF;

                  -- ****** controleer en vertaal gegevens ONDERZOEK_INDICATIE bij onderzoek
                  -- tbv error: sleutel is imon_id#imoi_id!
                  v_nrofonin := 0;
                  FOR r_imoi IN c_imoi(p_imfi_id
                                      ,r_imon.imon_id)
                  LOOP
                     v_onin_old := v_onin_null;
                     v_onin_new := v_onin_null;
                     -- bepaal verwijzing:
                     v_err_id := r_imoi.imon_id || '#' || r_imoi.imoi_id;
                     v_opm_tab_en_id := 'ONDERZOEK_INDICATIE(' || v_opm_tab_en_id_pers || 'ONDE-' || r_imoi.imon_id || '; ' || r_imoi.imoi_id || ')';
                     IF i_fase = v_c_checking THEN -- init
                        v_tab_imoi_err(v_err_id) := FALSE;
                     END IF;
                     show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_onin_id=' || r_imoi.kgc_onin_id || ')');

                     -- controleer koppeling
                     v_onin_old.onin_id := NVL(r_imoi.kgc_onin_id, TO_NUMBER(SUBSTR(get_im2hlx('IMOI' || v_err_id), 5)));
                     IF v_onin_old.onin_id IS NOT NULL -- is deze al eerder geweest?
                     THEN
                        BEGIN
                           cg$kgc_onderzoek_indicaties.slct(v_onin_old);
                           v_onin_new.onin_id := v_onin_old.onin_id;
                           cg$kgc_onderzoek_indicaties.slct(v_onin_new);
                           -- oud is de basis voor nieuw
                        EXCEPTION
                           WHEN OTHERS THEN -- no data found
                              cg$errors.clear;
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_onin_old.onin_id) ||
                                      ') verwijst niet naar een bestaande onderzoekindicatie. Maak het Helix-id leeg (onderzoekindicatie wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de onderzoekindicatiegegevens.');
                              v_tab_imoi_err(v_err_id) := TRUE;
                              v_verwerkingstatus := 'E';
                              EXIT; -- klaar met controleren
                        END;
                     END IF;

                     -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2onin( p_imoi => r_imoi
                                   , p_id => v_opm_tab_en_id
                                   , p_onin_old => v_onin_old
                                   , x_onin => v_onin_new
                                   , x_opmerkingen => v_imfi_opmerkingen_enti)
                     THEN
                        v_error_enti := TRUE;
                     END IF;

                     -- oei - rekening houden met het feit dat 2 im-indicaties gekoppeld kunnen zijn aan
                     -- dezelfde helix-indicatie (r_imoi.impw_id_indi > helix_waarde)

                     -- probeer te koppelen
                     IF v_onin_new.onin_id IS NULL
                     THEN
                        IF v_onin_new.indi_id IS NOT NULL
                        THEN
                           -- bestaat de onin al?
                           BEGIN
                              SELECT MAX(onin.onin_id)
                                INTO v_onin_new.onin_id
                                FROM kgc_onderzoek_indicaties onin
                               WHERE onin.onde_id = v_onin_new.onde_id
                                 AND onin.indi_id = v_onin_new.indi_id;
                           EXCEPTION
                              WHEN OTHERS THEN
                                 v_onin_new.onin_id := NULL;
                           END;
                        END IF;
                     END IF;

                     -- evt nieuw bepaalde gegevens ophalen
                     IF v_onin_new.onin_id IS NOT NULL
                     THEN
                        -- sla op in globale tabel
                        set_im2hlx(p_im => 'IMOI' || v_err_id, p_hlx => 'ONIN' || v_onin_new.onin_id);

                        IF v_onin_old.the_rowid IS NULL
                        THEN
                           v_onin_old.onin_id := v_onin_new.onin_id;
                           cg$kgc_onderzoek_indicaties.slct(v_onin_old);
                        END IF;
                        IF v_onin_new.the_rowid IS NULL
                        THEN
                           cg$kgc_onderzoek_indicaties.slct(v_onin_new); -- oud is de basis voor nieuw
                           -- ****** controleer en vertaal gegevens (obv evt nieuw opgehaalde gegevens)
                           v_imfi_opmerkingen_enti := NULL;
                           v_error_enti := FALSE;
                           IF NOT im2onin( p_imoi => r_imoi
                                         , p_id => v_opm_tab_en_id
                                         , p_onin_old => v_onin_old
                                         , x_onin => v_onin_new
                                         , x_opmerkingen => v_imfi_opmerkingen_enti)
                           THEN
                              v_error_enti := TRUE;
                           END IF;
                        END IF;
                     END IF;
                     -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_imfi_opmerkingen_enti);
                     v_tab_imoi_err(v_err_id) := (v_tab_imoi_err(v_err_id) OR v_error_enti);

                     IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                     THEN
                        show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                        IF v_verwerkingstatus = 'E'
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imon_err(r_imoi.imon_id)) -- het onderzoek geeft een fout
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imoi_err(r_imoi.imon_id || '#' || r_imoi.imoi_id)) -- deze indicatie geeft een fout
                           OR (v_verwerkingswijze = 'AON' AND imxx_error)
                        THEN
                           NULL;
                        ELSE
                           -- ******** de indicatie evt toevoegen
                           -- indien bestaande onderzoek al afgerond was => geen aktie
                           IF nvl(v_onde_old.afgerond
                                 ,'N') <> 'J'
                           THEN
                              show_output('Onderzoek-indicatie: ' || r_imoi.imon_id || '#' || r_imoi.imoi_id);
                              -- nog steeds geen blokkerende fout: toevoegen
                              IF v_onin_new.onin_id IS NULL
                              THEN
                                 show_output('Onin nieuw toevoegen (' || r_imoi.imoi_id || ')!');
                                 v_nrofonin := v_nrofonin + 1;
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Nieuwe onderzoekindicatie toegevoegd (' || kgc_util_00.pk2uk('INDI', v_onin_new.indi_id, 'CODE') || ')');

                                 -- tbv default-zetten:
                                 v_onin_ind := cg$kgc_onderzoek_indicaties.cg$ind_true;
                                 IF v_onin_new.hoofd_ind IS NULL  THEN v_onin_ind.hoofd_ind := FALSE; END IF;

                                 cg$kgc_onderzoek_indicaties.ins(cg$rec => v_onin_new
                                                                ,cg$ind => v_onin_ind);
                                 set_im2hlx(p_im => 'IMOI' || v_err_id, p_hlx => 'ONIN' || v_onin_new.onin_id);
                                 -- mantis 5715: copy notities
                                 copy_notities(p_imfi_id, 'ONIN', v_onin_new.onin_id);

                              ELSE
                                 v_onin_ind := v_onin_ind_false;
                                 v_imfi_verslag_tmp := verschil_onin( p_o => v_onin_old -- tbv wijzigingsverslag
                                                                    , p_n => v_onin_new
                                                                    , p_i => v_onin_ind
                                                                    );
                                 IF v_imfi_verslag_tmp IS NULL
                                 THEN
                                    -- geen te wijzigen kolommen
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,'Onderzoeksindicatie ' || kgc_util_00.pk2uk('INDI', v_onin_new.indi_id, 'CODE') || ' is ongewijzigd');
                                 ELSE
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,'Onderzoekindicatie gewijzigd (' || kgc_util_00.pk2uk('INDI', v_onin_new.indi_id, 'CODE') || ': ' || v_imfi_verslag_tmp || ')');
                                    cg$kgc_onderzoek_indicaties.upd(cg$rec => v_onin_new
                                                                   ,cg$ind => v_onin_ind);
                                 END IF;
                              END IF;

                              IF r_imoi.kgc_onin_id IS NULL
                              THEN -- onin_id aanbrengen in interface
                                 show_output('Ond.Indicatie-id in interface bijwerken!');
                                 UPDATE kgc_im_onderzoek_indicaties imoi
                                    SET imoi.kgc_onin_id = v_onin_new.onin_id
                                  WHERE imoi.imfi_id = p_imfi_id
                                    AND imoi.imon_id = r_imoi.imon_id
                                    AND imoi.imoi_id = r_imoi.imoi_id;
                              END IF;
                           END IF; -- nvl(v_onde_old.afgerond,'N') <> 'J'
                        END IF;
                     END IF;
                  END LOOP; -- c_imoi
                  -- verslag:
                  IF v_nrofonin > 0
                  THEN
                     addtext(v_imfi_verslag
                            ,v_max_len
                            ,'Bij onderzoek zijn ' || to_char(v_nrofonin) || ' indicaties toegevoegd/gewijzigd!');
                  END IF;

                  -- ****** controleer en vertaal gegevens ONDERZOEK_KOPIEHOUDERS bij onderzoek
                  -- tbv error: sleutel is imon_id#imok_id!

                  v_nrofonkh := 0;
                  FOR r_imok IN c_imok(p_imfi_id
                                      ,r_imon.imon_id)
                  LOOP
                     v_onkh_old := v_onkh_null; -- 8.13.2.1: was v_onin_old
                     v_onkh_new := v_onkh_null;
                     -- bepaal verwijzing:
                     v_err_id := r_imok.imon_id || '#' || r_imok.imok_id;
                     v_opm_tab_en_id := 'ONDERZOEK_KOPIEHOUDER(' || v_opm_tab_en_id_pers || 'ONDE-' || r_imok.imon_id || '; ' || r_imok.imok_id || ')';
                     IF i_fase = v_c_checking THEN -- init
                        v_tab_imok_err(v_err_id) := FALSE;
                     END IF;

                     -- controleer koppeling
                     v_onkh_old.onkh_id := NVL(r_imok.kgc_onkh_id, TO_NUMBER(SUBSTR(get_im2hlx('IMOK' || v_err_id), 5)));
                     IF v_onkh_old.onkh_id IS NOT NULL -- is deze al eerder geweest?
                     THEN
                        BEGIN
                           cg$kgc_onderzoek_kopiehouders.slct(v_onkh_old);
                           v_onkh_new.onkh_id := v_onkh_old.onkh_id;
                           cg$kgc_onderzoek_kopiehouders.slct(v_onkh_new);
                           -- oud is de basis voor nieuw
                        EXCEPTION
                           WHEN OTHERS THEN -- no data found
                              cg$errors.clear;
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_onkh_old.onkh_id) ||
                                      ') verwijst niet naar een bestaande onderzoek-kopiehouder. Maak het Helix-id leeg (onderzoek-kopiehouder wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de onderzoek-kopiehoudergegevens.');
                              v_tab_imok_err(v_err_id) := TRUE;
                              v_verwerkingstatus := 'E';
                              EXIT; -- klaar met controleren
                        END;
                     END IF;

                     -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2onkh( p_imok => r_imok
                                   , p_id => v_opm_tab_en_id
                                   , p_onkh_old => v_onkh_old
                                   , x_onkh => v_onkh_new
                                   , x_opmerkingen => v_imfi_opmerkingen_enti)
                     THEN
                        v_error_enti := TRUE;
                     END IF;

                     -- probeer te koppelen
                     -- oei - rekening houden met het feit dat 2 im-indicaties gekoppeld kunnen zijn aan
                     -- dezelfde helix-indicatie (r_imok.impw_id_rela > helix_waarde)
                     IF v_onkh_new.onkh_id IS NULL
                     THEN
                        IF v_onkh_new.rela_id IS NOT NULL
                        THEN
                           -- bestaat de onkh al?
                           BEGIN
                              SELECT MAX(onkh.onkh_id)
                                INTO v_onkh_new.onkh_id
                                FROM kgc_onderzoek_kopiehouders onkh
                               WHERE onkh.onde_id = v_onkh_new.onde_id
                                 AND onkh.rela_id = v_onkh_new.rela_id;
                           EXCEPTION
                              WHEN OTHERS THEN
                                 v_onkh_new.onkh_id := NULL;
                           END;
                        END IF;
                     END IF;

                     -- evt nieuw bepaalde gegevens ophalen
                     IF v_onkh_new.onkh_id IS NOT NULL
                     THEN
                        -- sla op in globale tabel
                        set_im2hlx(p_im => 'IMOK' || v_err_id, p_hlx => 'ONKH' || v_onkh_new.onkh_id);

                        IF v_onkh_old.the_rowid IS NULL
                        THEN
                           v_onkh_old.onkh_id := v_onkh_new.onkh_id;
                           cg$kgc_onderzoek_kopiehouders.slct(v_onkh_old);
                        END IF;
                        IF v_onin_new.the_rowid IS NULL
                        THEN
                           cg$kgc_onderzoek_kopiehouders.slct(v_onkh_new); -- oud is de basis voor nieuw
                           -- ****** controleer en vertaal gegevens (obv evt nieuw opgehaalde gegevens)
                           v_imfi_opmerkingen_enti := NULL;
                           v_error_enti := FALSE;
                           IF NOT im2onkh( p_imok => r_imok
                                         , p_id => v_opm_tab_en_id
                                         , p_onkh_old => v_onkh_old
                                         , x_onkh => v_onkh_new
                                         , x_opmerkingen => v_imfi_opmerkingen_enti)
                           THEN
                              v_error_enti := TRUE;
                           END IF;
                        END IF;
                     END IF;
                     -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_imfi_opmerkingen_enti);
                     v_tab_imok_err(v_err_id) := (v_tab_imok_err(v_err_id) OR v_error_enti);

                     IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                     THEN
                        show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                        IF v_verwerkingstatus = 'E'
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imon_err(r_imok.imon_id)) -- het onderzoek geeft een fout
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imok_err(r_imok.imon_id || '#' || r_imok.imok_id))
                           OR (v_verwerkingswijze = 'AON' AND imxx_error)
                        THEN
                           NULL;
                        ELSE
                           -- indien bestaande onderzoek al afgerond was => geen aktie
                           IF nvl(v_onde_old.afgerond
                                 ,'N') <> 'J'
                           THEN
                              -- ******** onderzoek_kopiehouder toevoegen
                              show_output('Onderzoek-kopiehouder: ' || r_imok.imon_id || '#' || r_imok.imok_id);
                              -- nog steeds geen blokkerende fout: toevoegen
                              IF v_onkh_new.onkh_id IS NULL
                              THEN
                                 show_output('Onkh nieuw toevoegen (' || r_imok.imok_id || ')!');
                                 v_nrofonkh := v_nrofonkh + 1;
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Nieuwe onderzoekkopiehouder toegevoegd (' || kgc_util_00.pk2uk('RELA', v_onkh_new.rela_id, 'CODE') || ')');

                                 -- tbv default-zetten:
                                 v_onkh_ind := cg$kgc_onderzoek_kopiehouders.cg$ind_true;

                                 cg$kgc_onderzoek_kopiehouders.ins(cg$rec => v_onkh_new
                                                                  ,cg$ind => v_onkh_ind);
                                 set_im2hlx(p_im => 'IMOK' || v_err_id, p_hlx => 'ONKH' || v_onkh_new.onkh_id);
                                 -- mantis 5715: copy notities
                                 copy_notities(p_imfi_id, 'ONKH', v_onkh_new.onkh_id);
                              ELSE
                                 v_onkh_ind := v_onkh_ind_false;
                                 v_imfi_verslag_tmp := verschil_onkh( p_o => v_onkh_old -- tbv wijzigingsverslag
                                                                    , p_n => v_onkh_new
                                                                    , p_i => v_onkh_ind
                                                                    );
                                 IF v_imfi_verslag_tmp IS NULL
                                 THEN
                                    -- geen te wijzigen kolommen
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,'Onderzoekkopiehouder ' || kgc_util_00.pk2uk('RELA', v_onkh_new.rela_id, 'CODE') || ' is ongewijzigd');
                                 ELSE
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,'Onderzoekkopiehouder gewijzigd (' || kgc_util_00.pk2uk('RELA', v_onkh_new.rela_id, 'CODE') || ': ' || v_imfi_verslag_tmp || ')');
                                    cg$kgc_onderzoek_kopiehouders.upd(cg$rec => v_onkh_new
                                                                     ,cg$ind => v_onkh_ind);
                                 END IF;
                              END IF;

                              IF r_imok.kgc_onkh_id IS NULL
                              THEN
                                 -- onkh_id aanbrengen in interface
                                 show_output('Ond.Kopiehouder-id in interface bijwerken!');
                                 UPDATE kgc_im_onde_kopie imok
                                    SET imok.kgc_onkh_id = v_onkh_new.onkh_id
                                  WHERE imok.imfi_id = p_imfi_id
                                    AND imok.imon_id = r_imok.imon_id
                                    AND imok.imok_id = r_imok.imok_id;
                              END IF;
                           END IF;
                        END IF;
                     END IF;
                  END LOOP; -- c_imok

                  -- verslag:
                  IF v_nrofonkh > 0
                  THEN
                     addtext(v_imfi_verslag
                            ,v_max_len
                            ,'Bij onderzoek zijn ' || to_char(v_nrofonkh) || ' kopiehouders toegevoegd/gewijzigd!');
                  END IF;

                  -- ****** controleer en vertaal gegevens UITSLAGEN bij onderzoek
                  -- tbv error: sleutel is imon_id#imui_id!
                  v_nrofuits := 0;
                  FOR r_imui IN c_imui(p_imfi_id
                                      ,r_imon.imon_id)
                  LOOP
                     v_uits_old := v_uits_null;
                     v_uits_new := v_uits_null;
                     v_datum_brie_print := NULL;
                     v_err_id := r_imui.imon_id || '#' || r_imui.imui_id;
                     IF i_fase = v_c_checking THEN -- init
                        v_tab_imui_err(v_err_id) := FALSE;
                     END IF;
                     v_opm_tab_en_id := 'UITSLAG(' || v_opm_tab_en_id_pers || 'ONDE-' || r_imui.imon_id || '; ' || r_imui.imui_id || ')';
                     -- bepaal verwijzing:
                     show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_uits_id=' || r_imui.kgc_uits_id || ')');

                     -- controleer koppeling
                     v_uits_old.uits_id := NVL(r_imui.kgc_uits_id, TO_NUMBER(SUBSTR(get_im2hlx('IMUI' || v_err_id), 5)));
                     IF v_uits_old.uits_id IS NOT NULL -- is deze al eerder geweest?
                     THEN
                        BEGIN
                           cg$kgc_uitslagen.slct(v_uits_old);
                           v_uits_new.uits_id := v_uits_old.uits_id;
                           cg$kgc_uitslagen.slct(v_uits_new);
                           -- oud is de basis voor nieuw
                        EXCEPTION
                           WHEN OTHERS THEN -- no data found
                              cg$errors.clear;
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || v_uits_old.uits_id ||
                                      ') verwijst niet naar een bestaande uitslag. Maak het Helix-id leeg (uitslag wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de uitslagengegevens.');
                              v_verwerkingstatus := 'E';
                              v_tab_imui_err(v_err_id) := TRUE;
                              EXIT; -- klaar met controleren
                        END;
                     END IF;

                     -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2uits( p_imui => r_imui
                                   , p_id => v_opm_tab_en_id
                                   , p_uits_old => v_uits_old
                                   , x_uits => v_uits_new
                                   , x_opmerkingen => v_imfi_opmerkingen_enti
                                   , x_datum_brie_print => v_datum_brie_print)
                     THEN
                        v_error_enti := TRUE;
                     END IF;

                     -- probeer te koppelen
                     IF v_uits_new.uits_id IS NULL
                     THEN
                        IF v_uits_new.brty_id IS NOT NULL
                        THEN
                           -- bestaat de uits al?
                           BEGIN
                              SELECT MAX(uits.uits_id)
                                INTO v_uits_new.uits_id
                                FROM kgc_uitslagen uits
                               WHERE uits.onde_id = v_uits_new.onde_id
                                 AND uits.brty_id = v_uits_new.brty_id;
                           EXCEPTION
                              WHEN OTHERS THEN
                                 v_uits_new.uits_id := NULL;
                           END;
                        END IF;
                     END IF;

                     -- evt nieuw bepaalde gegevens ophalen
                     IF v_uits_new.uits_id IS NOT NULL
                     THEN
                        -- sla op in globale tabel
                        set_im2hlx(p_im => 'IMUI' || v_err_id, p_hlx => 'UITS' || v_uits_new.uits_id);

                        IF v_uits_old.the_rowid IS NULL
                        THEN
                           v_uits_old.uits_id := v_uits_new.uits_id;
                           cg$kgc_uitslagen.slct(v_uits_old);
                        END IF;
                        IF v_uits_new.the_rowid IS NULL
                        THEN
                           cg$kgc_uitslagen.slct(v_uits_new); -- oud is de basis voor nieuw
                           -- ****** controleer en vertaal gegevens (obv evt nieuw opgehaalde gegevens)
                           v_imfi_opmerkingen_enti := NULL;
                           v_error_enti := FALSE;
                           IF NOT im2uits( p_imui => r_imui
                                         , p_id => v_opm_tab_en_id
                                         , p_uits_old => v_uits_old
                                         , x_uits => v_uits_new
                                         , x_opmerkingen => v_imfi_opmerkingen_enti
                                         , x_datum_brie_print => v_datum_brie_print)
                           THEN
                              v_error_enti := TRUE;
                           END IF;
                        END IF;
                     END IF;
                     -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_imfi_opmerkingen_enti);
                     v_tab_imui_err(v_err_id) := (v_tab_imui_err(v_err_id) OR v_error_enti);

                     -- controleer te-koppelen of gekoppelde uitslag
                     IF v_uits_new.uits_id IS NOT NULL
                     THEN
                        DECLARE
                           CURSOR c_uits_loc IS
                              SELECT uits.onde_id
                                   , onde.onderzoeknr
                                   , brty.code brty_code
                                FROM kgc_uitslagen uits
                                   , kgc_onderzoeken onde
                                   , kgc_brieftypes brty
                               WHERE uits.uits_id = v_uits_new.uits_id
                                 AND uits.onde_id = onde.onde_id
                                 AND uits.brty_id = brty.brty_id(+);
                           r_uits_loc c_uits_loc%ROWTYPE;
                        BEGIN
                           OPEN c_uits_loc;
                           FETCH c_uits_loc INTO r_uits_loc;
                           IF r_uits_loc.onde_id != v_uits_new.onde_id
                           THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_uits_new.uits_id) ||
                                      ') verwijst in Helix naar een uitslag van onderzoek ' || r_uits_loc.onderzoeknr || '. In de importtabellen hoort deze uitslag echter bij een ander onderzoek. Maak het Helix-id leeg (uitslag wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of koppel het aan de juiste uitslag.');
                              v_tab_imui_err(v_err_id) := TRUE;
                           END IF;
                           CLOSE c_uits_loc;
                        END;
                     END IF;

                     IF v_uits_new.uits_id IS NULL
                     THEN -- toevoegen? Onderzoek mag niet afgerond zijn
                        IF NVL(v_onde_old.afgerond, 'N') = 'J'
                        THEN -- new=J? dan wordt deze nog afgerond...
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - er moet een nieuwe uitslag toegevoegd worden bij onderzoek '
                                   || v_onde_old.onderzoeknr || ', maar deze is reeds afgerond!');
                           v_tab_imui_err(v_err_id) := TRUE;
                        END IF;
                     END IF;

                     IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                     THEN
                        show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                        IF v_verwerkingstatus = 'E'
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imon_err(r_imui.imon_id)) -- het onderzoek geeft een fout
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imui_err(r_imui.imon_id || '#' || r_imui.imui_id))
                           OR (v_verwerkingswijze = 'AON' AND imxx_error)
                        THEN
                           NULL;
                        ELSE
                           IF v_uits_new.uits_id IS NULL
                           THEN -- toevoegen? Onderzoek mag niet afgerond zijn
                              show_output('Uits nieuw toevoegen (' || r_imui.imui_id || ')!');
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Nieuwe uitslag toegevoegd (' || kgc_util_00.pk2uk('BRTY', v_uits_new.brty_id, 'CODE') || ')');

                              -- tbv default-zetten:
                              v_uits_ind := cg$kgc_uitslagen.cg$ind_true;
                              IF v_uits_new.datum_uitslag IS NULL  THEN v_uits_ind.datum_uitslag := FALSE; END IF;

                              cg$kgc_uitslagen.ins(cg$rec => v_uits_new
                                                  ,cg$ind => v_uits_ind);
                              set_im2hlx(p_im => 'IMUI' || v_err_id, p_hlx => 'UITS' || v_uits_new.uits_id);
                              v_nrofuits := v_nrofuits + 1;
                              -- mantis 5715: copy notities
                              copy_notities(p_imfi_id, 'UITS', v_uits_new.uits_id);
                              -- indien v_datum_brie_print gevuld is + nieuwe uitslag, dan moet er een briefregistratie komen bij de uitslag.
                              IF v_datum_brie_print IS NOT NULL
                              THEN
                                 kgc_brie_00.registreer
                                 ( p_onde_id   => v_uits_new.onde_id
                                 , p_uits_id   => v_uits_new.uits_id
                                 , p_rela_id   => v_uits_new.rela_id
                                 , p_brty_id   => v_uits_new.brty_id
                                 , p_brieftype => 'UITSLAG'
                                 , p_geadresseerde => v_uits_new.volledig_adres
                                 , p_kopie     => 'N'
                                 , p_kafd_id   => v_uits_new.kafd_id
                                 , p_ongr_id   => v_onde_new.ongr_id
                                 , p_ramo_code => NULL
                                 , p_taal_id   => v_onde_new.taal_id
                                 , p_destype   => NULL
                                 , p_best_id   => NULL
                                 );
-- todo... bestand koppelen?
-- todo... check kopiehouders?
                              END IF;
                           ELSE
                              v_uits_ind := v_uits_ind_false;
                              v_imfi_verslag_tmp := verschil_uits( p_o => v_uits_old -- tbv wijzigingsverslag
                                                                 , p_n => v_uits_new
                                                                 , p_i => v_uits_ind
                                                                 );
                              IF v_imfi_verslag_tmp IS NULL
                              THEN
                                 -- geen te wijzigen kolommen
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Uitslag ' || v_onde_new.onderzoeknr || ' - ' || kgc_util_00.pk2uk('BRTY', v_uits_new.brty_id, 'CODE') || ' is ongewijzigd.');
                              ELSE
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Uitslag ' || v_onde_new.onderzoeknr || ' - ' || kgc_util_00.pk2uk('BRTY', v_uits_new.brty_id, 'CODE') || ' wordt NIET gewijzigd; aanwezige wijzigingen: ' || v_imfi_verslag_tmp);
                              END IF;
                           END IF;

                           IF r_imui.kgc_uits_id IS NULL
                           THEN
                              -- uits_id aanbrengen in interface
                              show_output('Uitslag-id in interface bijwerken!');
                              UPDATE kgc_im_uitslagen imui
                                 SET imui.kgc_uits_id = v_uits_new.uits_id
                               WHERE imui.imfi_id = p_imfi_id
                                 AND imui.imon_id = r_imui.imon_id
                                 AND imui.imui_id = r_imui.imui_id;
                           END IF;
                        END IF;
                     END IF;
                  END LOOP; -- c_imui
                  -- verslag:
                  IF v_nrofuits > 0
                  THEN
                     addtext(v_imfi_verslag
                            ,v_max_len
                            ,'Bij onderzoek zijn ' || to_char(v_nrofuits) || ' uitslagen toegevoegd/gewijzigd!');
                  END IF;

                  -- metingen: deze kunnen pas als de fracties e.d. ook toegevoegd zijn. Dus later...
               END LOOP; -- c_imon

               -- ****** controleer en vertaal gegevens MONSTER bij persoon
               FOR r_immo IN c_immo(p_imfi_id
                                   ,r_impe.impe_id)
               LOOP
                  v_tab_immo_err(r_immo.immo_id) := FALSE;
                  v_mons_old := v_mons_null;
                  v_mons_new := v_mons_null;
                  -- bepaal verwijzing:
                  v_opm_tab_en_id := 'MONSTER(' || v_opm_tab_en_id_pers || r_immo.immo_id || ')';
                  show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_mons_id=' || r_immo.kgc_mons_id || ')');

                  -- kgc_id leeg of moet bestaan
                  v_mons_old.mons_id := NVL(r_immo.kgc_mons_id, TO_NUMBER(SUBSTR(get_im2hlx('IMMO' || r_immo.immo_id), 5)));
                  IF v_mons_old.mons_id IS NOT NULL -- is deze al eerder geweest?
                  THEN
                     BEGIN
                        cg$kgc_monsters.slct(v_mons_old);
                        v_mons_new.mons_id := v_mons_old.mons_id;
                        cg$kgc_monsters.slct(v_mons_new);
                     EXCEPTION
                        WHEN OTHERS THEN -- no data found
                           cg$errors.clear;
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_mons_old.mons_id) || ') verwijst niet naar een bestaand monster. Maak het Helix-id leeg (monster wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de monstergegevens.');
                           v_tab_immo_err(r_immo.immo_id) := TRUE;
                           EXIT; -- klaar met controleren
                     END;
                  END IF;

                  -- ****** controleer en vertaal gegevens MONSTER
                  v_imfi_opmerkingen_enti := NULL;
                  v_error_enti := FALSE;
                  IF NOT im2mons( p_immo => r_immo
                                , p_id => v_opm_tab_en_id
                                , p_mons_old => v_mons_old
                                , p_type => NULL
                                , x_mons => v_mons_new
                                , x_opmerkingen => v_imfi_opmerkingen_enti)
                  THEN
                     v_error_enti := TRUE;
                  END IF;

                  IF NOT v_tab_immo_err(r_immo.immo_id)
                  THEN -- alleen dan proberen te koppelen
                     IF v_mons_new.mons_id IS NULL
                     THEN
                        v_imfi_verslag_tmp := NULL;
                        IF NOT koppel_mons( x_mons => v_mons_new
                                           , x_opmerkingen => v_imfi_verslag_tmp
                                           , p_onde_mons_toevoegen => r_imfi.onde_mons_toevoegen)
                        THEN -- fout!
                           v_tab_immo_err(r_immo.immo_id) := TRUE;
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || v_imfi_verslag_tmp);
                        END IF;
                     END IF;
                  END IF;

                  IF NOT v_tab_immo_err(r_immo.immo_id)
                  THEN -- alleen dan verder verwerken
                     -- evt nieuw bepaalde oude gegevens ophalen
                     IF v_mons_new.mons_id IS NOT NULL
                     THEN
                        IF v_mons_old.the_rowid IS NULL
                        THEN
                           v_mons_old.mons_id := v_mons_new.mons_id;
--show_output('5853 cg$kgc_monsters.slct mons_id=' || v_mons_old.mons_id);
                           cg$kgc_monsters.slct(v_mons_old);
                        END IF;
                     END IF;

                     -- controleer te-koppelen of gekoppelde monster
                     IF v_mons_new.mons_id IS NOT NULL
                     THEN
                        DECLARE
                           CURSOR c_mons_loc IS
                              SELECT mons.pers_id
                                   , mons.monsternummer
                                   , mons.ongr_id
                                   , mons.mate_id
                                   , ongr.code ongr_code
                                   , mons_kafd.code mons_kafd_code
                                   , mate.code mate_code
                                   , mate.omschrijving mate_omschrijving
                                   , mate_kafd.code mate_kafd_code
                                FROM kgc_monsters mons
                                   , kgc_onderzoeksgroepen ongr
                                   , kgc_kgc_afdelingen mons_kafd
                                   , kgc_materialen mate
                                   , kgc_kgc_afdelingen mate_kafd
                               WHERE mons.mons_id = v_mons_new.mons_id
                                 AND mons.ongr_id = ongr.ongr_id
                                 AND ongr.kafd_id = mons_kafd.kafd_id
                                 AND mons.mate_id = mate.mate_id
                                 AND mate.kafd_id = mate_kafd.kafd_id;
                           r_mons_loc c_mons_loc%ROWTYPE;
                           CURSOR c_mate_loc(p_mate_id NUMBER) IS
                              SELECT mate.code mate_code
                                   , mate.omschrijving mate_omschrijving
                                   , mate_kafd.code mate_kafd_code
                                FROM kgc_materialen mate
                                   , kgc_kgc_afdelingen mate_kafd
                               WHERE mate.mate_id = p_mate_id
                                 AND mate.kafd_id = mate_kafd.kafd_id;
                           r_mate_loc c_mate_loc%ROWTYPE;
                        BEGIN
                           OPEN c_mons_loc;
                           FETCH c_mons_loc INTO r_mons_loc;
                           IF c_mons_loc%NOTFOUND
                           THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_mons_new.mons_id) || ') verwijst niet naar een bestaand monster. Maak het Helix-id leeg (monster wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de monstergegevens.');
                              v_tab_immo_err(r_immo.immo_id) := TRUE;
                           ELSE -- bestaat! Controleer pers, ongr, mat
                              IF r_immo.kgc_mons_id IS NOT NULL AND r_mons_loc.pers_id != v_pers_new.pers_id
                              THEN
                                 addtext(v_imfi_opmerkingen
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - Helix-id (' || to_char(r_immo.kgc_mons_id) ||
                                         ') verwijst in de importtabellen naar een andere persoon dan in Helix. Maak het Helix-id leeg (monster wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of koppel het aan het juiste monster.');
                                 v_tab_immo_err(r_immo.immo_id) := TRUE;
                              END IF;
                              IF r_immo.monsternummer IS NOT NULL AND r_immo.monsternummer <> '*KOPPEL*' -- 8.13.2.1
                              THEN -- mag niet wijzigen
                                 IF r_mons_loc.monsternummer != r_immo.monsternummer
                                 THEN
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' - monster ' || r_immo.monsternummer || ' heeft in Helix aan een ander monsternummer (' || r_mons_loc.monsternummer || ')!');
                                    v_tab_immo_err(r_immo.immo_id) := TRUE;
                                 END IF;
                              END IF;
/* deze controles waren onterecht ingevoerd bij release 8.13. in 8.13.3 weer verwijderd!
                              IF r_mons_loc.ongr_id != v_mons_new.ongr_id
                              THEN
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - monster ' || v_mons_new.monsternummer || ' is in Helix aan een andere onderzoeksgroep (' || r_mons_loc.ongr_code || ') gekoppeld!');
                                 v_tab_immo_err(r_immo.immo_id) := TRUE;
                              END IF;
                              OPEN c_mate_loc(v_mons_new.mate_id);
                              FETCH c_mate_loc INTO r_mate_loc;
                              IF c_mate_loc%NOTFOUND
                              THEN
                                 NULL; -- dit is al gecontroleerd
                              ELSE
                                 IF r_mons_loc.mate_id != v_mons_new.mate_id
                                 THEN
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' - monster ' || v_mons_new.monsternummer || ' heeft in Helix materiaal ' || r_mons_loc.mate_code || ' (' || r_mons_loc.mate_omschrijving || '; ' || r_mons_loc.mate_kafd_code || '), en dit kan niet gewijzigd worden naar materiaal ' || r_mate_loc.mate_code || ' (' || r_mate_loc.mate_omschrijving || '; ' || r_mate_loc.mate_kafd_code || ')!');
                                    v_tab_immo_err(r_immo.immo_id) := TRUE;
                                 END IF;
                              END IF;
                              CLOSE c_mate_loc;
*/
                           END IF;
                           CLOSE c_mons_loc;
                        END;
                     END IF;
                     -- evt nieuw bepaalde nieuwe gegevens ophalen
                     IF v_mons_new.mons_id IS NOT NULL
                     THEN
                        -- sla op in globale tabel
                        set_im2hlx(p_im => 'IMMO' || r_immo.immo_id, p_hlx => 'MONS' || v_mons_new.mons_id);

                        IF v_mons_old.the_rowid IS NULL
                        THEN
                           v_mons_old.mons_id := v_mons_new.mons_id;
                           cg$kgc_monsters.slct(v_mons_old);
                        END IF;
                        IF v_mons_new.the_rowid IS NULL
                        THEN
                           cg$kgc_monsters.slct(v_mons_new); -- oud is de basis voor nieuw
                           v_imfi_opmerkingen_enti := NULL;
                           v_error_enti := FALSE;
                           IF NOT im2mons( p_immo => r_immo
                                         , p_id => v_opm_tab_en_id
                                         , p_mons_old => v_mons_old
                                         , p_type => NULL
                                         , x_mons => v_mons_new
                                         , x_opmerkingen => v_imfi_opmerkingen_enti)
                           THEN
                              v_error_enti := TRUE;
                           END IF;
                        END IF;
                     END IF;
                  END IF;
                  -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,v_imfi_opmerkingen_enti);
                  v_tab_immo_err(r_immo.immo_id) := (v_tab_immo_err(r_immo.immo_id) OR v_error_enti);

                  IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                  THEN
                     show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                     IF v_verwerkingstatus = 'E'
                        OR (v_verwerkingswijze = 'WM' AND v_tab_immo_err(r_immo.immo_id))
                        OR (v_verwerkingswijze = 'AON' AND imxx_error)
                     THEN
                        NULL;
                     ELSE
                        IF v_mons_new.the_rowid IS NULL
                        THEN
                           show_output('Monster nieuw toevoegen (' || v_opm_tab_en_id || ')!');
                           -- evt ook nieuw monsternummer uitgeven
                           IF v_mons_new.monsternummer IS NULL THEN
                              -- waarde1 ophalen - letter_monsternr!
                              r_mate := NULL;
                              OPEN c_mate(p_mate_id => v_mons_new.mate_id);
                              FETCH c_mate
                                 INTO r_mate;
                              CLOSE c_mate;
                              v_mons_new.monsternummer := kgc_nust_00.genereer_nr(p_nummertype => 'MONS'
                                                                                 ,p_ongr_id    => v_mons_new.ongr_id
                                                                                 ,p_waarde1    => r_mate.letter_monsternr);
                           END IF;
                           IF v_mons_new.datum_aanmelding IS NULL
                           THEN -- Mantis 3184: Interface voor Dragon: vullen met systeemdatum
                              v_mons_new.datum_aanmelding := SYSDATE;
                           END IF;
                           IF v_mons_new.mede_id IS NULL
                           THEN -- Mantis 3184: Interface voor Dragon: vullen met aangelogde gebruiker
                              v_mons_new.mede_id := kgc_mede_00.medewerker_id;
                           END IF;

                           -- tbv default-zetten:
                           v_mons_ind := cg$kgc_monsters.cg$ind_true;
                           IF v_mons_new.prematuur IS NULL  THEN v_mons_ind.prematuur := FALSE; END IF;
                           IF v_mons_new.bewaren IS NULL  THEN v_mons_ind.bewaren := FALSE; END IF;
                           IF v_mons_new.nader_gebruiken IS NULL  THEN v_mons_ind.nader_gebruiken := FALSE; END IF;
                           IF v_mons_new.declareren IS NULL  THEN v_mons_ind.declareren := FALSE; END IF;
                           IF v_mons_new.alleen_voor_opslag IS NULL  THEN v_mons_ind.alleen_voor_opslag := FALSE; END IF;
                           IF v_mons_new.datum_aanmelding IS NULL  THEN v_mons_ind.datum_aanmelding := FALSE; END IF;

                           cg$kgc_monsters.ins(cg$rec => v_mons_new
                                              ,cg$ind => v_mons_ind);
                           set_im2hlx(p_im => 'IMMO' || r_immo.immo_id, p_hlx => 'MONS' || v_mons_new.mons_id);
                           copy_notities(p_imfi_id, 'MONS', v_mons_new.mons_id);
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Nieuw monster (' || v_mons_new.monsternummer || ') toegevoegd ' || aantal_fracties(p_mons_id => v_mons_new.mons_id) || '.');
                        ELSE
                           -- update
                           show_output('Monster bijwerken!');
                           -- nooit bijwerken: onderzoeksgroep, monsternummer, afdeling ... zie hieronder!
                           v_mons_ind := v_mons_ind_false;
                           v_imfi_verslag_tmp := verschil_mons( p_o => v_mons_old -- tbv wijzigingsverslag
                                                              , p_n => v_mons_new
                                                              , p_i => v_mons_ind
                                                              );
                           IF v_imfi_verslag_tmp IS NULL
                           THEN -- geen te wijzigen kolommen!
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Monster ' || v_mons_old.monsternummer || ' is ongewijzigd ' || aantal_fracties(p_mons_id => v_mons_new.mons_id) || '.');
                           ELSIF r_imfi.onde_mons_toevoegen = 'J'
                                 OR r_immo.monsternummer = '*KOPPEL*'
                                 OR v_mons_new.mons_id IS NOT NULL
                           THEN -- indien alleen maar toevoegen: ook geen update!
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Monster ' || v_mons_old.monsternummer || ' wordt niet gewijzigd ' || aantal_fracties(p_mons_id => v_mons_new.mons_id) || '.');
                           ELSE
                              -- reeds opgewerkt?
                              SELECT COUNT(*)
                                INTO v_count
                                FROM bas_fracties frac
                               WHERE frac.mons_id = v_mons_old.mons_id;
                              IF v_count > 0
                              THEN
                                 -- geen aktie
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Monster ' || v_mons_old.monsternummer ||
                                         ' wordt niet gewijzigd: er zijn reeds fracties aanwezig ' || aantal_fracties(p_mons_id => v_mons_new.mons_id) || '!'); -- rework Mantis 3184: Interface voor Dragon
                              ELSE
                                 cg$kgc_monsters.upd(cg$rec => v_mons_new
                                                    ,cg$ind => v_mons_ind);

                                 -- mogelijk leeftijdscategorie en leeftijd bijwerken?
                                 v_mons_new.leeftijd := kgc_leef_00.leeftijd(p_pers_id      => v_mons_new.pers_id
                                                                            ,p_foet_id      => v_mons_new.foet_id
                                                                            ,p_datum_afname => v_mons_new.datum_afname); -- andere parameters (p_geboortedatum, p_datum_aterm worden autom. bepaald)
                                 v_mons_new.leef_id  := kgc_leef_00.leeftijdscategorie(p_kafd_id      => v_mons_new.kafd_id
                                                                                      ,p_aantal_dagen => v_mons_new.leeftijd);
                                 UPDATE kgc_monsters mons
                                    SET mons.leeftijd = nvl(v_mons_new.leeftijd
                                                           ,mons.leeftijd)
                                       ,mons.leef_id  = nvl(v_mons_new.leef_id
                                                           ,mons.leef_id)
                                  WHERE mons.mons_id = v_mons_new.mons_id;

                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Monster ' || v_mons_old.monsternummer || ' gewijzigd: ' || v_imfi_verslag_tmp || ' ' || aantal_fracties(p_mons_id => v_mons_new.mons_id) || '.');

                              END IF;
                           END IF;

                           -- ALLEEN igv monsternr = *KOPPEL* apart de notities kopieren.
                           -- Dit moet apart naast bovenstaande aanroep van copy_notities omdat bv na handmatige koppeling er geen gegevens gewijzigd hoeven te worden...
                           -- Dan toch proberen de nog niet aanwezige notities over te halen
                           IF r_immo.monsternummer = '*KOPPEL*'
                           THEN
                              copy_notities(p_imfi_id, 'MONS', v_mons_new.mons_id);
                           END IF;
                        END IF;

                        IF r_immo.kgc_mons_id IS NULL
                        THEN
                           -- mons_id aanbrengen in interface
                           show_output('Monster-id in interface bijwerken!');
                           UPDATE kgc_im_monsters immo
                              SET immo.kgc_mons_id = v_mons_new.mons_id
                            WHERE immo.imfi_id = p_imfi_id
                              AND immo.immo_id = r_immo.immo_id;
                        END IF;

                        -- Als laatste stap proberen we nu binnen het bericht een onderzoek aan het
                        -- monster te koppelen
                        IF r_imfi.standaard_logica = 'J'
                        THEN -- mantis 11620
                           koppel_onderzoek_monster(p_imfi_id            => p_imfi_id
                                                   ,p_immo_id            => r_immo.immo_id
                                                   ,x_fracties_betrekken => v_fracties_betrekken
                                                   ,x_verslag            => v_imfi_verslag);
                        END IF;
                     END IF;
                  END IF;
                  -- einde monster
                  -- ****** controleer en vertaal gegevens FRACTIES bij monster
                  -- tbv error: sleutel is imon_id#imok_id!
                  v_nroffrac := 0;
                  FOR r_imfr IN c_imfr(p_imfi_id
                                      ,r_immo.immo_id)
                  LOOP
                     v_frac_old := v_frac_null;
                     v_frac_new := v_frac_null;
                     v_err_id := r_imfr.immo_id || '#' || r_imfr.imfr_id;
                     v_tab_imfr_err(v_err_id) := FALSE;
                     -- bepaal verwijzing:
                     v_opm_tab_en_id := 'FRACTIE(' || v_opm_tab_en_id_pers || 'MONS-' || r_imfr.immo_id || '; ' || r_imfr.imfr_id || ')';
                     show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_frac_id=' || r_imfr.kgc_frac_id || ')');

                     -- controleer koppeling
                     v_frac_old.frac_id := NVL(r_imfr.kgc_frac_id, TO_NUMBER(SUBSTR(get_im2hlx('IMFR' || v_err_id), 5)));
                     IF v_frac_old.frac_id IS NOT NULL -- is deze al eerder geweest?
                     THEN
                        BEGIN
                           cg$bas_fracties.slct(v_frac_old);
                           v_frac_new.frac_id := v_frac_old.frac_id;
                           cg$bas_fracties.slct(v_frac_new);
                           -- oud is de basis voor nieuw
                        EXCEPTION
                           WHEN OTHERS THEN -- no data found
                              cg$errors.clear;
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || v_frac_old.frac_id ||
                                      ') bestaat niet als fractie. Maak het Helix-id leeg (fractie wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de fractiegegevens.');
                              v_verwerkingstatus := 'E';
                              v_tab_imfr_err(v_err_id) := TRUE;
                              EXIT; -- klaar met controleren
                        END;
                     END IF;

                     -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2frac( p_imfr => r_imfr
                                   , p_id => v_opm_tab_en_id
                                   , p_frac_old => v_frac_old
                                   , x_frac => v_frac_new
                                   , x_opmerkingen => v_imfi_opmerkingen_enti)
                     THEN
                        v_error_enti := TRUE;
                     END IF;

                     -- probeer te koppelen
                     IF v_frac_new.frac_id IS NULL THEN
                        IF v_frac_new.fractienummer IS NOT NULL
                        THEN -- koppel obv fractienummer (mantis 11620)
                           v_logregel := 'koppel frac obv nr=' || v_frac_new.fractienummer;
                           OPEN c_frac_zoek(p_fractienummer => v_frac_new.fractienummer);
                           FETCH c_frac_zoek
                              INTO r_frac_zoek;
                           IF c_frac_zoek%FOUND
                           THEN -- fractie bestaat!
                              v_frac_new.frac_id := r_frac_zoek.frac_id;
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Bestaande fractie gekoppeld obv fractienummer (' || v_frac_new.fractienummer || ')');
                              v_logregel := v_logregel || '... gevonden! frac_id=' || r_frac_zoek.frac_id;
                           ELSE -- niet gevonden; geen aktie: in de volgende stap worden de gegevens gecontroleerd
                              v_logregel := v_logregel || '... niet gevonden!';
                           END IF;
                           CLOSE c_frac_zoek;
                           show_output(v_logregel);
                        END IF;
                     END IF;

                     -- evt nieuw bepaalde gegevens ophalen
                     IF v_frac_new.frac_id IS NOT NULL
                     THEN
                        -- sla op in globale tabel
                        set_im2hlx(p_im => 'IMFR' || v_err_id, p_hlx => 'FRAC' || v_frac_new.frac_id);

                        IF v_frac_old.the_rowid IS NULL
                        THEN
                           v_frac_old.frac_id := v_frac_new.frac_id;
                           cg$bas_fracties.slct(v_frac_old);
                        END IF;
                        IF v_frac_new.the_rowid IS NULL
                        THEN
                           cg$bas_fracties.slct(v_frac_new); -- oud is de basis voor nieuw
                           -- ****** controleer en vertaal gegevens (obv evt nieuw opgehaalde gegevens)
                           v_imfi_opmerkingen_enti := NULL;
                           v_error_enti := FALSE;
                           IF NOT im2frac( p_imfr => r_imfr
                                         , p_id => v_opm_tab_en_id
                                         , p_frac_old => v_frac_old
                                         , x_frac => v_frac_new
                                         , x_opmerkingen => v_imfi_opmerkingen_enti)
                           THEN
                              v_error_enti := TRUE;
                           END IF;
                        END IF;
                     END IF;
                     -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_imfi_opmerkingen_enti);
                     v_tab_imfr_err(v_err_id) := (v_tab_imfr_err(v_err_id) OR v_error_enti);

                     -- controleer te-koppelen of gekoppelde fractie
                     IF v_frac_new.frac_id IS NOT NULL
                     THEN
                        DECLARE
                           CURSOR c_frac_loc IS
                              SELECT frac.mons_id
                                   , frac.fractienummer
                                   , mons.monsternummer
                                FROM bas_fracties frac
                                   , kgc_monsters mons
                               WHERE frac.frac_id = v_frac_new.frac_id
                                 AND frac.mons_id = mons.mons_id;
                           r_frac_loc c_frac_loc%ROWTYPE;
                        BEGIN
                           OPEN c_frac_loc;
                           FETCH c_frac_loc INTO r_frac_loc;
                           IF r_frac_loc.fractienummer != v_frac_new.fractienummer
                           THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - fractie ' || v_frac_new.fractienummer || ' heeft in Helix aan een ander fractienummer (' || r_frac_loc.fractienummer || ')!');
                              v_tab_imfr_err(v_err_id) := TRUE;
                           ELSIF r_frac_loc.mons_id != v_mons_new.mons_id
                           THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || v_frac_new.frac_id ||
                                      ') / fractie ' || v_frac_new.fractienummer || ' verwijst in Helix naar fractie ' || r_frac_loc.fractienummer || ' van monster ' || r_frac_loc.monsternummer || '. In de importtabellen hoort deze fractie echter bij een ander monster. Maak het Helix-id leeg (fractie wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of koppel het aan de juiste fractie.');
                              v_tab_imfr_err(v_err_id) := TRUE;
                           END IF;
                           CLOSE c_frac_loc;
                        END;
                     END IF;

                     IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                     THEN
                        show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                        IF v_verwerkingstatus = 'E'
                           OR (v_verwerkingswijze = 'WM' AND v_tab_immo_err(r_imfr.immo_id)) -- het monster geeft een fout
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imfr_err(r_imfr.immo_id || '#' || r_imfr.imfr_id))
                           OR (v_verwerkingswijze = 'AON' AND imxx_error)
                        THEN
                           NULL;
                        ELSE
                           IF v_frac_new.frac_id IS NULL
                           THEN -- toevoegen? Onderzoek mag niet afgerond zijn
                              show_output('Frac nieuw toevoegen (' || r_imfr.imfr_id || ')!');
                              -- evt nieuw fractienummer
                              IF v_frac_new.fractienummer IS NULL
                              THEN
                                 v_frac_new.fractienummer := kgc_nust_00.genereer_nr(p_nummertype => 'FRAC'
                                                                                    ,p_ongr_id => v_mons_new.ongr_id
                                                                                    ,p_waarde1 => v_mons_new.monsternummer
                                                                                    ,p_waarde2 => kgc_util_00.pk2uk('FRTY', v_frac_new.frty_id, 'LETTER_FRACTIENR')
                                                                                    ,p_waarde3 => TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')
                                                                                    );
                              END IF;

                              -- tbv default-zetten:
                              v_frac_ind := cg$bas_fracties.cg$ind_true;
                              IF v_frac_new.controle IS NULL  THEN v_frac_ind.controle := FALSE; END IF;

                              cg$bas_fracties.ins(cg$rec => v_frac_new
                                                 ,cg$ind => v_frac_ind);
                              set_im2hlx(p_im => 'IMFR' || v_err_id, p_hlx => 'FRAC' || v_frac_new.frac_id);
                              v_nroffrac := v_nroffrac + 1;
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Nieuwe fractie toegevoegd (' || v_frac_new.fractienummer || ')');
                              -- mantis 5715: copy notities
                              copy_notities(p_imfi_id, 'FRAC', v_frac_new.frac_id);
                           ELSE
                              v_frac_ind := v_frac_ind_false;
                              v_imfi_verslag_tmp := verschil_frac( p_o => v_frac_old -- tbv wijzigingsverslag
                                                                 , p_n => v_frac_new
                                                                 , p_i => v_frac_ind
                                                                 );
                              IF v_imfi_verslag_tmp IS NULL
                              THEN -- geen te wijzigen kolommen
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Fractie ' || v_frac_new.fractienummer || ' is ongewijzigd.');
                              ELSE
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,'Fractie ' || v_frac_new.fractienummer || ' wordt NIET gewijzigd; aanwezige wijzigingen: ' || v_imfi_verslag_tmp);
                              END IF;
                           END IF;

                           IF r_imfr.kgc_frac_id IS NULL
                           THEN -- frac_id aanbrengen in interface
                              show_output('Fractie-id in interface bijwerken!');
                              UPDATE kgc_im_fracties imfr
                                 SET imfr.kgc_frac_id = v_frac_new.frac_id
                               WHERE imfr.imfi_id = p_imfi_id
                                 AND imfr.immo_id = r_imfr.immo_id
                                 AND imfr.imfr_id = r_imfr.imfr_id;
                           END IF;
                        END IF;
                     END IF;
                  END LOOP; -- c_imfr
                  -- verslag:
                  IF v_nroffrac > 0
                  THEN
                     addtext(v_imfi_verslag
                            ,v_max_len
                            ,'Bij monster zijn ' || to_char(v_nroffrac) || ' fracties toegevoegd/gewijzigd!');
                  END IF;

               END LOOP; -- c_immo

            END LOOP; -- c_impe

            -- nu zijn in principe alle onderzoeken / monsters / fracties beschikbaar
            -- ******** alle onbe / onmo controleren / toevoegen - per onderzoek
            -- ******** alle metingen controleren / toevoegen - per onderzoek
            FOR r_impe IN c_impe(p_imfi_id)
            LOOP
               IF v_impe_count > 1
               THEN
                  v_opm_tab_en_id_pers := 'PERS-' || r_impe.impe_id || '; ';
               END IF;

               FOR r_imon IN c_imon(p_imfi_id
                                   ,r_impe.impe_id)
               LOOP
                  -- ****** controleer en vertaal gegevens ONBE / ONMO bij onderzoek
                  v_nrofonbe := 0;
                  v_nrofonmo := 0;
                  FOR r_imob IN c_imob(p_imfi_id
                                      ,r_imon.imon_id)
                  LOOP
                     v_onbe_old := v_onbe_null;
                     v_onbe_new := v_onbe_null;
                     v_onmo_old := v_onmo_null;
                     v_onmo_new := v_onmo_null;
                     v_err_id := r_imob.imon_id || '#' || r_imob.imob_id;
                     v_tab_imob_err(v_err_id) := FALSE;
                     -- bepaal verwijzing:
                     v_opm_tab_en_id := 'ONMO-ONBE(' || v_opm_tab_en_id_pers || 'ONDE-' || r_imob.imon_id || '; ' || r_imob.imob_id || ')';
                     show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_onmo_id=' || r_imob.kgc_onmo_id || ', kgc_onbe_id=' || r_imob.kgc_onbe_id || ')');
                     -- monster gevuld > ONMO
                     -- fractie gevuld > ONBE
                     IF r_imob.immo_id IS NOT NULL AND r_imob.imfr_id IS NOT NULL
                     THEN
                        addtext(v_imfi_opmerkingen
                               ,v_max_len
                               ,v_opm_tab_en_id || ' - Zowel monster (voor onmo) als fractie (voor onbe) is gevuld; dit is niet toegestaan.');
                        v_verwerkingstatus := 'E';
                        v_tab_imob_err(v_err_id) := TRUE;
                        EXIT; -- klaar met controleren
                     END IF;

                     IF r_imob.kgc_onmo_id IS NOT NULL AND r_imob.kgc_onbe_id IS NOT NULL
                     THEN
                        addtext(v_imfi_opmerkingen
                               ,v_max_len
                               ,v_opm_tab_en_id || ' - Zowel helix-id voor onmo als helix-id voor onbe is gevuld; dit is niet toegestaan.');
                        v_verwerkingstatus := 'E';
                        v_tab_imob_err(v_err_id) := TRUE;
                        EXIT; -- klaar met controleren
                     END IF;

                     IF r_imob.immo_id IS NOT NULL
                     THEN
                        v_onmo_old.onmo_id := NVL(r_imob.kgc_onmo_id, TO_NUMBER(SUBSTR(get_im2hlx('IMMO' || r_imob.imob_id), 5)));
                        IF v_onmo_old.onmo_id IS NOT NULL
                        THEN -- ophalen voor vervolg
                           BEGIN
                              cg$kgc_onderzoek_monsters.slct(v_onmo_old);
                              v_onmo_new.onmo_id := v_onmo_old.onmo_id;
                              cg$kgc_onderzoek_monsters.slct(v_onmo_new);
                           EXCEPTION
                              WHEN OTHERS THEN
                                 cg$errors.clear;
                                 addtext(v_imfi_opmerkingen
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - Helix-onmo-id (' || to_char(v_onmo_old.onmo_id) ||
                                         ') bestaat niet als onderzoek-monster. Maak het Helix-id leeg (onderzoek-monster wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de onderzoek-monstergegevens.');
                                 v_verwerkingstatus := 'E';
                                 v_tab_imob_err(v_err_id) := TRUE;
                                 EXIT; -- klaar met controleren
                           END;
                        END IF;
                     ELSIF r_imob.imfr_id IS NOT NULL
                     THEN
                        v_onbe_old.onbe_id := NVL(r_imob.kgc_onbe_id, TO_NUMBER(SUBSTR(get_im2hlx('IMMO' || r_imob.imob_id), 5)));
                        IF v_onbe_old.onbe_id IS NOT NULL
                        THEN -- ophalen voor vervolg
                           BEGIN
                              cg$kgc_onderzoek_betrokkenen.slct(v_onbe_old);
                              v_onbe_new.onbe_id := v_onbe_old.onbe_id;
                              cg$kgc_onderzoek_betrokkenen.slct(v_onbe_new);
                           EXCEPTION
                              WHEN OTHERS THEN
                                 cg$errors.clear;
                                 addtext(v_imfi_opmerkingen
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - Helix-onbe-id (' || to_char(v_onbe_old.onbe_id) ||
                                         ') bestaat niet als onderzoek-betrokkene. Maak het Helix-id leeg (betrokkene wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de betrokkenegegevens.');
                                 v_verwerkingstatus := 'E';
                                 v_tab_imob_err(v_err_id) := TRUE;
                                 EXIT; -- klaar met controleren
                           END;
                        END IF;
                     END IF;

                     -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2onmobe( p_imob => r_imob
                                     , p_id => v_opm_tab_en_id
                                     , p_onmo_old => v_onmo_old
                                     , x_onmo => v_onmo_new
                                     , p_onbe_old => v_onbe_old
                                     , x_onbe => v_onbe_new
                                     , x_opmerkingen => v_imfi_opmerkingen_enti)
                     THEN
                        v_error_enti := TRUE;
                     END IF;

                     IF r_imob.immo_id IS NOT NULL
                     THEN -- *** naar onmo
                        -- gegevens tbv koppeling ophalen
                        -- onmo: niets nodig: onde-mons is uniek
                        IF v_onmo_new.onmo_id IS NULL
                        THEN
                           v_logregel := 'koppel onmo obv attributen onde=' || v_onmo_new.onde_id || '; mons=' || v_onmo_new.mons_id;
                           DECLARE
                              CURSOR c_onmo_loc IS
                                 SELECT onmo.*
                                   FROM kgc_onderzoek_monsters onmo
                                  WHERE onmo.onde_id = v_onmo_new.onde_id
                                    AND onmo.mons_id = v_onmo_new.mons_id;
                              r_onmo_loc c_onmo_loc%ROWTYPE;
                           BEGIN
                              OPEN c_onmo_loc;
                              FETCH c_onmo_loc
                                 INTO r_onmo_loc;
                              IF c_onmo_loc%FOUND THEN
                                 v_logregel := v_logregel || '; onmo gevonden: ' || r_onmo_loc.onmo_id;
                                 v_onmo_new.onmo_id := r_onmo_loc.onmo_id;
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - Onmo gekoppeld aan Helix-onmo obv ref-onde en ref-mons.');
                              ELSE
                                 v_logregel := v_logregel || '; geen onmo gevonden';
                              END IF;
                              CLOSE c_onmo_loc;
                           END;
                           show_output(v_logregel);
                        END IF;
                        -- *** einde naar onmo
                     ELSIF r_imob.imfr_id IS NOT NULL
                     THEN -- *** naar onbe
                        -- onbe: idem: we gaan uit van unieke fracties per onderzoek
                        -- kan er een poging tot koppelen gedaan worden?
                        IF v_onbe_new.onbe_id IS NULL
                        THEN
                           v_logregel := 'koppel onbe obv attributen onde=' || v_onbe_new.onde_id || '; frac=' || v_onbe_new.frac_id;
                           DECLARE
                              CURSOR c_onbe_loc IS
                                 SELECT onbe.*
                                   FROM kgc_onderzoek_betrokkenen onbe
                                  WHERE onbe.onde_id = v_onbe_new.onde_id
                                    AND onbe.frac_id = v_onbe_new.frac_id;
                              r_onbe_loc c_onbe_loc%ROWTYPE;
                           BEGIN
                              OPEN c_onbe_loc;
                              FETCH c_onbe_loc
                                 INTO r_onbe_loc;
                              IF c_onbe_loc%FOUND THEN
                                 v_logregel := v_logregel || '; onbe gevonden: ' || r_onbe_loc.onbe_id;
                                 v_onbe_new.onbe_id := r_onbe_loc.onbe_id;
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - Onbe gekoppeld aan Helix-onbe obv ref-onde en ref-frac.');
                              ELSE
                                 v_logregel := v_logregel || '; geen onbe gevonden';
                              END IF;
                              CLOSE c_onbe_loc;
                           END;
                           show_output(v_logregel);
                        END IF;
                     END IF;

                     -- evt nieuw bepaalde gegevens ophalen
                     IF r_imob.immo_id IS NOT NULL
                     THEN
                        IF v_onmo_new.onmo_id IS NOT NULL
                        THEN
                           -- sla op in globale tabel
                           set_im2hlx(p_im => 'IMOB' || r_imob.imob_id, p_hlx => 'ONMO' || v_onmo_new.onmo_id);

                           IF v_onmo_old.the_rowid IS NULL
                           THEN
                              v_onmo_old.onmo_id := v_onmo_new.onmo_id;
                              cg$kgc_onderzoek_monsters.slct(v_onmo_old);
                           END IF;

                           IF v_onmo_new.the_rowid IS NULL
                           THEN
                              cg$kgc_onderzoek_monsters.slct(v_onmo_new); -- oud is de basis voor nieuw
                              -- ****** controleer en vertaal gegevens ONBE/ONMO
                              v_imfi_opmerkingen_enti := NULL;
                              v_error_enti := FALSE;
                              IF NOT im2onmobe( p_imob => r_imob
                                              , p_id => v_opm_tab_en_id
                                              , p_onmo_old => v_onmo_old
                                              , x_onmo => v_onmo_new
                                              , p_onbe_old => v_onbe_old
                                              , x_onbe => v_onbe_new
                                              , x_opmerkingen => v_imfi_opmerkingen_enti)
                              THEN
                                 v_error_enti := TRUE;
                              END IF;
                           END IF;
                        ELSIF v_onbe_new.onbe_id IS NOT NULL
                        THEN
                           -- sla op in globale tabel
                           set_im2hlx(p_im => 'IMOB' || r_imob.imob_id, p_hlx => 'ONBE' || v_onbe_new.onbe_id);

                           IF v_onbe_old.the_rowid IS NULL
                           THEN
                              v_onbe_old.onbe_id := v_onbe_new.onbe_id;
                              cg$kgc_onderzoek_betrokkenen.slct(v_onbe_old);
                           END IF;

                           IF v_onbe_new.the_rowid IS NULL
                           THEN
                              cg$kgc_onderzoek_betrokkenen.slct(v_onbe_new); -- oud is de basis voor nieuw
                              -- ****** controleer en vertaal gegevens ONBE/ONMO
                              v_imfi_opmerkingen_enti := NULL;
                              v_error_enti := FALSE;
                              IF NOT im2onmobe( p_imob => r_imob
                                              , p_id => v_opm_tab_en_id
                                              , p_onmo_old => v_onmo_old
                                              , x_onmo => v_onmo_new
                                              , p_onbe_old => v_onbe_old
                                              , x_onbe => v_onbe_new
                                              , x_opmerkingen => v_imfi_opmerkingen_enti)
                              THEN
                                 v_error_enti := TRUE;
                              END IF;
                           END IF;
                        END IF;
                     END IF;
                     -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_imfi_opmerkingen_enti);
                     v_tab_imob_err(v_err_id) := (v_tab_imob_err(v_err_id) OR v_error_enti);

                     -- controleer koppelingen
                     IF r_imob.immo_id IS NOT NULL
                     THEN
                        -- controleer te-koppelen of gekoppelde onmo
                        IF v_onmo_new.onmo_id IS NOT NULL
                        THEN -- verwijzing naar onmo: geen wijziging in onde, mons
                           null;
                        END IF;
                     ELSIF r_imob.imfr_id IS NOT NULL
                     THEN
                        -- controleer te-koppelen of gekoppelde onbe
                        IF v_onbe_new.onbe_id IS NOT NULL
                        THEN -- verwijzing naar onbe: geen wijziging in onde, frac
                           null;
                        END IF;
                     END IF;

                     IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                     THEN
                        show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                        IF v_verwerkingstatus = 'E'
                           OR (v_tab_imob_err(r_imob.imon_id || '#' || r_imob.imob_id))
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imon_err(r_imob.imon_id)) -- het onderzoek geeft een fout
                           OR (v_verwerkingswijze = 'WM' AND r_imob.immo_id IS NOT NULL AND v_tab_immo_err(r_imob.immo_id)) -- het monster geeft een fout
                           OR (v_verwerkingswijze = 'WM' AND r_imob.imfr_id IS NOT NULL AND v_tab_imfr_err(r_imob.imfr_id)) -- de fractie geeft een fout
                           OR (v_verwerkingswijze = 'AON' AND imxx_error)
                           -- alle referenties aanwezig!
                        THEN
                           NULL;
                        ELSE
                           IF r_imob.immo_id IS NOT NULL
                           THEN -- ******** onmo ins/upd
                              IF v_onmo_new.the_rowid IS NULL
                              THEN
                                 show_output('Toevoegen ' || v_err_id);

                                 -- tbv default-zetten:
                                 v_onmo_ind := cg$kgc_onderzoek_monsters.cg$ind_true;

                                 cg$kgc_onderzoek_monsters.ins(cg$rec => v_onmo_new
                                                              ,cg$ind => v_onmo_ind);
                                 set_im2hlx(p_im => 'IMOB' || r_imob.imob_id, p_hlx => 'ONMO' || v_onmo_new.onmo_id);
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' toegevoegd.');
                                 v_nrofonmo := v_nrofonmo + 1;
                              ELSE  -- update
                                 v_onmo_ind := v_onmo_ind_false;
                                 v_imfi_verslag_tmp := verschil_onmo( p_o => v_onmo_old
                                                                    , p_n => v_onmo_new
                                                                    , p_i => v_onmo_ind);
                                 show_output('Wijzigen ' || v_err_id || ': ' || v_imfi_verslag_tmp);
                                 IF v_imfi_verslag_tmp IS NULL
                                 THEN
                                    -- geen te wijzigen kolommen
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' is ongewijzigd.');
                                 ELSE
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' wordt niet gewijzigd.');
                                 END IF;
                              END IF;

                              IF r_imob.kgc_onmo_id IS NULL
                              THEN
                                 -- id aanbrengen in interface
                                 show_output('onmo-id in interface bijwerken!');
                                 UPDATE kgc_im_onderzoek_betrokkenen imob
                                    SET imob.kgc_onmo_id = v_onmo_new.onmo_id
                                  WHERE imob.imfi_id = p_imfi_id
                                    AND imob.imob_id = r_imob.imob_id;
                              END IF;

                           ELSIF r_imob.imfr_id IS NOT NULL
                           THEN -- ******** onbe ins/upd
                              IF v_onbe_new.the_rowid IS NULL
                              THEN
                                 show_output('Toevoegen ' || v_err_id);

                                 -- tbv default-zetten:
                                 v_onbe_ind := cg$kgc_onderzoek_betrokkenen.cg$ind_true;
                                 IF v_onbe_new.declareren IS NULL  THEN v_onbe_ind.declareren := FALSE; END IF;
                                 IF v_onbe_new.adviesvrager IS NULL  THEN v_onbe_ind.adviesvrager := FALSE; END IF;
                                 IF v_onbe_new.aangedaan IS NULL  THEN v_onbe_ind.aangedaan := FALSE; END IF;
                                 IF v_onbe_new.drager IS NULL  THEN v_onbe_ind.drager := FALSE; END IF;
                                 IF v_onbe_new.neven_geadresseerde IS NULL  THEN v_onbe_ind.neven_geadresseerde := FALSE; END IF;
                                 IF v_onbe_new.probandus IS NULL  THEN v_onbe_ind.probandus := FALSE; END IF;

                                 cg$kgc_onderzoek_betrokkenen.ins(cg$rec => v_onbe_new
                                                                 ,cg$ind => v_onbe_ind);
                                 set_im2hlx(p_im => 'IMOB' || r_imob.imob_id, p_hlx => 'ONBE' || v_onbe_new.onbe_id);
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' toegevoegd.');
                                 v_nrofonbe := v_nrofonbe + 1;
                              ELSE  -- update
                                 v_onbe_ind := v_onbe_ind_false;
                                 v_imfi_verslag_tmp := verschil_onbe( p_o => v_onbe_old
                                                                    , p_n => v_onbe_new
                                                                    , p_i => v_onbe_ind);
                                 show_output('Wijzigen ' || v_err_id || ': ' || v_imfi_verslag_tmp);
                                 IF v_imfi_verslag_tmp IS NULL
                                 THEN
                                    -- geen te wijzigen kolommen
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' is ongewijzigd.');
                                 ELSE
                                    addtext(v_imfi_verslag
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' wordt niet gewijzigd.');
                                 END IF;
                              END IF;

                              IF r_imob.kgc_onbe_id IS NULL
                              THEN
                                 -- id aanbrengen in interface
                                 show_output('onbe-id in interface bijwerken!');
                                 UPDATE kgc_im_onderzoek_betrokkenen imob
                                    SET imob.kgc_onbe_id = v_onbe_new.onbe_id
                                  WHERE imob.imfi_id = p_imfi_id
                                    AND imob.imob_id = r_imob.imob_id;
                              END IF;
                           END IF;
                        END IF;
                     END IF;

                  END LOOP; -- c_imob

                  -- ****** controleer en vertaal gegevens METINGEN bij onderzoek
                  -- tbv error: sleutel is imon_id#imme_id!
                  v_nrofmeti := 0;
                  FOR r_imme IN c_imme(p_imfi_id
                                      ,r_imon.imon_id)
                  LOOP
                     v_meti_old := v_meti_null;
                     v_meti_new := v_meti_null;
                     -- bepaal verwijzing:
                     v_err_id := r_imme.imon_id || '#' || r_imme.imme_id;
                     v_opm_tab_en_id := 'METING(' || v_opm_tab_en_id_pers || 'ONDE-' || r_imme.imon_id || '; ' || r_imme.imme_id || ')';
                     v_tab_imme_err(v_err_id) := FALSE;
                     show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_meti_id=' || r_imme.kgc_meti_id || ')');

                     v_meti_old.meti_id := NVL(r_imme.kgc_meti_id, TO_NUMBER(SUBSTR(get_im2hlx('IMME' || v_err_id), 5)));
                     -- ophalen voor vervolg
                     IF v_meti_old.meti_id IS NOT NULL
                     THEN
                        BEGIN
                           cg$bas_metingen.slct(v_meti_old);
                           v_meti_new.meti_id := v_meti_old.meti_id;
                           cg$bas_metingen.slct(v_meti_new);
                        EXCEPTION
                           WHEN OTHERS THEN
                              cg$errors.clear;
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_meti_old.meti_id) ||
                                      ') bestaat niet als meting. Maak het Helix-id leeg (meting wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de metinggegevens.');
                              v_verwerkingstatus := 'E';
                              v_tab_imme_err(v_err_id) := TRUE;
                              EXIT; -- klaar met controleren
                        END;
                     END IF;

                     -- gegevens tbv koppeling ophalen
                     -- ****** controleer en vertaal gegevens METING
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2meti( p_imme => r_imme
                                   , p_id => v_opm_tab_en_id
                                   , p_meti_old => v_meti_old
                                   , x_meti => v_meti_new
                                   , x_opmerkingen => v_imfi_opmerkingen_enti)
                     THEN
                        v_error_enti := TRUE;
                     END IF;

                     -- kan er een poging tot koppelen gedaan worden?
                     IF v_meti_new.meti_id IS NULL
                     THEN
                        DECLARE
                           CURSOR c_meti_zoek_via_attr(p_attr_code VARCHAR2, p_waarde VARCHAR2) IS
                              SELECT meti.meti_id
                                FROM bas_metingen meti
                                   , kgc_attributen attr
                                   , kgc_attribuut_waarden atwa
                               WHERE attr.code = p_attr_code
                                 AND attr.attr_id = atwa.attr_id
                                 AND atwa.waarde = p_waarde
                                 AND atwa.id = meti.meti_id;
                           r_meti_zoek_via_attr c_meti_zoek_via_attr%ROWTYPE;
                        BEGIN
                           -- mogelijk: koppelen op externe id
                           IF r_imme.extra1 IS NULL THEN
                              show_output('koppel obv METI_EXTRA1 gaat niet: leeg.');
                           ELSE -- zoek op extra attribuut uit imfi
                              v_logregel := 'koppel obv METI_EXTRA1 (=' || r_imme.extra1 || '). ';
                              v_waarde := kgc_attr_00.waarde('KGC_IMPORT_FILES', 'METI_EXTRA1', p_imfi_id);
                              IF v_waarde IS NULL
                              THEN
                                 v_logregel := v_logregel || 'Echter: geen METI_EXTRA1 bij imfi gevonden.';
                              ELSE -- bv ATTv_meti_old_ID_MUMC
                                 v_logregel := v_logregel || '. imfi.METI_EXTRA1=' || v_waarde;
                                 IF SUBSTR(v_waarde, 1, 5) = 'ATTR_'
                                 THEN
                                    v_attr_code := SUBSTR(v_waarde, 6);
                                    OPEN c_meti_zoek_via_attr(p_attr_code => v_attr_code, p_waarde => r_imme.extra1);
                                    FETCH c_meti_zoek_via_attr INTO r_meti_zoek_via_attr;
                                    IF c_meti_zoek_via_attr%FOUND THEN
                                       v_logregel := v_logregel || 'Gevonden: meti_id=' || r_meti_zoek_via_attr.meti_id;
                                       v_meti_new.meti_id := r_meti_zoek_via_attr.meti_id;
                                       IF i_fase = v_c_updating
                                       THEN
                                          addtext(v_imfi_verslag
                                                 ,v_max_len
                                                 ,v_opm_tab_en_id || ' - Meting gekoppeld aan Helix-meting obv externe-id (extra1-' || r_imme.extra1 || ').');
                                       END IF;
                                    ELSE -- niet fout!
                                       v_logregel := v_logregel || 'Geen meti_id gevonden.';
                                    END IF;
                                    CLOSE c_meti_zoek_via_attr;
                                 END IF;
                              END IF;
                              show_output(v_logregel);
                           END IF;
                        END;
                     END IF;

                     -- evt nieuw bepaalde gegevens ophalen
                     IF v_meti_new.meti_id IS NOT NULL
                     THEN
                        -- sla op in globale tabel
                        set_im2hlx(p_im => 'IMME' || v_err_id, p_hlx => 'METI' || v_meti_new.meti_id);

                        IF v_meti_old.the_rowid IS NULL
                        THEN
                           v_meti_old.meti_id := v_meti_new.meti_id;
                           cg$bas_metingen.slct(v_meti_old);
                        END IF;
                        IF v_meti_new.the_rowid IS NULL
                        THEN
                           cg$bas_metingen.slct(v_meti_new); -- oud is de basis voor nieuw
                           -- ****** controleer en vertaal gegevens METING
                           v_imfi_opmerkingen_enti := NULL;
                           v_error_enti := FALSE;
                           IF NOT im2meti( p_imme => r_imme
                                         , p_id => v_opm_tab_en_id
                                         , p_meti_old => v_meti_old
                                         , x_meti => v_meti_new
                                         , x_opmerkingen => v_imfi_opmerkingen_enti)
                           THEN
                              v_error_enti := TRUE;
                           END IF;
                        END IF;
                     END IF;
                     -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_imfi_opmerkingen_enti);
                     v_tab_imme_err(v_err_id) := (v_tab_imme_err(v_err_id) OR v_error_enti);

                     -- controleer te-koppelen of gekoppelde meting
                     IF v_meti_new.the_rowid IS NULL
                     THEN -- toevoegen => check onderzoek afgerond
                        IF kgc_onde_00.afgerond(p_onde_id => v_meti_new.onde_id
                                               ,p_rftf => FALSE)
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' moet toegevoegd worden, maar het onderzoek is al afgerond!');
                           v_tab_imme_err(v_err_id) := TRUE;
                        END IF;
                     ELSE --  v_meti_new.meti_id IS NOT NULL
                        DECLARE
                           CURSOR c_meti_loc IS
                              SELECT meti.onde_id
                                   , mons.mons_id
                                   , meti.frac_id
                                   , onde.onderzoeknr
                                   , mons.monsternummer
                                   , frac.fractienummer
                                FROM bas_metingen meti
                                   , kgc_onderzoeken onde
                                   , kgc_onderzoek_monsters onmo
                                   , kgc_monsters mons
                                   , bas_fracties frac
                               WHERE meti.meti_id = v_meti_new.meti_id
                                 AND meti.onde_id = onde.onde_id(+)
                                 AND meti.onmo_id = onmo.onmo_id(+)
                                 AND onmo.mons_id = mons.mons_id(+)
                                 AND meti.frac_id = frac.frac_id(+);
                           v_meti_old_loc c_meti_loc%ROWTYPE;
                        BEGIN
                           OPEN c_meti_loc;
                           FETCH c_meti_loc INTO v_meti_old_loc;
                           IF c_meti_loc%NOTFOUND
                           THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_meti_new.meti_id) ||
                                      ') bestaat niet als meting. Maak het Helix-id leeg (meting wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de metinggegevens.');
                              v_tab_imme_err(v_err_id) := TRUE;
                           ELSE
                              IF v_meti_old_loc.onde_id != v_meti_new.onde_id
                              THEN
                                 addtext(v_imfi_opmerkingen
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_meti_new.meti_id) ||
                                         ') verwijst in Helix naar een meting van onderzoek ' || v_meti_old_loc.onderzoeknr || '. In de importtabellen hoort deze meting echter bij een ander onderzoek. Maak het Helix-id leeg (meting wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of koppel het aan de juiste meting.');
                                 v_tab_imme_err(v_err_id) := TRUE;
                              END IF;
                              -- check verwijzing immo/imfr
                              IF r_imme.immo_id IS NOT NULL THEN
                                 OPEN c_immo1(p_imfi_id => p_imfi_id
                                             ,p_immo_id => r_imme.immo_id
                                             ,p_onde_id => r_imon.kgc_onde_id);
                                 FETCH c_immo1
                                    INTO r_immo1; -- altijd gevonden: FK.
                                 IF r_immo1.monsternummer !=  v_meti_old_loc.monsternummer
                                 THEN
                                    addtext(v_imfi_opmerkingen
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_meti_new.meti_id) ||
                                            ') verwijst in Helix naar een meting van monster ' || v_meti_old_loc.monsternummer || '. In de importtabellen hoort deze meting echter bij een ander monster (' || r_immo1.monsternummer || '). Maak het Helix-id leeg (meting wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of koppel het aan de juiste meting.');
                                    v_tab_imme_err(v_err_id) := TRUE;
                                 END IF;
                                 CLOSE c_immo1;
                                 v_meti_new.onmo_id := r_immo1.onmo_id;
                              END IF;
                              IF r_imme.imfr_id IS NOT NULL THEN
                                 OPEN c_imfr1(p_imfi_id => p_imfi_id
                                             ,p_immo_id => r_imme.immo_id
                                             ,p_imfr_id => r_imme.imfr_id);
                                 FETCH c_imfr1
                                    INTO r_imfr1; -- altijd gevonden: FK.
                                 IF r_imfr1.fractienummer !=  v_meti_old_loc.fractienummer
                                 THEN
                                    addtext(v_imfi_opmerkingen
                                           ,v_max_len
                                           ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_meti_new.meti_id) ||
                                            ') verwijst in Helix naar een meting van fractie ' || v_meti_old_loc.fractienummer || '. In de importtabellen hoort deze meting echter bij een andere fractie (' || r_imfr1.fractienummer || '). Maak het Helix-id leeg (meting wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of koppel het aan de juiste meting.');
                                    v_tab_imme_err(v_err_id) := TRUE;
                                 END IF;
                                 v_meti_new.frac_id := r_imfr1.frac_id;
                                 CLOSE c_imfr1;
                              END IF;
                              -- ophalen voor vervolg
                              cg$bas_metingen.slct(v_meti_old);
                           END IF;
                           CLOSE c_meti_loc;
                        END;
                     END IF;

                     IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                     THEN
                        show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                        IF v_verwerkingstatus = 'E'
                           OR (v_tab_imme_err(r_imme.imon_id || '#' || r_imme.imme_id))
                           OR (v_verwerkingswijze = 'AON' AND imxx_error)
                           -- alle referenties aanwezig?
                           OR (r_imme.imon_id IS NOT NULL AND v_meti_new.onde_id IS NULL)
                           OR (r_imme.imfr_id IS NOT NULL AND v_meti_new.frac_id IS NULL)
                           OR (v_verwerkingswijze = 'WM' AND v_tab_imon_err(r_imme.imon_id)) -- het onderzoek geeft een fout
                           OR r_imon.kgc_onde_id IS NULL
                        THEN
                           NULL;
                        ELSE
                        show_output('Aktie! r_imme.imon_id=' || r_imme.imon_id || '; v_meti_new.onde_id=' || v_meti_new.onde_id);
                           -- ellendig: bij ins en upd wordt onde_id afgeleid uit onmo_id... indien leeg, dan wordt onde_id leeggemaakt???
                           DECLARE
                              CURSOR c_onmo_loc IS
                                 SELECT onmo.onmo_id
                                 FROM   kgc_onderzoek_monsters onmo
                                 ,      bas_fracties frac
                                 WHERE  onmo.onde_id = v_meti_new.onde_id
                                 AND    onmo.mons_id = frac.mons_id
                                 AND    frac.frac_id = v_meti_new.frac_id
                                 ;
                           BEGIN
                              IF v_meti_new.onmo_id IS NULL
                              THEN
                                 OPEN c_onmo_loc;
                                 FETCH c_onmo_loc INTO v_meti_new.onmo_id;
                                 CLOSE c_onmo_loc;
                              END IF;
                           END;
                           -- ******** metingen toevoegen
                           IF v_meti_new.the_rowid IS NULL
                           THEN
                              show_output('Toevoegen ' || v_err_id);

                              -- tbv default-zetten:
                              v_meti_ind := cg$bas_metingen.cg$ind_true;
                              IF v_meti_new.datum_aanmelding IS NULL  THEN v_meti_ind.datum_aanmelding := FALSE; END IF;
                              IF v_meti_new.afgerond IS NULL  THEN v_meti_ind.afgerond := FALSE; END IF;
                              IF v_meti_new.herhalen IS NULL  THEN v_meti_ind.herhalen := FALSE; END IF;
                              IF v_meti_new.snelheid IS NULL  THEN v_meti_ind.snelheid := FALSE; END IF;
                              IF v_meti_new.prioriteit IS NULL  THEN v_meti_ind.prioriteit := FALSE; END IF;
                              IF v_meti_new.declareren IS NULL  THEN v_meti_ind.declareren := FALSE; END IF;

                              cg$bas_metingen.ins(cg$rec => v_meti_new
                                                 ,cg$ind => v_meti_ind);
                              set_im2hlx(p_im => 'IMME' || v_err_id, p_hlx => 'METI' || v_meti_new.meti_id);
                              copy_notities(p_imfi_id, 'METI', v_meti_new.meti_id); -- mantis 5715: copy notities
                              set_atwa(p_atwa_waarde => r_imme.extra1
                                      ,p_spec_waarde => kgc_attr_00.waarde('KGC_IMPORT_FILES', 'METI_EXTRA1', p_imfi_id)
                                      ,p_tabel_naam  => 'BAS_METINGEN'
                                      ,p_id          => v_meti_new.meti_id);
                              v_nrofmeti := v_nrofmeti + 1;
                           ELSE  -- update
                              v_meti_ind := v_meti_ind_false;
                              v_imfi_verslag_tmp := verschil_meti( p_o => v_meti_old
                                                                 , p_n => v_meti_new
                                                                 , p_i => v_meti_ind);
                              show_output('Wijzigen ' || v_err_id || ': ' || v_imfi_verslag_tmp);
                              IF v_imfi_verslag_tmp IS NULL
                              THEN
                                 -- geen te wijzigen kolommen
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' is ongewijzigd.');
                              ELSE
                                 addtext(v_imfi_verslag
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' wordt niet gewijzigd.');
                              END IF;
                           END IF;

                           IF r_imme.kgc_meti_id IS NULL
                           THEN
                              -- uits_id aanbrengen in interface
                              show_output('Meting-id in interface bijwerken!');
                              UPDATE kgc_im_metingen imme
                                 SET imme.kgc_meti_id = v_meti_new.meti_id
                               WHERE imme.imfi_id = p_imfi_id
                                 AND imme.imme_id = r_imme.imme_id;
                           END IF;
                        END IF;
                     END IF;
                  END LOOP; -- c_imme
                  -- verslag:
                  IF v_nrofmeti > 0
                  THEN
                     addtext(v_imfi_verslag
                            ,v_max_len
                            ,'Bij onderzoek zijn ' || to_char(v_nrofmeti) || ' metingen toegevoegd/gewijzigd!');
                  END IF;

                  --*** evt onderzoek afronden
                  IF i_fase = v_c_updating -- evt insert/update afronden?
                  THEN
                     show_output('Onderzoek afronden?; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || '; afgerond=' || r_imon.afgerond || ')');
                     -- is er iets fout gegaan? Mogelijk is het onderzoek OK, maar zijn onderliggende onderdelen nog niet toegevoegd
                     -- door een fout. Dan nog niet afronden.
                     IF v_verwerkingstatus = 'E'
                        OR (v_verwerkingswijze = 'WM' AND imxx_error('#ONDE#ONIN#ONKH#UITS#METI#ONMO#ONBE#')) -- alles wat samenhangt met een onderzoek
                        OR (v_verwerkingswijze = 'AON' AND imxx_error)
                     THEN
                        NULL;
                     ELSE
                        -- afronding onderzoek
                        IF r_imon.afgerond = 'J'
                        THEN
                           -- onderzoek bijwerken als 'afgerond' - kan nu pas, want anders fout bij bijwerken indicaties e.d.
                           UPDATE kgc_onderzoeken onde
                              SET onde.afgerond = 'J'
                            WHERE onde.onde_id = r_imon.kgc_onde_id
                              AND nvl(onde.afgerond
                                     ,'N') <> 'J';
                        END IF;
                        -- Mantis 3184: Interface voor Dragon:
                        -- koppeling familieonderzoek - persoon - onderzoek leggen
                        koppel_faon_pers_onde(p_imfi_id => p_imfi_id
                                             ,p_pers_id => r_impe.kgc_pers_id
                                             ,p_onde_id => r_imon.kgc_onde_id
                                             ,x_verslag => v_imfi_verslag);

                     END IF; -- evt niet doen door fout
                  END IF;
               END LOOP; -- c_imon
            END LOOP; -- c_impe

            -- ****** controleer en vertaal gegevens FAMILIE
            -- alle families
            FOR r_imfa IN c_imfa(p_imfi_id)
            LOOP
               v_fami_old := v_fami_null;
               v_fami_new := v_fami_null;
               v_tab_imfa_err(r_imfa.imfa_id) := FALSE;
               v_opm_tab_en_id := 'FAMILIE(' || r_imfa.imfa_id || ')';
               show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_fami_id=' || r_imfa.kgc_fami_id || ')');

               IF c_imfa%ROWCOUNT > 1
               THEN
                  -- mag niet: hele verwerking in de war!
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,'Import met id ' || to_char(p_imfi_id) || ' / naam ' || r_imfi.filenaam ||
                          ' bevat een fout: er is meer dan 1 familie aangetroffen!');
                  v_verwerkingstatus := 'E';
                  v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                  EXIT; -- klaar met controleren
               END IF;
               IF v_verwerkingstatus = 'E'
               THEN
                  EXIT;
               END IF;

               -- controleer koppeling
               v_fami_old.fami_id := NVL(r_imfa.kgc_fami_id, TO_NUMBER(SUBSTR(get_im2hlx('IMFA' || r_imfa.imfa_id), 5)));
               IF v_fami_old.fami_id IS NOT NULL -- is deze al eerder geweest?
               THEN
                  BEGIN
                     cg$kgc_families.slct(v_fami_old);
                     v_fami_new.fami_id := v_fami_old.fami_id;
                     cg$kgc_families.slct(v_fami_new);
                     -- oud is de basis voor nieuw
                  EXCEPTION
                     WHEN OTHERS THEN -- no data found
                        cg$errors.clear;
                        addtext(v_imfi_opmerkingen
                               ,v_max_len
                               ,v_opm_tab_en_id || ' - Helix-id (' || v_fami_old.fami_id ||
                                ') verwijst niet naar een bestaande familie. Maak het Helix-id leeg (familie wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de familiegegevens.');
                        v_verwerkingstatus := 'E';
                        v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                        EXIT; -- klaar met controleren
                  END;
               END IF;

               -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
               v_imfi_opmerkingen_enti := NULL;
               v_error_enti := FALSE;
               IF NOT im2fami( p_imfa => r_imfa
                             , p_id => v_opm_tab_en_id
                             , p_fami_old => v_fami_old
                             , x_fami => v_fami_new
                             , x_opmerkingen => v_imfi_opmerkingen_enti)
               THEN
                  v_error_enti := TRUE;
               END IF;

               -- probeer te koppelen
               IF v_fami_new.fami_id IS NULL
               THEN
                  -- een familie moet gekoppeld worden aan de niet-anonieme persoon
                  IF v_pers_id_fami IS NULL AND imxx_error('#PERS#') THEN
                     -- met deze combi kan geen controle worden uitgevoerd. Indien geen errors: dan een nieuwe persoon!
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_opm_tab_en_id || ' - De controle op aanwezige familie kan niet uitgevoerd worden aangezien de Helix-persoon niet bekend is.');
                     v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                  ELSE
                     -- let op: v_pers_id_fami kan leeg zijn = nieuwe persoon
                     SELECT COUNT(*)
                       INTO v_fale_count
                       FROM kgc_familie_leden fale
                      WHERE fale.pers_id = v_pers_id_fami;
                     IF v_fami_new.familienummer = '*MERGE*'
                     THEN
                        IF v_fale_count = 0
                        THEN -- naam verplicht
                           IF v_fami_new.naam IS NOT NULL
                           THEN  -- ok, nieuwe familie toevoegen en koppelen
                              NULL;
                           ELSE
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - Naam moet gevuld zijn!');
                              v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                           END IF;
                        ELSIF v_fale_count = 1
                        THEN -- deze familie moet worden gebruik!
                           OPEN c_fale_zoek(p_pers_id => v_pers_id_fami);
                           FETCH c_fale_zoek INTO v_fami_new.fami_id;
                           CLOSE c_fale_zoek;
                        ELSIF v_fale_count > 1
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - De opgegeven persoon zit in ' || v_fale_count || ' Helix-families (' || kgc_pers_00.families(v_pers_id_fami, 'K') || '); koppel handmatig!');
                           v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                        END IF;
                        v_fami_new.familienummer := NULL; -- zeker niet overnemen in familienummer!
                     ELSIF v_fami_new.familienummer LIKE '*ID%'
                     THEN
                        -- zoek gerelateerde imfi
                        OPEN c_imfi_fami_zoek(p_imlo_id => r_imfi.imlo_id, p_filenaam => SUBSTR(v_fami_new.familienummer, 4));
                        FETCH c_imfi_fami_zoek INTO r_imfi_fami_zoek;
                        IF c_imfi_fami_zoek%NOTFOUND
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - De verwijzing naar importbestand met id ' || v_fami_new.familienummer || ' kon niet gevonden worden; koppel de familie handmatig!');
                           v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                           v_fami_new.familienummer := NULL; -- zeker niet overnemen in familienummer; wordt mogelijk verder gebruikt!
                        ELSIF r_imfi_fami_zoek.imfa_id IS NULL
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - In het gerelateerde importbestand met naam ' || r_imfi_fami_zoek.filenaam || ' is geen familie aanwezig!');
                           v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                           v_fami_new.familienummer := NULL; -- zeker niet overnemen in familienummer; wordt mogelijk verder gebruikt!
                        ELSIF r_imfi_fami_zoek.kgc_fami_id IS NULL
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - In het gerelateerde importbestand met naam ' || r_imfi_fami_zoek.filenaam || ' is de familie niet gekoppeld!');
                           v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                           v_fami_new.familienummer := NULL; -- zeker niet overnemen in familienummer; wordt mogelijk verder gebruikt!
                        ELSE
                           -- gerelateerde familie gevonden; deze net zo behandelen alsof dit familienummer opgegeven is
                           v_fami_new.familienummer := r_imfi_fami_zoek.familienummer;
                           v_fami_new.fami_id := r_imfi_fami_zoek.kgc_fami_id;
                        END IF;
                        CLOSE c_imfi_fami_zoek;
                     END IF;

                     IF v_fami_new.fami_id IS NULL
                        AND v_fami_new.familienummer IS NOT NULL
                     THEN -- zoek met familienummer
                        OPEN c_fami_zoek(p_familienummer => v_fami_new.familienummer);
                        FETCH c_fami_zoek INTO r_fami_zoek;
                        IF c_fami_zoek%NOTFOUND
                        THEN
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Het opgegeven familienummer ' || v_fami_new.familienummer || ' bestaat niet in Helix.');
                           v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                        ELSE -- familie bestaat
                           IF v_fale_count = 0
                           THEN
                              -- ok, koppelen aan bestaande familie
                              v_fami_new.fami_id := r_fami_zoek.fami_id;
                           ELSIF v_fale_count > 1
                           THEN
                              addtext(v_imfi_opmerkingen
                                     ,v_max_len
                                     ,v_opm_tab_en_id || ' - De opgegeven persoon zit in ' || v_fale_count || ' Helix-families (' || kgc_pers_00.families(v_pers_id_fami, 'K') || '); koppel handmatig!');
                              v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                           ELSE -- persoon heeft 1 familie; controleer opgegeven familie
                              OPEN c_fami_zoek2(p_pers_id => v_pers_id_fami);
                              FETCH c_fami_zoek2 INTO r_fami_zoek2;
                              CLOSE c_fami_zoek2;
                              IF v_fami_new.familienummer <> r_fami_zoek2.familienummer
                              THEN
                                 addtext(v_imfi_opmerkingen
                                        ,v_max_len
                                        ,v_opm_tab_en_id || ' - De opgegeven persoon zit in Helix-familie '|| r_fami_zoek2.familienummer ||
                                         ', en deze verschilt van de opgegeven familie (' || v_fami_new.familienummer || ')!');
                                 v_tab_imfa_err(r_imfa.imfa_id) := TRUE;
                              ELSE -- ok, gebruiken (eigenlijk niet nodig: de familie bestaat en de persoon zit ook al in de familie...)
                                 v_fami_new.fami_id := r_fami_zoek2.fami_id;
                              END IF;
                           END IF;
                        END IF;
                        CLOSE c_fami_zoek;
                     END IF;
                  END IF;
               END IF;

               -- evt nieuw bepaalde gegevens ophalen
               IF v_fami_new.fami_id IS NOT NULL
               THEN
                  -- sla op in globale tabel
                  set_im2hlx(p_im => 'IMFA' || r_imfa.imfa_id, p_hlx => 'FAMI' || v_fami_new.fami_id);

                  IF v_fami_old.the_rowid IS NULL
                  THEN
                     v_fami_old.fami_id := v_fami_new.fami_id;
                     cg$kgc_families.slct(v_fami_old);
                  END IF;
                  IF v_fami_new.the_rowid IS NULL
                  THEN
                     cg$kgc_families.slct(v_fami_new); -- oud is de basis voor nieuw
                     v_imfi_opmerkingen_enti := NULL;
                     v_error_enti := FALSE;
                     IF NOT im2fami( p_imfa => r_imfa
                                   , p_id => v_opm_tab_en_id
                                   , p_fami_old => v_fami_old
                                   , x_fami => v_fami_new
                                   , x_opmerkingen => v_imfi_opmerkingen_enti)
                     THEN
                        v_error_enti := TRUE;
                     END IF;
                  END IF;
               END IF;
               -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
               addtext(v_imfi_opmerkingen
                      ,v_max_len
                      ,v_imfi_opmerkingen_enti);
               v_tab_imfa_err(r_imfa.imfa_id) := (v_tab_imfa_err(r_imfa.imfa_id) OR v_error_enti);

               IF i_fase = v_c_updating -- evt insert/update uitvoeren?
               THEN
                  show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                  IF v_verwerkingstatus = 'E'
                     OR (v_verwerkingswijze = 'WM' AND v_tab_imfa_err(r_imfa.imfa_id))
                     OR (v_verwerkingswijze = 'AON' AND imxx_error)
                  THEN
                     NULL;
                  ELSE
                     IF v_fami_new.fami_id IS NULL
                     THEN -- toevoegen
                        show_output('Fami nieuw toevoegen (' || r_imfa.imfa_id || ')!');
                        -- en ook evt een nieuw familienummer
                        IF v_fami_new.familienummer IS NULL THEN
                           v_fami_new.familienummer := kgc_nust_00.genereer_nr(p_nummertype => 'FAMI'
                                                                              ,p_ongr_id    => NULL);
                        END IF;
                        -- tbv default-zetten:
                        v_fami_ind := cg$kgc_families.cg$ind_true;

                        cg$kgc_families.ins(cg$rec => v_fami_new
                                           ,cg$ind => v_fami_ind);
                        set_im2hlx(p_im => 'IMFA' || r_imfa.imfa_id, p_hlx => 'FAMI' || v_fami_new.fami_id);
                        addtext(v_imfi_verslag
                               ,v_max_len
                               ,'Nieuwe familie (' || v_fami_new.familienummer || ') toegevoegd.');
                        -- mantis 5715: copy notities
                        copy_notities(p_imfi_id, 'FAMI', v_fami_new.fami_id);
                     ELSE
                        show_output('Familie bestaat; niet bijwerken!');
                        v_fami_ind := v_fami_ind_false;
                        v_imfi_verslag_tmp := verschil_fami( p_o => v_fami_old -- tbv wijzigingsverslag
                                                           , p_n => v_fami_new
                                                           , p_i => v_fami_ind
                                                           );
                        IF v_imfi_verslag_tmp IS NULL
                        THEN
                           -- geen te wijzigen kolommen
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Familie ' || v_fami_new.familienummer || ' is ongewijzigd.');
                        ELSE
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Familie ' || v_fami_new.familienummer || ' wordt NIET gewijzigd; aanwezige wijzigingen: ' || v_imfi_verslag_tmp);
                        END IF;
                     END IF;

                     IF r_imfa.kgc_fami_id IS NULL
                     THEN
                        -- fami_id aanbrengen in interface
                        show_output('Familie-id in interface bijwerken!');
                        UPDATE kgc_im_families imfa
                           SET imfa.kgc_fami_id = v_fami_new.fami_id
                         WHERE imfa.imfi_id = p_imfi_id
                           AND imfa.imfa_id = r_imfa.imfa_id;
                     END IF;
                     -- koppeling familie - persoon leggen
                     SELECT COUNT(*)
                       INTO v_count
                       FROM kgc_familie_leden fale
                      WHERE fale.pers_id = v_pers_id_fami
                        AND fale.fami_id = v_fami_new.fami_id;
                     IF v_count = 0
                     THEN
                        INSERT INTO kgc_familie_leden (fami_id, pers_id)
                        VALUES (v_fami_new.fami_id, v_pers_id_fami);
                        addtext(v_imfi_verslag
                               ,v_max_len
                               ,'Persoon toegevoegd in familie ' || v_fami_new.familienummer || '.');
                     END IF;
                  END IF;
               END IF;
               -- einde familie
               -- ****** controleer en vertaal gegevens FAMILIEONDERZOEK bij FAMILIE
               FOR r_imfz IN c_imfz(p_imfi_id
                                   ,r_imfa.imfa_id)
               LOOP
                  v_faon_old := v_faon_null;
                  v_faon_new := v_faon_null;
                  v_tab_imfz_err(r_imfz.imfz_id) := FALSE;
                  v_opm_tab_en_id := 'FAMILIEONDERZOEK(' || r_imfz.imfz_id || ')';
                  show_output('Check ' || v_opm_tab_en_id || '; fase ' || i_fase || '(kgc_faon_id=' || r_imfz.kgc_faon_id || ')');

                  -- controleer koppeling
                  v_faon_old.faon_id := NVL(r_imfz.kgc_faon_id, TO_NUMBER(SUBSTR(get_im2hlx('IMFZ' || r_imfz.imfz_id), 5)));
                  IF v_faon_old.faon_id IS NOT NULL -- is deze al eerder geweest?
                  THEN
                     BEGIN
                        cg$kgc_familie_onderzoeken.slct(v_faon_old);
                        v_faon_new.faon_id := v_faon_old.faon_id;
                        cg$kgc_familie_onderzoeken.slct(v_faon_new);
                        -- oud is de basis voor nieuw
                     EXCEPTION
                        WHEN OTHERS THEN -- no data found
                           cg$errors.clear;
                           addtext(v_imfi_opmerkingen
                                  ,v_max_len
                                  ,v_opm_tab_en_id || ' - Helix-id (' || to_char(v_faon_old.faon_id) ||
                                   ') verwijst niet naar een bestaand familieonderzoek. Maak het Helix-id leeg (familieonderzoek wordt gekoppeld/nieuw toegevoegd bij volgende verwerking) of verwijder de familieonderzoeksgegevens.');
                           v_verwerkingstatus := 'E';
                           v_tab_imfz_err(r_imfz.imfz_id) := TRUE;
                           EXIT; -- klaar met controleren
                     END;
                  END IF;

                  -- ****** controleer en vertaal gegevens (ook nodig voor evt koppeling)
                  v_imfi_opmerkingen_enti := NULL;
                  v_error_enti := FALSE;
                  IF NOT im2faon( p_imfz => r_imfz
                                , p_id => v_opm_tab_en_id
                                , p_faon_old => v_faon_old
                                , x_faon => v_faon_new
                                , x_opmerkingen => v_imfi_opmerkingen_enti)
                  THEN
                     v_error_enti := TRUE;
                  END IF;

                  -- mogelijk moet een familieonderzoek gekoppeld worden aan een bestaande
                  -- zie sypa FAMILIEONDERZOEK
                  -- sypa FAMILIEONDERZOEK = ALGEMEEN, dan koppel op familie, ongr en indicatie
                  -- sypa FAMILIEONDERZOEK = SPECIFIEK, dan koppel op familie, ongr, datum, aanvrager
                  IF v_faon_new.faon_id IS NULL
                  THEN
                     v_sypa_waarde := upper( kgc_sypa_00.standaard_waarde
                                             ( p_parameter_code => 'FAMILIEONDERZOEK'
                                             , p_kafd_id => v_faon_new.kafd_id
                                             , p_ongr_id => v_faon_new.ongr_id
                                             )
                                           );
                     IF v_sypa_waarde = 'ALGEMEEN'
                     THEN
                        OPEN c_faon_zoek_alg(p_fami_id => v_faon_new.fami_id
                                            ,p_ongr_id => v_faon_new.ongr_id
                                            ,p_indi_id => v_faon_new.indi_id);
                        FETCH c_faon_zoek_alg
                           INTO v_faon_new.faon_id;
                        IF c_faon_zoek_alg%NOTFOUND
                        THEN
                           v_faon_new.faon_id := NULL;
                        END IF;
                        CLOSE c_faon_zoek_alg;
                     ELSIF v_sypa_waarde = 'SPECIFIEK'
                     THEN
                        OPEN c_faon_zoek_spec(p_fami_id => v_faon_new.fami_id
                                             ,p_ongr_id => v_faon_new.ongr_id
                                             ,p_indi_id => v_faon_new.indi_id
                                             ,p_datum   => v_faon_new.datum
                                             ,p_rela_id => v_faon_new.rela_id);
                        FETCH c_faon_zoek_spec
                           INTO v_faon_new.faon_id;
                        IF c_faon_zoek_spec%NOTFOUND
                        THEN
                           v_faon_new.faon_id := NULL;
                        END IF;
                        CLOSE c_faon_zoek_spec;
                     END IF;
                  END IF;

                  -- evt nieuw bepaalde gegevens ophalen
                  IF v_faon_new.faon_id IS NOT NULL
                  THEN
                     -- sla op in globale tabel
                     set_im2hlx(p_im => 'IMFZ' || r_imfz.imfz_id, p_hlx => 'FAMI' || v_faon_new.faon_id);

                     IF v_faon_old.the_rowid IS NULL
                     THEN
                        v_faon_old.faon_id := v_faon_new.faon_id;
                        cg$kgc_familie_onderzoeken.slct(v_faon_old);
                     END IF;
                     IF v_faon_new.the_rowid IS NULL
                     THEN
                        cg$kgc_familie_onderzoeken.slct(v_faon_new); -- oud is de basis voor nieuw
                        v_imfi_opmerkingen_enti := NULL;
                        v_error_enti := FALSE;
                        IF NOT im2faon( p_imfz => r_imfz
                                      , p_id => v_opm_tab_en_id
                                      , p_faon_old => v_faon_old
                                      , x_faon => v_faon_new
                                      , x_opmerkingen => v_imfi_opmerkingen_enti)
                        THEN
                           v_error_enti := TRUE;
                        END IF;
                     END IF;
                  END IF;
                  -- nu is v_imfi_opmerkingen_enti bepaald. Toevoegen aan v_imfi_opmerkingen
                  addtext(v_imfi_opmerkingen
                         ,v_max_len
                         ,v_imfi_opmerkingen_enti);
                  v_tab_imfz_err(r_imfz.imfz_id) := (v_tab_imfz_err(r_imfz.imfz_id) OR v_error_enti);

                  -- controleer samenhang
                  IF v_faon_new.faon_id IS NOT NULL AND v_faon_old.familieonderzoeknr != v_faon_new.familieonderzoeknr
                  THEN
                     addtext(v_imfi_opmerkingen
                            ,v_max_len
                            ,v_opm_tab_en_id || ' - familieonderzoeknr mag niet gewijzigd worden (van ' || v_faon_old.familieonderzoeknr || ' naar ' || v_faon_new.familieonderzoeknr || ')');
                     v_tab_imfz_err(r_imfz.imfz_id) := TRUE;
                  END IF;

                  IF i_fase = v_c_updating -- evt insert/update uitvoeren?
                  THEN
                     show_output('Updating ' || v_opm_tab_en_id || '; fase ' || i_fase || '(v_verwerkingstatus=' || v_verwerkingstatus || ')');
                     IF v_verwerkingstatus = 'E'
                        OR (v_verwerkingswijze = 'WM' AND v_tab_imfa_err(r_imfz.imfa_id)) -- de familie geeft een fout
                        OR (v_verwerkingswijze = 'WM' AND v_tab_imfz_err(r_imfz.imfz_id))
                        OR (v_verwerkingswijze = 'AON' AND imxx_error)
                     THEN
                        NULL;
                     ELSE
                        IF v_faon_new.faon_id IS NULL
                        THEN -- toevoegen
                           show_output('Faon nieuw toevoegen (' || r_imfz.imfz_id || ')!');
                           IF v_faon_new.familieonderzoeknr IS NULL
                           THEN
                              v_faon_new.familieonderzoeknr := kgc_nust_00.genereer_nr(p_nummertype => 'FAON'
                                                                                      ,p_ongr_id    => v_faon_new.ongr_id);
                           END IF;

                           -- tbv default-zetten:
                           v_faon_ind := cg$kgc_familie_onderzoeken.cg$ind_true;
                           IF v_faon_new.datum IS NULL  THEN v_faon_ind.datum := FALSE; END IF;
                           IF v_faon_new.onderzoekstype IS NULL  THEN v_faon_ind.onderzoekstype := FALSE; END IF;
                           IF v_faon_new.afgerond IS NULL  THEN v_faon_ind.afgerond := FALSE; END IF;
                           IF v_faon_new.spoed IS NULL  THEN v_faon_ind.spoed := FALSE; END IF;
                           IF v_faon_new.onderzoekswijze IS NULL  THEN v_faon_ind.onderzoekswijze := FALSE; END IF;

                           cg$kgc_familie_onderzoeken.ins(cg$rec => v_faon_new
                                                         ,cg$ind => v_faon_ind);
                           set_im2hlx(p_im => 'IMFZ' || r_imfz.imfz_id, p_hlx => 'FAON' || v_faon_new.faon_id);
                           addtext(v_imfi_verslag
                                  ,v_max_len
                                  ,'Nieuw familieonderzoek (' || v_faon_new.familieonderzoeknr || ') toegevoegd.');
                           -- mantis 5715: copy notities
                           copy_notities(p_imfi_id, 'FAON', v_faon_new.faon_id);
                        ELSE
                           v_faon_ind := v_faon_ind_false;
                           v_imfi_verslag_tmp := verschil_faon( p_o => v_faon_old -- tbv wijzigingsverslag
                                                              , p_n => v_faon_new
                                                              , p_i => v_faon_ind
                                                              );
                           IF v_imfi_verslag_tmp IS NULL
                           THEN
                              -- geen te wijzigen kolommen
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Familieonderzoek ' || v_faon_new.familieonderzoeknr || ' is ongewijzigd.');
                           ELSE
                              addtext(v_imfi_verslag
                                     ,v_max_len
                                     ,'Familieonderzoek ' || v_faon_new.familieonderzoeknr || ' wordt NIET gewijzigd; aanwezige wijzigingen: ' || v_imfi_verslag_tmp);
                           END IF;
                        END IF;

                        IF r_imfz.kgc_faon_id IS NULL
                        THEN
                           -- faon_id aanbrengen in interface
                           show_output('Familieonderzoeks-id in interface bijwerken!');
                           UPDATE kgc_im_familie_onderzoeken imfz
                              SET imfz.kgc_faon_id = v_faon_new.faon_id
                            WHERE imfz.imfi_id = p_imfi_id
                              AND imfz.imfz_id = r_imfz.imfz_id;
                        END IF;
                        -- koppeling familieonderzoek - persoon - onderzoek leggen
                        koppel_faon_pers_onde(p_imfi_id => p_imfi_id
                                             ,p_pers_id => v_pers_id_fami
                                             ,p_faon_id => v_faon_new.faon_id
                                             ,x_verslag => v_imfi_verslag);

                     END IF;
                  END IF;

               END LOOP; -- c_imfz
            END LOOP; -- c_imfa


            IF i_fase = v_c_updating -- ******** eventueel bestanden verplaatsen
            THEN
               show_output('Bestanden verplaatsen (v_verwerkingstatus=' || v_verwerkingstatus || ')');
               IF v_verwerkingstatus = 'E'
                  OR (v_verwerkingswijze = 'WM' AND imxx_error('#ONDE#MONS#UITS#'))
                  OR (v_verwerkingswijze = 'AON' AND imxx_error)
               THEN
                  NULL; -- geen bestand-actie
               ELSE -- bestanden verplaatsen mag alleen indien alle onderzoeken/monsters goed zijn verwerkt
                  -- zijn er bestanden te verplaatsen?
                  SAVEPOINT save_best;
                  DECLARE
                     CURSOR c_best (p_imfi_id NUMBER) IS
                        SELECT best.*
                          FROM kgc_bestanden best
                             , kgc_bestand_types btyp
                         WHERE best.entiteit_code = 'IMFI'
                           AND best.entiteit_pk = p_imfi_id
                           AND best.btyp_id = btyp.btyp_id
                           AND btyp.code = 'IMFI_DOC' -- mantis 5715
                        ;
                     r_best c_best%ROWTYPE;
                     CURSOR c_imon_imfi (p_imfi_id NUMBER) IS
                        SELECT kgc_onde_id
                          FROM kgc_im_onderzoeken imon
                         WHERE imon.imfi_id = p_imfi_id
                           AND kgc_onde_id IS NOT NULL -- igv geen fouten, dan altijd gevuld, dus eigenlijk overbodig
                        ;
                     CURSOR c_immo_imfi (p_imfi_id NUMBER) IS
                        SELECT kgc_mons_id
                          FROM kgc_im_monsters immo
                         WHERE immo.imfi_id = p_imfi_id
                           AND kgc_mons_id IS NOT NULL -- igv geen fouten, dan altijd gevuld, dus eigenlijk overbodig
                        ;
                     CURSOR c_btyp (p_code VARCHAR2) IS
                        SELECT *
                          FROM kgc_bestand_types btyp
                         WHERE code = p_code
                        ;
                     CURSOR c_btyp_via_id (p_btyp_id NUMBER) IS
                        SELECT *
                          FROM kgc_bestand_types btyp
                         WHERE btyp_id = p_btyp_id
                        ;
                     r_btyp c_btyp%ROWTYPE;
                     CURSOR c_im_best(p_imfi_id NUMBER) IS
                        SELECT 'ONDE' entiteit_code
                             , imon.kgc_onde_id entiteit_pk
                             , 'IMON' entiteit_code_from
                             , imon.imfi_id || '_' || imon.imon_id entiteit_pk_from
                             , imon.bestandsnaam
                             , imon.impw_id_btyp
                          FROM kgc_im_onderzoeken imon
                         WHERE imon.imfi_id = p_imfi_id
                           AND imon.bestandsnaam IS NOT NULL
                        UNION ALL
                        SELECT 'UITS' entiteit_code
                             , imui.kgc_uits_id entiteit_pk
                             , 'IMUI' entiteit_code_from
                             , imui.imfi_id || '_' || imui.imui_id entiteit_pk_from
                             , imui.bestandsnaam
                             , imui.impw_id_btyp
                          FROM kgc_im_uitslagen imui
                         WHERE imui.imfi_id = p_imfi_id
                           AND imui.bestandsnaam IS NOT NULL
                        UNION ALL
                        SELECT 'MONS' entiteit_code
                             , immo.kgc_mons_id entiteit_pk
                             , 'IMMO' entiteit_code_from
                             , immo.imfi_id || '_' || immo.immo_id entiteit_pk_from
                             , immo.bestandsnaam
                             , immo.impw_id_btyp
                          FROM kgc_im_monsters immo
                         WHERE immo.imfi_id = p_imfi_id
                           AND immo.bestandsnaam IS NOT NULL
                        ;
                     v_best_trow      cg$kgc_bestanden.cg$row_type;
                     v_best_trow_null cg$kgc_bestanden.cg$row_type;
                     v_ok             VARCHAR2(4000);
                     v_btyp_id        NUMBER;
                     v_bestand_specificatie kgc_bestanden.bestand_specificatie%TYPE;
                     v_doStap2        BOOLEAN := TRUE; -- het verplaatsen van de IMFI_DOC naar MONS/ONDE_DOC
                  BEGIN
                     -- zijn er onderzoek / monster / uitslag-bestanden te veplaatsen?
                     -- dit moet alleen indien bij de doel-entiteit er nog geen btyp aanwezig is! (ivm update/koppel: niet nog een keer kopieren)
                     FOR r_im_best IN c_im_best(p_imfi_id => p_imfi_id)
                     LOOP
                        v_doStap2 := FALSE; -- want: max 1 bestand per rij in kgc_inport_files
                        v_db_waarde := NULL;
                        v_imfi_verslag_tmp := NULL;
                        v_bdummie := refimpwok(r_im_best.impw_id_btyp
                                              ,'Bestandtype'
                                              ,TRUE
                                              ,v_db_waarde
                                              ,v_imfi_verslag_tmp);
                        -- is al gecontroleerd
                        v_btyp_id := TO_NUMBER(v_db_waarde);
                        r_btyp := NULL;
                        OPEN c_btyp_via_id(p_btyp_id => v_btyp_id);
                        FETCH c_btyp_via_id INTO r_btyp;
                        CLOSE c_btyp_via_id;
                        SELECT COUNT(*)
                          INTO v_count
                          FROM kgc_bestanden best
                         WHERE best.entiteit_code = r_im_best.entiteit_code
                           AND best.entiteit_pk = r_im_best.entiteit_pk
                           AND best.btyp_id = v_btyp_id;
                        IF v_count = 0
                        THEN
                           OPEN c_best(p_imfi_id => p_imfi_id);
                           FETCH c_best INTO r_best;
                           CLOSE c_best;
                           v_best_trow := v_best_trow_null;
                           v_best_trow.best_id := r_best.best_id;
                           cg$kgc_bestanden.slct(v_best_trow);
                           -- verplaats van r_imfi.directorynaam naar r_btyp.standaard_pad
                           -- en verhang de bestaande bestandsregistratie
                           v_bestand_specificatie := v_best_trow.bestand_specificatie;
                           v_best_trow.btyp_id := v_btyp_id;
                           v_best_trow.locatie := RTRIM(r_btyp.standaard_pad, '\') || '\';
                           v_best_trow.bestandsnaam := r_im_best.entiteit_code_from || r_im_best.entiteit_pk_from || '_' || TO_CHAR(SYSDATE, 'yyyymmdd_hh24miss') || SUBSTR(r_im_best.bestandsnaam, INSTR(r_im_best.bestandsnaam, '.', -1));
                           v_best_trow.bestand_specificatie := v_best_trow.locatie || v_best_trow.bestandsnaam;
                           -- copy naar target
                           v_ok := kgc_interface_generiek.copy_bestand(p_file_van  => v_bestand_specificatie
                                                                      ,p_file_naar => v_best_trow.bestand_specificatie);
                           IF v_ok != 'OK'
                           THEN
                              raise_application_error(-20000
                                                     ,v_ok);
                           END IF;
                           -- registratie
                           v_best_trow.entiteit_code := r_im_best.entiteit_code;
                           v_best_trow.entiteit_pk := r_im_best.entiteit_pk;
                           cg$kgc_bestanden.upd(cg$rec => v_best_trow
                                               ,cg$ind => cg$kgc_bestanden.cg$ind_true);
                           -- delete source bestand
                           -- effe wachten op vrijgeven file. Zucht.
                           FOR i_del IN 1 .. 5 LOOP
                              BEGIN
                                 v_ok := kgc_interface_generiek.delete_bestand(v_bestand_specificatie);
                                 IF v_ok = 'OK' THEN
                                    EXIT;
                                 END IF;
                                 IF i_del < 5 THEN
                                    dbms_lock.sleep(1 * i_del); -- seconden!
                                 ELSE
                                    raise_application_error(-20000
                                                           ,v_ok);
                                 END IF;
                              END;
                           END LOOP;
                        END IF;
                     END LOOP;

                     IF v_doStap2 THEN
                        FOR r_best IN c_best(p_imfi_id => p_imfi_id)
                        LOOP
                           v_best_trow := v_best_trow_null;
                           v_best_trow.best_id := r_best.best_id;
                           cg$kgc_bestanden.slct(v_best_trow);
                           -- zoek target tbv onde
                           OPEN c_btyp(p_code => 'IMFI_ONDE');
                           FETCH c_btyp into r_btyp;
                           IF c_btyp%NOTFOUND
                           THEN
                              raise_application_error(-20000
                                                     ,'Het bestandstype ''IMFI_ONDE'' werd niet gevonden!');
                           END IF;
                           CLOSE c_btyp;

                           -- registratie aanmaken voor alle onderzoeken
                           v_count := 0;
                           FOR r_imon_imfi IN c_imon_imfi(p_imfi_id => p_imfi_id)
                           LOOP
                              v_count := v_count + 1;
                              v_best_trow.the_rowid := NULL;
                              v_best_trow.best_id := NULL;
                              v_best_trow.btyp_id := r_btyp.btyp_id; -- mantis 11620
                              v_best_trow.entiteit_code := 'ONDE';
                              v_best_trow.entiteit_pk := r_imon_imfi.kgc_onde_id;
                              v_best_trow.locatie := RTRIM(r_btyp.standaard_pad, '\') || '\';
                              v_best_trow.bestand_specificatie := v_best_trow.locatie || v_best_trow.bestandsnaam;
                              -- 2017-05-05: eerst controleren of deze al bestaat. Dubbele registratie is zinloos
                              SELECT COUNT(*)
                                INTO v_count_best
                                FROM kgc_bestanden best
                               WHERE best.btyp_id = v_best_trow.btyp_id
                                 AND best.entiteit_code = v_best_trow.entiteit_code
                                 AND best.entiteit_pk = v_best_trow.entiteit_pk
                                 AND best.bestand_specificatie = v_best_trow.bestand_specificatie;
                              IF v_count_best = 0 THEN
                                 cg$kgc_bestanden.ins(cg$rec => v_best_trow
                                                     ,cg$ind => cg$kgc_bestanden.cg$ind_true);
                              END IF;
                           END LOOP;
                           -- indien geen onderzoeken aanwezig, dan naar monsters:
                           IF v_count = 0
                           THEN
                              -- zoek target tbv mons
                              OPEN c_btyp(p_code => 'IMFI_MONS');
                              FETCH c_btyp into r_btyp;
                              IF c_btyp%NOTFOUND
                              THEN
                                 raise_application_error(-20000
                                                        ,'Het bestandstype ''IMFI_MONS'' werd niet gevonden!');
                              END IF;
                              CLOSE c_btyp;
                              FOR r_immo_imfi IN c_immo_imfi(p_imfi_id => p_imfi_id)
                              LOOP
                                 v_count := v_count + 1;
                                 v_best_trow.the_rowid := NULL;
                                 v_best_trow.best_id := NULL;
                                 v_best_trow.btyp_id := r_btyp.btyp_id; -- mantis 11620
                                 v_best_trow.entiteit_code := 'MONS';
                                 v_best_trow.entiteit_pk := r_immo_imfi.kgc_mons_id;
                                 v_best_trow.locatie := RTRIM(r_btyp.standaard_pad, '\') || '\';
                                 v_best_trow.bestand_specificatie := v_best_trow.locatie || v_best_trow.bestandsnaam;
                                 -- 2017-05-05: eerst controleren of deze al bestaat. Dubbele registratie is zinloos
                                 SELECT COUNT(*)
                                   INTO v_count_best
                                   FROM kgc_bestanden best
                                  WHERE best.btyp_id = v_best_trow.btyp_id
                                    AND best.entiteit_code = v_best_trow.entiteit_code
                                    AND best.entiteit_pk = v_best_trow.entiteit_pk
                                    AND best.bestand_specificatie = v_best_trow.bestand_specificatie;
                                 IF v_count_best = 0 THEN
                                    cg$kgc_bestanden.ins(cg$rec => v_best_trow
                                                     ,cg$ind => cg$kgc_bestanden.cg$ind_true);
                                 END IF;
                              END LOOP;
                           END IF;
                           IF v_count > 0
                           THEN
                             -- copy naar target
                             -- 2017-05-05: in Nijmegen kunnen er meerdere imfis zijn voor hetzelfde document. Dan mag dit dus niet verwijderd worden,
                             -- want bij de verwerking van de 2e imfi is het bestand ook nodig.
                             -- truuk: maak de bron- en doel-map gelijk, zodat het bestand niet verplaatst hoeft te worden!
                             -- dit ivm imfi-gegevens dia NA verwerking nog aangevuld worden met een extra onderzoek of monster
                             IF UPPER(TRIM(r_best.bestand_specificatie)) = UPPER(TRIM(v_best_trow.bestand_specificatie)) THEN
                                v_best_verplaatsen := FALSE;
                             ELSE
                                v_best_verplaatsen := TRUE;
                             END IF;
                             IF v_best_verplaatsen THEN
                                v_ok := kgc_interface_generiek.copy_bestand(p_file_van  => r_best.bestand_specificatie
                                                                           ,p_file_naar => v_best_trow.bestand_specificatie);
                                IF v_ok != 'OK'
                                THEN
                                   raise_application_error(-20000
                                                          ,v_ok);
                                END IF;
                             END IF;
                             -- delete r_best
                             DELETE FROM kgc_bestanden best
                             WHERE best.best_id = r_best.best_id;
                             -- delete source bestand?
                             IF v_best_verplaatsen THEN
                                -- effe wachten op vrijgeven file. Zucht. rde 2017-04-04
                                FOR i_del IN 1 .. 5 LOOP
                                   BEGIN
                                      v_ok := kgc_interface_generiek.delete_bestand(r_best.bestand_specificatie);
                                      IF v_ok = 'OK' THEN
                                         EXIT;
                                      END IF;
                                      IF i_del < 5 THEN
                                         dbms_lock.sleep(1 * i_del); -- seconden!
                                      ELSE
                                         raise_application_error(-20000
                                                                ,v_ok);
                                      END IF;
                                   END;
                                END LOOP;
                              END IF;
                           END IF;
                        END LOOP; -- c_best
                     END IF;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ROLLBACK TO SAVEPOINT save_best;
                        kgc_import.addtext(v_imfi_opmerkingen
                                          ,v_max_len
                                          ,'Fout in Helix: ' || SQLERRM);
                        v_verwerkingstatus := 'E';
                  END;
               END IF;
            END IF;

            -- alleen specifieke verwerking indien alles OK
            IF v_verwerkingstatus NOT IN ('E', 'K') AND (NOT imxx_error) AND r_imfi.specifieke_verwerking IS NOT NULL
            THEN
               CASE kgc_sypa_00.systeem_waarde('ZIS_LOCATIE')
               WHEN 'UTRECHT' THEN
                  v_verwerkingstatus := kgc_import_utrecht.specifieke_verwerking(p_imfi => v_imfi
                                                                                ,p_plekvanaanroep => 'NA_CHECK'
                                                                                ,x_imfi_verslag => v_imfi_verslag
                                                                                );
               WHEN 'NIJMEGEN' THEN
                  v_verwerkingstatus := kgc_import_nijmegen.specifieke_verwerking(p_imfi => v_imfi
                                                                                 ,p_plekvanaanroep => 'NA_CHECK'
                                                                                 ,x_imfi_verslag => v_imfi_verslag
                                                                                 );
               WHEN 'MAASTRICHT' THEN
                  v_verwerkingstatus := kgc_import_maastricht.specifieke_verwerking(p_imfi => v_imfi
                                                                                   ,p_plekvanaanroep => 'NA_CHECK'
                                                                                   ,x_imfi_verslag => v_imfi_verslag
                                                                                   );
               ELSE
                  v_verwerkingstatus := 'D'; -- Doorgaan
               END CASE;
               IF v_verwerkingstatus != 'D'
               THEN
                  EXIT;
               END IF;
            END IF;

         END LOOP; -- i_fase
      EXCEPTION
         WHEN OTHERS THEN
            g_standaard_logica := 'J';
            addtext(v_imfi_opmerkingen
                   ,v_max_len
                   ,'Error ' || SQLERRM);
            v_verwerkingstatus := 'E';
      END;

      -- alleen specifieke verwerking indien alles OK en nog niet klaar
      IF v_verwerkingstatus NOT IN ('E', 'K') AND (NOT imxx_error) AND r_imfi.specifieke_verwerking IS NOT NULL
      THEN
         CASE kgc_sypa_00.systeem_waarde('ZIS_LOCATIE')
         WHEN 'UTRECHT' THEN
            v_verwerkingstatus := kgc_import_utrecht.specifieke_verwerking(p_imfi => v_imfi
                                                                          ,p_plekvanaanroep => 'NA_VERW'
                                                                          ,x_imfi_verslag => v_imfi_verslag
                                                                          );
         WHEN 'NIJMEGEN' THEN
            v_verwerkingstatus := kgc_import_nijmegen.specifieke_verwerking(p_imfi => v_imfi
                                                                           ,p_plekvanaanroep => 'NA_VERW'
                                                                           ,x_imfi_verslag => v_imfi_verslag
                                                                           );
         WHEN 'MAASTRICHT' THEN
            v_verwerkingstatus := kgc_import_maastricht.specifieke_verwerking(p_imfi => v_imfi
                                                                             ,p_plekvanaanroep => 'NA_VERW'
                                                                             ,x_imfi_verslag => v_imfi_verslag
                                                                             );
         ELSE
            v_verwerkingstatus := 'D'; -- Doorgaan
         END CASE;
      END IF;

-- testing:
--ROLLBACK;
--kgc_import.addtext(v_imfi_opmerkingen
--                  ,v_max_len
--                  ,'Wijzigingen zijn NIET doorgevoerd tbv testen!');
-- einde testing

      -- afronding: bijwerken opmerkingen/afgerond
      IF v_verwerkingstatus != 'E'
         AND v_impe_count = 0
      THEN
         -- geen gegevens ingelezen? geen actie!
         -- mededeling toevoegen indien nodig:
         v_imfi_verslag_tmp := '; Geen verwerking - geen persoon gevonden!';
         UPDATE kgc_import_files imfi
            SET opmerkingen = substr(opmerkingen || v_imfi_verslag_tmp
                                    ,1
                                    ,v_max_len)
          WHERE imfi.imfi_id = p_imfi_id
            AND imfi.opmerkingen NOT LIKE '%' || v_imfi_verslag_tmp;

      ELSE
         -- aanroep lokale aanvulling, bv voor utrecht tbv hpo; rde 18-06-2014
         IF v_verwerkingstatus != 'E'
            AND NOT imxx_error
            AND v_imfi_opmerkingen IS NULL
         THEN
            CASE kgc_sypa_00.systeem_waarde('ZIS_LOCATIE')
            WHEN 'UTRECHT' THEN
               kgc_import_utrecht.vertaal_naar_helix(p_imfi_id => p_imfi_id, x_imfi_verslag => v_imfi_verslag);
            WHEN 'NIJMEGEN' THEN
               kgc_import_nijmegen.vertaal_naar_helix(p_imfi_id => p_imfi_id, x_imfi_verslag => v_imfi_verslag);
            WHEN 'MAASTRICHT' THEN
               kgc_import_maastricht.vertaal_naar_helix(p_imfi_id => p_imfi_id, x_imfi_verslag => v_imfi_verslag);
            END CASE;
         END IF;
         -- einde
         IF nvl(length(v_imfi_verslag)
               ,0) > 0
         THEN
            -- 'header' toevoegen
            v_imfi_verslag := 'Start verwerking ' || to_char(SYSDATE
                                                            ,'DD-MM-YYYY HH24:MI:SS') || '; Wijzigingen (was->wordt).' ||
                              chr(10) || v_imfi_verslag;
         ELSIF v_verwerkingstatus != 'E'
         THEN
            -- aangeven dat er niets gewijzigd is
            v_imfi_verslag := 'Er zijn geen Helix-gegevens gewijzigd!';
         END IF;
         IF v_verwerkingstatus != 'E'
            AND NOT imxx_error
            AND v_imfi_opmerkingen IS NULL
         THEN
            -- geen fout; alles goedgegaan
            v_imfi_afgehandeld := 'J';
            v_mons_koppel := FALSE;
            SELECT COUNT(*)
              INTO v_count
              FROM kgc_im_monsters immo
             WHERE immo.monsternummer = '*KOPPEL*'
               AND immo.imfi_id = p_imfi_id;
            IF v_count > 0 THEN
               v_mons_koppel := TRUE;
            END IF;
            IF v_mons_koppel AND (NOT p_online)
            THEN
               v_imfi_afgehandeld := 'N'; -- niet op afgehandeld zetten indien monster met *KOPPEL* is aangeleverd in Batch
            ELSIF v_fracties_betrekken = 'J'
            THEN
               v_imfi_afgehandeld := 'N'; -- niet op afgehandeld zetten indien er nog fracties betrokken moeten worden
            END IF;
            UPDATE kgc_import_files imfi
               SET afgehandeld = v_imfi_afgehandeld
                  ,opmerkingen = substr(v_imfi_verslag -- rde 19-12-2013 beperking op lengte toegevoegd
                                       ,1
                                       ,v_max_len)
             WHERE imfi.imfi_id = p_imfi_id;
         ELSE
            IF v_verwerkingstatus = 'E'
            THEN
               -- eerst rollback, dan registratie opmerkingen
               ROLLBACK;
               addtext(v_imfi_opmerkingen
                      ,v_max_len
                      ,'Er zijn geen Helix-gegevens gewijzigd!');
            END IF;
            -- en vervolgens de opmerkingen / verslag opslaan:
            IF nvl(length(v_imfi_verslag)
                  ,0) > 0
            THEN
               -- toevoegen aan de opmerkingen
               addtext(v_imfi_opmerkingen
                      ,v_max_len
                      ,v_imfi_verslag);
            END IF;
            UPDATE kgc_import_files imfi
               SET opmerkingen = substr(v_imfi_opmerkingen
                                       ,1
                                       ,v_max_len)
             WHERE imfi.imfi_id = p_imfi_id;
            show_output('Opmerkingen: ');
            show_output(v_imfi_opmerkingen);
         END IF;
      END IF;
      IF v_verwerkingstatus = 'E'
      THEN
         show_output('Error is TRUE');
      END IF;
      g_standaard_logica := 'J';
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         show_output('FOUT:' || SQLERRM);
         ROLLBACK;
         raise_application_error(-20000
                                ,substr(SQLERRM || ' ' || v_imfi_opmerkingen
                                       ,1
                                       ,255));
   END vertaal_naar_helix;

   --
   /**************************************************************************************
   ** NtB, 28-01-2009:
   ** Programmatuur is verplaatst naar kgc_interface_astraia.
   ** Om aanroepen vanuit andere programmatuur intact te laten blijven volgende
   ** links bestaan.
   **************************************************************************************/
   PROCEDURE lees_in_astraia(p_imlo_id  IN NUMBER
                            ,p_filenaam IN VARCHAR2) IS
   BEGIN
      kgc_interface_astraia.lees_in_astraia(p_imlo_id  => p_imlo_id
                                           ,p_filenaam => p_filenaam);
   END lees_in_astraia;

   PROCEDURE verwerk_bestanden IS
   BEGIN
      kgc_interface_astraia.verwerk_bestanden;
   END verwerk_bestanden;
   /*************************************************************************************/

   FUNCTION tonum(p_vc VARCHAR2) RETURN NUMBER IS
      v_return NUMBER;
   BEGIN
      IF p_vc IS NULL
      THEN
         v_return  := NULL;
      ELSE
         BEGIN
            v_return := to_number(p_vc);
         EXCEPTION WHEN OTHERS
         THEN
            v_return := NULL;
         END;
      END IF;
      RETURN(v_return);
   END tonum;

   FUNCTION verplichtok(v_waarde       IN VARCHAR2
                       ,v_kolom        IN VARCHAR2
                       ,v_verplicht    IN BOOLEAN
                       ,x_errormessage IN OUT VARCHAR2) RETURN BOOLEAN IS
      --      v_Pos1   VARCHAR2(1);
      --      v_Datum  DATE;
      --      v_Nummer NUMBER;
   BEGIN
      IF v_verplicht
      THEN
         IF v_waarde IS NULL
         THEN
            addtext(x_errormessage
                   ,v_max_len
                   ,'Kolom ' || v_kolom || ' moet gevuld zijn!');
            RETURN FALSE;
         END IF;
      END IF;
      RETURN TRUE;
   END verplichtok;

   FUNCTION typeok(v_waarde       IN VARCHAR2
                  ,v_kolom        IN VARCHAR2
                  ,v_type         IN VARCHAR2
                  ,v_maxlengte    IN NUMBER
                  ,v_decimalen    IN NUMBER
                  ,v_verplicht    IN BOOLEAN
                  ,x_waarde       OUT VARCHAR2
                  ,x_errormessage IN OUT VARCHAR2) RETURN BOOLEAN IS
      v_pos1   VARCHAR2(1);
      v_datum  DATE;
      v_nummer NUMBER;
   BEGIN
      x_waarde := NULL;
      IF v_waarde IS NOT NULL
      THEN
         IF v_type = 'J'
         THEN
            -- Ja nee
            v_pos1 := upper(substr(v_waarde
                                  ,1
                                  ,1));
            IF v_pos1 IN ('J', 'Y')
            THEN
               x_waarde := 'J';
            ELSIF v_pos1 IN ('N')
            THEN
               x_waarde := 'N';
            ELSE
               addtext(x_errormessage
                      ,v_max_len
                      ,'Kolom ' || v_kolom ||
                       ' moet J (Ja) of N (Nee) bevatten, of leeg zijn; de foute waarde is ''' || v_waarde || '''!');
               RETURN FALSE;
            END IF;
         ELSIF v_type = 'N'
         THEN
            -- Numeriek
            BEGIN
               v_nummer := to_number(v_waarde);
            EXCEPTION
               WHEN OTHERS THEN
                  addtext(x_errormessage
                         ,v_max_len
                         ,'Kolom ' || v_kolom || ' is niet numeriek; de foute waarde is ''' || v_waarde || '''!');
                  RETURN FALSE;
            END;
            x_waarde := to_char(v_nummer);
         ELSIF v_type = 'D'
         THEN
            -- Datum
            BEGIN
               v_datum := to_date(v_waarde
                                 ,v_datumformaatdoel);
            EXCEPTION
               WHEN OTHERS THEN
                  addtext(x_errormessage
                         ,v_max_len
                         ,'Kolom ' || v_kolom || ' heeft niet datumformaat ' || v_datumformaatdoel ||
                          '; de foute waarde is ''' || v_waarde || '''!');
                  RETURN FALSE;
            END;
            x_waarde := v_waarde; -- deze heeft nu het doel-formaat!

            -- NtB: nav. aanpassingen RDE, 26-02-2008
         ELSIF v_type = 'DT' -- RDE 20-02-2008
         THEN
            -- DatumTijd
            BEGIN
               v_datum := to_date(v_waarde
                                 ,v_datumtijdformaatdoel);
            EXCEPTION
               WHEN OTHERS THEN
                  addtext(x_errormessage
                         ,v_max_len
                         ,'Kolom ' || v_kolom || ' heeft niet datumtijdformaat ' || v_datumtijdformaatdoel ||
                          '; de foute waarde is ''' || v_waarde || '''!');
                  RETURN FALSE;
            END;
            x_waarde := v_waarde; -- deze heeft nu het doel-formaat!

         ELSIF v_type = 'T'
         THEN
            --> mantis 1363
            -- timestamp
            DECLARE
               v_tijdstip TIMESTAMP;
            BEGIN
               v_tijdstip := to_timestamp(v_waarde
                                         ,g_tijdstip_masker);
            EXCEPTION
               WHEN OTHERS THEN
                  addtext(x_errormessage
                         ,v_max_len
                         ,'Kolom ' || v_kolom || ' heeft niet het tijdformaat ' || g_tijdstip_masker ||
                          '; de foute waarde is ''' || v_waarde || '''!');
                  RETURN FALSE;
            END;
            x_waarde := v_waarde;
            --<-- mantis 1363
         ELSIF v_type = 'C'
         THEN
            -- Char
            IF length(v_waarde) > v_maxlengte
            THEN
               addtext(x_errormessage
                      ,v_max_len
                      ,'Kolom ' || v_kolom || ' mag maximaal ' || to_char(v_maxlengte) || ' lang zijn, maar is ' ||
                       to_char(length(v_waarde)) || ' lang; de foute waarde is ''' || v_waarde || '''!');
               RETURN FALSE;
            END IF;
            x_waarde := v_waarde; -- mantis 1363

         END IF;
      ELSE
         -- leeg! Verplicht?
         IF NOT verplichtok(v_waarde
                           ,v_kolom
                           ,v_verplicht
                           ,x_errormessage)
         THEN
            RETURN FALSE;
         END IF;
      END IF;
      RETURN TRUE;
   END typeok;

   FUNCTION refimpwok(p_impw_id      IN NUMBER
                     ,p_kolom        IN VARCHAR2
                     ,p_verplicht    IN BOOLEAN
                     ,p_check_in_LOV IN BOOLEAN -- mantis 5715
                     ,p_waarde_oud   IN VARCHAR2 -- mantis 5715
                     ,p_cimpw_domtab IN VARCHAR2 -- mantis 5715
                     ,x_waarde       IN OUT VARCHAR2
                     ,x_errormessage IN OUT VARCHAR2) RETURN BOOLEAN IS
      CURSOR c_impw IS
         SELECT impw.impa_id
              , impw.helix_waarde
              , impw.externe_id
              , impa.helix_table_domain
              , UPPER(TRANSLATE(impa.lov_query, ' ' || CHR(9) || CHR(10) || CHR(13), '    ')) lov_query
           FROM kgc_import_param_waarden impw
              , kgc_import_parameters impa
          WHERE impw.impw_id = p_impw_id
            AND impw.impa_id = impa.impa_id;
      r_impw c_impw%ROWTYPE;
      v_count NUMBER;
      v_pos NUMBER;
      v_alias VARCHAR2(30);
      v_prefix VARCHAR2(30);
      v_pk VARCHAR2(30);
      v_sql VARCHAR2(32767);
      v_nummer NUMBER;
   BEGIN
      x_waarde := NULL;
      IF p_impw_id IS NULL
      THEN
         IF NOT verplichtok(''
                           ,p_kolom
                           ,p_verplicht
                           ,x_errormessage)
         THEN
            RETURN FALSE;
         END IF;
         RETURN TRUE; -- OK
      END IF;
      OPEN c_impw;
      FETCH c_impw
         INTO r_impw;
      IF c_impw%NOTFOUND
      THEN
         -- mag niet; fout bij inlezen
         CLOSE c_impw;
         addtext(x_errormessage
                ,v_max_len
                ,'Systeemfout - impw_id ' || to_char(p_impw_id) || ' niet gevonden!');
         RETURN FALSE;
      END IF;
      CLOSE c_impw;
      -- check type
      IF p_cimpw_domtab IS NOT NULL AND r_impw.helix_table_domain != p_cimpw_domtab
      THEN
         addtext(x_errormessage
                ,v_max_len
                ,'Bij ''' || p_kolom || ''' is een waarde aanwezig (' || p_impw_id ||
                 ') van type ' || r_impw.helix_table_domain || ', maar dit moet type ' || p_cimpw_domtab || ' zijn!');
         RETURN FALSE;
      END IF;
      -- check vervallen (via LOV
      IF p_check_in_LOV AND r_impw.helix_waarde IS NOT NULL -- AND r_impw.lov_query IS NOT NULL
         AND NVL(p_waarde_oud, CHR(2)) != NVL(r_impw.helix_waarde, CHR(2))
      THEN -- check + gewijzigd
         SELECT COUNT(*)
           INTO v_count
           FROM all_tab_columns atc
          WHERE atc.owner = 'HELIX'
            AND atc.table_name = p_cimpw_domtab;
         IF v_count > 0
         THEN -- het is een tabel; dan moet
            -- Numeriek?
            BEGIN
               v_nummer := to_number(r_impw.helix_waarde);
            EXCEPTION
               WHEN OTHERS THEN
                  addtext(x_errormessage
                         ,v_max_len
                         ,'Kolom ' || p_kolom || ' is niet numeriek; de foute waarde is ''' || r_impw.helix_waarde || '''!');
                  RETURN FALSE;
            END;
            v_alias := kgc_util_00.tabel_alias(p_cimpw_domtab);
            v_sql := 'SELECT COUNT(*) INTO :result FROM ' || p_cimpw_domtab || ' WHERE ' || v_alias || '_id = ' || r_impw.helix_waarde;
            BEGIN
               v_count := kgc_util_00.dyn_exec(v_sql);
            EXCEPTION
               WHEN OTHERS THEN
                  addtext(x_errormessage
                         ,v_max_len
                         ,'ERROR in query=' || v_sql || '; error=' || SQLERRM);
                  v_count := 0;
            END;
            IF v_count = 0
            THEN -- error
               addtext(x_errormessage
                      ,v_max_len
                      ,'Bij ''' || p_kolom || ''' is een externe waarde (' || r_impw.externe_id ||
                       ') gekoppeld aan een Helix-waarde (' || r_impw.helix_waarde || '), en deze Helix-waarde bestaat niet!');
               RETURN FALSE;
            END IF;
            -- betreft het een tabel met een vervallen-kolom?
            SELECT COUNT(*)
              INTO v_count
              FROM all_tab_columns atc
             WHERE atc.owner = 'HELIX'
               AND atc.table_name = p_cimpw_domtab
               AND atc.column_name = 'VERVALLEN';
            IF v_count > 0
            THEN -- controle mogelijk
               v_sql := 'SELECT COUNT(*) INTO :result FROM ' || p_cimpw_domtab || ' WHERE ' || v_alias || '_id = ' || r_impw.helix_waarde || ' AND vervallen = ''N''';
               BEGIN
                  v_count := kgc_util_00.dyn_exec(v_sql);
               EXCEPTION
                  WHEN OTHERS THEN
                     addtext(x_errormessage
                            ,v_max_len
                            ,'ERROR in query=' || v_sql || '; error=' || SQLERRM);
                     v_count := 0;
               END;
               IF v_count = 0
               THEN -- error
                  addtext(x_errormessage
                         ,v_max_len
                         ,'Bij ''' || p_kolom || ''' is een externe waarde (' || r_impw.externe_id ||
                          ') gekoppeld aan een Helix-waarde (' || r_impw.helix_waarde || '), en deze Helix-waarde is vervallen!');
                  RETURN FALSE;
               END IF;
            END IF;
         END IF;
      END IF;
      IF r_impw.helix_waarde IS NULL
      THEN
         addtext(x_errormessage
                ,v_max_len
                ,'Bij ''' || p_kolom || ''' is een externe waarde aanwezig (' || r_impw.externe_id ||
                 '), maar daar is nog geen corresponderende Helix-' || r_impw.helix_table_domain ||
                 ' aan gekoppeld!');
         RETURN FALSE; -- fout: geen waarde opgegeven bij een impw-id
      END IF;
      -- ok
      x_waarde := r_impw.helix_waarde;
      RETURN TRUE;
   END refimpwok;

   FUNCTION refimpwok(p_impw_id      IN NUMBER
                     ,p_kolom        IN VARCHAR2
                     ,p_verplicht    IN BOOLEAN
                     ,x_waarde       IN OUT VARCHAR2
                     ,x_errormessage IN OUT VARCHAR2) RETURN BOOLEAN IS
   BEGIN
      RETURN refimpwok(p_impw_id   => p_impw_id
                              ,p_kolom  => p_kolom
                              ,p_verplicht => p_verplicht
                              ,p_check_in_LOV => FALSE
                              ,p_waarde_oud   => NULL
                              ,p_cimpw_domtab => NULL
                              ,x_waarde       => x_waarde
                              ,x_errormessage => x_errormessage
                              );
   END;

   FUNCTION im2pers
   ( p_impe        IN     kgc_im_personen%ROWTYPE     -- bron
   , p_id          IN     VARCHAR2                    -- identificatie impe-record
   , p_zisnr_overnemen IN VARCHAR2
   , p_beheer_in_zis IN VARCHAR2
   , p_pers_old    IN     cg$kgc_personen.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_pers        IN OUT cg$kgc_personen.cg$row_type -- doel
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      -- vertaal impe naar pers, en controleer
      CURSOR c_rela_huisarts(p_rela_id NUMBER) IS
         SELECT NULL
           FROM kgc_relaties rela
          WHERE rela.rela_id = p_rela_id
            AND rela.relatie_type = 'HA';
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
      v_pers_aanspreken VARCHAR2(2000);
      v_beheer_in_zis            VARCHAR2(1) := p_beheer_in_zis;

   BEGIN
      -- achternaam
      IF NOT typeok(p_impe.achternaam
                   ,p_id || ' - Achternaam'
                   ,'C'
                   ,30
                   ,0
                   ,TRUE
                   ,x_pers.achternaam
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         kgc_formaat_00.formatteer('ACHTERNAAM'
                                  ,x_pers.achternaam);
      END IF;

      -- gehuwd: leeg, of JaNee-like
      IF NOT typeok(p_impe.gehuwd
                   ,p_id || ' - Gehuwd'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_pers.gehuwd
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.gehuwd := NVL(x_pers.gehuwd, 'N');
      END IF;
      -- geslacht: uit kgc_import_param_waarden
      IF NOT refimpwok(p_impe.impw_id_geslacht
                      ,p_id || ' - Geslacht'
                      ,FALSE
                      ,x_pers.geslacht
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.geslacht := NVL(x_pers.geslacht, 'O');
      END IF;
      -- bloedverwant
      IF NOT typeok(p_impe.bloedverwant
                   ,p_id || ' - Bloedverwant'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_pers.bloedverwant
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.bloedverwant := NVL(x_pers.bloedverwant, 'N');
      END IF;
      -- meerling
      IF NOT typeok(p_impe.meerling
                   ,p_id || ' - Meerling'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_pers.meerling
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.meerling := NVL(x_pers.meerling, 'N');
      END IF;
      -- overleden
      IF NOT typeok(p_impe.overleden
                   ,p_id || ' - Overleden'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_pers.overleden
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.overleden := NVL(x_pers.overleden, 'N');
      END IF;
      -- default_aanspreken
      IF NOT typeok(p_impe.default_aanspreken
                   ,p_id || ' - Default aanspreken'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_pers.default_aanspreken
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.default_aanspreken := NVL(x_pers.default_aanspreken, 'J');
      END IF;
      -- 5 kolommen overslaan
      -- rela_id
      IF NOT refimpwok(p_impe.impw_id_rela
                      ,p_id || ' - Huisarts'
                      ,FALSE
                      ,TRUE
                      ,p_pers_old.rela_id
                      ,g_cimpw_tab_rela
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.rela_id := TO_NUMBER(v_db_waarde);
         IF x_pers.rela_id IS NOT NULL
         THEN
            -- controle of relatie ook een huisarts is
            OPEN c_rela_huisarts(x_pers.rela_id);
            FETCH c_rela_huisarts
               INTO v_db_waarde;
            IF c_rela_huisarts%NOTFOUND
            THEN
               v_ok := FALSE;
               addtext(x_opmerkingen
                      ,v_max_len
                      ,p_id || ' - Huisarts is wel een Helix-relatie, maar deze relatie is niet van het type Huisarts!');
            END IF;
            CLOSE c_rela_huisarts;
         END IF;
      END IF;
      -- verz_id
      IF NOT refimpwok(p_impe.impw_id_verz
                      ,p_id || ' - Verzekeraar'
                      ,FALSE
                      ,TRUE
                      ,p_pers_old.verz_id
                      ,g_cimpw_tab_verz
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.verz_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- ZISnr evt controleren
      IF p_zisnr_overnemen = 'J'
      THEN
         IF NOT typeok(p_impe.zisnr
                      ,p_id || ' - ZISnr'
                      ,'C'
                      ,10
                      ,0
                      ,FALSE
                      ,x_pers.zisnr
                      ,x_opmerkingen)
         THEN
            v_ok := FALSE;
         END IF;
      END IF;
      --
      -- BSN geverifieerd
      IF NOT typeok(p_impe.bsn_geverifieerd
                   ,p_id || ' - BSN_GEVERIFIEERD'
                   ,'J'
                   ,1
                   ,0
                   ,FALSE
                   ,x_pers.bsn_geverifieerd
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;

      -- NtB, 12-06-2009, 283: indien beheer_in_zis is 'N' (igv. ASTRAIA),
      -- dan moet verificatie ook op 'N'.
      -- NtB, 16-06-2009, 283: indien beheer_in_zis is 'J' (igv. MOSOS)
      -- en BSN is gevuld, dan moet verificatie ook op 'J' -> wordt al gezet
      -- door MOSOS-ZIS-interactie.
      IF p_beheer_in_zis = 'N'
      THEN
         x_pers.bsn_geverifieerd := 'N';
      END IF;

      -- aanspreken komt later
      -- geboortedatum
      IF NOT typeok(p_impe.geboortedatum
                   ,p_id || ' - Geboortedatum'
                   ,'D'
                   ,0
                   ,0
                   ,(NVL(p_impe.anoniem, 'N') = 'N') -- mantis 11620
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.geboortedatum := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- partiele geboortedatum
      IF NOT typeok(p_impe.part_geboortedatum
                   ,p_id || ' - Partiele geboortedatum'
                   ,'C'
                   ,10
                   ,0
                   ,FALSE
                   ,x_pers.part_geboortedatum
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;

      -- BSN (pas na geboortedatum
      IF NOT typeok(p_impe.bsn
                   ,p_id || ' - BSN'
                   ,'N'
                   ,9
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         -- gaat nu ook via controleer_bsn
         BEGIN
            x_pers.bsn := TO_NUMBER(v_db_waarde);
            kgc_pers_00.controleer_bsn(p_pers_id              => x_pers.pers_id -- mantis 11620: deze functie wordt meerdere keren aangeroepen, 1 keer om gegevens op te halen tbv koppeling; 2e keer na evt koppeling
                                      ,p_zisnr                => x_pers.zisnr
                                      ,p_bsn                  => x_pers.bsn
                                      ,p_bsn_geverifieerd     => x_pers.bsn_geverifieerd
                                      ,p_controleer_uniciteit => 'J'
                                      -- Mantis-3068 11-10-2016 RDE: 3 extra parameters
                                      ,p_achternaam           => x_pers.achternaam
                                      ,p_geboortedatum        => x_pers.geboortedatum
                                      ,p_geslacht             => x_pers.geslacht
                                      ); -- 11-05-2009: ook op dubbele controleren
         EXCEPTION
            WHEN OTHERS THEN
               -- fouten ophalen van error-stack
               addtext(x_opmerkingen
                      ,v_max_len
                      ,p_id || ' - BSN fout: ' || nvl(cg$errors.geterrors()
                                                          ,SQLERRM));

               v_ok := FALSE;
               -- en vervolgens doorgaan...
         END;
      END IF;
      -- voorletters
      IF NOT typeok(p_impe.voorletters
                   ,p_id || ' - Voorletters'
                   ,'C'
                   ,30 -- rde 17-03-2014 vergeten bij mantis 182
                   ,0
                   ,(NVL(p_impe.anoniem, 'N') = 'N') -- mantis 11620
                   ,x_pers.voorletters
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         kgc_formaat_00.formatteer('VOORLETTERS'
                                  ,x_pers.voorletters);
      END IF;
      -- voorvoegsel
      IF NOT typeok(p_impe.voorvoegsel
                   ,p_id || ' - Voorvoegsel'
                   ,'C'
                   ,10
                   ,0
                   ,FALSE
                   ,x_pers.voorvoegsel
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         kgc_formaat_00.formatteer('VOORVOEGSEL'
                                  ,x_pers.voorvoegsel);
      END IF;
      -- achternaam partner
      IF NOT typeok(p_impe.achternaam_partner
                   ,p_id || ' - Achternaam partner'
                   ,'C'
                   ,30
                   ,0
                   ,FALSE
                   ,x_pers.achternaam_partner
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         kgc_formaat_00.formatteer('ACHTERNAAM'
                                  ,x_pers.achternaam_partner);
      END IF;
      -- voorvoegsel partner
      IF NOT typeok(p_impe.voorvoegsel_partner
                   ,p_id || ' - Voorvoegsel partner'
                   ,'C'
                   ,10
                   ,0
                   ,FALSE
                   ,x_pers.voorvoegsel_partner
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         kgc_formaat_00.formatteer('VOORVOEGSEL'
                                  ,x_pers.voorvoegsel_partner);
      END IF;
      -- *** begin adres
      -- vertaal land, postcode, adres, woonplaats
      DECLARE
         -- vertaal adresgegevens;
         v_adres          VARCHAR2(2000) := p_impe.adres;
         v_postcode       VARCHAR2(2000) := p_impe.postcode;
         v_woonplaats     VARCHAR2(2000) := p_impe.woonplaats;
         v_woonplaats_pc  VARCHAR2(2000);
         v_provincie      VARCHAR2(2000);
         v_provincie_pc   VARCHAR2(2000);
         v_land           VARCHAR2(2000);

         v_naar_adres     BOOLEAN := FALSE;
         v_dummy_boolean  BOOLEAN;
      BEGIN
         -- indien land leeg: NL
         v_db_waarde := p_impe.land;
         x_pers.land := NULL;
         IF v_db_waarde IS NOT NULL
         THEN
            kgc_land_00.controleer(v_db_waarde);
            v_qms_error_mess := cg$errors.geterrors();
            IF length(v_qms_error_mess) > 0
            THEN
               addtext(x_opmerkingen
                      ,v_max_len
                      ,'Kolom PERSOON - Land fout: ' || v_qms_error_mess);
               v_ok := FALSE;
            ELSIF v_db_waarde IS NULL
            THEN
               -- fout: land niet gevonden
               addtext(x_opmerkingen
                      ,v_max_len
                      ,'Kolom PERSOON - Land komt niet voor in Helix!');
               v_ok := FALSE;
            ELSE
               kgc_formaat_00.formatteer('LAND'
                                        ,v_db_waarde);
               IF NOT typeok(v_db_waarde
                            ,'PERSOON - Land'
                            ,'C'
                            ,50
                            ,0
                            ,FALSE
                            ,x_pers.land
                            ,x_opmerkingen)
               THEN
                  v_ok := FALSE;
               END IF;
            END IF;
         END IF;
         -- postcode
         IF x_pers.land IS NULL
            OR upper(x_pers.land) = 'NEDERLAND'
         THEN
            -- formateer plaats
            v_dummy_boolean := kgc_pocd_00.postcode_ok(p_postcode => v_postcode);
            kgc_pocd_00.gerelateerde_gegevens(p_postcode  => v_postcode
                                             ,x_gevonden  => v_dummy_boolean
                                             ,x_plaats    => v_woonplaats_pc
                                             ,x_provincie => v_provincie_pc
                                             ,x_land      => v_land   -- niets mee doen, al ok
                                             );
            IF nvl(length(v_woonplaats)
                  ,0) = 0
            THEN
               v_woonplaats := v_woonplaats_pc;
            END IF;
            IF nvl(length(v_provincie)
                  ,0) = 0
            THEN
               x_pers.provincie := v_provincie_pc;
            END IF;
         ELSE
            v_naar_adres := TRUE;
         END IF;
         -- postcode
         IF NOT typeok(v_postcode
                      ,'PERSOON - Postcode'
                      ,'C'
                      ,7
                      ,0
                      ,FALSE
                      ,x_pers.postcode
                      ,x_opmerkingen)
         THEN
            v_ok := FALSE;
         END IF;
         -- adres
         IF NOT typeok(v_adres
                      ,'PERSOON - Adres'
                      ,'C'
                      ,250
                      ,0
                      ,FALSE
                      ,x_pers.adres
                      ,x_opmerkingen)
         THEN
            v_ok := FALSE;
         END IF;
         -- woonplaats
         kgc_formaat_00.formatteer('PLAATS'
                                  ,v_woonplaats);
         IF NOT typeok(v_woonplaats
                      ,'PERSOON - Woonplaats'
                      ,'C'
                      ,50
                      ,0
                      ,FALSE
                      ,x_pers.woonplaats
                      ,x_opmerkingen)
         THEN
            v_ok := FALSE;
         END IF;

         IF v_naar_adres
         THEN
            x_pers.adres      := kgc_formaat_00.volledig_adres(p_naam       => NULL
                                                              ,p_adres      => v_adres
                                                              ,p_postcode   => v_postcode
                                                              ,p_woonplaats => v_woonplaats
                                                              ,p_land       => x_pers.land);
            x_pers.postcode   := NULL;
            x_pers.woonplaats := NULL;
            x_pers.provincie  := NULL;
            -- lengte...
            IF length(v_adres) > 250
            THEN
               addtext(x_opmerkingen
                      ,v_max_len
                      ,p_id || ' - Samenstelling adres is te lang (maximaal 250)');
               v_ok := FALSE;
            END IF;
         END IF;
         -- provincie - eventueel al bekend via postcode!
         IF nvl(length(x_pers.provincie)
               ,0) = 0
         THEN
            IF NOT refimpwok(p_impe.impw_id_provincie
                            ,p_id || ' - Provincie'
                            ,FALSE
                            ,x_pers.provincie
                            ,x_opmerkingen)
            THEN
               v_ok := FALSE;
            END IF;
         END IF;


      EXCEPTION
         WHEN OTHERS THEN
            addtext(x_opmerkingen
                   ,v_max_len
                   ,'Error in vertaal_adres: ' || NVL(cg$errors.geterrors, SQLERRM));
            v_ok := FALSE;
      END; -- vertaal_adres;


      -- *** einde adres
      -- telefoon
      IF NOT typeok(p_impe.telefoon
                   ,p_id || ' - Telefoon'
                   ,'C'
                   ,30 -- Rework Mantis 3184: Interface voor Dragon
                   ,0
                   ,FALSE
                   ,x_pers.telefoon
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- telefax
      IF NOT typeok(p_impe.telefax
                   ,p_id || ' - Telefax'
                   ,'C'
                   ,30
                   ,0
                   ,FALSE
                   ,x_pers.telefax
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- email
      IF NOT typeok(p_impe.email
                   ,p_id || ' - Email'
                   ,'C'
                   ,30
                   ,0
                   ,FALSE
                   ,x_pers.email
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- verzekeringswijze
      IF NOT refimpwok(p_impe.impw_id_verzwijze
                      ,p_id || ' - Verzekeringswijze'
                      ,FALSE
                      ,x_pers.verzekeringswijze
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- verzekeringsnr
      IF NOT typeok(p_impe.verzekeringsnr
                   ,p_id || ' - Verzekeringsnr'
                   ,'C'
                   ,30
                   ,0
                   ,FALSE
                   ,x_pers.verzekeringsnr
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- overlijdensdatum
      IF NOT typeok(p_impe.overlijdensdatum
                   ,p_id || ' - Overlijdensdatum'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.overlijdensdatum := TO_DATE(v_db_waarde, v_datumtijdformaatdoel);
      END IF;
      -- hoeveelling
      IF NOT typeok(p_impe.hoeveelling
                   ,p_id || ' - Hoeveelling'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_pers.hoeveelling := TO_NUMBER(v_db_waarde);
      END IF;
      -- aanspreken - evt opnieuw bepalen obv aangeleverde gegevens:
      v_pers_aanspreken := p_impe.aanspreken;
      IF x_pers.default_aanspreken = 'J'
         OR -- altijd doen
         p_impe.aanspreken IS NULL
         OR -- niet aangeleverd
         upper(p_impe.aanspreken) = upper(x_pers.achternaam_partner)
         OR -- fout aangeleverd: afgesproken is om aanspreken in patient.name_partner te zetten: deze is over want er is ook een partner.name. En: bij aanspreken horen altijd de eigen voorletters, dus dit kan niet per ongeluk gelijk zijn!
         (p_impe.kgc_pers_id IS NULL AND x_pers.default_aanspreken IS NULL) -- nieuw
      THEN
         kgc_formaat_00.standaard_aanspreken(p_type                => 'PERS'
                                            ,p_voorletters         => x_pers.voorletters
                                            ,p_voorvoegsel         => x_pers.voorvoegsel
                                            ,p_achternaam          => x_pers.achternaam
                                            ,p_voorvoegsel_partner => x_pers.voorvoegsel_partner
                                            ,p_achternaam_partner  => x_pers.achternaam_partner
                                            ,p_gehuwd              => x_pers.gehuwd
                                            ,p_geslacht            => x_pers.geslacht
                                            ,p_prefix              => NULL
                                            ,p_postfix             => NULL
                                            ,x_aanspreken          => v_pers_aanspreken);
      END IF;
      IF NOT typeok(v_pers_aanspreken
                   ,p_id || ' - Aanspreken'
                   ,'C'
                   ,125 -- rde 17-03-2014 vergeten bij mantis 182
                   ,0
                   ,FALSE
                   ,x_pers.aanspreken
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;

      -- beheer_in_zis
      -- voor zowel een nieuwe als bestaande persoon: overnemen uit parameter
      --            beheer_in_zis op N geldt alleen voor ASTRAIA; MOSOS
      -- echter: ALLEEN indien ZISnr gevuld is! (toevoeging agv mantis 11620)
      IF x_pers.zisnr IS NULL
      THEN
         v_beheer_in_zis := 'N';
      END IF;
      IF NOT typeok(v_beheer_in_zis
                   ,p_id || ' - beheer_in_zis'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_pers.beheer_in_zis
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;


      -- zoekfamilie - wordt automatisch bijgewerkt!
      RETURN v_ok;
   END im2pers;

   FUNCTION verschil
   ( p_n VARCHAR2
   , p_o VARCHAR2
   , p_kolomnaam VARCHAR2
   , x_gewijzigd IN OUT BOOLEAN
   , p_format VARCHAR2 := NULL
   ) RETURN VARCHAR2 IS
   BEGIN
      -- voor een aantal kolommen geldt: casewijziging: niet doorvoeren, evenals puntje verschil (in bv vltrs: RLF = R.L.F)
      IF LOWER(p_kolomnaam) IN ('achternaam', 'aanspreken', 'voorletters', 'voorvoegsel', 'achternaam_partner', 'voorvoegsel_partner')
      THEN
         x_gewijzigd := NOT (NVL(UPPER(REPLACE(p_n, '.', '')), CHR(2)) = NVL(UPPER(REPLACE(p_o, '.', '')), CHR(2)));
      ELSE
         x_gewijzigd := NOT (NVL(p_o, CHR(2)) = NVL(p_n, CHR(2)));
      END IF;

      IF x_gewijzigd
      THEN
         RETURN p_kolomnaam || '(' || NVL(p_o,' ') || '->' || NVL(p_n, ' ') || ')';
      END IF;
      RETURN NULL;
   END verschil;

   FUNCTION verschil
   ( p_n DATE
   , p_o DATE
   , p_kolomnaam VARCHAR2
   , x_gewijzigd IN OUT BOOLEAN
   , p_format VARCHAR2 := NULL
   ) RETURN VARCHAR2 IS
   BEGIN
      RETURN verschil
             ( p_n => TO_CHAR(p_n, v_datumformaatdoel)
             , p_o => TO_CHAR(p_o, v_datumformaatdoel)
             , p_kolomnaam => p_kolomnaam
             , x_gewijzigd => x_gewijzigd
             , p_format => p_format
             );
   END verschil;

   FUNCTION verschil_pers
   ( p_o cg$kgc_personen.cg$row_type
   , p_n cg$kgc_personen.cg$row_type
   , p_i IN OUT cg$kgc_personen.cg$ind_type
   , p_type VARCHAR2 -- PI = alleen persoonsidentificarend, anders alles
   , p_zisnr_overnemen IN VARCHAR2
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken

      addtext(verschil(p_n.achternaam
                      ,p_o.achternaam
                      ,'achternaam'
                      ,p_i.achternaam));
      addtext(verschil(p_n.aanspreken
                      ,p_o.aanspreken
                      ,'aanspreken'
                      ,p_i.aanspreken));
      IF p_n.geslacht IS NULL
      THEN
         p_i.geslacht := FALSE;
      ELSE
         addtext(verschil(p_n.geslacht
                         ,p_o.geslacht
                         ,'geslacht'
                         ,p_i.geslacht));
      END IF;
      -- zisnr: indien zisnr_overnemen = N, dan heeft p_n.zisnr de bestaande waarde
      IF p_zisnr_overnemen = 'J'
      THEN
         addtext(verschil(p_n.zisnr
                         ,p_o.zisnr
                         ,'zisnr'
                         ,p_i.zisnr));
      END IF;
      -- bsn: nooit leegmaken!
      IF p_n.bsn IS NOT NULL
      THEN
         addtext(verschil(p_n.bsn
                         ,p_o.bsn
                         ,'bsn'
                         ,p_i.bsn));
      END IF;
      addtext(verschil(p_n.voorletters
                      ,p_o.voorletters
                      ,'voorletters'
                      ,p_i.voorletters));
      addtext(verschil(p_n.voorvoegsel
                      ,p_o.voorvoegsel
                      ,'voorvoegsel'
                      ,p_i.voorvoegsel));
      addtext(verschil(p_n.geboortedatum
                      ,p_o.geboortedatum
                      ,'geboortedatum'
                      ,p_i.geboortedatum));

      IF p_type = 'PI'
      THEN
         -- let op: alleen partner nodig indien gebruikt in aanspreken: default_aanspreken + gehuwd
         IF p_n.default_aanspreken = 'J'
            AND p_n.gehuwd = 'J'
         THEN
            addtext(verschil(p_n.achternaam_partner
                            ,p_o.achternaam_partner
                            ,'achternaam_partner'
                            ,p_i.achternaam_partner));
            addtext(verschil(p_n.voorvoegsel_partner
                            ,p_o.voorvoegsel_partner
                            ,'voorvoegsel_partner'
                            ,p_i.voorvoegsel_partner));
         END IF;
         RETURN v_verschil; -- klaar met persoonsidentificerende
      END IF;

      addtext(verschil(p_n.achternaam_partner
                      ,p_o.achternaam_partner
                      ,'achternaam_partner'
                      ,p_i.achternaam_partner));
      addtext(verschil(p_n.voorvoegsel_partner
                      ,p_o.voorvoegsel_partner
                      ,'voorvoegsel_partner'
                      ,p_i.voorvoegsel_partner));
      IF p_n.bsn_geverifieerd IS NULL
      THEN
         p_i.bsn_geverifieerd := FALSE;
      ELSE
         addtext(verschil(p_n.bsn_geverifieerd
                         ,p_o.bsn_geverifieerd
                         ,'bsn_geverifieerd'
                         ,p_i.bsn_geverifieerd));
      END IF;
      addtext(verschil(p_n.beheer_in_zis
                      ,p_o.beheer_in_zis
                      ,'beheer_in_zis'
                      ,p_i.beheer_in_zis));
      IF p_n.gehuwd IS NULL
      THEN
         p_i.gehuwd := FALSE;
      ELSE
         addtext(verschil(p_n.gehuwd
                         ,p_o.gehuwd
                         ,'gehuwd'
                         ,p_i.gehuwd));
      END IF;
      IF p_n.bloedverwant IS NULL
      THEN
         p_i.bloedverwant := FALSE;
      ELSE
         addtext(verschil(p_n.bloedverwant
                         ,p_o.bloedverwant
                         ,'bloedverwant'
                         ,p_i.bloedverwant));
      END IF;
      IF p_n.meerling IS NULL
      THEN
         p_i.meerling := FALSE;
      ELSE
         addtext(verschil(p_n.meerling
                         ,p_o.meerling
                         ,'meerling'
                         ,p_i.meerling));
      END IF;
      IF p_n.overleden IS NULL
      THEN
         p_i.overleden := FALSE;
      ELSE
         addtext(verschil(p_n.overleden
                         ,p_o.overleden
                         ,'overleden'
                         ,p_i.overleden));
      END IF;
      IF p_n.default_aanspreken IS NULL
      THEN
         p_i.default_aanspreken := FALSE;
      ELSE
         addtext(verschil(p_n.default_aanspreken
                         ,p_o.default_aanspreken
                         ,'default_aanspreken'
                         ,p_i.default_aanspreken));
      END IF;
      addtext(verschil(p_n.rela_id
                      ,p_o.rela_id
                      ,'rela_id'
                      ,p_i.rela_id));
      addtext(verschil(p_n.verz_id
                      ,p_o.verz_id
                      ,'verz_id'
                      ,p_i.zisnr));
      addtext(verschil(p_n.part_geboortedatum
                      ,p_o.part_geboortedatum
                      ,'part_geboortedatum'
                      ,p_i.part_geboortedatum));
      addtext(verschil(p_n.adres
                      ,p_o.adres
                      ,'adres'
                      ,p_i.adres));
      addtext(verschil(p_n.postcode
                      ,p_o.postcode
                      ,'postcode'
                      ,p_i.postcode));
      addtext(verschil(p_n.woonplaats
                      ,p_o.woonplaats
                      ,'woonplaats'
                      ,p_i.woonplaats));
      addtext(verschil(p_n.provincie
                      ,p_o.provincie
                      ,'provincie'
                      ,p_i.provincie));
      IF p_n.land IS NULL
      THEN
         p_i.land := FALSE;
      ELSE
         addtext(verschil(p_n.land
                         ,p_o.land
                         ,'land'
                         ,p_i.land));
      END IF;
      addtext(verschil(p_n.telefoon
                      ,p_o.telefoon
                      ,'telefoon'
                      ,p_i.telefoon));
      addtext(verschil(p_n.telefoon2
                      ,p_o.telefoon2
                      ,'telefoon2'
                      ,p_i.telefoon2));
      addtext(verschil(p_n.telefax
                      ,p_o.telefax
                      ,'telefax'
                      ,p_i.telefax));
      addtext(verschil(p_n.email
                      ,p_o.email
                      ,'email'
                      ,p_i.email));
      addtext(verschil(p_n.verzekeringswijze
                      ,p_o.verzekeringswijze
                      ,'verzekeringswijze'
                      ,p_i.verzekeringswijze));
      addtext(verschil(p_n.verzekeringsnr
                      ,p_o.verzekeringsnr
                      ,'verzekeringsnr'
                      ,p_i.verzekeringsnr));
      addtext(verschil(p_n.overlijdensdatum
                      ,p_o.overlijdensdatum
                      ,'overlijdensdatum'
                      ,p_i.overlijdensdatum));
      addtext(verschil(p_n.hoeveelling
                      ,p_o.hoeveelling
                      ,'hoeveelling'
                      ,p_i.hoeveelling));
      RETURN v_verschil;
   END verschil_pers;

   FUNCTION im2zwan
   ( p_imzw        IN     kgc_im_zwangerschappen%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_zwan_old    IN     cg$kgc_zwangerschappen.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_zwan        IN OUT cg$kgc_zwangerschappen.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal im naar x_, en controleer
      x_zwan.pers_id := TO_NUMBER(SUBSTR(get_im2hlx('IMPE' || p_imzw.impe_id), 5));

      -- nummer; verplicht!
      IF NOT typeok(p_imzw.nummer
                   ,p_id || ' - Hoeveelste zwangerschap'
                   ,'N'
                   ,2
                   ,0
                   ,TRUE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.nummer := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum_aterm
      IF NOT typeok(p_imzw.datum_aterm
                   ,p_id || ' - Aterme datum'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.datum_aterm := to_date(v_db_waarde
                                          ,v_datumformaatdoel);
      END IF;
      -- info_geslacht
      IF NOT typeok(p_imzw.info_geslacht
                   ,p_id || ' - Info geslacht'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.info_geslacht := v_db_waarde;
      END IF;
      -- consanguiniteit
      IF NOT typeok(p_imzw.consanguiniteit
                   ,p_id || ' - Consanguiniteit'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.consanguiniteit := v_db_waarde;
      END IF;
      -- impw_id_rela
      IF NOT refimpwok(p_imzw.impw_id_rela
                      ,p_id || ' - Verwijzer'
                      ,FALSE
                      ,TRUE
                      ,p_zwan_old.rela_id
                      ,g_cimpw_tab_rela
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.rela_id := v_db_waarde;
      END IF;
      -- impw_id_zekerheid_datum_lm
      IF NOT refimpwok(p_imzw.impw_id_zekerheid_datum_lm
                      ,p_id || ' - Zekerheid datum LM'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.zekerheid_datum_lm := v_db_waarde;
      END IF;
      -- datum_lm
      IF NOT typeok(p_imzw.datum_lm
                   ,p_id || ' - Datum Laatste menstuatie'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.datum_lm := to_date(v_db_waarde
                                       ,v_datumformaatdoel);
      END IF;
      -- impw_id_berekeningswijze
      IF NOT refimpwok(p_imzw.impw_id_berekeningswijze
                      ,p_id || ' - Berekeningswijze LM'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.berekeningswijze := v_db_waarde;
      END IF;
      -- medicatie
      IF NOT typeok(p_imzw.medicatie
                   ,p_id || ' - Medicatie'
                   ,'C'
                   ,1000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.medicatie := v_db_waarde;
      END IF;
      -- opmerkingen
      IF NOT typeok(p_imzw.opmerkingen
                   ,p_id || ' - Opmerkingen'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.opmerkingen := v_db_waarde;
      END IF;
      -- Abortus
      IF NOT typeok(p_imzw.abortus
                   ,p_id || ' - Abortus'
                   ,'C'
                   ,1
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.abortus := v_db_waarde;
      END IF;
      -- Abortussen
      IF NOT typeok(p_imzw.abortussen
                   ,p_id || ' - Abortussen'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.abortussen := TO_NUMBER(v_db_waarde);
      END IF;
      -- Apla
      IF NOT typeok(p_imzw.apla
                   ,p_id || ' - Apla'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.apla := TO_NUMBER(v_db_waarde);
      END IF;
      -- Eug
      IF NOT typeok(p_imzw.eug
                   ,p_id || ' - Eug'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.eug := TO_NUMBER(v_db_waarde);
      END IF;
      -- Hoeveelling
      IF NOT typeok(p_imzw.hoeveelling
                   ,p_id || ' - Hoeveelling'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.hoeveelling := TO_NUMBER(v_db_waarde);
      END IF;
      -- Mola
      IF NOT typeok(p_imzw.mola
                   ,p_id || ' - Mola'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.mola := TO_NUMBER(v_db_waarde);
      END IF;
      -- Para
      IF NOT typeok(p_imzw.para
                   ,p_id || ' - Para'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.para := TO_NUMBER(v_db_waarde);
      END IF;
      -- Progenituur
      IF NOT typeok(p_imzw.progenituur
                   ,p_id || ' - Progenituur'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_zwan.progenituur := TO_NUMBER(v_db_waarde);
      END IF;

      RETURN v_ok;
   END im2zwan;

   FUNCTION verschil_zwan
   ( p_o cg$kgc_zwangerschappen.cg$row_type
   , p_n cg$kgc_zwangerschappen.cg$row_type
   , p_i IN OUT cg$kgc_zwangerschappen.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      IF p_n.info_geslacht IS NULL
      THEN
         p_i.info_geslacht := FALSE;
      ELSE
         addtext(verschil(p_n.info_geslacht
                         ,p_o.info_geslacht
                         ,'info_geslacht'
                         ,p_i.info_geslacht));
      END IF;
      IF p_n.consanguiniteit IS NULL
      THEN
         p_i.consanguiniteit := FALSE;
      ELSE
         addtext(verschil(p_n.consanguiniteit
                         ,p_o.consanguiniteit
                         ,'consanguiniteit'
                         ,p_i.consanguiniteit));
      END IF;
      addtext(verschil(p_n.rela_id
                      ,p_o.rela_id
                      ,'rela_id'
                      ,p_i.rela_id));
      addtext(verschil(p_n.zekerheid_datum_lm
                      ,p_o.zekerheid_datum_lm
                      ,'zekerheid_datum_lm'
                      ,p_i.zekerheid_datum_lm));
      addtext(verschil(p_n.datum_lm
                      ,p_o.datum_lm
                      ,'datum_lm'
                      ,p_i.datum_lm));
      IF p_n.berekeningswijze IS NULL
      THEN
         p_i.berekeningswijze := FALSE;
      ELSE
         addtext(verschil(p_n.berekeningswijze
                         ,p_o.berekeningswijze
                         ,'berekeningswijze'
                         ,p_i.berekeningswijze));
      END IF;
      addtext(verschil(p_n.datum_aterm
                      ,p_o.datum_aterm
                      ,'datum_aterm'
                      ,p_i.datum_aterm));
      addtext(verschil(p_n.medicatie
                      ,p_o.medicatie
                      ,'medicatie'
                      ,p_i.medicatie));
      addtext(verschil(p_n.opmerkingen
                      ,p_o.opmerkingen
                      ,'opmerkingen'
                      ,p_i.opmerkingen));
      IF p_n.abortus IS NULL
      THEN
         p_i.abortus := FALSE;
      ELSE
         addtext(verschil(p_n.abortus
                         ,p_o.abortus
                         ,'abortus'
                         ,p_i.abortus));
      END IF;
      addtext(verschil(p_n.abortussen
                      ,p_o.abortussen
                      ,'abortussen'
                      ,p_i.abortussen));
      addtext(verschil(p_n.apla
                      ,p_o.apla
                      ,'apla'
                      ,p_i.apla));
      addtext(verschil(p_n.eug
                      ,p_o.eug
                      ,'eug'
                      ,p_i.eug));
      addtext(verschil(p_n.hoeveelling
                      ,p_o.hoeveelling
                      ,'hoeveelling'
                      ,p_i.hoeveelling));
      addtext(verschil(p_n.mola
                      ,p_o.mola
                      ,'mola'
                      ,p_i.mola));
      addtext(verschil(p_n.para
                      ,p_o.para
                      ,'para'
                      ,p_i.para));
      addtext(verschil(p_n.progenituur
                      ,p_o.progenituur
                      ,'progenituur'
                      ,p_i.progenituur));
      RETURN v_verschil;
   END verschil_zwan;

   FUNCTION im2foet
   ( p_imfo        IN     kgc_im_foetussen%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_foet_old    IN     cg$kgc_foetussen.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_foet        IN OUT cg$kgc_foetussen.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      CURSOR c_impe_zoek(p_imfi_id NUMBER, p_impe_id NUMBER) IS
         SELECT kgc_pers_id
           FROM kgc_im_personen impe
          WHERE impe.imfi_id = p_imfi_id
            AND impe.impe_id = p_impe_id;

      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
      v_foet_volgnr              VARCHAR2(4000);
      v_foet_pers_id_geboren_als VARCHAR2(20);
   BEGIN
      -- vertaal im naar x_, en controleer
      x_foet.pers_id_moeder := TO_NUMBER(SUBSTR(get_im2hlx('IMPE' || p_imfo.impe_id_moeder), 5));
      x_foet.zwan_id := TO_NUMBER(SUBSTR(get_im2hlx('IMZW' || p_imfo.imzw_id), 5));
      v_foet_volgnr := SUBSTR(get_im2hlx('IMFO' || p_imfo.imfo_id || 'volgnr'), 5); -- deze wordt ook evt bepaald, en dus opgeslagen

      -- geplande_geboortedatum
      IF NOT typeok(p_imfo.geplande_geboortedatum
                   ,p_id || ' - Geplande geboortedatum'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_foet.geplande_geboortedatum := TO_DATE(v_db_waarde
                                                     ,v_datumformaatdoel);
      END IF;
      -- volgnr; hier niet verplicht - Mantis 5715:
      IF NOT typeok(NVL(p_imfo.volgnr, v_foet_volgnr)
                   ,p_id || ' - Volgnr'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         -- alleen toevoegen indien leeg (mogelijk al bepaald na de eerste aanroep)
         x_foet.volgnr := NVL(x_foet.volgnr, to_number(v_db_waarde));
         IF x_foet.volgnr IS NULL
         THEN
            IF x_foet.foet_id IS NULL
            THEN -- Mantis 5715: indien foetus-volgnr leeg, dan proberen te bepalen
               IF x_foet.pers_id_moeder IS NULL
               THEN -- moeder kon niet bepaald worden - dit wordt een nieuwe persoon, OF de foutmelding komt van elders!
                  x_foet.volgnr := 1;
                  show_output('foet.volgnr: default op 1 gezet');
               ELSE
                  DECLARE
                     CURSOR c_foet_per (p_pers_id NUMBER
                                       ,p_geplande_geboortedatum DATE
                                       ,p_commentaar VARCHAR2) IS
                        SELECT foet.foet_id
                        ,      foet.volgnr
                        FROM   kgc_foetussen foet
                        WHERE  foet.pers_id_moeder = p_pers_id
                        AND    p_geplande_geboortedatum BETWEEN ADD_MONTHS(foet.geplande_geboortedatum, -9) AND foet.geplande_geboortedatum + 14
                        AND    (p_commentaar IS NULL OR foet.commentaar IS NULL OR UPPER(REPLACE(p_commentaar, ' ')) = UPPER(REPLACE(foet.commentaar, ' ')))
                        ;
                     r_foet_per c_foet_per%ROWTYPE;
                     r_foet_per2 c_foet_per%ROWTYPE;
                     v_count   NUMBER := 0;
                  BEGIN
                     -- selecteer foetus in periode 9 mnd voor tm 2 weken na
                     OPEN c_foet_per(x_foet.pers_id_moeder
                                    ,x_foet.geplande_geboortedatum
                                    ,NULL);
                     FETCH c_foet_per INTO r_foet_per;
                     IF c_foet_per%NOTFOUND THEN -- nieuw volgnr uitgeven!
                        CLOSE c_foet_per;
                        SELECT NVL(MAX(foet.volgnr), 0) + 1
                        INTO   x_foet.volgnr
                        FROM   kgc_foetussen foet
                        WHERE  foet.pers_id_moeder = x_foet.pers_id_moeder;
                        show_output('foet.volgnr: max + 1 bepaald = ' || x_foet.volgnr);
                     ELSE -- alle doorlopen, nu met evt selectie op commentaar
                        CLOSE c_foet_per;
                        FOR r_foet_per IN c_foet_per(x_foet.pers_id_moeder
                                                    ,x_foet.geplande_geboortedatum
                                                    ,p_imfo.commentaar)
                        LOOP
                           v_count := v_count + 1;
                           r_foet_per2 := r_foet_per;
                        END LOOP;
                        IF v_count = 0 THEN
                           addtext(x_opmerkingen
                                  ,v_max_len
                                  ,'Bepaling foetus.volgnr: wel 1 of meerdere foetussen gevonden, maar geen met overeenkomstig commentaar; koppel handmatig!');
                           v_ok := FALSE;
                        ELSIF v_count = 1 THEN -- gewenste gevonden!
--                           v_foet_new.foet_id := r_foet_per2.foet_id; later obv volgnr
                           x_foet.volgnr := r_foet_per2.volgnr;
                           show_output('foet.volgnr: max + 1 bepaald = ' || x_foet.volgnr);
                           addtext(x_opmerkingen
                                  ,v_max_len
                                  ,'Bepaling foetus.volgnr: ' || x_foet.volgnr || ' (obv aanwezige foetussen)!');
                        ELSIF v_count > 1 THEN
                           addtext(x_opmerkingen
                                  ,v_max_len
                                  ,'Bepaling foetus.volgnr: ' || v_count || ' foetussen gevonden die voldoen; koppel handmatig!');
                           v_ok := FALSE;
                        END IF;
                     END IF;
                  END;
               END IF;
            END IF;

            IF x_foet.volgnr IS NOT NULL
            THEN -- opslaan!
                  set_im2hlx(p_im => 'IMFO' || p_imfo.imfo_id || 'volgnr', p_hlx => 'FOET' || x_foet.volgnr);
            END IF;
         END IF;


      END IF;

      -- niet_geboren
      IF NOT typeok(p_imfo.niet_geboren
                   ,p_id || ' - Niet geboren'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_foet.niet_geboren := v_db_waarde;
      END IF;
      -- geslacht; verplicht
      IF NOT refimpwok(p_imfo.impw_id_geslacht
                      ,p_id || ' - Geslacht'
                      ,TRUE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_foet.geslacht := v_db_waarde;
      END IF;
      -- geboren_als - persoon moet bestaan!
      -- deze moet al eerder zijn toegevoegd
      IF p_imfo.impe_id_geboren_als IS NOT NULL
      THEN
         -- ophalen
         v_foet_pers_id_geboren_als := NULL;
         OPEN c_impe_zoek(p_imfo.imfi_id
                         ,p_imfo.impe_id_geboren_als);
         FETCH c_impe_zoek
            INTO v_foet_pers_id_geboren_als;
         IF c_impe_zoek%NOTFOUND
         THEN
            -- mag niet: moet al eerder toegevoegd zijn!
            addtext(x_opmerkingen
                   ,v_max_len
                   ,'Systeemfout: kgc_im_foetussen met id = ' || to_char(p_imfo.imfi_id) || '/' || p_imfo.imfo_id ||
                    ' verwijst met geboren_als naar een niet bestaand record uit kgc_im_personen (impe_id = ' ||
                    p_imfo.impe_id_geboren_als || '!');
            v_ok := FALSE;
         ELSIF v_foet_pers_id_geboren_als IS NULL
         THEN
            addtext(x_opmerkingen
                   ,v_max_len
                   ,'Systeemfout: kgc_im_foetussen met id = ' || to_char(p_imfo.imfi_id) || '/' || p_imfo.imfo_id ||
                    ' verwijst met geboren_als naar een bestaand record uit kgc_im_personen (impe_id = ' ||
                    p_imfo.impe_id_geboren_als ||
                    '. Deze is echter nog niet aan een Helix-persoon gekoppeld!');
            v_ok := FALSE;
         ELSE
            x_foet.pers_id_geboren_als := v_foet_pers_id_geboren_als;
         END IF;
         CLOSE c_impe_zoek;
      END IF;
      -- commentaar
      IF NOT typeok(p_imfo.commentaar
                   ,p_id || ' - Commentaar'
                   ,'C'
                   ,1000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_foet.commentaar := v_db_waarde;
      END IF;
      -- impw_id_posi
      IF NOT refimpwok(p_imfo.impw_id_positie
                      ,p_id || ' - Positie'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_foet.posi_id := TO_NUMBER(v_db_waarde);
      END IF;

      RETURN v_ok;
   END im2foet;

   FUNCTION verschil_foet
   ( p_o cg$kgc_foetussen.cg$row_type
   , p_n cg$kgc_foetussen.cg$row_type
   , p_i IN OUT cg$kgc_foetussen.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      -- niet_geboren
      IF p_n.niet_geboren IS NULL THEN
         p_i.niet_geboren := FALSE; -- geen update
      ELSE
         addtext(verschil(p_n.niet_geboren
                         ,p_o.niet_geboren
                         ,'niet_geboren'
                         ,p_i.niet_geboren));
      END IF;
      -- geslacht
      IF p_n.geslacht IS NULL THEN
         p_i.geslacht := FALSE; -- geen update
      ELSE
        addtext(verschil(p_n.geslacht
                        ,p_o.geslacht
                        ,'geslacht'
                        ,p_i.geslacht));
      END IF;
      -- geplande_geboortedatum
      addtext(verschil(p_n.geplande_geboortedatum
                      ,p_o.geplande_geboortedatum
                      ,'geplande_geboortedatum'
                      ,p_i.geplande_geboortedatum));
      IF p_n.pers_id_geboren_als IS NOT NULL
      THEN -- blijkbaar kan dit niet teruggezet worden...
         -- anders: al gecontroleerd
         addtext(verschil(p_n.pers_id_geboren_als
                         ,p_o.pers_id_geboren_als
                         ,'pers_id_geboren_als'
                         ,p_i.pers_id_geboren_als));
      ELSE
         p_i.pers_id_geboren_als := FALSE; -- niet wijzigen!
      END IF;
      -- commentaar
      addtext(verschil(p_n.commentaar
                      ,p_o.commentaar
                      ,'commentaar'
                      ,p_i.commentaar));
      -- zwan-ref
      addtext(verschil(p_n.zwan_id
                      ,p_o.zwan_id
                      ,'zwan_id'
                      ,p_i.zwan_id));
      -- impw_id_posi
      addtext(verschil(p_n.posi_id
                      ,p_o.posi_id
                      ,'posi_id'
                      ,p_i.posi_id));

      RETURN v_verschil;
   END verschil_foet;

   FUNCTION im2mons
   ( p_immo        IN     kgc_im_monsters%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_mons_old    IN     cg$kgc_monsters.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , p_type        IN     VARCHAR2 -- KP = alleen gegevens tbv koppeling
   , x_mons        IN OUT cg$kgc_monsters.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      CURSOR c_imfo_zoek
      (p_imfi_id NUMBER, p_imfo_id VARCHAR2) IS
         SELECT kgc_foet_id
           FROM kgc_im_foetussen imfo
          WHERE imfo.imfi_id = p_imfi_id
            AND imfo.imfo_id = p_imfo_id;
      CURSOR c_btyp(p_btyp_id NUMBER) IS
         SELECT *
           FROM kgc_bestand_types btyp
          WHERE btyp.btyp_id = p_btyp_id;
      r_btyp c_btyp%ROWTYPE;

      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
      v_check_exists             VARCHAR2(1000);
      v_count                    NUMBER;
      v_verplicht                BOOLEAN;
   BEGIN
      -- vertaal im naar x_, en controleer
      x_mons.pers_id := TO_NUMBER(SUBSTR(get_im2hlx('IMPE' || p_immo.impe_id), 5));

      -- monsternummer
      IF NOT typeok(p_immo.monsternummer
                   ,p_id || ' - monsternummer'
                   ,'C'
                   ,20
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.monsternummer := v_db_waarde;
      END IF;
      -- onderzoeksgroep
      IF NOT refimpwok(p_immo.impw_id_ongr
                      ,p_id || ' - Onderzoeksgroep'
                      ,TRUE
                      ,TRUE
                      ,p_mons_old.ongr_id
                      ,g_cimpw_tab_ongr
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.ongr_id := TO_NUMBER(v_db_waarde); -- tbv test koppel hieronder
      END IF;
      -- Materiaal; verplicht indien niet koppel - Release 8.13.4
      v_verplicht := NOT (p_immo.monsternummer = '*KOPPEL*');
      IF NOT refimpwok(p_immo.impw_id_mate
                      ,p_id || ' - Materiaal'
                      ,v_verplicht
                      ,TRUE
                      ,p_mons_old.mate_id
                      ,g_cimpw_tab_mate
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.mate_id := TO_NUMBER(v_db_waarde); -- tbv test koppel hieronder
      END IF;
      
      IF p_immo.monsternummer = '*KOPPEL*' THEN -- Release 8.13.4 - niet verder controleren
         RETURN v_ok;
      END IF;
      -- datum afname
      IF NOT typeok(p_immo.datum_afname
                   ,p_id || ' - Datum afname'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.datum_afname := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- ckcl nummer
      IF NOT typeok(p_immo.ckcl_nummer
                   ,p_id || ' - CKCL Nummer'
                   ,'C'
                   ,10
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.ckcl_nummer := v_db_waarde;
      END IF;
      -- foetus
      IF p_immo.imfo_id IS NOT NULL
      THEN
         OPEN c_imfo_zoek(p_immo.imfi_id
                         ,p_immo.imfo_id);
         FETCH c_imfo_zoek
            INTO x_mons.foet_id;
         CLOSE c_imfo_zoek; -- geen check: altijd gevonden door FK
      END IF;

      IF p_type = 'KP'
      THEN -- *** genoeg gegevens voor koppelen?
         RETURN v_ok;
      END IF;

      -- afdeling; verplicht tenzij ongr aanwezig is
      IF NOT refimpwok(p_immo.impw_id_kafd
                      ,p_id || ' - Afdeling'
                      ,(p_immo.impw_id_ongr IS NULL)
                      ,TRUE
                      ,p_mons_old.kafd_id
                      ,g_cimpw_tab_kafd
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.kafd_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- prematuur; niet verplicht
      IF NOT typeok(p_immo.prematuur
                   ,p_id || ' - prematuur'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.prematuur := v_db_waarde;
      END IF;
      -- bewaren; niet verplicht
      IF NOT typeok(p_immo.bewaren
                   ,p_id || ' - Bewaren'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.bewaren := v_db_waarde;
      END IF;
      -- nader gebruiken; niet verplicht
      IF NOT typeok(p_immo.nader_gebruiken
                   ,p_id || ' - Nader gebruiken'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.nader_gebruiken := v_db_waarde;
      END IF;
      -- 4 kolommen overslaan (created_by, ...)
      -- datum aanmelding
      IF NOT typeok(p_immo.datum_aanmelding
                   ,p_id || ' - Datum aanmelding'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.datum_aanmelding := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- medewerker; vanaf hier niets verplicht
      IF NOT refimpwok(p_immo.impw_id_mede
                      ,p_id || ' - Medewerker'
                      ,FALSE
                      ,TRUE
                      ,p_mons_old.mede_id
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.mede_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- inzender
      IF NOT refimpwok(p_immo.impw_id_rela
                      ,p_id || ' - Inzender'
                      ,FALSE
                      ,TRUE
                      ,p_mons_old.rela_id
                      ,g_cimpw_tab_rela
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.rela_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- buffer
      IF NOT refimpwok(p_immo.impw_id_buff
                      ,p_id || ' - Buffer'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.buff_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- conditie
      IF NOT refimpwok(p_immo.impw_id_cond
                      ,p_id || ' - Conditie'
                      ,FALSE
                      ,TRUE
                      ,p_mons_old.cond_id
                      ,g_cimpw_tab_cond
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.cond_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- Leeftijd
      IF NOT refimpwok(p_immo.impw_id_leef
                      ,p_id || ' - Leeftijd'
                      ,FALSE
                      ,TRUE
                      ,p_mons_old.leef_id
                      ,g_cimpw_tab_leef
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.leef_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- leeftijd
      IF NOT typeok(p_immo.leeftijd
                   ,p_id || ' - leeftijd'
                   ,'N'
                   ,5
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.leeftijd := TO_NUMBER(v_db_waarde);
      END IF;
      -- Berging kort
      IF NOT refimpwok(p_immo.impw_id_bepl_kort
                      ,p_id || ' - Berging tijdens ondz'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.bepl_id_kort := TO_NUMBER(v_db_waarde);
      END IF;
      -- Berging lang
      IF NOT refimpwok(p_immo.impw_id_bepl_lang
                      ,p_id || ' - Berging na ondz'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.bepl_id_kort := TO_NUMBER(v_db_waarde);
      END IF;
      -- tijd afname
      IF NOT typeok(p_immo.tijd_afname
                   ,p_id || ' - Tijd afname'
                   ,'DT' -- RDE 20-02-2008
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.tijd_afname := TO_DATE(v_db_waarde, v_datumtijdformaatdoel);
      END IF;
      -- periode afname
      IF NOT typeok(p_immo.periode_afname
                   ,p_id || ' - Periode afname'
                   ,'C'
                   ,30
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.periode_afname := v_db_waarde;
      END IF;
      -- kweek nummer
      IF NOT typeok(p_immo.kweeknummer
                   ,p_id || ' - Kweek Nummer'
                   ,'C'
                   ,80
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.kweeknummer := v_db_waarde;
      END IF;
      -- aantal buizen
      IF NOT typeok(p_immo.aantal_buizen
                   ,p_id || ' - Aantal buizen'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.aantal_buizen := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum verwacht
      IF NOT typeok(p_immo.datum_verwacht
                   ,p_id || ' - Datum verwacht'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.datum_verwacht := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- hoeveelheid monster
      IF NOT typeok(p_immo.hoeveelheid_monster
                   ,p_id || ' - Hoeveelheid monster'
                   ,'N'
                   ,6
                   ,2
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.hoeveelheid_monster := TO_NUMBER(v_db_waarde);
      END IF;
      -- hoeveelheid buffer
      IF NOT typeok(p_immo.hoeveelheid_buffer
                   ,p_id || ' - Hoeveelheid buffer'
                   ,'N'
                   ,6
                   ,2
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.hoeveelheid_buffer := TO_NUMBER(v_db_waarde);
      END IF;
      -- medicatie
      IF NOT typeok(p_immo.medicatie
                   ,p_id || ' - Medicatie'
                   ,'C'
                   ,1000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.medicatie := v_db_waarde;
      END IF;
      -- voeding
      IF NOT typeok(p_immo.voeding
                   ,p_id || ' - Voeding'
                   ,'C'
                   ,1000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.voeding := v_db_waarde;
      END IF;
      -- medische indicatie
      IF NOT typeok(p_immo.medische_indicatie
                   ,p_id || ' - Medische indicatie'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.medische_indicatie := v_db_waarde;
      END IF;
      -- commentaar
      IF NOT typeok(p_immo.commentaar
                   ,p_id || ' - Commentaar'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.commentaar := v_db_waarde;
      END IF;

      -- declareren; niet verplicht
      IF NOT typeok(p_immo.declareren
                   ,p_id || ' - Declareren'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.declareren := v_db_waarde;
      END IF;
      -- herkomst
      IF NOT refimpwok(p_immo.impw_id_herk
                      ,p_id || ' - Herkomst'
                      ,FALSE
                      ,TRUE
                      ,p_mons_old.herk_id
                      ,g_cimpw_tab_herk
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_mons.herk_id := TO_NUMBER(v_db_waarde);
      END IF;

      -- controleer btyp
      IF p_immo.bestandsnaam IS NOT NULL
      THEN
         IF NOT refimpwok(p_immo.impw_id_btyp
                         ,p_id || ' - Bestandtype'
                         ,TRUE
                         ,v_db_waarde
                         ,x_opmerkingen)
         THEN
            v_ok := FALSE;
         ELSE -- controleer type en bestand
            OPEN c_btyp(p_btyp_id => TO_NUMBER(v_db_waarde));
            FETCH c_btyp INTO r_btyp;
            CLOSE c_btyp;
            IF NVL(r_btyp.enti_code, 'X') <> 'MONS'
            THEN
               addtext(x_opmerkingen
                      ,v_max_len
                      ,p_id || ' - bestandstype hoort niet bij entiteit MONS, maar ' || r_btyp.enti_code  || '!');
               v_ok := FALSE;
/*            ELSE -- bestaat bestand?
               SELECT COUNT(*)
                 INTO v_count
                 FROM kgc_bestanden best
                WHERE best.entiteit_code = 'MONS'
                  AND best.entiteit_pk = x_mons.mons_id
                  AND best.btyp_id = r_btyp.btyp_id;
               IF x_mons.mons_id IS NULL OR v_count = 0
               THEN -- controleren, want: nieuw of gekoppeld en nog niet verplaatst
                  v_check_exists := kgc_interface_generiek.FileExists(p_file  => RTRIM(r_btyp.standaard_pad, '\') || '\' || p_immo.bestandsnaam);
                  IF v_check_exists != 'OK'
                  THEN
                     addtext(x_opmerkingen
                            ,v_max_len
                            ,p_id || ' - bestand ' || p_immo.bestandsnaam || ' bestaat niet in map ' || r_btyp.standaard_pad  || '!');
                     v_ok := FALSE;
                  END IF;
               END IF;*/
            END IF;
         END IF;
      END IF;

      RETURN v_ok;
   END im2mons;

   FUNCTION verschil_mons
   ( p_o cg$kgc_monsters.cg$row_type
   , p_n cg$kgc_monsters.cg$row_type
   , p_i IN OUT cg$kgc_monsters.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      addtext(verschil(p_n.ongr_id
                      ,p_o.ongr_id
                      ,'ongr_id'
                      ,p_i.ongr_id));
      IF p_n.monsternummer IS NOT NULL
      THEN -- niet leegmaken!
         addtext(verschil(p_n.monsternummer
                         ,p_o.monsternummer
                         ,'monsternummer'
                         ,p_i.monsternummer));
      END IF;
      addtext(verschil(p_n.kafd_id
                      ,p_o.kafd_id
                      ,'kafd_id'
                      ,p_i.kafd_id));
      addtext(verschil(p_n.foet_id
                      ,p_o.foet_id
                      ,'foet_id'
                      ,p_i.foet_id));
      addtext(verschil(p_n.mate_id
                      ,p_o.mate_id
                      ,'mate_id'
                      ,p_i.mate_id));
      IF p_n.prematuur IS NULL
      THEN
         p_i.prematuur := FALSE;
      ELSE
         addtext(verschil(p_n.prematuur
                         ,p_o.prematuur
                         ,'prematuur'
                         ,p_i.prematuur));
      END IF;
      IF p_n.bewaren IS NULL
      THEN
         p_i.bewaren := FALSE;
      ELSE
         addtext(verschil(p_n.bewaren
                         ,p_o.bewaren
                         ,'bewaren'
                         ,p_i.bewaren));
      END IF;
      IF p_n.nader_gebruiken IS NULL
      THEN
         p_i.nader_gebruiken := FALSE;
      ELSE
         addtext(verschil(p_n.nader_gebruiken
                         ,p_o.nader_gebruiken
                         ,'nader_gebruiken'
                         ,p_i.nader_gebruiken));
      END IF;
      IF p_n.datum_aanmelding IS NULL
      THEN
         p_i.datum_aanmelding := FALSE;
      ELSE
         addtext(verschil(p_n.datum_aanmelding
                         ,p_o.datum_aanmelding
                         ,'datum_aanmelding'
                         ,p_i.datum_aanmelding));
      END IF;
      addtext(verschil(p_n.mede_id
                      ,p_o.mede_id
                      ,'mede_id'
                      ,p_i.mede_id));
      addtext(verschil(p_n.rela_id
                      ,p_o.rela_id
                      ,'rela_id'
                      ,p_i.rela_id));
      addtext(verschil(p_n.buff_id
                      ,p_o.buff_id
                      ,'buff_id'
                      ,p_i.buff_id));
      addtext(verschil(p_n.cond_id
                      ,p_o.cond_id
                      ,'cond_id'
                      ,p_i.cond_id));
      addtext(verschil(p_n.leef_id
                      ,p_o.leef_id
                      ,'leef_id'
                      ,p_i.leef_id));
      addtext(verschil(p_n.leeftijd
                      ,p_o.leeftijd
                      ,'leeftijd'
                      ,p_i.leeftijd));
      addtext(verschil(p_n.bepl_id_kort
                      ,p_o.bepl_id_kort
                      ,'bepl_id_kort'
                      ,p_i.bepl_id_kort));
      addtext(verschil(p_n.bepl_id_lang
                      ,p_o.bepl_id_lang
                      ,'bepl_id_lang'
                      ,p_i.bepl_id_lang));
      addtext(verschil(p_n.datum_afname
                      ,p_o.datum_afname
                      ,'datum_afname'
                      ,p_i.datum_afname));
      addtext(verschil(TO_CHAR(p_n.tijd_afname
                              ,v_datumtijdformaatdoel)
                      ,TO_CHAR(p_o.tijd_afname
                              ,v_datumtijdformaatdoel)
                     ,'tijd_afname'
                      ,p_i.tijd_afname));
      addtext(verschil(p_n.periode_afname
                      ,p_o.periode_afname
                      ,'periode_afname'
                      ,p_i.periode_afname));
      addtext(verschil(p_n.ckcl_nummer
                      ,p_o.ckcl_nummer
                      ,'ckcl_nummer'
                      ,p_i.ckcl_nummer));
      addtext(verschil(p_n.kweeknummer
                      ,p_o.kweeknummer
                      ,'kweeknummer'
                      ,p_i.kweeknummer));
      addtext(verschil(p_n.aantal_buizen
                      ,p_o.aantal_buizen
                      ,'aantal_buizen'
                      ,p_i.aantal_buizen));
      addtext(verschil(p_n.datum_verwacht
                      ,p_o.datum_verwacht
                      ,'datum_verwacht'
                      ,p_i.datum_verwacht));
      addtext(verschil(p_n.hoeveelheid_monster
                      ,p_o.hoeveelheid_monster
                      ,'hoeveelheid_monster'
                      ,p_i.hoeveelheid_monster));
      addtext(verschil(p_n.hoeveelheid_buffer
                      ,p_o.hoeveelheid_buffer
                      ,'hoeveelheid_buffer'
                      ,p_i.hoeveelheid_buffer));
      addtext(verschil(p_n.medicatie
                      ,p_o.medicatie
                      ,'medicatie'
                      ,p_i.medicatie));
      addtext(verschil(p_n.voeding
                      ,p_o.voeding
                      ,'voeding'
                      ,p_i.voeding));
      addtext(verschil(p_n.medische_indicatie
                      ,p_o.medische_indicatie
                      ,'medische_indicatie'
                      ,p_i.medische_indicatie));
      addtext(verschil(p_n.commentaar
                      ,p_o.commentaar
                      ,'commentaar'
                      ,p_i.commentaar));
      IF p_n.declareren IS NULL
      THEN
         p_i.declareren := FALSE;
      ELSE
         addtext(verschil(p_n.declareren
                         ,p_o.declareren
                         ,'declareren'
                         ,p_i.declareren));
      END IF;
      addtext(verschil(p_n.herk_id
                      ,p_o.herk_id
                      ,'herk_id'
                      ,p_i.herk_id));

      RETURN v_verschil;
   END verschil_mons;

   FUNCTION im2frac
   ( p_imfr        IN     kgc_im_fracties%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_frac_old    IN     cg$bas_fracties.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_frac        IN OUT cg$bas_fracties.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal im naar x_, en controleer
      x_frac.mons_id := TO_NUMBER(SUBSTR(get_im2hlx('IMMO' || p_imfr.immo_id), 5));

      -- andere waarden
      -- fractienummer
      IF NOT typeok(p_imfr.fractienummer
                   ,p_id || ' - fractienummer'
                   ,'C'
                   ,20
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.fractienummer := v_db_waarde;
      END IF;

      -- controle; niet verplicht
      IF NOT typeok(p_imfr.controle
                   ,p_id || ' - controle'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.controle := v_db_waarde;
      END IF;
      -- afdeling
      IF NOT refimpwok(p_imfr.impw_id_kafd
                      ,p_id || ' - Afdeling'
                      ,TRUE
                      ,TRUE
                      ,p_frac_old.kafd_id
                      ,g_cimpw_tab_kafd
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.kafd_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- status
      IF NOT refimpwok(p_imfr.impw_id_status
                      ,p_id || ' - Status'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.status := v_db_waarde;
      END IF;
      -- fractietype
      IF NOT refimpwok(p_imfr.impw_id_frty
                      ,p_id || ' - Fractietype'
                      ,FALSE
                      ,TRUE
                      ,p_frac_old.frty_id
                      ,g_cimpw_tab_frty
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.frty_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- opwerken
      IF NOT refimpwok(p_imfr.impw_id_opwe
                      ,p_id || ' - Opwerken'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.opwe_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- conditie
      IF NOT refimpwok(p_imfr.impw_id_cond
                      ,p_id || ' - Conditie'
                      ,FALSE
                      ,TRUE
                      ,p_frac_old.cond_id
                      ,g_cimpw_tab_cond
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.cond_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- type afleiding
      IF NOT refimpwok(p_imfr.impw_id_type_afleiding
                      ,p_id || ' - Type afleiding'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.type_afleiding := v_db_waarde;
      END IF;
      -- eenheid
      IF NOT refimpwok(p_imfr.impw_id_eenh
                      ,p_id || ' - Eenheid'
                      ,FALSE
                      ,TRUE
                      ,p_frac_old.eenh_id
                      ,g_cimpw_tab_eenh
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.eenh_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- commentaar
      IF NOT typeok(p_imfr.commentaar
                   ,p_id || ' - Comentaar'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.commentaar := v_db_waarde;
      END IF;
      -- concentratie
      IF NOT typeok(p_imfr.concentratie
                   ,p_id || ' - Concentratie'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.concentratie := v_db_waarde;
      END IF;
      -- datum_concentratie_bepaling
      IF NOT typeok(p_imfr.datum_concentratie_bepaling
                   ,p_id || ' - Datum concentratie bepaling'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_frac.datum_concentratie_bepaling := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- extra1
      IF NOT typeok(p_imfr.extra1
                   ,p_id || ' - Extra1'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         NULL; -- nog geen doel voorzien
      END IF;

      RETURN v_ok;
   END im2frac;

   FUNCTION verschil_frac
   ( p_o cg$bas_fracties.cg$row_type
   , p_n cg$bas_fracties.cg$row_type
   , p_i IN OUT cg$bas_fracties.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      -- fractienummer
      IF p_n.fractienummer IS NOT NULL
      THEN -- niet leegmaken!
         addtext(verschil(p_n.fractienummer
                         ,p_o.fractienummer
                         ,'fractienummer'
                         ,p_i.fractienummer));
      END IF;
      IF p_n.controle IS NULL
      THEN
         p_i.controle := FALSE;
      ELSE
         addtext(verschil(p_n.controle
                         ,p_o.controle
                         ,'controle'
                         ,p_i.controle));
      END IF;
      addtext(verschil(p_n.kafd_id
                      ,p_o.kafd_id
                      ,'kafd_id'
                      ,p_i.kafd_id));
      addtext(verschil(p_n.status
                      ,p_o.status
                      ,'status'
                      ,p_i.status));
      addtext(verschil(p_n.frty_id
                      ,p_o.frty_id
                      ,'frty_id'
                      ,p_i.frty_id));
      addtext(verschil(p_n.opwe_id
                      ,p_o.opwe_id
                      ,'opwe_id'
                      ,p_i.opwe_id));
      addtext(verschil(p_n.cond_id
                      ,p_o.cond_id
                      ,'cond_id'
                      ,p_i.cond_id));
      addtext(verschil(p_n.type_afleiding
                      ,p_o.type_afleiding
                      ,'type_afleiding'
                      ,p_i.type_afleiding));
      addtext(verschil(p_n.eenh_id
                      ,p_o.eenh_id
                      ,'eenh_id'
                      ,p_i.eenh_id));
      addtext(verschil(p_n.commentaar
                      ,p_o.commentaar
                      ,'commentaar'
                      ,p_i.commentaar));
      addtext(verschil(p_n.concentratie
                      ,p_o.concentratie
                      ,'concentratie'
                      ,p_i.concentratie));
      addtext(verschil(p_n.datum_concentratie_bepaling
                      ,p_o.datum_concentratie_bepaling
                      ,'datum_concentratie_bepaling'
                      ,p_i.datum_concentratie_bepaling));

      RETURN v_verschil;
   END verschil_frac;

   FUNCTION im2onde
   ( p_imon        IN     kgc_im_onderzoeken%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_onde_old    IN     cg$kgc_onderzoeken.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_onde        IN OUT cg$kgc_onderzoeken.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      CURSOR c_ongr(p_ongr_id NUMBER) IS
         SELECT ongr.kafd_id
              , ongr.code
           FROM kgc_onderzoeksgroepen ongr
          WHERE ongr.ongr_id = p_ongr_id;
      r_ongr c_ongr%ROWTYPE;

      CURSOR c_imfo_zoek -- tbv onde
      (p_imfi_id NUMBER, p_imfo_id VARCHAR2) IS
         SELECT kgc_foet_id
           FROM kgc_im_foetussen imfo
          WHERE imfo.imfi_id = p_imfi_id
            AND imfo.imfo_id = p_imfo_id;

      CURSOR c_onwy(p_onwy_id NUMBER) IS
         SELECT code
           FROM kgc_onderzoekswijzen
          WHERE onwy_id = p_onwy_id;

      CURSOR c_mede_auto(p_kafd_id NUMBER, p_mede_id NUMBER) IS
         SELECT NULL
           FROM kgc_autorisatoren_vw
          WHERE kafd_id = p_kafd_id
            AND mede_id = p_mede_id
            AND vervallen = 'N';
      CURSOR c_btyp(p_btyp_id NUMBER) IS
         SELECT *
           FROM kgc_bestand_types btyp
          WHERE btyp.btyp_id = p_btyp_id;
      r_btyp c_btyp%ROWTYPE;

      v_ok                       BOOLEAN := TRUE;
      v_bverplicht               BOOLEAN;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
      v_check_exists             VARCHAR2(1000);
      v_count                    NUMBER;
   BEGIN
      -- vertaal im naar x_, en controleer
      x_onde.pers_id := TO_NUMBER(SUBSTR(get_im2hlx('IMPE' || p_imon.impe_id), 5));

      -- onderzoeksgroep; verplicht
      IF NOT refimpwok(p_imon.impw_id_ongr
                      ,p_id || ' - Onderzoeksgroep'
                      ,TRUE
                      ,TRUE
                      ,p_onde_old.ongr_id
                      ,g_cimpw_tab_ongr
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.ongr_id := to_number(v_db_waarde); -- nodig voor nummer-gen
         -- bepaal kafd
         OPEN c_ongr(p_ongr_id => x_onde.ongr_id);
         FETCH c_ongr INTO r_ongr;
         CLOSE c_ongr;
      END IF;
      -- afdeling; verplicht tenzij ongr aanwezig is
      v_bverplicht := TRUE;
      IF x_onde.ongr_id IS NOT NULL
      THEN
         v_bverplicht := FALSE;
      END IF;
      IF NOT refimpwok(p_imon.impw_id_kafd
                      ,p_id || ' - Afdeling'
                      ,v_bverplicht
                      ,TRUE
                      ,p_onde_old.kafd_id
                      ,g_cimpw_tab_kafd
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.kafd_id := NVL(to_number(v_db_waarde), r_ongr.kafd_id);
      END IF;

      -- datum binnen
      IF NOT typeok(p_imon.datum_binnen
                   ,p_id || ' - Datum binnen'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.datum_binnen := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;

      -- foetus
      IF p_imon.imfo_id IS NOT NULL
      THEN
         -- foetus: evt net toegevoegd
         OPEN c_imfo_zoek(p_imon.imfi_id
                         ,p_imon.imfo_id);
         FETCH c_imfo_zoek
            INTO x_onde.foet_id;
         CLOSE c_imfo_zoek; -- notfound: kan niet: is FK.
      END IF;
      -- onderzoekswijze
      IF NOT refimpwok(p_imon.impw_id_onderzoekswijze
                      ,p_id || ' - Onderzoekswijze'
                      ,FALSE
                      ,TRUE
                      ,NULL -- p_onde_old.onwy_id bestaat niet, onderzoekswijze (code) wel
                      ,g_cimpw_tab_onwy
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         OPEN c_onwy(to_number(v_db_waarde));
         FETCH c_onwy
            INTO x_onde.onderzoekswijze;
         CLOSE c_onwy;
      END IF;
      -- let op: soms MOET er een onderzoekswijze zijn. B.v. igv OBS...
      IF x_onde.onderzoekswijze IS NULL
         AND x_onde.kafd_id = kgc_uk2pk.kafd_id('OBS')
      THEN
         addtext(x_opmerkingen
                ,v_max_len
                ,p_id ||
                 ' - De onderzoekswijze bij het onderzoek is verplicht! (zijn de indicaties aanwezig? Nee? Dan in Astraia toevoegen en opnieuw valideren!)');
         v_ok := FALSE;
      END IF;
      -- referentie
      IF NOT typeok(p_imon.referentie
                   ,p_id || ' - Referentie'
                   ,'C'
                   ,100
                   ,0
                   ,FALSE
                   ,x_onde.referentie
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;

      -- onderzoekstype; niet verplicht (er is een default)
      IF NOT refimpwok(p_imon.impw_id_onderzoekstype
                      ,p_id || ' - Onderzoekstype'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.onderzoekstype := v_db_waarde;
      END IF;
      -- onderzoeknr; niet verplicht
      IF NOT typeok(p_imon.onderzoeknr
                   ,p_id || ' - Onderzoeknr'
                   ,'C'
                   ,20
                   ,0
                   ,FALSE
                   ,x_onde.onderzoeknr
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- aanvrager; verplicht
      IF NOT refimpwok(p_imon.impw_id_rela
                      ,p_id || ' - Aanvrager'
                      ,TRUE
                      ,TRUE
                      ,p_onde_old.rela_id
                      ,g_cimpw_tab_rela
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.rela_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- spoed; niet verplicht
      IF NOT typeok(p_imon.spoed
                   ,p_id || ' - spoed'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_onde.spoed
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- declareren; niet verplicht
      IF NOT typeok(p_imon.declareren
                   ,p_id || ' - declareren'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_onde.declareren
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- afgerond; niet verplicht
      IF NOT typeok(p_imon.afgerond
                   ,p_id || ' - afgerond'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,x_onde.afgerond -- wordt hier nog niets mee gedaan
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- geplande einddatum
      IF NOT typeok(p_imon.geplande_einddatum
                   ,p_id || ' - geplande einddatum'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.geplande_einddatum := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- herkomst
      IF NOT refimpwok(p_imon.impw_id_herk
                      ,p_id || ' - Herkomst'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.herk_id
                      ,g_cimpw_tab_herk
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.herk_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- instelling
      IF NOT refimpwok(p_imon.impw_id_inst
                      ,p_id || ' - Patientstatus in'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.inst_id
                      ,g_cimpw_tab_inst
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.inst_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- status
      IF NOT refimpwok(p_imon.impw_id_status
                      ,p_id || ' - Onderzoekstatus'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.status
                      ,g_cimpw_dom_onde_status
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
        x_onde.status := v_db_waarde;
      END IF;
      -- beoordelaar
      IF NOT refimpwok(p_imon.impw_id_beoordelaar
                      ,p_id || ' - Beoordelaar'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.mede_id_beoordelaar
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.mede_id_beoordelaar := TO_NUMBER(v_db_waarde);
      END IF;
      -- autorisator
      IF NOT refimpwok(p_imon.impw_id_autorisator
                      ,p_id || ' - Autorisator'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.mede_id_autorisator
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.mede_id_autorisator := TO_NUMBER(v_db_waarde);
         IF x_onde.mede_id_autorisator IS NOT NULL
         THEN
            -- controle of de medewerker ook mag autoriseren
            OPEN c_mede_auto(x_onde.kafd_id
                            ,x_onde.mede_id_autorisator);
            FETCH c_mede_auto
               INTO v_db_waarde;
            IF c_mede_auto%NOTFOUND
            THEN
               addtext(x_opmerkingen
                      ,v_max_len
                      ,p_id ||
                       ' - Autorisator is geen medewerker die onderzoeken mag autoriseren!');
               v_ok := FALSE;
            END IF;
            CLOSE c_mede_auto;
         END IF;
      END IF;
      -- Controle
      IF NOT refimpwok(p_imon.impw_id_controle
                      ,p_id || ' - Controle'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.mede_id_controle
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.mede_id_controle := TO_NUMBER(v_db_waarde);
      END IF;
      -- Dia autorisator
      IF NOT refimpwok(p_imon.impw_id_dia_autorisator
                      ,p_id || ' - Diagnose autorisator'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.mede_id_dia_autorisator
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.mede_id_dia_autorisator := TO_NUMBER(v_db_waarde);
      END IF;
      -- onui
      IF NOT refimpwok(p_imon.impw_id_onui
                      ,p_id || ' - Onderzoeksuitslag'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.onui_id
                      ,g_cimpw_tab_onui
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.onui_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- coty
      IF NOT refimpwok(p_imon.impw_id_coty
                      ,p_id || ' - Uitslagcode'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.coty_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- diagnose code
      IF NOT refimpwok(p_imon.impw_id_diagnose_code
                      ,p_id || ' - Diagnose code'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.diagnose_code := v_db_waarde;
      END IF;
      -- Erfmodus
      IF NOT refimpwok(p_imon.impw_id_ermo
                      ,p_id || ' - Erfmodus'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.ermo_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- Diagnose status
      IF NOT refimpwok(p_imon.impw_id_dist
                      ,p_id || ' - Diagnose status'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.dist_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- Diagnose herkomst
      IF NOT refimpwok(p_imon.impw_id_herk_dia
                      ,p_id || ' - Diagnose herkomst'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.herk_id_dia := TO_NUMBER(v_db_waarde);
      END IF;
      -- Taal
      IF NOT refimpwok(p_imon.impw_id_taal
                      ,p_id || ' - Taal uitslag'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.taal_id
                      ,g_cimpw_tab_taal
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.taal_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- verzekeraar
      IF NOT refimpwok(p_imon.impw_id_verz
                      ,p_id || ' - Verzekeraar'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.verz_id
                      ,g_cimpw_tab_verz
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.verz_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- Project
      IF NOT refimpwok(p_imon.impw_id_proj
                      ,p_id || ' - Project'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.proj_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- Familie relatie
      IF NOT refimpwok(p_imon.impw_id_fare
                      ,p_id || ' - Familie relatie'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.fare_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- Declaratie wijze
      IF NOT refimpwok(p_imon.impw_id_dewy
                      ,p_id || ' - Declaratie wijze'
                      ,FALSE
                      ,TRUE
                      ,p_onde_old.dewy_id
                      ,g_cimpw_tab_dewy
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.dewy_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- Zekerheid diagnose
      IF NOT refimpwok(p_imon.impw_id_zekerheid_diagnose
                      ,p_id || ' - Zekerheid diagnose'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.zekerheid_diagnose := v_db_waarde;
      END IF;
      -- Zekerheid status
      IF NOT refimpwok(p_imon.impw_id_zekerheid_status
                      ,p_id || ' - Zekerheid status'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.zekerheid_status := v_db_waarde;
      END IF;
      -- Zekerheid erfmodus
      IF NOT refimpwok(p_imon.impw_id_zekerheid_erfmodus
                      ,p_id || ' - Zekerheid erfmodus'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.zekerheid_erfmodus := v_db_waarde;
      END IF;
      -- datum autorisatie
      IF NOT typeok(p_imon.datum_autorisatie
                   ,p_id || ' - Datum autorisatie'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onde.datum_autorisatie := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- kariotypering
      IF NOT typeok(p_imon.kariotypering
                   ,p_id || ' - Kariotypering'
                   ,'C'
                   ,200
                   ,0
                   ,FALSE
                   ,x_onde.kariotypering
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- Nota adres
      IF NOT typeok(p_imon.nota_adres
                   ,p_id || ' - Nota adres'
                   ,'C'
                   ,250
                   ,0
                   ,FALSE
                   ,x_onde.nota_adres
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- omschrijving
      IF NOT typeok(p_imon.omschrijving
                   ,p_id || ' - Omschrijving'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,x_onde.omschrijving
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;

      -- controleer btyp
      IF p_imon.bestandsnaam IS NOT NULL
      THEN
         IF NOT refimpwok(p_imon.impw_id_btyp
                         ,p_id || ' - Bestandtype'
                         ,TRUE
                         ,v_db_waarde
                         ,x_opmerkingen)
         THEN
            v_ok := FALSE;
         ELSE -- controleer type en bestand
            OPEN c_btyp(p_btyp_id => TO_NUMBER(v_db_waarde));
            FETCH c_btyp INTO r_btyp;
            CLOSE c_btyp;
            IF NVL(r_btyp.enti_code, 'X') <> 'ONDE'
            THEN
               addtext(x_opmerkingen
                      ,v_max_len
                      ,p_id || ' - bestandstype hoort niet bij entiteit ONDE, maar ' || r_btyp.enti_code  || '!');
               v_ok := FALSE;
/*            ELSE -- bestaat bestand?
               SELECT COUNT(*)
                 INTO v_count
                 FROM kgc_bestanden best
                WHERE best.entiteit_code = 'ONDE'
                  AND best.entiteit_pk = x_onde.onde_id
                  AND best.btyp_id = r_btyp.btyp_id;

               IF x_onde.onde_id IS NULL OR v_count = 0
               THEN -- controleren, want: nieuw of gekoppeld en nog niet verplaatst
                  v_check_exists := kgc_interface_generiek.FileExists(p_file  => RTRIM(r_btyp.standaard_pad, '\') || '\' || p_imon.bestandsnaam);
                  IF v_check_exists != 'OK'
                  THEN
                     addtext(x_opmerkingen
                            ,v_max_len
                            ,p_id || ' - bestand ' || p_imon.bestandsnaam || ' bestaat niet in map ' || r_btyp.standaard_pad  || '!');
                     v_ok := FALSE;
                  END IF;
               END IF;*/
            END IF;
         END IF;
      END IF;

      RETURN v_ok;
   END im2onde;

   FUNCTION verschil_onde
   ( p_o cg$kgc_onderzoeken.cg$row_type
   , p_n cg$kgc_onderzoeken.cg$row_type
   , p_i IN OUT cg$kgc_onderzoeken.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      IF p_n.onde_id IS NULL
      THEN -- niet bijwerken!
         addtext(verschil(p_n.ongr_id
                         ,p_o.ongr_id
                         ,'ongr_id'
                         ,p_i.ongr_id));
         -- kafd_id toevoegen tbv trigger cg$BIR_KGC_ONDERZOEKEN
         addtext(verschil(p_n.kafd_id
                         ,p_o.kafd_id
                         ,'kafd_id'
                         ,p_i.kafd_id));
         IF p_n.onderzoekstype IS NULL
         THEN
            p_i.onderzoekstype := FALSE;
         ELSE
            addtext(verschil(p_n.onderzoekstype
                            ,p_o.onderzoekstype
                            ,'onderzoekstype'
                            ,p_i.onderzoekstype));
         END IF;
         addtext(verschil(p_n.onderzoeknr
                         ,p_o.onderzoeknr
                         ,'onderzoeknr'
                         ,p_i.onderzoeknr));
         IF p_n.foet_id IS NOT NULL
         THEN
            addtext(verschil(p_n.foet_id
                            ,p_o.foet_id
                            ,'foet_id'
                            ,p_i.foet_id));
         END IF;
      END IF;
      -- aanvrager
      addtext(verschil(p_n.rela_id
                      ,p_o.rela_id
                      ,'rela_id'
                      ,p_i.rela_id));
      -- spoed
      IF p_n.spoed IS NULL
      THEN
         p_i.spoed := FALSE;
      ELSE
         addtext(verschil(p_n.spoed
                         ,p_o.spoed
                         ,'spoed'
                         ,p_i.spoed));
      END IF;
      -- declareren
      IF p_n.declareren IS NULL
      THEN
         p_i.declareren := FALSE;
      ELSE
         addtext(verschil(p_n.declareren
                         ,p_o.declareren
                         ,'declareren'
                         ,p_i.declareren));
      END IF;
      IF p_n.datum_binnen IS NULL
      THEN
         p_i.datum_binnen := FALSE;
      ELSE
         addtext(verschil(p_n.datum_binnen
                         ,p_o.datum_binnen
                         ,'datum_binnen'
                         ,p_i.datum_binnen));
      END IF;
      addtext(verschil(p_n.geplande_einddatum
                      ,p_o.geplande_einddatum
                      ,'geplande_einddatum'
                      ,p_i.geplande_einddatum));
      addtext(verschil(p_n.herk_id
                      ,p_o.herk_id
                      ,'herk_id'
                      ,p_i.herk_id));
      addtext(verschil(p_n.inst_id
                      ,p_o.inst_id
                      ,'inst_id'
                      ,p_i.inst_id));
      -- onderzoekswijze
      addtext(verschil(p_n.onderzoekswijze
                      ,p_o.onderzoekswijze
                      ,'onderzoekswijze'
                      ,p_i.onderzoekswijze));
      addtext(verschil(p_n.status
                      ,p_o.status
                      ,'status'
                      ,p_i.status));
      addtext(verschil(p_n.mede_id_beoordelaar
                      ,p_o.mede_id_beoordelaar
                      ,'mede_id_beoordelaar'
                      ,p_i.mede_id_beoordelaar));
      addtext(verschil(p_n.mede_id_autorisator
                      ,p_o.mede_id_autorisator
                      ,'mede_id_autorisator'
                      ,p_i.mede_id_autorisator));
      addtext(verschil(p_n.mede_id_controle
                      ,p_o.mede_id_controle
                      ,'mede_id_controle'
                      ,p_i.mede_id_controle));
      addtext(verschil(p_n.mede_id_dia_autorisator
                      ,p_o.mede_id_dia_autorisator
                      ,'mede_id_dia_autorisator'
                      ,p_i.mede_id_dia_autorisator));
      addtext(verschil(p_n.onui_id
                      ,p_o.onui_id
                      ,'onui_id'
                      ,p_i.onui_id));
      addtext(verschil(p_n.coty_id
                      ,p_o.coty_id
                      ,'coty_id'
                      ,p_i.coty_id));
      addtext(verschil(p_n.diagnose_code
                      ,p_o.diagnose_code
                      ,'diagnose_code'
                      ,p_i.diagnose_code));
      addtext(verschil(p_n.ermo_id
                      ,p_o.ermo_id
                      ,'ermo_id'
                      ,p_i.ermo_id));
      addtext(verschil(p_n.dist_id
                      ,p_o.dist_id
                      ,'dist_id'
                      ,p_i.dist_id));
      addtext(verschil(p_n.herk_id_dia
                      ,p_o.herk_id_dia
                      ,'herk_id_dia'
                      ,p_i.herk_id_dia));
      addtext(verschil(p_n.taal_id
                      ,p_o.taal_id
                      ,'taal_id'
                      ,p_i.taal_id));
      addtext(verschil(p_n.verz_id
                      ,p_o.verz_id
                      ,'verz_id'
                      ,p_i.verz_id));
      addtext(verschil(p_n.proj_id
                      ,p_o.proj_id
                      ,'proj_id'
                      ,p_i.proj_id));
      addtext(verschil(p_n.fare_id
                      ,p_o.fare_id
                      ,'fare_id'
                      ,p_i.fare_id));
      -- Declaratie wijze: default gaat via insert-trigger met kafd_id!!!
      addtext(verschil(p_n.dewy_id
                      ,p_o.dewy_id
                      ,'dewy_id'
                      ,p_i.dewy_id));
      addtext(verschil(p_n.zekerheid_diagnose
                      ,p_o.zekerheid_diagnose
                      ,'zekerheid_diagnose'
                      ,p_i.zekerheid_diagnose));
      addtext(verschil(p_n.zekerheid_status
                      ,p_o.zekerheid_status
                      ,'zekerheid_status'
                      ,p_i.zekerheid_status));
      addtext(verschil(p_n.zekerheid_erfmodus
                      ,p_o.zekerheid_erfmodus
                      ,'zekerheid_erfmodus'
                      ,p_i.zekerheid_erfmodus));
      addtext(verschil(p_n.datum_autorisatie
                      ,p_o.datum_autorisatie
                      ,'datum_autorisatie'
                      ,p_i.datum_autorisatie));
      addtext(verschil(p_n.kariotypering
                      ,p_o.kariotypering
                      ,'kariotypering'
                      ,p_i.kariotypering));
      addtext(verschil(p_n.referentie
                      ,p_o.referentie
                      ,'referentie'
                      ,p_i.referentie));
      addtext(verschil(p_n.nota_adres
                      ,p_o.nota_adres
                      ,'nota_adres'
                      ,p_i.nota_adres));
      addtext(verschil(p_n.omschrijving
                      ,p_o.omschrijving
                      ,'omschrijving'
                      ,p_i.omschrijving));
      -- afgerond wordt aan het einde van de loop gezet, maar het verschil moet al wel direct duidelijk zijn
      addtext(verschil(p_n.afgerond
                      ,p_o.afgerond
                      ,'afgerond'
                      ,p_i.afgerond));

      RETURN v_verschil;
   END verschil_onde;

   FUNCTION im2onin
   ( p_imoi        IN     kgc_im_onderzoek_indicaties%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_onin_old    IN     cg$kgc_onderzoek_indicaties.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_onin        IN OUT cg$kgc_onderzoek_indicaties.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal im naar x_, en controleer
      x_onin.onde_id := TO_NUMBER(SUBSTR(get_im2hlx('IMON' || p_imoi.imon_id), 5));

      IF NOT refimpwok(p_imoi.impw_id_indi
                      ,p_id || ' - Indicatie'
                      ,FALSE
                      ,TRUE
                      ,p_onin_old.indi_id
                      ,g_cimpw_tab_indi
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onin.indi_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- volgorde; verplicht
      IF NOT typeok(p_imoi.volgorde
                   ,p_id || ' - Volgorde'
                   ,'N'
                   ,2
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onin.volgorde := TO_NUMBER(v_db_waarde);
      END IF;

      -- Indicatiegroep
      IF NOT refimpwok(p_imoi.impw_id_ingr
                      ,p_id || ' - Indicatiegroep'
                      ,FALSE
                      ,TRUE
                      ,p_onin_old.ingr_id
                      ,g_cimpw_tab_ingr
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onin.ingr_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- commentaar; niet ok: wel aanwezig in im, niet in onin!
      -- opmerking; niet verplicht
      IF NOT typeok(p_imoi.opmerking
                   ,p_id || ' - Opmerking'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,x_onin.opmerking
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- Mantis 5715
      -- onderzoeksreden
      IF NOT refimpwok(p_imoi.impw_id_onre
                      ,p_id || ' - Reden'
                      ,FALSE
                      ,TRUE
                      ,p_onin_old.onre_id
                      ,g_cimpw_tab_onre
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onin.onre_id := TO_NUMBER(v_db_waarde);
      END IF;

      RETURN v_ok;
   END im2onin;

   FUNCTION verschil_onin
   ( p_o cg$kgc_onderzoek_indicaties.cg$row_type
   , p_n cg$kgc_onderzoek_indicaties.cg$row_type
   , p_i IN OUT cg$kgc_onderzoek_indicaties.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      IF p_o.onin_id IS NULL
      THEN -- alleen toevoegen, niet wijzigen!
         addtext(verschil(p_n.volgorde
                         ,p_o.volgorde
                         ,'volgorde'
                         ,p_i.volgorde));
         addtext(verschil(p_n.indi_id
                         ,p_o.indi_id
                         ,'indi_id'
                         ,p_i.indi_id));
         addtext(verschil(p_n.ingr_id
                         ,p_o.ingr_id
                         ,'ingr_id'
                         ,p_i.ingr_id));
         --                  v_bDummie := TypeOK(r_imoi.commentaar, 'ONDERZOEK_INDICATIE - Commentaar', 'C', 1, 0, FALSE, v_db_waarde, v_imfi_opmerkingen);
         --                  addtext(verschil(v_db_waarde, p_o.commentaar, 'X', 'commentaar', true, v_bAdd, v_SQL, v_SQL2, v_sep, v_imfi_verslag_tmp);
      END IF;

      addtext(verschil(p_n.opmerking
                      ,p_o.opmerking
                      ,'opmerking'
                      ,p_i.opmerking));
      -- Mantis 5715
      addtext(verschil(p_n.onre_id
                      ,p_o.onre_id
                      ,'onre_id'
                      ,p_i.onre_id));

      RETURN v_verschil;
   END verschil_onin;

   FUNCTION im2onkh
   ( p_imok        IN     kgc_im_onde_kopie%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_onkh_old    IN     cg$kgc_onderzoek_kopiehouders.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_onkh        IN OUT cg$kgc_onderzoek_kopiehouders.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal im naar x_, en controleer
      x_onkh.onde_id := TO_NUMBER(SUBSTR(get_im2hlx('IMON' || p_imok.imon_id), 5));

      -- Kopiehouder/Relatie
      IF NOT refimpwok(p_imok.impw_id_rela
                      ,p_id || ' - Relatie'
                      ,FALSE
                      ,TRUE
                      ,p_onkh_old.rela_id
                      ,g_cimpw_tab_rela
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onkh.rela_id := TO_NUMBER(v_db_waarde);
      END IF;

      -- Code_kopiehouder
      IF NOT typeok(p_imok.code_kopiehouder
                   ,p_id || ' - Code kopiehouder'
                   ,'C'
                   ,150
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         null; -- niks mee doen
      END IF;

      RETURN v_ok;
   END im2onkh;

   FUNCTION verschil_onkh
   ( p_o cg$kgc_onderzoek_kopiehouders.cg$row_type
   , p_n cg$kgc_onderzoek_kopiehouders.cg$row_type
   , p_i IN OUT cg$kgc_onderzoek_kopiehouders.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      IF p_n.onkh_id IS NULL
      THEN
         addtext(verschil(p_n.rela_id
                         ,p_o.rela_id
                         ,'rela_id'
                         ,p_i.rela_id));
      END IF;


      RETURN v_verschil;
   END verschil_onkh;

   FUNCTION im2uits
   ( p_imui        IN     kgc_im_uitslagen%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_uits_old    IN     cg$kgc_uitslagen.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_uits        IN OUT cg$kgc_uitslagen.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   , x_datum_brie_print IN OUT DATE
   ) RETURN BOOLEAN IS
      CURSOR c_btyp(p_btyp_id NUMBER) IS
         SELECT *
           FROM kgc_bestand_types btyp
          WHERE btyp.btyp_id = p_btyp_id;
      r_btyp c_btyp%ROWTYPE;

      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
      v_check_exists             VARCHAR2(1000);
      v_count                    NUMBER;
   BEGIN
      -- vertaal im naar x_, en controleer
      x_uits.onde_id := TO_NUMBER(SUBSTR(get_im2hlx('IMON' || p_imui.imon_id), 5));

      -- brieftype
      IF NOT refimpwok(p_imui.impw_id_brty
                      ,p_id || ' - Brieftype'
                      ,FALSE
                      ,TRUE
                      ,p_uits_old.brty_id
                      ,g_cimpw_tab_brty
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.brty_id := TO_NUMBER(v_db_waarde);
      END IF;

      -- afdeling
      IF NOT refimpwok(p_imui.impw_id_kafd
                      ,p_id || ' - Afdeling'
                      ,TRUE
                      ,TRUE
                      ,p_uits_old.kafd_id
                      ,g_cimpw_tab_kafd
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.kafd_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- relatie
      IF NOT refimpwok(p_imui.impw_id_rela
                      ,p_id || ' - Relatie'
                      ,TRUE
                      ,TRUE
                      ,p_uits_old.rela_id
                      ,g_cimpw_tab_rela
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.rela_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- medewerker
      IF NOT refimpwok(p_imui.impw_id_mede
                      ,p_id || ' - Medewerker'
                      ,FALSE
                      ,TRUE
                      ,p_uits_old.mede_id
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.mede_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum uitslag
      IF NOT typeok(p_imui.datum_uitslag
                   ,p_id || ' - Datum uitslag'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.datum_uitslag := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- datum autorisatie
      IF NOT typeok(p_imui.datum_autorisatie
                   ,p_id || ' - Datum autorisatie'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.datum_autorisatie := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- volledig_adres; niet verplicht
      IF NOT typeok(p_imui.volledig_adres
                   ,p_id || ' - Volledig adres'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.volledig_adres := v_db_waarde;
      END IF;
      -- tekst; niet verplicht
      IF NOT typeok(p_imui.tekst
                   ,p_id || ' - Tekst'
                   ,'C'
                   ,4000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.tekst := v_db_waarde;
      END IF;
      -- commentaar; niet verplicht
      IF NOT typeok(p_imui.commentaar
                   ,p_id || ' - Commentaar'
                   ,'C'
                   ,4000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.commentaar := v_db_waarde;
      END IF;
      -- toelichting; niet verplicht
      IF NOT typeok(p_imui.toelichting
                   ,p_id || ' - Toelichting'
                   ,'C'
                   ,4000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.toelichting := v_db_waarde;
      END IF;
      -- medewerker2
      IF NOT refimpwok(p_imui.impw_id_mede2
                      ,p_id || ' - Medewerker2'
                      ,FALSE
                      ,TRUE
                      ,p_uits_old.mede_id2
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.mede_id2 := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum mede_id
      IF NOT typeok(p_imui.datum_mede_id
                   ,p_id || ' - Datum mede_id'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.datum_mede_id := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- datum mede_id2
      IF NOT typeok(p_imui.datum_mede_id2
                   ,p_id || ' - Datum mede_id2'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.datum_mede_id2 := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- medewerker3
      IF NOT refimpwok(p_imui.impw_id_mede3
                      ,p_id || ' - Medewerker3'
                      ,FALSE
                      ,TRUE
                      ,p_uits_old.mede_id3
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.mede_id3 := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum mede_id3
      IF NOT typeok(p_imui.datum_mede_id3
                   ,p_id || ' - Datum mede_id3'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.datum_mede_id3 := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- medewerker4
      IF NOT refimpwok(p_imui.impw_id_mede4
                      ,p_id || ' - Medewerker4'
                      ,FALSE
                      ,TRUE
                      ,p_uits_old.mede_id4
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.mede_id4 := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum_mede_id4
      IF NOT typeok(p_imui.datum_mede_id4
                   ,p_id || ' - Datum mede_id4'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_uits.datum_mede_id4 := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- datum_brie_print
      IF NOT typeok(p_imui.datum_brie_print
                   ,p_id || ' - Datum brief print'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_datum_brie_print := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;

      -- controleer btyp
      IF p_imui.bestandsnaam IS NOT NULL
      THEN
         IF NOT refimpwok(p_imui.impw_id_btyp
                         ,p_id || ' - Bestandtype'
                         ,TRUE
                         ,v_db_waarde
                         ,x_opmerkingen)
         THEN
            v_ok := FALSE;
         ELSE -- controleer type en bestand
            OPEN c_btyp(p_btyp_id => TO_NUMBER(v_db_waarde));
            FETCH c_btyp INTO r_btyp;
            CLOSE c_btyp;
            IF NVL(r_btyp.enti_code, 'X') <> 'UITS'
            THEN
               addtext(x_opmerkingen
                      ,v_max_len
                      ,p_id || ' - bestandstype hoort niet bij entiteit UITS, maar ' || r_btyp.enti_code  || '!');
               v_ok := FALSE;
/*            ELSE -- bestaat bestand?
               SELECT COUNT(*)
                 INTO v_count
                 FROM kgc_bestanden best
                WHERE best.entiteit_code = 'UITS'
                  AND best.entiteit_pk = x_uits.uits_id
                  AND best.btyp_id = r_btyp.btyp_id;
               IF x_uits.uits_id IS NULL OR v_count = 0
               THEN -- controleren, want: nieuw of gekoppeld en nog niet verplaatst
                  v_check_exists := kgc_interface_generiek.FileExists(p_file  => RTRIM(r_btyp.standaard_pad, '\') || '\' || p_imui.bestandsnaam);
                  IF v_check_exists != 'OK'
                  THEN
                     addtext(x_opmerkingen
                            ,v_max_len
                            ,p_id || ' - bestand ' || p_imui.bestandsnaam || ' bestaat niet in map ' || r_btyp.standaard_pad  || '!');
                     v_ok := FALSE;
                  END IF;
               END IF;*/
            END IF;
         END IF;
      END IF;

      RETURN v_ok;
   END im2uits;

   FUNCTION verschil_uits
   ( p_o cg$kgc_uitslagen.cg$row_type
   , p_n cg$kgc_uitslagen.cg$row_type
   , p_i IN OUT cg$kgc_uitslagen.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      -- afdeling
      addtext(verschil(p_n.kafd_id
                      ,p_o.kafd_id
                      ,'kafd_id'
                      ,p_i.kafd_id));
      -- relatie
      addtext(verschil(p_n.rela_id
                      ,p_o.rela_id
                      ,'rela_id'
                      ,p_i.rela_id));
      -- brieftype
      addtext(verschil(p_n.brty_id
                      ,p_o.brty_id
                      ,'brty_id'
                      ,p_i.brty_id));
      -- medewerker
      addtext(verschil(p_n.mede_id
                      ,p_o.mede_id
                      ,'mede_id'
                      ,p_i.mede_id));
      -- datum uitslag
      IF p_n.datum_uitslag IS NULL
      THEN
         p_i.datum_uitslag := FALSE;
      ELSE
         addtext(verschil(p_n.datum_uitslag
                         ,p_o.datum_uitslag
                         ,'datum_uitslag'
                         ,p_i.datum_uitslag));
      END IF;
      -- datum autorisatie
      addtext(verschil(p_n.datum_autorisatie
                      ,p_o.datum_autorisatie
                      ,'datum_autorisatie'
                      ,p_i.datum_autorisatie));
      -- volledig_adres; niet verplicht
      addtext(verschil(p_n.volledig_adres
                      ,p_o.volledig_adres
                      ,'volledig_adres'
                      ,p_i.volledig_adres));
      -- tekst; niet verplicht
      addtext(verschil(p_n.tekst
                      ,p_o.tekst
                      ,'tekst'
                      ,p_i.tekst));
      -- commentaar; niet verplicht
      addtext(verschil(p_n.commentaar
                      ,p_o.commentaar
                      ,'commentaar'
                      ,p_i.commentaar));
      -- toelichting; niet verplicht
      addtext(verschil(p_n.toelichting
                      ,p_o.toelichting
                      ,'commentaar'
                      ,p_i.toelichting));
      -- medewerker2
      addtext(verschil(p_n.mede_id2
                      ,p_o.mede_id2
                      ,'mede_id2'
                      ,p_i.mede_id2));
      -- datum mede_id
      addtext(verschil(p_n.datum_mede_id
                      ,p_o.datum_mede_id
                      ,'datum_mede_id'
                      ,p_i.datum_mede_id));
      -- datum mede_id2
      addtext(verschil(p_n.datum_mede_id2
                      ,p_o.datum_mede_id2
                      ,'datum_mede_id2'
                      ,p_i.datum_mede_id2));
      -- medewerker3
      addtext(verschil(p_n.mede_id3
                      ,p_o.mede_id3
                      ,'mede_id3'
                      ,p_i.mede_id3));
      -- datum mede_id3
      addtext(verschil(p_n.datum_mede_id3
                      ,p_o.datum_mede_id3
                      ,'datum_mede_id3'
                      ,p_i.datum_mede_id3));
      -- medewerker4
      addtext(verschil(p_n.mede_id4
                      ,p_o.mede_id4
                      ,'mede_id4'
                      ,p_i.mede_id4));
      -- datum_mede_id4
      addtext(verschil(p_n.datum_mede_id4
                      ,p_o.datum_mede_id4
                      ,'datum_mede_id4'
                      ,p_i.datum_mede_id4));

      RETURN v_verschil;
   END verschil_uits;


   FUNCTION im2fami
   ( p_imfa        IN     kgc_im_families%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_fami_old    IN     cg$kgc_families.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_fami        IN OUT cg$kgc_families.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal im naar x_, en controleer

      -- andere waarden
      -- familienummer
      IF NOT typeok(p_imfa.familienummer
                   ,p_id || ' - Familienummer'
                   ,'C'
                   ,20
                   ,0
                   ,TRUE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := TRUE;
      ELSE
         x_fami.familienummer := v_db_waarde;
      END IF;

      -- ****** controleer en vertaal gegevens FAMILIE
      -- naam
      -- familienaam; verplicht wordt gecontroleerd bij de aanroeper
      IF NOT typeok(p_imfa.naam
                   ,p_id || ' - Naam'
                   ,'C'
                   ,100
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := TRUE;
      ELSE
         x_fami.naam := v_db_waarde;
      END IF;
      -- archief
      IF NOT typeok(p_imfa.archief
                   ,p_id || ' - Archief'
                   ,'C'
                   ,100
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := TRUE;
      ELSE
         x_fami.archief := v_db_waarde;
      END IF;
      -- opmerkingen
      IF NOT typeok(p_imfa.opmerkingen
                   ,p_id || ' - Opmerkingen'
                   ,'C'
                   ,100
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := TRUE;
      ELSE
         x_fami.opmerkingen := v_db_waarde;
      END IF;

      RETURN v_ok;
   END im2fami;

   FUNCTION verschil_fami
   ( p_o cg$kgc_families.cg$row_type
   , p_n cg$kgc_families.cg$row_type
   , p_i IN OUT cg$kgc_families.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      IF p_n.familienummer IS NOT NULL
      THEN -- niet leegmaken!
         addtext(verschil(p_n.familienummer
                         ,p_o.familienummer
                         ,'familienummer'
                         ,p_i.familienummer));
      END IF;

      addtext(verschil(p_n.naam
                      ,p_o.naam
                      ,'naam'
                      ,p_i.naam));
      addtext(verschil(p_n.archief
                      ,p_o.archief
                      ,'archief'
                      ,p_i.archief));
      addtext(verschil(p_n.opmerkingen
                      ,p_o.opmerkingen
                      ,'opmerkingen'
                      ,p_i.opmerkingen));

      RETURN v_verschil;
   END verschil_fami;

   FUNCTION im2faon
   ( p_imfz        IN     kgc_im_familie_onderzoeken%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_faon_old    IN     cg$kgc_familie_onderzoeken.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_faon        IN OUT cg$kgc_familie_onderzoeken.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      CURSOR c_ongr(p_ongr_id NUMBER) IS
         SELECT ongr.kafd_id
              , ongr.code
           FROM kgc_onderzoeksgroepen ongr
          WHERE ongr.ongr_id = p_ongr_id;
      r_ongr c_ongr%ROWTYPE;

      v_ok                       BOOLEAN := TRUE;
      v_bverplicht               BOOLEAN;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal im naar x_, en controleer
      x_faon.fami_id := TO_NUMBER(SUBSTR(get_im2hlx('IMFA' || p_imfz.imfa_id), 5));

      -- bepaal gegevens voor mogelijke koppeling
      -- IMPW_ID_KAFD
      -- IMPW_ID_ONGR - onderzoeksgroep; verplicht
      IF NOT refimpwok(p_imfz.impw_id_ongr
                      ,p_id || ' - Onderzoeksgroep'
                      ,TRUE
                      ,TRUE
                      ,p_faon_old.ongr_id
                      ,g_cimpw_tab_ongr
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_faon.ongr_id := to_number(v_db_waarde);
         -- bepaal kafd
         OPEN c_ongr(p_ongr_id => x_faon.ongr_id);
         FETCH c_ongr INTO r_ongr;
         CLOSE c_ongr;
      END IF;
      -- afdeling; verplicht tenzij ongr aanwezig is
      v_bverplicht := TRUE;
      IF p_imfz.impw_id_ongr IS NOT NULL
      THEN
         v_bverplicht := FALSE;
      END IF;
      IF NOT refimpwok(p_imfz.impw_id_kafd
                      ,p_id || ' - Afdeling'
                      ,v_bverplicht
                      ,TRUE
                      ,p_faon_old.kafd_id
                      ,g_cimpw_tab_kafd
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_faon.kafd_id := NVL(to_number(v_db_waarde), r_ongr.kafd_id);
      END IF;

      -- aanvrager; verplicht
      IF NOT refimpwok(p_imfz.impw_id_rela
                      ,p_id || ' - Aanvrager'
                      ,TRUE
                      ,TRUE
                      ,p_faon_old.rela_id
                      ,g_cimpw_tab_rela
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_faon.rela_id := to_number(v_db_waarde);
      END IF;
      -- Indicatie
      IF NOT refimpwok(p_imfz.impw_id_indi
                      ,p_id || ' - Indicatie'
                      ,FALSE
                      ,TRUE
                      ,p_faon_old.indi_id
                      ,g_cimpw_tab_indi
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_faon.indi_id := to_number(v_db_waarde);
      END IF;
      -- datum
      IF NOT typeok(p_imfz.datum
                   ,p_id || ' - Datum'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_faon.datum := TO_DATE(v_db_waarde
                                    ,v_datumformaatdoel);
      END IF;

      -- familieonderzoeknr; niet verplicht
      IF NOT typeok(p_imfz.familieonderzoeknr
                   ,p_id || ' - Familieonderzoeknr'
                   ,'C'
                   ,20
                   ,0
                   ,FALSE
                   ,x_faon.familieonderzoeknr
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- onderzoekstype; niet verplicht (er is een default)
      IF NOT refimpwok(p_imfz.impw_id_onderzoekstype
                      ,p_id || ' - Onderzoekstype'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- afgerond; niet verplicht
      IF NOT typeok(p_imfz.afgerond
                   ,p_id || ' - afgerond'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- spoed; niet verplicht
      IF NOT typeok(p_imfz.spoed
                   ,p_id || ' - spoed'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- onderzoekswijze
      IF NOT refimpwok(p_imfz.impw_id_onderzoekswijze
                      ,p_id || ' - Onderzoekswijze'
                      ,FALSE
                      ,TRUE
                      ,NULL -- p_faon_old.onwy_id bestaat niet
                      ,g_cimpw_tab_onwy
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- Geplande einddatum
      IF NOT typeok(p_imfz.geplande_einddatum
                   ,p_id || ' - geplande einddatum'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- referentie
      IF NOT typeok(p_imfz.referentie
                   ,p_id || ' - Referentie'
                   ,'C'
                   ,100
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- omschrijving
      IF NOT typeok(p_imfz.omschrijving
                   ,p_id || ' - Omschrijving'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;

      RETURN v_ok;
   END im2faon;

   FUNCTION verschil_faon
   ( p_o cg$kgc_familie_onderzoeken.cg$row_type
   , p_n cg$kgc_familie_onderzoeken.cg$row_type
   , p_i IN OUT cg$kgc_familie_onderzoeken.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      addtext(verschil(p_n.ongr_id
                      ,p_o.ongr_id
                      ,'ongr_id'
                      ,p_i.ongr_id));
      addtext(verschil(p_n.kafd_id
                      ,p_o.kafd_id
                      ,'kafd_id'
                      ,p_i.kafd_id));
      addtext(verschil(p_n.rela_id
                      ,p_o.rela_id
                      ,'rela_id'
                      ,p_i.rela_id));
      IF p_n.familieonderzoeknr IS NOT NULL
      THEN -- niet leegmaken!
         addtext(verschil(p_n.familieonderzoeknr
                         ,p_o.familieonderzoeknr
                         ,'familieonderzoeknr'
                         ,p_i.familieonderzoeknr));
      END IF;
      IF p_n.datum IS NULL
      THEN
         p_i.datum := FALSE;
      ELSE
         addtext(verschil(p_n.datum
                         ,p_o.datum
                         ,'datum'
                         ,p_i.datum));
      END IF;
      IF p_n.onderzoekstype IS NULL
      THEN
         p_i.onderzoekstype := FALSE;
      ELSE
         addtext(verschil(p_n.onderzoekstype
                         ,p_o.onderzoekstype
                         ,'onderzoekstype'
                         ,p_i.onderzoekstype));
      END IF;
      IF p_n.afgerond IS NULL
      THEN
         p_i.afgerond := FALSE;
      ELSE
         addtext(verschil(p_n.afgerond
                         ,p_o.afgerond
                         ,'afgerond'
                         ,p_i.afgerond));
      END IF;
      IF p_n.spoed IS NULL
      THEN
         p_i.spoed := FALSE;
      ELSE
         addtext(verschil(p_n.spoed
                         ,p_o.spoed
                         ,'spoed'
                         ,p_i.spoed));
      END IF;
      IF p_n.onderzoekswijze IS NULL
      THEN
         p_i.onderzoekswijze := FALSE;
      ELSE
         addtext(verschil(p_n.onderzoekswijze
                         ,p_o.onderzoekswijze
                         ,'onderzoekswijze'
                         ,p_i.onderzoekswijze));
      END IF;
      addtext(verschil(p_n.geplande_einddatum
                      ,p_o.geplande_einddatum
                      ,'geplande_einddatum'
                      ,p_i.geplande_einddatum));
      addtext(verschil(p_n.referentie
                      ,p_o.referentie
                      ,'referentie'
                      ,p_i.referentie));
      addtext(verschil(p_n.omschrijving
                      ,p_o.omschrijving
                      ,'omschrijving'
                      ,p_i.omschrijving));
      addtext(verschil(p_n.indi_id
                      ,p_o.indi_id
                      ,'indi_id'
                      ,p_i.indi_id));

      RETURN v_verschil;
   END verschil_faon;

   FUNCTION im2onmobe
   ( p_imob        IN     kgc_im_onderzoek_betrokkenen%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie im-record
   , p_onmo_old    IN     cg$kgc_onderzoek_monsters.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_onmo        IN OUT cg$kgc_onderzoek_monsters.cg$row_type
   , p_onbe_old    IN     cg$kgc_onderzoek_betrokkenen.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_onbe        IN OUT cg$kgc_onderzoek_betrokkenen.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal im naar x_, en controleer
      -- verwijzingen
      IF p_imob.immo_id IS NOT NULL
      THEN
         x_onmo.onde_id := TO_NUMBER(SUBSTR(get_im2hlx('IMON' || p_imob.imon_id), 5));
         x_onmo.mons_id := TO_NUMBER(SUBSTR(get_im2hlx('IMMO' || p_imob.immo_id), 5));
      ELSIF p_imob.imfr_id IS NOT NULL
      THEN
         x_onbe.onde_id := TO_NUMBER(SUBSTR(get_im2hlx('IMON' || p_imob.imon_id), 5));
         x_onbe.frac_id := TO_NUMBER(SUBSTR(get_im2hlx('IMFR' || p_imob.imfr_id), 5));
         x_onbe.pers_id := TO_NUMBER(SUBSTR(get_im2hlx('IMPE' || p_imob.impe_id), 5));
         x_onbe.foet_id := TO_NUMBER(SUBSTR(get_im2hlx('IMFO' || p_imob.imfo_id), 5));
      END IF;

      -- opmerking_algemeen; niet verplicht
      IF NOT typeok(p_imob.opmerking
                   ,p_id || ' - Opmerking'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_onbe.opmerking := v_db_waarde;
      END IF;

      RETURN v_ok;
   END im2onmobe;

   FUNCTION verschil_onmo
   ( p_o cg$kgc_onderzoek_monsters.cg$row_type
   , p_n cg$kgc_onderzoek_monsters.cg$row_type
   , p_i IN OUT cg$kgc_onderzoek_monsters.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      addtext(verschil(p_n.onde_id
                      ,p_o.onde_id
                      ,'onde_id'
                      ,p_i.onde_id));
      addtext(verschil(p_n.mons_id
                      ,p_o.mons_id
                      ,'mons_id'
                      ,p_i.mons_id));
      RETURN v_verschil;
   END verschil_onmo;

   FUNCTION verschil_onbe
   ( p_o cg$kgc_onderzoek_betrokkenen.cg$row_type
   , p_n cg$kgc_onderzoek_betrokkenen.cg$row_type
   , p_i IN OUT cg$kgc_onderzoek_betrokkenen.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      -- alle relevante kolommen vergelijken
      addtext(verschil(p_n.onde_id
                      ,p_o.onde_id
                      ,'onde_id'
                      ,p_i.onde_id));
      addtext(verschil(p_n.frac_id
                      ,p_o.frac_id
                      ,'frac_id'
                      ,p_i.frac_id));
      addtext(verschil(p_n.pers_id
                      ,p_o.pers_id
                      ,'pers_id'
                      ,p_i.pers_id));
      IF p_n.declareren IS NULL
      THEN
         p_i.declareren := FALSE;
      ELSE
         addtext(verschil(p_n.declareren
                         ,p_o.declareren
                         ,'declareren'
                         ,p_i.declareren));
      END IF;
      addtext(verschil(p_n.rnde_id
                      ,p_o.rnde_id
                      ,'rnde_id'
                      ,p_i.rnde_id));
      addtext(verschil(p_n.rol
                      ,p_o.rol
                      ,'rol'
                      ,p_i.rol));
      IF p_n.adviesvrager IS NULL
      THEN
         p_i.adviesvrager := FALSE;
      ELSE
         addtext(verschil(p_n.adviesvrager
                         ,p_o.adviesvrager
                         ,'adviesvrager'
                         ,p_i.adviesvrager));
      END IF;
      IF p_n.aangedaan IS NULL
      THEN
         p_i.aangedaan := FALSE;
      ELSE
         addtext(verschil(p_n.aangedaan
                         ,p_o.aangedaan
                         ,'aangedaan'
                         ,p_i.aangedaan));
      END IF;
      IF p_n.drager IS NULL
      THEN
         p_i.drager := FALSE;
      ELSE
         addtext(verschil(p_n.drager
                         ,p_o.drager
                         ,'drager'
                         ,p_i.drager));
      END IF;
      IF p_n.neven_geadresseerde IS NULL
      THEN
         p_i.neven_geadresseerde := FALSE;
      ELSE
         addtext(verschil(p_n.neven_geadresseerde
                         ,p_o.neven_geadresseerde
                         ,'neven_geadresseerde'
                         ,p_i.neven_geadresseerde));
      END IF;
      IF p_n.probandus IS NULL
      THEN
         p_i.probandus := FALSE;
      ELSE
         addtext(verschil(p_n.probandus
                         ,p_o.probandus
                         ,'probandus'
                         ,p_i.probandus));
      END IF;
      addtext(verschil(p_n.opmerking
                      ,p_o.opmerking
                      ,'opmerking'
                      ,p_i.opmerking));
      addtext(verschil(p_n.foet_id
                      ,p_o.foet_id
                      ,'foet_id'
                      ,p_i.foet_id));
      RETURN v_verschil;
   END verschil_onbe;

   FUNCTION im2meti
   ( p_imme        IN     kgc_im_metingen%ROWTYPE
   , p_id          IN     VARCHAR2                    -- identificatie impe-record
   , p_meti_old    IN     cg$bas_metingen.cg$row_type -- tbv constatering 'waarde gewijzigd'
   , x_meti        IN OUT cg$bas_metingen.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   ) RETURN BOOLEAN IS
      v_ok                       BOOLEAN := TRUE;
      v_db_waarde                VARCHAR2(4000);
      v_qms_error_mess           VARCHAR2(2000); -- agv qms$errors.show_message
   BEGIN
      -- vertaal imme naar meti, en controleer
      -- verwijzing naar fractie is monster#fractie
      x_meti.frac_id := TO_NUMBER(SUBSTR(get_im2hlx('IMFR' || p_imme.immo_id || '#' || p_imme.imfr_id), 5));
      x_meti.onde_id := TO_NUMBER(SUBSTR(get_im2hlx('IMON' || p_imme.imon_id), 5));
      -- IMPW_ID_STAN: niet in gebruik
      -- datum aanmelding
      IF NOT typeok(p_imme.datum_aanmelding
                   ,p_id || ' - Datum aanmelding'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.datum_aanmelding := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- afgerond; niet verplicht
      IF NOT typeok(p_imme.afgerond
                   ,p_id || ' - afgerond'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.afgerond := NVL(v_db_waarde, 'N');
      END IF;
      -- herhalen; niet verplicht
      IF NOT typeok(p_imme.herhalen
                   ,p_id || ' - herhalen'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.herhalen := NVL(v_db_waarde, 'N');
      END IF;
      -- Snelheid
      IF NOT refimpwok(p_imme.impw_id_snelheid
                      ,p_id || ' - Snelheid'
                      ,FALSE
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.snelheid := NVL(v_db_waarde, 'K');
      END IF;
      -- prioriteit; niet verplicht
      IF NOT typeok(p_imme.prioriteit
                   ,p_id || ' - prioriteit'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.prioriteit := NVL(v_db_waarde, 'N');
      END IF;
      -- datum afgerond
      IF NOT typeok(p_imme.datum_afgerond
                   ,p_id || ' - Datum afgerond'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.datum_afgerond := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- datum conclusie
      IF NOT typeok(p_imme.datum_conclusie
                   ,p_id || ' - Datum conclusie'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.datum_conclusie := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;


      -- setnaam; niet verplicht
      IF NOT typeok(p_imme.setnaam
                   ,p_id || ' - Setnaam'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.setnaam := v_db_waarde;
      END IF;
      -- opmerking_algemeen; niet verplicht
      IF NOT typeok(p_imme.opmerking_algemeen
                   ,p_id || ' - Opmerking algemeen'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.opmerking_algemeen := v_db_waarde;
      END IF;
      -- opmerking_analyse; niet verplicht
      IF NOT typeok(p_imme.opmerking_analyse
                   ,p_id || ' - Opmerking analyse'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,x_meti.opmerking_analyse
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      END IF;
      -- datum selectie_robot
      IF NOT typeok(p_imme.datum_selectie_robot
                   ,p_id || ' - Datum selectie_robot'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.datum_selectie_robot := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- datum uitslag_robot
      IF NOT typeok(p_imme.datum_uitslag_robot
                   ,p_id || ' - Datum uitslag_robot'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.datum_uitslag_robot := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- declareren; niet verplicht
      IF NOT typeok(p_imme.declareren
                   ,p_id || ' - Declareren'
                   ,'J'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.declareren := NVL(v_db_waarde, 'N');
      END IF;
      -- impw_id_stgr
      IF NOT refimpwok(p_imme.impw_id_stgr
                      ,p_id || ' - Stoftestgroep'
                      ,FALSE
                      ,TRUE
                      ,p_meti_old.stgr_id
                      ,g_cimpw_tab_stgr
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.stgr_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- impw_id_mede
      IF NOT refimpwok(p_imme.impw_id_mede
                      ,p_id || ' - Medewerker'
                      ,FALSE
                      ,TRUE
                      ,p_meti_old.mede_id
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.mede_id := TO_NUMBER(v_db_waarde);
      END IF;
      -- analysenr; niet verplicht
      IF NOT typeok(p_imme.analysenr
                   ,p_id || ' - Analysenr'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.analysenr := v_db_waarde;
      END IF;
      -- datum analyse
      IF NOT typeok(p_imme.datum_analyse
                   ,p_id || ' - Datum analyse'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.datum_analyse := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- impw_id_mede_analyse2
      IF NOT refimpwok(p_imme.impw_id_mede_analyse2
                      ,p_id || ' - Medewerker analyse2'
                      ,FALSE
                      ,TRUE
                      ,p_meti_old.mede_id_analyse2
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.mede_id_analyse2 := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum analyse2
      IF NOT typeok(p_imme.datum_analyse2
                   ,p_id || ' - Datum analyse2'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.datum_analyse2 := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- impw_id_mede_controle
      IF NOT refimpwok(p_imme.impw_id_mede_controle
                      ,p_id || ' - Medewerker controle'
                      ,FALSE
                      ,TRUE
                      ,p_meti_old.mede_id_controle
                      ,g_cimpw_tab_mede
                      ,v_db_waarde
                      ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.mede_id_controle := TO_NUMBER(v_db_waarde);
      END IF;
      -- datum geplande_einddatum
      IF NOT typeok(p_imme.geplande_einddatum
                   ,p_id || ' - Geplande einddatum'
                   ,'D'
                   ,0
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.geplande_einddatum := TO_DATE(v_db_waarde, v_datumformaatdoel);
      END IF;
      -- conclusie; niet verplicht
      IF NOT typeok(p_imme.conclusie
                   ,p_id || ' - Conclusie'
                   ,'C'
                   ,2000
                   ,0
                   ,FALSE
                   ,v_db_waarde
                   ,x_opmerkingen)
      THEN
         v_ok := FALSE;
      ELSE
         x_meti.conclusie := v_db_waarde;
      END IF;
      RETURN v_ok;
   END im2meti;

   FUNCTION verschil_meti
   ( p_o cg$bas_metingen.cg$row_type
   , p_n cg$bas_metingen.cg$row_type
   , p_i IN OUT cg$bas_metingen.cg$ind_type
   ) RETURN VARCHAR2 IS
      v_verschil VARCHAR2(32767);
      PROCEDURE addtext(p_tekst VARCHAR2) IS
      BEGIN
         kgc_import.addtext(v_verschil
                ,v_max_len
                ,p_tekst
                ,'; ');
      END;
   BEGIN
      -- alle relevante kolommen vergelijken
      -- datum aanmelding
      IF p_n.datum_aanmelding IS NULL
      THEN
         p_i.datum_aanmelding := FALSE;
      ELSE
         addtext(verschil(p_n.datum_aanmelding
                         ,p_o.datum_aanmelding
                         ,'datum_aanmelding'
                         ,p_i.datum_aanmelding));
      END IF;
      -- afgerond; niet verplicht
      IF p_n.afgerond IS NULL
      THEN
         p_i.afgerond := FALSE;
      ELSE
         addtext(verschil(p_n.afgerond
                         ,p_o.afgerond
                         ,'afgerond'
                         ,p_i.afgerond));
      END IF;
      -- herhalen; niet verplicht
      IF p_n.herhalen IS NULL
      THEN
         p_i.herhalen := FALSE;
      ELSE
         addtext(verschil(p_n.herhalen
                         ,p_o.herhalen
                         ,'herhalen'
                         ,p_i.herhalen));
      END IF;
      -- Snelheid
      IF p_n.snelheid IS NULL
      THEN
         p_i.snelheid := FALSE;
      ELSE
         addtext(verschil(p_n.snelheid
                         ,p_o.snelheid
                         ,'snelheid'
                         ,p_i.snelheid));
      END IF;
      -- prioriteit; niet verplicht
      IF p_n.prioriteit IS NULL
      THEN
         p_i.prioriteit := FALSE;
      ELSE
         addtext(verschil(p_n.prioriteit
                         ,p_o.prioriteit
                         ,'prioriteit'
                         ,p_i.prioriteit));
      END IF;
      -- datum afgerond
      addtext(verschil(p_n.datum_afgerond
                      ,p_o.datum_afgerond
                      ,'datum_afgerond'
                      ,p_i.datum_afgerond));
      -- datum conclusie
      addtext(verschil(p_n.datum_conclusie
                      ,p_o.datum_conclusie
                      ,'datum_conclusie'
                      ,p_i.datum_conclusie));
      -- setnaam; niet verplicht
      addtext(verschil(p_n.setnaam
                      ,p_o.setnaam
                      ,'setnaam'
                      ,p_i.setnaam));
      -- opmerking_algemeen; niet verplicht
      addtext(verschil(p_n.opmerking_algemeen
                      ,p_o.opmerking_algemeen
                      ,'opmerking_algemeen'
                      ,p_i.opmerking_algemeen));
      -- opmerking_analyse; niet verplicht
      addtext(verschil(p_n.opmerking_analyse
                      ,p_o.opmerking_analyse
                      ,'opmerking_analyse'
                      ,p_i.opmerking_analyse));
      -- datum selectie_robot
      addtext(verschil(p_n.datum_selectie_robot
                      ,p_o.datum_selectie_robot
                      ,'datum_selectie_robot'
                      ,p_i.datum_selectie_robot));
      -- datum uitslag_robot
      addtext(verschil(p_n.datum_uitslag_robot
                      ,p_o.datum_uitslag_robot
                      ,'datum_uitslag_robot'
                      ,p_i.datum_uitslag_robot));
      -- declareren; niet verplicht
      IF p_n.declareren IS NULL
      THEN
         p_i.declareren := FALSE;
      ELSE
         addtext(verschil(p_n.declareren
                         ,p_o.declareren
                         ,'declareren'
                         ,p_i.declareren));
      END IF;
      -- impw_id_stgr
      addtext(verschil(p_n.stgr_id
                      ,p_o.stgr_id
                      ,'stgr_id'
                      ,p_i.stgr_id));
      -- impw_id_mede
      addtext(verschil(p_n.mede_id
                      ,p_o.mede_id
                      ,'mede_id'
                      ,p_i.mede_id));
      -- analysenr; niet verplicht
      addtext(verschil(p_n.analysenr
                      ,p_o.analysenr
                      ,'analysenr'
                      ,p_i.analysenr));
      -- datum analyse
      addtext(verschil(p_n.datum_analyse
                      ,p_o.datum_analyse
                      ,'datum_analyse'
                      ,p_i.datum_analyse));
      -- impw_id_mede_analyse2
      addtext(verschil(p_n.mede_id_analyse2
                      ,p_o.mede_id_analyse2
                      ,'mede_id_analyse2'
                      ,p_i.mede_id_analyse2));
      -- datum analyse2
      addtext(verschil(p_n.datum_analyse2
                      ,p_o.datum_analyse2
                      ,'datum_analyse2'
                      ,p_i.datum_analyse2));
      -- impw_id_mede_controle
      addtext(verschil(p_n.mede_id_controle
                      ,p_o.mede_id_controle
                      ,'mede_id_controle'
                      ,p_i.mede_id_controle));
      -- datum geplande_einddatum
      addtext(verschil(p_n.geplande_einddatum
                      ,p_o.geplande_einddatum
                      ,'geplande_einddatum'
                      ,p_i.geplande_einddatum));
      -- conclusie; niet verplicht
      addtext(verschil(p_n.conclusie
                      ,p_o.conclusie
                      ,'conclusie'
                      ,p_i.conclusie));

      RETURN v_verschil;
   END verschil_meti;

   FUNCTION koppel_mons
   ( x_mons IN OUT cg$kgc_monsters.cg$row_type
   , x_opmerkingen IN OUT VARCHAR2
   , p_onde_mons_toevoegen VARCHAR2
   ) RETURN BOOLEAN IS
      -- koppel mons uit immo aan bestaand monster
      CURSOR c_mons_zoek2(p_pers_id NUMBER, p_ongr_id NUMBER, p_mate_id NUMBER) IS
         SELECT mons.mons_id
--              , monsternummer
           FROM kgc_monsters mons
          WHERE mons.pers_id = p_pers_id
            AND mons.ongr_id = p_ongr_id
            AND mons.mate_id = NVL(p_mate_id, mons.mate_id) -- release 8.13.4
            ;
      r_mons_zoek2 c_mons_zoek2%ROWTYPE;

      CURSOR c_mons_zoek3(p_monsternummer VARCHAR2) IS
         SELECT mons.mons_id
              , mons.pers_id
           FROM kgc_monsters mons
          WHERE mons.monsternummer = p_monsternummer;
      r_mons_zoek3 c_mons_zoek3%ROWTYPE;

      CURSOR c_mons_zoek(p_pers_id NUMBER, p_datum DATE, p_ongr_id NUMBER, p_mate_id NUMBER, p_foet_id NUMBER, p_ckcl_nummer VARCHAR2) IS
         SELECT mons.mons_id
              , mons.monsternummer
           FROM kgc_monsters mons
          WHERE mons.pers_id = p_pers_id
            AND mons.datum_afname = p_datum
            AND mons.ongr_id = p_ongr_id
            AND mons.mate_id = p_mate_id
            AND (mons.foet_id = p_foet_id OR (mons.foet_id IS NULL AND p_foet_id IS NULL))
            AND (mons.ckcl_nummer = p_ckcl_nummer OR (mons.ckcl_nummer IS NULL AND p_ckcl_nummer IS NULL));
      r_mons_zoek c_mons_zoek%ROWTYPE;

      v_ok                       BOOLEAN := TRUE;
   BEGIN
      -- mogelijk bevat het monsternummer '*KOPPEL*'; dan alleen check op aanwezigheid
      IF x_mons.monsternummer = '*KOPPEL*'
      THEN
         OPEN c_mons_zoek2(p_pers_id => x_mons.pers_id
                          ,p_ongr_id => x_mons.ongr_id
                          ,p_mate_id => x_mons.mate_id -- release 8.13.4
                          );
         FETCH c_mons_zoek2
            INTO x_mons.mons_id;
         IF c_mons_zoek2%NOTFOUND
         THEN
            addtext(x_opmerkingen
                   ,v_max_len
                   ,' - Er is aangegeven dat bestaand materiaal gebruikt moet worden;' ||
                    ' dit is echter niet gevonden voor de opgegeven onderzoeksgroep!');
            v_ok := FALSE; -- 8.13.2.1 niet OK!
         ELSE -- check uniek
            FETCH c_mons_zoek2
               INTO r_mons_zoek2;
            IF c_mons_zoek2%FOUND
            THEN
               addtext(x_opmerkingen
                      ,v_max_len
                      ,' - Er is aangegeven dat bestaand materiaal gebruikt moet worden;' ||
                       ' er zijn echter meerdere monsters gevonden! Koppel handmatig!');
               v_ok := FALSE; -- 8.13.2.1 niet OK!
               x_mons.mons_id := NULL; -- 8.13.2.1 leeg maken
            END IF;
         END IF;
         CLOSE c_mons_zoek2;
--         v_mons_new.monsternummer := NULL; -- zeker niet gebruiken voor insert oid
      ELSIF x_mons.monsternummer IS NOT NULL -- koppel obv monsternummer (mantis 11620)
      THEN
         OPEN c_mons_zoek3(p_monsternummer => x_mons.monsternummer);
         FETCH c_mons_zoek3
            INTO r_mons_zoek3;
         IF c_mons_zoek3%FOUND
         THEN
            x_mons.mons_id := r_mons_zoek3.mons_id;
            addtext(x_opmerkingen
                   ,v_max_len
                   ,' - Bestaand monster gekoppeld obv monsternummer (' || x_mons.monsternummer || ')');
         ELSE -- niet gevonden; geen aktie: in de volgende stap worden de gegevens gecontroleerd
            NULL;
         END IF;
         CLOSE c_mons_zoek3;
      ELSIF p_onde_mons_toevoegen = 'N'  -- indien niet "altijd onde/mons toevoegen": check bestaand
            AND x_mons.datum_afname IS NOT NULL -- datum afname
      THEN
         -- bestaat het monster al?
         OPEN c_mons_zoek(x_mons.pers_id
                         ,x_mons.datum_afname
                         ,x_mons.ongr_id
                         ,x_mons.mate_id
                         ,x_mons.foet_id
                         ,x_mons.ckcl_nummer
                          );
         FETCH c_mons_zoek
            INTO r_mons_zoek;
         IF c_mons_zoek%FOUND
         THEN
            x_mons.mons_id := r_mons_zoek.mons_id;
            addtext(x_opmerkingen
                   ,v_max_len
                   ,' - Bestaand monster gekoppeld obv monstergegevens (' || r_mons_zoek.monsternummer || ')');
         ELSE
            x_mons.mons_id := NULL;
         END IF;
         CLOSE c_mons_zoek;
      END IF;
      RETURN v_ok;
   END koppel_mons;

   PROCEDURE reset_im2hlx IS
   BEGIN
      g_tab_im2hlx.delete;
   END reset_im2hlx;

   PROCEDURE set_im2hlx(p_im VARCHAR2, p_hlx VARCHAR2) IS
   BEGIN
      g_tab_im2hlx(p_im) := p_hlx;
   END set_im2hlx;

   FUNCTION get_im2hlx(p_im VARCHAR2) RETURN VARCHAR2 IS
      v_waarde VARCHAR2(100);
   BEGIN
      BEGIN
         v_waarde := g_tab_im2hlx(p_im);
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END;
      IF v_waarde IS NULL THEN
         RETURN NULL;
      END IF;
      RETURN v_waarde;
   END get_im2hlx;
   FUNCTION get_im2hlx(p_im VARCHAR2, x_entiteit IN OUT VARCHAR2) RETURN NUMBER IS
      v_waarde VARCHAR2(100);
   BEGIN
      BEGIN
         v_waarde := g_tab_im2hlx(p_im);
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END;
      --Error ORA-01403: Geen gegevens gevonden.
      x_entiteit := SUBSTR(v_waarde, 1, 4);
      IF v_waarde IS NULL THEN
         RETURN NULL;
      END IF;
      RETURN TO_NUMBER(SUBSTR(v_waarde, 5));
   END get_im2hlx;

   FUNCTION get_standaard_logica RETURN VARCHAR2 IS
   BEGIN
      RETURN g_standaard_logica;
   END get_standaard_logica;

   FUNCTION isPersoonAnoniem(p_pers cg$kgc_personen.cg$row_type) RETURN VARCHAR2 IS
      v_anoniem VARCHAR2(1) := 'N';
   BEGIN
      IF p_pers.zisnr IS NULL AND
         p_pers.bsn IS NULL AND
         p_pers.geboortedatum IS NULL AND
         p_pers.voorletters IS NULL AND
         p_pers.adres IS NULL AND
         p_pers.rela_id IS NULL
      THEN
         v_anoniem := 'J';
      END IF;
      RETURN v_anoniem;
   END isPersoonAnoniem;

   PROCEDURE set_atwa(p_atwa_waarde VARCHAR2, p_spec_waarde VARCHAR2, p_tabel_naam VARCHAR2, p_id NUMBER) IS
      v_attr_code VARCHAR2(32);
   BEGIN
      IF p_atwa_waarde IS NULL OR p_spec_waarde IS NULL
      THEN
         RETURN;
      END IF;
      IF SUBSTR(p_spec_waarde, 1, 5) = 'ATTR_'
      THEN -- bv ATTR_ZISNR_MUMC
         v_attr_code := SUBSTR(p_spec_waarde, 6);
         kgc_attr_00.set_waarde(p_tabel_naam, v_attr_code, p_id, p_atwa_waarde);
      END IF;
   END set_atwa;

   FUNCTION get_externe_id_via_impw_id(p_imlo_id NUMBER, p_prefix VARCHAR2, p_tabel VARCHAR2, p_id VARCHAR2) RETURN VARCHAR2 IS
      -- bepaal adhv de parameters of een tabel-rij al een keer gematched is
      -- uitkomst: NULL of p_prefix (bv #EXT-ID=MEDE.CODE:) || ext-id
      -- reden van deze functie: maastricht stuurt een aanvraag naar Nijm, Nijm stuurt goedkeuring terug. Bij verwerken goedkeuring moet
      -- Maastricht de onde.aanvrager weer koppelen, terwijl deze info al bekend is. De koppel-info wordt met deze functie opgehaald.
      CURSOR c_impw IS
         SELECT impw.externe_id
         FROM   kgc_import_param_waarden impw
         ,      kgc_import_parameters impa
         WHERE  impa.helix_table_domain = p_tabel
         AND    impa.impa_id = impw.impa_id
         AND    impw.imlo_id = p_imlo_id
         AND    impw.helix_waarde = p_id
         AND    ( p_prefix IS NULL
                  OR
                  (p_prefix IS NOT NULL AND impw.externe_id NOT LIKE p_prefix || '%') -- zie verklaring
                )
         ORDER BY impw.impw_id DESC
         ;
         -- verklaring conditie
         -- Stel hetvolgende is gebeurd:
         -- 1. Nijm stuurt nieuwe aanvraag, aanvragercode AAA
         -- 2. Maastricht leest in, koppelt, keurt goed en exporteert. De externe code AAA moet weer gebruikt worden om terug te sturen, dit wordt #EXT-ID=RELA.CODE:AAA
         -- 3. Nijmegen leest in: automatisch wordt #EXT-ID=RELA.CODE:AAA gekoppeld aan AAA; prima.
         -- ...
         -- x. Nijmegen stuurt een nieuwe aanvraag,  aanvragercode AAA. Via aanroep get_externe_id_via_impw_id en stap 3 vind je externe-id #EXT-ID=RELA.CODE:AAA,
         --    maar dit mag je NIET gebruiken. Wel de originele code AAA, identiek aan stap 1.
         --    dus met deze conditie wordt de eigen retourgekomen codering uitgesloten. Dus wellicht wordt er NULL teruggegeven, of er wordt nog een andere Maastrichtse code gevonden BBB,
         --    en dan kan #EXT-ID=RELA.CODE:BBB teruggegeven worden.
      r_impw c_impw%ROWTYPE;
   BEGIN
      OPEN c_impw;
      FETCH c_impw INTO r_impw;
      CLOSE c_impw;
      IF r_impw.externe_id IS NULL THEN
         RETURN NULL;
      END IF;
      RETURN p_prefix || r_impw.externe_id;
   END get_externe_id_via_impw_id;

END kgc_import;
/
