CREATE OR REPLACE PACKAGE "HELIX"."KGC_BOUWSTEEN_00" IS

  TYPE t_bouwsteen_cursor IS REF CURSOR;

  FUNCTION bouwsteen (
    p_bouwsteen   IN   VARCHAR2
   ,p_kolommen    IN   VARCHAR2
   ,p_where       IN   VARCHAR2
   ,p_groep       IN   VARCHAR2 := NULL
   ,p_prefix      IN   VARCHAR2 := NULL
  )
    RETURN kgc_xml_00.t_xml_regel_collection;
END KGC_BOUWSTEEN_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_BOUWSTEEN_00" IS
/******************************************************************************
   NAME:       kgc_bouwsteen_00
   PURPOSE:    aanroepen van bouwstenen voor XML generatie

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31-03-2006   HZO              Creatie
******************************************************************************/

  /******************************************************************************
     NAME:       bouwsteen
     PURPOSE:    roep een bouwsteen (view of tabel) aan. Een collection met XML
                 regels wordt geretourneerd

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        6-4-2006    HZO             Creatie

     NOTES:

  ******************************************************************************/
  FUNCTION bouwsteen (
    p_bouwsteen   IN   VARCHAR2
   ,p_kolommen    IN   VARCHAR2
   ,p_where       IN   VARCHAR2
   ,p_groep       IN   VARCHAR2 := NULL
   ,p_prefix      IN   VARCHAR2 := NULL
  )
    RETURN kgc_xml_00.t_xml_regel_collection
  IS
    v_xml_regel_collection   kgc_xml_00.t_xml_regel_collection;
    v_sql                    VARCHAR2 (4000);
    v_c                      INTEGER                  := dbms_sql.open_cursor;
    v_status                 INTEGER;
    v_aant_kolommen          NUMBER                            := 0;
    -- col_type (uit view sys.all_tab_cols
    v_cv1                    VARCHAR2 (4000);
    -- 1=VARCHAR2; ignore NVARCHAR2
    v_vc2float               FLOAT;
    -- 2 + scale=null; precision not null
    v_vc2num                 NUMBER;                            -- 2 (overig)
    v_vc8long                LONG;                              -- 8, 'LONG',
    v_cv9                    VARCHAR (4000);
    -- 9=VARCHAR; ignore NCHAR VARYING
    v_vc12date               DATE;                              -- 12, 'DATE'
    v_cv96char               CHAR (4000);            -- 96=CHAR; ignore NCHAR
    v_kolom_waarden          kgc_xml_00.t_kolom_waarden;
    v_desc_tab               dbms_sql.desc_tab;
    v_stap                   VARCHAR2 (100);
  BEGIN
    -- query parsing
    v_stap := ' query parsing ';
    v_sql :=    'SELECT  '
             || p_kolommen
             || ' FROM '
             || p_bouwsteen
             || ' WHERE '
             || p_where;
    dbms_sql.parse (v_c
                   ,v_sql
                   ,dbms_sql.native
                   );
    v_aant_kolommen := v_desc_tab.COUNT;

    IF v_aant_kolommen = 0                          -- reeds eerder uitgevoerd
    THEN
      dbms_sql.describe_columns (v_c
                                ,v_aant_kolommen
                                ,v_desc_tab
                                );
    END IF;

    -- definitie van de kolommen
    v_stap := 'Definitie van kolommen ';

    FOR i IN 1 .. v_aant_kolommen
    LOOP
      IF v_desc_tab (i).col_type = 1
      THEN                                                        -- varchar2
        dbms_sql.define_column (v_c
                               ,i
                               ,v_cv1
                               ,4000
                               );
      ELSIF v_desc_tab (i).col_type = 2
      THEN                                                   -- float / number
        IF (v_desc_tab (i).col_scale IS NULL
            AND v_desc_tab (i).col_precision IS NOT NULL
           )
        THEN
          dbms_sql.define_column (v_c
                                 ,i
                                 ,v_vc2float
                                 );
        ELSE
          dbms_sql.define_column (v_c
                                 ,i
                                 ,v_vc2num
                                 );
        END IF;
      ELSIF v_desc_tab (i).col_type = 8
      THEN
        dbms_sql.define_column (v_c
                               ,i
                               ,v_vc8long
                               );
      ELSIF v_desc_tab (i).col_type = 9
      THEN
        dbms_sql.define_column (v_c
                               ,i
                               ,v_cv9
                               ,4000
                               );
      ELSIF v_desc_tab (i).col_type = 12
      THEN                                                             -- date
        dbms_sql.define_column (v_c
                               ,i
                               ,v_vc12date
                               );
      ELSIF v_desc_tab (i).col_type = 96
      THEN
        dbms_sql.define_column (v_c
                               ,i
                               ,v_cv96char
                               ,4000
                               );
      END IF;
    END LOOP;

    v_status := dbms_sql.EXECUTE (v_c);
    -- doorloop alle records en maak van ieder
    -- record een XML regel
    v_stap := ' door alle records en mak XML regel ';

    WHILE (dbms_sql.fetch_rows (v_c) > 0)
    LOOP
      FOR i IN 1 .. v_aant_kolommen
      LOOP
        v_cv1 := NULL;

        IF v_desc_tab (i).col_type = 1
        THEN                                                      -- varchar2
          dbms_sql.column_value (v_c
                                ,i
                                ,v_cv1
                                );
        ELSIF v_desc_tab (i).col_type = 2
        THEN                                                 -- float / number
          IF (v_desc_tab (i).col_scale IS NULL
              AND v_desc_tab (i).col_precision IS NOT NULL
             )
          THEN
            dbms_sql.column_value (v_c
                                  ,i
                                  ,v_vc2float
                                  );
            v_cv1 := to_char (v_vc2float);
          ELSE
            dbms_sql.column_value (v_c
                                  ,i
                                  ,v_vc2num
                                  );
            v_cv1 := to_char (v_vc2num);
          END IF;
        ELSIF v_desc_tab (i).col_type = 8
        THEN
          dbms_sql.column_value (v_c
                                ,i
                                ,v_vc8long
                                );
          v_cv1 := to_char (v_vc8long);
        ELSIF v_desc_tab (i).col_type = 9
        THEN
          dbms_sql.column_value (v_c
                                ,i
                                ,v_cv9
                                );
          v_cv1 := v_cv9;
        ELSIF v_desc_tab (i).col_type = 12
        THEN                                                           -- date
          dbms_sql.column_value (v_c
                                ,i
                                ,v_vc12date
                                );
          v_cv1 := to_char (v_vc12date, 'dd-mm-yyyy hh24:mi:ss');
        ELSIF v_desc_tab (i).col_type = 96
        THEN
          dbms_sql.column_value (v_c
                                ,i
                                ,v_cv96char
                                );
          v_cv1 := v_cv96char;
        ELSE
          dbms_output.put_line (   'X:'
                                || v_desc_tab (i).col_name
                                || ' ander type: '
                                || v_desc_tab (i).col_type);
        END IF;

        v_kolom_waarden (v_desc_tab (i).col_name) := v_cv1;
      END LOOP;

      v_xml_regel_collection (v_xml_regel_collection.COUNT + 1) :=
        kgc_xml_00.naar_xml (p_kolom_waarden     => v_kolom_waarden
                            ,p_groep             => p_groep
                            ,p_prefix            => p_prefix
                            );
    END LOOP;

    dbms_sql.close_cursor (v_c);
    RETURN v_xml_regel_collection;

  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_BOUWSTEEN_00.bouwsteen'
                              ,p_param2     => sqlerrm
                              );
  END bouwsteen;
END kgc_bouwsteen_00;
/

/
QUIT
