CREATE OR REPLACE PACKAGE "HELIX"."KGC_CONTROLES_00" IS
 -- PL/SQL Specification
-- altijd GOED
FUNCTION goed
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;
-- altijd FOUT
FUNCTION fout
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;

-- controleer geslacht
FUNCTION  geslacht_m
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;
FUNCTION  geslacht_v
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;

END  KGC_CONTROLES_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_CONTROLES_00" IS
FUNCTION goed
( p_meet_id IN NUMBER
)
RETURN VARCHAR2
IS
-- PL/SQL Block
BEGIN
  RETURN( 'J' );
END goed;

FUNCTION fout
( p_meet_id IN NUMBER
)
RETURN VARCHAR2
IS
BEGIN
  RETURN( 'N' );
END fout;

FUNCTION  geslacht_m
( p_meet_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR c
  IS
    SELECT 'J'
    FROM   kgc_personen pers
    ,      kgc_foetussen foet
    ,      kgc_monsters mons
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    mons.pers_id = pers.pers_id
    AND    mons.foet_id = foet.foet_id (+)
    AND    DECODE( foet.foet_id
                 , NULL, pers.geslacht
                 , foet.geslacht
                 ) in ( 'M', 'O' )
    AND    meet.meet_id = p_meet_id
    ;
BEGIN
  OPEN  c;
  FETCH c
  INTO v_return;
  CLOSE c;
  RETURN( v_return );
END geslacht_m;

FUNCTION  geslacht_v
( p_meet_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR c
  IS
    SELECT 'J'
    FROM   kgc_personen pers
    ,      kgc_foetussen foet
    ,      kgc_monsters mons
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    mons.pers_id = pers.pers_id
    AND    mons.foet_id = foet.foet_id (+)
    AND    DECODE( foet.foet_id
                 , NULL, pers.geslacht
                 , foet.geslacht
                 ) in ( 'V', 'O' )
    AND    meet.meet_id = p_meet_id
    ;
  v_geslacht VARCHAR2(1);
BEGIN
  OPEN  c;
  FETCH c
  INTO v_return;
  CLOSE c;
  RETURN( v_return );
END geslacht_v;

END  kgc_controles_00;
/

/
QUIT
