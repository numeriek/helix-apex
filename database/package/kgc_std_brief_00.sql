CREATE OR REPLACE PACKAGE "HELIX"."KGC_STD_BRIEF_00" IS


    PROCEDURE verzoekbrief_001 (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );
  PROCEDURE uitslagbrief_001 (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
         ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );
END KGC_STD_BRIEF_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_STD_BRIEF_00" IS


   /******************************************************************************
     NAME:       KGC_STD_BRIEF_00
     PURPOSE:    Aanmaken van de standaard brieven in XML-formaat.

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        5-4-2006    HZO             1. Aangemaakt
  *****************************************************************************/

  /******************************************************************************
     NAME:       verzoekbrief_001
     PURPOSE:    Aanmaken standaard verzoekbrief

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        6-4-2006    HZO              Creatie

     NOTES:

  *****************************************************************************/
  PROCEDURE verzoekbrief_001 (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER DEFAULT NULL
  )
  IS
    v_rela_adres             VARCHAR2 (4000);
    v_pers_adres             VARCHAR2 (4000);
    v_xml_doc                CLOB;
    v_xml_regel              kgc_xml_00.t_xml_regel;
    v_xml_regel_collection   kgc_xml_00.t_xml_regel_collection;
    v_datum_print            DATE                                := sysdate;
    v_nl            CONSTANT VARCHAR2 (2)                        := chr (10);
    v_sql                    VARCHAR2 (1000);
    c_xvvz                   kgc_bouwsteen_00.t_bouwsteen_cursor;
    r_xvvz                   kgc_xml_verzoek_vw%ROWTYPE;
  BEGIN
    -- hoofdselectie
    kgc_xml_00.init;

    OPEN c_xvvz FOR 'SELECT * FROM ' || p_view_name || ' ' || p_where_clause;

    LOOP
      FETCH c_xvvz
       INTO r_xvvz;

      EXIT WHEN c_xvvz%NOTFOUND;
      -- maak nieuw xml document aan
      v_xml_doc := kgc_xml_00.nieuwe_xml (p_naam     => 'inlichtingenbrief');
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'datum_print'
                                   ,p_xml_regel     => to_char (v_datum_print
                                                               ,'DD-MM-YYYY')
                                   );
      -- haal onderzoeks gegevens, via bouwsteen
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen (p_bouwsteen     => 'KGC_XML_ONDERZOEKEN_VW'
                                   ,p_kolommen      => 'onderzoeknr, datum_binnen'
                                   ,p_where         =>    'onde_id = '
                                                       || to_char
                                                               (r_xvvz.onde_id)
                                   ,p_prefix        => 'onde'
                                   );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);

     -- persoon
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_XML_PERSOON_VW'
          ,p_kolommen      => 'aanspreken, geboortedatum, geslacht'--, overlijdensdatum'
          ,p_where         => 'pers_id = ' || to_char (r_xvvz.pers_id)
          ,p_prefix        => 'pers'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- persoon adres
      v_pers_adres := kgc_adres_00.persoon (r_xvvz.pers_id, 'N');
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'pers_adres'
                         ,p_xml_regel     =>    v_nl
                                             || kgc_xml_00.cr_naar_tags
                                                                 (v_pers_adres)
                         );

      -- counselor
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
                              (p_bouwsteen     => 'KGC_MEDEWERKERS'
                              ,p_kolommen      => 'naam'--, email'
                              ,p_where         =>    'mede_id = '
                                                  || to_char
                                                       (r_xvvz.mede_id_beoordelaar)
                              ,p_prefix        => 'mede'
                              );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- relatie  adres
      v_rela_adres := kgc_adres_00.relatie (r_xvvz.rela_id, 'J');
      kgc_xml_00.voegtoe_xml_regel
                       (p_xml_clob      => v_xml_doc
                       ,p_tag           => 'rela_adres'
                       ,p_xml_regel     =>    v_nl
                                           || kgc_xml_00.cr_naar_tags
                                                                 (v_rela_adres)
                                           || v_nl
                       );
      -- sluit
      kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                           ,p_naam         => 'inlichtingenbrief');
      kgc_xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);
    END LOOP;

    CLOSE c_xvvz;
  END verzoekbrief_001;

  /******************************************************************************
     NAME:       uitslagbrief_001
     PURPOSE:    Aanmaken standaard uitslagbrief

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        22-5-2006   YAR              Creatie

     NOTES:

  *****************************************************************************/
  PROCEDURE uitslagbrief_001 (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER DEFAULT NULL
  )
  IS
    cursor c_ramo (b_brty_id in number)
        is
        select ramo.ramo_id
        from kgc_rapport_modules ramo
        ,    kgc_brieftypes brty
        where brty.brty_id = b_brty_id
        and   brty.ramo_id = ramo.ramo_id
        ;

    v_rela_adres             VARCHAR2 (4000);
    v_pers_adres             VARCHAR2 (4000);
    v_xml_doc                CLOB;
    v_xml_regel              kgc_xml_00.t_xml_regel;
    v_xml_regel_collection   kgc_xml_00.t_xml_regel_collection;
    v_datum_print            DATE                                := sysdate;
    v_nl            CONSTANT VARCHAR2 (2)                        := chr (10);
    v_sql                    VARCHAR2 (1000);
    c_xvvz                   kgc_bouwsteen_00.t_bouwsteen_cursor;
    r_xvvz                   kgc_uitslagbrief_vw%ROWTYPE;
        v_ramo_id                number;
  BEGIN
    -- hoofdselectie
    kgc_xml_00.init;

    OPEN c_xvvz FOR 'SELECT * FROM ' || p_view_name || ' ' || p_where_clause;

    LOOP
      FETCH c_xvvz
       INTO r_xvvz;

      EXIT WHEN c_xvvz%NOTFOUND;
      -- maak nieuw xml document aan
      v_xml_doc := kgc_xml_00.nieuwe_xml (p_naam     => 'uitslagbrief');
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'datum_print'
                                   ,p_xml_regel     => to_char (v_datum_print
                                                               ,'DD-MM-YYYY')
                                   );
      -- haal onderzoeks gegevens, via bouwsteen
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_UITSLAGBRIEF_VW'
          ,p_kolommen      => 'geadresseerde'--, onde_referentie, aanvraagdatum, geboortedatum, geslacht'
          ,p_where         => 'uits_id = ' || to_char (r_xvvz.uits_id)
          ,p_prefix        => 'uitv'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- sluit
      kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                           ,p_naam         => 'uitslagbrief');
      kgc_xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

      IF p_ind_brief = 'J'
      THEN
            open c_ramo (b_brty_id => r_xvvz.brty_id);
                fetch c_ramo into v_ramo_id;
                close c_ramo;

        kgc_brie_00.registreer (p_datum             => sysdate
                               ,p_kopie             => p_ind_kopie
                               ,p_brieftype         => 'UITSLAG'
                               ,p_brty_id           => r_xvvz.brty_id
                               ,p_pers_id           => r_xvvz.pers_id
                               ,p_kafd_id           => r_xvvz.kafd_id
                               ,p_onde_id           => r_xvvz.onde_id
                               ,p_rela_id           => r_xvvz.rela_id
                               ,p_uits_id           => r_xvvz.uits_id
                               ,p_geadresseerde     => r_xvvz.geadresseerde
                               ,p_ongr_id           => r_xvvz.ongr_id
                               ,p_taal_id           => r_xvvz.taal_id
                               ,p_destype           => p_destype
                               ,p_best_id           => p_best_id
                               ,p_ind_commit        => 'J'
                                                           ,p_ramo_id           => v_ramo_id
                               );
      END IF;
    END LOOP;

    CLOSE c_xvvz;
  END uitslagbrief_001;
END kgc_std_brief_00;
/

/
QUIT
