CREATE OR REPLACE PACKAGE "HELIX"."KGC_SQLLIMS" IS
-- procedures en functions uit deze package worden aangeroepen
-- vanuit de helix-package KGC_INTERFACE (dus niet direct!).
-- Van hieruit worden procedures en functions aangeroepen uit de remote API-package


-- roep SQLLIMS API aan (via (synonyms en) databaselink SQLLIMS)
-- zet tray informatie over naar "wellplate"
PROCEDURE customer_well_plate
( p_max_columns IN NUMBER
, p_max_rows IN NUMBER
, p_project IN VARCHAR2
, p_color_info1 IN VARCHAR2 := NULL
, p_color_comment1 IN VARCHAR2 := NULL
, p_color_info2 IN VARCHAR2 := NULL
, p_color_comment2 IN VARCHAR2 := NULL
, p_color_info3 IN VARCHAR2 := NULL
, p_color_comment3 IN VARCHAR2 := NULL
, p_color_info4 IN VARCHAR2 := NULL
, p_color_comment4 IN VARCHAR2 := NULL
, p_color_info5 IN VARCHAR2 := NULL
, p_color_comment5 IN VARCHAR2 := NULL
, p_userstamp IN VARCHAR2
, p_date_logged IN DATE := NULL
, x_plate_id OUT NUMBER
, x_plate_name OUT VARCHAR2
, x_error OUT BOOLEAN
, x_error_text OUT VARCHAR2
);
-- zet tray vulling informatie over naar "wellplate samples"
-- De samplenaam in SQL*LIMS wordt daar later voorzien van een prefix dat de positie aangeeft
-- Via een vertaal-tabel kan de SQL*LIMS samplenaam met de Helix-samlenaam worden gelinkt.
PROCEDURE customer_sample
( p_sample_name IN VARCHAR2
, p_sample_comment IN VARCHAR2 := NULL
, p_plate_id IN NUMBER
, p_column IN VARCHAR2
, p_row IN VARCHAR2
, p_color_info1 IN VARCHAR2 := NULL
, p_color_comment1 IN VARCHAR2 := NULL
, p_color_info2 IN VARCHAR2 := NULL
, p_color_comment2 IN VARCHAR2 := NULL
, p_color_info3 IN VARCHAR2 := NULL
, p_color_comment3 IN VARCHAR2 := NULL
, p_color_info4 IN VARCHAR2 := NULL
, p_color_comment4 IN VARCHAR2 := NULL
, p_color_info5 IN VARCHAR2 := NULL
, p_color_comment5 IN VARCHAR2 := NULL
, p_userstamp IN VARCHAR2
, x_error OUT BOOLEAN
, x_error_text OUT VARCHAR2
);
-- controleer of BIOLIMS klaar is en neem meetwaarde over
PROCEDURE sample_complete
( p_sample_id IN VARCHAR2
, x_complete OUT VARCHAR2
, x_value OUT VARCHAR2
);

-- zet interface data (parameters/attributen) over
PROCEDURE   plate_attribute
 (  p_plate_id  IN  NUMBER
 ,  p_sequence  IN  NUMBER  :=  NULL
 ,  p_name  IN  VARCHAR2
 ,  p_value  IN  VARCHAR2
 ,  x_error  OUT  BOOLEAN
 ,  x_error_text  OUT  VARCHAR2
 );
END KGC_SQLLIMS;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_SQLLIMS" IS
PROCEDURE customer_well_plate
( p_max_columns IN NUMBER
, p_max_rows IN NUMBER
, p_project IN VARCHAR2
, p_color_info1 IN VARCHAR2 := NULL
, p_color_comment1 IN VARCHAR2 := NULL
, p_color_info2 IN VARCHAR2 := NULL
, p_color_comment2 IN VARCHAR2 := NULL
, p_color_info3 IN VARCHAR2 := NULL
, p_color_comment3 IN VARCHAR2 := NULL
, p_color_info4 IN VARCHAR2 := NULL
, p_color_comment4 IN VARCHAR2 := NULL
, p_color_info5 IN VARCHAR2 := NULL
, p_color_comment5 IN VARCHAR2 := NULL
, p_userstamp IN VARCHAR2
, p_date_logged IN DATE := NULL
, x_plate_id OUT NUMBER
, x_plate_name OUT VARCHAR2
, x_error OUT BOOLEAN
, x_error_text OUT VARCHAR2
)
IS
  CURSOR proj_cur
  IS
    SELECT UPPER(application) application
    FROM   cwebv_api_projects
    WHERE  name = p_project
    ;
  proj_rec proj_cur%rowtype;
  v_plate_id NUMBER;
  v_plate_name varchar2(100); -- kgc_tray_gebruik.externe_id
  v_status NUMBER;
  -- PL/SQL Block
BEGIN
-- na ipn van labvantage obsolete verklaard.
  NULL;
/*
  OPEN  proj_cur;
  FETCH proj_cur
  INTO  proj_rec;
  CLOSE proj_cur;

  cweb_api.InsertPlate
  ( pPlate_id => v_plate_id
  , pPlateName => v_plate_name
  , pColumn# => p_max_columns
  , pRow# => p_max_rows
  , pProject => p_project
  , pStatus => v_status
  , pUser => p_userstamp
  );

  IF ( v_status = 0 )
  THEN
    x_plate_id := v_plate_id;
    x_plate_name := v_plate_name;
    x_error := FALSE;
    x_error_text := NULL;
  ELSE
    x_error := TRUE;
    cweb_api.ErrorText
    ( pCode => v_status
    , pMessage => x_error_text
    , pStatus => v_status
    );
  END IF;
*/
EXCEPTION
  WHEN OTHERS
  THEN
    x_error := TRUE;
    x_error_text := SQLERRM;
END customer_well_plate;

PROCEDURE  customer_sample
 (  p_sample_name  IN  VARCHAR2
 ,  p_sample_comment  IN  VARCHAR2  :=  NULL
 ,  p_plate_id  IN  NUMBER
 ,  p_column  IN  VARCHAR2
 ,  p_row  IN  VARCHAR2
 ,  p_color_info1  IN  VARCHAR2  :=  NULL
 ,  p_color_comment1  IN  VARCHAR2  :=  NULL
 ,  p_color_info2  IN  VARCHAR2  :=  NULL
 ,  p_color_comment2  IN  VARCHAR2  :=  NULL
 ,  p_color_info3  IN  VARCHAR2  :=  NULL
 ,  p_color_comment3  IN  VARCHAR2  :=  NULL
 ,  p_color_info4  IN  VARCHAR2  :=  NULL
 ,  p_color_comment4  IN  VARCHAR2  :=  NULL
 ,  p_color_info5  IN  VARCHAR2  :=  NULL
 ,  p_color_comment5  IN  VARCHAR2  :=  NULL
 ,  p_userstamp  IN  VARCHAR2
 ,  x_error  OUT  BOOLEAN
 ,  x_error_text  OUT  VARCHAR2
 )
 IS
  v_status NUMBER;
BEGIN
-- na ipn van labvantage obsolete verklaard.
  NULL;
/*
  cweb_api.InsertSample
  ( pSample_name => p_sample_name
  , pSample_comment => p_sample_comment
  , pPlate_id => p_plate_id
  , pUser => p_userstamp
  , pColumn# => p_column
  , pRow# => p_row
  , pColor_info1 => p_color_info1
  , pColor_comment1 => p_color_comment1
  , pColor_info2 => p_color_info2
  , pColor_comment2 => p_color_comment2
  , pColor_info3 => p_color_info3
  , pColor_comment3 => p_color_comment3
  , pColor_info4 => p_color_info4
  , pColor_comment4 => p_color_comment4
  , pColor_info5 => p_color_info5
  , pColor_comment5 => p_color_comment5
  , pStatus => v_status
  );
  IF ( v_status = 0 )
  THEN
    x_error := FALSE;
    x_error_text := NULL;
  ELSE
    x_error := TRUE;
    cweb_api.ErrorText
    ( pCode => v_status
    , pMessage => x_error_text
    , pStatus => v_status
    );
  END IF;
  */
EXCEPTION
  WHEN OTHERS
  THEN
    x_error := TRUE;
    x_error_text := SQLERRM;
END customer_sample;

PROCEDURE  sample_complete
 (  p_sample_id  IN  VARCHAR2
 ,  x_complete  OUT  VARCHAR2
 ,  x_value  OUT  VARCHAR2
 )
 IS
  CURSOR c
  IS
    SELECT complete
    FROM   cwebv_api_sample_name
    WHERE  helix_name = p_sample_id
    ;
  r c%rowtype;
 BEGIN
 -- na ipn van labvantage obsolete verklaard.
  NULL;
/*
  OPEN  c;
  FETCH c
  INTO  r;
  CLOSE c;
  IF ( r.complete = 'Y' )
  THEN
    x_complete := 'J';
  ELSE
    x_complete := 'N';
  END IF;
  -- meetwaarde komt (nog) niet terug
  x_value := NULL;
  */
END sample_complete;

PROCEDURE  plate_attribute
 (  p_plate_id  IN  NUMBER
 ,  p_sequence  IN  NUMBER
 ,  p_name  IN  VARCHAR2
 ,  p_value  IN  VARCHAR2
 ,  x_error  OUT  BOOLEAN
 ,  x_error_text  OUT  VARCHAR2
 )
IS
  v_status NUMBER;
BEGIN
-- na ipn van labvantage obsolete verklaard.
  NULL;
/*
  cweb_api.AddPlateAttribute
  ( pPlate_id => p_plate_id
  , pSequence => p_sequence
  , pName => p_name
  , pText_value => p_value
  , pStatus => v_status
  );

  IF ( v_status = 0 )
  THEN
    x_error := FALSE;
    x_error_text := NULL;
  ELSE
    x_error := TRUE;
    x_error_text := 'ORA-'||to_char(v_status);
  END IF;
  */
EXCEPTION
  WHEN OTHERS
  THEN
    x_error := TRUE;
    x_error_text := SQLERRM;
END plate_attribute;
END  kgc_sqllims;
/

/
QUIT
