CREATE OR REPLACE PACKAGE "HELIX"."T" IS
  -- global variables:
  -- pl/sql-tables
  type string_tab is table of varchar2(100) index by binary_integer;
  -- what mode is T currently in
  tmode varchar2(10) := null;
  -- what schema do you want to look at (but always own schema)
  default_schema varchar2(30) := null;
  -- show warnings (when not forced)
  show_warnings boolean := true;
  -- should in-parameters be enclosed by wildcards
  use_wildcards boolean := true;
  -- add result of lib(rary), procedures and functions,
  -- to the result of find_obj, tables, views, synonyms, columns
  add_lib_to_obj boolean := true;
  function wildcard return varchar2;
  pragma restrict_references(wildcard, WNDS, WNPS, RNDS);
  -- help, tip and warning system
  -- remind developers of bugs and curiosa while using this package
  warnings string_tab;
  procedure warning(p_text  in varchar2,
                    p_force in boolean := false,
                    p_type  in varchar2 := 'W');
  -- show the package version number
  procedure version;
  function version return varchar2;
  -- mode to run 'T' in
  -- 'N'ovice, 'E'xpert
  -- 'S'tandard, 'A'dditional info,
  procedure setmode(p_mode in varchar2 := user);
  -- database version
  procedure dbver;
  -- who am i (non-philosophical)
  procedure whoami;
  -- show some help text
  procedure help(p_subject varchar2 := null);
  -- my own objects
  procedure own(p_object_type in varchar2 := null,
                p_object_name in varchar2 := null);
  -- find an object ('oracle objects', but also columns)
  procedure obj(p_object_name in varchar2,
                p_object_type in varchar2 := null,
                p_owner       in varchar2 := null);
  -- describe a column
  procedure coldesc(p_column_name in varchar2,
                    p_table_name  in varchar2 := null,
                    p_owner       in varchar2 := null);
  -- list of (packaged) stored procedures and functions
  -- the so-called 'library'
  procedure lib(p_object_name in varchar2 := null,
                p_object_type in varchar2 := null,
                p_owner       in varchar2 := null);
  -- find a qms-message by code, by text or helptext
  procedure msg(p_instring in varchar2);
  -- count rows in tables
  procedure count(p_table_name in varchar2, p_owner in varchar2 := null);
  -- create default list of 'dirty words'
  -- 'dirty words' are strings that should not appear in
  -- deliverable code
  chk_strings string_tab;
  procedure standard_dirty_words;
  -- add a new dirty word to the list
  procedure add_dirty_word(p_word in varchar2);
  -- remove all dirty words from list
  procedure clear_dirty_words;
  -- execute some dynamic sql
  function dyn_exec(p_statement in varchar2) return varchar2;
  -- call previous function
  procedure dyn_exec(p_statement in varchar2);
  -- compile all invalid objects
  procedure cmp;
  -- recompile an object
  procedure recmp(p_object_name in varchar2,
                  p_object_type in varchar2 := null);
  -- check stored code on 'dirty' words
  procedure chk_in_source(p_string      in varchar2 := null,
                          p_object_name in varchar2 := null);
  -- check trigger code on 'dirty' words
  procedure chk_in_trigger(p_string      in varchar2 := null,
                           p_object_name in varchar2 := null);
  -- check view code on 'dirty' words
  procedure chk_in_view(p_string      in varchar2 := null,
                        p_object_name in varchar2 := null);
  -- find a string in source- or trigger-text
  procedure find(p_string      in varchar2 := null,
                 p_object_name in varchar2 := null);
  -- display invalid objects
  procedure inv;
  -- display disabled objects (constraints/triggers)
  procedure dis;
  -- show compilation errors of procedures, functions and triggers
  procedure err(p_object_name in varchar2 := null);
  -- spool a (packaged) procedure or function
  -- the in-parameter is [<package name>.]object_name
  -- if object is not found then a search through the packages is made
  procedure sp(p_object_name in varchar2,
               p_object_type in varchar2 := null);
  procedure sp(p_object_name in varchar2,
               p_line        in number,
               p_range       in number := 15,
               p_object_type in varchar2 := 'PACKAGE BODY');
  -- show query plan after 'explain plan for ...'
  --procedure plan;   plan_table is verwijderd
END T;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."T" IS
  -- version of this package
tversion varchar2(10) := '1.0-beta';
-- Note: there are private objects below the (public) procedure 'help'
procedure help
( p_subject varchar2 := null
)
is
  v_subject varchar2(2000) := p_subject;
  v_word varchar2(100);
  v_text varchar2(2000);
  v_found boolean := false;
  -- local function
  function help_text
  ( p_item in varchar2
  )
  return varchar2
  is
    v_return_value varchar2(2000);
    v_item varchar2(30);
    v_sep varchar2(10) := chr(10)||p.confix;
  begin
    v_item := ltrim( rtrim( upper( p_item ) ) );
    v_item := replace( v_item, '_', ' ' );
    if ( v_item  in ('WARN','WARNING','WARNINGS') )
    then
      v_return_value := 'Switch the warningsystem on/off: ''exec t.show_warnings := true/false'''
                      ;
    elsif ( v_item  in ('MODE','TMODE') ) -- do not add 'SET'
    then
      v_return_value := 'T.TMODE(mode) changes all kinds of settings in this package.'
              ||v_sep|| 'Predefined modes are: ''N''ovice,''E''xpert,''S''tandard,''A''dditional info.'
              ||v_sep|| 'Type your username for mode to set your personal values (if known)'
              ||v_sep|| 'No mode is equal to ''usermode''.'
                      ;
      -- additional help
      t.help( 'SET' );
    elsif ( v_item  in ('SET','SETTING','USERVALUES') )
    then
      v_return_value := 'The outcome of t-objects depends on global variables'
              ||v_sep|| 'Individual global variables can be changed.'
              ||v_sep|| 'exec t.<variable> := <value>'
              ||v_sep|| 'exec t.default_schema := <some schema>'
              ||v_sep|| 'Note that your own schema and ''public'' are always taken in consideration'
              ||v_sep|| 'exec t.show_warnings := [true|false]'
              ||v_sep|| 'exec t.use_wildcards := [true|false]'
              ||v_sep|| 'exec t.add_lib_to_obj := [true|false]'
                      ;
    elsif ( v_item  in ('OBJ','OBJECT','OBJECTS') ) -- do not add 'SET'
    then
      v_return_value := 'T.OBJ gives you a list of objects'
              ||v_sep|| 'Optional parameters : object_name, object_type, object_owner'
                      ;
      -- additional help
      t.help( 'SET' );
    elsif ( v_item  in ('LIB','LIBRARY','PROCEDURE','PROCEDURES','FUNCTION','FUNCTIONS' ) ) -- do not add 'SET'
    then
      v_return_value := 'T.LIB gives you a list of (packaged) procedures and functions'
                      ;
      -- additional help
      t.help( 'SET' );
    elsif ( v_item  in ('INV','INVALID','INVALIDS') )
    then
      v_return_value := 'T.INV gives you a list of invalid objects in your own schema '
                      ;
    elsif ( v_item  in ('DIS','DISABLE','DISABLED') )
    then
      v_return_value := 'T.DIS gives you a list of disabled triggers andd constraints in your own schema '
                      ;
    else
      v_return_value := '';
    end if;
    return( v_return_value );
  end help_text; -- local function
begin
  if ( p_subject is null )
  then -- general messages
    p.l( 'This package is created by, maintained by and used by all developers' );
    p.l( '''Help(subject)'' gives help on a specific item' );
    p.l( 'Look into the package-code for more details' );
  else -- specific help text on a subject
    while ( v_subject is not null )
    loop
      if ( instr( v_subject, ' ' ) > 0 )
      then
        v_word := substr( v_subject
                        , 1
                        , instr( v_subject, ' ' )
                        );
        v_subject := ltrim( substr( v_subject
                                  , instr( v_subject, ' ' ) + 1
                                  )
                          );
      else
        v_word := v_subject;
        v_subject := null;
      end if;
      if ( v_word is not null )
      then
        v_text := help_text( v_word );
        if ( v_text is not null )
        then
          p.l( v_text );
          v_found := true;
        end if;
      end if;
    end loop;
    if ( not v_found )
    then
      p.l( 'No helptext on this subject found in the help system' );
    end if;
  end if;
end help;
-- private objects:
function wildcard
return varchar2
is
begin
  if ( t.use_wildcards )
  then
    return( '%' );
  else
    return( '' );
  end if;
end wildcard;
procedure default_warnings
is
  i pls_integer;
begin
  if ( t.warnings.count = 0 )
  then
    t.warnings(1) := 'Check package (T) code';
    t.warnings(2) := 'Objects granted to you through ''roles'' are not shown in the list';
  end if;
end default_warnings;
/* return the enclosing procedure or function
** in which a string was found (see chk_in_source)
*/
function enclosing_object
( p_source_name in varchar2
, p_source_type in varchar2
, p_source_line in number
)
return varchar2
is
  v_return_value varchar2(2000);
  cursor c_chk
  is
    select null
    from   user_source
    where  name = upper( p_source_name )
    and    type = upper( p_source_type )
    and    line = p_source_line
    and    upper(ltrim(text)) not like 'PROCEDURE%'
    and    upper(ltrim(text)) not like 'FUNCTION%'
    and    upper(ltrim(text)) not like 'END%'
    and    upper(ltrim(text)) not like '/*%'
    and    upper(ltrim(text)) not like '--%'
    ;
  v_dummy varchar2(1);
  cursor c
  is
    select rtrim( text, chr(10) ) text
    from   user_source
    where  name = upper( p_source_name )
    and    type in ('PROCEDURE','FUNCTION','PACKAGE BODY')
    and    line < p_source_line
    and  ( upper(ltrim(text)) like 'PROCEDURE%'
        or upper(ltrim(text)) like 'FUNCTION%'
         )
    order by line desc
    ;
begin
  -- check if incoming line is a 'code' line
  open c_chk;
  fetch c_chk
  into v_dummy;
  if ( c_chk%notfound )
  then
    close c_chk;
    return( null );
  end if;
  if ( c_chk%isopen )
  then
    close c_chk;
  end if;
  -- get a previous line starting with 'procedure' or 'function'
  open c;
  fetch c
  into v_return_value;
  close c;
  return( v_return_value );
end enclosing_object;
-- public objects:
function version
return varchar2
is
begin
  return( 'T-Version '||t.tversion );
end version;
procedure version
is
begin
  p.l( t.version );
end version
;
procedure setmode
( p_mode in varchar2
)
is
  v_mode varchar2(10) := upper( p_mode );
  v_found boolean := true;
begin
  -- userspecific modes
  if ( v_mode = 'MKL' )
  then
    t.show_warnings := false;
    t.use_wildcards := true;
    t.default_schema := null;
    t.add_lib_to_obj := false;
  elsif ( v_mode = 'TEST' )
  then
    t.show_warnings := true;
    t.use_wildcards := false;
    t.default_schema := 'TEST_FPS';
    t.add_lib_to_obj := true;
  else
    v_mode := substr( v_mode,1,1);
    -- predefined modes
    if ( v_mode = 'N' ) -- novice
    then
      t.show_warnings := true;
      t.use_wildcards := true;
      t.default_schema := '%';
      t.add_lib_to_obj := true;
    elsif ( v_mode = 'E' ) -- expert
    then
      t.show_warnings := false;
      t.use_wildcards := false;
      t.default_schema := null;
      t.add_lib_to_obj := false;
    elsif ( v_mode = 'A' ) -- add.info
    then
      t.show_warnings := true;
      t.use_wildcards := true;
      t.default_schema := '%';
      t.add_lib_to_obj := true;
    elsif ( v_mode = 'S' ) -- standard
    then
      t.show_warnings := false;
      t.use_wildcards := true;
      t.default_schema := null;
      t.add_lib_to_obj := true;
    else
      v_found := false;
    end if;
  end if;
  if ( v_found )
  then
    t.tmode := nvl( v_mode, p_mode);
    p.l( 'Global variable values set in mode '||t.tmode);
  else
    -- no change of mode
    p.l( 'Unknown mode' );
  end if;
end setmode;
procedure warning
( p_text in varchar2
, p_force in boolean
, p_type in varchar2
)
is
  v_text varchar2(100);
  v_type varchar2(10);
begin
  -- fill warnings-table if not already there
  t.default_warnings;
  begin
    v_text := t.warnings( p_text );
  exception
    when no_data_found or value_error
    then
      v_text := p_text;
  end;
  if ( show_warnings or p_force )
  then
    if ( p_type = 'E' )
    then
      v_type := 'Error';
    else
      v_type := 'Warning';
    end if;
    p.l( v_type || ': '|| v_text );
    -- identify myself...
    if ( p_force and p_text = 2 )
    then
      t.whoami;
      p.l( '... running in ''T''-mode '||t.tmode );
    end if;
  end if;
end warning;
procedure dbver
is
   cursor c
   is
     select *
     from   v$version
     ;
  v_found boolean := false;
begin
  for r in c
  loop
    p.l( r.banner );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'No record found');
  end if;
end dbver;
procedure whoami
is
   cursor c
   is
     select *
     from   global_name
     ;
  v_found boolean := false;
begin
  for r in c
  loop
    p.l( 'You are '|| user || ' on ' ||r.global_name );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'No record found');
  end if;
end whoami;
procedure own
( p_object_type in varchar2
, p_object_name in varchar2
)
is
  cursor c
  is
    select object_type
    ,      object_name
    ,      status
    from   user_objects
    where  object_type not in ( 'SYNONYM', 'INDEX')
    and    object_name not like 'RF%'
    and    object_name like t.wildcard||upper( p_object_name )||t.wildcard
    and    object_type like t.wildcard||upper( p_object_type )||t.wildcard
    order by 1, 2
    ;
  v_found boolean := false;
  v_object_type varchar2(15);
begin
  for r in c
  loop
    p.l( rpad( r.object_type, 15, ' ' )
      || rpad( r.object_name, 31, ' ' )
      || lower( r.status )
       );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    if ( p_object_type is null )
    then
      v_object_type := '';
    else
      v_object_type := ' '''||p_object_type||'''';
    end if;
    p.l( 'No' || v_object_type || ' objects '
      || 'named ''' || nvl(p_object_name,'%')
      || ''' found to call your own' );
  end if;
end own;
procedure obj
( p_object_name in varchar2
, p_object_type in varchar2
, p_owner in varchar2
)
is
  cursor c
  is
    select obj.owner
    ,      obj.object_type
    ,      obj.object_name
    ,      null parent_object
    from   all_objects obj
    where  obj.object_name like t.wildcard||upper(p_object_name)||t.wildcard
    and    obj.object_type like t.wildcard||upper(p_object_type)||t.wildcard
    and  ( obj.owner like upper(nvl(p_owner,t.default_schema))
        or owner in (user, 'PUBLIC')
         )
    union
    select owner
    ,      'COLUMN'
    ,      column_name
    ,      'in table '|| table_name
    from   all_tab_columns
    where  column_name like t.wildcard||upper(p_object_name)||t.wildcard
    and    'COLUMN' like t.wildcard||upper(p_object_type)||t.wildcard
    and  ( owner like upper(nvl(p_owner,t.default_schema))
        or owner in (user, 'PUBLIC')
         )
     ;
  v_found boolean := false;
begin
  t.warning( 2 ); -- see default_warnings for text
  for r in c
  loop
    p.l( r.owner || ' owns ' || lower( r.object_type ) || ' '
      || r.object_name || ' ' || r.parent_object
       );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'Object could not be located');
    t.warning(2, true);
  end if;
  if ( t.add_lib_to_obj )
  then
    p.l( 'Look in the ''library'' ...');
    t.lib
    ( p_object_name
    , p_object_type
    , p_owner
    );
  end if;
end obj;
procedure coldesc
( p_column_name in varchar2
, p_table_name in varchar2
, p_owner in varchar2
)
is
  cursor c
  is
    select owner
    ,      column_name
    ,      table_name
    ,      data_type
    ,      data_length
    ,      data_precision
    ,      data_scale
    ,      nullable
    ,      data_default
    from   all_tab_columns
    where  column_name like t.wildcard||upper(p_column_name)||t.wildcard
    and    table_name like t.wildcard||upper(p_table_name)||t.wildcard
    and  ( owner like upper(nvl(p_owner,t.default_schema))
        or owner in (user, 'PUBLIC')
         )
     order by owner, table_name, column_id
     ;
  v_data_length varchar2(10);
  v_nullable varchar2(8);
  v_data_default varchar2(2000); -- 'long' !!
  v_found boolean := false;
begin
  t.warning( 2 ); -- see default_warnings for text
  for r in c
  loop
    if ( r.data_type = 'NUMBER' )
    then
      if ( r.data_precision is null )
      then
        v_data_length := '';
      elsif ( r.data_scale = 0 )
      then
        v_data_length := '('||r.data_precision||')';
      else
        v_data_length := '('||r.data_precision||','||r.data_scale||')';
      end if;
    elsif ( r.data_type in ( 'DATE', 'LONG' ) )
    then
      v_data_length := '';
    else
      if( r.data_length is null )
      then
        v_data_length := '';
      else
        v_data_length := '('||r.data_length||')';
      end if;
    end if;
    if ( r.nullable = 'Y' )
    then
      v_nullable := '    NULL';
    else
      v_nullable := 'NOT NULL';
    end if;
    if ( r.data_default is null )
    then
      v_data_default := '';
    else
      v_data_default := 'Dflt.: '||r.data_default;
    end if;
    p.l( rpad(r.owner || '.' || r.table_name || '.' || r.column_name, 70, '.' )
      || ' ' || rpad(r.data_type||v_data_length, 15, ' ')
      || v_nullable || '  ' || v_data_default
       );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'Column could not be located');
    t.warning(2, true);
  end if;
end coldesc;
procedure lib
( p_object_name in varchar2
, p_object_type in varchar2
, p_owner in varchar2
)
is
  cursor c
  is
    select obj.owner
    ,      obj.object_name
    ,      to_number(null) overload
    ,      obj.object_type
    ,      null package_name
    ,      obj.object_id
    from   all_objects obj
    where  obj.object_type in ( 'PROCEDURE', 'FUNCTION' )
    and    obj.object_name like t.wildcard||upper( p_object_name )||t.wildcard
    and    obj.object_type like t.wildcard||upper( p_object_type )||t.wildcard
    and  ( obj.owner like upper(nvl(p_owner,t.default_schema))
        or obj.owner in (user, 'PUBLIC')
         )
    union
    select obj.owner
    ,      arg.object_name
    ,      to_number( arg.overload )
    ,      'FUNCTION'
    ,      arg.package_name
    ,      obj.object_id
    from   all_objects obj
    ,      all_arguments arg
    where  obj.object_type = 'PACKAGE'
    and    arg.object_name like t.wildcard||upper( p_object_name )||t.wildcard
    and    'FUNCTION' like t.wildcard||upper( p_object_type )||t.wildcard
    and  ( obj.owner like upper(nvl(p_owner,t.default_schema))
        or obj.owner in (user, 'PUBLIC')
         )
    and    arg.object_id = obj.object_id
    group by obj.owner
    ,        arg.object_name
    ,        arg.overload
    ,        arg.package_name
    ,        obj.object_id
    having   min(arg.position) = 0
    union
    select obj.owner
    ,      arg.object_name
    ,      to_number( arg.overload )
    ,      'PROCEDURE'
    ,      arg.package_name
    ,      obj.object_id
    from   all_objects obj
    ,      all_arguments arg
    where  obj.object_type = 'PACKAGE'
    and    arg.object_name like t.wildcard||upper( p_object_name )||t.wildcard
    and    'PROCEDURE' like t.wildcard||upper( p_object_type )||t.wildcard
    and  ( obj.owner like upper(nvl(p_owner,t.default_schema))
        or obj.owner in (user, 'PUBLIC')
         )
    and    arg.object_id (+) = obj.object_id
    group by obj.owner
    ,        arg.object_name
    ,        arg.overload
    ,        arg.package_name
    ,        obj.object_id
    having   nvl( min(arg.position), 1 ) = 1
    order by 2,1,5,3
    ;
  v_found boolean := false;
  v_package varchar2(100);
begin
  t.warning( 2 ); -- see default_warnings for text
  for r in c
  loop
    if ( r.package_name is not null )
    then
      v_package := 'in '||r.package_name||'('||r.overload||')';
    else
      v_package := '';
    end if;
    v_package := rpad( nvl(v_package,' '), 30, '.');
    p.l( rpad( r.object_name, 31, '.')||' '||rpad(r.object_type,10,' ')
      || v_package ||' owned by '||r.owner);
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'There is nothing for you in the library' );
    t.warning(2, true);
  end if;
end lib;
procedure msg
( p_instring in varchar2
)
is
  cursor c
  is
    select 'X' msg_code
	,      'ENG' lang_code
	,      'No messages available' msg_text
	,      to_char(null) help_text
	from   dual
	where  1=2
	;
/*
    select *
    from   qms_messages
    where ( msg_code = upper( p_instring )
         or upper( msg_text ) like t.wildcard||upper(p_instring)||t.wildcard
         or upper( help_text ) like t.wildcard||upper(p_instring)||t.wildcard
          )
    order by msg_code, lang_code
    ;
*/
  v_found boolean := false;
begin
  for r in c
  loop
    p.l( r.msg_code || ' ' || r.lang_code || ' ' || r.msg_text );
    if ( r.help_text is not null )
    then
      p.l( r.help_text );
    end if;
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'No message matches the pattern');
  end if;
end msg;
procedure count
( p_table_name in varchar2
, p_owner in varchar2 := null
)
is
  v_statement varchar2(200);
  -- faster for user-tables only
  cursor co
  is
    select table_name
    ,      owner
    from   all_tables
    where  table_name like t.wildcard||upper(p_table_name)||t.wildcard
    and    owner = user
    ;
  cursor ca
  is
    select table_name
    ,      owner
    from   all_tables
    where  table_name like t.wildcard||upper(p_table_name)||t.wildcard
    and  ( owner like upper(nvl(p_owner,t.default_schema))
        or owner in (user, 'PUBLIC')
         )
    ;
  v_count number := 0;
  v_found boolean := false;
begin
  if ( p_owner is null and
       t.default_schema is null
     )
  then -- mine
    for r in co
    loop
      v_found := true;
      v_statement := 'select count(rowid) from '||r.table_name||'';
      v_count := t.dyn_exec( v_statement );
      p.l( rpad( r.table_name, 31, ' ')
        || to_char( v_count )||' rows' );
    end loop;
  else -- all
    for r in ca
    loop
      v_found := true;
      v_statement := 'select count(rowid) from '||r.table_name||'';
      v_count := t.dyn_exec( v_statement );
      p.l( 'Table '||r.owner||'.'||r.table_name||' has '
        || to_char( v_count )||' rows' );
    end loop;
  end if;
  if ( not v_found )
  then
    p.l( 'Table not found' );
    t.warning(2, true);
  end if;
end count;
procedure standard_dirty_words
is
begin
  -- fill table with default 'dirty words'
  t.add_dirty_word( 'MKL.' ); -- user
  t.add_dirty_word( ' T.' ); -- developers toolbox
  t.add_dirty_word( 'P.L' ); -- debug messages
  t.add_dirty_word( 'DBMS_OUTPUT.PUT_LINE' ); -- debug messages
  t.add_dirty_word( '??' ); -- unsolved questions / open issues
end standard_dirty_words;
procedure add_dirty_word
( p_word in varchar2
)
is
  i pls_integer := nvl( chk_strings.last, 0 ) + 1;
begin
  chk_strings(i) := p_word;
end add_dirty_word;
procedure clear_dirty_words
is
begin
  chk_strings.delete;
end;
function dyn_exec
( p_statement in varchar2 )
return varchar2
is
  -- remove <CR>, spaces and <CR> again (if statement started off with spaces)...
  v_chk_statement varchar2(2000) := ltrim( ltrim( ltrim( p_statement, chr(10)
                                                       )
                                                , chr(10)
                                                )
                                         );
begin
  declare
    cid integer;
    rows_processed integer;
    v_result varchar2(2000);
    v_statement varchar2(2000) := null;
    v_with_bind_var boolean := true;
  begin
     v_statement := v_chk_statement;
     -- v_chk_statement is now only used to find strings in...
     v_chk_statement := upper( v_chk_statement );
     -- Analyze the incoming statement ...
     if ( instr( v_chk_statement
               , 'BEGIN'
               ) > 0 )
     then -- an anonymous PL/SQL-block
       v_statement := v_statement; -- no change
     elsif ( instr( v_chk_statement
                  , 'SELECT'
                  ) = 1 )
     then -- a select statement
       if ( instr( v_chk_statement
                 , 'INTO'
                 ) > 0
           )
       then -- a select statement with 'into'
         v_statement := substr( v_statement
                              , 1
                              , instr( upper(v_statement), 'INTO' ) - 1
                              )
                     || 'into :result ' || chr(10)
                     || substr( v_statement
                              , instr( upper(v_statement), 'FROM ' )
                              )
                      ;
       else -- a select statement without 'into'
         v_statement := substr( v_statement
                              , 1
                              , instr( upper(v_statement), 'FROM ' ) - 1
                              )
                     || 'into :result ' || chr(10)
                     || substr( v_statement
                              , instr( upper(v_statement), 'FROM ' )
                              )
                      ;
       end if;
       v_statement := 'begin ' || chr(10)
                   || v_statement || ';' || chr(10)
                   || 'exception when others then :result := null;' || chr(10)
                   || 'end;'
                    ;
     else -- could be anything, most likely a dml-statement
       v_statement := rtrim( v_statement, ';' );
     end if;
     if ( instr( upper( v_statement )
               , ':RESULT'
               ) > 0 )
     then
       v_with_bind_var := true;
     else
       v_with_bind_var := false;
     end if;
     /* open new cursor and return cursor id. */
     cid := dbms_sql.open_cursor;
     /* parse string */
     dbms_sql.parse( cid
                   , v_statement
                   , dbms_sql.native
                   );
          if ( v_with_bind_var )
     then
       /* bind variables */
       /* don't forget the max length of the result !!! */
       dbms_sql.bind_variable( cid
                             , ':result'
                             , v_result
                             , 2000 );
     end if;
     /* execute the parsed statement */
     rows_processed := dbms_sql.execute( cid );
     if ( v_with_bind_var )
     then
       /* retrieve the value */
       dbms_sql.variable_value( cid
                              , ':result'
                              , v_result );
     end if;
     /* close cursor. */
     dbms_sql.close_cursor( cid );
     return( v_result );
  exception
    when others
    then
      if ( dbms_sql.is_open( cid ) )
      then
        dbms_sql.close_cursor( cid );
      end if;
      raise_application_error
      ( -20000
      , 'Error in ' || v_statement || chr(10) || chr(10)
     || sqlerrm
      );
  end ;
exception
  when others then
    if (dbms_utility.format_error_stack is null)
    then
      raise_application_error
      ( -20000
      , 'Dynamic Exec Error: ' || SQLERRM );
    else
      raise;
    end if;
end dyn_exec;
procedure dyn_exec
( p_statement in varchar2
)
is
  v_dummy varchar2(100);
begin
  v_dummy := t.dyn_exec( p_statement );
end  dyn_exec;
procedure cmp
is
  cursor c
  is
    select 'alter ' || object_type ||
           ' ' || object_name || ' compile;' compile_statement
    from   user_objects
    where  status <> 'VALID'
    and    object_type in ( 'PROCEDURE', 'FUNCTION', 'PACKAGE' , 'TRIGGER', 'VIEW' )
    and    object_name not in ( 'T', 'P' )
    union
    select 'alter package ' ||
            object_name || ' compile body;'
    from   user_objects
    where  status <> 'VALID'
    and    object_type in ( 'PACKAGE BODY' )
    and    object_name not in ( 'T', 'P' )
    ;
  cursor c_inv
  is
    select object_type
    ,      object_name
    from   user_objects
    where  status <> 'VALID'
    and    object_type in ( 'PROCEDURE', 'FUNCTION'
                          , 'PACKAGE', 'PACKAGE BODY'
                          , 'TRIGGER', 'VIEW'
                          )
    ;
  v_first boolean := true;
  v_found boolean := false;
begin
  for r in c
  loop
    p.l( chr(10)||r.compile_statement );
    begin
      t.dyn_exec( r.compile_statement );
    exception
      when others
      then
        null;
    end;
  end loop;
  for r2 in c_inv
  loop
    if ( v_first )
    then
      p.l( 'Still invalid are:' );
      v_first := false;
      v_found := true;
    end if;
    p.l( r2.object_type, r2.object_name );
  end loop;
  if ( not v_found )
  then
    p.l( 'All seem to be valid' );
  else
    t.err;
  end if;
end cmp;
procedure recmp
( p_object_name in varchar2
, p_object_type in varchar2 := null
)
is
  v_statement varchar2(32767);
  cid integer;
  rows_processed integer;
  cursor c_obj
  is
    select object_type
    ,      object_name
    from   user_objects
    where  object_name like t.wildcard||upper(p_object_name)||t.wildcard
    and    object_type like t.wildcard||upper(nvl(p_object_type
                                                 ,'PACKAGE BODY'
                                                 )
                                             )||t.wildcard
    and    object_name not in ( 'T', 'P' )
    ;
  v_found boolean := false;
begin
  for r in c_obj
  loop
    v_found := true;
    if ( r.object_type in ( 'PROCEDURE','FUNCTION','PACKAGE','VIEW','TRIGGER') )
    then
      v_statement := 'ALTER ' || r.object_type ||' '|| r.object_name || ' COMPILE' ;
    else
      v_statement := 'ALTER PACKAGE '|| r.object_name || ' COMPILE BODY' ;
    end if;
    p.l( v_statement );
    dyn_exec( v_statement );
  end loop;
  if ( not v_found )
  then
    p.l( 'Object to recompile not found in your schema' );
  end if;
  t.err;
end recmp;
procedure chk_in_source
( p_string in varchar2
, p_object_name in varchar2
)
is
  cursor c
  ( l_string in varchar2
  )
  is
    select owner
    ,      type
    ,      name
    ,      rtrim( text, chr(10) ) text
    ,      line
    from   all_source
    where upper(text) like '%'||upper( l_string )||'%'
    and   name like t.wildcard||upper( p_object_name )||t.wildcard
    and   name not in ( 'T', 'P' )
    and  ( owner like upper(t.default_schema)
        or owner in (user, 'PUBLIC')
         )
     order by name, type, line
     ;
  i pls_integer;
  v_string varchar2(2000);
begin
  if ( p_string is not null )
  then
    t.add_dirty_word( p_string );
  else
    t.standard_dirty_words;
  end if;
  -- check for all entries in the pl/sql-table...
  i := t.chk_strings.first;
  while ( i is not null )
  loop
    v_string := t.chk_strings( i );
    p.l( 'Check for string', v_string );
    for r in c( v_string )
    loop
      p.l( 'Found in ' ||r.owner||'.'|| r.type || ' ' || r.name, rtrim(r.text) );
      -- find procedure or function in where the string appears
      declare
        v_enclosing_object varchar2(2000);
      begin
        v_enclosing_object := t.enclosing_object
                              ( r.name
                              , r.type
                              , r.line
                              );
        if ( v_enclosing_object is not null )
        then
          p.l( '   -> (enclosing object)',rtrim(v_enclosing_object) );
        end if;
      end;
    end loop;
    i := t.chk_strings.next(i);
  end loop;
  t.clear_dirty_words;
end chk_in_source;
procedure chk_in_trigger
( p_string in varchar2 := null
, p_object_name in varchar2 := null
)
is
  cursor c
  is
     select table_name
     ,      trigger_name
     ,      trigger_body
     from   all_triggers
     where  ( trigger_name like t.wildcard||upper( p_object_name )||t.wildcard
           or table_name  like t.wildcard||upper( p_object_name )||t.wildcard
            )
    and  ( owner like upper(t.default_schema)
        or owner in (user, 'PUBLIC')
         )
     order by table_name, trigger_name
     ;
  v_text varchar2(32767); -- trigger text
  v_pos number;
  v_start_pos number;
  v_end_pos number;
  v_line varchar2(100); -- one line of trigger text
  i pls_integer;
  v_string varchar2(2000);
begin
  if ( p_string is not null )
  then
    t.add_dirty_word( p_string );
  else
    t.standard_dirty_words;
  end if;
  -- check for all entries in the pl/sql-table...
  i := t.chk_strings.first;
  while ( i is not null )
  loop
    v_string := t.chk_strings( i );
    p.l( 'Check for string', v_string );
    for r in c
    loop
      v_text := r.trigger_body;
      v_pos := instr( upper( v_text ), upper( v_string ) );
      if ( v_pos > 0 )
      then
        v_start_pos := greatest( instr( substr( v_text, 1, v_pos )
                                      , chr(10)
                                      , -1
                                      )
                                , 1
                                );
        v_end_pos := least( instr( substr( v_text, v_start_pos + 1)
                                 , chr(10)
                                 )
                          , length( v_text )
                          );
        v_line := substr( v_text
                        , v_start_pos
                        , v_end_pos
                        );
        p.l( 'Found in '|| r.trigger_name || ' on ' || r.table_name, v_line );
      end if;
    end loop;
    i := t.chk_strings.next(i);
  end loop;
  t.clear_dirty_words;
end chk_in_trigger;
procedure chk_in_view
( p_string in varchar2 := null
, p_object_name in varchar2 := null
)
is
  cursor c
  is
     select owner
     ,      view_name
     ,      text
     from   all_views
     where  view_name  like t.wildcard||upper( p_object_name )||t.wildcard
    and  ( owner like upper(t.default_schema)
        or owner in (user, 'PUBLIC')
         )
     order by view_name
     ;
  v_text varchar2(32767); -- view text
  v_pos number;
  v_start_pos number;
  v_end_pos number;
  v_line varchar2(100); -- one line of view text
  i pls_integer;
  v_string varchar2(2000);
begin
  if ( p_string is not null )
  then
    t.add_dirty_word( p_string );
  else
    t.standard_dirty_words;
  end if;
  -- check for all entries in the pl/sql-table...
  i := t.chk_strings.first;
  while ( i is not null )
  loop
    v_string := t.chk_strings( i );
    p.l( 'Check for string', v_string );
    for r in c
    loop
      v_text := r.text;
      v_pos := instr( upper( v_text ), upper( v_string ) );
      if ( v_pos > 0 )
      then
        v_start_pos := greatest( instr( substr( v_text, 1, v_pos )
                                      , chr(10)
                                      , -1
                                      )
                                , 1
                                );
        v_end_pos := least( instr( substr( v_text, v_start_pos + 1)
                                 , chr(10)
                                 )
                          , least( length( v_text ), 100 )
                          );
        v_line := substr( v_text
                        , v_start_pos
                        , v_end_pos
                        );
        p.l( 'Found in '|| r.owner||'.'||r.view_name,  rtrim( v_line ) );
      end if;
    end loop;
    i := t.chk_strings.next(i);
  end loop;
  t.clear_dirty_words;
end chk_in_view;
procedure find
( p_string in varchar2
, p_object_name in varchar2
)
is
begin
  p.l( 'Check user source...' );
  t.chk_in_source( p_string, p_object_name);
  p.l( 'Check user triggers...' );
  t.chk_in_trigger( p_string, p_object_name);
  p.l( 'Check user views...' );
  t.chk_in_view( p_string, p_object_name);
end find;
procedure inv
is
  cursor c
  is
    select object_type
    ,      object_name
    ,      status
    from   user_objects
    where  status <> 'VALID'
    order by 1,2
    ;
  v_found boolean := false;
begin
  for r in c
  loop
    p.l( initcap(r.object_type) || ' ' || r.object_name
      || ' is ' || lower( r.status ) );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'All user objects are valid' );
  end if;
end inv;
procedure dis
is
  cursor c
  is
    select 'CONSTRAINT' object_type
    ,      constraint_name object_name
    ,      table_name
    ,      status
    from   user_constraints
    where status <> 'ENABLED'
    union
    select 'TRIGGER'
    ,      trigger_name
    ,      table_name
    ,      status
    from   user_triggers
    where status <> 'ENABLED'
    order by 3,1,2
    ;
  v_found boolean := false;
begin
  for r in c
  loop
    p.l( initcap(r.object_type) || ' ' || r.object_name || ' on ' || r.table_name
      || ' is ' || lower(r.status) );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'All user constraints and triggers are enabled' );
  end if;
end dis;
procedure err
( p_object_name in varchar2
)
is
  cursor c
  is
    select type
    ,      name
    ,      line||'/'||position pos
    ,      text
    from   user_errors
    where  name like t.wildcard||upper(p_object_name)||t.wildcard
    order by name
    ,        type
    ,        sequence
    ;
  v_found boolean := false;
begin
  p.l( 'Invalid objects:' );
  t.inv;
  p.l( 'Known errors' );
  for r in c
  loop
    p.l( rpad(r.type||' '||r.name,22,' ')
      || rpad(r.pos,8,' ')||r.text
       );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'None' );
  end if;
end err;
procedure sp
( p_object_name in varchar2
, p_object_type in varchar2
)
is
  cursor c
  is
    select obj.object_name
    ,      obj.object_type
    ,      null package_name
    ,      null overload
    from   user_objects obj
    where  obj.object_type in ( 'PROCEDURE', 'FUNCTION' )
    and    obj.object_name like upper( p_object_name )
    and    obj.object_type like t.wildcard||upper( p_object_type )||t.wildcard
    union
    select arg.object_name
    ,      'FUNCTION'
    ,      arg.package_name
    ,      arg.overload
    from   user_objects obj
    ,      user_arguments arg
    where  obj.object_type = 'PACKAGE'
    and    arg.object_name like upper( p_object_name )
    and    'FUNCTION' like t.wildcard||upper( p_object_type )||t.wildcard
    and    arg.object_id = obj.object_id
    group by arg.object_name
    ,        arg.package_name
    ,        arg.overload
    having   min(arg.position) = 0
    union
    select arg.object_name
    ,      'PROCEDURE'
    ,      arg.package_name
    ,      arg.overload
    from   user_objects obj
    ,      user_arguments arg
    where  obj.object_type = 'PACKAGE'
    and    arg.object_name like upper( p_object_name )
    and    'PROCEDURE' like t.wildcard||upper( p_object_type )||t.wildcard
    and    arg.object_id (+) = obj.object_id
    group by arg.object_name
    ,        arg.package_name
    ,        arg.overload
    having   nvl( min(arg.position), 1 ) = 1
    order by 1,3,2
    ;
  v_found boolean := false;
  v_type varchar2(12);
  -- startline of object in a package
  -- note: the text is printed until 'end[ <name> ];[ ]'
  cursor c_line
  ( l_package in varchar2
  , l_name in varchar2
  , l_type in varchar2
  )
  is
    select min(line)
    from   user_source
    where  name = l_package
    and    type = 'PACKAGE BODY'
    and    upper( text ) like upper( l_type )||'%'||upper(l_name)||'%'
    ;
  v_startline number;
  cursor c_txt
  ( l_name in varchar2
  , l_type in varchar2
  , l_startline in number
  )
  is
    select text
    ,      line
    from   user_source
    where  name = l_name
    and    type = l_type
    and    line >= l_startline
    order by line
    ;
  v_txt varchar2(100);
begin
  for r in c
  loop
    if ( c%rowcount > 1 )
    then
      p.l( chr(10) );
      p.l( rpad( '*', 70, '*' ) );
    end if;
    if ( r.package_name is not null )
    then
      v_type := 'PACKAGE BODY';
      p.l( 'Source code for '||r.object_type||' '||r.object_name
        || ' in PACKAGE '||r.package_name||':');
    else
      v_type := r.object_type;
      p.l( 'Source code for '||r.object_type||' '||r.object_name||':');
    end if;
    p.l( chr(10) );
    if ( r.package_name is not null )
    then
      open c_line( r.package_name
                 , r.object_name
                 , r.object_type
                 );
      fetch c_line
      into  v_startline;
      close c_line;
    else
      v_startline := 1;
    end if;
    for r_txt in c_txt( nvl( r.package_name, r.object_name )
                      , v_type
                      , v_startline
                      )
    loop
      v_txt := rtrim( r_txt.text, chr(10) );
      p.l( lpad( r_txt.line, 4, ' '), v_txt );
      if ( upper( r_txt.text ) like 'END%;%' )
      then
        exit;
      end if;
    end loop;
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'Procedure or function not found in your schema' );
  end if;
end sp;
procedure sp
( p_object_name in varchar2
, p_line in number
, p_range in number -- default 15
, p_object_type in varchar2 -- default 'PACKAGE BODY'
)
is
  v_found boolean := false;
  cursor c_txt
  ( l_name in varchar2
  , l_type in varchar2
  )
  is
    select text
    ,      line
    from   user_source
    where  name = upper( l_name )
    and    type like t.wildcard||upper( l_type )||t.wildcard
    and    line between p_line - nvl( p_range, 0 )
                    and p_line + nvl( p_range, 0 )
    order by line
    ;
  v_txt varchar2(1000);
begin
  p.l( 'Source in '||p_object_type||' '||p_object_name );
  p.l( 'From '|| to_char( p_line - nvl( p_range, 0 ) )
    || ' until '|| to_char( p_line + nvl( p_range, 0 ) )||':'
     );
  p.l( chr(10) );
  for r_txt in c_txt( p_object_name
                    , p_object_type
                    )
  loop
    v_txt := rtrim( r_txt.text, chr(10) );
    p.l( lpad( r_txt.line, 4, ' '), v_txt );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'Procedure or function not found in your schema' );
  end if;
end sp;
-- plantable verwijderd
/*
procedure plan
is
  cursor c
  is
    select lpad( ' ', 2*(level-1) ) || operation
        || ' ' || options
        || ' ' || substr(object_name, 1, 32)
	    || ' ' || decode( id, 0, 'Cost = ' || position ) query_plan
    from   plan_table
    start with	id = 0
    connect by prior id = parent_id
  ;
  v_found boolean := false;
begin
  p.l( 'Query plan' );
  for r in c
  loop
    p.l( r.query_plan );
    v_found := true;
  end loop;
  if ( not v_found )
  then
    p.l( 'No query explained yet');
  else
    begin
      delete
      from plan_table
      ;
    end;
  end if;
end plan;
*/
end t;
/

/
QUIT
