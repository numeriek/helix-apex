CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_GAF"
AS
   /*
   ================================================================================
   Package Name : gen_pck_gaf
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_feedback
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   PROCEDURE p_ins_gen_appl_feedback(i_gaf_rec IN gen_appl_feedback%ROWTYPE);
END gen_pck_gaf;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_GAF"
AS
   /*
   ================================================================================
   Package Name : gen_pck_gaf
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_feedback
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */

   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   --
   PROCEDURE p_ins_gen_appl_feedback(i_gaf_rec IN gen_appl_feedback%ROWTYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'p_ins_gen_appl_feedback';
      l_mte_rec         gen_mail_templates%ROWTYPE;
      l_feedback_id     NUMBER;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      INSERT INTO gen_appl_feedback(application_id
                                   ,application_name
                                   ,page_id
                                   ,page_name
                                   ,feedback_user
                                   ,feedback_date
                                   ,feedback_type
                                   ,feedback)
           VALUES (i_gaf_rec.application_id
                  ,i_gaf_rec.application_name
                  ,i_gaf_rec.page_id
                  ,i_gaf_rec.page_name
                  ,i_gaf_rec.feedback_user
                  ,i_gaf_rec.feedback_date
                  ,i_gaf_rec.feedback_type
                  ,i_gaf_rec.feedback)
        RETURNING id
             INTO l_feedback_id;
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);


      -- Use apex mail to send the actual mail
      gen_pck_mail_templates.p_get_mail_template('FEEDBACK_MAIL'
                                              ,l_mte_rec);

      l_mte_rec.recipient      :=
         gen_pck_general.f_get_appl_parameter_value('FEEDBACK_MAILGROUP');
      l_mte_rec.MESSAGE      :=
         REPLACE(
            REPLACE(
               REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(l_mte_rec.MESSAGE
                                                      ,'#DATE_TIME#'
                                                      ,i_gaf_rec.feedback_date)
                                              ,'#FEEDBACK_ID#'
                                              ,l_feedback_id)
                                      ,'#APPLICATION_NAME#'
                                      ,i_gaf_rec.application_name)
                              ,'#PAGE_NAME#'
                              ,i_gaf_rec.page_name)
                      ,'#USER_NAME#'
                      ,i_gaf_rec.feedback_user)
              ,'#FEEDBACK_TYPE#'
              ,i_gaf_rec.feedback_type)
           ,'#FEEDBACK#'
           ,i_gaf_rec.feedback);
      l_mte_rec.message_html      :=
         REPLACE(
            REPLACE(
               REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(l_mte_rec.message_html
                                                      ,'#DATE_TIME#'
                                                      ,i_gaf_rec.feedback_date)
                                              ,'#FEEDBACK_ID#'
                                              ,l_feedback_id)
                                      ,'#APPLICATION_NAME#'
                                      ,i_gaf_rec.application_name)
                              ,'#PAGE_NAME#'
                              ,i_gaf_rec.page_name)
                      ,'#USER_NAME#'
                      ,i_gaf_rec.feedback_user)
              ,'#FEEDBACK_TYPE#'
              ,i_gaf_rec.feedback_type)
           ,'#FEEDBACK#'
           ,i_gaf_rec.feedback);
      gen_pck_mail_templates.p_send_mail(l_mte_rec);



   END p_ins_gen_appl_feedback;
END gen_pck_gaf;
/

/
QUIT
