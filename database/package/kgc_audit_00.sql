CREATE OR REPLACE PACKAGE "HELIX"."KGC_AUDIT_00" IS

-- verwijder alle bestaande audit db-triggers.
PROCEDURE drop_audit_triggers;

-- creeer db-triggers op basis van rijen in kgc_audit_def.
PROCEDURE create_audit_triggers
( p_audd_id_start IN  NUMBER := NULL
, x_audd_id_fout  OUT NUMBER -- NULL wil zeggen alle rijen verwerkt
, x_fout_tekst    OUT VARCHAR2
)
;

-- haal gegenereerde trigger op
FUNCTION trigger_tekst
( p_audd_id IN NUMBER
)
RETURN VARCHAR2;

-- haal voor gebruiker zinvolle beschijving van een id. op
FUNCTION context
( p_tabel_naam IN VARCHAR2
, p_id IN NUMBER
)
RETURN VARCHAR2;

-- registreer de actie
PROCEDURE loggen
( p_tabel IN VARCHAR2
, p_kolom IN VARCHAR2
, p_aktie IN VARCHAR2
, p_id IN NUMBER
, p_beschrijving IN VARCHAR2
, p_waarde_oud IN VARCHAR2
, p_waarde_nieuw IN VARCHAR2
, p_identificerende_kolommen IN VARCHAR2
)
;
END KGC_AUDIT_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_AUDIT_00" IS
PROCEDURE drop_audit_triggers
IS
  v_owner VARCHAR2(30) := kgc_util_00.appl_owner;
  CURSOR trig_cur
  IS
    SELECT trigger_name
    FROM   user_triggers
    WHERE  table_owner = v_owner
    AND    trigger_name LIKE 'KGC_AUDIT_%'
  ;
  v_sqltekst VARCHAR2(100);
BEGIN
  FOR r_trig IN trig_cur
  LOOP
    v_sqltekst := 'DROP TRIGGER '||'"'||v_owner||'".'||r_trig.trigger_name;
    kgc_util_00.dyn_exec(v_sqltekst);
  END LOOP;
END drop_audit_triggers;

PROCEDURE create_audit_triggers
( p_audd_id_start IN  NUMBER := NULL
, x_audd_id_fout  OUT NUMBER
, x_fout_tekst    OUT VARCHAR2
)
IS
  v_owner VARCHAR2(30) := kgc_util_00.appl_owner;

  CURSOR audd_cur
  IS
    SELECT audd_id
    ,      tabel_naam
    ,      kolom_naam
    ,      aktie
    ,      identificerende_kolommen
    ,      def_where
    ,      aktief
    ,      beschrijving
    FROM   kgc_audit_def
    WHERE  aktief = 'J'
    AND    audd_id > NVL( p_audd_id_start, 0 )
    ORDER BY audd_id
  ;

  CURSOR col_cur
    ( b_tabel_naam IN VARCHAR2
    , b_kolom_naam IN VARCHAR2
    )
  IS
    SELECT data_type
    FROM   all_tab_columns
    WHERE  owner       = v_owner
    AND    table_name  = b_tabel_naam
    AND    column_name = b_kolom_naam
    ;
  v_col_rec col_cur%ROWTYPE;

  v_trigger_naam VARCHAR2(100);
  v_sqltekst VARCHAR2(2000);
  v_action VARCHAR2(10);
  v_tabel_alias VARCHAR2(4);
  v_kolomnaam VARCHAR2(100);
  v_old_kolomnaam VARCHAR2(100);
  v_new_kolomnaam VARCHAR2(100);
  v_identificerende_kolommen VARCHAR2(2000);
  v_end_if_toevoegen BOOLEAN;
BEGIN
  -- Creeer voor iedere rij uit KGC_AUDIT_DEF waarvoor geldt aktief = 'J' een trigger. --
  FOR r_audd IN audd_cur
  LOOP
    v_tabel_alias := kgc_util_00.tabel_alias
                       ( p_owner     => v_owner
                       , p_tabelnaam => r_audd.tabel_naam
                       );

    IF r_audd.identificerende_kolommen IS NULL
    THEN
      v_identificerende_kolommen := 'NULL';
    ELSE
      v_identificerende_kolommen := r_audd.identificerende_kolommen;
    END IF;

    IF r_audd.kolom_naam IS NULL
    THEN
      v_kolomnaam     := 'NULL';
      v_old_kolomnaam := 'NULL';
      v_new_kolomnaam := 'NULL';
    ELSE
      v_kolomnaam     := ''''||r_audd.kolom_naam||'''';
      v_old_kolomnaam := ':old.'||r_audd.kolom_naam;
      v_new_kolomnaam := ':new.'||r_audd.kolom_naam;
    END IF;

    IF    r_audd.aktie = 'INS'
    THEN
      v_action := 'INSERT';
      -- De eventueel opgegeven kolom negeren; er worden geen waarden gelogd bij de INSERT
      v_kolomnaam     := 'NULL';
      v_old_kolomnaam := 'NULL';
      v_new_kolomnaam := 'NULL';
    ELSIF r_audd.aktie = 'UPD'
    THEN
      v_action := 'UPDATE';
    ELSIF r_audd.aktie = 'DEL'
    THEN
      v_action := 'DELETE';
      -- De eventueel opgegeven kolom negeren; er worden geen waarden gelogd bij de DELETE
      v_kolomnaam     := 'NULL';
      v_old_kolomnaam := 'NULL';
      v_new_kolomnaam := 'NULL';
    END IF;

    -- opbouwen sqltekst
    v_trigger_naam := '"'||v_owner||'".KGC_AUDIT_'||TO_CHAR(r_audd.audd_id)||'_TRG';

    v_sqltekst := 'CREATE OR REPLACE TRIGGER ' || v_trigger_naam||CHR(10)
               || 'AFTER '||v_action||CHR(10)
               || 'ON '||r_audd.tabel_naam||' FOR EACH ROW'||CHR(10);

    IF r_audd.def_where IS NOT NULL
    THEN
      v_sqltekst := v_sqltekst || 'WHEN (' || r_audd.def_where||')'||CHR(10);
    END IF;

    v_sqltekst := v_sqltekst || 'BEGIN'||CHR(10);

    -- Alleen loggen als er een verschil is, waarbij afhankelijk van het kolom-type een andere NVL-clausule wordt toegepast
    v_end_if_toevoegen := FALSE;
    IF ( r_audd.aktie = 'UPD' AND
         r_audd.kolom_naam IS NOT NULL )
    THEN
      OPEN  col_cur( b_tabel_naam => r_audd.tabel_naam
                   , b_kolom_naam => r_audd.kolom_naam
                   );
      FETCH col_cur
      INTO  v_col_rec;
      CLOSE col_cur;
      IF    v_col_rec.data_type = 'VARCHAR2'
      THEN
         v_sqltekst := v_sqltekst ||'  IF nvl( :old.'||r_audd.kolom_naam||', chr(2) ) <>  nvl( :new.'||r_audd.kolom_naam  ||', chr(2) ) ' ||CHR(10)
                                  ||'  THEN'         ||CHR(10);
         v_end_if_toevoegen := TRUE;
      ELSIF v_col_rec.data_type = 'NUMBER'
      THEN
         v_sqltekst := v_sqltekst ||'  IF nvl( :old.'||r_audd.kolom_naam||', -9 ) <>  nvl( :new.'||r_audd.kolom_naam  ||', -9 ) ' ||CHR(10)
                                  ||'  THEN'         ||CHR(10);
         v_end_if_toevoegen := TRUE;
      ELSIF v_col_rec.data_type = 'DATE'
      THEN
         v_sqltekst := v_sqltekst ||'  IF nvl( :old.'||r_audd.kolom_naam||', to_date(''01-01-0001'', ''dd-mm-yyyy'') ) <>  nvl( :new.'||r_audd.kolom_naam  ||', to_date(''01-01-0001'', ''dd-mm-yyyy'') ) ' ||CHR(10)
                                  ||'  THEN'         ||CHR(10);
         v_end_if_toevoegen := TRUE;
      END IF;
    END IF;

    -- De trigger-code die de auditing uit gaat voeren
    v_sqltekst := v_sqltekst ||'  kgc_audit_00.loggen( p_tabel                    => '''||r_audd.tabel_naam||''''||CHR(10)
                             ||'                     , p_kolom                    => '||v_kolomnaam||CHR(10)
                             ||'                     , p_aktie                    => '''||r_audd.aktie||''''||CHR(10)
                             ||'                     , p_id                       => nvl(:new.'||v_tabel_alias||'_id'||CHR(10)
                             ||'                                                        ,:old.'||v_tabel_alias||'_id'||CHR(10)
                             ||'                                                        )'                           ||CHR(10)
                             ||'                     , p_beschrijving             => '''||r_audd.beschrijving ||''''||CHR(10)
                             ||'                     , p_waarde_oud               => '||v_old_kolomnaam||CHR(10)
                             ||'                     , p_waarde_nieuw             => '||v_new_kolomnaam||CHR(10)
                             ||'                     , p_identificerende_kolommen => '||v_identificerende_kolommen||CHR(10)
                             ||'                     );'||CHR(10)
                             ;

    IF v_end_if_toevoegen
    THEN
      v_sqltekst := v_sqltekst || '  END IF;'||CHR(10);
    END IF;

    v_sqltekst := v_sqltekst || 'END;';

    BEGIN
      kgc_util_00.dyn_exec(v_sqltekst);
    EXCEPTION
    WHEN OTHERS
    THEN
      -- verwijder foutieve trigger
      BEGIN
        v_sqltekst := 'DROP TRIGGER '||v_trigger_naam;
        kgc_util_00.dyn_exec(v_sqltekst);
      EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
     END;
      -- meld fout terug aan form
      x_audd_id_fout := r_audd.audd_id;
      x_fout_tekst   := 'Fout in trigger ' ||v_trigger_naam    ||CHR(10)||CHR(10)||
                        'Tabelnaam= '      ||r_audd.tabel_naam ||CHR(10)||
                        'Kolomnaam= '      ||v_kolomnaam       ||CHR(10)||
                        'Aktie= '          ||r_audd.aktie      ||CHR(10)||
                        'Beschrijving= '   ||r_audd.beschrijving;
      RETURN;
    END;
  END LOOP;
  x_audd_id_fout := NULL;
  x_fout_tekst   := NULL;

END create_audit_triggers;

FUNCTION trigger_tekst
( p_audd_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  v_owner VARCHAR2(30) := kgc_util_00.appl_owner;
  v_triggernaam VARCHAR2(30);
  --
  CURSOR trig_cur
  IS
    SELECT trigger_body
    FROM   all_triggers
    WHERE  trigger_name = v_triggernaam
    AND    owner = v_owner
    ;
BEGIN
  v_triggernaam := 'KGC_AUDIT_'||TO_CHAR(p_audd_id) || '_TRG';
  OPEN  trig_cur;
  FETCH trig_cur
  INTO  v_return;
  CLOSE trig_cur;
  RETURN( v_return );
END trigger_tekst;

FUNCTION context
( p_tabel_naam IN VARCHAR2
, p_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(250);
  v_tabel_alias VARCHAR2(4);
  v_owner VARCHAR2(30) := kgc_util_00.appl_owner;
BEGIN
  v_tabel_alias := KGC_UTIL_00.tabel_alias
                   ( p_owner => v_owner
	               , p_tabelnaam => p_tabel_naam
                   );
  v_return := kgc_util_00.pk2uk
              ( p_entiteit => v_tabel_alias
              , p_id => p_id
              , p_kolommen => NULL
              );
  RETURN(v_return);
END context;

PROCEDURE LOGGEN
( p_tabel                    IN VARCHAR2
, p_kolom                    IN VARCHAR2
, p_aktie                    IN VARCHAR2
, p_id                       IN NUMBER
, p_beschrijving             IN VARCHAR2
, p_waarde_oud               IN VARCHAR2
, p_waarde_nieuw             IN VARCHAR2
, p_identificerende_kolommen IN VARCHAR2
)
IS
  v_dummy VARCHAR2(1) := NULL;
BEGIN
  INSERT INTO KGC_AUDIT_LOG
  (gebruiker
  ,datum_tijd
  ,tabel_naam
  ,kolom_naam
  ,aktie
  ,id
  ,beschrijving
  ,waarde_oud
  ,waarde_nieuw
  ,identificatie
  )
  VALUES
  (USER
  ,SYSDATE
  ,UPPER(p_tabel)
  ,UPPER(p_kolom)
  ,UPPER(p_aktie)
  ,p_id
  ,p_beschrijving
  ,p_waarde_oud
  ,p_waarde_nieuw
  ,p_identificerende_kolommen
  );

--EXCEPTION  WHEN OTHERS
--THEN
--  NULL;
END loggen;
END KGC_AUDIT_00;
/

/
QUIT
