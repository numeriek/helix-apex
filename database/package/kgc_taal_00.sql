CREATE OR REPLACE PACKAGE "HELIX"."KGC_TAAL_00" IS
FUNCTION id
( p_code IN VARCHAR2
)
RETURN NUMBER;
END KGC_TAAL_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_TAAL_00" IS
FUNCTION id
( p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR taal_cur
  IS
    SELECT taal_id
    FROM   kgc_talen
    WHERE  code = UPPER( p_code )
    ;
  v_id NUMBER;
-- PL/SQL Block
BEGIN
  OPEN  taal_cur;
  FETCH taal_cur
  INTO  v_id;
  CLOSE taal_cur;
  RETURN( v_id );
END id;
END kgc_taal_00;
/

/
QUIT
