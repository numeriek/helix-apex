CREATE OR REPLACE PACKAGE "HELIX"."KGC_TECO_00" IS
-- Package Specification
-- package gecreeerd tbv KGCTEC010
--
FUNCTION context_tekst_bestaat_jn
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER)
RETURN VARCHAR2;
--
PROCEDURE insert_teksten_context
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER)
;
--
PROCEDURE delete_teksten_context
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER);
--
PROCEDURE insert_als_teco_niet_bestaat
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER
, p_bestaat      OUT VARCHAR2 );
--
PROCEDURE  delete_als_teco_bestaat
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER
, p_bestaat      OUT VARCHAR2);
--
END KGC_TECO_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_TECO_00" IS
-- PL/SQL Block
FUNCTION context_tekst_bestaat_jn
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  cursor cote_cur
  is
    SELECT 'J'
	FROM   kgc_teksten_context teco
    WHERE  nvl(  teco.conc_id, -9999999999 ) = nvl( decode( p_tekst_type,'CONC',p_tekst_id,teco.conc_id ), -9999999999 )
    AND    nvl(  teco.tote_id, -9999999999 ) = nvl( decode( p_tekst_type,'TOTE',p_tekst_id,teco.tote_id ), -9999999999 )
    AND    nvl(  teco.rete_id, -9999999999 ) = nvl( decode( p_tekst_type,'RETE',p_tekst_id,teco.rete_id ), -9999999999 )
    AND    nvl(  teco.indi_id, -9999999999 ) = nvl( decode( p_context_type,'INDI',p_context_id,teco.indi_id), -9999999999 )
    AND    nvl(  teco.stgr_id, -9999999999 ) = nvl( decode( p_context_type,'STGR',p_context_id,teco.stgr_id), -9999999999 );
BEGIN
  open cote_cur;
  fetch cote_cur into v_return;
  close cote_cur;
  return v_return;
END context_tekst_bestaat_jn;
--
PROCEDURE insert_teksten_context
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER)
IS
BEGIN
  insert into kgc_teksten_context
  ( conc_id
  , tote_id
  , rete_id
  , indi_id
  , stgr_id )
  values
  ( decode( p_tekst_type,'CONC',p_tekst_id, NULL )
  , decode( p_tekst_type,'TOTE',p_tekst_id, NULL )
  , decode( p_tekst_type,'RETE',p_tekst_id, NULL )
  , decode( p_context_type,'INDI',p_context_id, NULL )
  , decode( p_context_type,'STGR',p_context_id, NULL )
  );
END;
--
PROCEDURE delete_teksten_context
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER)
IS
BEGIN
  DELETE FROM kgc_teksten_context teco
  WHERE  nvl(  teco.conc_id, -9999999999 ) = nvl( decode( p_tekst_type,'CONC',p_tekst_id,teco.conc_id ), -9999999999 )
  AND    nvl(  teco.tote_id, -9999999999 ) = nvl( decode( p_tekst_type,'TOTE',p_tekst_id,teco.tote_id ), -9999999999 )
  AND    nvl(  teco.rete_id, -9999999999 ) = nvl( decode( p_tekst_type,'RETE',p_tekst_id,teco.rete_id ), -9999999999 )
  AND    nvl(  teco.indi_id, -9999999999 ) = nvl( decode( p_context_type,'INDI',p_context_id,teco.indi_id), -9999999999 )
  AND    nvl(  teco.stgr_id, -9999999999 ) = nvl( decode( p_context_type,'STGR',p_context_id,teco.stgr_id), -9999999999 );
END;
--
PROCEDURE insert_als_teco_niet_bestaat
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER
, p_bestaat      OUT VARCHAR2 )
IS
BEGIN
  if context_tekst_bestaat_jn
    ( p_context_type => p_context_type
    , p_context_id   => p_context_id
    , p_tekst_type   => p_tekst_type
    , p_tekst_id     => p_tekst_id ) = 'N'
  then
	p_bestaat := 'N';
    insert_teksten_context
    ( p_context_type => p_context_type
    , p_context_id   => p_context_id
    , p_tekst_type   => p_tekst_type
    , p_tekst_id     => p_tekst_id );
  end if;
END;
--
PROCEDURE delete_als_teco_bestaat
( p_context_type VARCHAR2
, p_context_id   NUMBER
, p_tekst_type   VARCHAR2
, p_tekst_id     NUMBER
, p_bestaat      OUT VARCHAR2 )
IS
BEGIN
  if context_tekst_bestaat_jn
    ( p_context_type => p_context_type
    , p_context_id   => p_context_id
    , p_tekst_type   => p_tekst_type
    , p_tekst_id     => p_tekst_id ) = 'J'
  then
	p_bestaat := 'J';
    delete_teksten_context
    ( p_context_type => p_context_type
    , p_context_id   => p_context_id
    , p_tekst_type   => p_tekst_type
    , p_tekst_id     => p_tekst_id );
  end if;
END delete_als_teco_bestaat;
--
END KGC_TECO_00;
/

/
QUIT
