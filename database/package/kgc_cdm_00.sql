CREATE OR REPLACE PACKAGE "HELIX"."KGC_CDM_00" IS
/* RfC734 2007-11-27 cdm_techiek is vervangen door cdm_protocol
-- is er voor de fractie in het onderzoek een stoftest met cdm-techniek aangemeld
function cdm_techniek
( p_frac_id in number
, p_onde_id in number
)
return varchar2; -- J/N
*/

-- Is er voor de fractie in het onderzoek een meting met stoftestgroep gelijk aan
-- systeemparameter CDM_PROTOCOL_MEE en ongelijk aan CDM_PROTOCOL_NIET_MEE aangemeld?
function cdm_protocol
( p_code    in varchar2
, p_kafd_id in number
, p_ongr_id in number
) return varchar2; -- J/N

-- stel notitie samen voor xml-document naar CDM
function cdm_notitie
( p_frac_id in number
, p_onde_id in number
)
return varchar2; -- spoed, datum,  techniekcode en geslacht foetus

-- genereer een bestandsnaam
procedure bestandsnaam
( p_cdmh_id in number
, x_directory out varchar2
, x_bestand out varchar2
);

-- giet helix-data voor CDM in XML-formaat
function helix_naar_CDM_xml
( p_cdmh_id in number
)
return varchar2;

-- zoek een aangemelde stoftest (karyotypering) voor een fractie in een onderzoek
function meet_id
( p_frac_id in number
, p_onde_id in number
, p_openstaand in varchar2 := 'J'
)
return number;

-- lees CDM-xml en verwerk in helix
procedure CDM_naar_helix
( p_xml_doc in varchar2
);

-- verwerk de binnengekomen bestanden
PROCEDURE verwerk_bestanden;
END KGC_CDM_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_CDM_00" IS
 /******************************************************************************
      NAME:       KGC_CDM_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.9        2016-02-17   SKA             Mantis 9355 TO_CHAR added for conclusie in procedure
                                              automatisch_resultaat release 8.11.0.2

   *****************************************************************************/
-- lokale funktie bouwt een regel op
function xml_regel
( p_indent in number
, p_tag in varchar2
, p_waarde in varchar2
, p_cr in varchar2 := 'VOOR' -- en/of 'NA'
)
return varchar2
is
  v_return varchar2(1000);
begin
  v_return := kgc_util_00.tag
              ( p_string => p_waarde
              , p_tag => p_tag
              );
  v_return := lpad( ' ', p_indent, ' ' ) || v_return;
  if ( p_cr like 'VOOR%' )
  then
    v_return := chr(10) || v_return;
  end if;
  if ( p_cr like '%NA' )
  then
    v_return := v_return || chr(10);
  end if;
  return( v_return );
end xml_regel;

-- lokale funktie leest stuk uit xml-document
function xml_waarde
( p_string in varchar2
, p_tag in varchar2
)
return varchar2
is
  v_return varchar2(32767);
begin
  if ( instr( p_string, p_tag ) > 0 )
  then
    v_return := kgc_util_00.strip
                ( p_string => p_string
                , p_tag_va => '<'||p_tag||'>'
                , p_tag_tm => '</'||p_tag||'>'
                );
  end if;
  return( v_return );
end xml_waarde;

-- lokale procedure om automatisch resultaat te bepalen
PROCEDURE automatisch_resultaat
( p_meet_id in number
, p_meti_id in number
, p_meetwaarde in varchar2
)
IS
  v_tekst VARCHAR2(2000);
  v_controle_ok BOOLEAN := TRUE;
  v_severity VARCHAR2(1) := 'I';
  v_melding VARCHAR2(200) := NULL;

  cursor meti_cur
  is
    select meti_id
    ,      TO_CHAR(conclusie) --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014)  --TO_CHAR(conclusie) changed to  conclusie by sachin 12-06-2015 for reverting 9355 changes in 201502 release rework  --  TO_CHAR added by SKA for MANTIS 9355  2016-02-17 release 8.11.0.2
    from   bas_metingen meti
    where  meti_id = p_meti_id
    and    NVL( TO_CHAR(conclusie), CHR(2) ) <> NVL( v_tekst, CHR(2) ) --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014) --TO_CHAR(conclusie) changed to  conclusie by sachin 12-06-2015 for reverting 9355 changes in 201502 release rework   --  TO_CHAR added by SKA for MANTIS 9355  2016-02-17 release 8.11.0.2
    and    afgerond = 'N'
    for update of conclusie nowait
    ;
BEGIN
  kgc_mere_00.resultaat
  ( p_meet_id => p_meet_id
  , p_waarde => p_meetwaarde
  , p_afgerond => 'J'
  , x_tekst => v_tekst
  , x_controle_ok => v_controle_ok
  , x_strengheid => v_severity
  , x_melding => v_melding
  );
  IF ( NOT v_controle_ok
   AND v_melding IS NOT NULL
     )
  THEN
    qms$errors.show_message
    ( p_mesg => v_melding
    , p_errtp => NVL( v_severity, 'W' )
    , p_rftf => FALSE
    );
  END IF;

  if ( v_tekst is not null )
  then
    for r in meti_cur
    loop
      if ( NOT kgc_onde_00.afgerond( p_meti_id => r.meti_id, p_rftf => false ) )
      then
        update bas_metingen
        set    conclusie = v_tekst
        ,      datum_conclusie = sysdate
        where current of meti_cur;
      end if;
    end loop;
  end if;
END automatisch_resultaat;

/*  2007-11-27  cdm_techniek is vervangen door cdm_protocol

 function cdm_techniek
( p_frac_id in number
, p_onde_id in number
)
return varchar2 -- J/N
is
  v_return varchar2(1) := 'N';

  cursor onde_cur
  ( b_onde_id in number
  )
  is
    select onde.kafd_id
    ,      onde.ongr_id
    from   kgc_onderzoeken onde
    where  onde.onde_id = b_onde_id
    ;
  onde_rec onde_cur%rowtype;

  cursor tech_cur
  ( b_frac_id in number
  , b_onde_id in number
  , b_cdm_technieken in varchar2
  )
  is
    select tech.code
    from   kgc_technieken tech
    ,      kgc_stoftesten stof
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    where  meet.meti_id = meti.meti_id
    and    meet.stof_id = stof.stof_id
    and    stof.tech_id = tech.tech_id
    and    meti.frac_id = b_frac_id
    and    meti.onde_id = b_onde_id
    and    instr( b_cdm_technieken, tech.code ) > 0
    ;
  tech_rec tech_cur%rowtype;

  v_sypa varchar2(1000);
begin
  if ( p_onde_id is null
    or p_frac_id is null
     )
  then
    return( 'N' );
  end if;

  open  onde_cur( p_onde_id );
  fetch onde_cur
  into  onde_rec;
  close onde_cur;
  v_sypa := upper( kgc_sypa_00.standaard_waarde
                   ( p_parameter_code => 'CDM_TECHNIEK'
                   , p_kafd_id => onde_rec.kafd_id
                   , p_ongr_id => onde_rec.ongr_id
                   )
                 );
  open  tech_cur( b_frac_id => p_frac_id
                , b_onde_id => p_onde_id
                , b_cdm_technieken => v_sypa
                );
  fetch tech_cur
  into  tech_rec;
  close tech_cur;
  if ( tech_rec.code is not null )
  then
    return( 'J' );
  end if;
  return( 'N' );
exception
  when others
  then
    return( 'N' );
end cdm_techniek;

*/

function cdm_protocol
( p_code    in varchar2
, p_kafd_id in number
, p_ongr_id in number
) return varchar2 -- J/N

 is

   c_delimiter constant varchar2(1) default '#'; -- scheidingsteken

   v_code_ok       boolean default false;
   v_sypa_prot     varchar2(1000);
   v_sypa_not_prot varchar2(1000);
   v_zoekstring    kgc_systeem_par_waarden.waarde%type;

begin
   -- Systeemparameters ophalen
   v_sypa_prot     := upper(kgc_sypa_00.standaard_waarde(p_parameter_code => 'CDM_PROTOCOL_MEE'
                                                        ,p_kafd_id        => p_kafd_id
                                                        ,p_ongr_id        => p_ongr_id));
   v_sypa_not_prot := upper(kgc_sypa_00.standaard_waarde(p_parameter_code => 'CDM_PROTOCOL_NIET_MEE'
                                                        ,p_kafd_id        => p_kafd_id
                                                        ,p_ongr_id        => p_ongr_id));

   -- Zoekstrings in systeemparameter CDM_PROTOCOL_MEE ontrafelen in een loop
   -- In ieder geval zorgen dat systeemparameters eindigen op scheidingsteken
   -- (anders gaat hem mis) en de eerste erafhalen (niet zinvol)
   v_sypa_prot := trim(c_delimiter from v_sypa_prot) || c_delimiter;

   while v_sypa_prot is not null
   loop
      -- Er is minimaal 1 karakter voor 1e scheidingsteken
      --if instr(v_sypa_prot,c_delimiter,1,1) >=  2
      --then
      -- Linker blokje: AB%#C# levert AB%
      v_zoekstring := substr(v_sypa_prot
                            ,1
                            ,instr(v_sypa_prot
                                  ,c_delimiter
                                  ,1
                                  ,1) - 1);

      -- Indien zoekstring gelijk aan de doorgegeven code,
      if p_code like v_zoekstring
      then
         -- dan is het OK en loop verlaten
         v_code_ok := true;
         exit;
      else
         -- naar links schuiven en volgende zoekstring ontrafelen,
         -- code blijft niet OK
         v_sypa_prot := substr(v_sypa_prot
                              ,instr(v_sypa_prot
                                    ,c_delimiter
                                    ,1
                                    ,1) + 1);
      end if;

   end loop;

   -- Indien code voldoet aan 1 van de zoekstrings in parameter CDM_PROTOCOL_MEE
   if v_code_ok
   then
      -- In ieder geval zorgen dat systeemparameters eindigen op scheidingsteken
      -- (anders gaat hem mis) en de eerste erafhalen (niet zinvol)
      v_sypa_not_prot := trim(c_delimiter from v_sypa_not_prot) || c_delimiter;

      -- Zoekstrings in systeemparameter CDM_PROTOCOL_NIET_MEE ontrafelen in een loop
      while v_sypa_not_prot is not null
      loop
         -- Linker blokje: AB%#C# levert AB%
         v_zoekstring := substr(v_sypa_not_prot
                               ,1
                               ,instr(v_sypa_not_prot
                                     ,c_delimiter
                                     ,1
                                     ,1) - 1);

         -- Indien zoekstring gelijk aan de doorgegeven code,
         if p_code like v_zoekstring
         then
            -- dan is het alsnog niet OK en loop verlaten
            v_code_ok := false;
            exit;
         else
            -- naar links schuiven en volgende zoekstring ontrafelen,
            -- code blijft OK
            v_sypa_not_prot := substr(v_sypa_not_prot
                                     ,instr(v_sypa_not_prot
                                           ,c_delimiter
                                           ,1
                                           ,1) + 1);
         end if;

      end loop;

   end if;

   if v_code_ok
   then
      return('J');
   else
      return('N');
   end if;

exception
   when others then
      return('N');

end cdm_protocol;

function cdm_notitie
( p_frac_id in number
, p_onde_id in number
)
return varchar2
is
  v_return varchar2(1000);
  v_regel varchar2(1000);
  v_geslacht varchar2(100);

  /*cursor meet_cur
  ( b_frac_id in number
  , b_onde_id in number
  , b_cdm_technieken in varchar2
  )
  is
    select distinct decode( onde.spoed
                 , 'J', 'SPOED'
                 , decode( meti.prioriteit
                         , 'J', 'SPOED'
                         , null
                         )
                 ) spoed
    ,      to_char( onde.geplande_einddatum,'dd-mm-yyyy' ) datum-- of to_char( nvl( meti.geplande_einddatum, onde.geplande_einddatum ),'dd-mm-yyyy' )
    ,      stgr.code code
    from   kgc_technieken tech
    ,      kgc_stoftesten stof
    ,      kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_stoftestgroepen stgr
    where  meet.meti_id = meti.meti_id
    and    meti.onde_id = onde.onde_id
    and    meet.stof_id = stof.stof_id
    and    stof.tech_id = tech.tech_id
    and    meti.frac_id = b_frac_id
    and    meti.onde_id = b_onde_id
    and    meti.stgr_id = stgr.stgr_id (+)
    and    instr( b_cdm_technieken, tech.code ) > 0
    ;*/

-- Mantis 734 PHE Bovenstaande cursor vervangencode stoftestgroep
-- Aanvullende selectie op cdm_protocol (ipv controle op techniekcode via SYPA CDM_TECHNIEK)
-- Foetus toegevoegd
  cursor meti_cur
  ( b_frac_id in number
  , b_onde_id in number
  )
  is
    select distinct
           decode( meti.prioriteit, 'J', 'SPOED', null) spoed
    ,      to_char( meti.geplande_einddatum,'dd-mm-yyyy' ) geplande_einddatum
    ,      stgr.code stgr_code
    ,      decode(foet.geslacht,'V','Geslacht foetus: Vrouwelijk',
                                'M','Geslacht foetus: Mannelijk',
                                'O','Geslacht foetus: Onbekend',null) geslacht
    from   bas_metingen meti
    ,      kgc_stoftestgroepen stgr
    ,      kgc_onderzoeken onde
    ,      kgc_foetussen foet
    where  meti.onde_id = onde.onde_id
    and    meti.stgr_id = stgr.stgr_id
    and    onde.foet_id = foet.foet_id (+)
    and    meti.frac_id = b_frac_id
    and    meti.onde_id = b_onde_id
    and   kgc_cdm_00.cdm_protocol( stgr.code, onde.kafd_id, onde.ongr_id) = 'J'
    ;

begin
  if ( p_onde_id is null
    or p_frac_id is null
     )
  then
    return( null );
  end if;

  for meti_rec in meti_cur( b_frac_id => p_frac_id
                          , b_onde_id => p_onde_id
                          )
  loop
    v_regel := meti_rec.stgr_code;

    if meti_rec.spoed is not null
    then
      if ( v_regel is not null )
      then
        v_regel := v_regel || ' ' || meti_rec.spoed;
      else
        v_regel := meti_rec.spoed;
      end if;
    end if;

    if meti_rec.geplande_einddatum is not null
    then
      if ( v_regel is not null )
      then
        v_regel := v_regel || ' ' || meti_rec.geplande_einddatum;
      else
        v_regel := meti_rec.geplande_einddatum;
      end if;
    end if;

    v_geslacht := meti_rec.geslacht;  -- geslacht alleen aan einde van de notes zetten

    if ( v_return is null )
    then
      v_return := v_regel;
    else
      if v_regel is not null
      then
         v_return := v_return||' / '|| v_regel;
      end if;
    end if;
  end loop;

  -- notes afsluiten met geslacht
  if v_geslacht is not null
    then
      if ( v_return is not null )
      then
        v_return := v_return || ' ' || v_geslacht;
      else
        v_return := v_geslacht;
      end if;
   end if;

  return( v_return );
exception
  when others
  then
    return( null );
end cdm_notitie;

procedure bestandsnaam
( p_cdmh_id in number
, x_directory out varchar2
, x_bestand out varchar2
)
is
  cursor cdmh_cur
  ( b_cdmh_id in number
  )
  is
    select cdmh.casenr
    from   kgc_cdm_helix cdmh
    where  cdmh.cdmh_id = b_cdmh_id
    ;
  cdmh_rec cdmh_cur%rowtype;
begin
  x_directory := RTRIM( kgc_sypa_00.standaard_waarde
                        ( p_parameter_code => 'CDM_XML_DIR_IN'
                        , p_kafd_id => null
                        )
                      , '\'
                      )||'\';
  open  cdmh_cur( p_cdmh_id );
  fetch cdmh_cur
  into  cdmh_rec;
  close cdmh_cur;
  -- ongeoorloofde tekens vervangen door underscore
  x_bestand := replace( cdmh_rec.casenr, '\', '_' );
  x_bestand := replace( x_bestand, '?', '_' );
  x_bestand := replace( x_bestand, '*', '_' );
  x_bestand := replace( x_bestand, '/', '_' );
  x_bestand := replace( x_bestand, ',', '_' );
  x_bestand := replace( x_bestand, ' ', '_' );
  x_bestand := x_bestand ||'.xml';
end bestandsnaam;

function helix_naar_CDM_xml
( p_cdmh_id in number
)
return varchar2
is
  v_return varchar2(4000);
  cursor cdmh_cur
  ( b_cdmh_id in number
  )
  is
    select cdmh.casenr
    ,      cdms.pers_aanspreken
    ,      to_char( cdms.pers_geboortedatum, 'dd-mm-yyyy' ) pers_geboortedatum
    ,      cdms.pers_geslacht
    ,      cdms.familienummer
    ,      cdms.mate_code
    ,      cdms.rela_aanspreken
    ,      cdms.indicaties
    ,      cdms.notitie
    from   kgc_cdm_selectie_vw cdms
    ,      kgc_cdm_helix cdmh
    where  cdmh.frac_id = cdms.frac_id
    and    cdmh.onde_id = cdms.onde_id
    and    cdmh.cdmh_id = b_cdmh_id
    ;
  cdmh_rec cdmh_cur%rowtype;
begin
  open  cdmh_cur( p_cdmh_id );
  fetch cdmh_cur
  into  cdmh_rec;
  close cdmh_cur;
  if ( cdmh_rec.casenr is not null )
  then
    v_return := xml_regel( 0, 'CASE',
                  xml_regel( 2, 'CASENR', cdmh_rec.casenr )
               || xml_regel( 2, 'PATIENT_NAME', cdmh_rec.pers_aanspreken )
               || xml_regel( 2, 'PATIENT_ID', cdmh_rec.familienummer )
               || xml_regel( 2, 'DATE_OF_BIRTH', cdmh_rec.pers_geboortedatum )
               || xml_regel( 2, 'GENDER', cdmh_rec.pers_geslacht )
               || xml_regel( 2, 'SPECIMEN', cdmh_rec.mate_code )
               || xml_regel( 2, 'PHYSICIAN', cdmh_rec.rela_aanspreken )
               || xml_regel( 2, 'INDICATION', cdmh_rec.indicaties )
               || xml_regel( 2, 'NOTES',  cdmh_rec.notitie, 'VOORNA' )
                         , null );
  end if;
  return( v_return );
end helix_naar_CDM_xml;

function meet_id
( p_frac_id in number
, p_onde_id in number
, p_openstaand in varchar2 := 'J'
)
return number
is
  v_return number;
  v_stof_id number;

  cursor onde_cur
  ( b_onde_id in varchar2
  )
  is
    select onde.kafd_id
    ,      onde.ongr_id
    from   kgc_onderzoeken onde
    where  onde.onde_id = b_onde_id
    ;
  onde_rec onde_cur%rowtype;

  --  meetwaarde voor karyotypering
  cursor meet_open_cur
  ( b_frac_id in number
  , b_onde_id in number
  , b_stof_id in number
  )
  is
    select meet.meet_id
    from   kgc_cdm_selectie_vw cdms
    ,      bas_meetwaarden meet
    ,      bas_metingen meti
    where  meti.frac_id = cdms.frac_id
    and    meti.onde_id = cdms.onde_id
    and    meti.meti_id = meet.meti_id
    and    meet.afgerond = 'N'
    and    meti.afgerond = 'N'
    and    meet.stof_id = b_stof_id
    and    meti.frac_id = b_frac_id
    and    meti.onde_id = b_onde_id
    order by meet.meet_id asc -- laatste eerst (normaliter is er maar 1)
    ;
-- PHE Mantis 914
-- Gebruik view specifiek voor CDM01, waarbij de afgeronde onderzoeken en afgeronde metingen -- ook worden meegenomen
  cursor meet_alle_cur
  ( b_frac_id in number
  , b_onde_id in number
  , b_stof_id in number
  )
  is
    select meet.meet_id
    from   kgc_cdm_selectie_cdm01_vw cdm1
    ,      bas_meetwaarden meet
    ,      bas_metingen meti
    where  meti.frac_id = cdm1.frac_id
    and    meti.onde_id = cdm1.onde_id
    and    meti.meti_id = meet.meti_id
    and    meet.stof_id = b_stof_id
    and    meti.frac_id = b_frac_id
    and    meti.onde_id = b_onde_id
    order by meet.meet_id asc -- laatste eerst (normaliter is er maar 1)
    ;
begin
  open  onde_cur( p_onde_id );
  fetch onde_cur
  into  onde_rec;
  close onde_cur;

  -- stoftest karyotypering
  begin
    v_stof_id := kgc_uk2pk.stof_id( p_kafd_id => onde_rec.kafd_id
                                  , p_code => kgc_sypa_00.standaard_waarde
                                              ( p_parameter_code => 'REFERENTIE_STOFTEST'
                                              , p_kafd_id => onde_rec.kafd_id
                                              , p_ongr_id => onde_rec.ongr_id
                                              )
                                  );
  exception
    when others
    then
      v_stof_id := null;
  end;

  -- zoek de juiste meetwaarde
  if ( nvl( p_openstaand, 'J' ) = 'J' )
  then
    open  meet_open_cur( b_frac_id => p_frac_id
                       , b_onde_id => p_onde_id
                       , b_stof_id => v_stof_id
                       );
    fetch meet_open_cur
    into  v_return;
    close meet_open_cur;
  else
    open  meet_alle_cur( b_frac_id => p_frac_id
                       , b_onde_id => p_onde_id
                       , b_stof_id => v_stof_id
                       );
    fetch meet_alle_cur
    into  v_return;
    close meet_alle_cur;
  end if;
  return( v_return );
end meet_id;

procedure CDM_naar_helix
( p_xml_doc in varchar2
)
is
  v_xml_doc varchar2(32767) := p_xml_doc;
  v_analyse varchar2(32767);

-- Mantis 1522 lengte van velden v_regel en v_waarde  vergroot van 4000 naar 32K
  v_regel varchar2(32767);
  v_casenr varchar2(100);
  v_waarde varchar2(32767);
  v_foutmelding varchar2(2000);
  v_datum date;
  v_cdm_mede_id kgc_medewerkers.mede_id%type;

  cursor cdmh_cur
  ( b_casenr in varchar2
  )
  is
    select cdmh.cdmh_id
    ,      cdmh.frac_id
    ,      cdmh.onde_id
    ,      onde.kafd_id
    ,      onde.ongr_id
    from   kgc_onderzoeken onde
    ,      kgc_cdm_helix cdmh
    where  cdmh.onde_id = onde.onde_id
    and    cdmh.casenr = b_casenr
    and    cdmh.status = 'O'
    ;
  cdmh_rec cdmh_cur%rowtype;

  cursor kwfr_cur
  ( b_frac_id in number
  , b_code in varchar2
  )
  is
    select kwfr_id
    from   kgc_kweekfracties
    where  frac_id = b_frac_id
    and    code = b_code
    ;

  cursor meet_cur
  ( b_meet_id in number
  )
  is
    select meet.meet_id
    ,      meet.meti_id
    from   bas_meetwaarden meet
    where  meet.meet_id = b_meet_id
    ;

  kwfr_trow kgc_kweekfracties%rowtype;
  kwfr_null kgc_kweekfracties%rowtype;
  meti_trow bas_metingen%rowtype;
  meti_null bas_metingen%rowtype;
  meet_trow bas_meetwaarden%rowtype;
  meet_null bas_meetwaarden%rowtype;
  kary_trow kgc_karyogrammen%rowtype;
  kary_null kgc_karyogrammen%rowtype;

  procedure meting_detail
  ( b_meti_id in number
  , b_tag in varchar2
  , b_detail in varchar2
  , b_volgorde in number
  )
  is
  begin
    v_waarde := xml_waarde
                ( p_string => v_xml_doc
                , p_tag => b_tag
                );
    if ( v_waarde is not null )
    then
      update bas_meting_details
      set    waarde = v_waarde
      where  meti_id = b_meti_id
      and    upper(prompt) = b_detail
      ;
      if ( sql%rowcount = 0 )
      then
        insert into bas_meting_details
        ( meti_id
        , volgorde
        , prompt
        , waarde
        )
        values
        ( b_meti_id
        , b_volgorde
        , b_detail
        , v_waarde
        );
      end if;
    end if;
  end meting_detail;

begin
  -- identificeer fractie en onderzoek
  v_casenr := xml_waarde
              ( p_string => v_xml_doc
              , p_tag => 'CASENR'
              );
  if ( v_casenr is not null )
  then
    open  cdmh_cur( v_casenr );
    fetch cdmh_cur
    into  cdmh_rec;
    close cdmh_cur;
  end if;

  meti_trow := meti_null;
  meet_trow := meet_null;
  open  meet_cur( b_meet_id => kgc_cdm_00.meet_id
                               ( p_frac_id => cdmh_rec.frac_id
                               , p_onde_id => cdmh_rec.onde_id
                               , p_openstaand => 'J'
                               )
                );
  fetch meet_cur
  into  meet_trow.meet_id
  ,     meti_trow.meti_id;
  close meet_cur;

  if ( cdmh_rec.frac_id is not null
   and cdmh_rec.onde_id is not null
   and meti_trow.meti_id is not null
   and meet_trow.meet_id is not null
     )
  then
    -- verwerk eerst <ANALYSIS> deel en daarna de rest
    v_analyse := xml_waarde
                 ( p_string => v_xml_doc
                 , p_tag => 'ANALYSIS'
                 );
    v_xml_doc := kgc_util_00.strip
                 ( p_string => v_xml_doc
                 , p_tag_va => '<ANALYSIS>'
                 , p_tag_tm => '</ANALYSIS>'
                 , p_rest => 'J'
                 );

    v_datum := nvl( to_date( xml_waarde
                               ( p_string => v_xml_doc
                               , p_tag    => 'RESULT_DATE'
                               )
                           , 'dd-mm-yyyy'
                           )
                  , trunc( SYSDATE ) -- Mantis 1585
                  );
    v_cdm_mede_id := kgc_uk2pk.mede_id
                       ( p_code => 'CDM' -- Mantis 1585
                       );

    -- karyogrammen worden alleen bijgehouden in CDM
    delete from kgc_karyogrammen
    where  meet_id = meet_trow.meet_id;

    while instr( v_analyse, '<LINE>' ) > 0
    loop
      v_regel := xml_waarde
                 ( p_string => v_analyse
                 , p_tag => 'LINE'
                 );
      v_analyse := kgc_util_00.strip
                   ( p_string => v_analyse
                   , p_tag_va => '<LINE>'
                   , p_tag_tm => '</LINE>'
                   , p_rest => 'J'
                   );
      v_waarde := xml_waarde
                  ( p_string => v_regel
                  , p_tag => 'CULTURE'
                  );

      if ( v_waarde is not null )
      then
        kwfr_trow := kwfr_null;
        -- zoek kweekfractie of maak hem aan (zonder kweektype)
        open  kwfr_cur( b_frac_id => cdmh_rec.frac_id
                      , b_code => v_waarde
                      );
        fetch kwfr_cur
        into  kwfr_trow.kwfr_id;
        close kwfr_cur;
        if ( kwfr_trow.kwfr_id is null )
        then
          kwfr_trow.frac_id := cdmh_rec.frac_id;
          kwfr_trow.code := v_waarde;
          kwfr_trow.omschrijving := v_waarde; -- ??
          kwfr_trow.vervallen := 'N';
          kwfr_trow.kwty_id := null;
          kwfr_trow.kweekduur := null;
          kwfr_trow.kwfr_id_parent := null;
          kwfr_trow.mede_id_controle := null;
          kwfr_trow.datum_controle := null;
          kwfr_trow.mede_id_inzet := v_cdm_mede_id;
          kwfr_trow.datum_inzet := v_datum;
          kwfr_trow.mede_id_uithaal := v_cdm_mede_id;
          kwfr_trow.datum_uithaal := v_datum;
          kwfr_trow.mede_id_opslaan := null;
          kwfr_trow.datum_opslaan := null;
          kwfr_trow.opslag_elders := null;
          select kgc_kwfr_seq.nextval into kwfr_trow.kwfr_id from dual;
          insert into kgc_kweekfracties
          ( kwfr_id
          , frac_id
          , code
          , omschrijving
          , vervallen
          , kwty_id
          , kweekduur
          , kwfr_id_parent
          , mede_id_controle
          , datum_controle
          , mede_id_inzet
          , datum_inzet
          , mede_id_uithaal
          , datum_uithaal
          , mede_id_opslaan
          , datum_opslaan
          , opslag_elders
          )
          values
          ( kwfr_trow.kwfr_id
          , kwfr_trow.frac_id
          , kwfr_trow.code
          , kwfr_trow.omschrijving
          , kwfr_trow.vervallen
          , kwfr_trow.kwty_id
          , kwfr_trow.kweekduur
          , kwfr_trow.kwfr_id_parent
          , kwfr_trow.mede_id_controle
          , kwfr_trow.datum_controle
          , kwfr_trow.mede_id_inzet
          , kwfr_trow.datum_inzet
          , kwfr_trow.mede_id_uithaal
          , kwfr_trow.datum_uithaal
          , kwfr_trow.mede_id_opslaan
          , kwfr_trow.datum_opslaan
          , kwfr_trow.opslag_elders
          );
        end if;

        kary_trow := kary_null;
        kary_trow.meet_id := meet_trow.meet_id;
        kary_trow.kwfr_id := kwfr_trow.kwfr_id;
        kary_trow.cel := xml_waarde
                         ( p_string => v_regel
                         , p_tag => 'LINENO'
                         );
        kary_trow.tekst_chromosomen := xml_waarde
                                       ( p_string => v_regel
                                       , p_tag => 'KARYOTYPE'
                                       );
        kary_trow.aantal := xml_waarde
                            ( p_string => v_regel
                            , p_tag => 'NO_OF_METAPHASES'
                            );
        if ( kary_trow.aantal is not null )
        then
          kary_trow.tekst_chromosomen := kary_trow.tekst_chromosomen
                                      || ' in ' || kary_trow.aantal || ' metafases';
        end if;
        kary_trow.opmerkingen := null;
        kary_trow.kwaliteit := null;
        kary_trow.nonius := null;
        kary_trow.opname := 'N';
        kary_trow.opname_k := 'N';

        insert into kgc_karyogrammen
        ( meet_id
        , kwfr_id
        , cel
        , aantal
        , tekst_chromosomen
        , opmerkingen
        , kwaliteit
        , nonius
        , opname
        , opname_k
        )
        values
        ( kary_trow.meet_id
        , kary_trow.kwfr_id
        , kary_trow.cel
        , kary_trow.aantal
        , kary_trow.tekst_chromosomen
        , kary_trow.opmerkingen
        , kary_trow.kwaliteit
        , kary_trow.nonius
        , kary_trow.opname
        , kary_trow.opname_k
        );
      else
        v_foutmelding := 'Waarde voor CULTURE is niet aangeleverd';
      end if;
    end loop;

    -- rest komt niet uit <ANALYSIS> deel
    if ( meet_trow.meet_id is not null )
    then
      meet_trow.mede_id := kgc_uk2pk.mede_id
                           ( p_code => upper( xml_waarde
                                              ( p_string => v_xml_doc
                                              , p_tag => 'COMPLETED_BY'
                                              )
                                            )
                           );
      meet_trow.meetwaarde := xml_waarde
                              ( p_string => v_xml_doc
                              , p_tag => 'RESULT'
                              );
      meet_trow.datum_meting := to_date( xml_waarde
                                         ( p_string => v_xml_doc
                                         , p_tag => 'RESULT_DATE'
                                         )
                                         , 'dd-mm-yyyy'
                                       );
      meet_trow.datum_afgerond := to_date( xml_waarde
                                           ( p_string => v_xml_doc
                                           , p_tag => 'CASE_CLOSED_DATE'
                                           )
                                           , 'dd-mm-yyyy'
                                         );
      -- bepaal de resultaattekst (protocol/meting)
      automatisch_resultaat
      ( p_meet_id => meet_trow.meet_id
      , p_meti_id => meti_trow.meti_id
      , p_meetwaarde => meet_trow.meetwaarde
      );

      update bas_meetwaarden
      set    mede_id = meet_trow.mede_id
      ,      meetwaarde = meet_trow.meetwaarde
      ,      datum_meting = meet_trow.datum_meting
--      ,      afgerond = 'J'
--      ,      datum_afgerond = meet_trow.datum_afgerond
      where  meet_id = meet_trow.meet_id;

      -- in 2 stappen vanwege synchronisatie bas_meetwaarden.meetwaarde - bas-meetwaarden_details
      -- en controle op afgerond zijn in bas_meetwaarde_details
      update bas_meetwaarden
      set    afgerond = 'J'
      ,      datum_afgerond = meet_trow.datum_afgerond
      where  meet_id = meet_trow.meet_id;

    end if;

    if ( meti_trow.meti_id is not null )
    then
      meti_trow.mede_id := kgc_uk2pk.mede_id
                           ( p_code => upper( xml_waarde
                                              ( p_string => v_xml_doc
                                              , p_tag => 'ANALIST'
                                              )
                                            )
                           );
      -- resultaattekst is hiervoor al bepaald
      update bas_metingen
      set    mede_id = meti_trow.mede_id
      where  meti_id = meti_trow.meti_id;

      -- verwerk detailwaarden meting (zie ook view kgc_cdm_cases_vw)
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'CULTURE_QUALITY'
      , b_detail => 'KWEEK_KWALITEIT'
      , b_volgorde => 1
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'BANDING_QUALITY'
      , b_detail => 'KLEUR_KWALITEIT'
      , b_volgorde => 2
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'BANDING_METHOD'
      , b_detail => 'METHODE'
      , b_volgorde => 3
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'STANDARD_INVESTIGATION'
      , b_detail => 'STANDAARD_ONDERZOEK'
      , b_volgorde => 4
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'TOTAL_CELLS'
      , b_detail => 'TOTAAL_AANTAL'
      , b_volgorde => 5
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'NO_COUNTED'
      , b_detail => 'TOTAAL_GETELD'
      , b_volgorde => 6
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'NO_ANALYSED'
      , b_detail => 'TOTAAL_GEANALYSEERD'
      , b_volgorde => 7
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'NO_KARYOTYPED'
      , b_detail => 'TOTAAL_GEKARYOTYPEERD'
      , b_volgorde => 8
      );
      meting_detail
      ( b_meti_id => meti_trow.meti_id
      , b_tag => 'NO_PRINTED'
      , b_detail => 'TOTAAL_OP_FOTO'
      , b_volgorde => 9
      );
    end if;

    v_waarde := xml_waarde
                ( p_string => v_xml_doc
                , p_tag => 'DATE_OF_SAMPLE'
                );
    update kgc_cdm_helix
    set    status = 'V'
    ,      datum_overgezet = nvl( to_date( v_waarde, 'dd-mm-yyyy' ), datum_overgezet )
    ,      datum_verwerkt = sysdate
    ,      opmerkingen = NULL
    where  cdmh_id = cdmh_rec.cdmh_id
    ;
    commit;
  else
    v_foutmelding := 'Case '||v_casenr||' kan niet worden verwerkt';
    if ( meet_trow.meet_id is null )
    then
      v_foutmelding := v_foutmelding||chr(10)||'Geen aangemelde stoftest gevonden';
    end if;
    if ( cdmh_rec.frac_id is null
      or cdmh_rec.onde_id is null
       )
    then
      v_foutmelding := v_foutmelding||chr(10)||'Fractie / onderzoek is niet overgezet naar CDM';
    end if;
    raise value_error;
  end if;
exception
  when others
  then
    if ( v_foutmelding is null )
    then
      v_foutmelding := 'Case '||v_casenr||' kan niet worden verwerkt'||chr(10)||sqlerrm;
    else
      v_foutmelding := v_foutmelding; -- ||chr(10)||sqlerrm;
    end if;
    update kgc_cdm_helix
    set    opmerkingen = v_foutmelding
    where  cdmh_id = cdmh_rec.cdmh_id
    ;
    commit;
    raise value_error; -- zie afhandeling bij aanroep
end CDM_naar_helix;

PROCEDURE verwerk_bestanden IS
-- Deze procedure wordt vanuit een Oracle-JOB automatisch aangeroepen.
-- er is geen gevaar dat deze twee keer parallel gaat draaien: pas als de job klaar
-- is, wordt de volgende gescheduled.
  v_sypa_server_dirname VARCHAR2(4000);
  v_sypa_unpw           VARCHAR2(200);
  v_msg                 VARCHAR2(32767);
  v_errormessage        VARCHAR2(32767);
  v_ftperrormessage     VARCHAR2(32767);
  v_ftpdirfiles         kgc_ftp.string_table;
  v_ftpremotedir        VARCHAR2(4000);
  v_ftpservername       VARCHAR2(200);
  v_ftpusername         VARCHAR2(100) := 'anonymus';
  v_ftppassword         VARCHAR2(100) := 'helix@umcn.nl';
  v_ftpfilename         VARCHAR2(4000);
  v_ftppos              NUMBER;
  v_xml_doc             VARCHAR2(32767);
  e_delete_error        EXCEPTION;
BEGIN
  -- un pw van FTP-server ophalen:
  v_sypa_unpw := kgc_sypa_00.systeem_waarde( p_parameter_code => 'CDM_FTP_UN_PW' );
  v_ftppos := instr( v_sypa_unpw, '/' );
  IF v_ftppos > 0
  THEN
     v_ftpusername := substr( v_sypa_unpw, 1, v_ftppos - 1 );
     v_ftppassword := substr( v_sypa_unpw, v_ftppos + 1 );
  END IF;

  -- Server- en directory-naam van FTP-server ophalen:
  v_sypa_server_dirname := kgc_sypa_00.systeem_waarde( p_parameter_code => 'CDM_XML_DIR_UIT_FTP' );
  v_ftpremotedir := lower( v_sypa_server_dirname );
  v_ftpremotedir := REPLACE( v_ftpremotedir, 'ftp://', NULL );
  v_ftpremotedir := REPLACE( v_ftpremotedir, '\', '/' );
  v_ftpremotedir := rtrim( v_ftpremotedir, '/' );
  v_ftppos       := instr( v_ftpremotedir, '/' ); -- scheiding server en dir
  IF (v_ftppos > 0)
  THEN
     v_ftpservername := substr( v_ftpremotedir, 1, v_ftppos - 1 );
     v_ftpremotedir  := substr( v_ftpremotedir, v_ftppos );
  END IF;

  --dbms_output.put_line( 'CDM-verwerk: bestandenlijst ophalen van: S: ' || v_ftpservername ||
  --                      ' UN: ' || v_ftpusername || ' PW: ' || v_ftppassword || ' RemDir:' || v_ftpremotedir );

  -- Bestandenlijst van FTP-server inlezen;
  v_ftperrormessage := NULL;
  v_ftperrormessage := kgc_ftp.ftpdir( p_servername => v_ftpservername
                                     , p_port       => '21'
                                     , p_username   => v_ftpusername
                                     , p_password   => v_ftppassword
                                     , p_remotedir  => v_ftpremotedir
                                     , p_dirlist    => v_ftpdirfiles );
  IF (nvl(length(v_ftperrormessage), 0) > 0)
  THEN
     RAISE value_error;
  END IF;

  FOR i IN 1 .. v_ftpdirfiles.COUNT
  LOOP
     -- v_ftpDirFiles(I) bevat iets als
     --    '01-28-04  02:23PM       <DIR>          test'
     -- of '01-28-04  04:05PM                 6029 RZN_patient1.xml'

     v_ftpfilename := ltrim( rtrim( substr( v_ftpdirfiles(i), 40 ) ) );
     v_ftpfilename := rtrim( v_ftpfilename, chr(10) );
     v_ftpfilename := rtrim( v_ftpfilename, chr(13) );

     --dbms_output.put_line('Aanwezig bestand: ' || v_ftpFileName);
     -- alleen *.xml verwerken: geen andere of SUBDIRECTORIES!

     IF instr( lower( v_ftpfilename ), '.xml' ) > 0
     THEN
       -- inlezen bestand in v_xml_doc:
       v_ftperrormessage := NULL;
       v_ftperrormessage := kgc_ftp.ftpgettochar( p_servername => v_ftpservername
                                                , p_port       => '21'
                                                , p_username   => v_ftpusername
                                                , p_password   => v_ftppassword
                                                , p_sourcefile => v_ftpremotedir || '/' || v_ftpfilename
                                                , x_data       => v_xml_doc );
       IF (nvl(length(v_ftperrormessage), 0) > 0)
       THEN
          RAISE value_error;
       END IF;

       BEGIN
         -- Verwerken v_xml_doc:
         -- dbms_output.put_line('Verwerken bestand: ' || v_ftpfilename);

         CDM_naar_helix( p_xml_doc => v_xml_doc );

         -- Bronbestand verwijderen als de verwerking van het bestand geslaagd was
         v_ftperrormessage := NULL;
         v_ftperrormessage := kgc_ftp.ftpdelete( p_servername => v_ftpservername
                                               , p_port       => '21'
                                               , p_username   => v_ftpusername
                                               , p_password   => v_ftppassword
                                               , p_sourcefile => v_ftpremotedir || '/' || v_ftpfilename );
         -- dbms_output.put_line('Verwijdering uitgevoerd ' || v_ftpErrorMessage);
         IF (nvl(length(v_ftperrormessage), 0) > 0)
         THEN
            RAISE e_delete_error;
         END IF;
       EXCEPTION
         WHEN e_delete_error THEN
           RAISE value_error; -- verwerking afbreken
         WHEN OTHERS THEN
           NULL; -- verwerking van het v_xml_doc was niet succesvol: bronbestand niet verwijderen
       END;
     END IF;
   END LOOP;
   v_ftpdirfiles.DELETE;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
     -- Foutmelding-tekst samenstellen:
     v_errormessage := 'kgc_cdm_00.verwerk_bestanden error: ';
     IF (nvl(length(v_ftperrormessage), 0) > 0)
     THEN
        kgc_import.addtext(v_errormessage, 32767, v_ftperrormessage);
     ELSE -- just-in-case
        kgc_import.addtext(v_errormessage, 32767, SQLERRM);
        v_msg := cg$errors.GetErrors;
        IF nvl(length(v_msg), 0) > 0 THEN
           kgc_import.addtext(v_errormessage, 32767, 'cg$errors: ' || v_msg);
        END IF;
     END IF;

     -- Foutmelding-tekst mailen naar beheerders:
     kgc_zis.post_mail_zis( p_sypa_aan  => 'CDM_MAIL_ERR'
                          , p_onderwerp => 'kgc_cdm_00.verwerk_bestanden: Fout (database: ' || upper(database_name) || ')!'
                          , p_inhoud    => v_errormessage
                          , p_html      => FALSE
                          , p_conditie  => NULL
                          , p_sypa_van  => 'CDM_MAIL_AFZENDER');
END verwerk_bestanden;
end kgc_cdm_00;
/

/
QUIT
