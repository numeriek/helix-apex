CREATE OR REPLACE PACKAGE "HELIX"."KGC_STOF_00" IS
FUNCTION get_stof_record
  ( p_stof_id IN NUMBER
  , p_type IN VARCHAR
  )
RETURN VARCHAR2;
END KGC_STOF_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_STOF_00" IS
FUNCTION get_stof_record
  ( p_stof_id IN NUMBER
  , p_type IN VARCHAR
  )
RETURN varchar2 IS

  cursor c_stof (b_stof_id in number)
  is
  select code
  ,      omschrijving
  from kgc_stoftesten
  where stof_id = b_stof_id
  ;
  v_code kgc_stoftesten.code%type;
  v_omschrijving kgc_stoftesten.omschrijving%type;

BEGIN

  open c_stof (b_stof_id => p_stof_id);
  fetch c_stof into v_code, v_omschrijving;
  close c_stof;

  if p_type = 'code'
  then
    return (v_code);
  elsif p_type = 'omschrijving'
  then
    return (v_omschrijving);
  end if;

END;
END KGC_STOF_00;
/

/
QUIT
