CREATE OR REPLACE PACKAGE "HELIX"."KGC_REKEN" IS

-- retourneer null als waarde niet-numeriek is
FUNCTION numeriek
( p_waarde  IN  VARCHAR2
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( numeriek, WNDS, WNPS, RNPS );

-- ontdoe waarde van alle niet-numerieke tekens
FUNCTION rekenwaarde
( p_meetwaarde  IN  VARCHAR2
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( rekenwaarde, WNDS, WNPS, RNPS );

END KGC_REKEN;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_REKEN" IS
  FUNCTION numeriek
  ( p_waarde  IN  VARCHAR2
  ) RETURN NUMBER
  IS
BEGIN
    RETURN TO_NUMBER(p_waarde);
  EXCEPTION
    WHEN VALUE_ERROR THEN RETURN TO_NUMBER(NULL);
  END;

  FUNCTION rekenwaarde
  ( p_meetwaarde  IN  VARCHAR2
  )
  RETURN NUMBER
  IS
    v_waarde varchar2(1000);
    v_return NUMBER;
  BEGIN
    if ( length( p_meetwaarde) > 40 )
    then -- zeker niet-numeriek
      v_return := to_number(null);
    elsif ( p_meetwaarde IS NOT NULL )
    THEN
      FOR i IN 1..length(p_meetwaarde)
      LOOP
        if ( substr( p_meetwaarde, i, 1 ) in ( '0','1','2','3','4','5','6','7','8','9','+','-','.',',' ) )
        then
          v_waarde := v_waarde || substr( p_meetwaarde, i, 1 );
        end if;
      END LOOP;
      v_return := numeriek( RTRIM(LTRIM(v_waarde)) );
    END IF;
    RETURN v_return;
  END rekenwaarde;
END KGC_REKEN;
/

/
QUIT
