CREATE OR REPLACE PACKAGE "HELIX"."KGC_DFT_INTERFACE" IS
/******************************************************************************
   NAME:       kgc_dft_interface
   PURPOSE:    controleren van declaratiegegevens en overzetten naar financieel systeem

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   	30-08-2010  GWE              Mantis 1268
                                           Extra functies haal_bericht en test_hl7
                                           t.b.v. het testen zonder echte koppeling
   1.0        30-06-2006  MKL              HLX-DECL-11395.
***************************************************************************** */

PROCEDURE open_verbinding
( p_zis_locatie in varchar2 := null
);

PROCEDURE sluit_verbinding
( p_zis_locatie in varchar2 := null
);

PROCEDURE controleer
( p_decl_id IN NUMBER
, p_zis_locatie in varchar2 := null
, x_ok out boolean
);

PROCEDURE verwerk
( p_decl_id IN NUMBER
, p_zis_locatie in varchar2 := null
, x_ok out boolean
);

PROCEDURE draai_terug
( p_decl_id IN NUMBER
, p_zis_locatie in varchar2 := null
, x_ok out boolean
);

procedure log
( p_tekst in out varchar2
, p_extra_tekst in varchar2
);

FUNCTION send_msg
( p_strHL7in   IN     VARCHAR2
, p_timeout    IN     NUMBER -- tienden van seconden
, x_strHL7out  IN OUT VARCHAR2
, x_msg        IN OUT VARCHAR2
) RETURN BOOLEAN;

-- functie die gebruikt wordt voor het testen van de interface
FUNCTION haal_bericht RETURN VARCHAR2;

-- functie die teruggeeft of de hl7 koppeling gesimuleerd wordt
FUNCTION test_hl7 RETURN BOOLEAN;
END KGC_DFT_INTERFACE;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DFT_INTERFACE" IS
/******************************************************************************
   NAME:       kgc_dft_interface
   PURPOSE:    controleren van declaratiegegevens en overzetten naar financieel systeem
               de inhoud van controle en verwerken is locatiespecifiek

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2          13-04-2007  RDE              Close connectie-fout afvangen
   1.0        30-06-2006  MKL              HLX-DECL-11395.
***************************************************************************** */

-- TCP/IP connection to HL7-communicatie-engine (Cloverleaf/TDM/...):
g_conn       utl_tcp.connection;
g_connected  boolean := FALSE;
g_test       boolean := FALSE;

-- bericht dat wordt veiliggesteld voor het gebruik in de test package beh_dft_simulatie
g_hl7_bericht VARCHAR2(32767);

-- lokale procedure om verwerkingstekst te vullen
procedure log
( p_tekst in out varchar2
, p_extra_tekst in varchar2
)
is
begin
  if ( p_extra_tekst is not null )
  then
    if ( p_tekst is null )
    then
      p_tekst := p_extra_tekst;
    else
      p_tekst := substr(p_extra_tekst || chr(10) || p_tekst, 1, 4000);
    end if;
  end if;
end log;


FUNCTION test_hl7 RETURN BOOLEAN IS
BEGIN
  RETURN g_test;
END;

FUNCTION haal_bericht RETURN VARCHAR2 IS
BEGIN
  RETURN g_hl7_bericht;
END;

PROCEDURE schrijf_test_bericht(p_hl7_bericht IN VARCHAR2)
IS
  v_statement VARCHAR2(2000);
BEGIN
  -- eerst veiligstellen bericht
  g_hl7_bericht := p_hl7_bericht;

  -- aanroep schrijf actie
  v_statement := 'begin beh_hl7_simulatie.schrijf_dft_bericht; end;';
  EXECUTE IMMEDIATE v_statement;

END;

PROCEDURE open_verbinding
( p_zis_locatie in varchar2
)
is
  v_hl7_svr_naam_dft  kgc_systeem_parameters.standaard_waarde%type;
  v_hl7_svr_poort_dft NUMBER;
  v_sqlerrm           VARCHAR2(4000);
begin
  dbms_output.put_line('KGC_DFT_INTERFACE.open_verbinding - start');
  v_hl7_svr_poort_dft := kgc_zis.getHL7Poort('ZIS_HL7_SVR_POORT_DFT');
  v_hl7_svr_naam_dft := kgc_sypa_00.systeem_waarde('ZIS_HL7_SVR_NAAM_DFT');
  IF NVL(LENGTH(v_hl7_svr_naam_dft),0) = 0 THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_param0 => 'Geen standaardwaarde gevonden voor systeemparameter ZIS_HL7_SVR_NAAM_DFT!'
    , p_param1 => ''
    , p_errtp => 'E'
    , p_rftf => true
    );
    RETURN;
  END IF;
  dbms_output.put_line('ZIS_HL7_SVR_NAAM_DFT = ' || v_hl7_svr_naam_dft);
  dbms_output.put_line('ZIS_HL7_SVR_POORT_DFT = ' || to_char(v_hl7_svr_poort_dft));

  IF g_connected THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_param0 => 'De verbinding naar ' || v_hl7_svr_naam_dft || ':'
                  || to_char(v_hl7_svr_poort_dft) || ' is reeds geopend!'
    , p_param1 => sqlerrm
    , p_errtp => 'E'
    , p_rftf => true
    );
  ELSE
    IF g_test
    THEN
      g_connected := TRUE;
    ELSE
      BEGIN
        g_conn := utl_tcp.open_connection
          ( remote_host => v_hl7_svr_naam_dft
          , remote_port =>  v_hl7_svr_poort_dft
          , tx_timeout => 0.5 -- seconden! Zie doc: dit is niet de tijd voor wacht-op-connectie, maar wacht bij read!!! 0=nietwachten, null=waitforever
        );
        g_connected := TRUE;
      EXCEPTION
        WHEN OTHERS THEN
          v_sqlerrm := sqlerrm; -- fout opbergen
          BEGIN -- v2
            UTL_TCP.close_connection(g_conn);
          EXCEPTION
            WHEN OTHERS THEN NULL;
          END;
          g_connected := FALSE;
          qms$errors.show_message
          ( p_mesg => 'KGC-00000'
          , p_param0 => 'Fout in verbinding naar ' || v_hl7_svr_naam_dft || ':'
                    || to_char(v_hl7_svr_poort_dft) || '!'
          , p_param1 => v_sqlerrm
          , p_errtp => 'E'
          , p_rftf => true
          );
      END;
    END IF;
  END IF;
  dbms_output.put_line('KGC_DFT_INTERFACE.open_verbinding - einde');
exception
  when others
  then
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_param0 => 'Verbinding is niet geopend!'
    , p_param1 => sqlerrm
    , p_errtp => 'E'
    , p_rftf => true
    );
end open_verbinding;

PROCEDURE sluit_verbinding
( p_zis_locatie in varchar2 := null
)
is
begin
  IF g_connected THEN
    g_connected := FALSE;
    IF g_test THEN
      NULL;
    ELSE
      UTL_TCP.close_connection(g_conn);
    END IF;
  END IF;
exception
  when others
  then
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_param0 => 'Verbinding is niet gesloten!'
    , p_param1 => sqlerrm
    , p_errtp => 'E'
    , p_rftf => true
    );
end sluit_verbinding;

PROCEDURE controleer
( p_decl_id IN NUMBER
, p_zis_locatie in varchar2 := null
, x_ok out boolean
)
is
  v_zis_locatie varchar2(100) := substr( upper( p_zis_locatie ), 1, 100 );
  cursor decl_cur
  is
    select decl.decl_id
    ,      decl.ingr_id
    ,      decl.verz_id
    ,      decl.nota_adres
    ,      decl.verzekeringsnr
    ,      decl.verwerking
    ,      pers.zisnr
    ,      pers.overleden
    from   kgc_personen pers
    ,      kgc_declaraties decl
    where  decl.pers_id = pers.pers_id
    and    decl.decl_id = p_decl_id
    and    decl.declareren = 'J'
    and    nvl( decl.status, 'H' ) = 'H'
    for update of decl.verwerking nowait
    ;
  decl_rec decl_cur%rowtype;
begin
  if ( p_decl_id is null )
  then
    x_ok := false;
    return;
  end if;

  x_ok := true;
  if ( v_zis_locatie is null )
  then
    begin
      v_zis_locatie := kgc_sypa_00.standaard_waarde
                       ( p_parameter_code => 'ZIS_LOCATIE'
                       , p_kafd_id => null -- niet afdelingsspecifiek!
                       );
    exception
      when others
      then
        null;
    end;
  end if;
  if ( v_zis_locatie = 'NIJMEGEN' )
  then
    kgc_dft_nijmegen.controleer
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  elsif ( v_zis_locatie = 'UTRECHT' )
  then
    kgc_dft_utrecht.controleer
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  elsif ( v_zis_locatie = 'MAASTRICHT' )
  then
    kgc_dft_maastricht.controleer
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  else
    --
    -- er is default controle procedure hier: wordt in werkelijkheid geschreven op locatie (zie hierboven)
    --
    open  decl_cur;
    fetch decl_cur
    into  decl_rec;
    close decl_cur;
    decl_rec.verwerking := null; -- init/reset
    log( decl_rec.verwerking, 'Controle op '||to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')||':' );
    if ( decl_rec.ingr_id is null )
    then
      log( decl_rec.verwerking, 'Niet OK: geen machtigingsindicatie' );
      x_ok := false;
    end if;
    if ( decl_rec.verz_id is null and decl_rec.nota_adres is null)
    then
      log( decl_rec.verwerking, 'Niet OK: geen verzekeraar of alternatief nota-adres' );
      x_ok := false;
    end if;
    if ( decl_rec.verzekeringsnr is null)
    then
      log( decl_rec.verwerking, 'Niet OK: geen verzekeringsnummer' );
      x_ok := false;
    end if;
    if ( decl_rec.zisnr is null)
    then
      log( decl_rec.verwerking, 'Niet OK: geen ZISnr' );
      x_ok := false;
    end if;
    if ( decl_rec.overleden = 'J')
    then
      log( decl_rec.verwerking, 'Niet OK: persoon is overleden' );
      x_ok := false;
    end if;
    if ( x_ok )
    then
      log( decl_rec.verwerking, 'Controle ok.' );
    end if;
    update kgc_declaraties
    set    verwerking = decl_rec.verwerking
    where  decl_id = decl_rec.decl_id
    ;
    commit;
  end if;
exception
when others
then
  x_ok := false;
  log( decl_rec.verwerking, 'Fout: '||sqlerrm );
  update kgc_declaraties
  set    verwerking = decl_rec.verwerking
  where  decl_id = decl_rec.decl_id
  ;
  commit;
end controleer;

PROCEDURE verwerk
( p_decl_id IN NUMBER
, p_zis_locatie in varchar2
, x_ok out boolean
)
is
  v_zis_locatie varchar2(100) := substr( upper( p_zis_locatie ), 1, 100 );
  cursor decl_cur
  is
    select decl.decl_id
    ,      decl.status
    ,      decl.datum_verwerking
    ,      decl.verwerking
    from   kgc_declaraties decl
    where  decl.decl_id = p_decl_id
    and    decl.declareren = 'J'
    and    nvl( decl.status, 'H' ) = 'H'
    for update of decl.verwerking nowait
    ;
  decl_rec decl_cur%rowtype;
  v_sqlerrm varchar2(4000);
begin
  if ( p_decl_id is null )
  then
    return;
  end if;
  if ( v_zis_locatie is null )
  then
    begin
      v_zis_locatie := kgc_sypa_00.standaard_waarde
                       ( p_parameter_code => 'ZIS_LOCATIE'
                       , p_kafd_id => null -- niet afdelingsspecifiek!
                       );
    exception
      when others
      then
        null;
    end;
  end if;
  if ( v_zis_locatie = 'NIJMEGEN' )
  then
    kgc_dft_nijmegen.verwerk
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  elsif ( v_zis_locatie = 'UTRECHT' )
  then
    kgc_dft_utrecht.verwerk
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  elsif ( v_zis_locatie = 'MAASTRICHT' )
  then
    kgc_dft_maastricht.verwerk
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  else
    open  decl_cur;
    fetch decl_cur
    into  decl_rec;
    close decl_cur;
    --
    -- er is geen overzet procedure hier: wordt in werkelijkheid geschreven op locatie (zie hierboven)
    --
    x_ok := true;
    log( decl_rec.verwerking, 'Verwerkt op '||to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')||'.' );
    update kgc_declaraties
    set    verwerking = decl_rec.verwerking
    ,      status = 'V'
    ,      datum_verwerking = trunc( sysdate )
    where  decl_id = decl_rec.decl_id
    ;
    commit;
  end if;
exception
when others
then
  v_sqlerrm := substr(sqlerrm,1,4000);
  x_ok := false;
  log( decl_rec.verwerking, 'Fout: '||v_sqlerrm );
  update kgc_declaraties
  set    verwerking = decl_rec.verwerking
  where  decl_id = decl_rec.decl_id
  ;
  commit;
  qms$errors.show_message
  ( p_mesg => 'KGC-00081'
  , p_param0 => 'Fatale fout onstaan tijdens verwerking van declaratie '||p_decl_id||'.'
    ||chr(10)|| 'De verdere verwerking is gestopt!'
  , p_param1 => v_sqlerrm
  );
end verwerk;

PROCEDURE draai_terug
( p_decl_id IN NUMBER
, p_zis_locatie in varchar2 := null
, x_ok out boolean
)
is
  v_zis_locatie varchar2(100) := substr( upper( p_zis_locatie ), 1, 100 );
  cursor decl_cur
  is
    select decl.decl_id
    ,      decl.status
    ,      decl.datum_verwerking
    ,      decl.verwerking
    from   kgc_declaraties decl
    where  decl.decl_id = p_decl_id
    and    decl.declareren = 'J'
    and    decl.status = 'V'
    for update of decl.verwerking nowait
    ;
  decl_rec decl_cur%rowtype;
begin
  if ( p_decl_id is null )
  then
    return;
  end if;
  if ( v_zis_locatie is null )
  then
    begin
      v_zis_locatie := kgc_sypa_00.standaard_waarde
                       ( p_parameter_code => 'ZIS_LOCATIE'
                       , p_kafd_id => null -- niet afdelingsspecifiek!
                       );
    exception
      when others
      then
        null;
    end;
  end if;
  if ( v_zis_locatie = 'NIJMEGEN' )
  then
    kgc_dft_nijmegen.draai_terug
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  elsif ( v_zis_locatie = 'UTRECHT' )
  then
    kgc_dft_utrecht.draai_terug
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  elsif ( v_zis_locatie = 'MAASTRICHT' )
  then
    kgc_dft_maastricht.draai_terug
    ( p_decl_id => p_decl_id
    , x_ok => x_ok
    );
  else
    open  decl_cur;
    fetch decl_cur
    into  decl_rec;
    close decl_cur;
    --
    -- er is geen terugdraai procedure hier: wordt in werkelijkheid geschreven op locatie (zie hierboven)
    --
    x_ok := true;
    log( decl_rec.verwerking, 'Teruggedraaid op '||to_char(sysdate,'dd-mm-yyyy  hh24:mi:ss')||'.' );
    update kgc_declaraties
    set    verwerking = decl_rec.verwerking
    ,      status = 'R'
    ,      datum_verwerking = trunc( sysdate )
    where  decl_id = decl_rec.decl_id
    ;
    commit;
  end if;
exception
when others
then
  x_ok := false;
  log( decl_rec.verwerking, 'Fout: '||sqlerrm );
  update kgc_declaraties
  set    verwerking = decl_rec.verwerking
  where  decl_id = decl_rec.decl_id
  ;
  commit;
end draai_terug;

-- deze functie is een verbeterde versie van de functie kgc_zis.send_msg
FUNCTION send_msg
( p_strHL7in   IN     VARCHAR2
, p_timeout    IN     NUMBER -- tienden van seconden
, x_strHL7out  IN OUT VARCHAR2
, x_msg        IN OUT VARCHAR2
) RETURN BOOLEAN IS
  v_ret_val          BOOLEAN := TRUE;
  v_ret_conn         pls_integer;
  v_data_available   pls_integer;
  v_NrCharRec        pls_integer;
  v_loop             NUMBER := 0;
  v_str              VARCHAR2(32000);
  v_strAntw          VARCHAR2(32000);
  v_thisproc         VARCHAR2(100) := 'kgc_zis.send_msg';
BEGIN
  IF NOT g_connected THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_param0 => 'Fout in ' || v_thisproc || ': eerst moet de verbinding geopend worden!'
    , p_param1 => sqlerrm
    , p_errtp => 'E'
    , p_rftf => true
    );
  END IF;
  kgc_zis.log(v_thisproc || ': Connectie is geopend; message wordt verzonden...');
  IF g_test
  THEN
    schrijf_test_bericht(p_strHL7in);
    RETURN TRUE;
  ELSE
    v_ret_conn := utl_tcp.write_raw(g_conn, UTL_RAW.Cast_To_Raw(p_strHL7in)); -- send request
    IF v_ret_conn = 0 THEN -- aantal verzonden bytes
      x_msg := v_thisproc || ': Error: er kon geen bericht verzonden worden!';
      kgc_zis.log(x_msg);
      RETURN FALSE;
    END IF;

    kgc_zis.log(v_thisproc || ': Message is verzonden; wacht op antwoord...');
    WHILE TRUE LOOP
      v_loop := v_loop + 1;
      v_data_available := utl_tcp.available(g_conn);
      -- terugkomende gegevens ophalen:
      IF v_data_available > 0
      THEN
        BEGIN
          LOOP
            IF v_data_available = 0
            THEN
              EXIT;
            END IF;
            v_NrCharRec := utl_tcp.read_text(g_conn, v_str, 1024); -- zie doc:'On some platforms, this function (= available) may only return 1, to indicate that some data is available'
            v_strAntw := v_strAntw || v_str;
            v_data_available := utl_tcp.available(g_conn); -- meer?
          END LOOP;
        EXCEPTION
          WHEN utl_tcp.end_of_input THEN
            NULL; -- end of input; geen fout!
          WHEN OTHERS THEN
            IF SQLCODE = -29260 THEN
              NULL; -- network error: TNS:connection closed; geen fout!
            ELSE
              raise;
            END IF;
        END;
        -- gegevens gelezen? => klaar!
        kgc_zis.log(v_thisproc || ': Antwoord ingelezen!');
        EXIT;
      END IF;
      IF v_loop < p_timeout THEN -- effe wachten () en doorgaan
        dbms_lock.sleep(0.1); -- seconden! sys must grant execute on dbms_lock to ...;
      ELSE -- timeout!!!
        x_msg := 'Timeout na ' || to_char(p_timeout / 10) || ' seconden!';
        kgc_zis.log(v_thisproc || ': ' || x_msg);
        v_ret_val := FALSE;
        EXIT;
      END IF;
    END LOOP;
    IF NVL(LENGTH(v_strAntw), 0) = 0 AND v_ret_val = TRUE THEN
      x_msg := 'Lege reactie ontvangen!';
      v_ret_val := FALSE;
    END IF;
    x_strHL7out := v_strAntw;
    RETURN v_ret_val;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    x_msg := v_thisproc || ': Error: '|| SQLERRM;
    kgc_zis.log(x_msg);
    RETURN FALSE;
END send_msg;
BEGIN
  -- initialisatie van de verwerking
  -- bepaal g_test
  begin
    IF kgc_sypa_00.standaard_waarde
                  ( p_parameter_code => 'BEH_DFT_TEST'
                  , p_kafd_id => null -- niet afdelingsspecifiek!
                   ) = 'J'
    THEN
      g_test := TRUE;
    END IF;
  exception
    when others
    then
      null;
  end;

END kgc_dft_interface;
/

/
QUIT
