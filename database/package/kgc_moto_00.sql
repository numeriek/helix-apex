CREATE OR REPLACE PACKAGE "HELIX"."KGC_MOTO_00" IS
-- bepaal of de module toegankelijk is voor een medewerker (direct of via een rol)
-- registratie staat in kgc_module_toegang
-- aannames:
-- de rol beheer mag alles

FUNCTION toegang
 (  p_module           IN VARCHAR2
  , p_meet_id          IN NUMBER   := NULL
  , p_meti_id          IN NUMBER   := NULL
  , p_frac_id          IN NUMBER   := NULL
  , p_onmo_id          IN NUMBER   := NULL
  , p_mons_id          IN NUMBER   := NULL
  , p_onde_id          IN NUMBER   := NULL
  , p_entiteit         IN VARCHAR2 := NULL
  , p_id               IN NUMBER   := NULL
  , p_kafd_id          IN NUMBER   := NULL
  , p_toegangscontrole IN VARCHAR2 := 'N'
 )
RETURN  VARCHAR2; -- bij toegang 'null', bij geen toegang de KGC-melding.

FUNCTION toegang_jn
 (  p_module           IN VARCHAR2
  , p_meet_id          IN NUMBER   := NULL
  , p_meti_id          IN NUMBER   := NULL
  , p_frac_id          IN NUMBER   := NULL
  , p_onmo_id          IN NUMBER   := NULL
  , p_mons_id          IN NUMBER   := NULL
  , p_onde_id          IN NUMBER   := NULL
  , p_entiteit         IN VARCHAR2 := NULL
  , p_id               IN NUMBER   := NULL
  , p_kafd_id          IN NUMBER   := NULL
  , p_toegangscontrole IN VARCHAR2 := 'N'
 )
RETURN  VARCHAR2; -- bij toegang 'J', bij geen toegang 'N'.
END KGC_MOTO_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MOTO_00" IS
/*******************************************************************************
    NAME:       kgc_moto_00
    PURPOSE:    Controleren of toegang tot een module is toegestaan
    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.3        24-11-2014  Y.Arts           Mantis 8633 :
                                            KGCBEST02 toegevoegd
    1.2        29-04-2009  JKO              Mantis 2072 rework:
                                            p_toegangscontrole 'P' ingebouwd.
    1.1        25-02-2009  JKO              Mantis 2072:
                                            Function  'toegang' aangepast
    1.0        19-12-2008  JKO              Mantis 2072:
                                            Function  'toegang' herschreven
                                            Procedure 'toegang' verwijderd
*******************************************************************************/

FUNCTION toegang
 (  p_module           IN VARCHAR2
  , p_meet_id          IN NUMBER   := NULL
  , p_meti_id          IN NUMBER   := NULL
  , p_frac_id          IN NUMBER   := NULL
  , p_onmo_id          IN NUMBER   := NULL
  , p_mons_id          IN NUMBER   := NULL
  , p_onde_id          IN NUMBER   := NULL
  , p_entiteit         IN VARCHAR2 := NULL
  , p_id               IN NUMBER   := NULL
  , p_kafd_id          IN NUMBER   := NULL
  , p_toegangscontrole IN VARCHAR2 := 'N' -- 'N': nee; 'J': ja; 'P': ja, bij aanroep vanuit patienthistorie KGCPAHI05
 )
RETURN VARCHAR2 -- bij toegang: 'null', bij geen toegang: de KGC-melding.
IS
  CURSOR moto_cur
  ( b_module in varchar2
  , b_mede_id in number
  )
  IS
    SELECT moto.mede_id
    ,      moto.andere_kafd
    FROM   kgc_module_toegang moto
    WHERE  moto.module = b_module
    AND    moto.mede_id = b_mede_id
    union
    SELECT moto.mede_id -- = null
    ,      moto.andere_kafd
    FROM   kgc_mede_kafd meka
    ,      kgc_module_toegang moto
    WHERE  moto.module = b_module
    AND    moto.mede_id is null
    AND    meka.rol = moto.rol
    and    meka.mede_id = b_mede_id
    ORDER BY 1 ASC  -- eerst medewerker-specifiek
    ,        2 ASC -- eerst J dan N
    ;
  moto_rec moto_cur%rowtype;
  v_dummy varchar2(1);

-- 2-dimensionale PL/SQL tabellen tbv. controle A1 en A2:
TYPE parameter_tabtype IS VARRAY(10) OF VARCHAR2(20)
  ;
  i_par      BINARY_INTEGER;
  i_par_mons BINARY_INTEGER;
  i_par_onde BINARY_INTEGER;
TYPE module_record IS RECORD
  ( module    kgc_module_toegang.module%type
  , parameter parameter_tabtype
  );
TYPE module_tabtype IS VARRAY(20) OF module_record
  ;
  i_mod BINARY_INTEGER;

  v_module_tab_A1 module_tabtype;
  v_module_tab_A2 module_tabtype;

  -- Losse variabelen:
  i_tmp            BINARY_INTEGER;
  v_kafd_id        kgc_kgc_afdelingen.kafd_id%TYPE;
  v_kafd_id_mons   kgc_kgc_afdelingen.kafd_id%TYPE;
  v_kafd_id_onde   kgc_kgc_afdelingen.kafd_id%TYPE;
  v_ongr_id        kgc_onderzoeksgroepen.ongr_id%TYPE;
  v_zisnr          kgc_personen.zisnr%TYPE;
  v_afgerond       kgc_onderzoeken.afgerond%TYPE;
  v_koppeling_oke  BOOLEAN; -- resultaat van controle A1/2b

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION zoek_module
    ( p_module_tab IN module_tabtype
    , p_module     IN VARCHAR2
    )
    RETURN NUMBER
    IS
      i_ret BINARY_INTEGER; -- Index return-waarde
    BEGIN
      i_ret := null;
      FOR i_tmp IN 1 .. p_module_tab.last
      LOOP
        IF p_module_tab(i_tmp).module = p_module
        THEN
          i_ret := i_tmp;
        EXIT;
        END IF;
      END LOOP;
      --
      --dbms_output.put_line( 'Module-IN//i_ret-OUT: '||p_module||'//'||NVL(TO_CHAR(i_ret), '=NIET GEVONDEN=') );
      --
      RETURN(i_ret);
    END zoek_module;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION zoek_eerstgevulde_parameter
    ( p_module_tab IN module_tabtype
    , p_parameter  IN VARCHAR2 := NULL
    )
    RETURN NUMBER
    IS
      i_ret BINARY_INTEGER; -- Index return-waarde
    BEGIN
      i_ret := null;
      FOR i_tmp IN 1 .. p_module_tab(i_mod).parameter.last
      LOOP
        IF p_module_tab(i_mod).parameter(i_tmp) = NVL( p_parameter, p_module_tab(i_mod).parameter(i_tmp) )
        THEN
          -- parameter moet gevuld zijn
          IF    p_module_tab(i_mod).parameter(i_tmp) = 'P_MEET_ID'
          THEN
            IF p_meet_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          ELSIF p_module_tab(i_mod).parameter(i_tmp) = 'P_METI_ID'
          THEN
            IF p_meti_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          ELSIF p_module_tab(i_mod).parameter(i_tmp) = 'P_FRAC_ID'
          THEN
            IF p_frac_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          ELSIF p_module_tab(i_mod).parameter(i_tmp) = 'P_ONMO_ID'
          THEN
            IF p_onmo_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          ELSIF p_module_tab(i_mod).parameter(i_tmp) = 'P_MONS_ID'
          THEN
            IF p_mons_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          ELSIF p_module_tab(i_mod).parameter(i_tmp) = 'P_ONDE_ID'
          THEN
            IF p_onde_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          ELSIF p_module_tab(i_mod).parameter(i_tmp) = 'P_KAFD_ID'
          THEN
            IF p_kafd_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          ELSIF p_module_tab(i_mod).parameter(i_tmp) = 'P_ENTI_ID' -- fictieve id bij notities
          THEN
            -- P_ENTITEIT moet toegestaan, en P_ID moet gevuld zijn
            IF p_entiteit IN ('OBMG', 'UITS', 'ONDE', 'MONS', 'FRAC', 'GESP', 'GEBE', 'DIAG', 'ONBE', 'WLST') AND
               p_id IS NOT NULL
            THEN
              i_ret := i_tmp;
              EXIT;
            END IF;
          END IF;
        END IF;
      END LOOP;
      --
      --IF i_ret IS NULL
      --THEN
      --  dbms_output.put_line( 'Module-IN(i_mod-IN)/parameter-IN//parameter-OUT(i_ret-OUT): '||p_module_tab(i_mod).module||'('||TO_CHAR(i_mod)||')/'||NVL(p_parameter, '=LEEG=')||'//'||'=NIET GEVONDEN=(=NIET GEVONDEN=)' );
      --ELSE
      --  dbms_output.put_line( 'Module-IN(i_mod-IN)/parameter-IN//parameter-OUT(i_ret-OUT): '||p_module_tab(i_mod).module||'('||TO_CHAR(i_mod)||')/'||NVL(p_parameter, '=LEEG=')||'//'||p_module_tab(i_mod).parameter(i_ret)||'('||TO_CHAR(i_ret)||')' );
      --END IF;
      --
      RETURN(i_ret);
    END zoek_eerstgevulde_parameter;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION kafd_id_frac
    ( p_frac_id IN bas_fracties.frac_id%TYPE )
    RETURN NUMBER
    IS
      CURSOR frac_cur
      ( b_frac_id bas_fracties.frac_id%TYPE )
      IS
        SELECT frac.kafd_id
          FROM bas_fracties frac
         WHERE frac.frac_id = b_frac_id
       ;
     frac_rec frac_cur%ROWTYPE := NULL;
    BEGIN
      OPEN   frac_cur( p_frac_id );
      FETCH  frac_cur
      INTO   frac_rec;
      CLOSE  frac_cur;
      RETURN frac_rec.kafd_id;
    END kafd_id_frac;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION kafd_id_onde
    ( p_onde_id IN kgc_onderzoeken.onde_id%TYPE )
    RETURN NUMBER
    IS
      CURSOR onde_cur
      ( b_onde_id kgc_onderzoeken.onde_id%TYPE )
      IS
        SELECT onde.kafd_id
          FROM kgc_onderzoeken onde
         WHERE onde.onde_id = b_onde_id
       ;
     onde_rec onde_cur%ROWTYPE := NULL;
    BEGIN
      OPEN   onde_cur( p_onde_id );
      FETCH  onde_cur
      INTO   onde_rec;
      CLOSE  onde_cur;
      RETURN onde_rec.kafd_id;
    END kafd_id_onde;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION kafd_id_mons
    ( p_mons_id IN kgc_monsters.mons_id%TYPE )
    RETURN NUMBER
    IS
      CURSOR mons_cur
      ( b_mons_id kgc_monsters.mons_id%TYPE )
      IS
        SELECT mons.kafd_id
          FROM kgc_monsters mons
         WHERE mons.mons_id = b_mons_id
       ;
     mons_rec mons_cur%ROWTYPE := NULL;
    BEGIN
      OPEN   mons_cur( p_mons_id );
      FETCH  mons_cur
      INTO   mons_rec;
      CLOSE  mons_cur;
      RETURN mons_rec.kafd_id;
    END kafd_id_mons;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION kafd_id_onmo
    ( p_onmo_id IN kgc_onderzoek_monsters.onmo_id%TYPE )
    RETURN NUMBER
    IS
      CURSOR onmo_cur
      ( b_onmo_id kgc_onderzoek_monsters.onmo_id%TYPE )
      IS
        SELECT onmo.onde_id
          FROM kgc_onderzoek_monsters onmo
         WHERE onmo.onmo_id = b_onmo_id
       ;
     onmo_rec onmo_cur%ROWTYPE := NULL;
    BEGIN
      -- Stap 1: onde_id ophalen
      OPEN   onmo_cur( p_onmo_id );
      FETCH  onmo_cur
      INTO   onmo_rec;
      CLOSE  onmo_cur;
      -- Stap 2: afdeling ophalen mbv onde_id
      IF onmo_rec.onde_id IS NOT NULL
      THEN
        RETURN( kafd_id_onde( p_onde_id => onmo_rec.onde_id ) );
      ELSE
        RETURN( NULL );
      END IF;
    END kafd_id_onmo;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION kafd_id_meti
    ( p_meti_id IN bas_metingen.meti_id%TYPE )
    RETURN NUMBER
    IS
      CURSOR meti_cur
      ( b_meti_id bas_metingen.meti_id%TYPE )
      IS
        SELECT meti.onde_id
             , meti.frac_id
          FROM bas_metingen meti
         WHERE meti.meti_id = b_meti_id
       ;
     meti_rec meti_cur%ROWTYPE := NULL;
    BEGIN
      -- Stap 1: onde_id en frac_id ophalen (1 van beide altijd gevuld)
      OPEN   meti_cur( p_meti_id );
      FETCH  meti_cur
      INTO   meti_rec;
      CLOSE  meti_cur;
      -- Stap 2: afdeling ophalen mbv onde_id of frac_id
      IF    meti_rec.onde_id IS NOT NULL
      THEN
        RETURN( kafd_id_onde( p_onde_id => meti_rec.onde_id ) );
      ELSIF meti_rec.frac_id IS NOT NULL
      THEN
        RETURN( kafd_id_frac( p_frac_id => meti_rec.frac_id ) );
      ELSE
        RETURN( NULL );
      END IF;
    END kafd_id_meti;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION kafd_id_meet
    ( p_meet_id IN bas_meetwaarden.meet_id%TYPE )
    RETURN NUMBER
    IS
      CURSOR meet_cur
      ( b_meet_id bas_meetwaarden.meet_id%TYPE )
      IS
        SELECT meet.meti_id
          FROM bas_meetwaarden meet
         WHERE meet.meet_id = b_meet_id
       ;
     meet_rec meet_cur%ROWTYPE := NULL;
    BEGIN
      -- Stap 1: meti_id ophalen
      OPEN   meet_cur( p_meet_id );
      FETCH  meet_cur
      INTO   meet_rec;
      CLOSE  meet_cur;
      -- Stap 2: afdeling ophalen mbv meti_id
      IF meet_rec.meti_id IS NOT NULL
      THEN
        RETURN( kafd_id_meti( p_meti_id => meet_rec.meti_id ) );
      ELSE
        RETURN( NULL );
      END IF;
    END kafd_id_meet;

    -- LOCALE FUNCTION --------------------------------------------------------------------------
    FUNCTION afgerond_onde
    ( p_onde_id IN kgc_onderzoeken.onde_id%TYPE )
    RETURN VARCHAR2
    IS
      CURSOR onde_cur
      ( b_onde_id kgc_onderzoeken.onde_id%TYPE )
      IS
        SELECT onde.afgerond
          FROM kgc_onderzoeken onde
         WHERE onde.onde_id = b_onde_id
       ;
     onde_rec onde_cur%ROWTYPE := NULL;
    BEGIN
      OPEN   onde_cur( p_onde_id );
      FETCH  onde_cur
      INTO   onde_rec;
      CLOSE  onde_cur;
      RETURN onde_rec.afgerond;
    END afgerond_onde;

-- HOOFDVERWERKING ------------------------------------------------------------------------------
BEGIN
  -- Applicatie-eigenaar mag alles
  IF UPPER(kgc_sypa_00.standaard_waarde( 'APPLICATIE_EIGENAAR', 0 ) ) = USER
  THEN
    RETURN( NULL ); --Aanname Assai!
  END IF;

  -- Standaard controles S1 die voor elke module worden uitgevoerd ------------------------------
  -- Controle S1/1: geen registratie van module betekent vrije toegang:
  SELECT MIN( 1 )
  INTO  v_dummy
  FROM  kgc_module_toegang
  WHERE module = UPPER( p_module );
  IF v_dummy IS NULL
  THEN
    NULL; -- controle S1/1 oke, doorgaan met A1-controles
  ELSE
    -- Controle S1/2 dan wel S1/3: medewerker of rol is gekoppeld:
    OPEN  moto_cur( b_module => upper( p_module )
                  , b_mede_id => kgc_mede_00.medewerker_id
                  );
    FETCH moto_cur
    INTO  moto_rec;
    IF moto_cur%found
    THEN -- controle S1/2 dan wel S1/3 oke, doorgaan met A1-controles
      CLOSE moto_cur;
    ELSE -- controle S1/2 dan wel S1/3 nok, toegang weigeren
      CLOSE moto_cur;
      RETURN( 'KGC-00009' );
    END IF;
  END IF;

  -- Aanvullende controles A1 -------------------------------------------------------------------
  -- Controle A1/1-a: geen aanroep waarbij controle nodig is:
  IF p_toegangscontrole = 'N'
  THEN
    RETURN( NULL ); -- controle A1/1-a oke
  END IF;

  -- Vullen PL/SQL-tabel met module-gegevens waarvoor de aanvullende controle A1 geldt:
  v_module_tab_A1 := module_tabtype(); -- initialiseer lege varray
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'BASFLOW01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_METI_ID', 'P_FRAC_ID', 'P_MONS_ID', 'P_ONDE_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'BASISOL05';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_FRAC_ID', 'P_MONS_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'BASMEET05';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_MEET_ID', 'P_METI_ID', 'P_ONDE_ID', 'P_FRAC_ID', 'P_MONS_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'BASMETI05';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_METI_ID', 'P_ONDE_ID', 'P_FRAC_ID', 'P_MONS_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCANME01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_MEET_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCGESP01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCINDI03';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_MONS_ID', 'P_ONDE_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCMONS01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_MONS_ID' );
  -- Bij notities fictieve parameter 'P_ENTI_ID':
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCNOTI01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_ENTI_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCONBE01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCONDE01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCONDE15';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_KAFD_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCONIN01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCONMW05';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCPAHI10';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  -- Rapport patienthistorie:
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCPAHI51';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_KAFD_ID' );
  --
  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGC_CDM01';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_MEET_ID', 'P_METI_ID', 'P_FRAC_ID', 'P_ONMO_ID', 'P_ONDE_ID' );

  v_module_tab_A1.EXTEND; v_module_tab_A1(v_module_tab_A1.last).module := 'KGCBEST02';
  v_module_tab_A1(v_module_tab_A1.last).parameter := parameter_tabtype  ( 'P_KAFD_ID' );

  -- Vullen PL/SQL-tabel met module-gegevens waarvoor de aanvullende controle A2 geldt:
  v_module_tab_A2 := module_tabtype(); -- initialiseer lege varray
  --
  v_module_tab_A2.EXTEND; v_module_tab_A2(v_module_tab_A2.last).module := 'KGCGESP01';
  v_module_tab_A2(v_module_tab_A2.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  v_module_tab_A2.EXTEND; v_module_tab_A2(v_module_tab_A2.last).module := 'KGCMONS01';
  v_module_tab_A2(v_module_tab_A2.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  v_module_tab_A2.EXTEND; v_module_tab_A2(v_module_tab_A2.last).module := 'KGCONDE15';
  v_module_tab_A2(v_module_tab_A2.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  --
  if P_ONDE_ID is not null then
    v_module_tab_A2.EXTEND; v_module_tab_A2(v_module_tab_A2.last).module := 'KGCBEST02';
    v_module_tab_A2(v_module_tab_A2.last).parameter := parameter_tabtype  ( 'P_ONDE_ID' );
  end if;
  --

  -- Controle A1/1-b: module komt niet voor in de serie waarvoor aanvullende controle A1 geldt:
  i_tmp := zoek_module( p_module_tab => v_module_tab_A1
                      , p_module     => p_module
                      );
  IF i_tmp IS NULL
  THEN
    RETURN( NULL ); -- controle A1/1-b oke
  END IF;
  i_mod := i_tmp; -- i_mod bevat index van A1

  -- Controle A1/2:  gebruiker heeft toegang op grond van S1: is al gecontroleerd en oke.
  -- Controle A1/2a: geen parameter(s) meegegeven waarmee de afdeling herleid kan worden:
  IF v_module_tab_A1(i_mod).module = 'KGCINDI03'
  THEN -- parameters P_MONS_ID en P_ONDE_ID kunnen beide gevuld zijn; dan later beide afdelingen controleren
    i_par_mons := zoek_eerstgevulde_parameter( p_module_tab => v_module_tab_A1
                                             , p_parameter  => 'P_MONS_ID'
                                             );
    i_par_onde := zoek_eerstgevulde_parameter( p_module_tab => v_module_tab_A1
                                             , p_parameter  => 'P_ONDE_ID'
                                             );
    IF ( i_par_mons IS NULL AND
         i_par_onde IS NULL )
    THEN
      RETURN( NULL ); -- controle A1/2a oke
    END IF;
  ELSE -- 1 willekeurige parameter is voldoende
    i_par := zoek_eerstgevulde_parameter( p_module_tab => v_module_tab_A1
                                        , p_parameter  => NULL
                                        );
    IF i_par IS NULL
    THEN
      RETURN( NULL ); -- controle A1/2a oke
    END IF;
  END IF;

  -- Controle A1/2b: medewerker heeft een koppeling met de herleide afdeling:
  v_koppeling_oke := NULL;

  IF v_module_tab_A1(i_mod).module = 'KGCINDI03'
  THEN
    -- herleiden afdeling uit meegegeven parameters
    -- 1 of 2 parameters gebruiken: P_MONS_ID en/of P_ONDE_ID
    IF i_par_mons IS NOT NULL
    THEN
      v_kafd_id_mons := kafd_id_mons( p_mons_id => p_mons_id );
    ELSE
      v_kafd_id_mons := NULL;
    END IF;
    IF i_par_onde IS NOT NULL
    THEN
      v_kafd_id_onde := kafd_id_onde( p_onde_id => p_onde_id );
    ELSE
      v_kafd_id_onde := NULL;
    END IF;
    --
    --dbms_output.put_line( '2 parms v_kafd_id mons/onde: '||v_kafd_id_mons||'/'||v_kafd_id_onde );
    --
    IF    ( v_kafd_id_mons IS NOT NULL AND
            v_kafd_id_onde IS NOT NULL )
    THEN
      -- Controle A1/2b medewerker heeft een koppeling met de afdeling van het monster en het onderzoek
      IF ( kgc_kafd_00.eigen_afdeling( p_kafd_id => v_kafd_id_mons ) AND
           kgc_kafd_00.eigen_afdeling( p_kafd_id => v_kafd_id_onde ) )
      THEN
        v_koppeling_oke := TRUE;  -- controle A1/2b oke
      ELSE
        v_koppeling_oke := FALSE; -- controle A1/2b nok
      END IF;
    ELSIF ( v_kafd_id_mons IS NOT NULL AND
            v_kafd_id_onde IS NULL )
    THEN
      -- Controle A1/2b medewerker heeft een koppeling met de afdeling van het monster
      IF kgc_kafd_00.eigen_afdeling( p_kafd_id => v_kafd_id_mons )
      THEN
        v_koppeling_oke := TRUE;  -- controle A1/2b oke
      ELSE
        v_koppeling_oke := FALSE; -- controle A1/2b nok
      END IF;
    ELSIF ( v_kafd_id_mons IS NULL AND
            v_kafd_id_onde IS NOT NULL )
    THEN
      -- Controle A1/2b medewerker heeft een koppeling met de afdeling van het onderzoek
      IF kgc_kafd_00.eigen_afdeling( p_kafd_id => v_kafd_id_onde )
      THEN
        v_koppeling_oke := TRUE;  -- controle A1/2b oke
      ELSE
        v_koppeling_oke := FALSE; -- controle A1/2b nok
      END IF;
    END IF;
  ELSE
    -- herleiden afdeling uit meegegeven parameters
    -- 1 parameter gebruiken
    IF    v_module_tab_A1(i_mod).parameter(i_par) = 'P_MEET_ID'
    THEN
      v_kafd_id := kafd_id_meet( p_meet_id => p_meet_id );
    ELSIF v_module_tab_A1(i_mod).parameter(i_par) = 'P_METI_ID'
    THEN
      v_kafd_id := kafd_id_meti( p_meti_id => p_meti_id );
    ELSIF v_module_tab_A1(i_mod).parameter(i_par) = 'P_FRAC_ID'
    THEN
      v_kafd_id := kafd_id_frac( p_frac_id => p_frac_id );
    ELSIF v_module_tab_A1(i_mod).parameter(i_par) = 'P_ONMO_ID'
    THEN
      v_kafd_id := kafd_id_onmo( p_onmo_id => p_onmo_id );
    ELSIF v_module_tab_A1(i_mod).parameter(i_par) = 'P_MONS_ID'
    THEN
      v_kafd_id := kafd_id_mons( p_mons_id => p_mons_id );
    ELSIF v_module_tab_A1(i_mod).parameter(i_par) = 'P_ONDE_ID'
    THEN
      v_kafd_id := kafd_id_onde( p_onde_id => p_onde_id );
    ELSIF v_module_tab_A1(i_mod).parameter(i_par) = 'P_KAFD_ID'
    THEN
      v_kafd_id := p_kafd_id;
    ELSIF v_module_tab_A1(i_mod).parameter(i_par) = 'P_ENTI_ID'
    THEN
      kgc_word_00.kafd_id_ongr_id_van_entiteit( p_entiteit_code => p_entiteit
                                              , p_entiteit_pk   => p_id
                                              , p_kafd_id       => v_kafd_id
                                              , p_ongr_id       => v_ongr_id
                                              , p_zisnr         => v_zisnr
                                              );
    END IF;
    --
    --dbms_output.put_line( '1 parm v_kafd_id: '||v_kafd_id );
    --
    IF v_kafd_id IS NOT NULL
    THEN
      -- Controle A1/2b medewerker heeft een koppeling
      IF kgc_kafd_00.eigen_afdeling( p_kafd_id => v_kafd_id )
      THEN
        v_koppeling_oke := TRUE;  -- controle A1/2b oke
      ELSE
        v_koppeling_oke := FALSE; -- controle A1/2b nok
      END IF;
    END IF;
  END IF;

  IF ( v_koppeling_oke IS NULL OR -- afdeling is niet herleid (kan eigenlijk niet voorkomen); toegang toegestaan
       v_koppeling_oke )          -- medewerker heeft een koppeling; toegang toegestaan
  THEN
    RETURN( NULL );
  END IF;
  -- v_koppeling_oke is FALSE; medewerker heeft in principe geen koppeling, tenzij:

  -- 1e uitzondering: ingeval van rapport KGCPAHI51 hebben stafleden alsnog "toegang":
  IF p_module = 'KGCPAHI51'
  THEN
    IF ( NVL( moto_rec.andere_kafd, 'N' ) = 'J' )
    THEN
      RETURN( NULL );
    ELSE
      RETURN( 'KGC-00009' ); -- controle A1/2b nok!
    END IF;
  END IF;

  -- 2e uitzondering: is de module-aanroep vanuit KGCPAHI05 gedaan, dan kunnen stafleden alsnog toegang krijgen.

  -----------------------------------------------------------------------------------------------
  -- Aanvullende controles A2                                                                  --
  -----------------------------------------------------------------------------------------------

  -- Controle A2/1: de module moet vanuit KGCPAHI05 zijn aangeroepen om alsnog toegang te kunnen krijgen:
  IF p_toegangscontrole <> 'P'
  THEN
    RETURN( 'KGC-00009' ); -- controle A1/2b nok!
  END IF;

  -- Controle: module komt niet voor in de serie waarvoor aanvullende controle A2 geldt:
  i_tmp := zoek_module( p_module_tab => v_module_tab_A2
                      , p_module     => p_module
                      );
  IF i_tmp IS NULL
  THEN
    RETURN( 'KGC-00009' ); -- controle A1/2b nok!
  END IF;
  i_mod := i_tmp; -- i_mod bevat index van A2

  -- Controle A2/2: indicatie 'andere_kafd' moet 'aan' staan om alsnog toegang te kunnen krijgen:
  IF ( NVL( moto_rec.andere_kafd, 'N' ) = 'N' )
  THEN
    RETURN( 'KGC-00009' ); -- controle A1/2b nok!
  END IF;

  -- Controle A2/2a: geen parameter(s) meegegeven waarmee het onderzoek herleid kan worden:
  i_par := zoek_eerstgevulde_parameter( p_module_tab => v_module_tab_A2
                                      , p_parameter  => 'P_ONDE_ID'
                                      );
  IF i_par IS NULL
  THEN
    RETURN( NULL ); -- controle A2/2a oke, controle A1/2b nok wordt overruled
  END IF;

  -- Controle A2/2b: onderzoek is afgerond
  v_afgerond := afgerond_onde( p_onde_id => p_onde_id );
  --
  --dbms_output.put_line( 'v_afgerond: '||v_afgerond );
  --
  IF NVL( v_afgerond, 'J' ) = 'J'
  THEN
    RETURN( NULL ); -- controle A2/2b oke, controle A1/2b nok wordt overruled
  ELSE
    RETURN( 'KGC-00040' ); -- melding van controle A2/2b nok, gaat voor de melding van controle A1/2b nok
  END IF;

END toegang;

FUNCTION toegang_jn
 (  p_module           IN VARCHAR2
  , p_meet_id          IN NUMBER   := NULL
  , p_meti_id          IN NUMBER   := NULL
  , p_frac_id          IN NUMBER   := NULL
  , p_onmo_id          IN NUMBER   := NULL
  , p_mons_id          IN NUMBER   := NULL
  , p_onde_id          IN NUMBER   := NULL
  , p_entiteit         IN VARCHAR2 := NULL
  , p_id               IN NUMBER   := NULL
  , p_kafd_id          IN NUMBER   := NULL
  , p_toegangscontrole IN VARCHAR2 := 'N' -- 'N': nee; 'J': ja; 'P': ja, bij aanroep vanuit patienthistorie KGCPAHI05
 )
RETURN VARCHAR2 -- bij toegang 'J', bij geen toegang 'N'.
IS
  v_melding VARCHAR2(9);
BEGIN
  v_melding := toegang
               ( p_module           => p_module
               , p_meet_id          => p_meet_id
               , p_meti_id          => p_meti_id
               , p_frac_id          => p_frac_id
               , p_onmo_id          => p_onmo_id
               , p_mons_id          => p_mons_id
               , p_onde_id          => p_onde_id
               , p_entiteit         => p_entiteit
               , p_id               => p_id
               , p_kafd_id          => p_kafd_id
               , p_toegangscontrole => p_toegangscontrole
               );
  IF ( v_melding IS NULL )
  THEN
    RETURN('J');
  ELSE
    RETURN('N');
  END IF;
END toegang_jn;
END KGC_MOTO_00;
/

/
QUIT
