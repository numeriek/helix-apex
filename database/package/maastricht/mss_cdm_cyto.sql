CREATE OR REPLACE PACKAGE "HELIX"."MSS_CDM_CYTO"
as

  function maak_regel
  (  p_onderzoeknr_va in kgc_onderzoeken.onderzoeknr%type
  ,  p_onderzoeknr_tm in kgc_onderzoeken.onderzoeknr%type
  )
  return t_mss_cdm_cyto_type PIPELINED;

end mss_cdm_cyto;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_CDM_CYTO"
as

  function maak_regel
  (  p_onderzoeknr_va in kgc_onderzoeken.onderzoeknr%type
  ,  p_onderzoeknr_tm in kgc_onderzoeken.onderzoeknr%type
  )
  return t_mss_cdm_cyto_type PIPELINED
  is
    cursor c_onde( b_onderzoeknr_va in kgc_onderzoeken.onderzoeknr%type
                 , b_onderzoeknr_tm in kgc_onderzoeken.onderzoeknr%type
                 )
    is
      select onde.onde_id
      from   kgc_onderzoeken  onde
      where  onde.onderzoeknr between b_onderzoeknr_va
                              and     b_onderzoeknr_tm
      and    onde.kafd_id = kgc_kafd_00.id('CYTO')
      and    exists
               ( select null
                 from   kgc_kweekfracties       kwfr
                 ,      bas_fracties            frac
                 ,      kgc_onderzoek_monsters  onmo
                 where  onmo.onde_id = onde.onde_id
                 and    frac.mons_id = onmo.mons_id
                 and    kwfr.frac_id = frac.frac_id
                 and    kwfr.code like '%gl%'
               )
      and    exists
               ( select null
                 from   kgc_stoftestgroepen     stgr
                 ,      bas_metingen            meti
                 ,      kgc_onderzoek_monsters  onmo
                 where  onmo.onde_id = onde.onde_id
                 and    meti.onmo_id = onmo.onmo_id
                 and    stgr.stgr_id = meti.stgr_id
                 and    stgr.code in ('KARYO-PO','KARYO-PR','KARYO-TU', 'FISH-KARYO')
               )
    ;
    --
    cursor c_cdm_onde( b_onde_id in kgc_onderzoeken.onde_id%type
                     )
    is
      select distinct
             lpad(' ',4)||XMLELEMENT("Name",onde.onderzoeknr).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("PatientID",mss_samenvoegen.onde_monsternummers_cyto(onde.onde_id)).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("PatientFirstName",pers.voorletters).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("PatientMiddleName",pers.voorvoegsel).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("PatientLastName",pers.achternaam).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("PatientDateOfBirth",pers.geboortedatum).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("PatientGender",
                                        case
                                          when pers.geslacht = 'M'
                                          then 2
                                          when pers.geslacht = 'V'
                                          then 1
                                          else 3
                                        end).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("Specimen",mss_samenvoegen.onde_mate_codes_cyto(onde.onde_id)).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("Physician",rela.aanspreken).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("ClinicalIndication",mss_samenvoegen.onde_indi_codes(onde.onde_id)).getstringval()||chr(10)||
             lpad(' ',4)||XMLELEMENT("Notes",stgr.code).getstringval()||chr(10) regel
      from   ( select onmo.onde_id  onde_id
               ,      stgr.code     code
               from   kgc_stoftestgroepen     stgr
               ,      bas_metingen            meti
               ,      kgc_onderzoek_monsters  onmo
               where  meti.onmo_id = onmo.onmo_id
               and    stgr.stgr_id = meti.stgr_id
               and    stgr.code in ('KARYO-PO','KARYO-PR','KARYO-TU', 'FISH-KARYO')
             )                stgr
      ,      kgc_relaties     rela
      ,      kgc_personen     pers
      ,      kgc_onderzoeken  onde
      where  onde.onde_id = b_onde_id
      and    stgr.onde_id = onde.onde_id
      and    pers.pers_id = onde.pers_id
      and    rela.rela_id = onde.rela_id
    ;
    --
    cursor c_cdm_kwfr( b_onde_id in kgc_onderzoeken.onde_id%type
                     )
    is
      select distinct
             lpad(' ',6)||'<Slide>'||chr(10)||
             lpad(' ',8)||XMLELEMENT("Name",rownum).getstringval()||chr(10)||
             lpad(' ',8)||XMLELEMENT("Label",kwfr.code).getstringval()||chr(10)||
             lpad(' ',8)||XMLELEMENT("Technology",'1').getstringval()||chr(10)||
             lpad(' ',8)||XMLELEMENT("AutomationCode",
                                        case
                                          when mons.monsternummer like ('%PRA%')      -- Amniocytic fluid
                                          then 'AM-PRA colonies'
                                          when mons.monsternummer like ('%PRC%')      -- Chorion villi
                                          then 'CV-PRC'
                                          when mons.monsternummer like ('%PMB%')      -- Peripheral blood
                                          then 'BL-PMB'
                                          when mons.monsternummer like ('%PMF%')      -- Tissue
                                          then 'TIS-PMF'
                                          when (   kwfr.code like ('%BL24%')          -- Blood tumor
                                                or kwfr.code like ('%Bl24R%')
                                                or kwfr.code like ('%BL48%')
                                                or kwfr.code like ('%BL48R%')
                                                or kwfr.code like ('%BL96%')
                                                or kwfr.code like ('%BLDIR%')
                                                )
                                          then 'BL-TMB'
                                          when (   kwfr.code like ('%BM24%')          -- Bonemarrow
                                                or kwfr.code like ('%BM24R%')
                                                or kwfr.code like ('%BM48%')
                                                or kwfr.code like ('%BM48R%')
                                                or kwfr.code like ('%BM96%')
                                                or kwfr.code like ('%BMDIR%')
                                               )
                                          then 'BM-TMM'
                                        end).getstringval()||chr(10)||
             lpad(' ',8)||XMLELEMENT("CultureName",kwfr.code).getstringval()||chr(10)||
             lpad(' ',8)||XMLELEMENT("Barcode",replace(onde.onderzoeknr,'/','-')||kwfr.code).getstringval()||chr(10)||
             lpad(' ',6)||'</Slide>'  regel
      from   kgc_kweektypes          kwty
      ,      kgc_kweekfracties       kwfr
      ,      bas_fracties            frac
      ,      kgc_onderzoeksgroepen   ongr
      ,      kgc_monsters            mons
      ,      kgc_onderzoek_monsters  onmo
      ,      kgc_onderzoeken         onde
      where  onde.onde_id = b_onde_id
      and    onmo.onde_id = onde.onde_id
      and    mons.mons_id = onmo.mons_id
      and    ongr.ongr_id = mons.ongr_id
      and    ongr.code in ('PM','TM','PR')
      and    frac.mons_id = mons.mons_id
      and    kwfr.frac_id = frac.frac_id
      and    kwfr.code like '%gl%'
      and    kwty.kwty_id = kwfr.kwty_id
    ;
    --
  begin
    --
    pipe row(mss_cdm_cyto_type('<Cases>'));
    pipe row(mss_cdm_cyto_type(chr(10)));
    --
    for r_onde in c_onde( p_onderzoeknr_va
                        , p_onderzoeknr_tm
                        )
    loop
      for r_cdm_onde in c_cdm_onde(r_onde.onde_id)
      loop
        pipe row(mss_cdm_cyto_type(lpad(' ',2)||'<Case>'));
        pipe row(mss_cdm_cyto_type(r_cdm_onde.regel));
      end loop;
      --
      pipe row(mss_cdm_cyto_type(lpad(' ',4)||'<Slides>'));
      --
      for r_cdm_kwfr in c_cdm_kwfr(r_onde.onde_id)
      loop
        pipe row(mss_cdm_cyto_type(r_cdm_kwfr.regel));
      end loop;
      --
      pipe row(mss_cdm_cyto_type(lpad(' ',4)||'</Slides>'));
      pipe row(mss_cdm_cyto_type(lpad(' ',2)||'</Case>'));
      pipe row(mss_cdm_cyto_type(chr(10)));
    end loop;
    --
    pipe row(mss_cdm_cyto_type('</Cases>'));
    --
    return;
  end maak_regel;

END mss_cdm_cyto;
/

/
QUIT
