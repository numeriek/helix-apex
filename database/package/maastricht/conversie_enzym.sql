CREATE OR REPLACE PACKAGE "HELIX"."CONVERSIE_ENZYM" AS
/*
  function leef_oms
  ( p_onder in number
  , p_boven in number
  )
  return varchar2;
  pragma restrict_references( leef_oms, WNDS, WNPS );
  PROCEDURE opschonen( p_jaar IN  VARCHAR2 );
  PROCEDURE ref_data;
  PROCEDURE onderzoeken( p_jaar IN VARCHAR2 );
*/
END CONVERSIE_ENZYM;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."CONVERSIE_ENZYM" AS
/*
  -- Init conversie_metab: 23/02/2004
  -- Last update : 23/02/2004
  --  ==========================================================================================
  --   S E S S I E   V A R I A B E L E N
  --
  c_kafd_id      kgc_kgc_afdelingen.kafd_id%TYPE;
--  c_ongr_id_b    kgc_onderzoeksgroepen.ongr_id%TYPE;
--  c_ongr_id_h    kgc_onderzoeksgroepen.ongr_id%TYPE;
  c_ongr_id_l    kgc_onderzoeksgroepen.ongr_id%TYPE;
  c_ongr_id_m    kgc_onderzoeksgroepen.ongr_id%TYPE;
  c_rela_id_onb  kgc_relaties.rela_id%TYPE;

  --  ==========================================================================================
  --   L O K A L E     P R O C E D U R E S
  --

  procedure debug
  ( p_regel in varchar2
  )
  is
  begin
    null;
--    p.l( p_regel );
  end debug;

  FUNCTION conv_datum
  ( p_datum_char  IN  VARCHAR2
  )
  RETURN DATE
  IS
    v_date  DATE := NULL;
  BEGIN
    IF  p_datum_char IS NOT NULL
    THEN
      IF p_datum_char <> '01-01-1111'
      THEN
        v_date := TO_DATE( p_datum_char, 'DD-MM-YYYY' );
      END IF;
    END IF;
    RETURN v_date;
  EXCEPTION
    WHEN OTHERS THEN RETURN TO_DATE( NULL );
  END conv_datum;

  FUNCTION persoon_ok( p_pers_id IN VARCHAR2 ) RETURN BOOLEAN IS
    CURSOR pers_cur IS
	  SELECT '1'  dummy
	  FROM   kgc_personen  pers
	  WHERE  pers.pers_id  = p_pers_id
	  ;
	pers_rec  pers_cur%ROWTYPE;
	v_ret  BOOLEAN := TRUE;
  BEGIN
    OPEN  pers_cur;
	FETCH pers_cur
	INTO  pers_rec;
	IF pers_cur%NOTFOUND
	THEN
	  v_ret := FALSE;
	END IF;
	IF pers_cur%ISOPEN
	THEN
	  CLOSE pers_cur;
	END IF;
	RETURN v_ret;
  EXCEPTION
    WHEN OTHERS
	THEN
	  RETURN FALSE;
  END persoon_ok;

  PROCEDURE check_pers_foet(
      x_pers_id  IN OUT  NUMBER
    , x_foet_id  IN OUT  NUMBER
                           ) IS
    CURSOR pers_foet_cur IS
      SELECT pers.pers_id       pers_id
      ,      TO_NUMBER( NULL )  foet_id
      ,      1                  volgorde
      FROM   kgc_personen  pers
      WHERE  pers.pers_id  = x_pers_id
      UNION
      SELECT foet.pers_id_moeder
      ,      foet.foet_id
      ,      2
      FROM   kgc_foetussen  foet
      ,      koppel_foetus_moeder  kopp
      WHERE  foet.foet_id  = kopp.foet_id
      AND    kopp.persnr   = x_pers_id
      ORDER BY volgorde
      ;
    pers_foet_rec  pers_foet_cur%ROWTYPE;
  BEGIN
    OPEN  pers_foet_cur;
    FETCH pers_foet_cur
    INTO  pers_foet_rec;
    CLOSE pers_foet_cur;
    x_pers_id := pers_foet_rec.pers_id;
    x_foet_id := pers_foet_rec.foet_id;
  EXCEPTION
    WHEN OTHERS
    THEN
      x_pers_id := NULL;
      x_foet_id := NULL;
  END check_pers_foet;

  PROCEDURE vergelijk_verwijzer_voor_na
  ( p_onderzoeknr    IN  VARCHAR2
  , p_verwijzer_voor IN  VARCHAR2
  , p_verwijzer_na   IN  VARCHAR2
  )
  IS
    v_verwijzer_voor VARCHAR2(2000);
    v_verwijzer_na   VARCHAR2(2000);
    FUNCTION repl
	( p_naam  IN  VARCHAR2
	)
    RETURN VARCHAR2
    IS
      v_naam  VARCHAR2(2000);
    BEGIN
      v_naam := LTRIM(RTRIM(UPPER(p_naam)));
      v_naam := REPLACE( v_naam, 'DHR.' , '');
      v_naam := REPLACE( v_naam, 'DRS.' , '');
      v_naam := REPLACE( v_naam, 'DRA.' , '');
      v_naam := REPLACE( v_naam, 'DR.'  , '');
      v_naam := REPLACE( v_naam, 'MR.'  , '');
      v_naam := REPLACE( v_naam, 'MW.'  , '');
      v_naam := REPLACE( v_naam, 'MEVR.', '');
      v_naam := REPLACE( v_naam, 'DHR ' , '');
      v_naam := REPLACE( v_naam, 'DRS ' , '');
      v_naam := REPLACE( v_naam, 'DRA ' , '');
      v_naam := REPLACE( v_naam, 'DR '  , '');
      v_naam := REPLACE( v_naam, 'MR '  , '');
      v_naam := REPLACE( v_naam, 'MW '  , '');
      v_naam := REPLACE( v_naam, 'MEVR ', '');
      v_naam := REPLACE( v_naam, ' - '  ,'-');
      v_naam := REPLACE( v_naam, '- '   ,'-');
      v_naam := REPLACE( v_naam, ' -'   ,'-');
      v_naam := LTRIM(RTRIM(UPPER(v_naam)));
      RETURN v_naam;
    END;
  BEGIN
    v_verwijzer_voor := repl( p_verwijzer_voor );
    v_verwijzer_na   := repl( p_verwijzer_na );
    IF   v_verwijzer_voor <> v_verwijzer_na
    THEN
      p.l( 'Onderzoek : '||p_onderzoeknr||', '
        || 'verwijzer voor : '||p_verwijzer_voor||' '
        || 'verwijzer na   : '||p_verwijzer_na
         );
    END IF;
  END vergelijk_verwijzer_voor_na;

  function leef_oms
  ( p_onder in number
  , p_boven in number
  )
  return varchar2
  is
    v_oms varchar2(100);
  begin
    if ( p_onder is null
     and p_boven is null
       )
    then
      v_oms := null;
    else
      v_oms := 'Van';
      if ( p_onder is null )
      then
        v_oms := v_oms||' nul';
      else
        if ( p_onder < 31 )
        then
          v_oms := v_oms||' '||to_char(p_onder)||' dagen';
        elsif ( p_onder < 2*366 )
        then
          v_oms := v_oms||' '||to_char(round(p_onder/31))||' maand';
        else
          v_oms := v_oms||' '||to_char(round(p_onder/365))||' jaar';
        end if;
      end if;
      v_oms := v_oms||' tot';
      if ( p_boven is null )
      then
        v_oms := v_oms||' ...';
      else
        if ( p_boven < 31 )
        then
          v_oms := v_oms||' '||to_char(p_boven)||' dagen';
        elsif ( p_boven < 2*366 )
        then
          v_oms := v_oms||' '||to_char(round(p_boven/31))||' maand';
        else
          v_oms := v_oms||' '||to_char(round(p_boven/365))||' jaar';
        end if;
      end if;
    end if;
    return( v_oms );
  end leef_oms;

  function leeftijd_id
  ( p_onder in number
  , p_boven in number
  )
  return number
  is
    v_return number;
    v_oms varchar2(100);
    cursor c
    is
      select leef_id
      from   kgc_leeftijden
      where  kafd_id = c_kafd_id
      and    omschrijving = v_oms
      ;
    cursor c2
    is
      select leef_id
      from   kgc_leeftijden
      where  kafd_id = c_kafd_id
      and    ondergrens = p_onder
      and    bovengrens >= p_boven
      order by (bovengrens - p_boven) asc -- dichtsbijzijnde eerst
      ;
  begin
    v_oms := leef_oms( p_onder, p_boven );
    if ( v_oms is not null )
    then
      open  c;
      fetch c into v_return;
      close c;
    end if;
    if ( v_return is null )
    then
      open  c2;
      fetch c2 into v_return;
      close c2;
    end if;
    return( v_return );
  end leeftijd_id;

  function stoftest_id
  ( p_stofnaam in varchar2
  )
  return number
  is
    v_return number;
    cursor c
    is
      select stof.stof_id
      from   kgc_stoftesten stof
      ,      metab_stofnaam mets
      where  stof.kafd_id = c_kafd_id
      and    mets.stof_id = stof.stof_id
      and    mets.stofnaam = p_stofnaam
      ;
    cursor c2
    is
      select stof.stof_id
      from   kgc_stoftesten stof
      ,      metab_stofnaam mets
      where  stof.kafd_id = c_kafd_id
      and    mets.stof_id = stof.stof_id
      and    upper(replace(mets.stofnaam,' ','')) = upper(replace(p_stofnaam,' ',''))
      ;
  begin
    open  c;
    fetch c into v_return;
    close c;
    if ( v_return is null )
    then
      open  c2;
      fetch c2 into v_return;
      close c2;
    end if;
    return( v_return );
  end stoftest_id;

  function stgr_code
  ( p_testcd in varchar2
  , p_matcd in varchar2
  )
  return varchar2
  is
    v_code varchar2(10);
  begin
    if ( length( p_testcd||';'||p_matcd ) <= 10 )
    then
      v_code := upper(p_testcd||';'||p_matcd);
    else
      v_code := upper(substr(p_testcd,1,6)||';'||p_matcd);
    end if;
    return( v_code );
  end stgr_code;

  FUNCTION specialisme_id
  ( p_omschrijving  IN  VARCHAR2
  )
  RETURN NUMBER
  IS
    CURSOR c
    IS
      SELECT spec.spec_id
      FROM   kgc_specialismen  spec
      WHERE  TRUNC(UPPER( spec.omschrijving )) = TRUNC(UPPER( p_omschrijving ))
      ;
    r c%ROWTYPE;
  BEGIN
    IF  p_omschrijving IS NOT NULL
    THEN
      OPEN  c;
      FETCH c
      INTO  r;
      CLOSE c;
    END IF;
    RETURN r.spec_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END specialisme_id;

  FUNCTION instelling_id
  ( p_code  IN  VARCHAR2
  )
  RETURN NUMBER
  IS
    CURSOR c
    IS
      SELECT inst_id
      FROM   kgc_instellingen
      WHERE  code = TRUNC(UPPER( p_code ))
      ;
    r c%ROWTYPE;
  BEGIN
    IF  p_code IS NOT NULL
    THEN
      OPEN  c;
      FETCH c
      INTO  r;
      CLOSE c;
    END IF;
    RETURN r.inst_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END instelling_id;

  FUNCTION familierelatie_id
  ( p_omschrijving  IN  VARCHAR2
  )
  RETURN NUMBER
  IS
    CURSOR c
    IS
      SELECT fare.fare_id
      FROM   kgc_familie_relaties  fare
      WHERE  TRUNC(UPPER( fare.omschrijving )) = TRUNC(UPPER( p_omschrijving ))
      ;
    r c%ROWTYPE;
  BEGIN
    IF  p_omschrijving IS NOT NULL
    THEN
      OPEN  c;
      FETCH c
      INTO  r;
      CLOSE c;
    END IF;
    RETURN r.fare_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END familierelatie_id;

  FUNCTION conclusietekst
  ( p_code  IN  VARCHAR2
  )
  RETURN NUMBER
  IS
    CURSOR c
    IS
      SELECT conc.omschrijving
      FROM   kgc_conclusie_teksten conc
      WHERE  kafd_id = c_kafd_id
      and    code = upper( p_code )
      ;
    r c%ROWTYPE;
  BEGIN
    IF  p_code IS NOT NULL
    THEN
      OPEN  c;
      FETCH c
      INTO  r;
      CLOSE c;
    END IF;
    RETURN r.omschrijving;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END conclusietekst;

  -- bepaal onderzoeksgroep adhv. testcd/protocol
  -- een HOM-test, dan homocystiene
  function check_testcd
  ( p_odbnr in number )
  return varchar2
  is
    v_return varchar2(1) := 'B';
    cursor c
    is
      select null
      from   tmp_met_test
      where  odbnr = p_odbnr
      and    testcd like '%HOM%'
      ;
    v_dummy varchar2(1);
  begin
    open  c;
    fetch c
    into  v_dummy;
    if ( c%found )
    then
      v_return := 'H';
    end if;
    close c;
    return (v_return);
  end check_testcd;

  PROCEDURE bepaal_afdeling IS
    FUNCTION voeg_kafd_toe RETURN NUMBER IS
      v_kafd_id  NUMBER;
    BEGIN
      SELECT kgc_kafd_seq.nextval
      INTO   v_kafd_id
      FROM   DUAL
      ;
      INSERT INTO kgc_kgc_afdelingen (
               kafd_id
      ,        code
      ,        naam
      )
      VALUES ( v_kafd_id
      ,        'METAB'
      ,        'Metabole diagostiek'
      );
      RETURN v_kafd_id;
    END voeg_kafd_toe;
  BEGIN
    c_kafd_id := kgc_uk2pk.kafd_id( p_code => 'METAB' );
    IF c_kafd_id IS NULL
    THEN
      c_kafd_id := voeg_kafd_toe;
    END IF;
  END;

  PROCEDURE bepaal_onderzoeksgroepen IS
    FUNCTION voeg_ongr_toe( p_code  IN  VARCHAR2 ) RETURN NUMBER IS
	  v_ongr_id  NUMBER;
    BEGIN
      SELECT kgc_ongr_seq.nextval
	  INTO   v_ongr_id
	  FROM   DUAL
	  ;
      INSERT INTO kgc_onderzoeksgroepen (
               ongr_id
      ,        kafd_id
      ,        code
      ,        omschrijving
      ,        vervallen
      )
      VALUES ( v_ongr_id
	  ,        c_kafd_id
      ,        p_code
      ,        INITCAP(p_code)||' (CONVERSIE)'
      ,        'N'
      );
	  RETURN v_ongr_id;
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN NULL;
      WHEN OTHERS           THEN RAISE;
    END;
  BEGIN
    c_ongr_id_l  := kgc_uk2pk.ongr_id( p_kafd_id => c_kafd_id
                                     , p_code    => 'LYSO'
                                     );
    IF c_ongr_id_l IS NULL
	THEN
      c_ongr_id_l := voeg_ongr_toe( p_code => 'LYSO' );
	END IF;
    c_ongr_id_m  := kgc_uk2pk.ongr_id( p_kafd_id => c_kafd_id
                                     , p_code    => 'MITO'
                                     );
    IF c_ongr_id_m IS NULL
	THEN
      c_ongr_id_m := voeg_ongr_toe( p_code => 'MITO' );
	END IF;
  END bepaal_onderzoeksgroepen;

  PROCEDURE bepaal_rela_onbekend IS
    CURSOR rela_cur IS
      SELECT rela.rela_id
      FROM   kgc_relaties  rela
      WHERE  rela.code = 'ONBEKEND'
      ;
    v_rela_id  NUMBER;
  BEGIN
    OPEN   rela_cur;
    FETCH  rela_cur
    INTO   c_rela_id_onb;
    CLOSE  rela_cur;
  EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
      SELECT kgc_rela_seq.nextval
      INTO  c_rela_id_onb
      FROM  DUAL
      ;
      INSERT INTO kgc_relaties (
               rela_id
      ,        code
      ,        achternaam
      ,        vervallen
      )
      VALUES ( c_rela_id_onb
      ,        'ONBEKEND'
      ,        'Onbekend (conversie metab)'
      ,        'N'
      );
    WHEN OTHERS
    THEN
      RAISE;
  END bepaal_rela_onbekend;

  PROCEDURE toevoegen_mede IS
    -- Medewerkers niet in HELIX nieuw
    --
    CURSOR mede_cur IS
      SELECT init
      ,      naam
      ,      username
      ,      afdeling
      ,      functie
      ,      functie_code
      FROM   tmp_stc329
      WHERE  init  NOT IN
           ( SELECT mede.code
             FROM   kgc_medewerkers  mede
           )
      ;
    v_mede_id  NUMBER;
    v_func_id  NUMBER;
  BEGIN
    FOR  mede_rec IN mede_cur
    LOOP
      SELECT kgc_mede_seq.nextval
      INTO   v_mede_id
      FROM   DUAL
      ;
      BEGIN

        INSERT INTO kgc_medewerkers(
                 mede_id
        ,        code
        ,        naam
        ,        vervallen
        )
        VALUES ( v_mede_id
        ,        mede_rec.init
        ,        mede_rec.naam
        ,        'N'
        );
        BEGIN
          v_func_id := kgc_uk2pk.func_id( p_code => upper(mede_rec.functie) );
          INSERT INTO kgc_mede_kafd(
                   mede_id
          ,        kafd_id
          ,        func_id
          ,        autorisator
          )
          VALUES ( v_mede_id
          ,        c_kafd_id
          ,        v_func_id
          ,        decode( mede_rec.functie_code
                         , 'AUTOR', 'J'
                         , 'N'
                         )
          );
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN NULL;
          WHEN OTHERS           THEN RAISE;
        END;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_mede;

  PROCEDURE toevoegen_dewy IS
    CURSOR oud_cur IS
      SELECT declaratiecode
      ,      declaratie_oms
      ,      vervallen
      FROM   tmp_stc909
      WHERE  declaratiecode NOT IN
           ( SELECT dewy.code
             FROM   kgc_declaratiewijzen dewy
           )
      ;
  BEGIN
    for r in oud_cur
    loop
      BEGIN
        INSERT INTO kgc_declaratiewijzen (
                 kafd_id
        ,        code
        ,        omschrijving
        ,        vervallen
        )
        VALUES ( c_kafd_id
        ,        r.declaratiecode
        ,        r.declaratie_oms
        ,        r.vervallen
        );
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    end loop;
  END toevoegen_dewy;

  PROCEDURE toevoegen_mwst IS
  BEGIN
    BEGIN
      INSERT INTO kgc_meetwaardestructuur
      ( code
      , omschrijving
      )
      VALUES
      ( 'METAB_OUD'
      , 'Tbv. conversie metab'
      );
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN NULL;
      WHEN OTHERS           THEN RAISE;
    END;

    BEGIN
      INSERT INTO kgc_mwst_samenstelling
      ( mwst_id
      , code
      , volgorde
      , lengte
      , soort
      , prompt
      , presentatie
      , gebruik
      , in_uitslag
      )
      select mwst_id
      ,      'UITSLAG1'
      ,      1
      ,      15
      ,      'C'
      ,      'Uitslag'
      ,      'TEXT'
      ,      1
      ,      'J'
      from   kgc_meetwaardestructuur
      where  code = 'METAB_OUD';
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN NULL;
      WHEN OTHERS           THEN RAISE;
    END;
  END toevoegen_mwst;

  PROCEDURE toevoegen_onty IS
  BEGIN
    BEGIN
      INSERT INTO kgc_onderzoekrelatie_types (
               code
      ,        omschrijving
      ,        vervallen
      )
      VALUES ( 'VRG'
      ,        'Voorgaand onderzoek'
      ,        'N'
      );
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN NULL;
      WHEN OTHERS           THEN RAISE;
    END;
  END toevoegen_onty;

  PROCEDURE toevoegen_proj IS
    CURSOR oud_cur IS
      SELECT projcd
      ,      projtext
      FROM   tmp_stc305
      ;
  BEGIN
    FOR r IN oud_cur
    LOOP
      BEGIN
        insert into kgc_projecten
        ( kafd_id
        , code
        , omschrijving
        , vervallen
        )
        select c_kafd_id
        ,      upper(r.projcd)
        ,      r.projtext
        ,      'J'
        from   dual
        where  not exists
               ( select null
                 from   kgc_projecten
                 where  kafd_id = c_kafd_id
                 and    code = upper(r.projcd)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_proj;

  PROCEDURE toevoegen_stof IS
    CURSOR oud_cur IS
      SELECT stofnaam stof_naam
      ,      code
      ,      omschrijving
      ,      stof_id
      FROM   metab_stofnaam
      where  stof_id is null
      and    code is not null
      for update of stof_id
      ;
    -- eenheid, aantal decimalen
    cursor c2
    ( b_stof in varchar2 )
    is
      select max(decode( r.uitsl1_ogrens
                   , null, nvl(p.eenheid2, p.eenheid1)
                   , p.eenheid1
                   )) eenheid
      ,      max(decode( p.decimalen
                   , 'A', null
                   , p.decimalen
                   )) decimalen
      ,      count(*) aantal
      from   tmp_stc311 r
      ,      tmp_stc306 p
      ,      tmp_stc309 s
      where  s.testcd = r.testcd
      and    s.stof_naam = r.stof_naam
      and    s.testcd = p.testcd
      and    s.stof_naam = b_stof
      and    r.stof_naam = b_stof
      order by 3 desc
      ;
    r2 c2%rowtype;
    r2_null c2%rowtype;

    v_stof_id number;
    v_dummy number;
    v_mwst_id number := kgc_uk2pk.mwst_id( 'METAB_OUD' );
  BEGIN
    FOR r IN oud_cur
    LOOP
      r2 := r2_null;
      open  c2( r.stof_naam);
      fetch c2 into r2;
      close c2;
      BEGIN
        insert into kgc_stoftesten
        ( stof_id
        , kafd_id
        , code
        , omschrijving
        , omschrijving_lang
        , aantal_decimalen
        , eenheid
        , vervallen
        , mwst_id
        )
        select kgc_stof_seq.nextval
        ,      c_kafd_id
        ,      nvl( r.code, to_char(kgc_stof_seq.currval) )
        ,      nvl( r.omschrijving, initcap(r.stof_naam) )
        ,      r.stof_naam
        ,      r2.decimalen
        ,      r2.eenheid
        ,      'N'
        ,      v_mwst_id
        from   dual
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
    begin
      update metab_stofnaam ms
      set    ms.stof_id = ( select stof.stof_id
                            from   kgc_stoftesten stof
                            where  stof.kafd_id = c_kafd_id
                            and    stof.code = ms.code
                          )
      where ms.stof_id is null;
    exception
      when others
      then
        p.l( 'Metab_stofnaam is niet gewijzigd!'||chr(10)||sqlerrm );
    end;
  END toevoegen_stof;

  PROCEDURE toevoegen_mate IS
    CURSOR oud_cur IS
      SELECT materiaalcode
      ,      materiaal_oms
      ,      vervallen
      FROM   tmp_stc902
      ;
  BEGIN
    FOR r IN oud_cur
    LOOP
      BEGIN
        insert into kgc_materialen
        ( kafd_id
        , code
        , omschrijving
        , vervallen
        )
        select c_kafd_id
        ,      upper(r.materiaalcode)
        ,      r.materiaal_oms
        ,      r.vervallen
        from   dual
        where  not exists
               ( select null
                 from   kgc_materialen
                 where  kafd_id = c_kafd_id
                 and    code = upper(r.materiaalcode)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_mate;

  PROCEDURE toevoegen_onui IS
    CURSOR oud_cur IS
      SELECT afwcatcd
      ,      afwcattext
      FROM   tmp_stc303
      ;
  BEGIN
    FOR r IN oud_cur
    LOOP
      BEGIN
        insert into kgc_onderzoek_uitslagcodes
        ( kafd_id
        , code
        , omschrijving
        , vervallen
        )
        select c_kafd_id
        ,      upper(r.afwcatcd)
        ,      r.afwcattext
        ,      'J'
        from   dual
        where  not exists
               ( select null
                 from   kgc_onderzoek_uitslagcodes
                 where  kafd_id = c_kafd_id
                 and    code = upper(r.afwcatcd)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_onui;

  PROCEDURE toevoegen_stgr IS
    CURSOR oud_cur IS
      SELECT t306.testcd
      ,      t306.test_naam
      ,      t306.matcd
      ,      t306.vervallen
      ,      t306.standaard
      ,      t306.eenheid1
      ,      t306.eenheid2
      ,      t306.sorteer t306_sorteer
      ,      t306.screeningtest
      ,      t306.decimalen
      ,      t306.getal_opn
      ,      t309.stof_naam
      ,      t309.sorteer t309_sorteer
      FROM   tmp_stc306  t306
      ,      tmp_stc309  t309
      where  t306.testcd = t309.testcd
      and    t306.matcd = t309.matcd
      ;

    v_mate_id number;
    v_stof_id number;
    stgr_init_rec kgc_stoftestgroepen%rowtype;
    stgr_rec kgc_stoftestgroepen%rowtype;
  BEGIN
    FOR r IN oud_cur
    LOOP
      stgr_rec := stgr_init_rec;
      stgr_rec.code := stgr_code ( r.testcd, r.matcd );
      BEGIN
        insert into kgc_stoftestgroepen
        ( kafd_id
        , code
        , omschrijving
        , protocol
        , vervallen
        )
        select c_kafd_id
        ,      stgr_rec.code
        ,      r.test_naam
        ,      'J'
        ,      nvl( r.vervallen, 'N' )
        from   dual
        where  not exists
               ( select null
                 from kgc_stoftestgroepen
                 where  kafd_id = c_kafd_id
                 and    code = stgr_rec.code
               );
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
      v_mate_id := kgc_uk2pk.mate_id( p_kafd_id => c_kafd_id
                                    , p_code    => r.matcd
                                    );
      BEGIN
        insert into kgc_stoftestgroep_gebruik
        ( kafd_id
        , stgr_id
        , standaard
        , screeningtest
        , getal_opnemen
        , vervallen
        , mate_id
        )
        select c_kafd_id
        ,      stgr_id
        ,      nvl( r.standaard, 'N' )
        ,      nvl( r.screeningtest, 'N' )
        ,      nvl( r.getal_opn, 'N' )
        ,      nvl( r.vervallen, 'N' )
        ,      v_mate_id
        from   kgc_stoftestgroepen
        where  kafd_id = c_kafd_id
        and    code = stgr_rec.code
        and    stgr_id not in
               ( select stgr_id
                 from   kgc_stoftestgroep_gebruik
                 where  kafd_id = c_kafd_id
                 and    mate_id = v_mate_id
               );
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;

      v_stof_id := null;
      v_stof_id := stoftest_id( r.stof_naam );
      if ( v_stof_id is not null )
      then
        BEGIN
          insert into kgc_protocol_stoftesten
          ( stgr_id
          , stof_id
          , standaard
          , volgorde
          , aantal_decimalen
          , eenheid
          )
          select stgr_id
          ,      v_stof_id
          ,      nvl( r.standaard, 'N' )
          ,      r.t309_sorteer
          ,      decode( r.decimalen
                       , 'A', null
                       , r.decimalen
                       )
          ,      r.eenheid1
          from   kgc_stoftestgroepen
          where  kafd_id = c_kafd_id
          and    code = stgr_rec.code
          and    stgr_id not in
                 ( select stgr_id
                   from   kgc_protocol_stoftesten
                   where  stof_id = v_stof_id
                 );
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN NULL;
          WHEN OTHERS           THEN RAISE;
        END;
      end if;
    END LOOP;
  END toevoegen_stgr;

  procedure toevoegen_leef
  is
    cursor oud_cur
    is
      select leef_oms( lft_ogrens, lft_bgrens ) omschr
      ,      min(lft_ogrens) onder
      ,      max(lft_bgrens ) boven
      from   tmp_stc311
      group by leef_oms( lft_ogrens, lft_bgrens )
      ;
    v_code varchar2(100);
  begin
    for r in oud_cur
    loop
      v_code := upper(replace(r.omschr, ' ', ''));
      v_code := ltrim( v_code, 'VAN');
      v_code := replace( v_code, 'TOT','-');
      v_code := replace( v_code, 'DAGEN','D');
      v_code := replace( v_code, 'DAG','D');
      v_code := replace( v_code, 'MAAND','M');
      v_code := replace( v_code, 'JAAR','J');
      begin
        insert into kgc_leeftijden
        ( kafd_id
        , code
        , omschrijving
        , vervallen
        , ondergrens
        , bovengrens
        )
        select c_kafd_id
        ,      v_code
        ,      r.omschr
        ,      'N'
        ,      r.onder
        ,      r.boven
        from   dual
        where  not exists
               ( select null from kgc_leeftijden
                 where  kafd_id = c_kafd_id
                 and    code = v_code
               );
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      end;
    end loop;
  end toevoegen_leef;

  procedure toevoegen_rang
  is
    cursor oud_cur
    is
      select testcd
      ,      matcd
      ,      stof_naam
      ,      lft_ogrens
      ,      lft_bgrens
      ,      nvl( uitsl1_ogrens, uitsl2_ogrens ) ogrens
      ,      nvl( uitsl1_bgrens, uitsl2_bgrens ) bgrens
      from   tmp_stc311
      ;

    cursor mate_cur
    ( p_code in varchar2 )
    is
      select mate_id
      from   kgc_materialen
      where  code = p_code
      and    kafd_id = c_kafd_id
      ;
    cursor stgr_cur
    ( p_code in varchar2 )
    is
      select stgr_id
      from   kgc_stoftestgroepen
      where  code = p_code
      and    kafd_id = c_kafd_id
      ;
    v_mate_id number;
    v_stof_id number;
    v_leef_id number;
    v_stgr_id number;
    v_bety_id number := null;
    v_geslacht varchar2(1) := null;
  begin
    for r in oud_cur
    loop
      v_mate_id := null;
      v_stof_id := null;
      v_leef_id := null;
      v_stgr_id := null;
      if ( r.matcd is not null )
      then
        open  mate_cur( r.matcd );
        fetch mate_cur into v_mate_id;
        close mate_cur;
      end if;
      if ( r.testcd is not null )
      then
        open  stgr_cur( stgr_code ( r.testcd, r.matcd ) );
        fetch stgr_cur into v_stgr_id;
        close stgr_cur;
      end if;
      if ( r.stof_naam is not null )
      then
        v_stof_id := stoftest_id( r.stof_naam );
      end if;
      if ( r.lft_ogrens is not null
        or r.lft_bgrens is not null
         )
      then
        v_leef_id := leeftijd_id( nvl( r.lft_ogrens, 0 ), nvl( r.lft_bgrens, 9999999999 ) );
      end if;
      if ( v_stof_id is not null
       and v_leef_id is not null
       and v_mate_id is not null
         )
      then
        begin
          insert into bas_ranges
          ( kafd_id
          , mate_id
          , stof_id
          , leef_id
          , stgr_id
          , bety_id
          , geslacht
          , vervallen
          , laag
          , hoog
          )
          select c_kafd_id
          ,      v_mate_id
          ,      v_stof_id
          ,      v_leef_id
          ,      v_stgr_id
          ,      v_bety_id
          ,      v_geslacht
          ,      'N'
          ,      r.ogrens
          ,      r.bgrens
          from   dual
          where  not exists
                 ( select null from bas_ranges
                   where  kafd_id = c_kafd_id
                   and    stof_id = v_stof_id
                   and    mate_id = v_mate_id
                   and    leef_id = v_leef_id
                   and    nvl( stgr_id, -9 ) = nvl( v_stgr_id, -9 )
                   and    nvl( bety_id, -9 ) = nvl( v_bety_id, -9 )
                   and    nvl( geslacht, 'x' ) = nvl( v_geslacht, 'x' )
                 );
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN p.l( 'dubbel (mat/test/stof/leef): ('||r.matcd||'/'||r.testcd||'/'||r.stof_naam||'/'||leef_oms( r.lft_ogrens, r.lft_bgrens)||')' );
          WHEN OTHERS           THEN RAISE;
        end;
      end if;
    end loop;
  end toevoegen_rang;

  PROCEDURE toevoegen_ingr IS
    CURSOR oud_cur IS
      SELECT indgrp
      ,      indgrptext
      FROM   tmp_stc905
      ;
  BEGIN
    FOR r IN oud_cur
    LOOP
      -- lyso
      BEGIN
        insert into kgc_indicatie_groepen
        ( ongr_id
        , code
        , omschrijving
        , machtigingsindicatie
        , vervallen
        )
        select c_ongr_id_l
        ,      upper(r.indgrp)
        ,      r.indgrptext
        ,      r.indgrptext
        ,      'N'
        from   dual
        where  not exists
               ( select null
                 from   kgc_indicatie_groepen
                 where  ongr_id = c_ongr_id_l
                 and    code = upper(r.indgrp)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
      -- mito
      BEGIN
        insert into kgc_indicatie_groepen
        ( ongr_id
        , code
        , omschrijving
        , machtigingsindicatie
        , vervallen
        )
        select c_ongr_id_m
        ,      upper(r.indgrp)
        ,      r.indgrptext
        ,      r.indgrptext
        ,      'N'
        from   dual
        where  not exists
               ( select null
                 from   kgc_indicatie_groepen
                 where  ongr_id = c_ongr_id_m
                 and    code = upper(r.indgrp)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_ingr;

  PROCEDURE toevoegen_indi IS
    CURSOR oud_cur IS
      SELECT indgrp
      ,      indcd
      ,      indtext
      ,      vervallen
      FROM   tmp_stc904
      ;
  BEGIN
    FOR r IN oud_cur
    LOOP
      -- lyso
      BEGIN
        insert into kgc_indicatie_teksten
        ( ongr_id
        , ingr_id
        , code
        , omschrijving
        , omschrijving_reden
        , vervallen
        )
        select c_ongr_id_l
        ,      ingr.ingr_id
        ,      r.indcd
        ,      r.indtext
        ,      r.indtext
        ,      r.vervallen
        from   kgc_indicatie_groepen ingr
        where  ingr.ongr_id = c_ongr_id_l
        and    ingr.code = r.indgrp
        and    not exists
               ( select null
                 from   kgc_indicatie_teksten indi
                 where  indi.ongr_id = c_ongr_id_l
                 and    indi.ingr_id = ingr.ingr_id
                 and    indi.code = upper(r.indcd)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
      -- mito
      BEGIN
        insert into kgc_indicatie_teksten
        ( ongr_id
        , ingr_id
        , code
        , omschrijving
        , omschrijving_reden
        , vervallen
        )
        select c_ongr_id_m
        ,      ingr.ingr_id
        ,      r.indcd
        ,      r.indtext
        ,      r.indtext
        ,      r.vervallen
        from   kgc_indicatie_groepen ingr
        where  ingr.ongr_id = c_ongr_id_m
        and    ingr.code = r.indgrp
        and    not exists
               ( select null
                 from   kgc_indicatie_teksten indi
                 where  indi.ongr_id = c_ongr_id_m
                 and    indi.ingr_id = ingr.ingr_id
                 and    indi.code = upper(r.indcd)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_indi;

  PROCEDURE toevoegen_conc IS
    CURSOR oud_cur IS
      SELECT 'B '||stan_cd code
      ,      stan_text omschrijving
      FROM   tmp_stc315
      union
      SELECT 'H '||res_code
      ,      res_omschrijving
      FROM   tmp_stc316
      ;
  BEGIN
    FOR r IN oud_cur
    LOOP
      BEGIN
        insert into kgc_conclusie_teksten
        ( kafd_id
        , code
        , omschrijving
        , vervallen
        )
        select c_kafd_id
        ,      upper(r.code)
        ,      nvl( r.omschrijving, r.code )
        ,      'N'
        from   dual
        where  not exists
               ( select null
                 from   kgc_conclusie_teksten
                 where  kafd_id = c_kafd_id
                 and    code = upper(r.code)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_conc;

  PROCEDURE toevoegen_cond IS
    CURSOR oud_cur IS
      SELECT conditie_code
      ,      conditie_oms
      ,      vervallen
      FROM   tmp_stc900
      ;
  BEGIN
    FOR r IN oud_cur
    LOOP
      BEGIN
        insert into kgc_conclusie_teksten
        ( kafd_id
        , code
        , omschrijving
        , vervallen
        )
        select c_kafd_id
        ,      upper(r.conditie_code)
        ,      r.conditie_oms
        ,      r.vervallen
        from   dual
        where  not exists
               ( select null
                 from   kgc_conclusie_teksten
                 where  kafd_id = c_kafd_id
                 and    code = upper(r.conditie_code)
               )
        ;
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
        WHEN OTHERS           THEN RAISE;
      END;
    END LOOP;
  END toevoegen_cond;

  --  ==========================================================================================
  --   P U B L I E K E    P R O C E D U R E S
  --

  PROCEDURE opschonen( p_jaar IN  VARCHAR2 ) IS
    cursor mons_cur
    is
      select mons_id
      ,      monsternummer
      from   kgc_monsters
      where  kafd_id = c_kafd_id
      and    to_char( datum_aanmelding, 'yyyy' ) = p_jaar
and 1=2
      ;
    cursor onde_cur
    is
      select onde_id
      ,      onderzoeknr
      from   kgc_onderzoeken
      where  kafd_id = c_kafd_id
      and    to_char( datum_binnen, 'yyyy' ) = p_jaar
and 1=2
      ;
    v_fout boolean;
    v_resultaat  varchar2(4000);
  BEGIN
    bepaal_afdeling;
    bepaal_onderzoeksgroepen;
    for r in onde_cur
    loop
      -- brieven eerst (ivm foute volgorde in kgc_delete_00
      delete from kgc_brieven
      where  decl_id in
             ( select decl_id
               from   kgc_declaraties
               where  kafd_id = c_kafd_id
               and    entiteit = 'ONDE'
               and    id = r.onde_id
             )
      ;
      delete from kgc_brieven
      where  kafd_id = c_kafd_id
      and    onde_id = r.onde_id
      ;
      kgc_delete_00.init( true );
      kgc_delete_00.verwijder_onderzoek
      (  p_onde_id => r.onde_id
      ,  x_fout => v_fout
      ,  x_resultaat => v_resultaat
      );
      if ( v_fout )
      then
        raise_application_error( -20013, 'Fout bij verwijderen van onderzoek '||r.onderzoeknr||chr(10)||v_resultaat );
      end if;
      commit;
    end loop;
    for r in mons_cur
    loop
      kgc_delete_00.init( true );
      kgc_delete_00.verwijder_monster
      (  p_mons_id => r.mons_id
      ,  x_fout => v_fout
      ,  x_resultaat => v_resultaat
      );
      if ( v_fout )
      then
        raise_application_error( -20013, 'Fout bij verwijderen van monster '||r.monsternummer||chr(10)||v_resultaat );
      end if;
      commit;
    end loop;
  END opschonen;

  --  Procedure : REF_DATA, Converteer referentie data
  PROCEDURE ref_data IS
  BEGIN
    bepaal_afdeling;
    bepaal_onderzoeksgroepen;
	--  Medewerkers
--    toevoegen_mede;
	--  Declaratiewijzen
    toevoegen_dewy;
	--  Meetwaardestructuur
--    toevoegen_mwst;
	--  Projecten
--    toevoegen_proj;
    -- Stoftesten
--    toevoegen_stof;
    -- Materialen
    toevoegen_mate;
    -- Uitslagcodes
--    toevoegen_onui;
    -- Stoftestgroepen
--    toevoegen_stgr;
    -- Leeftijden
--    toevoegen_leef;
     -- Ranges
--   toevoegen_rang;
     -- Indicatiegroepen
   toevoegen_ingr;
    -- Indicatieteksten
   toevoegen_indi;
    -- Conclusieteksten
--   toevoegen_conc;
    -- Condities
   toevoegen_cond;
    commit;
  END ref_data;

  --  Procedure : ONDERZOEKEN
  --
  PROCEDURE onderzoeken( p_jaar IN VARCHAR2 ) IS

    cursor onde_cur_o
    is
      -- E-nummers (lysosomaal)
      select o.*
      ,      po.onderznr
      ,      po.persnr
      ,      po.afgerond
      ,      stc304.ondcattext reden
      from   tmp_stc304 stc304
      ,      tmp_pers_onderz po
      ,      tmp_met_aanvraag o
      where  po.odbnr = o.odbnr
      and    o.ondcat = stc304.ondcatcd (+)
      and    o.ondcat = 'E'
      and    substr( o.binnendat, 7, 4 ) = p_jaar
      and    not exists
             ( SELECT null
               FROM   kgc_onderzoeken onde
               where onde.onde_id = o.odbnr
             )
      order by o.odbnr
      ;
    cursor e_onde_cur_o
    is
      -- ENZ_N (mitochondrieel)
      select o.*
      ,      po.onderznr
      ,      po.persnr
      ,      po.afgerond
      from   tmp_pers_onderz po
      ,      tmp_enz_onderzoek o
      where  po.odbnr = o.odbnr
      and    substr( o.datum_binnen, 7, 4 ) = p_jaar
      and    not exists
             ( SELECT null
               FROM   kgc_onderzoeken onde
               where onde.onde_id = o.odbnr
             )
      order by o.odbnr
      ;
    cursor mons_cur_o
    ( b_odbnr in number
    )
    is
      select *
      from   tmp_met_monster
      where  odbnr = b_odbnr
      order by monsternr
      ;
    cursor klinfo_cur
    ( b_odbnr in number )
    is
      select info_text
      from   tmp_met_klinfo
      where  odbnr = b_odbnr
      order by regnr
      ;
    -- voor mons.medische_indicatie en voor ONIN:
    cursor indi_cur
    ( b_odbnr in number
    , b_ongr_id in number
    )
    is
      select indi.indi_id
      ,      indi.code
      ,      indi.omschrijving
      from   kgc_indicatie_teksten indi
      ,      tmp_met_indicatie i
      where  indi.kafd_id = c_kafd_id
      and    indi.ongr_id = b_ongr_id
      and    indi.code = i.indcd
      and    i.odbnr = b_odbnr
      order by i.indcd
      ;
    -- (E) voor mons.medische_indicatie en voor ONIN:
    cursor e_indi_cur
    ( b_odbnr in number
    , b_ongr_id in number
    )
    is
      select indi.indi_id
      ,      indi.code
      ,      indi.omschrijving
      from   kgc_indicatie_teksten indi
      ,      tmp_enz_indicatie i
      where  indi.kafd_id = c_kafd_id
      and    indi.ongr_id = b_ongr_id
      and    indi.code = i.med_indicatie
      and    i.odbnr = b_odbnr
      order by i.med_indicatie
      ;
    cursor e_indi_tekst_cur
    ( b_odbnr in number
    )
    is
      select med_indicatie_tekst
      from   tmp_enz_indicatie_tekst
      where  odbnr = b_odbnr
      and    med_indicatie_tekst is not null
      ;

    -- alle tests afgerond betekent dat het onderzoek ook is afgerond
    cursor test_af_cur
    ( b_odbnr in number
    )
    is
      select null
      from   tmp_met_test
      where  odbnr = b_odbnr
      and    afgerond = 'N'
      ;
    cursor meti_cur_o
    ( b_odbnr in number
    , b_monsternr in number
    )
    is
      select *
      from   tmp_met_test
      where  odbnr = b_odbnr
      and    monsternr = b_monsternr
      order by matcd, testcd
      ;
    -- test (meting) aangemeld via iuf-print
    cursor iuf_cur
    ( b_odbnr in number
    , b_monsternr in number
    , b_testcd in varchar2
    )
    is
      select max( invdat ) datum_aanmelding
      from   tmp_iuf_print
      where  odbnr = b_odbnr
      and    monsternr = b_monsternr
      and    testcd = b_testcd
      and    invdat is not null
      ;
    cursor meet_cur_o
    ( b_odbnr in number
    , b_monsternr in number
    , b_matcd varchar2
    , b_testcd varchar2
    )
    is
      select *
      from   tmp_met_test_uitslag
      where  odbnr = b_odbnr
      and    monsternr = b_monsternr
      and    matcd = b_matcd
      and    testcd = b_testcd
      ;

    onde_rec_init kgc_onderzoeken%rowtype;
    onde_rec      kgc_onderzoeken%rowtype;
    onin_rec_init kgc_onderzoek_indicaties%rowtype;
    onin_rec      kgc_onderzoek_indicaties%rowtype;
    decl_rec_init kgc_declaraties%rowtype;
    decl_rec      kgc_declaraties%rowtype;
    mons_rec_init kgc_monsters%rowtype;
    mons_rec      kgc_monsters%rowtype;
    meti_rec_init bas_metingen%rowtype;
    meti_rec      bas_metingen%rowtype;
    meet_rec_init bas_meetwaarden%rowtype;
    meet_rec      bas_meetwaarden%rowtype;
    mdet_rec_init bas_meetwaarde_details%rowtype;
    mdet_rec      bas_meetwaarde_details%rowtype;
    uits_rec_init kgc_uitslagen%rowtype;
    uits_rec      kgc_uitslagen%rowtype;
    uico_rec_init kgc_uitslag_conclusies%rowtype;
    uico_rec      kgc_uitslag_conclusies%rowtype;

    v_dummy varchar2(1);
    v_naam_verwijzer_na   VARCHAR2(2000);
    v_klinische_info varchar2(2000);
    v_brty_id_e  number := kgc_uk2pk.brty_id( 'EINDBRIEF' );
    v_mwst_id number := kgc_uk2pk.mwst_id( 'METAB_OUD' );
    v_onty_id number := kgc_uk2pk.onty_id( 'VRG' );
    v_indi_id number;

    v_nr_tot   NUMBER := 0;
    v_nr_onde  NUMBER := 0;
    v_nr_onin  NUMBER := 0;
    v_nr_decl  NUMBER := 0;
    v_nr_mons  NUMBER := 0;
    v_nr_meti  NUMBER := 0;
    v_nr_meet  NUMBER := 0;
    v_nr_uits  NUMBER := 0;
    v_nr_brie  NUMBER := 0;
    v_nr_reon  NUMBER := 0;
  BEGIN
    bepaal_afdeling;
    bepaal_onderzoeksgroepen;
    bepaal_rela_onbekend;
    for onde_rec_o in onde_cur_o
    loop
--exit when onde_cur_o%rowcount > 10;
debug( to_char(sysdate,'hh24:mi:ss')||' odbnr='||onde_rec_o.odbnr );
      v_nr_tot := v_nr_tot+1;
      onde_rec := onde_rec_init;
      onin_rec := onin_rec_init;
      decl_rec := decl_rec_init;
      uits_rec := uits_rec_init;
      uico_rec := uico_rec_init;
      v_naam_verwijzer_na := null;
      -- persoon, foetus
      onde_rec.pers_id := onde_rec_o.persnr;
      check_pers_foet( x_pers_id => onde_rec.pers_id
                     , x_foet_id => onde_rec.foet_id
                     );
      IF onde_rec.pers_id IS NULL
      THEN
        onde_rec.pers_id := onde_rec_o.persnr;
      END IF;

      IF persoon_ok( p_pers_id => onde_rec.pers_id )
      THEN
debug( to_char(sysdate,'hh24:mi:ss')||' persoon ok='||onde_rec_o.persnr );
        onde_rec.kafd_id := c_kafd_id;
        onde_rec.onde_id := onde_rec_o.odbnr;
        onde_rec.onderzoeknr := onde_rec_o.onderznr;
        onde_rec.datum_binnen := nvl( conv_datum( onde_rec_o.binnendat ), onde_rec_o.invdat );
        onde_rec.spoed := nvl( onde_rec_o.spoed, 'N' );
        onde_rec.omschrijving := onde_rec_o.reden;
        onde_rec.inst_id := instelling_id( onde_rec_o.ziekhs ); -- instelling aanvrager EN locatie patientstatus
        -- onderzoeksgroep
        onde_rec.ongr_id := c_ongr_id_l;
        -- onderzoekstype
        if ( substr( onde_rec_o.onderznr, 1, 1 ) = 'R' )
        then
          onde_rec.onderzoekstype :=  'R';
        elsif ( substr( onde_rec_o.onderznr, 1, 1 ) = 'C' )
        then
          onde_rec.onderzoekstype := 'C';
        else
          onde_rec.onderzoekstype := 'D';
        end if;
        -- declareren
        if ( onde_rec.datum_binnen < to_date( '01-07-1996', 'dd-mm-yyyy' ) )
        then -- oude stijl
          onde_rec.declareren := nvl( onde_rec_o.decl, 'N' );
        else -- nieuwe stijl
          onde_rec.declareren := nvl( onde_rec_o.met_macht, 'N' ); -- mogelijk hieronder overruled
        end if;
        -- declaratiewijze
        if ( onde_rec.declareren = 'J' )
        then
          onde_rec.dewy_id := kgc_uk2pk.dewy_id( c_kafd_id, 'BI' );
        end if;
        -- aanvrager
        if ( onde_rec_o.relcode is not null )
        then
          onde_rec.rela_id := kgc_uk2pk.rela_id( onde_rec_o.relcode );
        end if;
debug( to_char(sysdate,'hh24:mi:ss')||' bekende relatie?='||onde_rec_o.relcode );
        if ( onde_rec.rela_id is null )
        then
          coun_koppel.bekende_relatie
          ( p_code       => onde_rec_o.relcode
          , p_naam       => onde_rec_o.anvrgr_1
          , p_adres      => onde_rec_o.anvrgr_2 -- anvrgr_3 is iets met postbus, staat ook in _2.
          , p_postcode   => onde_rec_o.anvrgr_4
          , p_woonplaats => onde_rec_o.anvrgr_5
          , p_spec_id    => specialisme_id( onde_rec_o.spec_anvrgr )
          , p_inst_id    => onde_rec.inst_id
          , x_rela_id    => onde_rec.rela_id
          , x_aanspreken => v_naam_verwijzer_na
          );
        end if;
        IF  onde_rec.rela_id IS NULL
    	THEN
          onde_rec.rela_id := c_rela_id_onb;
        ELSE
          vergelijk_verwijzer_voor_na
          ( p_onderzoeknr    => onde_rec.onderzoeknr
          , p_verwijzer_voor => onde_rec_o.anvrgr_1
          , p_verwijzer_na   => v_naam_verwijzer_na
          );
        END IF;
debug( to_char(sysdate,'hh24:mi:ss')||' bekende relatie='||onde_rec.rela_id );
        -- project
        if ( onde_rec_o.projcd is not null )
        then
          onde_rec.proj_id := kgc_uk2pk.proj_id( c_kafd_id, onde_rec_o.projcd );
        end if;
        -- herkomst
        if ( onde_rec_o.herkcd is not null )
        then
          onde_rec.herk_id := kgc_uk2pk.herk_id( onde_rec_o.herkcd );
        end if;
        -- familie relatie
        if ( onde_rec_o.relatie is not null )
        then
          onde_rec.fare_id := familierelatie_id( onde_rec_o.relatie );
        end if;
        -- uitslagcode
        if ( onde_rec_o.afwcat is not null )
        then
          onde_rec.onui_id := kgc_uk2pk.onui_id( c_kafd_id, onde_rec_o.afwcat );
        end if;
        -- autorisator
        if ( onde_rec_o.autorisator is not null )
        then
          if ( onde_rec_o.autorisator = 'JT' )
          then
            onde_rec_o.autorisator := 'FT';
          elsif ( onde_rec_o.autorisator = 'JBAS' )
          then
            onde_rec_o.autorisator := 'JBA';
          elsif ( onde_rec_o.autorisator in ( 'FTS', 'HBS', 'JJS', 'RAS', 'RWS' ) )
          then
            onde_rec_o.autorisator := substr( onde_rec_o.autorisator, 1, 2 );
          end if;
          onde_rec.mede_id_autorisator := kgc_uk2pk.mede_id( onde_rec_o.autorisator );
          onde_rec.datum_autorisatie := conv_datum( onde_rec_o.aut_dat );
        end if;
        -- afgerond
        if ( onde_rec_o.brief_prn_dat is not null
         and onde_rec_o.aut_dat is not null
           )
        then
          onde_rec.afgerond := 'J';
        else
          onde_rec.afgerond := nvl( onde_rec_o.afgerond, 'N' );
          if ( onde_rec.afgerond = 'N' )
          then
            open  test_af_cur( onde_rec_o.odbnr );
            fetch test_af_cur
            into  v_dummy;
            if ( test_af_cur%notfound ) -- alle testen afgerond
            then
              onde_rec.afgerond := 'J';
            end if;
            close test_af_cur;
          end if;
        end if;

        BEGIN
          --  Onderzoek
          INSERT INTO kgc_onderzoeken
          (       kafd_id
          ,       ongr_id
          ,       onde_id
          ,       pers_id
          ,       foet_id
          ,       onderzoekstype
          ,       onderzoeknr
          ,       rela_id
          ,       herk_id
          ,       spoed
          ,       declareren
          ,       dewy_id
          ,       afgerond
          ,       datum_binnen
          ,       inst_id
          ,       mede_id_autorisator
          ,       fare_id
          ,       proj_id
          ,       datum_autorisatie
          ,       omschrijving
          ,       created_by
  		  ,       creation_date
  		  ,       last_updated_by
  		  ,       last_update_date
          )
          values
          (       onde_rec.kafd_id
          ,       onde_rec.ongr_id
          ,       onde_rec.onde_id
          ,       onde_rec.pers_id
          ,       onde_rec.foet_id
          ,       onde_rec.onderzoekstype
          ,       onde_rec.onderzoeknr
          ,       onde_rec.rela_id
          ,       onde_rec.herk_id
          ,       onde_rec.spoed
          ,       onde_rec.declareren
          ,       onde_rec.dewy_id
          ,       onde_rec.afgerond
          ,       onde_rec.datum_binnen
          ,       onde_rec.inst_id
          ,       onde_rec.mede_id_autorisator
          ,       onde_rec.fare_id
          ,       onde_rec.proj_id
          ,       onde_rec.datum_autorisatie
          ,       onde_rec.omschrijving
  		  ,       'CONVERSIE'
  	  	  ,       SYSDATE
  		  ,       'CONVERSIE'
  		  ,       SYSDATE
    	  );
debug( to_char(sysdate,'hh24:mi:ss')||' insert onde='||onde_rec.onde_id );
          v_klinische_info := null;
          for klinfo_rec in klinfo_cur( onde_rec_o.odbnr )
          loop
            if ( v_klinische_info is null  )
            then
              v_klinische_info := klinfo_rec.info_text;
            else
              v_klinische_info := v_klinische_info||chr(10)||klinfo_rec.info_text;
            end if;
          end loop;
          -- (machtigings)indicaties
          for indi_rec in indi_cur( onde_rec_o.odbnr, onde_rec.ongr_id )
          loop
            insert into kgc_onderzoek_indicaties
            ( onde_id
            , indi_id
            , opmerking
            , created_by
            , creation_date
            , last_updated_by
            , last_update_date
            )
            values
            ( onde_rec.onde_id
            , indi_rec.indi_id
            , decode( indi_rec.code, '999', v_klinische_info, null )
            , 'CONVERSIE'
            , SYSDATE
            , 'CONVERSIE'
            , SYSDATE
            );
            v_nr_onin := v_nr_onin+1;
          end loop;
debug( to_char(sysdate,'hh24:mi:ss')||' insert onin='||onde_rec.onde_id );

          -- declaratie: niet overlaten aan standaard: C-nummers zijn ook gedeclareerd!
          if ( onde_rec.declareren = 'J' )
          then
            decl_rec.entiteit := 'ONDE';
            decl_rec.id := onde_rec.onde_id;
            decl_rec.declareren := 'J';
            decl_rec.kafd_id := onde_rec.kafd_id;
            decl_rec.ongr_id := onde_rec.ongr_id;
            decl_rec.pers_id := onde_rec.pers_id;
            decl_rec.dewy_id := onde_rec.dewy_id;
            decl_rec.ingr_id := kgc_indi_00.ingr_id( p_onde_id => onde_rec.onde_id );
            decl_rec.fare_id := onde_rec.fare_id;
            if ( onde_rec.datum_binnen < to_date( '01-07-1996', 'dd-mm-yyyy' ) )
            then -- oude stijl
              decl_rec.status := 'V'; -- =verwerkt; voorkom machtigingsbrieven
            end if;
         -- via insert-statement>>
         --   decl_rec.verz_id
         --   decl_rec.nota_adres
         --   decl_rec.verzekeringswijze
         --   decl_rec.verzekeringsnr
            select kgc_decl_seq.nextval
            into   decl_rec.decl_id
            from   dual;
            insert into kgc_declaraties
            ( decl_id
            , entiteit
            , id
            , declareren
            , status
            , kafd_id
            , ongr_id
            , pers_id
            , dewy_id
            , ingr_id
            , fare_id
            , verz_id
            , nota_adres
            , verzekeringswijze
            , verzekeringsnr
            , created_by
    	    , creation_date
  	        , last_updated_by
  	        , last_update_date
            )
            select
              decl_rec.decl_id
            , decl_rec.entiteit
            , decl_rec.id
            , decl_rec.declareren
            , decl_rec.status
            , decl_rec.kafd_id
            , decl_rec.ongr_id
            , decl_rec.pers_id
            , decl_rec.dewy_id
            , decl_rec.ingr_id
            , decl_rec.fare_id
            , pers.verz_id
            , kgc_adres_00.verzekeraar( pers.verz_id, 'J' )
            , pers.verzekeringswijze
            , pers.verzekeringsnr
            , 'CONVERSIE'
            , SYSDATE
            , 'CONVERSIE'
            , SYSDATE
            from  kgc_personen pers
            where pers.pers_id = decl_rec.pers_id
            ;
            v_nr_decl := v_nr_decl+1;
          end if;
debug( to_char(sysdate,'hh24:mi:ss')||' insert decl='||onde_rec.onde_id );
          -- Machtigingsbrief
          if ( nvl(onde_rec_o.nota_prn_dat,onde_rec_o.met_aangevr) is not null )
          then
            kgc_brie_00.registreer
            ( p_decl_id => decl_rec.decl_id
            , p_kafd_id => c_kafd_id
            , p_pers_id => onde_rec.pers_id
            , p_onde_id => onde_rec.onde_id
            , p_brieftype => 'MACHTIGING'
            , p_datum => conv_datum( nvl( onde_rec_o.met_aangevr, onde_rec_o.nota_prn_dat ) )
            , p_geadresseerde => '.' -- kgc_adres_00.verzekeraar( decl_rec.verz_id, 'J' ) is NIET gevuld!!!
            );
            v_nr_brie := v_nr_brie+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert machtbrief='||onde_rec.onde_id );
          end if;

          -- monsters
          for mons_rec_o in mons_cur_o ( onde_rec_o.odbnr )
          loop
            mons_rec := mons_rec_init;
            mons_rec.kafd_id := onde_rec.kafd_id;
            mons_rec.ongr_id := onde_rec.ongr_id;
            mons_rec.pers_id := onde_rec.pers_id;
            mons_rec.foet_id := onde_rec.foet_id;
            mons_rec.monsternummer := onde_rec.onderzoeknr;
            mons_rec.mate_id := kgc_uk2pk.mate_id( c_kafd_id, mons_rec_o.matcd );
            mons_rec.hoeveelheid_monster := mons_rec_o.volume;
            mons_rec.bewaren := nvl( onde_rec_o.bewaren, 'N' ); -- !!! uit aanvraag !!!
            mons_rec.datum_aanmelding := conv_datum( mons_rec_o.binnendat );
            begin
              mons_rec.datum_afname := conv_datum( nvl( mons_rec_o.afnamedat, mons_rec_o.binnendat ) );
            exception
              when others
              then
                mons_rec.datum_afname := mons_rec.datum_aanmelding;
            end;
            if ( instr( mons_rec_o.afnametijd, '99' ) > 0 )
            then
              mons_rec_o.afnametijd := null;
            end if;
            if ( mons_rec_o.afnametijd is not null )
            then
              mons_rec.tijd_afname := to_date( to_char( mons_rec.datum_afname,'dd-mm-yyyy')
                                               ||' '||mons_rec_o.afnametijd
                                             , 'dd-mm-yyyy hh24:mi'
                                             );
            end if;
            mons_rec.leeftijd := kgc_leef_00.leeftijd
                                 ( p_pers_id => mons_rec.pers_id
                                 , p_foet_id => mons_rec.foet_id
                                 , p_datum_afname => mons_rec.datum_afname
                                 );
            mons_rec.leef_id := kgc_leef_00.leeftijdscategorie
                                ( p_kafd_id => c_kafd_id
                                , p_aantal_dagen => mons_rec.leeftijd
                                );
            if ( mons_rec_o.verzameltijd is not null )
            then
              mons_rec.periode_afname := to_char( mons_rec_o.verzameltijd )||' uur';
            end if;
            mons_rec.periode_afname := mons_rec_o.ckclnr;
            mons_rec.ckcl_nummer := mons_rec_o.ckclnr;
            mons_rec.commentaar := mons_rec_o.opmerking;
            for indi_rec in indi_cur( onde_rec_o.odbnr, onde_rec.ongr_id )
            loop
              if ( mons_rec.medische_indicatie is null )
              then
                mons_rec.medische_indicatie := indi_rec.omschrijving;
              else
                mons_rec.medische_indicatie := mons_rec.medische_indicatie||chr(10)||indi_rec.omschrijving;
              end if;
            end loop;
            if ( v_klinische_info is not null ) -- bepaald bij ONIN
            then
              if ( mons_rec.medische_indicatie is null )
              then
                mons_rec.medische_indicatie := v_klinische_info;
              else
                mons_rec.medische_indicatie := mons_rec.medische_indicatie||chr(10)||v_klinische_info;
              end if;
            end if;

            begin
              select kgc_mons_seq.nextval
              into   mons_rec.mons_id
              from   dual;
              insert into kgc_monsters
              ( mons_id
              , monsternummer
              , ongr_id
              , kafd_id
              , pers_id
              , foet_id
              , mate_id
              , declareren
              , bewaren
              , datum_aanmelding
              , rela_id
              , leef_id
              , leeftijd
              , datum_afname
              , tijd_afname
              , periode_afname
              , ckcl_nummer
              , hoeveelheid_monster
              , medische_indicatie
              , commentaar
              , created_by
              , creation_date
              , last_updated_by
              , last_update_date
              )
              values
              ( mons_rec.mons_id
              , mons_rec.monsternummer
              , mons_rec.ongr_id
              , mons_rec.kafd_id
              , mons_rec.pers_id
              , mons_rec.foet_id
              , mons_rec.mate_id
              , 'N'
              , mons_rec.bewaren
              , mons_rec.datum_aanmelding
              , mons_rec.rela_id
              , mons_rec.leef_id
              , mons_rec.leeftijd
              , mons_rec.datum_afname
              , mons_rec.tijd_afname
              , mons_rec.periode_afname
              , mons_rec.ckcl_nummer
              , mons_rec.hoeveelheid_monster
              , mons_rec.medische_indicatie
              , mons_rec.commentaar
              , 'CONVERSIE'
              , SYSDATE
              , 'CONVERSIE'
              , SYSDATE
              );
              insert into kgc_onderzoek_monsters
              ( onde_id
              , mons_id
              , created_by
              , creation_date
              , last_updated_by
              , last_update_date
              )
              values
              ( onde_rec.onde_id
              , mons_rec.mons_id
              , 'CONVERSIE'
              , SYSDATE
              , 'CONVERSIE'
              , SYSDATE
              );
debug( to_char(sysdate,'hh24:mi:ss')||' insert mons='||mons_rec.mons_id );

              insert into bas_fracties
              ( mons_id
              , fractienummer
              , commentaar
              , created_by
              , creation_date
              , last_updated_by
              , last_update_date
              )
              values
              ( mons_rec.mons_id
              , mons_rec.monsternummer
              , 'Uit conversie: monster'
              , 'CONVERSIE'
              , SYSDATE
              , 'CONVERSIE'
              , SYSDATE
              );
              v_nr_mons := v_nr_mons+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert frac='||mons_rec.mons_id );

              -- Metingen, meetwaarden
              for meti_rec_o in meti_cur_o( onde_rec_o.odbnr, mons_rec_o.monsternr )
              loop
                meti_rec := meti_rec_init;
                meti_rec.afgerond := nvl( meti_rec_o.afgerond, 'N' );
                if ( meti_rec.afgerond = 'A' )
                then
                  meti_rec.afgerond := 'J';
                end if;
                meti_rec.herhalen := nvl( meti_rec_o.herhalen, 'N' );
                meti_rec.prioriteit := nvl( mons_rec_o.spoed, 'N' );
                meti_rec.stgr_id := kgc_uk2pk.stgr_id( c_kafd_id, stgr_code( meti_rec_o.testcd,meti_rec_o.matcd ) );
                meti_rec.mede_id := kgc_uk2pk.mede_id( meti_rec_o.analist );
                open  iuf_cur( b_odbnr => onde_rec_o.odbnr
                             , b_monsternr => mons_rec_o.monsternr
                             , b_testcd => meti_rec_o.testcd
                             );
                fetch iuf_cur into meti_rec.datum_aanmelding;
                close iuf_cur;
                if ( meti_rec.datum_aanmelding is null )
                then
                  meti_rec.datum_aanmelding := mons_rec.datum_aanmelding;
                end if;
                meti_rec.datum_afgerond := conv_datum( meti_rec_o.uitslagdat );
                meti_rec.datum_conclusie := meti_rec.datum_afgerond;
                meti_rec.opmerking_algemeen := null;
                meti_rec.opmerking_analyse := meti_rec_o.opmerking;
                meti_rec.datum_selectie_robot := conv_datum( meti_rec_o.mil_ins_datum );
                meti_rec.datum_uitslag_robot := conv_datum( meti_rec_o.mil_datum );
                meti_rec.conclusie := substr(meti_rec_o.beoordeling||meti_rec_o.beoordeling2,1,2000);
                begin
                  select bas_meti_seq.nextval
                  into meti_rec.meti_id
                  from dual;
                  insert into bas_metingen
                  ( meti_id
                  , onmo_id
                  , onde_id
                  , frac_id
                  , declareren
                  , afgerond
                  , herhalen
                  , prioriteit
                  , snelheid
                  , stgr_id
                  , mede_id
                  , datum_aanmelding
                  , datum_afgerond
                  , datum_conclusie
                  , opmerking_algemeen
                  , opmerking_analyse
                  , datum_selectie_robot
                  , datum_uitslag_robot
                  , conclusie
                  , created_by
                  , creation_date
                  , last_updated_by
                  , last_update_date
                  )
                  select
                    meti_rec.meti_id
                  , onmo.onmo_id
                  , onmo.onde_id
                  , frac.frac_id
                  , 'N'
                  , meti_rec.afgerond
                  , meti_rec.herhalen
                  , meti_rec.prioriteit
                  , 'K'
                  , meti_rec.stgr_id
                  , meti_rec.mede_id
                  , meti_rec.datum_aanmelding
                  , meti_rec.datum_afgerond
                  , meti_rec.datum_conclusie
                  , meti_rec.opmerking_algemeen
                  , meti_rec.opmerking_analyse
                  , meti_rec.datum_selectie_robot
                  , meti_rec.datum_uitslag_robot
                  , meti_rec.conclusie
                  , 'CONVERSIE'
                  , SYSDATE
                  , 'CONVERSIE'
                  , SYSDATE
                  from  bas_fracties frac
                  ,     kgc_onderzoek_monsters onmo
                  where onmo.mons_id = frac.mons_id  -- 1 fractie per monster
                  and   onmo.onde_id = onde_rec.onde_id
                  and   onmo.mons_id = mons_rec.mons_id
                  ;
                  v_nr_meti := v_nr_meti+sql%rowcount; -- normaliter 1.
debug( to_char(sysdate,'hh24:mi:ss')||' insert meti='||meti_rec.meti_id );
                  for meet_rec_o in meet_cur_o( b_odbnr => onde_rec_o.odbnr
                                              , b_monsternr => mons_rec_o.monsternr
                                              , b_matcd => meti_rec_o.matcd
                                              , b_testcd => meti_rec_o.testcd
                                              )
                  loop
                    meet_rec := meet_rec_init;
                    meet_rec.stof_id := stoftest_id( meet_rec_o.stof_naam );
                    if ( meet_rec.stof_id is not null )
                    then
                      meet_rec.meti_id := meti_rec.meti_id;
                      meet_rec.afgerond := meti_rec.afgerond;
                      meet_rec.meet_reken := 'M';
                      meet_rec.meetwaarde := meet_rec_o.uitslag1;
                      meet_rec.meeteenheid := meti_rec_o.eenheid1;
                      meet_rec.mwst_id := v_mwst_id;
                      meet_rec.mede_id := meti_rec.mede_id;
                      meet_rec.datum_meting := meti_rec.datum_afgerond;
                      meet_rec.normaalwaarde_ondergrens := bas_meet_00.normaalwaarde_onder( meet_rec.stof_id, meet_rec.meti_id, NULL, NULL );
                      meet_rec.normaalwaarde_bovengrens := bas_meet_00.normaalwaarde_boven( meet_rec.stof_id, meet_rec.meti_id, NULL, NULL );
                      begin
                        SELECT bas_meet_seq.nextval
                        INTO   meet_rec.meet_id
                        FROM   dual;
                        INSERT INTO bas_meetwaarden
                        ( meet_id
                        , meti_id
                        , stof_id
                        , mwst_id
                        , meet_reken
                        , meeteenheid
                        , normaalwaarde_ondergrens
                        , normaalwaarde_bovengrens
                        , mede_id
                        , afgerond
                        , meetwaarde
                        , datum_meting
                        )
                        VALUES
                        ( meet_rec.meet_id
                        , meet_rec.meti_id
                        , meet_rec.stof_id
                        , meet_rec.mwst_id
                        , meet_rec.meet_reken
                        , meet_rec.meeteenheid
                        , meet_rec.normaalwaarde_ondergrens
                        , meet_rec.normaalwaarde_bovengrens
                        , meet_rec.mede_id
                        , meet_rec.afgerond
                        , meet_rec.meetwaarde
                        , meet_rec.datum_meting
                        );
                        v_nr_meet := v_nr_meet+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert meet='||meet_rec.meet_id );
                        begin
                          update bas_meetwaarde_details
                          set    waarde = meet_rec_o.uitslag1
                          where  meet_id = meet_rec.meet_id
                          and    volgorde = 3
                          ;
                          if ( sql%rowcount = 0 )
                          then
                            insert into bas_meetwaarde_details
                            ( meet_id
                            , volgorde
                            , prompt
                            , waarde
                            )
                            values
                            ( meet_rec.meet_id
                            , 3
                            , 'Gemiddelde'
                            , meet_rec_o.uitslag1
                            );
                          end if;
debug( to_char(sysdate,'hh24:mi:ss')||' insert mdet='||meet_rec.meet_id );
                        exception
                          when others then null;
                        end;
                      EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
		                  NULL;
                        WHEN OTHERS THEN
                          p.l( 'STOFTEST '||meet_rec_o.stof_naam||' voor '||mons_rec.monsternummer||' ('||onde_rec_o.odbnr||')' );
                          p.l( SQLERRM );
                      end;
                    end if;
                  end loop;
                EXCEPTION
                  WHEN DUP_VAL_ON_INDEX THEN
		            NULL;
                  WHEN OTHERS THEN
                    p.l( 'TEST '||meti_rec_o.testcd||' voor '||mons_rec.monsternummer||' ('||onde_rec_o.odbnr||')' );
                    p.l( SQLERRM );
                end;
              end loop;
            EXCEPTION
              WHEN DUP_VAL_ON_INDEX THEN
		        NULL;
              WHEN OTHERS THEN
                p.l( 'MONSTER '||mons_rec.monsternummer||' ('||onde_rec_o.odbnr||')' );
                p.l( SQLERRM );
            end;
          end loop;
          -- uitslag
          if ( onde_rec_o.stan_text is not null or onde_rec_o.conclusie is not null )
          then
            uits_rec.onde_id := onde_rec.onde_id;
            uits_rec.kafd_id := c_kafd_id;
            uits_rec.rela_id := onde_rec.rela_id;
            uits_rec.brty_id := v_brty_id_e;
            uits_rec.mede_id := onde_rec.mede_id_autorisator;
            uits_rec.datum_autorisatie := conv_datum( onde_rec_o.aut_dat );
            uits_rec.datum_uitslag := uits_rec.datum_autorisatie;
            if ( onde_rec_o.anvrgr_1 is not null or onde_rec_o.anvrgr_2 is not null )
            then
              uits_rec.volledig_adres := onde_rec_o.anvrgr_1
                             ||chr(10)|| onde_rec_o.anvrgr_2
                             ||chr(10)|| onde_rec_o.anvrgr_4||' '||onde_rec_o.anvrgr_5
                             ||chr(10)|| onde_rec_o.anvrgr_6
                                     ;
            else
              uits_rec.volledig_adres := kgc_adres_00.relatie( uits_rec.rela_id, 'J' );
            end if;
            uits_rec.tekst := conclusietekst( onde_rec_o.stan_text );
            if ( onde_rec_o.conclusie is not null )
            then
              if ( uits_rec.tekst is null )
              then
                uits_rec.tekst := onde_rec_o.conclusie;
              else
                uits_rec.tekst := uits_rec.tekst||chr(10)||onde_rec_o.conclusie;
              end if;
            end if;
            if ( uits_rec.tekst is null )
            then
              uits_rec.commentaar := 'Standaardtekst '||onde_rec_o.stan_text;
            end if;
            select kgc_uits_seq.nextval
            into   uits_rec.uits_id
            from   dual;
            insert into kgc_uitslagen
            ( uits_id
            , onde_id
            , kafd_id
            , rela_id
            , brty_id
            , mede_id
            , datum_uitslag
            , datum_autorisatie
            , volledig_adres
            , tekst
            , commentaar
            )
            values
            ( uits_rec.uits_id
            , uits_rec.onde_id
            , uits_rec.kafd_id
            , uits_rec.rela_id
            , uits_rec.brty_id
            , uits_rec.mede_id
            , uits_rec.datum_uitslag
            , uits_rec.datum_autorisatie
            , uits_rec.volledig_adres
            , uits_rec.tekst
            , uits_rec.commentaar
            );
            v_nr_uits := v_nr_uits+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert uits='||onde_rec.onde_id );
            if ( onde_rec_o.stan_text is not null )
            then
              uico_rec.uits_id := uits_rec.uits_id;
              uico_rec.conc_id := kgc_uk2pk.conc_id( c_kafd_id, onde_rec_o.stan_text );
              if ( uico_rec.conc_id is not null )
              then
                insert into kgc_uitslag_conclusies
                ( uits_id
                , conc_id
                )
                values
                ( uico_rec.uits_id
                , uico_rec.conc_id
                );
              end if;
debug( to_char(sysdate,'hh24:mi:ss')||' insert uico='||onde_rec.onde_id );
            end if;
            -- Uitslagbrief
            kgc_brie_00.registreer
            ( p_onde_id => uits_rec.onde_id
            , p_kafd_id => c_kafd_id
            , p_pers_id => onde_rec.pers_id
            , p_uits_id => uits_rec.uits_id
            , p_rela_id => uits_rec.rela_id
            , p_brieftype => 'UITSLAG'
            , p_datum => uits_rec.datum_uitslag
            , p_geadresseerde => uits_rec.volledig_adres
            );
            v_nr_brie := v_nr_brie+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert uitslagbrief='||onde_rec.onde_id );
          end if;

          if ( onde_rec_o.vrg_onderznr is not null )
          then
            insert into kgc_gerelateerde_onderzoeken
            ( onde_id
            , externe_onderzoeknr
            , onty_id
            )
            values
            ( onde_rec.onde_id
            , onde_rec_o.vrg_onderznr
            , v_onty_id
            );
            v_nr_reon := v_nr_reon+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert reon='||onde_rec_o.vrg_onderznr );
          end if;
          v_nr_onde := v_nr_onde+1;
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN
		    NULL;
          WHEN OTHERS THEN
            p.l( 'ONDERZOEK '||onde_rec.onderzoeknr||' ('||onde_rec_o.odbnr||')' );
            p.l( SQLERRM );
        END;
        commit;
debug( to_char(sysdate,'hh24:mi:ss')||' commit onde='||onde_rec.onde_id );
      END IF; -- persoon ok?
    end loop;

    for onde_rec_o in e_onde_cur_o
    loop
-- exit when e_onde_cur_o%rowcount > 10;
debug( to_char(sysdate,'hh24:mi:ss')||' odbnr='||onde_rec_o.odbnr );
      v_nr_tot := v_nr_tot+1;
      onde_rec := onde_rec_init;
      onin_rec := onin_rec_init;
      decl_rec := decl_rec_init;
      uits_rec := uits_rec_init;
      uico_rec := uico_rec_init;
      v_naam_verwijzer_na := null;
      -- persoon, foetus
      onde_rec.pers_id := onde_rec_o.persnr;
      check_pers_foet( x_pers_id => onde_rec.pers_id
                     , x_foet_id => onde_rec.foet_id
                     );
      IF onde_rec.pers_id IS NULL
      THEN
        onde_rec.pers_id := onde_rec_o.persnr;
      END IF;

      IF persoon_ok( p_pers_id => onde_rec.pers_id )
      THEN
debug( to_char(sysdate,'hh24:mi:ss')||' persoon ok='||onde_rec_o.persnr );
        onde_rec.kafd_id := c_kafd_id;
        onde_rec.onde_id := onde_rec_o.odbnr;
        onde_rec.onderzoeknr := onde_rec_o.onderznr;
        onde_rec.creation_date := conv_datum( onde_rec_o.datum_invoer );
        onde_rec.datum_binnen := nvl( conv_datum( onde_rec_o.datum_binnen ), onde_rec.creation_date );
        onde_rec.spoed := 'N';
        if ( substr( onde_rec.onderzoeknr, 1, 1 ) = 'M' )
        then
          onde_rec.omschrijving := 'Enzymdiagnostiek: mitochondrieel';
        elsif ( substr( onde_rec.onderzoeknr, 1, 1 ) = 'W' )
        then
          onde_rec.omschrijving := 'Enzymdiagnostiek: homocysteine';
        end if;
        -- onderzoeksgroep
        onde_rec.ongr_id := c_ongr_id_m;
        -- onderzoekstype
        onde_rec.onderzoekstype := 'D';
        -- declareren, declaratiewijze
        if ( onde_rec_o.declareren = 'J' )
        then
          onde_rec.declareren := 'J';
          onde_rec.dewy_id := kgc_uk2pk.dewy_id( c_kafd_id, 'BI' );
        elsif ( onde_rec_o.declareren = 'B' )
        then
          onde_rec.declareren := 'J';
          onde_rec.dewy_id := kgc_uk2pk.dewy_id( c_kafd_id, 'BU' );
        elsif ( onde_rec_o.declareren = 'E' )
        then
          onde_rec.declareren := 'J';
          onde_rec.dewy_id := kgc_uk2pk.dewy_id( c_kafd_id, 'E' );
        elsif ( onde_rec_o.declareren = 'N' )
        then
          onde_rec.declareren := 'N';
          onde_rec.dewy_id := null;
        else
          onde_rec.dewy_id := kgc_uk2pk.dewy_id( c_kafd_id, onde_rec_o.declareren );
          if (onde_rec.dewy_id is not null )
          then
            onde_rec.declareren := 'J';
          else
            onde_rec.declareren := 'N';
          end if;
        end if;
        -- aanvrager
        if ( onde_rec_o.aanvragercode is not null )
        then
          onde_rec.rela_id := kgc_uk2pk.rela_id( onde_rec_o.aanvragercode );
        end if;
debug( to_char(sysdate,'hh24:mi:ss')||' bekende relatie?='||onde_rec_o.aanvragercode );
        if ( onde_rec.rela_id is null )
        then
          coun_koppel.bekende_relatie
          ( p_code       => onde_rec_o.aanvragercode
          , p_naam       => onde_rec_o.aanvr_naam
          , p_adres      => onde_rec_o.aanvr_straat_2
          , p_postcode   => onde_rec_o.aanvr_postcd
          , p_woonplaats => onde_rec_o.aanvr_woonplaats
          , p_spec_id    => specialisme_id( onde_rec_o.aanvr_specialisme )
          , x_rela_id    => onde_rec.rela_id
          , x_aanspreken => v_naam_verwijzer_na
          );
        end if;
        IF  onde_rec.rela_id IS NULL
    	THEN
          onde_rec.rela_id := c_rela_id_onb;
        ELSE
          vergelijk_verwijzer_voor_na
          ( p_onderzoeknr    => onde_rec.onderzoeknr
          , p_verwijzer_voor => onde_rec_o.aanvr_naam
          , p_verwijzer_na   => v_naam_verwijzer_na
          );
        END IF;
debug( to_char(sysdate,'hh24:mi:ss')||' bekende relatie='||onde_rec.rela_id );
        -- herkomst
        if ( onde_rec_o.herkomst is not null )
        then
          onde_rec.herk_id := kgc_uk2pk.herk_id( onde_rec_o.herkomst );
        end if;
        -- familie relatie
        if ( onde_rec_o.fam_relatie is not null )
        then
          onde_rec.fare_id := familierelatie_id( onde_rec_o.fam_relatie );
        end if;
        -- autorisator
        if ( onde_rec_o.autorisator is not null )
        then
          if ( onde_rec_o.autorisator = 'LVDH' )
          then
            onde_rec_o.autorisator := 'BVDH';
          elsif ( onde_rec_o.autorisator in ( 'FTS', 'WRS' ) )
          then
            onde_rec_o.autorisator := substr( onde_rec_o.autorisator, 1, 2 );
          end if;
          onde_rec.mede_id_autorisator := kgc_uk2pk.mede_id( onde_rec_o.autorisator );
          onde_rec.datum_autorisatie := conv_datum( onde_rec_o.datum_autorisatie );
        end if;
        -- afgerond
        onde_rec.afgerond := nvl(onde_rec_o.onderzoek_af, 'N' );
        -- taal
        if ( onde_rec_o.taal_brief = 'D' )
        then
          onde_rec.taal_id := kgc_uk2pk.taal_id( 'DEU' );
        elsif ( onde_rec_o.taal_brief = 'E' )
        then
          onde_rec.taal_id := kgc_uk2pk.taal_id( 'ENG' );
        elsif ( onde_rec_o.taal_brief = 'NL' )
        then
          onde_rec.taal_id := kgc_uk2pk.taal_id( 'NED' );
        end if;
        if ( onde_rec_o.nota_adres_1 is not null )
        then
          select decode( onde_rec_o.nota_adres_1,null,null,chr(10)|| onde_rec_o.nota_adres_1)
              || decode( onde_rec_o.nota_adres_2,null,null,chr(10)|| onde_rec_o.nota_adres_2)
              || decode( onde_rec_o.nota_adres_3,null,null,chr(10)|| onde_rec_o.nota_adres_3)
              || decode( onde_rec_o.nota_adres_4,null,null,chr(10)|| onde_rec_o.nota_adres_4)
              || decode( onde_rec_o.nota_adres_5,null,null,chr(10)|| onde_rec_o.nota_adres_5)
              || decode( onde_rec_o.nota_adres_6,null,null,chr(10)|| onde_rec_o.nota_adres_6)
              || decode( onde_rec_o.nota_adres_7,null,null,chr(10)|| onde_rec_o.nota_adres_7)
              || decode( onde_rec_o.nota_adres_8,null,null,chr(10)|| onde_rec_o.nota_adres_8)
          into   onde_rec.nota_adres
          from   dual;
        end if;
        BEGIN
          --  Onderzoek
          INSERT INTO kgc_onderzoeken
          (       kafd_id
          ,       ongr_id
          ,       onde_id
          ,       pers_id
          ,       foet_id
          ,       onderzoekstype
          ,       onderzoeknr
          ,       rela_id
          ,       herk_id
          ,       taal_id
          ,       spoed
          ,       declareren
          ,       dewy_id
          ,       afgerond
          ,       datum_binnen
          ,       mede_id_autorisator
          ,       fare_id
          ,       nota_adres
          ,       datum_autorisatie
          ,       omschrijving
          ,       created_by
  		  ,       creation_date
  		  ,       last_updated_by
  		  ,       last_update_date
          )
          values
          (       onde_rec.kafd_id
          ,       onde_rec.ongr_id
          ,       onde_rec.onde_id
          ,       onde_rec.pers_id
          ,       onde_rec.foet_id
          ,       onde_rec.onderzoekstype
          ,       onde_rec.onderzoeknr
          ,       onde_rec.rela_id
          ,       onde_rec.herk_id
          ,       onde_rec.taal_id
          ,       onde_rec.spoed
          ,       onde_rec.declareren
          ,       onde_rec.dewy_id
          ,       onde_rec.afgerond
          ,       onde_rec.datum_binnen
          ,       onde_rec.mede_id_autorisator
          ,       onde_rec.fare_id
          ,       onde_rec.nota_adres
          ,       onde_rec.datum_autorisatie
          ,       onde_rec.omschrijving
  		  ,       'CONVERSIE'
  	  	  ,       nvl( onde_rec.creation_date, sysdate )
  		  ,       'CONVERSIE'
  		  ,       SYSDATE
    	  );
debug( to_char(sysdate,'hh24:mi:ss')||' insert onde='||onde_rec.onde_id );
          -- (machtigings)indicaties
          for indi_rec in e_indi_cur( onde_rec_o.odbnr, onde_rec.ongr_id )
          loop
            insert into kgc_onderzoek_indicaties
            ( onde_id
            , indi_id
            , opmerking
            , created_by
            , creation_date
            , last_updated_by
            , last_update_date
            )
            values
            ( onde_rec.onde_id
            , indi_rec.indi_id
            , null
            , 'CONVERSIE'
            , SYSDATE
            , 'CONVERSIE'
            , SYSDATE
            );
            v_nr_onin := v_nr_onin+1;
          end loop;
debug( to_char(sysdate,'hh24:mi:ss')||' insert onin='||onde_rec.onde_id );
          v_indi_id := kgc_uk2pk.indi_id( onde_rec.ongr_id, '999' );
          -- opmerking bij indicatie
          for indi_rec in e_indi_tekst_cur( onde_rec_o.odbnr )
          loop
            update kgc_onderzoek_indicaties
            set    opmerking = indi_rec.med_indicatie_tekst
            where  onde_id = onde_rec.onde_id
            and    indi_id = v_indi_id;
            if ( sql%rowcount = 0 )
            then
              insert into kgc_onderzoek_indicaties
              ( onde_id
              , indi_id
              , opmerking
              , created_by
              , creation_date
              , last_updated_by
              , last_update_date
              )
              values
              ( onde_rec.onde_id
              , v_indi_id
              , indi_rec.med_indicatie_tekst
              , 'CONVERSIE'
              , SYSDATE
              , 'CONVERSIE'
              , SYSDATE
              );
            end if;
          end loop;

          -- declaratie: niet overlaten aan standaard
          if ( onde_rec.declareren = 'J' )
          then
            decl_rec.entiteit := 'ONDE';
            decl_rec.id := onde_rec.onde_id;
            decl_rec.declareren := 'J';
            decl_rec.kafd_id := onde_rec.kafd_id;
            decl_rec.ongr_id := onde_rec.ongr_id;
            decl_rec.pers_id := onde_rec.pers_id;
            decl_rec.dewy_id := onde_rec.dewy_id;
            decl_rec.ingr_id := kgc_indi_00.ingr_id( p_onde_id => onde_rec.onde_id );
            decl_rec.fare_id := onde_rec.fare_id;
         -- via insert-statement>>
         --   decl_rec.verz_id
         --   decl_rec.nota_adres
         --   decl_rec.verzekeringswijze
         --   decl_rec.verzekeringsnr
            select kgc_decl_seq.nextval
            into   decl_rec.decl_id
            from   dual;
            insert into kgc_declaraties
            ( decl_id
            , entiteit
            , id
            , declareren
            , status
            , kafd_id
            , ongr_id
            , pers_id
            , dewy_id
            , ingr_id
            , fare_id
            , verz_id
            , nota_adres
            , verzekeringswijze
            , verzekeringsnr
            , created_by
    	    , creation_date
  	        , last_updated_by
  	        , last_update_date
            )
            select
              decl_rec.decl_id
            , decl_rec.entiteit
            , decl_rec.id
            , decl_rec.declareren
            , decl_rec.status
            , decl_rec.kafd_id
            , decl_rec.ongr_id
            , decl_rec.pers_id
            , decl_rec.dewy_id
            , decl_rec.ingr_id
            , decl_rec.fare_id
            , pers.verz_id
            , nvl( onde_rec.nota_adres, kgc_adres_00.verzekeraar( pers.verz_id, 'J' ) )
            , pers.verzekeringswijze
            , pers.verzekeringsnr
            , 'CONVERSIE'
            , SYSDATE
            , 'CONVERSIE'
            , SYSDATE
            from  kgc_personen pers
            where pers.pers_id = decl_rec.pers_id
            ;
            v_nr_decl := v_nr_decl+1;
          end if;
debug( to_char(sysdate,'hh24:mi:ss')||' insert decl='||onde_rec.onde_id );
          -- Machtigingsbrief
          if ( onde_rec_o.datum_print_machtiging is not null )
          then
            kgc_brie_00.registreer
            ( p_decl_id => decl_rec.decl_id
            , p_kafd_id => c_kafd_id
            , p_pers_id => onde_rec.pers_id
            , p_onde_id => onde_rec.onde_id
            , p_brieftype => 'MACHTIGING'
            , p_datum => conv_datum( onde_rec_o.datum_print_machtiging )
            , p_geadresseerde => nvl( onde_rec.nota_adres, '.' ) -- kgc_adres_00.verzekeraar( decl_rec.verz_id, 'J' ) is NIET gevuld!!!
            );
            v_nr_brie := v_nr_brie+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert machtbrief='||onde_rec.onde_id );
          end if;
          if ( onde_rec_o.print_datum_extra_macht is not null )
          then
            kgc_brie_00.registreer
            ( p_decl_id => decl_rec.decl_id
            , p_kafd_id => c_kafd_id
            , p_pers_id => onde_rec.pers_id
            , p_onde_id => onde_rec.onde_id
            , p_brieftype => 'MACHTIGING'
            , p_datum => conv_datum( onde_rec_o.print_datum_extra_macht )
            , p_kopie => 'J'
            , p_geadresseerde => nvl( onde_rec.nota_adres, '.' ) -- kgc_adres_00.verzekeraar( decl_rec.verz_id, 'J' ) is NIET gevuld!!!
            );
            v_nr_brie := v_nr_brie+1;
          end if;

          -- monsters
          if ( onde_rec_o.materiaal_temp is not null )
          then
            mons_rec := mons_rec_init;
            mons_rec.kafd_id := onde_rec.kafd_id;
            mons_rec.ongr_id := onde_rec.ongr_id;
            mons_rec.pers_id := onde_rec.pers_id;
            mons_rec.foet_id := onde_rec.foet_id;
            mons_rec.monsternummer := onde_rec.onderzoeknr;
            mons_rec.mate_id := kgc_uk2pk.mate_id( c_kafd_id, onde_rec_o.materiaal_temp );
            mons_rec.datum_aanmelding := onde_rec.datum_binnen;
            mons_rec.medicatie := onde_rec_o.medicatie;

            begin
              select kgc_mons_seq.nextval
              into   mons_rec.mons_id
              from   dual;
              insert into kgc_monsters
              ( mons_id
              , monsternummer
              , ongr_id
              , kafd_id
              , pers_id
              , foet_id
              , mate_id
              , declareren
              , datum_aanmelding
              , medicatie
              , commentaar
              , created_by
              , creation_date
              , last_updated_by
              , last_update_date
              )
              values
              ( mons_rec.mons_id
              , mons_rec.monsternummer
              , mons_rec.ongr_id
              , mons_rec.kafd_id
              , mons_rec.pers_id
              , mons_rec.foet_id
              , mons_rec.mate_id
              , 'N'
              , mons_rec.datum_aanmelding
              , mons_rec.medicatie
              , mons_rec.commentaar
              , 'CONVERSIE'
              , SYSDATE
              , 'CONVERSIE'
              , SYSDATE
              );
              insert into kgc_onderzoek_monsters
              ( onde_id
              , mons_id
              , created_by
              , creation_date
              , last_updated_by
              , last_update_date
              )
              values
              ( onde_rec.onde_id
              , mons_rec.mons_id
              , 'CONVERSIE'
              , SYSDATE
              , 'CONVERSIE'
              , SYSDATE
              );
debug( to_char(sysdate,'hh24:mi:ss')||' insert mons='||mons_rec.mons_id );
              v_nr_mons := v_nr_mons+1;
            EXCEPTION
              WHEN DUP_VAL_ON_INDEX THEN
		        NULL;
              WHEN OTHERS THEN
                p.l( 'MONSTER '||mons_rec.monsternummer||' ('||onde_rec_o.odbnr||')' );
                p.l( SQLERRM );
            end;
          end if;
          -- uitslag
          if ( onde_rec_o.uitslag is not null )
          then
            uits_rec.onde_id := onde_rec.onde_id;
            uits_rec.kafd_id := c_kafd_id;
            uits_rec.rela_id := onde_rec.rela_id;
            uits_rec.brty_id := v_brty_id_e;
            uits_rec.mede_id := onde_rec.mede_id_autorisator;
            uits_rec.datum_autorisatie := onde_rec.datum_autorisatie;
            uits_rec.datum_uitslag := uits_rec.datum_autorisatie;
            if ( onde_rec_o.aanvr_naam is not null )
            then
              select onde_rec_o.aanvr_naam
                  || decode( onde_rec_o.aanvr_specialisme,null,null,chr(10)|| onde_rec_o.aanvr_specialisme)
                  || decode( onde_rec_o.aanvr_afdeling,null,null,chr(10)|| onde_rec_o.aanvr_afdeling)
                  || decode( onde_rec_o.aanvr_straat_1,null,null,chr(10)|| onde_rec_o.aanvr_straat_1)
                  || decode( onde_rec_o.aanvr_straat_2,null,null,chr(10)|| onde_rec_o.aanvr_straat_2)
                  || decode( onde_rec_o.aanvr_straat_3,null,null,chr(10)|| onde_rec_o.aanvr_straat_3)
                  || decode( onde_rec_o.aanvr_straat_4,null,null,chr(10)|| onde_rec_o.aanvr_straat_4)
                  || decode( onde_rec_o.aanvr_woonplaats,null,null,chr(10)|| onde_rec_o.aanvr_postcd||' '||onde_rec_o.aanvr_woonplaats)
                  || decode( onde_rec_o.aanvr_land,null,null,chr(10)|| onde_rec_o.aanvr_land)
              into   uits_rec.volledig_adres
              from   dual;
            else
              uits_rec.volledig_adres := kgc_adres_00.relatie( uits_rec.rela_id, 'J' );
            end if;
            uits_rec.tekst := onde_rec_o.uitslag;
            uits_rec.commentaar := onde_rec_o.commentaar_uitslag;
            select kgc_uits_seq.nextval
            into   uits_rec.uits_id
            from   dual;
            insert into kgc_uitslagen
            ( uits_id
            , onde_id
            , kafd_id
            , rela_id
            , brty_id
            , mede_id
            , datum_uitslag
            , datum_autorisatie
            , volledig_adres
            , tekst
            , commentaar
            )
            values
            ( uits_rec.uits_id
            , uits_rec.onde_id
            , uits_rec.kafd_id
            , uits_rec.rela_id
            , uits_rec.brty_id
            , uits_rec.mede_id
            , uits_rec.datum_uitslag
            , uits_rec.datum_autorisatie
            , uits_rec.volledig_adres
            , uits_rec.tekst
            , uits_rec.commentaar
            );
            v_nr_uits := v_nr_uits+1;
debug( to_char(sysdate,'hh24:mi:ss')||' insert uits='||onde_rec.onde_id );
            if ( onde_rec_o.datum_print_brief is not null )
            then
              -- Uitslagbrief
              kgc_brie_00.registreer
              ( p_onde_id => uits_rec.onde_id
              , p_kafd_id => c_kafd_id
              , p_pers_id => onde_rec.pers_id
              , p_uits_id => uits_rec.uits_id
              , p_rela_id => uits_rec.rela_id
              , p_brieftype => 'UITSLAG'
              , p_datum => conv_datum( onde_rec_o.datum_print_brief )
              , p_geadresseerde => uits_rec.volledig_adres
              );
              v_nr_brie := v_nr_brie+1;
            end if;
            if ( onde_rec_o.datum_print_kopie is not null )
            then
              -- Uitslagbrief
              kgc_brie_00.registreer
              ( p_onde_id => uits_rec.onde_id
              , p_kafd_id => c_kafd_id
              , p_pers_id => onde_rec.pers_id
              , p_uits_id => uits_rec.uits_id
              , p_rela_id => uits_rec.rela_id
              , p_brieftype => 'UITSLAG'
              , p_datum => conv_datum( onde_rec_o.datum_print_kopie )
              , p_kopie => 'J'
              , p_geadresseerde => uits_rec.volledig_adres
              );
              v_nr_brie := v_nr_brie+1;
            end if;
debug( to_char(sysdate,'hh24:mi:ss')||' insert uitslagbrief='||onde_rec.onde_id );
          end if;

          v_nr_onde := v_nr_onde+1;
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN
		    NULL;
          WHEN OTHERS THEN
            p.l( 'ONDERZOEK '||onde_rec.onderzoeknr||' ('||onde_rec_o.odbnr||')' );
            p.l( SQLERRM );
        END;
        commit;
debug( to_char(sysdate,'hh24:mi:ss')||' commit onde='||onde_rec.onde_id );
      END IF; -- persoon ok?
    end loop;

    p.l( 'Totaal aantal oud               : '||v_nr_tot );
    p.l( 'Aantal onderzoeken              : '||v_nr_onde );
    p.l( 'Aantal onderzoekindicaties      : '||v_nr_onin );
    p.l( 'Aantal declaraties              : '||v_nr_decl );
    p.l( 'Aantal monsters                 : '||v_nr_mons );
    p.l( 'Aantal metingen                 : '||v_nr_meti );
    p.l( 'Aantal meetwaarden              : '||v_nr_meet );
    p.l( 'Aantal uitslagen                : '||v_nr_uits );
    p.l( 'Aantal brieven                  : '||v_nr_brie );
    p.l( 'Aantal gerelateerde onderzoeken : '||v_nr_reon );

  END onderzoeken;
*/
END CONVERSIE_ENZYM;
/

/
QUIT
