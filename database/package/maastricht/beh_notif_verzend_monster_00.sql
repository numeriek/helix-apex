CREATE OR REPLACE PACKAGE "HELIX"."BEH_NOTIF_VERZEND_MONSTER_00" AS
/******************************************************************************
   NAME:       BEH_NOTIF_VERZEND_MONSTER_00
   PURPOSE: Deze package wordt wordt gebruikt om emails te versturen naar de centra waar monsters naartoe verzonden worden, bijvoorbeeld Nijmegen ontvang materiaal, deze is vooor MUMC bestemd. Deze materiaal wordt in Helix geregistreerd met de monster indicatie Verzonden naar MUMC

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8-4-2014      Z268164       1. Created this package.
******************************************************************************/

  procedure verwerk_log
  ( p_verzonden           in varchar
  , p_ontvanger_id_intern in number
  , p_ontvanger_id_extern in varchar
  , p_ontvanger_type      in varchar
  , p_adres_verzonden     in varchar
  , p_log_tekst           in varchar
  , p_entiteit_id         in number
  , p_bericht_tekst       in varchar
  );

  procedure verwerking_mail_monster;

end BEH_NOTIF_VERZEND_MONSTER_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_NOTIF_VERZEND_MONSTER_00" AS
/******************************************************************************
   NAME:       BEH_NOTIF_VERZEND_MONSTER_00
   PURPOSE: Deze package wordt wordt gebruikt om emails te versturen naar de centra waar monsters naartoe verzonden worden, bijvoorbeeld Nijmegen ontvang materiaal, deze is vooor MUMC bestemd. Deze materiaal wordt in Helix geregistreerd met de monster indicatie 'MUMC'

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8-4-2014      Z268164       1. Created this package.
******************************************************************************/

  procedure verwerk_log
  ( p_verzonden           in varchar
  , p_ontvanger_id_intern in number
  , p_ontvanger_id_extern in varchar
  , p_ontvanger_type      in varchar
  , p_adres_verzonden     in varchar
  , p_log_tekst           in varchar
  , p_entiteit_id         in number
  , p_bericht_tekst       in varchar
  )
  is
    cursor c_bety
    is
      select bety.bety_id
      from   beh_bericht_types bety
      where  bety.code = 'EMAIL'
      and    upper(bety.vervallen) = upper('N')
    ;
    --
    pl_bety_id  beh_bericht_types.bety_id%type;
    --
  begin
    open  c_bety;
    fetch c_bety
    into  pl_bety_id;
    close c_bety;
    --
    insert into beh_berichten
    ( bety_id
    , verzonden
    , datum_verzonden
    , oracle_uid
    , ontvanger_id_intern
    , ontvanger_id_extern
    , ontvanger_type
    , adres_verzonden
    , log_tekst
    , entiteit
    , entiteit_id
    , bericht_tekst
    )
    values
    ( pl_bety_id
    , p_verzonden
    , sysdate
    , sys_context('USERENV', 'SESSION_USER')
    , p_ontvanger_id_intern
    , p_ontvanger_id_extern
    , p_ontvanger_type
    , p_adres_verzonden
    , p_log_tekst
    , 'MOIN'
    , p_entiteit_id
    , p_bericht_tekst
    )
    ;
    --
  end verwerk_log;

  procedure verwerking_mail_monster
  is
    cursor c_moin
    is
      select moin.moin_id        moin_id
      ,      mons.monsternummer  monsternummer
      ,      indi.omschrijving   locatie
      ,      indi.locus          emailadres_aan
      from   kgc_monsters            mons
      ,      kgc_monster_indicaties  moin
      ,      kgc_indicatie_teksten   indi
      ,      kgc_indicatie_groepen   ingr
      where  trunc(mons.datum_aanmelding) >= to_date('17112014','ddmmyyyy') -- Om te voorkomen dat oude monsters voorzien worden van een notificatie.
      and    mons.mons_id = moin.mons_id
      and    moin.indi_id = indi.indi_id
      and    indi.ingr_id = ingr.ingr_id
      and    ingr.code = 'VERSTUURD'
      and    moin.moin_id not in
               ( select beri.entiteit_id
                 from   beh_berichten beri
                 where  beri.entiteit = 'MOIN'
               )
      and    indi.locus is not null
    ;
    --
    r_moin_rec          c_moin%ROWTYPE;
    pl_email_onderwerp  varchar2(512);
    pl_email_tekst      varchar2(2000);
    pl_verzonden        varchar2(10)   := null;
    --
  begin
    for r_moin_rec in c_moin
    loop
      if kgc_sypa_00.systeem_waarde('DATABASE_SIDNAAM') != 'PHELIX'
      then
        pl_email_onderwerp := 'DIT IS EEN TEST! Monster met nummer: ' || r_moin_rec.monsternummer ||' - wordt/is verzonden naar uw locatie';
      else
        pl_email_onderwerp := 'Monster met nummer: ' || r_moin_rec.monsternummer ||' - wordt/is verzonden naar uw locatie';
      end if;
      --
      pl_email_tekst := '<span style="font-family:arial;font-size:10pt;color:black">Geachte heer/mevrouw,<br><br>'||
                        'Monster met nummer: ' || r_moin_rec.monsternummer ||' - wordt/is verzonden naar uw locatie: ' || r_moin_rec.locatie || '.<br><br>'||
                        'Indien u vragen heeft, dan kunt u altijd contact met ons opnemen.<br><br>'||
                        'Met vriendelijke groeten,<br>'||
                        'Laboratorium Clinical Genomics<br>'||
                        'Academisch Ziekenhuis Maastricht<br>'||
                        'tel: 043-3875843<br>'||
                        kgc_sypa_00.systeem_waarde('ENOT_VERZ_MONS_EMAILADRES_VAN')||'</span>';
      --
      mss_mail.post_mail( kgc_sypa_00.systeem_waarde('ENOT_VERZ_MONS_EMAILADRES_VAN')
                        , r_moin_rec.emailadres_aan
                        , pl_email_onderwerp
                        , pl_email_tekst
                        , TRUE -- HTML
                        );
      --
      pl_verzonden := 'J';
      --
      verwerk_log( pl_verzonden
                 , NULL
                 , NULL
                 , 'NULL'
                 , r_moin_rec.emailadres_aan
                 , pl_email_onderwerp
                 , r_moin_rec.moin_id
                 , pl_email_tekst
                 );
    end loop;
    --
  end verwerking_mail_monster;

END BEH_NOTIF_VERZEND_MONSTER_00;
/

/
QUIT
