CREATE OR REPLACE PACKAGE "HELIX"."CWEB_API" IS
-- dummy package; te gebruiken tot dat remote package met dezelfde naam beschikbaar is
-- dan: create synonym cweb_api for cweb_api@sqllims;
--==============================================================================
-- Versie dd 05-03-2006 M.Herrijgers, ABS

--   Description:	API for WEB-Plate logging functionality
--
--   History:
--
--  Version Date        Name                 Reason
--  ------- ----------- -------------------- -------------------------------
--  1.00    16-Aug-2001 S. Ferket            Created
--          09-Oct-2002 S. Ferket            Added AddPlateAttribute procedure
--          16-Oct-2002 S. Ferket            Overloaded procedures
--                                           InsertSequencePlate and
--                                           InsertGenescanPlate
--  2.00    25-Feb-2006 M. Herrijgers        Revision of database package
--                                           Removal of obsolete procedures
--                                           Added InsertPlate
--  2.10    05-Mar-2006 M. Herrijgers        Re-introduction insertsample
--                                           procedure
--          06 Maaart 2006 E.Soeteman        Wijziging in AddPlateAttribute
--                                           pPlate_id gewijzigd in Number, %type
--                                           is niet bruikbaar voor deze dummy.
--==============================================================================

  procedure GetPlateID( pPlate_id   out  number
                      , pStatus     out  number   ) ;

  procedure InsertPlate( pPlate_id        in out number
                       , pColumn#         in     number
                       , pRow#            in     number
                       , pProject         in     varchar2
                       , pStatus          out    number
                       , pUser            in     varchar2 ) ;

  procedure InsertSample( pSample_name           in     varchar2
                        , pSample_comment        in     varchar2
                        , pPlate_id              in     number
                        , pColumn#               in     varchar2
                        , pRow#                  in     varchar2
                        , pStatus                out    number
                        , pColor_info1           in     varchar2
                        , pcolor_comment1        in     varchar2
                        , pColor_info2           in     varchar2
                        , pcolor_comment2        in     varchar2
                        , pColor_info3           in     varchar2
                        , pcolor_comment3        in     varchar2
                        , pColor_info4           in     varchar2
                        , pcolor_comment4        in     varchar2
                        , pColor_info5           in     varchar2
                        , pcolor_comment5        in     varchar2
                        , pUser                  in     varchar2 ) ;

  procedure MakePlateAvailable( pPlate_id        in     number
                              , pStatus          out    number  ) ;

  procedure ErrorText( pCode                     in     number
                     , pMessage                  out    varchar2
                     , pStatus                   out    number  ) ;

  procedure AddPlateAttribute(
                         pPlate_id             number -- cwebs_plates.plate_id%type
                       , pSequence             number
                       , pName                 varchar2
                       , pText_value           varchar2
                       , pStatus          out    number  ) ;
END CWEB_API;
/

/
QUIT
