CREATE OR REPLACE PACKAGE "HELIX"."ZIP_UTIL_PKG"
IS
   /*
   Purpose:      Package handles zipping and unzipping of files
   Remarks:      by Anton Scheffer, see http://forums.oracle.com/forums/thread.jspa?messageID=9289744#9289744
                 for unzipping, see http://technology.amis.nl/blog/8090/parsing-a-microsoft-word-docx-and-unzip-zipfiles-with-plsql
                 for zipping, see http://forums.oracle.com/forums/thread.jspa?threadID=1115748=0
   Who     Date        Description
   ------  ----------  --------------------------------
   MBR     09.01.2011  Created
   */
   TYPE t_file_list IS TABLE OF CLOB;
   --
   FUNCTION get_file_list(p_dir       IN VARCHAR2
                         ,p_zip_file  IN VARCHAR2
                         ,p_encoding  IN VARCHAR2 := NULL)
      RETURN t_file_list;
   --
   FUNCTION get_file_list(p_zipped_blob  IN BLOB
                         ,p_encoding     IN VARCHAR2 := NULL /* Use CP850 for zip files created with a German Winzip to see umlauts, etc */
                                                            )
      RETURN t_file_list;
   --
   FUNCTION get_file(p_dir        IN VARCHAR2
                    ,p_zip_file   IN VARCHAR2
                    ,p_file_name  IN VARCHAR2
                    ,p_encoding   IN VARCHAR2 := NULL)
      RETURN BLOB;
   --
   FUNCTION get_file(p_zipped_blob  IN BLOB
                    ,p_file_name    IN VARCHAR2
                    ,p_encoding     IN VARCHAR2 := NULL)
      RETURN BLOB;
   --
   PROCEDURE add_file(p_zipped_blob  IN OUT BLOB
                     ,p_name         IN     VARCHAR2
                     ,p_content      IN     BLOB);
   --
   PROCEDURE finish_zip(p_zipped_blob IN OUT BLOB);
   --
   PROCEDURE save_zip(p_zipped_blob  IN BLOB
                     ,p_dir          IN VARCHAR2
                     ,p_filename     IN VARCHAR2);
END zip_util_pkg;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."ZIP_UTIL_PKG"
IS
   /*
   Purpose:      Package handles zipping and unzipping of files
   Remarks:      by Anton Scheffer, see http://forums.oracle.com/forums/thread.jspa?messageID=9289744#9289744
                 for unzipping, see http://technology.amis.nl/blog/8090/parsing-a-microsoft-word-docx-and-unzip-zipfiles-with-plsql
                 for zipping, see http://forums.oracle.com/forums/thread.jspa?threadID=1115748=0
   Who     Date        Description
   ------  ----------  --------------------------------
   MBR     09.01.2011  Created
   MBR     21.05.2012  Fixed a bug related to use of dbms_lob.substr in get_file (use dbms_lob.copy instead)
   */
   FUNCTION raw2num(p_value IN RAW)
      RETURN NUMBER
   IS
   BEGIN -- note: FFFFFFFF => -1
      RETURN utl_raw.cast_to_binary_integer(p_value
                                           ,utl_raw.little_endian);
   END;
   --
   FUNCTION file2blob(p_dir        IN VARCHAR2
                     ,p_file_name  IN VARCHAR2)
      RETURN BLOB
   IS
      file_lob   BFILE;
      file_blob  BLOB;
   BEGIN
      file_lob      :=
         BFILENAME(p_dir
                  ,p_file_name);
      dbms_lob.open(file_lob
                   ,dbms_lob.file_readonly);
      dbms_lob.createtemporary(file_blob
                              ,TRUE);
      dbms_lob.loadfromfile(file_blob
                           ,file_lob
                           ,dbms_lob.lobmaxsize);
      dbms_lob.close(file_lob);
      RETURN file_blob;
   EXCEPTION
      WHEN OTHERS
      THEN
         IF dbms_lob.ISOPEN(file_lob) = 1
         THEN
            dbms_lob.close(file_lob);
         END IF;
         IF dbms_lob.istemporary(file_blob) = 1
         THEN
            dbms_lob.freetemporary(file_blob);
         END IF;
         RAISE;
   END;
   --
   FUNCTION raw2varchar2(p_raw       IN RAW
                        ,p_encoding  IN VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN NVL(utl_i18n.raw_to_char(p_raw
                                     ,p_encoding)
                ,utl_i18n.raw_to_char(p_raw
                                     ,utl_i18n.map_charset(
                                         p_encoding
                                        ,utl_i18n.generic_context
                                        ,utl_i18n.iana_to_oracle)));
   END;
   FUNCTION get_file_list(p_dir       IN VARCHAR2
                         ,p_zip_file  IN VARCHAR2
                         ,p_encoding  IN VARCHAR2 := NULL)
      RETURN t_file_list
   IS
   BEGIN
      RETURN get_file_list(file2blob(p_dir
                                    ,p_zip_file)
                          ,p_encoding);
   END;
   --
   FUNCTION get_file_list(p_zipped_blob  IN BLOB
                         ,p_encoding     IN VARCHAR2 := NULL)
      RETURN t_file_list
   IS
      t_ind     INTEGER;
      t_hd_ind  INTEGER;
      t_rv      t_file_list;
   BEGIN
      t_ind   := dbms_lob.getlength(p_zipped_blob) - 21;
      LOOP
         EXIT WHEN dbms_lob.SUBSTR(p_zipped_blob
                                  ,4
                                  ,t_ind) = HEXTORAW('504B0506')
                OR t_ind < 1;
         t_ind   := t_ind - 1;
      END LOOP;
      --
      IF t_ind <= 0
      THEN
         RETURN NULL;
      END IF;
      --
      t_hd_ind      :=
           raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                  ,4
                                  ,t_ind + 16))
         + 1;
      t_rv    := t_file_list();
      t_rv.EXTEND(raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                         ,2
                                         ,t_ind + 10)));
      FOR i IN 1 ..
               raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                      ,2
                                      ,t_ind + 8))
      LOOP
         t_rv(i)      :=
            raw2varchar2(dbms_lob.SUBSTR(
                            p_zipped_blob
                           ,raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                                   ,2
                                                   ,t_hd_ind + 28))
                           ,t_hd_ind + 46)
                        ,p_encoding);
         t_hd_ind      :=
              t_hd_ind
            + 46
            + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                     ,2
                                     ,t_hd_ind + 28))
            + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                     ,2
                                     ,t_hd_ind + 30))
            + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                     ,2
                                     ,t_hd_ind + 32));
      END LOOP;
      --
      RETURN t_rv;
   END;
   --
   FUNCTION get_file(p_dir        IN VARCHAR2
                    ,p_zip_file   IN VARCHAR2
                    ,p_file_name  IN VARCHAR2
                    ,p_encoding   IN VARCHAR2 := NULL)
      RETURN BLOB
   IS
   BEGIN
      RETURN get_file(file2blob(p_dir
                               ,p_zip_file)
                     ,p_file_name
                     ,p_encoding);
   END;
   --
   FUNCTION get_file(p_zipped_blob  IN BLOB
                    ,p_file_name    IN VARCHAR2
                    ,p_encoding     IN VARCHAR2 := NULL)
      RETURN BLOB
   IS
      t_tmp     BLOB;
      t_ind     INTEGER;
      t_hd_ind  INTEGER;
      t_fl_ind  INTEGER;
   BEGIN
      t_ind   := dbms_lob.getlength(p_zipped_blob) - 21;
      LOOP
         EXIT WHEN dbms_lob.SUBSTR(p_zipped_blob
                                  ,4
                                  ,t_ind) = HEXTORAW('504B0506')
                OR t_ind < 1;
         t_ind   := t_ind - 1;
      END LOOP;
      --
      IF t_ind <= 0
      THEN
         RETURN NULL;
      END IF;
      --
      t_hd_ind      :=
           raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                  ,4
                                  ,t_ind + 16))
         + 1;
      FOR i IN 1 ..
               raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                      ,2
                                      ,t_ind + 8))
      LOOP
         IF p_file_name = raw2varchar2(dbms_lob.SUBSTR(
                                          p_zipped_blob
                                         ,raw2num(dbms_lob.SUBSTR(
                                                     p_zipped_blob
                                                    ,2
                                                    ,t_hd_ind + 28))
                                         ,t_hd_ind + 46)
                                      ,p_encoding)
         THEN
            IF dbms_lob.SUBSTR(p_zipped_blob
                              ,2
                              ,t_hd_ind + 10) = HEXTORAW('0800') -- deflate
            THEN
               t_fl_ind      :=
                  raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                         ,4
                                         ,t_hd_ind + 42));
               t_tmp   := HEXTORAW('1F8B0800000000000003'); -- gzip header
               dbms_lob.COPY(t_tmp
                            ,p_zipped_blob
                            ,raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                                    ,4
                                                    ,t_fl_ind + 19))
                            ,11
                            ,  t_fl_ind
                             + 31
                             + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                                      ,2
                                                      ,t_fl_ind + 27))
                             + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                                      ,2
                                                      ,t_fl_ind + 29)));
               dbms_lob.append(t_tmp
                              ,dbms_lob.SUBSTR(p_zipped_blob
                                              ,4
                                              ,t_fl_ind + 15));
               dbms_lob.append(t_tmp
                              ,dbms_lob.SUBSTR(p_zipped_blob
                                              ,4
                                              ,t_fl_ind + 23));
               RETURN utl_compress.lz_uncompress(t_tmp);
            END IF;
            --
            IF dbms_lob.SUBSTR(p_zipped_blob
                              ,2
                              ,t_hd_ind + 10) = HEXTORAW('0000')
            -- The file is stored (no compression)
            THEN
               t_fl_ind      :=
                  raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                         ,4
                                         ,t_hd_ind + 42));
               dbms_lob.createtemporary(t_tmp
                                       ,cache => TRUE);
               dbms_lob.COPY(dest_lob => t_tmp
                            ,src_lob => p_zipped_blob
                            ,amount => raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                                              ,4
                                                              ,t_fl_ind + 19))
                            ,dest_offset => 1
                            ,src_offset =>   t_fl_ind
                                           + 31
                                           + raw2num(dbms_lob.SUBSTR(
                                                        p_zipped_blob
                                                       ,2
                                                       ,t_fl_ind + 27))
                                           + raw2num(dbms_lob.SUBSTR(
                                                        p_zipped_blob
                                                       ,2
                                                       ,t_fl_ind + 29)));
               RETURN t_tmp;
            END IF;
         END IF;
         t_hd_ind      :=
              t_hd_ind
            + 46
            + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                     ,2
                                     ,t_hd_ind + 28))
            + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                     ,2
                                     ,t_hd_ind + 30))
            + raw2num(dbms_lob.SUBSTR(p_zipped_blob
                                     ,2
                                     ,t_hd_ind + 32));
      END LOOP;
      --
      RETURN NULL;
   END;
   --
   FUNCTION little_endian(p_big    IN NUMBER
                         ,p_bytes  IN PLS_INTEGER := 4)
      RETURN RAW
   IS
   BEGIN
      RETURN utl_raw.SUBSTR(utl_raw.cast_from_binary_integer(
                               p_big
                              ,utl_raw.little_endian)
                           ,1
                           ,p_bytes);
   END;
   --
   PROCEDURE add_file(p_zipped_blob  IN OUT BLOB
                     ,p_name         IN     VARCHAR2
                     ,p_content      IN     BLOB)
   IS
      t_now   DATE;
      t_blob  BLOB;
      t_clen  INTEGER;
   BEGIN
      t_now    := SYSDATE;
      t_blob   := utl_compress.lz_compress(p_content);
      t_clen   := dbms_lob.getlength(t_blob);
      IF p_zipped_blob IS NULL
      THEN
         dbms_lob.createtemporary(p_zipped_blob
                                 ,TRUE);
      END IF;
      dbms_lob.append(p_zipped_blob
                     ,utl_raw.CONCAT(
                         HEXTORAW('504B0304') -- Local file header signature
                        ,HEXTORAW('1400') -- version 2.0
                        ,HEXTORAW('0000') -- no General purpose bits
                        ,HEXTORAW('0800') -- deflate
                        ,little_endian(    TO_NUMBER(TO_CHAR(t_now
                                                            ,'ss'))
                                         / 2
                                       +   TO_NUMBER(TO_CHAR(t_now
                                                            ,'mi'))
                                         * 32
                                       +   TO_NUMBER(TO_CHAR(t_now
                                                            ,'hh24'))
                                         * 2048
                                      ,2) -- File last modification time
                        ,little_endian(  TO_NUMBER(TO_CHAR(t_now
                                                          ,'dd'))
                                       +   TO_NUMBER(TO_CHAR(t_now
                                                            ,'mm'))
                                         * 32
                                       +   (  TO_NUMBER(TO_CHAR(t_now
                                                               ,'yyyy'))
                                            - 1980)
                                         * 512
                                      ,2) -- File last modification date
                        ,dbms_lob.SUBSTR(t_blob
                                        ,4
                                        ,t_clen - 7) -- CRC-32
                        ,little_endian(t_clen - 18) -- compressed size
                        ,little_endian(dbms_lob.getlength(p_content))
                        -- uncompressed size
                        ,little_endian(LENGTH(p_name)
                                      ,2) -- File name length
                        ,HEXTORAW('0000') -- Extra field length
                        ,utl_raw.cast_to_raw(p_name) -- File name
                                                    ));
      dbms_lob.COPY(p_zipped_blob
                   ,t_blob
                   ,t_clen - 18
                   ,dbms_lob.getlength(p_zipped_blob) + 1
                   ,11); -- compressed content
      dbms_lob.freetemporary(t_blob);
   END;
   --
   PROCEDURE finish_zip(p_zipped_blob IN OUT BLOB)
   IS
      t_cnt              PLS_INTEGER := 0;
      t_offs             INTEGER;
      t_offs_dir_header  INTEGER;
      t_offs_end_header  INTEGER;
      t_comment          RAW(32767)
         := utl_raw.cast_to_raw('Implementation by Anton Scheffer');
   BEGIN
      t_offs_dir_header   := dbms_lob.getlength(p_zipped_blob);
      t_offs              :=
         dbms_lob.INSTR(p_zipped_blob
                       ,HEXTORAW('504B0304')
                       ,1);
      WHILE t_offs > 0
      LOOP
         t_cnt   := t_cnt + 1;
         dbms_lob.append(p_zipped_blob
                        ,utl_raw.CONCAT(HEXTORAW('504B0102')
                                       -- Central directory file header signature
                                       ,HEXTORAW('1400') -- version 2.0
                                       ,dbms_lob.SUBSTR(p_zipped_blob
                                                       ,26
                                                       ,t_offs + 4)
                                       ,HEXTORAW('0000') -- File comment length
                                       ,HEXTORAW('0000') -- Disk number where file starts
                                       ,HEXTORAW('0100') -- Internal file attributes
                                       ,HEXTORAW('2000B681') -- External file attributes
                                       ,little_endian(t_offs - 1)
                                       -- Relative offset of local file header
                                       ,dbms_lob.SUBSTR(p_zipped_blob
                                                       ,utl_raw.cast_to_binary_integer(
                                                           dbms_lob.SUBSTR(
                                                              p_zipped_blob
                                                             ,2
                                                             ,t_offs + 26)
                                                          ,utl_raw.little_endian)
                                                       ,t_offs + 30) -- File name
                                                                    ));
         t_offs      :=
            dbms_lob.INSTR(p_zipped_blob
                          ,HEXTORAW('504B0304')
                          ,t_offs + 32);
      END LOOP;
      t_offs_end_header   := dbms_lob.getlength(p_zipped_blob);
      dbms_lob.append(p_zipped_blob
                     ,utl_raw.CONCAT(
                         HEXTORAW('504B0506') -- End of central directory signature
                        ,HEXTORAW('0000') -- Number of this disk
                        ,HEXTORAW('0000') -- Disk where central directory starts
                        ,little_endian(t_cnt
                                      ,2) -- Number of central directory records on this disk
                        ,little_endian(t_cnt
                                      ,2) -- Total number of central directory records
                        ,little_endian(t_offs_end_header - t_offs_dir_header)
                        -- Size of central directory
                        ,little_endian(t_offs_dir_header)
                        -- Relative offset of local file header
                        ,little_endian(NVL(utl_raw.LENGTH(t_comment), 0)
                                      ,2) -- ZIP file comment length
                        ,t_comment));
   END;
   --
   PROCEDURE save_zip(p_zipped_blob  IN BLOB
                     ,p_dir          IN VARCHAR2
                     ,p_filename     IN VARCHAR2)
   IS
      t_fh   utl_file.file_type;
      t_len  PLS_INTEGER := 32767;
   BEGIN
      t_fh      :=
         utl_file.fopen(p_dir
                       ,p_filename
                       ,'wb');
      FOR i IN 0 .. TRUNC((dbms_lob.getlength(p_zipped_blob) - 1) / t_len)
      LOOP
         utl_file.put_raw(t_fh
                         ,dbms_lob.SUBSTR(p_zipped_blob
                                         ,t_len
                                         ,i * t_len + 1));
      END LOOP;
      utl_file.fclose(t_fh);
   END;
--
END zip_util_pkg;
/

/
QUIT
