CREATE OR REPLACE PACKAGE "HELIX"."CONV_CYTO" AS
  PROCEDURE vul_data_onderz;
  PROCEDURE vul_data_zwan;
END conv_cyto;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."CONV_CYTO"
AS
   /* @@2DO:
      (-) resultaat --> meetwaarde ook invullen in bas_meetwaarden, dus niet
                        alleen bij bas_meetwaarde_details --> gedaan voor
                        KARY-PO en KARY-PR
      (-) mate_srt niet nodig??
      (-) add_brief --> id's zijn null
      - FISH
        - kweeksoort --> wordt niet ingevuld in het lab (wel door autorisator?? --> Nee)
        - opmerking  --> staat al bij meting
      - MLPA
        - opmerking  --> staat al bij meting
        - doorgebeld --> staat bij reden onderzoek
      - Werklijst
      - instelling (instantie) --> niet in VAX
   */
   CURSOR onde_cur
   IS
      SELECT   onderznr, afdeling, persnr, jaar
          FROM cyto_pers_onderz po
         WHERE onderznr NOT LIKE 'A50%'                 -- afname-registratie
           AND onderznr NOT LIKE 'C50%'                       -- retrospectief
           AND afdeling IN ('HAEM', 'CYTO', 'CYTOV', 'PREC', 'HAEMV')
           AND NOT EXISTS (SELECT 1
                             FROM cyto_onderzoek
                            WHERE odbnr = po.odbnr)
           AND NOT EXISTS (SELECT 1
                             FROM cyto_haem_onderzoek
                            WHERE odbnr = po.odbnr)
           AND NOT EXISTS (SELECT 1
                             FROM cyto_prec_onderzoek
                            WHERE odbnr = po.odbnr)
      ORDER BY onderznr;

   CURSOR onde_cur_o
   IS
      SELECT   *
          /*bron, foet_id, odbnr, datum_afname, binnendat, relatie, spoed, opmerking,
                   indicatie1, indicatie2, indicatie3, karyotype, onde_afgerond,
                   datum_afgerond, kopiehouder1, kopiehouder2, kopiehouder3,
                   kopiehouder4, toelichting1, toelichting2, toelichting3, toelichting4,
                   toelichting5, toelichting6, toelichting7, toelichting8, toelichting9,
                   toelichting10, toelichting11, toelichting12, uitslagdatum,
                   autorisator, mate_code, tel_uitslag, afwijkend, tel_contactp,
                   analyse_door, declaratie, onderzoeksreden, afdeling, onderznr,
                   persnr, afgerond, indicatie, afp, afpcd, afp_door, afpdat, achecd,
                   ache_door, achedat, odbnre, pkweek, kweek_ingezet, kweek_door,
                   hoevmat, helderhcd, hoevsed, aspectsed, brief_verstuurd, geslopenb,
                   afronddat, machtind, afndoor, afnplaats, typeafn, aantpog, typenld,
                   antid, aantdrpl, bijz, versldat, creator, machtgrp, geboortedatum*/
      FROM     (SELECT 'PREC' bron, foet.foet_id foet_id,
                       prec_onderz.odbnr odbnr,

                       -- kgc_onderzoeken.onde_id
                       prec_onderz.afndat datum_afname,

                       -- kgc_monsters.datum_afname
                       prec_onderz.binnendat binnendat,

                       -- kgc_onderzoeken.datum_binnen
                       prec_onderz.anvrgr_titel relatie,
                                                        -- kgc_onderzoeken.rela_id
                                                        TO_CHAR (NULL) spoed,
                       prec_onderz.opm_1 opmerking,

                       -- bas_metingen.opmerking_algemeen
                       prec_onderz.indcd_1 indicatie1,

                       -- kgc_indicaties.code
                       prec_onderz.indcd_2 indicatie2,

                       -- kgc_indicaties.code
                       prec_onderz.indcd_3 indicatie3,

                       -- kgc_indicaties.code
                       prec_onderz.karyotype karyotype,

                       -- bas_meetwaarden.meetwaarde
                       prec_onderz.afgerond onde_afgerond,

                       -- kgc_onderzoeken.afgerond
                       NULL datum_afgerond,
                                           -- bas_meetwaarden.datum_afgerond
                                           prec_onderz.copy_1 kopiehouder1,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       prec_onderz.copy_2 kopiehouder2,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       prec_onderz.copy_3 kopiehouder3,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       prec_onderz.copy_4 kopiehouder4,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       prec_onderz.toelicht_1 toelichting1,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_2 toelichting2,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_3 toelichting3,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_4 toelichting4,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_5 toelichting5,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_6 toelichting6,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_7 toelichting7,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_8 toelichting8,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_9 toelichting9,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_10 toelichting10,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_11 toelichting11,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.toelicht_12 toelichting12,

                       -- kgc_uitslagen.toelichting
                       prec_onderz.uitslagdat uitslagdatum,

                       -- kgc_uitslagen.datum_uitslag
                       prec_onderz.autorisator autorisator,

                       -- stc025.code , kgc_onderzoeken.mede_id_autorisator
                       prec_onderz.amatcd mate_code,

                       -- stc004.code , kgc_monsters.mate_id
                       TO_CHAR (NULL) tel_uitslag,
                       prec_onderz.afwijkend afwijkend,

                       -- kgc_uitslagen.tekst
                       TO_CHAR (NULL) tel_contactp,
                       prec_onderz.anal_door analyse_door,

                       -- bas_metingen.mede_id
                       prec_onderz.decl declaratie,

                       -- kgc_onderzoeken.declareren  J/N
                       prec_onderz.indicatie onderzoeksreden,

                       -- kgc_onderzoeken.onre_id, kgc_onderzoekdredenen.omschrijving
                       pers_onderz.afdeling afdeling,

                       -- kgc_onderzoeken.kafd_id, kgc_onderzoeken.ongr_id (afhankelijk onderzoeksnummer)
                       pers_onderz.onderznr onderznr,
                       pers.pers_id persnr       --, pers_onderz.persnr persnr
                                          ,
                       pers_onderz.afgerond afgerond,
                       pers_onderz.indicatie indicatie, prec_onderz.afp afp,

                       -- bas_meetwaarden.meetwaarde
                       prec_onderz.afpcd afpcd,
                                               -- bas_meetwaarden.commentaar
                                               prec_onderz.afp_door afp_door,

                       -- bas_meetwaarden.mede_id
                       prec_onderz.afpdat afpdat,
                                                 -- bas_meetwaarden.datum_meting
                                                 prec_onderz.achecd achecd,

                       -- bas_meetwaarden.commentaar
                       prec_onderz.ache_door ache_door,

                       -- bas_meetwaarden.mede_id
                       prec_onderz.achedat achedat,
                                                   -- bas_meetwaarden.datum_meting
                                                   prec_onderz.odbnre odbnre,
                       prec_onderz.pkweek pkweek,
                       prec_onderz.kweek_ingezet kweek_ingezet,
                       prec_onderz.kweek_door kweek_door,
                       prec_onderz.hoevmat hoevmat,
                       prec_onderz.helderhcd helderhcd,
                       prec_onderz.hoevsed hoevsed,
                       prec_onderz.aspectsed aspectsed,
                       prec_onderz.brief_verstuurd brief_verstuurd,
                       prec_onderz.geslopenb geslopenb,
                       prec_onderz.afronddat afronddat,
                       prec_onderz.machtind machtind,
                       prec_onderz.afndoor afndoor,
                       prec_onderz.afnplaats afnplaats,
                       prec_onderz.typeafn typeafn,
                       prec_onderz.aantpog aantpog,
                       prec_onderz.typenld typenld, prec_onderz.antid antid,
                       prec_onderz.aantdrpl aantdrpl, prec_onderz.bijz bijz,
                       prec_onderz.versldat versldat,
                       prec_onderz.creator creator,
                       prec_onderz.machtgrp machtgrp,
                       pers.geboortedatum geboortedatum,
                       UPPER (pers.land) land
                  -- zowel Nederland als NEDERLAND (niet NL)
                FROM   kgc_personen pers,
                       kgc_foetussen foet,
                       cyto_pers_onderz pers_onderz,
                       cyto_prec_onderzoek prec_onderz,
                       cyto_foet_pers_conv fpc
                 WHERE pers_onderz.odbnr = prec_onderz.odbnr
                   AND fpc.odbnr = pers_onderz.odbnr
                   AND foet.foet_id = fpc.foet_id_helix
                   AND pers.pers_id = foet.pers_id_moeder
                   AND NOT EXISTS (
                                 SELECT 1
                                   FROM kgc_onderzoeken onde
                                  WHERE onde.onderzoeknr =
                                                          pers_onderz.onderznr)
                   AND pers_onderz.onderznr NOT LIKE 'A50%'
                   -- afname-registratie
                   AND pers_onderz.onderznr NOT LIKE 'C50%'   -- retrospectief
                   AND pers_onderz.afdeling IN
                                   ('HAEM', 'CYTO', 'CYTOV', 'PREC', 'HAEMV')
                UNION ALL
                SELECT 'HAEM' bron, NULL foet_id, haem_onderz.odbnr odbnr,

                       -- kgc_onderzoeken.onde_id
                       haem_onderz.aanmelddat datum_afname,

                       -- aanmelddat
                       haem_onderz.binnendat binnendat,

                       -- kgc_onderzoeken.datum_binnen
                       haem_onderz.anvrgr_titel relatie,
                                                        -- kgc_onderzoeken.rela_id
                       NULL spoed, haem_onderz.opm opmerking,

                       -- bas_metingen.opmerking_algemeen
                       haem_onderz.indcd_1 indicatie1,

                       -- kgc_indicaties.code
                       haem_onderz.indcd_2 indicatie2,

                       -- kgc_indicaties.code
                       haem_onderz.indcd_3 indicatie3,

                       -- kgc_indicaties.code
                       haem_onderz.karyotype karyotype,

                       -- bas_meetwaarden.meetwaarde
                       haem_onderz.afgerond onde_afgerond,

                       -- kgc_onderzoeken.afgerond
                       haem_onderz.afronddat datum_afgerond,

                       -- bas_meetwaarden.datum_afgerond
                       haem_onderz.copy_1 kopiehouder1,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       haem_onderz.copy_2 kopiehouder2,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       haem_onderz.copy_3 kopiehouder3,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       haem_onderz.copy_4 kopiehouder4,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       haem_onderz.toelicht_1 toelichting1,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_2 toelichting2,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_3 toelichting3,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_4 toelichting4,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_5 toelichting5,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_6 toelichting6,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_7 toelichting7,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_8 toelichting8,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_9 toelichting9,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_10 toelichting10,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_11 toelichting11,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.toelicht_12 toelichting12,

                       -- kgc_uitslagen.toelichting
                       haem_onderz.uitslagdat uitslagdatum,

                       -- kgc_uitslagen.datum_uitslag
                       haem_onderz.autorisator autorisator,

                       -- stc025.code , kgc_onderzoeken.mede_id_autorisator
                       haem_onderz.amatcdrow mate_code,
                                                       -- stc004.code , kgc_monsters.mate_id
                       NULL tel_uitslag, haem_onderz.afwijkend afwijkend,

                       -- kgc_uitslagen.tekst
                       NULL tel_contactp, haem_onderz.anal_door analyse_door,

                       -- bas_metingen.mede_id
                       haem_onderz.decl declaratie,

                       -- kgc_onderzoeken.declareren  J/N
                       haem_onderz.indicatie onderzoeksreden,

                       -- kgc_onderzoeken.onre_id, kgc_onderzoekdredenen.omschrijving
                       pers_onderz.afdeling afdeling,

                       -- kgc_onderzoeken.kafd_id, kgc_onderzoeken.ongr_id (afhankelijk onderzoeksnummer)
                       pers_onderz.onderznr onderznr,
                       pers_onderz.persnr persnr,
                       pers_onderz.afgerond afgerond,
                       pers_onderz.indicatie indicatie,
                                                       -- NULL printdat_uitsl,
                       NULL afp,                 -- bas_meetwaarden.meetwaarde
                                NULL afpcd,      -- bas_meetwaarden.commentaar
                                           NULL afp_door,
                                                         -- bas_meetwaarden.mede_id
                       NULL afpdat,
                                   -- bas_meetwaarden.datum_meting
                       NULL achecd,
                                   -- bas_meetwaarden.commentaar
                       NULL ache_door,              -- bas_meetwaarden.mede_id
                                      NULL achedat,
                                                   -- bas_meetwaarden.datum_meting
                       NULL odbnre, NULL pkweek, NULL kweek_ingezet,
                       NULL kweek_door, NULL hoevmat, NULL helderhcd,
                       NULL hoevsed, NULL aspectsed, NULL brief_verstuurd,
                       NULL geslopenb, NULL afronddat, NULL machtind,
                       NULL afndoor, NULL afnplaats, NULL typeafn,
                       NULL aantpog, NULL typenld, NULL antid, NULL aantdrpl,
                       NULL bijz, NULL versldat, NULL creator, NULL machtgrp,
                       pers.geboortedatum geboortedatum,
                       UPPER (pers.land) land
                  -- zowel Nederland als NEDERLAND (niet NL)
                FROM   kgc_personen pers,
                       cyto_pers_onderz pers_onderz,
                       cyto_haem_onderzoek haem_onderz
                 WHERE pers_onderz.odbnr = haem_onderz.odbnr
                   AND pers_onderz.persnr = pers.pers_id
                   AND haem_onderz.amatcdrow IS NOT NULL
                   AND NOT EXISTS (
                                 SELECT 1
                                   FROM kgc_onderzoeken onde
                                  WHERE onde.onderzoeknr =
                                                          pers_onderz.onderznr)
                   AND pers_onderz.afdeling IN
                                   ('HAEM', 'CYTO', 'CYTOV', 'PREC', 'HAEMV')
                --           AND pers_onderz.afdeling IN ('HAEM', 'HAEMV')
                UNION ALL
                SELECT 'CYTO' bron, NULL foet_id, cyto_onderz.odbnr odbnr,

                       -- kgc_onderzoeken.onde_id
                       cyto_onderz.aanmelddat datum_afname,       --aanmelddat
                       cyto_onderz.binnendat binnendat,

                       -- kgc_onderzoeken.datum_binnen
                       cyto_onderz.anvrgr_titel relatie,

                       -- kgc_onderzoeken.rela_id
                       cyto_onderz.spoed spoed,
                                               -- kgc_onderzoeken.spoed
                                               cyto_onderz.opm opmerking,

                       -- bas_metingen.opmerking_algemeen
                       cyto_onderz.indcd_1 indicatie1,

                       -- kgc_indicaties.code
                       cyto_onderz.indcd_2 indicatie2,

                       -- kgc_indicaties.code
                       cyto_onderz.indcd_3 indicatie3,

                       -- kgc_indicaties.code
                       cyto_onderz.karyotype karyotype,

                       -- bas_meetwaarden.meetwaarde
                       cyto_onderz.afgerond onde_afgerond,

                       -- kgc_onderzoeken.afgerond
                       NULL datum_afgerond,
                                           -- bas_meetwaarden.datum_afgerond
                                           cyto_onderz.copy_1 kopiehouder1,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       cyto_onderz.copy_2 kopiehouder2,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       cyto_onderz.copy_3 kopiehouder3,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       cyto_onderz.copy_4 kopiehouder4,

                       -- kgc_kopiehouders.rela_id, kgc_onderzoek.onkh_id
                       cyto_onderz.toelicht_1 toelichting1,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_2 toelichting2,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_3 toelichting3,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_4 toelichting4,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_5 toelichting5,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_6 toelichting6,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_7 toelichting7,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_8 toelichting8,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_9 toelichting9,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_10 toelichting10,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_11 toelichting11,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.toelicht_12 toelichting12,

                       -- kgc_uitslagen.toelichting
                       cyto_onderz.uitslagdat uitslagdatum,

                       -- kgc_uitslagen.datum_uitslag
                       cyto_onderz.autorisator autorisator,

                       -- stc025.code , kgc_onderzoeken.mede_id_autorisator
                       cyto_onderz.amatcd mate_code,

                       -- stc004.code , kgc_monsters.mate_id
                       cyto_onderz.tel_uitslag tel_uitslag,

                       -- kgc_onderzoeken.omschrijving (toevoegen aan)
                       cyto_onderz.afwijkend afwijkend,

                       -- kgc_uitslagen.tekst
                       cyto_onderz.tel_contactp tel_contactp,

                       -- kgc_onderzoeken.omschrijving (toevoegen aan)
                       cyto_onderz.anal_door analyse_door,

                       -- bas_metingen.mede_id, bas_meetwaarden.mede_id
                       cyto_onderz.decl declaratie,

                       -- kgc_onderzoeken.declareren  J/N
                       cyto_onderz.indicatie onderzoeksreden,

                       -- kgc_onderzoeken.onre_id, kgc_onderzoekdredenen.omschrijving
                       pers_onderz.afdeling afdeling,

                       -- kgc_onderzoeken.kafd_id, kgc_onderzoeken.ongr_id (afhankelijk onderzoeksnummer)
                       pers_onderz.onderznr onderznr,
                       pers_onderz.persnr persnr,
                       pers_onderz.afgerond afgerond,
                       pers_onderz.indicatie indicatie, NULL afp,
                                                                 -- bas_meetwaarden.meetwaarde
                       NULL afpcd,
                                  -- bas_meetwaarden.commentaar
                       NULL afp_door,
                                     -- bas_meetwaarden.mede_id
                       NULL afpdat,
                                   -- bas_meetwaarden.datum_meting
                       NULL achecd,              -- bas_meetwaarden.commentaar
                                   NULL ache_door,
                                                  -- bas_meetwaarden.mede_id
                       NULL achedat,
                                    -- bas_meetwaarden.datum_meting
                       NULL odbnre, NULL pkweek, NULL kweek_ingezet,
                       NULL kweek_door, NULL hoevmat, NULL helderhcd,
                       NULL hoevsed, NULL aspectsed, NULL brief_verstuurd,
                       NULL geslopenb, NULL afronddat, NULL machtind,
                       NULL afndoor, NULL afnplaats, NULL typeafn,
                       NULL aantpog, NULL typenld, NULL antid, NULL aantdrpl,
                       NULL bijz, NULL versldat, NULL creator, NULL machtgrp,
                       pers.geboortedatum geboortedatum,
                       UPPER (pers.land) land -- zowel Nederland als NEDERLAND
                  FROM kgc_personen pers,
                       cyto_pers_onderz pers_onderz,
                       cyto_onderzoek cyto_onderz
                 WHERE pers_onderz.odbnr = cyto_onderz.odbnr
                   AND NOT EXISTS (
                                 SELECT 1
                                   FROM kgc_onderzoeken onde
                                  WHERE onde.onderzoeknr =
                                                          pers_onderz.onderznr)
                   /* sommige odbnr's (H94% t/m H99%) komen zowel in cyto_onderzoek
                      als haem_onderzoek voor */
                   AND NOT EXISTS (SELECT 1
                                     FROM cyto_dubbele_odbnrs onde
                                    WHERE onde.odbnr = pers_onderz.odbnr)
                   AND pers_onderz.persnr = pers.pers_id
                   AND pers_onderz.afdeling IN
                                   ('HAEM', 'CYTO', 'CYTOV', 'PREC', 'HAEMV')
                                                                             -- niet alleen CYTO* !!!
               )
      ORDER BY odbnr;

   c_kafd_id         kgc_kgc_afdelingen.kafd_id%TYPE
                                                 := kgc_uk2pk.kafd_id ('CYTO');
   c_user            VARCHAR2 (30)                     := 'CONVERSIE_CYTO';
   --c_brty_id         kgc_brieftypes.brty_id%TYPE
   --                                         := kgc_uk2pk.brty_id ('EINDBRIEF');
   c_rela_id_onb     NUMBER                  := kgc_uk2pk.rela_id ('ONBEKEND');
   g_onde_rec_o      onde_cur_o%ROWTYPE;
   g_onde_rec        kgc_onderzoeken%ROWTYPE;
   g_uits_rec        kgc_uitslagen%ROWTYPE;
   g_mons_rec        kgc_monsters%ROWTYPE;
   g_frac_rec        bas_fracties%ROWTYPE;
   g_meti_rec        bas_metingen%ROWTYPE;
   g_meet_id_super   bas_metingen.meti_id%TYPE;
   g_meet_id         bas_meetwaarden.meet_id%TYPE;
   g_ongr_code       VARCHAR2 (2);
   g_odbnr           NUMBER;
   g_plek            VARCHAR2 (30);
   g_kopiehouders1   NUMBER;
   g_kopiehouders2   NUMBER;
   g_kopiehouders3   NUMBER;
   g_kopiehouders4   NUMBER;
   g_meti_rec_init   bas_metingen%ROWTYPE;
   g_mons_rec_init   kgc_monsters%ROWTYPE;
   g_frac_rec_init   bas_fracties%ROWTYPE;
   g_onde_rec_init   kgc_onderzoeken%ROWTYPE;
   g_uits_rec_init   kgc_uitslagen%ROWTYPE;
   g_decl_rec_init   kgc_declaraties%ROWTYPE;

   TYPE t_afp_tekst IS TABLE OF kgc_notities.omschrijving%TYPE
      INDEX BY cyto_prec_onderzoek.bijz%TYPE;

   g_afp_tekst       t_afp_tekst;

   TYPE t_stgr_id IS TABLE OF kgc_stoftestgroepen.stgr_id%TYPE
      INDEX BY kgc_stoftestgroepen.code%TYPE;

   g_stgr_id         t_stgr_id;

   TYPE t_stof_id IS TABLE OF kgc_stoftesten.stof_id%TYPE
      INDEX BY kgc_stoftesten.code%TYPE;

   g_stof_id         t_stof_id;

   TYPE t_mwst_id IS TABLE OF kgc_meetwaardestructuur.mwst_id%TYPE
      INDEX BY kgc_meetwaardestructuur.code%TYPE;

   g_mwst_id         t_mwst_id;

   TYPE t_mede_id IS TABLE OF kgc_medewerkers.mede_id%TYPE
      INDEX BY kgc_medewerkers.code%TYPE;

   g_mede_id         t_mede_id;

   TYPE t_rela_id IS TABLE OF kgc_relaties.rela_id%TYPE
      INDEX BY kgc_relaties.code%TYPE;

   g_rela_id         t_rela_id;

   TYPE t_onui_id IS TABLE OF kgc_onderzoek_uitslagcodes.onui_id%TYPE
      INDEX BY kgc_onderzoek_uitslagcodes.code%TYPE;

   g_onui_id         t_onui_id;

   TYPE t_mate_id IS TABLE OF kgc_materialen.mate_id%TYPE
      INDEX BY kgc_materialen.code%TYPE;

   g_mate_id         t_mate_id;

   TYPE t_mate_srt IS TABLE OF kgc_materialen.letter_monsternr%TYPE
      INDEX BY kgc_materialen.code%TYPE;

   g_mate_srt        t_mate_srt;

   TYPE t_herk_id IS TABLE OF kgc_herkomsten.herk_id%TYPE
      INDEX BY kgc_herkomsten.code%TYPE;

   g_herk_id         t_herk_id;

   TYPE t_cond_id IS TABLE OF kgc_condities.cond_id%TYPE
      INDEX BY kgc_condities.code%TYPE;

   g_cond_id         t_cond_id;

   TYPE t_ongr_id IS TABLE OF kgc_onderzoeksgroepen.ongr_id%TYPE
      INDEX BY kgc_onderzoeksgroepen.code%TYPE;

   g_ongr_id         t_ongr_id;

   TYPE t_dewy_id IS TABLE OF kgc_declaratiewijzen.dewy_id%TYPE
      INDEX BY kgc_declaratiewijzen.code%TYPE;

   g_dewy_id         t_dewy_id;

   /*
      TYPE t_brty_id IS TABLE OF kgc_brieftypes.brty_id%TYPE
         INDEX BY kgc_brieftypes.code%TYPE;

      g_brty_id         t_brty_id;
   */
   PROCEDURE log_exception (p_id_namen VARCHAR2, p_id_waarden VARCHAR2)
   IS
      l_err_msg   VARCHAR2 (300) := SQLERRM;
   BEGIN
      /*
        CREATE    TABLE conv_errors
            ( onderzoeknr varchar2(20),
              odbnr number,
              plek VARCHAR2(30),
              id_namen VARCHAR2(100),
              id_waarden VARCHAR2(200),
              datum_tijd date,
              errmsg varchar2(300)
            ) TABLESPACE HELIX_D;
      */
      INSERT INTO conv_errors
                  (onderzoeknr, odbnr, plek,
                   id_namen, id_waarden, datum_tijd, errmsg
                  )
           VALUES (g_onde_rec_o.onderznr, g_onde_rec_o.odbnr, g_plek,
                   p_id_namen, p_id_waarden, SYSDATE, l_err_msg
                  );
   END log_exception;

   FUNCTION bepaal_ongr_id_en_code
      RETURN NUMBER
   IS
      CURSOR c_onderz (b_persnr IN NUMBER, b_binnendat IN DATE)
      IS
         SELECT afdeling, geef_geldige_datum (binnendat) binnendat
           FROM cyto_pers_onderz
          WHERE persnr = b_persnr
            -- +/- half jaar
            AND b_binnendat IS NOT NULL
            AND geef_geldige_datum (binnendat) > (b_binnendat - 183)
            AND geef_geldige_datum (binnendat) < (b_binnendat + 183)
            AND (onderznr LIKE 'P%' OR onderznr LIKE 'A%'
                 OR onderznr LIKE 'F%'
                )
            AND afdeling IN ('HAEM', 'CYTO', 'CYTOV', 'PREC', 'HAEMV');

      l_ongr_id          NUMBER;
      l_diff             NUMBER;
      l_prev_diff        NUMBER;
      l_onde_binnendat   DATE;
   BEGIN
      g_plek := 'bepaal_ongr_id';

      IF    g_onde_rec_o.onderznr LIKE 'I%/6%'
         OR g_onde_rec_o.onderznr LIKE 'I%/7%'
         OR g_onde_rec_o.onderznr LIKE 'I%/8%'
         OR g_onde_rec_o.onderznr LIKE 'I%/9%'
      THEN                                                     -- MLPA -> Cyto
         /* materialen: vruchtwater, vlok en foetaal bloed horen per definitie
            bij prenataal onderzoek
         */
         IF g_onde_rec_o.mate_code IN ('VRW', 'VLK', 'FBL')
         THEN
            g_onde_rec_o.afdeling := 'PREC';
         ELSE
            -- g_onde_rec_o.afdeling hier nog 'CYTO' of 'CYTOV'
            l_prev_diff := 184;
            l_onde_binnendat := geef_geldige_datum (g_onde_rec_o.binnendat);

            FOR r_onderz IN c_onderz (g_onde_rec_o.persnr, l_onde_binnendat)
            LOOP
               l_diff := ABS (r_onderz.binnendat - l_onde_binnendat);

               IF l_diff < l_prev_diff
               THEN
                  g_onde_rec_o.afdeling := r_onderz.afdeling;
                  l_prev_diff := l_diff;                           --20080626
               END IF;
            END LOOP;
         END IF;
      END IF;

      g_ongr_code :=
         CASE
            WHEN g_onde_rec_o.afdeling = 'CYTO'
               THEN 'PM'
            WHEN g_onde_rec_o.afdeling = 'CYTOV'
               THEN 'PV'
            WHEN g_onde_rec_o.afdeling = 'HAEM'
               THEN 'TM'
            WHEN g_onde_rec_o.afdeling = 'HAEMV'
               THEN 'TV'
            WHEN g_onde_rec_o.afdeling = 'PREC'
               THEN 'PR'
         END;
      l_ongr_id :=
         CASE
            WHEN g_onde_rec_o.afdeling IS NULL
               THEN NULL
            ELSE g_ongr_id (g_ongr_code)
         END;
      RETURN l_ongr_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'afdeling',
                        p_id_waarden      => g_onde_rec_o.afdeling
                       );
         RAISE;
   END bepaal_ongr_id_en_code;

   PROCEDURE init_afp_tekst
   IS
   BEGIN
      g_afp_tekst ('M') := 'Meting mislukt';
      g_afp_tekst ('N') := 'Meetwaarde normaal';
      g_afp_tekst ('A') := 'Meetwaarde afwijkend';
      g_afp_tekst ('K') := 'Meetwaarde te klein om af te lezen';
      g_afp_tekst ('G') := 'Meetwaarde te groot om af te lezen';
   -- X = Niet verricht --> meting niet toevoegen
   END init_afp_tekst;

   PROCEDURE init_mede_id
   IS
   BEGIN
      g_plek := 'init_mede_id';

      FOR r_mede IN (SELECT m.vms_username, m.mede_id
                       FROM kgc_medewerkers m, kgc_mede_kafd a
                      WHERE m.mede_id = a.mede_id
                        AND (a.kafd_id = c_kafd_id OR m.code = 'ONB')
                        AND m.vms_username IS NOT NULL)
      LOOP
         g_mede_id (r_mede.vms_username) := r_mede.mede_id;
      END LOOP;

      g_mede_id ('LH') := g_mede_id ('ONB');
      g_mede_id ('XX') := g_mede_id ('ONB');
      g_mede_id ('TP') := g_mede_id ('ONB');
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'mede_id', p_id_waarden => NULL);
         RAISE;
   END init_mede_id;

   PROCEDURE init_mate_id_and_srt
   IS
   BEGIN
      g_plek := 'init_mate_id';

      FOR r_mate IN (SELECT code, mate_id, letter_monsternr mate_srt
                       FROM kgc_materialen
                      WHERE kafd_id = c_kafd_id)
      LOOP
         g_mate_id (r_mate.code) := r_mate.mate_id;
         g_mate_srt (r_mate.code) := r_mate.mate_srt;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'mate_id', p_id_waarden => NULL);
         RAISE;
   END init_mate_id_and_srt;

   PROCEDURE init_onui_id
   IS
   BEGIN
      g_plek := 'init_onui_id';

      FOR r_onui IN (SELECT onui_id, code
                       FROM kgc_onderzoek_uitslagcodes
                      WHERE kafd_id = c_kafd_id
                            AND code IN ('AF', 'NA', 'GD'))
      LOOP
         g_onui_id (r_onui.code) := r_onui.onui_id;
      END LOOP;

      -- vertaling van VAX naar Helix:
      g_onui_id ('J') := g_onui_id ('AF');                        -- afwijkend
      g_onui_id ('N') := g_onui_id ('NA');                   -- niet afwijkend
      g_onui_id ('C') := g_onui_id ('AF');       -- @@2DO afwijkend ?? -> FISH
      g_onui_id ('G') := g_onui_id ('GD');                      -- geen deling
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'onui_id', p_id_waarden => NULL);
         RAISE;
   END init_onui_id;

   PROCEDURE init_rela_id
   IS
   BEGIN
      g_plek := 'init_rela_id';

      FOR r_rela IN (SELECT rela_id, code
                       FROM kgc_relaties)
      LOOP
         g_rela_id (r_rela.code) := r_rela.rela_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'rela_id', p_id_waarden => NULL);
         RAISE;
   END init_rela_id;

   PROCEDURE init_stof_id
   IS
   BEGIN
      g_plek := 'init_stof_id';

      FOR r_stof IN (SELECT stof_id, code
                       FROM kgc_stoftesten
                      WHERE kafd_id = c_kafd_id)
      LOOP
         g_stof_id (r_stof.code) := r_stof.stof_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'stof_id', p_id_waarden => NULL);
         RAISE;
   END init_stof_id;

   PROCEDURE init_stgr_id
   IS
   BEGIN
      g_plek := 'init_stgr_id 1';

      FOR r_stgr IN (SELECT stgr_id, code
                       FROM kgc_stoftestgroepen
                      WHERE kafd_id = c_kafd_id)
      LOOP
         g_stgr_id (r_stgr.code) := r_stgr.stgr_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'stgr_id', p_id_waarden => NULL);
         RAISE;
   END init_stgr_id;

   PROCEDURE init_mwst_id
   IS
   BEGIN
      g_plek := 'init_mwst_id';

      FOR r_mwst IN (SELECT mwst_id, code
                       FROM kgc_meetwaardestructuur)
      LOOP
         g_mwst_id (r_mwst.code) := r_mwst.mwst_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'mwst_id', p_id_waarden => NULL);
         RAISE;
   END init_mwst_id;

   PROCEDURE init_cond_id
   IS
   BEGIN
      g_plek := 'init_cond_id';

      FOR r_cond IN (SELECT cond_id, code
                       FROM kgc_condities)
      LOOP
         g_cond_id (r_cond.code) := r_cond.cond_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'cond_id', p_id_waarden => NULL);
         RAISE;
   END init_cond_id;

   PROCEDURE init_herk_id
   IS
   BEGIN
      g_plek := 'init_herk_id';

      FOR r_herk IN (SELECT herk_id, code
                       FROM kgc_herkomsten)
      LOOP
         g_herk_id (r_herk.code) := r_herk.herk_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'herk_id', p_id_waarden => NULL);
         RAISE;
   END init_herk_id;

   PROCEDURE init_ongr_id
   IS
   BEGIN
      g_plek := 'init_ongr_id';

      FOR r_ongr IN (SELECT ongr_id, code
                       FROM kgc_onderzoeksgroepen
                      WHERE kafd_id = c_kafd_id)
      LOOP
         g_ongr_id (r_ongr.code) := r_ongr.ongr_id;
      END LOOP;

      -- afdelingen zoals deze in VAX bekend zijn, vertalen:
      g_ongr_id ('CYTO') := g_ongr_id ('PM');
      g_ongr_id ('CYTOV') := g_ongr_id ('PV');
      g_ongr_id ('HAEM') := g_ongr_id ('TM');
      g_ongr_id ('HAEMV') := g_ongr_id ('TV');
      g_ongr_id ('PREC') := g_ongr_id ('PR');
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'ongr_id', p_id_waarden => NULL);
         RAISE;
   END init_ongr_id;

   PROCEDURE init_dewy_id
   IS
   BEGIN
      g_plek := 'init_dewy_id';

      FOR r_dewy IN (SELECT dewy_id, code
                       FROM kgc_declaratiewijzen
                      WHERE kafd_id = c_kafd_id)
      LOOP
         g_dewy_id (r_dewy.code) := r_dewy.dewy_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'dewy_id', p_id_waarden => NULL);
         RAISE;
   END init_dewy_id;

   /* PROCEDURE init_brty_id
      IS
      BEGIN
         g_plek := 'init_brty_id';

         FOR r_brty IN (SELECT brty_id, code
                          FROM kgc_brieftypes)
         LOOP
            g_brty_id (r_brty.code) := r_brty.brty_id;
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            log_exception (p_id_namen => 'brty_id', p_id_waarden => NULL);
            RAISE;
      END init_brty_id;
   */
   PROCEDURE init_ids
   IS
   BEGIN
      init_afp_tekst;
      init_mede_id;
      init_mate_id_and_srt;
      init_onui_id;
      init_rela_id;
      init_stof_id;
      init_stgr_id;
      init_mwst_id;
      init_cond_id;
      init_herk_id;
      init_ongr_id;
      init_dewy_id;
      -- init_brty_id;
      g_mons_rec_init.kafd_id := c_kafd_id;
   END init_ids;

   PROCEDURE add_zwangerschappen
   IS
      CURSOR c_zwang
      IS
         SELECT hp.pers_id pers_id_geboren_als, q_onderz.*
           FROM (SELECT   po.onderznr,
                          geef_geldige_datum (pf.gebdat) geboortedatum,
                          pf.aanspreken,
                          DECODE (pf.gesl, 'M', 'M', 'V', 'V', 'O') geslacht,
                          pf.persnr pers_id_foet_1, pm.aanspreken naam_moeder,
                          pm.persnr pers_id_moeder, pz.persnr pers_id_foet_2,
                          pz.naam, cz.gravida, cz.info info_geslacht,
                          NVL (pz.congafw, 'N') consanguiniteit,
                          DECODE (cz.lmzeker,
                                  'J', '4',
                                  '2'
                                 ) zekerheid_datum_lm,
                          cz.lmdat datum_lm, cz.atdat datum_aterm,
                          cz.bijz opmerkingen, po.odbnr, cz.hoeveel,
                          cz.copy_1, cz.abortus
                     FROM cyto_pers_onderz po,
                          cyto_prec_onderzoek pr,
                          cyto_echo e,
                          cyto_consult c,
                          cyto_pers_onderz po2,
                          cyto_persoon pf                            -- foetus
                                         ,
                          cyto_persoon pm                            -- moeder
                                         ,
                          cyto_zwangers cz,
                          cyto_pers_zwangers pz
                    WHERE pr.odbnr = po.odbnr
                      AND (po.onderznr LIKE 'A%' OR po.onderznr LIKE 'C%')
                      AND po.onderznr NOT LIKE 'A50%'
                      AND po.onderznr NOT LIKE 'C50%'
                      AND po.afdeling IN
                                   ('HAEM', 'CYTO', 'CYTOV', 'PREC', 'HAEMV')
                      AND e.odbnre = pr.odbnre
                      AND c.odbnrc = e.odbnrc
                      AND po2.odbnr = c.odbnr
                      AND pf.persnr = po.persnr
                      AND pm.persnr = po2.persnr
                      AND cz.odbnr = po2.odbnr
                      AND pz.odbnr = cz.odbnr
                 ORDER BY pm.persnr, pz.odbnr) q_onderz,
                kgc_personen hp
          WHERE q_onderz.pers_id_foet_1 = q_onderz.pers_id_foet_2
            AND pers_id_foet_1 = hp.pers_id(+);

      p_zwang          c_zwang%ROWTYPE;
      l_zwan_id        NUMBER;
      l_foet_id        NUMBER;
      l_foetus_nr      NUMBER;
      l_prev_pers_id   NUMBER;

      PROCEDURE add_foet_pers_conv
      IS
      BEGIN
         g_plek := 'add_foet_pers_conv';

         INSERT INTO cyto_foet_pers_conv
                     (foet_id_vax, foet_id_helix,
                      pers_id_geboren_als, pers_id_moeder,
                      onderznr, odbnr, aanspreken,
                      naam, naam_moeder
                     )
              VALUES (p_zwang.pers_id_foet_1, l_foet_id,
                      p_zwang.pers_id_geboren_als, p_zwang.pers_id_moeder,
                      p_zwang.onderznr, p_zwang.odbnr, p_zwang.aanspreken,
                      p_zwang.naam, p_zwang.naam_moeder
                     );
      EXCEPTION
         WHEN OTHERS
         THEN
            log_exception (p_id_namen => 'pers_id', p_id_waarden => NULL);
      END add_foet_pers_conv;

      PROCEDURE add_zwangerschap
      IS
         l_rela_id   NUMBER;
      BEGIN
         g_plek := 'add_zwangerschap';

         BEGIN
            l_rela_id :=
               CASE
                  WHEN p_zwang.copy_1 IS NULL
                     THEN NULL
                  ELSE g_rela_id (p_zwang.copy_1)
               END;
         EXCEPTION
            WHEN OTHERS
            THEN
               log_exception (p_id_namen        => 'rela_code',
                              p_id_waarden      => p_zwang.copy_1
                             );
         END;

         SELECT kgc_zwan_seq.NEXTVAL
           INTO l_zwan_id
           FROM DUAL;

         INSERT INTO kgc_zwangerschappen
                     (zwan_id, pers_id,
                      nummer,
                      info_geslacht,
                      consanguiniteit, rela_id,
                      zekerheid_datum_lm,
                      datum_lm, berekeningswijze,
                      datum_aterm,
                      opmerkingen, mola, apla,
                      eug, para, abortussen,
                      abortus,
                      hoeveelling, progenituur,
                      created_by, creation_date, last_updated_by,
                      last_update_date
                     )
              VALUES (l_zwan_id, p_zwang.pers_id_moeder,
                      l_foetus_nr                            --p_zwang.gravida
                                 ,
                      DECODE (p_zwang.info_geslacht, 'J', 'J', 'N'),
                      p_zwang.consanguiniteit, l_rela_id,
                      geef_geldige_datum (p_zwang.zekerheid_datum_lm),
                      geef_geldige_datum (p_zwang.datum_lm), 'LM',
                      geef_geldige_datum (p_zwang.datum_aterm),
                      p_zwang.opmerkingen, NULL                        -- MOLA
                                               , NULL                  -- APLA
                                                     ,
                      NULL                                              -- EUG
                          , NULL                                       -- PARA
                                , p_zwang.abortus                -- ABORTUSSEN
                                                 ,
                      CASE
                         WHEN p_zwang.abortus = 0
                            THEN 'N'
                         WHEN p_zwang.abortus IS NULL
                            THEN 'O'
                         ELSE 'J'
                      END                                           -- ABORTUS
                         ,
                      p_zwang.hoeveel                           -- HOEVEELLING
                                     , NULL                     -- PROGENITUUR
                                           ,
                      c_user, SYSDATE, c_user,
                      SYSDATE
                     );
      EXCEPTION
         WHEN OTHERS
         THEN
            log_exception (p_id_namen => 'pers_id', p_id_waarden => NULL);
      END add_zwangerschap;

      PROCEDURE add_foetus
      IS
      BEGIN
         g_plek := 'add_foetus';

         SELECT kgc_foet_seq.NEXTVAL
           INTO l_foet_id
           FROM DUAL;

         INSERT INTO kgc_foetussen
                     (foet_id, pers_id_moeder, volgnr, niet_geboren,
                      geslacht, geplande_geboortedatum,
                      pers_id_geboren_als, zwan_id, created_by,
                      creation_date, last_updated_by, last_update_date
                     )
              VALUES (l_foet_id, p_zwang.pers_id_moeder, l_foetus_nr, 'N',
                      p_zwang.geslacht, p_zwang.geboortedatum,
                      p_zwang.pers_id_geboren_als, l_zwan_id, c_user,
                      SYSDATE, c_user, SYSDATE
                     );
      EXCEPTION
         WHEN OTHERS
         THEN
            log_exception (p_id_namen => 'pers_id', p_id_waarden => NULL);
      END add_foetus;
   BEGIN
      g_plek := 'vul_cyto_zwangerschappen';
      l_foetus_nr := 0;
      l_prev_pers_id := -1;

      FOR r_zwang IN c_zwang
      LOOP
         p_zwang := r_zwang;

         IF p_zwang.pers_id_moeder = l_prev_pers_id
         THEN
            l_foetus_nr := l_foetus_nr + 1;
         ELSE
            l_foetus_nr := 1;
         END IF;

         l_prev_pers_id := p_zwang.pers_id_moeder;
         add_zwangerschap;
         add_foetus;
         add_foet_pers_conv;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'pers_id', p_id_waarden => NULL);
   END add_zwangerschappen;

   PROCEDURE add_notitie
   IS
      l_noti_id   NUMBER;
   BEGIN
      g_plek := 'add_notitie';

      SELECT kgc_noti_seq.NEXTVAL
        INTO l_noti_id
        FROM DUAL;

      INSERT INTO kgc_notities
                  (noti_id, entiteit, ID,
                   omschrijving, created_by, creation_date, last_updated_by,
                   last_update_date, code,
                   commentaar
                  )
           VALUES (l_noti_id, 'ONDE', g_onde_rec.onde_id,
                   g_onde_rec_o.bijz                           -- omschrijving
                                    , c_user, SYSDATE, c_user,
                   SYSDATE, 'BIJZONDERH'                  -- max. 10 karakters
                                        ,
                   'Overgenomen vanuit de VAX'
                  );                                             -- commentaar
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'bijz',
                        p_id_waarden      => g_onde_rec_o.bijz
                       );
   END;

   FUNCTION bepaal_materiaal (p_mate_code IN VARCHAR2)
      RETURN NUMBER
   IS
      l_mate_id   NUMBER;

      PROCEDURE voeg_mate_toe
      IS
         CURSOR c_stc004 (b_mate_code IN VARCHAR2)
         IS
            SELECT materiaal
              FROM cyto_stc004
             WHERE code = b_mate_code;

         l_mate_omschrijving   kgc_materialen.omschrijving%TYPE;
      BEGIN
         g_plek := 'bepaal_materiaal';

         SELECT kgc_mate_seq.NEXTVAL
           INTO l_mate_id
           FROM DUAL;

         OPEN c_stc004 (b_mate_code => p_mate_code);

         FETCH c_stc004
          INTO l_mate_omschrijving;

         CLOSE c_stc004;

         INSERT INTO kgc_materialen
                     (mate_id, kafd_id, code, omschrijving,
                      created_by, creation_date, last_updated_by,
                      last_update_date
                     )
              VALUES (l_mate_id, c_kafd_id, p_mate_code, l_mate_omschrijving,
                      c_user, SYSDATE, c_user,
                      SYSDATE
                     );

         g_mate_id (p_mate_code) := l_mate_id;
         g_mate_srt (p_mate_code) := NULL;
      END voeg_mate_toe;
   BEGIN
      l_mate_id :=
              CASE
                 WHEN p_mate_code IS NULL
                    THEN NULL
                 ELSE g_mate_id (p_mate_code)
              END;
      RETURN l_mate_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         voeg_mate_toe;
         RETURN l_mate_id;
   END bepaal_materiaal;

   FUNCTION bepaal_conditie (p_cond_code IN VARCHAR2)
      RETURN NUMBER
   IS
      l_cond_id   NUMBER;

      PROCEDURE voeg_cond_toe
      IS
         CURSOR c_stc119 (b_cond_code IN VARCHAR2)
         IS
            SELECT helderheid
              FROM cyto_stc119
             WHERE code = p_cond_code;

         l_cond_omschrijving   kgc_condities.omschrijving%TYPE;
      BEGIN
         g_plek := 'bepaal_conditie';

         SELECT kgc_cond_seq.NEXTVAL
           INTO l_cond_id
           FROM DUAL;

         OPEN c_stc119 (b_cond_code => p_cond_code);

         FETCH c_stc119
          INTO l_cond_omschrijving;

         CLOSE c_stc119;

         INSERT INTO kgc_condities
                     (cond_id, code, omschrijving, vervallen,
                      created_by, creation_date, last_updated_by,
                      last_update_date
                     )
              VALUES (l_cond_id, p_cond_code, l_cond_omschrijving, 'N',
                      c_user, SYSDATE, c_user,
                      SYSDATE
                     );
      END voeg_cond_toe;
   BEGIN
      l_cond_id :=
              CASE
                 WHEN p_cond_code IS NULL
                    THEN NULL
                 ELSE g_cond_id (p_cond_code)
              END;
      RETURN l_cond_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         voeg_cond_toe;
         RETURN l_cond_id;
   END bepaal_conditie;

   FUNCTION bepaal_herkomst (p_herk_code IN VARCHAR2)
      RETURN NUMBER
   IS
      l_herk_id   NUMBER;

      PROCEDURE voeg_herk_toe
      IS
         CURSOR c_stc019 (b_herk_code IN VARCHAR2)
         IS
            SELECT znaam
              FROM cyto_stc019
             WHERE zkhscd = b_herk_code;

         l_herk_omschrijving   kgc_herkomsten.omschrijving%TYPE;
      BEGIN
         g_plek := 'bepaal_herkomst';

         SELECT kgc_herk_seq.NEXTVAL
           INTO l_herk_id
           FROM DUAL;

         OPEN c_stc019 (b_herk_code => p_herk_code);

         FETCH c_stc019
          INTO l_herk_omschrijving;

         CLOSE c_stc019;

         INSERT INTO kgc_herkomsten
                     (herk_id, code, omschrijving, vervallen,
                      created_by, creation_date, last_updated_by,
                      last_update_date
                     )
              VALUES (l_herk_id, p_herk_code, l_herk_omschrijving, 'N',
                      c_user, SYSDATE, c_user,
                      SYSDATE
                     );
      END voeg_herk_toe;
   BEGIN
      l_herk_id :=
              CASE
                 WHEN p_herk_code IS NULL
                    THEN NULL
                 ELSE g_herk_id (p_herk_code)
              END;
      RETURN l_herk_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         voeg_herk_toe;
         RETURN l_herk_id;
   END bepaal_herkomst;

   PROCEDURE indicaties_vullen (p_indicatie IN VARCHAR2, p_num_ind IN INTEGER)
   IS
      CURSOR c_indi (b_ongr_id NUMBER, b_indicatie VARCHAR2)
      IS
         SELECT indi_id
           FROM kgc_indicatie_teksten
          WHERE kafd_id = c_kafd_id
            AND ongr_id = b_ongr_id
            AND code = UPPER (p_indicatie);

      l_indi_id   NUMBER;
      l_onin_id   NUMBER;
   BEGIN
      g_plek := 'indicaties_vullen';

      OPEN c_indi (g_onde_rec.ongr_id, p_indicatie);

      FETCH c_indi
       INTO l_indi_id;

      CLOSE c_indi;

      SELECT kgc_onin_seq.NEXTVAL
        INTO l_onin_id
        FROM DUAL;

      INSERT INTO kgc_onderzoek_indicaties
                  (onin_id, onde_id, indi_id,
                   hoofd_ind, created_by, creation_date,
                   last_updated_by, last_update_date
                  )
           VALUES (l_onin_id, g_onde_rec.onde_id, l_indi_id,
                   CASE
                      WHEN p_num_ind = 1
                         THEN 'J'
                      ELSE 'N'
                   END, c_user, SYSDATE,
                   c_user, SYSDATE
                  );
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         NULL;
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'indi_code | onde_id | ongr_id',
                        p_id_waarden      =>    p_indicatie
                                             || ' | '
                                             || g_onde_rec.onde_id
                                             || ' | '
                                             || g_onde_rec.ongr_id
                       );
   END indicaties_vullen;                          -- indicatie teksten vullen

   PROCEDURE kopiehouders_vullen (p_kopiehouder IN NUMBER, p_uits_id IN NUMBER)
   IS
      l_id   NUMBER;
   BEGIN
      g_plek := 'kopiehouders_vullen';

      SELECT kgc_koho_seq.NEXTVAL
        INTO l_id
        FROM DUAL;

      INSERT INTO kgc_kopiehouders
                  (koho_id, uits_id, rela_id,
                   created_by, creation_date,
                   last_updated_by, last_update_date
                  )
           VALUES (l_id, p_uits_id, p_kopiehouder                   -- rela_id
                                                 ,
                   c_user                                        -- created_by
                         , SYSDATE                            -- creation_date
                                  ,
                   c_user                                   -- last_updated_by
                         , SYSDATE
                  );                                       -- last_update_date

      SELECT kgc_onkh_seq.NEXTVAL
        INTO l_id
        FROM DUAL;

      INSERT INTO kgc_onderzoek_kopiehouders
                  (onkh_id, onde_id,
                   rela_id, created_by,
                   creation_date, last_updated_by, last_update_date
                  )
           VALUES (l_id                                             -- onkh_id
                       , g_onde_rec.onde_id,
                   p_kopiehouder                                    -- rela_id
                                , c_user                         -- created_by
                                        ,
                   SYSDATE                                    -- creation_date
                          , c_user                          -- last_updated_by
                                  , SYSDATE
                  );                                       -- last_update_date
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'kopie_houder | uits_id',
                        p_id_waarden      => p_kopiehouder || ' | '
                                             || p_uits_id
                       );
   END kopiehouders_vullen;

   PROCEDURE add_onderzoek
   IS
      l_suffix   VARCHAR2 (2);
   BEGIN
      g_plek := 'add_onderzoek';
      g_onde_rec := g_onde_rec_init;
      g_odbnr := g_onde_rec_o.odbnr;
      g_onde_rec.pers_id := g_onde_rec_o.persnr;
      g_onde_rec.kafd_id := c_kafd_id;
      g_onde_rec.datum_binnen := geef_geldige_datum (g_onde_rec_o.binnendat);
      g_onde_rec.spoed := NVL (g_onde_rec_o.spoed, 'N');
          /* juiste persoon en foetus ID is eerder geregeld bij persoon-conversie,
          maak gebruik van deze ID's --> NB! KANS DAT PERSONEN/FOETUSSEN NIET
          WORDEN GECONVERTEERD */
      /*    check_pers_foet( x_pers_id     => g_onde_rec.pers_id
                         , p_onderznr    => g_onde_rec.onderzoeknr
                         , x_foet_id     => g_onde_rec.foet_id );*/
      g_onde_rec.foet_id := g_onde_rec_o.foet_id;
      g_plek := 'add_onderzoek';           -- overschreven in check_pers_foet

      IF g_onde_rec_o.mate_code IS NULL
      THEN
         g_onde_rec_o.mate_code := 'BL';
      -- AANNAME ! --> geverifieerd met AJ
      END IF;

      IF g_onde_rec_o.tel_uitslag IS NOT NULL
      THEN
         g_onde_rec.omschrijving :=
                                 'uitslagdatum: ' || g_onde_rec_o.tel_uitslag;
      END IF;

      IF g_onde_rec_o.tel_contactp IS NOT NULL
      THEN
         IF g_onde_rec_o.tel_uitslag IS NOT NULL
         THEN
            g_onde_rec.omschrijving :=
                  g_onde_rec.omschrijving
               || ' , '
               || 'Contactpersoon: '
               || g_onde_rec_o.tel_contactp;
         ELSE
            g_onde_rec.omschrijving :=
                              'Contactpersoon: ' || g_onde_rec_o.tel_contactp;
         END IF;
      END IF;

      g_plek := 'add_onderzoek (ongr_id)';
      g_onde_rec.ongr_id := bepaal_ongr_id_en_code;
      g_onde_rec.onderzoeknr := g_onde_rec_o.onderznr || '-' || g_ongr_code;
      g_onde_rec.onderzoekstype := 'D';                            -- diagnose

      IF g_onde_rec_o.declaratie IS NOT NULL
      THEN
         g_onde_rec.declareren := 'J';
      ELSE
         g_onde_rec.declareren := 'N';
      END IF;

      -- declaratiewijze
      g_plek := 'add_onderzoek (dewy_id)';
      /*
      niet goed: declaratie IN (J/N), terwijl dewy Binnen/buitenland
      IF g_onde_rec_o.declaratie IS NOT NULL THEN
        g_onde_rec.dewy_id         :=
          CASE
            WHEN g_onde_rec_o.declaratie IS NULL THEN NULL
            ELSE g_dewy_id( g_onde_rec_o.declaratie )
          END;
      END IF;*/
      /* NB: het gaat om een momentopname (huidige woonplaats), dat zal juist zijn
         bij het laatste onderzoek voor de betreffende persoon, maar dat zou kunnen
         afwijkend bij oudere onderzoeken.
      */
      g_onde_rec.dewy_id :=
         CASE
            WHEN g_onde_rec_o.land IS NULL
               THEN NULL
            WHEN g_onde_rec_o.land = 'NEDERLAND'
               THEN g_dewy_id ('BI')                             -- binnenland
            ELSE g_dewy_id ('BU')                                -- buitenland
         END;
      -- aanvrager
      g_plek := 'add_onderzoek (rela_id)';

      IF g_onde_rec_o.relatie IS NOT NULL
      THEN
         g_onde_rec.rela_id :=
            CASE
               WHEN g_onde_rec_o.relatie IS NULL
                  THEN NULL
               ELSE g_rela_id (g_onde_rec_o.relatie)
            END;
      --g_onde_rec.inst_id         := g_onde_rec.rela_id; instelling <> relatie !
      END IF;

      IF g_onde_rec.rela_id IS NULL
      THEN
         g_onde_rec.rela_id := c_rela_id_onb;
      END IF;

      -- autorisator
      g_plek := 'add_onderzoek (autorisator)';

      IF g_onde_rec_o.autorisator IS NOT NULL
      THEN
         BEGIN
            g_onde_rec.mede_id_autorisator :=
               CASE
                  WHEN g_onde_rec_o.autorisator IS NULL
                     THEN NULL
                  ELSE g_mede_id (g_onde_rec_o.autorisator)
               END;
            g_onde_rec.datum_autorisatie :=
                               geef_geldige_datum (g_onde_rec_o.uitslagdatum);

            IF g_onde_rec.datum_autorisatie IS NULL
            THEN
               g_onde_rec.datum_autorisatie :=
                                geef_geldige_datum (g_onde_rec_o.tel_uitslag);
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               g_onde_rec.mede_id_autorisator := g_mede_id ('ONB');
               log_exception (p_id_namen        => 'autorisator',
                              p_id_waarden      => g_onde_rec_o.autorisator
                             );
         END;
      END IF;

      IF g_onde_rec_o.onde_afgerond <> 'J'
      THEN
         g_onde_rec_o.onde_afgerond := NULL;
      END IF;

      IF g_onde_rec_o.afgerond <> 'J'
      THEN
         g_onde_rec_o.afgerond := NULL;
      END IF;

      g_onde_rec.afgerond :=
            NVL (NVL (g_onde_rec_o.onde_afgerond, g_onde_rec_o.afgerond), 'N');
      g_plek := 'add_onderzoek (onui_id)';
      g_onde_rec.onui_id :=
         CASE
            WHEN g_onde_rec_o.afwijkend IS NULL
               THEN NULL
            ELSE g_onui_id (g_onde_rec_o.afwijkend)
         END;
      g_onde_rec.herk_id :=
         CASE
            WHEN g_onde_rec_o.afnplaats IS NULL
               THEN NULL
            ELSE bepaal_herkomst (p_herk_code => g_onde_rec_o.afnplaats)
         END;
      -- familie relatie
      g_onde_rec.fare_id := TO_NUMBER (NULL);
      g_plek := 'add_onderzoek (insert into)';

      SELECT kgc_onde_seq.NEXTVAL
        INTO g_onde_rec.onde_id
        FROM DUAL;

      INSERT INTO kgc_onderzoeken
                  (onde_id, ongr_id,
                   kafd_id, pers_id,
                   onderzoekstype, onderzoeknr,
                   rela_id, spoed,
                   declareren, afgerond, created_by,
                   creation_date, last_updated_by, last_update_date, foet_id,
                   datum_binnen,
                                --geplande_einddatum
                                herk_id,
                   inst_id,
                           --onderzoekswijze, status, mede_id_beoordelaar,
                           mede_id_autorisator,
                   --mede_id_controle, mede_id_dia_autorisator,
                   onui_id,
                           --coty_id, diagnose_code, ermo_id, dist_id,
                           --herk_id_dia, taal_id, verz_id,
                           proj_id,
                   fare_id, dewy_id,
                   --zekerheid_diagnose, zekerheid_status, zekerheid_erfmodus,
                   datum_autorisatie,
                                     --kariotypering, referentie, nota_adres,
                                     omschrijving
                  --onre_id, rela_id_oorsprong
                  )
           VALUES (g_onde_rec.onde_id, g_onde_rec.ongr_id,
                   g_onde_rec.kafd_id, g_onde_rec.pers_id,
                   g_onde_rec.onderzoekstype, g_onde_rec.onderzoeknr,
                   g_onde_rec.rela_id, g_onde_rec.spoed,
                   g_onde_rec.declareren, g_onde_rec.afgerond, c_user,
                   SYSDATE, c_user, SYSDATE, g_onde_rec.foet_id,
                   g_onde_rec.datum_binnen,
                                           --TO_DATE (NULL), TO_NUMBER (NULL)
                                           g_onde_rec.herk_id,
                   g_onde_rec.inst_id,
                                      --NULL, NULL, TO_NUMBER (NULL),
                                      g_onde_rec.mede_id_autorisator,
                   --TO_NUMBER (NULL), TO_NUMBER (NULL),
                   g_onde_rec.onui_id,
                                      --TO_NUMBER (NULL), NULL, TO_NUMBER (NULL), TO_NUMBER (NULL),
                                      --TO_NUMBER (NULL), TO_NUMBER (NULL), TO_NUMBER (NULL),
                                      g_onde_rec.proj_id,
                   g_onde_rec.fare_id                                  -- NULL
                                     , g_onde_rec.dewy_id,
                   --NULL, NULL, NULL,
                   g_onde_rec.datum_autorisatie,
                                                --NULL, NULL, NULL,
                                                g_onde_rec.omschrijving
                  --TO_NUMBER (NULL),TO_NUMBER (NULL)
                  );

      IF     g_onde_rec_o.bijz IS NOT NULL
         AND UPPER (TRIM (g_onde_rec_o.bijz)) NOT LIKE 'GEEN%'
         AND TRIM (g_onde_rec_o.bijz) NOT IN ('', '-')
      THEN
         add_notitie;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'onderzoeknr',
                        p_id_waarden      => g_onde_rec.onderzoeknr
                       );
   END add_onderzoek;

   PROCEDURE add_decl
   IS
      decl_rec   kgc_declaraties%ROWTYPE;
   BEGIN
      g_plek := 'add_decl';
      decl_rec := g_decl_rec_init;
      decl_rec.entiteit := 'ONDE';
      decl_rec.ID := g_onde_rec.onde_id;
      decl_rec.declareren := 'J';
      decl_rec.kafd_id := g_onde_rec.kafd_id;
      decl_rec.ongr_id := g_onde_rec.ongr_id;
      decl_rec.pers_id := g_onde_rec.pers_id;
      decl_rec.dewy_id := g_onde_rec.dewy_id;
      decl_rec.ingr_id :=
                        kgc_indi_00.ingr_id (p_onde_id      => g_onde_rec.onde_id);
      decl_rec.fare_id := g_onde_rec.fare_id;

      IF g_onde_rec.datum_binnen < TO_DATE ('13-04-2000', 'dd-mm-yyyy')
      THEN                                                            -- if_3
         -- oude stijl (verwerkt, voorkom machtigingsbrieven)
         decl_rec.status := 'V';       -- STATUS IN ('H', 'V', 'X', 'T', 'R')
      END IF;                                                          -- if_3

      SELECT kgc_decl_seq.NEXTVAL
        INTO decl_rec.decl_id
        FROM DUAL;

      INSERT INTO kgc_declaraties
                  (decl_id, entiteit, ID, declareren, created_by,
                   creation_date, last_updated_by, last_update_date, kafd_id,
                   ongr_id, pers_id, dewy_id, ingr_id, verz_id, fare_id,
                   rnde_id, status, datum_verwerking, nota_adres,
                   verzekeringswijze, verzekeringsnr, toelichting,
                   betreft_tekst)
         SELECT decl_rec.decl_id, decl_rec.entiteit, decl_rec.ID,
                decl_rec.declareren, c_user, SYSDATE, c_user, SYSDATE,
                decl_rec.kafd_id, decl_rec.ongr_id, decl_rec.pers_id,
                decl_rec.dewy_id, decl_rec.ingr_id, pers.verz_id,
                decl_rec.fare_id, TO_NUMBER (NULL), decl_rec.status,
                TO_DATE (NULL), kgc_adres_00.verzekeraar (pers.verz_id, 'J'),
                pers.verzekeringswijze, pers.verzekeringsnr, NULL, NULL
           FROM kgc_personen pers
          WHERE pers.pers_id = decl_rec.pers_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'onde_id',
                        p_id_waarden      => g_onde_rec.onde_id
                       );
   END add_decl;

   PROCEDURE add_monster
   IS
      l_mate_srt    kgc_materialen.letter_monsternr%TYPE;
      l_onmo_id     NUMBER;
      l_ongr_code   VARCHAR (2);
   BEGIN
      g_plek := 'add_monster';
      g_mons_rec := g_mons_rec_init;
      g_mons_rec.monsternummer := TO_CHAR (g_onde_rec.onderzoeknr);
      g_mons_rec.ongr_id := g_onde_rec.ongr_id;
      g_mons_rec.pers_id := g_onde_rec.pers_id;
      g_mons_rec.foet_id := g_onde_rec.foet_id;
      g_mons_rec.mate_id := bepaal_materiaal (g_onde_rec_o.mate_code);
      g_mons_rec.cond_id := bepaal_conditie (g_onde_rec_o.helderhcd);
      g_mons_rec.herk_id :=
                      bepaal_herkomst (p_herk_code      => g_onde_rec_o.afnplaats);
      g_mons_rec.datum_aanmelding :=
                                  geef_geldige_datum (g_onde_rec_o.binnendat);
      g_mons_rec.datum_afname := geef_geldige_datum (g_onde_rec_o.binnendat);
      g_mons_rec.tijd_afname := TO_DATE (NULL);
      g_mons_rec.leeftijd :=
         SUBSTR
            (kgc_leef_00.leeftijd
                               (p_pers_id            => g_mons_rec.pers_id,
                                p_foet_id            => g_mons_rec.foet_id,
                                p_datum_afname       => g_mons_rec.datum_afname,
                                p_geboortedatum      => g_onde_rec_o.geboortedatum,
                                p_datum_aterm        => NULL
                               ),
             1,
             5
            );
      g_mons_rec.leef_id :=
         kgc_leef_00.leeftijdscategorie (p_kafd_id           => c_kafd_id,
                                         p_aantal_dagen      => g_mons_rec.leeftijd
                                        );
      g_mons_rec.ckcl_nummer := NULL;
      g_mons_rec.commentaar := g_onde_rec_o.opmerking;
      g_mons_rec.bewaren := 'N';          -- AANNAME ! --> geverifieerd met AJ

      IF SUBSTR (g_onde_rec_o.hoevmat, 1, 1) IN ('>', '<')
      THEN
         g_mons_rec.hoeveelheid_monster := NULL;
      ELSE
         g_mons_rec.hoeveelheid_monster := g_onde_rec_o.hoevmat;
      END IF;

      g_mons_rec.rela_id :=
         CASE
            WHEN g_onde_rec_o.afndoor IS NULL
               THEN NULL
            ELSE g_rela_id (g_onde_rec_o.afndoor)
         END;

      SELECT kgc_mons_seq.NEXTVAL
        INTO g_mons_rec.mons_id
        FROM DUAL;

      SELECT kgc_onmo_seq.NEXTVAL
        INTO l_onmo_id
        FROM DUAL;

      INSERT INTO kgc_monsters
                  (mons_id, monsternummer,
                   ongr_id, kafd_id,
                   pers_id, mate_id, prematuur,
                   bewaren, nader_gebruiken, created_by, creation_date,
                   last_updated_by, last_update_date, datum_aanmelding,
                   mede_id, rela_id, buff_id,
                   cond_id, leef_id,
                   leeftijd, bepl_id_kort, bepl_id_lang,
                   datum_afname, tijd_afname,
                   periode_afname, ckcl_nummer, kweeknummer,
                   aantal_buizen, datum_verwacht,
                   hoeveelheid_monster, hoeveelheid_buffer, medicatie,
                   voeding, medische_indicatie,
                   commentaar,
                   foet_id, declareren, herk_id,
                   bwre_id, alleen_voor_opslag
                  )
           VALUES (g_mons_rec.mons_id, g_mons_rec.monsternummer,
                   g_mons_rec.ongr_id, g_mons_rec.kafd_id,
                   g_mons_rec.pers_id, g_mons_rec.mate_id, 'N',
                   g_mons_rec.bewaren, 'N', c_user, SYSDATE,
                   c_user, SYSDATE, g_mons_rec.datum_aanmelding,
                   TO_NUMBER (NULL), g_mons_rec.rela_id, TO_NUMBER (NULL),
                   g_mons_rec.cond_id, g_mons_rec.leef_id,
                   g_mons_rec.leeftijd, TO_NUMBER (NULL), TO_NUMBER (NULL),
                   g_mons_rec.datum_afname, g_mons_rec.tijd_afname,
                   g_mons_rec.periode_afname,
                                             -- periode_afname is null !!!
                                             g_mons_rec.ckcl_nummer, NULL,
                   TO_NUMBER (NULL), TO_DATE (NULL),
                   g_mons_rec.hoeveelheid_monster,
                                                  -- hoeveelheid_monster is null !!!
                                                  TO_NUMBER (NULL), NULL,
                   NULL, g_mons_rec.medische_indicatie,
                   NULL       -- g_mons_rec.commentaar verplaatsen naar meting
                       ,
                   -- medische_indicatie is null !!!
                   g_mons_rec.foet_id, 'N', g_mons_rec.herk_id,
                   TO_NUMBER (NULL), 'N'
                  );

      INSERT INTO kgc_onderzoek_monsters
                  (onmo_id, onde_id, mons_id, created_by,
                   creation_date, last_updated_by, last_update_date
                  )
           VALUES (l_onmo_id, g_onde_rec.onde_id, g_mons_rec.mons_id, c_user,
                   SYSDATE, c_user, SYSDATE
                  );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'monsternr',
                        p_id_waarden      => g_mons_rec.monsternummer
                       );
   END add_monster;

   PROCEDURE add_frac (
      p_aspectsed   IN   cyto_prec_onderzoek.aspectsed%TYPE,
      p_hoevsed     IN   cyto_prec_onderzoek.hoevsed%TYPE,
      p_matcd       IN   cyto_prec_onderzoek.amatcd%TYPE
   )
   IS
      /*
          Let op: voordat add_frac aangeroepen wordt,
          eerst g_frac_rec := g_frac_rec_init;
          en daarna g_frac_rec.fractienummer initialiseren !!!
      */
      CURSOR c_aspectsed (b_aspectsed IN VARCHAR2)
      IS
         SELECT aspect
           FROM cyto_stc123
          WHERE code = b_aspectsed;

      CURSOR c_hoevsed (b_hoevsed IN VARCHAR2, b_mate_code IN VARCHAR2)
      IS
         SELECT hoeveelheid
           FROM helix.cyto_stc114
          WHERE code = b_hoevsed AND amatcd = b_mate_code;

      l_hoevsed     cyto_prec_onderzoek.hoevsed%TYPE;
      l_aspectsed   cyto_prec_onderzoek.aspectsed%TYPE;
   BEGIN
      g_plek := 'add_frac';

      -- NIET hier: g_frac_rec                 := g_frac_rec_init;
      IF p_hoevsed IS NOT NULL
      THEN
         BEGIN
            OPEN c_hoevsed (b_hoevsed => p_hoevsed, b_mate_code => p_matcd);

            FETCH c_hoevsed
             INTO l_hoevsed;

            CLOSE c_hoevsed;

            IF l_hoevsed IS NOT NULL
            THEN
               g_frac_rec.commentaar := 'Hoeveelheid fractie: ' || l_hoevsed;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;                                            -- no problem
         END;
      END IF;

      IF p_aspectsed IS NOT NULL
      THEN
         BEGIN
            OPEN c_aspectsed (b_aspectsed => p_aspectsed);

            FETCH c_aspectsed
             INTO l_aspectsed;

            CLOSE c_aspectsed;

            IF l_aspectsed IS NOT NULL
            THEN
               IF g_frac_rec.commentaar IS NOT NULL
               THEN
                  g_frac_rec.commentaar :=
                     g_frac_rec.commentaar || ', Aspectcode: ' || l_aspectsed;
               ELSE
                  g_frac_rec.commentaar := 'Aspectcode: ' || l_aspectsed;
               END IF;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;                                            -- no problem
         END;
      END IF;

      IF g_frac_rec.commentaar IS NULL
      THEN
         g_frac_rec.commentaar := 'Uit conversie: monster';
      ELSE
         g_frac_rec.commentaar :=
                          g_frac_rec.commentaar || ', Uit conversie: monster';
      END IF;

      -- @@ 2DO: frty_id bepalen via mate_id, stgr_id, onwy_id en indi_id
      SELECT bas_frac_seq.NEXTVAL
        INTO g_frac_rec.frac_id
        FROM DUAL;

      INSERT INTO bas_fracties
                  (frac_id, mons_id,
                   fractienummer, controle, created_by, creation_date,
                   last_updated_by, last_update_date, status, frty_id,
                   opwe_id, cond_id, frac_id_parent,
                   commentaar, kafd_id
                  )
           VALUES (g_frac_rec.frac_id, g_mons_rec.mons_id,
                   g_frac_rec.fractienummer, 'N', c_user, SYSDATE,
                   c_user, SYSDATE, NULL, TO_NUMBER (NULL),
                   TO_NUMBER (NULL), TO_NUMBER (NULL), TO_NUMBER (NULL),
                   g_frac_rec.commentaar, c_kafd_id
                  );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception
                    (p_id_namen        => 'aspectsed | hoevsed | matcd | fractienr',
                     p_id_waarden      =>    p_aspectsed
                                          || ' | '
                                          || p_hoevsed
                                          || ' | '
                                          || p_matcd
                                          || ' | '
                                          || g_frac_rec.fractienummer
                    );
   END add_frac;

   PROCEDURE add_meti (p_stoftest IN VARCHAR2)
   IS
      l_stgr_id   NUMBER;
      l_opmerking_algemeen  VARCHAR2(2000);
   BEGIN
      g_plek := 'add_meti';
      g_plek := 'add_meti (stgr_id)';
      l_stgr_id :=
                CASE
                   WHEN p_stoftest IS NULL
                      THEN NULL
                   ELSE g_stgr_id (p_stoftest)
                END;
      g_plek := 'add_meti (insert)';

      SELECT bas_meti_seq.NEXTVAL
        INTO g_meti_rec.meti_id
        FROM DUAL;

      -- niet hier : !!! g_meti_rec                 := g_meti_rec_init;
      IF g_mons_rec.commentaar IS NOT NULL
      THEN
         IF INSTR(g_meti_rec.opmerking_algemeen,trim(g_mons_rec.commentaar)) = 0
            THEN
               l_opmerking_algemeen :=
               TRIM (g_mons_rec.commentaar) || ' >> ' || TRIM(g_meti_rec.opmerking_algemeen);
            ELSE
               l_opmerking_algemeen := TRIM(g_meti_rec.opmerking_algemeen);
         END IF;

         IF LENGTH(l_opmerking_algemeen) > 255
         THEN
            g_meti_rec.opmerking_algemeen := SUBSTR(l_opmerking_algemeen,1,255);
            INSERT INTO conv_opmerkingen (meti_id, opmerking)
            VALUES (g_meti_rec.meti_id, l_opmerking_algemeen);
         ELSE
            g_meti_rec.opmerking_algemeen := l_opmerking_algemeen;
         END IF;
      END IF;

      INSERT INTO bas_metingen
                  (meti_id, datum_aanmelding, afgerond, herhalen, snelheid,
                   prioriteit, declareren, created_by, creation_date,
                   last_updated_by, last_update_date, frac_id, onmo_id,
                   onde_id, stan_id, stgr_id, datum_afgerond, datum_conclusie,
                   setnaam,
                           -- cyto_onderz is null !!!
                           opmerking_algemeen, opmerking_analyse,

                   -- opmerking_analyse is null !!!
                   datum_selectie_robot, datum_uitslag_robot, conclusie,
                   mede_id, analysenr)
         SELECT g_meti_rec.meti_id, g_meti_rec.datum_aanmelding,
                CASE
                   WHEN p_stoftest = 'KWK_CON_PO'
                      THEN 'J'
                   ELSE g_meti_rec.afgerond
                END,
                g_meti_rec.herhalen, 'K', g_meti_rec.prioriteit, 'N', c_user,
                SYSDATE, c_user, SYSDATE, g_frac_rec.frac_id, onmo.onmo_id,
                onmo.onde_id, TO_NUMBER (NULL),
                CASE
                   WHEN p_stoftest IS NULL
                      THEN NULL
                   ELSE g_stgr_id (p_stoftest)
                END, g_meti_rec.datum_afgerond, g_meti_rec.datum_conclusie,
                NULL, g_meti_rec.opmerking_algemeen,
                g_meti_rec.opmerking_analyse, g_meti_rec.datum_selectie_robot,
                g_meti_rec.datum_uitslag_robot,
                NULL           --> wordt na toevoeging van meetwaarden bepaald
                    ,
                g_meti_rec.mede_id, NULL
           FROM bas_fracties frac, kgc_onderzoek_monsters onmo
          WHERE onmo.mons_id = frac.mons_id
            AND frac.frac_id = g_frac_rec.frac_id
            AND onmo.onde_id = g_onde_rec.onde_id
            AND onmo.mons_id = g_mons_rec.mons_id;

      g_meet_id_super := g_meti_rec.meti_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'stofcode | frac_id',
                        p_id_waarden      =>    p_stoftest
                                             || ' | '
                                             || g_frac_rec.frac_id
                       );
   END add_meti;

   PROCEDURE add_mw (
      p_mwst_code       IN   VARCHAR2,
      p_stof_code       IN   VARCHAR2,
      p_mede_id         IN   NUMBER,
      p_datum           IN   DATE,
      p_waarde          IN   VARCHAR2 DEFAULT NULL,
      p_meetreken       IN   VARCHAR2 DEFAULT 'M',
      p_meet_id_super   IN   NUMBER DEFAULT NULL
   )
   IS
      l_stof_id      NUMBER;
      l_mwst_id      NUMBER;
      l_commentaar   bas_meetwaarden.commentaar%TYPE;
      l_conclusie    bas_metingen.conclusie%TYPE;
   BEGIN
      g_plek := 'add_mw';

      BEGIN
         l_stof_id :=
              CASE
                 WHEN p_stof_code IS NULL
                    THEN NULL
                 ELSE g_stof_id (p_stof_code)
              END;                                                 -- stof_id
      EXCEPTION
         WHEN OTHERS
         THEN
            log_exception (p_id_namen        => 'stofcode',
                           p_id_waarden      => p_stof_code || ' is onbekend'
                          );
            RETURN;
      END;

      BEGIN
         l_mwst_id :=
              CASE
                 WHEN p_mwst_code IS NULL
                    THEN NULL
                 ELSE g_mwst_id (p_mwst_code)
              END;                                                 -- mwst_id
      EXCEPTION
         WHEN OTHERS
         THEN
            log_exception (p_id_namen        => 'mwstcode',
                           p_id_waarden      => p_mwst_code || ' is onbekend'
                          );
            RETURN;
      END;

      BEGIN
         IF g_onde_rec_o.afpcd IS NOT NULL AND g_onde_rec_o.afpcd <> 'X'
         THEN
            l_commentaar :=
               TRIM (   g_onde_rec_o.opmerking
                     || ' '
                     || g_afp_tekst (g_onde_rec_o.afpcd)
                    );
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            log_exception (p_id_namen        => 'afpcd',
                           p_id_waarden      =>    g_onde_rec_o.afpcd
                                                || ' is onbekend'
                          );
            RETURN;
      END;

      SELECT bas_meet_seq.NEXTVAL
        INTO g_meet_id
        FROM DUAL;

      g_plek := 'add_mw (insert)';

      INSERT INTO bas_meetwaarden
                  (meet_id, meti_id                                  -- meting
                                   ,
                   stof_id,
                   afgerond,
                   meet_reken, meetwaarde,
                   meeteenheid,
                   mwst_id                             -- meetwaarde-structuur
                          ,
                           --                   mest_id                                -- meetwaarde-status
                           mede_id, mede_id_controle,
                   datum_meting,
                   meet_id_super,
                   datum_afgerond,
                   spoed,
                   tonen_als,
                   eenheid, commentaar, created_by,
                   creation_date, last_updated_by, last_update_date
                  )
           VALUES (g_meet_id                                        -- meet_id
                            , g_meti_rec.meti_id                    -- meti_id
                                                ,
                   l_stof_id,
                   CASE
                      WHEN p_stof_code = 'KWK_CON_PO'
                         THEN 'J'
                      ELSE g_meti_rec.afgerond
                   END                                              --afgerond
                      ,
                   p_meetreken                                    --meet_reken
                              , p_waarde                         -- meetwaarde
                                        ,
                   CASE                                         -- meeteenheid
                      WHEN p_stof_code = 'AFP'
                         THEN '?g/ml'
                      ELSE NULL
                   END,
                   l_mwst_id,
                             --                   l_mest_id,
                             p_mede_id, NULL               -- mede_id_controle
                                            ,
                   p_datum   -- g_onde_rec.datum_autorisatie   -- datum_meting
                          ,
                   p_meet_id_super                            -- meet_id_super
                                  ,
                   p_datum -- g_onde_rec.datum_autorisatie   -- datum_afgerond
                          ,
                   NVL (g_onde_rec.spoed, 'N')                        -- spoed
                                              ,
                   NULL          -- BAS_MEET_00.toon_meetwaarde() -- tonen_als
                       ,
                   NULL                                             -- eenheid
                       , l_commentaar, c_user                    -- CREATED_BY
                                             ,
                   SYSDATE                                    -- CREATION_DATE
                          , c_user                          -- LAST_UPDATED_BY
                                  , SYSDATE
                  );                                       -- LAST_UPDATE_DATE
   -- NIET in deze procedure: g_meet_id_super := g_meet_id; !!
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'meti_id | stofcode',
                        p_id_waarden      =>    g_meti_rec.meti_id
                                             || ' | '
                                             || p_stof_code
                       );
   END add_mw;

   PROCEDURE add_mw (
      p_mwst_code       IN   VARCHAR2,
      p_stof_code       IN   VARCHAR2,
      p_mede_id         IN   NUMBER,
      p_datum           IN   VARCHAR2,
      p_waarde          IN   VARCHAR2,
      p_meetreken       IN   VARCHAR2 DEFAULT 'M',
      p_meet_id_super   IN   NUMBER DEFAULT NULL
   )
   IS
   BEGIN
      g_plek := 'add_mw 2';
      add_mw (p_mwst_code,
              p_stof_code,
              p_mede_id,
              geef_geldige_datum (p_datum),
              p_waarde,
              p_meetreken,
              p_meet_id_super
             );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'meti_id | stofcode',
                        p_id_waarden      =>    g_meti_rec.meti_id
                                             || ' | '
                                             || p_stof_code
                       );
   END add_mw;

   PROCEDURE add_mwd (
      p_volgorde   IN   INTEGER,
      p_prompt     IN   VARCHAR2,
      p_waarde     IN   VARCHAR2
   )
   IS
      l_mdet_id   NUMBER;
   BEGIN
      g_plek := 'add_mwd';

      IF p_waarde IS NULL AND g_meti_rec.afgerond = 'J'
      THEN
         RETURN;
      END IF;

      SELECT bas_mdet_seq.NEXTVAL
        INTO l_mdet_id
        FROM DUAL;

      INSERT INTO bas_meetwaarde_details
                  (mdet_id, meet_id, volgorde, prompt, waarde,
                   created_by, creation_date, last_updated_by,
                   last_update_date
                  )
           VALUES (l_mdet_id, g_meet_id, p_volgorde, p_prompt, p_waarde,
                   c_user, SYSDATE, c_user,
                   SYSDATE
                  );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'meti_id',
                        p_id_waarden      => g_meti_rec.meti_id
                       );
   END add_mwd;

   PROCEDURE add_mw_afp (
      p_waarde         VARCHAR2,
      p_mede_id   IN   NUMBER,
      p_datum     IN   VARCHAR2
   )
   IS
   BEGIN
      g_plek := 'add_mw_afp';

      IF p_waarde IS NULL AND g_meti_rec.afgerond = 'J'
      THEN
         RETURN;
      END IF;

      add_mw (p_mwst_code      => 'AFP',
              p_stof_code      => 'AFP',
              p_mede_id        => p_mede_id,
              p_datum          => geef_geldige_datum (p_datum),
              p_waarde         => p_waarde
             );
      add_mwd (p_volgorde      => 1,
               p_prompt        => 'AFP (?g/ml)',
               p_waarde        => p_waarde
              );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'meti_id',
                        p_id_waarden      => g_meti_rec.meti_id
                       );
   END add_mw_afp;

   PROCEDURE add_mw_ache (
      p_waarde         VARCHAR2,
      p_mede_id   IN   NUMBER,
      p_datum     IN   VARCHAR2
   )
   IS
   BEGIN
      g_plek := 'add_mw_ache';

      IF p_waarde IS NULL AND g_meti_rec.afgerond = 'J'
      THEN
         RETURN;
      END IF;

      add_mw (p_mwst_code      => 'ACHE',
              p_stof_code      => 'ACHE',
              p_mede_id        => p_mede_id,
              p_datum          => geef_geldige_datum (p_datum),
              p_waarde         => p_waarde
             );
      add_mwd (p_volgorde      => 1,
               p_prompt        => 'Interpretatie',
               p_waarde        => p_waarde
              );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'meti_id',
                        p_id_waarden      => g_meti_rec.meti_id
                       );
   END add_mw_ache;

   PROCEDURE add_mw_cyto_kweek
   IS
      CURSOR c_kweek_cyto (b_odbnr IN VARCHAR2)
      IS
         SELECT kweek.kweekcd
                             -- op het scherm wordt 400/550/850 getoond
                ,
                DECODE (kwaliteit,
                        'S', '400',
                        'R', '400',
                        'G', '550',
                        'Z', '850'
                       ) AS kwaliteit,
                kweek.kleurcd, kweek.cel_geteld, kweek.cel_micro,
                kweek.cel_foto
           --, kweek.opm altijd leeg op 3 gevallen na: daarbij staat: "Geen opmerking"
         FROM   cyto_kweek kweek
          WHERE kweek.odbnr = b_odbnr;

      l_num   INTEGER := 0;
   BEGIN
      g_plek := 'add_mw_cyto_kweek';

      FOR r_kweek IN c_kweek_cyto (g_odbnr)
      LOOP
         l_num := l_num + 1;

         -- voeg meetwaarde toe
         IF l_num = 1
         THEN
            add_mw (p_mwst_code          => 'KWK_CON_PO',
                    p_stof_code          => 'KWK_CON_PO',
                    p_mede_id            => g_meti_rec.mede_id,
                    p_datum              => g_meti_rec.datum_aanmelding,
                    p_waarde             => NULL,
                    p_meetreken          => 'S',
                    p_meet_id_super      => NULL
                   );
            g_meet_id_super := g_meet_id;
         END IF;

         add_mw (p_mwst_code          => 'KWK_CON_PO',
                 p_stof_code          => r_kweek.kweekcd,
                 p_mede_id            => g_meti_rec.mede_id,
                 p_datum              => g_meti_rec.datum_aanmelding,
                 p_waarde             => NULL,
                 p_meetreken          => 'M',
                 p_meet_id_super      => g_meet_id_super
                );
         -- voeg meetwaardedetails toe
         add_mwd (p_volgorde      => 1,
                  p_prompt        => 'Aantal cellen geteld',
                  p_waarde        => r_kweek.cel_geteld
                 );
         add_mwd (p_volgorde      => 2,
                  p_prompt        => 'Aantal cellen geanaliseerd',
                  p_waarde        => r_kweek.cel_micro
                 );                                                -- ?? OK ??
         add_mwd (p_volgorde      => 3,
                  p_prompt        => 'op foto',
                  p_waarde        => r_kweek.cel_foto
                 );
         add_mwd (p_volgorde      => 4,
                  p_prompt        => 'Kwaliteit',
                  p_waarde        => r_kweek.kwaliteit
                 );
         add_mwd (p_volgorde      => 5,
                  p_prompt        => 'Kleuring',
                  p_waarde        => r_kweek.kleurcd
                 );
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_cyto_kweek;

   PROCEDURE add_mw_prec_kweken (p_mwst_code VARCHAR2, p_stof_code VARCHAR2)
   IS
      -- p_stof_code IN ('AMN','COL','DIR','KAR' ....)
      CURSOR c_kwfr_prec (b_odbnr IN VARCHAR2, b_kwmedium IN VARCHAR2)
      IS
         SELECT kweek.nr                            -- kgc_kweekfracties.code
                        ,
                kweek.kwmedium
                              --> vertalen naar stoftest in combinatie met nr
                , kweek.kwk_kwal, kweek.klr_kwal, kweek.cel_geteld,
                kweek.cel_micro, kweek.cel_foto, kweek.kloon_geteld,
                kweek.getrypt, kweek.gebr_kleur
           FROM cyto_prec_kweken kweek
          WHERE kweek.odbnr = b_odbnr AND kwmedium = b_kwmedium;

      l_num   INTEGER := 0;
   BEGIN
      g_plek := 'add_mw_prec_kweken';

      FOR r_kweek IN c_kwfr_prec (g_odbnr, p_stof_code)
      LOOP
         l_num := l_num + 1;

         -- voeg meetwaarde toe
         IF l_num = 1
         THEN
            add_mw (p_mwst_code          => p_mwst_code,
                    p_stof_code          => p_mwst_code
                                                       -- nog niet p_stof_code !!
            ,
                    p_datum              => g_onde_rec.datum_autorisatie,
                    p_mede_id            => g_meti_rec.mede_id,
                    p_meetreken          => 'S',
                    p_meet_id_super      => NULL
                   );
            g_meet_id_super := g_meet_id;
         END IF;

         add_mw (p_mwst_code          => p_mwst_code,
                 p_stof_code          => p_stof_code || TO_CHAR (l_num)
                                                                       -- AMN1 t/m AMN7 e.d.
         ,
                 p_datum              => g_onde_rec.datum_autorisatie,
                 p_mede_id            => g_meti_rec.mede_id,
                 p_meetreken          => 'M',
                 p_meet_id_super      => g_meet_id_super
                );
         -- voeg meetwaardedetails toe
         add_mwd (p_volgorde      => 1,
                  p_prompt        => 'Getrypsineerd',
                  p_waarde        => r_kweek.getrypt
                 );
         add_mwd (p_volgorde      => 2,
                  p_prompt        => 'aant. onderz. cel.',
                  p_waarde        => r_kweek.cel_micro
                 );                                         -- niet cel_geteld
         add_mwd (p_volgorde      => 3,
                  p_prompt        => 'aant. onderz. klonen',
                  p_waarde        => r_kweek.kloon_geteld
                 );
         add_mwd (p_volgorde      => 4,
                  p_prompt        => 'Foto',
                  p_waarde        => r_kweek.cel_foto
                 );
         add_mwd (p_volgorde      => 5,
                  p_prompt        => 'Kwaliteit kleuring',
                  p_waarde        => r_kweek.klr_kwal
                 );
         add_mwd (p_volgorde      => 6,
                  p_prompt        => 'Kwaliteit kweek',
                  p_waarde        => r_kweek.kwk_kwal
                 );
         add_mwd (p_volgorde      => 7,
                  p_prompt        => 'Kleuring',
                  p_waarde        => r_kweek.gebr_kleur
                 );
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_prec_kweken;

   PROCEDURE add_mw_prec_kweken
   IS
   BEGIN
      g_plek := 'add_mw_prec_kweken';
      add_mw_prec_kweken ('KWK_CON_PR', 'AMN');
      add_mw_prec_kweken ('KWK_CON_PR', 'COL');
      add_mw_prec_kweken ('KWK_CON_PR', 'DIR');
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_prec_kweken;

   PROCEDURE add_mw_prec_uits (p_mwst_code IN VARCHAR2)
   IS
      CURSOR c_prec_uitslag (b_odbnr IN INTEGER)
      IS
         SELECT nr, kar, aant_cel, aant_kloon, kloon
           FROM cyto_prec_uitslag
          WHERE odbnr = b_odbnr;

      l_num   INTEGER := 0;
   BEGIN
      g_plek := 'add_mw_prec_uits';

      FOR r_uits IN c_prec_uitslag (g_odbnr)
      LOOP
         l_num := l_num + 1;

         -- voeg meetwaarde toe
         IF l_num = 1
         THEN
            add_mw (p_mwst_code          => p_mwst_code,
                    p_stof_code          => p_mwst_code
                                                       -- nog niet p_stof_code !!
            ,
                    p_datum              => g_onde_rec.datum_autorisatie,
                    p_mede_id            => g_meti_rec.mede_id,
                    p_meetreken          => 'S',
                    p_meet_id_super      => NULL
                   );
            g_meet_id_super := g_meet_id;
         END IF;

         add_mw (p_mwst_code          => p_mwst_code,
                 p_stof_code          =>    'KAR'
                                         || CASE
                                               WHEN p_mwst_code = 'ANA_CON_PR'
                                                  THEN '-'
                                               ELSE ''
                                            END
                                         || TO_CHAR (l_num)
                                                           -- KAR-1 t/m KAR-10 of KAR1 t/m KAR10
         ,
                 p_meetreken          => 'M',
                 p_datum              => g_onde_rec.datum_autorisatie,
                 p_mede_id            => g_meti_rec.mede_id,
                 p_meet_id_super      => g_meet_id_super
                );
         -- voeg meetwaardedetails toe
         add_mwd (p_volgorde      => 1,
                  p_prompt        => 'Karyogram',
                  p_waarde        => r_uits.kar
                 );
         add_mwd (p_volgorde      => 2,
                  p_prompt        => 'Aantal cellen',
                  p_waarde        => r_uits.aant_cel
                 );
         add_mwd (p_volgorde      => 3,
                  p_prompt        => 'van kloon nr',
                  p_waarde        => r_uits.kloon
                 );
         add_mwd (p_volgorde      => 4,
                  p_prompt        => 'Glaasje',
                  p_waarde        => r_uits.nr
                 );
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_prec_uits;

   PROCEDURE add_mw_kary (p_stof_code IN VARCHAR2)
   IS
   BEGIN
      g_plek := 'add_mw_kary';
      -- vul commentaar in meetwaarde aan met karyotype
      g_onde_rec_o.opmerking :=
               TRIM (g_onde_rec_o.opmerking || ' ' || g_onde_rec_o.karyotype);
      add_mw (p_mwst_code          => 'KGCKARY01',
              p_stof_code          => p_stof_code,
              p_datum              => g_onde_rec.datum_autorisatie,
              p_mede_id            => g_meti_rec.mede_id,
              p_meetreken          => 'M',
              p_meet_id_super      => NULL,
              p_waarde             => g_onde_rec_o.karyotype
             );
      add_mwd (p_volgorde      => 1,
               p_prompt        => 'Karyotype',
               p_waarde        => g_onde_rec_o.karyotype
              );
      add_mwd (p_volgorde      => 2,
               p_prompt        => 'Afwijking',
               p_waarde        => g_onde_rec_o.afwijkend
              );
      /* niet vanuit migratie vullen, maar achteraf wel mogelijk handmatig,
       tenzij de meting al afgerond is */
      -- geteld --> bij OPM --> staat al bij opmerking
      add_mwd (p_volgorde => 3, p_prompt => 'Geteld', p_waarde => NULL);
      -- geanalyseerd --> wordt soms bij opmerking geplaatst
      add_mwd (p_volgorde => 4, p_prompt => 'Geanalyseerd', p_waarde => NULL);
      add_mwd (p_volgorde => 5, p_prompt => 'Chr.1 t/m 22', p_waarde => NULL);
      add_mwd (p_volgorde => 6, p_prompt => 'XY', p_waarde => NULL);
      add_mwd (p_volgorde => 7, p_prompt => 'EQAS', p_waarde => NULL);
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_kary;

   PROCEDURE add_mw_haem_kweek
   IS
      CURSOR c_haem_kwk (b_odbnr NUMBER)
      IS
         SELECT kweekcd, kwaliteit, kleurcd, cel_geteld, cel_micro, cel_foto
           FROM cyto_haem_kweken
          WHERE odbnr = b_odbnr;

      l_num   INTEGER := 0;
   BEGIN
      g_plek := 'add_mw_haem_kweek';

      FOR r_kwk IN c_haem_kwk (g_odbnr)
      LOOP
         l_num := l_num + 1;

         -- voeg meetwaarde toe
         IF l_num = 1
         THEN
            add_mw (p_mwst_code          => 'KWK_CON_TU',
                    p_stof_code          => 'KWK_CON_TU',
                    p_datum              => g_onde_rec.datum_autorisatie,
                    p_mede_id            => g_meti_rec.mede_id,
                    p_meetreken          => 'S',
                    p_meet_id_super      => NULL
                   );
            g_meet_id_super := g_meet_id;
         END IF;

         add_mw (p_mwst_code          => 'KWK_CON_TU',
                 p_stof_code          => '*' || r_kwk.kweekcd
                                                             -- '*' + materiaalcode
         ,
                 p_datum              => g_onde_rec.datum_autorisatie,
                 p_mede_id            => g_meti_rec.mede_id,
                 p_meetreken          => 'M',
                 p_meet_id_super      => g_meet_id_super
                );
         -- voeg meetwaardedetails toe
         add_mwd (p_volgorde      => 1,
                  p_prompt        => 'Aant. cel gescr.',
                  p_waarde        => r_kwk.cel_geteld
                 );
         add_mwd (p_volgorde      => 2,
                  p_prompt        => 'Aant. cel geanal.',
                  p_waarde        => r_kwk.cel_micro
                 );
         add_mwd (p_volgorde      => 3,
                  p_prompt        => 'Totaal',
                  p_waarde        => r_kwk.cel_geteld + r_kwk.cel_micro
                 );
         add_mwd (p_volgorde      => 4,
                  p_prompt        => 'Foto                 Kwaliteit',
                  p_waarde        =>    RPAD (r_kwk.cel_foto, 21)
                                     || r_kwk.kwaliteit
                 );
         add_mwd (p_volgorde      => 5,
                  p_prompt        => 'Kleuring',
                  p_waarde        => r_kwk.kleurcd
                 );
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_haem_kweek;

   PROCEDURE add_mw_cyto_uits
   IS
      CURSOR c_uits_cyto (b_odbnr IN NUMBER)
      IS
         SELECT kar, aant_cel, kweekcd
           FROM cyto_labuitslag
          WHERE odbnr = b_odbnr;

      l_num        INTEGER                    := 0;
      l_stofcode   kgc_stoftesten.code%TYPE;
   BEGIN
      g_plek := 'add_mw_cyto_uits';

      FOR r_uits IN c_uits_cyto (g_odbnr)
      LOOP
         l_num := l_num + 1;

         IF l_num = 1
         THEN
            add_mw (p_mwst_code          => 'ANA_CON_PO',
                    p_stof_code          => 'ANA_CON_PO',
                    p_datum              => g_onde_rec.datum_autorisatie,
                    p_mede_id            => g_meti_rec.mede_id,
                    p_meetreken          => 'S',
                    p_meet_id_super      => NULL
                   );
            g_meet_id_super := g_meet_id;
         END IF;

         /*
               SELECT DECODE( TRIM( r_uits.kweekcd )
                            , '1', '1'
                            , '2', '2'
                            , '3', '3'
                            , '4', '4'
                            , '-5', '5'
                            , '5', '5'
                            , '13A', '13'
                            , '13B', '13'
                            , '13B', '13'
                            , '13C', '13'
                            , '13D', '13'
                            , '01`', '1'
                            , '0-5', '5'
                            , 'RPMI', '5'
                            , 'HR', '5'
                            , 'L', '1'
                            , 'R', '1'
                            , 'ONB' )
               INTO   l_stofcode
               FROM   DUAL;
               IF l_stofcode <> 'ONB' THEN
                 l_stofcode                 := 'KAR-' || l_stofcode;
               END IF;
         */
         add_mw (p_mwst_code          => 'ANA_CON_PO',
                 p_stof_code          => 'KAR' || c_uits_cyto%ROWCOUNT,
                 p_datum              => g_onde_rec.datum_autorisatie,
                 p_mede_id            => g_meti_rec.mede_id,
                 p_meetreken          => 'M',
                 p_meet_id_super      => g_meet_id_super
                --, p_waarde           => r_uits.kar
                );
         -- voeg meetwaardedetails toe
         add_mwd (p_volgorde      => 1,
                  p_prompt        => 'Karyotype'       --'          Karyotype'
                                                ,
                  p_waarde        => r_uits.kar
                 );
         add_mwd (p_volgorde      => 2,
                  p_prompt        => 'Aantal cellen',
                  p_waarde        => r_uits.aant_cel
                 );
         add_mwd (p_volgorde      => 3,
                  p_prompt        => 'uit kweeksoort',
                  p_waarde        => r_uits.kweekcd
                 );
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_cyto_uits;

   PROCEDURE add_mw_haem_uits
   IS
      CURSOR c_uits_haem (b_odbnr IN NUMBER)
      IS
         SELECT kweekcd, n, nrl, ka, karl, nka, nkarl, gesl
           FROM cyto_haem_uitslag
          WHERE odbnr = b_odbnr;

      l_num         NUMBER := 0;
      l_tot_n       NUMBER := 0;
      l_tot_ka      NUMBER := 0;
      l_tot_nka     NUMBER := 0;
      l_tot_nrl     NUMBER := 0;
      l_tot_karl    NUMBER := 0;
      l_tot_nkarl   NUMBER := 0;
   BEGIN
      g_plek := 'add_mw_haem_uits';

      FOR r_uits IN c_uits_haem (g_odbnr)
      LOOP
         l_num := l_num + 1;

         -- voeg meetwaarde toe
         IF l_num = 1
         THEN
            add_mw (p_mwst_code          => 'ANA_CON_TU',
                    p_stof_code          => 'ANA_CON_TU',
                    p_datum              => g_onde_rec.datum_autorisatie,
                    p_mede_id            => g_meti_rec.mede_id,
                    p_meetreken          => 'S',
                    p_meet_id_super      => NULL
                   );
            g_meet_id_super := g_meet_id;
         END IF;

         add_mw (p_mwst_code          => 'ANA_CON_TU',
                 p_stof_code          => '/' || r_uits.kweekcd
                                                              -- '/' + materiaalcode
         ,
                 p_datum              => g_onde_rec.datum_autorisatie,
                 p_mede_id            => g_meti_rec.mede_id,
                 p_meetreken          => 'M',
                 p_meet_id_super      => g_meet_id_super
                );
         -- voeg meetwaardedetails toe
         add_mwd (p_volgorde      => 1,
                  p_prompt        => 'Norm          (RL)',
                  p_waarde        =>    RPAD (LPAD (TO_CHAR (r_uits.n), 2),
                                              16)
                                     || '( '
                                     || LPAD (TO_CHAR (r_uits.nrl), 2)
                                     || ' )'
                 );
         l_tot_n := l_tot_n + NVL (r_uits.n, 0);
         l_tot_nrl := l_tot_nrl + NVL (r_uits.nrl, 0);
         add_mwd (p_volgorde      => 2,
                  p_prompt        => 'Klon afw      (RL)',
                  p_waarde        =>    RPAD (LPAD (TO_CHAR (r_uits.ka), 2),
                                              16
                                             )
                                     || '( '
                                     || LPAD (TO_CHAR (r_uits.karl), 2)
                                     || ' )'
                 );
         l_tot_ka := l_tot_ka + NVL (r_uits.ka, 0);
         l_tot_karl := l_tot_karl + NVL (r_uits.karl, 0);
         add_mwd (p_volgorde      => 3,
                  p_prompt        => 'Niet Klon afw (RL)',
                  p_waarde        =>    RPAD (LPAD (TO_CHAR (r_uits.nka), 2),
                                              16
                                             )
                                     || '( '
                                     || LPAD (TO_CHAR (r_uits.nkarl), 2)
                                     || ' )'
                 );
         l_tot_nka := l_tot_nka + NVL (r_uits.nka, 0);
         l_tot_nkarl := l_tot_nkarl + NVL (r_uits.nkarl, 0);
         add_mwd (p_volgorde      => 4,
                  p_prompt        => 'Totaal',
                  p_waarde        => TO_CHAR (  NVL (r_uits.n, 0)
                                              + NVL (r_uits.ka, 0)
                                              + NVL (r_uits.nka, 0)
                                             )
                 );
      END LOOP;

      -- totaal-generaal toevoegen
      add_mw (p_mwst_code          => 'ANA_CON_TU',
              p_stof_code          => 'TOTAAL',
              p_datum              => g_onde_rec.datum_autorisatie,
              p_mede_id            => g_meti_rec.mede_id,
              p_meetreken          => 'M',
              p_meet_id_super      => g_meet_id_super
             );
      add_mwd (p_volgorde      => 5,
               p_prompt        => 'Norm          (RL)',
               p_waarde        =>    RPAD (LPAD (TO_CHAR (l_tot_n), 2), 16)
                                  || '( '
                                  || LPAD (TO_CHAR (l_tot_nrl), 2)
                                  || ' )'
              );
      add_mwd (p_volgorde      => 6,
               p_prompt        => 'Klon afw      (RL)',
               p_waarde        =>    RPAD (LPAD (TO_CHAR (l_tot_ka), 2), 16)
                                  || '( '
                                  || LPAD (TO_CHAR (l_tot_karl), 2)
                                  || ' )'
              );
      add_mwd (p_volgorde      => 7,
               p_prompt        => 'Niet Klon afw (RL)',
               p_waarde        =>    RPAD (LPAD (TO_CHAR (l_tot_nka), 2), 16)
                                  || '( '
                                  || LPAD (TO_CHAR (l_tot_nkarl), 2)
                                  || ' )'
              );
      add_mwd (p_volgorde      => 8,
               p_prompt        => 'Totaal',
               p_waarde        => TO_CHAR (l_tot_n + l_tot_ka + l_tot_nka)
              );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_haem_uits;

   PROCEDURE add_mw_fish
   IS
   BEGIN
      g_plek := 'add_mw_fish';

      -- KleurCd ISH in CYTO_KWEEK
      IF g_meti_rec.afgerond = 'J' AND g_meti_rec.opmerking_algemeen IS NULL
      THEN
         RETURN;
      END IF;

      add_mw (p_mwst_code          => 'FISH_CONV',
              p_stof_code          => 'FISH_CONV',
              p_datum              => g_onde_rec.datum_autorisatie,
              p_mede_id            => g_meti_rec.mede_id,
              p_meetreken          => 'M',
              p_meet_id_super      => NULL
             );
      /* @@ NOT2DO
      - FISH
        - kweeksoort --> wordt niet ingevuld in het lab (wel door autorisator?? --> Nee)
        - opmerking  --> staat al bij meting
      */
      add_mwd (p_volgorde      => 1,
               p_prompt        => 'Karyotype',
               p_waarde        => g_onde_rec_o.karyotype
              );
      add_mwd (p_volgorde      => 2,
               p_prompt        => 'Opmerking',
               p_waarde        => g_meti_rec.opmerking_algemeen
              );
   -- @@2DO: Check kweek bij Fish
   /*
   add_mwd (p_volgorde      => 4,
            p_prompt        => 'Kleuring',
            p_waarde        => g_meti_rec.opmerking_algemeen
           );
   add_mwd (p_volgorde      => 2,
            p_prompt        => 'Status',
            p_waarde        => g_meti_rec.opmerking_algemeen
           );
    */
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_fish;

   PROCEDURE add_mw_mlpa
   IS
   BEGIN
      g_plek := 'add_mw_mlpa';

      IF g_meti_rec.afgerond = 'J' AND g_meti_rec.opmerking_algemeen IS NULL
      THEN
         RETURN;
      END IF;

      add_mw (p_mwst_code          => 'MLPA_CONV',
              p_stof_code          => 'MLPA_CONV',
              p_datum              => g_onde_rec.datum_autorisatie,
              p_mede_id            => g_meti_rec.mede_id,
              p_meetreken          => 'M',
              p_meet_id_super      => NULL
             );
      /*
            - MLPA
              - opmerking  --> staat al bij meting
              - doorgebeld --> staat bij reden onderzoek
      */
      add_mwd (p_volgorde      => 1,
               p_prompt        => 'Karyotype',
               p_waarde        => g_onde_rec_o.karyotype
              );
      add_mwd (p_volgorde      => 2,
               p_prompt        => 'opmerking',
               p_waarde        => g_meti_rec.opmerking_algemeen
              );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_mw_mlpa;

   PROCEDURE upd_mw_conc
   IS
   /*    CURSOR c_mw( b_meet_id IN NUMBER ) IS
         SELECT meetwaarde
         FROM   bas_meetwaarden
         WHERE  meet_id = b_meet_id;
       l_conc  bas_metingen.conclusie%TYPE;
   */
   BEGIN
      g_plek := 'upd_mw_conc';
--    g_meti_rec.conclusie       :=
--                TRIM( g_meti_rec.conclusie || ' ' || g_onde_rec_o.karyotype );
      UPDATE bas_metingen
         SET conclusie =
                TRIM (conclusie || ' ' || g_onde_rec_o.karyotype)
                                                        --g_meti_rec.conclusie
       WHERE onde_id = g_onde_rec.onde_id;     --meti_id = g_meti_rec.meti_id;

     /*    BAS_BERE_00.g_in_progress := FALSE;
         BAS_BERE_00.berekening( p_meet_id =>
     , p_meti_id  =>@@@@@@@@@@
     , p_bety_id  =>
     , p_bewy_id  =>
     , p_form_id  =>
     , p_negeer_afgerond  => 'N'
     , p_commit  => TRUE);
     */    -- conclusie van meting aanvullen
           /*  veroorzaakt:
               ORA-20999: --> user specified error
   HELIX.CG$ERRORS
   HELIX.QMS$ERRORS
               ORA-20998:
           */
       /*    FOR r_mw IN c_mw( g_meti_rec.meti_id ) LOOP
             g_meti_rec.conclusie       :=
               kgc_mere_00.resultaat
                                  ( p_meet_id          => g_meet_id
                                  , p_stof_id          => CASE
                                      WHEN p_stof_code IS NULL THEN NULL
                                      ELSE g_stof_id( p_stof_code )
                                    END
                                  , p_afgerond         => 'J'
                                  , p_waarde           => p_waarde
                                  , p_tekst            => l_conclusie
                                  , p_meti_afgerond    => 'J' );
           END LOOP;
           UPDATE bas_metingen
           SET conclusie = g_meti_rec.conclusie
           WHERE  meti_id = g_meti_rec.meti_id;
           g_meti_rec.conclusie       := NULL;
           */
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'meti_id',
                        p_id_waarden      => g_meti_rec.meti_id
                       );
   END upd_mw_conc;

   PROCEDURE add_brief
   IS
      l_afgedaan   BOOLEAN;
      -- l_brty_id    INTEGER;
      l_brie_id    INTEGER;
   BEGIN
      g_plek := 'add_brief';
      -- @@2DO: check ***_id's NULL
      l_afgedaan :=
         kgc_onde_00.afgerond (p_onde_id      => g_onde_rec.onde_id,
                               p_uits_id      => NULL,
                               p_mons_id      => NULL,
                               p_frac_id      => NULL,
                               p_meti_id      => NULL,
                               p_meet_id      => NULL,
                               p_rftf         => FALSE
                              );

      SELECT kgc_brie_seq.NEXTVAL
        INTO l_brie_id
        FROM DUAL;

      INSERT INTO kgc_brieven
                  (brie_id, kafd_id, pers_id,
                   datum_print,
                   kopie, created_by, creation_date, last_updated_by,
                   last_update_date, brty_id, onde_id,
                   rela_id, uits_id,
                   zwan_id,
                   datum_verzonden,
                   geadresseerde,
                   decl_id, onbe_id, obmg_id
                  )
           VALUES (l_brie_id, c_kafd_id, g_onde_rec.pers_id,
                   NVL (g_uits_rec.datum_uitslag, SYSDATE)      -- datum_print
                                                          ,
                   'N'                                                -- kopie
                      , c_user, SYSDATE, c_user,
                   SYSDATE, NULL                                 -- brief-type
                                , g_onde_rec.onde_id,
                   g_uits_rec.rela_id, g_uits_rec.uits_id,
                   TO_NUMBER (NULL)                                 -- zwan_id
                                   ,
                   TO_DATE (NULL)                           -- datum_verzonden
                                 ,
                   LTRIM
                      (RTRIM
                          (NVL
                              (g_uits_rec.volledig_adres,
                               DECODE
                                   (g_uits_rec.rela_id,
                                    NULL, kgc_adres_00.relatie
                                                          (g_uits_rec.rela_id,
                                                           'J'
                                                          ),
                                    kgc_adres_00.persoon (g_onde_rec.pers_id,
                                                          'J'
                                                         )
                                   )
                              )
                          )
                      ),
                   TO_NUMBER (NULL), TO_NUMBER (NULL), TO_NUMBER (NULL)
                  );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'onde_id',
                        p_id_waarden      => g_onde_rec.onde_id
                       );
   END add_brief;

   PROCEDURE add_uits
   IS
   BEGIN
      g_plek := 'add_uits';
      g_uits_rec.onde_id := g_onde_rec.onde_id;
      g_uits_rec.kafd_id := c_kafd_id;
      g_uits_rec.rela_id := g_onde_rec.rela_id;
      g_uits_rec.mede_id := g_onde_rec.mede_id_autorisator;
      g_uits_rec.datum_autorisatie := g_onde_rec.datum_autorisatie;
      g_uits_rec.datum_uitslag := g_uits_rec.datum_autorisatie;
      g_uits_rec.volledig_adres :=
                               kgc_adres_00.relatie (g_uits_rec.rela_id, 'J');
      -- g_uits_rec.brty_id := c_brty_id;
      g_uits_rec.tekst :=
         TRIM (   g_onde_rec_o.toelichting1
               || ' '
               || g_onde_rec_o.toelichting2
               || ' '
               || g_onde_rec_o.toelichting3
               || ' '
               || g_onde_rec_o.toelichting4
               || ' '
               || g_onde_rec_o.toelichting5
               || ' '
               || g_onde_rec_o.toelichting6
               || ' '
               || g_onde_rec_o.toelichting7
               || ' '
               || g_onde_rec_o.toelichting8
               || ' '
               || g_onde_rec_o.toelichting9
               || ' '
               || g_onde_rec_o.toelichting10
               || ' '
               || g_onde_rec_o.toelichting11
               || ' '
               || g_onde_rec_o.toelichting12
              );

      SELECT kgc_uits_seq.NEXTVAL
        INTO g_uits_rec.uits_id
        FROM DUAL;

      INSERT INTO kgc_uitslagen
                  (uits_id, onde_id,
                   kafd_id, rela_id, created_by, creation_date,
                   last_updated_by, last_update_date, brty_id,
                   mede_id, datum_uitslag,
                   datum_autorisatie, volledig_adres,
                   tekst, commentaar,
                   toelichting, mede_id2
                  )
           VALUES (g_uits_rec.uits_id, g_uits_rec.onde_id,
                   g_uits_rec.kafd_id, g_uits_rec.rela_id, c_user, SYSDATE,
                   c_user, SYSDATE, NULL       -- brieftype g_uits_rec.brty_id
                                        ,
                   g_uits_rec.mede_id, g_uits_rec.datum_uitslag,
                   g_uits_rec.datum_autorisatie, g_uits_rec.volledig_adres,
                   g_uits_rec.tekst, g_uits_rec.commentaar,
                   NULL                                         -- toelichting
                       , TO_NUMBER (NULL)                          -- mede_id2
                  );
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'onde_id',
                        p_id_waarden      => g_uits_rec.onde_id
                       );
   END add_uits;

   PROCEDURE add_onderzoek_indicaties
   IS
      l_num_ind   INTEGER := 0;
   BEGIN
      g_plek := 'add_onderzoek_indicaties';

      IF g_onde_rec_o.indicatie1 IS NOT NULL
      THEN
         l_num_ind := l_num_ind + 1;
         indicaties_vullen (p_indicatie      => g_onde_rec_o.indicatie1,
                            p_num_ind        => l_num_ind
                           );
      END IF;

      IF g_onde_rec_o.indicatie2 IS NOT NULL
      THEN
         l_num_ind := l_num_ind + 1;
         indicaties_vullen (p_indicatie      => g_onde_rec_o.indicatie2,
                            p_num_ind        => l_num_ind
                           );
      END IF;

      IF g_onde_rec_o.indicatie3 IS NOT NULL
      THEN
         l_num_ind := l_num_ind + 1;
         indicaties_vullen (p_indicatie      => g_onde_rec_o.indicatie3,
                            p_num_ind        => l_num_ind
                           );
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'onde_id',
                        p_id_waarden      => g_uits_rec.onde_id
                       );
   END add_onderzoek_indicaties;

   PROCEDURE add_kopiehouders
   IS
   BEGIN
      g_plek := 'add_kopiehouders';
      g_kopiehouders1 :=
         CASE
            WHEN g_onde_rec_o.kopiehouder1 IS NULL
               THEN NULL
            ELSE g_rela_id (g_onde_rec_o.kopiehouder1)
         END;
      g_kopiehouders2 :=
         CASE
            WHEN g_onde_rec_o.kopiehouder2 IS NULL
               THEN NULL
            ELSE g_rela_id (g_onde_rec_o.kopiehouder2)
         END;
      g_kopiehouders3 :=
         CASE
            WHEN g_onde_rec_o.kopiehouder3 IS NULL
               THEN NULL
            ELSE g_rela_id (g_onde_rec_o.kopiehouder3)
         END;
      g_kopiehouders4 :=
         CASE
            WHEN g_onde_rec_o.kopiehouder4 IS NULL
               THEN NULL
            ELSE g_rela_id (g_onde_rec_o.kopiehouder4)
         END;

      IF g_kopiehouders1 IS NOT NULL
      THEN
         kopiehouders_vullen (p_kopiehouder      => g_kopiehouders1,
                              p_uits_id          => g_uits_rec.uits_id
                             );
      END IF;

      IF     g_kopiehouders2 IS NOT NULL
         AND (g_kopiehouders2 <> g_kopiehouders1 OR g_kopiehouders1 IS NULL)
      THEN
         kopiehouders_vullen (p_kopiehouder      => g_kopiehouders2,
                              p_uits_id          => g_uits_rec.uits_id
                             );
      END IF;

      IF     g_kopiehouders3 IS NOT NULL
         AND (g_kopiehouders3 <> g_kopiehouders2 OR g_kopiehouders2 IS NULL)
         AND (g_kopiehouders3 <> g_kopiehouders1 OR g_kopiehouders1 IS NULL)
      THEN
         kopiehouders_vullen (p_kopiehouder      => g_kopiehouders3,
                              p_uits_id          => g_uits_rec.uits_id
                             );
      END IF;

      IF     g_kopiehouders4 IS NOT NULL
         AND (g_kopiehouders4 <> g_kopiehouders3 OR g_kopiehouders3 IS NULL)
         AND (g_kopiehouders4 <> g_kopiehouders2 OR g_kopiehouders2 IS NULL)
         AND (g_kopiehouders4 <> g_kopiehouders1 OR g_kopiehouders1 IS NULL)
      THEN
         kopiehouders_vullen (p_kopiehouder      => g_kopiehouders4,
                              p_uits_id          => g_uits_rec.uits_id
                             );
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen        => 'onde_id',
                        p_id_waarden      => g_uits_rec.onde_id
                       );
   END add_kopiehouders;

   PROCEDURE upd_onderz_afgerond
   IS
   BEGIN
      UPDATE kgc_onderzoeken
         SET afgerond = 'J'
       WHERE onderzoeknr = g_onde_rec_o.onderznr;
   END upd_onderz_afgerond;

   PROCEDURE vul_onderzoek_tabs
   IS
      l_num   NUMBER;
   BEGIN
      g_plek := 'vul_onderzoek_tabs';

      -- @@ alle VAX-onderzoeken verwerkt?
      -- eerst (pers)onderzoeken zonder bijbehorende onderzoeken in haem/prec/cyto
      FOR onde_rec IN onde_cur
      LOOP
         g_onde_rec_o.onderznr := onde_rec.onderznr;
         g_onde_rec_o.afdeling := onde_rec.afdeling;
         g_onde_rec_o.persnr := onde_rec.persnr;
         g_onde_rec_o.binnendat := '01-01-' || TO_CHAR (onde_rec.jaar);
         g_onde_rec_o.onde_afgerond := 'J';
         g_onde_rec_o.declaratie := NULL;               -- declareren := 'N';
         add_onderzoek;
      END LOOP;

      FOR onde_rec_o IN onde_cur_o
      LOOP
         g_onde_rec_o := onde_rec_o;
         g_uits_rec := g_uits_rec_init;
         -- @@ voeg onderzoek en onderzoek-indicatie(s) toe
         add_onderzoek;
         add_onderzoek_indicaties;

         -- declaratie: niet overlaten aan standaard: c-nummers zijn ook gedeclareerd!
         -- @@ mag ondezoek gedeclareerd worden?
         IF g_onde_rec.declareren = 'J'
         THEN
            add_decl;                               -- @@ voeg declaratie toe
         END IF;

         -- @@ voeg monster, onderzoek_monster, fractie,  meting en notitie toe
         add_monster;
         g_frac_rec := g_frac_rec_init;

         IF g_onde_rec.onderzoeknr LIKE 'A%'
         THEN
            g_frac_rec.fractienummer := g_mons_rec.monsternummer || '_CEL';
         ELSE
            g_frac_rec.fractienummer := g_mons_rec.monsternummer;
         END IF;

         add_frac (g_onde_rec_o.aspectsed,
                   g_onde_rec_o.hoevsed,
                   g_onde_rec_o.mate_code
                  );
         g_plek := 'vul_onderzoek_tabs';
         g_meti_rec := g_meti_rec_init;
         g_meti_rec.afgerond := NVL (g_onde_rec_o.afgerond, 'N');
         g_meti_rec.prioriteit := NVL (g_onde_rec_o.spoed, 'N');

         BEGIN
            g_meti_rec.mede_id :=
               CASE
                  WHEN g_onde_rec_o.analyse_door IS NULL
                     THEN NULL
                  ELSE g_mede_id (g_onde_rec_o.analyse_door)
               END;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               g_plek := 'vul_onderzoek_tabs';
               log_exception (p_id_namen        => 'analyse_door',
                              p_id_waarden      => g_onde_rec_o.analyse_door
                             );
         END;

         g_meti_rec.datum_aanmelding :=
                    NVL (geef_geldige_datum (g_onde_rec_o.binnendat), SYSDATE);
         g_meti_rec.datum_afgerond :=
                                   geef_geldige_datum (g_onde_rec_o.afronddat);
         g_meti_rec.opmerking_algemeen := g_onde_rec_o.opmerking;
         g_meti_rec.datum_selectie_robot := TO_DATE (NULL);
         g_meti_rec.datum_uitslag_robot := TO_DATE (NULL);
         g_meti_rec.herhalen := 'N';

         -- bas_metingen, bas_meetwaarden en bas_meetwaarde_details en eventueel nog 1 fractie
         IF g_onde_rec.onderzoeknr LIKE 'A%'
         THEN                                                 -- Amnio -> prec
            -- meting toevoegen
            add_meti ('KWK_CON_PR');
            add_mw_prec_kweken;                      -- meetwaarden toevoegen
            -- upd_mw_conc;
            add_meti ('ANA_CON_PR');
            add_mw_prec_uits ('ANA_CON_PR');         -- meetwaarden toevoegen
            -- upd_mw_conc;
            add_meti ('KARYO-PR');
            add_mw_kary ('KARYO-PR');
            upd_mw_conc;
            g_plek := 'vul_onderzoek_tabs';
            -- tweede fractie toevoegen
            g_frac_rec := g_frac_rec_init;
            g_frac_rec.fractienummer := g_mons_rec.monsternummer || '_SUP';
            add_frac (g_onde_rec_o.aspectsed,
                      g_onde_rec_o.hoevsed,
                      g_onde_rec_o.mate_code
                     );
            g_plek := 'vul_onderzoek_tabs';

            -- X = niet verricht
            IF g_onde_rec_o.afp IS NOT NULL AND g_onde_rec_o.afpcd <> 'X'
            THEN
               add_meti ('AFP');

               BEGIN
                  add_mw_afp
                     (p_waarde       => g_onde_rec_o.afp,
                      p_mede_id      => CASE
                         WHEN g_onde_rec_o.afp_door IS NULL
                            THEN NULL
                         ELSE g_mede_id (g_onde_rec_o.afp_door)
                      END,
                      p_datum        => g_onde_rec_o.afpdat
                     );
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     g_plek := 'vul_onderzoek_tabs';
                     log_exception (p_id_namen        => 'afp_door',
                                    p_id_waarden      => g_onde_rec_o.afp_door
                                   );
               END;

               -- upd_mw_conc;   -- conclusie bij meting bijwerken
               g_plek := 'vul_onderzoek_tabs';
            END IF;

            IF g_onde_rec_o.achecd IS NOT NULL AND g_onde_rec_o.achecd <> 'X'
            THEN
               add_meti ('ACHE');

               BEGIN
                  add_mw_ache
                     (p_waarde       => g_onde_rec_o.achecd,
                      p_mede_id      => CASE
                         WHEN g_onde_rec_o.ache_door IS NULL
                            THEN NULL
                         ELSE g_mede_id (g_onde_rec_o.ache_door)
                      END,
                      p_datum        => g_onde_rec_o.achedat
                     );
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     g_plek := 'vul_onderzoek_tabs';
                     log_exception (p_id_namen        => 'ache_door',
                                    p_id_waarden      => g_onde_rec_o.ache_door
                                   );
               END;

               -- upd_mw_conc;   -- conclusie bij meting bijwerken
               g_plek := 'vul_onderzoek_tabs';
            END IF;
         ELSIF g_onde_rec.onderzoeknr LIKE 'C%'
         THEN                                                -- Chorio -> prec
            add_meti ('KWK_CON_PR');
            add_mw_prec_kweken;                      -- meetwaarden toevoegen
            -- upd_mw_conc;   -- conclusie bij meting bijwerken
            add_meti ('ANA_CON_PR');
            add_mw_prec_uits ('ANA_CON_PR');         -- meetwaarden toevoegen
            -- upd_mw_conc;   -- conclusie bij meting bijwerken
            add_meti ('KARYO-PR');
            add_mw_kary ('KARYO-PR');
            upd_mw_conc;                    -- conclusie bij meting bijwerken
         ELSIF g_onde_rec.onderzoeknr LIKE 'F%'
         THEN                                            -- Fibroblast -> cyto
            add_meti ('KWK_CON_PO');
            add_mw_cyto_kweek;                       -- meetwaarden toevoegen
            -- upd_mw_conc;   -- conclusie bij meting bijwerken
            upd_onderz_afgerond;
            add_meti ('ANA_CON_PO');
            add_mw_cyto_uits;                        -- meetwaarden toevoegen
            -- upd_mw_conc;   -- conclusie bij meting bijwerken
            add_meti ('KARYO-PO');
            add_mw_kary ('KARYO-PO');                -- meetwaarden toevoegen
            upd_mw_conc;                    -- conclusie bij meting bijwerken
         ELSIF g_onde_rec.onderzoeknr LIKE 'H%'
         THEN                                          -- Haemotologie -> haem
            add_meti ('KARYO-PO');
            add_mw_kary ('KARYO-PO');                -- meetwaarden toevoegen
            upd_mw_conc;                    -- conclusie bij meting bijwerken
            add_meti ('KWK_CON_TU');

            SELECT COUNT (1)
              INTO l_num
              FROM cyto_dubbele_odbnrs
             WHERE onderznr = g_onde_rec.onderzoeknr
               AND onderznr NOT IN ('H98/0132', 'H99/0238', 'H99/0241');

            -- meetwaarden toevoegen
            IF l_num > 0
            THEN
               -- in bepaalde gevallen zijn haem-kweken toegevoegd aan cyto-kweken
               add_mw_cyto_kweek;
            ELSE
               add_mw_haem_kweek;
            END IF;

            -- upd_mw_conc;   -- conclusie bij meting bijwerken
            add_meti ('ANA_CON_TU');

            -- in 1 geval zijn haem-uitslagen toegevoegd aan cyto-uitslagen
            IF g_onde_rec.onderzoeknr = 'H95/0037'
            THEN
               add_mw_cyto_uits;
            ELSE
               add_mw_haem_uits;                     -- meetwaarden toevoegen
            END IF;
         -- upd_mw_conc;   -- conclusie bij meting bijwerken
         ELSIF    g_onde_rec.onderzoeknr LIKE 'I%/0%'
               OR g_onde_rec.onderzoeknr LIKE 'I%/1%'
               OR g_onde_rec.onderzoeknr LIKE 'I%/2%'
               OR g_onde_rec.onderzoeknr LIKE 'I01.02%'
               -- I01.0221 e.d. komt voor in de VAX
               OR g_onde_rec.onderzoeknr LIKE 'I%/3%'
               OR g_onde_rec.onderzoeknr LIKE 'I%/4%'
         THEN                                       -- Fish Maastricht -> Cyto
            add_meti ('FISH_CONV');
            add_mw_fish;                             -- meetwaarden toevoegen
         -- upd_mw_conc;   -- conclusie bij meting bijwerken
         ELSIF g_onde_rec.onderzoeknr LIKE 'I%/5%'
         THEN                                        -- Fish Veldhoven -> Cyto
            add_meti ('FISH_CONV');
            add_mw_fish;                             -- meetwaarden toevoegen
         -- upd_mw_conc;   -- conclusie bij meting bijwerken
         ELSIF    g_onde_rec.onderzoeknr LIKE 'I%/6%'
               OR g_onde_rec.onderzoeknr LIKE 'I%/7%'
               OR g_onde_rec.onderzoeknr LIKE 'I%/8%'
               OR g_onde_rec.onderzoeknr LIKE 'I%/9%'
         THEN                                                  -- MLPA -> Cyto
            add_meti ('MLPA_CONV');
            add_mw_mlpa;                             -- meetwaarden toevoegen
         ELSIF g_onde_rec.onderzoeknr LIKE 'N%'
         THEN                                                   -- DNA -> prec
            add_meti ('KARYO-PO');
            add_mw_kary ('KARYO-PO');                -- meetwaarden toevoegen
            upd_mw_conc;                    -- conclusie bij meting bijwerken
         ELSIF g_onde_rec.onderzoeknr LIKE 'P%'
         THEN                                            -- Postnataal -> cyto
            add_meti ('KWK_CON_PO');
            add_mw_cyto_kweek;                       -- meetwaarden toevoegen
            -- upd_mw_conc;   -- conclusie bij meting bijwerken
            upd_onderz_afgerond;
            add_meti ('ANA_CON_PO');
            add_mw_cyto_uits;                        -- meetwaarden toevoegen
            -- upd_mw_conc;   -- conclusie bij meting bijwerken
            add_meti ('KARYO-PO');                   -- meetwaarden toevoegen
            add_mw_kary ('KARYO-PO');
            upd_mw_conc;                    -- conclusie bij meting bijwerken
         ELSIF g_onde_rec.onderzoeknr LIKE 'R%'
         THEN                                                   -- RNA -> haem
            add_meti ('KARYO-PO');
            add_mw_kary ('KARYO-PO');                -- meetwaarden toevoegen
            upd_mw_conc;                    -- conclusie bij meting bijwerken
         ELSE
            log_exception (p_id_namen        => 'onderzoeknr',
                           p_id_waarden      => g_onde_rec.onderzoeknr
                          );
         END IF;

         g_plek := 'vul_onderzoek_tabs';

         IF    g_onde_rec_o.uitslagdatum IS NOT NULL
            OR g_onde_rec_o.brief_verstuurd = 'J'
            OR g_onde_rec_o.toelichting1 IS NOT NULL
         THEN
            add_uits;
            add_kopiehouders;
            add_brief;
         END IF;

         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_plek := 'vul_onderzoek_tabs';
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END vul_onderzoek_tabs;

   PROCEDURE add_onderz_gyn
   IS
      CURSOR c_onde_cur_gyn
      IS
         SELECT DISTINCT o.onderznr, o.binnendat, o.anvrgr_1, o.persnr,
                         o.afdeling, o.indicatie, c.anvrgr_titel relatie,
                         o.odbnr
                    FROM cyto_pers_onderz o, cyto_consult c
                   WHERE o.onderznr LIKE 'Z%'
                     AND o.afdeling IN ('GYN', 'GYNE')
                     AND c.odbnr = o.odbnr
                     AND NOT EXISTS (SELECT onderzoeknr
                                       FROM kgc_onderzoeken
                                      WHERE onderzoeknr = o.onderznr);

      l_onde_id   NUMBER;
      l_rela_id   NUMBER;
      c_ongr_id   NUMBER    := g_ongr_id ('PR');
      c_onui_id   NUMBER    := g_onui_id ('NA');             -- niet afwijkend

      TYPE t_kafd_id IS TABLE OF kgc_kgc_afdelingen.kafd_id%TYPE
         INDEX BY kgc_kgc_afdelingen.code%TYPE;

      l_kafd_id   t_kafd_id;
   BEGIN
      g_plek := 'add_onderz_gyn';
      l_kafd_id ('GYN') := kgc_uk2pk.kafd_id ('GYN');
      l_kafd_id ('GYNE') := kgc_uk2pk.kafd_id ('GYNE');

      FOR r_onde IN c_onde_cur_gyn
      LOOP
         g_odbnr := r_onde.odbnr;
         SELECT kgc_onde_seq.NEXTVAL
           INTO l_onde_id
           FROM DUAL;

         l_rela_id :=
            CASE
               WHEN r_onde.relatie IS NULL
                  THEN NULL
               ELSE g_rela_id (r_onde.relatie)
            END;

         IF l_rela_id IS NULL
         THEN
            l_rela_id := c_rela_id_onb;
         END IF;

         g_onde_rec_o.onderznr := r_onde.onderznr;
         g_onde_rec_o.odbnr := r_onde.odbnr;

         BEGIN
            INSERT INTO kgc_onderzoeken
                        (onde_id, ongr_id,
                         kafd_id,
                         pers_id, rela_id,
                         onui_id, onderzoekstype,
                         onderzoeknr, declareren,
                         afgerond, created_by,
                         creation_date, last_updated_by,
                         last_update_date,
                         datum_binnen
                        )
                 VALUES (l_onde_id                                  -- onde_id
                                  , c_ongr_id                       -- ongr_id
                                             ,
                         l_kafd_id (r_onde.afdeling)                -- kafd_id
                                                    ,
                         r_onde.persnr                              -- pers_id
                                      , l_rela_id                   -- rela_id
                                                 ,
                         c_onui_id, 'D'         -- onderzoekstype --> Diagnose
                                       ,
                         r_onde.onderznr                        -- onderzoeknr
                                        , 'N'                    -- declareren
                                             ,
                         'J'                                       -- afgerond
                            , c_user                             -- created_by
                                    ,
                         SYSDATE                              -- creation_date
                                , c_user                    -- last_updated_by
                                        ,
                         SYSDATE                           -- last_update_date
                                ,
                         geef_geldige_datum (r_onde.binnendat) -- datum_binnen
                        );
         EXCEPTION
            WHEN OTHERS
            THEN
               log_exception (p_id_namen        => 'odbnr',
                              p_id_waarden      => g_odbnr);
         END;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => 'odbnr', p_id_waarden => g_odbnr);
   END add_onderz_gyn;

-- -------------------------------
-- PUBLIEKE FUNCTIES EN PROCEDURES
-- -------------------------------
   PROCEDURE vul_data_onderz
   IS
   BEGIN
      g_plek := 'vul_data_onderz';
      DBMS_OUTPUT.ENABLE;
      disable_all_triggers;
      disable_all_constraints;
      /* wordt al gedaan bij vullen zwangerschappen:
          EXECUTE IMMEDIATE 'truncate table conv_errors';
      */  -- records tellen voor migratie
          -- store_numrec;   -- bevat commit
      init_ids;
      --   vul_cyto_zwangerschappen;   -- bevat commit
      vul_onderzoek_tabs;                                     -- bevat commit
      add_onderz_gyn;                                         -- bevat commit
      upd_seq;                                         -- sequences bijwerken
      -- records tellen na migratie
      store_numrec;                                           -- bevat commit
      enable_all_constraints;
      enable_all_triggers;
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => NULL, p_id_waarden => NULL);
         COMMIT;                                      -- t.b.v. log_exception
         RAISE;
   END vul_data_onderz;

   PROCEDURE vul_data_zwan
   IS
   BEGIN
      g_plek := 'vul_data_zwan';
      DBMS_OUTPUT.ENABLE;
      disable_all_triggers;
      disable_all_constraints;
      -- records tellen voor de migratie
      store_numrec;                                           -- bevat commit
      init_ids;

      EXECUTE IMMEDIATE 'truncate table conv_errors';

      --    vul_cyto_zwangerschappen;
      add_zwangerschappen;
      COMMIT;
      upd_seq;                                         -- sequences bijwerken
   EXCEPTION
      WHEN OTHERS
      THEN
         log_exception (p_id_namen => NULL, p_id_waarden => NULL);
         COMMIT;                                      -- t.b.v. log_exception
         RAISE;
   END vul_data_zwan;
END conv_cyto;
/

/
QUIT
