CREATE OR REPLACE PACKAGE "HELIX"."EPD_LUS_INTERFACE_MAASTICHT" IS

/*******************************************************************************
    NAME:      KGC_ORU_MAASTRICHT
    PURPOSE:   Aanmaken XML bestanden voor SAP-LUS (Laboratorium-Uitslagen-Systeem van SAP)
               vanuit Helix-databse
               Onderdeel van de Opvragen Uitslagen Interface ("ORU Interface"),
               fase 1 (Afdeling "CYTO")

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.0        2012-11-05  W.A.Brouwer/Cap  Initiele versie
    1.2        2013-02-14  R Konings        Aanpassingen ORU Fase 2
    1.3        2013-10-23  R Konings        Aanpassingen i.v.m Embargo
*******************************************************************************/


----------------------------------------------------------------
-- Get_Oru_Verslagen: geef een xml met lijst met onderzoeken bij
--                    gegeven Patientnummer (KGC_Personen.Zisnr)
----------------------------------------------------------------
FUNCTION Get_Oru_Verslagen (P_ZisNr in varchar2)
  RETURN Clob;

--------------------------------------------------------------------------------
-- Get_Oru_Verslag_Detail: geef een xml met detail-gegevens bij een
--                         gegeven OnderzoekNummer (KGC_Onderzoeken.Onderzoeknr)
--------------------------------------------------------------------------------
FUNCTION Get_Oru_Verslag_Detail (P_Onderzoeknr in varchar2)
  RETURN Clob;

Function GetInterne_Relatie(p_pers_id Integer, P_onde_id Integer) return varchar2;
Function CheckRelatie(p_pers_id Integer) return boolean;

Function GetOraReportLink(P_ongr_id Number, P_onde_id Number ) return Clob;

Function GetMonsterFractie(P_onde_Id Number, p_fractie boolean) return Clob;

Function GetEmbargo(p_ongr_id Number, p_input varchar2) return varchar2;
Function GetIndicatie(p_onde_ID number) return Clob;
Function GetRelatieGegevens(p_Rela_id Number)return Varchar2;
Function GetOnderzoekWijze(p_ONWY_cd Varchar2) Return Clob;
Function GetProtocol(p_onde_id Number) return Clob;
Function GetUitslagTeksten( p_onde_id Number,
                            p_ongr_id Number,
                            p_status varchar2,
                            p_datum_aut Date,
                            p_mede_id Number,
                            p_relatie varchar2
                          )
Return Clob;
Function GetKopieHouders(puits_id integer) return clob;
Function GetIndicatiesDetail(p_onde_id Number) Return Clob;
Function ReplaceUitslag(Tekst in clob) return clob;
Procedure SetUitslagTekstEPD(
                             p_onde_id number,
                             p_ongr_id Number,
                             p_Uits_id Number
                            , p_Brty_id Number
                            , p_Tekst varchar2
                            , p_Toelichting varchar2
                            , p_kafd_id number
                            , p_Commentaar varchar2
                            , p_datum_autorisatie date
                            , p_relatie varchar2
                            , p_uitslag out clob
                            , p_geneticus  out clob
                            , p_embargo_termijn out number
                              );


procedure maak_epd_uitslag_aan;

/*
Function LUS_GetOrders(p_PatientID Varchar2, p_amount Integer, p_offSet Integer) Return Clob; -- Ophalen Orders
Function LUS_OnderzoekAfgerond(p_mons_id number) return varchar2; -- Check of een Onderzoek afgerond is
Function LUS_GetObservations(p_mons_id Number, p_stgr_id Number, p_dat_afgerond out varchar2)  return clob; -- Ophalen bepalingen behorende bij de Orders
Function LUS_GetRange(p_Bovengrens varchar2, p_ondergrens varchar2) return varchar2; -- Ophalen & Formateren Range Gegegevens naar LUS Formaat
Function LUS_GetOrdersFiltererd(p_patientId varchar2, p_Start_Date Date, p_End_Date Date, p_stoftest varchar2)return Clob; -- Get Orders Filtered voor de grafiek functie van LUS
Function LUS_GetObservationsFiltered(p_mons_id Number, p_stgr_id Number, p_stof_id number, p_dat_afgerond out varchar2)  -- Voor grafiek functie van LUS.
        return clob;




*/


END EPD_LUS_INTERFACE_MAASTICHT;
/

/
QUIT
