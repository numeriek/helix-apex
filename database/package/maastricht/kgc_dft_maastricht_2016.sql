CREATE OR REPLACE PACKAGE "HELIX"."KGC_DFT_MAASTRICHT_2016" Is
    /******************************************************************************
      NAME:       kgc_dft_maastricht
      PURPOSE:    controleren van declaratiegegevens en overzetten naar financieel systeem

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      3.0        17-07-2012  RKON             Added nieuwe business logica voor het vullen van
                                              variabelen tbv DFT bericht.
                                              Added procedure om voorbeeld bericht aan te maken.
                                              Added procedure specificatie voor het transformeren
                                              en versturen DFT bericht.
      2.4        08-09-2010  GWE              Mantis 1268: Alternatief persoon voor declaratie
      2          17-04-2007  RDE              Gelijktrekken met Nijmegen/Utrecht
      1          04-07-2006  RDE              op locatie
   ***************************************************************************** */
    Function getpersoon(p_pers_id In Number) Return kgc_personen%Rowtype;

    Function getonderzoeksgroep(p_ongr_id Number) Return Varchar2;

    Function getonderzoek
    (
        p_id       Number,
        p_entiteit Varchar2
    ) Return kgc_onderzoeken%Rowtype;

    Function getaanvrager
    (
        p_rela_id     In Number,
        p_dewy_code   In Varchar2,
        p_kafd_id     In Number,
        p_pers_id     In Number,
        p_prod_dept   In Varchar2,
        p_verrichting In Varchar2,
    p_ongr_id     in number -- Pers_id. Kgc_declaraties.pers_id Persoon van declaratie record .
    ) Return t_dft_aanvrager;

    Function getabwcode
    (
        p_dewy_code  In Varchar2,
        p_decl       In kgc_declaraties%Rowtype,
        p_pers_rec   In kgc_personen%Rowtype,
        p_ovl_foetus In Varchar2,
        p_aanvrager  In t_dft_aanvrager

    ) Return Varchar2;

    Function getdeclaratiewijze(p_dewy_id Number) Return Varchar2;

    Function getcategoriewaarde
    (
        p_cate_code Varchar2,
        p_caty_code Varchar2
    ) Return Varchar2;

    Function verwerk_declaraties
    (
        p_decl_id    In Number,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean;

    Function verwerk_onbe
    (
        p_decl       kgc_declaraties%Rowtype,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean;

    Function verwerk_onde
    (
        p_decl       kgc_declaraties%Rowtype,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean;

    Function verwerk_meti
    (
        p_decl       kgc_declaraties%Rowtype,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean;

    Procedure controleer
    (
        p_decl_id In Number,
        x_ok      Out Boolean
    );

    Procedure verwerk
    (
        p_decl_id In Number,
        x_ok      Out Boolean
    );

    Procedure draai_terug
    (
        p_decl_id In Number,
        x_ok      Out Boolean
    );

    /* Controleer of de Foetus overleden is Op basis van een ONDE_ID */
    Function foetusdeceased_onde(pid Integer) Return Varchar2;

    /* Controleer of de Foetus overleden is Op basis van een METI_ID */
    Function foetusdeceased_meti(pid Integer) Return Varchar2;

    /* Controleer of de Persoon overleden is Op basis van een ONBE_ID  */
    Function foetusdeceased_onbe(pid Integer) Return Varchar2;

    /* VOEG COMMENTAAR TOE AAN KGC_DCLARATIES */

    /* Creeer een file in de tabel voor controle door Key-user, met daarin de belangrijkste
   DFT waarden */

    /* Functie voor het verzenden van de DFT berichten */
    /*  SPECIFICATIE CORRESPONDEREND MET FO
      p_patient_id               PID 2
      p_achternaam               PID 5
      p_roepnaam                 PID 5
      p_geboortedatum            PID 7
      p_straatnaam               PID 11
      p_code_verzekeraar         IN1 3
      p_declaratienr             FT1 2
      p_verrichtingsdatum        FT1 4
      p_polisnummer              IN1 2
      p_verrichting              FT1 7
      p_omschrijving             FT1 8
      p_verrichtingshoeveelheid  FT1 10
      p_aanvragende_afdeling     FT1 13
      p_uitvoerendspecialisme    FT1 20
      p_ABW_CODE                 FT1 14 INSURANCE_PLAN_ID
      p_aanvragend_specialisme   FT1 21 + ORC 21
      p_log_id
   */
    Function verstuur_soap_dft
    (
        p_pers_rec                kgc_personen%Rowtype,
        p_declaratienr            Integer,
        p_verrichtingsdatum       Varchar2,
        p_verrichting             Varchar2,
        p_omschrijving            Varchar2,
        p_verrichtingshoeveelheid Varchar2,
        p_aanvragende_afdeling    Varchar2,
        p_uitvoerendspecialisme   Varchar2,
        p_abw_code                Varchar2,
        p_aanvragend_specialisme  Varchar2,
        p_aanvragend_speci_nm     Varchar2,
        p_geslacht                Varchar2,
        p_prod_afd                Varchar2,
        p_persoon_declaratie_rec  Varchar2 -- Persoon volgens Kgc_declaraties.pers_id
        --            ,p_log_id                  integer
    ) Return Boolean;
    Function persoonalsfoetus_overleden(ppers_id Number) Return Boolean;

    Procedure setaltdecl;

  Function getcategoriewaardeDecl(p_ongr_cd kgc_onderzoeksgroepen.code%Type) return varchar2;

End kgc_dft_maastricht_2016;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DFT_MAASTRICHT_2016" Is
    /******************************************************************************
   NAME: kgc_dft_maastricht
   PURPOSE: controleren van declaratiegegevens en overzetten naar financieel systeem

   REVISIONS:
   Ver Date Author Description
   --------- ---------- --------------- ------------------------------------

   4.2  20-05-2015 Externe aanvragen, niet-WDS omgezet van PTEX naar labcode/Bij WDS omzetting kostendrager 70074 naar 70191
   4.1  06-05-2015 Aanpassing SetAltDecl door Guido Herben i.v.m. Mantis Call 0011287
   4.0  10-04-2014 START Nieuwe VERSIE, I.V.M. ONOVERZICHTELIJKHEID OUDE versie.
                   In deze versie worden de berichten afhandeld per entiteit


   3.5  17-12-2013 AANPASSINGEN i.v.m. veranderde eisen 2014
   3.4  27-09-2012 Wijzigingen aanvrager extern toegevoegd
   3.3  01-09-2012 Wijzingen business logica toegevoegd
   3.2  06-08-2012 RKON programmatuur Herschreven aan aangepast.
   3.1  30-07-2012 M vd Kamp Programmateur voor dft koppeling toegevoegd
   3.0  12-7-2012  Aanpassingen tbv Koppeling met SAP op basis van XML.
   2.5  10-7-2012  Commentaar toegevoegd tbv VOORBEREIDING DECLARATIE WEBSERVICE
   2.4  08-09-2010 Gerrit Wiskerke   Mantis 1268: Alternatief persoon voor declaratie
   2.3  02-07-2010 Marc Woutersen In check_verstuur_dft_p03 is het volgende aangepast:
        Toegevoegd voor alle afdelingen: Als aan de entiteit (ONDE/ONBE/METI/GESP) een vervallen onderzoek hangt,
        wordt de betreffende declaratie tegengehouden
        Aangapast voor DNA: Als bij het onderzoek declaratiewijze BU, BU_1, BU_2, BU_3 of BU_4 is vastgelegd,
        moet de aanvragerscode (zowel arts als afdeling) op DIV worden gezet
   2.2  29-06-2010 Marc Woutersen In check_verstuur_dft_p03 code r_rela.inst_code = 'AZM' vervangen door r_rela.inst_code like 'AZM%'
   2.1  16-12-2009 Roy Konings    Toevoegen laatste commentaar aan programma blok ten behoeve
                                  van de code NB.
   2.0  23-11-2009 Roy Konings Herzien van programmatuur betreffende code NB.
   1.10 13-11-2009 Roy Konings Aanpassingen Package op basis van wensen DNA.
   1.9  20-07-2009 Roy Konings Update aanpassing Code NB
   1.8  10-06-2009 Roy Konings Aanpassing Aanvr/prod AfdelingCode
   1.7  01-06-2009 Roy Konings Aanpassing ABW CODE op NB overledenen
   1.6  04-02-2009 RDE Aanpassing tbv CYTO
   1.5  21-11-2007 RDE Aanpassing tbv DNA - DIV gewijzigd
   1.4  02-10-2007 RDE Aanpassing tbv DNA
   1.3  30-05-2007 RDE Aanpassing tbv Metabool
   1.2  17-04-2007 RDE Aanpassing tbv Metabool
   1.0  16-10-2006 MKL Op basis van kgc_dft_utrecht (Rob Debets)

   ***************************************************************************** */

    -- sla systeemparameter-waarden ZIS_DFT_CONTOLES op in pl-sqltable
    -- 1= algemeen nivo
    -- 2= afdelingsnivo
    -- 3=onderzoeksgroepnivo

    v_message  Varchar2(32000);
    v_linesep  Varchar2(4) := chr(10);
    v_err_stat Varchar2(1);

    e_no_onbe       Exception;
    e_no_onde       Exception;
    e_no_meti       Exception;
    e_no_rela       Exception;
    e_abw_bep       Exception;
    e_no_pers       Exception;
    e_no_prd_dep    Exception;
    e_err_orderprov Exception;
    e_err_telcode   Exception;

    Procedure add2msg(p_message Varchar2) Is
    Begin
        v_message := v_message || chr(10) || p_message;

    End;

    Procedure update_decl
    (
        p_decl_id Number,
        p_message Varchar2,
        p_status  Varchar2,
        p_upd_dat Boolean
    ) As

        Cursor c_decl_len(c_decl_id Number) Is
            Select length(decl.verwerking)
            From   kgc_declaraties decl
            Where  decl.decl_id = c_decl_id;

        lengte Integer;

    Begin
        Open c_decl_len(p_decl_id);
        Fetch c_decl_len
            Into lengte;
        Close c_decl_len;

        If lengte > 3000
        Then
            If p_upd_dat
            Then
                Update kgc_declaraties
                Set    status           = p_status,
                         verwerking       = p_message,
                         datum_verwerking = Sysdate
                Where  decl_id = p_decl_id;

            Else
                Update kgc_declaraties
                Set    status     = p_status,
                         verwerking = p_message
                Where  decl_id = p_decl_id;

            End If;

        Else
            If p_upd_dat
            Then
                Update kgc_declaraties
                Set    status           = p_status,
                         verwerking       = p_message || chr(10) || verwerking,
                         datum_verwerking = Sysdate
                Where  decl_id = p_decl_id;

            Else

                Update kgc_declaraties
                Set    status     = p_status,
                         verwerking = p_message || chr(10) || verwerking
                Where  decl_id = p_decl_id;

            End If;

        End If;
    End;

    Function insert_log_record
    (
        p_zisnr                  Varchar2,
        p_aanspreken             Varchar2,
        p_geboortedatum          Varchar2,
        p_decl_id                Integer,
        p_verrichting_dat        Varchar2,
        p_verrichting            Varchar2,
        p_omschrijving           Varchar2,
        p_aanvr_afd              Varchar2,
        p_abw_code               Varchar2,
        p_aanvrager              Varchar2,
        p_prod_afd               Varchar2,
        p_persoon_declaratie_rec Varchar2

    ) Return Integer As
        v_declaratie Varchar2(4000);
        v_id         Integer;
    Begin
        v_declaratie := 'Persoon volgens Kgc_declaraties.pers_id:  ' ||
                             p_persoon_declaratie_rec || chr(10) ||
                             'Verrichting komt op naam van:  ' || p_zisnr || ' | ' ||
                             p_aanspreken || chr(10) || 'Geboren Op: ' ||
                             p_geboortedatum || chr(10) || chr(10) ||
                             'Declaratieparameters: ' || chr(10) ||
                             'Declaratie_ID: ' || chr(9) || to_char(p_decl_id) ||
                             chr(10) || 'Verrichtingsdatum: ' || chr(9) ||
                             p_verrichting_dat || chr(10) || 'Verrichting: ' ||
                             chr(9) || p_verrichting || chr(10) || 'ABW Code: ' ||
                             chr(9) || p_abw_code || chr(10) || 'Bedrag: ' ||
                             chr(9) || p_omschrijving || chr(10) || 'Aanvrager: ' ||
                             chr(9) || p_aanvrager || chr(10) ||
                             'Aanvragende Afdeling: ' || p_aanvr_afd || chr(10) ||
                             'Uitvoerende Afd: ' || chr(9) || p_prod_afd;

        v_id := dft_log_seq.nextval;
        Insert Into dft_log
            (decctl_id, zisnr, decl_id, declaratie, datum_verzonden)
        Values
            (v_id,
             p_zisnr,
             p_decl_id,
             v_declaratie,
             to_char(Sysdate, 'dd-mm-yyyy'));
        Commit;
        Return(v_id);

    End;

    Procedure update_log_record
    (
        p_log_id        Integer,
        p_message       Clob,
        p_response      Clob,
        p_verzenddatum  Date,
        p_verzendtijd   Varchar2,
        p_responsedatum Date,
        p_responsetijd  Varchar2,
        p_optie         Varchar2 -- 1 is verzenden 2 is response
    ) As
    Begin
        Case p_optie
            When '1' Then
                Update dft_log
                Set    message         = p_message,
                         datum_verzonden = p_verzenddatum,
                         tijd_verzonden  = p_verzendtijd
                Where  decctl_id = p_log_id;
            When '2' Then
                Update dft_log
                Set    response       = p_response,
                         datum_response = p_responsedatum,
                         tijd_response  = p_responsetijd
                Where  decctl_id = p_log_id;
        End Case;
        Commit;

    End;

    Function getpersoon(p_pers_id In Number)
    /* Ophalen Persoonsgegevens op basis van pers_id */
     Return kgc_personen%Rowtype As
        Cursor c_pers(c_pers_id Number) Is
            Select * From kgc_personen Where pers_id = c_pers_id;

        r_pers c_pers%Rowtype;

    Begin
        Open c_pers(p_pers_id);
        Fetch c_pers
            Into r_pers;
        If c_pers%Notfound
        Then
            add2msg('Persoons ID: ' || to_char(p_pers_id) || ' niet gevonden');
            Raise e_no_pers;
        End If;
        Return r_pers;
    End;

    Function getonderzoeksgroep(p_ongr_id Number) Return Varchar2 As
        Cursor c_ongr(c_ongr_id Number) Is
            Select code From kgc_onderzoeksgroepen Where ongr_id = c_ongr_id;

        r_ongr c_ongr%Rowtype;

    Begin
        Open c_ongr(p_ongr_id);
        Fetch c_ongr
            Into r_ongr;
        Close c_ongr;
        Return r_ongr.code;
    End;

    Function getonderzoek
    (
        p_id       Number,
        p_entiteit Varchar2
    )
    /* Ophalen van Onderzoek op basis van de entiteit en ID van Kgc_declaraties */
     Return kgc_onderzoeken%Rowtype As
        -- Ophalen ONDE_ID middels de onderzoeks betrokkene
        Cursor c_onbe(c_onbe_id Number) Is
            Select onbe.onde_id
            From   kgc_onderzoek_betrokkenen onbe
            Where  onbe.onbe_id = c_onbe_id;

        -- Ophalen Onderzoek
        Cursor c_onde(c_onde_id Number) Is
            Select onde.onde_id,
                     onde.ongr_id,
                     onde.kafd_id,
                     onde.pers_id,
                     onde.onderzoekstype,
                     onde.onderzoeknr,
                     nvl(onde.rela_id_oorsprong, onde.rela_id) rela_id,
                     onde.herk_id,
                     onde.spoed,
                     onde.declareren,
                     onde.afgerond,
                     onde.created_by,
                     onde.creation_date,
                     onde.last_updated_by,
                     onde.last_update_date,
                     onde.datum_binnen,
                     onde.geplande_einddatum,
                     onde.inst_id,
                     onde.onre_id,
                     onde.onderzoekswijze,
                     onde.status,
                     onde.mede_id_beoordelaar,
                     onde.mede_id_autorisator,
                     onde.mede_id_controle,
                     onde.mede_id_dia_autorisator,
                     onde.onui_id,
                     onde.coty_id,
                     onde.diagnose_code,
                     onde.ermo_id,
                     onde.dist_id,
                     onde.herk_id_dia,
                     onde.taal_id,
                     onde.verz_id,
                     onde.proj_id,
                     onde.fare_id,
                     onde.dewy_id,
                     onde.zekerheid_diagnose,
                     onde.zekerheid_status,
                     onde.zekerheid_erfmodus,
                     onde.datum_autorisatie,
                     onde.kariotypering,
                     onde.referentie,
                     onde.nota_adres,
                     onde.omschrijving,
                     onde.foet_id,
                     onde.rela_id_oorsprong,
                     onde.pers_id_alt_decl

            From   kgc_onderzoeken onde
            Where  onde.onde_id = c_onde_id;
        -- Ophalen ONDE_ID middels de meting
        Cursor c_meti(c_meti_id Number) Is
            Select meti.onde_id
            From   bas_metingen meti
            Where  meti_id = c_meti_id;

        r_onde    c_onde%Rowtype;
        v_onde_id Number(10);

    Begin

        Case p_entiteit
            When 'ONBE' Then
                Begin
                    Open c_onbe(p_id);
                    Fetch c_onbe
                        Into v_onde_id;
                    If c_onbe%Notfound
                    Then
                        -- Indien gevonden ga door
                        -- Anders genereer proces exceptie
                        add2msg('Onderzoeksbetrokkenne Onbekend');
                        Raise e_no_onbe;
                    End If;
                    Close c_onbe;
                End;

            When 'METI' Then
                Begin
                    Open c_meti(p_id);
                    Fetch c_meti
                        Into v_onde_id;
                    If c_meti%Notfound
                    Then
                        -- Indien gevonden ga door
                        -- genereer proces exceptie
                        add2msg('Meting Onbekend');
                        Raise e_no_meti;
                    End If;
                    Close c_meti;
                End;

            When 'ONDE' Then
                v_onde_id := p_id;
            Else
                Null;

        End Case;

        -- Haal Onderzoek(ONDE) op
        Open c_onde(v_onde_id);
        Fetch c_onde
            Into r_onde;
        If c_onde%Notfound
        Then
            -- Indien gevonden ga door
            -- Anders genereer Proces Exceptie
            add2msg('Onderzoek Onbekend');
            Raise e_no_onde;
        End If;
        Close c_onde;

        Return r_onde;

    End;

    Function getaanvrager
    (
        p_rela_id     In Number,
        p_dewy_code   In Varchar2,
        p_kafd_id     In Number,
        p_pers_id     In Number, -- Pers_id. Kgc_declaraties.pers_id Persoon van declaratie record .
        p_prod_dept   In Varchar2,
        p_verrichting In Varchar2,
      p_ongr_id     in number
    ) Return t_dft_aanvrager
    -- Bepaal de codes voor de aanvrager  afdeling
        -- t.b.v. de declaraties

     As

        -- Ophalen relatie gegevens.
        Cursor c_rela(c_rela_id Number) Is
            Select rela.code         rela_code,
                     rela.relatie_type,
                     rela.liszcode,
                     rela.aanspreken,
                     rela.land,
                     afdg.code         afdg_code,
                     inst.code         inst_code
            From   kgc_relaties     rela,
                     kgc_afdelingen   afdg,
                     kgc_instellingen inst
            Where  rela.rela_id = c_rela_id
            And    rela.afdg_id = afdg.afdg_id(+)
            And    rela.inst_id = inst.inst_id(+);

        r_rela c_rela%Rowtype; -- Aanvrager onderzoek

        v_pers_rec cg$kgc_personen.cg$row_type;

        -- Aanvrager Type
        v_dft_aanvrager  t_dft_aanvrager;
        v_order_provider Varchar2(20);
        v_order_prov_dep Varchar2(10);

    Begin
        v_dft_aanvrager := t_dft_aanvrager(Null, Null, Null, Null, Null, Null);

        If p_pers_id Is Not Null
        Then
            v_pers_rec.pers_id := p_pers_id;
            cg$kgc_personen.slct(v_pers_rec);
        End If;

        -- Ophalen gegevens Aanvrager Onderzoek.
        Open c_rela(p_rela_id);
        Fetch c_rela
            Into r_rela;
        If c_rela%Notfound
        Then
            add2msg('Aanvrager v.h. onderzoek is onbekend');
            Raise e_no_rela;
        End If;
        Close c_rela;

        -- Bepaal Codes op basis van relatie type en declaratie wijze.
        Case r_rela.relatie_type
            When 'IN' Then
                If Trim(r_rela.liszcode) Is Null
                Then
                    v_dft_aanvrager.addaanvragercodes('KLG9999999',
                                                                 'KGMA',
                                                                 r_rela.relatie_type,
                                                                 r_rela.aanspreken,
                                                                 substr('KLG9999999', 1, 2),
                                                                 '');
                Else
                    -- Indien de afdeling ontbreekt dan mag de declaratie niet doorgaan.
                    If Trim(r_rela.afdg_code) Is Null
                    Then
                        add2msg('Afdeling ontbreekt voor Interne aanvrager: ' ||
                                  r_rela.rela_code || '| ' || r_rela.aanspreken);
                        Raise e_no_rela;
                    End If;
                    v_dft_aanvrager.addaanvragercodes(r_rela.liszcode,
                                                                 r_rela.afdg_code,
                                                                 r_rela.relatie_type,
                                                                 r_rela.aanspreken,
                                                                 substr(r_rela.liszcode,
                                                                          1,
                                                                          2),
                                                                 '');
                End If;

            When 'EX' Then
                -- Indien de instelling ontbreekt dan mag de declaratie niet doorgaan.
                If Trim(r_rela.inst_code) Is Null
                Then
                    add2msg('Instelling ontbreekt voor Externe aanvrager: ' ||
                              r_rela.rela_code || '| ' || r_rela.aanspreken);
                    Raise e_no_rela;
                Else
                    If substr(r_rela.inst_code, 1, 2) Not In ('70', '60')
                    Then
                        add2msg('Verkeerde Instellingscode bij Externe-aanvrager: ' ||
                                  r_rela.liszcode || ' Instelling:' ||
                                  r_rela.inst_code);
                        Raise e_err_orderprov;

                    End If;
                End If;

                If getcategoriewaardedecl(getonderzoeksgroep(p_ongr_id)) = 'METAB'
                Then
                    If p_verrichting Not In ('377884', '377885')
                    Then
                        If r_rela.inst_code = '70074'
                        Then
                            r_rela.inst_code := '70191';
                        End If;
                    End If;
                End If;


                If p_dewy_code Like 'S2%'
                Then
                    v_dft_aanvrager.addaanvragercodes('KLG9999999',
                                                                 'KGMA',
                                                                 r_rela.relatie_type,
                                                                 r_rela.liszcode || ' ' ||
                                                                 r_rela.aanspreken,
                                                                 substr(r_rela.liszcode,
                                                                          1,
                                                                          2),
                                                                 '');

                Elsif p_dewy_code Like '%WDS%' Or p_dewy_code Like 'NF_IBD'
                Then

                    v_dft_aanvrager.addaanvragercodes(lpad(r_rela.inst_code,
                                                                        10,
                                                                        '0'),
                                                                 'PTEX',
                                                                 r_rela.relatie_type,
                                                                 r_rela.liszcode || ' ' ||
                                                                 r_rela.aanspreken,
                                                                 substr(r_rela.liszcode,
                                                                          1,
                                                                          2),
                                                                 '');
                Else
                    v_dft_aanvrager.addaanvragercodes(lpad(r_rela.inst_code,
                                                                        10,
                                                                        '0'),
                                                                 p_prod_dept,
                                                                 r_rela.relatie_type,
                                                                 r_rela.liszcode || ' ' ||
                                                                 r_rela.aanspreken,
                                                                 substr(r_rela.liszcode,
                                                                          1,
                                                                          2),
                                                                 '');

                End If;






            When 'HA' Then
                Begin
                    If substr(r_rela.rela_code, 1, 2) In ('HA', 'IA')
                    Then
                        v_order_provider := r_rela.liszcode;
                    Else
                        add2msg('Relatiecode huisarts niet correct: ' ||
                                  r_rela.rela_code || '| ' || r_rela.aanspreken);
                        Raise e_no_rela;
                    End If;

                    If Trim(r_rela.land) Is Not Null And
                        upper(r_rela.land) <> 'NEDERLAND'
                    Then
                        v_order_prov_dep := 'HABU';
                    Else
                        If p_rela_id = v_pers_rec.rela_id
                        Then
                            v_order_prov_dep := 'HUAR';
                        Else
                            v_order_prov_dep := 'VVHA';
                        End If;
                    End If;
                    v_dft_aanvrager.addaanvragercodes(v_order_provider,
                                                                 v_order_prov_dep,
                                                                 'HA',
                                                                 v_order_provider ||
                                                                 r_rela.aanspreken,
                                                                 substr(r_rela.liszcode,
                                                                          1,
                                                                          2),
                                                                 '');

                End;

            Else
                add2msg('Relatietype ontbreekt voor aanvrager: ' ||
                          r_rela.rela_code || '| ' || r_rela.aanspreken);
                Raise e_no_rela;
        End Case;
        Return v_dft_aanvrager;
    End;

    Function getabwcode
    (
        p_dewy_code  In Varchar2,
        p_decl       In kgc_declaraties%Rowtype,
        p_pers_rec   In kgc_personen%Rowtype,
        p_ovl_foetus In Varchar2,
        p_aanvrager  In t_dft_aanvrager

    )

     Return Varchar2 As
        v_abw  Varchar2(2);
        v_stap Varchar2(20);

    Begin
        /* Functie bepaald de ABW Code behorende bij een declaratie op basis van diverse input parameters */

        v_stap := 'BV Code';
        If p_aanvrager.order_prov_type = 'EX'
        Then
            If p_dewy_code Like '%WDS%'
            Then
                If p_dewy_code = 'WDS_NF'
                Then
                    v_abw := 'NF';
                Else
                    If getcategoriewaardedecl(getonderzoeksgroep(p_decl.ongr_id)) = 'DNA'
                    Then
                        v_abw := 'WD';
                    End If;
                End If;
            Else
                If p_dewy_code Like 'S2%'
                Then
                    Null;
                Else
                    If getcategoriewaardedecl(getonderzoeksgroep(p_decl.ongr_id)) = 'METAB'
                    Then
                        If p_decl.verrichtingcode In ('377884', '377885', '')
                        Then
                            v_abw := 'BV';
                        End If;
                    Else
                        v_abw := 'BV';
                    End If;

                End If;
            End If;
        End If;

        If p_decl.pers_id_alt_decl Is Null
        Then

            If p_decl.declareren = 'N' Or p_dewy_code Like 'NF%'
            Then
                v_stap := 'Declareren';
                v_abw  := 'NF';
            Else
                v_stap := 'Datum verg(geb-ovl)';
                If to_char(p_pers_rec.geboortedatum, 'YYYYMMDD') =
                    to_char(p_pers_rec.overlijdensdatum, 'YYYYMMDD') Or
                    to_char(p_pers_rec.geboortedatum + 1, 'YYYYMMDD') =
                    to_char(p_pers_rec.overlijdensdatum, 'YYYYMMDD')
                Then
                    v_abw  := 'NF';
                    v_stap := 'datum < sysdate';
                Else
                    If ((trunc(p_pers_rec.overlijdensdatum) + 730) <
                        trunc(Sysdate))
                    Then

                        v_abw := 'NF';
                    Else
                        If ((trunc(p_decl.datum) + 730) < trunc(Sysdate))
                        Then
                            v_abw := 'NF';
                        Else
                            v_stap := 'ovl foetus';
                            /* Case p_ovl_foetus
                         When 'J' Then
                           v_abw := 'NF';
                         When 'E' Then
                           Begin
                             add2msg('Bepaling van de overlijdensstatus van de Foetus is Mislukt');
                             Raise e_abw_bep;
                           End;
                         Else
                           Null;
                       End Case;
                     */
                        End If;
                    End If;
                End If;
            End If;
        End If;

        Return v_abw;

    Exception
        When Others Then
            Begin
                add2msg('Technische fout ABW' || Sqlcode || Sqlerrm || v_stap);
                Raise e_abw_bep;
            End;

    End;

    Function getdeclaratiewijze(p_dewy_id Number) Return Varchar2 Is
        Cursor c_dewy(c_dewy_id Number) Is
            Select code From kgc_declaratiewijzen Where dewy_id = c_dewy_id;

        r_dewy c_dewy%Rowtype;

    Begin
        Open c_dewy(p_dewy_id);
        Fetch c_dewy
            Into r_dewy;
        If c_dewy%Notfound
        Then
            r_dewy.code := Null;
        End If;
        Close c_dewy;
        Return r_dewy.code;
    End;

    Function getcategoriewaarde
    (
        p_cate_code Varchar2,
        p_caty_code Varchar2
    ) Return Varchar2 Is
        Cursor c_cawa
        (
            c_cate_code Varchar2,
            c_caty_code Varchar2
        ) Is
            Select cate.code
            From   kgc_categorie_waarden cawa,
                     kgc_categorieen       cate,
                     kgc_categorie_types   caty
            Where  caty.code = c_caty_code
            And    caty.caty_id = cate.caty_id
            And    cawa.waarde = c_cate_code
            And    cate.cate_id = cawa.cate_id;

        r_cawa c_cawa%Rowtype;

    Begin
        Open c_cawa(p_cate_code, p_caty_code);
        Fetch c_cawa
            Into r_cawa;
        If c_cawa%Notfound Or r_cawa.code Is Null
        Then
            r_cawa.code := 'ZZ99';
        End If;
        Return(r_cawa.code);
    End;

    -- lokaal

    Function verwerk_declaraties
    (
        p_decl_id    In Number,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean Is

        Cursor c_decl(c_decl_id Number) Is
            Select decl.*

            From   kgc_declaraties decl
            Where  decl.decl_id = c_decl_id;

        r_decl c_decl%Rowtype;

        v_verstuur Boolean := True;
        v_succes   Boolean := True;

        e_no_declaration    Exception;
        e_entiteit_onbekend Exception;

    Begin
        If p_check_only
        Then
            v_err_stat := '';
        Else
            v_err_stat := 'T';
        End If;
        Begin
            -- Controleer of Declaratie bestaat.
            Open c_decl(p_decl_id);
            Fetch c_decl
                Into r_decl;
            If c_decl%Notfound
            Then
                Raise e_no_declaration;
            End If;
            Close c_decl;
            -- Generieke Controles

            Case r_decl.status
                When 'V' Then
                    If p_aantal = 1
                    Then
                        Rollback;
                        add2msg('Bericht reeds verwerkt');
                        v_verstuur := False;
                    End If;
                When 'R' Then
                    If p_aantal = -1
                    Then
                        Rollback;
                        add2msg('Bericht reeds teruggedraaid');
                        v_verstuur := False;
                    End If;
                Else
                    v_verstuur := True;

            End Case;

            If r_decl.declareren <> 'J'
            Then
                add2msg('Declareren staat op N(Nee)');
                v_verstuur := False;
            End If;

            If r_decl.pers_id Is Null
            Then
                add2msg('Geen Persoon bekend bij declaratierecord');
                v_verstuur := False;
            End If;

            If r_decl.kafd_id Is Null
            Then
                add2msg('Declaratie.Afdeling is afwezig!');
                v_verstuur := False;
            End If;

            If r_decl.datum Is Null
            Then
                add2msg('Verrichtingsdatum ontbreekt');
                v_verstuur := False;
            End If;

            If r_decl.verrichtingcode Is Null
            Then
                add2msg('Verrichtings- en/of Telcode ontbreekt');
                v_verstuur := False;
            End If;

            If r_decl.pers_id_alt_decl Is Not Null And getcategoriewaardedecl(getonderzoeksgroep(r_decl.ongr_id)) = 'METAB'
            Then
                add2msg('Declaratie van Metabool/EMZ mag geen alternatief persoon bevatten');
                v_verstuur := False;
            End If;

            -- Indien generieke Controlles Onjuist, dan breek verdere verwerking af.
            If v_verstuur
            Then
                Case r_decl.entiteit
                    When 'ONBE' Then
                        v_succes := verwerk_onbe(r_decl, p_aantal, p_check_only);

                    When 'ONDE' Then
                        v_succes := verwerk_onde(r_decl, p_aantal, p_check_only);

                    When 'METI' Then
                        v_succes := verwerk_meti(r_decl, p_aantal, p_check_only);

                    Else
                        add2msg('Entiteit niet bekend als declaratie-entiteit');
                        Raise e_entiteit_onbekend;

                End Case;

            Else
                kgc_dft_interface.log(r_decl.verwerking,
                                             'Controle NIET OK op ' ||
                                             to_char(Sysdate, 'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                                             v_message);
                update_decl(r_decl.decl_id, v_message, v_err_stat, False);
                v_succes := False;
            End If;

            Commit;
            v_message := '';
        Exception
            When e_no_declaration Then
                raise_application_error(-20000,
                                                'VERWERK DECLARATIES' ||
                                                ': p_decl_id niet gevonden: ' ||
                                                to_char(p_decl_id));
            When e_entiteit_onbekend Then
                Begin
                    update_decl(r_decl.decl_id,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message,
                                    v_err_stat,
                                    False);
                    kgc_dft_interface.log(r_decl.verwerking,
                                                 'Controle NIET OK op ' ||
                                                 to_char(Sysdate,
                                                            'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                                                 v_message);
                    v_succes := False;
                End;
        End;
        Return v_succes;

    End;

    /* globaal ***************** */

    Function verwerk_onbe
    (
        p_decl       kgc_declaraties%Rowtype,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean
    /****** VERWERK ALLE DECLARATIES MET DE ENTITEIT ONBE, ONDERZOEKS BETROKKENE *******/
     As

        v_dft_aanvrager t_dft_aanvrager;

        v_succes      Boolean := True;
        v_onde_rec    kgc_onderzoeken%Rowtype;
        v_alternatief Boolean;

        v_verrichting    Varchar2(20);
        v_prod_dept      Varchar2(10);
        v_prod_spec      Varchar2(10);
        v_dewy_code      Varchar2(20);
        v_declaratie_oms Varchar2(20);
        v_abw_code       Varchar2(2);
        v_pers_rec       kgc_personen%Rowtype; -- Persoons record op basis van kgc_declaraties.pers_id
        v_pers_dec       kgc_personen%Rowtype; -- Persoons Record waar de declaratie naar toe gaat
        v_pers_cnt       kgc_personen%Rowtype; -- Persoons Record voor SAP BW Tellingen, ziektegevallen, zodat de juiste ziektegeval bij

        v_stap Varchar2(20);

        -- juiste persoon terecht komt.
    Begin
        Begin
            v_dft_aanvrager := t_dft_aanvrager(Null,
                                                          Null,
                                                          Null,
                                                          Null,
                                                          Null,
                                                          Null);
            v_prod_spec     := 'KLG9999999'; -- Producerende afdeling.
            v_stap          := 'PROD AFD';
            v_prod_dept     := kgc_attr_00.waarde('KGC_ONDERZOEKSGROEPEN',
                                                              'UITVOERENDE_AFDELING',
                                                              p_decl.ongr_id);
            If (v_prod_dept Is Null)
            Then
                add2msg('Declaratie.Producerende afdeling ontbreekt!(extra attribuut PRODUCERENDE_AFDELING bij tabel KGC_ONDERZOEKSGROEPEN)');
                Raise e_no_prd_dep;
            End If;

            v_stap := 'DEWY_CODE';
            -- Haal Code Declaratie Wijze Op.
            v_dewy_code := getdeclaratiewijze(p_decl.dewy_id);

            -- Ophalen ONDERZOEK(ONDE).
            v_stap     := 'ONDERZOEK';
            v_onde_rec := getonderzoek(p_decl.id, p_decl.entiteit);
            -- Ophalen Aanvrager van Onderzoek.
            v_stap          := 'AANVRAGER';
            v_dft_aanvrager := getaanvrager(v_onde_rec.rela_id,
                                                      v_dewy_code,
                                                      p_decl.kafd_id,
                                                      p_decl.pers_id,
                                                      v_prod_dept,
                                                      p_decl.verrichtingcode,
                                         p_decl.ongr_id);

            -- Bepaal op basis van WDS declaraties welk bedrag er gedeclareerd word.
            v_stap := 'WDS';
            If v_dewy_code = 'WDS'
            Then
                v_declaratie_oms := kgc_attr_00.waarde(p_tabel_naam => 'KGC_ONDERZOEK_BETROKKENEN',
                                                                    p_code       => 'BEDRAG',
                                                                    p_id         => p_decl.id);
            Else
                If v_dewy_code Like '%WDS%'
                Then
                    v_declaratie_oms := kgc_attr_00.waarde(p_tabel_naam => 'KGC_DECLARATIEWIJZEN',
                                                                        p_code       => 'BEDRAG',
                                                                        p_id         => p_decl.dewy_id);
                End If;
            End If;

            v_stap     := 'PERSOONS RECORD';
            v_pers_rec := getpersoon(p_decl.pers_id);
            If v_pers_rec.zisnr Is Null
            Then
                add2msg('SAP-nummer ontbreekt');
                Raise e_no_pers;
            End If;

            -- Bepaal ABW CODE
            v_abw_code := getabwcode(v_dewy_code,
                                             p_decl,
                                             v_pers_rec,
                                             'N',
                                             v_dft_aanvrager

                                             );
            v_stap     := 'ABW';

            -- Bepalen naar wie de declaratie verstuurd word op basis van gegevens in declaratie record.
            v_stap := 'BEPAAL DECL PERS';
            If p_decl.pers_id_alt_decl Is Not Null
            Then
                -- Indien alternatief declaratie persoon in declaratie record, declaratie persoon = Alternatief declaratie persoon.
                v_stap        := 'DECL PERS_ALT';
                v_alternatief := True;
                v_pers_dec    := getpersoon(p_decl.pers_id_alt_decl); -- Declaratie Persoon =  kgc_declaraties.pers_id_alt_decl
                v_pers_cnt    := v_pers_rec; -- SAP ziektegeval/BW registratie persoon = kgc_declaraties.pers_id

                If v_abw_code In ('NF', 'WD')
                Then
                    v_verrichting := p_decl.verrichtingcode;
                Else
                    v_verrichting := p_decl.verrichtingcode || 'F';
                End If;

            Else

                v_stap := 'DECL PERS deCL';
                -- Als bovenstaande condities niet gelden dan gaat de declaratie naar de persoon die is in kgc_declaraties.pers_id.
                v_pers_dec    := v_pers_rec; -- Declaratie Persoon =  kgc_declaraties.pers_id
                v_alternatief := False;
                v_verrichting := p_decl.verrichtingcode;

            End If;

            If p_check_only
            Then
                v_succes := True;
                add2msg('Controle OK op: ' ||
                          to_char(Sysdate, 'DD-MM-YYYY hh:mi:ss') || chr(10) ||
                          'Persoon die de factuur ontvangt: ' ||
                          v_pers_dec.zisnr || ' ' || v_pers_dec.aanspreken ||
                          chr(10) || 'Declaratiepersoon volgens Helix: ' ||
                          v_pers_rec.zisnr || ' ' || v_pers_rec.aanspreken);

                update_decl(p_decl.decl_id,
                                v_message,
                                '',
                                False

                                );
            Else
                v_stap := 'VERSTUUR';
                -- Verstuur Declaratie
                v_succes := verstuur_soap_dft(v_pers_dec,
                                                        p_decl.decl_id,
                                                        to_char(p_decl.datum, 'YYYYMMDD'),
                                                        v_verrichting,
                                                        v_declaratie_oms,
                                                        to_char(p_aantal),
                                                        v_dft_aanvrager.order_prov_dep,
                                                        v_prod_spec,
                                                        v_abw_code,
                                                        v_dft_aanvrager.order_provider,
                                                        v_dft_aanvrager.order_prov_nm,
                                                        Replace(v_pers_dec.geslacht,
                                                                  'V',
                                                                  'F'),
                                                        v_prod_dept,
                                                        v_pers_rec.zisnr || ' | ' ||
                                                        v_pers_rec.aanspreken);

                If v_alternatief
                Then
                    -- Stuur extra declaraties uit voor SAP BW Tellingen/SAP ziektegevallen, met TelCode.
                    -- Persoon voor tellingen en completering ziekte gevallen is vastgelegd in v_pers_cnt.
                    -- Benodigd om ervoor te zorgen dat de dossier vorming bij de patient correct is.
                    v_succes := verstuur_soap_dft(v_pers_cnt,
                                                            p_decl.decl_id,
                                                            to_char(p_decl.datum,
                                                                      'YYYYMMDD'),
                                                            '989901',
                                                            v_declaratie_oms,
                                                            to_char(p_aantal),
                                                            v_dft_aanvrager.order_prov_dep,
                                                            v_prod_spec,
                                                            'NF',
                                                            v_dft_aanvrager.order_provider,
                                                            v_dft_aanvrager.order_prov_nm,
                                                            Replace(v_pers_cnt.geslacht,
                                                                      'V',
                                                                      'F'),
                                                            v_prod_dept,
                                                            v_pers_cnt.zisnr || ' | ' ||
                                                            v_pers_cnt.aanspreken);

                End If;

                v_stap := 'LOG SUCCES';

                add2msg('Verwerkt op: ' || to_char(Sysdate, 'DD-MM-YYYY') ||
                          chr(10) || 'Persoon die de factuur ontvangt: ' ||
                          v_pers_dec.zisnr || ' ' || v_pers_dec.aanspreken ||
                          chr(10) || 'Declaratiepersoon volgens Helix: ' ||
                          v_pers_rec.zisnr || ' ' || v_pers_rec.aanspreken);

                If p_aantal >= 1
                Then
                    update_decl(p_decl.decl_id, v_message, 'V', True);
                Else
                    update_decl(p_decl.decl_id, v_message, 'R', True);
                End If;

            End If;

        Exception
            When e_no_onbe Then
                Begin
                    update_decl(p_decl.decl_id,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;
                End;

            When e_no_onde Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_rela Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_pers Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_prd_dep Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_abw_bep Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_err_orderprov Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When Others Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap || Sqlerrm,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;
        End;
        Commit;
        Return v_succes;

    End;

    Function verwerk_onde
    (
        p_decl       kgc_declaraties%Rowtype,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean As

        Cursor c_indi(c_onde_id Number) Is
            Select indi.code
            From   kgc_indicatie_teksten    indi,
                     kgc_onderzoek_indicaties onin
            Where  onin.onde_id = c_onde_id
            And    onin.indi_id = indi.indi_id;


        r_indi          c_indi%Rowtype;
        v_dft_aanvrager t_dft_aanvrager;

        v_succes         Boolean := True;
        v_onde_rec       kgc_onderzoeken%Rowtype;
        v_extradecl      Varchar2(1); -- A is alternatief persoon, T is telRecord en B is beide(alternatief en Tel).
        v_telcodesap     Varchar2(20);
        v_dewy_code      Varchar2(20);
        v_verrichting    Varchar2(20);
        v_catverricht    Varchar2(20);
        v_prod_dept      Varchar2(10);
        v_prod_spec      Varchar2(10);
        v_declaratie_oms Varchar2(20);
        v_abw_code       Varchar2(2);
        v_ovl_foet       Varchar2(1);
        v_pers_rec       kgc_personen%Rowtype; -- Persoons record op basis van kgc_declaraties.pers_id
        v_pers_dec       kgc_personen%Rowtype; -- Persoons Record waar de declaratie naar toe gaat
        v_pers_cnt       kgc_personen%Rowtype; -- Persoons Record voor SAP BW Tellingen, ziektegevallen, zodat de juiste ziektegeval bij
        v_alternatief    Boolean;
        v_stap           Varchar2(20); -- juiste persoon terecht komt.
        v_nf_farma       Varchar2(2);

    Begin
        Begin
            v_extradecl     := '';
            v_dft_aanvrager := t_dft_aanvrager(Null,
                                                          Null,
                                                          Null,
                                                          Null,
                                                          Null,
                                                          Null);
            v_prod_spec     := 'KLG9999999'; -- Producerende afdeling.
            v_stap          := 'PROD AFD';
            v_prod_dept     := kgc_attr_00.waarde('KGC_ONDERZOEKSGROEPEN',
                                                              'UITVOERENDE_AFDELING',
                                                              p_decl.ongr_id);
            If (v_prod_dept Is Null)
            Then
                add2msg('Declaratie.Producerende afdeling ontbreekt!(extra attribuut PRODUCERENDE_AFDELING bij tabel KGC_ONDERZOEKSGROEPEN)');
                Raise e_no_prd_dep;
            End If;

            v_stap := 'DEWY_CODE';
            -- Haal Code Declaratie Wijze Op.
            v_dewy_code := getdeclaratiewijze(p_decl.dewy_id);

            -- Ophalen ONDERZOEK(ONDE).
            v_stap     := 'ONDERZOEK';
            v_onde_rec := getonderzoek(p_decl.id, p_decl.entiteit);

            -- Ophalen Indicatie code
            Open c_indi(v_onde_rec.onde_id);
            Fetch c_indi
                Into r_indi;
            Close c_indi;

            -- Ophalen Aanvrager van Onderzoek.
            v_stap          := 'AANVRAGER';
            v_dft_aanvrager := getaanvrager(v_onde_rec.rela_id,
                                                      v_dewy_code,
                                                      p_decl.kafd_id,
                                                      p_decl.pers_id,
                                                      v_prod_dept,
                                                      p_decl.verrichtingcode,
                            p_decl.ongr_id
                            );

            -- Bepaal op basis van WDS declaraties welk bedrag er gedeclareerd word.
            v_stap := 'WDS';
            If v_dewy_code = 'WDS'
            Then
                v_declaratie_oms := kgc_attr_00.waarde(p_tabel_naam => 'KGC_ONDERZOEKEN',
                                                                    p_code       => 'BEDRAG',
                                                                    p_id         => p_decl.id);
            Else
                If v_dewy_code Like '%WDS%'
                Then
                    v_declaratie_oms := kgc_attr_00.waarde(p_tabel_naam => 'KGC_DECLARATIEWIJZEN',
                                                                        p_code       => 'BEDRAG',
                                                                        p_id         => p_decl.dewy_id);
                End If;
            End If;

            v_stap     := 'PERSOONS RECORD';
            v_pers_rec := getpersoon(p_decl.pers_id);
            If v_pers_rec.zisnr Is Null
            Then
                add2msg('SAPNummer Onbekend');
                Raise e_no_pers;
            End If;

            -- Foetus Overleden?
            --v_ovl_foet := FoetusDeceased_Onde(p_decl.pers_id);

            -- Bepaal ABW CODE

            If p_decl.pers_id_alt_decl Is Not Null
            Then
                -- Indien alternatief declaratie persoon in declaratie record, declaratie persoon = Alternatief declaratie persoon.
                v_stap        := 'DECL PERS_ALT';
                v_alternatief := True;
                v_pers_dec    := getpersoon(p_decl.pers_id_alt_decl); -- Declaratie Persoon =  kgc_declaraties.pers_id_alt_decl
                v_pers_cnt    := v_pers_rec; -- SAP ziektegeval/BW registratie persoon = kgc_declaraties.pers_id

                If v_abw_code In ('NF', 'WD')
                Then
                    v_verrichting := p_decl.verrichtingcode;
                Else
                    v_verrichting := p_decl.verrichtingcode || 'F';
                End If;

            Else
                v_stap := 'DECL PERS deCL';
                -- Als bovenstaande condities niet gelden dan gaat de declaratie naar de persoon die is in kgc_declaraties.pers_id.
                v_pers_dec    := v_pers_rec; -- Declaratie Persoon =  kgc_declaraties.pers_id
                v_alternatief := False;
                v_verrichting := p_decl.verrichtingcode;
            End If;

            Case getcategoriewaardedecl(getonderzoeksgroep(p_decl.ongr_id))
                When 'CYTO' Then
                    Begin
                        v_verrichting := getcategoriewaarde(p_decl.verrichtingcode,
                                                                        'VERR_CYTO');
                        If v_verrichting <> 'ZZ99'
                        Then
                            v_telcodesap := p_decl.verrichtingcode;
                            If v_alternatief
                            Then
                                v_extradecl   := 'B';
                                v_verrichting := v_verrichting || 'F';
                            Else
                                v_extradecl := 'T';
                            End If;

                        Else
                            add2msg('VerrichtingsCode Ontbreekt bij Telcode: ' ||
                                      p_decl.verrichtingcode);
                            Raise e_err_telcode;

                        End If;

                    End;
                When 'DNA' Then
                    Begin
                        If v_alternatief
                        Then
                            v_telcodesap := '989901';
                            v_extradecl  := 'B';
                        End If;
                    End;
                When 'METAB' Then
                    Begin
                        Null;
                    End;

                Else
                    Null;
            End Case;

            -- Bepaal de waarde van v_verrichtingscode.
            -- v_verrichtingscode bevat de daadwerkelijke declarabele verrichtingscode.
            -- Tevens word er aan de hand van onderstaande verglijking bepaald of de
            -- extra declaratie die verstuurd word een telcode en

            v_abw_code := getabwcode(v_dewy_code,
                                             p_decl,
                                             v_pers_rec,
                                             v_ovl_foet,
                                             v_dft_aanvrager

                                             );
            -- AAnpassing Mantis 12436 , uitzondering Indicatie ALPE toegevoegd
            -- geen verrichtingscode 377889 voor farma onderzoeken met de indicatie ALPE
            If (getonderzoeksgroep(p_decl.ongr_id) = 'FARMA') And
                (r_indi.code <> 'ALPE')
            Then
                v_extradecl  := 'P';
                v_telcodesap := '377889';
                add2msg('FARMACOGENETICA DECLARATIE');


            End If;

            If p_check_only
            Then
                v_succes := True;
                add2msg('Controle OK op: ' ||
                          to_char(Sysdate, 'DD-MM-YYYY hh:mi:ss') || chr(10) ||
                          'Persoon die de factuur ontvangt: ' ||
                          v_pers_dec.zisnr || ' ' || v_pers_dec.aanspreken ||
                          chr(10) || 'Declaratiepersoon volgens Helix: ' ||
                          v_pers_rec.zisnr || ' ' || v_pers_rec.aanspreken);

                update_decl(p_decl.decl_id,
                                v_message,
                                '',
                                False

                                );
            Else
                v_stap := 'VERSTUUR';
                -- Verstuur de Declaratie, naar declaratie persoon(betaalende instantie)
                v_succes := verstuur_soap_dft(v_pers_dec,
                                                        p_decl.decl_id,
                                                        to_char(p_decl.datum, 'YYYYMMDD'),
                                                        v_verrichting,
                                                        v_declaratie_oms,
                                                        to_char(p_aantal),
                                                        v_dft_aanvrager.order_prov_dep,
                                                        v_prod_spec,
                                                        v_abw_code,
                                                        v_dft_aanvrager.order_provider,
                                                        v_dft_aanvrager.order_prov_nm,
                                                        Replace(v_pers_dec.geslacht,
                                                                  'V',
                                                                  'F'),
                                                        v_prod_dept,
                                                        v_pers_rec.zisnr || ' | ' ||
                                                        v_pers_rec.aanspreken);

                Case v_extradecl
                    When 'A' Then
                        -- Verstuur ook nog een bericht t.b.v. kgc_Declaraties.pers_id
                        v_succes := verstuur_soap_dft(v_pers_cnt,
                                                                p_decl.decl_id,
                                                                to_char(p_decl.datum,
                                                                          'YYYYMMDD'),
                                                                v_verrichting || 'F',
                                                                v_declaratie_oms,
                                                                to_char(p_aantal),
                                                                v_dft_aanvrager.order_prov_dep,
                                                                v_prod_spec,
                                                                v_abw_code,
                                                                v_dft_aanvrager.order_provider,
                                                                v_dft_aanvrager.order_prov_nm,
                                                                Replace(v_pers_cnt.geslacht,
                                                                          'V',
                                                                          'F'),
                                                                v_prod_dept,
                                                                v_pers_rec.zisnr || ' | ' ||
                                                                v_pers_rec.aanspreken);

                    When 'B' Then
                        -- Stuur tweede bericht t.b.v. kgc_Declaraties.pers_id Met TelcodeSAP
                        v_succes := verstuur_soap_dft(v_pers_cnt,
                                                                p_decl.decl_id,
                                                                to_char(p_decl.datum,
                                                                          'YYYYMMDD'),
                                                                v_telcodesap,
                                                                v_declaratie_oms,
                                                                to_char(p_aantal),
                                                                v_dft_aanvrager.order_prov_dep,
                                                                v_prod_spec,
                                                                'NF',
                                                                v_dft_aanvrager.order_provider,
                                                                v_dft_aanvrager.order_prov_nm,
                                                                Replace(v_pers_cnt.geslacht,
                                                                          'V',
                                                                          'F'),
                                                                v_prod_dept,
                                                                v_pers_rec.zisnr || ' | ' ||
                                                                v_pers_rec.aanspreken);

                    When 'T' Then
                        -- Stuur tweede bericht Met TelcodeSAP
                        v_succes := verstuur_soap_dft(v_pers_dec,
                                                                p_decl.decl_id,
                                                                to_char(p_decl.datum,
                                                                          'YYYYMMDD'),
                                                                v_telcodesap,
                                                                v_declaratie_oms,
                                                                to_char(p_aantal),
                                                                v_dft_aanvrager.order_prov_dep,
                                                                v_prod_spec,
                                                                'NF',
                                                                v_dft_aanvrager.order_provider,
                                                                v_dft_aanvrager.order_prov_nm,
                                                                Replace(v_pers_dec.geslacht,
                                                                          'V',
                                                                          'F'),
                                                                v_prod_dept,
                                                                v_pers_rec.zisnr || ' | ' ||
                                                                v_pers_rec.aanspreken);
                    When 'P' Then

                        /*  24-05-2016 RKON Aanpassing Mantis 12436.
                      Code uit gecommenteerd

                     if r_indi.code = 'DPD' then
                        v_nf_farma := '';
                     else
                        v_nf_farma := 'NF';
                     end if;
                  */
                        v_nf_farma := 'NF';
                        v_succes   := verstuur_soap_dft(v_pers_dec,
                                                                  p_decl.decl_id,
                                                                  to_char(p_decl.datum,
                                                                             'YYYYMMDD'),
                                                                  v_telcodesap,
                                                                  v_declaratie_oms,
                                                                  to_char(p_aantal),
                                                                  'KGMB',
                                                                  'KLG9999999',
                                                                  v_nf_farma,
                                                                  'KLG9999999',
                                                                  'KLG9999999',
                                                                  Replace(v_pers_dec.geslacht,
                                                                             'V',
                                                                             'F'),
                                                                  'KGDN',
                                                                  v_pers_rec.zisnr || ' | ' ||
                                                                  v_pers_rec.aanspreken);


                    Else
                        Null;

                End Case;

                v_stap := 'LOG SUCCES';

                add2msg('Verwerkt op: ' || to_char(Sysdate, 'DD-MM-YYYY') ||
                          chr(10) || 'Persoon die de factuur ontvangt: ' ||
                          v_pers_dec.zisnr || ' ' || v_pers_dec.aanspreken ||
                          chr(10) || 'Declaratiepersoon volgens Helix: ' ||
                          v_pers_rec.zisnr || ' ' || v_pers_rec.aanspreken);

                If p_aantal >= 1
                Then
                    update_decl(p_decl.decl_id, v_message, 'V', True);
                Else

                    update_decl(p_decl.decl_id, v_message, 'R', True);
                End If;

            End If;

        Exception
            When e_no_onbe Then
                Begin
                    update_decl(p_decl.decl_id,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;
                End;

            When e_no_onde Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_rela Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_pers Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_prd_dep Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_abw_bep Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;
            When e_err_orderprov Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_err_telcode Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When Others Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;
        End;
        Commit;
        Return v_succes;

    End;

    Function verwerk_meti
    (
        p_decl       kgc_declaraties%Rowtype,
        p_aantal     In Number,
        p_check_only In Boolean
    ) Return Boolean As

        Cursor c_indi(c_onde_id Number) Is
            Select indi.code
            From   kgc_indicatie_teksten    indi,
                     kgc_onderzoek_indicaties onin
            Where  onin.onde_id = c_onde_id
            And    onin.indi_id = indi.indi_id;


        r_indi c_indi%Rowtype;

        v_dft_aanvrager t_dft_aanvrager;

        v_succes         Boolean := True;
        v_onde_rec       kgc_onderzoeken%Rowtype;
        v_extradecl      Varchar2(1); -- A is alternatief persoon, T is telRecord en B is beide(alternatief en Tel).
        v_telcodesap     Varchar2(20);
        v_dewy_code      Varchar2(20);
        v_verrichting    Varchar2(20);
        v_prod_dept      Varchar2(10);
        v_prod_spec      Varchar2(10);
        v_ovl_foet       Varchar2(1);
        v_declaratie_oms Varchar2(20);
        v_abw_code       Varchar2(2);
        v_alternatief    Boolean;
        v_pers_rec       kgc_personen%Rowtype; -- Persoons record op basis van kgc_declaraties.pers_id
        v_pers_dec       kgc_personen%Rowtype; -- Persoons Record waar de declaratie naar toe gaat
        v_pers_cnt       kgc_personen%Rowtype; -- Persoons Record voor SAP BW Tellingen, ziektegevallen, zodat de juiste ziektegeval bij
        v_nf_farma       Varchar2(2);
        v_stap           Varchar2(20); -- juiste persoon terecht komt.
    Begin
        Begin


            v_extradecl     := '';
            v_dft_aanvrager := t_dft_aanvrager(Null,
                                                          Null,
                                                          Null,
                                                          Null,
                                                          Null,
                                                          Null);
            v_prod_spec     := 'KLG9999999'; -- Producerende afdeling.
            v_stap          := 'PROD AFD';
            v_prod_dept     := kgc_attr_00.waarde('KGC_ONDERZOEKSGROEPEN',
                                                              'UITVOERENDE_AFDELING',
                                                              p_decl.ongr_id);
            If (v_prod_dept Is Null)
            Then
                add2msg('Declaratie. Producerende afdeling ontbreekt!(extra attribuut PRODUCERENDE_AFDELING bij tabel KGC_ONDERZOEKSGROEPEN)');
                Raise e_no_prd_dep;
            End If;

            v_stap := 'DEWY_CODE';
            -- Haal Code Declaratie Wijze Op.
            v_dewy_code := getdeclaratiewijze(p_decl.dewy_id);

            -- Ophalen ONDERZOEK(ONDE).
            v_stap     := 'ONDERZOEK';
            v_onde_rec := getonderzoek(p_decl.id, p_decl.entiteit);
            -- Ophalen Indicatie code
            Open c_indi(v_onde_rec.onde_id);
            Fetch c_indi
                Into r_indi;
            Close c_indi;

            -- Ophalen Aanvrager van Onderzoek.
            v_stap          := 'AANVRAGER';
            v_dft_aanvrager := getaanvrager(v_onde_rec.rela_id,
                                                      v_dewy_code,
                                                      p_decl.kafd_id,
                                                      p_decl.pers_id,
                                                      v_prod_dept,
                                                      p_decl.verrichtingcode,
                            p_decl.ongr_id);
            -- Bepaal op basis van WDS declaraties welk bedrag er gedeclareerd word.
            v_stap := 'WDS';
            If v_dewy_code = 'WDS'
            Then
                v_declaratie_oms := kgc_attr_00.waarde(p_tabel_naam => 'BAS_METINGEN',
                                                                    p_code       => 'BEDRAG',
                                                                    p_id         => p_decl.id);
            Else
                If v_dewy_code Like '%WDS%'
                Then
                    v_declaratie_oms := kgc_attr_00.waarde(p_tabel_naam => 'KGC_DECLARATIEWIJZEN',
                                                                        p_code       => 'BEDRAG',
                                                                        p_id         => p_decl.dewy_id);
                End If;
            End If;

            v_stap     := 'PERSOONS RECORD';
            v_pers_rec := getpersoon(p_decl.pers_id);
            If v_pers_rec.zisnr Is Null
            Then
                add2msg('SAP-nummer ontbreekt');
                Raise e_no_pers;
            End If;
            -- Foetus Overleden?
            -- v_ovl_foet := FoetusDeceased_Meti(p_decl.pers_id);
            -- Bepaal ABW CODE
            v_abw_code := getabwcode(v_dewy_code,
                                             p_decl,
                                             v_pers_rec,
                                             v_ovl_foet,
                                             v_dft_aanvrager

                                             );

            If p_decl.pers_id_alt_decl Is Not Null
            Then
                -- Indien alternatief declaratie persoon in declaratie record, declaratie persoon = Alternatief declaratie persoon.
                v_stap        := 'DECL PERS_ALT';
                v_alternatief := True;
                v_pers_dec    := getpersoon(p_decl.pers_id_alt_decl); -- Declaratie Persoon =  kgc_declaraties.pers_id_alt_decl
                v_pers_cnt    := v_pers_rec; -- SAP ziektegeval/BW registratie persoon = kgc_declaraties.pers_id

                If v_abw_code In ('NF', 'WD')
                Then
                    v_verrichting := p_decl.verrichtingcode;
                Else
                    v_verrichting := p_decl.verrichtingcode || 'F';
                End If;

            Else
                v_stap := 'DECL PERS deCL';
                -- Als bovenstaande condities niet gelden dan gaat de declaratie naar de persoon die is in kgc_declaraties.pers_id.
                v_pers_dec    := v_pers_rec; -- Declaratie Persoon =  kgc_declaraties.pers_id
                v_alternatief := False;
                v_verrichting := p_decl.verrichtingcode;
            End If;

            Case getcategoriewaardedecl(getonderzoeksgroep(p_decl.ongr_id))
                When 'CYTO' Then
                    Begin
                        v_verrichting := getcategoriewaarde(p_decl.verrichtingcode,
                                                                        'VERR_CYTO');
                        If v_verrichting <> 'ZZ99'
                        Then
                            v_telcodesap := p_decl.verrichtingcode;
                            If v_alternatief
                            Then
                                v_extradecl   := 'B';
                                v_verrichting := v_verrichting || 'F';
                            Else
                                v_extradecl := 'T';
                            End If;

                        Else
                            add2msg('VerrichtingsCode Ontbreekt bij Telcode: ' ||
                                      p_decl.verrichtingcode);
                            Raise e_err_telcode;

                        End If;

                    End;
                When 'DNA' Then
                    Begin
                        v_telcodesap := '989901';
                        v_extradecl  := 'B';
                    End;
                When 'METAB' Then
                    Begin
                        Null;
                    End;

                Else
                    Null;
            End Case;

            -- AAnpassing Mantis 12436 , uitzondering Indicatie ALPE toegevoegd
            -- geen verrichtingscode 377889 voor farma onderzoeken met de indicatie ALPE
            If (getonderzoeksgroep(p_decl.ongr_id) = 'FARMA') And
                (r_indi.code <> 'ALPE')
            Then
                v_extradecl  := 'P';
                v_telcodesap := '377889';
                add2msg('FARMACOGENETICA DECLARATIE');

            End If;

            If p_check_only
            Then
                v_succes := True;
                add2msg('Controle OK op: ' ||
                          to_char(Sysdate, 'DD-MM-YYYY hh:mi:ss') || chr(10) ||
                          'Persoon die de factuur ontvangt: ' ||
                          v_pers_dec.zisnr || ' ' || v_pers_dec.aanspreken ||
                          chr(10) || 'Declaratiepersoon volgens Helix: ' ||
                          v_pers_rec.zisnr || ' ' || v_pers_rec.aanspreken);

                update_decl(p_decl.decl_id,
                                v_message,
                                '',
                                False

                                );

            Else
                v_stap := 'VERSTUUR';
                -- Verstuur de Declaratie, naar declaratie persoon(betaalende instantie)

                v_succes := verstuur_soap_dft(v_pers_dec,
                                                        p_decl.decl_id,
                                                        to_char(p_decl.datum, 'YYYYMMDD'),
                                                        v_verrichting,
                                                        v_declaratie_oms,
                                                        to_char(p_aantal),
                                                        v_dft_aanvrager.order_prov_dep,
                                                        v_prod_spec,
                                                        v_abw_code,
                                                        v_dft_aanvrager.order_provider,
                                                        v_dft_aanvrager.order_prov_nm,
                                                        Replace(v_pers_dec.geslacht,
                                                                  'V',
                                                                  'F'),
                                                        v_prod_dept,
                                                        v_pers_rec.zisnr || ' | ' ||
                                                        v_pers_rec.aanspreken);

                Case v_extradecl
                    When 'A' Then
                        -- Verstuur ook nog een bericht t.b.v. kgc_Declaraties.pers_id
                        v_succes := verstuur_soap_dft(v_pers_cnt,
                                                                p_decl.decl_id,
                                                                to_char(p_decl.datum,
                                                                          'YYYYMMDD'),
                                                                v_verrichting || 'F',
                                                                v_declaratie_oms,
                                                                to_char(p_aantal),
                                                                v_dft_aanvrager.order_prov_dep,
                                                                v_prod_spec,
                                                                v_abw_code,
                                                                v_dft_aanvrager.order_provider,
                                                                v_dft_aanvrager.order_prov_nm,
                                                                Replace(v_pers_cnt.geslacht,
                                                                          'V',
                                                                          'F'),
                                                                v_prod_dept,
                                                                v_pers_rec.zisnr || ' | ' ||
                                                                v_pers_rec.aanspreken);

                    When 'B' Then
                        -- Stuur tweede bericht t.b.v. kgc_Declaraties.pers_id Met TelcodeSAP
                        v_succes := verstuur_soap_dft(v_pers_cnt,
                                                                p_decl.decl_id,
                                                                to_char(p_decl.datum,
                                                                          'YYYYMMDD'),
                                                                v_telcodesap,
                                                                v_declaratie_oms,
                                                                to_char(p_aantal),
                                                                v_dft_aanvrager.order_prov_dep,
                                                                v_prod_spec,
                                                                'NF',
                                                                v_dft_aanvrager.order_provider,
                                                                v_dft_aanvrager.order_prov_nm,
                                                                Replace(v_pers_cnt.geslacht,
                                                                          'V',
                                                                          'F'),
                                                                v_prod_dept,
                                                                v_pers_rec.zisnr || ' | ' ||
                                                                v_pers_rec.aanspreken);

                    When 'T' Then
                        -- Stuur tweede bericht Met TelcodeSAP
                        v_succes := verstuur_soap_dft(v_pers_dec,
                                                                p_decl.decl_id,
                                                                to_char(p_decl.datum,
                                                                          'YYYYMMDD'),
                                                                v_telcodesap,
                                                                v_declaratie_oms,
                                                                to_char(p_aantal),
                                                                v_dft_aanvrager.order_prov_dep,
                                                                v_prod_spec,
                                                                'NF',
                                                                v_dft_aanvrager.order_provider,
                                                                v_dft_aanvrager.order_prov_nm,
                                                                Replace(v_pers_dec.geslacht,
                                                                          'V',
                                                                          'F'),
                                                                v_prod_dept,
                                                                v_pers_rec.zisnr || ' | ' ||
                                                                v_pers_rec.aanspreken);
                    When 'P' Then


                        /*  24-05-2016 RKON Aanpassing Mantis 12436.
                      Code uit gecommenteerd

                     if r_indi.code = 'DPD' then
                        v_nf_farma := '';
                     else
                        v_nf_farma := 'NF';
                     end if;
                  */
                        v_nf_farma := 'NF';
                        v_succes   := verstuur_soap_dft(v_pers_dec,
                                                                  p_decl.decl_id,
                                                                  to_char(p_decl.datum,
                                                                             'YYYYMMDD'),
                                                                  v_telcodesap,
                                                                  v_declaratie_oms,
                                                                  to_char(p_aantal),
                                                                  'KGMB',
                                                                  'KLG9999999',
                                                                  v_nf_farma,
                                                                  'KLG9999999',
                                                                  'KLG9999999',
                                                                  Replace(v_pers_dec.geslacht,
                                                                             'V',
                                                                             'F'),
                                                                  'KGDN',
                                                                  v_pers_rec.zisnr || ' | ' ||
                                                                  v_pers_rec.aanspreken);
                    Else
                        Null;

                End Case;

                v_stap := 'LOG SUCCES';

                add2msg('Verwerkt op: ' || to_char(Sysdate, 'DD-MM-YYYY') ||
                          chr(10) || 'Persoon die de factuur ontvangt: ' ||
                          v_pers_dec.zisnr || ' ' || v_pers_dec.aanspreken ||
                          chr(10) || 'Declaratiepersoon volgens Helix: ' ||
                          v_pers_rec.zisnr || ' ' || v_pers_rec.aanspreken);

                If p_aantal >= 1
                Then
                    update_decl(p_decl.decl_id, v_message, 'V', True);
                Else

                    update_decl(p_decl.decl_id, v_message, 'R', True);
                End If;

            End If;

        Exception
            When e_no_onbe Then
                Begin
                    update_decl(p_decl.decl_id,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;
                End;

            When e_no_onde Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_rela Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_pers Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_no_prd_dep Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_abw_bep Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_err_orderprov Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When e_err_telcode Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;

            When Others Then
                Begin
                    update_decl(p_decl.decl_id

                                  ,
                                    'Controle niet OK ' || to_char(Sysdate) || ' ' ||
                                    v_message || ' | ' || v_stap,
                                    v_err_stat,
                                    False);
                    v_succes := False;

                End;
        End;
        Commit;
        Return v_succes;

    End;

    Procedure controleer
    (
        p_decl_id In Number,
        x_ok      Out Boolean
    ) Is
    Begin
        x_ok := verwerk_declaraties(p_decl_id    => p_decl_id,
                                             p_aantal     => 1,
                                             p_check_only => True);
    End controleer;

    Procedure verwerk
    (
        p_decl_id In Number,
        x_ok      Out Boolean
    ) Is
    Begin
        x_ok := verwerk_declaraties(p_decl_id    => p_decl_id,
                                             p_aantal     => 1,
                                             p_check_only => False);

    End verwerk;

    Procedure draai_terug
    (
        p_decl_id In Number,
        x_ok      Out Boolean
    ) Is
    Begin
        x_ok := verwerk_declaraties(p_decl_id    => p_decl_id,
                                             p_aantal     => -1,
                                             p_check_only => False);

    End draai_terug;

    /*---------------------------------------------------------------------------------------------------
     HAAL NIET_GEBOREN VELD INFORMATIE OP VANUIT TABEL KGC_FOETUSSEN, GEEF WAARDE TERUG
     AAN CHECK_VERSTUUR_DFT. R KONINGS JUNI 2009 / NOVEMBER 2009.
   ------------------------------------------------------------------------------------------------------*/

    -------------------------- METI -----------------------------------------------
    -------------------------------------------------------------------------------
    Function foetusdeceased_meti(pid Integer) Return Varchar2 Is
        /* HAAL NIET_GEBOREN GEGEVENS OP VANUIT TABEL KGC_FOETUSSEN OP BASIS VAN HET
        AANGELEVERDE METI_ID.
       JUNI 2009 RKONINGS.
      */

        Cursor foet_onde(pid In Integer) -- RETRIEVE VELD NIET_GEBOREN(J/N)
        Is
            Select foet.niet_geboren
            From   kgc_foetussen foet
            Where  foet.foet_id In
                     (Select onde.foet_id
                      From   kgc_onderzoeken onde
                      Where  onde.onde_id In
                                (Select meti.onde_id
                                 From   bas_metingen meti
                                 Where  meti.meti_id = pid));

        ovl    Varchar2(1);
        ovlchk Boolean; -- CONTROLE OF OVL_CHECK UITGEVOERDKAN WORDEN.

    Begin
        -- HAAL GEGEVENS OP VANUIT KGC_FOETUSSEN(NIET GEBOREN JA/NEE).
        Open foet_onde(pid);
        Fetch foet_onde
            Into ovl;
        ovlchk := foet_onde%Found;
        Close foet_onde;

        -- CHECK OF VELD LEEG IS, ZO JA DAN GEEF EEN FOUTMELDING TERUG
        -- ANDERS GEEF DE INHOUD VAN HET VELD TERUG.
        If ovlchk
        Then
            If Trim(ovl) Is Not Null
            Then
                Return(ovl);
            Else
                Return('E');
            End If;
        Else
            Return('N');

        End If;
        -- INDIEN ER EEN ANDERE EXCEPTION OPTREEDT DAN GEEF CODE E(ERROR) TERUG.
    Exception
        When Others Then
            Return('E');

    End;

    ------------------ ONDE ------------------------------------
    ------------------------------------------------------------

    Function foetusdeceased_onde(pid Integer) Return Varchar2 Is
        /* HAAL NIET_GEBOREN GEGEVENS OP VANUIT TABEL KGC_FOETUSSEN OP BASIS VAN HET
         AANGELEVERDE ONDE_ID.
         JUNI 2009 RKONINGS.
      */

        Cursor foet_onde(pid In Integer) -- RETRIEVE VELD NIET_GEBOREN(J/N)
        Is
            Select foet.niet_geboren
            From   kgc_foetussen foet
            Where  foet.foet_id In (Select onde.foet_id
                                            From   kgc_onderzoeken onde
                                            Where  onde.onde_id = pid);

        ovl    Varchar2(1);
        ovlchk Boolean;

    Begin
        -- HAAL GEGEVENS OP VANUIT KGC_FOETUSSEN(NIET GEBOREN JA/NEE).
        Open foet_onde(pid);
        Fetch foet_onde
            Into ovl;
        ovlchk := foet_onde%Found;
        Close foet_onde;

        -- CHECK OF VELD LEEG IS, ZO JA DAN GEEF EEN FOUTMELDING TERUG
        -- ANDERS GEEF DE INHOUD VAN HET VELD TERUG.
        If ovlchk
        Then
            If Trim(ovl) Is Not Null
            Then
                Return(ovl);
            Else
                Return('E');
            End If;
        Else
            Return('N');

        End If;
        -- INDIEN ER EEN ANDERE EXCEPTION OPTREEDT DAN GEEF CODE E(ERROR) TERUG.
    Exception
        When Others Then
            Return('E');

    End;

    ----------------- ONBE ------------------------------------------------
    -----------------------------------------------------------------------
    Function foetusdeceased_onbe(pid Integer) Return Varchar2 Is

        /* HAAL NIET_GEBOREN GEGEVENS OP VANUIT TABEL KGC_FOETUSSEN OP BASIS VAN HET
          AANGELEVERDE ONBE_ID.
          NOVEMBER 2009 RKONINGS.
      */

        Cursor foet_onde(pid In Integer) -- RETRIEVE VELD NIET_GEBOREN(J/N)
        Is
            Select foet.niet_geboren
            From   kgc_foetussen foet
            Where  foet.foet_id In
                     (Select onde.foet_id
                      From   kgc_onderzoeken onde
                      Where  onde.onde_id In
                                (Select onbe.onde_id
                                 From   kgc_onderzoek_betrokkenen onbe
                                 Where  onbe.onbe_id = pid));
        ovl    Varchar2(1);
        ovlchk Boolean;

    Begin
        -- HAAL GEGEVENS OP VANUIT KGC_FOETUSSEN(NIET GEBOREN JA/NEE).
        Open foet_onde(pid);
        Fetch foet_onde
            Into ovl; -- Haal gegevens foetus op
        ovlchk := foet_onde%Found;
        Close foet_onde;

        -- CHECK OF VELD LEEG IS, ZO JA DAN GEEF EEN FOUTMELDING TERUG
        -- ANDERS GEEF DE INHOUD VAN HET VELD TERUG.
        If ovlchk
        Then
            If Trim(ovl) Is Not Null
            Then
                Return(ovl);
            Else
                Return('E');
            End If;
        Else
            Return('N');

        End If;
        -- INDIEN ER EEN ANDERE EXCEPTION OPTREEDT DAN GEEF CODE E(ERROR) TERUG.
    Exception
        When Others Then
            Return('E');
            -- EINDE functie
    End;

    Function verstuur_soap_dft
    (
        p_pers_rec                kgc_personen%Rowtype,
        p_declaratienr            Integer,
        p_verrichtingsdatum       Varchar2,
        p_verrichting             Varchar2,
        p_omschrijving            Varchar2,
        p_verrichtingshoeveelheid Varchar2,
        p_aanvragende_afdeling    Varchar2,
        p_uitvoerendspecialisme   Varchar2,
        p_abw_code                Varchar2,
        p_aanvragend_specialisme  Varchar2,
        p_aanvragend_speci_nm     Varchar2,
        p_geslacht                Varchar2,
        p_prod_afd                Varchar2,
        p_persoon_declaratie_rec  Varchar2 -- Persoon volgens Kgc_declaraties.pers_id
        --   ,p_log_id                  integer
    ) Return Boolean
    /* FUNCTIE VERSTUREN DFT BERICHTEN*/
     As
        Cursor c_sypa(c_code Varchar2) Is
            Select standaard_waarde
            From   kgc_systeem_parameters
            Where  code = c_code;

        r_sypa c_sypa%Rowtype;

        Cursor c_gname Is
            Select * From global_name;

        r_gname c_gname%Rowtype;

        v_code          Varchar2(30);
        v_log           Integer;
        v_verzenddatum  Date;
        v_verzendtijd   Date;
        v_responsedatum Date;
        v_responsetijd  Date;
        v_response      Clob;
        v_message       Clob;
        e_verstuur_soap_dft_other Exception;

        --Variables voor het versturen van het soap bericht
        l_request  soap_api.t_request;
        l_response soap_api.t_response;

        l_url       Varchar2(32767);
        l_namespace Varchar2(32767);
        l_method    Varchar2(32767);
        -- l_soap_action  VARCHAR2(32767);
        --   l_result_name  VARCHAR2(32767);
        l_env Varchar2(32767);

    Begin
        -- Initialisatie CLOB.
        --    Dbms_Lob.createtemporary(v_Response, False);
        --    Dbms_lob.createtemporary(v_message, False);

        --------------------------------------------------------------
        ---           TRANSFORMEER DATA NAAR XML                   ---
        --------------------------------------------------------------
        --l_url         := 'http://pwiso009.prd.corp:52700/XISOAPAdapter/MessageServlet?senderParty==SYS_Helix_D===HL7_V24_DFT_P03_post_detail_financial_transactions_OutAs=urn:azm-nl:HL7:DFT:v24';
        --l_url         := 'http://awisa011.a.corp:80/XISOAPAdapter/MessageServlet?senderParty==SYS_Helix_Q===HL7_V24_DFT_P03_post_detail_financial_transactions_OutAs=urn:azm-nl:HL7:DFT:v24';
        -- l_url         := 'http://pwisp014.prd.corp:80/XISOAPAdapter/MessageServlet?senderParty==SYS_Helix_P===HL7_V24_DFT_P03_post_detail_financial_transactions_OutAs=urn:azm-nl:HL7:DFT:v24';

        Open c_gname;
        Fetch c_gname
            Into r_gname;
        Close c_gname;

        v_code := 'DFT_' || r_gname.global_name;

        Open c_sypa(v_code);
        Fetch c_sypa
            Into r_sypa;
        If c_sypa%Notfound
        Then
            l_url := Null;
        Else
            l_url := r_sypa.standaard_waarde;
        End If;
        Close c_sypa;

        l_namespace := 'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"';
        l_method    := 'DFT_P03_post_detail_financial_transactions';
        --  l_soap_action := 'http://sap.com/xi/WebService/soap1.1';
        --  l_result_name := '';

        v_log := insert_log_record(p_pers_rec.zisnr,
                                            p_pers_rec.aanspreken,
                                            to_char(p_pers_rec.geboortedatum,
                                                      'dd-mm-yyyy'),
                                            p_declaratienr,
                                            p_verrichtingsdatum,
                                            p_verrichting,
                                            p_omschrijving,
                                            p_aanvragende_afdeling,
                                            p_abw_code,
                                            p_aanvragend_specialisme,
                                            p_prod_afd,
                                            p_persoon_declaratie_rec);

        --Opbouwen van het bericht

        l_request := soap_api.new_request(p_method    => l_method,
                                                     p_namespace => l_namespace);

        soap_api.add_complex_parameter(l_request, '<MSH>');
        -- Field Separator
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Field_Separator',
                                      p_value   => '|');

        -- Encoding Characters
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Encoding_Characters',
                                      p_value   => '^~\;');
        -- Sending application
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Sending_Application>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'namespace_ID',
                                      p_value   => 'HELIX');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Sending_Application>'));

        -- Sending facility
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Sending_Facility>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'namespace_ID',
                                      p_value   => 'HELIX_DB');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Sending_Facility>'));

        -- Receiving application
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Receiving_Application>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'namespace_ID',
                                      p_value   => 'SAP');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Receiving_Application>'));
        -- Receiving facility

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Receiving_Facility>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'namespace_ID',
                                      p_value   => 'SAP_PI');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Receiving_Facility>'));
        -- Date_time_off_message
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Date_Time_Of_Message>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'free_text',
                                      p_value   => to_char(Sysdate,
                                                                  'YYYYMMDDHH24MISS'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Date_Time_Of_Message>'));

        -- Security
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Security/>'));
        -- Message type

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Message_Type>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<privilege>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'identifier',
                                      p_value   => 'DFT');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</privilege>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<privilege_class>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'identifier',
                                      p_value   => 'P03');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</privilege_class>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Message_Type>'));

        -- Message control ID
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Message_Control_ID',
                                      p_value   => to_char(v_log));

        -- Processing ID
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Processing_ID>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'processing_ID',
                                      p_value   => 'P');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Processing_ID>'));

        -- Version ID
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Version_ID>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'version_ID',
                                      p_value   => '2.4');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Version_ID>'));

        -- Accept_Acknowledgment_Type
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Accept_Acknowledgment_Type',
                                      p_value   => 'NE');
        -- Application_Acknowledgment_Type
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Application_Acknowledgment_Type',
                                      p_value   => 'AL');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</MSH>'));

        -- EVN
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<EVN>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Event_Type_Code',
                                      p_value   => 'P03');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Recorded_Date_Time>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'free_text',
                                      p_value   => to_char(Sysdate,
                                                                  'YYYYMMDDHH24MISS'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Recorded_Date_Time>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</EVN>'));

        -- PID
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<PID>'));

        -- Patient_Identifier_List
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Patient_Identifier_List>'));
        -- ID
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'ID',
                                      p_value   => p_pers_rec.zisnr);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<check_digit/>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<code_ID_with_check_digit_identifying_the_check_digit_scheme_employed/>'));
        -- assigning_authority
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<assigning_authority>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'namespace_ID',
                                      p_value   => 'SAP');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</assigning_authority>'));
        -- identifier_type_code
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'identifier_type_code',
                                      p_value   => 'PI');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Patient_Identifier_List>'));
        -- Alternate id
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Alternate_Patient_ID-PID>'));
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<ID/>'));
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Alternate_Patient_ID-PID>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Patient_Name>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<family_name>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'surname',
                                      p_value   => p_pers_rec.achternaam);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</family_name>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Patient_Name>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Date_Time_of_Birth>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'free_text',
                                      p_value   => to_char(p_pers_rec.geboortedatum,
                                                                  'YYYYMMDDHH24MISS'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Date_Time_of_Birth>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Administrative_Sex',
                                      p_value   => p_geslacht);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</PID>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Cluster_2>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<FT1>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Set_ID-FT1',
                                      p_value   => '1');

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Transaction_ID',
                                      p_value   => to_char(p_declaratienr));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Transaction_Date>'));

        --  v_verrichdatchr := to_char(p_verrichtingsdatum, 'YYYYMMDD');
        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'free_text',
                                      p_value   => p_verrichtingsdatum);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Transaction_Date>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Transaction_Posting_Date>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'free_text',
                                      p_value   => to_char(Sysdate,
                                                                  'YYYYMMDDHH24MISS'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Transaction_Posting_Date>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Transaction_Type',
                                      p_value   => 'C');

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Transaction_Code>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'identifier',
                                      p_value   => p_verrichting);

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'text',
                                      p_value   => p_omschrijving);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Transaction_Code>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Transaction_Description',
                                      p_value   => p_omschrijving);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Transaction_Description-Alt/>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'Transaction_Quantity',
                                      p_value   => to_char(p_verrichtingshoeveelheid));

        -- Transaction amount extended
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Transaction_Amount-Extended>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<price>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<quantity/>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</price>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Transaction_Amount-Extended>'));

        -- Transaction_Amount-Unit
        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Transaction_Amount-Unit>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<price>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<quantity/>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</price>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Transaction_Amount-Unit>'));

        -- Department code

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Department_Code>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'identifier',
                                      p_value   => p_prod_afd);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Department_Code>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Insurance_Plan_ID>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'identifier',
                                      p_value   => p_abw_code);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Insurance_Plan_ID>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Performed_By_Code>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'ID_number',
                                      p_value   => p_uitvoerendspecialisme);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Performed_By_Code>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Ordered_By_Code>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'ID_number',
                                      p_value   => p_aanvragend_specialisme);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => '<family_name>');

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'surname',
                                      p_value   => p_aanvragend_speci_nm);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</family_name>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Ordered_By_Code>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</FT1>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Cluster_4>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<ORC_2>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('<Ordering_Facility_Name>'));

        soap_api.add_parameter(p_request => l_request,
                                      p_name    => 'organization_name',
                                      p_value   => p_aanvragende_afdeling);

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Ordering_Facility_Name>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</ORC_2>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Cluster_4>'));

        soap_api.add_complex_parameter(p_request => l_request,
                                                 p_xml     => ('</Cluster_2>'));

        l_env := '<' || l_request.envelope_tag || ':Envelope xmlns:' ||
                    l_request.envelope_tag ||
                    '="http://schemas.xmlsoap.org/soap/envelope/" ' ||
                    'xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">' || '<' ||
                    l_request.envelope_tag || ':Body>' || '<' ||
                    l_request.method || ' ' || l_request.namespace || ' ' ||
                    l_request.envelope_tag ||
                    ':encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' ||
                    l_request.body || '</' || l_request.method || '>' || '</' ||
                    l_request.envelope_tag || ':Body>' || '</' ||
                    l_request.envelope_tag || ':Envelope>';

        --   dbms_lob.writeappend(v_message,Length(l_env),l_env);
        v_message := l_env;
        --------------------------------------------------------------
        ---           LOG HET TE VERSTUREN BERICHT.                ---
        --------------------------------------------------------------
        v_verzenddatum := Sysdate;
        --   v_VerzendTijd  := to_char(sysdate, 'HH:MM:SS');
        update_log_record(p_log_id        => v_log,
                                p_message       => v_message,
                                p_response      => v_response,
                                p_verzenddatum  => v_verzenddatum,
                                p_verzendtijd   => v_verzendtijd,
                                p_responsedatum => Sysdate,
                                p_responsetijd  => 'LEEG',
                                p_optie         => '1');
        Commit;
        ------------------------------------------------------------
        ---              VERZENDEN XML BERICHT                   ---
        ------------------------------------------------------------

        l_response := soap_api.invoke(p_request => l_request,
                                                p_url     => l_url,
                                                p_actions => 'DFT_P03_post_detail_financial_transactions',
                                                p_message => to_char(v_log),
                                                p_auth    => 'J',
                                                p_type    => 'DFT');

        v_response := l_response.envelope_tag;
        dbms_lob.writeappend(v_response,
                                    length(l_response.envelope_tag),
                                    l_response.envelope_tag);

        -------------------------------------------------------------
        ---              LOG het response bericht                 ---
        -------------------------------------------------------------
        v_verzenddatum := Sysdate;
        --   v_VerzendTijd  := to_char(sysdate, 'HH:MM:SS');
        update_log_record(p_log_id        => v_log,
                                p_message       => v_message,
                                p_response      => v_response,
                                p_verzenddatum  => v_verzenddatum,
                                p_verzendtijd   => v_verzendtijd,
                                p_responsedatum => v_responsedatum,
                                p_responsetijd  => v_responsetijd,
                                p_optie         => '2');

        ------------------------------------------------------------
        ---             EINDE                                    ---
        ------------------------------------------------------------

        Return(True); -- Indien alles goed gegaan is geef TRUE terug

        /*   EXCEPTION WHEN OTHERS
           THEN RAISE e_verstuur_soap_dft_other;
      */

    End;

    Function persoonalsfoetus_overleden(ppers_id Number) Return Boolean As
        Cursor c_foet(c_pers_id Number) Is
            Select foet.niet_geboren
            From   kgc_foetussen foet
            Where  foet.pers_id_geboren_als = c_pers_id;

        r_foet c_foet%Rowtype;

    Begin
        Open c_foet(ppers_id);
        Fetch c_foet
            Into r_foet;
        Close c_foet;

        Return(r_foet.niet_geboren = 'J');

    End;

    Procedure setaltdecl As

        Cursor c_onbe(c_onbe_id Number) Is
            Select onbe.pers_id onbepers,
                     onde.pers_id ondepers
            From   kgc_onderzoek_betrokkenen onbe,
                     kgc_onderzoeken           onde
            Where  onbe.onbe_id = c_onbe_id
            And    onbe.onde_id = onde.onde_id;

        r_onbe c_onbe%Rowtype;

    Begin

        For r_decl In (Select *
                            From   kgc_declaraties
                            Where  creation_date > '01-01-2013'
                            And    status Is Null)
        Loop
            If r_decl.pers_id_alt_decl Is Null
            Then

                Open c_onbe(r_decl.id);
                Fetch c_onbe
                    Into r_onbe;
                If c_onbe%Found
                Then

                    If r_onbe.onbepers <> r_onbe.ondepers
                    Then
                        Update kgc_declaraties decl
                        Set    decl.pers_id_alt_decl = r_onbe.ondepers
                        Where  decl.decl_id = r_decl.decl_id;
                        -- and decl.pers_id_alt_decl is NULL; -- added line by GH 2015-05-06
                    End If;

                End If;
                Close c_onbe;
            End If;
        End Loop;

    End;

   Function getcategoriewaardeDecl(p_ongr_cd kgc_onderzoeksgroepen.code%Type)
    Return Varchar2 As

    Cursor c_cawa_ongr(c_ongr_cd kgc_onderzoeksgroepen.code%Type) Is
      Select cate.code
      From   kgc_categorie_types   caty,
           kgc_categorieen       cate,
           kgc_categorie_waarden cawa
      Where  caty.caty_id = cate.caty_id
      And    cawa.cate_id = cate.cate_id
      And    cawa.waarde = c_ongr_cd
      and    caty.code = 'AFDELINGEN';


    r_cawa_ongr c_cawa_ongr%rowtype;
    v_return varchar2(20);
  Begin


      Open c_cawa_ongr(p_ongr_cd);
    Fetch c_cawa_ongr
      Into r_cawa_ongr;
    If c_cawa_ongr%Notfound
    Then
        v_return := 'DNA';

      Else
         v_return := r_cawa_ongr.code;

      End If;

      return v_return;
  End;


End kgc_dft_maastricht_2016;
/

/
QUIT
