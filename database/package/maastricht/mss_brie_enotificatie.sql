CREATE OR REPLACE PACKAGE "HELIX"."MSS_BRIE_ENOTIFICATIE"
IS
  --
  FUNCTION is_interne_kg_rela ( p_rela_id in kgc_relaties.rela_id%type
                              )
  RETURN varchar2;
  --
  FUNCTION alle_koho_zijn_interne_kg_rela ( p_uits_id in kgc_uitslagen.uits_id%type
                                          )
  RETURN varchar2;
  --
  FUNCTION bep_emailadres ( p_rela_id in kgc_relaties.rela_id%type
                          , p_is_aanv in varchar2
                          )
  RETURN varchar2;
  --
  PROCEDURE maak_log_rec ( p_brie_id in kgc_brieven.brie_id%type
                         , p_uits_id in kgc_brieven.uits_id%type
                         , p_onde_id in kgc_brieven.onde_id%type
                         , p_rela_id in kgc_brieven.rela_id%type
                         , p_brty_id in kgc_brieven.brty_id%type
                         );
  --
  PROCEDURE verstuur_email;
  --
END mss_brie_enotificatie;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_BRIE_ENOTIFICATIE"
IS

  FUNCTION is_interne_kg_rela ( p_rela_id in kgc_relaties.rela_id%type
                              )
  RETURN varchar2
  IS
    cursor c_rela ( b_rela_id in kgc_relaties.rela_id%type
                  )
    is
      select null
      from   kgc_relaties rela
      where  rela.rela_id = b_rela_id
      and    rela.relatie_type = 'IN' -- Interne (azM) aanvrager
      and    rela.code like 'KLG%' -- Behorende tot afdeling Klinische Genetica
    ;
    --
    pl_dummy  varchar2(1);
    pl_return varchar2(10);
    --
  BEGIN
    open  c_rela(p_rela_id);
    fetch c_rela
    into  pl_dummy;
    if c_rela%FOUND
    then
      pl_return := 'TRUE';
    else
      pl_return := 'FALSE';
    end if;
    close c_rela;
    --
    return pl_return;
    --
  END is_interne_kg_rela;

  FUNCTION alle_koho_zijn_interne_kg_rela ( p_uits_id in kgc_uitslagen.uits_id%type
                                          )
  RETURN varchar2
  IS
    cursor c_koho ( b_uits_id in kgc_uitslagen.uits_id%type
                  )
    is
      select null -- check bestaan externe relatie
      from   kgc_relaties      rela
      ,      kgc_kopiehouders  koho
      where  koho.uits_id = b_uits_id
      and    rela.rela_id = koho.rela_id
      and    (    rela.relatie_type != 'IN' -- Interne (azM) aanvrager
               or rela.relatie_type is null
               or rela.code not like 'KLG%' -- Behorende tot afdeling Klinische Genetica
             )
    ;
    --
    pl_dummy  varchar2(1);
    pl_return varchar2(10);
    --
  BEGIN
    open  c_koho(p_uits_id);
    fetch c_koho
    into  pl_dummy;
    if c_koho%FOUND -- minimaal 1 externe kopiehouder gevonden
    then
      pl_return := 'FALSE';
    else
      pl_return := 'TRUE';
    end if;
    close c_koho;
    --
    return pl_return;
    --
  END alle_koho_zijn_interne_kg_rela;

  --
  -- Bepaal emailadres
  --
  FUNCTION bep_emailadres ( p_rela_id in kgc_relaties.rela_id%type
                          , p_is_aanv in varchar2
                          )
  RETURN varchar2
  IS
    cursor c_madr ( b_rela_id in kgc_relaties.rela_id%type
                  , b_is_aanv in varchar2
                  )
    is
      select case
               when b_is_aanv = 'J' -- Het betreft de aanvrager
               then lower(decode( upper(substr(rela.code,1,3))
                                , 'KLG', kgc_sypa_00.systeem_waarde('ENOT_EMAILADRES_AAN')
                                , decode( rela.email
                                        , 'LANG', kgc_attr_00.waarde('KGC_RELATIES','EMAIL_TE_LANG',rela.rela_id)
                                        , rela.email
                                        )
                                ))
               else lower(decode( rela.email
                                , 'LANG', kgc_attr_00.waarde('KGC_RELATIES','EMAIL_TE_LANG',rela.rela_id)
                                , rela.email
                                )) -- Het betreft de kopiehouder
             end emailadres
      from   kgc_relaties    rela
      where  rela.rela_id = b_rela_id
    ;
    r_madr_rec c_madr%rowtype;
    pl_return  varchar2(256);
  BEGIN
    open c_madr( p_rela_id
               , p_is_aanv
               );
    fetch c_madr
    into  r_madr_rec;
    if c_madr%NOTFOUND
    then
      pl_return := null;
    else
      pl_return := r_madr_rec.emailadres;
    end if;
    close c_madr;
    --
    return pl_return;
    --
  END bep_emailadres;

  PROCEDURE maak_log_rec ( p_brie_id in kgc_brieven.brie_id%type
                         , p_uits_id in kgc_brieven.uits_id%type
                         , p_onde_id in kgc_brieven.onde_id%type
                         , p_rela_id in kgc_brieven.rela_id%type
                         , p_brty_id in kgc_brieven.brty_id%type
                         )
  IS
    --
    -- Alleen emailnatificatie uitsturen voor aanvrager/kopiehouder indien aan de volgende voorwaarden wordt voldaan:
    -- (1) Aanvrager/koipehouder is interne (azM) aanvrager/kopiehouder
    -- (2) Sapnr van patient mag niet leeg zijn
    -- (3) Er is voor de betreffende aanvrager/kopiehouder niet eerder een emailnotificatie verstuurd waarvan de brief niet is verwijderd
    -- (4) Brief is van bepaald brieftype
    -- (5) Emailadres waarnaar de emailnotificatie wordt gestuurd moet eindigen op @mumc.nl (hoofdletterongevoelig)
    --
    cursor c_aanvr_koho( b_uits_id in kgc_brieven.uits_id%type
                       , b_onde_id in kgc_brieven.onde_id%type
                       , b_rela_id in kgc_brieven.rela_id%type
                       , b_brty_id in kgc_brieven.brty_id%type
                       )
    is
      select onde.onde_id                                               onde_id
      ,      onde.onderzoeknr                                           onderzoeknr
      ,      rela.rela_id                                               rela_id
      ,      onde.referentie                                            referentie
      ,      pers.pers_id                                               pers_id
      ,      pers.zisnr                                                 zisnr
      ,      kgc_indi_00.onderzoek_indi_omschrijvingen(onde.onde_id)    indi_omschrijving
      ,      bep_emailadres( rela.rela_id
                           , decode( rela.rela_id
                                   , onde.rela_id, 'J'
                                   ,'N'
                                   )
                           )                                            emailadres
      from   kgc_brieftypes          brty
      ,      kgc_relaties            rela
      ,      kgc_personen            pers
      ,      kgc_onderzoeksgroepen   ongr
      ,      kgc_onderzoeken         onde
      ,      kgc_uitslagen           uits
      where  uits.uits_id = b_uits_id
      and    onde.onde_id = b_onde_id
      and    onde.onde_id = uits.onde_id
      and    ongr.ongr_id = onde.ongr_id
      and    not exists
               ( select null
                 from   mss_brie_enotificatie_log enot
                 where  enot.brty_id = b_brty_id
                 and    enot.uits_id = b_uits_id
                 and    enot.onde_id = b_onde_id
                 and    enot.rela_id = b_rela_id
                 and    enot.brie_deleted = 'N'
               )
      and    pers.pers_id = onde.pers_id
      and    pers.zisnr is not null
      and    rela.rela_id = b_rela_id
      and    rela.relatie_type = 'IN'
      and    bep_emailadres( rela.rela_id
                           , decode( rela.rela_id
                                   , onde.rela_id, 'J'
                                   ,'N'
                                   )
                           ) like '%@mumc.nl'
      and    brty.brty_id = b_brty_id
      and    ( (      ongr.code in ('CYTOM', 'CYTOV', 'DNA', 'FARMA', 'PGD', 'PM', 'PR', 'TM', 'TV', 'PV')
                 and  brty.code in
                       ( select cawa.waarde
                         from   kgc_categorie_waarden  cawa
                         ,      kgc_categorieen        cate
                         ,      kgc_categorie_types    caty
                         where  caty.code = 'BRIE_EMAIL'
                         and    cate.caty_id = caty.caty_id
                         and    cawa.cate_id = cate.cate_id
                       )
               )
               or
               (      ongr.code in ('METAB','E', 'BASIS2016')
                 and  brty.code not in ('EMAIL') -- Bij versturen van emailnotificatie wordt brief van type EMAIL gemaakt
               )
             )
    ;
    --
    r_aanvr_koho_rec  c_aanvr_koho%rowtype;
    pl_found          boolean;
    --
  BEGIN
    open  c_aanvr_koho ( p_uits_id
                       , p_onde_id
                       , p_rela_id
                       , p_brty_id
                       );
    fetch c_aanvr_koho
    into  r_aanvr_koho_rec;
    pl_found := c_aanvr_koho%FOUND;
    close c_aanvr_koho;
    --
    if pl_found
    then
      --
      -- Er is aan de voorwaarden voldaan om een emailnotificatie te versturen. Maak logfile aan voor de te versturen email.
      --
      insert into mss_brie_enotificatie_log
      ( brel_id
      , brie_id
      , brty_id
      , brie_deleted
      , onde_id
      , onderzoeknr
      , rela_id
      , referentie
      , uits_id
      , naam_aanvr_kopieh
      , emailadres_van
      , emailadres_aan
      , email_inhoud
      , pers_id
      , zisnr
      , indicatie
      , email_verstuurd
      , datum_verstuurd
      , creation_date
      )
      values
      ( mss_brel_seq.nextval
      , p_brie_id
      , p_brty_id
      , 'N'
      , r_aanvr_koho_rec.onde_id
      , r_aanvr_koho_rec.onderzoeknr
      , r_aanvr_koho_rec.rela_id
      , r_aanvr_koho_rec.referentie
      , p_uits_id
      , mss_samenvoegen.naam_aanvr_kopieh(p_uits_id,'B')
      , kgc_sypa_00.systeem_waarde('ENOT_EMAILADRES_VAN')
      , r_aanvr_koho_rec.emailadres
      , null
      , r_aanvr_koho_rec.pers_id
      , r_aanvr_koho_rec.zisnr
      , r_aanvr_koho_rec.indi_omschrijving
      , 'N'
      , null
      , sysdate
      );
    else
      null;
    end if;
    --
  END maak_log_rec;

  PROCEDURE verstuur_email
  IS
    --
    -- Ophalen te versturen emailgegevens.
    --
    cursor c_benl
    is
      select benl.brie_id                                                           brie_id
      ,      case
               when benl.rela_id = onde.rela_id -- Het betreft de aanvrager
               then 'A' -- aanvrager
               else 'K' -- kopiehouder
             end                                                                    aanvr_koho
      ,      brty.omschrijving                                                      brie_oms
      ,      benl.onde_id                                                           onde_id
      ,      benl.onderzoeknr                                                       onderzoeknr
      ,      case
               when brty.code = 'UITSLAG_T'
               then ( select distinct
                             kgc_mede_00.medewerker_naam(nvl(first_value(audl.waarde_oud) over (partition by audl.id,audl.tabel_naam,audl.aktie order by audl.datum_tijd desc),-99))
                      from   kgc_audit_log audl
                      where  audl.id = benl.uits_id
                      and    audl.tabel_naam = 'KGC_UITSLAGEN'
                      and    audl.aktie = 'UPD'
                      and    audl.waarde_oud is not null
                      and    audl.waarde_nieuw is null
                    )
               else mede.naam                                                                   -- Bij tussentijdse uitslagen wordt veld autorisator niet gevuld
             end                                                                    autorisator -- met de autorisatornaam. Gebruik in dat geval de auditlogging.
      ,      benl.referentie                                                        referentie
      ,      benl.uits_id                                                           uits_id
      ,      substr(uits.tekst,1,100)                                               uits_tekst -- Neem eerste 100 karakters
      ,      mss_samenvoegen.naam_aanvr_kopieh(benl.uits_id,'A')                    naam_aanvr
      ,      mss_samenvoegen.naam_aanvr_kopieh(benl.uits_id,'K')                    naam_kopieh
      ,      benl.emailadres_van                                                    emailadres_van
      ,      benl.emailadres_aan                                                    emailadres_aan
      ,      benl.pers_id                                                           pers_id
      ,      pers.aanspreken                                                        pers_aanspreken
      ,      to_char(pers.geboortedatum,'dd-mm-yyyy')                               pers_geboortedatum
      ,      pers.geslacht                                                          pers_geslacht
      ,      mss_samenvoegen.familienummers(benl.onde_id)                           familienr
      ,      mss_samenvoegen.genen(benl.onde_id)                                    genen
      ,      mss_samenvoegen.xnrs(benl.onde_id)                                     xnrs
      ,      case -- Haal voorloopnullen weg, check eerst of zisnr getal is
               when length(trim(translate(benl.zisnr,'0123456789',' '))) is null
               then to_char(to_number(benl.zisnr))
               else benl.zisnr
             end                                                                    bsn
      ,      benl.indicatie                                                         indicatie
      ,      mss_samenvoegen.onde_indi_opm_regel(benl.onde_id)                      indicatie_opm
      ,      onde.kafd_id                                                           kafd_id
      ,      benl.afdeling_code                                                     afdeling_code
      ,      benl.email_verstuurd                                                   email_verstuurd
      ,      benl.datum_verstuurd                                                   datum_verstuurd
      ,      benl.creation_date                                                     creation_date
      from   kgc_uitslagen               uits
      ,      kgc_personen                pers
      ,      kgc_brieftypes              brty
      ,      kgc_brieven                 brie
      ,      kgc_onderzoek_uitslagcodes  onui
      ,      kgc_medewerkers             mede
      ,      kgc_onderzoeken             onde
      ,      mss_brie_enotificatie_log   benl
      where  benl.email_verstuurd = 'N'
      and    benl.brie_deleted = 'N'
      and    benl.foutmelding is null
      and    onde.onde_id = benl.onde_id
      and    mede.mede_id (+) = onde.mede_id_autorisator
      and    onui.onui_id (+) = onde.onui_id
      and    brie.brie_id = benl.brie_id
      and    brty.brty_id = brie.brty_id
      and    pers.pers_id = benl.pers_id
      and    uits.uits_id = benl.uits_id
      order
      by     benl.afdeling_code
      ,      benl.zisnr
      for update of benl.email_verstuurd
    ;
    --
    cursor c_brty
    is
      select brty.brty_id
      from   kgc_brieftypes brty
      where  brty.code = 'EMAIL'
    ;
    --
    cursor c_inon (b_onde_id in kgc_onderzoeken.onde_id%type)
    is
      select indi.omschrijving  indicatie
      ,      onin.opmerking     opmerking
      from   kgc_indicatie_teksten     indi
      ,      kgc_onderzoek_indicaties  onin
      where  onin.onde_id = b_onde_id
      and    indi.indi_id = onin.indi_id
      order
      by     onin.volgorde asc
      ,      onin.onin_id  asc
    ;
    --
    pl_email_onderwerp  varchar2(512);
    pl_email_tekst      varchar2(4000);
    pl_ref_tekst        varchar2(200);
    pl_brty_id          kgc_brieftypes.brty_id%type;
    pl_indicaties       varchar2(512);
    pl_opmerkingen      varchar2(512);
    pl_eerste_keer      boolean;
    pl_kopregel         varchar2(256) := '<span style="font-family:arial;font-size:10pt;color:black">Beste collega,<br><br>';
    pl_sluitregel       varchar2(256) := '<br><br> </p> <p style="font-family:arial;font-size:8pt;color:gray">*** Dit is een automatisch gegenereerd emailbericht. '||
                                         'Antwoorden op deze mail is niet mogelijk. ***</span>';
    pl_err_msg          varchar2(100);
  BEGIN
    --
    open  c_brty;
    fetch c_brty
    into  pl_brty_id;
    close c_brty;
    --
    for r_benl_rec in c_benl
    loop
      --
      pl_indicaties  := null;
      pl_opmerkingen := null;
      pl_eerste_keer := TRUE;
      --
      for r_inon_rec in c_inon(r_benl_rec.onde_id)
      loop
        if pl_eerste_keer = TRUE
        then
          pl_indicaties  := r_inon_rec.indicatie;  -- indicatie altijd gevuld
          pl_opmerkingen := r_inon_rec.opmerking;  -- opmerking kan leeg zijn
        else
          pl_indicaties  := pl_indicaties  || ', ' || r_inon_rec.indicatie;
          if r_inon_rec.opmerking is not null
          then
            if pl_opmerkingen is null
            then
              pl_opmerkingen := r_inon_rec.opmerking;
            else
              pl_opmerkingen := pl_opmerkingen || ', ' || r_inon_rec.opmerking;
            end if;
          else
            null;
          end if;
        end if;
        --
        pl_eerste_keer := FALSE;
      end loop;
      --
      if r_benl_rec.aanvr_koho = 'K' -- Indien kopiehouder, dan dient er in de onderwerp regel ***KOPIE*** te worden opgenomen (vooraan in de regel).
      then
        pl_email_onderwerp := '***KOPIE*** KG-uitslag gereed voor patient '||
                                r_benl_rec.bsn||', '||r_benl_rec.xnrs||', '||r_benl_rec.genen||', aanvr: '||r_benl_rec.naam_aanvr;
      else
        pl_email_onderwerp := 'KG-uitslag gereed voor patient '||
                                r_benl_rec.bsn||', '||r_benl_rec.xnrs||', '||r_benl_rec.genen||', aanvr: '||r_benl_rec.naam_aanvr;
      end if;
      --
      pl_email_tekst := pl_kopregel||
                          'De volgende klinisch genetische uitslag is gereed. Deze is beschikbaar in het EPD.<br><br>'||
                          '<table border="1">'||
                          '<tr><td><strong>SAP-nr</strong></td><td>'||r_benl_rec.bsn||'</td></tr>'||
                          '<tr><td><strong>Naam patient</strong></td><td>'||r_benl_rec.pers_aanspreken||' ('||r_benl_rec.pers_geslacht||')'||'</td></tr>'||
                          '<tr><td><strong>Geboortedatum</strong></td><td>'||r_benl_rec.pers_geboortedatum||'</td></tr>'||
                          '<tr><td><strong>Klinisch Genetisch Dossier</strong></td><td>'||r_benl_rec.xnrs||'</td></tr>'||
                          '<tr><td><strong>Familienummer</strong></td><td>'||r_benl_rec.familienr||'</td></tr>'||
                          '<tr><td><strong>Onderzoeksnummer</strong></td><td>'||r_benl_rec.onderzoeknr||'</td></tr>'||
                          '<tr><td><strong>Indicatie(s)</strong></td><td>'||pl_indicaties||'</td></tr>'||
                          '<tr><td><strong>Indicatie opmerking(en)</strong></td><td>'||pl_opmerkingen||'</td></tr>'||
                          '<tr><td><strong>Gen</strong></td><td>'||r_benl_rec.genen||'</td></tr>'||
                          '<tr><td><strong>Autorisator</strong></td><td>'||r_benl_rec.autorisator||'</td></tr>'||
                          '<tr><td><strong>Aanvrager</strong></td><td>'||r_benl_rec.naam_aanvr||'</td></tr>'||
                          '<tr><td><strong>Kopiehouders</strong></td><td>'||r_benl_rec.naam_kopieh||'</td></tr>'||
                          '<tr><td><strong>Uitslag-tekstfragment</strong></td><td>'||r_benl_rec.uits_tekst||'...'||'</td></tr>'||
                          '<tr><td><strong>Brieftype</strong></td><td>'||r_benl_rec.brie_oms||'</td></tr>'||
                          '</table>'||
                          pl_sluitregel;
      --
      begin
        mss_mail.post_mail( r_benl_rec.emailadres_van
                          , r_benl_rec.emailadres_aan
                          , r_benl_rec.emailadres_van  -- CC
                          , pl_email_onderwerp
                          , pl_email_tekst
                          , TRUE -- HTML
                          );
        --
        update mss_brie_enotificatie_log benl2
        set    benl2.email_inhoud = pl_email_tekst
        ,      benl2.email_verstuurd = 'J'
        ,      benl2.datum_verstuurd = sysdate
        where  current of c_benl;
        --
        -- Wanneer er een emailnotificatie gegenereerd wordt, dient deze ook als brief in KGC_BRIEVEN geregistreerd te worden met brieftype "EMAIL".
        --
        insert into kgc_brieven
        ( brie_id
        , kafd_id
        , pers_id
        , datum_print
        , datum_verzonden
        , geadresseerde
        , brty_id
        , onde_id
        , uits_id
        )
        values
        ( kgc_brie_seq.nextval
        , r_benl_rec.kafd_id
        , r_benl_rec.pers_id
        , sysdate
        , sysdate
        , r_benl_rec.emailadres_aan
        , pl_brty_id
        , r_benl_rec.onde_id
        , r_benl_rec.uits_id
        );
      exception
        when others
        then begin
               pl_err_msg := substr(SQLERRM, 1, 100);
               --
               update mss_brie_enotificatie_log benl2
               set    benl2.foutmelding = pl_err_msg
               where  current of c_benl;
               --
               mss_mail.post_mail( r_benl_rec.emailadres_van
                                 , 'functioneelbeheer.kg@mumc.nl'
                                 , '!!! FOUT BIJ VERSTUREN EMAILNOTIFICATIE NAAR '||r_benl_rec.emailadres_aan||' ('||r_benl_rec.onderzoeknr||') !!!'
                                 , 'Foutmelding: '||pl_err_msg
                                 , TRUE -- HTML
                                 );
             end;
      end;
    end loop;
    --
  END verstuur_email;

END mss_brie_enotificatie;
/

/
QUIT
