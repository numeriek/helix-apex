CREATE OR REPLACE PACKAGE "HELIX"."BEH_STAMBOOM_00" as

g_scheidingsteken varchar2(2) := ', ';

function haal_alle_fami_op
(
  p_pers_id in kgc_personen.pers_id%type
)
return varchar2
;

function haal_alle_onde_op
(
  p_pers_id in kgc_personen.pers_id%type
)
return varchar2
;

function waardebepaling
(
  p_meet_id        in  bas_meetwaarden.meet_id%type
, p_mwst_code      in  kgc_meetwaardestructuur.code%type
, p_kolom_in_view  in  varchar2
, p_promptnr       in  number
)
return varchar2
;

function foetus_mons
(
  p_foet_id in kgc_foetussen.foet_id%type
)
return varchar2
;

function pers_id_moeder
(
  p_mons_pers_id in kgc_monsters.pers_id%type
)
return number
;

function foetus_pers
(
  p_mons_pers_id in kgc_monsters.pers_id%type
)
return varchar2
;

end beh_stamboom_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_STAMBOOM_00" as

function haal_alle_fami_op
( p_pers_id in kgc_personen.pers_id%type
)
return varchar2
is
  cursor c_fami( b_pers_id in kgc_personen.pers_id%type
               )
  is
    select fale.pers_id
    ,      fami.fami_id
    ,      fami.familienummer
    from   kgc_families       fami
    ,      kgc_familie_leden  fale
    where  fale.pers_id = b_pers_id
    and    fale.fami_id = fami.fami_id
  ;
  --
  pl_return varchar2(10000);
  pl_count  number := 0;
begin
  for r_fami_rec in c_fami(p_pers_id)
  loop
    pl_count := pl_count + 1;
    --
    if pl_count = 1
    then
      pl_return := r_fami_rec.familienummer;
    else
      pl_return := pl_return || g_scheidingsteken || r_fami_rec.familienummer;
    end if;
  end loop;
  --
  return pl_return;
end haal_alle_fami_op;

function haal_alle_onde_op
( p_pers_id in kgc_personen.pers_id%type
)
return varchar2
is
  cursor c_onde( b_pers_id in kgc_personen.pers_id%type
               )
  is
    select onde.onderzoeknr
    from   kgc_onderzoeken onde
    where  onde.pers_id = b_pers_id
  ;
  --
  pl_return varchar2(10000);
  pl_count  number := 0;
begin
  for r_onde_rec in c_onde(p_pers_id)
  loop
    pl_count := pl_count + 1;
    --
    if pl_count = 1 then
      pl_return := r_onde_rec.onderzoeknr;
    else
      pl_return := pl_return || g_scheidingsteken || r_onde_rec.onderzoeknr;
    end if;
  end loop;
  --
  return pl_return;
end haal_alle_onde_op;

function waardebepaling
(
  p_meet_id        in  bas_meetwaarden.meet_id%type
, p_mwst_code      in  kgc_meetwaardestructuur.code%type
, p_kolom_in_view  in  varchar2
, p_promptnr       in  number
)
return varchar2
is
  cursor c_stam( b_mwst_code      in  kgc_meetwaardestructuur.code%type
               , b_kolom_in_view  in  varchar2
               , b_meet_id        in  bas_meetwaarden.meet_id%type
               , b_promptnr       in  number
               )
  is
    select stam.mdet_prompt                  mdet_prompt
    ,      stam.waarde_kolom_in_view         waarde_kol_in_view
    ,      stam.spw                          spw
    ,      stam.aantal_sgest_prompts         aant_sgest_prompts
    ,      stam.samengesteld_prompt1         sprompt1
    ,      stam.samengesteld_prompt2         sprompt2
    ,      stam.samengesteld_prompt3         sprompt3
    ,      case
             when b_promptnr = 1
             then stam.samengesteld_prompt1
             when b_promptnr = 2
             then stam.samengesteld_prompt2
             when b_promptnr = 3
             then stam.samengesteld_prompt3
            end                              sprompt
    ,      stam.soort_samenstelling          samenstelling
    ,      to_char(null)                     waarde
    from   beh_waardebep_stamboom  stam
    where  stam.mwst_code = b_mwst_code
    and    upper(stam.kolom_in_view) = upper(b_kolom_in_view)
    and    stam.spw not in ('W', 'PW')
    union
    select stam.mdet_prompt                  mdet_prompt
    ,      stam.waarde_kolom_in_view         waarde_kol_in_view
    ,      stam.spw                          spw
    ,      stam.aantal_sgest_prompts         aant_sgest_prompts
    ,      stam.samengesteld_prompt1         sprompt1
    ,      stam.samengesteld_prompt2         sprompt2
    ,      stam.samengesteld_prompt3         sprompt3
    ,      to_char(null)                     sprompt
    ,      stam.soort_samenstelling          samenstelling
    ,      case
             when stam.spw = 'PW'
             then replace(stam.mdet_prompt,'p.A467T aanw?','p.A467T')||' '||mdet.waarde
             else mdet.waarde
           end                               waarde
    from   bas_mdet_ok             mdok
    ,      bas_meetwaarde_details  mdet
    ,      beh_waardebep_stamboom  stam
    where  stam.mwst_code = b_mwst_code
    and    upper(stam.kolom_in_view) = upper(b_kolom_in_view)
    and    stam.spw in ('W', 'PW')
    and    mdet.meet_id = b_meet_id
    and    mdok.mdet_id = mdet.mdet_id
    and    mdok.naar_stamboom = 'J'
    and    upper(mdet.prompt) = upper(stam.mdet_prompt)
  ;
  --
  cursor c_sgst( b_meet_id      in  bas_meetwaarden.meet_id%type
               , b_mdet_prompt  in  bas_meetwaarde_details.prompt%type
               )
  is
    select mdet.waarde  waarde
    from   bas_mdet_ok             mdok
    ,      bas_meetwaarde_details  mdet
    where  mdet.meet_id = b_meet_id
    and    mdok.mdet_id = mdet.mdet_id
    and    mdok.naar_stamboom = 'J'
    and    upper(mdet.prompt) = upper(b_mdet_prompt)
  ;
  --
  cursor c_sgst_t( b_meet_id        in  bas_meetwaarden.meet_id%type
                 , b_mdet_prompt    in  bas_meetwaarde_details.prompt%type
                 , b_mwst_code      in  beh_wbep_sboom_s_mdet_w.mwst_code%type
                 , b_kolom_in_view  in  beh_wbep_sboom_s_mdet_w.kolom_in_view%type
                 )
  is
    select wsmw.waarde_kolom_in_view  waarde
    from   beh_wbep_sboom_s_mdet_w wsmw
    ,      bas_mdet_ok             mdok
    ,      bas_meetwaarde_details  mdet
    where  mdet.meet_id = b_meet_id
    and    mdok.mdet_id = mdet.mdet_id
    and    mdok.naar_stamboom = 'J'
    and    upper(mdet.prompt) = upper(b_mdet_prompt)
    and    wsmw.mwst_code = b_mwst_code
    and    upper(wsmw.kolom_in_view) = upper(b_kolom_in_view)
    and    upper(wsmw.prompt) = upper(mdet.prompt)
    and    upper(wsmw.mdet_waarde) = upper(mdet.waarde)
  ;
  --
  r_stam_rec    c_stam%rowtype;
  r_sgst_rec    c_sgst%rowtype;
  r_sgst_t_rec  c_sgst_t%rowtype;
  pl_sprompt    bas_meetwaarde_details.prompt%type;
  pl_swaarde    varchar2(100);
  pl_found      boolean;
  pl_return     varchar2(10000);
  --
begin
  for r_stam_rec in c_stam( p_mwst_code
                          , p_kolom_in_view
                          , p_meet_id
                          , p_promptnr
                          )
  loop
    if r_stam_rec.spw in ('PW', 'W') -- <Prompt> <Waarde>, <Waarde>
    then
      if  r_stam_rec.mdet_prompt = 'Naam vd afw.'
      and r_stam_rec.waarde is null
      then
        pl_return := 'N'; -- Normaal
      else
        pl_return := r_stam_rec.waarde;
      end if;
    elsif r_stam_rec.spw = 'PWT' -- <Prompt> <Waarde> bepaal getoonde waarde via tabel BEH_WBEP_SBOOM_S_MDET_W
    then
      open  c_sgst_t( p_meet_id
                    , pl_sprompt
                    , p_mwst_code
                    , p_kolom_in_view
                    );
      fetch c_sgst_t
      into  r_sgst_t_rec;
      pl_found := c_sgst_t%FOUND;
      close c_sgst_t;
      --
      if  pl_found
      and r_sgst_t_rec.waarde is not null
      then
        pl_return := r_sgst_t_rec.waarde;
      else
        pl_return := null;
      end if;
    elsif r_stam_rec.spw = 'SWN' -- Samengesteld, meerdere prompts, resultaat is uiteindelijk meerdere records
    then                         -- met per record 1 mdet waarde obv van 1 prompt (= alleen mwst.code 'SEQ_FRAG','SEQ_STND' of 'SEQ_STND_O'
      open  c_sgst( p_meet_id
                  , r_stam_rec.sprompt
                  );
      fetch c_sgst
      into  r_sgst_rec;
      pl_found := c_sgst%FOUND;
      close c_sgst;
      --
      if  pl_found
      and r_sgst_rec.waarde is not null
      then
        pl_return := r_sgst_rec.waarde;
      else
        pl_return := null;
      end if;
    elsif r_stam_rec.spw = 'STN'  -- Samengesteld, bepaal getoonde waarde via tabel BEH_WBEP_SBOOM_S_MDET_W,
    then                          -- meerdere prompts, resultaat is uiteindelijk meerdere records
      open  c_sgst_t( p_meet_id   -- met per record 1 mdet waarde obv van 1 prompt (alleen mwst.code = 'SEQ_FRAG','SEQ_STND' of 'SEQ_STND_O'
                    , r_stam_rec.sprompt
                    , p_mwst_code
                    , p_kolom_in_view
                    );
      fetch c_sgst_t
      into  r_sgst_t_rec;
      pl_found := c_sgst_t%FOUND;
      close c_sgst_t;
      --
      if  pl_found
      and r_sgst_t_rec.waarde is not null
      then
        pl_return := r_sgst_t_rec.waarde;
      else
        pl_return := null;
      end if;
    elsif r_stam_rec.spw = 'SW1' -- Samengesteld, meerdere prompts, resultaat is uiteindelijk 1 record
    then                         -- met samengestelde waarde van aan elkaar geplakte mdet waarden obv meerdere prompts
      pl_swaarde := null;        -- (= alleen mwst.code != 'SEQ_FRAG','SEQ_STND' of 'SEQ_STND_O')
      --
      for pl_teller in 1..r_stam_rec.aant_sgest_prompts
      loop
        if pl_teller = 1
        then
          pl_sprompt := r_stam_rec.sprompt1;
        elsif pl_teller = 2
        then
          pl_sprompt := r_stam_rec.sprompt2;
        elsif pl_teller = 3
        then
          pl_sprompt := r_stam_rec.sprompt3;
        else
          pl_sprompt := null;
        end if;
        --
        open  c_sgst( p_meet_id
                    , pl_sprompt
                    );
        fetch c_sgst
        into  r_sgst_rec;
        pl_found := c_sgst%FOUND;
        close c_sgst;
        --
        if  pl_found
        and r_sgst_rec.waarde is not null
        then
          if pl_swaarde is null
          then
            pl_swaarde := r_sgst_rec.waarde;
          else
            pl_swaarde := pl_swaarde||'/'||r_sgst_rec.waarde;
          end if;
        else
          null;
        end if;
      end loop;
    elsif r_stam_rec.spw = 'ST1' -- Samengesteld, bepaal getoonde waarde via tabel BEH_WBEP_SBOOM_S_MDET_W ,meerdere prompts,
    then                         -- resultaat is uiteindelijk 1 record met samengestelde waarde van aan elkaar geplakte
      pl_swaarde := null;        -- mdet waarden obv meerdere prompts (= alleen mwst.code != 'SEQ_FRAG','SEQ_STND' of 'SEQ_STND_O')
      --
      for pl_teller in 1..r_stam_rec.aant_sgest_prompts
      loop
        if pl_teller = 1
        then
          pl_sprompt := r_stam_rec.sprompt1;
        elsif pl_teller = 2
        then
          pl_sprompt := r_stam_rec.sprompt2;
        elsif pl_teller = 3
        then
          pl_sprompt := r_stam_rec.sprompt3;
        else
          pl_sprompt := null;
        end if;
        --
        open  c_sgst_t( p_meet_id
                      , pl_sprompt
                      , p_mwst_code
                      , p_kolom_in_view
                      );
        fetch c_sgst_t
        into  r_sgst_t_rec;
        pl_found := c_sgst_t%FOUND;
        close c_sgst_t;
        --
        if  pl_found
        and r_sgst_t_rec.waarde is not null
        then
          if pl_swaarde is null
          then
            pl_swaarde := r_sgst_t_rec.waarde;
          else
            pl_swaarde := pl_swaarde||'/'||r_sgst_t_rec.waarde;
          end if;
        else
          null;
        end if;
      end loop;
    else
      pl_swaarde := null;
    end if;
    --
    if r_stam_rec.spw in ('SW1', 'ST1')
    then
      if r_stam_rec.samenstelling in('w1/w2','w1/w2/w3')
      then
        if  r_stam_rec.waarde_kol_in_view = 'Samengesteld met Naam vd afw., Naam vd 2e afw. en Naam vd 3e afw.  ; resultaat in N records'
        and pl_swaarde is null
        then
          pl_return := 'N'; -- Normaal
        else
          pl_return := pl_swaarde;
        end if;
        --
      elsif r_stam_rec.samenstelling = 'Afw (AN;NA;AA;TA;AT); N (NN); Twf (NT;TN)'
      then
        pl_return :=
          case
            when pl_swaarde in ('A/N', 'N/A','A/A','T/A','A/T')
            then 'Afw'
            when pl_swaarde = 'N/N'
            then 'N'
            when pl_swaarde in ('N/T','T/N')
            then 'Twf'
            else null
          end;
      elsif r_stam_rec.samenstelling = 'N (N,N); J'
      then
        pl_return :=
          case
            when pl_swaarde in ('N','N/N')
            then 'N'
            when pl_swaarde is null
            then null
            else 'J'
           end;
      elsif r_stam_rec.samenstelling = 'N (N,N,N;); J'
      then
        pl_return :=
          case
            when pl_swaarde in ('N','N/N','N/N/N')
            then 'N'
            when pl_swaarde is null
            then null
            else 'J'
           end;
        if pl_swaarde in ('N','N/N','N/N/N')
        then
          pl_return := 'N';
        elsif pl_swaarde is null
        then
          pl_return := null;
        else
          pl_return := 'J';
        end if;
      else
        pl_return := null;
      end if;
    else
      null;
    end if;
  end loop;
  --
  return pl_return;
end waardebepaling;

function foetus_mons
(
  p_foet_id in kgc_foetussen.foet_id%type
)
return varchar2
is
  cursor c_foet(b_foet_id in kgc_foetussen.foet_id%type)
  is
    select to_char(zwan.nummer)||'-'||to_char(foet.volgnr) foetus_mons
    from   kgc_zwangerschappen  zwan
    ,      kgc_foetussen        foet
    where  foet.foet_id = b_foet_id
    and    zwan.zwan_id = foet.zwan_id
  ;
  --
  r_foet_rec  c_foet%rowtype;
  pl_return   varchar2(100);
  --
begin
  open  c_foet(p_foet_id);
  fetch c_foet
  into  r_foet_rec;
  close c_foet;
  --
  pl_return := r_foet_rec.foetus_mons;
  --
  return pl_return;
end foetus_mons;

function pers_id_moeder
(
  p_mons_pers_id in kgc_monsters.pers_id%type
)
return number
is
  cursor c_foet(b_mons_pers_id in kgc_monsters.pers_id%type)
  is
    select foet.pers_id_moeder
    from   kgc_foetussen foet
    where  foet.pers_id_geboren_als = b_mons_pers_id
  ;
  --
  r_foet_rec  c_foet%rowtype;
  pl_return   kgc_foetussen.foet_id%type;
  --
begin
  open  c_foet(p_mons_pers_id);
  fetch c_foet
  into  r_foet_rec;
  close c_foet;
  --
  pl_return := r_foet_rec.pers_id_moeder;
  --
  return pl_return;
end pers_id_moeder;

function foetus_pers
(
  p_mons_pers_id in kgc_monsters.pers_id%type
)
return varchar2
is
  cursor c_foet(b_mons_pers_id in kgc_monsters.pers_id%type)
  is
    select to_char(zwan.nummer)||'-'||to_char(foet.volgnr) foetus_mons
    from   kgc_zwangerschappen  zwan
    ,      kgc_foetussen        foet
    where  foet.pers_id_geboren_als = b_mons_pers_id
    and    zwan.zwan_id = foet.zwan_id
  ;
  --
  r_foet_rec  c_foet%rowtype;
  pl_return   varchar2(100);
  --
begin
  open  c_foet(p_mons_pers_id);
  fetch c_foet
  into  r_foet_rec;
  close c_foet;
  --
  pl_return := r_foet_rec.foetus_mons;
  --
  return pl_return;
end foetus_pers;

end beh_stamboom_00;
/

/
QUIT
