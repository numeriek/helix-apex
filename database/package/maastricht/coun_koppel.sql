CREATE OR REPLACE PACKAGE "HELIX"."COUN_KOPPEL" IS
FUNCTION bekende_rela
( p_code in varchar2 := null
, p_relatie_type in varchar2 := null
, p_naam in varchar2 := null
, p_adres in varchar2 := null
, p_postcode in varchar2 := null
, p_woonplaats in varchar2 := null
, p_inst_id in number := null
, p_spec_id in number := null
)
RETURN number;
PROCEDURE bekende_relatie
( p_code in varchar2 := null
, p_relatie_type in varchar2 := null
, p_naam in varchar2 := null
, p_adres in varchar2 := null
, p_postcode in varchar2 := null
, p_woonplaats in varchar2 := null
, p_inst_id in number := null
, p_spec_id in number := null
, x_rela_id    IN OUT  NUMBER
, x_aanspreken IN OUT  VARCHAR2
);
END coun_koppel;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."COUN_KOPPEL" IS
function fmt_adres
( p_adres in varchar2
)
return varchar2
is
  v_adres varchar2(500) := p_adres;
begin
  v_adres := ltrim( rtrim( upper( v_adres ) ) );
  v_adres := replace( v_adres, 'ALHIER', '' );
  v_adres := replace( v_adres, '.', ' ' );
  v_adres := replace( v_adres, 'STR ', 'STRAAT ' );
  v_adres := replace( v_adres, 'ST ', 'STRAAT ' );
  v_adres := replace( v_adres, 'WG ', 'WEG ' );
  v_adres := replace( v_adres, 'LN ', 'LAAN ' );
  v_adres := replace( v_adres, 'PLN ', 'PLEIN ' );
  v_adres := replace( v_adres, 'HF ', 'HOF ' );
  v_adres := replace( v_adres, 'DYK ', 'DIJK ' );
  return ( v_adres );
end fmt_adres;

function fmt_achternaam
( p_naam in varchar2
)
return varchar2
is
  v_achternaam varchar2(100) := p_naam;
  v_tmp_naam varchar2(100);
begin
  v_achternaam := upper( ltrim( rtrim( v_achternaam ) ) );
  v_achternaam := replace( v_achternaam, ' - ', '-' );
  v_achternaam := replace( v_achternaam, '- ', '-' );
  v_achternaam := replace( v_achternaam, ' -', '-' );
  v_tmp_naam := v_achternaam;
  v_achternaam := replace( v_achternaam, 'DHR' );
  v_achternaam := replace( v_achternaam, 'MEVR' );
  v_achternaam := ltrim( v_achternaam, '.' );
  v_achternaam := ltrim( v_achternaam, '.' );
  while instr( v_achternaam, '.' ) > 0
  loop
    v_achternaam := replace( v_achternaam
                           , substr( v_achternaam
                                   , greatest(instr( v_achternaam, '.' ) - 1, 1 )
                                   , 2
                                   )
                           , ''
                           );
  end loop;
  if ( instr( v_achternaam,' ', -1 ) > 0 )
  then
    v_achternaam := substr( v_achternaam, instr( v_achternaam,' ', -1 ) );
  end if;
  v_achternaam := upper( ltrim( rtrim( v_achternaam ) ) );
  if ( instr( v_tmp_naam, v_achternaam ) = 0 )
  then
    v_achternaam := v_tmp_naam;
  end if;
  kgc_formaat_00.formatteer
  ( p_naamdeel => 'ACHTERNAAM'
  , x_naam => v_achternaam
  );
  return( v_achternaam );
end fmt_achternaam;

FUNCTION bekende_rela
( p_code in varchar2 := null
, p_relatie_type in varchar2 := null
, p_naam in varchar2 := null
, p_adres in varchar2 := null
, p_postcode in varchar2 := null
, p_woonplaats in varchar2 := null
, p_inst_id in number := null
, p_spec_id in number := null
)
RETURN NUMBER
IS
  v_rela_id     kgc_relaties.rela_id%TYPE;
  v_aanspreken  kgc_relaties.aanspreken%TYPE;
BEGIN
  coun_koppel.bekende_relatie( p_code       => p_code
                        , p_relatie_type => p_relatie_type
                        , p_naam       => p_naam
                        , p_adres      => p_adres
                        , p_postcode   => p_postcode
                        , p_woonplaats => p_woonplaats
                        , p_inst_id    => p_inst_id
                        , p_spec_id    => p_spec_id
                        , x_rela_id    => v_rela_id
                        , x_aanspreken => v_aanspreken
                        );
  RETURN v_rela_id;
END bekende_rela;

PROCEDURE bekende_relatie
( p_code in varchar2 := null
, p_relatie_type in varchar2 := null
, p_naam in varchar2 := null
, p_adres in varchar2 := null
, p_postcode in varchar2 := null
, p_woonplaats in varchar2 := null
, p_inst_id in number := null
, p_spec_id in number := null
, x_rela_id    IN OUT  NUMBER
, x_aanspreken IN OUT  VARCHAR2
)
IS
  v_rela_id    number := null;
  v_aanspreken kgc_relaties.aanspreken%TYPE;
  v_code varchar2(10);
  v_achternaam varchar2(100);
  v_adres varchar2(250);

  -- zelfde code
  cursor c1
  is
    select rela_id
    ,      aanspreken
    from   kgc_relaties
    where  code = v_code
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    ;
  -- eerste achternaam, daarna aanspreken
  -- zelfde naam, adres, woonplaats, specialisme
 cursor c02
  is
    select rela_id
    ,      adres
    ,      aanspreken
    from   kgc_relaties
    where  achternaam = v_achternaam
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
       and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    and  ( spec_id = nvl( p_spec_id, spec_id ) or spec_id is null )
    and  ( inst_id = nvl( p_inst_id, inst_id ) or inst_id is null )
    ;
  -- zelfde naam, woonplaats, specialisme
 cursor c03
  is
    select rela_id
    ,      aanspreken
    from   kgc_relaties
    where  achternaam = v_achternaam
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
       and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    and  ( spec_id = nvl( p_spec_id, spec_id ) or spec_id is null )
    and  ( inst_id = nvl( p_inst_id, inst_id ) or inst_id is null )
    ;
  -- zelfde naam, adres, woonplaats
 cursor c04
  is
    select rela_id
    ,      adres
    ,      aanspreken
    from   kgc_relaties
    where  achternaam = v_achternaam
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
        or upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    ;
  -- zelfde naam, woonplaats
 cursor c05
  is
    select rela_id
    ,      aanspreken
    from   kgc_relaties
    where  achternaam = v_achternaam
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
       and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    ;
  -- zelfde naam, geen andere gegevens
 cursor c06
  is
    select rela_id
    ,      aanspreken
    from   kgc_relaties
    where  achternaam = v_achternaam
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and    adres is null
    and    woonplaats is null
    and    postcode is null
    ;

  -- idem c2/6 , met aanspreken (erg traag!!!!)
  -- zelfde naam, adres, woonplaats, specialisme
  cursor c2
  is
    select rela_id
    ,      adres
    ,      aanspreken
    from   kgc_relaties
    where  aanspreken like v_aanspreken
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
       and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    and  ( spec_id = nvl( p_spec_id, spec_id ) or spec_id is null )
    and  ( inst_id = nvl( p_inst_id, inst_id ) or inst_id is null )
    ;
  -- zelfde naam, woonplaats, specialisme
  cursor c3
  is
    select rela_id
    ,      aanspreken
    from   kgc_relaties
    where  aanspreken like v_aanspreken
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
       and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    and  ( spec_id = nvl( p_spec_id, spec_id ) or spec_id is null )
    and  ( inst_id = nvl( p_inst_id, inst_id ) or inst_id is null )
    ;
  -- zelfde naam, adres, woonplaats
  cursor c4
  is
    select rela_id
    ,      adres
    ,      aanspreken
    from   kgc_relaties
    where  aanspreken like v_aanspreken
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
        or upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    ;
  -- zelfde naam, woonplaats
  cursor c5
  is
    select rela_id
    ,      aanspreken
    from   kgc_relaties
    where  aanspreken like v_aanspreken
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
       and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
         )
    ;
  -- zelfde naam, geen andere gegevens
  cursor c6
  is
    select rela_id
    ,      aanspreken
    from   kgc_relaties
    where  aanspreken like v_aanspreken
    and  ( relatie_type = p_relatie_type
        or p_relatie_type is null
         )
    and    adres is null
    and    woonplaats is null
    and    postcode is null
    ;

  v_r_adres varchar2(250);

begin
  v_code := upper( p_code );
  v_achternaam := fmt_achternaam( p_naam );
  v_aanspreken := '%'||fmt_achternaam( p_naam )||'%';
  v_adres := fmt_adres( p_adres );
  if ( p_code = 'ONBEKEND' )
  then
    null;
  elsif ( upper(p_naam) like '%ONBEKEND%'
       or p_naam = '?'
        )
  then
    v_rela_id := bekende_rela
                 ( p_code => 'ONBEKEND'
                 );
  end if;
  --
  if ( v_rela_id is null )
  then
    if ( p_code is not null )
    then
      open  c1;
      fetch c1
      into  v_rela_id
      ,     v_aanspreken
      ;
      close c1;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and p_adres is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c02;
      fetch c02
      into v_rela_id
      ,    v_r_adres
      ,    v_aanspreken
      ;
      while c02%found
      loop
        if ( fmt_adres( v_r_adres ) = v_adres )
       then
          exit;
       else
          v_rela_id := null;
        end if;
        fetch c02 into v_rela_id, v_r_adres, v_aanspreken;
      end loop;
      close c02;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c03;
      fetch c03 into v_rela_id, v_aanspreken;
      close c03;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and p_adres is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c04;
      fetch c04 into v_rela_id, v_r_adres, v_aanspreken;
      while c04%found
      loop
        if ( fmt_adres( v_r_adres ) = v_adres )
       then
          exit;
       else
          v_rela_id := null;
        end if;
        fetch c04 into v_rela_id, v_r_adres, v_aanspreken;
      end loop;
      close c04;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c05;
      fetch c05 into v_rela_id, v_aanspreken;
      close c05;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and p_adres is null
     and p_postcode is null
     and p_woonplaats is null
       )
    then
      open  c06;
      fetch c06 into v_rela_id, v_aanspreken;
      close c06;
    end if;
  end if;
  -- nog niks??? Nu wordt het traag...
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and p_adres is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c2;
      fetch c2
      into v_rela_id
      ,    v_r_adres
      ,    v_aanspreken
	  ;
      while c2%found
      loop
        if ( fmt_adres( v_r_adres ) = v_adres )
       then
          exit;
       else
          v_rela_id := null;
        end if;
        fetch c2 into v_rela_id, v_r_adres, v_aanspreken;
      end loop;
      close c2;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c3;
      fetch c3 into v_rela_id, v_aanspreken;
      close c3;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and p_adres is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c4;
      fetch c4 into v_rela_id, v_r_adres, v_aanspreken;
      while c4%found
      loop
        if ( fmt_adres( v_r_adres ) = v_adres )
       then
          exit;
       else
          v_rela_id := null;
        end if;
        fetch c4 into v_rela_id, v_r_adres, v_aanspreken;
      end loop;
      close c4;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and ( p_postcode is not null or p_woonplaats is not null )
       )
    then
      open  c5;
      fetch c5 into v_rela_id, v_aanspreken;
      close c5;
    end if;
  end if;
  if ( v_rela_id is null )
  then
    if ( p_naam is not null
     and p_adres is null
     and p_postcode is null
     and p_woonplaats is null
       )
    then
      open  c6;
      fetch c6 into v_rela_id, v_aanspreken;
      close c6;
    end if;
  end if;

  x_rela_id    := v_rela_id;
  x_aanspreken := v_aanspreken;

end bekende_relatie;

END coun_koppel;
/

/
QUIT
