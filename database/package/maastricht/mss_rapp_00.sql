CREATE OR REPLACE PACKAGE "HELIX"."MSS_RAPP_00" IS
  -- bepaal rapp_id op basis van binnengekomen parameters
  FUNCTION rapport_id
  ( p_ramo_id IN NUMBER := NULL
  , p_kafd_id IN NUMBER := NULL
  , p_ongr_id IN NUMBER := NULL
  , p_taal_id IN NUMBER := NULL
  , p_ramo_code IN VARCHAR2 := NULL
  , p_kafd_code IN VARCHAR2 := NULL
  , p_ongr_code IN VARCHAR2 := NULL
  , p_taal_code IN VARCHAR2 := NULL
  )
  RETURN NUMBER;
  -- Haal de vaste tekst voor een bepaald rapport op
  -- Indien geen taal bekend is, wordt de tekst zonder taalaanduiding genomen
  -- <systeemdatum> wordt vervangen door de huidige datum (dd-mm-yyyy)
  -- <waarde> wordt vervangen door de doorgegeven waarde (bv. een kolomwaarde uit een query)
  --
  FUNCTION formatteer
  ( p_kafd_id IN NUMBER
  , p_ongr_id IN NUMBER := NULL
  , p_ramo_id IN NUMBER := NULL
  , p_module IN VARCHAR2 := NULL
  , p_code IN VARCHAR2
  , p_taal_id IN NUMBER := NULL
  , p_waarde1 IN VARCHAR2 := NULL
  , p_waarde2 IN VARCHAR2 := NULL
  , p_waarde3 IN VARCHAR2 := NULL
  , p_waarde4 IN VARCHAR2 := NULL
  , p_waarde5 IN VARCHAR2 := NULL
  , p_waarde6 IN VARCHAR2 := NULL
  , p_waarde7 IN VARCHAR2 := NULL
  , p_waarde8 IN VARCHAR2 := NULL
  )
  RETURN VARCHAR2;
  --
  FUNCTION afnamedatum_monster
    (p_zwan_id in varchar2
    ) RETURN date;
  --
  FUNCTION resultaat_tekst_dna
  ( p_onde_id IN NUMBER
  )
  RETURN VARCHAR2;
  --
  FUNCTION bepaal_best_id
  ( p_prit_id IN NUMBER
  , p_best_id IN NUMBER
  , p_rapport IN VARCHAR2
  , p_uits_id IN NUMBER
  , p_koho_id IN NUMBER
  ) RETURN NUMBER;
  --
END MSS_RAPP_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_RAPP_00" IS

  -- bepaal rapp_id op basis van binnengekomen parameters
  FUNCTION rapport_id
  ( p_ramo_id IN NUMBER := NULL
  , p_kafd_id IN NUMBER := NULL
  , p_ongr_id IN NUMBER := NULL
  , p_taal_id IN NUMBER := NULL
  , p_ramo_code IN VARCHAR2 := NULL
  , p_kafd_code IN VARCHAR2 := NULL
  , p_ongr_code IN VARCHAR2 := NULL
  , p_taal_code IN VARCHAR2 := NULL
  )
  RETURN NUMBER
  IS
    v_rapp_id NUMBER;
    v_ramo_id NUMBER;
    v_kafd_id NUMBER;
    v_ongr_id NUMBER;
    v_taal_id NUMBER;
    CURSOR ramo_cur
    IS
      SELECT ramo_id
      FROM   kgc_rapport_modules
      WHERE  code = p_ramo_code
      ;
    CURSOR kafd_cur
    IS
      SELECT kafd_id
      FROM   kgc_kgc_afdelingen
      WHERE  code = p_kafd_code
      ;
    CURSOR ongr_cur
    IS
      SELECT ongr_id
      FROM   kgc_onderzoeksgroepen
      WHERE  kafd_id = v_kafd_id
      AND    code = p_ongr_code
      ;
    CURSOR taal_cur
    IS
      SELECT taal_id
      FROM   kgc_talen
      WHERE  code = p_taal_code
      ;
    CURSOR rapp_cur
    IS
      SELECT rapp.rapp_id
      FROM   kgc_rapport_modules ramo
      ,      kgc_rapporten rapp
      WHERE  rapp.kafd_id = v_kafd_id
      AND    rapp.ramo_id = ramo.ramo_id
      AND    ramo.ramo_id = v_ramo_id
      AND  ( rapp.ongr_id = v_ongr_id
          OR rapp.ongr_id IS NULL
           )
      AND  ( rapp.taal_id = v_taal_id
          OR rapp.taal_id IS NULL
           )
      ORDER BY rapp.ongr_id ASC -- zonder ongr als laatst
      ,        rapp.taal_id ASC -- zonder taal als laatst
      ;
  BEGIN
    IF ( p_ramo_id IS NOT NULL )
    THEN
      v_ramo_id := p_ramo_id;
    ELSE
      OPEN  ramo_cur;
      FETCH ramo_cur
      INTO  v_ramo_id;
      CLOSE ramo_cur;
    END IF;
    IF ( p_kafd_id IS NOT NULL )
    THEN
      v_kafd_id := p_kafd_id;
    ELSE
      OPEN  kafd_cur;
      FETCH kafd_cur
      INTO  v_kafd_id;
      CLOSE kafd_cur;
    END IF;
    IF ( p_ongr_id IS NOT NULL )
    THEN
      v_ongr_id := p_ongr_id;
    ELSE
      OPEN  ongr_cur;
      FETCH ongr_cur
      INTO  v_ongr_id;
      CLOSE ongr_cur;
    END IF;
    IF ( p_taal_id IS NOT NULL )
    THEN
      v_taal_id := p_taal_id;
    ELSE
      OPEN  taal_cur;
      FETCH taal_cur
      INTO  v_taal_id;
      CLOSE taal_cur;
    END IF;

    OPEN  rapp_cur;
    FETCH rapp_cur
    INTO  v_rapp_id;
    CLOSE rapp_cur;
    RETURN( v_rapp_id );
  END rapport_id;

  -- publiek
  FUNCTION formatteer
  ( p_kafd_id IN NUMBER
  , p_ongr_id IN NUMBER := NULL
  , p_ramo_id IN NUMBER := NULL
  , p_module IN VARCHAR2 := NULL
  , p_code IN VARCHAR2
  , p_taal_id IN NUMBER := NULL
  , p_waarde1 IN VARCHAR2 := NULL
  , p_waarde2 IN VARCHAR2 := NULL
  , p_waarde3 IN VARCHAR2 := NULL
  , p_waarde4 IN VARCHAR2 := NULL
  , p_waarde5 IN VARCHAR2 := NULL
  , p_waarde6 IN VARCHAR2 := NULL
  , p_waarde7 IN VARCHAR2 := NULL
  , p_waarde8 IN VARCHAR2 := NULL
  )
  RETURN VARCHAR2
  IS
    v_tekst VARCHAR2(4000);
    v_rapp_id NUMBER;
    CURSOR rate_cur
    IS
      SELECT rate.tekst
      ,      rste.label
      FROM   kgc_rapport_stand_teksten rste
      ,      kgc_rapporten rapp
      ,      kgc_rapport_teksten rate
      WHERE  rate.rapp_id = rapp.rapp_id
      AND    rste.ramo_id = rapp.ramo_id
      AND    rste.code = rate.code
      AND    rapp.rapp_id = v_rapp_id
      AND    rate.code = UPPER( p_code )
      ;
    rate_rec rate_cur%rowtype;
  BEGIN
    v_rapp_id := rapport_id
                 ( p_ramo_code => UPPER( p_module )
                 , p_ramo_id => p_ramo_id
                 , p_kafd_id => p_kafd_id
                 , p_ongr_id => p_ongr_id
                 , p_taal_id => p_taal_id
                 );
    OPEN  rate_cur;
    FETCH rate_cur
    INTO  rate_rec;
    IF ( rate_cur%notfound )
    THEN
      CLOSE rate_cur;
      -- MACHT_BU en MACHT_BEG worden door elkaar gebruikt
      -- MACHT_BEG(eleidende brief) is eigenlijk verouderd
      IF ( UPPER(p_module) = 'MACHT_BU' )
      THEN
        v_rapp_id := rapport_id
                     ( p_ramo_code => 'MACHT_BEG'
                     , p_ramo_id => p_ramo_id
                     , p_kafd_id => p_kafd_id
                     , p_ongr_id => p_ongr_id
                     , p_taal_id => p_taal_id
                     );
      ELSIF ( UPPER(p_module) = 'MACHT_BEG' )
      THEN
        v_rapp_id := rapport_id
                     ( p_ramo_code => 'MACHT_BU'
                     , p_ramo_id => p_ramo_id
                     , p_kafd_id => p_kafd_id
                     , p_ongr_id => p_ongr_id
                     , p_taal_id => p_taal_id
                     );
      END IF;
      OPEN  rate_cur;
      FETCH rate_cur
      INTO  rate_rec;
      CLOSE rate_cur;
    ELSE
      CLOSE rate_cur;
    END IF;
    v_tekst := rate_rec.tekst;
    --
    -- substitutiecodes case insensitive
    v_tekst := REPLACE( v_tekst, '<systeemdatum>', '<SYSTEEMDATUM>' );
    v_tekst := REPLACE( v_tekst, '<waarde1>', '<WAARDE1>' );
    v_tekst := REPLACE( v_tekst, '<waarde2>', '<WAARDE2>' );
    v_tekst := REPLACE( v_tekst, '<waarde3>', '<WAARDE3>' );
    v_tekst := REPLACE( v_tekst, '<waarde4>', '<WAARDE4>' );
    v_tekst := REPLACE( v_tekst, '<waarde5>', '<WAARDE5>' );
    v_tekst := REPLACE( v_tekst, '<waarde6>', '<WAARDE6>' );
    v_tekst := REPLACE( v_tekst, '<waarde7>', '<WAARDE7>' );
    v_tekst := REPLACE( v_tekst, '<waarde8>', '<WAARDE8>' );
    --
    -- label is 1-regelig
    IF ( rate_rec.label = 'J' )
    THEN
      v_tekst := RTRIM( LTRIM( REPLACE( v_tekst, CHR(10), ' ' ) ) );
    END IF;
    IF ( INSTR( v_tekst, '<SYSTEEMDATUM>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<SYSTEEMDATUM>', TO_CHAR( SYSDATE, 'dd-mon-yyyy' ) );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE1>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE1>', p_waarde1 );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE2>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE2>', p_waarde2 );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE3>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE3>', p_waarde3 );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE4>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE4>', p_waarde4 );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE5>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE5>', p_waarde5 );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE6>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE6>', p_waarde6 );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE7>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE7>', p_waarde7 );
    END IF;
    IF ( INSTR( v_tekst, '<WAARDE8>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<WAARDE8>', p_waarde8 );
    END IF;
    --
    RETURN( v_tekst );
  END formatteer;

  function afnamedatum_monster
  (
    p_zwan_id in varchar2
  )
  return date
  is
    cursor c_afnamedatum (b_zwan_id kgc_prenataal_zwan_onde_vw.zwan_id%type)
    is
      SELECT mons.datum_afname datum_afname
      FROM   kgc_materialen             mate
      ,      kgc_monsters               mons
      ,      kgc_onderzoek_monsters     onmo
      ,      kgc_prenataal_zwan_onde_vw zwon
      WHERE  zwon.onde_id = onmo.onde_id
      AND    zwon.foet_id = mons.foet_id
      AND    onmo.mons_id = mons.mons_id
      AND    mons.mate_id = mate.mate_id
      AND    zwon.zwan_id = b_zwan_id
      ORDER BY mons.mons_id DESC
    ;
    --
    r_afnamedatum_rec  c_afnamedatum%rowtype;
    pl_return          date;
  begin
    open  c_afnamedatum(p_zwan_id);
    fetch c_afnamedatum
    into  r_afnamedatum_rec;
    --
    if c_afnamedatum%NOTFOUND
    then
      pl_return := null;
    else
      pl_return := r_afnamedatum_rec.datum_afname;
    end if;
    --
    close c_afnamedatum;
    --
    return pl_return;
    --
  end afnamedatum_monster;

  FUNCTION resultaat_tekst_dna
  ( p_onde_id IN NUMBER
  )
  RETURN VARCHAR2
  IS
  BEGIN
    RETURN ( mss_onde_00.resultaat_tekst_dna
             ( p_onde_id => p_onde_id
             )
           );
  END resultaat_tekst_dna;

  FUNCTION bepaal_best_id
  ( p_prit_id IN NUMBER
  , p_best_id IN NUMBER
  , p_rapport IN VARCHAR2
  , p_uits_id IN NUMBER
  , p_koho_id IN NUMBER
  ) RETURN NUMBER
  IS
   CURSOR c_prit_uits
   IS
     SELECT best_id
       FROM beh_printset_temp prit
      WHERE prit.prit_id = p_prit_id
        AND prit.rapport = p_rapport
        AND prit.uits_id = p_uits_id
        AND nvl(prit.koho_id,-99) = nvl(p_koho_id,-99);
  BEGIN
    IF p_prit_id IS NOT NULL
    THEN
      IF p_uits_id IS NOT NULL
      THEN
        FOR r_prit IN c_prit_uits
        LOOP
          RETURN r_prit.best_id;
        END LOOP;
      END IF;
      RETURN NULL;
    ELSE
      RETURN p_best_id;
    END IF;
  END bepaal_best_id;

END   mss_rapp_00;
/

/
QUIT
