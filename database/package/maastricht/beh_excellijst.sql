CREATE OR REPLACE PACKAGE "HELIX"."BEH_EXCELLIJST"
is

  procedure vul_beh_excl_temp
  ( p_indi_code1   in kgc_indicatie_teksten.code%type
  , p_indi_code2   in kgc_indicatie_teksten.code%type
  , p_indi_code3   in kgc_indicatie_teksten.code%type
  , p_indi_code4   in kgc_indicatie_teksten.code%type
  , p_indi_code5   in kgc_indicatie_teksten.code%type
  , p_indi_code6   in kgc_indicatie_teksten.code%type
  , p_indi_code7   in kgc_indicatie_teksten.code%type
  , p_indi_code8   in kgc_indicatie_teksten.code%type
  , p_basis_stof   in varchar2
  , p_onderz_wijze in kgc_onderzoeken.onderzoekswijze%type
  , p_datum_va     in varchar2
  , p_datum_tm     in varchar2
  , p_afgerond     in kgc_onderzoeken.afgerond%type
  , p_stat_testen  in varchar2
  , p_incl_mlpa    in varchar2
  );

  procedure vul_beh_excl_elem
  ( p_indi_code1   in kgc_indicatie_teksten.code%type
  , p_indi_code2   in kgc_indicatie_teksten.code%type
  , p_indi_code3   in kgc_indicatie_teksten.code%type
  , p_indi_code4   in kgc_indicatie_teksten.code%type
  , p_indi_code5   in kgc_indicatie_teksten.code%type
  , p_indi_code6   in kgc_indicatie_teksten.code%type
  , p_indi_code7   in kgc_indicatie_teksten.code%type
  , p_indi_code8   in kgc_indicatie_teksten.code%type
  , p_basis_stof   in varchar2
  , p_onderz_wijze in kgc_onderzoeken.onderzoekswijze%type
  , p_datum_va     in varchar2
  , p_datum_tm     in varchar2
  , p_afgerond     in kgc_onderzoeken.afgerond%type
  , p_stat_testen  in varchar2
  , p_incl_mlpa    in varchar2
  );

  procedure insert_excl_elem
  ( p_elem_text in varchar2
  );

end beh_excellijst;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_EXCELLIJST"
is

  pl_volgnr number(10) := 0;

  procedure vul_beh_excl_temp
  ( p_indi_code1   in kgc_indicatie_teksten.code%type
  , p_indi_code2   in kgc_indicatie_teksten.code%type
  , p_indi_code3   in kgc_indicatie_teksten.code%type
  , p_indi_code4   in kgc_indicatie_teksten.code%type
  , p_indi_code5   in kgc_indicatie_teksten.code%type
  , p_indi_code6   in kgc_indicatie_teksten.code%type
  , p_indi_code7   in kgc_indicatie_teksten.code%type
  , p_indi_code8   in kgc_indicatie_teksten.code%type
  , p_basis_stof   in varchar2
  , p_onderz_wijze in kgc_onderzoeken.onderzoekswijze%type
  , p_datum_va     in varchar2
  , p_datum_tm     in varchar2
  , p_afgerond     in kgc_onderzoeken.afgerond%type
  , p_stat_testen  in varchar2
  , p_incl_mlpa    in varchar2
  )
  is
    --
    -- Vul global temp tabel beh_excl_temp
    --
    cursor c_table ( b_indi_code1   in kgc_indicatie_teksten.code%type
                   , b_indi_code2   in kgc_indicatie_teksten.code%type
                   , b_indi_code3   in kgc_indicatie_teksten.code%type
                   , b_indi_code4   in kgc_indicatie_teksten.code%type
                   , b_indi_code5   in kgc_indicatie_teksten.code%type
                   , b_indi_code6   in kgc_indicatie_teksten.code%type
                   , b_indi_code7   in kgc_indicatie_teksten.code%type
                   , b_indi_code8   in kgc_indicatie_teksten.code%type
                   , b_basis_stof   in varchar2
                   , b_onderz_wijze in kgc_onderzoeken.onderzoekswijze%type
                   , b_datum_va     in varchar2
                   , b_datum_tm     in varchar2
                   , b_afgerond     in kgc_onderzoeken.afgerond%type
                   , b_stat_testen  in varchar2
                   , b_incl_mlpa    in varchar2
                   )
    is
      -- Gedeelte indien basis van de weer te geven stoftesten = Onderzoek
      select onde.onde_id                                                                                               onde_id
      ,      onde.onderzoeknr                                                                                           onderzoeknr
      ,      indi.code                                                                                                  indi_code
      ,      onde.pers_id                                                                                               pers_id
      ,      meet.meet_id                                                                                               meet_id
      ,      meet.afgerond                                                                                              meet_afgerond
      ,      case
               when stof.omschrijving like 'SEQ_%'
               then 'EX_'||translate(substr(stof.omschrijving,instr(stof.omschrijving, '_',1,2)+1,100),'>.+-','____')
               when stof.omschrijving like 'MLPA_%'
               then 'EX_'||translate(substr(stof.omschrijving,instr(stof.omschrijving, '_',1,1)+1,100),'>.+-','____')
               else null
             end                                                                                                        stof_omschrijving
      ,      case
               when stof.omschrijving like 'SEQ_%'
               then 'SEQ'
               when stof.omschrijving like 'MLPA_%'
               then 'MLPA'
               else null
             end                                                                                                        ind_seq_mlpa
      from   kgc_stoftesten            stof
      ,      bas_meetwaarden           meet
      ,      bas_metingen              meti
      ,      kgc_indicatie_teksten     indi
      ,      kgc_onderzoek_indicaties  onin
      ,      kgc_onderzoeken           onde
      where  upper(b_basis_stof) = 'O'
      --and    onde.kafd_id = kgc_kafd_00.id('DNA')
      and    (   (upper(onde.onderzoekswijze) LIKE upper(b_onderz_wijze))
              or (upper(b_onderz_wijze) = 'A')  -- Als parameter = A alles meenemen
             )
      and    onde.datum_binnen
                  between to_date(b_datum_va,'DDMMYYYY')
                  and     to_date(b_datum_tm,'DDMMYYYY')
      and    (   (onde.afgerond = upper(b_afgerond))
              or (upper(b_afgerond) = 'A')  -- Als parameter = A alles meenemen
             )
      and    (   (     upper(b_stat_testen) = 'O' -- Open
                   and meet.afgerond = 'N'
                   and meti.afgerond = 'N'
                   and onde.afgerond = 'N'
                 )
              or (     upper(b_stat_testen) = 'A' -- Afgerond
                   and meet.afgerond = 'J'
                   and meet.mede_id_controle is not null
                 )
              or ( upper(b_stat_testen) = 'T' -- Totaal = Open + Afgerond zonder conditie op mede_id_controle
                   and (    (     meet.afgerond = 'N'
                              and meti.afgerond = 'N'
                              and onde.afgerond = 'N'
                            )
                         or (     meet.afgerond = 'J'
                            )
                       )
                 )
             )
      and    onin.onde_id = onde.onde_id
      and    onin.hoofd_ind = 'J'
      and    indi.indi_id = onin.indi_id
      and    indi.code in (b_indi_code1, b_indi_code2, b_indi_code3, b_indi_code4, b_indi_code5, b_indi_code6, b_indi_code7, b_indi_code8)
      and    meti.onde_id = onde.onde_id
      and    meet.meti_id = meti.meti_id
      and    stof.stof_id = meet.stof_id
      and    (   (upper(stof.omschrijving) like 'SEQ_%')
              or (    upper(b_incl_mlpa) = 'J'
                  and upper(stof.omschrijving) like 'MLPA_%'
                 )
             )
      union
      -- Gedeelte indien basis van de weer te geven stoftesten = Protocol
      select onde.onde_id                                                                                               onde_id
      ,      onde.onderzoeknr                                                                                           onderzoeknr
      ,      indi.code                                                                                                  indi_code
      ,      onde.pers_id                                                                                               pers_id
      ,      meet.meet_id                                                                                               meet_id
      ,      meet.afgerond                                                                                              meet_afgerond
      ,      case
               when stof.omschrijving like 'SEQ_%'
               then 'EX_'||translate(substr(stof.omschrijving,instr(stof.omschrijving, '_',1,2)+1,100),'>.+-','____')
               when stof.omschrijving like 'MLPA_%'
               then 'EX_'||translate(substr(stof.omschrijving,instr(stof.omschrijving, '_',1,1)+1,100),'>.+-','____')
               else null
             end                                                                                                        stof_omschrijving
      ,      case
               when stof.omschrijving like 'SEQ_%'
               then 'SEQ'
               when stof.omschrijving like 'MLPA_%'
               then 'MLPA'
               else null
             end                                                                                                        ind_seq_mlpa
      from   bas_meetwaarden            meet
      ,      bas_metingen               meti
      ,      kgc_onderzoeken            onde
      ,      kgc_stoftesten             stof
      ,      kgc_protocol_stoftesten    prst
      ,      kgc_stoftestgroepen        stgr
      ,      kgc_stoftestgroep_gebruik  stgg
      ,      kgc_indicatie_teksten      indi
      where  upper(b_basis_stof) = 'P'
      and    indi.code in (b_indi_code1, b_indi_code2, b_indi_code3, b_indi_code4, b_indi_code5, b_indi_code6, b_indi_code7, b_indi_code8)
      and    stgg.indi_id = indi.indi_id
      and    stgg.standaard = 'J'
      and    stgr.stgr_id = stgg.stgr_id
      and    prst.stgr_id = stgr.stgr_id
      and    prst.standaard = 'J'
      and    stof.stof_id = prst.stof_id
      --and    onde.kafd_id = kgc_kafd_00.id('DNA')
      and    (   (upper(onde.onderzoekswijze) LIKE upper(b_onderz_wijze))
              or (upper(b_onderz_wijze) = 'A')  -- Als parameter = A alles meenemen
             )
      and    onde.datum_binnen
                  between to_date(b_datum_va,'DDMMYYYY')
                  and     to_date(b_datum_tm,'DDMMYYYY')
      and    (   (onde.afgerond = upper(b_afgerond))
              or (upper(b_afgerond) = 'A')  -- Als parameter = A alles meenemen
             )
      and    meti.onde_id = onde.onde_id
      and    meet.meti_id = meti.meti_id
      and    stof.stof_id = meet.stof_id
      and    (   (upper(stof.omschrijving) like 'SEQ_%')
              or (    upper(b_incl_mlpa) = 'J'
                  and upper(stof.omschrijving) like 'MLPA_%'
                 )
             )
      and    (   (     upper(b_stat_testen) = 'O' -- Open
                   and meet.afgerond = 'N'
                   and meti.afgerond = 'N'
                   and onde.afgerond = 'N'
                 )
              or (     upper(b_stat_testen) = 'A' -- Afgerond
                   and meet.afgerond = 'J'
                   and meet.mede_id_controle is not null
                 )
              or ( upper(b_stat_testen) = 'T' -- Totaal = Open + Afgerond zonder conditie op mede_id_controle
                   and (    (     meet.afgerond = 'N'
                              and meti.afgerond = 'N'
                              and onde.afgerond = 'N'
                            )
                         or (     meet.afgerond = 'J'
                            )
                       )
                 )
             )
    ;
    --
  begin
    for r_table_rec in c_table ( p_indi_code1
                               , p_indi_code2
                               , p_indi_code3
                               , p_indi_code4
                               , p_indi_code5
                               , p_indi_code6
                               , p_indi_code7
                               , p_indi_code8
                               , p_basis_stof
                               , p_onderz_wijze
                               , p_datum_va
                               , p_datum_tm
                               , p_afgerond
                               , p_stat_testen
                               , p_incl_mlpa
                               )
      loop
        insert into beh_excl_temp( onde_id
                                 , onderzoeknr
                                 , indi_code
                                 , pers_id
                                 , meet_id
                                 , meet_afgerond
                                 , stof_omschrijving
                                 , ind_seq_mlpa
                                 )
      values ( r_table_rec.onde_id
             , r_table_rec.onderzoeknr
             , r_table_rec.indi_code
             , r_table_rec.pers_id
             , r_table_rec.meet_id
             , r_table_rec.meet_afgerond
             , r_table_rec.stof_omschrijving
             , r_table_rec.ind_seq_mlpa
             );
    end loop;
    --
  end vul_beh_excl_temp;

  procedure vul_beh_excl_elem
  ( p_indi_code1   in kgc_indicatie_teksten.code%type
  , p_indi_code2   in kgc_indicatie_teksten.code%type
  , p_indi_code3   in kgc_indicatie_teksten.code%type
  , p_indi_code4   in kgc_indicatie_teksten.code%type
  , p_indi_code5   in kgc_indicatie_teksten.code%type
  , p_indi_code6   in kgc_indicatie_teksten.code%type
  , p_indi_code7   in kgc_indicatie_teksten.code%type
  , p_indi_code8   in kgc_indicatie_teksten.code%type
  , p_basis_stof   in varchar2
  , p_onderz_wijze in kgc_onderzoeken.onderzoekswijze%type
  , p_datum_va     in varchar2
  , p_datum_tm     in varchar2
  , p_afgerond     in kgc_onderzoeken.afgerond%type
  , p_stat_testen  in varchar2
  , p_incl_mlpa    in varchar2
  )
  is
    --
    -- Vul global temp tabel vul_beh_excl_temp
    --
    --
    cursor c_table ( b_indicatie in varchar2
                   )
    is
      select distinct
             excl.stof_omschrijving  stof_omschrijving
      ,      excl.ind_seq_mlpa       ind_seq_mlpa
      from   beh_excl_temp           excl
      where  excl.indi_code = b_indicatie
      order
      by     excl.ind_seq_mlpa        desc
      ,      excl.stof_omschrijving   asc
    ;
    --
    cursor c_bep_indi_codes ( b_indi_code1  in kgc_indicatie_teksten.code%type
                            , b_indi_code2  in kgc_indicatie_teksten.code%type
                            , b_indi_code3  in kgc_indicatie_teksten.code%type
                            , b_indi_code4  in kgc_indicatie_teksten.code%type
                            , b_indi_code5  in kgc_indicatie_teksten.code%type
                            , b_indi_code6  in kgc_indicatie_teksten.code%type
                            , b_indi_code7  in kgc_indicatie_teksten.code%type
                            , b_indi_code8  in kgc_indicatie_teksten.code%type
                            )
    is
      select b_indi_code1 indi_code
      from   dual
      union
      select b_indi_code2  indi_code
      from   dual
      where  b_indi_code2 is not null
      union
      select b_indi_code3  indi_code
      from   dual
      where  b_indi_code3 is not null
      union
      select b_indi_code4  indi_code
      from   dual
      where  b_indi_code4 is not null
      union
      select b_indi_code5  indi_code
      from   dual
      where  b_indi_code5 is not null
      union
      select b_indi_code6  indi_code
      from   dual
      where  b_indi_code6 is not null
      union
      select b_indi_code7  indi_code
      from   dual
      where  b_indi_code7 is not null
      union
      select b_indi_code8  indi_code
      from   dual
      where  b_indi_code8 is not null
      order
      by     1
    ;
    --
    cursor c_onde( b_indicatie in varchar2
                 )
    is
      select distinct
             mss_samenvoegen.uv_pathogene_mut_dna(excl.onde_id)             mutaties
      ,      fami.familienummer                                             familienummer
      ,      replace( pers.achternaam||', '||pers.voorletters,'''','''''')  pat_naam
      ,      to_char(pers.geboortedatum,'dd-mm-yyyy')                       pat_gebdat
      ,      pers.geslacht                                                  pat_gesl
      ,      excl.onde_id                                                   onde_id
      ,      excl.onderzoeknr                                               onderzoeknummer
      ,      rela.aanspreken                                                aanvrager
      ,      mss_samenvoegen.fractienr_met_meet(excl.onde_id)               fractienummer
      from   kgc_relaties                                   rela
      ,      kgc_onderzoeken                                onde
      ,      kgc_personen                                   pers
      ,      ( select fale.pers_id        pers_id
               ,      fami.familienummer  familienummer
               from   kgc_families      fami
               ,      kgc_familie_leden fale
               where  fami.fami_id = fale.fami_id
             )                                              fami
      ,      beh_excl_temp                                  excl
      where  excl.indi_code = b_indicatie
      and    fami.pers_id (+) = excl.pers_id
      and    pers.pers_id = excl.pers_id
      and    onde.onde_id = excl.onde_id
      and    rela.rela_id = onde.rela_id
      order
      by     excl.onderzoeknr
    ;
    --
    cursor c_extra_kol( b_indicatie in varchar2
                      )
    is
      select distinct
             excl.stof_omschrijving  stof_omschrijving
      ,      excl.ind_seq_mlpa       ind_seq_mlpa
      from   beh_excl_temp  excl
      where  excl.indi_code = b_indicatie
      order
      by     excl.ind_seq_mlpa        desc
      ,      excl.stof_omschrijving   asc
    ;
    --
    cursor c_meet_mdet_check ( b_onde_id       in kgc_onderzoeken.onde_id%type
                             , b_kolom         in varchar2
                             , b_indicatie     in varchar2
                             , b_ind_seq_mlpa  in varchar2
                             )
    is
      select ( select count(1)
               from   beh_excl_temp  excl
               where  excl.onde_id = b_onde_id
               and    excl.stof_omschrijving = b_kolom
               and    excl.indi_code = b_indicatie
               and    excl.ind_seq_mlpa = b_ind_seq_mlpa
             ) meet_aant_aanw
      ,      ( select count(1)
               from   beh_excl_temp  excl
               where  excl.onde_id = b_onde_id
               and    excl.meet_afgerond = 'J'
               and    excl.stof_omschrijving = b_kolom
               and    excl.indi_code = b_indicatie
               and    excl.ind_seq_mlpa = b_ind_seq_mlpa
             ) meet_aant_afgerond_j
      ,      ( select count(1)
               from   bas_meetwaarde_details  mdet
               ,      beh_excl_temp           excl
               where  excl.onde_id = b_onde_id
               and    excl.meet_afgerond = 'J'
               and    excl.stof_omschrijving = b_kolom
               and    excl.indi_code = b_indicatie
               and    excl.ind_seq_mlpa = b_ind_seq_mlpa
               and    mdet.meet_id = excl.meet_id
               and    (    (    b_ind_seq_mlpa = 'SEQ'
                            and mdet.prompt = 'OK?'
                            and mdet.waarde = 'J'
                           )
                       or
                           (    b_ind_seq_mlpa = 'MLPA'
                            and mdet.prompt in ('OK?','test OK?') -- Meetwaardestructuur is in 2013 veranderd ('OK?' >= 2013, 'test OK?' < 2013)
                            and mdet.waarde = 'J'
                           )
                      )
             ) mdet_ok_afw
      from   dual
    ;
    --
    cursor c_bep_waarde ( b_onde_id   in kgc_onderzoeken.onde_id%type
                        , b_kolom     in varchar2
                        , b_indicatie in varchar2
                        )
    is
      -- Meetwaarde zonder afwijking
      select 'n ' kolomwaarde
      from   bas_meetwaarde_details  mdet
      ,      beh_excl_temp           excl
      where  excl.onde_id = b_onde_id
      and    excl.meet_afgerond = 'J'
      and    excl.stof_omschrijving = b_kolom
      and    excl.indi_code = b_indicatie
      and    mdet.meet_id = excl.meet_id
      and    (   (    excl.ind_seq_mlpa = 'SEQ'
                  and mdet.prompt = 'Afwijking?'
                  and (   mdet.waarde = 'N'
                       or mdet.waarde is null
                      )
                 )
              or
                 (    excl.ind_seq_mlpa = 'MLPA'
                  and mdet.prompt in ('Naam vd afw.', 'Omschrijving van de afwijking') -- Meetwaardestructuur is in 2013 veranderd ('Naam...' >= 2013, 'Omschrijving...' < 2013)
                  and mdet.waarde is null
                 )
             )
      union
      -- Meetwaarde met afwijking
      select mdet_naam.waarde||'('||mdet_soort.waarde||')' kolomwaarde
      from   bas_meetwaarde_details  mdet_soort
      ,      bas_meetwaarde_details  mdet_naam
      ,      beh_excl_temp           excl
      where  excl.onde_id = b_onde_id
      and    excl.meet_afgerond = 'J'
      and    excl.stof_omschrijving = b_kolom
      and    excl.indi_code = b_indicatie
      and    excl.ind_seq_mlpa = 'SEQ'
      and    mdet_naam.meet_id = excl.meet_id
      and    mdet_naam.prompt = 'Naam vd afw.'
      and    mdet_naam.waarde is not null
      and    mdet_soort.meet_id = excl.meet_id
      and    mdet_soort.prompt = 'Soort afw.'
      and    mdet_soort.waarde is not null
      union
      select mdet_naam.waarde||'('||mdet_soort.waarde||')' kolomwaarde
      from   bas_meetwaarde_details  mdet_soort
      ,      bas_meetwaarde_details  mdet_naam
      ,      beh_excl_temp           excl
      where  excl.onde_id = b_onde_id
      and    excl.meet_afgerond = 'J'
      and    excl.stof_omschrijving = b_kolom
      and    excl.indi_code = b_indicatie
      and    excl.ind_seq_mlpa = 'SEQ'
      and    mdet_naam.meet_id = excl.meet_id
      and    mdet_naam.prompt = 'Naam van de 2e afwijking'
      and    mdet_naam.waarde is not null
      and    mdet_soort.meet_id = excl.meet_id
      and    mdet_soort.prompt = 'Soort afwijking (2e)'
      and    mdet_soort.waarde is not null
      union
      select mdet_naam.waarde||'('||mdet_soort.waarde||')' kolomwaarde
      from   bas_meetwaarde_details  mdet_soort
      ,      bas_meetwaarde_details  mdet_naam
      ,      beh_excl_temp           excl
      where  excl.onde_id = b_onde_id
      and    excl.meet_afgerond = 'J'
      and    excl.stof_omschrijving = b_kolom
      and    excl.indi_code = b_indicatie
      and    excl.ind_seq_mlpa = 'SEQ'
      and    mdet_naam.meet_id = excl.meet_id
      and    mdet_naam.prompt = 'Naam van de 3e afwijking'
      and    mdet_naam.waarde is not null
      and    mdet_soort.meet_id = excl.meet_id
      and    mdet_soort.prompt = 'Soort afwijking (3e)'
      and    mdet_soort.waarde is not null
      union
      select mdet_naam.waarde kolomwaarde
      from   bas_meetwaarde_details  mdet_naam
      ,      beh_excl_temp           excl
      where  excl.onde_id = b_onde_id
      and    excl.meet_afgerond = 'J'
      and    excl.stof_omschrijving = b_kolom
      and    excl.indi_code = b_indicatie
      and    excl.ind_seq_mlpa = 'MLPA'
      and    mdet_naam.meet_id = excl.meet_id
      and    mdet_naam.prompt in ('Naam vd afw.', 'Omschrijving van de afwijking') -- Meetwaardestructuur is in 2013 veranderd ('Naam...' >= 2013, 'Omschrijving...' < 2013)
      and    mdet_naam.waarde is not null
      group
      by     mdet_naam.waarde
    ;
    --
    r_meet_mdet_check_rec  c_meet_mdet_check%rowtype;
    --
    pl_teller              number(10);
    pl_extra_kol_waarde    varchar2(400);

    --
  begin
    for r_bep_indi_codes in c_bep_indi_codes( p_indi_code1
                                            , p_indi_code2
                                            , p_indi_code3
                                            , p_indi_code4
                                            , p_indi_code5
                                            , p_indi_code6
                                            , p_indi_code7
                                            , p_indi_code8
                                            )
    loop
      insert_excl_elem('<p><table border=''1'' width=''90%'' align=''center'' summary=''Script output''>');
      insert_excl_elem('<tr><td>(1) Indicatie: '||r_bep_indi_codes.indi_code||'</td></tr>');
      insert_excl_elem('<tr><td>(2) Basis stoftesten: '||p_basis_stof||'</td></tr>');
      insert_excl_elem('<tr><td>(3) Onderz.wijze: '||p_onderz_wijze||'</td></tr>');
      insert_excl_elem('<tr><td>(4) Periode: '||p_datum_va||' t/m '||p_datum_tm||'</td></tr>');
      insert_excl_elem('<tr><td>(6) Afgerond: '||p_afgerond||'</td></tr>');
      insert_excl_elem('<tr><td>(7) Status van de testen: '||p_stat_testen||'</td></tr>');
      insert_excl_elem('<tr><td>(8) Inclusief MLPA: '||p_incl_mlpa||'</td></tr>');
      insert_excl_elem('</table><p>');
      --
      insert_excl_elem('<p><table border=''1'' width=''90%'' align=''center'' summary=''Script output''><tr>');
      insert_excl_elem('<th scope="col">MUTATIES</th>');
      insert_excl_elem('<th scope="col">FAMILIENUMMER</th>');
      insert_excl_elem('<th scope="col">PATIENTNAAM</th>');
      insert_excl_elem('<th scope="col">GEBOORTEDATUM</th>');
      insert_excl_elem('<th scope="col">GESLACHT</th>');
      insert_excl_elem('<th scope="col">ONDERZOEKNUMMER</th>');
      insert_excl_elem('<th scope="col">AANVRAGER</th>');
      insert_excl_elem('<th scope="col">FRACTIENUMMER</th>');
      --
      for r_table_rec in c_table( r_bep_indi_codes.indi_code
                                )
      loop
        insert_excl_elem('<th scope="col">'||r_table_rec.stof_omschrijving||'</th>');
      end loop;
      insert_excl_elem('</tr>');
      --
      for r_onde_rec in c_onde( r_bep_indi_codes.indi_code
                              )
      loop
        insert_excl_elem('<tr>');
        insert_excl_elem('<td>'||r_onde_rec.mutaties||'</td>');
        insert_excl_elem('<td>'||r_onde_rec.familienummer||'</td>');
        insert_excl_elem('<td>'||r_onde_rec.pat_naam||'</td>');
        insert_excl_elem('<td>'||r_onde_rec.pat_gebdat||'</td>');
        insert_excl_elem('<td>'||r_onde_rec.pat_gesl||'</td>');
        insert_excl_elem('<td>'||r_onde_rec.onderzoeknummer||'</td>');
        insert_excl_elem('<td>'||r_onde_rec.aanvrager||'</td>');
        insert_excl_elem('<td>'||r_onde_rec.fractienummer||'</td>');
        --
        for r_extra_kol in c_extra_kol( r_bep_indi_codes.indi_code
                                      )
        loop
          pl_extra_kol_waarde := '''ONBEPAALD''';
          --
          open  c_meet_mdet_check( r_onde_rec.onde_id
                                 , r_extra_kol.stof_omschrijving
                                 , r_bep_indi_codes.indi_code
                                 , r_extra_kol.ind_seq_mlpa
                                 );
          fetch c_meet_mdet_check
          into  r_meet_mdet_check_rec;
          close c_meet_mdet_check;
          --
          if r_meet_mdet_check_rec.meet_aant_aanw = 0 -- De stoftest is niet aangemeld
          then
            pl_extra_kol_waarde := 'NVT';
          elsif r_meet_mdet_check_rec.meet_aant_afgerond_j = 0 -- Er is geen afgeronde meetwaarde voor de stoftest te vinden
          then
            pl_extra_kol_waarde := '-';
          elsif r_meet_mdet_check_rec.mdet_ok_afw = 0 -- Er is geen gelukte test te vinden
          then
            pl_extra_kol_waarde := '-';
          else
            pl_teller := 0;
            --
            for r_bep_waarde_rec in c_bep_waarde ( r_onde_rec.onde_id
                                                 , r_extra_kol.stof_omschrijving
                                                 , r_bep_indi_codes.indi_code
                                                 )
            loop
              if pl_teller = 0
              then
               pl_extra_kol_waarde := null;
              else
                pl_extra_kol_waarde := pl_extra_kol_waarde||', ';
              end if;
              --
              pl_extra_kol_waarde := pl_extra_kol_waarde||r_bep_waarde_rec.kolomwaarde;
              pl_teller := pl_teller + 1;
            end loop;
            --
          end if;
          --
          insert_excl_elem('<td>'||pl_extra_kol_waarde||'</td>');
        end loop;
        --
        insert_excl_elem('</tr>');
        --
      end loop;
      --
      insert_excl_elem('</table><p>');
      --
    end loop;
    --
  end vul_beh_excl_elem;

  procedure insert_excl_elem ( p_elem_text in varchar2
                             )
  is
  begin
    pl_volgnr := pl_volgnr + 1;
    --
    insert into beh_excl_elem( excel_element
                             , volgnr
                             )
    values ( p_elem_text
           , pl_volgnr
           );
  end insert_excl_elem;

end beh_excellijst;
/

/
QUIT
