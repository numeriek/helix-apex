CREATE OR REPLACE PACKAGE "HELIX"."BEH_SOAP_API" AS
-- --------------------------------------------------------------------------
-- Name         : soap_api
-- Changelist   : mk: Adjusted the soap api to aZM requirements
--                rk: Removed change above and added overloaded fucntion of the invoke funtion
--                    for ADT/DFT support

TYPE t_request IS RECORD (
  method        VARCHAR2(256),
  namespace     VARCHAR2(32767),
  body          VARCHAR2(32767),
  envelope_tag  VARCHAR2(30)
);

TYPE t_response IS RECORD
(
  doc           XMLTYPE,
  envelope_tag  VARCHAR2(30)
);

FUNCTION new_request(p_method        IN  VARCHAR2,
                     p_namespace     IN  VARCHAR2,
                     p_envelope_tag  IN  VARCHAR2 DEFAULT 'soap')
  RETURN t_request;


PROCEDURE add_parameter(p_request  IN OUT NOCOPY  t_request,
                        p_name     IN             VARCHAR2,
                        p_value    IN             VARCHAR2,
                        p_type     IN             VARCHAR2 := NULL);

PROCEDURE add_complex_parameter(p_request  IN OUT NOCOPY  t_request,
                                p_xml      IN             VARCHAR2);

FUNCTION invoke(p_request  IN OUT NOCOPY  t_request,
                p_url      IN             VARCHAR2,
                p_action   IN             VARCHAR2
                )
  RETURN t_response;

FUNCTION invoke(p_request IN OUT NOCOPY  t_request,
                p_url     IN             VARCHAR2,
                p_actions IN             VARCHAR2,
                p_message IN             VARCHAR2,
                p_auth IN           VARCHAR2,
                p_type IN           VARCHAR2
                )

  RETURN t_response ;

FUNCTION get_return_value(p_response   IN OUT NOCOPY  t_response,
                          p_name       IN             VARCHAR2,
                          p_namespace  IN             VARCHAR2)
  RETURN VARCHAR2;

PROCEDURE debug_on;
PROCEDURE debug_off;

END BEH_SOAP_API;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_SOAP_API" AS
-- --------------------------------------------------------------------------
-- Name         : BEH_SOAP_API
-- Author       : MK
-- --------------------------------------------------------------------------

g_debug  BOOLEAN := FALSE;

PROCEDURE show_envelope(p_env     IN  VARCHAR2,
                        p_heading IN  VARCHAR2 DEFAULT NULL);



-- ---------------------------------------------------------------------
FUNCTION new_request(p_method        IN  VARCHAR2,
                     p_namespace     IN  VARCHAR2,
                     p_envelope_tag  IN  VARCHAR2 DEFAULT 'soap')
  RETURN t_request AS
-- ---------------------------------------------------------------------
  l_request  t_request;
BEGIN
  l_request.method       := p_method;
  l_request.namespace    := p_namespace;
  l_request.envelope_tag := p_envelope_tag;
  RETURN l_request;
END;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
PROCEDURE add_parameter(p_request  IN OUT NOCOPY  t_request,
                        p_name     IN             VARCHAR2,
                        p_value    IN             VARCHAR2,
                        p_type     IN             VARCHAR2 := NULL) AS
-- ---------------------------------------------------------------------
BEGIN
  IF p_type IS NULL THEN
    p_request.body := p_request.body||'<'||p_name||'>'||p_value||'</'||p_name||'>';
  ELSE
    p_request.body := p_request.body||'<'||p_name||' xsi:type="'||p_type||'">'||p_value||'</'||p_name||'>';
  END IF;
END;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
PROCEDURE add_complex_parameter(p_request  IN OUT NOCOPY  t_request,
                                p_xml      IN             VARCHAR2) AS
-- ---------------------------------------------------------------------
BEGIN
  p_request.body := p_request.body||p_xml;
END;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
PROCEDURE generate_envelope(p_request  IN OUT NOCOPY  t_request,
                            p_env      IN OUT NOCOPY  VARCHAR2) AS
-- ---------------------------------------------------------------------
BEGIN
  p_env := '<'||p_request.envelope_tag||':Envelope xmlns:'||p_request.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/" ' ||
               'xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">' ||
             '<'||p_request.envelope_tag||':Body>' ||
               '<'||p_request.method||' '||p_request.namespace||' '||p_request.envelope_tag||':encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' ||
                   p_request.body ||
               '</'||p_request.method||'>' ||
             '</'||p_request.envelope_tag||':Body>' ||
           '</'||p_request.envelope_tag||':Envelope>';
END;

PROCEDURE generate_envelope_cdm(p_request  IN OUT NOCOPY  t_request,
                            p_env      IN OUT NOCOPY  VARCHAR2 )as
Begin
  p_env := '<'||p_request.envelope_tag||':Envelope xmlns:'||p_request.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/" ' ||
               'xmlns:ns1="http://helix/">' ||
             '<'||p_request.envelope_tag||':Header/>' ||
             '<'||p_request.envelope_tag||':Body>' ||
               '<ns1:'||p_request.method||'>'||
                   p_request.body ||
               '</ns1:'||p_request.method||'>' ||
             '</'||p_request.envelope_tag||':Body>' ||
           '</'||p_request.envelope_tag||':Envelope>';

End;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
PROCEDURE show_envelope(p_env     IN  VARCHAR2,
                        p_heading IN  VARCHAR2 DEFAULT NULL) AS
-- ---------------------------------------------------------------------
  i      PLS_INTEGER;
  l_len  PLS_INTEGER;
BEGIN
  IF g_debug THEN
    IF p_heading IS NOT NULL THEN
      DBMS_OUTPUT.put_line('*****' || p_heading || '*****');
    END IF;

    i := 1; l_len := LENGTH(p_env);
    WHILE (i <= l_len) LOOP
      DBMS_OUTPUT.put_line(SUBSTR(p_env, i, 2000));
      i := i + 2000;
    END LOOP;
  END IF;
END;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
PROCEDURE check_fault(p_response IN OUT NOCOPY  t_response) AS
-- ---------------------------------------------------------------------
  l_fault_node    XMLTYPE;
  l_fault_code    VARCHAR2(256);
  l_fault_string  VARCHAR2(32767);
BEGIN
  l_fault_node := p_response.doc.extract('/'||p_response.envelope_tag||':Fault',
                                         'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/');
  IF (l_fault_node IS NOT NULL) THEN
    l_fault_code   := l_fault_node.extract('/'||p_response.envelope_tag||':Fault/faultcode/child::text()',
                                           'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/').getstringval();
    l_fault_string := l_fault_node.extract('/'||p_response.envelope_tag||':Fault/faultstring/child::text()',
                                           'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/').getstringval();
    RAISE_APPLICATION_ERROR(-20000, l_fault_code || ' - ' || l_fault_string);
  END IF;
END;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
FUNCTION invoke(p_request IN OUT NOCOPY  t_request,
                p_url     IN             VARCHAR2,
                p_action  IN             VARCHAR2)
  RETURN t_response AS
-- ---------------------------------------------------------------------
  l_envelope       VARCHAR2(32767);
  l_http_request   UTL_HTTP.req;
  l_http_response  UTL_HTTP.resp;
  l_response       t_response;
BEGIN
  generate_envelope(p_request, l_envelope);
  show_envelope(l_envelope, 'Request');
   utl_http.set_transfer_timeout(120);
  l_http_request := UTL_HTTP.begin_request(p_url, 'POST','HTTP/1.1');

  UTL_HTTP.set_header(l_http_request, 'Content-Type', 'text/xml');
  UTL_HTTP.set_header(l_http_request, 'Content-Length', LENGTH(l_envelope));
  UTL_HTTP.set_header(l_http_request, 'SOAPAction', p_action);
 -- insert into verstuur_bericht(bericht) values(l_envelope);
 -- commit;
  UTL_HTTP.write_text(l_http_request, l_envelope);
  l_http_response := UTL_HTTP.get_response(l_http_request);
  UTL_HTTP.read_text(l_http_response, l_envelope);
  UTL_HTTP.end_response(l_http_response);


  show_envelope(l_envelope, 'Response');


  l_response.doc := XMLTYPE.createxml(l_envelope);
  l_response.envelope_tag := p_request.envelope_tag;
--  l_response.doc := l_response.doc.extract('/'||l_response.envelope_tag||':Envelope/'||l_response.envelope_tag||':Body/child::node()',
--                                          'xmlns:'||l_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/"');


  commit;

 check_fault(l_response);



  RETURN l_response;


END;
--------------------------------------------------------------------------------------
---- OVERLOADED FUNCTION OF THE STANDARD INVOKE FUNCTION   ------
---- ADJUSTED FOR USE WITH ADT/DFT/ORU XML MESSAGES TO SAP ------
---- Added support for to many open connections and support for empty response messages
--------------------------------------------------------------------------------------
FUNCTION invoke(p_request IN OUT NOCOPY  t_request,
                p_url     IN             VARCHAR2,
                p_actions IN             VARCHAR2,
                p_message IN             VARCHAR2,
                p_auth IN                VARCHAR2,
                P_Type IN                VARCHAR2
                )
  RETURN t_response AS
-- ---------------------------------------------------------------------
  l_envelope      VARCHAR2(32767);
	l_log_env       VARCHAR2(4000);
  l_http_request  UTL_HTTP.req;
  l_http_response UTL_HTTP.resp;
  l_response      t_response;
  e_error         Exception;
  e_soap_invoke   EXCEPTION;
  v_soap_id       Number(12);
  v_req           VARCHAR2(2000);
  v_resp          VARCHAR2(2000);
BEGIN
  -- Zet instellingen
  if p_type like 'CDM' then
     generate_envelope_cdm(p_request, l_envelope);
  else
     generate_envelope(p_request, l_envelope);
  end if;
  show_envelope(l_envelope, 'Request');
  utl_http.set_response_error_check(true);
  utl_http.set_persistent_conn_support(true);
  utl_http.set_transfer_timeout(120);


  -- CREEER LOG ENTRY
  v_soap_id := BEH_SOAP_IN_OUT_SEQ.nextval ;
  Beh_Logging.Beh_Soap_In_Out_Ins( v_soap_id,
                                      p_message,
                                      sysdate,
                                      'N'
                                    );


  -- START VESRTUREN SOAP BERICHT
  BEGIN
      v_resp := '-';

     -- Send Http Request to SAP PI.
     l_http_request := UTL_HTTP.begin_request(p_url, 'POST');

     UTL_HTTP.set_header(l_http_request, 'Content-Type', 'text/xml');
     UTL_HTTP.set_header(l_http_request, 'Content-Length', LENGTH(l_envelope));
     UTL_HTTP.set_header(l_http_request, 'SOAPAction', p_actions);

     if p_auth = 'J' THEN
        UTL_HTTP.set_authentication(l_http_request,'webservice2','WEB1234#');
     end if;
    -- Schrijf XML bericht naar SAP PI



     UTL_HTTP.write_text(l_http_request, l_envelope);

		 if length(l_envelope) > 3999 then
			  l_log_env := substr(l_envelope,0,3998);
		else
			   l_log_env := l_envelope ;
		end if;

     v_req := l_http_request.HTTP_VERSION||CHR(10)||l_http_request.method;

     Beh_logging.Beh_Soap_in_out_Upd(  P_SOAP_ID       => v_soap_id,
                                       P_REQUEST       => l_log_env ,
                                       P_RESPONSE      => v_resp ,
                                       P_ERRORMSG      =>'',
                                       P_DATUM_REPONSE => sysdate,
                                       P_URL           => p_url,
                                       P_METHODE       => 'REQ' );


     -- Get response bericht
     l_http_response := UTL_HTTP.get_response(l_http_request);
     UTL_HTTP.read_text(l_http_response, l_envelope);
     -- Sluit verbinding
     UTL_HTTP.end_response(l_http_response);

     v_resp := l_http_response.http_version||CHR(10)||
               to_char(l_http_response.Status_code)||' '||CHR(10)||
                       l_http_response.reason_phrase;


     Beh_logging.Beh_Soap_in_out_Upd(  P_SOAP_ID       => v_soap_id,
                                       P_REQUEST       => l_log_env ,
                                       P_RESPONSE      => v_resp ,
                                       P_ERRORMSG      =>'',
                                       P_DATUM_REPONSE => sysdate,
                                       P_URL           => p_url,
                                       P_METHODE       => 'RES' );

     -- VERWERK RESPONSE BERICHT.
     if l_http_response.status_code = 200 then
        show_envelope(l_envelope, 'Response');
        l_response.doc := XMLTYPE.createxml(l_envelope);
        l_response.envelope_tag := p_request.envelope_tag;
        COMMIT;
     else
        l_response.envelope_tag := 'FOUT';
     End if   ;
        RETURN l_response;

  EXCEPTION
     WHEN UTL_HTTP.BAD_ARGUMENT
     THEN
     BEGIN
        Beh_logging.Beh_Soap_in_out_Upd( P_SOAP_ID       => v_soap_id,
                                             P_REQUEST       => l_log_env ,
                                             P_RESPONSE      => v_resp ,
                                             P_ERRORMSG      =>'BAD_ARGUMENT',
                                             P_DATUM_REPONSE => sysdate,
                                             P_URL           => p_url,
                                             P_METHODE       => 'ERR' );
     commit;
     END;

     WHEN UTL_HTTP.TRANSFER_TIMEOUT
     THEN
     BEGIN
        Beh_logging.Beh_Soap_in_out_Upd( P_SOAP_ID       => v_soap_id,
                                             P_REQUEST       => l_log_env ,
                                             P_RESPONSE      => v_resp ,
                                             P_ERRORMSG      =>'TRANSFER_TIMEOUT',
                                             P_DATUM_REPONSE => sysdate,
                                             P_URL           => p_url,
                                             P_METHODE       => 'ERR' );
        commit;
        RAISE UTL_HTTP.TRANSFER_TIMEOUT ;
     END;

     WHEN UTL_HTTP.Request_failed
     THEN
     BEGIN

            Beh_logging.Beh_Soap_in_out_Upd( P_SOAP_ID       => v_soap_id,
                                             P_REQUEST       => l_log_env ,
                                             P_RESPONSE      => v_resp ,
                                             P_ERRORMSG      =>'REQUEST FAILED',
                                             P_DATUM_REPONSE => sysdate,
                                             P_URL           => p_url,
                                             P_METHODE       => 'ERR' );
            commit;
            Raise UTL_HTTP.Request_failed ;
     END;

     WHEN UTL_HTTP.too_many_requests
     THEN UTL_HTTP.end_request(l_http_request) ;

     WHEN UTL_HTTP.illegal_call
     THEN
     BEGIN
         Beh_logging.Beh_Soap_in_out_Upd( P_SOAP_ID       => v_soap_id,
                                             P_REQUEST       => l_log_env ,
                                             P_RESPONSE      => v_resp ,
                                             P_ERRORMSG      =>'ILLEGAL CALL',
                                             P_DATUM_REPONSE => sysdate,
                                             P_URL           => p_url,
                                             P_METHODE       => 'ERR' );
         commit;
         UTL_HTTP.end_request(l_http_request) ;
         raise utl_http.illegal_call;

     END;

     WHEN OTHERS THEN
     BEGIN
        Beh_logging.Beh_Soap_in_out_Upd( P_SOAP_ID       => v_soap_id,
                                             P_REQUEST       => l_log_env ,
                                             P_RESPONSE      => v_resp ,
                                             P_ERRORMSG      =>'ERROR UNKNOWN',
                                             P_DATUM_REPONSE => sysdate,
                                             P_URL           => p_url,
                                             P_METHODE       => 'ERR' );
       commit;

        RAISE e_soap_invoke;

     END;


  END;




END;


-- ---------------------------------------------------------------------
FUNCTION get_return_value(p_response   IN OUT NOCOPY  t_response,
                          p_name       IN             VARCHAR2,
                          p_namespace  IN             VARCHAR2)
  RETURN VARCHAR2 AS
-- ---------------------------------------------------------------------
BEGIN
  RETURN p_response.doc.extract('//'||p_name||'/child::text()',p_namespace).getstringval();
END;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
PROCEDURE debug_on AS
-- ---------------------------------------------------------------------
BEGIN
  g_debug := TRUE;
END;
-- ---------------------------------------------------------------------



-- ---------------------------------------------------------------------
PROCEDURE debug_off AS
-- ---------------------------------------------------------------------
BEGIN
  g_debug := FALSE;
END;
-- ---------------------------------------------------------------------

END BEH_SOAP_API;
/

/
QUIT
