CREATE OR REPLACE PACKAGE "HELIX"."BEH_JAARVERSLAG"
IS
  --
  -- Procedure vult tabel beh_jaarverslag_temp.
  --
  procedure vul_jvsl_temp ( p_datum_va in varchar2
                          , p_datum_tm in varchar2
                          );

  --
  -- Functie bepaalt alle verschillende indicatie omschrijvingen waarop de totale produktie tellingen van Farmacogenetica betrekking hebben.
  -- En geeft deze terug als 1 lange string gescheiden door komma's.
  --
  function bep_farma_indi_omschr
  return varchar2;

end beh_jaarverslag;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_JAARVERSLAG"
IS

  --
  -- Procedure vult tabel beh_jaarverslag_temp.
  --
  procedure vul_jvsl_temp ( p_datum_va in varchar2
                          , p_datum_tm in varchar2
                          )
  is
    begin
    --
    -- Toevoegen records voor afdeling DNA op basis van declaraties
    --
    insert
    into   beh_jaarverslag_temp( decl_id
                               , entiteit
                               , id
                               , declareren
                               , kafd_id
                               , ongr_id
                               , ongr_code
                               , verrichtingcode
                               , verrichting_omschrijving
                               , telcode
                               , pers_id_alt_decl
                               , decl_status
                               , decl_pers_id
                               , dewy_code
                               , indi_code
                               , indi_omschrijving
                               , onde_id
                               , onde_status
                               , onwy_omschrijving
                               , onderzoekstype
                               , onde_pers_id
                               , onui_code
                               , ind_decl_onde
                               )
      ( select decl.decl_id                   decl_id
        ,      decl.entiteit                  entiteit
        ,      decl.id                        id
        ,      decl.declareren                declareren
        ,      decl.kafd_id                   kafd_id
        ,      decl.ongr_id                   ongr_id
        ,      ongr.code                      ongr_code
        ,      decl.verrichtingcode           verrichtingcode
        ,      decl.betreft_tekst             verrichting_omschrijving
        ,      to_char(null)                  telcode -- alleen van belang voor CYTO
        ,      decl.pers_id_alt_decl          pers_id_alt_decl
        ,      decl.status                    decl_status
        ,      decl.pers_id                   decl_pers_id
        ,      dewy.code                      dewy_code
        ,      indi.code                      indi_code
        ,      indi.omschrijving              indi_omschrijving
        ,      onde.onde_id                   onde_id
        ,      onde.status                    onde_status
        ,      onwy.omschrijving              onwy_omschrijving
        ,      onde.onderzoekstype            onderzoekstype
        ,      onde.pers_id                   onde_pers_id
        ,      onui.code                      onui_code
        ,      'DECL'                         ind_decl_onde
        from   kgc_onderzoek_uitslagcodes  onui
        ,      kgc_indicatie_teksten       indi
        ,      kgc_onderzoek_indicaties    onin
        ,      kgc_declaratiewijzen        dewy
        ,      kgc_onderzoekswijzen        onwy
        ,      kgc_onderzoeksgroepen       ongr
        ,      kgc_kgc_afdelingen          kafd
        ,      kgc_onderzoeken             onde
        ,      kgc_declaraties             decl
        where  decl.datum between to_date(p_datum_va,'ddmmyyyy')
                          and     to_date(p_datum_tm,'ddmmyyyy')
        and    decl.kafd_id = kafd.kafd_id
        and    kafd.code = 'LAB'
        and    ongr.code = 'DNA'
        and    ongr.ongr_id = decl.ongr_id
        and    onde.onde_id = beh_getonde_id(decl.entiteit,decl.id)
        and    onwy.code (+) = onde.onderzoekswijze
        and    nvl(onwy.kafd_id,kafd.kafd_id) = kafd.kafd_id
        and    dewy.dewy_id (+) = decl.dewy_id
        and    onin.onde_id = onde.onde_id
        and    indi.indi_id = onin.indi_id
        and    onui.onui_id (+) = onde.onui_id
      );
    --
    -- Toevoegen records voor FARMACOGENETICA op basis van declaraties (afdeling onafhankelijk)
    --
    insert
    into   beh_jaarverslag_temp( decl_id
                               , entiteit
                               , id
                               , declareren
                               , kafd_id
                               , ongr_id
                               , ongr_code
                               , verrichtingcode
                               , verrichting_omschrijving
                               , telcode
                               , pers_id_alt_decl
                               , decl_status
                               , decl_pers_id
                               , dewy_code
                               , indi_code
                               , indi_omschrijving
                               , onde_id
                               , onde_status
                               , onwy_omschrijving
                               , onderzoekstype
                               , onde_pers_id
                               , onui_code
                               , ind_decl_onde
                               )
      ( select decl.decl_id                   decl_id
        ,      decl.entiteit                  entiteit
        ,      decl.id                        id
        ,      decl.declareren                declareren
        ,      decl.kafd_id                   kafd_id
        ,      decl.ongr_id                   ongr_id
        ,      ongr.code                      ongr_code
        ,      decl.verrichtingcode           verrichtingcode
        ,      decl.betreft_tekst             verrichting_omschrijving
        ,      to_char(null)                  telcode -- alleen van belang voor CYTO (FARMA is DNA/METAB)
        ,      decl.pers_id_alt_decl          pers_id_alt_decl
        ,      decl.status                    decl_status
        ,      decl.pers_id                   decl_pers_id
        ,      dewy.code                      dewy_code
        ,      to_char(null)                  indi_code
        ,      to_char(null)                  indi_omschrijving
        ,      onde.onde_id                   onde_id
        ,      onde.status                    onde_status
        ,      to_char(null)                  onwy_omschrijving
        ,      onde.onderzoekstype            onderzoekstype
        ,      onde.pers_id                   onde_pers_id
        ,      to_char(null)                  onui_code
        ,      'DECL'                         ind_decl_onde
        from   kgc_declaratiewijzen        dewy
        ,      kgc_onderzoeksgroepen       ongr
        ,      kgc_onderzoeken             onde
        ,      kgc_declaraties             decl
        where  decl.datum between to_date(p_datum_va,'ddmmyyyy')
                          and     to_date(p_datum_tm,'ddmmyyyy')
        and    ongr.ongr_id = decl.ongr_id
        and    ongr.code = 'FARMA'
        and    onde.onde_id = beh_getonde_id(decl.entiteit,decl.id)
        and    dewy.dewy_id = decl.dewy_id
      );
    --
    -- Toevoegen records voor afdeling CYTO op basis van declaraties
    --
    insert
    into   beh_jaarverslag_temp( decl_id
                               , entiteit
                               , id
                               , declareren
                               , kafd_id
                               , ongr_id
                               , ongr_code
                               , verrichtingcode
                               , verrichting_omschrijving
                               , telcode
                               , pers_id_alt_decl
                               , decl_status
                               , decl_pers_id
                               , dewy_code
                               , indi_code
                               , indi_omschrijving
                               , onde_id
                               , onde_status
                               , onwy_omschrijving
                               , onderzoekstype
                               , onde_pers_id
                               , onui_code
                               , ind_decl_onde
                               )
      ( select decl.decl_id                                      decl_id
        ,      decl.entiteit                                     entiteit
        ,      decl.id                                           id
        ,      decl.declareren                                   declareren
        ,      decl.kafd_id                                      kafd_id
        ,      decl.ongr_id                                      ongr_id
        ,      ongr.code                                         ongr_code
        ,      cate.code                                         verrichtingcode
        ,      cate.omschrijving                                 verrichting_omschrijving
        ,      decl.verrichtingcode                              telcode
        ,      decl.pers_id_alt_decl                             pers_id_alt_decl
        ,      decl.status                                       decl_status
        ,      decl.pers_id                                      decl_pers_id
        ,      dewy.code                                         dewy_code
        ,      indi.code                                         indi_code
        ,      indi.omschrijving                                 indi_omschrijving
        ,      onde.onde_id                                      onde_id
        ,      onde.status                                       onde_status
        ,      to_char(null)                                     onwy_omschrijving
        ,      onde.onderzoekstype                               onderzoekstype
        ,      onde.pers_id                                      onde_pers_id
        ,      onui.code                                         onui_code
        ,      'DECL'                                            ind_decl_onde
        from   kgc_categorie_types                    caty
        ,      kgc_categorieen                        cate
        ,      kgc_categorie_waarden                  cawa
        ,      ( select onin.onde_id       onde_id
                 ,      indi.code          code          -- Er kunnen meerdere indicatiecodes per onderzoek voorkomen
                 ,      indi.omschrijving  omschrijving  -- Neem willekeurig eentje zodat per declaratie 1 record wordt
                 from   kgc_indicatie_teksten    indi    -- geteld. Het gaat erom dat de aantallen declaraties kloppen.
                 ,      kgc_onderzoek_indicaties onin
                 where  onin.onin_id = ( select min(onin2.onin_id)
                                         from   kgc_onderzoek_indicaties onin2
                                         where  onin2.onde_id = onin.onde_id
                                       )
                 and    indi.indi_id = onin.indi_id
               )                                      indi
        ,      kgc_onderzoek_uitslagcodes             onui
        ,      kgc_declaratiewijzen                   dewy
        ,      kgc_onderzoeksgroepen                  ongr
        ,      kgc_kgc_afdelingen                     kafd
        ,      kgc_onderzoeken                        onde
        ,      kgc_declaraties                        decl
        where  decl.datum between to_date(p_datum_va,'ddmmyyyy')
                          and     to_date(p_datum_tm,'ddmmyyyy')
        and    decl.kafd_id = kafd.kafd_id
        and    kafd.code = 'LAB'
        and    ongr.code in ('PR','PM','PV','TM','TV')
        and    ongr.ongr_id = decl.ongr_id
        and    onde.onde_id = beh_getonde_id(decl.entiteit,decl.id)
        and    dewy.dewy_id (+) = decl.dewy_id
        and    onui.onui_id (+) = onde.onui_id
        and    indi.onde_id (+) = onde.onde_id
        and    cawa.waarde (+) = decl.verrichtingcode
        and    cate.cate_id (+) = cawa.cate_id
        and    caty.caty_id (+) = cate.caty_id
        and    nvl(caty.code,'VERR_CYTO') = 'VERR_CYTO'
      );
    --
    -- Toevoegen records voor afdeling DNA op basis van onderzoeken
    --
    insert
    into   beh_jaarverslag_temp( decl_id
                               , entiteit
                               , id
                               , declareren
                               , kafd_id
                               , ongr_id
                               , ongr_code
                               , verrichtingcode
                               , verrichting_omschrijving
                               , telcode
                               , pers_id_alt_decl
                               , decl_status
                               , decl_pers_id
                               , dewy_code
                               , indi_code
                               , indi_omschrijving
                               , onde_id
                               , onde_status
                               , onwy_omschrijving
                               , onderzoekstype
                               , onde_pers_id
                               , onui_code
                               , ind_decl_onde
                               )
      ( select to_number(null)                                      decl_id
        ,      to_char(null)                                        entiteit
        ,      to_number(null)                                      id
        ,      onde.declareren                                      declareren
        ,      onde.kafd_id                                         kafd_id
        ,      onde.ongr_id                                         ongr_id
        ,      ongr.code                                            ongr_code
        ,      case
                 when onbe.onde_id is null
                 and  lower (onwy.omschrijving) like '%prenata%'
                 then 377891
                 when onbe.onde_id is null
                 and  lower (onwy.omschrijving) not like '%prenata%'
                 then 377889
                 when onbe.onde_id is not null
                 and  mons.foet_id is not null
                 then 377891
                 when onbe.onde_id is not null
                 and  mons.foet_id is null
                 then 377889
               end                                                  verrichtingcode
        ,      to_char(null)                                        verrichting_omschrijving
        ,      to_char(null)                                        telcode -- alleen van belang voor CYTO
        ,      to_number(null)                                      pers_id_alt_decl
        ,      to_char(null)                                        decl_status
        ,      to_number(null)                                      decl_pers_id
        ,      to_char(null)                                        dewy_code
        ,      indi.code                                            indi_code
        ,      indi.omschrijving                                    indi_omschrijving
        ,      onde.onde_id                                         onde_id
        ,      onde.status                                          onde_status
        ,      onwy.omschrijving                                    onwy_omschrijving
        ,      onde.onderzoekstype                                  onderzoekstype
        ,      onde.pers_id                                         onde_pers_id
        ,      to_char(null)                                        onui_code
        ,      'ONDE'                                               ind_decl_onde
        from   kgc_monsters                mons
        ,      bas_fracties                frac
        ,      kgc_onderzoek_betrokkenen   onbe
        ,      kgc_indicatie_teksten       indi
        ,      kgc_onderzoek_indicaties    onin
        ,      kgc_onderzoekswijzen        onwy
        ,      kgc_onderzoeksgroepen       ongr
        ,      kgc_kgc_afdelingen          kafd
        ,      kgc_onderzoeken             onde
        where  onde.datum_binnen between to_date(p_datum_va,'ddmmyyyy')
                                 and     to_date(p_datum_tm,'ddmmyyyy')
        and    onde.kafd_id = kafd.kafd_id
        and    kafd.code = 'LAB'
        and    ongr.code = 'DNA'
        and    ongr.ongr_id = onde.ongr_id
        and    onde.status = 'G'
        and    onwy.code (+) = onde.onderzoekswijze
        and    nvl(onwy.kafd_id,kafd.kafd_id) = kafd.kafd_id
        and    onin.onde_id = onde.onde_id
        and    indi.indi_id = onin.indi_id
        and    onbe.onde_id (+) = onde.onde_id
        and    nvl(onbe.onbe_id,-99)  in ( select nvl(min(onbe2.onbe_id),-99)
                                           from   kgc_monsters               mons2
                                           ,      bas_fracties               frac2
                                           ,      kgc_onderzoek_betrokkenen  onbe2
                                           where  onbe2.onde_id = onde.onde_id
                                           and    onbe2.pers_id = onbe.pers_id
                                           and    frac2.frac_id (+) = onbe2.frac_id
                                           and    mons2.mons_id (+) = frac2.mons_id
                                           group
                                           by     onbe2.pers_id
                                           ,      mons2.foet_id  -- tel het aantal unieke personen in de betrokkenen
                                         )                       -- foetus telt apart
        and    frac.frac_id (+) = onbe.frac_id
        and    mons.mons_id (+) = frac.mons_id
      );
    --
    -- Toevoegen records voor afdeling CYTO op basis van onderzoeken
    --
    insert
    into   beh_jaarverslag_temp( decl_id
                               , entiteit
                               , id
                               , declareren
                               , kafd_id
                               , ongr_id
                               , ongr_code
                               , verrichtingcode
                               , verrichting_omschrijving
                               , telcode
                               , pers_id_alt_decl
                               , decl_status
                               , decl_pers_id
                               , dewy_code
                               , indi_code
                               , indi_omschrijving
                               , onde_id
                               , onde_status
                               , onwy_omschrijving
                               , onderzoekstype
                               , onde_pers_id
                               , onui_code
                               , ind_decl_onde
                               )
      ( select to_number(null)                                      decl_id
        ,      to_char(null)                                        entiteit
        ,      to_number(null)                                      id
        ,      onde.declareren                                      declareren
        ,      onde.kafd_id                                         kafd_id
        ,      onde.ongr_id                                         ongr_id
        ,      ongr.code                                            ongr_code
        ,      to_char(null)                                        verrichtingcode
        ,      to_char(null)                                        verrichting_omschrijving
        ,      to_char(null)                                        telcode
        ,      to_number(null)                                      pers_id_alt_decl
        ,      to_char(null)                                        decl_status
        ,      to_number(null)                                      decl_pers_id
        ,      to_char(null)                                        dewy_code
        ,      to_char(null)                                        indi_code
        ,      to_char(null)                                        indi_omschrijving
        ,      onde.onde_id                                         onde_id
        ,      onde.status                                          onde_status
        ,      onwy.omschrijving                                    onwy_omschrijving
        ,      onde.onderzoekstype                                  onderzoekstype
        ,      onde.pers_id                                         onde_pers_id
        ,      onui.code                                            onui_code
        ,      'ONDE'                                               ind_decl_onde
        from   kgc_onderzoek_uitslagcodes  onui
        ,      kgc_onderzoekswijzen        onwy
        ,      kgc_onderzoeksgroepen       ongr
        ,      kgc_kgc_afdelingen          kafd
        ,      kgc_onderzoeken             onde
        where  onde.datum_binnen between to_date(p_datum_va,'ddmmyyyy')
                                 and     to_date(p_datum_tm,'ddmmyyyy')
        and    onde.kafd_id = kafd.kafd_id
        and    kafd.code = 'LAB'
        and    ongr.code in ('PR','PM','PV','TM','TV')
        and    ongr.ongr_id = onde.ongr_id
        and    onwy.code (+) = onde.onderzoekswijze
        and    nvl(onwy.kafd_id,kafd.kafd_id) = kafd.kafd_id
        and    onui.onui_id (+) = onde.onui_id
      );
  end vul_jvsl_temp;

  --
  -- Functie bepaalt alle verschillende indicatie omschrijvingen waarop de totale produktie tellingen van Farmacogenetica betrekking hebben.
  -- En geeft deze terug als 1 lange string gescheiden door komma's.
  --
  function bep_farma_indi_omschr
  return varchar2
  is
    cursor c_jvsl
    is
      select  distinct
              indi.omschrijving omschrijving
      from    kgc_indicatie_teksten    indi
      ,       kgc_onderzoek_indicaties onin
      ,       beh_jaarverslag_temp     jvsl
      where   jvsl.ongr_code = 'FARMA'
      and     onin.onde_id = jvsl.onde_id
      and     indi.indi_id = onin.indi_id
    ;
    --
    pl_indi_codes varchar2(256);
    --
  begin
    pl_indi_codes := null;
    --
    for r_jvsl_rec in c_jvsl
    loop
      if (pl_indi_codes is null)
      then
        pl_indi_codes := r_jvsl_rec.omschrijving;
      else
        pl_indi_codes := pl_indi_codes ||', '||r_jvsl_rec.omschrijving;
      end if;
    end loop;
    --
    return(pl_indi_codes);
    --
  end bep_farma_indi_omschr;

end beh_jaarverslag;
/

/
QUIT
