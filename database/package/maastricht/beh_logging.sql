CREATE OR REPLACE PACKAGE "HELIX"."BEH_LOGGING" AS
/* OVERAL PACKAGE VOOR HET LOGGEN VAN BERICHTEN DIE IN DE DIVERSE INTERFACES VERWERKT WORDEN.*/

PROCEDURE BEH_SOAP_IN_OUT_INS ( P_SOAP_ID  NUMBER,
                                P_MESSAGE_ID  VARCHAR2,
                                P_DATUM_TIJD DATE,
                                P_STATUS     VARCHAR2);




PROCEDURE BEH_SOAP_IN_OUT_UPD ( P_SOAP_ID  NUMBER,
                                P_REQUEST   VARCHAR2,
                                P_RESPONSE  VARCHAR2,
                                P_ERRORMSG VARCHAR2,
                                P_DATUM_REPONSE DATE,
                                P_URL  VARCHAR2,
                                P_METHODE VARCHAR2
                              );

PROCEDURE BEH_ADT_LOG_INS ( P_ADT_ID   NUMBER,
                            P_XML_OUT  CLOB,
                            P_DATUM    DATE,
                            P_ERRORMSG VARCHAR2
                          );

PROCEDURE BEH_ADT_LOG_UPD ( P_ADT_ID NUMBER,
                            P_XML_IN IN OUT CLOB
                          );

Procedure CDM_LOGERRORS( Perrormsg varchar2,
                         Perrorpos varchar2,
                         Pid Number,
                         Pid_From varchar2
                       );


END;
/

/
QUIT
