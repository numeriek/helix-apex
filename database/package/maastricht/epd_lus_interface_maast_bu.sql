CREATE OR REPLACE PACKAGE "HELIX"."EPD_LUS_INTERFACE_MAAST_BU" IS

/*******************************************************************************
    NAME:      KGC_ORU_MAASTRICHT
    PURPOSE:   Aanmaken XML bestanden voor SAP-LUS (Laboratorium-Uitslagen-Systeem van SAP)
               vanuit Helix-databse
               Onderdeel van de Opvragen Uitslagen Interface ("ORU Interface"),
               fase 1 (Afdeling "CYTO")

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.0        2012-11-05  W.A.Brouwer/Cap  Initiele versie
    1.2        2013-02-14  R Konings        Aanpassingen ORU Fase 2
    1.3        2013-10-23  R Konings        Aanpassingen i.v.m Embargo
*******************************************************************************/


----------------------------------------------------------------
-- Get_Oru_Verslagen: geef een xml met lijst met onderzoeken bij
--                    gegeven Patientnummer (KGC_Personen.Zisnr)
----------------------------------------------------------------
FUNCTION Get_Oru_Verslagen (P_ZisNr in varchar2)
  RETURN Clob;

--------------------------------------------------------------------------------
-- Get_Oru_Verslag_Detail: geef een xml met detail-gegevens bij een
--                         gegeven OnderzoekNummer (KGC_Onderzoeken.Onderzoeknr)
--------------------------------------------------------------------------------
FUNCTION Get_Oru_Verslag_Detail (P_Onderzoeknr in varchar2)
  RETURN Clob;

function GetInterne_Relatie(P_Pers_id Integer) return boolean;


Function GetOraReportLink(P_ongr_id Number, P_onde_id Number ) return Clob;

Function GetMonsterFractie(P_onde_Id Number, p_fractie boolean) return Clob;

Function GetEmbargo(p_ongr_id Number, p_input varchar2) return varchar2;
Function GetIndicatie(p_onde_ID number) return Clob;
Function GetRelatieGegevens(p_Rela_id Number)return Varchar2;
Function GetOnderzoekWijze(p_ONWY_cd Varchar2) Return Clob;
Function GetProtocol(p_onde_id Number) return Clob;
Function GetUitslagTeksten( p_onde_id Number,
                            p_ongr_id Number,
                            p_status varchar2,
                            p_datum_aut Date,
                            p_mede_id Number,
                            p_relatie varchar2
                          )
Return Clob;
 Function GetKopieHouders(puits_id integer) return clob;
Function GetIndicatiesDetail(p_onde_id Number) Return Clob;
Function ReplaceUitslag(Tekst in varchar2) return varchar2;







END EPD_LUS_INTERFACE_MAAST_BU;
/

/
QUIT
