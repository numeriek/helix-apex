CREATE OR REPLACE PACKAGE "HELIX"."BEH_HEBO"
is

  procedure update_hebo
  ( p_onde_id in kgc_onderzoeken.onde_id%type
  , p_indi_id in kgc_onderzoek_indicaties.indi_id%type
  );

  procedure check_opraken_hebonnummers;

end beh_hebo;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_HEBO"
is
  --
  -- Zodra er bij een pati�nt een onderzoek wordt aangemaakt met als indicatiecode 'BC1', 'BC2' of 'BC3',
  -- dient het pers_id bij het eerstvolgende vrije Hebonnummer te worden vastgelegd in tabel BEH_HEBON,
  -- evenals de datum van deze uitreiking. In de praktijk betekent dit dat de trigger op de tabel
  -- KGC_ONDERZOEK_INDICATIES moet komen.
  -- Voorwaarden: onderzoek is aangevraagd door interne counseler en onderzoekspersoon moet minimaal 18 jaar zijn.
  --
  procedure update_hebo( p_onde_id in kgc_onderzoeken.onde_id%type
                       , p_indi_id in kgc_onderzoek_indicaties.indi_id%type
                       )
  is
    cursor c_onde( b_onde_id in kgc_onderzoeken.onde_id%type
                 , b_indi_id in kgc_onderzoek_indicaties.indi_id%type
                 )
    is
      select onde.pers_id
      ,      onde.ongr_id
      ,      onde.rela_id
      ,      onde.onderzoekstype
      ,      pers.overleden
      ,      indi.code
      from   kgc_indicatie_teksten  indi
      ,      kgc_personen           pers
      ,      kgc_onderzoeken        onde
      where  onde.onde_id = b_onde_id
      and    pers.pers_id = onde.pers_id
      and    indi.indi_id = b_indi_id
    ;
    cursor c_hebo ( b_pers_id in kgc_personen.pers_id%type
                  )
    is
      select hebo.hebo_id
      from   beh_hebon hebo
      where  hebo.pers_id is null
      and    not exists                   -- Het pers_id moet uniek zijn.
               ( select null              -- Eenzelfde pers_id mag maar ��n hebonnummer krijgen.
                 from   beh_hebon hebo2
                 where  hebo2.pers_id = b_pers_id
               )
      order
      by     hebo.hebo_id asc
      for update of hebo.pers_id
    ;
    --
    r_onde_rec  c_onde%rowtype;
    pl_hebo_id  beh_hebon.hebo_id%type;
    pl_found    boolean;
    --
  begin
    if p_indi_id is not null
    then
      open  c_onde( p_onde_id
                  , p_indi_id
                  );
      fetch c_onde
      into  r_onde_rec;
      close c_onde;
      --
      if  r_onde_rec.ongr_id = kgc_ongr_00.id('DNA')
      and r_onde_rec.code in ('BC1','BC2','BC3')
      and r_onde_rec.onderzoekstype = 'D' -- Alleen onderzoeken van de afdeling DNA
      and mss_brie_enotificatie.is_interne_kg_rela(r_onde_rec.rela_id) = 'TRUE'  -- Onderzoek is aangevraagd door interne counseler
      and r_onde_rec.overleden = 'N' -- Persoon mag niet overleden zijn
      then
        open  c_hebo( r_onde_rec.pers_id
                    );
        fetch c_hebo
        into  pl_hebo_id;
        pl_found := c_hebo%found;
        --
        if pl_found
        then
          update beh_hebon hebo
          set    hebo.pers_id = r_onde_rec.pers_id
          ,      hebo.onde_id = p_onde_id
          ,      hebo.datum_uitgereikt = sysdate
          where  current of c_hebo;
          --
          -- Voer na een Hebon registratie een telling uit naar het aantal overgebleven vrije Hebonnummers. Als dat beneden een bepaald pijl
          -- komt dient een mail verstuurd te worden.
          --
          check_opraken_hebonnummers;
        else
          null;
        end if;
        close c_hebo;
      else
        null;
      end if;
    else  -- indien geen indicatie code gevuld hoeft er niets te gebeuren
      null;
    end if;
  end update_hebo;

  --
  -- Als er nog slechts een x-tal niet-uitgereikte Hebonnummers in de tabel BEH_HEBON voorkomen, moet er een email worden verzonden
  -- dat de Hebonnummers moeten worden aangevuld.
  --
  procedure check_opraken_hebonnummers
  is
    cursor c_hebo
    is
      select count(1)                                                      aantal_over
      ,      kgc_sypa_00.systeem_waarde('HEBON_GRENSAANT_STUREN_EMAIL')    grensaantal
      ,      kgc_sypa_00.systeem_waarde('HEBON_EMAILADRES_VAN')            emailadres_van
      ,      kgc_sypa_00.systeem_waarde('HEBON_EMAILADRES_AAN')            emailadres_aan
      from   beh_hebon hebo
      where  hebo.pers_id is null
    ;
    --
    r_hebo_rec c_hebo%rowtype;
    --
  begin
    open  c_hebo;
    fetch c_hebo
    into  r_hebo_rec;
    close c_hebo;
    --
    -- Als #niet uitgereikte Hebonnummers in de tabel <= sys.par. HEBON_GRENSAANT_STUREN_EMAIL, dan moet een mail gestuurd worden naar
    -- het emailadres wat in sys.par. HEBON_EMAILADRES_AAN staat met de mededeling dat de Hebonnummers moeten worden aangevuld.
    --
    if r_hebo_rec.aantal_over <= r_hebo_rec.grensaantal
    then
      mss_mail.post_mail( r_hebo_rec.emailadres_van
                        , r_hebo_rec.emailadres_aan
                        , 'Hebonnummers moeten worden aangevuld!'
                        , 'Hebonnummers moeten worden aangevuld. '||
                          'Er is/zijn er nog '||r_hebo_rec.aantal_over||' over.'
                        , TRUE -- HTML
                        );
    else
      null;
    end if;
  end check_opraken_hebonnummers;

end beh_hebo;
/

/
QUIT
