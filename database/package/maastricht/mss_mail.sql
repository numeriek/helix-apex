CREATE OR REPLACE PACKAGE "HELIX"."MSS_MAIL" IS
   PROCEDURE post_mail(p_van       IN VARCHAR2
                      ,p_aan       IN VARCHAR2
                      ,p_cc        IN VARCHAR2
                      ,p_onderwerp IN VARCHAR2
                      ,p_inhoud    IN VARCHAR2
                      ,p_html      IN BOOLEAN := FALSE);
   --
   PROCEDURE post_mail(p_van       IN VARCHAR2
                      ,p_aan       IN VARCHAR2
                      ,p_onderwerp IN VARCHAR2
                      ,p_inhoud    IN VARCHAR2
                      ,p_html      IN BOOLEAN := FALSE);
   --
   PROCEDURE post_mail_prog(p_van       IN VARCHAR2
                           ,p_aan       IN VARCHAR2
                           ,p_cc        IN VARCHAR2
                           ,p_onderwerp IN VARCHAR2
                           ,p_inhoud    IN VARCHAR2
                           ,p_html      IN BOOLEAN := FALSE);
END MSS_MAIL;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_MAIL" IS
   --
   PROCEDURE post_mail(p_van       IN VARCHAR2
                      ,p_aan       IN VARCHAR2
                      ,p_cc        IN VARCHAR2
                      ,p_onderwerp IN VARCHAR2
                      ,p_inhoud    IN VARCHAR2
                      ,p_html      IN BOOLEAN := FALSE
                      )
   IS
   BEGIN
      post_mail_prog(p_van
                    ,p_aan
                    ,p_cc
                    ,p_onderwerp
                    ,p_inhoud
                    ,p_html
                    );
   END post_mail;
   --
   PROCEDURE post_mail(p_van       IN VARCHAR2
                      ,p_aan       IN VARCHAR2
                      ,p_onderwerp IN VARCHAR2
                      ,p_inhoud    IN VARCHAR2
                      ,p_html      IN BOOLEAN := FALSE
                      )
   IS
   BEGIN
      post_mail_prog(p_van
                    ,p_aan
                    ,null
                    ,p_onderwerp
                    ,p_inhoud
                    ,p_html
                    );
   END post_mail;
   --
   PROCEDURE post_mail_prog(p_van       IN VARCHAR2
                           ,p_aan       IN VARCHAR2
                           ,p_cc        IN VARCHAR2
                           ,p_onderwerp IN VARCHAR2
                           ,p_inhoud    IN VARCHAR2
                           ,p_html      IN BOOLEAN := FALSE
                           )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;

      CURSOR c_sypa IS
         SELECT sypa.standaard_waarde
           FROM kgc_systeem_parameters sypa
          WHERE sypa.code = 'SMTP_GPR'
            AND sypa.standaard_waarde = 'J';

      r_sypa        c_sypa%ROWTYPE;
      v_emailserver VARCHAR2(30) := kgc_sypa_00.standaard_waarde(p_parameter_code => 'MAIL_SERVER'
                                                                ,p_kafd_id        => NULL
                                                                ,p_ongr_id        => NULL
                                                                ,p_mede_id        => NULL);
      v_port        NUMBER := 25;
      v_conn        utl_smtp.connection;
      v_crlf        VARCHAR2(2) := chr(13) || chr(10);
      v_msg         VARCHAR2(32767);
      v_smtp_gpr    BOOLEAN;

   BEGIN

      OPEN c_sypa;
      FETCH c_sypa
         INTO r_sypa;
      v_smtp_gpr := c_sypa%NOTFOUND;
      CLOSE c_sypa;

      IF v_smtp_gpr
      THEN
         v_conn := utl_smtp.open_connection(v_emailserver
                                           ,v_port);
         utl_smtp.helo(v_conn
                      ,v_emailserver);
         utl_smtp.mail(v_conn
                      ,p_van);
         utl_smtp.rcpt(v_conn
                      ,p_aan);
         if p_cc is not null
         then
           utl_smtp.rcpt(v_conn
                        ,p_cc);
         else
           null;
         end if;
         IF p_html
         THEN
            v_msg := 'MIME-Version: 1.0' || v_crlf || 'Content-type: text/html' || v_crlf;
         END IF;
         v_msg := v_msg ||
                    substr('Subject: ' || p_onderwerp || v_crlf ||
                           'Date: ' ||to_char(systimestamp,'dd mm yyyy hh24:mi:ss tzhtzm') || v_crlf ||
                           'From:'  || p_van || v_crlf ||
                           'To: '   || p_aan || v_crlf ||
                           case
                             when p_cc is null
                             then null
                             else 'CC: ' || p_cc || v_crlf
                           end ||
                           '' || v_crlf ||
                           p_inhoud
                          ,1,32766);
         utl_smtp.data(v_conn
                      ,v_msg);
         utl_smtp.quit(v_conn);

         /* NtB, 16-09-2009: code ge-inactiveerd, lokaal reactiveren, indien gewenst
         ELSE
            INSERT INTO hlx_email_afvang
               (van
               ,aan
               ,onderwerp
               ,message)
            VALUES
               (p_van
               ,p_aan
               ,p_onderwerp
               ,p_inhoud);
            COMMIT;
            */
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line(substr('SQLErrm ' || SQLERRM
                                    ,1
                                    ,255));
         RAISE;
   END post_mail_prog;
   --
END MSS_MAIL;
/

/
QUIT
