CREATE OR REPLACE PACKAGE "HELIX"."MSS_SAMENVOEGEN"
IS

  --
  -- Retourneer, als geconcateneerde string, de indicatiecodes die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_indi_codes ( p_onde_id in kgc_onderzoeken.onde_id%type
                           )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de indicatiecodes die horen bij monster p_mons_id
  --
  FUNCTION mons_indi_codes ( p_mons_id in kgc_monsters.mons_id%type
                           )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de opmerkingen bij de indicaties die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_indi_opmerkingen ( p_onde_id in kgc_onderzoeken.onde_id%type
                                 )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string met elke opmerking op een nieuwe regel, de opmerkingen bij de indicaties die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_indi_opm_regel ( p_onde_id in kgc_onderzoeken.onde_id%type
                               )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de fractienummers die horen bij onderzoek p_onde_id
  -- Voorwaarde is dat er meetwaarden bij geregistreed zijn
  --
  FUNCTION fractienr_met_meet ( p_onde_id in kgc_onderzoeken.onde_id%type
                              )
  RETURN VARCHAR2;
 --
  -- Retourneer, als geconcateneerde string, alle fractienummers die horen bij persoon p_pers_id.
  -- D.w.z. een opsomming van alle fractienummers (van DNA, Cyto en EMZ) die bij de persoon bekend zijn, gescheiden door een komma.
  --
  FUNCTION fractienr_bij_pers_id ( p_pers_id in kgc_personen.pers_id%type
                                 )
  RETURN VARCHAR2;
  --
  -- Retourneer, als geconcateneerde string, de monsternummers die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_monsternummers_cyto ( p_onde_id IN kgc_onderzoeken.onde_id%type
                                    )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de materiaalcodes van de monsters die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_mate_codes_cyto ( p_onde_id IN kgc_onderzoeken.onde_id%type
                           )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, namen van UV's en pathogene mutaties met daarachter
  -- tussen haakjes het type mutatie bij onderzoek p_onde_id
  --
  FUNCTION uv_pathogene_mut_dna ( p_onde_id in kgc_onderzoeken.onde_id%type
                           )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de verschillende onderzoeknummers van onderzoeken waarbij de gegeven persoon p_pers_id is betrokken
  --
  FUNCTION onbe_onderderzoeknr ( p_pers_id in kgc_personen.pers_id%type
                               , p_kafd_id in kgc_kgc_afdelingen.kafd_id%type
                               )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de verschillende familienummers voor de persoon in gegeven onde_id (theoretisch > 1)
  --
  FUNCTION familienummers ( p_onde_id in kgc_onderzoeken.onde_id%type
                          )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de genen die horen bij de indicatie omschrijvingen bij onderzoek p_onde_id
  --
  FUNCTION genen ( p_onde_id in kgc_onderzoeken.onde_id%type
                 )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de x-nrs die horen bij onderzoek p_onde_id
  -- en waarbij de pati�nt de adviesvrager is
  --
  FUNCTION xnrs ( p_onde_id in kgc_onderzoeken.onde_id%type
                )
  RETURN VARCHAR2;

  --
  -- Retourneer, als geconcateneerde string, de opgemaakte naam van de aanvrager (A), de kopiehouders (K) of allebei (B)
  -- die horen bij uitslag p_uits_id
  --
  FUNCTION naam_aanvr_kopieh ( p_uits_id      in kgc_uitslagen.uits_id%type
                             , p_aanv_of_koho in varchar2
                             )
  RETURN VARCHAR2;

END mss_samenvoegen;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_SAMENVOEGEN"
IS

  --
  -- Retourneer, als geconcateneerde string, de indicatiecodes die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_indi_codes ( p_onde_id in kgc_onderzoeken.onde_id%type
                           )
  RETURN VARCHAR2
  IS
    cursor c_indi( b_onde_id kgc_onderzoeken.onde_id%type
                 )
    is
      select indi.code
      from   kgc_indicatie_teksten     indi
      ,      kgc_onderzoek_indicaties  onin
      where  onin.onde_id = b_onde_id
      and    indi.indi_id = onin.indi_id
      order
      by     onin.volgorde asc
      ,      onin.onin_id  asc
    ;
    --
    pl_indi_codes varchar2(2048);
    --
  BEGIN
    pl_indi_codes := null;
    --
    for r_indi_rec in c_indi(p_onde_id)
    loop
      if (pl_indi_codes is null)
      then
        pl_indi_codes := r_indi_rec.code;
      else
        pl_indi_codes := pl_indi_codes||', '||r_indi_rec.code;
      end if;
    end loop;
    --
    return(pl_indi_codes);
    --
  END onde_indi_codes;

  --
  -- Retourneer, als geconcateneerde string, de indicatiecodes die horen bij monster p_mons_id
  --
  FUNCTION mons_indi_codes ( p_mons_id in kgc_monsters.mons_id%type
                           )
  RETURN VARCHAR2
  IS
    cursor c_indi( b_mons_id kgc_monsters.mons_id%type
                 )
    is
      select indi.code
      from   kgc_indicatie_teksten    indi
      ,      kgc_monster_indicaties   moin
      where  moin.mons_id = b_mons_id
      and    indi.indi_id = moin.indi_id
      order
      by     moin.moin_id  asc
    ;
    --
    pl_indi_codes varchar2(2048);
    --
  BEGIN
    pl_indi_codes := null;
    --
    for r_indi_rec in c_indi(p_mons_id)
    loop
      if (pl_indi_codes is null)
      then
        pl_indi_codes := r_indi_rec.code;
      else
        pl_indi_codes := pl_indi_codes||', '||r_indi_rec.code;
      end if;
    end loop;
    --
    return(pl_indi_codes);
    --
  END mons_indi_codes;

  --
  -- Retourneer, als geconcateneerde string, de opmerkingen bij de indicaties die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_indi_opmerkingen ( p_onde_id in kgc_onderzoeken.onde_id%type
                                 )
  RETURN VARCHAR2
  IS
    cursor c_indi( b_onde_id kgc_onderzoeken.onde_id%type
                 )
    IS
      select onin.opmerking
      from   kgc_indicatie_teksten     indi
      ,      kgc_onderzoek_indicaties  onin
      where  onin.onde_id = b_onde_id
      and    indi.indi_id = onin.indi_id
      order
      by     onin.volgorde asc
      ,      onin.onin_id  asc
    ;
    --
    pl_indi_opm varchar2(2048);
    --
  BEGIN
    --
    pl_indi_opm := null;
    --
    for r_indi_rec in c_indi(p_onde_id)
    loop
      if (pl_indi_opm is null)
      then
        pl_indi_opm := r_indi_rec.opmerking;
      else
        pl_indi_opm := pl_indi_opm||', '||r_indi_rec.opmerking;
      end if;
    end loop;
    --
    return(pl_indi_opm);
    --
  END onde_indi_opmerkingen;

  --
  -- Retourneer, als geconcateneerde string met elke opmerking op een nieuwe regel, de opmerkingen bij de indicaties die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_indi_opm_regel ( p_onde_id in kgc_onderzoeken.onde_id%type
                               )
  RETURN VARCHAR2
  IS
    cursor c_indi( b_onde_id kgc_onderzoeken.onde_id%type
                 )
    IS
      select onin.opmerking
      from   kgc_indicatie_teksten     indi
      ,      kgc_onderzoek_indicaties  onin
      where  onin.onde_id = b_onde_id
      and    indi.indi_id = onin.indi_id
      order
      by     onin.volgorde asc
      ,      onin.onin_id  asc
    ;
    --
    pl_indi_opm varchar2(2048);
    --
  BEGIN
    --
    pl_indi_opm := null;
    --
    for r_indi_rec in c_indi(p_onde_id)
    loop
      if (pl_indi_opm is null)
      then
        pl_indi_opm := r_indi_rec.opmerking;
      else
        pl_indi_opm := pl_indi_opm||chr(10)||r_indi_rec.opmerking;
      end if;
    end loop;
    --
    return(pl_indi_opm);
    --
  END onde_indi_opm_regel;


  --
  -- Retourneer, als geconcateneerde string, de fractienummers die horen bij onderzoek p_onde_id
  -- Voorwaarde is dat er meetwaarden bij geregistreed zijn
  --
  FUNCTION fractienr_met_meet ( p_onde_id in kgc_onderzoeken.onde_id%type
                              )
  RETURN VARCHAR2
  IS
    cursor c_frac( b_onde_id kgc_onderzoeken.onde_id%type
                 )
    is
      select distinct frac.fractienummer fractienummer
      from   bas_fracties  frac
      ,      bas_metingen  meti
      where  meti.onde_id = b_onde_id
      and    frac.frac_id = meti.frac_id
      and    exists
               ( select null
                 from   bas_meetwaarden meet
                 where  meet.meti_id = meti.meti_id
               )
      order
      by     frac.fractienummer
    ;
    --
    pl_fracnr varchar2(512);
    --
  BEGIN
    pl_fracnr := null;
    --
    for r_frac_rec in c_frac(p_onde_id)
    loop
      if (pl_fracnr is null)
      then
        pl_fracnr := r_frac_rec.fractienummer;
      else
        pl_fracnr := pl_fracnr||', '||r_frac_rec.fractienummer;
      end if;
    end loop;
    --
    return(pl_fracnr);
    --
  END fractienr_met_meet;
 --
  -- Retourneer, als geconcateneerde string, alle fractienummers die horen bij persoon p_pers_id.
  -- D.w.z. een opsomming van alle fractienummers (van DNA, Cyto en EMZ) die bij de persoon bekend zijn, gescheiden door een komma.
  --
  FUNCTION fractienr_bij_pers_id ( p_pers_id in kgc_personen.pers_id%type
                                 )
  RETURN VARCHAR2
  IS
    cursor c_frac( b_pers_id kgc_personen.pers_id%type
                 )
    is
      select distinct frac.fractienummer fractienummer
      from   bas_fracties  frac
      ,      kgc_monsters  mons
      where  mons.pers_id = b_pers_id
      and    frac.mons_id = mons.mons_id
      order
      by     frac.fractienummer
    ;
    --
    pl_fracnr varchar2(4000);
    --
  BEGIN
    pl_fracnr := null;
    --
    for r_frac_rec in c_frac(p_pers_id)
    loop
      if (pl_fracnr is null)
      then
        pl_fracnr := r_frac_rec.fractienummer;
      else
        pl_fracnr := pl_fracnr||', '||r_frac_rec.fractienummer;
      end if;
    end loop;
    --
    return(pl_fracnr);
    --
  END fractienr_bij_pers_id;
  --
  -- Retourneer, als geconcateneerde string, de monsternummers die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_monsternummers_cyto ( p_onde_id IN kgc_onderzoeken.onde_id%type
                                    )
  RETURN VARCHAR2
  IS
    cursor c_mons( b_onde_id kgc_onderzoeken.onde_id%type
                 )
    is
      select mons.monsternummer
      from   kgc_monsters            mons
      ,      kgc_onderzoek_monsters  onmo
      where  onmo.onde_id = b_onde_id
      and    mons.mons_id = onmo.mons_id
      and    exists
               ( select null
                 from   kgc_stoftestgroepen  stgr
                 ,      bas_metingen         meti
                 where  meti.onmo_id = onmo.onmo_id
                 and    stgr.stgr_id = meti.stgr_id
                 and    stgr.code in ('KARYO-PO','KARYO-PR','KARYO-TU', 'FISH-KARYO')
               )
      order
      by     mons.monsternummer
    ;
    --
    pl_mons_nummers varchar2(256);
    --
  BEGIN
    pl_mons_nummers := null;
    --
    for r_mons_rec in c_mons(p_onde_id)
    loop
      if (pl_mons_nummers is null)
      then
        pl_mons_nummers := r_mons_rec.monsternummer;
      else
        pl_mons_nummers := pl_mons_nummers||', '||r_mons_rec.monsternummer;
      end if;
    end loop;
    --
    return(pl_mons_nummers);
    --
  END onde_monsternummers_cyto;

  --
  -- Retourneer, als geconcateneerde string, de materiaalcodes van de monsters die horen bij onderzoek p_onde_id
  --
  FUNCTION onde_mate_codes_cyto ( p_onde_id IN kgc_onderzoeken.onde_id%type
                                )
  RETURN VARCHAR2
  IS
    cursor c_mate( b_onde_id kgc_onderzoeken.onde_id%type
                 )
    is
      select mate.code
      from   kgc_materialen          mate
      ,      kgc_monsters            mons
      ,      kgc_onderzoek_monsters  onmo
      where  onmo.onde_id = b_onde_id
      and    mons.mons_id = onmo.mons_id
      and    mate.mate_id = mons.mate_id
      and    exists
               ( select null
                 from   kgc_stoftestgroepen  stgr
                 ,      bas_metingen         meti
                 where  meti.onmo_id = onmo.onmo_id
                 and    stgr.stgr_id = meti.stgr_id
                 and    stgr.code in ('KARYO-PO','KARYO-PR','KARYO-TU', 'FISH-KARYO')
               )
      order
      by     mons.monsternummer
    ;
    --
    pl_mate_codes varchar2(256);
    --
  BEGIN
    pl_mate_codes := null;
    --
    for r_mate_rec in c_mate(p_onde_id)
    loop
      if (pl_mate_codes is null)
      then
        pl_mate_codes := r_mate_rec.code;
      else
        pl_mate_codes := pl_mate_codes||', '||r_mate_rec.code;
      end if;
    end loop;
    --
    return(pl_mate_codes);
    --
  END onde_mate_codes_cyto;

  --
  -- Retourneer, als geconcateneerde string, namen van UV's en pathogene mutaties met daarachter
  -- tussen haakjes het type mutatie bij onderzoek p_onde_id
  --
  FUNCTION uv_pathogene_mut_dna ( p_onde_id in kgc_onderzoeken.onde_id%type
                                )
  RETURN VARCHAR2
  IS
    cursor c_mut ( b_onde_id in kgc_onderzoeken.onde_id%type
                 )
    is
      select distinct
             mdet_naam.waarde||'('||mdet_soort.waarde||')' mutatie
      from   bas_meetwaarde_details mdet_soort
      ,      bas_meetwaarde_details mdet_naam
      ,      bas_meetwaarden        meet
      ,      bas_metingen           meti
      where  meti.onde_id = b_onde_id
      and    meet.meti_id = meti.meti_id
      and    exists
               ( select null
                 from   bas_meetwaarde_details mdet2
                 where  mdet2.meet_id = meet.meet_id
                 and    mdet2.prompt in ('Soort afw.','Soort afwijking (2e)','Soort afwijking (3e)')
                 and    mdet2.waarde in ('UV','UV1','UV2','UV3','Mut','RecPath')
               )
      and    mdet_naam.meet_id = meet.meet_id
      and    mdet_soort.meet_id = meet.meet_id
      and    ( (    mdet_naam.prompt = 'Naam vd afw.'
                and mdet_naam.waarde is not null
                and mdet_soort.prompt = 'Soort afw.'
                and mdet_soort.waarde in ('UV','UV1','UV2','UV3','Mut','RecPath')
               )
               or
               (    mdet_naam.prompt = 'Naam van de 2e afwijking'
                and mdet_naam.waarde is not null
                and mdet_soort.prompt = 'Soort afwijking (2e)'
                and mdet_soort.waarde in ('UV','UV1','UV2','UV3','Mut','RecPath')
               )
               or
               (    mdet_naam.prompt = 'Naam van de 3e afwijking'
                and mdet_naam.waarde is not null
                and mdet_soort.prompt = 'Soort afwijking (3e)'
                and mdet_soort.waarde in ('UV','UV1','UV2','UV3','Mut','RecPath')
               )
             )
    ;
    --
    pl_mutaties varchar2(512);
    --
  BEGIN
    pl_mutaties := null;
    --
    for r_mut_rec in c_mut(p_onde_id)
    loop
      if (pl_mutaties is null)
      then
        pl_mutaties := r_mut_rec.mutatie;
      else
        pl_mutaties := pl_mutaties||', '||r_mut_rec.mutatie;
      end if;
    end loop;
    --
    return(pl_mutaties);
    --
  END uv_pathogene_mut_dna;

  --
  -- Retourneer, als geconcateneerde string, de verschillende onderzoeknummers van onderzoeken waarbij de gegeven persoon p_pers_id is betrokken
  --
  FUNCTION onbe_onderderzoeknr ( p_pers_id in kgc_personen.pers_id%type
                               , p_kafd_id in kgc_kgc_afdelingen.kafd_id%type
                               )
  RETURN VARCHAR2
  IS
    cursor c_onbe( b_pers_id kgc_personen.pers_id%type
                 , b_kafd_id kgc_kgc_afdelingen.kafd_id%type
                 )
    is
      select distinct onde.onderzoeknr
      from   kgc_onderzoeken            onde
      ,      kgc_onderzoek_betrokkenen  onbe
      where  onbe.pers_id = b_pers_id
      and    onde.onde_id = onbe.onde_id
      and    onde.kafd_id = b_kafd_id
      order
      by     onde.onderzoeknr asc
    ;
    --
    pl_onderzoeknr varchar2(2048);
    --
  BEGIN
    pl_onderzoeknr := null;
    --
    for r_onbe_rec in c_onbe( p_pers_id
                            , p_kafd_id
                            )
    loop
      if (pl_onderzoeknr is null)
      then
        pl_onderzoeknr := r_onbe_rec.onderzoeknr;
      else
        pl_onderzoeknr := pl_onderzoeknr||', '||r_onbe_rec.onderzoeknr;
      end if;
    end loop;
    --
    return(pl_onderzoeknr);
    --
  END onbe_onderderzoeknr;

  --
  -- Retourneer, als geconcateneerde string, de verschillende familienummers voor de persoon in gegeven onde_id (theoretisch > 1)
  --
  FUNCTION familienummers ( p_onde_id in kgc_onderzoeken.onde_id%type
                          )
  RETURN VARCHAR2
  IS
    cursor c_fami ( b_onde_id in kgc_personen.pers_id%type
                  )
    is
      select fami.familienummer
      from   kgc_families       fami
      ,      kgc_familie_leden  fale
      ,      kgc_onderzoeken    onde
      where  onde.onde_id = b_onde_id
      and    fale.pers_id = onde.pers_id
      and    fami.fami_id = fale.fami_id
    ;
    --
    pl_familienr varchar2(100);
    --
  BEGIN
    pl_familienr := null;
    --
    for r_fami_rec in c_fami( p_onde_id
                            )
    loop
      if (pl_familienr is null)
      then
        pl_familienr := r_fami_rec.familienummer;
      else
        pl_familienr := pl_familienr||', '||r_fami_rec.familienummer;
      end if;
    end loop;
    --
    return(pl_familienr);
    --
  END familienummers;

  --
  -- Retourneer, als geconcateneerde string, de genen die horen bij de indicatie omschrijvingen bij onderzoek p_onde_id
  --
  FUNCTION genen ( p_onde_id in kgc_onderzoeken.onde_id%type
                 )
  RETURN VARCHAR2
  IS
    cursor c_genen( b_onde_id kgc_onderzoeken.onde_id%type
                  )
    is
      select atwa.waarde gen
      from   kgc_attribuut_waarden     atwa
      ,      kgc_attributen            attr
      ,      kgc_indicatie_teksten     indi
      ,      kgc_onderzoek_indicaties  onin
      where  onin.onde_id = b_onde_id
      and    indi.indi_id = onin.indi_id
      and    attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
      and    attr.code = 'GEN'
      and    atwa.attr_id = attr.attr_id
      and    id = indi.indi_id
      order
      by     onin.volgorde asc
      ,      onin.onin_id  asc
    ;
    --
    pl_genen varchar2(2048);
    --
  BEGIN
    pl_genen := null;
    --
    for r_gen_rec in c_genen(p_onde_id)
    loop
      if (pl_genen is null)
      then
        pl_genen := r_gen_rec.gen;
      else
        pl_genen := pl_genen||', '||r_gen_rec.gen;
      end if;
    end loop;
    --
    return(pl_genen);
    --
  END genen;

  --
  -- Retourneer, als geconcateneerde string, de x-nrs die horen bij onderzoek p_onde_id
  -- en waarbij de pati�nt de adviesvrager is
  --
  FUNCTION xnrs ( p_onde_id in kgc_onderzoeken.onde_id%type
                )
  RETURN VARCHAR2
  IS
    cursor c_xnrs( b_onde_id kgc_onderzoeken.onde_id%type
                 )
    is
      select onde_x.onderzoeknr xnr
      from   kgc_onderzoeken           onde_x
      ,      kgc_onderzoek_betrokkenen onbe
      ,      kgc_onderzoeken           onde
      where  onde.onde_id = b_onde_id
      and    onbe.pers_id = onde.pers_id
      and    onbe.rol like 'A%'  -- pati�nt is de adviesvrager
      and    onde_x.onde_id = onbe.onde_id
      and    onde_x.kafd_id = kgc_kafd_00.id('COUN')
      order
      by     onde_x.onderzoeknr  asc
    ;
    --
    pl_xnrs varchar2(2048);
    --
  BEGIN
    pl_xnrs := null;
    --
    for r_xnrs_rec in c_xnrs(p_onde_id)
    loop
      if (pl_xnrs is null)
      then
        pl_xnrs := r_xnrs_rec.xnr;
      else
        pl_xnrs := pl_xnrs||', '||r_xnrs_rec.xnr;
      end if;
    end loop;
    --
    return(pl_xnrs);
    --
  END xnrs;

  --
  -- Retourneer, als geconcateneerde string, de opgemaakte naam van de aanvrager (A), de kopiehouders (K) of allebei (B)
  -- die horen bij uitslag p_uits_id
  --
  FUNCTION naam_aanvr_kopieh ( p_uits_id      in kgc_uitslagen.uits_id%type
                             , p_aanv_of_koho in varchar2
                             )
  RETURN VARCHAR2
  IS
    cursor c_aanv ( b_uits_id in kgc_uitslagen.uits_id%type
                  )
    is
      select rela.aanspreken aanspreken
      from   kgc_relaties   rela
      ,      kgc_uitslagen  uits
      where  uits.uits_id = b_uits_id
      and    rela.rela_id = uits.rela_id
    ;
    --
    cursor c_koho ( b_uits_id in kgc_uitslagen.uits_id%type
                  )
    is
      select rela.aanspreken aanspreken
      from   kgc_relaties      rela
      ,      kgc_kopiehouders  koho
      where  koho.uits_id = b_uits_id
      and    rela.rela_id = koho.rela_id
    ;
    --
    pl_naam_aanvr   varchar2(500);
    pl_naam_kopieh  varchar2(500);
    pl_teller       number(5);
    --
  BEGIN
    if (p_aanv_of_koho = 'A')
    or (p_aanv_of_koho = 'B')
    then
      open  c_aanv( p_uits_id
                  );
      fetch c_aanv
      into  pl_naam_aanvr;
      close c_aanv;
    else
      null;
    end if;
    --
    if (p_aanv_of_koho = 'K')
    or (p_aanv_of_koho = 'B')
    then
      pl_teller := 0;
      for r_koho_rec in c_koho ( p_uits_id
                               )
      loop
        if  pl_teller = 0
        then
          pl_naam_kopieh := r_koho_rec.aanspreken;
        else
          pl_naam_kopieh := pl_naam_kopieh||', '||r_koho_rec.aanspreken;
        end if;
        pl_teller := pl_teller + 1;
      end loop;
    else
      null;
    end if;
    --
    if p_aanv_of_koho = 'A'
    then
      return pl_naam_aanvr;
    elsif p_aanv_of_koho = 'K'
    then
      return pl_naam_kopieh;
    elsif p_aanv_of_koho = 'B'
    then
       if pl_teller = 0
       then
         return pl_naam_aanvr;
       else
         return pl_naam_aanvr||' ('||pl_naam_kopieh||')';
       end if;
    else
      return null;
    end if;
  END naam_aanvr_kopieh;

END mss_samenvoegen;
/

/
QUIT
