CREATE OR REPLACE PACKAGE "HELIX"."BEH_XML_DOC"
IS
  PROCEDURE obmg_001 (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );

  PROCEDURE gesp_kg_ondepers (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );

  PROCEDURE gesp_kg_gesppers (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );

  PROCEDURE gebe_kg_pers (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );

  PROCEDURE uits_cyto (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );


 PROCEDURE vtbrf_dna (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  );

  PROCEDURE uits_met (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
   );

  PROCEDURE uits_met_tpmt (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
   );

  FUNCTION persoon_info_cyto(
       p_mons_id IN NUMBER   := NULL
     , p_onde_id IN NUMBER   := NULL
     , p_pers_id IN NUMBER   := NULL
     , p_lengte  IN VARCHAR2 := NULL
   )RETURN VARCHAR2;

END Beh_Xml_Doc;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_XML_DOC"
IS
   /******************************************************************************
     NAME:       beh_xml_doc
     PURPOSE:    Aanmaken xml-structuur tbv documenten (word, pdf)

     REVISIONS:
     Ver        Date         Author    Description
     ---------  ----------   --------  ----------------------------------------------------------------------------------------------------------------------
     2.5        06-12-2011   MWO       Tbv H201105-002 (emailnotificatie) is in procedure uits_cyto intern_tag toegevoegd en
                                       Tbv H201103-002 (kopiebrieven laten vervallen) is in procedure uits_cyto 1 kopie select uit cursor c_uits_koho gehaald
     2.4        07-06-2010   MWO       In uits_met in cursor c_n_urine_meti aan kolom mate_omschrijving het monsternummer toegevoegd
                                       en de sortering aangepast van 6 asc,2 desc,7 asc naar 6 asc,3 asc,7 asc
     2.3        07-05-2010   MWO       Ivm grootte vd brieven bijlagen tussen <metis< ? >metis> niet toevoegen voor kopiehouders in uits_met
     2.2        31-07-2009   RKO       Uitslag Cyto aangepast, afdeling toegevoegd
     2.1        31-07-2009   RKO       Vertragings brief DNA toegevoegd
     2          22-05-2007   RDE       Toegevoegd: uits_cyto
     1.0        15-05-2006   RDE       1. Aangemaakt
  ******************************************************************************/


FUNCTION toxml(p_tekst VARCHAR2) RETURN VARCHAR2 IS
BEGIN
  RETURN Kgc_Xml_00.neem_letterlijk(p_tekst);
END;

PROCEDURE uitv_kopiehouders
( p_uits_id          IN     NUMBER
, p_rela_id_adr      IN     NUMBER
, p_rela_id_uits     IN     NUMBER
, p_xml_doc          IN OUT CLOB
) IS
  CURSOR c_koho (p_uits_id IN NUMBER) IS
    SELECT NVL(uitv.rela_id, koho.rela_id) rela_id_adr
    FROM   kgc_uitslagbrief_vw uitv
    ,      KGC_KOPIEHOUDERS koho
    WHERE  uitv.uits_id = p_uits_id
    AND    uitv.koho_id = koho.koho_id (+)
    ORDER BY DECODE(uitv.KOHO_ID,NULL,9,1) DESC
    ;
  CURSOR rela_cur (p_rela_id number)
  IS
    SELECT rela.aanspreken
    ,      rela.adres
    ,      rela.postcode
    ,      rela.woonplaats
    ,      rela.land
    ,      rela.inst_id
    ,      rela.afdg_id
    ,      rela.interne_post
    ,      inst.naam inst_naam
    FROM   kgc_relaties rela
    ,      kgc_instellingen inst
    WHERE  rela.rela_id = p_rela_id
    AND    rela.inst_id = inst.inst_id (+)
    ;
  rela_rec_null rela_cur%rowtype;
  rela_rec rela_cur%rowtype;
  v_count_cc               NUMBER;
  v_toon_kohos             BOOLEAN;
  v_tekst                  VARCHAR2(2000);
  v_nl                     VARCHAR2(2) := CHR(10);
  v_adres                  VARCHAR2(2000);
BEGIN
    dbms_lob.writeappend (p_xml_doc
                         ,LENGTH ('<kohos>')
                         ,'<kohos>'
                         );
    v_toon_kohos := FALSE;
    FOR r_koho IN c_koho(p_uits_id) LOOP
      IF c_koho%ROWCOUNT > 1 THEN -- in de view krijg je orig + koho's!
        v_toon_kohos := TRUE;
        EXIT;
      END IF;
    END LOOP;
    IF v_toon_kohos THEN
      v_count_cc := 0;
      FOR r_koho IN c_koho(p_uits_id) LOOP
        -- igv koho = rela_id_uitslag: alle kopiehouders
        -- anders: alleen origineel
        IF (p_rela_id_uits = p_rela_id_adr AND -- brief aan aanvrager
            r_koho.rela_id_adr <> p_rela_id_uits  -- alleen kopiehouders
           ) OR
           (p_rela_id_uits <> p_rela_id_adr AND -- brief aan kopiehouders
            r_koho.rela_id_adr = p_rela_id_uits  -- alleen aanvrager
           )
        THEN
          dbms_lob.writeappend (p_xml_doc
                               ,LENGTH ('<koho>')
                               ,'<koho>'
                               );
          -- type: Origineel of CC of leeg
          IF r_koho.rela_id_adr = p_rela_id_uits THEN
            v_tekst := 'Origineel:';
          ELSIF v_count_cc = 0 THEN
            v_tekst := 'CC:';
            v_count_cc := v_count_cc + 1;
          ELSE
            v_tekst := NULL;
          END IF;
          Kgc_Xml_00.voegtoe_xml_regel (p_xml_clob      => p_xml_doc
                                       ,p_tag           => 'type'
                                       ,p_xml_regel     => v_tekst
                                       );
          -- rela-gegevens (naam, instelling)
          rela_rec := rela_rec_null;
          OPEN  rela_cur (p_rela_id => r_koho.rela_id_adr);
          FETCH rela_cur
          INTO  rela_rec;
          CLOSE rela_cur;
          v_adres := rela_rec.aanspreken;
          IF NVL(LENGTH(rela_rec.inst_naam), 0) > 0 THEN
            v_adres := v_adres || ', ' || rela_rec.inst_naam;
          END IF;
          v_adres := REPLACE(v_adres, CHR(10), ' ');
          Kgc_Xml_00.voegtoe_xml_regel
                             (p_xml_clob      => p_xml_doc
                             ,p_tag           => 'koho_naw'
                             ,p_xml_regel     => toxml(v_adres)
                             );
          dbms_lob.writeappend (p_xml_doc
                               ,LENGTH ('</koho>')
                               ,'</koho>'
                               );
        END IF;
      END LOOP;
    END IF;
    dbms_lob.writeappend (p_xml_doc
                         ,LENGTH ('</kohos>')
                         ,'</kohos>'
                         );
END uitv_kopiehouders;

PROCEDURE uitv_kopiehouders_cyto
( p_uits_id          IN     NUMBER
, p_rela_id_adr      IN     NUMBER
, p_rela_id_uits     IN     NUMBER
, p_xml_doc          IN OUT CLOB
) IS
  CURSOR c_koho (p_uits_id IN NUMBER) IS
    SELECT NVL(uitv.rela_id, koho.rela_id) rela_id_adr
    FROM   kgc_uitslagbrief_vw uitv
    ,      KGC_KOPIEHOUDERS koho
    WHERE  uitv.uits_id = p_uits_id
    AND    uitv.koho_id = koho.koho_id (+)
    ORDER BY DECODE(uitv.KOHO_ID,NULL,9,1) DESC
    ;
  CURSOR rela_cur (p_rela_id number)
  IS
    SELECT rela.aanspreken
    ,      rela.adres
    ,      rela.postcode
    ,      rela.woonplaats
    ,      rela.land
    ,      rela.inst_id
    ,      rela.afdg_id
    ,      rela.interne_post
    ,      inst.naam inst_naam
    ,      afd.omschrijving
    FROM   kgc_relaties rela
    ,      kgc_instellingen inst
    ,      kgc_afdelingen afd
    WHERE  rela.rela_id = p_rela_id
    AND    rela.inst_id = inst.inst_id (+)
    and    rela.afdg_id = afd.afdg_id(+);

  rela_rec_null rela_cur%rowtype;
  rela_rec rela_cur%rowtype;
  v_count_cc               NUMBER;
  v_toon_kohos             BOOLEAN;
  v_tekst                  VARCHAR2(2000);
  v_nl                     VARCHAR2(2) := CHR(10);
  v_adres                  VARCHAR2(2000);
BEGIN
    dbms_lob.writeappend (p_xml_doc
                         ,LENGTH ('<kohos>')
                         ,'<kohos>'
                         );
    v_toon_kohos := FALSE;
    FOR r_koho IN c_koho(p_uits_id) LOOP
      IF c_koho%ROWCOUNT > 1 THEN -- in de view krijg je orig + koho's!
        v_toon_kohos := TRUE;
        EXIT;
      END IF;
    END LOOP;
    IF v_toon_kohos THEN
      v_count_cc := 0;
      FOR r_koho IN c_koho(p_uits_id) LOOP
        -- igv koho = rela_id_uitslag: alle kopiehouders
        -- anders: alleen origineel
        IF (p_rela_id_uits = p_rela_id_adr AND -- brief aan aanvrager
            r_koho.rela_id_adr <> p_rela_id_uits  -- alleen kopiehouders
           ) OR
           (p_rela_id_uits <> p_rela_id_adr AND -- brief aan kopiehouders
            r_koho.rela_id_adr = p_rela_id_uits  -- alleen aanvrager
           )
        THEN
          dbms_lob.writeappend (p_xml_doc
                               ,LENGTH ('<koho>')
                               ,'<koho>'
                               );
          -- type: Origineel of CC of leeg
          IF r_koho.rela_id_adr = p_rela_id_uits THEN
            v_tekst := 'Origineel:';
          ELSIF v_count_cc = 0 THEN
            v_tekst := 'CC:';
            v_count_cc := v_count_cc + 1;
          ELSE
            v_tekst := NULL;
          END IF;
          Kgc_Xml_00.voegtoe_xml_regel (p_xml_clob      => p_xml_doc
                                       ,p_tag           => 'type'
                                       ,p_xml_regel     => v_tekst
                                       );
          -- rela-gegevens (naam, instelling)
          rela_rec := rela_rec_null;
          OPEN  rela_cur (p_rela_id => r_koho.rela_id_adr);
          FETCH rela_cur
          INTO  rela_rec;
          CLOSE rela_cur;
          v_adres := rela_rec.aanspreken;
          -- Afdeling teoegevoegd door RKO
          IF NVL(LENGTH(rela_rec.omschrijving), 0) > 0 THEN
            v_adres := v_adres || ', ' || rela_rec.omschrijving;
          END IF;
          IF NVL(LENGTH(rela_rec.inst_naam), 0) > 0 THEN

            v_adres := v_adres || ', '|| rela_rec.inst_naam;
          END IF;
          v_adres := REPLACE(v_adres, CHR(10), ' ');
          Kgc_Xml_00.voegtoe_xml_regel
                             (p_xml_clob      => p_xml_doc
                             ,p_tag           => 'koho_naw'
                             ,p_xml_regel     => toxml(v_adres)
                             );
          dbms_lob.writeappend (p_xml_doc
                               ,LENGTH ('</koho>')
                               ,'</koho>'
                               );
        END IF;
      END LOOP;
    END IF;
    dbms_lob.writeappend (p_xml_doc
                         ,LENGTH ('</kohos>')
                         ,'</kohos>'
                         );
END uitv_kopiehouders_cyto;

PROCEDURE obmg_001 (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  )
  IS
    v_rela_adres             VARCHAR2 (4000);
    v_pers_adres             VARCHAR2 (4000);
    v_xml_doc                CLOB;
    v_xml_regel              Kgc_Xml_00.t_xml_regel;
    v_xml_regel_collection   Kgc_Xml_00.t_xml_regel_collection;
    v_datum_print            DATE                                := SYSDATE;
    v_nl            CONSTANT VARCHAR2 (2)                        := CHR (10);
    v_sql                    VARCHAR2 (1000);
    c_xvvz                   Kgc_Bouwsteen_00.t_bouwsteen_cursor;
    r_xvvz                   kgc_xml_verzoek_vw%ROWTYPE;
  BEGIN
    -- hoofdselectie

    Kgc_Xml_00.init;

    OPEN c_xvvz FOR 'SELECT * FROM ' || p_view_name || ' ' || p_where_clause;

    LOOP
      FETCH c_xvvz
       INTO r_xvvz;

      EXIT WHEN c_xvvz%NOTFOUND;

    -- maak nieuw xml document aan
      v_xml_doc := Kgc_Xml_00.nieuwe_xml (p_naam => 'inlichtingenbrief');

      Kgc_Xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'datum_print'
                                   ,p_xml_regel     => TO_CHAR (v_datum_print
                                                               ,'DD-MM-YYYY')
                                   );
      -- haal obmg gegevens, via bouwsteen
      v_xml_regel_collection :=
        Kgc_Bouwsteen_00.bouwsteen (p_bouwsteen     => 'KGC_ONBE_MED_GEGEVENS'
                                   ,p_kolommen      => '*'
                                   ,p_where         =>    'obmg_id = '
                                                       || TO_CHAR
                                                               (r_xvvz.obmg_id)
                                   ,p_prefix        => 'obmg'
                                   );
      Kgc_Xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- haal onderzoeks gegevens, via bouwsteen
      v_xml_regel_collection :=
        Kgc_Bouwsteen_00.bouwsteen (p_bouwsteen     => 'KGC_XML_ONDERZOEKEN_VW'
                                   ,p_kolommen      => 'onderzoeknr, datum_binnen'
                                   ,p_where         =>    'onde_id = '
                                                       || TO_CHAR
                                                               (r_xvvz.onde_id)
                                   ,p_prefix        => 'onde'
                                   );
      Kgc_Xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- persoon
      v_xml_regel_collection :=
        Kgc_Bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_XML_PERSOON_VW'
          ,p_kolommen      => 'aanspreken, geboortedatum, geslacht, overlijdensdatum'
          ,p_where         => 'pers_id = ' || TO_CHAR (r_xvvz.pers_id)
          ,p_prefix        => 'pers'
          );
      Kgc_Xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- persoon adres
      v_pers_adres := Kgc_Adres_00.persoon (r_xvvz.pers_id, 'N');
      Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'pers_adres'
                         ,p_xml_regel     =>    v_nl
                                             || Kgc_Xml_00.cr_naar_tags
                                                                 (v_pers_adres)
                         );
      -- pers familienummers
      Kgc_Xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'pers_famnrs'
                                   ,p_xml_regel     => Kgc_Pers_00.families(r_xvvz.pers_id, 'K')
                                   );
      -- counselor
      v_xml_regel_collection :=
        Kgc_Bouwsteen_00.bouwsteen
                              (p_bouwsteen     => 'KGC_MEDEWERKERS'
                              ,p_kolommen      => 'formele_naam, formele_naam as formele_naam2, naam, naam as naam2'
                              ,p_where         =>    'mede_id = '
                                                  || TO_CHAR
                                                       (r_xvvz.mede_id_beoordelaar)
                              ,p_prefix        => 'mede'
                              );
      Kgc_Xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- functie counselor
      v_xml_regel_collection :=
        Kgc_Bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_MEDE_KAFD_VW'
          ,p_kolommen      => '*'
          ,p_where         =>    'mede_id = '
                              || TO_CHAR(r_xvvz.mede_id_beoordelaar)
                              || ' and kafd_id = (select kafd_id from kgc_onderzoeken where onde_id = '
                              || TO_CHAR(r_xvvz.onde_id)
                              || ')'
          ,p_prefix        => 'meka'
          );
      Kgc_Xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- relatie  adres
      v_rela_adres := Kgc_Adres_00.relatie (r_xvvz.rela_id, 'J');
      Kgc_Xml_00.voegtoe_xml_regel
                       (p_xml_clob      => v_xml_doc
                       ,p_tag           => 'rela_adres'
                       ,p_xml_regel     =>    v_nl
                                           || Kgc_Xml_00.cr_naar_tags
                                                                 (v_rela_adres)
                                           || v_nl
                       );
      -- sluit
      Kgc_Xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                           ,p_naam         => 'inlichtingenbrief');

      Kgc_Xml_00.voegtoe_xml_doc ( p_xml_doc   =>  v_xml_doc);


    END LOOP;

    CLOSE c_xvvz;
  END obmg_001;


  /* uitnodigingsbrief: voornamlijk voor maastricht!!! */
  PROCEDURE uitnodigingsbrief
  ( p_gesp_id          IN   NUMBER
  , p_pers_ids         IN   VARCHAR -- 1 of meerdere pers_id's, gescheiden door een komma
                                    -- tabletype is beter, maar deze moet dan globaal - niet fijn
  , p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  , p_xml_doc          IN OUT CLOB
  ) IS
    TYPE t_pers_id_tab IS TABLE OF NUMBER;
    CURSOR c_uitn IS
      SELECT gesp.onde_id
      ,      gesp.ongr_id
      ,      gesp.mede_id
      ,      gesp.loca_id
      ,      onde.kafd_id
      FROM   KGC_GESPREKKEN gesp
      ,      KGC_ONDERZOEKEN onde
      WHERE  gesp.gesp_id = p_gesp_id
      AND    gesp.onde_id = onde.onde_id
      ;
    r_uitn c_uitn%ROWTYPE;
    CURSOR c_ramo (b_brty_id IN NUMBER) IS
      SELECT ramo.ramo_id
      FROM   KGC_RAPPORT_MODULES ramo
      ,      KGC_BRIEFTYPES brty
      WHERE  brty.brty_id = b_brty_id
      AND    brty.ramo_id = ramo.ramo_id
      ;
    CURSOR c_noti
    ( p_entiteit VARCHAR2
    , p_id       NUMBER
    , p_code     VARCHAR2
    ) IS
      SELECT omschrijving
      FROM   KGC_NOTITIES noti
      WHERE  noti.entiteit = p_entiteit
      AND    noti.ID = p_id
      AND    noti.code = p_code
      ;
    r_noti c_noti%ROWTYPE;
    CURSOR c_pers (p_pers_id NUMBER) IS
      SELECT pers.aanspreken
      ,      pers.geslacht
      ,      MONTHS_BETWEEN(SYSDATE, pers.geboortedatum) leeftijd_mnd
      FROM   KGC_PERSONEN pers
      WHERE  pers.pers_id = p_pers_id
      ;
    r_pers c_pers%ROWTYPE;
    v_pers_id_tab            t_pers_id_tab := t_pers_id_tab();
    v_pers_ids               VARCHAR2 (2000) := p_pers_ids;
    v_pers_id_minderj        NUMBER;
    v_adressering            VARCHAR2(2000);
    v_aanhef                 VARCHAR2(100);
    v_pers_adres             VARCHAR2 (4000);
    v_xml_regel_collection   Kgc_Xml_00.t_xml_regel_collection;
    v_datum_print            DATE                                := SYSDATE;
    v_nl            CONSTANT VARCHAR2 (2)                        := CHR (10);
    v_ramo_id                NUMBER;
    v_brty_id                NUMBER;
    v_attr_waarde            VARCHAR2(100);
    v_pos                    NUMBER;
    v_stap                   VARCHAR2(100) := 'start';
  BEGIN
    -- p_pers_ids naar tabel:
    v_stap := 'pid2tab';
    WHILE NVL(LENGTH(v_pers_ids), 0) > 0 LOOP
      v_pos := INSTR(v_pers_ids, ',');
      IF v_pos = 0 THEN -- laatste
        v_pers_id_tab.EXTEND(1);
        v_pers_id_tab(v_pers_id_tab.COUNT) := TO_NUMBER(v_pers_ids);
        EXIT;
      ELSE
        v_pers_id_tab.EXTEND(1);
        v_pers_id_tab(v_pers_id_tab.COUNT) := TO_NUMBER(SUBSTR(v_pers_ids, 1, v_pos - 1));
        v_pers_ids := LTRIM(RTRIM(SUBSTR(v_pers_ids, v_pos + 1)));
      END IF;
    END LOOP;
    IF v_pers_id_tab.COUNT = 0 THEN
      RAISE_APPLICATION_ERROR(-20000, 'beh_temp.uitnodigingsbrief: geen pers_id meegegeven!');
    END IF;
    -- zit er een minderjarige tussen? Bepalend voor adressering + aanhef!
    v_stap := 'minderj_bep';
    FOR i IN v_pers_id_tab.FIRST..v_pers_id_tab.LAST LOOP
      OPEN c_pers(v_pers_id_tab(i));
      FETCH c_pers INTO r_pers;
      IF r_pers.leeftijd_mnd < 216 THEN
        v_pers_id_minderj := v_pers_id_tab(i);
        v_adressering := 'Aan de ouders van ' || r_pers.aanspreken;
        v_aanhef      := 'Geachte heer en/of mevrouw';
        CLOSE c_pers;
        EXIT;
      END IF;
      IF i > 1 THEN
        v_adressering := v_adressering || CHR(10);
      END IF;
      IF r_pers.geslacht = 'V' THEN
        v_adressering := v_adressering || 'Mw. ' || r_pers.aanspreken;
        v_aanhef := 'Geachte mevrouw';
      ELSIF r_pers.geslacht = 'M' THEN
        v_adressering := v_adressering || 'Dhr. ' || r_pers.aanspreken;
        v_aanhef := 'Geachte heer';
      ELSE
        v_adressering := v_adressering || r_pers.aanspreken;
        v_aanhef := 'Geachte heer en/of mevrouw';
      END IF;
      CLOSE c_pers;
    END LOOP;
    IF v_pers_id_tab.COUNT > 1 AND v_pers_id_minderj IS NULL THEN
      -- meerdere personen, geen minderjarige?
      v_aanhef := 'Geachte heer en/of mevrouw';
    END IF;

    -- open c_uitn
    v_stap := 'open c_uitn';
    OPEN c_uitn;
    FETCH c_uitn INTO r_uitn;
    CLOSE c_uitn;

    Kgc_Xml_00.voegtoe_xml_regel (p_xml_clob      => p_xml_doc
                                 ,p_tag           => 'datum_print'
                                 ,p_xml_regel     => TO_CHAR (v_datum_print,'DD-MM-YYYY')
                                 );
    -- telefoon tbv header
    v_stap := 'header';
    v_attr_waarde := Kgc_Attr_00.waarde('KGC_LOCATIES', 'TELEFOON_UITNODIGING', r_uitn.loca_id);
    Kgc_Xml_00.voegtoe_xml_regel (p_xml_clob      => p_xml_doc
                                 ,p_tag           => 'telefoon_uitnodiging'
                                 ,p_xml_regel     => v_attr_waarde
                                 );
    -- extra bijlagen tbv footer (evt meerdere regels!)
    v_stap := 'footer';
    v_attr_waarde := Kgc_Attr_00.waarde('KGC_LOCATIES', 'BIJLAGEN_UITNODIGING', r_uitn.loca_id);
    Kgc_Xml_00.voegtoe_xml_regel
                       (p_xml_clob      => p_xml_doc
                       ,p_tag           => 'bijlagen_uitnodiging'
                       ,p_xml_regel     => v_nl || Kgc_Xml_00.cr_naar_tags(v_attr_waarde)
                       );
    -- aanhef
    v_stap := 'aanhef';
    Kgc_Xml_00.voegtoe_xml_regel
                       (p_xml_clob      => p_xml_doc
                       ,p_tag           => 'aanhef_uitnodiging'
                       ,p_xml_regel     => v_aanhef
                       );
    -- body van de brief
    v_stap := 'open c_noti';
    OPEN c_noti(p_entiteit => 'LOCA', p_id => r_uitn.loca_id, p_code => 'TEKST_UITN');
    FETCH c_noti INTO r_noti;
    CLOSE c_noti;
    Kgc_Xml_00.voegtoe_xml_regel
                       (p_xml_clob      => p_xml_doc
                       ,p_tag           => 'tekst_uitnodiging'
                       ,p_xml_regel     => v_nl || Kgc_Xml_00.cr_naar_tags(r_noti.omschrijving)
                       );
    -- geadresseerde = adres 1e persoon
    v_stap := 'geadresseerde';
    v_pers_adres := v_adressering || CHR(10)
      || Kgc_Adres_00.persoon (v_pers_id_tab(1), 'N'); -- namen al aanwezig
    Kgc_Xml_00.voegtoe_xml_regel
                       (p_xml_clob      => p_xml_doc
                       ,p_tag           => 'pers_adres'
                       ,p_xml_regel     => v_nl || Kgc_Xml_00.cr_naar_tags(v_pers_adres)
                       );
    -- haal onderzoek gegevens, via bouwsteen
    v_stap := 'onderzoek';
    v_xml_regel_collection :=
      Kgc_Bouwsteen_00.bouwsteen
        (p_bouwsteen     => 'KGC_ONDERZOEKEN'
        ,p_kolommen      => 'onderzoeknr'
        ,p_where         => 'onde_id = ' || TO_CHAR (r_uitn.onde_id)
        ,p_prefix        => 'onde'
        );
    Kgc_Xml_00.voegtoe_xml_regels
                           (p_xml_clob                 => p_xml_doc
                           ,p_xml_regel_collection     => v_xml_regel_collection);
    -- haal gesprek gegevens, via bouwsteen
    v_stap := 'gesprek';
    v_xml_regel_collection :=
      Kgc_Bouwsteen_00.bouwsteen
        (p_bouwsteen     => 'KGC_GESPREKKEN'
        ,p_kolommen      => 'to_char(datum, ''dd'') dd, to_char(datum, ''mm'') mm, to_char(datum, ''yyyy'') yyyy, ' ||
                            ' to_char(datum, ''hh24'') hh, to_char(datum, ''mi'') mi'
        ,p_where         => 'gesp_id = ' || TO_CHAR (p_gesp_id)
        ,p_prefix        => 'gesp'
        );
    Kgc_Xml_00.voegtoe_xml_regels
                           (p_xml_clob                 => p_xml_doc
                           ,p_xml_regel_collection     => v_xml_regel_collection);
    -- haal locatieomschrijving, via bouwsteen
    v_stap := 'locatie oms';
    v_xml_regel_collection :=
      Kgc_Bouwsteen_00.bouwsteen
        (p_bouwsteen     => 'KGC_LOCATIES'
        ,p_kolommen      => 'omschrijving'
        ,p_where         => 'loca_id = ' || TO_CHAR (r_uitn.loca_id)
        ,p_prefix        => 'loca'
        );
    Kgc_Xml_00.voegtoe_xml_regels
                           (p_xml_clob                 => p_xml_doc
                           ,p_xml_regel_collection     => v_xml_regel_collection);
    -- counselor
    v_stap := 'counselor';
    v_xml_regel_collection :=
      Kgc_Bouwsteen_00.bouwsteen
                            (p_bouwsteen     => 'KGC_MEDEWERKERS'
                            ,p_kolommen      => 'kgc_medewerkers.*, nvl(formele_naam, naam) as formele_naam1, nvl(formele_naam, naam) as formele_naam2'
                            ,p_where         => 'mede_id = ' || TO_CHAR(r_uitn.mede_id)
                            ,p_prefix        => 'mede'
                            );
    Kgc_Xml_00.voegtoe_xml_regels
                           (p_xml_clob                 => p_xml_doc
                           ,p_xml_regel_collection     => v_xml_regel_collection);
    -- functie counselor
    v_stap := 'functie counselor';
    v_xml_regel_collection :=
      Kgc_Bouwsteen_00.bouwsteen
        (p_bouwsteen     => 'KGC_MEDE_KAFD_VW'
        ,p_kolommen      => '*'
        ,p_where         =>    'mede_id = ' || TO_CHAR(r_uitn.mede_id)
                            || ' and kafd_id = ' || TO_CHAR(r_uitn.kafd_id)
        ,p_prefix        => 'meka'
        );
    Kgc_Xml_00.voegtoe_xml_regels
                           (p_xml_clob                 => p_xml_doc
                           ,p_xml_regel_collection     => v_xml_regel_collection);

    IF p_ind_brief = 'J'
    THEN
      v_stap := 'brief registreren';
      v_brty_id := Kgc_Uk2pk.brty_id('UITNODIGNG');
      OPEN c_ramo (b_brty_id => v_brty_id);
      FETCH c_ramo INTO v_ramo_id;
      CLOSE c_ramo;
      IF v_brty_id IS NULL OR v_ramo_id IS NULL THEN
        Qms$errors.show_message
        ( p_mesg => 'KGC-00000'
        , p_param0 => 'Er is geen brieftype en/of rapportmodule bij brieftype-code UITNODIGNG gevonden!'
        , p_param1 => 'beh_xml_doc.uitnodigingsbrief rekent hierop!'
        , p_errtp => 'E'
        , p_rftf => TRUE
        );
        RETURN;
      END IF;
      -- registreer de brief bij iedere persoon
      v_stap := 'brief registreren per persoon';
      FOR i IN v_pers_id_tab.FIRST..v_pers_id_tab.LAST LOOP
        Kgc_Brie_00.registreer (p_datum             => SYSDATE
                               ,p_kopie             => 'N'
                               ,p_brieftype         => 'UITNODIGNG'
                               ,p_brty_id           => v_brty_id
                               ,p_pers_id           => v_pers_id_tab(i)
                               ,p_kafd_id           => r_uitn.kafd_id
                               ,p_onde_id           => r_uitn.onde_id
                               ,p_rela_id           => NULL
                               ,p_uits_id           => NULL
                               ,p_geadresseerde     => v_pers_adres
                               ,p_ongr_id           => r_uitn.ongr_id
                               ,p_taal_id           => NULL
                               ,p_destype           => p_destype
                               ,p_best_id           => p_best_id
                               ,p_ind_commit        => 'J'
                               ,p_ramo_id           => v_ramo_id
                               );
      END LOOP;
    END IF;
    v_stap := 'klaar';
  EXCEPTION
    WHEN OTHERS THEN
      Qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'BEH_XML_DOC.uitnodigingsbrief'
                              ,p_param2     => SQLERRM
                              );
  END uitnodigingsbrief;
  /* uitnodigingsbrief voor de onderzoekspersoon */

  PROCEDURE gesp_kg_ondepers (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  ) IS
    CURSOR c_onde (p_onde_id NUMBER) IS
      SELECT pers_id
      FROM   KGC_ONDERZOEKEN onde
      WHERE  onde.onde_id = p_onde_id
      ;
    v_gesp_id                NUMBER;
    v_onde_id                NUMBER;
    v_pers_id                NUMBER;
    v_xml_doc                CLOB;
    v_xml_regel              Kgc_Xml_00.t_xml_regel;
    v_xml_regel_collection   Kgc_Xml_00.t_xml_regel_collection;
    v_nl            CONSTANT VARCHAR2 (2)                        := CHR (10);
    v_sql                    VARCHAR2 (1000);
    v_xml_hoofdtag           VARCHAR2 (100) := 'uitnodigingsbrieven';
    v_xml_subtag             VARCHAR2 (100) := 'uitnodigingsbrief';
    c_xvvz                   Kgc_Bouwsteen_00.t_bouwsteen_cursor;
  BEGIN
    -- hoofdselectie
    Kgc_Xml_00.init;
    v_xml_doc := Kgc_Xml_00.nieuwe_xml (p_naam     => v_xml_hoofdtag);

    OPEN c_xvvz FOR 'SELECT gesp_id, onde_id FROM KGC_GESPREKKEN ' || p_where_clause;


    LOOP
      FETCH c_xvvz
       INTO v_gesp_id, v_onde_id;

      EXIT WHEN c_xvvz%NOTFOUND;
      -- begin 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('<' || v_xml_subtag || '>' || v_nl)
                           , '<' || v_xml_subtag || '>' || v_nl
                           );
      -- inhoud
      OPEN c_onde(v_onde_id);
      FETCH c_onde INTO v_pers_id;
      CLOSE c_onde;
      uitnodigingsbrief
      ( p_pers_ids => TO_CHAR(v_pers_id)
      , p_gesp_id => v_gesp_id
      , p_ind_brief => p_ind_brief
      , p_destype => p_destype
      , p_best_id => p_best_id
      , p_xml_doc => v_xml_doc
      );
      -- einde 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('</' || v_xml_subtag || '>' || v_nl)
                           , '</' || v_xml_subtag || '>' || v_nl
                           );
    END LOOP;

    Kgc_Xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);
    Kgc_Xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

    CLOSE c_xvvz;
  END gesp_kg_ondepers;

  /* uitnodigingsbrieven voor alle gespreksbetrokken personen */
  PROCEDURE gesp_kg_gesppers (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  ) IS
    CURSOR c_gebe (p_gesp_id NUMBER) IS
      SELECT gebe.pers_id
      FROM   KGC_GESPREK_BETROKKENEN gebe
      ,      KGC_PERSONEN pers
      WHERE  gebe.gesp_id = p_gesp_id
      AND    gebe.pers_id = pers.pers_id
      ORDER BY pers.geslacht DESC -- eerst V, dan M
      ;
    -- selecteer andere gespreksaanwezigen, samenwonend met p_pers_id
    CURSOR c_pers_samen (p_gesp_id NUMBER, p_pers_id NUMBER) IS
      SELECT pers2.pers_id
      FROM   KGC_GESPREK_BETROKKENEN gebe
      ,      KGC_PERSONEN pers1
      ,      KGC_PERSONEN pers2
      WHERE  gebe.gesp_id = p_gesp_id
      AND    gebe.pers_id <> p_pers_id
      AND    pers1.pers_id = p_pers_id
      AND    pers2.pers_id = gebe.pers_id
      AND    pers1.adres = pers2.adres
      AND    NVL(pers1.postcode, 'x') = NVL(pers2.postcode, 'x')
      AND    UPPER(NVL(pers1.woonplaats, 'x')) = UPPER(NVL(pers2.woonplaats, 'x'))
      ;
    v_gesp_id                NUMBER;
    v_onde_id                NUMBER;
    v_pers_id                NUMBER;
    v_pers_ids               VARCHAR2(2000);
    v_pers_ids_al_geweest    VARCHAR2(2000);
    v_xml_doc                CLOB;
    v_xml_regel              Kgc_Xml_00.t_xml_regel;
    v_xml_regel_collection   Kgc_Xml_00.t_xml_regel_collection;
    v_nl            CONSTANT VARCHAR2 (2)                        := CHR (10);
    v_sql                    VARCHAR2 (1000);
    v_xml_hoofdtag           VARCHAR2 (100) := 'uitnodigingsbrieven';
    v_xml_subtag             VARCHAR2 (100) := 'uitnodigingsbrief';
    c_xvvz                   Kgc_Bouwsteen_00.t_bouwsteen_cursor;
  BEGIN
    -- hoofdselectie
    Kgc_Xml_00.init;
    v_xml_doc := Kgc_Xml_00.nieuwe_xml (p_naam     => v_xml_hoofdtag);

    OPEN c_xvvz FOR 'SELECT gesp_id, onde_id FROM KGC_GESPREKKEN ' || p_where_clause;

    LOOP
      FETCH c_xvvz
       INTO v_gesp_id, v_onde_id;

      EXIT WHEN c_xvvz%NOTFOUND;
      FOR r_gebe IN c_gebe(v_gesp_id) LOOP
        -- is de persoon al opgenomen als samenwonende?
        IF INSTR(v_pers_ids_al_geweest, '#' || TO_CHAR(r_gebe.pers_id) || '#') > 0 THEN
          NULL; -- niet nog een brief maken!
        ELSE
          v_pers_ids := TO_CHAR(r_gebe.pers_id);
          -- welke andere personen wonen op het adres van pers_id?
          FOR r_pers IN c_pers_samen(p_gesp_id => v_gesp_id, p_pers_id => r_gebe.pers_id) LOOP
            v_pers_ids := v_pers_ids || ',' || TO_CHAR(r_pers.pers_id);
            -- registreren 'al geweest':
            v_pers_ids_al_geweest := v_pers_ids_al_geweest || '#' || TO_CHAR(r_pers.pers_id) || '#';
          END LOOP;
          -- begin 1 brief:
          dbms_lob.writeappend ( v_xml_doc
                               , LENGTH ('<' || v_xml_subtag || '>' || v_nl)
                               , '<' || v_xml_subtag || '>' || v_nl
                               );
          -- inhoud
          uitnodigingsbrief
          ( p_pers_ids => v_pers_ids
          , p_gesp_id => v_gesp_id
          , p_ind_brief => p_ind_brief
          , p_destype => p_destype
          , p_best_id => p_best_id
          , p_xml_doc => v_xml_doc
          );
          -- einde 1 brief:
          dbms_lob.writeappend ( v_xml_doc
                               , LENGTH ('</' || v_xml_subtag || '>' || v_nl)
                               , '</' || v_xml_subtag || '>' || v_nl
                               );
        END IF;
      END LOOP;
    END LOOP;

    Kgc_Xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);
    Kgc_Xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

    CLOSE c_xvvz;
  END gesp_kg_gesppers;

  /* uitnodigingsbrieven voor 1 gespreksbetrokken persoon */
  PROCEDURE gebe_kg_pers (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  ) IS
    v_gesp_id                NUMBER;
    v_pers_id                NUMBER;
    v_xml_doc                CLOB;
    v_xml_regel              Kgc_Xml_00.t_xml_regel;
    v_xml_regel_collection   Kgc_Xml_00.t_xml_regel_collection;
    v_nl            CONSTANT VARCHAR2 (2)                        := CHR (10);
    v_sql                    VARCHAR2 (1000);
    v_xml_hoofdtag           VARCHAR2 (100) := 'uitnodigingsbrieven';
    v_xml_subtag             VARCHAR2 (100) := 'uitnodigingsbrief';
    c_xvvz                   Kgc_Bouwsteen_00.t_bouwsteen_cursor;
  BEGIN
    -- hoofdselectie
    Kgc_Xml_00.init;
    v_xml_doc := Kgc_Xml_00.nieuwe_xml (p_naam     => v_xml_hoofdtag);

    OPEN c_xvvz FOR 'SELECT gesp.gesp_id, gebe.pers_id FROM KGC_GESPREKKEN gesp, KGC_GESPREK_BETROKKENEN gebe '
                  || p_where_clause
                  || ' and gesp.gesp_id = gebe.gesp_id';

    LOOP
      FETCH c_xvvz
       INTO v_gesp_id, v_pers_id;

      EXIT WHEN c_xvvz%NOTFOUND;
      -- begin 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('<' || v_xml_subtag || '>' || v_nl)
                           , '<' || v_xml_subtag || '>' || v_nl
                           );
      -- inhoud
      uitnodigingsbrief
      ( p_pers_ids => TO_CHAR(v_pers_id)
      , p_gesp_id => v_gesp_id
      , p_ind_brief => p_ind_brief
      , p_destype => p_destype
      , p_best_id => p_best_id
      , p_xml_doc => v_xml_doc
      );
      -- einde 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('</' || v_xml_subtag || '>' || v_nl)
                           , '</' || v_xml_subtag || '>' || v_nl
                           );
    END LOOP;

    Kgc_Xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);
    Kgc_Xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

    CLOSE c_xvvz;
  END gebe_kg_pers;

PROCEDURE uits_cyto (
  p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
 ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
 ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
 ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
 ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
 ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
 ,p_best_id          IN   NUMBER   DEFAULT NULL
)
IS
  CURSOR c_ramo
  ( b_brty_id IN NUMBER
  ) IS
    SELECT ramo.ramo_id
    ,      brty.code brty_code
    FROM   KGC_RAPPORT_MODULES ramo
    ,      KGC_BRIEFTYPES brty
    WHERE  brty.brty_id = b_brty_id
    AND    brty.ramo_id = ramo.ramo_id
    ;
  CURSOR c_rapp
  ( p_ramo_id IN NUMBER
  , p_kafd_id IN NUMBER
  , p_ongr_id IN NUMBER
  ) IS
    SELECT rapp.rapp_id
    FROM   KGC_RAPPORTEN rapp
    WHERE  rapp.ramo_id = p_ramo_id
    AND    rapp.kafd_id = p_kafd_id
    AND    (rapp.ongr_id is null or p_ongr_id = rapp.ongr_id)
    ORDER BY decode(rapp.ongr_id, null, 1, 0) -- specifieke voorop
    ;
  CURSOR c_uits_koho ( p_uits_id IN NUMBER
                     , p_zisnr   IN kgc_personen.zisnr%type
                     ) IS
/* oud    SELECT NVL(uitv.rela_id, koho.rela_id) rela_id_adr
    ,      geadresseerde
    FROM   kgc_uitslagbrief_vw uitv
    ,      KGC_KOPIEHOUDERS koho
    WHERE  uitv.uits_id = p_uits_id
    AND    uitv.koho_id = koho.koho_id (+)
    ORDER BY DECODE(uitv.KOHO_ID,NULL,9,1) DESC
*/
    SELECT kgc_adres_00.relatie( uits.rela_id, 'J' ) geadresseerde
    ,      uits.rela_id rela_id_adr
    ,      case
             when kgc_attr_00.waarde('KGC_RELATIES', 'EMAIL', uits.rela_id) = 'J'
             and  p_zisnr is not null
             then 'Intern'
             else null
           end intern_tag -- H201105-002
    ,      1
    from   kgc_uitslagen uits
    WHERE  uits.uits_id = p_uits_id
    UNION ALL -- en kopiehouders
    SELECT kgc_adres_00.relatie( koho.rela_id, 'J' ) geadresseerde
    ,      koho.rela_id rela_id_adr
    ,      case
             when kgc_attr_00.waarde( 'KGC_RELATIES', 'EMAIL', koho.rela_id) = 'J'
             and  p_zisnr is not null
             then 'Intern'
             else null
           end intern_tag -- H201105-002
    ,      koho.koho_id
    from   kgc_uitslagen uits
    ,      kgc_kopiehouders koho
    WHERE  uits.uits_id = p_uits_id
    and    uits.uits_id = koho.uits_id
    ORDER BY 3
    ;
  CURSOR c_rate (p_rapp_id NUMBER) IS
    SELECT rate.*
    FROM   kgc_rapport_teksten rate
    WHERE  rate.rapp_id = p_rapp_id
    ORDER BY rate.volgorde
    ;
  CURSOR c_extra (p_onde_id NUMBER) IS
    SELECT pers.rela_id rela_id_huis
    ,      ongr.omschrijving ongr_omschrijving
    ,      onde.onderzoeknr
    ,      onde.rela_id rela_id_aanv
    ,      onde.rela_id_oorsprong
    ,      onde.pers_id
    ,      onde.mede_id_beoordelaar
    ,      onde.datum_binnen
    ,      mede1.code onde_auth_code
    FROM   kgc_onderzoeken onde
    ,      kgc_personen pers
    ,      kgc_medewerkers mede1
    ,      kgc_onderzoeksgroepen ongr
    WHERE  onde.onde_id = p_onde_id
    AND    onde.pers_id = pers.pers_id
    AND    onde.mede_id_autorisator = mede1.mede_id (+)
    AND    onde.ongr_id = ongr.ongr_id
    ;
  r_extra          c_extra%ROWTYPE;
  CURSOR c_foet (p_foet_id NUMBER) IS
    SELECT 'Foetus '||NVL( posi.omschrijving, TO_CHAR(foet.volgnr))
    FROM   KGC_POSITIES posi
    ,      KGC_FOETUSSEN foet
    WHERE  foet.posi_id = posi.posi_id (+)
    AND    foet.foet_id = p_foet_id
    ;
  CURSOR c_mons(p_onde_id NUMBER, p_pers_id NUMBER) IS
    SELECT DISTINCT onmo.mons_id
    ,      mons.mate_id
    ,      mons.pers_id
    ,      mons.foet_id
    ,      mate.omschrijving mate_omschrijving
    ,      herk.omschrijving herk_omschrijving
    FROM   KGC_ONDERZOEK_MONSTERS onmo
    ,      KGC_MONSTERS mons
    ,      BAS_FRACTIES frac
    ,      BAS_METINGEN meti
    ,      KGC_MATERIALEN mate
    ,      KGC_HERKOMSTEN herk
    WHERE  onmo.onde_id = p_onde_id
    AND    onmo.MONS_ID = mons.MONS_ID
    AND    onmo.onmo_id = meti.onmo_id
    AND    meti.frac_id = frac.frac_id
    AND    mons.mate_id = mate.mate_id
    AND    mons.herk_id = herk.herk_id (+)
    AND    NVL(frac.status, 'O') <> 'M'
    ORDER BY DECODE( mons.pers_id
                   , p_pers_id, 1
                   , 9
                   )
    ;
  r_mons_1 c_mons%rowtype;
  r_mons_1_null c_mons%rowtype;
  cursor c_zwan(p_pers_id number, p_foet_id number) is
    select zwan.info_geslacht
    from   kgc_zwangerschappen zwan
    ,      kgc_foetussen foet
    where  zwan.pers_id = p_pers_id
    and    zwan.zwan_id = foet.zwan_id
    and    foet.foet_id = p_foet_id
    ;
  r_zwan c_zwan%rowtype;
  r_zwan_null c_zwan%rowtype;
  cursor c_fami(p_pers_id number) is
    select familienummer
    from   kgc_families fami
    ,      kgc_familie_leden fale
    where  fale.pers_id = p_pers_id
    and    fale.fami_id = fami.fami_id
    order by familienummer
    ;
  cursor c_mede(p_mede_id number, p_kafd_id number) is
    select mede.*
    ,      func.omschrijving func_omschrijving
    from   kgc_medewerkers mede
    ,      kgc_mede_kafd meka
    ,      kgc_functies func
    where  mede.mede_id = p_mede_id
    and    mede.mede_id = meka.mede_id (+)
    and    p_kafd_id = meka.kafd_id (+)
    and    meka.func_id = func.func_id (+)
    ;
  r_mede                   c_mede%rowtype;
  r_mede_null              c_mede%rowtype;
  cursor c_meti(p_meti_id number) is
    select *
    from   bas_metingen
    where  meti_id = p_meti_id
    ;
  r_meti                   c_meti%rowtype;
  r_meti_null              c_meti%rowtype;
  cursor c_meet(p_meet_id number) is
    select mest.omschrijving mest_omschrijving
    ,      meet.meetwaarde
    from   bas_meetwaarden meet
    ,      bas_meetwaarde_statussen mest
    where  meet.meet_id = p_meet_id
    and    meet.mest_id = mest.mest_id(+)
    ;
  r_meet c_meet%rowtype;
  r_meet_null c_meet%rowtype;
  cursor c_meti_meet (p_meti_id number) is
    select meet.meet_id
    ,      meet.stof_id
    from   bas_meetwaarden meet
    where  meet.meti_id = p_meti_id
    order by meet.stof_id
    ;
  cursor c_onde_meet (p_onde_id number) is
    select meti.meti_id
    ,      meet.meet_id
    ,      meet.stof_id
    from   bas_meetwaarden meet
    ,      bas_metingen meti
    where  meti.onde_id = p_onde_id
    and    meet.meti_id = meti.meti_id
    order by meet.stof_id
    ;
  cursor c_stgr (p_meti_id number) is
    select stgr.omschrijving
    from   bas_metingen meti
    ,      kgc_stoftestgroepen stgr
    where  meti.meti_id = p_meti_id
    and    meti.stgr_id = stgr.stgr_id
    ;
  r_stgr c_stgr%rowtype;
  r_stgr_null c_stgr%rowtype;
  cursor c_meet_sort (p_meti_id number) is
    select meet.meet_id
    ,      meet.stof_id
    ,      meet.mwst_id
    ,      stof.omschrijving
    ,      kgc_attr_00.waarde('KGC_STOFTESTEN', 'UITSLAG_SORT', meet.stof_id) uitslag_sort
    from   bas_meetwaarden meet
    ,      kgc_stoftesten stof
    where  meet.meti_id = p_meti_id
    and    meet.stof_id = stof.stof_id
    and    substr(kgc_attr_00.waarde('KGC_STOFTESTEN', 'UITSLAG_SORT', meet.stof_id), 1,4) IN ('KWAL','KWAN')
    order by kgc_attr_00.waarde('KGC_STOFTESTEN', 'UITSLAG_SORT', meet.stof_id)
    ,      meet.meet_id
    ;
  r_meet_sort c_meet_sort%rowtype;
  r_meet_sort_last c_meet_sort%rowtype;
  r_meet_sort_null c_meet_sort%rowtype;
  cursor c_mwst(p_mwst_id number) is
    select code
    ,      prompt
    from   kgc_mwst_samenstelling mwss
    where  mwss.mwst_id = p_mwst_id
    and    mwss.code not in ('NAAM','OPM')
    order by volgorde
    ;
  CURSOR c_best
  ( p_best_id NUMBER
  , p_kafd_id IN NUMBER
  , p_ongr_id IN NUMBER
  ) is
    SELECT btyp.code
    ,      rapp.ramo_id
    FROM   kgc_bestanden best
    ,      kgc_bestand_types btyp
    ,      kgc_rapporten rapp
    WHERE  best.best_id = p_best_id
    AND    best.btyp_id = btyp.btyp_id
    AND    btyp.code = rapp.rapport
    AND    rapp.kafd_id = p_kafd_id
    AND    (rapp.ongr_id is null or p_ongr_id = rapp.ongr_id)
    ORDER BY decode(rapp.ongr_id, null, 1, 0) -- specifieke voorop
    ;
  r_best c_best%rowtype;

  v_adres1                 VARCHAR2 (4000);
  v_adres2                 VARCHAR2 (4000);
  v_xml_doc                CLOB;
  v_xml_regel              Kgc_Xml_00.t_xml_regel;
  v_xml_regel_collection   Kgc_Xml_00.t_xml_regel_collection;
  v_datum_print            VARCHAR2 (10) := TO_CHAR(SYSDATE,'DD-MM-YYYY');
  v_nl            CONSTANT VARCHAR2 (2)                        := CHR (10);
  v_sql                    VARCHAR2 (1000);
  v_xml_hoofdtag           VARCHAR2 (100) := 'uitslagbrieven';
  v_xml_subtag             VARCHAR2 (100) := 'uitslagbrief';
  c                        Kgc_Bouwsteen_00.t_bouwsteen_cursor;
  r_uits                   kgc_uitslagbrief_1_vw%ROWTYPE;
  v_inst_posinlijst1       NUMBER := 0;
  v_meti_id                NUMBER;
  v_meet_id                NUMBER;
  v_ramo_id                NUMBER;
  v_brty_code              kgc_brieftypes.code%TYPE;
  v_rapp_id                NUMBER;
  v_geadresseerde          kgc_uitslagbrief_vw.geadresseerde%TYPE;
  v_ind_kopie              VARCHAR2(1);
  v_rela_id_geadress       NUMBER;
  v_brief_voor_kh          BOOLEAN;
  v_tekst                  VARCHAR2(32767);
  v_meetwaarde             VARCHAR2(32767);
  v_dummy                  BOOLEAN;
  v_pos                    NUMBER;
  v_pers_zwan_duur         VARCHAR2(200);
  v_ond_aanw_monsters      NUMBER := 0;
  v_attr_waarde            VARCHAR2(200);
  v_tekst_geslacht         VARCHAR2(4000);
  v_tum_fusts              VARCHAR2(4000);
  v_tum_mutsc              VARCHAR2(4000);
  v_tum_qpcr               VARCHAR2(4000);
  v_tekst_tum1             VARCHAR2(32767);
  v_tekst_tum2             VARCHAR2(32767);
  v_tekst_tum3             VARCHAR2(32767);
  v_tekst_tum4             VARCHAR2(32767);
  v_tekst_tum5             VARCHAR2(32767);
  FUNCTION add_tekst (p_tekst VARCHAR2, p_tekst_add VARCHAR2, p_tekst_sep VARCHAR2) RETURN VARCHAR2 IS
    v_ret VARCHAR2(32767) := p_tekst;
  BEGIN
    IF LENGTH(p_tekst_add) > 0 THEN
      IF LENGTH(v_ret) > 0 THEN
        v_ret := v_ret || p_tekst_sep;
      END IF;
      v_ret := v_ret || p_tekst_add;
    END IF;
    RETURN v_ret;
  END add_tekst;
/* niet meer nodig!!!  PROCEDURE init_tum_cate (p_kafd_id number) IS
    cursor c_cate is
      select stof.stof_id
      ,      cate.code
      from   kgc_categorie_types caty
      ,      kgc_categorieen cate
      ,      kgc_categorie_waarden cawa
      ,      kgc_stoftesten stof
      where  caty.code = 'CYTO_TUM'
      and    caty.caty_id = cate.caty_id
      and    cate.code in ('TUM_MUTSC', 'TUM_QPCR', 'TUM_FUSTS')
      and    cate.cate_id = cawa.cate_id
      and    cawa.waarde = stof.code
      and    stof.kafd_id = p_kafd_id
      ;
  BEGIN
    IF nvl(length(v_tum_qpcr), 0) + nvl(length(v_tum_mutsc), 0) + nvl(length(v_tum_fusts), 0) > 0 THEN
      RETURN; -- hoeft maar 1 keer!
    END IF;
    FOR r_cate IN c_cate LOOP
      IF r_cate.code = 'TUM_MUTSC' THEN
        v_tum_mutsc := v_tum_mutsc || '#' || r_cate.stof_id || '#';
      ELSIF r_cate.code = 'TUM_QPCR' THEN
        v_tum_qpcr := v_tum_qpcr || '#' || r_cate.stof_id || '#';
      ELSIF r_cate.code = 'TUM_FUSTS' THEN
        v_tum_fusts := v_tum_fusts || '#' || r_cate.stof_id || '#';
      END IF;
    END LOOP;
  END; */
  FUNCTION vervang_var
  ( p_tekst IN OUT VARCHAR
  , p_tekst_van IN VARCHAR2 := NULL
  , p_tekst_naar IN VARCHAR2 := NULL
  )
  RETURN BOOLEAN
  IS
    v_tekst VARCHAR2(32767) := p_tekst;
    v_tekst_van VARCHAR2(2000) := UPPER(p_tekst_van);
    v_gewijzigd BOOLEAN := FALSE;
  BEGIN
    -- substitutiecodes case insensitive
    v_tekst := REPLACE( v_tekst, '<systeemdatum>', '<SYSTEEMDATUM>' ); -- altijd
    IF ( INSTR( v_tekst, '<SYSTEEMDATUM>' ) > 0 )
    THEN
      v_tekst := REPLACE( v_tekst, '<SYSTEEMDATUM>', TO_CHAR( SYSDATE, 'dd-mm-yyyy' ) );
      v_gewijzigd := TRUE;
    END IF;
    --
    IF NVL(LENGTH(p_tekst_van), 0) > 0 THEN
      v_tekst := REPLACE( v_tekst, LOWER(p_tekst_van), v_tekst_van);
      IF ( INSTR( v_tekst, v_tekst_van ) > 0 )
      THEN
        v_tekst := REPLACE( v_tekst, v_tekst_van, p_tekst_naar );
        v_gewijzigd := TRUE;
      END IF;
    END IF;
    --
    p_tekst := v_tekst;
    RETURN( v_gewijzigd );
  END vervang_var;
  FUNCTION getMETI(p_kafd_id number, p_onde_id number, p_stgr_code varchar2) return number is
    cursor c_meti is
      select meti.meti_id
      from   bas_metingen meti
      ,      kgc_stoftestgroepen stgr
      where  meti.onde_id = p_onde_id
      and    meti.stgr_id = stgr.stgr_id
      and    stgr.code like p_stgr_code
      and    stgr.kafd_id = p_kafd_id
      ;
    r_meti c_meti%rowtype;
  BEGIN
    open c_meti;
    fetch c_meti into r_meti;
    close c_meti;
    return r_meti.meti_id;
  END getMETI;
  FUNCTION getMEET(p_kafd_id number, p_meti_id number, p_stof_code varchar2) return number is
    cursor c_meet is
      select meet.meet_id
      from   bas_meetwaarden meet
      ,      kgc_stoftesten stof
      where  meet.meti_id = p_meti_id
      and    meet.stof_id = stof.stof_id
      and    stof.code like p_stof_code
      and    stof.kafd_id = p_kafd_id
      ;
    r_meet c_meet%rowtype;
  BEGIN
    open c_meet;
    fetch c_meet into r_meet;
    close c_meet;
    return r_meet.meet_id;
  END getMEET;
  FUNCTION getRate(p_rapp_id number, p_code varchar2) return varchar2 is
    CURSOR c_rate IS
    SELECT rate.tekst
    FROM   kgc_rapport_teksten rate
    WHERE  rate.rapp_id = p_rapp_id
    AND    rate.code = p_code
    ;
  r_rate c_rate%rowtype;
  BEGIN
    open c_rate;
    fetch c_rate into r_rate;
    close c_rate;
    return r_rate.tekst;
  END getRate;
BEGIN
  -- hoofdselectie
  kgc_xml_00.init;

  -- p_view_name = kgc_uitslagbrief_1_vw - geen kopiehouders!
  OPEN c FOR 'SELECT * FROM kgc_uitslagbrief_1_vw '
    || ' ' || p_where_clause
    || ' order by onderzoeknr';

  LOOP
    FETCH c
     INTO r_uits;
    EXIT WHEN c%NOTFOUND;
    -- persoon zwangerschap duur
    v_pers_zwan_duur := kgc_zwan_00.duur(r_uits.onde_id, 'LANG');
    -- 1e monster ophalen tbv bepaling monster-materiaal en zwangerschap
    r_mons_1 := r_mons_1_null;
    open c_mons(r_uits.onde_id, r_extra.pers_id);
    fetch c_mons into r_mons_1;
    close c_mons;
    r_zwan := r_zwan_null;
    open c_zwan(p_pers_id => r_extra.pers_id, p_foet_id => r_mons_1.foet_id);
    fetch c_zwan into r_zwan;
    close c_zwan;

    v_xml_doc := kgc_xml_00.nieuwe_xml (p_naam => v_xml_hoofdtag);

    -- 1e poging om v_ramo_id te bepalen
    OPEN c_ramo (b_brty_id => r_uits.brty_id);
    FETCH c_ramo INTO v_ramo_id, v_brty_code;
    CLOSE c_ramo;
    -- 2e poging: deze is beter (indien deze lukt), want voor FISH en TUMOR aanvullend
    -- moeten andere teksten gebruikt worden (niet via brieftype UITSLAG, maar via het gekozen bestand-type
    OPEN c_best(p_best_id => p_best_id, p_kafd_id => r_uits.kafd_id, p_ongr_id => r_uits.ongr_id);
    FETCH c_best INTO r_best;
    IF c_best%FOUND THEN
      v_ramo_id := r_best.ramo_id;
    END IF;
    CLOSE c_best;
    OPEN c_rapp (p_ramo_id => v_ramo_id, p_kafd_id => r_uits.kafd_id, p_ongr_id => r_uits.ongr_id);
    FETCH c_rapp INTO v_rapp_id;
    CLOSE c_rapp;
    -- ter verificatie: where-claues!
    kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                 ,p_tag           => 'p_best_id'
                                 ,p_xml_regel     => toxml(p_best_id || '*?*' || r_best.code)
                                 );

    -- voor iedere uitslag 1 brief! (= aan aanvrager (2 keer!!) + kopiehouders):
    FOR r_uits_koho IN c_uits_koho ( r_uits.uits_id
                                   , r_uits.zisnr
                                   )
    LOOP
      -- begin 1 brief
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH ('<' || v_xml_subtag || '>')
                           ,'<' || v_xml_subtag || '>'
                           );

      OPEN c_extra(r_uits.onde_id);
      FETCH c_extra INTO r_extra;
      CLOSE c_extra;

      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'datum_print'
                                   ,p_xml_regel     => v_datum_print
                                   );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'onde_ongr_omschrijving'
                                   ,p_xml_regel     => r_extra.ongr_omschrijving
                                   );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'intern'
                                   ,p_xml_regel     => toxml(r_uits_koho.intern_tag)
                                   );  -- H201105-002

      v_rela_id_geadress := r_uits_koho.rela_id_adr; -- default; evt: zie GEADRESS
      v_brief_voor_kh := (r_uits_koho.rela_id_adr <> r_uits.rela_id);

      -- tabel met 1 of meer monstergegevens opnemen
      -- omdat dit naadloos aansluit op karyotype (in het template staat dit voor karyotype)
      -- moet dit in de xml OOK VOOR het karyotype... bizar...
      SELECT count(*) INTO v_ond_aanw_monsters
      FROM     kgc_rapport_teksten rate
      WHERE  rate.rapp_id = v_rapp_id
      AND    rate.code = 'MONSTERS'
      ;
      IF v_ond_aanw_monsters > 0 THEN
        v_tekst := '<monss>';
      ELSE
        v_tekst := '<mons1>';
      END IF;
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH (v_tekst)
                           ,v_tekst
                           );
      FOR r_mons IN c_mons(r_uits.onde_id, r_extra.pers_id) LOOP
        IF v_ond_aanw_monsters > 0 THEN
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<mons>')
                               ,'<mons>'
                               );
        ELSE
          kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                       ,p_tag           => 'mons1_materiaal'
                                       ,p_xml_regel     => toxml(r_mons.mate_omschrijving)
                                       );
          IF r_mons.herk_omschrijving IS NOT NULL THEN
            dbms_lob.writeappend (v_xml_doc,LENGTH ('<mons1_herkomst_opt>'),'<mons1_herkomst_opt>');
            kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                         ,p_tag           => 'mons1_herkomst'
                                         ,p_xml_regel     => toxml(r_mons.herk_omschrijving)
                                         );
            dbms_lob.writeappend (v_xml_doc,LENGTH ('</mons1_herkomst_opt>'),'</mons1_herkomst_opt>');
          END IF;
          EXIT; -- 1e is ok
        END IF;
        v_xml_regel_collection :=
          kgc_bouwsteen_00.bouwsteen (p_bouwsteen     => 'KGC_MONSTERS'
                                     ,p_kolommen      => 'MONSTERNUMMER, HOEVEELHEID_MONSTER, TO_CHAR(datum_aanmelding, ''dd-mm-yyyy'') datum_aanmelding'
                                     ,p_where         =>    'mons_id = '
                                                         || TO_CHAR(r_mons.mons_id)
                                     ,p_prefix        => 'mons'
                                     );
        kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
        -- van?
        v_tekst := NULL;
        IF r_mons.foet_id IS NOT NULL THEN
          OPEN c_foet(r_mons.foet_id);
          FETCH c_foet INTO v_tekst;
          CLOSE c_foet;
        ELSIF r_mons.pers_id <> r_extra.pers_id THEN
          v_tekst := kgc_pers_00.info( p_pers_id => r_mons.pers_id);
        END IF;
        kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                     ,p_tag           => 'mons_van'
                                     ,p_xml_regel     => toxml(v_tekst)
                                     );
        -- monster materiaal
        v_xml_regel_collection :=
          kgc_bouwsteen_00.bouwsteen (p_bouwsteen     => 'KGC_MATERIALEN'
                                     ,p_kolommen      => 'CODE, OMSCHRIJVING, EENHEID'
                                     ,p_where         => 'mate_id = ' || TO_CHAR(r_mons.mate_id)
                                     ,p_prefix        => 'mate'
                                     );
        kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
        IF v_ond_aanw_monsters > 0 THEN
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('</mons>')
                               ,'</mons>'
                               );
        END IF;
      END LOOP;
      IF v_ond_aanw_monsters > 0 THEN
        v_tekst := '</monss>';
      ELSE
        v_tekst := '</mons1>';
      END IF;
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH (v_tekst)
                           ,v_tekst
                           );

      FOR r_rate in c_rate(v_rapp_id) LOOP -- in volgorde van de 'rapport module'
        IF r_rate.code = 'KOPIE' THEN
  --        sluit_groep_af(v_groep);
          v_tekst := NULL;
          IF v_brief_voor_kh THEN
            v_tekst := 'Brief voor kopiehouder';
          ELSIF kgc_brie_00.eerder_geprint(r_uits.onde_id, NULL, r_uits.brty_id) = 'J' THEN
            v_tekst := 'Kopie';
          END IF;
          kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                       ,p_tag           => 'kopie_tekst'
                                       ,p_xml_regel     => toxml(v_tekst)
                                       );
        ELSIF r_rate.code = 'GEADRESS' THEN -- geadresseerde: aanvrager of kopiehouder
          v_adres1 := kgc_adres_00.relatie(NVL(r_uits_koho.rela_id_adr, 0), 'J');
          IF r_extra.rela_id_aanv = r_uits_koho.rela_id_adr THEN -- oorspr. aanv. klaarzetten
            v_adres2 := kgc_adres_00.relatie(NVL(r_extra.rela_id_oorsprong, NVL(r_uits_koho.rela_id_adr, 0)), 'J');
          ELSE -- altijd kopiehouder
            v_adres2 := v_adres1;
          END IF;
          v_tekst := r_rate.tekst;
          IF vervang_var( p_tekst => v_tekst, p_tekst_van => '<WAARDE_OORSPR>'
                            , p_tekst_naar => v_adres2)
          THEN -- geadresseerde is de oorspronkelijke aanvrager
            v_rela_id_geadress := NVL(r_extra.rela_id_oorsprong, NVL(r_uits_koho.rela_id_adr, 0));
          ELSE -- probeer <waarde>
            IF NOT vervang_var( p_tekst => v_tekst, p_tekst_van => '<WAARDE>'
                            , p_tekst_naar => v_adres1)
            THEN -- dan aanvrager toevoegen
              v_tekst := v_tekst || v_adres1;
            END IF;
          END IF;
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => 'uits_geadress_r'
                             ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                             );
        ELSIF r_rate.code IN ('LBL_LOK', 'LBL_ON_REF', 'LBL_TELF', 'LBL_DATUM', 'LOKATIEAFD') THEN
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst);
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => lower(r_rate.code)
                             ,p_xml_regel     => toxml(v_tekst)
                             );
        ELSIF r_rate.code in ('BETREFT', 'BETR_TEKST') THEN
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst);
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => lower(r_rate.code)
                             ,p_xml_regel     => toxml(v_tekst)
                             );
        ELSIF r_rate.code = 'LABTEL' THEN
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst);
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => 'labtel'
                             ,p_xml_regel     => toxml(v_tekst)
                             );
        ELSIF r_rate.code = 'UW_REF' THEN
          null;
        ELSIF r_rate.code = 'DATERING' THEN
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst);
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => 'datering'
                             ,p_xml_regel     => toxml(v_tekst)
                             );
        ELSIF r_rate.code = 'AANHEF' THEN
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst);
          v_dummy := vervang_var( p_tekst => v_tekst, p_tekst_van => '<PERS_ZWAN_DUUR>'
                            , p_tekst_naar => v_pers_zwan_duur);
          v_dummy := vervang_var( p_tekst => v_tekst, p_tekst_van => '<ONDE_DATUM_BINNEN>'
                            , p_tekst_naar => to_char(r_extra.datum_binnen, 'dd-mm-yyyy'));
          -- afname-omschrijving
          v_attr_waarde := kgc_attr_00.waarde('KGC_MATERIALEN', 'AFNAME_OMS', r_mons_1.mate_id);
          IF nvl(length( v_attr_waarde), 0) = 0 THEN
            v_attr_waarde := 'afname';
          END IF;
          v_dummy := vervang_var( p_tekst => v_tekst, p_tekst_van => '<MATE_AFNAME>'
                            , p_tekst_naar => v_attr_waarde);
          -- afp gedaan?
          v_meti_id := getMETI(p_kafd_id => r_uits.kafd_id, p_onde_id => r_uits.onde_id, p_stgr_code => 'AFP');
          IF v_meti_id IS NULL THEN
            v_attr_waarde := NULL;
          ELSE
            r_stgr := r_stgr_null;
            open c_stgr (p_meti_id => v_meti_id);
            fetch c_stgr into r_stgr;
            close c_stgr;
            v_attr_waarde := r_stgr.omschrijving || ' en ';
          END IF;
          v_dummy := vervang_var( p_tekst => v_tekst, p_tekst_van => '<AFP_PROTOCOL>'
                            , p_tekst_naar => v_attr_waarde);

          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => 'aanhef_r'
                             ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                             );
        ELSIF r_rate.code = 'VREUGDE' THEN
          IF r_zwan.info_geslacht = 'N' THEN
            v_tekst_geslacht := getRate(v_rapp_id, 'VREUGDE_N');
          ELSIF r_zwan.info_geslacht = 'J' THEN
            v_tekst_geslacht := getRate(v_rapp_id, 'VREUGDE_J');
          ELSE
            v_tekst := NULL;
          END IF;
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst, p_tekst_van => '<WAARDE>'
                          , p_tekst_naar => v_tekst_geslacht);
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => 'vreugde_r'
                             ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                             );
        ELSIF r_rate.code = 'SLOT' THEN
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst);
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => 'slot_r'
                             ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                             );
        ELSIF r_rate.code = 'GROET' THEN
          v_tekst := r_rate.tekst;
          v_dummy := vervang_var( p_tekst => v_tekst);
          kgc_xml_00.voegtoe_xml_regel
                             (p_xml_clob      => v_xml_doc
                             ,p_tag           => 'groet_r'
                             ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                             );
        ELSIF r_rate.code = 'KARYOTYPE' THEN
          IF r_best.code = 'KGCUITS51CYTO_AV' THEN -- dan karyo uit FISH halen
            IF v_brty_code IN ('FISH', 'VRLBRIEF') THEN
              v_meti_id := getMETI(p_kafd_id => r_uits.kafd_id, p_onde_id => r_uits.onde_id, p_stgr_code => 'FISH_KARYO');
              IF v_meti_id IS NOT NULL THEN
                r_meti := r_meti_null;
                open c_meti(p_meti_id => v_meti_id);
                fetch c_meti into r_meti;
                close c_meti;
                v_tekst := r_meti.conclusie;
                IF v_tekst IS NOT NULL THEN
                  dbms_lob.writeappend (v_xml_doc,LENGTH ('<karyo>'),'<karyo>');
                  kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                               ,p_tag           => 'karyo_res'
                                               ,p_xml_regel     => toxml(v_tekst)
                                               );
                  dbms_lob.writeappend (v_xml_doc,LENGTH ('</karyo>'),'</karyo>');
                END IF;
              END IF;
            ELSE
              null; -- geen karyotype opnemen!
            END IF;
          ELSE
            v_meti_id := getMETI(p_kafd_id => r_uits.kafd_id, p_onde_id => r_uits.onde_id, p_stgr_code => UPPER(TRIM(r_rate.tekst)));
            v_meet_id := getMEET(p_kafd_id => r_uits.kafd_id, p_meti_id => v_meti_id, p_stof_code => 'KARYO%');
            IF v_meet_id IS NOT NULL THEN
              r_meet := r_meet_null;
              open c_meet(p_meet_id => v_meet_id);
              fetch c_meet into r_meet;
              close c_meet;
              v_tekst := r_meet.meetwaarde;
              -- extra voorwaarde voor opnemen: indien het onderzoeksgroep TM/TV betreft: alleen opnemen indien brieftype = 'KARYO'
              IF r_uits.ongr_code NOT IN ('TM', 'TV') OR
                 v_brty_code IN ('KARYO') THEN
                dbms_lob.writeappend (v_xml_doc,LENGTH ('<karyo>'),'<karyo>');
      --          v_tekst := bas_meet_00.toon_meetwaarde( p_meet_id => v_meet_id, p_meet_bere => 'MEET', p_type => 'WAARDE');
                kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                             ,p_tag           => 'karyo_meet_id'
                                             ,p_xml_regel     => toxml(v_meet_id)
                                             );
                kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                             ,p_tag           => 'karyo_meti_id'
                                             ,p_xml_regel     => toxml(v_meti_id)
                                             );
                kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                             ,p_tag           => 'karyo_res'
                                             ,p_xml_regel     => toxml(v_tekst)
                                             );
                dbms_lob.writeappend (v_xml_doc,LENGTH ('</karyo>'),'</karyo>');
              END IF;
            END IF;
          END IF;
        ELSIF r_rate.code = 'EXTRA_POST' THEN
          dbms_lob.writeappend (v_xml_doc,LENGTH ('<extra_post>'),'<extra_post>');
          v_meti_id := getMETI(p_kafd_id => r_uits.kafd_id, p_onde_id => r_uits.onde_id, p_stgr_code => 'KARYO-PO');
          v_meet_id := getMEET(p_kafd_id => r_uits.kafd_id, p_meti_id => v_meti_id, p_stof_code => 'KARYO%');
          v_tekst := bas_mdet_00.detail_waarde(p_meet_id => v_meet_id, p_code => 'GETELD');
          --kgc_mwst_00.detailwaarde_meting( v_meti_id, 'TOTAAL_GETELD' );
          kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                       ,p_tag           => 'ex_geteld'
                                       ,p_xml_regel     => toxml(v_tekst)
                                       );
          v_tekst := bas_mdet_00.detail_waarde(p_meet_id => v_meet_id, p_code => 'GEANALYS');
--          v_tekst := kgc_mwst_00.detailwaarde_meting( v_meti_id, 'TOTAAL_GEANALYSEERD' );
          kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                       ,p_tag           => 'ex_geanalyseerd'
                                       ,p_xml_regel     => toxml(v_tekst)
                                       );
          v_tekst := bas_mdet_00.detail_waarde(p_meet_id => v_meet_id, p_code => 'GEM-KWAL');
          kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                       ,p_tag           => 'ex_status'
                                       ,p_xml_regel     => toxml(v_tekst)
                                       );
          dbms_lob.writeappend (v_xml_doc,LENGTH ('</extra_post>'),'</extra_post>');
/* miv 15092008 niet meer van toepassing!!! zucht...
        ELSIF r_rate.code = 'EXTRA_SENS' THEN
          -- alleen tovoegen indien er een stoftest is uit categorie TUM_FUSTS, TUM_MUTSC of TUM_QPCR
          v_tekst := NULL;
          init_tum_cate(p_kafd_id => r_uits.kafd_id);
          FOR r_onde_meet IN c_onde_meet (p_onde_id => r_uits.onde_id) LOOP
            IF INSTR(v_tum_fusts, '#' || r_onde_meet.stof_id || '#') > 0 OR
               INSTR(v_tum_mutsc, '#' || r_onde_meet.stof_id || '#') > 0 OR
               INSTR(v_tum_qpcr, '#' || r_onde_meet.stof_id || '#') > 0
            THEN
              v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_onde_meet.meet_id, p_code => 'NAAM');
              IF LENGTH(v_meetwaarde) > 0 THEN
                IF LENGTH(v_tekst) > 0 THEN
                  v_tekst := v_tekst || ', ';
                END IF;
                v_tekst := v_tekst || v_meetwaarde;
                v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_onde_meet.meet_id, p_code => 'SENS');
                IF LENGTH(v_meetwaarde) > 0 THEN
                  v_tekst := v_tekst || ' (' || v_meetwaarde || ')';
                END IF;
              END IF;
            END IF;
          END LOOP;
          IF LENGTH(v_tekst) > 0 THEN
            dbms_lob.writeappend (v_xml_doc,LENGTH ('<extra_sens>'),'<extra_sens>');
            kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                         ,p_tag           => 'tekst'
                                         ,p_xml_regel     => toxml(v_tekst)
                                         );
            dbms_lob.writeappend (v_xml_doc,LENGTH ('</extra_sens>'),'</extra_sens>');
          END IF;
        ELSIF r_rate.code = 'EXTRA_MUT' THEN
          -- alleen tovoegen indien er een stoftest is uit categorie TUM_MUTSC
          init_tum_cate(p_kafd_id => r_uits.kafd_id);
          FOR r_onde_meet IN c_onde_meet (p_onde_id => r_uits.onde_id) LOOP
            IF INSTR(v_tum_mutsc, '#' || r_onde_meet.stof_id || '#') > 0
            THEN
              v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_onde_meet.meet_id, p_code => 'UITG-MAT');
              v_tekst_tum1 := add_tekst(v_tekst_tum1, v_meetwaarde, ', ');
              v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_onde_meet.meet_id, p_code => 'LOCUS');
              v_tekst_tum2 := add_tekst(v_tekst_tum2, v_meetwaarde, ', ');
              v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_onde_meet.meet_id, p_code => 'CHROM');
              v_tekst_tum3 := add_tekst(v_tekst_tum3, v_meetwaarde, ', ');
              v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_onde_meet.meet_id, p_code => 'AFW');
              v_tekst_tum4 := add_tekst(v_tekst_tum4, v_meetwaarde, ', ');
              v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_onde_meet.meet_id, p_code => 'METH');
              v_tekst_tum5 := add_tekst(v_tekst_tum5, v_meetwaarde, ', ');
            END IF;
          END LOOP;
          IF NVL(LENGTH(v_tekst_tum1), 0) + NVL(LENGTH(v_tekst_tum2), 0) + NVL(LENGTH(v_tekst_tum3), 0) + NVL(LENGTH(v_tekst_tum4), 0) + NVL(LENGTH(v_tekst_tum5), 0) > 0 THEN
            dbms_lob.writeappend (v_xml_doc,LENGTH ('<extra_mut>'),'<extra_mut>');
            kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'uitgangsmateriaal', p_xml_regel => toxml(v_tekst_tum1));
            kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'locus', p_xml_regel => toxml(v_tekst_tum2));
            kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'chromosoom', p_xml_regel => toxml(v_tekst_tum3));
            kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'afwijking', p_xml_regel => toxml(v_tekst_tum4));
            kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'methode', p_xml_regel => toxml(v_tekst_tum5));
            dbms_lob.writeappend (v_xml_doc,LENGTH ('</extra_mut>'),'</extra_mut>');
          END IF;
        ELSIF r_rate.code = 'EXTRA_PCR' THEN
          -- alleen tovoegen indien er een stoftest is uit categorie TUM_QPCR
          init_tum_cate(p_kafd_id => r_uits.kafd_id);
          FOR r_onde_meet IN c_onde_meet (p_onde_id => r_uits.onde_id) LOOP
            IF INSTR(v_tum_qpcr, '#' || r_onde_meet.stof_id || '#') > 0
            THEN -- lege groep wegschrijven
              dbms_lob.writeappend (v_xml_doc,LENGTH ('<extra_pcr>'),'<extra_pcr>');
              kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'dummy', p_xml_regel => toxml(''));
              dbms_lob.writeappend (v_xml_doc,LENGTH ('</extra_pcr>'),'</extra_pcr>');
              EXIT; -- en klaar!
            END IF;
          END LOOP;
*/
        ELSIF r_rate.code = 'EXTRA_MD' THEN
          IF v_brty_code IN ('MOLDIAG') THEN
            v_meti_id := getMETI(p_kafd_id => r_uits.kafd_id, p_onde_id => r_uits.onde_id, p_stgr_code => 'MD');
            IF v_meti_id IS NOT NULL THEN
              dbms_lob.writeappend (v_xml_doc,LENGTH ('<extra_md><kmds>'),'<extra_md><kmds>');
              r_meet_sort_last := r_meet_sort_null;
              OPEN c_meet_sort(p_meti_id => v_meti_id);
              WHILE TRUE LOOP
                r_meet_sort := r_meet_sort_null;
                FETCH c_meet_sort INTO r_meet_sort;
                IF nvl(substr(r_meet_sort_last.uitslag_sort,1,4), '@#@') <> nvl(substr(r_meet_sort.uitslag_sort,1,4), '@#@') THEN
                  IF r_meet_sort_last.uitslag_sort IS NOT NULL THEN -- afsluiten
                    dbms_lob.writeappend (v_xml_doc,LENGTH ('</stofs></kmd>'),'</stofs></kmd>');
                  END IF;
                  IF r_meet_sort.uitslag_sort IS NOT NULL THEN -- openen
                    dbms_lob.writeappend (v_xml_doc,LENGTH ('<kmd>'),'<kmd>');
                    IF substr(r_meet_sort.uitslag_sort,1,4) = 'KWAL' THEN
                      v_tekst := 'Kwalitatieve moleculaire diagnostiek:';
                    ELSIF substr(r_meet_sort.uitslag_sort,1,4) = 'KWAN' THEN
                      v_tekst := 'Kwanitatieve moleculaire diagnostiek:';
                      IF r_meet_sort_last.uitslag_sort IS NOT NULL THEN -- dit is de tweede groep... newline extra
                        null;
                        -- niet nodig - in template geregeld! v_tekst := chr(10) || v_tekst;
                      END IF;
                    ELSE
                      v_tekst := 'Onbekende sortering';
                    END IF;
                    kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'type', p_xml_regel => v_nl || kgc_xml_00.cr_naar_tags(v_tekst));
                    dbms_lob.writeappend (v_xml_doc,LENGTH ('<stofs>'),'<stofs>');
                  END IF;
                END IF;
                IF c_meet_sort%notfound THEN
                  EXIT;
                END IF;
                r_meet_sort_last := r_meet_sort;
                -- stoftest tonen:
                dbms_lob.writeappend (v_xml_doc,LENGTH ('<stof>'),'<stof>');
                kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'naam', p_xml_regel => toxml(r_meet_sort.omschrijving));
                dbms_lob.writeappend (v_xml_doc,LENGTH ('<atts>'),'<atts>');
                FOR r_mwst IN c_mwst(p_mwst_id => r_meet_sort.mwst_id) LOOP
                  dbms_lob.writeappend (v_xml_doc,LENGTH ('<att>'),'<att>');
                  kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'prompt', p_xml_regel => toxml(r_mwst.prompt));
                  v_meetwaarde := bas_mdet_00.detail_waarde(p_meet_id => r_meet_sort.meet_id, p_code => r_mwst.code);
                  kgc_xml_00.voegtoe_xml_regel (v_xml_doc, p_tag => 'waarde', p_xml_regel => toxml(v_meetwaarde));
                  dbms_lob.writeappend (v_xml_doc,LENGTH ('</att>'),'</att>');
                END LOOP;
                dbms_lob.writeappend (v_xml_doc,LENGTH ('</atts>'),'</atts>');
                dbms_lob.writeappend (v_xml_doc,LENGTH ('</stof>'),'</stof>');
              END LOOP;
              CLOSE c_meet_sort;
              dbms_lob.writeappend (v_xml_doc,LENGTH ('</kmds></extra_md>'),'</kmds></extra_md>');
            END IF;
          END IF;
        END IF;
      END LOOP; -- c_rate

      -- vervolgblad gegevens:

      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'heading_onderzoeknr'
                                   ,p_xml_regel     => toxml(r_extra.onderzoeknr)
                                   );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'heading_datum_print'
                                   ,p_xml_regel     => v_datum_print
                                   );
      v_tekst := persoon_info_cyto (NULL , r_uits.onde_id , r_extra.pers_id , 'VERVOLGVEL' );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'heading_persinfo'
                                   ,p_xml_regel     => toxml(v_tekst)
                                   );
      -- onderzoekgegevens
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen (p_bouwsteen     => 'KGC_XML_ONDERZOEKEN_VW'
                                   ,p_kolommen      => '*'
                                   ,p_where         => 'onde_id = ' || TO_CHAR(r_uits.onde_id)
                                   ,p_prefix        => 'onde'
                                   );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- onderzoekindicaties:
      v_tekst := kgc_indi_00.onderzoek_indi_omschrijvingen(r_uits.onde_id);
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'onde_indicaties'
                                   ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                                   );
      -- vreugde?
      v_tekst := kgc_vreugdebrief_00.geslacht_in_uitslag(r_uits.onde_id);
      IF v_tekst = 'N' THEN
        v_tekst := 'Bovenstaande uitslag is reeds schriftelijk aan patiente doorgegeven. ' ||
          'Op nadrukkelijk verzoek van patiente is haar het geslacht niet meegedeeld.';
      ELSIF v_tekst = 'J' THEN
        v_tekst := 'Bovenstaande uitslag is reeds schriftelijk aan patiente doorgegeven.';
      ELSE
        v_tekst := NULL;
      END IF;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uits_vreugde'
                         ,p_xml_regel     => toxml(v_tekst)
                         );

      -- uitslaggevens
      v_tekst := r_uits.uitslag_tekst;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uits_tekst_r'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                         );
      if r_uits.toelichting is not null then -- optioneel!
        v_tekst := r_uits.toelichting;
        kgc_xml_00.voegtoe_xml_regel
                           (p_xml_clob      => v_xml_doc
                           ,p_tag           => 'uits_toelichting_r'
                           ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                           );
      end if;
/*
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen (p_bouwsteen     => 'KGC_UITSLAGEN'
                                   ,p_kolommen      => '*'
                                   ,p_where         => 'uits_id = ' || TO_CHAR(r_uits.uits_id)
                                   ,p_prefix        => 'uits'
                                   );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
*/
      -- persoon
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_XML_PERSOON_VW'
          ,p_kolommen      => '*'
          ,p_where         => 'pers_id = ' || TO_CHAR (r_extra.pers_id)
          ,p_prefix        => 'pers'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- persoon adres
      v_adres1 := kgc_adres_00.persoon (r_extra.pers_id, 'N');
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'pers_adres_r'
                         ,p_xml_regel     => v_nl || Kgc_Xml_00.cr_naar_tags(v_adres1)
                         );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'pers_zwan_duur'
                                   ,p_xml_regel     => toxml(v_pers_zwan_duur)
                                   );
      -- familienrs
      v_tekst := null;
      FOR r_fami IN c_fami(r_extra.pers_id) LOOP
        IF NVL(LENGTH(v_tekst), 0) > 0 THEN
          v_tekst := v_tekst || '; ';
        END IF;
        v_tekst := v_tekst || r_fami.familienummer;
      END LOOP;
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'pers_famnrs'
                                   ,p_xml_regel     => toxml(v_tekst)
                                   );
      -- autorisator - code
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'onde_auth_code'
                                   ,p_xml_regel     => toxml(r_extra.onde_auth_code)
                                   );
      -- ondertekenaars
      v_tekst := r_uits.autorisator;
      v_pos := instr(v_tekst, v_nl);
      if v_pos > 0 then -- 1 regel ondertekenaar...
        v_tekst := substr(v_tekst, 1, v_pos - 1);
      end if;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uits_autorisator'
                         ,p_xml_regel     => toxml(v_tekst)
                         );
      v_tekst := r_uits.autorisator;
      r_mede := r_mede_null;
      open c_mede(p_mede_id => r_uits.mede_id_autorisator, p_kafd_id => r_uits.kafd_id);
      fetch c_mede into r_mede;
      close c_mede;
      -- indien functie gevuld en nog niet aanwezig: toevoegen
      if r_mede.func_omschrijving is not null and
         instr(v_tekst, r_mede.func_omschrijving) = 0
      then
        v_tekst := v_tekst || chr(10) || r_mede.func_omschrijving;
      end if;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uits_teken1_r'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                         );
      -- 2e ondertekenaar
      r_mede := r_mede_null;
      open c_mede(p_mede_id => r_uits.mede_id_ondertekenaar2, p_kafd_id => r_uits.kafd_id);
      fetch c_mede into r_mede;
      close c_mede;
      v_tekst := r_uits.ondertekenaar2;
      if r_mede.func_omschrijving is not null and
         instr(v_tekst, r_mede.func_omschrijving) = 0
      then
        v_tekst := v_tekst || chr(10) || r_mede.func_omschrijving;
      end if;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uits_teken2_r'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(v_tekst)
                         );

      -- huisarts
      v_xml_regel_collection :=
        Kgc_Bouwsteen_00.bouwsteen
                              (p_bouwsteen     => 'KGC_RELATIES'
                              ,p_kolommen      => '*'
                              ,p_where         => 'rela_id = ' || TO_CHAR(NVL(r_extra.rela_id_huis, 0))
                              ,p_prefix        => 'rela_huis'
                              );
      Kgc_Xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- huisarts adres
      v_adres1 := Kgc_Adres_00.relatie (r_extra.rela_id_huis, 'N');
      Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'rela_huis_adres_r'
                         ,p_xml_regel     => v_nl || Kgc_Xml_00.cr_naar_tags(v_adres1)
                         );
      -- onderzoekgegevens

      -- kopiehouders:
      uitv_kopiehouders_cyto ( p_uits_id => r_uits.uits_id
                        , p_rela_id_adr => r_uits_koho.rela_id_adr
                        , p_rela_id_uits => r_uits.rela_id
                        , p_xml_doc => v_xml_doc
                        );

      -- einde 1 brief
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH ('</' || v_xml_subtag || '>')
                           ,'</' || v_xml_subtag || '>'
                           );
      -- per geadresserde/kopiehouder een brief registreren
      IF p_ind_brief = 'J'
      THEN
        IF v_brief_voor_kh THEN -- kopiehouder
          v_ind_kopie := 'J';
          v_geadresseerde := 'Kopiehouder: ' || r_uits_koho.geadresseerde;
        ELSE -- (orginele) aanvrager
          v_ind_kopie := p_ind_kopie;
          v_geadresseerde := r_uits_koho.geadresseerde;
        END IF;

        kgc_brie_00.registreer (p_datum             => SYSDATE
                               ,p_kopie             => v_ind_kopie
                               ,p_brieftype         => NULL -- niet 'UITSLAG'; nu wordt onderstaande brty_id gebruikt!
                               ,p_brty_id           => r_uits.brty_id
                               ,p_pers_id           => r_uits.pers_id
                               ,p_kafd_id           => r_uits.kafd_id
                               ,p_onde_id           => r_uits.onde_id
                               ,p_rela_id           => r_uits_koho.rela_id_adr --r_uits.rela_id
                               ,p_uits_id           => r_uits.uits_id
                               ,p_geadresseerde     => v_geadresseerde
                               ,p_ongr_id           => r_uits.ongr_id
                               ,p_taal_id           => r_uits.taal_id
                               ,p_destype           => p_destype
                               ,p_best_id           => p_best_id
                               ,p_ind_commit        => 'J'
                               ,p_ramo_id           => v_ramo_id
                               );
      END IF;
    END LOOP; -- c_uits_koho
    -- sluit
    kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);

    kgc_xml_00.voegtoe_xml_doc ( p_xml_doc   =>  v_xml_doc);
  END LOOP;
  CLOSE c;
END uits_cyto;

PROCEDURE vtbrf_dna ( -- Vertragingsbrief DNA
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
  ) as
  CR_VertBrf                   Kgc_Bouwsteen_00.t_bouwsteen_cursor;
  CR_Rec MSS_VERTRAGINGSBRFVW%rowtype;
  v_xml_doc                CLOB;
  v_xml_hoofdtag           VARCHAR2 (100) := 'Vertragingsbrieven';


  -- Brief waarin wordt weergegeven dat een onderzoek/uitslag vertraagd is.



Begin
     Kgc_Xml_00.init;

     v_xml_doc := kgc_xml_00.nieuwe_xml (p_naam => v_xml_hoofdtag);
    Open Cr_VertBrf for  'SELECT * FROM ' || p_view_name || ' ' || p_where_clause;

      If Cr_vertBrf%notfound then
          Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'No Data'
                         ,p_xml_regel     => 'NO DATA FOUND'
                         );
      Else

         Fetch CR_VertBrf into Cr_Rec ;


     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Onderzoeknr'
                         ,p_xml_regel     => Cr_rec.Onderzoeknr
                         );


     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'zisnr'
                         ,p_xml_regel     =>Cr_rec.ZisNr
                         );
     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Geboortedatum'
                         ,p_xml_regel     =>  Cr_rec.Geboortedatum
                         );

     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Geslacht'
                         ,p_xml_regel     => Cr_rec.Geslacht
                         );
     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Aanspreken'
                         ,p_xml_regel     => toxml(Cr_rec.Aanspreken)
                         );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Datum_Binnen'
                          ,p_xml_regel     => Cr_rec.Datum_Binnen
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Geplande_Einddatum'
                          ,p_xml_regel     => Cr_rec.Geplande_Einddatum
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'AAnvragend_arts'
                          ,p_xml_regel     => Kgc_Xml_00.cr_naar_tags(Cr_rec.Aanvr_arts)
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Systeemdatum'
                          ,p_xml_regel     => cr_rec.SysteemDat
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'gebruiker'
                          ,p_xml_regel     => cr_rec.Gebruiker
                           );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Indicatie'
                          ,p_xml_regel     => toxml(cr_rec.Omschrijving));

      Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Datum_binnen2'
                          ,p_xml_regel     => cr_rec.Datum_Binnen);
       Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'functie'
                          ,p_xml_regel     => cr_rec.functie);
      Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Aanvr_datum'
                          ,p_xml_regel     => cr_rec.aanvr_datum);



   End If;




     kgc_brie_00.registreer (p_datum             => SYSDATE
                               ,p_kopie             => p_ind_kopie
                               ,p_brieftype         => 'VERTBRF'
                               ,p_brty_id           => null
                               ,p_pers_id           => cr_rec.pers_id
                               ,p_kafd_id           => cr_rec.kafd_id
                               ,p_onde_id           => cr_rec.onde_id
                               ,p_rela_id           => cr_rec.rela_id --r_uits.rela_id
                               ,p_uits_id           => Null
                               ,p_geadresseerde     => cr_rec.Aanspreken
                               ,p_ongr_id           => cr_rec.ongr_id
                               ,p_taal_id           => cr_rec.taal_id
                               ,p_destype           => p_destype
                               ,p_best_id           => p_best_id
                               ,p_ind_commit        => 'J'
                               ,p_ramo_id           => null
                               );

   kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);

   kgc_xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

   Close CR_VertBrf;

End;

PROCEDURE uits_met (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
   )
  IS
    cursor c_ramo (b_brty_id in number) is
      select ramo.ramo_id
      from   kgc_rapport_modules ramo
      ,      kgc_brieftypes brty
      where  brty.brty_id = b_brty_id
      and    brty.ramo_id = ramo.ramo_id
      ;
    cursor c_uits (p_uits_id number) is
      select uits.rela_id
      ,      nvl( mede.formele_naam, mede.naam ) ondertekenaar
      ,      mede.mede_id
      ,      mede2.mede_id mede_id2
      ,      nvl( mede2.formele_naam, mede2.naam ) ondertekenaar2
      ,      nvl( mede3.formele_naam, mede3.naam ) autorisator
      ,      brty.code brty_code
      ,      brty.omschrijving brty_omschrijving
      from   kgc_uitslagen uits
      ,      kgc_medewerkers mede
      ,      kgc_medewerkers mede2
      ,      kgc_medewerkers mede3
      ,      kgc_brieftypes brty
      ,      kgc_onderzoeken onde
      where  uits.uits_id = p_uits_id
      and    uits.brty_id = brty.brty_id (+)
      and    uits.mede_id = mede.mede_id (+)
      and    uits.mede_id2 = mede2.mede_id (+)
      and    uits.onde_id = onde.onde_id
      and    onde.mede_id_autorisator = mede3.mede_id (+)
      ;
    r_uits c_uits%rowtype;
    CURSOR c_mons(p_onde_id NUMBER, p_kafd_id NUMBER, p_ongr_id NUMBER) IS
      select onmo.onde_id onde_id
      ,      onmo.mons_id mons_id
      ,      mons.monsternummer monsternummer
      ,      mate.code mate_code
      ,      mate.omschrijving  mate_omschrijving
      ,      mons.datum_afname datum_afname
      ,      mons.datum_aanmelding datum_aanmelding
      ,      instr( kgc_sypa_00.systeem_waarde( 'KGCUITS51_MATERIAAL_VOLGORDE', p_kafd_id, p_ongr_id), '#'||mate.code||'#') volgorde
      ,      onmo.onmo_id
      from   kgc_materialen mate
      ,      kgc_monsters mons
      ,      kgc_onderzoek_monsters onmo
      where  onmo.onde_id = p_onde_id
      and    mons.mons_id = onmo.mons_id
      and    mate.mate_id = mons.mate_id
      union -- gerelateerd onderzoek
      select reon.onde_id_gerelateerde
      ,      onmo.mons_id
      ,      mons.monsternummer
      ,      mate.code
      ,      mate.omschrijving
      ,      mons.datum_afname
      ,      mons.datum_aanmelding
      ,      instr( kgc_sypa_00.systeem_waarde( 'KGCUITS51_MATERIAAL_VOLGORDE', p_kafd_id, p_ongr_id), '#'||mate.code||'#')
      ,      onmo.onmo_id
      from   kgc_materialen mate
      ,      kgc_monsters mons
      ,      kgc_onderzoek_monsters onmo
      ,      kgc_gerelateerde_onderzoeken reon
      where  reon.onde_id_gerelateerde = p_onde_id
      and    reon.onde_id = onmo.onde_id
      and    mons.mons_id = onmo.mons_id
      and    mate.mate_id = mons.mate_id
/*      and    not exists
             ( select null
               from   kgc_onderzoek_monsters onmo2
               where  onmo2.onde_id = p_onde_id
               and    onmo2.mons_id = mons.mons_id
            ) ... v3: niet doen, want ook gebruikt om onmo-id te selecteren... dubbele monsters eruit halen bij 1e lijst */
      order by  8 asc -- via sypa
      ,             4 desc -- mate_code
      ,             3 asc -- monsternummer
      ;
    r_mons_vorig c_mons%rowtype;
    r_mons_null c_mons%rowtype;
    cursor c_urine (p_onde_id number) is
      select distinct mate.omschrijving || ' ' || mons.monsternummer urine_monster
      ,      onmo.onmo_id
      ,      kgc_cate_00.categorie( 'CATEGORIE_EINDBRIEF'
                                  , mate.kafd_id
                                  , NULL
                                  , 'KGC_MATERIALEN'
                                  , mate.mate_id
                                  ) cate_id
      from   kgc_materialen mate
      ,      kgc_monsters mons
      ,      kgc_onderzoek_monsters onmo
      where  onmo.onde_id = p_onde_id -- hier niet de gerelateerde onderzoeken!
      and    mons.mons_id = onmo.mons_id
      and    mate.mate_id = mons.mate_id
      and    upper(mate.omschrijving) like '%URINE%'
      order by urine_monster
      ;
    cursor c_urine_meet (p_onmo_id number, p_cate_id number, p_kafd_id number) is
      select  distinct
              cawa.volgorde
      ,       cawa.beschrijving  urine_stoftest
      ,       kgc_util_00.voor_deci_punt( bas_meet_00.meetwaarde_bij_monster(meti.onmo_id,stof.stof_id, 'WAARDE' )) urine_voor_decimaal
      ,       kgc_util_00.na_deci_punt( bas_meet_00.meetwaarde_bij_monster(meti.onmo_id,stof.stof_id, 'WAARDE' )) urine_na_decimaal
      ,       bas_meet_00.meetwaarde_bij_monster(meti.onmo_id,stof.stof_id, 'EENHEID' ) urine_eenheid
      from    kgc_categorie_waarden cawa
      ,       kgc_stoftesten stof
      ,       bas_metingen meti
      where   meti.onmo_id (+) = p_onmo_id
      AND     kgc_maastricht.tpmt_protocol( meti.stgr_id ) = 'N'
      and     cawa.cate_id = p_cate_id
      and     stof.kafd_id = p_kafd_id
      and     stof.code = cawa.waarde
      order by cawa.volgorde
      ;
    cursor c_urine_meti (p_onmo_id number, p_cate_id number, p_kafd_id number) is
      SELECT stgr.omschrijving stgr_omschrijving
      ,      meti.conclusie meti_conclusie
      ,      meti.meti_id
      ,      stgr.code stgr_code
      FROM   kgc_stoftestgroepen stgr
      ,      bas_metingen meti
      WHERE  meti.onmo_id = p_onmo_id
      AND    meti.stgr_id = stgr.stgr_id
      AND    kgc_maastricht.tpmt_protocol( meti.stgr_id ) = 'N'
      AND    meti.afgerond= 'J'
      AND    meti.conclusie IS NOT NULL
      and    not exists
             ( select null
               from   kgc_categorie_waarden cawa2
               ,      kgc_stoftesten stof2
               ,      bas_metingen meti2
               ,      bas_meetwaarden meet2
               where  meti2.onmo_id = meti.onmo_id
               and    meti2.stgr_id = meti.stgr_id
               and    meti2.meti_id = meet2.meti_id
               and    meet2.stof_id = stof2.stof_id
               and    stof2.kafd_id = p_kafd_id
               and    stof2.code = cawa2.waarde
               and    cawa2.cate_id = p_cate_id
               and    kgc_maastricht.kgcuits51_urine_stoftest( stof2.code ) = 'N'
             )
      ORDER BY stgr.code asc
      ,        meti.meti_id asc
      ;
    cursor c_n_urine_meti (p_onde_id number, p_kafd_id number, p_ongr_id number) is
      SELECT onmo.onde_id onde_id
      ,      mate.code mate_code
      ,      mate.omschrijving || ' ' || mons.monsternummer mate_omschrijving
      ,      stgr.omschrijving stgr_omschrijving
      ,      meti.conclusie meti_conclusie
      ,      instr( kgc_sypa_00.systeem_waarde( 'KGCUITS51_MATERIAAL_VOLGORDE', p_kafd_id, p_ongr_id), '#'||mate.code||'#') volgorde
      ,      stgr.code stgr_code
      FROM   kgc_materialen mate
      ,      kgc_stoftestgroepen stgr
      ,      kgc_monsters mons
      ,      bas_metingen meti
      ,      kgc_onderzoek_monsters onmo
      WHERE  onmo.onde_id = p_onde_id
      AND    mons.mons_id = onmo.mons_id
      AND    mate.mate_id = mons.mate_id
      AND    onmo.onmo_id = meti.onmo_id
      AND    meti.stgr_id = stgr.stgr_id
      AND    kgc_maastricht.tpmt_protocol( meti.stgr_id ) = 'N'
      AND    meti.afgerond= 'J'
      AND    meti.conclusie IS NOT NULL
      AND    UPPER(mate.omschrijving ) NOT LIKE '%URINE%'
      union
      SELECT reon.onde_id_gerelateerde
      ,      mate.code
      ,      mate.omschrijving || ' ' || mons.monsternummer mate_omschrijving
      ,      stgr.omschrijving stgr_omschrijving
      ,      meti.conclusie meti_conclusie
      ,      instr( kgc_sypa_00.systeem_waarde( 'KGCUITS51_MATERIAAL_VOLGORDE', p_kafd_id, p_ongr_id), '#'||mate.code||'#')
      ,      stgr.code stgr_code
      FROM   kgc_materialen mate
      ,      kgc_stoftestgroepen stgr
      ,      kgc_monsters mons
      ,      bas_metingen meti
      ,      kgc_onderzoek_monsters onmo
      ,      kgc_gerelateerde_onderzoeken reon
      WHERE  onmo.onde_id  = reon.onde_id
      and    reon.onde_id_gerelateerde = p_onde_id
      AND    mons.mons_id  = onmo.mons_id
      AND    mate.mate_id   = mons.mate_id
      AND    onmo.onmo_id = meti.onmo_id
      AND    meti.stgr_id = stgr.stgr_id
      AND    kgc_maastricht.tpmt_protocol( meti.stgr_id ) = 'N'
      AND    meti.afgerond= 'J'
      AND    meti.conclusie IS NOT NULL
      AND    UPPER(mate.omschrijving ) NOT LIKE '%URINE%'
      ORDER BY 6 asc -- via sypa
      ,        3 asc -- mate_omschrijving
      ,        7 asc -- stgr_code
      ;

    cursor c_meti (p_onmo_id number) is
      select meti.meti_id
      ,      meti.stgr_id stgr_id
      ,      stgr.code  stgr_code
      ,      stgr.omschrijving stgr_omschrijving
      ,      kgc_attr_00.waarde( 'KGC_STOFTESTGROEPEN', 'EENHEID_MEETWAARDE', stgr.stgr_id) meeteenheid
      ,      kgc_attr_00.waarde( 'KGC_STOFTESTGROEPEN', 'EENHEID_UITSLAG', stgr.stgr_id) meet_uitslageenheid
      ,      prst.volgorde volgorde
      ,      stof.omschrijving stof_omschrijving
      ,      kgc_util_00.voor_deci_punt(bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'MEETWAARDE')) meetwaarde_voor_decimaal
      ,      kgc_util_00.na_deci_punt( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'MEETWAARDE')) meetwaarde_na_decimaal
      ,      kgc_util_00.voor_deci_punt(bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'UITSLAG')) meet_uitslag_voor_decimaal
      ,      kgc_util_00.na_deci_punt( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'UITSLAG')) meet_uitslag_na_decimaal
      ,      DECODE( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'WAARDE')
                            , NULL , NULL
                            , kgc_util_00.voor_deci_punt( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'ONDERGRENS') )
                            ) norm_ondergrens_voor_decimaal
      ,      DECODE( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'WAARDE')
                            , NULL, NULL
                            , kgc_util_00.na_deci_punt( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'ONDERGRENS') )
                            ) norm_ondergrens_na_decimaal
      ,      DECODE( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'WAARDE')
                            , NULL, NULL
                            , kgc_util_00.voor_deci_punt( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'BOVENGRENS') )
                            ) norm_bovengrens_voor_decimaal
      ,      DECODE( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'WAARDE')
                            , NULL, NULL
                            , kgc_util_00.na_deci_punt( bas_meet_00.meetwaarde_bij_monster(p_onmo_id, prst.stof_id, meti.stgr_id, 'BOVENGRENS') )
                            ) norm_bovengrens_na_decimaal
      from   kgc_stoftestgroepen stgr
      ,      bas_metingen meti
      ,      kgc_stoftesten stof
      ,      kgc_protocol_stoftesten prst
      where  meti.onmo_id = p_onmo_id
      and    meti.stgr_id = stgr.stgr_id
      and    exists
             ( select null
               from   kgc_stoftestgroep_gebruik stgg
               where  stgg.stgr_id = meti.stgr_id
               and    stgg.getal_opnemen = 'J'
             )
      and    kgc_maastricht.tpmt_protocol( meti.stgr_id ) = 'N'
      and    meti.afgerond= 'J'                     -- Mantis 1145
      and    meti.conclusie IS NOT NULL  -- Mantis 1145
      and    prst.stgr_id = stgr.stgr_id
      and    prst.stof_id = stof.stof_id
      and    stof.vervallen = 'N'
      order by stgr.code
      ,      prst.volgorde
      ,      stof.omschrijving
      ;

    Cursor C_mede (pmedeID integer)is
                     Select m.waarde omschrijving
                     From kgc_attribuut_waarden m
                     Where m.id = pmedeid
                     And   m.attr_id = 375;

    R_mede C_mede%rowtype;


    v_rela_adres             VARCHAR2 (4000);
    v_pers_adres             VARCHAR2 (4000);
    v_xml_doc                CLOB;
    v_xml_regel              kgc_xml_00.t_xml_regel;
    v_xml_regel_collection   kgc_xml_00.t_xml_regel_collection;
    v_datum_print            DATE                                := sysdate;
    v_nl            CONSTANT VARCHAR2 (2)                        := chr (10);
    v_sql                    VARCHAR2 (1000);
    v_tekst                  VARCHAR2 (1000);
    v_xml_hoofdtag           VARCHAR2 (100) := 'uitslagbrieven';
    v_xml_subtag             VARCHAR2 (100) := 'uitslagbrief';
    c_xvvz                   kgc_bouwsteen_00.t_bouwsteen_cursor;
    r_xvvz                   KGC_UITSLAGBRIEF_UTMETAB_VW%ROWTYPE;
    v_ramo_id                number;
    v_stgr_id                number;
    v_uits_id                number := 0;
    v_where                  VARCHAR2(4000);
    v_dumm_id                number;
    v_mate_omschrijving_vorig VARCHAR2(200);
    v_urine_aanw             BOOLEAN;
    v_urine_urme_aanw        BOOLEAN;
    v_urine_urcc_aanw        BOOLEAN;
    v_n_urine_aanw           BOOLEAN;
  BEGIN
    -- hoofdselectie
    kgc_xml_00.init;

    -- let op: in kgc_uitslagbrief_vw zitten de briefgegevens voor de aanvrager EN alle kopiehouders!
    -- maak nieuw xml document aan
    v_xml_doc := kgc_xml_00.nieuwe_xml (p_naam     => v_xml_hoofdtag);

    OPEN c_xvvz FOR 'SELECT * FROM KGC_UITSLAGBRIEF_UTMETAB_VW ' || p_where_clause || ' ORDER BY decode(koho_id, null,9,1) desc';

    LOOP
      FETCH c_xvvz
       INTO r_xvvz;

      EXIT WHEN c_xvvz%NOTFOUND;
      -- check op 1 uits-id!
      IF c_xvvz%ROWCOUNT = 1 THEN
        v_uits_id := r_xvvz.uits_id;
      END IF;
      IF v_uits_id <> r_xvvz.uits_id THEN
        qms$errors.show_message
        ( p_mesg => 'KGC-00000'
        , p_param0 => 'Er kan alleen een brief per uitslag aangemaakt worden!'
        , p_param1 => 'beh_xml_doc.uits_met rekent hierop!'
        , p_errtp => 'E'
        , p_rftf => true
        );
      END IF;
      open c_uits (p_uits_id => v_uits_id);
      fetch c_uits into r_uits;
      close c_uits;
      -- voor metabool: aanvrager + iedere kopiehouder z'n eigen brief! (via view)
      -- begin 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('<' || v_xml_subtag || '>' || v_nl)
                           , '<' || v_xml_subtag || '>' || v_nl
                           );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'datum_print'
                                   ,p_xml_regel     => to_char (v_datum_print, 'DD-MM-YYYY')
                                   );
      -- referentie
      v_tekst := kgc_maastricht.referentie(r_xvvz.onde_id, r_xvvz.onderzoeknr);
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'onde_onze_referentie'
                                   ,p_xml_regel     => v_tekst
                                   );
      -- kopie
      v_tekst := NULL;
      IF kgc_brie_00.eerder_geprint(r_xvvz.onde_id, NULL, r_xvvz.brty_id) = 'N' THEN
        IF r_uits.rela_id <> r_xvvz.rela_id THEN
          v_tekst := 'Brief voor kopiehouder';
        END IF;
      ELSE
        IF r_uits.rela_id <> r_xvvz.rela_id THEN
          v_tekst := 'Kopiebrief voor kopiehouder';
        ELSE
          v_tekst := 'Kopie';
        END IF;
      END IF;
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'kopie_tekst'
                                   ,p_xml_regel     => v_tekst
                                   );
      -- brieftype
      v_tekst := NULL;
      IF r_uits.brty_code = 'EINDBRIEF' THEN
        v_tekst := 'Uitslag metabool onderzoek';
      ELSIF r_uits.brty_code = 'UITSLAG_AV' THEN
        v_tekst := 'Aanvullende uitslag metabool onderzoek ' ||
          to_char(kgc_brie_00.datum_eerdere_brief(r_xvvz.onde_id, r_xvvz.brty_id), 'dd-mm-yyyy');
      ELSE
        v_tekst := r_uits.brty_omschrijving;
      END IF;
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'brieftype'
                                   ,p_xml_regel     => v_tekst
                                   );
      -- geadresseerde
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_adres'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_xvvz.geadresseerde)
                         );
      -- haal persoon gegevens, via bouwsteen
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_PERSONEN'
          ,p_kolommen      => 'zisnr, aanspreken, to_char(geboortedatum, ''dd-mm-yyyy'') geboortedatum, geslacht'
          ,p_where         => 'pers_id = ' || to_char (r_xvvz.pers_id)
          ,p_prefix        => 'pers'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- headerpersoon gegevens,
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'pers_vervolgvel_info'
                         ,p_xml_regel     => v_nl || kgc_pers_00.persoon_info(null, r_xvvz.onde_id, r_xvvz.pers_id, 'VERVOLGVEL')
                         );
      -- monstergegevens
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH ('<monss>')
                           ,'<monss>'
                           );
      -- v3: dubbele monsters eruit filteren
      r_mons_vorig := r_mons_null;
      FOR r_mons IN c_mons(r_xvvz.onde_id, r_xvvz.kafd_id, r_xvvz.ongr_id) LOOP
        IF r_mons.monsternummer = nvl(r_mons_vorig.monsternummer, '#@#') THEN
          NULL; -- niets doen
        ELSE
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<mons>')
                               ,'<mons>'
                               );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'mons_monsternummer'
                                      , p_xml_regel     => toxml(r_mons.monsternummer)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'mons_mate_omschrijving'
                                      , p_xml_regel     => toxml(r_mons.mate_omschrijving)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'mons_volume'
                                      , p_xml_regel     => toxml(kgc_maastricht.volume(p_mons_id => r_mons.mons_id))
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'mons_datum_afname'
                                      , p_xml_regel     => toxml(to_char(r_mons.datum_afname, 'dd-mm-yyyy'))
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'mons_datum_aanmelding'
                                      , p_xml_regel     => toxml(to_char(r_mons.datum_aanmelding, 'dd-mm-yyyy'))
                                      );
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('</mons>')
                               ,'</mons>'
                               );
        END IF;
        r_mons_vorig := r_mons;
      END LOOP;
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH ('</monss>')
                           ,'</monss>'
                           );

      -- urinegegevens - alleen wegschrijven indien aanwezig!
      v_urine_aanw := FALSE;
      FOR r_urine IN c_urine(r_xvvz.onde_id) LOOP
        IF NOT v_urine_aanw THEN
          v_urine_aanw := TRUE;
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<urines>')
                               ,'<urines>'
                               );
        END IF;
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('<urine>')
                             ,'<urine>'
                             );
        kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                    , p_tag           => 'urine_monster'
                                    , p_xml_regel     => toxml(r_urine.urine_monster)
                                    );
        -- alle stoftesten / meetwaarden - alleen indien aanwezig
        v_urine_urme_aanw := FALSE;
        FOR r_urine_meet IN c_urine_meet(p_onmo_id => r_urine.onmo_id, p_cate_id => r_urine.cate_id, p_kafd_id => r_xvvz.kafd_id) LOOP
          IF NOT v_urine_urme_aanw THEN
            v_urine_urme_aanw := TRUE;
            dbms_lob.writeappend (v_xml_doc
                                 ,LENGTH ('<urmes>')
                                 ,'<urmes>'
                                 );
          END IF;
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<urme>')
                               ,'<urme>'
                               );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'urme_stoftest'
                                      , p_xml_regel     => toxml(r_urine_meet.urine_stoftest)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'urme_voor_decimaal'
                                      , p_xml_regel     => toxml(r_urine_meet.urine_voor_decimaal)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'urme_na_decimaal'
                                      , p_xml_regel     => toxml(r_urine_meet.urine_na_decimaal)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'urme_eenheid'
                                      , p_xml_regel     => toxml(r_urine_meet.urine_eenheid)
                                      );
          -- alle
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('</urme>')
                               ,'</urme>'
                               );
        END LOOP;
        IF v_urine_urme_aanw THEN
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('</urmes>')
                               ,'</urmes>'
                               );
        END IF;

        -- alle urine conclusies
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('<urccs>')
                             ,'<urccs>'
                             );
        FOR r_urine_meti IN c_urine_meti(p_onmo_id => r_urine.onmo_id, p_cate_id => r_urine.cate_id, p_kafd_id => r_xvvz.kafd_id) LOOP
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<urcc>')
                               ,'<urcc>'
                               );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'urcc_stgr_omschrijving'
                                      , p_xml_regel     => toxml(r_urine_meti.stgr_omschrijving)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'urcc_meti_conclusie'
                                      , p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_urine_meti.meti_conclusie)
                                      );
          -- alle
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('</urcc>')
                               ,'</urcc>'
                               );
        END LOOP;
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</urccs>')
                             ,'</urccs>'
                             );

        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</urine>')
                             ,'</urine>'
                             );
      END LOOP;
      IF v_urine_aanw THEN
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</urines>')
                             ,'</urines>'
                             );
      END IF;
      -- alle niet-urine uitslagen - alleen wegschrijven indien aanwezig!
      v_n_urine_aanw := FALSE;
      v_mate_omschrijving_vorig := null;
      FOR r_n_urine_meti IN c_n_urine_meti(p_onde_id => r_xvvz.onde_id, p_kafd_id => r_xvvz.kafd_id, p_ongr_id => r_xvvz.ongr_id) LOOP
        IF NOT v_n_urine_aanw THEN
          v_n_urine_aanw := TRUE;
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<n_urms>') -- niet-urine-materiaal
                               ,'<n_urms>'
                               );
        END IF;
        IF nvl(v_mate_omschrijving_vorig, '@#@') <> r_n_urine_meti.mate_omschrijving THEN -- nieuw materiaal
          IF v_mate_omschrijving_vorig IS NOT NULL THEN -- vorige groep afsluiten
            dbms_lob.writeappend (v_xml_doc
                                 ,LENGTH ('</n_urm_metis>')
                                 ,'</n_urm_metis>'
                                 );
            dbms_lob.writeappend (v_xml_doc
                                 ,LENGTH ('</n_urm>')
                                 ,'</n_urm>'
                                 );
          END IF;
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<n_urm>')
                               ,'<n_urm>'
                               );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'n_urm_mate_omschrijving'
                                      , p_xml_regel     => toxml(r_n_urine_meti.mate_omschrijving)
                                      );
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<n_urm_metis>')
                               ,'<n_urm_metis>'
                               );
          v_mate_omschrijving_vorig := r_n_urine_meti.mate_omschrijving;
        END IF;
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('<n_urm_meti>')
                             ,'<n_urm_meti>'
                             );
        kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                    , p_tag           => 'n_urmm_stgr_omschrijving'
                                    , p_xml_regel     => toxml(r_n_urine_meti.stgr_omschrijving)
                                    );
        kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                    , p_tag           => 'n_urmm_meti_conclusie'
                                    , p_xml_regel     => toxml(r_n_urine_meti.meti_conclusie)
                                    );
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</n_urm_meti>')
                             ,'</n_urm_meti>'
                             );
      END LOOP; -- niet-urine uitslagen
      IF v_mate_omschrijving_vorig IS NOT NULL THEN -- laatste groep afsluiten
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</n_urm_metis>')
                             ,'</n_urm_metis>'
                             );
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</n_urm>')
                             ,'</n_urm>'
                             );
      END IF;
      IF v_n_urine_aanw THEN
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</n_urms>')
                             ,'</n_urms>'
                             );
      END IF;

      -- haal uitslag gegevens, via bouwsteen
      v_where := 'uits_id = ' || to_char (r_xvvz.uits_id);
      IF r_xvvz.koho_id IS NULL THEN
        v_where := v_where || ' AND koho_id IS NULL';
      ELSE
        v_where := v_where || ' AND koho_id = ' || to_char (r_xvvz.koho_id);
      END IF;
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_UITSLAGBRIEF_UTMETAB_VW'
          ,p_kolommen      => 'onde_referentie'
                              || ', onderzoeknr, materiaal, to_char(afname_datum, ''dd-mm-yyyy'') afname_datum'
                              || ', to_char(datum_autorisatie, ''dd-mm-yyyy'') datum_autorisatie'
          ,p_where         => v_where
          ,p_prefix        => 'uitv'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);

      -- uitslag_ondertekenaars (meerdere regels)



      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'onde_autorisator'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_uits.autorisator)
                         );
      Open  C_mede(r_uits.mede_id);
          Fetch C_mede into R_mede ;
      close c_mede;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_ondertekenaar'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_uits.ondertekenaar)
                         );

       kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_Functie_onderteken1'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_mede.omschrijving)
                         );
      r_mede.omschrijving := '';
      Open  C_mede(r_uits.mede_id2);
          Fetch C_mede into R_mede ;
      close c_mede;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_ondertekenaar2'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_uits.ondertekenaar2)
                         );

       kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_Functie_onderteken2'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_mede.omschrijving)
                         );

      -- uitslag_tekst
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_uitslag_tekst_f'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_xvvz.uitslag_tekst)
                         );
      -- toelichting
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_toelichting_f'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_xvvz.toelichting)
                         );

      -- headeruitslag gegevens, via bouwsteen
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_UITSLAGBRIEF_UTMETAB_VW'
          ,p_kolommen      => 'onderzoeknr, materiaal, to_char(afname_datum, ''dd-mm-yyyy'') afname_datum'
          ,p_where         => v_where
          ,p_prefix        => 'uitv2'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);

      -- kopiehouders alleen indien geadresseerde = aanvrager
      uitv_kopiehouders ( p_uits_id => r_xvvz.uits_id
                        , p_rela_id_adr => r_xvvz.rela_id
                        , p_rela_id_uits => r_uits.rela_id
                        , p_xml_doc => v_xml_doc
                        );

      --
      -- In verband met de grootte van de files worden de bijlagen tussen <metis< ? >metis> niet uitgevoerd voor kopiehouders
      --
      IF r_uits.rela_id <> r_xvvz.rela_id THEN
        null;
      ELSE

        -- voor de bijlagen:
        -- alle monsters met bijbehorende protocollen
        --   per protocol de stoftesten + uitslagen
        FOR r_mons IN c_mons(r_xvvz.onde_id, r_xvvz.kafd_id, r_xvvz.ongr_id) LOOP
          --   per monster de protocollen
          dbms_lob.writeappend ( v_xml_doc, LENGTH ('<metis>' || v_nl), '<metis>' || v_nl);
          v_stgr_id := 0;
          FOR r_meti IN c_meti (r_mons.onmo_id) LOOP
            IF v_stgr_id <> r_meti.stgr_id THEN -- nieuwe groep!
              v_stgr_id := r_meti.stgr_id;
              IF c_meti%ROWCOUNT > 1 THEN -- afsluiten
                dbms_lob.writeappend ( v_xml_doc, LENGTH ('</meets>' || v_nl), '</meets>' || v_nl);
                dbms_lob.writeappend ( v_xml_doc, LENGTH ('</meti>' || v_nl), '</meti>' || v_nl);
              END IF;
              dbms_lob.writeappend ( v_xml_doc, LENGTH ('<meti>' || v_nl), '<meti>' || v_nl);
              kgc_xml_00.voegtoe_xml_regel
                                 (p_xml_clob      => v_xml_doc
                                 ,p_tag           => 'meti_stgr_omschrijving'
                                 ,p_xml_regel     => toxml(r_meti.stgr_omschrijving)
                                 );
              kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                          , p_tag           => 'meti_monsternummer'
                                          , p_xml_regel     => toxml(r_mons.monsternummer)
                                          );
              kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                          , p_tag           => 'meti_mate_omschrijving'
                                          , p_xml_regel     => toxml(r_mons.mate_omschrijving)
                                          );
              kgc_xml_00.voegtoe_xml_regel
                                 (p_xml_clob      => v_xml_doc
                                 ,p_tag           => 'meti_meeteenheid'
                                 ,p_xml_regel     => toxml(r_meti.meeteenheid)
                                 );
              kgc_xml_00.voegtoe_xml_regel
                                 (p_xml_clob      => v_xml_doc
                                 ,p_tag           => 'meti_meet_uitslageenheid'
                                 ,p_xml_regel     => toxml(r_meti.meet_uitslageenheid)
                                 );
              kgc_xml_00.voegtoe_xml_regel
                                 (p_xml_clob      => v_xml_doc
                                 ,p_tag           => 'meti_meet_uitslageenheid2'
                                 ,p_xml_regel     => toxml(r_meti.meet_uitslageenheid)
                                 );
              dbms_lob.writeappend ( v_xml_doc, LENGTH ('<meets>' || v_nl), '<meets>' || v_nl);
            END IF;
            -- per meting de groep meetwaarden
            -- meet-groep beginnen
            dbms_lob.writeappend ( v_xml_doc, LENGTH ('<meet>' || v_nl), '<meet>' || v_nl);
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_stof_omschrijving'
                               ,p_xml_regel     => toxml(r_meti.stof_omschrijving)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_mw_vd'
                               ,p_xml_regel     => toxml(r_meti.meetwaarde_voor_decimaal)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_mw_nd'
                               ,p_xml_regel     => toxml(r_meti.meetwaarde_na_decimaal)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_ui_vd'
                               ,p_xml_regel     => toxml(r_meti.meet_uitslag_voor_decimaal)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_ui_nd'
                               ,p_xml_regel     => toxml(r_meti.meet_uitslag_na_decimaal)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_nog_vd'
                               ,p_xml_regel     => toxml(r_meti.norm_ondergrens_voor_decimaal)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_nog_nd'
                               ,p_xml_regel     => toxml(r_meti.norm_ondergrens_na_decimaal)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_nbg_vd'
                               ,p_xml_regel     => toxml(r_meti.norm_bovengrens_voor_decimaal)
                               );
            kgc_xml_00.voegtoe_xml_regel
                               (p_xml_clob      => v_xml_doc
                               ,p_tag           => 'meet_nbg_nd'
                               ,p_xml_regel     => toxml(r_meti.norm_bovengrens_na_decimaal)
                              );

            dbms_lob.writeappend ( v_xml_doc, LENGTH ('</meet>' || v_nl), '</meet>' || v_nl);
          END LOOP;
          IF v_stgr_id <> 0 THEN -- laatste afsluiten
            dbms_lob.writeappend ( v_xml_doc, LENGTH ('</meets>' || v_nl), '</meets>' || v_nl);
            dbms_lob.writeappend ( v_xml_doc, LENGTH ('</meti>' || v_nl), '</meti>' || v_nl);
          END IF;
          dbms_lob.writeappend ( v_xml_doc, LENGTH ('</metis>' || v_nl), '</metis>' || v_nl);
        END LOOP; -- einde monsters
      END IF; -- einde bijlage niet afdrukken voor kopiehouders

      -- einde 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('</' || v_xml_subtag || '>' || v_nl)
                           , '</' || v_xml_subtag || '>' || v_nl
                           );

      -- evt EN igv eerste uitslag: registreren
      IF p_ind_brief = 'J' AND c_xvvz%ROWCOUNT = 1
      THEN
        open c_ramo (b_brty_id => r_xvvz.brty_id);
        fetch c_ramo into v_ramo_id;
        close c_ramo;

        kgc_brie_00.registreer (p_datum             => sysdate
                               ,p_kopie             => p_ind_kopie
                               ,p_brieftype         => r_uits.brty_code --'UITSLAG'
                               ,p_brty_id           => r_xvvz.brty_id
                               ,p_pers_id           => r_xvvz.pers_id
                               ,p_kafd_id           => r_xvvz.kafd_id
                               ,p_onde_id           => r_xvvz.onde_id
                               ,p_rela_id           => r_xvvz.rela_id
                               ,p_uits_id           => r_xvvz.uits_id
                               ,p_geadresseerde     => r_xvvz.geadresseerde
                               ,p_ongr_id           => r_xvvz.ongr_id
                               ,p_taal_id           => r_xvvz.taal_id
                               ,p_destype           => p_destype
                               ,p_best_id           => p_best_id
                               ,p_ind_commit        => 'J'
                               ,p_ramo_id           => v_ramo_id
                               );
      END IF;
    END LOOP;

    CLOSE c_xvvz;

    kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);
    kgc_xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

  END uits_met;

  /******************************************************************************
     NAME:       uits_met_tpmt
     PURPOSE:    primair gebruikt voor uitslag Metabool/Enzym (incl registreren brief)

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        13-8-2008    RDE             1. Aangemaakt

     NOTES:

  ******************************************************************************/
  PROCEDURE uits_met_tpmt (
    p_view_name        IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
   )
  IS
    cursor c_ramo (b_brty_id in number) is
      select ramo.ramo_id
      from   kgc_rapport_modules ramo
      ,      kgc_brieftypes brty
      where  brty.brty_id = b_brty_id
      and    brty.ramo_id = ramo.ramo_id
      ;
    cursor c_uits (p_uits_id number) is
      select uits.rela_id
      ,      nvl( mede.formele_naam, mede.naam ) ondertekenaar
      ,      mede.mede_id
      ,      mede2.mede_id mede_id2
      ,      nvl( mede2.formele_naam, mede2.naam ) ondertekenaar2
      ,      nvl( mede3.formele_naam, mede3.naam ) autorisator
      ,      brty.code brty_code
      ,      brty.omschrijving brty_omschrijving
      from   kgc_uitslagen uits
      ,      kgc_medewerkers mede
      ,      kgc_medewerkers mede2
      ,      kgc_medewerkers mede3
      ,      kgc_brieftypes brty
      ,      kgc_onderzoeken onde
      where  uits.uits_id = p_uits_id
      and    uits.brty_id = brty.brty_id (+)
      and    uits.mede_id = mede.mede_id (+)
      and    uits.mede_id2 = mede2.mede_id (+)
      and    uits.onde_id = onde.onde_id
      and    onde.mede_id_autorisator = mede3.mede_id (+)
      ;
    r_uits c_uits%rowtype;
    cursor c_mons1(p_onde_id number) is
      select mate.omschrijving mate_omschrijving
      ,      mons.datum_aanmelding
      ,      mons.monsternummer
      from   kgc_materialen mate
      ,      kgc_monsters mons
      ,      kgc_onderzoek_monsters onmo
      where  onmo.onde_id = p_onde_id
      and    onmo.mons_id = mons.mons_id
      and    mons.mate_id = mate.mate_id
      and    exists (select null from bas_metingen meti
                     where meti.onmo_id = onmo.onmo_id
                     and kgc_maastricht.tpmt_protocol( meti.stgr_id) = 'J'
                    )
      ;
    r_mons1 c_mons1%rowtype;
    cursor c_meti(p_onde_id number) is
      select meti.onde_id
      ,      meti.meti_id
      ,      meti.stgr_id
      ,      stgr.omschrijving stgr_omschrijving
      ,      kgc_attr_00.waarde('KGC_STOFTESTGROEPEN', 'ACTIVITEIT', stgr.stgr_id) kop_activiteit
      ,      meti.conclusie resultaat
      from   kgc_stoftestgroepen stgr
      ,      bas_metingen meti
      where  meti.onde_id = p_onde_id
      and    meti.stgr_id = stgr.stgr_id
      and    kgc_maastricht.tpmt_protocol( stgr.stgr_id ) = 'J'
      and  ( meti.afgerond = 'J' OR exists (select 1 from bas_meetwaarden meet where meet.meti_id = meti.meti_id and meet.afgerond = 'J'))
      order by stgr.omschrijving desc
    ;
    cursor c_meet(p_meti_id number, p_stgr_id number) is
      select meet.tonen_als waarde
      ,      kgc_attr_00.waarde('KGC_STOFTESTEN', 'MEAN_SD', prst.stof_id) referentie_waarden
      ,      kgc_attr_00.waarde('KGC_STOFTESTEN', 'RANGE', prst.stof_id) range
      ,      meet.meeteenheid eenheid
      ,      prst.volgorde
      from   bas_meetwaarde_statussen mest
      ,      kgc_protocol_stoftesten prst
      ,      bas_meetwaarden meet
      ,      bas_metingen meti
      where  meti.meti_id = p_meti_id
      and    meet.meti_id = meti.meti_id
      and    meet.stof_id = prst.stof_id
      and    prst.stgr_id = p_stgr_id
      and    meet.meet_reken = 'R'
      and  ( meet.afgerond = 'J' or meti.afgerond = 'J' )
      and    meet.mest_id = mest.mest_id (+)
      and    nvl(mest.in_uitslag,'J') = 'J'
      order by prst.volgorde asc
    ;

      Cursor C_mede (pmedeID integer)is
                     Select m.waarde omschrijving
                     From kgc_attribuut_waarden m
                     Where m.id = pmedeid
                     And   m.attr_id = 375;

    R_mede C_mede%rowtype;
    v_rela_adres             VARCHAR2 (4000);
    v_pers_adres             VARCHAR2 (4000);
    v_xml_doc                CLOB;
    v_xml_regel              kgc_xml_00.t_xml_regel;
    v_xml_regel_collection   kgc_xml_00.t_xml_regel_collection;
    v_datum_print            DATE                                := sysdate;
    v_nl            CONSTANT VARCHAR2 (2)                        := chr (10);
    v_sql                    VARCHAR2 (1000);
    v_tekst                  VARCHAR2 (1000);
    v_xml_hoofdtag           VARCHAR2 (100) := 'uitslagbrieven';
    v_xml_subtag             VARCHAR2 (100) := 'uitslagbrief';
    c_xvvz                   kgc_bouwsteen_00.t_bouwsteen_cursor;
    r_xvvz                   KGC_UITSLAGBRIEF_VW%ROWTYPE;
    v_ramo_id                number;
    v_stgr_id                number;
    v_uits_id                number := 0;
    v_where                  VARCHAR2(4000);
    v_dumm_id                number;
  BEGIN
    -- hoofdselectie
    kgc_xml_00.init;

    -- let op: in kgc_uitslagbrief_vw zitten de briefgegevens voor de aanvrager EN alle kopiehouders!
    -- maak nieuw xml document aan
    v_xml_doc := kgc_xml_00.nieuwe_xml (p_naam     => v_xml_hoofdtag);

    OPEN c_xvvz FOR 'SELECT * FROM KGC_UITSLAGBRIEF_VW ' || p_where_clause || ' ORDER BY onderzoeknr, decode(koho_id, null,9,1) desc';

    LOOP
      FETCH c_xvvz
       INTO r_xvvz;

      EXIT WHEN c_xvvz%NOTFOUND;
      -- check op 1 uits-id!
      IF c_xvvz%ROWCOUNT = 1 THEN
        v_uits_id := r_xvvz.uits_id;
      END IF;
      IF v_uits_id <> r_xvvz.uits_id THEN
        qms$errors.show_message
        ( p_mesg => 'KGC-00000'
        , p_param0 => 'Er kan alleen een brief per uitslag aangemaakt worden!'
        , p_param1 => 'beh_xml_doc.uits_met_tpmt rekent hierop!'
        , p_errtp => 'E'
        , p_rftf => true
        );
      END IF;
      open c_uits (p_uits_id => v_uits_id);
      fetch c_uits into r_uits;
      close c_uits;
      -- voor metabool: aanvrager + iedere kopiehouder z'n eigen brief! (via view)
      -- begin 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('<' || v_xml_subtag || '>' || v_nl)
                           , '<' || v_xml_subtag || '>' || v_nl
                           );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'datum_print'
                                   ,p_xml_regel     => to_char (v_datum_print, 'DD-MM-YYYY')
                                   );
      -- referentie
      v_tekst := kgc_maastricht.referentie(r_xvvz.onde_id, r_xvvz.onderzoeknr);
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'onde_onze_referentie'
                                   ,p_xml_regel     => v_tekst
                                   );
      -- kopie
      v_tekst := NULL;
      IF kgc_brie_00.eerder_geprint(r_xvvz.onde_id, NULL, r_xvvz.brty_id) = 'N' THEN
        IF r_uits.rela_id <> r_xvvz.rela_id THEN
          v_tekst := 'Brief voor kopiehouder';
        END IF;
      ELSE
        IF r_uits.rela_id <> r_xvvz.rela_id THEN
          v_tekst := 'Kopiebrief voor kopiehouder';
        ELSE
          v_tekst := 'Kopie';
        END IF;
      END IF;
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'kopie_tekst'
                                   ,p_xml_regel     => v_tekst
                                   );
      -- geadresseerde
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_adres'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_xvvz.geadresseerde)
                         );
    -- haal persoon gegevens
    --  kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
    --                               ,p_tag           => 'pers_aanspreken'
    --                               ,p_xml_regel     => toxml(r_xvvz.persoon)
    --                               );
    --  kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
    --                              , p_tag           => 'pers_geboortedatum'
    --                              , p_xml_regel     => toxml(to_char(r_xvvz.geboortedatum, 'dd-mm-yyyy'))
    --                              );
       v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_PERSONEN'
          ,p_kolommen      => 'zisnr, aanspreken, to_char(geboortedatum, ''dd-mm-yyyy'') geboortedatum, geslacht'
          ,p_where         => 'pers_id = ' || to_char (r_xvvz.pers_id)
          ,p_prefix        => 'pers'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
      -- headerpersoon gegevens,
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'pers_vervolgvel_info'
                         ,p_xml_regel     => v_nl || kgc_pers_00.persoon_info(null, r_xvvz.onde_id, r_xvvz.pers_id, 'VERVOLGVEL')
                         );
      -- monstergegevens1
      open c_mons1(r_xvvz.onde_id);
      fetch c_mons1 into r_mons1;
      close c_mons1;
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'mate_omschrijving'
                                   ,p_xml_regel     => toxml(r_mons1.mate_omschrijving)
                                   );
      kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                  , p_tag           => 'mons_datum_aanmelding'
                                  , p_xml_regel     => toxml(to_char(r_mons1.datum_aanmelding, 'dd-mm-yyyy'))
                                  );
      kgc_xml_00.voegtoe_xml_regel (p_xml_clob      => v_xml_doc
                                   ,p_tag           => 'mons_monsternummer'
                                   ,p_xml_regel     => toxml(r_mons1.monsternummer)
                                   );

--***************
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH ('<metis>')
                           ,'<metis>'
                           );
      FOR r_meti IN c_meti(r_xvvz.onde_id) LOOP
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('<meti>')
                             ,'<meti>'
                             );
        kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                    , p_tag           => 'stgr_omschrijving'
                                    , p_xml_regel     => toxml(r_meti.stgr_omschrijving)
                                    );
        kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                    , p_tag           => 'kop_activiteit'
                                    , p_xml_regel     => toxml(r_meti.kop_activiteit)
                                    );
        kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                    , p_tag           => 'resultaat'
                                    , p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_meti.resultaat)
                                    );
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('<meets>')
                             ,'<meets>'
                             );
        FOR r_meet in c_meet(r_meti.meti_id, r_meti.stgr_id) LOOP
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('<meet>')
                               ,'<meet>'
                               );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'waarde'
                                      , p_xml_regel     => toxml(r_meet.waarde)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'referentie_waarden'
                                      , p_xml_regel     => toxml(r_meet.referentie_waarden)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'range'
                                      , p_xml_regel     => toxml(r_meet.range)
                                      );
          kgc_xml_00.voegtoe_xml_regel( p_xml_clob      => v_xml_doc
                                      , p_tag           => 'eenheid'
                                      , p_xml_regel     => toxml(r_meet.eenheid)
                                      );
          dbms_lob.writeappend (v_xml_doc
                               ,LENGTH ('</meet>')
                               ,'</meet>'
                               );
        END LOOP;
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</meets>')
                             ,'</meets>'
                             );
        dbms_lob.writeappend (v_xml_doc
                             ,LENGTH ('</meti>')
                             ,'</meti>'
                             );
      END LOOP;
      dbms_lob.writeappend (v_xml_doc
                           ,LENGTH ('</metis>')
                           ,'</metis>'
                           );
      -- haal uitslag gegevens, via bouwsteen
/*      v_where := 'uits_id = ' || to_char (r_xvvz.uits_id);
      IF r_xvvz.koho_id IS NULL THEN
        v_where := v_where || ' AND koho_id IS NULL';
      ELSE
        v_where := v_where || ' AND koho_id = ' || to_char (r_xvvz.koho_id);
      END IF;
      v_xml_regel_collection :=
        kgc_bouwsteen_00.bouwsteen
          (p_bouwsteen     => 'KGC_UITSLAGBRIEF_UTMETAB_VW'
          ,p_kolommen      => 'onde_referentie'
                              || ', onderzoeknr, materiaal, to_char(afname_datum, ''dd-mm-yyyy'') afname_datum'
                              || ', to_char(datum_autorisatie, ''dd-mm-yyyy'') datum_autorisatie'
          ,p_where         => v_where
          ,p_prefix        => 'uitv'
          );
      kgc_xml_00.voegtoe_xml_regels
                             (p_xml_clob                 => v_xml_doc
                             ,p_xml_regel_collection     => v_xml_regel_collection);
*/
      -- uitslag_ondertekenaars (meerdere regels)
      Open  C_mede(r_uits.mede_id);
          Fetch C_mede into R_mede ;
      close c_mede;

      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'onde_autorisator'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_uits.autorisator)
                         );
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_ondertekenaar'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_uits.ondertekenaar)
                         );

       kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_Functie_onderteken1'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_mede.omschrijving)
                         );
      r_mede.omschrijving := '';

      Open  C_mede(r_uits.mede_id2);
          Fetch C_mede into R_mede ;
      close c_mede;
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_ondertekenaar2'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_uits.ondertekenaar2)
                         );

       kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_Functie_onderteken2'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_mede.omschrijving)
                         );

      -- uitslag_tekst
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_uitslag_tekst_f'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_xvvz.uitslag_tekst)
                         );
      -- toelichting
      kgc_xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'uitv_toelichting_f'
                         ,p_xml_regel     => v_nl || kgc_xml_00.cr_naar_tags(r_xvvz.toelichting)
                         );

      -- kopiehouders alleen indien geadresseerde = aanvrager
      uitv_kopiehouders ( p_uits_id => r_xvvz.uits_id
                        , p_rela_id_adr => r_xvvz.rela_id
                        , p_rela_id_uits => r_uits.rela_id
                        , p_xml_doc => v_xml_doc
                        );

      -- einde 1 brief:
      dbms_lob.writeappend ( v_xml_doc
                           , LENGTH ('</' || v_xml_subtag || '>' || v_nl)
                           , '</' || v_xml_subtag || '>' || v_nl
                           );

      -- evt EN igv eerste uitslag: registreren
      IF p_ind_brief = 'J' AND c_xvvz%ROWCOUNT = 1
      THEN
        open c_ramo (b_brty_id => r_xvvz.brty_id);
        fetch c_ramo into v_ramo_id;
        close c_ramo;

        kgc_brie_00.registreer (p_datum             => sysdate
                               ,p_kopie             => p_ind_kopie
                               ,p_brieftype         => r_uits.brty_code --'UITSLAG'
                               ,p_brty_id           => r_xvvz.brty_id
                               ,p_pers_id           => r_xvvz.pers_id
                               ,p_kafd_id           => r_xvvz.kafd_id
                               ,p_onde_id           => r_xvvz.onde_id
                               ,p_rela_id           => r_xvvz.rela_id
                               ,p_uits_id           => r_xvvz.uits_id
                               ,p_geadresseerde     => r_xvvz.geadresseerde
                               ,p_ongr_id           => r_xvvz.ongr_id
                               ,p_taal_id           => r_xvvz.taal_id
                               ,p_destype           => p_destype
                               ,p_best_id           => p_best_id
                               ,p_ind_commit        => 'J'
                               ,p_ramo_id           => v_ramo_id
                               );
      END IF;
    END LOOP;

    CLOSE c_xvvz;

    kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);

    kgc_xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

  END uits_met_tpmt;

FUNCTION persoon_info_cyto
( p_mons_id IN NUMBER   := NULL
, p_onde_id IN NUMBER   := NULL
, p_pers_id IN NUMBER   := NULL
, p_lengte  IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_persoon   VARCHAR2(2000);
  v_pers_id   NUMBER;
  v_foet_id   NUMBER;
  v_sypa_code VARCHAR2(100);
  v_mede_id   NUMBER;
  v_kafd_id   NUMBER;
  v_ongr_id   NUMBER;
  --
  CURSOR mons_cur
  IS
    SELECT mons_id
    ,      pers_id
    ,      foet_id
    ,      kafd_id
    ,      ongr_id
    FROM   kgc_monsters
    WHERE  mons_id = p_mons_id
    ;
  mons_rec mons_cur%rowtype;

  CURSOR onde_cur
  IS
    SELECT pers_id
    ,      foet_id
    ,      kafd_id
    ,      ongr_id
    ,      taal_id
    ,      onderzoeknr
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
  onde_rec onde_cur%rowtype;
  --
  CURSOR pers_cur
  IS
  SELECT achternaam
  ,      aanspreken
  ,      geboortedatum
  ,      part_geboortedatum
  ,      geslacht
  ,      DECODE( achternaam_partner
                 , NULL, SUBSTR(achternaam,1,3)
                 , SUBSTR(achternaam,1,3)||'-'||SUBSTR(achternaam_partner,1,3)
                 ) afkorting
  ,      DECODE( overleden
                 , 'J', 'Overleden '||TO_CHAR(overlijdensdatum,'dd-mm-yyyy')
                 , NULL
                 ) datum_overleden
  FROM   kgc_personen
  WHERE  pers_id = v_pers_id
    ;
  pers_rec pers_cur%rowtype;
  --
BEGIN
  IF ( p_mons_id IS NULL
   AND p_onde_id IS NULL
   AND p_pers_id IS NULL
     )
  THEN
    RETURN( NULL );
  END IF;
  v_mede_id := kgc_mede_00.medewerker_id;
  v_sypa_code := 'PERSOONSGEGEVENS_'||UPPER( NVL( p_lengte, 'LANG' ) );
  v_pers_id := p_pers_id;
  IF ( p_mons_id IS NOT NULL )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  mons_rec;
    v_pers_id := mons_rec.pers_id;
    v_foet_id := mons_rec.foet_id;
    v_kafd_id := mons_rec.kafd_id;
    v_ongr_id := mons_rec.ongr_id;
    CLOSE mons_cur;
  ELSIF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  onde_rec;
    v_pers_id := onde_rec.pers_id;
    v_foet_id := onde_rec.foet_id;
    v_kafd_id := onde_rec.kafd_id;
    v_ongr_id := onde_rec.ongr_id;
    CLOSE onde_cur;
  END IF;
  -- systeem waarde ophalen.
  BEGIN
    v_persoon := kgc_sypa_00.standaard_waarde
                 ( p_parameter_code => v_sypa_code
                 , p_kafd_id => v_kafd_id
                 , p_ongr_id => v_ongr_id
                 , p_mede_id => v_mede_id
                 );
  EXCEPTION
  WHEN OTHERS
  THEN
    -- als de systeem parameter niet bestaat de persoon_info van voor 2004 geven.
    v_persoon := '<AANSPREKEN> (<GESLACHT>) <GEBOORTEDATUM> <OVERLEDEN> <FOETUS> <ZWANGERSCHAP>';
  END;

  OPEN  pers_cur;
  FETCH pers_cur
  INTO  pers_rec;
  CLOSE pers_cur;
  v_persoon:= REPLACE(v_persoon, '<achternaam>', '<ACHTERNAAM>');
  v_persoon:= REPLACE(v_persoon, '<ACHTERNAAM>', pers_rec.achternaam);
  v_persoon:= REPLACE(v_persoon, '<aanspreken>', '<AANSPREKEN>');
  v_persoon:= REPLACE(v_persoon, '<AANSPREKEN>', pers_rec.aanspreken);
  v_persoon:= REPLACE(v_persoon, '<geboortedatum>', '<GEBOORTEDATUM>');
  v_persoon:= REPLACE(v_persoon, '<GEBOORTEDATUM>', NVL( TO_CHAR( pers_rec.geboortedatum, 'dd-mm-yyyy' ), pers_rec.part_geboortedatum ));
  v_persoon:= REPLACE(v_persoon, '<overleden>', '<OVERLEDEN>');
  v_persoon:= REPLACE(v_persoon, '<OVERLEDEN>', pers_rec.datum_overleden);
  v_persoon:= REPLACE(v_persoon, '<geslacht>', '<GESLACHT>');
  if  pers_rec.geslacht in ('V', 'F') then

       v_persoon:= REPLACE(v_persoon, '<GESLACHT>', 'vrouwelijk');
  else if pers_rec.geslacht in ('M') then
          v_persoon:= REPLACE(v_persoon, '<GESLACHT>', 'mannelijk');
       else
         v_persoon:= REPLACE(v_persoon, '<GESLACHT>', 'onbekend');
       end if;
  end if;
--  v_persoon:= REPLACE(v_persoon, '<GESLACHT>', kgc_vert_00.vertaling('PERS_GESL',NULL,pers_rec.geslacht,onde_rec.taal_id));

  v_persoon:= REPLACE(v_persoon, '<initialen>', '<INITIALEN>');
  v_persoon:= REPLACE(v_persoon, '<INITIALEN>', pers_rec.afkorting);
  v_persoon:= REPLACE(v_persoon, '<onderzoeknr>', '<ONDERZOEKNR>');
  v_persoon:= REPLACE(v_persoon, '<ONDERZOEKNR>', onde_rec.onderzoeknr );

  v_persoon:= REPLACE(v_persoon, '<foetus>', '<FOETUS>');
  IF INSTR( v_persoon, '<FOETUS>') > 0
  THEN
    v_persoon := REPLACE(v_persoon, '<FOETUS>', kgc_foet_00.foetus_info( p_foet_id => v_foet_id ));
  END IF;

  v_persoon:= REPLACE(v_persoon, '<zwangerschap>', '<ZWANGERSCHAP>');
  IF INSTR(v_persoon, '<ZWANGERSCHAP>') > 0
  THEN
    v_persoon := REPLACE(v_persoon, '<ZWANGERSCHAP>', kgc_leef_00.omschrijving( p_mons_id => p_mons_id ));
  END IF;

  v_persoon:= REPLACE(v_persoon, '<bsn>', '<BSN>');
  IF INSTR(v_persoon, '<BSN>') > 0
  THEN
    v_persoon := REPLACE(v_persoon, '<BSN>', kgc_pers_00.bsn_opgemaakt( p_pers_id => v_pers_id ));
  END IF;

  RETURN( LTRIM( v_persoon ) );
END persoon_info_cyto
;

END Beh_Xml_Doc;
/

/
QUIT
