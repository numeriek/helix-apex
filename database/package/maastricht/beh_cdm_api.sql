CREATE OR REPLACE PACKAGE "HELIX"."BEH_CDM_API" As
/******************************************************************************
   NAME:      BEH_CDM_API
   PURPOSE: PROVIDES API ROUTINES FOR CREATING,UPDATING AND PROCESSING CDM RECORDS.
            Suppports both WcfCdmImport as WcfCdmExport.

  REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
    2.2        29/05/2013      RKON       4. Added Comments update Send notification for
                                             use with Job
    2.1        01/05/2013      RKON       3. Update PAckage added send Notification routine
    2.0        01/04/2013      RKON       2. Updated Package with CdmWcfImportRoutines
    1.0        29/11/2012      RKON       1.Created Package Added Routines for the
                                            Cdm Export Function
******************************************************************************/







    Function GetCaseInformation( pQueueId Number
                                ,pStatus  Varchar2
                                )
             Return Clob;

    Function GetSlideInformation(PQueueId Number, PStatus Varchar2 ) Return Clob;

    Function GetTechnologie(pProtocol varchar2) Return varchar2;

    Function GetAutomationCode(prefixMonsterNummer varchar2, ONGR_ID number) Return varchar2;

    Procedure Create_QueueRecord( pWlst_Id Number,
                                  pMeet_id Number,
                                  pMons_id Number
                                );
   Procedure Update_QueueRecord (pQueue_id Number,
                                 pStatus Varchar2,
                                 pError  Varchar2
                                );



   PRocedure Send_Notifications ; -- Triggert by JOB sends multiple notifications at once
   PRocedure Send_Notification (pQueue_id Number) ; -- Triggert by manual script sends one notification
/* CDM IMPORT ROUTINES
*/



End BEH_CDM_API;
/

/
QUIT
