CREATE OR REPLACE PACKAGE "HELIX"."MSS_VREUGDEBRIEF_00" IS
-- moet er een vreugdebrief worden gemaakt?

-- Retourneer het geslacht van de foetus, indien de ouder het wil weten
FUNCTION info_geslacht
( p_kafd_id IN NUMBER
, p_zwan_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( info_geslacht, WNDS, WNPS, RNPS );

-- Bepaal herkomstcode van het monster(s) die horen bij de foetus(sen) van de zwangerschap
FUNCTION bep_herkomst_monster
( p_zwan_id IN number
)
RETURN  VARCHAR2;

END MSS_VREUGDEBRIEF_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_VREUGDEBRIEF_00" IS

FUNCTION info_geslacht
( p_kafd_id in number
, p_zwan_id IN number
)
RETURN  VARCHAR2
IS
  cursor c_zwan (b_zwan_id in kgc_zwangerschappen.zwan_id%type)
  is
    select zwan.info_geslacht
    from   kgc_zwangerschappen zwan
    where  zwan.zwan_id = b_zwan_id
  ;
  --
  cursor c_bep_geslacht( b_kafd_id in kgc_onderzoeken.kafd_id%type
                       , b_zwan_id in kgc_zwangerschappen.zwan_id%type )
  is
    select case
             when instr(upper(meti.conclusie), 'XX') > 0 then 'V'
             when instr(upper(meti.conclusie), 'XY') > 0 then 'M'
             else null
           end  geslacht
    from   kgc_stoftestgroepen  stgr
    ,      bas_metingen         meti
    ,      kgc_onderzoeken      onde
    ,      kgc_foetussen        foet
    ,      kgc_zwangerschappen  zwan
    where  zwan.zwan_id = b_zwan_id
    and    foet.zwan_id = zwan.zwan_id
    and    onde.foet_id = foet.foet_id
    and    onde.kafd_id = b_kafd_id
    and    meti.onde_id = onde.onde_id
    and    (   (instr(upper(meti.conclusie), 'XX') > 0)
            or (instr(upper(meti.conclusie), 'XY') > 0)
           )
    and    stgr.stgr_id = meti.stgr_id
    and    stgr.code = 'KARYO-PR'
  ;
  --
  r_zwan_rec     c_zwan%rowtype;
  pl_tot_aantal  number(2);
  pl_aantal_m    number(2);
  pl_aantal_v    number(2);
  pl_return      varchar2(200);
  --
BEGIN
  open  c_zwan(p_zwan_id);
  fetch c_zwan
  into  r_zwan_rec;
  close c_zwan;
  --
  if r_zwan_rec.info_geslacht = 'N'
  then
    pl_return := null;
  else
    pl_tot_aantal := 0;
    pl_aantal_m   := 0;
    pl_aantal_v   := 0;
    --
    for r_bep_geslacht_rec in c_bep_geslacht( p_kafd_id
                                            , p_zwan_id )
    loop
      pl_tot_aantal := pl_tot_aantal + 1;
      --
      if r_bep_geslacht_rec.geslacht = 'M'
      then
        pl_aantal_m := pl_aantal_m + 1;
      elsif r_bep_geslacht_rec.geslacht = 'V'
      then
        pl_aantal_v := pl_aantal_v + 1;
      else
        null;
      end if;
    end loop;
    --
    if pl_tot_aantal = 0
    then
      pl_return := null;
    else
      if pl_aantal_v = 0
      then
        if pl_aantal_m = 1
        then
          pl_return := 'een jongen';
        elsif pl_aantal_m > 1
        then
          pl_return := TO_CHAR(pl_aantal_m)||' jongens';
        end if;
      elsif pl_aantal_m = 0
      then
        if pl_aantal_v = 1
        then
          pl_return := 'een meisje';
        elsif pl_aantal_v > 1
        then
          pl_return := TO_CHAR(pl_aantal_v)||' meisjes';
        end if;
      else
        if  pl_aantal_m = 1
        and pl_aantal_v = 1
        then
          pl_return := 'een jongen en een meisje';
        elsif pl_aantal_m > 1
        and   pl_aantal_v = 1
        then
          pl_return := TO_CHAR(pl_aantal_m)||' jongens en een meisje';
        elsif pl_aantal_m = 1
        and   pl_aantal_v > 1
        then
          pl_return := 'een jongen en '||TO_CHAR(pl_aantal_v)||' meisjes';
        elsif pl_aantal_m > 1
        and   pl_aantal_v > 1
        then
          pl_return := TO_CHAR(pl_aantal_m)||' jongens en '||TO_CHAR(pl_aantal_v)||' meisjes';
        else
          null;
        end if;
      end if;
    end if;
    --
  end if;
  --
  return pl_return;
  --
END info_geslacht;

FUNCTION bep_herkomst_monster
( p_zwan_id IN number
)
RETURN  VARCHAR2
IS
  cursor c_herk (b_zwan_id in kgc_zwangerschappen.zwan_id%type)
  is
    select distinct
           herk.code
    from   kgc_herkomsten herk
    ,      kgc_monsters   mons
    ,      kgc_foetussen  foet
    where  foet.zwan_id = b_zwan_id
    and    mons.foet_id = foet.foet_id
    and    mons.herk_id is not null
    and    herk.herk_id = mons.herk_id
  ;
  --
  r_herk_rec  c_herk%rowtype;
  pl_found    boolean;
  pl_return   kgc_herkomsten.code%type;
  --
BEGIN
  open  c_herk(p_zwan_id);
  fetch c_herk
  into  r_herk_rec;
  --
  if c_herk%NOTFOUND
  then
    pl_return := null;
  else
    pl_return := r_herk_rec.code;
  end if;
  --
  close c_herk;
  return pl_return;
  --
END bep_herkomst_monster;

END  mss_vreugdebrief_00;
/

/
QUIT
