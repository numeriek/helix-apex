CREATE OR REPLACE PACKAGE "HELIX"."MSS_ALGEMEEN"
IS

  --
  -- Bepaal brieftype code op basis van brty_id
  --
  function bep_brty_code ( p_brty_id in kgc_brieftypes.brty_id%type
                         )
  return kgc_brieftypes.code%type;

  --
  -- Bepaal fractietype code op basis van frty_id
  --
  function bep_frty_code ( p_frty_id in bas_fractie_types.frty_id%type
                         )
  return bas_fractie_types.code%type;

  --
  -- Bepaal categorie code code voor gegeven categorie waarde binnen gegeven categorie type code
  --
  function bep_cate_code ( p_caty_code   in kgc_categorie_types.code%type
                         , p_cawa_waarde in kgc_categorie_waarden.waarde%type
                         )
  return kgc_categorieen.code%type;

  --
  -- Bepaal geslacht voor gegeven geslacht code
  --
  function bep_geslacht_pat ( p_geslacht in kgc_personen.geslacht%type
                            )
  return varchar2;

  --
  -- Bepaal bsn voor gegeven bsn en gegeven geverifeerd
  --
  function bep_bsn_pat ( p_bsn     in kgc_personen.bsn%type
                       , p_bsn_gev in kgc_personen.bsn_geverifieerd%type
                       )
  return varchar2;

  --
  -- Bepaal familienummer voor gegeven onde_id
  --
  function bep_familienummer ( p_onde_id in kgc_onderzoeken.onde_id%type
                             )
  return kgc_families.familienummer%type;

  --
  -- Bepaal uw kenmerk voor gegeven onde_id
  --
  function bep_uw_kenmerk ( p_onde_id in kgc_onderzoeken.onde_id%type
                          )
  return kgc_gerelateerde_onderzoeken.externe_onderzoeknr%type;

  --
  -- Bepaal de vertaling (naar de taal gegeven door taal_id)
  --
  function bep_vertaling ( p_taal_id    in kgc_talen.taal_id%type
                         , p_tabel_naam in kgc_attributen.tabel_naam%type
                         , p_id         in kgc_attribuut_waarden.id%type
                         )
  return kgc_attribuut_waarden.waarde%type;

  --
  -- Bepaal meet_id voor gegeven kafd_id, meti_id en stof_code
  --
  function bep_meet_obv_meti_stof ( p_kafd_id   in kgc_kgc_afdelingen.kafd_id%type
                                  , p_meti_id   in bas_metingen.meti_id%type
                                  , p_stof_code in kgc_stoftesten.code%type
                                  )
  return bas_meetwaarden.meet_id%type;

  --
  -- Bepaal attr_id voor gegeven tabelnaam en attribuut code
  --
  function bep_attr_id ( p_tabel_naam in kgc_attributen.tabel_naam%type
                       , p_code       in kgc_attributen.code%type
                       )
  return kgc_attributen.attr_id%type;

  --
  -- Functie retourneert inputwaarde behalve als inputwaarde 0 is, dan wordt null geretourneerd.
  --
  function echte_0_wordt_null ( p_waarde in number
                              )
  return number;

  --
  -- Functie bepaalt tbv bulkprinten of er reeds een brief aanwezig (geprint) is voor
  -- gegeven uits_id, brty_id en rela_id
  -- Retourneerwaarde: varchar2
  --   'J' --> brief is aanwezig voor gegeven parameters
  --   'N' --> brief is niet aanwezig voor gegeven parameters
  --
  function bbrief_reeds_geprint ( p_uits_id in kgc_uitslagen.uits_id%type
                                , p_brty_id in kgc_brieftypes.brty_id%type
                                , p_rela_id in kgc_relaties.rela_id%type
                                )
  return varchar2;

  --
  -- Functie bepaalt tbv bulkprinten of er reeds een eindbrief aanwezig (geprint) is voor gegeven
  -- uits_id, brty_id en rela_id. Tussentijdse uitslagen altijd beschouwen als niet eerder geprint.
  -- Retourneerwaarde: varchar2
  --   'J' --> eindbrief is aanwezig voor gegeven parameters
  --   'N' --> eindbrief is niet aanwezig voor gegeven parameters
  --
  function beindbrief_reeds_geprint ( p_uits_id in kgc_uitslagen.uits_id%type
                                    , p_brty_id in kgc_brieftypes.brty_id%type
                                    , p_rela_id in kgc_relaties.rela_id%type
                                    )
  return varchar2;

  --
  -- Functie bepaalt meetwaarde detail waarde voor gegeven meet_id en prompt
  --
  function bep_mdet_waarde ( p_meet_id in bas_meetwaarden.meet_id%type
                           , p_prompt  in bas_meetwaarde_details.prompt%type
                           )
  return bas_meetwaarde_details.waarde%type;

  --
  -- Functie bepaalt voor gegeven meet_id en prompt of mdet naar stamboom mag
  --
  function bep_mdet_naar_stamboom ( p_meet_id in bas_meetwaarden.meet_id%type
                                  , p_prompt  in bas_meetwaarde_details.prompt%type
                                  )
  return bas_mdet_ok.naar_stamboom%type;

END mss_algemeen;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_ALGEMEEN"
IS

  --
  -- Bepaal brieftype code op basis van brty_id
  --
  function bep_brty_code ( p_brty_id in kgc_brieftypes.brty_id%type
                         )
  return kgc_brieftypes.code%type
  is
    cursor c_brty ( b_brty_id in kgc_brieftypes.brty_id%type
                  )
    is
      select brty.code
      from   kgc_brieftypes brty
      where  brty.brty_id = b_brty_id
    ;
    pl_brty_code kgc_brieftypes.code%type;
    --
  begin
    open  c_brty(p_brty_id);
    fetch c_brty
    into  pl_brty_code;
    close c_brty;
    --
    return pl_brty_code;
  end bep_brty_code;

  --
  -- Bepaal fractietype code op basis van frty_id
  --
  function bep_frty_code ( p_frty_id in bas_fractie_types.frty_id%type
                         )
  return bas_fractie_types.code%type
  is
    cursor c_frty ( b_frty_id in bas_fractie_types.frty_id%type
                  )
    is
      select frty.code
      from   bas_fractie_types frty
      where  frty.frty_id = b_frty_id
    ;
    pl_frty_code bas_fractie_types.code%type;
    --
  begin
    open  c_frty(p_frty_id);
    fetch c_frty
    into  pl_frty_code;
    close c_frty;
    --
    return pl_frty_code;
  end bep_frty_code;

  --
  -- Bepaal categorie code code voor gegeven categorie waarde binnen gegeven categorie type code
  --
  function bep_cate_code ( p_caty_code   in kgc_categorie_types.code%type
                         , p_cawa_waarde in kgc_categorie_waarden.waarde%type
                         )
  return kgc_categorieen.code%type
  is
    cursor c_cate ( b_caty_code   in kgc_categorie_types.code%type
                  , b_cawa_waarde in kgc_categorie_waarden.waarde%type
                  )
    is
      select cate.code
      from   kgc_categorie_waarden  cawa
      ,      kgc_categorieen        cate
      ,      kgc_categorie_types    caty
      where  caty.code = b_caty_code
      and    cate.caty_id = caty.caty_id
      and    cawa.cate_id = cate.cate_id
      and    cawa.waarde = b_cawa_waarde
    ;
    pl_cate_code kgc_categorieen.code%type;
    --
  begin
    open  c_cate( p_caty_code
                , p_cawa_waarde);
    fetch c_cate
    into  pl_cate_code;
    close c_cate;
    --
    return pl_cate_code;
  end bep_cate_code;

  --
  -- Bepaal geslacht voor gegeven geslacht code
  --
  function bep_geslacht_pat ( p_geslacht in kgc_personen.geslacht%type
                            )
  return varchar2
  is
    pl_return varchar2(20);
  begin
    if p_geslacht = 'M'
    then
      pl_return := 'Mannelijk';
    elsif p_geslacht = 'V'
    then
      pl_return := 'Vrouwelijk';
    else
      pl_return := 'Onbekend';
    end if;
    --
    return pl_return;
  end bep_geslacht_pat;

  --
  -- Bepaal bsn voor gegeven bsn en gegeven geverifeerd
  --
  function bep_bsn_pat ( p_bsn     in kgc_personen.bsn%type
                       , p_bsn_gev in kgc_personen.bsn_geverifieerd%type
                       )
  return varchar2
  is
    pl_return varchar2(10);
  begin
    if  p_bsn is not null
    and p_bsn_gev = 'J'
    then
      pl_return := to_char(p_bsn);
    elsif p_bsn is null
    then
      pl_return := '-';
    else
      pl_return := to_char(null);
    end if;
    --
    return pl_return;
  end bep_bsn_pat;

  --
  -- Bepaal familienummer voor gegeven onde_id
  --
  function bep_familienummer ( p_onde_id in kgc_onderzoeken.onde_id%type
                             )
  return kgc_families.familienummer%type
  is
    cursor c_fami ( b_onde_id in kgc_onderzoeken.onde_id%type
                  )
    is
      select listagg(fami.familienummer, ', ') within group (order by fami.familienummer) familienummers
      from   kgc_families             fami
      ,      kgc_familie_leden        fale
      ,      kgc_onderzoeken          onde
      where  onde.onde_id = b_onde_id
      and    fale.pers_id = onde.pers_id
      and    fami.fami_id = fale.fami_id
      order
      by     fami.fami_id
    ;
    pl_familienummers varchar2(100);
    --
  begin
    open  c_fami( p_onde_id
                );
    fetch c_fami
    into  pl_familienummers;
    close c_fami;
    --
    return pl_familienummers;
  end bep_familienummer;

  --
  -- Bepaal uw kenmerk voor gegeven onde_id
  --
  function bep_uw_kenmerk ( p_onde_id in kgc_onderzoeken.onde_id%type
                          )
  return kgc_gerelateerde_onderzoeken.externe_onderzoeknr%type
  is
    cursor c_reon ( b_onde_id in kgc_onderzoeken.onde_id%type
                  )
    is
      select reon.externe_onderzoeknr externe_onderzoeknr
      from   kgc_onderzoekrelatie_types    onty
      ,      kgc_gerelateerde_onderzoeken  reon
      where  reon.onde_id = b_onde_id
      and    onty.onty_id = reon.onty_id
      and    onty.code = 'UW_KENM'
    ;
    pl_externe_onderzoeknr  kgc_gerelateerde_onderzoeken.externe_onderzoeknr%type;
    --
  begin
    open  c_reon( p_onde_id
                );
    fetch c_reon
    into  pl_externe_onderzoeknr;
    close c_reon;
    --
    return pl_externe_onderzoeknr;
  end bep_uw_kenmerk;

  --
  -- Bepaal de vertaling (naar de taal gegeven door taal_id)
  --
  function bep_vertaling ( p_taal_id    in kgc_talen.taal_id%type
                         , p_tabel_naam in kgc_attributen.tabel_naam%type
                         , p_id         in kgc_attribuut_waarden.id%type
                         )
  return kgc_attribuut_waarden.waarde%type
  is
    cursor c_vert ( b_taal_id    in kgc_talen.taal_id%type
                  , b_tabel_naam in kgc_attributen.tabel_naam%type
                  , b_id         in kgc_attribuut_waarden.id%type
                  )
    is
      select atwa.waarde  vertaalde_tekst
      from   kgc_attribuut_waarden  atwa
      ,      kgc_talen              taal
      ,      kgc_attributen         attr
      where  attr.tabel_naam = b_tabel_naam
      and    taal.taal_id = b_taal_id
      and    instr(upper(attr.code), upper(taal.code)) > 0
      and    atwa.attr_id = attr.attr_id
      and    atwa.id = b_id
    ;
    pl_vertaalde_tekst kgc_attribuut_waarden.waarde%type;
    --
  begin
    open  c_vert ( p_taal_id
                 , p_tabel_naam
                 , p_id
                 );
    fetch c_vert
    into  pl_vertaalde_tekst;
    close c_vert;
    --
    return pl_vertaalde_tekst;
  end bep_vertaling;

  --
  -- Bepaal meet_id voor gegeven kafd_id, meti_id en stof_code
  --
  function bep_meet_obv_meti_stof ( p_kafd_id   in kgc_kgc_afdelingen.kafd_id%type
                                  , p_meti_id   in bas_metingen.meti_id%type
                                  , p_stof_code in kgc_stoftesten.code%type
                                  )
  return bas_meetwaarden.meet_id%type
  is
    cursor c_meet ( b_kafd_id   in kgc_kgc_afdelingen.kafd_id%type
                  , b_meti_id   in bas_metingen.meti_id%type
                  , b_stof_code in kgc_stoftesten.code%type
                  )
    is
      select meet.meet_id
      from   kgc_stoftesten  stof
      ,      bas_meetwaarden meet
      where  meet.meti_id = b_meti_id
      and    stof.stof_id = meet.stof_id
      and    stof.kafd_id = b_kafd_id
      and    stof.code like p_stof_code
    ;
    pl_meet_id bas_meetwaarden.meet_id%type;
    --
  begin
    open  c_meet ( p_kafd_id
                 , p_meti_id
                 , p_stof_code
                 );
    fetch c_meet
    into  pl_meet_id;
    close c_meet;
    --
    return pl_meet_id;
  end bep_meet_obv_meti_stof;

  --
  -- Bepaal attr_id voor gegeven tabelnaam en attribuut code
  --
  function bep_attr_id ( p_tabel_naam in kgc_attributen.tabel_naam%type
                       , p_code       in kgc_attributen.code%type
                       )
  return kgc_attributen.attr_id%type
  is
    cursor c_attr ( b_tabel_naam in kgc_attributen.tabel_naam%type
                  , b_code       in kgc_attributen.code%type
                  )
    is
      select attr.attr_id
      from   kgc_attributen attr
      where  attr.tabel_naam = b_tabel_naam
      and    attr.code = b_code
    ;
    pl_attr_id kgc_attributen.attr_id%type;
    --
  begin
    open  c_attr ( p_tabel_naam
                 , p_code
                 );
    fetch c_attr
    into  pl_attr_id;
    close c_attr;
    --
    return pl_attr_id;
  end bep_attr_id;

  --
  -- Functie retourneert inputwaarde behalve als inputwaarde 0 is, dan wordt null geretourneerd.
  --
  function echte_0_wordt_null ( p_waarde in number
                              )
  return number
  is
    pl_return number(10);
  begin
    pl_return := case
                   when p_waarde = 0
                   then to_number(null)
                   else p_waarde
                 end;
    return pl_return;
  end echte_0_wordt_null;

  --
  -- Functie bepaalt tbv bulkprinten of er reeds een brief aanwezig (geprint) is voor
  -- gegeven uits_id, brty_id en rela_id
  -- Retourneerwaarde: varchar2
  --   'J' --> brief is aanwezig voor gegeven parameters
  --   'N' --> brief is niet aanwezig voor gegeven parameters
  --
  function bbrief_reeds_geprint ( p_uits_id in kgc_uitslagen.uits_id%type
                                , p_brty_id in kgc_brieftypes.brty_id%type
                                , p_rela_id in kgc_relaties.rela_id%type
                                )
  return varchar2
  is
    cursor c_brie ( b_uits_id in kgc_uitslagen.uits_id%type
                  , b_brty_id in kgc_brieftypes.brty_id%type
                  , b_rela_id in kgc_relaties.rela_id%type
                  )
    is
      select null
      from   kgc_brieven brie
      where  brie.uits_id = b_uits_id
      and    brie.brty_id = b_brty_id
      and    brie.rela_id = b_rela_id
    ;
    pl_dummy  varchar2(1);
    pl_return varchar2(1);
    --
  begin
    open  c_brie ( p_uits_id
                 , p_brty_id
                 , p_rela_id
                 );
    fetch c_brie
    into  pl_dummy;
    if c_brie%NOTFOUND
    then
      pl_return := 'N';
    else
      pl_return := 'J';
    end if;
    close c_brie;
    --
    return pl_return;
    --
  end bbrief_reeds_geprint;

  --
  -- Functie bepaalt tbv bulkprinten of er reeds een eindbrief aanwezig (geprint) is voor gegeven
  -- uits_id, brty_id en rela_id. Tussentijdse uitslagen altijd beschouwen als niet eerder geprint.
  -- Retourneerwaarde: varchar2
  --   'J' --> eindbrief is aanwezig voor gegeven parameters
  --   'N' --> eindbrief is niet aanwezig voor gegeven parameters
  --
  function beindbrief_reeds_geprint ( p_uits_id in kgc_uitslagen.uits_id%type
                                    , p_brty_id in kgc_brieftypes.brty_id%type
                                    , p_rela_id in kgc_relaties.rela_id%type
                                    )
  return varchar2
  is
    cursor c_onde ( b_uits_id in kgc_uitslagen.uits_id%type
                  )
    is
      select null
      from   kgc_onderzoeken onde
      ,      kgc_uitslagen   uits
      where  uits.uits_id = b_uits_id
      and    onde.onde_id = uits.onde_id
      and    onde.afgerond = 'N'
    ;
    pl_dummy  varchar2(1);
    pl_return varchar2(1);
    --
  begin
    open  c_onde ( p_uits_id
                 );
    fetch c_onde
    into  pl_dummy;
    if c_onde%FOUND
    then
      pl_return := 'N';
    else
      pl_return := bbrief_reeds_geprint ( p_uits_id
                                        , p_brty_id
                                        , p_rela_id
                                        );
    end if;
    close c_onde;
    --
    return pl_return;
    --
  end beindbrief_reeds_geprint;

  --
  -- Functie bepaalt meetwaarde detail waarde voor gegeven meet_id en prompt
  --
  function bep_mdet_waarde ( p_meet_id in bas_meetwaarden.meet_id%type
                           , p_prompt  in bas_meetwaarde_details.prompt%type
                           )
  return bas_meetwaarde_details.waarde%type
  is
    cursor c_mdet ( b_meet_id in bas_meetwaarden.meet_id%type
                  , b_prompt  in bas_meetwaarde_details.prompt%type
                  )
    is
      select mdet.waarde waarde
      from   bas_meetwaarde_details mdet
      where  mdet.meet_id = b_meet_id
      and    mdet.prompt = b_prompt
     ;
    --
    r_mdet_rec  c_mdet%rowtype;
    pl_return   bas_meetwaarde_details.waarde%type := null;
    --
  begin
    open  c_mdet ( p_meet_id
                 , p_prompt
                 );
    fetch c_mdet
    into  r_mdet_rec.waarde;
    --
    if c_mdet%NOTFOUND
    then
      pl_return := null;
    else
      pl_return := r_mdet_rec.waarde;
    end if;
    --
    close c_mdet;
    return pl_return;
    --
  end bep_mdet_waarde;

  --
  -- Functie bepaalt voor gegeven meet_id en prompt of mdet naar stamboom mag
  --
  function bep_mdet_naar_stamboom ( p_meet_id in bas_meetwaarden.meet_id%type
                                  , p_prompt  in bas_meetwaarde_details.prompt%type
                                  )
  return bas_mdet_ok.naar_stamboom%type
  is
    cursor c_mdoc ( b_meet_id in bas_meetwaarden.meet_id%type
                  , b_prompt  in bas_meetwaarde_details.prompt%type
                  )
    is
      select mdok.naar_stamboom
      from   bas_mdet_ok            mdok
      ,      bas_meetwaarde_details mdet
      where  mdet.meet_id = b_meet_id
      and    mdet.prompt = b_prompt
      and    mdok.mdet_id = mdet.mdet_id
     ;
    --
    r_mdoc_rec  c_mdoc%rowtype;
    pl_return   bas_mdet_ok.naar_stamboom%type := null;
    --
  begin
    open  c_mdoc ( p_meet_id
                 , p_prompt
                 );
    fetch c_mdoc
    into  r_mdoc_rec.naar_stamboom;
    --
    if c_mdoc%NOTFOUND
    then
      pl_return := 'N';
    else
      pl_return := r_mdoc_rec.naar_stamboom;
    end if;
    --
    close c_mdoc;
    return pl_return;
    --
  end bep_mdet_naar_stamboom;

END mss_algemeen;
/

/
QUIT
