CREATE OR REPLACE PACKAGE "HELIX"."BEH_IMPEXP" IS
/*
Ge�nitieerd door Guido Herben, MUMC+ MIT Unit Zorg team Consult en Diagnose.
Doel : Importeren en Exporteren van gegevens (en het bewerken indien nodig).
Auteur : Guido Herben
Ingesloten procedures :
  KASPAIMPORT voor het invoeren en verwerken van KASPA-gegevens.
  DIAGIMPORT voor het invoeren en verwerken van diagnoses.
  STOFIMPORT voor het invoeren en verwerken van stoftesten.
  STOFPUSH voor het min of meer handmatig aanroepen van BEH_IMPEXP.STOFIMPORT.
  LOGINSERT voor het invoeren van loggings.

Historie :
DATUM      - Versie    - Auteur       - Informatie

2015-11-06 -   1.0     - Guido Herben - Package opgeleverd als samenstelling van allemaal werkende invoer/imports.
2015-11-05 -   0.0.1   - Guido Herben - Initiatie package en procedures samengevoegd in package.
*/

  PROCEDURE KASPAIMPORT ( p_bestandsnaam in varchar2 );             -- Procedure om KASPA-gegevens te importeren en te verwerken.
  PROCEDURE DIAGIMPORT;                                             -- Procedure om diagnoses in te voeren en te verwerken.
  PROCEDURE STOFIMPORT ( p_inlezen varchar2, p_protocol varchar2 ); -- Procedure om Stoftesten te importeren en te verwerken.
  PROCEDURE STOFPUSH;                                               -- Procedure om Stoftest-imports (handmatig) te forceren.
  PROCEDURE LOGINSERT ( pLog beh_Log%rowtype );                     -- Procedure om loggings in te voeren.

END BEH_IMPEXP;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_IMPEXP" IS
/*
Ge�nitieerd door Guido Herben, MUMC+ MIT Unit Zorg team Consult en Diagnose.
Doel : Importeren en Exporteren van gegevens (en het bewerken indien nodig).
Auteur : Guido Herben
Ingesloten procedures :
  KASPAIMPORT voor het invoeren en verwerken van KASPA-gegevens.
  DIAGIMPORT voor het invoeren en verwerken van diagnoses.
  STOFIMPORT voor het invoeren en verwerken van stoftesten.
  STOFPUSH voor het min of meer handmatig aanroepen van BEH_IMPEXP.STOFIMPORT.
  LOGINSERT voor het invoeren van loggings.

Historie :
DATUM      - Versie    - Auteur       - Informatie

2015-11-06 -   1.0     - Guido Herben - Package opgeleverd als samenstelling van allemaal werkende invoer/imports.
2015-11-05 -   0.0.1   - Guido Herben - Initiatie package en procedures samengevoegd in package.
*/

  PROCEDURE KASPAIMPORT ( p_bestandsnaam in varchar2 )
  IS
/*
  Script voor geautomatiseerde invoer van KASPA-resultaten.
  De BAS_MEETWAARDE_DETAILS-tabel dient aangevuld te worden met data uit een txt- of csv-file.
  Randvoorwaarden :
  - Het bronbestand moet als naam het werklijstnummer (de werklijstnaam) hebben v��r de extentie. Bijvoorbeeld WL14-0001.
  - Het bronbestand mag in de status-kolom maximaal 100 tekens bevatten.
  - De structuur van de meetwaarde_details moet al bekend zijn in de BAS_MEETWAARDE_DETAILS-tabel


  DATUM      - Versie    - Auteur         - Informatie
  2015-11-24 -   1.1     - Guido herben   - Opgeleverd na aanpassings-suggestie Marc Woutersen.
  2015-10-01 -   1.0     - Guido Herben   - Procedure KASPAIMPORT na hoofdletteronafhankelijk maken van werklijstnaam/nummer opgeleverd.
  2015-07-23 -   0.0.1   - Guido Herben   - MANTIS-vraag 0011022 oplossen. Invoeren csv-file met KASP-resultaten in Helix.
  */
    --
    -- Parameters
    --
    v_aantal              number := null;
    v_meet_id             number := null;
    v_fractienummer       varchar2(50):= null;
    v_volgnummer          varchar2(4):= null;
    v_marker_name         varchar2(50):= null;
    v_waarde              varchar2(500):= null;
    v_log                 varchar2(2000):= null;
    v_os_user             varchar2(50):= null;
    v_session_user        varchar2(50):= null;
    v_sysdate             varchar2(25):= null;
    v_sender              varchar2(50) := 'berichtenservice.helix@mumc.nl';
    v_recipient           varchar2(100):= 'Guido.Herben@mumc.nl';
    --v_recipient           varchar2(100):= 'Guido.Herben@mumc.nl;rew.konings@mumc.nl;marc.woutersen@mumc.nl;dimitri.beuken@mumc.nl;kim.spee@mumc.nl';
    --v_recipient           varchar2(100):= 'Simone.vandenHeuvel@radboudumc.nl; Cootje.Vermaat@radboudumc.nl; Wendy.Buijsman@radboudumc.nl';
    v_title               varchar2(100) := 'Inlezen KASP data';
    v_message             varchar2(2000):= null;
    v_html                boolean := FALSE;
    v_db_name             varchar2(20) := null;
    v_wlst_name           varchar2(30) := null;
    --
    -- Cursoren
    --
    cursor c_sinf -- Systeem-parameters
    is
      select sys_context('userenv', 'db_name') as db_name
           , sys_context('userenv', 'os_user') as os_user
           , sys_context('userenv', 'session_user') as session_user
           , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss') as systeemdatum
        from dual;
    --
    cursor c_wlst -- Werklijsten
    ( b_werklijstnummer   kgc_werklijsten.werklijst_nummer%type
    )
    is
      select *
        from kgc_werklijsten
       where lower(werklijst_nummer) = lower(b_werklijstnummer) --GH finale aanpassing voor v1.0
    ;
    --
    cursor c_kinv -- Invoertabel
    is
      select * from beh_kaspinvoer
    ;
    --
    cursor c_knmi --KASPA-niet-matchende-werklijst-items-cursor
    ( b_werklijstnummer   kgc_werklijsten.werklijst_nummer%type
    )
    is
      select POSITIE
           , SAMPLENAAM
        from BEH_KASPINVOER
      minus
      select KINV.POSITIE
           , KINV.SAMPLENAAM
        from BEH_KASPINVOER       KINV
           , KGC_WERKLIJSTEN      WERK
           , KGC_WERKLIJST_ITEMS  WLIT
           , KGC_TRAY_VULLING     TRVU
           , BAS_MEETWAARDEN      MEET
           , BAS_METINGEN         METI
           , BAS_FRACTIES         FRAC
       where KINV.POSITIE = UPPER(CONCAT(TRVU.Y_POSITIE,TRVU.X_POSITIE))
         and UPPER(SUBSTR(KINV.SAMPLENAAM,1,INSTR(KINV.SAMPLENAAM,'_')-1)) = UPPER(TRIM(FRAC.FRACTIENUMMER))
         and WLIT.WLST_ID = WERK.WLST_ID
         and TRVU.MEET_ID = WLIT.MEET_ID
         and MEET.MEET_ID = WLIT.MEET_ID
         and METI.METI_ID = MEET.METI_ID
         and FRAC.FRAC_ID = METI.FRAC_ID
         and lower(WERK.WERKLIJST_NUMMER) = lower(b_werklijstnummer) --GH finale aanpassing voor v1.0
       order by 1
    ;
    --
    cursor c_kfmt --BAS_MEETWAARDE_DETAILS-INVOER-formattering
    ( b_werklijstnummer   kgc_werklijsten.werklijst_nummer%type
    )
    is
      select KINV.KLEUR         as COLOR
           , KINV.POSITIE       as POS
           , KINV.SAMPLENAAM    as NAME
           , KINV.FILTERWAARDE1 as FILTERWAARDE1
           , KINV.FILTERWAARDE2 as FILTERWAARDE2
           , CASE TRIM(LOWER(CALLWAARDE))
               WHEN 'allele x'
                 THEN SNIP.CODE || SNIP.CALLALLELEX || '_' || SNIP.ALLELE_X_TYPE
               WHEN 'allele y'
                 THEN SNIP.CODE || SNIP.CALLALLELEY || '_' || SNIP.ALLELE_Y_TYPE
               WHEN 'both alleles'
                 THEN CONCAT(SNIP.CODE,SNIP.CALLALLELEBOTH)
             END               as CALL
           , CASE TRIM(LOWER(CALLWAARDE))
               WHEN 'allele x'
                 THEN SNIP.CALLALLELEX
               WHEN 'allele y'
                 THEN SNIP.CALLALLELEY
               WHEN 'both alleles'
                 THEN SNIP.CALLALLELEX
             END               as ALLEL1
           , CASE TRIM(LOWER(CALLWAARDE))
               WHEN 'allele x'
                 THEN SNIP.CALLALLELEX
               WHEN 'allele y'
                 THEN SNIP.CALLALLELEY
               WHEN 'both alleles'
                 THEN SNIP.CALLALLELEY
             END               as ALLEL2
           , SNIP.STRAND       as STRAND
           , KINV.SCORE        as SCORE
           , MEET.MEET_ID      as MEET_ID
        from BEH_KASPINVOER       KINV
           , KGC_WERKLIJSTEN      WERK
           , KGC_WERKLIJST_ITEMS  WLIT
           , KGC_TRAY_VULLING     TRVU
           , BAS_MEETWAARDEN      MEET
           , BAS_METINGEN         METI
           , BAS_FRACTIES         FRAC
           , BEH_SNIPPET          SNIP
       where KINV.POSITIE = UPPER(CONCAT(TRVU.Y_POSITIE,TRVU.X_POSITIE))
         and UPPER(SUBSTR(KINV.SAMPLENAAM,1,INSTR(KINV.SAMPLENAAM,'_')-1)) = UPPER(TRIM(FRAC.FRACTIENUMMER))
         and not LOWER(KINV.CALLWAARDE) = 'negative'
         and WLIT.WLST_ID = WERK.WLST_ID
         and TRVU.MEET_ID = WLIT.MEET_ID
         and MEET.MEET_ID = WLIT.MEET_ID
         and METI.METI_ID = MEET.METI_ID
         and FRAC.FRAC_ID = METI.FRAC_ID
         and LOWER(SUBSTR(KINV.SAMPLENAAM,LENGTH(KINV.SAMPLENAAM)-8,9)) = LOWER(SUBSTR(SNIP.CODE,LENGTH(SNIP.CODE)-8,9))
         and lower(WERK.WERKLIJST_NUMMER) = lower(b_werklijstnummer) --GH finale aanpassing voor v1.0
       order by POS
    ;
    --
    -- Rowcounters
    --
    r_sinf_rec c_sinf%rowtype;
    r_wlst_rec c_wlst%rowtype;
    r_kinv_rec c_kinv%rowtype;
    r_knmi_rec c_knmi%rowtype;
    r_kfmt_rec c_kfmt%rowtype;
    --
    -- Locale variabelen
    --
    pl_positie            varchar2(3);   --eerste positie waarop ongeldige fractie staat.
    pl_werklijstnr        kgc_werklijsten.werklijst_nummer%type; --haal werklijstnummer op.
    --
    -- Foutafvangingen
    --
    e_onbekende_werklijst      exception; -- werklijst bestaat niet
    e_geen_fracties_ingelezen  exception; -- er zijn geen fracties te herkennen in de inleestabel
    e_ongeldige_fractie        exception; -- er is een niet in de werklijst voorkomende fractie in de inleestabel
    --
    --
    -- Programma
    --
  BEGIN
    pl_werklijstnr:=substr(p_bestandsnaam,1,instr(p_bestandsnaam,'.')-1); -- Zet werklijstnaam in BEH_KASPINVOER-tabel
    update beh_kaspinvoer set werklijstnummer=pl_werklijstnr;
    -- Contextuele gegevens ophalen
    open c_sinf;
    fetch c_sinf
     into r_sinf_rec;
    v_db_name      := r_sinf_rec.db_name;
    v_os_user      := r_sinf_rec.os_user;
    v_session_user := r_sinf_rec.session_user;
    v_sysdate      := r_sinf_rec.systeemdatum;
    close c_sinf;
    v_title := 'Omgeving: '||v_db_name||' '||v_title||' van '||pl_werklijstnr;
    -- Inleestabel op bestaan van werklijst checken
    open c_wlst ( pl_werklijstnr );
    fetch c_wlst into r_wlst_rec;
    if c_wlst%notfound
    then raise e_onbekende_werklijst;
    end if;
    close c_wlst;
    -- Inleestabel op aanwezigheid samplenamen checken;
    open c_kinv;
    fetch c_kinv into r_kinv_rec;
    if c_kinv%notfound
      then raise e_geen_fracties_ingelezen;
    end if;
    close c_kinv;
    -- Inleestabel op geldige samplenamen-lijst checken;
    open c_knmi ( pl_werklijstnr );
    fetch c_knmi
     into r_knmi_rec;
    if c_knmi%FOUND
      then pl_positie:=r_knmi_rec.positie;
           raise e_ongeldige_fractie;
    end if;
    close c_knmi;
    -- Invoeren nieuwe gegevens in BAS_MEETWAARDE_DETAILS
    for r_kfmt_rec in c_kfmt ( pl_werklijstnr )
    loop
      update bas_meetwaarde_details
         set waarde=r_kfmt_rec.call
       where meet_id=r_kfmt_rec.meet_id
         and prompt='Call';
      update bas_meetwaarde_details
         set waarde = r_kfmt_rec.score
       where meet_id=r_kfmt_rec.meet_id
         and prompt='Quality Value';
      update bas_meetwaarde_details
         set waarde=r_kfmt_rec.allel1
       where meet_id=r_kfmt_rec.meet_id
         and prompt='Allel_1';
      update bas_meetwaarde_details
         set waarde = r_kfmt_rec.allel2
       where meet_id=r_kfmt_rec.meet_id
         and prompt='Allel_2'   ;
      update bas_meetwaarde_details
         set waarde = r_kfmt_rec.strand
       where meet_id=r_kfmt_rec.meet_id
         and prompt='Strand'   ;
    end loop;
    commit;
    v_log := ' -GOED INGELEZEN- '||chr(13)||chr(10)||'De werklijst met nummer '||pl_werklijstnr|| ' is correct ingelezen.'||chr(13)||chr(10)||'Rapportage d.d. '||v_sysdate||' van HELIX-sessiegebruiker '||v_session_user||'.';
    insert into BEH_KASPINVOER_log(werklijstnummer, log_tekst, datum_tijd, os_user, session_user) values(pl_werklijstnr, v_log, v_sysdate, v_os_user, v_session_user);
    commit;
    v_message := v_log;
    v_message := substr(v_message, 1, 2000);

    --
    -- Exceptions
    --
  exception
      when e_onbekende_werklijst then
      rollback;
      v_log := ' -FOUTMELDING- '||chr(13)||chr(10)||'De werklijst met nummer '||pl_werklijstnr|| ' bestaat niet.'||chr(13)||chr(10)||'Rapportage d.d. '||v_sysdate||' van HELIX-sessiegebruiker '||v_session_user||'.';
      insert into BEH_KASPINVOER_log(werklijstnummer, log_tekst, datum_tijd, os_user, session_user) values(pl_werklijstnr, v_log, v_sysdate, v_os_user, v_session_user);
      commit;
      v_message := v_log;
      v_message := substr(v_message, 1, 2000);
      Raise_Application_Error(-20343,v_message);

      when e_geen_fracties_ingelezen then
      rollback;
      v_log := ' -FOUTMELDING- '||chr(13)||chr(10)||'In het bestand '||chr(34)||p_bestandsnaam||chr(34)||' zijn geen fracties gevonden.'||chr(13)||chr(10)||'Rapportage d.d. '||v_sysdate||' van HELIX-sessiegebruiker '||v_session_user||'.';
      insert into BEH_KASPINVOER_log(werklijstnummer, log_tekst, datum_tijd, os_user, session_user) values(pl_werklijstnr, v_log, v_sysdate, v_os_user, v_session_user);
      commit;
      v_message := v_log;
      v_message := substr(v_message, 1, 2000);
      Raise_Application_Error(-20343,v_message);

      when e_ongeldige_fractie then
      rollback;
      v_log := ' -FOUTMELDING- '||chr(13)||chr(10)||'Fractie op positie '||pl_positie||' klopt niet met werklijst '||pl_werklijstnr||'.'||chr(13)||chr(10)||'Rapportage d.d. '||v_sysdate||' van HELIX-sessiegebruiker '||v_session_user||'.';
      insert into BEH_KASPINVOER_log(werklijstnummer, log_tekst, datum_tijd, os_user, session_user) values(pl_werklijstnr, v_log, v_sysdate, v_os_user, v_session_user);
      commit;
      v_message := v_log;
      v_message := substr(v_message, 1, 2000);
      Raise_Application_Error(-20343,v_message);

      when others then
      rollback;
      v_log := ' -FOUTMELDING- '||chr(13)||chr(10)||'Verwerking van bestand '||chr(34)||p_bestandsnaam||chr(34)||' is mislukt.'||chr(13)||chr(10)||'Rapportage d.d. '||v_sysdate||' van HELIX-sessiegebruiker '||v_session_user||'.'||chr(13)||chr(10)||chr(13)||chr(10)||'Foutmelding vanuit Oracle :'||chr(13)||chr(10)||SQLCODE||'  '||SUBSTR(SQLERRM, 1, 200)||'.';
      insert into BEH_KASPINVOER_log(werklijstnummer, log_tekst, datum_tijd, os_user, session_user) values(pl_werklijstnr, v_log, v_sysdate, v_os_user, v_session_user);
      commit;
      v_message := v_log;
      v_message := substr(v_message, 1, 2000) ;
      Raise_Application_Error(-20343,v_message);


  END KASPAIMPORT;

  PROCEDURE DIAGIMPORT
  IS
  --
  -- Script voor geautomatiseerde invoer van diagnoses.
  -- De KGC_DIAGNOSES-tabel dient aangevuld te worden met data uit een csv-file.
  --
  -- 2015-06-04 Versie 1.2   - Guido Herben - Door wijziging van de systematiek door Marc Woutersen procedure moeten aanpassen...CIN_HFD_44 is nu CIN_HFD geworden.
  -- 2015-02-05 Versie 1.1   - Guido Herben - Afvangen foutloze rapportages en lege diagnose-datum werkend.
  -- 2015-01-28 Versie 1.0   - Guido Herben - MANTIS-vraag 009589 opgelost. Invoeren csv-file met diagnoses ter afronding onderzoeken.
  --
  --
  -- Definieer cursoren
  --
  cursor c_dinv --diagnose-invoer-cursor
  is
    select dinv.*
      from beh_invoer_diagnoses dinv
  ;
  --
  cursor c_dchk --diagnose-invoer-check-cursor
  ( b_patientnaam   kgc_personen.achternaam%type
  , b_geboortedatum kgc_personen.geboortedatum%type
  , b_x_nr          kgc_onderzoeken.onderzoeknr%type
  )
  is
    select pers.pers_id
      from kgc_personen              pers
         , kgc_onderzoek_betrokkenen onbe
         , kgc_onderzoeken           onde
     where (   (upper(pers.achternaam) like '%'||upper(b_patientnaam)||'%')
            or (upper(pers.achternaam_partner) like '%'||upper(b_patientnaam)||'%')
           )
       and pers.geboortedatum = b_geboortedatum
       and onde.onderzoeknr = b_x_nr
       and onbe.pers_id = pers.pers_id
       and onbe.onde_id = onde.onde_id
  ;
  --
  cursor c_dvdc --validate-diagnose-code-cursor
  ( b_diagnose_code beh_invoer_diagnoses.diagnose_code%type )
  is
    select ziektecode
      from cin_hfd cinh
     where lpad(b_diagnose_code,4,'0') = cinh.ziektecode
  ;
  --
  -- Definieer rowcounters
  --
  r_dchk_rec c_dchk%rowtype;
  --
  r_dvdc_rec c_dchk%rowtype;
  --
  --
  -- Definieer locale variabelen
  --
  pl_misn    varchar2(100); --errornote
  pl_pers_id kgc_personen.pers_id%type; --persoonstype uit persoonstabel. Bewaar persoon.
  pl_count   numeric(4); --tellertje voor correcte records.
  --
  -- Programma
  --
  BEGIN
    pl_count:=0;
    for r_dinv_rec in c_dinv
    loop
      pl_misn:=NULL;
      if r_dinv_rec.datum_diagnose is NULL -- Check op aanwezigheid diagnose_datum
      then
        pl_misn:='De diagnose_datum is niet ingevuld' ;
      end if;
     --
      if r_dinv_rec.diagnose_code is NULL -- Check op aanwezigheid diagnose_code
      then
        pl_misn:='De diagnose_code is niet ingevuld' ;
      end if;
      --
      if length(r_dinv_rec.diagnose_code) > 4 -- Check op te grote diagnose_code
      then
        pl_misn:='De diagnose_code is te lang en dus ongeldig' ;
      end if;
      --
       if pl_misn is NULL -- Check of diagnose_code voorkomt in CIN_HFD
      then
        open c_dvdc( lpad(r_dinv_rec.diagnose_code,4,'0') );
          fetch c_dvdc into r_dvdc_rec;
          if c_dvdc%NOTFOUND
          then
            pl_misn:='De diagnose_code is ongeldig' ;
          end if;
        close c_dvdc;
      end if;
      --
      if pl_misn is NULL -- Check of er een juiste persoon gebonden is aan de invoer.
      then
        open c_dchk( r_dinv_rec.patientnaam
                  , to_date(r_dinv_rec.geboortedatum,'dd-mm-yyyy')
                  , r_dinv_rec.x_nr
                  );
          fetch c_dchk
          into r_dchk_rec;
          case
            when c_dchk%ROWCOUNT = 0
              then pl_misn:='Geen juiste persoon gevonden';
            when c_dchk%ROWCOUNT = 1
              then pl_pers_id:=r_dchk_rec.pers_id;
            else pl_misn:='Meerdere personen gevonden'; -- rowcount > 1 m.a.w. meerdere personen gevonden.
          end case;
        close c_dchk;
      end if;
      --
      if pl_misn is NULL -- Insert in KGC_DIAGNOSES als record wel ok.
      then
        insert into kgc_diagnoses
                     ( onde_id
                     , onbe_id
                     , dist_id
                     , diagnose_code
                     , mede_id_autorisator
                     , coty_id
                     , ermo_id
                     , herk_id
                     , zekerhed_diagnose
                     , zekerheid_status
                     , zekerheid_erfmodus
                     , commentaar
                     , geverifieerd
                     , pers_id
                     , datum
                     )
                     values
                     ( (select onde.onde_id
                          from kgc_onderzoeken onde
                         where upper(onde.onderzoeknr) = upper(r_dinv_rec.x_nr)) -- GH 2015-01-15 Na opdrachtwijziging door DB alsnog meegenomen i.p.v. NULL-waarde.
                     , NULL
                     , NULL
                     , lpad(r_dinv_rec.diagnose_code,4,'0')
                     , (select mede.mede_id
                          from kgc_medewerkers mede
                         where upper(mede.code) = upper(r_dinv_rec.counceler)
                       )
                     , 4
                     , NULL
                     , NULL
                     , CASE
                         WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='UI' then '0'
                         WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='ON' then '1'
                         WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='MO' then '2'
                         WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='WA' then '3'
                         WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='ZE' then '4'
                         WHEN r_dinv_rec.ZEKERHEID is NULL then NULL -- GH 2015-01-09 Antwoord DB ... Indien in te importeren csv-bestand NULL dan moet dit NULL blijven.
                       END
                     , NULL
                     , NULL
                     , NULL
                     , 'N' -- GH 2015-01-09 Antwoord DB ... Als default N (Nee) hanteren.
                     , pl_pers_id
                     , to_date(r_dinv_rec.datum_diagnose,'dd-mm-yyyy')
                     );
      else -- Er is een foutief record gevonden, dus invoer in fouttabel.
        insert into beh_inverr_diagnoses
                  ( DATUM_DIAGNOSE
                  , PATIENTNAAM
                  , GEBOORTEDATUM
                  , X_NR
                  , DIAGNOSE_CODE
                  , ZEKERHEID
                  , COUNCELER
                  , OVERIGE_INFO
                  , FOUTTIJDSTEMPEL
                  , FOUTREDEN
                  )
                  values
                  ( CASE
                      WHEN r_dinv_rec.DATUM_DIAGNOSE is NULL then 'Geen datum'
                       ELSE r_dinv_rec.DATUM_DIAGNOSE
                    END
                  , r_dinv_rec.PATIENTNAAM
                  , r_dinv_rec.GEBOORTEDATUM
                  , r_dinv_rec.X_NR
                  , lpad(r_dinv_rec.DIAGNOSE_CODE,4,'0')
                  , r_dinv_rec.ZEKERHEID
                  , r_dinv_rec.COUNCELER
                  , r_dinv_rec.OVERIGE_INFO
                  , to_char (sysdate, 'YYYY-MM-DD HH24:MI:SS')
                  , pl_misn
                  );
      end if;
        --
      if pl_misn is NULL -- Insert in KGC_DIAGNOSES als record wel ok.
        then
        pl_count:=pl_count + 1 ;
      end if;
      --
      --
      -- Sluit onderzoek af --
      --
      update kgc_onderzoeken onde
           set onde.afgerond = 'J'
           ,   onde.mede_id_autorisator = (select mede.mede_id
                                             from kgc_medewerkers mede
                                            where upper(mede.code) = upper(r_dinv_rec.counceler)
                                           )
           ,   onde.datum_autorisatie = to_date(r_dinv_rec.datum_diagnose,'dd-mm-yyyy')
         where upper(onde.onderzoeknr) = upper(r_dinv_rec.x_nr)
           and onde.afgerond != 'J'
        ;
      --
    end loop;
    commit;
    --
    insert into beh_inverr_diagnoses
                ( DATUM_DIAGNOSE
                , PATIENTNAAM
                , GEBOORTEDATUM
                , X_NR
                , DIAGNOSE_CODE
                , ZEKERHEID
                , COUNCELER
                , OVERIGE_INFO
                , FOUTTIJDSTEMPEL
                , FOUTREDEN
                )
                values
                ( to_char (sysdate, 'YYYY-MM-DD')
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , to_char (sysdate, 'YYYY-MM-DD HH24:MI:SS')
                , 'Er zijn '||pl_count|| ' records goed gegaan.'
                );
    commit;
  END DIAGIMPORT;

  PROCEDURE STOFIMPORT ( p_inlezen varchar2, p_protocol varchar2 )
  AS
  --
  -- Script voor geautomatiseerde invoer van stoftesten.
  -- De KGC_STOFTESTEN-tabel en, indien nodig, de KGC_STOFTESTGROEPEN-tabel dient/dienen aangevuld te worden met data uit een csv-file.
  --
  -- 2015-12-04 Versie 1.0   - Guido Herben - Goedgekeurde versie na tests Kim Spee.
  -- 2015-11-02 Versie 0.0.1 - Guido Herben - MANTIS-vraag 11211 Invoeren csv-file met stoftesten en eventueel protocollen.
  --
   --
  -- Definieer cursoren
  --
  CURSOR c_stof ( b_code varchar2 ) -- KGC_STOFTESTEN-sleutel (PK) binnenhalen
  IS
    SELECT stof.stof_id
         , stof.code
      FROM KGC_STOFTESTEN stof
     WHERE stof.code = b_code
  ;
  --
  CURSOR c_prot ( b_code varchar2 ) -- KGC_STOFTGROEPEN-sleutel (PK) binnenhalen
  IS
    SELECT stgr.stgr_id
         , stgr.code
      FROM KGC_STOFTESTGROEPEN stgr
     WHERE stgr.code = b_code
  ;
  --
  CURSOR c_check -- controle op al bestaan van de stoftest-gegevens in KGC_STOFTESTEN
  IS
    SELECT bins.code
      FROM BEH_INVOER_STOFTESTEN bins
         , KGC_STOFTESTEN stof
     WHERE stof.code    = upper(bins.code)
       AND stof.kafd_id = 48
  ;
  --
  CURSOR c_multi -- controle op meervoudig voorkomen van gegevens meermaals in het invoerbestand
  IS
    SELECT code
      FROM BEH_INVOER_STOFTESTEN
     GROUP BY code
    HAVING COUNT(*) > 1
  ;
  --
  CURSOR c_nitst --  controle op aantal n�t nieuw ingevoerde stoftesten
  IS
    SELECT stof.code
      FROM KGC_STOFTESTEN stof
     WHERE stof.kafd_id = 48
       AND creation_date >= sysdate-(1/1440);
  --
  CURSOR c_bins --  BEH_INVOER_STOFTESTEN-gegevens binnenhalen
  IS
    SELECT *
      FROM BEH_INVOER_STOFTESTEN
     WHERE protocol is not NULL
  ;
  --
  CURSOR c_foutrij ( b_code varchar2 ) --  BEH_INVOER_STOFTESTEN-gegevens als foutrij ophalen
  IS
    SELECT *
      FROM BEH_INVOER_STOFTESTEN bins
     WHERE bins.protocol is not NULL
       AND bins.code     = b_code
  ;
  --
  -- Definieer rowtypes, rowcounters...
  --
  r_stof           c_stof%ROWTYPE;
  r_prot           c_prot%ROWTYPE;
  r_check          c_check%ROWTYPE;
  r_multi          c_multi%ROWTYPE;
  r_nitst          c_nitst%ROWTYPE;
  r_foutrij        c_foutrij%ROWTYPE; -- 1e Foutrecord afvangen.
  --
  -- Definieer parameters
  --
  vlogs            beh_log%ROWTYPE;
  v_melding        varchar2(2000):= null;
  v_count          integer := 0 ;
  --
  -- Foutafvangingen
  --
  e_multiples                exception; -- Stoftest-item komt meermaals voor.
  e_uniekesleutel            exception; -- Controle sleutelveld(en)-uniciteit.
  e_geenstoftestinvoer       exception; -- Controle of er daadwerkelijk iets is ingevoerd.
  e_geenprotrecords          exception; -- Er zijn geen te importeren protocol-records.
  e_geenprstofrecords        exception; -- Er zijn geen te importeren stoftest-records bij het protocol.
  --
  --
  -- Programma
  --
  BEGIN
    DELETE FROM BEH_INVOER_STOFTESTEN -- Verwijder zogenaamde codeloze items uit invoertabel.
     WHERE code is NULL;
    DELETE FROM BEH_INVOER_STOFTESTEN
     WHERE UPPER(code) LIKE 'CODE'
       AND UPPER(protocol) LIKE 'PROTOCOL';
    COMMIT;
    --
    BEGIN
      IF p_inlezen = 'J'
        THEN -- controle op meervoudig voorkomen van gegevens meermaals in het invoerbestand
          OPEN c_multi ;
            LOOP
              FETCH c_multi
                INTO r_multi;
              IF c_multi%FOUND
                THEN RAISE e_multiples;
                ELSE EXIT;
              END IF;
            END LOOP;
          CLOSE c_multi;
          OPEN c_check;
            LOOP
              FETCH c_check
                INTO r_check;
              IF c_check%FOUND
                THEN RAISE e_uniekesleutel;
                ELSE EXIT;
              END IF;
            END LOOP;
          CLOSE c_check;
          INSERT INTO kgc_stoftesten -- Voeg de nieuwe stoftesten toe
            ( kafd_id,code
            , omschrijving
            , meet_reken
            , vervallen
            , omschrijving_lang
            , tech_id
            , mwst_id
            , stof_id_voor
            )
          SELECT 48
               , bins.code
               , bins.omschrijving
               , substr(bins.meet_reken,1,1)
               , bins.vervallen
               , bins.omschrijving_lang
               , tech.tech_id
               , mwst.mwst_id
               , '' stof_id_voor
            FROM BEH_INVOER_STOFTESTEN   bins
               , KGC_TECHNIEKEN          tech
               , KGC_MEETWAARDESTRUCTUUR mwst
           WHERE bins.tech_omschrijving = tech.code (+) -- Right Join op tech,code
             AND bins.mwst_omschrijving = mwst.code (+) -- Right Join op mwst,code
             AND bins.code is not null;
          COMMIT;
          --
          OPEN c_nitst;
            LOOP
              FETCH c_nitst
                INTO r_nitst;
              IF c_nitst%NOTFOUND
                THEN RAISE e_geenstoftestinvoer;
                ELSE EXIT;
              END IF;
            END LOOP;
          CLOSE c_nitst;
          --
          INSERT INTO BEH_INVERR_STOFTESTEN
                    ( KAFD
                    , CODE
                    , OMSCHRIJVING
                    , MEET_REKEN
                    , VERVALLEN
                    , TECH_OMSCHRIJVING
                    , VOORTEST_CODE
                    , MWST_OMSCHRIJVING
                    , OMSCHRIJVING_LANG
                    , PROTOCOL
                    , STANDAARD
                    , FOUTTIJDSTEMPEL
                    , FOUTREDEN
                    )
                    values
                    ( NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                    , 'Alle stoftesten goed ingelezen'
                    );
          COMMIT;
        --
        ELSE NULL;
      END IF;
      --
      IF p_protocol = 'J'
        THEN -- Doorzoek het hele inleesbestand en verwerk de records naar de protocol tabel
          FOR r_bins in c_bins
            LOOP
              OPEN c_prot( r_bins.protocol );
                FETCH c_prot
                  INTO r_prot ;
                IF c_prot%NOTFOUND
                  THEN RAISE e_geenprotrecords;
                  ELSE NULL;
                END IF;
              CLOSE c_prot;
              OPEN c_stof( UPPER(r_bins.code) );
                FETCH c_stof
                  INTO r_stof ;
                IF c_stof%NOTFOUND
                  THEN RAISE e_geenprstofrecords;
                  ELSE NULL;
                END IF;
              CLOSE c_stof;
              --
              INSERT INTO KGC_PROTOCOL_STOFTESTEN
                            ( prst_id
                            , stof_id
                            , standaard
                            , created_by
                            , creation_date
                            , last_updated_by
                            , last_update_date
                            , stgr_id
                            )
                VALUES ( KGC_PRST_SEQ.nextval
                       , r_stof.stof_id
                       , r_bins.standaard
                       , 'HELIX'
                       , sysdate
                       , 'HELIX'
                       , sysdate
                       , r_prot.stgr_id
                       );
            END LOOP;
        ELSE NULL;
      END IF;
      COMMIT;
      INSERT INTO BEH_INVERR_STOFTESTEN
                ( KAFD
                , CODE
                , OMSCHRIJVING
                , MEET_REKEN
                , VERVALLEN
                , TECH_OMSCHRIJVING
                , VOORTEST_CODE
                , MWST_OMSCHRIJVING
                , OMSCHRIJVING_LANG
                , PROTOCOL
                , STANDAARD
                , FOUTTIJDSTEMPEL
                , FOUTREDEN
                )
                values
                ( NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , NULL
                , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                , 'Alle protocollen goed ingelezen'
                );
      COMMIT;
    --
    EXCEPTION -- Foutafhandeling.
      --
      WHEN e_multiples
        THEN ROLLBACK;
        v_melding     := v_melding || r_multi.code || ' ';
        vlogs.datum   := sysdate;
        vlogs.melding := v_melding;
        vlogs.proc    := 'BEH_IMPEXP.STOFIMPORT';
        vlogs.Error   := 'J';
        vlogs.stap    := 'Controle Meermaals Voorkomende Records';
        vlogs.Type    := 'IMPORT';
        v_melding     := 'FOUTMELDING d.d. ' || vlogs.datum || ' met melding bij stofcode ' || v_melding || ' tijdens de processtap ' || vlogs.stap || '. GEEN stoftesten ingelezen.' ;
        LOGINSERT(vlogs);
        OPEN c_foutrij (r_multi.code) ;
          FETCH c_foutrij
            INTO r_foutrij;
          INSERT INTO BEH_INVERR_STOFTESTEN
                    ( KAFD
                    , CODE
                    , OMSCHRIJVING
                    , MEET_REKEN
                    , VERVALLEN
                    , TECH_OMSCHRIJVING
                    , VOORTEST_CODE
                    , MWST_OMSCHRIJVING
                    , OMSCHRIJVING_LANG
                    , PROTOCOL
                    , STANDAARD
                    , FOUTTIJDSTEMPEL
                    , FOUTREDEN
                    )
                    values
                    ( r_foutrij.kafd
                    , r_foutrij.code
                    , r_foutrij.omschrijving
                    , r_foutrij.meet_reken
                    , r_foutrij.vervallen
                    , r_foutrij.tech_omschrijving
                    , r_foutrij.voortest_code
                    , r_foutrij.mwst_omschrijving
                    , r_foutrij.omschrijving_lang
                    , r_foutrij.protocol
                    , r_foutrij.standaard
                    , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                    , v_melding
                    );
          COMMIT;
        CLOSE c_foutrij;
        Raise_Application_Error(-20343,v_melding);
      --
      WHEN e_uniekesleutel
        THEN ROLLBACK;
        v_melding     := v_melding || r_check.code || ' ' ;
        vlogs.Datum   := sysdate;
        vlogs.melding := v_melding;
        vlogs.proc    := 'BEH_IMPEXP.STOFIMPORT';
        vlogs.Error   := 'J';
        vlogs.stap    := 'Controle Unique Key';
        vlogs.Type    := 'IMPORT';
        v_melding     := 'FOUTMELDING d.d. ' || vlogs.datum || ' met melding bij stofcode ' || v_melding || ' tijdens de processtap ' || vlogs.stap || '. GEEN stoftesten ingelezen.' ;
        LOGINSERT(vlogs);
        OPEN c_foutrij (r_check.code) ;
          FETCH c_foutrij
            INTO r_foutrij;
          INSERT INTO BEH_INVERR_STOFTESTEN
                    ( KAFD
                    , CODE
                    , OMSCHRIJVING
                    , MEET_REKEN
                    , VERVALLEN
                    , TECH_OMSCHRIJVING
                    , VOORTEST_CODE
                    , MWST_OMSCHRIJVING
                    , OMSCHRIJVING_LANG
                    , PROTOCOL
                    , STANDAARD
                    , FOUTTIJDSTEMPEL
                    , FOUTREDEN
                    )
                    values
                    ( r_foutrij.kafd
                    , r_foutrij.code
                    , r_foutrij.omschrijving
                    , r_foutrij.meet_reken
                    , r_foutrij.vervallen
                    , r_foutrij.tech_omschrijving
                    , r_foutrij.voortest_code
                    , r_foutrij.mwst_omschrijving
                    , r_foutrij.omschrijving_lang
                    , r_foutrij.protocol
                    , r_foutrij.standaard
                    , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                    , v_melding
                    );
          COMMIT;
        CLOSE c_foutrij;
        Raise_Application_Error(-20343,v_melding);
      --
      WHEN e_geenstoftestinvoer
        THEN ROLLBACK;
        v_melding     := v_melding ;
        vlogs.Datum   := sysdate;
        vlogs.melding := v_melding;
        vlogs.proc    := 'BEH_IMPEXP.STOFIMPORT';
        vlogs.Error   := 'J';
        vlogs.stap    := 'Controle Invoer';
        vlogs.Type    := 'IMPORT';
        v_melding     := 'FOUTMELDING d.d. ' || vlogs.datum || ' . Er zijn GEEN stoftesten ingelezen.' ;
        LOGINSERT(vlogs);
        INSERT INTO BEH_INVERR_STOFTESTEN
                  ( KAFD
                  , CODE
                  , OMSCHRIJVING
                  , MEET_REKEN
                  , VERVALLEN
                  , TECH_OMSCHRIJVING
                  , VOORTEST_CODE
                  , MWST_OMSCHRIJVING
                  , OMSCHRIJVING_LANG
                  , PROTOCOL
                  , STANDAARD
                  , FOUTTIJDSTEMPEL
                  , FOUTREDEN
                  )
                  values
                  ( NULL
                  , NULL
                  , NULL
                  , NULL
                  , NULL
                  , NULL
                  , NULL
                  , NULL
                  , NULL
                  , NULL
                  , NULL
                  , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                  , v_melding
                  );
        COMMIT;
        Raise_Application_Error(-20343,v_melding);
      --
      WHEN e_geenprotrecords
        THEN ROLLBACK;
        v_melding := 'Protocol ' || r_prot.code || ' niet aanwezig' ;
        vlogs.Datum   := sysdate;
        vlogs.melding := v_melding;
        vlogs.proc    := 'BEH_IMPEXP.STOFIMPORT';
        vlogs.Error   := 'J';
        vlogs.stap    := 'Geen te importeren protocol-records';
        vlogs.Type    := 'IMPORT';
        v_melding     := 'FOUTMELDING d.d. ' || vlogs.datum || ' met melding ' || v_melding || ' tijdens de processtap ' || vlogs.stap || '. ' ;
        LOGINSERT(vlogs);
        OPEN c_foutrij (r_prot.code) ;
          FETCH c_foutrij
            INTO r_foutrij;
          INSERT INTO BEH_INVERR_STOFTESTEN
                    ( KAFD
                    , CODE
                    , OMSCHRIJVING
                    , MEET_REKEN
                    , VERVALLEN
                    , TECH_OMSCHRIJVING
                    , VOORTEST_CODE
                    , MWST_OMSCHRIJVING
                    , OMSCHRIJVING_LANG
                    , PROTOCOL
                    , STANDAARD
                    , FOUTTIJDSTEMPEL
                    , FOUTREDEN
                    )
                    values
                    ( r_foutrij.kafd
                    , r_foutrij.code
                    , r_foutrij.omschrijving
                    , r_foutrij.meet_reken
                    , r_foutrij.vervallen
                    , r_foutrij.tech_omschrijving
                    , r_foutrij.voortest_code
                    , r_foutrij.mwst_omschrijving
                    , r_foutrij.omschrijving_lang
                    , r_foutrij.protocol
                    , r_foutrij.standaard
                    , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                    , v_melding
                    );
          COMMIT;
        CLOSE c_foutrij;
        Raise_Application_Error(-20343,v_melding);
      --
      WHEN e_geenprstofrecords
        THEN ROLLBACK;
        v_melding := 'Stoftest ' || r_stof.code || ' niet aanwezig' ;
        vlogs.Datum   := sysdate;
        vlogs.melding := v_melding;
        vlogs.proc    := 'BEH_IMPEXP.STOFIMPORT';
        vlogs.Error   := 'J';
        vlogs.stap    := 'Geen te importeren stoftest-records';
        vlogs.Type    := 'IMPORT';
        v_melding     := 'FOUTMELDING d.d. ' || vlogs.datum || ' met melding ' || v_melding || ' tijdens de processtap ' || vlogs.stap || ', dus GEEN bij het protocol behorende stoftesten ingelezen.' ;
        LOGINSERT(vlogs);
        OPEN c_foutrij (r_stof.code) ;
          FETCH c_foutrij
            INTO r_foutrij;
          INSERT INTO BEH_INVERR_STOFTESTEN
                    ( KAFD
                    , CODE
                    , OMSCHRIJVING
                    , MEET_REKEN
                    , VERVALLEN
                    , TECH_OMSCHRIJVING
                    , VOORTEST_CODE
                    , MWST_OMSCHRIJVING
                    , OMSCHRIJVING_LANG
                    , PROTOCOL
                    , STANDAARD
                    , FOUTTIJDSTEMPEL
                    , FOUTREDEN
                    )
                    values
                    ( r_foutrij.kafd
                    , r_foutrij.code
                    , r_foutrij.omschrijving
                    , r_foutrij.meet_reken
                    , r_foutrij.vervallen
                    , r_foutrij.tech_omschrijving
                    , r_foutrij.voortest_code
                    , r_foutrij.mwst_omschrijving
                    , r_foutrij.omschrijving_lang
                    , r_foutrij.protocol
                    , r_foutrij.standaard
                    , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                    , v_melding
                    );
          COMMIT;
        CLOSE c_foutrij;
        Raise_Application_Error(-20343,v_melding);
      --
      WHEN others
        THEN ROLLBACK;
        v_melding     := 'Onbekend/Andere fout. ' ;
        vlogs.Datum   := sysdate;
        vlogs.melding := v_melding;
        vlogs.proc    := 'BEH_IMPEXP.STOFIMPORT';
        vlogs.Error   := 'J';
        vlogs.Type    := 'IMPORT';
        v_melding     := 'FOUTMELDING d.d. ' || vlogs.datum || ' met melding ' || v_melding || ' tijdens de processtap ' || vlogs.stap || '.' ;
        LOGINSERT(vlogs);
        OPEN c_foutrij (r_multi.code) ;
          FETCH c_foutrij
            INTO r_foutrij;
          INSERT INTO BEH_INVERR_STOFTESTEN
                    ( KAFD
                    , CODE
                    , OMSCHRIJVING
                    , MEET_REKEN
                    , VERVALLEN
                    , TECH_OMSCHRIJVING
                    , VOORTEST_CODE
                    , MWST_OMSCHRIJVING
                    , OMSCHRIJVING_LANG
                    , PROTOCOL
                    , STANDAARD
                    , FOUTTIJDSTEMPEL
                    , FOUTREDEN
                    )
                    values
                    ( NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , NULL
                    , to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')
                    , v_melding
                    );
          COMMIT;
        CLOSE c_foutrij;
        Raise_Application_Error(-20343,v_melding);
    --
    END;

  END STOFIMPORT;
  --
  PROCEDURE STOFPUSH
  AS
  BEGIN
    BEH_IMPEXP.STOFIMPORT(null,null);
    BEH_IMPEXP.STOFIMPORT('J','J');
    BEH_IMPEXP.STOFIMPORT('J','N');
    BEH_IMPEXP.STOFIMPORT('J','');
    BEH_IMPEXP.STOFIMPORT('J',null);
    BEH_IMPEXP.STOFIMPORT('N','J');
    BEH_IMPEXP.STOFIMPORT('N','N');
    BEH_IMPEXP.STOFIMPORT('N','');
    BEH_IMPEXP.STOFIMPORT('N',null);
    BEH_IMPEXP.STOFIMPORT('','J');
    BEH_IMPEXP.STOFIMPORT('','N');
    BEH_IMPEXP.STOFIMPORT('','');
    BEH_IMPEXP.STOFIMPORT('',null);
    BEH_IMPEXP.STOFIMPORT(null,'J');
    BEH_IMPEXP.STOFIMPORT(null,'N');
    BEH_IMPEXP.STOFIMPORT(null,'');
    BEH_IMPEXP.STOFIMPORT(null,null);
    COMMIT ;
  END STOFPUSH;
  --
  PROCEDURE LOGINSERT (pLog beh_Log%RowType)
  AS
  BEGIN
    INSERT INTO beh_log
    VALUES plog ;
    COMMIT ;
  END LOGINSERT;
  --
END BEH_IMPEXP;
/

/
QUIT
