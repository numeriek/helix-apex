CREATE OR REPLACE PACKAGE "HELIX"."MSS_ONDE_00" IS

-- zet de resultaattekst (meting) van een onderzoek van de afdeling DNA in een (1) veld
-- gebruik alleen de teksten van de onderzochte persoon
FUNCTION resultaat_tekst_dna
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( resultaat_tekst_dna, WNDS, WNPS, RNPS );

END MSS_ONDE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_ONDE_00" IS
TYPE onde_id_tab_type IS TABLE OF NUMBER
INDEX BY BINARY_INTEGER; -- onde_id
onde_id_tab onde_id_tab_type;

FUNCTION resultaat_tekst_dna
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(32767);
  CURSOR ores_cur
  IS
    SELECT resultaat
    FROM   mss_onde_resultaten_dna_vw
    WHERE  onde_id = p_onde_id
    ORDER BY last_update_date
    ;
BEGIN
  FOR r IN ores_cur
  LOOP
    IF ( v_tekst IS NULL )
    THEN
      v_tekst := r.resultaat;
    ELSE
      IF ( INSTR( v_tekst, r.resultaat ) = 0 )
      THEN
        v_tekst := v_tekst||CHR(10)||r.resultaat;
      END IF;
    END IF;
  END LOOP;
  RETURN ( v_tekst );
END resultaat_tekst_dna;

END mss_onde_00;
/

/
QUIT
