CREATE OR REPLACE PACKAGE "HELIX"."BEH_UTILS" is

  -- Author  : RKON
  -- Created : 11-7-2013 11:01:55
  -- Purpose : utilities voor het inlezen en bewerken van gegevens

  -- Public type declarations

Procedure Inlezen_StofTesten(p_inlezen varchar2, p_protocol varchar2); -- Procedure Inlezen Stoftesten

Procedure Insert_Log (pLog beh_Log%RowType) ;
Procedure Import_Uitslag(p_import clob, p_uits_id Number, p_onde_id number, p_best_id number);
Procedure Update_status_cyto_best(p_best_id Number, p_status varchar2);

PRocedure CheckDeclaraties ;
Function GetCategorieWaarde(Telcode varchar2) return varchar2;

Function bepaal_pers_Id(ppers_id number, pid number, pentiteit varchar2 ) return number;

end Beh_Utils;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_UTILS" is

Procedure Inlezen_StofTesten(p_inlezen varchar2, p_protocol varchar2)

As
  -- Cursor declaraties

-- Ophalen PK KGC_STOFTESTEN OP BASIS VAN STOFTEST CODE
  Cursor C_stof (c_code varchar2 )
  Is
   Select stof.Stof_id
   From Kgc_stoftesten Stof
   Where stof.code = c_code;

-- Ophalen PK KGC_STOFTGROEPEN OP BASIS VAN STOFTESTGROEP CODE
  Cursor C_Prot(c_code varchar2)
  Is
   Select stgr_id
   from kgc_stoftestgroepen stgr
   where stgr.code = c_code;

-- Controleer of de gegevens reeds voorkomen
  Cursor c_check
  Is
    Select ins.code
    from  inlees_stoftesten ins,
          kgc_stoftesten stof
    Where  stof.code = upper(ins.code);

-- Controleer of de gegevens dubbel in het bestand voorkomen
  Cursor C_dubbel
  Is
    Select code
    from inlees_stoftesten
    group by code
    having count(*) > 1;

--  Ophalen gegevens Inlees_stoftest
  Cursor c_ins
  Is
    Select *
    From inlees_stoftesten
    Where protocol is not null;


  -- Records(RowTypes) behorende bij de cursors
  r_stof   c_stof%RowType;
  r_prot   c_prot%RowType;
  r_check  c_check%RowType;
  r_dubbel c_dubbel%RowType;
  vLog beh_log%RowType;

  v_melding Varchar2(2000);
  v_Stap varchar2(50);

  EDubbel Exception;
  ECheck  Exception;
  ENoRec  Exception;

  v_count   Integer := 0 ;


Begin
   Delete from INLEES_STOFTESTEN where code is null;
     Delete From Inlees_stoftesten where Upper(code) like 'CODE' and Upper(Protocol) like 'PROTOCOL';
     commit;
     Begin
   If P_inlezen = 'J' Then-- Controleer op dubbele records in  inlees bestand

   Open c_dubbel ;
   Loop
     Fetch c_dubbel into r_dubbel;
     If c_dubbel%Found Then
        v_melding := v_melding||r_dubbel.code||'||';
        raise EDubbel;
     Else
        Exit;

     End If;
   End Loop;
   Close c_dubbel;


   Open C_check;
   Loop
     Fetch c_check into R_check;
     If c_check%Found Then
        v_melding := v_melding||r_check.code||'||';
       raise ECheck;
     Else
        exit;

     End If;
   End Loop;



   -- Voeg de nieuwe stoftesten toe
   INSERT INTO kgc_stoftesten
   ( kafd_id,code, omschrijving, meet_reken, vervallen
  , omschrijving_lang, tech_id, mwst_id, stof_id_voor)
   SELECT 48, inl.code, inl.omschrijving, substr(inl.meet_reken,1,1), inl.vervallen
            , inl.omschrijving_lang
            , tech.tech_id
            , mwst.mwst_id
            ,'' stof_id_voor
   FROM inlees_stoftesten inl
      , kgc_technieken    tech
      , kgc_meetwaardestructuur mwst
   WHERE inl.tech_omschrijving = tech.code (+)
   AND inl.mwst_omschrijving = mwst.code (+)
   and  inl.code is not null;


   Commit;
   End If;


   IF  p_protocol = 'J' Then

   -- Loop Inleesbestand door en verwerk alle Records naar Protocol Tabel
   For r_ins in C_ins Loop
        Open C_Prot(r_ins.protocol);
          Fetch c_prot into R_prot ;
          If C_prot%NotFound Then
             v_melding := 'Protocol: '||R_ins.Protocol||' Niet aanwezig';
             Raise ENoRec;
          End If;
       Close C_prot;

        Open C_stof(Upper(r_ins.code));
         fetch c_stof into R_stof ;
         If c_stof%NotFound Then
             v_melding := 'Stoftest: '||R_ins.code||' Niet aanwezig';
         End If;
       Close C_stof;

        Insert into Kgc_Protocol_Stoftesten ( Prst_Id
                                            , stof_id
                                            , Standaard
                                            , Created_By
                                            , Creation_Date
                                            , Last_Updated_By
                                            , Last_Update_Date
                                            , Stgr_Id
                                            )

        Values ( kgc_prst_seq.nextval
               , r_stof.stof_id
               , r_ins.standaard
               , 'HELIX'
               , sysdate
               , 'HELIX'
               , sysdate
               , r_prot.stgr_id
               );




   End Loop;

  End If;

   Commit;


  Exception
    When EDubbel Then
      RollBack;
      vLog.Datum := Sysdate;
      vlog.melding := v_melding;
      vLog.proc := 'BEH_UTILS.INLEZEN_STOFTESTEN';
      vLog.Error     := 'J';
      vlog.stap      := 'Controle Dubbele Records';
      vLog.Type      := 'IMPORT';
      insert_log(vLog);

    When ECheck  Then
        RollBack;
      vLog.Datum := Sysdate;
      vlog.melding := v_melding;
      vLog.proc := 'BEH_UTILS.INLEZEN_STOFTESTEN';
      vLog.Error     := 'J';
      vlog.stap      := 'Controle unique key';
      vLog.Type      := 'IMPORT';
      insert_log(vLog);

    When ENoRec  Then
        RollBack;
      vLog.Datum := Sysdate;
      vlog.melding := v_melding;
      vLog.proc := 'BEH_UTILS.INLEZEN_STOFTESTEN';
      vLog.Error     := 'J';
      vLog.Type      := 'IMPORT';
      insert_log(vLog);

    When Others  Then
        RollBack;
      vLog.Datum := Sysdate;
      vlog.melding := v_melding;
      vLog.proc := 'BEH_UTILS.INLEZEN_STOFTESTEN';
      vLog.Error     := 'J';
      vLog.Type      := 'IMPORT';
      insert_log(vLog);

    End;

End;



Procedure Insert_Log (pLog beh_Log%RowType)
as
Begin
   insert into beh_log
   values plog ;

   commit ;
End;

Procedure Import_Uitslag(p_import clob, p_uits_id Number, p_onde_id number, p_best_id number)
  as
  E_Insert Exception;
  Begin
     insert into EPD_uitslagen_file(Import, onde_id, uits_id,Best_Id)
     values (p_import, P_onde_id, P_uits_id, P_BEST_ID);
     commit;

     Update_status_cyto_best(p_best_id,'C');
     commit;

     Exception When Others Then
      Update_status_cyto_best(p_best_id,'E');
      commit;
     raise E_insert;

  End ;

Procedure Update_status_cyto_best(p_best_id Number, p_status varchar2)
  as
  Begin
    Update BEH_BESTANDEN_PHELIX_CYTO
     set status = p_status
     where best_id = P_best_id;
     commit;
  End;


  Procedure CheckDeclaraties
As
  Cursor c_decl
    Is
      Select decl.decl_id,
               nvl(GetCategorieWaarde(decl.verrichtingcode),decl.verrichtingcode) verrichtingcode ,
                     decl.datum,
                     bepaal_pers_Id(decl.pers_id, decl.id, decl.entiteit) pers_id ,
                     decl.pers_id_alt_decl
        From   kgc_declaraties decl
        Where  datum > '01-01-2014'
        and    (status not in ('V','T','X','R')
    or     status is null) ;

    Cursor c_decl_H
    Is
      Select decl.decl_id,
               nvl(GetCategorieWaarde(decl.verrichtingcode),decl.verrichtingcode) verrichtingcode,
                     decl.datum,
                     decl.pers_id,
                     decl.pers_id_alt_decl
        From   kgc_declaraties decl
        Where  datum > '01-01-2014'
        and    (status not in ('V','T','X','R')
    or     status is null) ;



    Cursor c_decl2(c_verr varchar2, c_datum date, c_persID number)
    Is
      Select  decl.*

        From   kgc_declaraties decl
        Where   nvl(GetCategorieWaarde(decl.verrichtingcode),decl.verrichtingcode) = c_verr
        and    decl.datum           = c_datum
        and    decl.pers_id         = c_persID
    and    decl.pers_id_alt_decl is null;

      Cursor c_decl2_H(c_verr varchar2, c_datum date, c_persID_alt_decl number)
      Is
        Select decl.*
         From   kgc_declaraties decl
          Where  nvl(GetCategorieWaarde(decl.verrichtingcode),decl.verrichtingcode) = c_verr
          and    decl.datum           = c_datum
          and    decl.pers_id_alt_decl = c_persID_alt_decl;





  I Integer;
Begin

     For R_decl in c_decl Loop
          I := 0 ;
            For R_decl2 in C_decl2(r_decl.verrichtingcode, r_decl.datum, r_decl.pers_id ) Loop
                  I := I + 1 ;
                If I > 1 Then

            --             Insert into z_decl_bu
            --             values r_decl2 ;


                         Update kgc_declaraties
                         Set    datum  = ( R_decl2.Datum + (I-1))
                         where  decl_id = r_decl2.decl_id ;
                         commit;
                    End If;

        End Loop;

     End Loop;


     For R_decl_H in c_decl_H Loop
          I := 0 ;
            For R_decl2 in C_decl2_H(r_decl_H.verrichtingcode, r_decl_H.datum, r_decl_H.pers_id_alt_decl ) Loop
                  I := I + 1 ;
                If I > 1 Then

        --                 Insert into z_decl_bu
        --                 values r_decl2 ;

                         Update kgc_declaraties
                         Set    datum  = ( R_decl2.Datum + (I-1))
                         where  decl_id = r_decl2.decl_id ;
                         commit;
                    End If;

        End Loop;

     End Loop;



End;

Function GetCategorieWaarde(Telcode varchar2) return varchar2
  as
    Cursor c_cate(c_code varchar2)
    Is
     Select c.waarde
     From   kgc_categorie_waarden c
     where  c.cate_id in
          ( select cate_id
            from kgc_categorieen
            where code = c_code);

     r_cate c_cate%RowType;

  begin
     Open c_Cate(TelCode);
      Fetch c_cate into r_cate;
     Close c_Cate;

     Return r_cate.waarde;


  End ;


  Function bepaal_pers_Id(ppers_id number, pid number, pentiteit varchar2 )
    return number
  as
    Cursor c_Onbe(cid number)
    Is
      Select onbe.pers_id
      from   kgc_onderzoek_betrokkenen onbe
      where  onbe.onbe_id = cid ;

    r_Onbe c_onbe%RowType;

    v_pers_id Number;
  begin
    if pentiteit = 'ONBE' Then

        Open c_onbe(pid);
          Fetch c_onbe into R_onbe ;
        close c_onbe;

         if r_onbe.pers_id is null then
            v_pers_id := ppers_id;
         else
            v_pers_id := r_onbe.pers_id;
         end if;

     else
        v_pers_id := ppers_id;

     end if;
     return v_pers_id;
  End;
end Beh_Utils;
/

/
QUIT
