CREATE OR REPLACE PACKAGE "HELIX"."HELIX_INLEZEN_DEMO" is

-- uitlezen parameter inputlocatie voor scan
function get_scan_input_locatie return varchar2;
-- uitlezen parameter outputlocatie voor scan
function get_scan_output_locatie return varchar2;

-- inlezen lijst in external table naar echte tabel
procedure inlezen_scan_bestanden;

end helix_inlezen_demo;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."HELIX_INLEZEN_DEMO" as

  function get_scan_input_locatie return varchar2 as
  begin
    return '\\prd.corp\appdata\KGLE_Helixscans\INPUT';
  end get_scan_input_locatie;

  function get_scan_output_locatie return varchar2 as
  begin
    return '\\prd.corp\appdata\KGLE_Helixscans\OUTPUT';
  end get_scan_output_locatie;

  procedure inlezen_scan_bestanden as
  begin
    insert into ingelezen_scan_bestanden
      select bestandsnaam
      from scan_bestanden;
  end inlezen_scan_bestanden;

end helix_inlezen_demo;
/

/
QUIT
