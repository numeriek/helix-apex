CREATE OR REPLACE PACKAGE "HELIX"."EPD_LUS_INTERFACE_MAASTRICHT" Is

  /*******************************************************************************
       NAME:      KGC_ORU_MAASTRICHT
       PURPOSE:   Aanmaken XML bestanden voor SAP-LUS (Laboratorium-Uitslagen-Systeem van SAP)
                  vanuit Helix-databse
                  Onderdeel van de Opvragen Uitslagen Interface ("ORU Interface"),
                  fase 1 (Afdeling "CYTO")

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        2012-11-05  W.A.Brouwer/Cap  Initiele versie
       1.2        2013-02-14  R Konings        Aanpassingen ORU Fase 2
       1.3        2013-10-23  R Konings        Aanpassingen i.v.m Embargo
       2.0        2016        R  Konings       Aanpassingen op verzoek van FB
       2.1        2016-11-30  R Konings        Aanpassingen 1 helix project.
   *******************************************************************************/


  ----------------------------------------------------------------
  -- Get_Oru_Verslagen: geef een xml met lijst met onderzoeken bij
  --                    gegeven Patientnummer (KGC_Personen.Zisnr)
  ----------------------------------------------------------------
  Function get_oru_verslagen(p_zisnr In Varchar2) Return Clob;

  --------------------------------------------------------------------------------
  -- Get_Oru_Verslag_Detail: geef een xml met detail-gegevens bij een
  --                         gegeven OnderzoekNummer (KGC_Onderzoeken.Onderzoeknr)
  --------------------------------------------------------------------------------
  Function get_oru_verslag_detail(p_onderzoeknr In Varchar2) Return Clob;

  Function getinterne_relatie
  (
    p_pers_id Integer,
    p_onde_id Integer
  ) Return Varchar2;
  Function checkrelatie(p_pers_id Integer) Return Boolean;

  Function getorareportlink
  (
    p_ongr_id         Number,
    p_onde_id         kgc_onderzoeken.onde_id%Type,
    p_onderzoekswijze kgc_onderzoeken.onderzoekswijze%Type)
    return clob;

  Function getmonsterfractie
  (
    p_onde_id      Number,
    p_fractie      Boolean,
    p_datum_binnen Date
  ) Return Clob;

  Function getembargo
  (
    p_ongr_id Number,
    p_input   Varchar2
  ) Return Varchar2;
  Function getindicatie(p_onde_id Number) Return Clob;
  Function getrelatiegegevens(p_rela_id Number) Return Varchar2;
  Function getonderzoekwijze(p_onwy_cd Varchar2) Return Clob;
  Function getprotocol(p_onde_id Number) Return Clob;
  Function getuitslagteksten
  (
    p_onde_id   Number,
    p_ongr_id   Number,
    p_status    Varchar2,
    p_datum_aut Date,
    p_mede_id   Number,
    p_relatie   Varchar2
  ) Return Clob;
  Function getkopiehouders(puits_id Integer) Return Clob;
  Function getindicatiesdetail(p_onde_id Number) Return Clob;
  Function replaceuitslag(tekst In Clob) Return Clob;
  Procedure setuitslagtekstepd
  (
    p_onde_id           Number,
    p_ongr_id           Number,
    p_uits_id           Number,
    p_brty_id           Number,
    p_tekst             Varchar2,
    p_toelichting       Varchar2,
    p_kafd_id           Number,
    p_commentaar        Varchar2,
    p_datum_autorisatie Date,
    p_relatie           Varchar2,
    p_uitslag           Out Clob,
    p_geneticus         Out Clob,
    p_embargo_termijn   Out Number
  );


  Procedure maak_epd_uitslag_aan;

  Function getonderzoeksgroep(p_ongr_id kgc_onderzoeksgroepen.ongr_id%Type)
    Return Varchar2;
  Function getcategoriewaarde(p_ongr_cd kgc_onderzoeksgroepen.code%Type, p_afd_code varchar2) return varchar2;

/*
Function LUS_GetOrders(p_PatientID Varchar2, p_amount Integer, p_offSet Integer) Return Clob; -- Ophalen Orders
Function LUS_OnderzoekAfgerond(p_mons_id number) return varchar2; -- Check of een Onderzoek afgerond is
Function LUS_GetObservations(p_mons_id Number, p_stgr_id Number, p_dat_afgerond out varchar2)  return clob; -- Ophalen bepalingen behorende bij de Orders
Function LUS_GetRange(p_Bovengrens varchar2, p_ondergrens varchar2) return varchar2; -- Ophalen & Formateren Range Gegegevens naar LUS Formaat
Function LUS_GetOrdersFiltererd(p_patientId varchar2, p_Start_Date Date, p_End_Date Date, p_stoftest varchar2)return Clob; -- Get Orders Filtered voor de grafiek functie van LUS
Function LUS_GetObservationsFiltered(p_mons_id Number, p_stgr_id Number, p_stof_id number, p_dat_afgerond out varchar2)  -- Voor grafiek functie van LUS.
    return clob;




*/


End epd_lus_interface_maastricht;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."EPD_LUS_INTERFACE_MAASTRICHT" Is

  patientnummer_afwezig Exception;
  v_start       Constant Boolean := True;
  v_stop        Constant Boolean := False;
  v_date_format Constant Varchar2(10) := 'YYYY-MM-DD';

  --------------------------------------------------------------------------------
  -- FUNCTION Get_Patient_data: Maak XML-block met Patient data.
  -- Deze fuctie wordt aangeroepen vanuit
  --   a. FUNCTION Get_Oru_Verslagen      met parameter P_ZisNr
  --   b. FUNCTION Get_Oru_Verslag_Detail met parameter P_Pers_Id
  --------------------------------------------------------------------------------
  Function getsimplexmltag
  (
    p_veldid    Varchar2,
    p_veldvalue Clob
  ) Return Clob As
    v_xml_tag Clob;
  Begin
    If p_veldvalue Is Null
    Then
      Return '<' || p_veldid || '/>';
    Else
      Return '<' || p_veldid || '>' || p_veldvalue || '</' || p_veldid || '>' || chr(10);
    End If;

  End;

  Function epd_getcomplextype
  (
    p_complexid Varchar2,
    p_start     Boolean
  )
  -- Aanmaken XML_Complex types
   Return Clob As
  Begin
    If p_start
    Then
      Return '<' || p_complexid || '>' || chr(10);
    Else
      Return '</' || p_complexid || '>' || chr(10);
    End If;
  End;

  Function get_patient_data
  (
    p_zisnr   In Varchar2 Default Null,
    p_pers_id In Number Default Null
  ) Return Varchar2 Is
    -- Ophalen patient gegevens tbv PID segment bericht
    Cursor c_pers Is
      Select xmlelement(pid,
                  xmlelement("Patient_Identifier_List",
                          xmlelement("ID", zisnr),
                          xmlelement("Assigning_Authority",
                                 xmlelement("Namespace_ID",
                                        'SAP')),
                          xmlelement("Assigning_Facility",
                                 xmlelement("Namespace_ID",
                                        'SAP'))),
                  xmlelement("Patient_Name",
                          xmlelement("Family_Name",
                                 xmlelement("Surname",
                                        Trim(voorvoegsel || ' ' ||
                                            achternaam || ' ' ||
                                            decode(voorvoegsel_partner,
                                                Null,
                                                Null,
                                                voorvoegsel_partner || ' ') ||
                                            achternaam_partner)),
                                 xmlelement("Given_Name",
                                        voorletters))),
                  xmlelement("Date_Time_Of_Birth",
                          to_char(geboortedatum, 'yyyymmdd')),
                  xmlelement("Administrative_Sex", geslacht)) xml_field
      From   kgc_personen
      Where  (zisnr = p_zisnr And p_zisnr Is Not Null)
      Or     (pers_id = p_pers_id And p_pers_id Is Not Null);

    r_pers c_pers%Rowtype;

  Begin
    Open c_pers;
    Fetch c_pers
      Into r_pers;
    If c_pers%Notfound
    Then
      Close c_pers;
      Raise patientnummer_afwezig;
    End If;

    Close c_pers;
    Return(r_pers.xml_field.getclobval); --NOTE: r_pers.XML_FIELD has type "xmltype"

  End get_patient_data;

  --------------------------------------------------------------------------------
  Function get_oru_verslagen(p_zisnr In Varchar2) Return Clob
  /* Ophalen van lijst van alle onderzoeken die voor een specifiek patientNr hebben plaatsgevonden */

   Is
    v_xml_doc                Clob;
    v_xml                    xmltype;
    v_onderzoeksdatumuitslag Varchar2(8);
    v_onderzoekuitslagtype   Varchar2(500);
    teller                   Number := 0;

    onderzoeken_afwezig  Exception;
    onderzoeken_afgerond Exception;
    skip_dit_onderzoek   Exception;

    Cursor c_uits(p_onde_id Number) Is
      Select to_char(Min(datum_mede_id4), 'yyyymmdd') datum_ttijd_aut
      From   kgc_uitslagen
      Where  onde_id = p_onde_id
      And    mede_id4 Is Not Null;

    r_uits c_uits%Rowtype;


    -- RKON: 15022013 Ophalen Familienaam en Familienummer behorende bij een onderzoek
    Cursor c_fami(c_pers_id kgc_personen.pers_id%Type) Is
      Select /*+ RULE */
       fami.naam,
       fami.familienummer
      From   kgc_families       fami
      ,      kgc_familie_leden  fale
      where  fale.pers_id = c_pers_id
      and    fami.fami_id = fale.fami_id;

    r_fami c_fami%Rowtype;

    -- Systeem parameter

    v_intern         Varchar2(1);
    vint_rela        Boolean := False;
    v_onderzoek_aanw Boolean;
    v_familienummers Varchar2(100);
    i                Integer;

  Begin
    v_xml_doc := '<DbResult>' || chr(10) ||
             get_patient_data(p_zisnr => p_zisnr) || chr(10) ||
             '<Onderzoeken>';

    -- Ophalen onderzoeken
    For r_onde In (Select /*+ RULE*/
               onde.onde_id onde_id,
               onde.afgerond afgerond,
               rela.relatie_type relatie_type,
               ongr.omschrijving onderzoekgroep,
               kafd.code onderzoekafdelingcode,
               getindicatie(onde.onde_id) onderzoekindicatie,
               onde.onderzoeknr onderzoeknummer,
               to_char(onde.datum_binnen, 'yyyymmdd') onderzoekaanvraagdatum,
               rela.aanspreken onderzoekaanvragendarts,
               to_char(onde.datum_autorisatie, 'yyyymmdd') onderzoeksdatumuitslag
               --    ,      onui.omschrijving                           onderzoekuitslagcode
              ,
               onde.status,
               onde.onderzoekstype,
               pers.pers_id,
               onde.ongr_id
              From   kgc_personen               pers,
                   kgc_onderzoeken            onde,
                   kgc_onderzoeksgroepen      ongr,
                   kgc_kgc_afdelingen         kafd,
                   kgc_relaties               rela,
                   kgc_onderzoek_uitslagcodes onui
              Where  pers.zisnr = p_zisnr
              And    onde.pers_id = pers.pers_id
              And    onde.ongr_id = ongr.ongr_id
              And    onde.kafd_id = kafd.kafd_id
              And    onde.rela_id = rela.rela_id
              And    onde.onui_id = onui.onui_id(+)
              Order  By onde.last_update_date Desc)
    Loop
      Begin




        v_onderzoekuitslagtype := Null;
        v_onderzoek_aanw       := True;
        --------------------------------------------------------------------------
        -- Indien onderzoek NIET is afgerond moet het een geautoriseerd tussentijds
        -- verslag hebben, anders wordt het onderzoek niet getoond.
        -- Tevens word de uitslagdatum bepaald die getoond wordt in het EPD overzicht scherm
        --------------------------------------------------------------------------
        If r_onde.onderzoekgroep = 'DNA'
        Then
          If r_onde.status <> 'G'
          Then
            Raise skip_dit_onderzoek;
          End If;
          If r_onde.onderzoekstype <> 'D'
          Then
            Raise skip_dit_onderzoek;
          End If;
        End If;

        If r_onde.afgerond = 'N'
        Then
          Open c_uits(r_onde.onde_id);
          Fetch c_uits
            Into r_uits;
          If c_uits%Notfound
          Then
            Close c_uits;
            Raise skip_dit_onderzoek;
          Else
            v_onderzoeksdatumuitslag := r_uits.datum_ttijd_aut;
          End If;
          Close c_uits;
        Else
          v_onderzoeksdatumuitslag := r_onde.onderzoeksdatumuitslag;
        End If;
        --------------------------------------------------------------------------
        -- Onderzoek moet een interne relatie hebben ((Relatie_Type = 'IN')
        --  via KGC_Onderzoeken.rela_id , of
        --  via KGC_Onderzoek_Kopiehouders.rela_id, of
        --  via KGC_Kopiehouders
        --------------------------------------------------------------------------
        v_intern := getinterne_relatie(r_onde.pers_id, r_onde.onde_id);
        If v_intern = '1'
        Then
          Raise skip_dit_onderzoek;
        End If;



        --------------------------------------------------------------------------
        -- Samenvoegen Brieftype Omschrijvingen, behorend bij Onderzoek
        -- Meest recente eerst
        --------------------------------------------------------------------------
        For r_brty In (Select /*+ RULE*/
                   rownum,
                   brty.omschrijving
                  From   kgc_brieftypes brty,
                       kgc_uitslagen  uits
                  Where  uits.onde_id = r_onde.onde_id
                  And    uits.brty_id = brty.brty_id
                  Order  By uits.uits_id Desc)
        Loop
          v_onderzoekuitslagtype := Case r_brty.rownum
                              When 1 Then
                              r_brty.omschrijving
                              Else
                              substr(v_onderzoekuitslagtype || ', ' ||
                                   r_brty.omschrijving,
                                   1,
                                   500)
                            End;
        End Loop;
        -- Aangepast i.v.m. 1 Helix project: RKON 30-11-2016.
        -- Alle Familienummers moeten altijd getoond worden indien aanwezig.
        -- Kunnen null waarde bevatten

        i := 0;
        v_familienummers := null;
        For r_fami In c_fami(r_onde.pers_id)
        Loop
          If i > 0
          Then
            v_familienummers := v_familienummers || ',';
          End If;
          v_familienummers := v_familienummers || r_fami.familienummer;
          i := i + 1;
        End Loop;






        ------------------------------------------------------------------------------------
        -- Het is niet mogelijk om onderstaande string direct te assignen aan een variabele
        -- van type xmltype, dus vandaar de "select into from Dual" ....
        ------------------------------------------------------------------------------------
        Select xmlelement("Onderzoek",
                    xmlelement("OnderzoekGroep",
                            r_onde.onderzoekgroep),
                    xmlelement("OnderzoekAfdelingCode",
                            r_onde.onderzoekafdelingcode),
                    xmlelement("OnderzoekFamilieNummer",
                            v_familienummers) -- In latere fase
                    ,
                    xmlelement("OnderzoekFamilieNaam", r_fami.naam) -- in te vullen
                    ,
                    xmlelement("OnderzoekIndicatie",
                            r_onde.onderzoekindicatie),
                    xmlelement("OnderzoekNummer",
                            r_onde.onderzoeknummer),
                    xmlelement("OnderzoekAanvraagDatum",
                            r_onde.onderzoekaanvraagdatum),
                    xmlelement("OnderzoekAanvragendArts",
                            r_onde.onderzoekaanvragendarts),
                    xmlelement("OnderzoekUitslagType",
                            v_onderzoekuitslagtype),
                    xmlelement("OnderzoekDatumUitslag",
                            v_onderzoeksdatumuitslag),
                    xmlelement("OnderzoekUitslagcode", '')
                    -- uitslagcode leeggemaakt iom Functioneel beheer.
                    )

        Into   v_xml
        From   dual;
        v_xml_doc := v_xml_doc || chr(10) || v_xml.getclobval;
        teller    := teller + 1;

      Exception
        When skip_dit_onderzoek Then
          Null;
        When Others Then
          Raise;
      End;
    End Loop;

    If teller = 0
    Then
      If v_onderzoek_aanw
      Then
        Raise onderzoeken_afgerond;
      Else
        Raise onderzoeken_afwezig;
      End If;
    End If;





    v_xml_doc := v_xml_doc || chr(10) || '</Onderzoeken>' || chr(10) ||
             '</DbResult>';

    Return v_xml_doc;

  Exception
    When patientnummer_afwezig Then
      raise_application_error(-20020,
                      'Patientnummer bestaat niet in Helix');
    When onderzoeken_afgerond Then
      raise_application_error(-20050,
                      'U bezit niet voldoende rechten om deze informatie in te zien ');
    When onderzoeken_afwezig Then
      -- Aanpassing mantis 10423
      raise_application_error(-20040,
                      'Er zijn geen onderzoeken uitgevoerd voor deze patient.');
    When Others Then
      Raise;

  End get_oru_verslagen;

  --------------------------------------------------------------------------------
  Function get_oru_verslag_detail(p_onderzoeknr In Varchar2)
  /*****  OUTPUT: Detail gegevens Onderzoek (Protocol, Monsters/Materiaalen/Fracties,
                                                 Uitslag & Conclusie teksten,Indicaties,
                                                 Documenten, Aanvrager en de OnderzoeksWijze)
                      De detail gegevens worden getoond in het detailscherm in SAP.

              LOGICA: Ophalen gegevens, Bepalen Embargo Datum op basis van verkregen Embargo Termijn.

              Versie Historie: November 2012,   Willem Brouwer Cap Gemini.
                                                Eerste oplevering
                               September 2013,  Roy Konings  MUMC
                                                Aanpassingen Fase 2 functionaliteit.
                               Oktober 2013,    Roy Konings  MUMC
                                                Functie wordt helemaal opnieuw opgebouwd op basis van de
                                                aangescherpte wensen laboratoria units Klinische Genetica.
                                                Tevens worden diverse componenten van de code in separate
                                                procedures & Functies opgebouwd.
                               November 2013,   Roy Konings
                                                Oplevering Fase 3 functionaliteit
                               December 2014,   Kopiehouders uitslag toegevoegd.
                                                EPD DNA functionaliteit uitgebreid.
                               November 2016    Aanpassingen 1 helix project

      *****/
   Return Clob As
    -- Selectie onderzoek op basis van OnderzoekNr.
    Cursor c_onde(c_onderzoeknr Varchar2) Is
      Select onde.onde_id,
           onde.pers_id,
           onde.rela_id,
           onde.mede_id_autorisator,
           onde.ongr_id,
           onde.datum_autorisatie,
           onde.datum_binnen,
           onde.kafd_id,
           onde.afgerond,
           onde.onderzoekswijze
      From   kgc_onderzoeken onde
      Where  onde.onderzoeknr = c_onderzoeknr;


    Cursor c_ongr(c_code kgc_onderzoeksgroepen.code%Type) Is
      Select ongr_id From kgc_onderzoeksgroepen Where code = c_code;

    -- Record C_onde
    r_onde c_onde%Rowtype;
    r_ongr c_ongr%Rowtype;
    -- Lokale variabelen
    v_xml_doc         Clob;
    v_fractie_ophalen Boolean;
    v_relatie         Varchar2(4000);
    v_conclusies      Varchar2(8000) := '';
    v_xml             xmltype;

    -- Exceptions
    e_onderzoeksnr_bestaat_niet Exception;




  Begin


    -- Ophalen basis gegevens onderzoek, entry point ophalen verdere detail gegevens.
    Open c_onde(p_onderzoeknr);
    Fetch c_onde
      Into r_onde;
    If c_onde%Notfound
    Then
      Raise e_onderzoeksnr_bestaat_niet; -- Door een tussentijdse wijziging is het Onderzoek verwijderd.
    End If;
    Close c_onde;

    /**************************************************************************************************************
            Bepaal de instellingen voor het ophalen van uitslagen.
            De instellingen bepalen welke gegevens opgehaald dienen te worden(Monster & Fractie of alleen Monster).
            Tevens bepalen de instellingen de embargo_termijn.
      **************************************************************************************************************/

    Open c_ongr('DNA');
    Fetch c_ongr
      Into r_ongr;
    Close c_ongr;

    v_fractie_ophalen := r_onde.ongr_id = r_ongr.ongr_id;
    -- Bepalen of OOk de fracties opgehaald moeten worden, geld NU alleen voor de unit DNA

    /***************************************************************************************************************
            Aanmaken XML DOCUMENT, Vullen met gegevens uit Helix
      ****************************************************************************************************************/

    -- Maak XML Document aan.
    v_xml_doc := '<DbResult>' || chr(10) ||
             get_patient_data('', r_onde.pers_id) || '<Report>' ||
             chr(10);

    -- Toevoegen OnderzoeksNr
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('OnderzoekNummer', p_onderzoeknr);
    -- Toevoegen Datum Onderzoek
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('OnderzoekAanvraagDatum',
                        to_char(r_onde.datum_binnen, 'YYYYMMDD')); -- Datum onderzoek start
    -- Ophalen Relatie gegevens & Toevoegen aanvrager aan XML DOCUMENT
    v_relatie := getrelatiegegevens(r_onde.rela_id);
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('OnderzoekAanvragendArts', v_relatie); -- aanvragend arts
    -- Openbare uitslag standaard JA
    v_xml_doc := v_xml_doc || getsimplexmltag('Openbaar', 'JA');
    -- Toevoegen van Drie Flex velden
    v_xml_doc := v_xml_doc || getsimplexmltag('FlexCharVeld1', '');
    v_xml_doc := v_xml_doc || getsimplexmltag('FlexCharVeld2', '');
    v_xml_doc := v_xml_doc || getsimplexmltag('FlexDatumVeld1', '');
    -- Toevoegen OnderzoeksWijze
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('OnderzoeksWijze',
                        getonderzoekwijze(r_onde.onderzoekswijze));
    -- Toevoegen Monsters & Fracties.
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('Monsters',
                        getmonsterfractie(r_onde.onde_id,
                                    v_fractie_ophalen,
                                    r_onde.datum_binnen));
    -- Toevoegen protocol
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('Protocollen',
                        getprotocol(r_onde.onde_id));

    -- Toevoegen uitslag teksten
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('Uitslagen',
                        getuitslagteksten(r_onde.onde_id,
                                    r_onde.ongr_id,
                                    r_onde.afgerond,
                                    r_onde.datum_autorisatie,
                                    r_onde.mede_id_autorisator,
                                    v_relatie));
    --Toevoegen documenten
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('Documenten',
                        getorareportlink(r_onde.ongr_id,
                                   r_onde.onde_id,
                                                    r_onde.onderzoekswijze));


    -- Toevoegen indicaties
    v_xml_doc := v_xml_doc ||
             getsimplexmltag('Indicaties',
                        getindicatiesdetail(r_onde.onde_id));

    -- Sluit XML Document af.
    v_xml_doc := v_xml_doc || chr(10) || '</Report>' || chr(10) ||
             '</DbResult>';

    Return v_xml_doc;

  Exception
    When e_onderzoeksnr_bestaat_niet Then
      raise_application_error(-20010,
                      'Onderzoeksnummer bestaat niet in Helix');
      --  When others then raise;

  End get_oru_verslag_detail;




  Function getinterne_relatie
  (
    p_pers_id Integer,
    p_onde_id Integer
  ) Return Varchar2
  /****** Bepalen of voor de in Helix ingeschreven persoon/patient een Relatie met een
              interne aanvrager en/of Kopiehouder geld.
      ******/
   Is

    Cursor c_onbe(c_pers_id Number) Is
      Select /*+ RULE */
       onde.pers_id,
       onbe.onde_id,
       onde.kafd_id
      From   kgc_onderzoeken           onde,
           kgc_onderzoek_betrokkenen onbe
      Where  onbe.pers_id = c_pers_id
      And    onde.onde_id = onbe.onde_id;

    v_internb Boolean := True;
    v_intern  Varchar2(1);

  Begin
    -- Check relaties onderzoeksbetrokkenen

    v_internb := checkrelatie(p_pers_id);
    If Not v_internb
    Then
      For r_onbe In c_onbe(p_pers_id)
      Loop
        If Not v_internb
        Then
          v_internb := r_onbe.kafd_id = 40;
        End If;
      End Loop;
    End If;

    If v_internb
    Then
      v_intern := '0';
    Else
      v_intern := '1';

    End If;
    Return(v_intern);

  End getinterne_relatie;

  Function checkrelatie(p_pers_id Integer) Return Boolean As
    Cursor getrel(cpers_id Number) -- ophalen relatie gegevens, aanvrager onderzoek
    Is
      Select /*+ RULE */
       ko.rela_id
      From   kgc_onderzoeken ko
      Where  ko.pers_id = cpers_id
      And    ko.rela_id In
           (Select rela_id From kgc_relaties Where relatie_type = 'IN');



    Cursor getkoponde(cpers_id Number) -- ophalen relatie gegevens, onderzoek kopiehouder
    Is
      Select /*+ RULE */
       kok.rela_id
      From   kgc_onderzoeken            ko,
           kgc_onderzoek_kopiehouders kok
      Where  ko.pers_id = cpers_id
      And    kok.onde_id = ko.onde_id
      And    kok.rela_id In
           (Select rela_id From kgc_relaties Where relatie_type = 'IN');

    Cursor getkopuits(cpers_id Number) -- ophalen relatie gegevens, uitslag kopiehouder.
    Is
      Select /*+ FIRST_ROWS */
       koho.rela_id
      From   kgc_onderzoeken  onde,
           kgc_uitslagen    uits,
           kgc_kopiehouders koho
      Where  uits.onde_id In
           (Select onde_id
            From   kgc_onderzoeken onde
            Where  onde.pers_id = cpers_id)
      And    koho.uits_id = uits.uits_id
      And    koho.rela_id In
           (Select rela_id From kgc_relaties Where relatie_type = 'IN');

    aanvrelatie Integer := Null;
    kopieh      Integer := Null;
    v_intern    Boolean := True;

    v_pers   Integer;
    v_teller Integer := 1;
    i        Integer;

  Begin
    aanvrelatie := Null;

    Open getrel(p_pers_id);
    Fetch getrel
      Into aanvrelatie;
    v_intern := getrel%Found;
    Close getrel;

    -- Indien geen Interne aanvrager,
    -- check voor een kopiehouder bij een onderzoek van de patient.
    If Not v_intern
    Then
      Open getkoponde(p_pers_id);
      Fetch getkoponde
        Into kopieh;
      v_intern := getkoponde%Found;
      Close getkoponde;

      -- Indien geen Kopiehouder bij het onderzoek,
      -- check voor een kopiehouder bij een uitslag van de patient
      If Not v_intern
      Then
        kopieh := Null;
        Open getkopuits(p_pers_id);
        Fetch getkopuits
          Into kopieh;
        v_intern := getkopuits%Found;
        Close getkopuits;

        -- Indien geen Kopiehouder en/of Interne aanvrager bekend is,
        -- dan is er ook geen Interne relatie
      End If;
    End If;

    Return v_intern;

  End;


  Function getorareportlink
  (
    p_ongr_id         Number,
    p_onde_id         kgc_onderzoeken.onde_id%Type,
    p_onderzoekswijze kgc_onderzoeken.onderzoekswijze%Type
  )
  /*****   Ophalen documenten die bij een onderzoek behoren.
               Deze documenten kunnen zijn opgeslagen hard copies op een netwerk locatie.
               HTTP Links naar een Oracle Report, die gegenereerd worden met behulp van de
               Oracle Report Server. De output betreft altijd een PDF document.

               LET OP!!! Op een document is de EMBARGO Functienaliteit niet van toepassing.
      *****/
   Return Clob As
    -- Cursor Declaraties

    -- Ophalen Systeem parameters
    Cursor c_sypa(p_code Varchar2) Is
      Select sypa.standaard_waarde,
           sypa.sypa_id
      From   kgc_systeem_parameters sypa
      Where  code = p_code;

    -- Ophalen waarden systeem parameters per
    Cursor c_sywa
    (
      p_ongr_id Number,
      p_sypa_id Number
    ) Is
      Select sywa.waarde
      From   kgc_systeem_par_waarden sywa
      Where  sywa.sypa_id = p_sypa_id
      And    sywa.ongr_id = p_ongr_id;

    -- Ophalen XML_TAGS Link Oracle Report Server.
    Cursor c_xdoc
    (
      p_typ  Varchar2,
      p_link Varchar2
    ) Is
      Select xmlelement("Document",
                  xmlelement("DocumentID", p_typ),
                  xmlelement("DocumentPath", p_link)) docu
      From   dual;



    -- Cursor Record Declaraties.
    r_sypa c_sypa%Rowtype;
    r_sywa c_sywa%Rowtype;
    r_xdoc c_xdoc%Rowtype;

    -- Variabelen declaraties.
    vserver     Varchar2(200);
    vpoort      Varchar2(200);
    v_repserver Varchar(200);
    vlink       Varchar2(4000);
    vnfound     Boolean;
    v_tag       Varchar2(4000);
    v_doc       Clob;

    e_skip_build_report Exception;

  Begin

    -- Haal de Report server op
    Open c_sypa('APPLICATIE_SERVER');
    Fetch c_sypa
      Into r_sypa;
    Close c_sypa;
    If r_sypa.standaard_waarde = 'http://AWIOA402.A.CORP'
    Then
      vserver := 'http://AWIOA401.A.CORP';
    Else
      vserver := r_sypa.standaard_waarde;
    End If;

    -- Haal de poort waar de report server zich bevind op.
    Open c_sypa('APPLICATIE_SERVER_POORT');
    Fetch c_sypa
      Into r_sypa;
    Close c_sypa;
    vpoort := r_sypa.standaard_waarde;
    --   v_doc := '<Documenten>'||Chr(10);


    Begin

      --- Opbouwen link naar Oracle Report server, ophalen grafiek -----------
      if p_onderzoekswijze = 'TU' then

         Open c_sypa('ONDE_GRAFIEK');
      Fetch c_sypa
        Into r_sypa;
      Close c_sypa;

      Open c_sywa(p_ongr_id, r_sypa.sypa_id);
      Fetch c_sywa
        Into r_sywa;
      If c_sywa%Found
      Then
        vlink := vserver || ':' || vpoort || '/reports/rwservlet?' ||
              r_sywa.waarde || '+' || p_onde_id;
        Open c_xdoc('GRAFIEK', vlink);
        Fetch c_xdoc
          Into r_xdoc;
        Close c_xdoc;
        v_doc := v_doc || chr(10) || r_xdoc.docu.getclobval;
      End If;
      Close c_sywa;

      ---Opbouwen Link naar Oracle Report server, ophalen Tabel --------------------
      Open c_sypa('ONDE_TABEL');
      Fetch c_sypa
        Into r_sypa;
      Close c_sypa;

      Open c_sywa(p_ongr_id, r_sypa.sypa_id);
      Fetch c_sywa
        Into r_sywa;
      If c_sywa%Found
      Then
        vlink := vserver || ':' || vpoort || '/reports/rwservlet?' ||
              r_sywa.waarde || '+' || p_onde_id;
        Open c_xdoc('TABEL', vlink);
        Fetch c_xdoc
          Into r_xdoc;
        Close c_xdoc;
        v_doc := v_doc || chr(10) || r_xdoc.docu.getclobval;
      End If;
      Close c_sywa;
    End if;
      -- Ophalen Brieven, PDF documenten vanaf een netwerk locatie momenteel alleen voor de afd EMZ

      For r_best In (Select xmlelement("Document",
                            xmlelement("DocumentID", 'PDF'),
                            xmlelement("DocumentPath",
                                    best.bestand_specificatie),
                            xmlelement("DocumentName",
                                    best.bestandsnaam)) resultaat
                From   kgc_uitslagen uits,
                     kgc_brieven   brie,
                     kgc_bestanden best
                Where  uits.onde_id = p_onde_id
                And    uits.onde_id = brie.onde_id
                And    uits.brty_id = brie.brty_id -- Brieven moeten zelfde Brieftype als Uitslagen hebben
                And    brie.best_id = best.best_id
                And    p_ongr_id In
                     (Select ongr_id
                      From   kgc_onderzoeksgroepen
                      Where  code  not in (Select cawa.waarde
         From   kgc_categorie_types   caty,
           kgc_categorieen       cate,
           kgc_categorie_waarden cawa
      Where  caty.caty_id = cate.caty_id
      And    cawa.cate_id = cate.cate_id
      And    caty.code = 'EPD_XPDF'))

                )
      Loop
        v_doc := v_doc || chr(10) || r_best.resultaat.getclobval;
      End Loop;


      --  v_doc := v_doc||Chr(10) ||'</Documenten>';

      Return(v_doc);

    Exception
      When e_skip_build_report Then
        Null;
    End;






  End;


  Function getmonsterfractie
  (
    p_onde_id      Number,
    p_fractie      Boolean,
    p_datum_binnen Date
  )
  /*****  Functionaliteit voor het ophalen van Monstergegevens
              (MonsterNr, Datum van Ontvangst, Materiaal waaruit Monster bestaat(bijv. Bloed, Beenmerg, Urine)
              Indien de parameter p_Fractie de waarde TRUE bevat dan dienen ook de Fracties opgehaald te worden
              die bij een specifiek MonsterNummer Behoren. De Relatie met het Protocol(BAS_METING.Frac_ID) wordt niet
              afgedwongen.
      *****/
   Return Clob As
    -- Ophalen fracties behorende bij het onderzoek, toegang via Monsters
    Cursor c_frac(c_mons_id Number) Is
      Select /*+ RULE */
       frac.fractienummer,
       frac.creation_date,
       frty.omschrijving
      From   bas_fracties      frac,
           bas_fractie_types frty
      Where  frac.mons_id = c_mons_id
      And    frac.frty_id = frty.frty_id;

    r_frac c_frac%Rowtype;
    -- Ophalen materiaal soort behorende bij monster
    Cursor c_mate(c_mate_id Number) Is
      Select mate.omschrijving
      From   kgc_materialen mate
      Where  mate.mate_id = c_mate_id;

    r_mate c_mate%Rowtype;
    v_doc  Clob;

  Begin
    v_doc := '';
    -- Haal alle monsters op die bij het opgegeven onderzoek ID horen
    For r_mons In (Select mons.monsternummer,
                   mons.datum_aanmelding,
                   mons.mate_id,
                   mons.mons_id
              From   kgc_monsters           mons,
                   kgc_onderzoek_monsters onmo
              Where  onmo.onde_id = p_onde_id
              And    onmo.mons_id = mons.mons_id)
    Loop
      Open c_mate(r_mons.mate_id);
      Fetch c_mate
        Into r_mate;
      Close c_mate;
      -- XML_TAGS behorende tot de complex group Monster, op basis van MonsterNr
      If p_datum_binnen < to_date('01-01-2017','DD-MM-YYYY')
      Then
        v_doc := v_doc || '<Monster>' || chr(10) ||
              getsimplexmltag('MonsterNummer', r_mons.monsternummer) ||
              getsimplexmltag('MonsterMateriaal',
                         r_mate.omschrijving) ||
              getsimplexmltag('MonsterDatumAanmelding',
                         to_char(r_mons.datum_aanmelding,
                              'YYYYMMDD')) || '</Monster>' ||
              chr(10);
      End If;

      If p_fractie Or p_datum_binnen >= to_date('01-01-2017','DD-MM-YYYY')
      Then
        -- Als de modus Fractie True is dan dienen tevens de fracties opgehaald worden die bij een Monster behoren.
        For r_frac In c_frac(r_mons.mons_id)
        Loop
          v_doc := v_doc || '<Monster>' || chr(10) ||
                getsimplexmltag('MonsterNummer',
                           r_frac.fractienummer) ||
                getsimplexmltag('MonsterMateriaal',
                           r_frac.omschrijving) ||
                getsimplexmltag('MonsterDatumAanmelding',
                           to_char(r_frac.creation_date,
                                'YYYYMMDD')) ||
                '</Monster>' || chr(10);
        End Loop;
      End If;
    End Loop;

    Return v_doc; -- Geef de Xml_Tags terug aan Aanroepende Functie
  End;


  Function getembargo
  (
    p_ongr_id Number,
    p_input   Varchar2
  )
  /** Functie bepaald het EMABRGO TERMIJN in dagen die bij een datum
          opgeteld dienen te worden. Het Embargo termijn wordt bepaald aan
          de hand van de methode, invoer parameter .
      **/

   Return Varchar2 As
    Cursor c_sywa
    (
      c_code    Varchar2,
      c_ongr_id Number
    ) Is
      Select sywa.waarde
      From   kgc_systeem_parameters  sypa,
           kgc_systeem_par_waarden sywa
      Where  sypa.code = c_code
      And    sywa.sypa_id = sypa.sypa_id
      And    sywa.ongr_id = c_ongr_id;

    Cursor c_emset
    (
      c_embmethode Varchar2,
      c_ongr_id    Number
    ) Is
      Select erem.code,
           erem.waarde,
           eemt.code methode
      From   epd_embargo_setting erem,
           epd_embargo_methode eemt
      Where  erem.eemt_id = eemt.eemt_id
      And    eemt.code = c_embmethode
      And    erem.ongr_id = c_ongr_id;

    r_emset           c_emset%Rowtype;
    r_sywa            c_sywa%Rowtype;
    v_embargo         Boolean;
    v_embargo_termijn Integer;

  Begin
    Open c_sywa('EMBARGO_METHODE', p_ongr_id);
    Fetch c_sywa
      Into r_sywa;
    v_embargo := c_sywa%Found;
    Close c_sywa;
    If v_embargo
    Then
      Open c_emset(r_sywa.waarde, p_ongr_id);
      Fetch c_emset
        Into r_emset;
      Close c_emset;
      v_embargo_termijn := r_emset.waarde;
    Else
      v_embargo_termijn := -1;
    End If;
    Return v_embargo_termijn;

  End;

  Function getindicatie(p_onde_id Number) Return Clob
  -- Bepalen van de indicaties die worden weergeven in het overzicht scherm.
   As
    Cursor c_indi
    (
      c_onde_id Number,
      c_hoofd   Varchar2
    ) Is
      Select /*+ RULE */
       indi.omschrijving,
       indi.code,
       onin.opmerking
      From   kgc_indicatie_teksten    indi,
           kgc_onderzoek_indicaties onin
      Where  onin.onde_id = c_onde_id
      And    onin.indi_id = indi.indi_id
      And    onin.hoofd_ind = c_hoofd;

    r_indi      c_indi%Rowtype;
    aanw_h_indi Boolean := True;

    v_indicatie Varchar2(2000);
  Begin
    -- Ophalen hoofd Indicatie
    Open c_indi(p_onde_id, 'J');
    Fetch c_indi
      Into r_indi;
    aanw_h_indi := c_indi%Found; -- Bepaal of er een hoofd indicatie is.
    Close c_indi;

    If aanw_h_indi
    Then
      -- Indien hoofd indicatie aanwezig check of het in geval van EMZ opgegeven
      -- is in het opmerkingen veld van kgc_onderzoeks_indicaties.
      If r_indi.code = '999'
      Then
        v_indicatie := r_indi.opmerking;
      Else
        v_indicatie := r_indi.omschrijving;
      End If;
    Else
      -- Indien geen hoofd indicatie haal een willekeurige indicatie op en toon deze
      Open c_indi(p_onde_id, 'N');
      Fetch c_indi
        Into r_indi;
      If c_indi%Notfound
      Then
        -- indien geen indicatie gevonden , geef dit weer.
        v_indicatie := 'INDICATIE NIET OPGEVOERD BIJ ONDERZOEK';
      Else
        If r_indi.code = '999'
        Then
          v_indicatie := r_indi.opmerking;
        Else
          v_indicatie := r_indi.omschrijving;
        End If;
      End If;
      Close c_indi;
    End If;

    Return v_indicatie;

  End;

  Function getrelatiegegevens(p_rela_id Number)
  /**** Ophalen gegevens van een Relatie die gekoppeld is aan het onderzoek.
            Momenteel worden voor de EPD Koppeling alleen nog de aanvragers opgehaald.

      ****/
   Return Varchar2 As
    Cursor c_rela(c_rela_id Number) Is
      Select aanspreken From kgc_relaties Where rela_id = c_rela_id;

    r_rela c_rela%Rowtype;

  Begin
    Open c_rela(p_rela_id);
    Fetch c_rela
      Into r_rela;
    Close c_rela;

    Return r_rela.aanspreken; -- terugsturen het aanspreken veld

  End;

  Function getonderzoekwijze(p_onwy_cd Varchar2) Return Clob
  /**** Ophalen voor het onderzoek geldende OnderzoeksWijze
      ****/
   As
    Cursor c_onwy(c_code Varchar2) Is
      Select onwy.omschrijving
      From   kgc_onderzoekswijzen onwy
      Where  onwy.code = c_code;

    r_onwy c_onwy%Rowtype;

  Begin
    Open c_onwy(p_onwy_cd);
    Fetch c_onwy
      Into r_onwy;
    Close c_onwy;

    Return r_onwy.omschrijving;
  End;

  Function getprotocol(p_onde_id Number)
  /**** Ophalen Protocollen die bij het onderzoek horen
      ****/
   Return Clob As
    v_xml_doc Clob;

  Begin
    For r_prot In (Select /*+ RULE */
               stgr.omschrijving,
               meti.afgerond
              From   bas_metingen        meti,
                   kgc_stoftestgroepen stgr
              Where  stgr.stgr_id = meti.stgr_id
              And    meti.onde_id = p_onde_id
              And    meti.stgr_id Not In
                   (Select stgr_id From epd_protocol_niet_tonen))
    Loop
      v_xml_doc := v_xml_doc || '<Protocol>';
      v_xml_doc := v_xml_doc ||
               getsimplexmltag('ProtocolNaam', r_prot.omschrijving) ||
               getsimplexmltag('ProtocolStatus', r_prot.afgerond);
      v_xml_doc := v_xml_doc || '</Protocol>';


    End Loop;

    Return v_xml_doc;


  End;

  Function getuitslagteksten
  (
    p_onde_id   Number,
    p_ongr_id   Number,
    p_status    Varchar2,
    p_datum_aut Date,
    p_mede_id   Number,
    p_relatie   Varchar2
  ) Return Clob
  /** Deze functie haalt de uitslagteksten op per onderzoek.
          Op basis van afdelingsID wordt bepaald hoe de uitslagen opgebouwd worden.
          Voor Cytogenetica geld dat de historische onderzoeken op een andere manier dan de
          actuele onderzoeken worden opgehaald. Momenteel bepaald een datum in de systeem parameter tabel
          wat geld als een historische uitslag en wat geld als een actuele uitslag.

      **/
   As

    -- Ophalen brieftypes
    Cursor c_brty(c_brty_id Number) Is
      Select brty.code,
           brty.omschrijving
      From   kgc_brieftypes brty
      Where  brty.brty_id = c_brty_id;

    -- Ophalen gegevens medewerker
    Cursor c_mede(c_mede_id Number) Is
      Select mede.formele_naam,
           func.omschrijving functie
      From   kgc_medewerkers mede,
           kgc_functies    func,
           kgc_mede_kafd   meka
      Where  meka.mede_id = mede.mede_id
      And    meka.func_id = func.func_id
      And    mede.mede_id = c_mede_id;

    -- Conclusie teksten Cytogenetica ophalen
    Cursor c_meti
    (
      c_onde_id Number,
      c_stgr_id Varchar2
    ) Is
      Select meti.conclusie
      From   bas_metingen meti
      Where  meti.onde_id = c_onde_id
      And    meti.afgerond = 'J'
      And    meti.stgr_id = c_stgr_id;
    -- Conclusie teksten DNA ophalen
    Cursor c_meti_dna(c_onde_id Number) Is
      Select meti.conclusie
      From   bas_metingen meti
      Where  meti.onde_id = c_onde_id
      And    meti.afgerond = 'J';

    -- Ophalen standaard waarde systeemparameter
    Cursor c_sypa(c_code Varchar2) Is
      Select sypa.standaard_waarde
      From   kgc_systeem_parameters sypa
      Where  sypa.code = c_code;

    -- RowType declaraties Cursor

    r_brty     c_brty%Rowtype;
    r_mede     c_mede%Rowtype;
    r_meti     c_meti%Rowtype;
    r_meti_dna c_meti_dna%Rowtype;
    r_sypa     c_sypa%Rowtype;

    -- Lokale variabelen
    v_xml_doc         Clob;
    v_uitslag         Clob;
    v_conclusie       Clob;
    v_aanhef          Varchar2(4000);
    v_historie        Date;
    v_embargo_termijn Integer;
    v_embargo_datum   Date;
    v_lijn            Varchar2(40) := lpad('', 40, '-');
    v_cyto_prot       Varchar2(20);
    v_uits_gen        Clob;

  Begin
    -- Ophalen Aanhef uitslag EPD
    Open c_sypa('AANHEF_EPD');
    Fetch c_sypa
      Into r_sypa;
    Close c_sypa;
    v_aanhef := r_sypa.standaard_waarde;

    -- De datum vanaf wanneer de uitslagen technisch als hitorisch gezien worden
    Open c_sypa('EPD_HISTORIE_CYTO');
    Fetch c_sypa
      Into r_sypa;
    Close c_sypa;
    v_historie := to_date(r_sypa.standaard_waarde, 'DD-MM-YYYY');


    -- Haal alle geautoriseerde uitslagen op
    For r_uits In (Select uits.uits_id,
                   uits.brty_id,
                   replaceuitslag(uits.uitslag_alg) uitslag_alg,
                   replaceuitslag(uits.uitslag_gen) uitslag_gen,
                   uits.kafd_id,
                   decode(p_status, 'J', p_mede_id, uits.mede_id4) autorisator,
                   decode(p_status,
                        'J',
                        p_datum_aut,
                        uits.datum_mede_id4) datum_autorisatie,
                   embargo_termijn
              From   epd_uitslag uits
              Where  uits.onde_id = p_onde_id
              And    uits.epd_status = 'N'

              )
    Loop
      -- Op basis van kafd_id  wordt de opbouw en verwerking van de
      If r_uits.embargo_termijn = 9999
      Then
        v_embargo_datum := to_date('01-01-2114', 'DD-MM-YYYY');
      Else
        v_embargo_datum := r_uits.datum_autorisatie +
                     r_uits.embargo_termijn;
      End If;

      Open c_brty(r_uits.brty_id);
      Fetch c_brty
        Into r_brty;
      Close c_brty;


      Open c_mede(r_uits.autorisator);
      Fetch c_mede
        Into r_mede;
      Close c_mede;

      If p_status = 'J'
      Then
        v_xml_doc := v_xml_doc || '<Uitslag>' || chr(10) ||
                 getsimplexmltag('UitslagId',
                            to_char(r_uits.uits_id)) ||
                 chr(10) ||
                 getsimplexmltag('UitslagType', r_brty.code) ||
                 chr(10) ||
                 getsimplexmltag('UitslagTekst', r_uits.uitslag_gen) ||
                 chr(10) ||
                 getsimplexmltag('UitslagAutorisator',
                            r_mede.formele_naam) || chr(10) ||
                 getsimplexmltag('UitslagFunctieAutorisator',
                            r_mede.functie) || chr(10) ||
                 getsimplexmltag('UitslagAutorisatieDatum',
                            to_char(r_uits.datum_autorisatie,
                                 'YYYYMMDD')) || chr(10) ||
                 getsimplexmltag('UitslagEmbargoDatum',
                            to_char(v_embargo_datum, 'YYYYMMDD')) ||
                 chr(10) ||
                 getsimplexmltag('Kopiehouders',
                            getkopiehouders(r_uits.uits_id)) ||
                 chr(10) ||
                 getsimplexmltag('Uitslag_geneticus',
                            r_uits.uitslag_gen) || chr(10) ||
                 '</Uitslag>' || chr(10);
      Else
        If r_uits.datum_autorisatie Is Not Null
        Then
          v_xml_doc := v_xml_doc || '<Uitslag>' || chr(10) ||
                   getsimplexmltag('UitslagId',
                              to_char(r_uits.uits_id)) ||
                   chr(10) ||
                   getsimplexmltag('UitslagType', r_brty.code) ||
                   chr(10) ||
                   getsimplexmltag('UitslagTekst',
                              r_uits.uitslag_gen) || chr(10) ||
                   getsimplexmltag('UitslagAutorisator',
                              r_mede.formele_naam) || chr(10) ||
                   getsimplexmltag('UitslagFunctieAutorisator',
                              r_mede.functie) || chr(10) ||
                   getsimplexmltag('UitslagAutorisatieDatum',
                              to_char(r_uits.datum_autorisatie,
                                   'YYYYMMDD')) || chr(10) ||
                   getsimplexmltag('UitslagEmbargoDatum',
                              to_char(v_embargo_datum,
                                   'YYYYMMDD')) || chr(10) ||
                   getsimplexmltag('Kopiehouders',
                              getkopiehouders(r_uits.uits_id)) ||
                   chr(10) ||
                   getsimplexmltag('Uitslag_geneticus',
                              r_uits.uitslag_gen) || chr(10) ||
                   '</Uitslag>' || chr(10);
        End If;

      End If;



    End Loop;
    Return v_xml_doc;

  End;

  Function getkopiehouders(puits_id Integer) Return Clob As
    Cursor c_kopieh(c_uits_id Number) -- ophalen relatie gegevens, onderzoek kopiehouder
    Is
      Select /*+ FIRST_ROWS */
       rela.aanspreken,
       inst.naam
      From   kgc_kopiehouders koho,
           kgc_relaties     rela,
           kgc_instellingen inst
      Where  rela.rela_id = koho.rela_id
      And    koho.uits_id = c_uits_id
      And    inst.inst_id = rela.inst_id;

    v_kopieh Clob;

  Begin

    For r_kopieh In c_kopieh(puits_id)
    Loop
      v_kopieh := v_kopieh || '<Kopiehouder>' || chr(10);
      v_kopieh := v_kopieh ||
              getsimplexmltag('Kopiehouder_Naam',
                         r_kopieh.aanspreken);
      v_kopieh := v_kopieh || getsimplexmltag('Kopiehouder_Instelling',
                                 r_kopieh.naam);
      v_kopieh := v_kopieh || '</Kopiehouder>';

    End Loop;

    Return v_kopieh;
  End;



  Function getindicatiesdetail(p_onde_id Number)
  /** Ophalen detail gegevens indicaties **/
   Return Clob As
    v_xml_doc Clob;
  Begin
    For r_indi In (Select decode(indi.code,
                        '999',
                        onin.opmerking,
                        indi.omschrijving) indicatie
              From   kgc_onderzoek_indicaties onin,
                   kgc_indicatie_teksten    indi
              Where  onin.onde_id = p_onde_id
              And    onin.indi_id = indi.indi_id)
    Loop
      v_xml_doc := v_xml_doc || '<Indicatie>';
      v_xml_doc := v_xml_doc ||
               getsimplexmltag('IndicatieOmschrijving',
                          r_indi.indicatie);
      v_xml_doc := v_xml_doc || '</Indicatie>';

    End Loop;
    Return v_xml_doc;
  End;

  Function replaceuitslag(tekst In Clob) Return Clob Is
    v_tekst Clob;

  Begin
    v_tekst := Replace(tekst, '&', ';');
    v_tekst := Replace(v_tekst, '<', ';');
    Return v_tekst;

  End;

  Procedure setuitslagtekstepd
  (
    p_onde_id           Number,
    p_ongr_id           Number,
    p_uits_id           Number,
    p_brty_id           Number,
    p_tekst             Varchar2,
    p_toelichting       Varchar2,
    p_kafd_id           Number,
    p_commentaar        Varchar2,
    p_datum_autorisatie Date,
    p_relatie           Varchar2,
    p_uitslag           Out Clob,
    p_geneticus         Out Clob,
    p_embargo_termijn   Out Number
  ) As
    Cursor c_ebpt(c_brty_id Number) Is
      Select ebpt.stgr_id
      From   epd_brieftype_protocol ebpt
      Where  ebpt.brty_id = c_brty_id;
    -- Ophalen brieftypes
    Cursor c_brty(c_brty_id Number) Is
      Select brty.code,
           brty.omschrijving
      From   kgc_brieftypes brty
      Where  brty.brty_id = c_brty_id;

    -- Ophalen gegevens medewerker
    Cursor c_mede(c_mede_id Number) Is
      Select mede.formele_naam,
           func.omschrijving functie
      From   kgc_medewerkers mede,
           kgc_functies    func,
           kgc_mede_kafd   meka
      Where  meka.mede_id = mede.mede_id
      And    meka.func_id = func.func_id
      And    mede.mede_id = c_mede_id;

    -- Conclusie teksten Cytogenetica ophalen
    Cursor c_meti
    (
      c_onde_id Number,
      c_stgr_id Varchar2
    ) Is
      Select meti.conclusie
      From   bas_metingen meti
      Where  meti.onde_id = c_onde_id
      And    meti.afgerond = 'J'
      And    meti.stgr_id = c_stgr_id;
    -- Conclusie teksten DNA ophalen
    Cursor c_meti_dna(c_onde_id Number) Is
      Select meti.conclusie
      From   bas_metingen meti
      Where  meti.onde_id = c_onde_id
      And    meti.afgerond = 'J';


    -- Ophalen standaard waarde systeemparameter
    Cursor c_sypa(c_code Varchar2) Is
      Select sypa.standaard_waarde
      From   kgc_systeem_parameters sypa
      Where  sypa.code = c_code;

    -- RowType declaraties Cursor
    r_ebpt     c_ebpt%Rowtype;
    r_brty     c_brty%Rowtype;
    r_mede     c_mede%Rowtype;
    r_meti     c_meti%Rowtype;
    r_meti_dna c_meti_dna%Rowtype;
    r_sypa     c_sypa%Rowtype;

    -- Lokale variabelen
    v_xml_doc Clob;

    v_conclusie       Clob;
    v_aanhef          Varchar2(4000);
    v_historie        Date;
    v_embargo_termijn Integer;
    v_embargo_datum   Date;
    v_lijn            Varchar2(40) := lpad('', 40, '-');
    v_cyto_prot       Varchar2(20);

    v_tekst         Varchar2(4000);
    v_commentaar    Varchar2(4000);
    v_toelichting   Varchar2(4000);
    meerdaneen      Boolean;
    v_error         Varchar2(1000);
    v_ongr_cd       kgc_onderzoeksgroepen.code%Type;
    v_afdelingscode kgc_categorie_waarden.waarde%Type;
  Begin
    -- Ophalen Aanhef uitslag EPD
    -- Ophalen Aanhef uitslag EPD
    Insert Into epd_log_temp
      (creation_date, onderzoek, uitslag, melding, stap)
    Values
      (Sysdate, p_onde_id, p_uits_id, '', 'START');



    Open c_sypa('AANHEF_EPD');
    Fetch c_sypa
      Into r_sypa;
    Close c_sypa;
    v_aanhef := r_sypa.standaard_waarde;

    -- De datum vanaf wanneer de uitslagen technisch als hitorisch gezien worden
    Open c_sypa('EPD_HISTORIE_CYTO');
    Fetch c_sypa
      Into r_sypa;
    Close c_sypa;
    v_historie := to_date(r_sypa.standaard_waarde, 'DD-MM-YYYY');
    v_ongr_cd  := getonderzoeksgroep(p_ongr_id);
    v_afdelingscode := getcategoriewaarde(v_ongr_cd, null);
    -- Haal alle geautoriseerde uitslagen op

    -- Op basis van kafd_id  wordt de opbouw en verwerking van de
    -- uitslagen bepaald
    v_tekst       := replaceuitslag(p_tekst);
    v_toelichting := replaceuitslag(p_toelichting);
    v_commentaar  := replaceuitslag(p_commentaar);
    Open c_brty(p_brty_id);
    Fetch c_brty
      Into r_brty;
    Close c_brty;




    Case v_afdelingscode
      When 'METAB' Then
        -- Bepalen uitslag criteria EMZ
        p_geneticus := 'Zie bijgevoegd PDF Document voor de uitslag van Erfelijke Metabole Ziekten';
        p_uitslag   := p_geneticus;
      When 'CYTO' Then
        -- CYTO GENETICA SAMENSTELLEN UITSLAGEN
        If r_brty.code Like 'NIET_VERST'
        Then
          Null;
        Else

          -- Als de uitslag datum voor de gestelde datum ligt,
          -- dan dienen de cyto verslagen op een andere manier samengesteld te worden.
          -- dit i.v.m. Conversie van de historische WORD uitslagen naar de KGC_UITSLAGEN tabel
          -- De historische uitslag bevat in dit geval al de conclusie vanuit BAS_METINGEN

          If p_datum_autorisatie > v_historie
          Then
            --  Ophalen Actuele uitslagen uit METI en UITS


            For r_ebpt In c_ebpt(p_brty_id)
            Loop
              -- verwerk alle protocollen behorende bij het brieftype
              Open c_meti(p_onde_id, r_ebpt.stgr_id);
              Fetch c_meti
                Into r_meti;
              If c_meti%Found
              Then
                v_conclusie := v_conclusie || r_meti.conclusie;
              End If;
              Close c_meti;

            End Loop;

            -- Formateer uitslag in de juiste formaat
            p_geneticus := 'Deze uitslag is aangevraagd door: ' ||
                      p_relatie || '.' || chr(10) || v_aanhef ||
                      chr(10) || chr(10) || 'Uitslag: ' ||
                      chr(10) || replaceuitslag(v_conclusie) ||
                      chr(10) || v_lijn || chr(10) ||
                      'Conclusie:' || chr(10) || p_tekst ||
                      chr(10) || v_lijn || chr(10) ||
                      'Toelichting:' || chr(10) || p_toelichting;
          Else
            -- Ophalen hitsorische uitslag
            p_geneticus := 'Deze uitslag is aangevraagd door: ' ||
                      p_relatie || '.' || chr(10) || v_aanhef ||
                      chr(10) || chr(10) || 'Uitslag: ' ||
                      chr(10) || p_tekst;
          End If;
          p_uitslag := p_geneticus;
        End If;

      Else
        -- Bepalen uitslag criteria DNA pre samenvoegen en alle niet metab na samenvoegen.

        For r_meti_dna In c_meti_dna(p_onde_id)
        Loop
          v_conclusie := v_conclusie || r_meti_dna.conclusie ||
                    chr(10) || chr(10);
        End Loop;
        p_geneticus := 'Deze uitslag is aangevraagd door: ' ||
                  p_relatie || '.' || chr(10) || v_aanhef ||
                  chr(10) || chr(10) || 'Uitslag: ' || chr(10) ||
                  replaceuitslag(v_conclusie) || chr(10) || v_lijn ||
                  'Conclusie:' || chr(10) || p_tekst || chr(10) ||
                  chr(10) || 'Toelichting:' || chr(10) || v_lijn ||
                  chr(10) || p_toelichting;
        If p_datum_autorisatie >= '01-01-2015'
        Then
          p_uitslag := 'Deze uitslag is aangevraagd door: ' ||
                   p_relatie || '.' || chr(10) || v_aanhef ||
                   chr(10) || chr(10) || 'Uitslag: ' || chr(10) ||
                   replaceuitslag(v_conclusie) || chr(10) ||
                   v_lijn || chr(10) || 'Conclusie:' || chr(10) ||
                   p_tekst || chr(10) || v_lijn || chr(10) ||
                   'Indien u meer achtergrondinformatie over deze uitslag wenst, ' ||
                   'kunt u contact opnemen met de afdeling Klinische Genetica, laboratorium Clinical Genomics.';
        Else
          p_uitslag := 'Deze uitslag is aangevraagd door: ' ||
                   p_relatie || '.' || chr(10) || v_lijn ||
                   chr(10) ||
                   'DNA-uitslagen van voor 1 januari 2015 zijn binnen uw EPD-rechten afgeschermd.' ||
                   chr(10) ||
                   'Neemt u a.u.b. contact op met de aanvrager of de afdeling Klinische Genetica.';
        End If;


    End Case;

    v_embargo_termijn := getembargo(p_ongr_id, '');
    If p_datum_autorisatie > to_date('31-12-2014', 'DD-MM-YYYY')
    Then
      v_embargo_termijn := -1;
    End If;
    p_embargo_termijn := v_embargo_termijn;

    Insert Into epd_log_temp
      (creation_date, onderzoek, uitslag, melding, stap)
    Values
      (Sysdate, p_onde_id, p_uits_id, '', 'KLAAR');


  Exception
    When Others Then
      Begin
        v_error := Sqlerrm;
        Insert Into epd_log_temp
          (creation_date, onderzoek, uitslag, melding, stap)
        Values
          (Sysdate, p_onde_id, p_uits_id, v_error, 'ERROR');

      End;

  End;

  Procedure maak_epd_uitslag_aan As
    Cursor c_uits Is
      Select * From kgc_uitslagen;
    -- where uits_id in (select uits_id from Z_TEMP_CORRECTIE_EPD) ;

    Cursor c_uitsh(c_uits_id Number) Is
      Select Max(epd_versienr) versie
      From   epd_uitslag
      Where  uits_id = c_uits_id;

    Cursor c_evut(c_uits_id Number) Is
      Select evut.*
      From   epd_uitslag evut
      Where  evut.uits_id = c_uits_id;

    Cursor c_onde(c_onde_id Number) Is
      Select onde.ongr_id,
           rela.aanspreken,
           mede_id_autorisator,
           afgerond,
           datum_autorisatie
      From   kgc_onderzoeken onde,
           kgc_relaties    rela
      Where  onde.onde_id = c_onde_id
      And    onde.rela_id = rela.rela_id;

    r_uits   c_uits%Rowtype;
    onde_rec c_onde%Rowtype;
    r_uitsh  c_uitsh%Rowtype;
    r_evut   c_evut%Rowtype;
    v_versie Number := 1;
    e_error Exception;
    v_uitslag         Clob;
    v_uits_gen        Clob;
    v_embargo_termijn Number;
    v_dat_auto        Date;
    -- local variables here
  Begin

    --    In verband met Kwaliteits eisen worden meerdere versies van Uitslagen Record Bewaard in Historie tabel
    --    KGC_UITSLAGEN_HISTORIE  FUNCTIONALITEIT
    For r_uits In c_uits
    Loop

      Open c_uitsh(r_uits.uits_id);
      Fetch c_uitsh
        Into r_uitsh;
      If r_uitsh.versie Is Null
      Then
        v_versie := 1;
      Else
        v_versie := r_uitsh.versie + 1;
      End If;
      Close c_uitsh;


      /*  Door Tijds en status registratie in de tabel EPD_uitslag is onderstaande functie overbodig geworden.
             In EPD_uitslag wordt een bevroren uitslag van moment X getoond.
         */
      Open c_onde(r_uits.onde_id);
      Fetch c_onde
        Into onde_rec;
      Close c_onde;


      -- EPD FUNCTIONALITEIT UITSLAGEN --

      If r_uits.mede_id4 Is Not Null Or
        onde_rec.mede_id_autorisator Is Not Null
      Then

        Open c_evut(r_uits.uits_id);
        Fetch c_evut
          Into r_evut;

        If c_evut%Found
        Then
          Update epd_uitslag evut
          Set    evut.epd_status      = 'O',
               evut.epd_status_date = Sysdate
          Where  evut.uits_id = r_uits.uits_id;

        End If;
        Close c_evut;

        If onde_rec.mede_id_autorisator Is Not Null
        Then
          v_dat_auto := onde_rec.datum_autorisatie;
        Else
          v_dat_auto := r_uits.datum_mede_id4;

        End If;


        epd_lus_interface_maast_prd.setuitslagtekstepd(r_uits.onde_id,
                                        onde_rec.ongr_id,
                                        r_uits.uits_id,
                                        r_uits.brty_id,
                                        r_uits.tekst,
                                        r_uits.toelichting,
                                        r_uits.kafd_id,
                                        r_uits.commentaar,
                                        v_dat_auto,
                                        onde_rec.aanspreken,
                                        v_uitslag,
                                        v_uits_gen,
                                        v_embargo_termijn);



        Insert Into epd_uitslag
          (uits_id,
           onde_id,
           kafd_id,
           rela_id,
           created_by,
           creation_date,
           last_updated_by,
           last_update_date,
           brty_id,
           mede_id,
           datum_uitslag,
           datum_autorisatie,
           volledig_adres,
           mede_id2,
           datum_mede_id,
           datum_mede_id2,
           mede_id3,
           datum_mede_id3,
           mede_id4,
           datum_mede_id4,
           uitslag_gen,
           uitslag_alg,
           epd_status,
           epd_creation_date,
           epd_status_date,
           epd_versienr,
           embargo_termijn,
           onde_autorisator,
           onde_afgerond,
           onde_datum_autorisatie,
           epd_uits_id


           )
        Values
          (r_uits.uits_id,
           r_uits.onde_id,
           r_uits.kafd_id,
           r_uits.rela_id,
           r_uits.created_by,
           r_uits.creation_date,
           r_uits.last_updated_by,
           r_uits.last_update_date,
           r_uits.brty_id,
           r_uits.mede_id,
           r_uits.datum_uitslag,
           r_uits.datum_autorisatie,
           r_uits.volledig_adres,
           r_uits.mede_id2,
           r_uits.datum_mede_id,
           r_uits.datum_mede_id2,
           r_uits.mede_id3,
           r_uits.datum_mede_id3,
           r_uits.mede_id4,
           r_uits.datum_mede_id4,
           v_uits_gen,
           v_uitslag,
           'N',
           Sysdate,
           Sysdate,
           v_versie,
           v_embargo_termijn,
           onde_rec.mede_id_autorisator,
           onde_rec.afgerond,
           onde_rec.datum_autorisatie,
           epd_uits_seq.nextval);

      End If;




    End Loop;




  End;


  Function getonderzoeksgroep(p_ongr_id kgc_onderzoeksgroepen.ongr_id%Type)
    Return Varchar2

   As

    Cursor c_ongr(c_ongr_id kgc_onderzoeksgroepen.ongr_id%Type) Is
      Select code From kgc_onderzoeksgroepen Where ongr_id = c_ongr_id;


    r_ongr c_ongr%Rowtype;
  Begin
    Open c_ongr(p_ongr_id);
    Fetch c_ongr
      Into r_ongr;
    Close c_ongr;

    Return r_ongr.code;

  End;

  Function getcategoriewaarde(p_ongr_cd kgc_onderzoeksgroepen.code%Type, p_afd_code varchar2)
    Return Varchar2 As

    Cursor c_cawa_ongr(c_ongr_cd kgc_onderzoeksgroepen.code%Type) Is
      Select cate.code
      From   kgc_categorie_types   caty,
           kgc_categorieen       cate,
           kgc_categorie_waarden cawa
      Where  caty.caty_id = cate.caty_id
      And    cawa.cate_id = cate.cate_id
      And    cawa.waarde = c_ongr_cd;

      Cursor c_cawa_afd(c_afd_code varchar2) Is
      Select cawa.waarde
         From   kgc_categorie_types   caty,
           kgc_categorieen       cate,
           kgc_categorie_waarden cawa
      Where  caty.caty_id = cate.caty_id
      And    cawa.cate_id = cate.cate_id
      And    cate.code = 'METAB';

    r_cawa_ongr c_cawa_ongr%rowtype;
     r_cawa_afd  c_cawa_afd%rowtype;
      v_return varchar2(20);
  Begin
     If p_ongr_cd is not null Then

      Open c_cawa_ongr(p_ongr_cd);
    Fetch c_cawa_ongr
      Into r_cawa_ongr;
    If c_cawa_ongr%Notfound
    Then
        v_return := 'DNA';

      Else
         v_return := r_cawa_ongr.code;

      End If;
    Close c_cawa_ongr;

      elsif  p_afd_code is not null then
            Open c_cawa_afd(p_afd_code);
    Fetch c_cawa_afd
      Into r_cawa_afd;
    If c_cawa_afd%Notfound
    Then
        v_return := '';

      Else
         v_return := r_cawa_afd.waarde;

      End If;
    Close c_cawa_afd;

      End if;

      return v_return;
  End;


/***
Function LUS_GetOrders(p_PatientID Varchar2, p_amount Integer, p_offSet Integer)

     Ophalen EMZ Orders(Monsters) op Basis van PatientID , OFFSET en het aantal Bepalingen die opgehaald dienen te worden.

   Return Clob
  As

    -- Ophalen Interne Persoon Identificatie op basis van Externe ZISNR
    Cursor c_pers(c_zisNr varchar2)
    Is
      Select /*+ RULE*/
/*              Pers_id
            , Aanspreken
      From kgc_personen
      Where zisnr = c_zisNr ;

    -- Totaal aantal orders bepalen bij deze patient
    -- 1 Monster = 1 Order

    Cursor C_Order_amount(c_pers_id Number)
    Is
      Select Count(ELOR.Mons_id)
      From     EPD_LUS_ORDERS_VW ELOR
      Where    ELOR.pers_id = c_pers_id
      And      'J' =   LUS_OnderzoekAfgerond(ELOR.mons_id)
                          ;
     Cursor C_Rela(c_rela_id Number)
     Is
       Select  /*+ RULE */
/*               RELA.Aanspreken
             , AFDG.Omschrijving
       From    Kgc_relaties RELA,
               Kgc_Afdelingen AFDG
       Where   RELA.Rela_Id = c_rela_id
       And     RELA.Afdg_Id = AFDG.Afdg_Id ;

    Cursor C_Mate(c_mate_id Number)
    Is
      Select MATE.OMSCHRIJVING
      From   Kgc_Materialen MATE
      Where  MATE.Mate_Id = c_mate_id;


      r_pers c_pers%RowType;
      r_rela c_rela%RowType;
      r_mate c_mate%RowType;

      v_xml clob;

  --    v_date_format varchar2(10) := 'YYYYMMDD';
      v_total_orders Integer := 0;
      v_dat_afgerond varchar2(10);
      E_skip_Monster Exception;

  Begin
  --   v_xml := v_xml||'<xml version="1.0">';
     v_xml := v_xml||EPD_GetComplexType('Patient',v_start);
     Open c_pers(p_patientID);
     Fetch c_pers into r_pers ;
     Close c_pers;

     Open c_order_amount(r_pers.pers_id);
     Fetch c_order_amount into v_total_orders ;
     Close c_order_amount;

     v_xml := v_xml||GetSimpleXmlTag('Number',p_PatientID)
                   ||GetSimpleXmlTag('TotalAvailableOrders',to_char(v_total_orders));
     v_xml := v_xml||EPD_GetComplexType('Orders',v_start);


     For r_mons in ( Select
                              Mons_id,
                              MonsterNummer,
                              Datum_aanmelding,
                              Datum_afname,
                              Commentaar,
                              Mate_id,
                              Rela_id,
                              created_by
                       From ( Select ELOR.Mons_id,
                                     ELOR.MonsterNummer,
                                     ELOR.Datum_aanmelding,
                                     ELOR.Datum_afname,
                                     ELOR.Commentaar,
                                     ELOR.Mate_id,
                                     ELOR.Rela_id,
                                     ELOR.created_by,
                                     row_number() over (order by Mons_Id desc) T
                            From     EPD_LUS_ORDERS_VW ELOR
                             Where    ELOR.pers_id = r_pers.pers_id
                            And      'J' =   LUS_OnderzoekAfgerond(ELOR.mons_id)

                    )
                       Where    T between p_offSet and (p_offSet + p_amount)
                    )
     Loop
        Open c_mate(r_mons.mate_id);
        fetch c_mate into r_mate;
        Close c_mate;

        Open c_rela(r_mons.rela_id);
        fetch c_rela into r_rela;
        Close c_rela;

        v_xml := v_xml||EPD_GetComplexType('Order',v_start);

        v_xml := v_xml||GetSimpleXmlTag('Code',r_mons.Monsternummer)
                      ||GetSimpleXmlTag('SampleCollectionTime',to_char(r_mons.datum_aanmelding,v_date_format))
                      ||GetSimpleXmlTag('Requester',r_rela.aanspreken)
                      ||GetSimpleXmlTag('RequestingDepartment',r_rela.omschrijving)
                      ||GetSimpleXmlTag('State','3')
                      ||GetSimpleXmlTag('Observations',LUS_GetObservations(r_mons.mons_id, 0,v_dat_afgerond))
                      ||GetSimpleXmlTag('LastReportDate',v_dat_afgerond)
                      ||GetSimpleXmlTag('Comments',r_mons.commentaar)
                      ||GetSimpleXmlTag('MaterialType',r_mate.omschrijving)
                      ||GetSimpleXmlTag('MaterialReceived',to_char(r_mons.datum_aanmelding, v_date_format))    ;

        v_xml := v_xml||EPD_GetComplexType('Order',v_stop);

     End Loop;
     v_xml := v_xml||EPD_GetComplexType('Orders',v_stop);
     v_xml := v_xml||EPD_GetComplexType('Patient',v_stop);


     Return v_xml;

  End;

  Function  LUS_OnderzoekAfgerond(p_mons_id number)
  /**
     Controleer of alle onderzoeken die een relatie hebben met het doorgegeven monster
     afgerond zijn. Indien er een onderzoek niet afgerond is dan geef een 'N' terug
     deze 'N' staat voor niet afgerond.
  **/

/*    Return varchar2
  As
    Cursor c_Autorised(c_mons_id Number)
    Is
       Select /*+ RULE */
/*              Afgerond
       from kgc_onderzoeken onde,
            kgc_onderzoek_monsters onmo
       where onde.onde_id = onmo.onde_id
       and   onmo.mons_id = c_mons_id ;


    r_Autorised c_Autorised%RowType;
    v_Afgerond varchar2(1) := 'J';

  Begin
     For r_aut in  c_Autorised(p_mons_id)
       Loop
        If r_Aut.afgerond = 'N' Then
          v_afgerond := 'N';
          exit;
       End If;
       End Loop;
     Return v_afgerond;

  End;


  Function LUS_GetObservations(p_mons_id Number, p_stgr_id Number,  p_dat_afgerond out varchar2)
    return clob
    /** Ophalen bepalingen horende bij een Order **/
/*  As
    Cursor c_Bere (c_meet_id Number)
    Is
      Select  /*+ RULE */
/*              BERE.Uitslag
            , BERE.Uitslageenheid
            , BERE.Normaalwaarde_Ondergrens Ondergrens
            , BERE.Normaalwaarde_Bovengrens Bovengrens
            , BERE.Normaalwaarde_Gemiddelde Gemiddelde
            , BERE.CREATION_DATE Datum_berekening
      From    Bas_Berekeningen BERE
      Where   BERE.meet_id = c_meet_id;

      r_bere c_bere%RowType;
      v_bere_aanwezig Boolean;
      v_xml Clob;
      v_tel Integer := 0;
      v_commentaar varchar2(1000);
      v_dat_afgerond varchar2(10);
  Begin

     For r_meet in ( Select ELOB.*
                     From   EPD_LUS_OBSERVATIONS_VW ELOB
                     Where  ELOB.mons_id = p_mons_id
                    -- And    ELOB.stgr_id = p_stgr_id
                    )
     Loop
        r_bere := null;

        If v_tel = 0 Then
           v_tel := 1;
           v_dat_afgerond := to_char(r_meet.datum_afgerond,'YYYY-MM-DD');
        End If;

        If r_meet.Created_By = 'CONVERSIE' Then
           v_commentaar := 'Deze bepaling is in 2006 geconverteerd naar een Helix bepaling.';
           p_dat_afgerond := to_char(r_meet.creation_date,'YYYY-MM-DD');
        Else
            v_commentaar := '';
            p_dat_afgerond := v_dat_afgerond;
        End If;

        Open c_bere(r_meet.meet_id);
        If c_bere%Found Then
           v_bere_aanwezig := true;
           Fetch c_bere into r_bere;
        End If;
        Close c_bere;

         v_xml := v_xml||EPD_GetComplexType('Observation',v_start);

         v_xml := v_xml||GetSimpleXmlTag('Identifier', r_meet.stoftestcode)
                       ||GetSimpleXmlTag('OrderNumber', to_char(r_meet.meet_id))
                       ||GetSimpleXmlTag('MainGroupName',r_meet.stoftestgroep)
                       ||GetSimpleXmlTag('MainGroupOrderNumber',to_char(r_meet.meting))
                        ||GetSimpleXmlTag('SubgroupName','')
                       ||GetSimpleXmlTag('SubgroupOrderNumber','')
                       ||GetSimpleXmlTag('Name',r_meet.stoftest);

         If v_bere_aanwezig Then
            v_xml := v_xml||GetSimpleXmlTag('Value',to_char(r_bere.uitslag))
                          ||GetSimpleXmlTag('ValueType','4')
                          ||GetSimpleXmlTag('Unit',r_bere.uitslageenheid)
                          ||GetSimpleXmlTag('Range',LUS_getRange(to_char(r_bere.bovengrens), to_char(r_bere.ondergrens)))
                          ||GetSimpleXmlTag('State','3')
                          ||GetSimpleXmlTag('DateAnalyzed',to_char(r_bere.datum_berekening, v_date_format))
                          ||GetSimpleXmlTag('Comments',v_commentaar)  ;

         Else
           v_xml := v_xml||GetSimpleXmlTag('Value',r_meet.meetwaarde)
                         ||GetSimpleXmlTag('ValueType','4')
                         ||GetSimpleXmlTag('Unit',r_meet.meeteenheid)
                         ||GetSimpleXmlTag('Range',LUS_getRange(to_char(r_meet.bovengrens), to_char(r_meet.ondergrens)))
                         ||GetSimpleXmlTag('State','3')
                         ||GetSimpleXmlTag('DateAnalyzed',nvl(to_char(r_meet.datum_meting, v_date_format), to_char(r_meet.creation_date,v_date_format)))
                         ||GetSimpleXmlTag('Comments',v_commentaar)  ;

         End If;
             v_xml := v_xml||EPD_GetComplexType('Observation',v_stop);
    End Loop;
  --  p_dat_afgerond := v_dat_afgerond;

    return v_xml;

  End;

  Function LUS_GetRange(p_Bovengrens varchar2, p_ondergrens varchar2)
    return varchar2
  As
   v_range varchar2(500):= '';
  Begin
     If P_ondergrens is not null and p_bovengrens is not null Then
        v_range:= p_ondergrens||' - '||p_bovengrens  ;
     Else
        If p_ondergrens is not null Then
           v_range :=v_range||';'||p_onderGrens||' ';
        End If;

        If p_Bovengrens is not null Then
           v_range :=v_range||p_BovenGrens||';';
        End if;
     End if;
     return v_range;
  End;

Function LUS_GetOrdersFiltererd(p_patientId varchar2, p_Start_Date Date, p_End_Date Date, p_stoftest varchar2)
/**
     Ophalen EMZ Orders(Monsters) op Basis van PatientID , OFFSET en het aantal Bepalingen die opgehaald dienen te worden.
  **/
/*   Return Clob
  As

    -- Ophalen Interne Persoon Identificatie op basis van Externe ZISNR
    Cursor c_pers(c_zisNr varchar2)
    Is
      Select /*+ RULE*/
/*             Pers_id
            , Aanspreken
      From kgc_personen
      Where zisnr = c_zisNr ;

    -- Totaal aantal orders bepalen bij deze patient
    -- 1 Monster = 1 Order

    Cursor C_Order_amount(c_pers_id Number)
    Is
      Select Count(ELOR.Mons_id)
      From     EPD_LUS_ORDERS_VW ELOR
      Where    ELOR.pers_id = c_pers_id
      And      'J' =   LUS_OnderzoekAfgerond(ELOR.mons_id)
                          ;
     Cursor C_Rela(c_rela_id Number)
     Is
       Select  /*+ RULE */
/*              RELA.Aanspreken
             , AFDG.Omschrijving
       From    Kgc_relaties RELA,
               Kgc_Afdelingen AFDG
       Where   RELA.Rela_Id = c_rela_id
       And     RELA.Afdg_Id = AFDG.Afdg_Id ;

    Cursor C_Mate(c_mate_id Number)
    Is
      Select MATE.OMSCHRIJVING
      From   Kgc_Materialen MATE
      Where  MATE.Mate_Id = c_mate_id;


      r_pers c_pers%RowType;
      r_rela c_rela%RowType;
      r_mate c_mate%RowType;

      v_xml clob;

  --    v_date_format varchar2(10) := 'YYYYMMDD';
      v_total_orders Integer := 0;
      v_dat_afgerond varchar2(10);
      E_skip_Monster Exception;

  Begin
  --   v_xml := v_xml||'<xml version="1.0">';
     v_xml := v_xml||EPD_GetComplexType('Patient',v_start);
     Open c_pers(p_patientID);
     Fetch c_pers into r_pers ;
     Close c_pers;

     Open c_order_amount(r_pers.pers_id);
     Fetch c_order_amount into v_total_orders ;
     Close c_order_amount;

     v_xml := v_xml||GetSimpleXmlTag('Number',p_PatientID)
                   ||GetSimpleXmlTag('TotalAvailableOrders',to_char(v_total_orders));
     v_xml := v_xml||EPD_GetComplexType('Orders',v_start);


     For r_mons in ( Select
                              Mons_id,
                              MonsterNummer,
                              Datum_aanmelding,
                              Datum_afname,
                              Commentaar,
                              Mate_id,
                              Rela_id,
                              stgr_id,
                              created_by,
                              stoftestID

                       From ( Select ELOR.Mons_id,
                                     ELOR.MonsterNummer,
                                     ELOR.Datum_aanmelding,
                                     ELOR.Datum_afname,
                                     ELOR.Commentaar,
                                     ELOR.Mate_id,
                                     ELOR.Rela_id,
                                     ELOR.STGR_ID,
                                     ELOR.created_by,
                                     elor.stoftest,
                                     ELOR.stoftestID,
                                     row_number() over (order by Mons_Id desc) T
                            From     EPD_LUS_ORDERS_FILTERED_VW ELOR
                             Where    ELOR.pers_id = r_pers.pers_id
                            And      'J' =   LUS_OnderzoekAfgerond(ELOR.mons_id)

                    )
                       Where    datum_aanmelding between p_Start_Date and p_End_Date
                       and      stoftest = p_stoftest
                    )
     Loop
        Open c_mate(r_mons.mate_id);
        fetch c_mate into r_mate;
        Close c_mate;

        Open c_rela(r_mons.rela_id);
        fetch c_rela into r_rela;
        Close c_rela;

        v_xml := v_xml||EPD_GetComplexType('Order',v_start);

        v_xml := v_xml||GetSimpleXmlTag('Code',r_mons.Monsternummer)
                      ||GetSimpleXmlTag('SampleCollectionTime',to_char(r_mons.datum_aanmelding,v_date_format))
                      ||GetSimpleXmlTag('Requester',r_rela.aanspreken)
                      ||GetSimpleXmlTag('RequestingDepartment',r_rela.omschrijving)
                      ||GetSimpleXmlTag('State','3')
                      ||GetSimpleXmlTag('Observations',LUS_GetObservationsFiltered(r_mons.mons_id, r_mons.stgr_id,r_mons.stoftestID,v_dat_afgerond))
                      ||GetSimpleXmlTag('LastReportDate',v_dat_afgerond)
                      ||GetSimpleXmlTag('Comments',r_mons.commentaar)
                      ||GetSimpleXmlTag('MaterialType',r_mate.omschrijving)
                      ||GetSimpleXmlTag('MaterialReceived',to_char(r_mons.datum_aanmelding, v_date_format))    ;

        v_xml := v_xml||EPD_GetComplexType('Order',v_stop);

     End Loop;
     v_xml := v_xml||EPD_GetComplexType('Orders',v_stop);
     v_xml := v_xml||EPD_GetComplexType('Patient',v_stop);


     Return v_xml;

  End;

  Function LUS_GetObservationsFiltered(p_mons_id Number, p_stgr_id Number, p_stof_id number, p_dat_afgerond out varchar2)
    return clob
    /** Ophalen bepalingen horende bij een Order **/
/*  As
    Cursor c_Bere (c_meet_id Number)
    Is
      Select  /*+ RULE */
/*             BERE.Uitslag
            , BERE.Uitslageenheid
            , BERE.Normaalwaarde_Ondergrens Ondergrens
            , BERE.Normaalwaarde_Bovengrens Bovengrens
            , BERE.Normaalwaarde_Gemiddelde Gemiddelde
            , BERE.CREATION_DATE Datum_berekening
      From    Bas_Berekeningen BERE
      Where   BERE.meet_id = c_meet_id;

      r_bere c_bere%RowType;
      v_bere_aanwezig Boolean;
      v_xml Clob;
      v_tel Integer := 0;
      v_commentaar varchar2(1000);
      v_dat_afgerond varchar2(10);
  Begin

     For r_meet in ( Select ELOB.*
                     From   EPD_LUS_OBSERVATIONS_VW ELOB
                     Where  ELOB.mons_id = p_mons_id
                     And    ELOB.stgr_id = p_stgr_id
                     and    ELOB.stoftestId = p_stof_id
                    )
     Loop
        r_bere := null;

        If v_tel = 0 Then
           v_tel := 1;
           v_dat_afgerond := to_char(r_meet.datum_afgerond,'YYYY-MM-DD');
        End If;

        If r_meet.Created_By = 'CONVERSIE' Then
           v_commentaar := 'Deze bepaling is in 2006 geconverteerd naar een Helix bepaling.';
           p_dat_afgerond := to_char(r_meet.creation_date,'YYYY-MM-DD');
        Else
            v_commentaar := '';
            p_dat_afgerond := v_dat_afgerond;
        End If;

        Open c_bere(r_meet.meet_id);
        If c_bere%Found Then
           v_bere_aanwezig := true;
           Fetch c_bere into r_bere;
        End If;
        Close c_bere;

         v_xml := v_xml||EPD_GetComplexType('Observation',v_start);

         v_xml := v_xml||GetSimpleXmlTag('Identifier', r_meet.stoftestcode)
                       ||GetSimpleXmlTag('OrderNumber', to_char(r_meet.meet_id))
                       ||GetSimpleXmlTag('MainGroupName',r_meet.stoftestgroep)
                       ||GetSimpleXmlTag('MainGroupOrderNumber',to_char(r_meet.meting))
                        ||GetSimpleXmlTag('SubgroupName','')
                       ||GetSimpleXmlTag('SubgroupOrderNumber','')
                       ||GetSimpleXmlTag('Name',r_meet.stoftest);

         If v_bere_aanwezig Then
            v_xml := v_xml||GetSimpleXmlTag('Value',to_char(r_bere.uitslag))
                          ||GetSimpleXmlTag('ValueType','4')
                          ||GetSimpleXmlTag('Unit',r_bere.uitslageenheid)
                          ||GetSimpleXmlTag('Range',LUS_getRange(to_char(r_bere.bovengrens), to_char(r_bere.ondergrens)))
                          ||GetSimpleXmlTag('State','3')
                          ||GetSimpleXmlTag('DateAnalyzed',to_char(r_bere.datum_berekening, v_date_format))
                          ||GetSimpleXmlTag('Comments',v_commentaar)  ;

         Else
           v_xml := v_xml||GetSimpleXmlTag('Value',r_meet.meetwaarde)
                         ||GetSimpleXmlTag('ValueType','4')
                         ||GetSimpleXmlTag('Unit',r_meet.meeteenheid)
                         ||GetSimpleXmlTag('Range',LUS_getRange(to_char(r_meet.bovengrens), to_char(r_meet.ondergrens)))
                         ||GetSimpleXmlTag('State','3')
                         ||GetSimpleXmlTag('DateAnalyzed',nvl(to_char(r_meet.datum_meting, v_date_format), to_char(r_meet.creation_date,v_date_format)))
                         ||GetSimpleXmlTag('Comments',v_commentaar)  ;

         End If;
             v_xml := v_xml||EPD_GetComplexType('Observation',v_stop);
    End Loop;
  --  p_dat_afgerond := v_dat_afgerond;

    return v_xml;

  End;

  */


End epd_lus_interface_maastricht;
/

/
QUIT
