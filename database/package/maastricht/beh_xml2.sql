CREATE OR REPLACE PACKAGE "HELIX"."BEH_XML2" AS
/******************************************************************************
   NAME:       BEH_XML2
   PURPOSE: Creation of XML files for input in WORD Documents

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3-11-2011      RKON       1. Created this package.
******************************************************************************/
FUNCTION toxml(p_tekst VARCHAR2) RETURN VARCHAR2 ;

FUNCTION getPersoon(Patient_ID in varchar2) return CLOB ;

PROCEDURE CYTO_JAK_BRIEF (
    p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
   ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
   ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
   ,p_best_id          IN   NUMBER   DEFAULT NULL
    , PCLOB OUT CLOB
  );
  Function testXML return CLOB;

END BEH_XML2;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_XML2" IS
/******************************************************************************
   NAME:       BEH_XML2
   PURPOSE: Creation of XML files for input in WORD Documents

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3-11-2011      RKON       1. Created this package.
******************************************************************************/

FUNCTION toxml(p_tekst VARCHAR2) RETURN VARCHAR2 IS
BEGIN
  RETURN Kgc_Xml_00.neem_letterlijk(p_tekst);
END;


FUNCTION getPersoon(Patient_ID in varchar2) return CLOB  IS

 vPers_Data  CLOB ;

 Cursor C_Pers is
                            Select  PERS.ZISNR,
                                       PERS.ACHTERNAAM,
                                       PERS.ADRES,
                                       PERS.VOORLETTERS,
                                       PERS.BSN,
                                       PERS.LAND,
                                       PERS.GEBOORTEDATUM,
                                       PERS.POSTCODE,
                                       PERS.WOONPLAATS,
                                       PERS.TELEFOON,
                                       PERS.AANSPREKEN,
                                       PERS.ACHTERNAAM_PARTNER,
                                       PERS.VOORVOEGSEL,
                                       PERS.ZOEKNAAM,
                                       PERS.RELA_ID
                           From    KGC_personen PERS
                           Where  PERS.ZISNR = Patient_ID ;

 R_Pers  C_PERS%RowType ;

 Cursor C_Huisarts(cRelaId Number) is
                                Select   REL.CODE,
                                            REL.AANSPREKEN,
                                            REL.BRIEFAANHEF,
                                            REL.VOORLETTERS,
                                            REL.VOORVOEGSEL,
                                            REL.ACHTERNAAM,
                                            REL.ADRES,
                                            REL.POSTCODE,
                                            REL.WOONPLAATS
                               From     kgc_relaties REL
                               Where   REL.RELA_ID = cRelaId;

R_Huisarts C_Huisarts%rowtype;

zisnummer varchar2(100);

Begin
    Open C_Pers ;
       If C_Pers%notfound then
           Raise  NO_DATA_FOUND;
       else
            Fetch C_Pers into R_pers;
        end if;
    Close C_pers;

    Open C_huisarts(R_pers.rela_id);
        Fetch  C_huisarts into R_huisarts ;
    Close C_huisarts;



    Kgc_XML_00.init;
    dbms_lob.createtemporary (vpers_data, FALSE);

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                          ,p_xml_regel     => '<Persoonsgegevens'||
                                                       ' GroupHeader> '||Chr(10)
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_ZISNr'
                         ,p_xml_regel     => R_pers.Zisnr
                         );
    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Aanspreken'
                         ,p_xml_regel     => R_pers.aanspreken
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Achternaam'
                         ,p_xml_regel     => R_pers.achternaam
                         );
     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Achternaam_partner'
                         ,p_xml_regel     => R_pers.achternaam_partner
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Voorletters'
                         ,p_xml_regel     => R_pers.voorletters
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Voorvoegsel'
                         ,p_xml_regel     => R_pers.voorvoegsel
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_BSN'
                         ,p_xml_regel     => R_pers.bsn
                         );
    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Adres'
                         ,p_xml_regel     => R_pers.adres
                         );
    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Postcode'
                         ,p_xml_regel     => R_pers.postcode
                         );
    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Woonplaats'
                         ,p_xml_regel     => R_pers.woonplaats
                         );
    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Land'
                         ,p_xml_regel     => R_pers.land
                         );
    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'Pers_Geboortedatum'
                         ,p_xml_regel     => R_pers.geboortedatum
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                          ,p_xml_regel     => '</Persoonsgegevens'||
                                                       ' GroupHeader>'||CHR(10)
                         );
    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                          ,p_xml_regel     => '<Huisarts'||
                                                       ' > '
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'HuisArts_Code'
                         ,p_xml_regel     => R_Huisarts.Code
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'HuisArts_Aanspreken'
                         ,p_xml_regel     => R_Huisarts.aanspreken
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'HuisArts_Adres'
                         ,p_xml_regel     => R_Huisarts.Adres
                         );

   Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'HuisArts_Postcode'
                         ,p_xml_regel     => R_Huisarts.Postcode
                         );

   Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                         ,p_tag           => 'HuisArts_Woonplaats'
                         ,p_xml_regel     => R_Huisarts.Woonplaats
                         );

    Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => vpers_data
                          ,p_xml_regel     => '<Huisarts'||
                                                       ' > '||CHR(10)
                         );



    Return(vPers_data);

    Exception
    When NO_DATA_FOUND then
        Return('Error Creating Pers_data');


End;




Procedure CYTO_JAK_BRIEF (
                                            p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
                                           ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
                                           ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
                                           ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
                                           ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
                                           ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
                                           ,p_best_id          IN   NUMBER   DEFAULT NULL
                                           , PCLOB OUT CLOB
  ) as

  v_xml_hoofdtag   VARCHAR2(100):= 'JAK_BRIEF';
  v_xml_doc          CLOB;

Begin
    Kgc_xml_00.init;

    v_xml_doc := kgc_xml_00.nieuwe_xml(V_Xml_hoofdtag);

    v_xml_doc := v_xml_doc||getpersoon('9000001');

    PCLOB := v_xml_doc ;

    kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);

 --  kgc_xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

End;


PROCEDURE Cyto_Form_meti
                        (       p_view_name        IN   KGC_BESTAND_TYPES.view_name%TYPE
                                ,p_where_clause     IN   VARCHAR2 DEFAULT NULL
                                ,p_ind_kopie        IN   VARCHAR2 DEFAULT 'N'
                                ,p_ind_brief        IN   VARCHAR2 DEFAULT 'N'
                                ,p_speciale_tekst   IN   VARCHAR2 DEFAULT NULL
                                ,p_destype          IN   VARCHAR2 DEFAULT 'PREVIEW'
                                ,p_best_id          IN   NUMBER   DEFAULT NULL
                                , PCLOB OUT CLOB
)
As
  v_xml_hoofdtag   VARCHAR2(100):= 'Form_Meting';
  v_xml_doc          CLOB;

  C_FMeti             Kgc_Bouwsteen_00.t_bouwsteen_cursor;

Begin
      Kgc_xml_00.init;
      v_xml_doc := kgc_xml_00.nieuwe_xml(V_Xml_hoofdtag);

       Open C_FMeti for  'SELECT * FROM ' || p_view_name || ' ' || p_where_clause;  --  Open Cursor
        If C_FMeti%notFound then
               Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'No Data'
                         ,p_xml_regel     => 'NO DATA FOUND'
                         );
        Else
               v_xml_doc := v_xml_doc||getpersoon('9000001');
        End If;





End;



Function testXML return CLOB as
PCLOB CLOB;
VLOB  CLOB;
Begin

     dbms_lob.createtemporary (PCLOB, FALSE);



      CYTO_JAK_BRIEF('','','','','','','',PCLOB);



      return(PCLOB);

End;


END BEH_XML2;
/

/
QUIT
