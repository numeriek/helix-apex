CREATE OR REPLACE PACKAGE "HELIX"."MSS_VIEW_HELPER" is

  -- Author  : RKON
  -- Created : 6-7-2009 15:48:56
  -- Purpose : Support functions for all Views Created by the Application Specialists at
  -- the AZM.

  Function ShowUsername(Puser varchar2) return varchar2;
  Function ShowFunctieUser(PUser varchar2) return varchar2;
  Function ShowAuditDate(POnde_id integer)Return varchar2;
  function MSS_INST_RELA(prela_id integer) return  varchar2;



end MSS_VIEW_HELPER;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."MSS_VIEW_HELPER" is


/* Created by  R Konings Applicatie specialist AZM.
   Historie wijzigingen

   Datum               Auteur       Commentaar

   31-07-2009          RKON         Toevoegen commentaar

*/
  Function ShowUsername(Puser varchar2) return varchar2
  as
    -- Haalt de naam van de mederwerker op aan de hand van de
    -- ingelogde Oracle User.
    Cursor C_User(OraUser in Varchar2)
    is Select Mede.Formele_Naam
       From kgc_medewerkers Mede
       where Mede.Oracle_Uid = OraUser;
    MedeNm varchar2(100);
  Begin
     Open C_user(PUser) ;
     Fetch C_User into MedeNm;
     Close C_User;

     if MedeNm is null then
        Return('ONBEKEND');
     Else
        Return(MedeNm);
     End if;

  End;

   Function ShowFunctieUser(PUser varchar2) return varchar2
   as
     -- Haalt de functie van de medewerker op, op basis van oracle
     -- UserID
     Cursor C_Functie(OraUser in varchar2)
     is
     Select func.omschrijving
     From kgc_medewerkers mede,
          kgc_functies func,
          kgc_mede_kafd MAFD
     where mede.mede_id = mafd.mede_id
     and   func.func_id = mafd.func_id
     and   mede.oracle_uid = OraUser;
   Functie varchar2(100);
   begin
     Open C_Functie(PUser) ;
     Fetch C_Functie into Functie;
     Close C_Functie;

     if Functie is null then
        Return('Medewerker Klinische Genetica');
     Else
        Return(Functie);
     End if;
   end;

  Function ShowAuditDate(POnde_id integer)Return varchar2
  as
  -- Haalt de datum op dat een onderzoek goedgekeurd is
  Cursor C_Au (onde_id integer) is
   Select to_char(au.datum_tijd,'DD-MM-YYYY') datum
   from kgc_audit_log au
   where au.tabel_naam = 'KGC_ONDERZOEKEN'
   and   AU.KOLOM_NAAM = 'STATUS'
   and   AU.id = ONDE_ID;
  Datum varchar2(10);
  begin
      Open   C_AU(PONDE_ID);
      Fetch C_Au into Datum;
      Close C_AU ;

      return(Datum);


  end;

  /* Deze Functie haalt de adres gegevens van een Arts op, dit kan op basis van de gegevens
    in de relatie tabel. Indien de gegevens in de relatie tabel leeg zijn, worden de gegevens van de
    instellingen opgehaald */
  Function MSS_INST_RELA(prela_id integer) return  varchar2
  as

    Cursor CRel  (prela_id  integer)is -- Gegevens relatie tabel
           Select  Rela.Aanspreken ||' '||CHR(10) Aansprek,
                   Rela.Adres||' '||CHR(10)||
                   Rela.Postcode||' '||
                   Rela.Woonplaats||' '||CHR(10)||
                   Decode(rela.land,'NEDERLAND','',rela.land ) Adres,
                   Rela.Adres adrchk,
                   rela.inst_id
           from kgc_relaties rela
           where rela.rela_id = prela_id ;

   V_Aansp   varchar2(1000);
   V_Adres   varchar2(1500);
   V_adruit  varchar2(2500);
   V_instId  integer;
   V_Adr_chk varchar2(250);

   Cursor CIns (pinst_id integer) is -- Gegevens Instellingen tabel
          Select inst.naam||' '||CHR(10)||
                 inst.adres||' '||CHR(10)||
                 inst.postcode||' '||
                 inst.woonplaats||' '||CHR(10)||
                 Decode(inst.land,'NEDERLAND','',inst.land ) Adres
          from kgc_instellingen inst
          where inst.inst_id = pinst_id ;


begin
   Open Crel(prela_id) ;
    Fetch Crel into V_aansp, V_adres,V_adr_chk ,V_instid ;
   Close Crel ;

   if trim(V_adr_chk) is null then
      Open CIns(v_instId);
        Fetch Cins into V_adres ;
      Close CIns;
      v_adruit := v_aansp||V_adres;

   else v_adruit := V_Aansp || V_adres     ;

   end if;

   return(v_adruit);

end MSS_INST_RELA;


end MSS_VIEW_HELPER;
/

/
QUIT
