CREATE OR REPLACE PACKAGE "HELIX"."KGC_FTP" IS
  -- RDE 29-03-2012
  --   Tbv. ACL de parameter p_servername toegevoegd.
  -- JKO 21-01-2010
  --   Tbv. CDM de functie ftpgettochar toegevoegd.
  -- RDE 14-08-2003
  --   Package vernieuwd: java wordt niet meer gebruikt!
  --   Naast 'normale' ftp-functies: ook FTPDir, FTPDelete en FTPGetToCLOB
  --   LET OP - put/get niet getest, want niet in gebruik!!!
  --   Basis was: http://www.oracle-base.com/dba/miscellaneous/ftp.pkb
  TYPE string_table IS
    TABLE OF VARCHAR2(32767);

  -- downloads the files whose name is listed in SourceFile, whose type is
  -- in FileTypeIsBinary, stored on the remote server ServerName with account
  -- determined by UserName and Password, and writes it into the local file
  -- whose name is in DestFile;
  -- file name syntax is that of the respective local and remote O/S;
  -- return an error message if an error occured, an empty string if successful;
  FUNCTION ftpget( p_servername       IN VARCHAR2
                 , p_port             IN VARCHAR2
                 , p_username         IN VARCHAR2
                 , p_password         IN VARCHAR2
                 , p_sourcefile       IN VARCHAR2
                 , p_filetypeisbinary IN BOOLEAN
                 , p_destdir          IN VARCHAR2
                 , p_destfile         IN VARCHAR2)
    RETURN STRING;

  -- uploads the local file whose name is listed in SourceFile and whose
  -- type is in FileTypeIsBinary, and writes it into the remote server
  -- ServerName with account determined by UserName and Password, as file
  -- whose name is in DestFile;
  -- file name syntax is that of the respective local and remote O/S;
  -- return an error message if an error occured, an empty string if successful;
  FUNCTION ftpput( p_servername       IN VARCHAR2
                 , p_port             IN VARCHAR2
                 , p_username         IN VARCHAR2
                 , p_password         IN VARCHAR2
                 , p_sourcedir        IN VARCHAR2
                 , p_sourcefile       IN VARCHAR2
                 , p_filetypeisbinary IN BOOLEAN
                 , p_destfile         IN VARCHAR2)
    RETURN STRING;

  -- retrieves into DirList a list of file entries in the remote
  -- directory RemoteDir with account determined by UserName and Password;
  -- return an error message if an error occured, an empty string if successful;
  FUNCTION ftpdir( p_servername       IN VARCHAR2
                 , p_port             IN VARCHAR2
                 , p_username         IN VARCHAR2
                 , p_password         IN VARCHAR2
                 , p_remotedir        IN VARCHAR2
                 , p_dirlist         OUT string_table)
    RETURN STRING;

  -- Verwijder file van FTP-server
  -- complete pad opgeven bij file!
  -- return an error message if an error occured, an empty string if successful;
  FUNCTION ftpdelete( p_servername       IN VARCHAR2
                    , p_port             IN VARCHAR2
                    , p_username         IN VARCHAR2
                    , p_password         IN VARCHAR2
                    , p_sourcefile       IN VARCHAR2)
    RETURN STRING;

  -- zet file in clob: kolom 1 in query
  FUNCTION ftpgettoclob( p_servername       IN VARCHAR2
                       , p_port             IN VARCHAR2
                       , p_username         IN VARCHAR2
                       , p_password         IN VARCHAR2
                       , p_sourcefile       IN VARCHAR2
                       , p_querytable       IN VARCHAR2
                       , p_querycolumn      IN VARCHAR2
                       , p_querywhere       IN VARCHAR2)
    RETURN STRING;
  -- voor compat. met oude versie
  FUNCTION ftpgettoclob( ServerName   IN STRING
                       , UserName     IN STRING
                       , Password     IN STRING
                       , SourceFile   IN STRING
                       , QueryTable   IN STRING
                       , QueryColumn  IN STRING
                       , QueryWhere   IN STRING)
    RETURN STRING;

  -- Reads the file whose name is listed in SourceFile, whose type is
  -- in ASCII, stored on the remote server ServerName with account
  -- determined by UserName and Password, and returns it into x_data;
  -- file name syntax is that of the remote O/S;
  -- return an error message if an error occured.
  FUNCTION ftpgettochar( p_servername  IN VARCHAR2
                       , p_port        IN VARCHAR2
                       , p_username    IN VARCHAR2
                       , p_password    IN VARCHAR2
                       , p_sourcefile  IN VARCHAR2
                       , x_data       OUT VARCHAR2)
    RETURN STRING;
END KGC_FTP;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FTP" IS
g_reply         string_table := string_table();
g_binary        BOOLEAN := TRUE;
g_debug         BOOLEAN := FALSE; -- zet op TRUE voor output-boodschappen!
g_convert_crlf  BOOLEAN := TRUE;

-- private helper functions;

----------------------------------------------------------------------------
PROCEDURE debug (p_text  IN  VARCHAR2) IS
----------------------------------------------------------------------------
BEGIN
  IF g_debug THEN
    DBMS_OUTPUT.put_line(SUBSTR(p_text, 1, 255));
  END IF;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE get_reply (p_conn  IN OUT NOCOPY  UTL_TCP.connection) IS
----------------------------------------------------------------------------
  l_reply_code  VARCHAR2(3) := NULL;
BEGIN
  -- terugkomende gegevens ophalen:
  LOOP
    g_reply.extend;
    g_reply(g_reply.last) := UTL_TCP.get_line(p_conn, TRUE);
    debug(g_reply(g_reply.last));
    IF l_reply_code IS NULL THEN
      l_reply_code := SUBSTR(g_reply(g_reply.last), 1, 3);
    END IF;
    IF SUBSTR(l_reply_code, 1, 1) = '5' THEN
      RAISE_APPLICATION_ERROR(-20000, g_reply(g_reply.last));
    ELSIF (SUBSTR(g_reply(g_reply.last), 1, 3) = l_reply_code AND
           SUBSTR(g_reply(g_reply.last), 4, 1) = ' ') THEN
      EXIT;
    END IF;
  END LOOP;
EXCEPTION
  WHEN UTL_TCP.END_OF_INPUT THEN
    NULL;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE send_command (p_conn     IN OUT NOCOPY  UTL_TCP.connection,
                        p_command  IN             VARCHAR2,
                        p_reply    IN             BOOLEAN := TRUE) IS
----------------------------------------------------------------------------
  v_result  PLS_INTEGER;
BEGIN
  v_result := UTL_TCP.write_line(p_conn, p_command);

  IF p_reply THEN
    get_reply(p_conn);
  END IF;
END;
----------------------------------------------------------------------------


----------------------------------------------------------------------------
-- onderstaande functie geeft aan dat de data (NIET de bevestiging
-- van PASV) naar een andere poort op de client moeten. In de reply zit deze poort.
-- bv: 227 Entering Passive Mode (10,14,0,24,8,190).
-- Er wordt voor deze poort een nieuwe connectie gemaakt.
----------------------------------------------------------------------------
FUNCTION get_passive (p_conn       IN OUT NOCOPY  UTL_TCP.connection,
                      p_servername IN             VARCHAR2)
  RETURN UTL_TCP.connection IS
----------------------------------------------------------------------------
  v_conn    UTL_TCP.connection;
  v_reply   VARCHAR2(32767);
  v_host    VARCHAR(100);
  v_port1   NUMBER(10);
  v_port2   NUMBER(10);
BEGIN
  send_command(p_conn, 'PASV');
  v_reply := g_reply(g_reply.last);

  v_reply := REPLACE(SUBSTR(v_reply, INSTR(v_reply, '(') + 1, (INSTR(v_reply, ')')) - (INSTR(v_reply, '('))-1), ',', '.');
--  v_host  := SUBSTR(v_reply, 1, INSTR(v_reply, '.', 1, 4)-1);
  -- mantis 6808
  -- niet doen! dit is een IP-adres, en indien p_servername bv VM010008 is, dan heb je voor zowel de naam als het ip-adres een ACL nodig
  -- daarnaast: het ip-adres kan maarzo wijzigen, en dan werkt ftp niet meer...
  v_host := p_servername;

  v_port1 := TO_NUMBER(SUBSTR(v_reply, INSTR(v_reply, '.', 1, 4)+1, (INSTR(v_reply, '.', 1, 5)-1) - (INSTR(v_reply, '.', 1, 4))));
  v_port2 := TO_NUMBER(SUBSTR(v_reply, INSTR(v_reply, '.', 1, 5)+1));

  v_conn := utl_tcp.open_connection(v_host, 256 * v_port1 + v_port2);
  return v_conn;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE ascii (p_conn  IN OUT NOCOPY  UTL_TCP.connection) AS
----------------------------------------------------------------------------
BEGIN
  send_command(p_conn, 'TYPE A', TRUE);
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE binary (p_conn  IN OUT NOCOPY  UTL_TCP.connection) AS
----------------------------------------------------------------------------
BEGIN
  send_command(p_conn, 'TYPE I', TRUE);
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE convert_crlf (p_status  IN  BOOLEAN) AS
----------------------------------------------------------------------------
BEGIN
  g_convert_crlf := p_status;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION login (p_host  IN  VARCHAR2,
                p_port  IN  VARCHAR2,
                p_user  IN  VARCHAR2,
                p_pass  IN  VARCHAR2)
  RETURN UTL_TCP.connection IS
----------------------------------------------------------------------------
  v_conn  UTL_TCP.connection;
BEGIN
  g_reply.delete;

  v_conn := UTL_TCP.open_connection(p_host, p_port);
  get_reply (v_conn);
  send_command(v_conn, 'USER ' || p_user);
  send_command(v_conn, 'PASS ' || p_pass);
  RETURN v_conn;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE logout(p_conn   IN OUT NOCOPY  UTL_TCP.connection,
                 p_reply  IN             BOOLEAN := TRUE) AS
----------------------------------------------------------------------------
BEGIN
  send_command(p_conn, 'QUIT', p_reply);
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION get_local_ascii_data (p_dir   IN  VARCHAR2,
                               p_file  IN  VARCHAR2)
  RETURN CLOB IS
----------------------------------------------------------------------------
  v_bfile   BFILE;
  v_data    CLOB;
BEGIN
  DBMS_LOB.createtemporary (lob_loc => v_data,
                            cache   => TRUE,
                            dur     => DBMS_LOB.call);

  v_bfile := BFILENAME(p_dir, p_file);
  DBMS_LOB.fileopen(v_bfile, DBMS_LOB.file_readonly);
  DBMS_LOB.loadfromfile(v_data, v_bfile, DBMS_LOB.getlength(v_bfile));
  DBMS_LOB.fileclose(v_bfile);

  RETURN v_data;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION get_local_binary_data (p_dir   IN  VARCHAR2,
                                p_file  IN  VARCHAR2)
  RETURN BLOB IS
----------------------------------------------------------------------------
  v_bfile   BFILE;
  v_data    BLOB;
BEGIN
  DBMS_LOB.createtemporary (lob_loc => v_data,
                            cache   => TRUE,
                            dur     => DBMS_LOB.call);

  v_bfile := BFILENAME(p_dir, p_file);
  DBMS_LOB.fileopen(v_bfile, DBMS_LOB.file_readonly);
  DBMS_LOB.loadfromfile(v_data, v_bfile, DBMS_LOB.getlength(v_bfile));
  DBMS_LOB.fileclose(v_bfile);

  RETURN v_data;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION get_remote_ascii_data (p_conn       IN OUT NOCOPY  UTL_TCP.connection,
                                p_servername IN             VARCHAR2,
                                p_file       IN             VARCHAR2)
  RETURN CLOB IS
----------------------------------------------------------------------------
  v_conn    UTL_TCP.connection;
  v_amount  PLS_INTEGER;
  v_buffer  VARCHAR2(32767);
  v_data    CLOB;
BEGIN
  DBMS_LOB.createtemporary (lob_loc => v_data,
                            cache   => TRUE,
                            dur     => DBMS_LOB.call);

  v_conn := get_passive(p_conn, p_servername);
  send_command(p_conn, 'RETR ' || p_file, TRUE);
  logout(v_conn, FALSE);

  BEGIN
    LOOP
      v_amount := UTL_TCP.read_text (v_conn, v_buffer, 32767);
      DBMS_LOB.writeappend(v_data, v_amount, v_buffer);
    END LOOP;
  EXCEPTION
    WHEN UTL_TCP.END_OF_INPUT THEN
      NULL;
    WHEN OTHERS THEN
      NULL;
  END;
  UTL_TCP.close_connection(v_conn);

  RETURN v_data;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION get_remote_binary_data (p_conn       IN OUT NOCOPY  UTL_TCP.connection,
                                 p_servername IN             VARCHAR2,
                                 p_file       IN             VARCHAR2)
  RETURN BLOB IS
----------------------------------------------------------------------------
  v_conn    UTL_TCP.connection;
  v_amount  PLS_INTEGER;
  v_buffer  RAW(32767);
  v_data    BLOB;
BEGIN
  DBMS_LOB.createtemporary (lob_loc => v_data,
                            cache   => TRUE,
                            dur     => DBMS_LOB.call);

  v_conn := get_passive(p_conn, p_servername);
  send_command(p_conn, 'RETR ' || p_file, TRUE);

  BEGIN
    LOOP
      v_amount := UTL_TCP.read_raw (v_conn, v_buffer, 32767);
      DBMS_LOB.writeappend(v_data, v_amount, v_buffer);
    END LOOP;
  EXCEPTION
    WHEN UTL_TCP.END_OF_INPUT THEN
      NULL;
    WHEN OTHERS THEN
      NULL;
  END;
  UTL_TCP.close_connection(v_conn);

  RETURN v_data;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE put_local_ascii_data (p_data  IN  CLOB,
                                p_dir   IN  VARCHAR2,
                                p_file  IN  VARCHAR2) IS
----------------------------------------------------------------------------
  v_out_file  UTL_FILE.file_type;
  v_buffer    VARCHAR2(32767);
  v_amount    BINARY_INTEGER := 32767;
  v_pos       INTEGER := 1;
  v_clob_len  INTEGER;
BEGIN
  v_clob_len := DBMS_LOB.getlength(p_data);

  v_out_file := UTL_FILE.fopen(p_dir, p_file, 'w', 32767);

  WHILE v_pos < v_clob_len LOOP
    DBMS_LOB.read (p_data, v_amount, v_pos, v_buffer);
    IF g_convert_crlf THEN
      v_buffer := REPLACE(v_buffer, CHR(13), NULL);
    END IF;

    UTL_FILE.put(v_out_file, v_buffer);
    UTL_FILE.fflush(v_out_file);
    v_pos := v_pos + v_amount;
  END LOOP;

  UTL_FILE.fclose(v_out_file);
EXCEPTION
  WHEN OTHERS THEN
    IF UTL_FILE.is_open(v_out_file) THEN
      UTL_FILE.fclose(v_out_file);
    END IF;
    RAISE;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE put_local_binary_data (p_data  IN  BLOB,
                                 p_dir   IN  VARCHAR2,
                                 p_file  IN  VARCHAR2) IS
----------------------------------------------------------------------------
  v_out_file  UTL_FILE.file_type;
  v_buffer    RAW(32767);
  v_amount    BINARY_INTEGER := 32767;
  v_pos       INTEGER := 1;
  v_blob_len  INTEGER;
BEGIN
  v_blob_len := DBMS_LOB.getlength(p_data);

  v_out_file := UTL_FILE.fopen(p_dir, p_file, 'w', 32767);

  WHILE v_pos < v_blob_len LOOP
    DBMS_LOB.read (p_data, v_amount, v_pos, v_buffer);
--    UTL_FILE.put_raw(v_out_file, v_buffer, TRUE);
-- gaf fout; en: nog niet nodig!
    UTL_FILE.fflush(v_out_file);
    v_pos := v_pos + v_amount;
  END LOOP;

  UTL_FILE.fclose(v_out_file);
EXCEPTION
  WHEN OTHERS THEN
    IF UTL_FILE.is_open(v_out_file) THEN
      UTL_FILE.fclose(v_out_file);
    END IF;
    RAISE;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE put_remote_ascii_data (p_conn       IN OUT NOCOPY  UTL_TCP.connection,
                                 p_servername IN             VARCHAR2,
                                 p_file       IN             VARCHAR2,
                                 p_data       IN             CLOB) IS
----------------------------------------------------------------------------
  v_conn      UTL_TCP.connection;
  v_result    PLS_INTEGER;
  v_buffer    VARCHAR2(32767);
  v_amount    BINARY_INTEGER := 32767;
  v_pos       INTEGER := 1;
  v_clob_len  INTEGER;
BEGIN
  v_conn := get_passive(p_conn, p_servername);
  send_command(p_conn, 'STOR ' || p_file, TRUE);

  v_clob_len := DBMS_LOB.getlength(p_data);

  WHILE v_pos < v_clob_len LOOP
    DBMS_LOB.READ (p_data, v_amount, v_pos, v_buffer);
    IF g_convert_crlf THEN
      v_buffer := REPLACE(v_buffer, CHR(13), NULL);
    END IF;
    v_result := UTL_TCP.write_text(v_conn, v_buffer, LENGTH(v_buffer));
    UTL_TCP.flush(v_conn);
    v_pos := v_pos + v_amount;
  END LOOP;
  UTL_TCP.close_connection(v_conn);
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE put_remote_binary_data (p_conn       IN OUT NOCOPY  UTL_TCP.connection,
                                  p_servername IN             VARCHAR2,
                                  p_file       IN             VARCHAR2,
                                  p_data       IN             BLOB) IS
----------------------------------------------------------------------------
  v_conn      UTL_TCP.connection;
  v_result    PLS_INTEGER;
  v_buffer    RAW(32767);
  v_amount    BINARY_INTEGER := 32767;
  v_pos       INTEGER := 1;
  v_blob_len  INTEGER;
BEGIN
  v_conn := get_passive(p_conn, p_servername);
  send_command(p_conn, 'STOR ' || p_file, TRUE);

  v_blob_len := DBMS_LOB.getlength(p_data);

  WHILE v_pos < v_blob_len LOOP
    DBMS_LOB.READ (p_data, v_amount, v_pos, v_buffer);
    v_result := UTL_TCP.write_raw(v_conn, v_buffer, v_amount);
    UTL_TCP.flush(v_conn);
    v_pos := v_pos + v_amount;
  END LOOP;
  UTL_TCP.close_connection(v_conn);
END;
----------------------------------------------------------------------------

-- public functions;
----------------------------------------------------------------------------
FUNCTION ftpget( p_servername       IN VARCHAR2
               , p_port             IN VARCHAR2
               , p_username         IN VARCHAR2
               , p_password         IN VARCHAR2
               , p_sourcefile       IN VARCHAR2
               , p_filetypeisbinary IN BOOLEAN
               , p_destdir          IN VARCHAR2
               , p_destfile         IN VARCHAR2)
  RETURN STRING IS
----------------------------------------------------------------------------
  v_conn  UTL_TCP.connection;
BEGIN
  g_reply.delete;
  v_conn := login(p_servername, p_port, p_username, p_password);

  IF p_filetypeisbinary THEN
    put_local_binary_data(p_data  => get_remote_binary_data (v_conn, p_servername, p_sourcefile),
                          p_dir   => p_destdir,
                          p_file  => p_destfile);
  ELSE
    put_local_ascii_data(p_data  => get_remote_ascii_data (v_conn, p_servername, p_sourcefile),
                         p_dir   => p_destdir,
                         p_file  => p_destfile);
  END IF;
  get_reply(v_conn);
  logout(v_conn);
  UTL_TCP.close_all_connections;
  RETURN '';
EXCEPTION
  WHEN OTHERS THEN
    RETURN 'Error in KGC_FTP.ftpget: ' || SQLERRM;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION ftpput( p_servername       IN VARCHAR2
               , p_port             IN VARCHAR2
               , p_username         IN VARCHAR2
               , p_password         IN VARCHAR2
               , p_sourcedir        IN VARCHAR2
               , p_sourcefile       IN VARCHAR2
               , p_filetypeisbinary IN BOOLEAN
               , p_destfile         IN VARCHAR2)
  RETURN STRING IS
----------------------------------------------------------------------------
  v_conn  UTL_TCP.connection;
BEGIN
  g_reply.delete;
  v_conn := login(p_servername, p_port, p_username, p_password);

  IF p_filetypeisbinary THEN
    put_remote_binary_data(p_conn => v_conn,
                           p_servername => p_servername,
                           p_file => p_destfile,
                           p_data => get_local_binary_data(p_sourcedir, p_sourcefile));
  ELSE
    put_remote_ascii_data(p_conn => v_conn,
                          p_servername => p_servername,
                          p_file => p_destfile,
                          p_data => get_local_ascii_data(p_sourcedir, p_sourcefile));
  END IF;
  get_reply(v_conn);
  logout(v_conn);
  UTL_TCP.close_all_connections;
  RETURN '';
EXCEPTION
  WHEN OTHERS THEN
    RETURN 'Error in KGC_FTP.ftpput: ' || SQLERRM;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION ftpdir( p_servername       IN VARCHAR2
               , p_port             IN VARCHAR2
               , p_username         IN VARCHAR2
               , p_password         IN VARCHAR2
               , p_remotedir        IN VARCHAR2
               , p_dirlist         OUT string_table)
  RETURN STRING IS
----------------------------------------------------------------------------
  v_conn  UTL_TCP.connection;
  v_conn_p  UTL_TCP.connection;
  v_amount  PLS_INTEGER;
  v_buffer  VARCHAR2(32767);
  v_lf      VARCHAR2(1) := CHR(10);
  v_pos_lf  NUMBER;
  v_pos_start NUMBER;
  v_data_available  pls_integer;
BEGIN
  g_reply.delete;
  v_conn := login(p_servername, p_port, p_username, p_password);

  send_command(v_conn, 'CWD ' || p_remotedir);
  v_conn_p := get_passive(v_conn, p_servername); -- de data komt binnen via v_conn_p!
  send_command(v_conn, 'LIST');

  p_dirlist := string_table();
  BEGIN
    debug('Starting tcp-Loop');
    LOOP
      p_dirlist.extend;
      p_dirlist(p_dirlist.last) := UTL_TCP.get_line(v_conn_p, TRUE);
      debug(p_dirlist(p_dirlist.last));
    END LOOP;
  EXCEPTION
    WHEN UTL_TCP.END_OF_INPUT THEN
      NULL;
    WHEN OTHERS THEN
      NULL;
  END;
  debug('End tcp-Loop');

  logout(v_conn);
  debug('logged-out v_conn');
  UTL_TCP.close_all_connections;
  debug('logged-out all');
  RETURN '';
EXCEPTION
  WHEN OTHERS THEN
    RETURN 'Error in KGC_FTP.ftpdir: ' || SQLERRM;
END FTPDir;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION ftpdelete( p_servername       IN VARCHAR2
                  , p_port             IN VARCHAR2
                  , p_username         IN VARCHAR2
                  , p_password         IN VARCHAR2
                  , p_sourcefile       IN VARCHAR2)
  RETURN STRING IS
----------------------------------------------------------------------------
  v_conn  UTL_TCP.connection;
BEGIN
  g_reply.delete;
  v_conn := login(p_servername, p_port, p_username, p_password);

  send_command(v_conn, 'DELE ' || p_sourcefile);

  logout(v_conn);
  UTL_TCP.close_all_connections;
  RETURN '';
EXCEPTION
  WHEN OTHERS THEN
    RETURN 'Error in KGC_FTP.ftpdelete: ' || SQLERRM;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION ftpgettoclob( p_servername       IN VARCHAR2
                     , p_port             IN VARCHAR2
                     , p_username         IN VARCHAR2
                     , p_password         IN VARCHAR2
                     , p_sourcefile       IN VARCHAR2
                     , p_querytable       IN VARCHAR2 -- bv 'KGC_IMPORT_FILES'
                     , p_querycolumn      IN VARCHAR2 -- bv 'INHOUD'
                     , p_querywhere       IN VARCHAR2) -- bv 'WHERE IMFI_ID = ' || TO_CHAR(v_imfi_id))
  RETURN STRING IS
----------------------------------------------------------------------------
  v_conn    UTL_TCP.connection;
  v_count   NUMBER;
  v_clob    CLOB;
  v_clob_db CLOB;
  v_sql     VARCHAR2(2000);
  v_result  VARCHAR2(2000);
BEGIN
  g_reply.delete;

  v_conn := login(p_servername, p_port, p_username, p_password);
  ascii(p_conn => v_conn);
  v_clob := get_remote_ascii_data(v_conn, p_servername, p_sourcefile);
  -- de rij in de db moet al bestaan!
  v_sql := 'select count(*) into :result from ' || p_querytable || ' ' || p_querywhere;
  v_result := kgc_util_00.dyn_exec(v_sql);
  IF to_number(v_result) <> 1 THEN
    RAISE_APPLICATION_ERROR(-20000, 'Query ''' || v_sql || ''' levert niet precies 1 rij!');
  END IF;
  debug('leeg maken');
  v_sql := 'update ' || p_querytable || ' set '
    || p_querycolumn || ' = empty_clob() '
    || p_querywhere;
  kgc_util_00.dyn_exec(v_sql);

  debug('vullen');
  v_sql := 'select ' || p_querycolumn || ' from '
    || p_querytable || ' ' || p_querywhere || ' for update';
  EXECUTE IMMEDIATE v_sql
  INTO v_clob_db;
  debug('clob_db: ' || dbms_lob.substr(v_clob_db, 100));
  debug('clob_: ' || dbms_lob.substr(v_clob, 100));
  DBMS_LOB.copy(v_clob_db, v_clob, DBMS_LOB.getlength(v_clob));
  commit;

  debug('klaar');
  logout(v_conn);
  UTL_TCP.close_all_connections;
  debug('free clob');
  DBMS_LOB.freetemporary(lob_loc => v_clob);
  RETURN '';
EXCEPTION
  WHEN OTHERS THEN
    RETURN 'Error in KGC_FTP.ftpgettoclob: ' || SQLERRM;
    ROLLBACK;
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION ftpgettoclob( ServerName   IN STRING
                     , UserName     IN STRING
                     , Password     IN STRING
                     , SourceFile   IN STRING
                     , QueryTable   IN STRING
                     , QueryColumn  IN STRING
                     , QueryWhere   IN STRING)
  RETURN STRING IS
----------------------------------------------------------------------------
BEGIN
  RETURN ftpgettoclob( ServerName
                     , '21'
                     , UserName
                     , Password
                     , SourceFile
                     , QueryTable
                     , QueryColumn
                     , QueryColumn);
END;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
FUNCTION ftpgettochar(p_servername   IN VARCHAR2
                     , p_port        IN VARCHAR2
                     , p_username    IN VARCHAR2
                     , p_password    IN VARCHAR2
                     , p_sourcefile  IN VARCHAR2
                     , x_data       OUT VARCHAR2)
  RETURN STRING IS
----------------------------------------------------------------------------
  v_conn    UTL_TCP.connection;
  v_count   NUMBER;
  v_clob    CLOB;

  v_clob_len  INTEGER;
  v_amount    BINARY_INTEGER := 32767;
  v_pos       INTEGER := 1;
  v_buffer    VARCHAR2(32767) := NULL;

  e_overflow  EXCEPTION;
BEGIN
  x_data   := NULL;

  g_reply.delete;

  v_conn := login(p_servername, p_port, p_username, p_password);
  ascii(p_conn => v_conn);
  v_clob := get_remote_ascii_data(v_conn, p_servername, p_sourcefile);
  debug('v_clob: ' || dbms_lob.substr(v_clob, 100));

  -- Alleen de eerste 32K van de file kan worden overgezet --> max. lengte van VARCHAR2 is 32K!
  v_clob_len := DBMS_LOB.getlength(v_clob);
  IF v_clob_len <= 32767
  THEN
    DBMS_LOB.read (v_clob, v_amount, v_pos, v_buffer);
    debug('v_buffer: ' || substr(v_buffer, 100));
  ELSE
    RAISE e_overflow;
  END IF;

  debug('klaar');
  logout(v_conn);
  UTL_TCP.close_all_connections;
  debug('free clob');
  DBMS_LOB.freetemporary(lob_loc => v_clob);
  x_data := v_buffer;
  RETURN '';
EXCEPTION
  WHEN e_overflow THEN
    RETURN 'Error in KGC_FTP.ftpgettochar: File "' || p_sourcefile || '" cannot be processed, Filesize > 32K';
  WHEN OTHERS THEN
    RETURN 'Error in KGC_FTP.ftpgettochar: File "' || p_sourcefile || '" ' || SQLERRM;
END;
----------------------------------------------------------------------------
END KGC_FTP;
/

/
QUIT
