CREATE OR REPLACE PACKAGE "HELIX"."KGC_UTIL_00" IS

-- datum van vandaag ophalen bij een update
FUNCTION vandaag
RETURN DATE
;
-- execute some dynamic sql
FUNCTION dyn_exec
( p_statement IN VARCHAR2
)
RETURN VARCHAR2
;
-- call previous function
PROCEDURE dyn_exec
( p_statement IN VARCHAR2
)
;
-- creeer (public) synonyms voor de objecten
-- NOOT: de oracle-user die deze procedure uitvoerd moet direct
-- "grant public synonym" privilege hebben (dus niet via een role!)
-- Denk aan views zonder "with grant option": HST503.QMS_MODULES gebruikt in KGC_MODULES_VW.
PROCEDURE create_synonyms
(  p_object IN VARCHAR2
)
;
-- verleen alle benodigde rechten op de objecten
-- NOOT: de oracle-user die deze procedure uitvoerd moet direct
-- "grant any privilege" privilege hebben (dus niet via een role!)
PROCEDURE create_grants
(  p_object IN VARCHAR2
)
;
-- verleen rechten en (re-)creeer synoniemen op de objecten
PROCEDURE reconcile
( p_object IN VARCHAR2
)
;

-- creeer een before-row trigger (IUD) voor de tabellen
-- waarin autorisatie (toegang) op tabel wordt gecontroleerd
-- NOOT: de oracle-user die deze procedure uitvoerd moet direct
-- "grant any trigger" privilege hebben (dus niet via een role!)
PROCEDURE create_aut_triggers
(  p_table IN VARCHAR2
)
;

-- Doe het allemaal (autorisatie-triggers, synonyms en grants)
PROCEDURE init;

-- vertaal interne id naar iets begrijpelijks (PK->UK)
-- eventuele opgegeven kolommen gescheiden door || of , of ; of spatie
FUNCTION pk2uk
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_kolommen IN VARCHAR2 := NULL
)
RETURN VARCHAR2
;
PROCEDURE pk2uk
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_kolommen IN VARCHAR2 := NULL
)
;
-- tel het aantal voorkomens van een teken in een string
FUNCTION num_of_occur
( p_string IN VARCHAR2
, p_character IN VARCHAR2
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( num_of_occur, WNDS, WNPS, RNDS, RNPS );

-- return tekst tussen tags of ontdoe een string van een overbodig deel
FUNCTION strip
( p_string IN VARCHAR2
, p_tag_va IN VARCHAR2
, p_tag_tm IN VARCHAR2
, p_rest IN VARCHAR2 := 'N' -- N=return deel tussen characters; J=return rest
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( strip, WNDS, WNPS, RNDS, RNPS );

-- soort if-then-else
FUNCTION ite
( p_voorwaarde IN BOOLEAN
, p_then IN VARCHAR2
, p_else IN VARCHAR2
)
RETURN VARCHAR2;

-- zoek alias bij een tabel (via PK)
FUNCTION tabel_alias
( p_tabelnaam IN VARCHAR2
, p_owner     IN VARCHAR2 := NULL
)
RETURN VARCHAR2;

-- eigenaar van de helix-objecten
FUNCTION appl_owner
RETURN VARCHAR2
;

-- omsluit een string met tags
FUNCTION tag
( p_string IN VARCHAR2
, p_tag_va IN VARCHAR2
, p_tag_tm IN VARCHAR2
)
RETURN VARCHAR2;
FUNCTION tag
( p_string IN VARCHAR2
, p_tag IN VARCHAR2
)
RETURN VARCHAR2;
FUNCTION multi_replace
( p_string        IN VARCHAR2
, p_te_vervangen  IN VARCHAR2
, p_vervanging    IN VARCHAR2
)
RETURN VARCHAR2;

FUNCTION is_number
(p_string   IN VARCHAR2
)
RETURN VARCHAR2;

FUNCTION kolom_lijst
( p_kolom           VARCHAR2
, p_tabel           VARCHAR2
, p_where           VARCHAR2 := null
, p_kolom_is_number VARCHAR2 := 'J'
)
RETURN VARCHAR2;

-- 4 functies retourneren het deel van een getal (al dan niet als char binnenkomend) voor of na de decimale punt
function voor_deci_punt
( p_waarde in varchar2
)
return varchar2;
function voor_deci_punt
( p_waarde in number
)
return varchar2;
function na_deci_punt
( p_waarde in varchar2
)
return varchar2;
function na_deci_punt
( p_waarde in number
)
return varchar2;

--  is er verschil tussen de twee strings?
function verschil
( p_1 in varchar2
, p_2 in varchar2
)
return boolean;
END KGC_UTIL_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_UTIL_00" IS

-- lokale function retourneert het decimale teken: punt of komma
function decimaal_teken
return varchar2
is
begin
  return( substr( to_char(1/2,'9D9'), -2, 1 ) );
end decimaal_teken;

FUNCTION vandaag
RETURN DATE
IS
BEGIN
  RETURN (trunc(sysdate));
END vandaag;

FUNCTION dyn_exec
( p_statement IN VARCHAR2 )
RETURN VARCHAR2
IS
  -- remove <CR>, spaces and <CR> again (if statement started off with spaces)...
  v_chk_statement VARCHAR2(32767) := LTRIM( LTRIM( LTRIM( p_statement, CHR(10)
                                                        )
                                                 , CHR(10)
                                                 )
                                          );
BEGIN
  DECLARE
    cid INTEGER;
    rows_processed INTEGER;
    v_result VARCHAR2(2000);
    v_statement VARCHAR2(32767) := NULL;
    v_with_bind_var BOOLEAN := TRUE;
  BEGIN
     v_statement := v_chk_statement;
     -- v_chk_statement is now only used to find strings in...
     v_chk_statement := UPPER( v_chk_statement );
     -- Analyze the incoming statement ...
     IF ( INSTR( v_chk_statement
               , 'BEGIN'
               ) = 1 )
     THEN -- an anonymous PL/SQL-block
       v_statement := v_statement; -- no change
     ELSIF ( INSTR( v_chk_statement
               , 'DECLARE'
               ) = 1 )
     THEN -- an anonymous PL/SQL-block
       v_statement := v_statement; -- no change
     ELSIF ( INSTR( v_chk_statement
               , 'CREATE'
               ) = 1 )
     THEN -- creatie van een trigger
       v_statement := v_statement; -- no change
     ELSIF ( INSTR( v_chk_statement
                  , 'SELECT'
                  ) = 1 )
     THEN -- a select statement
       IF ( INSTR( v_chk_statement
                 , 'INTO'
                 ) > 0
           )
       THEN -- a select statement with into
         v_statement := SUBSTR( v_statement
                              , 1
                              , INSTR( UPPER(v_statement), 'INTO' ) - 1
                              )
                     || CHR(10)||'into :result ' || CHR(10)
                     || SUBSTR( v_statement
                              , INSTR( UPPER(v_statement), 'FROM ' )
                              )
                      ;
       ELSE -- a select statement without into
         v_statement := SUBSTR( v_statement
                              , 1
                              , INSTR( UPPER(v_statement), 'FROM ' ) - 1
                              )
                     || CHR(10)||'into :result ' || CHR(10)
                     || SUBSTR( v_statement
                              , INSTR( UPPER(v_statement), 'FROM ' )
                              )
                      ;
       END IF;
       v_statement := 'begin ' || CHR(10)
                   || v_statement || ';' || CHR(10)
                   || 'exception when zero_divide then raise;' || CHR(10)
                   || 'when others then :result := null;' || CHR(10)
                   || 'end;'
                    ;
     ELSE -- could be anything, most likely a dml-statement
       v_statement := RTRIM( v_statement, ';' );
     END IF;
     IF ( INSTR( UPPER( v_statement )
               , ':RESULT'
               ) > 0 )
     THEN
       v_with_bind_var := TRUE;
     ELSE
       v_with_bind_var := FALSE;
     END IF;
     /* open new cursor and return cursor id. */
     cid := dbms_sql.open_cursor;
     /* parse string */
     dbms_sql.parse( cid
                   , v_statement
                   , dbms_sql.native
                   );
          IF ( v_with_bind_var )
     THEN
       /* bind variables */
       /* do not forget the max length of the result !!! */
       dbms_sql.bind_variable( cid
                             , ':result'
                             , v_result
                             , 2000 );
     END IF;
     /* execute the parsed statement */
     rows_processed := dbms_sql.execute( cid );
     IF ( v_with_bind_var )
     THEN
       /* retrieve the value */
       dbms_sql.variable_value( cid
                              , ':result'
                              , v_result );
     END IF;
     /* close cursor. */
     dbms_sql.close_cursor( cid );
     RETURN( v_result );
  EXCEPTION
    WHEN OTHERS
    THEN
      IF ( dbms_sql.is_open( cid ) )
      THEN
        dbms_sql.close_cursor( cid );
      END IF;
      raise_application_error
      ( -20000
      , 'Error in ' || v_statement || CHR(10) || CHR(10)
     || SQLERRM
      );
  END ;
EXCEPTION
  WHEN OTHERS THEN
    IF (dbms_utility.format_error_stack IS NULL)
    THEN
      raise_application_error
      ( -20000
      , 'Dynamic Exec Error: ' || SQLERRM );
    ELSE
      RAISE;
    END IF;
END dyn_exec;
PROCEDURE dyn_exec
( p_statement IN VARCHAR2
)
IS
  v_dummy VARCHAR2(100);
BEGIN
  v_dummy := dyn_exec( p_statement );
END  dyn_exec;

PROCEDURE create_synonyms
(  p_object IN VARCHAR2
)
IS
  CURSOR obj_cur
  IS
    SELECT object_name
    FROM   user_objects
    WHERE  object_name LIKE NVL( UPPER(p_object), '%' )
    AND    object_type IN ( 'TABLE', 'VIEW', 'SEQUENCE', 'PACKAGE', 'PROCEDURE', 'FUNCTION' )
    ;
  v_statement VARCHAR2(4000);
BEGIN
  FOR r IN obj_cur
  LOOP
    v_statement := 'CREATE PUBLIC SYNONYM '||r.object_name||' FOR '||USER||'.'||r.object_name
                 ;
    BEGIN
      dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END LOOP;
END create_synonyms;

PROCEDURE create_grants
(  p_object IN VARCHAR2
)
IS
  CURSOR obj_cur
  IS
    SELECT object_name
    ,      object_type
    FROM   user_objects
    WHERE  object_name LIKE NVL( UPPER(p_object), '%' )
    AND    object_type IN ( 'TABLE', 'VIEW', 'SEQUENCE', 'PACKAGE', 'PROCEDURE', 'FUNCTION' )
    ;
  v_statement VARCHAR2(4000);
  v_grants VARCHAR2(200);
BEGIN
  FOR r IN obj_cur
  LOOP
    IF ( r.object_type = 'TABLE' )
    THEN
      v_grants := 'SELECT,INSERT,UPDATE,DELETE';
    ELSIF ( r.object_type IN ( 'PACKAGE', 'PROCEDURE', 'FUNCTION' ) )
    THEN
      v_grants := 'EXECUTE';
    ELSE
      v_grants := 'SELECT';
    END IF;
    v_statement := 'GRANT '||v_grants||' ON '||r.object_name||' TO PUBLIC;'
                 ;
    BEGIN
      dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END LOOP;
END create_grants;

PROCEDURE reconcile
( p_object IN VARCHAR2
)
IS
BEGIN
  create_grants( p_object => p_object );
  create_synonyms( p_object => p_object );
  create_aut_triggers( p_table => p_object );
END reconcile;

PROCEDURE create_aut_triggers
(  p_table IN VARCHAR2
)
IS
  CURSOR ttab_cur
  IS
    SELECT table_name
    FROM   user_tables
    WHERE  table_name LIKE NVL( UPPER(p_table), '%' )
    ;
  CURSOR tcol_cur
  ( p_table_name IN VARCHAR2
  )
  IS
    SELECT column_name
    FROM   user_tab_columns
    WHERE  table_name = p_table_name
    AND    column_name LIKE '%KAFD_ID%'
    ORDER BY DECODE( column_name
                   , 'KAFD_ID', 1
                   , 2
                   )
    ;
  tcol_rec tcol_cur%rowtype;
  v_kafd VARCHAR2(100);
  v_statement VARCHAR2(4000);
BEGIN
  FOR r IN ttab_cur
  LOOP
    v_kafd := NULL;
    -- vanwege mutating table probleem worden tabellen die gebruikt worden in kgc_tato_00 apart behandeld
    IF ( r.table_name IN ( 'KGC_TABEL_TOEGANG'
                         , 'KGC_MEDEWERKERS'
                         , 'KGC_MEDE_KAFD'
                         , 'KGC_SYSTEEM_PARAMETERS'
                         , 'KGC_SYSTEEM_PAR_WAARDEN'
                         )
      )
    THEN
      v_statement := 'CREATE OR REPLACE TRIGGER '||SUBSTR(r.table_name,1,24)||'_A_TRG'
         ||CHR(10)|| 'BEFORE INSERT'
         ||CHR(10)|| 'ON '||r.table_name||' FOR EACH ROW'
         ||CHR(10)|| 'DECLARE v_table varchar2(30) := '''||r.table_name||''';'
         ||CHR(10)|| 'BEGIN'
         ||CHR(10)|| 'kgc_tato_00.toegang( v_table, ''I'');'
         ||CHR(10)|| 'END;'
                   ;
    ELSE
      tcol_rec.column_name := NULL;
      OPEN  tcol_cur( p_table_name => r.table_name );
      FETCH tcol_cur
      INTO  tcol_rec;
      CLOSE tcol_cur;
      IF ( tcol_rec.column_name IS NOT NULL )
      THEN
        v_kafd := ', p_kafd_id => nvl(:new.'||tcol_rec.column_name
                               ||   ',:old.'||tcol_rec.column_name
                               ||   ')';
      END IF;
      v_statement := 'CREATE OR REPLACE TRIGGER '||SUBSTR(r.table_name,1,24)||'_A_TRG'
         ||CHR(10)|| 'BEFORE INSERT OR UPDATE OR DELETE'
         ||CHR(10)|| 'ON '||r.table_name||' FOR EACH ROW'
         ||CHR(10)|| 'DECLARE v_table varchar2(30) := '''||r.table_name||''';'
         ||CHR(10)|| 'BEGIN'
         ||CHR(10)|| 'IF (INSERTING ) THEN kgc_tato_00.toegang( v_table, ''I'''||v_kafd||');'
         ||CHR(10)|| 'ELSIF (UPDATING ) THEN kgc_tato_00.toegang( v_table, ''U'''||v_kafd||');'
         ||CHR(10)|| 'ELSIF (DELETING ) THEN kgc_tato_00.toegang( v_table, ''D'''||v_kafd||');'
         ||CHR(10)|| 'END IF;'
         ||CHR(10)|| 'END;'
                   ;
    END IF;
    dyn_exec( v_statement );
  END LOOP;
END create_aut_triggers;

PROCEDURE init
IS
BEGIN
  create_aut_triggers( 'KGC%' );
  create_aut_triggers( 'BAS%' );
  reconcile( 'QMS%' );
  reconcile( 'HIL%' );
  reconcile( 'KGC%' );
  reconcile( 'BAS%' );
  reconcile( 'ENZ%' );
  reconcile( 'DNA%' );
  reconcile( 'CG$%' );
  reconcile( 'DIA%' );
  reconcile( 'CIN%' );
  reconcile( 'DU%' );
END;

FUNCTION pk2uk
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_kolommen IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(250);
  v_owner VARCHAR2(30) := kgc_sypa_00.standaard_waarde
                          ( p_parameter_code => 'APPLICATIE_EIGENAAR'
                          , p_kafd_id => NULL
                          );
  v_pk_kolom VARCHAR2(30) := UPPER(p_entiteit)||'_ID';
  v_kolommen VARCHAR2(1000) := UPPER( p_kolommen );
  v_statement VARCHAR2(32767);
  -- bepaal tabel
  CURSOR ccol_pk_cur
  ( b_column_name IN VARCHAR2
  )
  IS
    SELECT cons.table_name
    FROM   all_constraints cons
    ,      all_cons_columns ccol
    WHERE  cons.constraint_name = ccol.constraint_name
    AND    cons.table_name = ccol.table_name
    AND    ccol.owner = v_owner
    AND    cons.constraint_type = 'P'
    AND    ccol.column_name = b_column_name
    ;
  ccol_pk_rec ccol_pk_cur%rowtype;
  -- bepaal uk
  CURSOR cons_uk_cur
  ( b_table_name IN VARCHAR2
  )
  IS
    SELECT constraint_name
    FROM   all_constraints
    WHERE  owner = v_owner
    AND    table_name = b_table_name
    AND    constraint_type = 'U'
    ;
  cons_uk_rec cons_uk_cur%rowtype;
  -- bepaal uk kolommen
  CURSOR ccol_uk_cur
  ( b_constraint_name IN VARCHAR2
  )
  IS
    SELECT column_name
    FROM   all_cons_columns
    WHERE  owner = v_owner
    AND    constraint_name = b_constraint_name
    ORDER BY position
    ;
  -- bepaal een (zoek)index, niet PK, UK, FK
  CURSOR indx_cur
  ( b_table_name IN VARCHAR2
  )
  IS
    SELECT index_name
    FROM   all_indexes
    WHERE  table_owner = v_owner
    AND    table_name = b_table_name
    AND    uniqueness = 'NONUNIQUE'
    AND    INSTR(index_name, '_PK' ) = 0
    AND    INSTR(index_name, '_UK' ) = 0
    AND    INSTR(index_name, '_FK' ) = 0
    ORDER BY DECODE( INSTR( index_name, 'NAAM' )
                   , 0, DECODE( INSTR( index_name, 'OMS' )
                              , 0, 9
                              , 2
                              )
                   , 1
                   )
    ;
  indx_rec indx_cur%rowtype;
  -- bepaal index kolommen
  CURSOR icol_cur
  ( b_index_name IN VARCHAR2
  )
  IS
    SELECT column_name
    FROM   all_ind_columns
    WHERE  table_owner = v_owner
    AND    index_name = b_index_name
    ORDER BY column_position
    ;
BEGIN
  IF p_id IS NULL
  THEN
    RETURN( NULL );
  END IF;

  OPEN  ccol_pk_cur( v_pk_kolom );
  FETCH ccol_pk_cur
  INTO  ccol_pk_rec;
  CLOSE ccol_pk_cur;
  IF ( ccol_pk_rec.table_name IS NULL )
  THEN
    raise_application_error( -20009, 'KGC_UTIL_00.PK2UK: Geen primary key bij entiteit '||UPPER(p_entiteit)
                                     ||CHR(10)||'of kolom '||UPPER(p_entiteit)||'_ID'||' onbekend'
                           );
  END IF;
  -- bepaal kolommen
  IF ( p_kolommen IS NULL )
  THEN
    OPEN  cons_uk_cur( ccol_pk_rec.table_name );
    FETCH cons_uk_cur
    INTO  cons_uk_rec;
    CLOSE cons_uk_cur;
    IF ( cons_uk_rec.constraint_name IS NULL )
    THEN
      -- probeer een index...
      OPEN  indx_cur( ccol_pk_rec.table_name );
      FETCH indx_cur
      INTO  indx_rec;
      CLOSE indx_cur;
      IF ( indx_rec.index_name IS NULL )
      THEN
        raise_application_error( -20009, 'KGC_UTIL_00.PK2UK: Geen unique key of non-unique index bij tabel '||ccol_pk_rec.table_name
                               );
      END IF;
    END IF;
    v_statement := 'SELECT SUBSTR(';
    IF ( cons_uk_rec.constraint_name IS NOT NULL )
    THEN
      FOR r IN ccol_uk_cur( cons_uk_rec.constraint_name )
      LOOP
        IF ( ccol_uk_cur%rowcount = 1 )
        THEN
          v_statement := v_statement||r.column_name;
        ELSE
          v_statement := v_statement||'||'' ''||'||r.column_name;
        END IF;
      END LOOP;
    ELSIF ( indx_rec.index_name IS NOT NULL )
    THEN
      FOR r IN icol_cur( indx_rec.index_name )
      LOOP
        IF ( icol_cur%rowcount = 1 )
        THEN
          v_statement := v_statement||r.column_name;
        ELSE
          v_statement := v_statement||'||'' ''||'||r.column_name;
        END IF;
      END LOOP;
    END IF;
  ELSE
    -- mogelijke scheidingstekens naar ;
    v_kolommen := REPLACE(v_kolommen,',',';');
    v_kolommen := REPLACE(v_kolommen,'||',';');
    v_kolommen := REPLACE(v_kolommen,'|',';');
    v_kolommen := REPLACE(v_kolommen,';;',';');
    v_kolommen := REPLACE(v_kolommen,' ',';');

    v_statement := 'SELECT SUBSTR('||REPLACE(v_kolommen,';','||'' ''||');
  END IF;
  v_statement := v_statement||',1,250) FROM '||ccol_pk_rec.table_name;
  v_statement := v_statement||' WHERE '||v_pk_kolom||' = '||TO_CHAR( p_id );
  BEGIN
    v_return := kgc_util_00.dyn_exec( v_statement ) ;
  EXCEPTION
    WHEN OTHERS
    THEN
      raise_application_error( -20009, 'KGC_UTIL_00.PK2UK: Fout in '||CHR(10)||v_statement
                             );
  END;
  IF ( v_return IS NULL )
  THEN
    raise_application_error( -20009, 'KGC_UTIL_00.PK2UK: Geen rij in tabel '||ccol_pk_rec.table_name||' bij '||v_pk_kolom||' = '||TO_CHAR( p_id )
                                     ||CHR(10)||
                                     v_statement
                           );
  END IF;
  RETURN( v_return );
END pk2uk;

PROCEDURE pk2uk
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_kolommen IN VARCHAR2 := NULL
)
IS
BEGIN
  dbms_output.put_line( pk2uk
                        ( p_entiteit => p_entiteit
                        , p_id => p_id
                        , p_kolommen => p_kolommen
                        )
                      );
END pk2uk;

FUNCTION num_of_occur
( p_string IN VARCHAR2
, p_character IN VARCHAR2
)
RETURN NUMBER
IS
  v_return NUMBER := 0;
BEGIN
  v_return := NVL( LENGTH( p_string ), 0 )
            - NVL( LENGTH( REPLACE( p_string, p_character, NULL ) ), 0 )
            ;
  RETURN( v_return );
END num_of_occur;

FUNCTION strip
( p_string IN VARCHAR2
, p_tag_va IN VARCHAR2
, p_tag_tm IN VARCHAR2
, p_rest IN VARCHAR2 := 'N' -- N=return deel tussen characters; J=return rest
)
RETURN VARCHAR2
IS

-- Mantis 1522 lengte van 2 onderstaande verden vergroot van 4000 naar 32K
  v_return VARCHAR2(32767);
  v_tussen_tags VARCHAR2(32767) := p_string;
  v_pos_va NUMBER;
  v_pos_tm NUMBER;
BEGIN
  v_pos_va := INSTR( p_string, p_tag_va, 1, 1 ) + LENGTH( p_tag_va ) - 1;
  v_pos_tm := INSTR( p_string, p_tag_tm, v_pos_va + 1, 1 );
  IF ( v_pos_va > 0
   AND v_pos_tm > v_pos_va
     )
  THEN
    v_tussen_tags := SUBSTR( v_tussen_tags
                           , v_pos_va + 1
                           , v_pos_tm - v_pos_va - 1
                           );
    IF ( p_rest = 'J' )
    THEN
      v_return := REPLACE( p_string, p_tag_va||v_tussen_tags||p_tag_tm, NULL );
    ELSE
      v_return := v_tussen_tags;
    END IF;
  ELSE
    IF ( p_rest = 'J' )
    THEN
      v_return := p_string;
    ELSE
      v_return := NULL;
    END IF;
  END IF;
  RETURN( LTRIM( RTRIM( v_return ) ) );
END strip;

FUNCTION ite
( p_voorwaarde IN BOOLEAN
, p_then IN VARCHAR2
, p_else IN VARCHAR2
)
RETURN VARCHAR2
IS
BEGIN
  IF ( p_voorwaarde )
  THEN
    RETURN( p_then );
  ELSE
    RETURN( p_else );
  END IF;
END ite;

FUNCTION tabel_alias
( p_tabelnaam IN VARCHAR2
, p_owner     IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_owner VARCHAR2(30);
  v_alias VARCHAR2(10);
  CURSOR pk_cur
  IS
    SELECT SUBSTR(ccol.column_name,1,4) alias
    FROM   all_cons_columns ccol
    ,      all_constraints cons
    WHERE  cons.owner = ccol.owner
    AND    cons.table_name = ccol.table_name
    AND    cons.constraint_name = ccol.constraint_name
    AND    cons.owner = v_owner
    AND    cons.table_name = UPPER(p_tabelnaam)
    AND    cons.constraint_type = 'P'
    ;
BEGIN
  IF ( p_owner IS NOT NULL )
  THEN
    v_owner := p_owner;
  ELSE
    v_owner := appl_owner;
  END IF;
  v_owner := UPPER( v_owner );
  IF ( p_tabelnaam IS NOT NULL )
  THEN
    OPEN  pk_cur;
    FETCH pk_cur
    INTO  v_alias;
    CLOSE pk_cur;
  END IF;
  RETURN( v_alias );
END tabel_alias;

FUNCTION appl_owner
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
BEGIN
  v_return := kgc_sypa_00.standaard_waarde
              ( p_parameter_code => 'APPLICATIE_EIGENAAR'
              , p_kafd_id => NULL
              );
  RETURN( SUBSTR(v_return,1,30) );
END appl_owner;

FUNCTION tag
( p_string IN VARCHAR2
, p_tag_va IN VARCHAR2
, p_tag_tm IN VARCHAR2
)
RETURN VARCHAR2
IS
  --v_return VARCHAR2(1000);Commnted by Rajendra for Mantis 4513(02-01-2014)
  v_return VARCHAR2(32767);--Added by Rajendra for Mantis 4513(02-01-2014)
BEGIN
  v_return := UPPER(p_tag_va)||p_string||UPPER(p_tag_tm);
  RETURN( v_return );
END tag;

FUNCTION tag
( p_string IN VARCHAR2
, p_tag IN VARCHAR2
)
RETURN VARCHAR2
IS
  --v_return VARCHAR2(1000);Commnted by Rajendra for Mantis 4513(02-01-2014)
  v_return VARCHAR2(32767);--Added by Rajendra for Mantis 4513(02-01-2014)
BEGIN
  v_return := tag( p_string => p_string
                 , p_tag_va => '<'||p_tag||'>'
                 , p_tag_tm => '</'||p_tag||'>'
                 );
  RETURN( v_return );
END tag;

FUNCTION multi_replace
( p_string        IN VARCHAR2
, p_te_vervangen  IN VARCHAR2
, p_vervanging    IN VARCHAR2
)
-- Vervangt in p_string elk voorkomen
-- van p_te_vervangen door p_vervanging
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
BEGIN
  v_return := p_string;
  WHILE instr(v_return,p_te_vervangen) > 0
  LOOP
    v_return := replace(v_return,p_te_vervangen,p_vervanging);
  end loop;
  RETURN( v_return );
END multi_replace;


FUNCTION is_number (p_string IN VARCHAR2) RETURN VARCHAR2
IS
  v_return varchar2(1) := 'J';
  v_nummer number;
BEGIN
  IF p_string IS NULL
  THEN
    v_return  := 'N';
  ELSE
    BEGIN
      v_nummer := to_number(p_string);
    EXCEPTION WHEN OTHERS
    THEN
     v_return := 'N';
    END;
  END IF;

  RETURN(v_return);
END is_number;

FUNCTION kolom_lijst
( p_kolom           VARCHAR2
, p_tabel           VARCHAR2
, p_where           VARCHAR2 := null
, p_kolom_is_number VARCHAR2 := 'J'
)
RETURN VARCHAR2
IS
   TYPE cursor_type IS REF CURSOR;
   c_cursor   cursor_type;
   v_value    VARCHAR2(32767);
   v_return   VARCHAR2(32767);
BEGIN
   OPEN c_cursor FOR
      'SELECT ' || p_kolom ||
      ' FROM '  || p_tabel ||
      ' WHERE ' || NVL (p_where,'1 = 1');

   LOOP
      FETCH c_cursor INTO v_value;
      EXIT WHEN c_cursor%NOTFOUND;

      IF p_kolom_is_number='N'
      THEN
        v_value := ''''||v_value||'''';
      END IF;

      IF v_return IS NULL
      THEN
        v_return := v_value;
      ELSE
        v_return := v_return ||','||v_value;
      END IF;

   END LOOP;

   CLOSE c_cursor;

   IF v_return IS NULL
   THEN
     v_return := 'NULL';
   END IF;

   RETURN ( v_return );
END kolom_lijst;

function voor_deci_punt
( p_waarde in number
)
return varchar2
is
  v_return varchar2(100);
begin
  v_return := voor_deci_punt( to_char( p_waarde ) );
  return( v_return );
end voor_deci_punt;

function voor_deci_punt
( p_waarde in varchar2
)
return varchar2
is
  v_return varchar2(100);
  v_waarde varchar2(100) := ltrim( rtrim( p_waarde ) );
  v_pos_deci_teken number;
begin
  if ( is_number( v_waarde ) = 'J' )
  then
    v_pos_deci_teken := instr( v_waarde, decimaal_teken );
    if ( v_pos_deci_teken > 0 )
    then
      v_return := substr( v_waarde, 1, v_pos_deci_teken - 1 );
    else
      v_return := v_waarde;
    end if;
    if ( v_return is null )
    then
      v_return := '0';
    elsif ( v_return = '-' )
    then
      v_return := '-0';
    end if;
  else
    v_return := v_waarde;
  end if;
  return( v_return );
end voor_deci_punt;

function na_deci_punt
( p_waarde in number
)
return varchar2
is
  v_return varchar2(100);
begin
  v_return := na_deci_punt( to_char( p_waarde) );
  return( v_return );
end na_deci_punt;

function na_deci_punt
( p_waarde in varchar2
)
return varchar2
is
  v_return varchar2(100);
  v_waarde varchar2(100) := ltrim( rtrim( p_waarde ) );
begin
  if ( is_number( v_waarde ) = 'J' )
  then
    if ( instr( v_waarde, decimaal_teken ) > 0 )
    then
      v_return := substr( v_waarde, instr( v_waarde, decimaal_teken ) );
    else
      v_return := null;
    end if;
  else
    v_return := null;
  end if;
  return( v_return );
end na_deci_punt;

function verschil
( p_1 in varchar2
, p_2 in varchar2
)
return boolean
is
begin
  return( nvl( p_1, chr(2) ) <> nvl( p_2, chr(2) ) );
end verschil;
END kgc_util_00;
/

/
QUIT
