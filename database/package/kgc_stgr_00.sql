CREATE OR REPLACE PACKAGE "HELIX"."KGC_STGR_00" IS
  -- Zoek een protocol/stoftestgroep die eenduidig en uniek bij een stoftest hoort in de situatie waarin
  -- die stoftest wordt aangemeld (onderzoek en fractie). De beschikbaarheid van protocollen staat gedefinieerd
  -- in stoftestgroep-gebruik.
  -- Alleen als er slechts 1 protocol wordt gevonden wordt de stgr_id teruggegeven

  FUNCTION stoftestgroep_bij_stoftest
  ( p_stof_id IN NUMBER
  , p_frac_id IN NUMBER
  , p_onde_id IN NUMBER
  )
  RETURN NUMBER;
END KGC_STGR_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_STGR_00" IS
-- Zoek een protocol/stoftestgroep die eenduidig en uniek bij een stoftest hoort in de situatie waarin
-- die stoftest wordt aangemeld (onderzoek en fractie). De beschikbaarheid van protocollen staat gedefinieerd
-- in stoftestgroep-gebruik.
-- Alleen als er slechts 1 protocol wordt gevonden wordt de stgr_id teruggegeven
  FUNCTION stoftestgroep_bij_stoftest
  ( p_stof_id IN NUMBER
  , p_frac_id IN NUMBER
  , p_onde_id IN NUMBER
  )
  RETURN NUMBER
  IS
    CURSOR stgr_cur
    ( b_stof_id IN NUMBER
    , b_frac_id IN NUMBER
    , b_onde_id IN NUMBER
    )
    IS
      SELECT stgr.stgr_id
      FROM   KGC_STOFTESTGROEPEN stgr
      ,      KGC_PROTOCOL_STOFTESTEN prst
      ,      KGC_STOFTESTEN stof
      WHERE stof.stof_id   =  b_stof_id
      AND   prst.stof_id   = stof.stof_id
      AND   stgr.stgr_id   = prst.stgr_id
      AND   stgr.vervallen = 'N'
      AND   stgr.kafd_id   = stof.kafd_id
      AND EXISTS ( SELECT NULL
                   FROM   KGC_STOFTESTGROEP_GEBRUIK stgg
                   ,      KGC_MONSTERS mons
                   ,      BAS_FRACTIES frac
                   WHERE  frac.frac_id = b_frac_id
                   AND    mons.mons_id = frac.mons_id
                   AND    stgg.stgr_id = stgr.stgr_id
                   AND    stgg.vervallen = 'N'
                   AND  ( stgg.mate_id = mons.mate_id
                       OR stgg.mate_id IS NULL
                        )
                   AND  ( exists (select NULL
                                  from KGC_ONDERZOEK_INDICATIES onin
                                  where onin.onde_id = b_onde_id
                                  and   onin.indi_id = stgg.indi_id
                                 )
                       OR stgg.indi_id IS NULL
                        )
                   AND  ( stgg.frty_id = frac.frty_id
                       OR stgg.frty_id IS NULL
                        )
                   AND  ( exists (select NULL
                                  from KGC_ONDERZOEKEN      onde
                                  ,    KGC_ONDERZOEKSWIJZEN onwy
                                  where onde.onde_id         = b_onde_id
                                  and   onde.onderzoekswijze = onwy.code
                                  and   onde.kafd_id         = onwy.kafd_id
                                 )
                       OR stgg.onwy_id IS NULL
                        )
                   AND  ( bas_meti_00.afnametijd_oke( mons.tijd_afname
                                                    , stgg.afnametijd
                                                    ) = 'J'
                      OR stgg.afnametijd IS NULL
                        )
                 )
    GROUP BY stgr.stgr_id
    HAVING count(*) = 1;

    v_return NUMBER;
  BEGIN
    OPEN stgr_cur
    ( b_stof_id => p_stof_id
    , b_frac_id => p_frac_id
    , b_onde_id => p_onde_id
    );
    FETCH stgr_cur
    INTO v_return;
    CLOSE stgr_cur;

    RETURN ( v_return );
  END;
END KGC_STGR_00;
/

/
QUIT
