CREATE OR REPLACE PACKAGE "HELIX"."KGC_NUST_00" IS
-- Genereer en controleer een nieuw onderzoeknummer, monsternummer, ...
-- p_nummertype = ONDE, MONS, FRAC, ...
-- p_prefix = vanuit aanroep bepaalde prefix: oa. gebruikt bij monsternummers van DNA,
--                                                doorgift fractietype voor fractienummers
-- p_prefix2 = idem; fractienummers bevatten soms monsternummer en fractietype.
--
-- Mogelijke formaten staan in de tabel KGC_NUMMERSTRUCTUUR:
--
-- Bij foutieve controle moet de fout worden verwerkt door het aanroepende programma
/*
Nummergenerator
===============
Helix kan codes (nummers,namen) genereren voor een aantal (12) entiteiten:
(stand van 08-02-2006)
Onderzoeknummer (ONDE)
Monsternummer (MONS)
Fractienummer (FRAC)
Standaardfractienummer (STAN)
Runnummer (RUNS)
Werklijst_nummer (WERK)
Isolatielijst_nummer (ISOL)
Familienummer (FAMI)
Familieonderzoeknr (FAON)
Meting_groep_nummer (MEGR)
Samplenaam (SAMP)
Bestandsnaam (BEST)

Welke nummerstructuur wordt gebruikt voor het genereren is geregistreerd bij de systeemparameters met de naam NUMMERSTRUCTUUR_xxxx, waarbij xxxx staat voor de entiteit (zie hierboven, bv. NUMMERSTRUCTUUR_ONDE).
Per kgc-afdeling en onderzoeksgroep kan (hoeft niet) een specifieke nummerstructuur worden vastgelegd.

Definitie van nummerstructuur
=============================
Er kan gebruik gemaakt worden van de volgende elementen
<USER>: de oracle-naam van de ingelogde gebruiker
<MEDE_CODE>: de medewerkercode van de ingelogde gebruiker
<SP>...</SP>: verwijst naar een systeemparameter, de waarde daarvan wordt gebruikt
<FN>...</FN>: verwijst naar een functie: is niet operationeel (doet het niet goed!)
<WAARDE1>: *)
<WAARDE2>: *)
<WAARDE3>: *)
<WAARDE4>: *)
<WAARDE5>: *)

*) de waarde verschilt per entiteit:
entiteit module         waarde1         waarde2         waarde3        waarde4         waarde5
-------- -----------    -------         -------         -------        -------         -------
BEST     KGCIDEF10,12   werklijstnummer externe_id      .              .               .
FAMI     KGCFAMI01      .               .               .              .               .
FAON     KGCFAON01      .               .               .              .               .
FRAC     BASFRAC01      monsternummer   frty_letter     opwerkdatum    .               .
ISOL     BASISOL01      .               .               .              .               .
MEGR     KGC_MEGR_00    .               .               .              .               .
MONS     KGCMONS01      mate_letter     .               .              .               .
ONDE     KGCONDE01      .               .               .              .               .
RUNS     BASRUNS01      .               .               .              .               .
SAMP     kgc_interface  fractienummer   stof_code       stof_omschr    stof_sample**)  .
STAN     BASSTAN01      .               .               .              .               .
WERK     KGCWLST01      .               .               .              .               .
**) stoftest_samplenaam is een extra attribuut bij een stoftest; als leeg dan wordt de stof_omschrijving gebruikt

*/
FUNCTION genereer_nr
( p_nummertype IN VARCHAR2 -- ONDE,MONS,FRAC,RUNS,WERK,STAN,ISOL,FAMI
, p_nummerstructuur IN VARCHAR2 := NULL
, p_ongr_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_waarde1 IN VARCHAR2 := NULL
, p_waarde2 IN VARCHAR2 := NULL
, p_waarde3 IN VARCHAR2 := NULL
, p_waarde4 IN VARCHAR2 := NULL
, p_waarde5 IN VARCHAR2 := NULL
)
RETURN VARCHAR2;

PROCEDURE controleer_nr
( p_nummertype IN VARCHAR2
, p_nummerstructuur IN VARCHAR2 := NULL
, p_ongr_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_nummer IN VARCHAR2
, p_waarde1 IN VARCHAR2 := NULL
, p_waarde2 IN VARCHAR2 := NULL
, p_waarde3 IN VARCHAR2 := NULL
, p_waarde4 IN VARCHAR2 := NULL
, p_waarde5 IN VARCHAR2 := NULL
, x_goed OUT BOOLEAN
, x_melding OUT VARCHAR2
);
END KGC_NUST_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_NUST_00" IS
-- lokale cursor
CURSOR ongr_cur
( p_ongr_id IN NUMBER
)
IS
  SELECT kafd_id
  FROM   kgc_onderzoeksgroepen
  WHERE  ongr_id = p_ongr_id
  ;

CURSOR nust_cur
( p_nummerstructuur IN VARCHAR2
)
IS
  SELECT nust.nust_id
  ,      nust.min_lengte
  ,      nust.max_lengte
  FROM   kgc_nummerstructuur nust
  WHERE  nust.code = p_nummerstructuur
  ;
nust_rec nust_cur%rowtype;

CURSOR nuss_cur
( p_nummerstructuur IN VARCHAR2
)
IS
  SELECT nuss.positie_va
  ,      nuss.lengte
  ,      nuss.soort
  ,      nuss.vaste_waarde
  ,      nuss.swp_parent_ind
  ,      nuss.min_waarde
  ,      nuss.max_waarde
  ,      nuss.stap
  ,      nuss.opvulteken
  FROM   kgc_nust_samenstelling nuss
  ,      kgc_nummerstructuur nust
  WHERE  nuss.nust_id = nust.nust_id
  AND    nust.code = p_nummerstructuur
  ORDER BY nuss.positie_va ASC
  ;
swp_rec nuss_cur%rowtype; -- tbv. opslag van waarden die gebruikt worden buiten de for-loop

-- lokale functie
-- LET OP: functies aangeroepen vanuit de nummerstructuur samenstelling
--         hebben een vastgesteld parametergebruik KAFD_ID, ONGR_ID, NUSS_REC, maar dat loop via variabelen
--         de functie retourneert een varchar2.
--
f_kafd_id NUMBER;
f_ongr_id NUMBER;
f_nuss_rec nuss_cur%rowtype;

-- cyto onderzoeksnummers wijken erg af:
-- dagstempel(yymmdd) met een volgnummer in de vorm van twee letters
-- het volgnummer start wekelijks opnieuw
-- Pas op: die eikels van Oracle laten het weeknummer op 1 januari veranderen!!!
FUNCTION twee_letters_per_week
RETURN VARCHAR2
IS
  v_correctie NUMBER := 0;
  v_combi VARCHAR2(2);
  CURSOR onde_cur
  IS
    SELECT MAX( SUBSTR( onderzoeknr, NVL(f_nuss_rec.positie_va,7),2 ) ) hoogste
    FROM   kgc_onderzoeken
    WHERE  ongr_id = f_ongr_id
    AND    TO_CHAR( datum_binnen - v_correctie, 'yyww' ) = TO_CHAR( SYSDATE - v_correctie, 'yyww' )
    AND    datum_binnen > TRUNC( sysdate-10 ) -- performance
    AND    datum_binnen < TRUNC( SYSDATE+10 ) -- performance
    ;
  v_een NUMBER;
  v_twee NUMBER;
  v_min_waarde VARCHAR2(2) := NVL( SUBSTR( f_nuss_rec.min_waarde, 1, 2 ), 'AA' );
  v_max_waarde VARCHAR2(2) := NVL( SUBSTR( f_nuss_rec.max_waarde, 1, 2 ), 'ZZ' );
  v_stap NUMBER := NVL( f_nuss_rec.stap, 1 );
-- PL/SQL Block


BEGIN
  -- correctie afhankelijk van dag waarop 1 januari valt
  v_correctie := TO_NUMBER( TO_CHAR( TO_DATE( '01-01-'||TO_CHAR(SYSDATE,'yyyy')
                                            , 'dd-mm-yyyy'
                                            )
                                   , 'd'
                                   )
                           ) - 1;
  OPEN  onde_cur;
  FETCH onde_cur
  INTO  v_combi;
  CLOSE onde_cur;
  IF ( v_combi IS NULL )
  THEN
    v_combi := v_min_waarde;
  ELSE
    v_een := ASCII( SUBSTR( v_combi, 1, 1 ) );
    v_twee := ASCII( SUBSTR( v_combi, 2, 1 ) );
    IF ( v_twee < ASCII( SUBSTR( v_max_waarde, 2, 1 ) ) )
    THEN
      v_twee := v_twee + v_stap;
    ELSE
      IF ( v_een < ASCII( SUBSTR( v_max_waarde, 1, 1 ) ) )
      THEN
        v_een := v_een + v_stap;
        v_twee := ASCII( SUBSTR( v_min_waarde, 2, 1 ) );
      ELSE
        v_een := ASCII( SUBSTR( v_min_waarde, 1, 1 ) );
        v_twee := ASCII( SUBSTR( v_min_waarde, 2, 1 ) );
      END IF;
    END IF;
    v_combi := CHR( v_een)||CHR(v_twee);
  END IF;
  RETURN( v_combi );
END twee_letters_per_week;

-- haal waarde tussen de tags uit
FUNCTION tag_waarde
( p_string IN VARCHAR2
, p_tag IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_waarde VARCHAR2(100) := NULL;
  v_begintag VARCHAR2(10) := '<'||UPPER(p_tag)||'>';
  v_eindtag VARCHAR2(10) := '</'||UPPER(p_tag)||'>';
  v_begin NUMBER := INSTR( p_string, v_begintag );
  v_einde NUMBER := INSTR( p_string, v_eindtag );
BEGIN
  IF ( v_begin > 0
   AND v_einde > 0
     )
  THEN
    v_waarde := SUBSTR( p_string
                      , v_begin + LENGTH( v_begintag )
                      , v_einde - ( v_begin + LENGTH( v_begintag ) )
                      );
  END IF;
  RETURN( v_waarde );
END tag_waarde;


FUNCTION formatteer
( p_datum_string IN VARCHAR2  -- altijd in het formaat 'dd-mm-yyyy hh24:mi:ss''
, p_datum_formaat IN VARCHAR2
)
RETURN VARCHAR2
IS
 v_datum_formaat VARCHAR2(20);
 v_waarde VARCHAR2(100) := NULL;
 v_datum DATE;
BEGIN
  IF p_datum_formaat IS NULL
  THEN
    v_datum_formaat := 'DD-MM-YYYY';
  ELSE
    v_datum_formaat := p_datum_formaat;
  END IF;

  v_datum := NVL(TO_DATE(p_datum_string,'dd-mm-yyyy hh24:mi:ss'),SYSDATE);
  v_waarde := TO_CHAR(v_datum, v_datum_formaat);
  RETURN (v_waarde);
END formatteer;


-- publieke functies/procedures
FUNCTION genereer_nr
( p_nummertype IN VARCHAR2
, p_nummerstructuur IN VARCHAR2 := NULL
, p_ongr_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_waarde1 IN VARCHAR2 := NULL
, p_waarde2 IN VARCHAR2 := NULL
, p_waarde3 IN VARCHAR2 := NULL
, p_waarde4 IN VARCHAR2 := NULL
, p_waarde5 IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_kafd_id NUMBER;
  v_nummerstructuur VARCHAR2(100);

  v_gen_nummer VARCHAR2(50);
  v_statement VARCHAR2(4000);
  v_tabel VARCHAR2(200); -- NtB, 798: vergroot, was 30
  v_kolom VARCHAR2(30);
  v_parent VARCHAR2(100);
  v_parent_pos NUMBER;
  v_volgnr_tov_parent VARCHAR2(1);
  v_volgnr VARCHAR2(100);
  v_datum_formaat VARCHAR2(30);

BEGIN
  IF ( p_ongr_id IS NOT NULL
   AND p_kafd_id IS NULL
     )
  THEN
    OPEN  ongr_cur( p_ongr_id );
    FETCH ongr_cur
    INTO  v_kafd_id;
    CLOSE ongr_cur;
  ELSE
    v_kafd_id := p_kafd_id;
  END IF;

  -- bepaal welke nummerstructuur moet worden gebruikt
  IF p_nummerstructuur is not null
  THEN
    v_nummerstructuur := p_nummerstructuur;
  ELSE
    BEGIN
      v_nummerstructuur := kgc_sypa_00.standaard_waarde
                         ( p_parameter_code => 'NUMMERSTRUCTUUR_'||p_nummertype
                         , p_kafd_id => v_kafd_id
                         , p_ongr_id => p_ongr_id
                         );
    EXCEPTION
      WHEN OTHERS THEN NULL;
    END;
    IF ( v_nummerstructuur IS NULL )
    THEN
      v_nummerstructuur := p_nummertype;
    END IF;
  END IF;

  --  doeltabel en -kolom
  IF ( p_nummertype = 'ONDE' )
  THEN
    v_tabel := 'kgc_onderzoeken';
    v_kolom := 'onderzoeknr';
  ELSIF ( p_nummertype = 'MONS' )
  THEN
    v_tabel := 'kgc_monsters';
    v_kolom := 'monsternummer';
  ELSIF ( p_nummertype = 'FRAC' )
  THEN
    v_tabel := 'bas_fracties';
    v_kolom := 'fractienummer';
  ELSIF ( p_nummertype = 'STAN' )
  THEN
    v_tabel := 'bas_standaardfracties';
    v_kolom := 'standaardfractienummer';
  ELSIF ( p_nummertype = 'RUNS' )
  THEN
    v_tabel := 'bas_runs';
    v_kolom := 'runnummer';
  ELSIF ( p_nummertype = 'WERK' )
  THEN
    v_tabel := 'kgc_werklijstnummers_vw';
    v_kolom := 'werklijst_nummer';
  ELSIF ( p_nummertype = 'WERK_ML' )
  THEN
    v_tabel := 'kgc_werklijstnummers_vw';
    v_kolom := 'werklijst_nummer';
  ELSIF ( p_nummertype = 'ISOL' )
  THEN
    -- v_tabel := 'bas_isolatielijsten'; -- NtB, 798: vervangen door:
    v_tabel := '( select isolatielijst_nummer from bas_isolatielijsten'||
               '  union all'||
               '  select isolatielijst_nummer from bas_isolatielijsten_jn)';
    v_kolom := 'isolatielijst_nummer';
  ELSIF ( p_nummertype = 'FAMI' )
  THEN
    v_tabel := 'kgc_families';
    v_kolom := 'familienummer';
  ELSIF ( p_nummertype = 'FAON' )
  THEN
    v_tabel := 'kgc_familie_onderzoeken';
    v_kolom := 'familieonderzoeknr';
  ELSIF ( p_nummertype = 'MEGR' )
  THEN
    v_tabel := 'kgc_meting_groepen';
    v_kolom := 'nummer';
  ELSIF ( p_nummertype = 'SAMP' )
  THEN
    v_tabel := 'kgc_samples';
    v_kolom := 'naam';
  ELSIF ( p_nummertype = 'BEST' )
  THEN
    v_tabel := 'kgc_bestanden';
    v_kolom := 'bestandsnaam';
  ELSIF ( p_nummertype = 'EMBR' )-- Kartiki for PGD Mantis 0671
  THEN
    v_tabel := 'kgc_embryos';
    v_kolom := 'embryonr';
  END IF;

  -- samenstelling ...
  OPEN  nuss_cur( v_nummerstructuur );
  FETCH nuss_cur
  INTO  swp_rec;
  IF ( nuss_cur%notfound )
  THEN
    CLOSE nuss_cur;
    raise_application_error
    ( -20000
    , 'Nummerstructuur '||v_nummerstructuur||' niet gevonden!'
    );
  END IF;
  CLOSE nuss_cur;
  v_gen_nummer := NULL;
  swp_rec.soort := NULL;
  v_parent := NULL;
  v_parent_pos := NULL;
  FOR r IN nuss_cur( v_nummerstructuur )
  LOOP
    v_datum_formaat := NULL;
    IF ( r.vaste_waarde IS NOT NULL )
    THEN
      IF    ( INSTR(UPPER(r.vaste_waarde), '<WAARDE1>' ) > 0 ) -- doorgegeven parameter  nr1
      THEN
	   v_datum_formaat := SUBSTR(r.vaste_waarde,10);
	   r.vaste_waarde := p_waarde1;
      ELSIF ( INSTR(UPPER(r.vaste_waarde), '<WAARDE2>' ) > 0 ) -- doorgegeven parameter nr2
      THEN
	   v_datum_formaat := SUBSTR(r.vaste_waarde,10);
	   r.vaste_waarde := p_waarde2;
      ELSIF ( INSTR(UPPER(r.vaste_waarde), '<WAARDE3>' ) > 0 ) -- doorgegeven parameter nr3
      THEN
	   v_datum_formaat := SUBSTR(r.vaste_waarde,10);
	   r.vaste_waarde := p_waarde3;
      ELSIF ( INSTR(UPPER(r.vaste_waarde), '<WAARDE4>' ) > 0 ) -- doorgegeven parameter nr4
      THEN
	   v_datum_formaat := SUBSTR(r.vaste_waarde,10);
	   r.vaste_waarde := p_waarde4;
      ELSIF ( INSTR(UPPER(r.vaste_waarde), '<WAARDE5>' ) > 0 ) -- doorgegeven parameter nr5
      THEN
   	    v_datum_formaat := SUBSTR(r.vaste_waarde,10);
    	r.vaste_waarde := p_waarde5;
      ELSIF ( INSTR(UPPER(r.vaste_waarde), '<USER>' ) > 0 ) -- oracle user id
      THEN
        r.vaste_waarde := USER;
      ELSIF ( INSTR(UPPER(r.vaste_waarde), '<MEDE_CODE>' ) > 0 ) -- medewerker code
      THEN
        r.vaste_waarde := kgc_mede_00.medewerker_code;
      ELSIF ( INSTR(UPPER(r.vaste_waarde), '<SP>' ) > 0 ) -- systeemparameter
      THEN
        r.vaste_waarde := tag_waarde( UPPER(r.vaste_waarde), 'SP' );
        r.vaste_waarde := kgc_sypa_00.standaard_waarde
                          ( p_parameter_code => UPPER(r.vaste_waarde)
                          , p_kafd_id => v_kafd_id
                          , p_ongr_id => p_ongr_id
                          );
      ELSIF ( INSTR(r.vaste_waarde, '<FN>' ) > 0 ) -- functie
      THEN
        r.vaste_waarde := tag_waarde( r.vaste_waarde, 'FN' );
        -- vul vaste functie-variabelen
        f_kafd_id := v_kafd_id;
        f_ongr_id := p_ongr_id;
        f_nuss_rec := r;
        IF ( LOWER(r.vaste_waarde) = 'twee_letters_per_week' )
        THEN
          r.vaste_waarde := twee_letters_per_week;
        ELSE
          r.vaste_waarde := RPAD( '?', r.lengte, '?' );
        END IF;
      END IF;

      IF ( r.soort = 'C' )
      THEN
        r.vaste_waarde := r.vaste_waarde;
      ELSIF ( r.soort = 'D' )
      THEN
        r.vaste_waarde := formatteer(r.vaste_waarde, v_datum_formaat);
      ELSIF ( r.soort = 'T' )
      THEN
        r.vaste_waarde := TO_CHAR(SYSDATE,r.vaste_waarde);
      ELSIF ( r.soort = 'S' )
      THEN
        r.vaste_waarde := NULL;
      ELSIF ( r.soort = 'N' )
      THEN
        r.vaste_waarde := TO_CHAR(TO_NUMBER(r.vaste_waarde));
      END IF;
      r.vaste_waarde := SUBSTR(r.vaste_waarde, 1, r.lengte );

      -- Mantis 1945 -- eventueel opvullen met opvulteken
      IF r.opvulteken IS NOT NULL
      THEN
        IF r.vaste_waarde IS NULL
        THEN
          r.vaste_waarde := rpad(r.opvulteken,r.lengte,r.opvulteken);
        ELSIF length(r.vaste_waarde) < r.lengte
        THEN
          r.vaste_waarde := rpad(r.vaste_waarde,r.lengte,r.opvulteken);
        END IF;
      END IF;

      v_gen_nummer := v_gen_nummer||r.vaste_waarde;
    END IF;

    IF ( r.swp_parent_ind = 'J' )
    THEN
      IF ( v_parent_pos IS NULL )
      THEN
        v_parent_pos := r.positie_va;
      ELSE
        v_parent_pos := LEAST( r.positie_va, v_parent_pos );
      END IF;
      v_parent := v_parent||r.vaste_waarde;
    ELSIF ( r.soort = 'S' )
    THEN
      IF ( v_parent_pos IS NULL
       AND v_parent IS NULL
         )
      THEN
        v_volgnr_tov_parent := 'V'; -- Voor
      ELSE
        v_volgnr_tov_parent := 'N'; -- Na(=dflt).
      END IF;
      swp_rec := r;
      v_gen_nummer := v_gen_nummer||'<swp>';
    END IF;
  END LOOP;
  IF ( v_tabel IS NOT NULL
   AND v_kolom IS NOT NULL
   AND swp_rec.soort = 'S'
     )
  THEN
    IF ( v_volgnr_tov_parent = 'V'
      AND v_parent is not null
       )
    THEN
      v_statement := 'select max( to_number( substr( '||v_kolom||',-'||TO_CHAR(LENGTH(v_parent)+swp_rec.lengte)||','||TO_CHAR(swp_rec.lengte)||')))'
         ||CHR(10)|| 'from '||v_tabel
         ||CHR(10)|| 'where substr( '||v_kolom||', -'||TO_CHAR(LENGTH(v_parent))||' ) = '''||v_parent||''''
         ||CHR(10)|| 'and ltrim(translate(substr( '||v_kolom||',-'||TO_CHAR(LENGTH(v_parent)+swp_rec.lengte)||','||TO_CHAR(swp_rec.lengte)||'),''0123456789'',''          '')) is null'
         ||chr(10)|| 'and substr( '||v_kolom||',-'||TO_CHAR(LENGTH(v_parent)+swp_rec.lengte)||','||TO_CHAR(swp_rec.lengte)||') between '''||nvl(swp_rec.min_waarde,'0000000000')||''' and '''||nvl(swp_rec.max_waarde,'9999999999')||''''
                   ;
    ELSIF ( v_parent IS NOT NULL )
    THEN
      v_statement := 'select max( to_number( substr( '||v_kolom||','||TO_CHAR(swp_rec.positie_va)||','||TO_CHAR(swp_rec.lengte)||')))'
         ||CHR(10)|| 'from '||v_tabel
         ||CHR(10)|| 'where substr( '||v_kolom||','||v_parent_pos||','||TO_CHAR(LENGTH(v_parent))||' ) = '''||v_parent||''''
         ||CHR(10)|| 'and ltrim(translate(substr( '||v_kolom||','||TO_CHAR(swp_rec.positie_va)||','||TO_CHAR(swp_rec.lengte)||'),''0123456789'',''          '')) is null'
         ||chr(10)|| 'and substr( '||v_kolom||','||TO_CHAR(swp_rec.positie_va)||','||TO_CHAR(swp_rec.lengte)||') between '''||nvl(swp_rec.min_waarde,'0000000000')||''' and '''||nvl(swp_rec.max_waarde,'9999999999')||''''
                   ;
    ELSE -- gegenereerde nummers zijn geheel numeriek
      v_statement := 'select max( to_number('||v_kolom||'))'
         ||CHR(10)|| 'from '||v_tabel
         ||CHR(10)|| 'where ltrim(translate('||v_kolom||',''0123456789'',''          '')) is null'
         ||chr(10)|| 'and '||v_kolom||' between '''||nvl(swp_rec.min_waarde,'0000000000')||''' and '''||nvl(swp_rec.max_waarde,'9999999999')||''''
                   ;
    END IF;

    BEGIN
      v_volgnr := NVL(TO_NUMBER(kgc_util_00.dyn_exec( v_statement )), 0) + NVL(swp_rec.stap,1);
    EXCEPTION
      WHEN OTHERS
      THEN
        v_volgnr := NULL;
    END;
    IF ( swp_rec.min_waarde IS NOT NULL )
    THEN
      IF ( v_volgnr < TO_NUMBER(swp_rec.min_waarde) )
      THEN
        v_volgnr := swp_rec.min_waarde;
      END IF;
    END IF;
    IF ( swp_rec.max_waarde IS NOT NULL )
    THEN
      IF ( v_volgnr > TO_NUMBER(swp_rec.max_waarde) )
      THEN
        v_volgnr := NVL( swp_rec.min_waarde, 1 );
      END IF;
    END IF;

    v_gen_nummer := REPLACE( v_gen_nummer,'<swp>',LPAD(v_volgnr,swp_rec.lengte,'0') );

    -- Kartiki incident



   /* If swp_rec.lengte > length(v_gen_nummer) THEN  -- if total length is greater than the length of generated string
       v_gen_nummer := LPAD(v_gen_nummer,swp_rec.lengte,'0');
    end if; */
  END IF;
  RETURN( v_gen_nummer );
END genereer_nr;
--
PROCEDURE controleer_nr
( p_nummertype IN VARCHAR2
, p_nummerstructuur IN VARCHAR2 := NULL
, p_ongr_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_nummer IN VARCHAR2
, p_waarde1 IN VARCHAR2 := NULL
, p_waarde2 IN VARCHAR2 := NULL
, p_waarde3 IN VARCHAR2 := NULL
, p_waarde4 IN VARCHAR2 := NULL
, p_waarde5 IN VARCHAR2 := NULL
, x_goed OUT BOOLEAN
, x_melding OUT VARCHAR2
)

IS
  v_kafd_id NUMBER;
  v_nummerstructuur VARCHAR2(100);
  v_nummer VARCHAR2(20) := p_nummer;
  v_gebruikte_waarde VARCHAR2(100);
  --
  v_melding VARCHAR2(1000);
  -- voeg (deel)melding toe aan complete melding tekst
  PROCEDURE voeg_toe
  ( l_melding IN VARCHAR2
  )
  IS
  BEGIN
    IF ( v_melding IS NULL )
    THEN
      v_melding := l_melding;
    ELSE
      v_melding := v_melding||CHR(10)||l_melding;
    END IF;
  END voeg_toe;
BEGIN
  IF ( p_ongr_id IS NOT NULL
   AND p_kafd_id IS NULL
     )
  THEN
    OPEN  ongr_cur( p_ongr_id );
    FETCH ongr_cur
    INTO  v_kafd_id;
    CLOSE ongr_cur;
  ELSE
    v_kafd_id := p_kafd_id;
  END IF;

  -- bepaal welke nummerstructuur moet worden gebruikt
  IF p_nummerstructuur is not null
  THEN
    v_nummerstructuur := p_nummerstructuur;
  ELSE
    BEGIN
      v_nummerstructuur := kgc_sypa_00.standaard_waarde
                         ( p_parameter_code => 'NUMMERSTRUCTUUR_'||p_nummertype
                         , p_kafd_id => v_kafd_id
                         , p_ongr_id => p_ongr_id
                         );
    EXCEPTION
      WHEN OTHERS THEN NULL;
    END;
    IF ( v_nummerstructuur IS NULL )
    THEN
      v_nummerstructuur := p_nummertype;
    END IF;
  END IF;

  OPEN  nust_cur( v_nummerstructuur );
  FETCH nust_cur
  INTO  nust_rec;
  CLOSE nust_cur;
  IF ( LENGTH( v_nummer ) < NVL( nust_rec.min_lengte, -999 ) )
  THEN
    voeg_toe( 'Lengte van nummer is te klein (<'||nust_rec.min_lengte||').' );
  END IF;
  IF ( LENGTH( v_nummer ) > NVL( nust_rec.max_lengte, 999 ) )
  THEN
    voeg_toe( 'Lengte van nummer is te groot (>'||nust_rec.max_lengte||').' );
  END IF;

  -- samenstelling ...
  OPEN  nuss_cur( v_nummerstructuur );
  FETCH nuss_cur
  INTO  swp_rec;
  IF ( nuss_cur%notfound )
  THEN
    CLOSE nuss_cur;
    raise_application_error
    ( -20000
    , 'Nummerstructuur '||v_nummerstructuur||' niet gevonden!'
    );
  END IF;
  CLOSE nuss_cur;
  FOR r IN nuss_cur( v_nummerstructuur )
  LOOP
    v_gebruikte_waarde := SUBSTR( v_nummer, r.positie_va, r.lengte );
    IF ( r.vaste_waarde IS NOT NULL )
	THEN
      IF ( INSTR(r.vaste_waarde, '<WAARDE1>' ) > 0 ) -- doorgegeven parameter nr 1
      THEN
        IF ( p_waarde1 IS NULL )
        THEN
          v_nummer := SUBSTR( v_nummer, 1, r.positie_va - 1 )
                   || RPAD( '#', r.lengte, '#' )
                   || SUBSTR( v_nummer, r.positie_va )
                    ;
        ELSE
          IF ( v_gebruikte_waarde <> p_waarde1 )
          THEN
            voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                   || ') komt niet overeen met verwachte waarde: ('||p_waarde1||').' );
          END IF;
        END IF;
      ELSIF ( INSTR(r.vaste_waarde, '<WAARDE2>' ) > 0 ) -- doorgegeven parameter nr 2
	  THEN
        IF ( p_waarde2 IS NULL )
        THEN
          v_nummer := SUBSTR( v_nummer, 1, r.positie_va - 1 )
                   || RPAD( '#', r.lengte, '#' )
                   || SUBSTR( v_nummer, r.positie_va )
                    ;
        ELSE
          IF ( v_gebruikte_waarde <> p_waarde2 )
          THEN
            voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                   || ') komt niet overeen met verwachte waarde: ('||p_waarde2||').' );
          END IF;
        END IF;
      ELSIF ( INSTR(r.vaste_waarde, '<WAARDE3>' ) > 0 ) -- doorgegeven parameter nr 3
	  THEN
        IF ( p_waarde3 IS NULL )
        THEN
          v_nummer := SUBSTR( v_nummer, 1, r.positie_va - 1 )
                   || RPAD( '#', r.lengte, '#' )
                   || SUBSTR( v_nummer, r.positie_va )
                    ;
        ELSE
          IF ( v_gebruikte_waarde <> p_waarde3 )
          THEN
            voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                   || ') komt niet overeen met verwachte waarde: ('||p_waarde3||').' );
          END IF;
        END IF;
      ELSIF ( INSTR(r.vaste_waarde, '<WAARDE4>' ) > 0 ) -- doorgegeven parameter nr 4
	  THEN
        IF ( p_waarde4 IS NULL )
        THEN
          v_nummer := SUBSTR( v_nummer, 1, r.positie_va - 1 )
                   || RPAD( '#', r.lengte, '#' )
                   || SUBSTR( v_nummer, r.positie_va )
                    ;
        ELSE
          IF ( v_gebruikte_waarde <> p_waarde4 )
          THEN
            voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                   || ') komt niet overeen met verwachte waarde: ('||p_waarde4||').' );
          END IF;
        END IF;
      ELSIF ( INSTR(r.vaste_waarde, '<WAARDE5>' ) > 0 ) -- doorgegeven parameter nr 5
	  THEN
        IF ( p_waarde5 IS NULL )
        THEN
          v_nummer := SUBSTR( v_nummer, 1, r.positie_va - 1 )
                   || RPAD( '#', r.lengte, '#' )
                   || SUBSTR( v_nummer, r.positie_va )
                    ;
        ELSE
          IF ( v_gebruikte_waarde <> p_waarde5 )
          THEN
            voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                   || ') komt niet overeen met verwachte waarde: ('||p_waarde5||').' );
          END IF;
        END IF;
      ELSIF ( INSTR(r.vaste_waarde, '<USER>' ) > 0 ) -- oracle user id
      THEN
        NULL; -- geen controle op user
      ELSIF ( INSTR(r.vaste_waarde, '<SP>' ) > 0 ) -- systeemparameter
      THEN
        r.vaste_waarde := tag_waarde( r.vaste_waarde, 'SP' );
        r.vaste_waarde := kgc_sypa_00.standaard_waarde
                          ( p_parameter_code => r.vaste_waarde
                          , p_kafd_id => v_kafd_id
                          , p_ongr_id => p_ongr_id
                          );
        v_gebruikte_waarde := SUBSTR(v_gebruikte_waarde,1,LENGTH(r.vaste_waarde));

        -- controleer inhoud met systeem-parameter
        IF ( r.vaste_waarde IS NOT NULL )
        THEN
          IF ( r.vaste_waarde <> v_gebruikte_waarde )
          THEN
            voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                   || ') komt niet overeen met systeemparameter: ('||r.vaste_waarde||').' );
          END IF;
        END IF;
      ELSIF ( INSTR(r.vaste_waarde, '<FN>' ) > 0 ) -- functie
      THEN
        NULL; -- geen controle op functie
      ELSE
        IF ( r.soort = 'C' )
        THEN
          -- controleer inhoud met opgegeven waarde
          IF ( r.vaste_waarde IS NOT NULL )
          THEN
            IF ( r.vaste_waarde <> v_gebruikte_waarde )
            THEN
              voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                     || ') komt niet overeen met vaste waarde: ('||r.vaste_waarde||').' );
            END IF;
          END IF;
        ELSIF ( r.soort = 'D' )
        THEN
          IF ( TO_CHAR( SYSDATE, r.vaste_waarde ) <> v_gebruikte_waarde )
          THEN
            voeg_toe( 'Gebruikte waarde ('||v_gebruikte_waarde
                   || ') staat niet in juiste datum-formaat: ('||r.vaste_waarde||').' );
          END IF;
        END IF;
      END IF;
    END IF;

    IF ( r.soort IN ( 'S', 'N' ) )
    THEN
      DECLARE
        v_num NUMBER;
      BEGIN
        v_num := TO_NUMBER( SUBSTR( v_nummer, r.positie_va, r.lengte ) );
      EXCEPTION
        WHEN VALUE_ERROR
        THEN
          voeg_toe( 'Waarde ('||SUBSTR( v_nummer, r.positie_va, r.lengte )
                 || ') is niet nummeriek.' );
      END;
    END IF;

    IF ( v_gebruikte_waarde < r.min_waarde )
    THEN
      voeg_toe( 'Waarde ('||v_gebruikte_waarde
             || ') is kleiner dan de minimum waarde: ('||r.min_waarde||').' );
    END IF;
    IF ( v_gebruikte_waarde > r.max_waarde )
    THEN
      voeg_toe( 'Waarde ('||v_gebruikte_waarde
             || ') is groter dan de maximum waarde: ('||r.max_waarde||').' );
    END IF;
  END LOOP;

  IF ( v_melding IS NOT NULL )
  THEN
    x_goed := FALSE;
    x_melding := v_melding;
  ELSE
    x_goed := TRUE;
  END IF;
END controleer_nr;
END kgc_nust_00;
/

/
QUIT
