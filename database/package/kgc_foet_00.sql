CREATE OR REPLACE PACKAGE "HELIX"."KGC_FOET_00" IS
-- retourneer foetusinformatie van een onderzoek of monster
-- NB.: Foetussen zijn pas in een laat stadium aan ONDE en MONS toegevoegd:
--      regulier inbouwen in applicatie is te tijdrovend
FUNCTION foetus_info
( p_foet_id IN NUMBER := NULL
, p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_kort IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( foetus_info, WNDS, WNPS, RNPS );
END KGC_FOET_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FOET_00" IS
FUNCTION foetus_info
( p_foet_id IN NUMBER := NULL
, p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_kort IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  v_foet_id NUMBER;
  CURSOR foet_cur
  IS
    SELECT zwan.nummer zwangerschap
    ,      posi.omschrijving positie
    ,      foet.volgnr
    ,      foet.pers_id_geboren_als
    FROM   kgc_posities posi
    ,      kgc_zwangerschappen zwan
    ,      kgc_foetussen foet
    WHERE  foet.zwan_id = zwan.zwan_id (+)
    AND    foet.posi_id = posi.posi_id (+)
    AND    foet.foet_id = p_foet_id
    ;
  foet_rec foet_cur%rowtype;
  CURSOR onde_cur
  IS
    SELECT foet_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
  CURSOR mons_cur
  IS
    SELECT foet_id
    FROM   kgc_monsters
    WHERE  mons_id = p_mons_id
    ;
BEGIN
  IF ( p_foet_id IS NOT NULL )
  THEN
    OPEN  foet_cur;
    FETCH foet_cur
    INTO  foet_rec;
    CLOSE foet_cur;
    IF ( p_kort = 'J' )
    THEN
      v_return := TO_CHAR(foet_rec.zwangerschap)
               || ' - '||TO_CHAR(foet_rec.volgnr)
                ;
    ELSE
      IF ( foet_rec.zwangerschap IS NOT NULL )
      THEN
        v_return := 'Zwangerschap '||TO_CHAR(foet_rec.zwangerschap)
                 || ', volgnr '||TO_CHAR(foet_rec.volgnr)
                  ;
        IF ( foet_rec.positie IS NOT NULL )
        THEN
          v_return := v_return
                   || ' ('||foet_rec.positie||')'
                    ;
        END IF;
      ELSIF ( foet_rec.volgnr IS NOT NULL )
      THEN
        v_return := 'Volgnr '||TO_CHAR(foet_rec.volgnr)
                 || ' (geen zwangerschap vermeld) ';
      ELSE
        v_return := 'Geen foetus-informatie bekend (id='||TO_CHAR( p_foet_id )||')';
      END IF;
      IF ( foet_rec.volgnr IS NOT NULL
       AND foet_rec.pers_id_geboren_als IS NOT NULL
         )
      THEN
        v_return := v_return
                 || ' (LET OP: geregistreerd als persoon!) ';
      END IF;
    END IF;
  ELSIF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_foet_id;
    CLOSE onde_cur;
    v_return := foetus_info( p_foet_id => v_foet_id, p_kort => p_kort );
  ELSIF ( p_mons_id IS NOT NULL )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  v_foet_id;
    CLOSE mons_cur;
    v_return := foetus_info( p_foet_id => v_foet_id, p_kort => p_kort );
  END IF;
  RETURN( v_return );
END foetus_info;

END kgc_foet_00;
/

/
QUIT
