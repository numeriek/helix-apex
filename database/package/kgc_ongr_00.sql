CREATE OR REPLACE PACKAGE "HELIX"."KGC_ONGR_00" IS
-- Haal de standaard onderzoeksgroep op
-- Zoek eerst in de systeemparameters
-- anders de ene van een kgc-afdeling, als er maar een is!
FUNCTION  default_groep
(  p_kafd_id  IN  NUMBER  := NULL
)
RETURN  VARCHAR2;

PROCEDURE  default_groep
(  p_kafd_id  IN  NUMBER := NULL
, x_ongr_id OUT NUMBER
, x_ongr_code OUT VARCHAR2
, x_ongr_omschrijving OUT VARCHAR2
);

FUNCTION id
( p_onderzoeksgroep IN VARCHAR2
)
RETURN NUMBER;

FUNCTION  get_code
(  p_ongr_id  IN  NUMBER  := NULL
)
RETURN  VARCHAR2;

-- nieuwe functie, mantis201, rob peijster, tbv scherm kgcfotc
FUNCTION  sorteerkenmerk
(  p_ongr_id  IN  NUMBER  := NULL
)
RETURN  VARCHAR2;
END KGC_ONGR_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ONGR_00" IS
FUNCTION default_groep
( p_kafd_id IN NUMBER  :=  NULL
)
RETURN VARCHAR2
IS
  v_return_waarde VARCHAR2(10) := NULL;
  v_kafd_id NUMBER := NVL( p_kafd_id, kgc_mede_00.afdeling_id );
  CURSOR ongr_cur
  IS
    SELECT code
    FROM   kgc_onderzoeksgroepen
    WHERE  kafd_id = v_kafd_id
    ;
  ongr_rec ongr_cur%rowtype;
BEGIN
  v_return_waarde := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'MEDEWERKER_ONDERZOEKSGROEP'
                     , p_kafd_id => v_kafd_id
                     );
  IF ( v_return_waarde IS NULL )
  THEN
    OPEN  ongr_cur;
    FETCH ongr_cur
    INTO  ongr_rec;
    IF ( ongr_cur%found )
    THEN
      v_return_waarde := ongr_rec.code;
      -- is er een tweede?
      FETCH ongr_cur
      INTO  ongr_rec;
      IF ( ongr_cur%found )
      THEN
        v_return_waarde := NULL;
      END IF;
    END IF;
    IF ( ongr_cur%isopen )
    THEN
      CLOSE ongr_cur;
    END IF;
  END IF;
  RETURN( v_return_waarde );
END default_groep;

PROCEDURE  default_groep
 (  p_kafd_id  IN  NUMBER  :=  NULL
 ,  x_ongr_id  OUT  NUMBER
 ,  x_ongr_code  OUT  VARCHAR2
 ,  x_ongr_omschrijving  OUT  VARCHAR2
 )
 IS
  v_kafd_id NUMBER := NVL( p_kafd_id, kgc_mede_00.afdeling_id );
  CURSOR ongr_cur
  IS
    SELECT ongr_id
    ,      omschrijving
    FROM   kgc_onderzoeksgroepen
    WHERE  kafd_id = v_kafd_id
    AND    code = x_ongr_code
    ;
  ongr_rec ongr_cur%rowtype;
 BEGIN
  x_ongr_code := default_groep( p_kafd_id => v_kafd_id );
  IF ( x_ongr_code IS NOT NULL )
  THEN
    OPEN  ongr_cur;
    FETCH ongr_cur
    INTO  x_ongr_id
    ,     x_ongr_omschrijving;
    CLOSE ongr_cur;
  END IF;
END default_groep;

FUNCTION id
( p_onderzoeksgroep IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR ongr_cur
  IS
    SELECT ongr_id
    FROM   kgc_onderzoeksgroepen
    WHERE  code = UPPER( p_onderzoeksgroep )
    ;
  v_id NUMBER;
BEGIN
-- MK, 11-10-2006
-- function is FOUT (geen kafd_id, code is niet uniek!!)
-- wordt nergens (meer) gebruikt en dat moet zo blijven!!!!
  OPEN  ongr_cur;
  FETCH ongr_cur
  INTO  v_id;
  CLOSE ongr_cur;
  RETURN( v_id );
END id;

FUNCTION  get_code
(  p_ongr_id  IN  NUMBER  := NULL
)
RETURN  VARCHAR2
IS
  CURSOR ongr_cur(b_ongr_id in number)
  IS
  SELECT code
  FROM kgc_onderzoeksgroepen
  WHERE ongr_id = b_ongr_id;

  v_code VARCHAR2(200);

BEGIN
  OPEN ongr_cur (b_ongr_id => p_ongr_id);
  FETCH ongr_cur INTO v_code;
  CLOSE ongr_cur;

  RETURN (p_ongr_id);
END get_code;

FUNCTION  sorteerkenmerk
/* Mantis201:
   Haal cd code van de onderzoeksgroep op aan de hand van ongr_id
*/
(  p_ongr_id  IN  NUMBER  := NULL
)
RETURN  VARCHAR2
IS
  CURSOR ongr_cur(b_ongr_id in number)
  IS
  SELECT code
  FROM kgc_onderzoeksgroepen
  WHERE ongr_id = b_ongr_id;

  v_code VARCHAR2(200);

BEGIN
  v_code := null;
  OPEN ongr_cur (b_ongr_id => p_ongr_id);
  FETCH ongr_cur INTO v_code;
  CLOSE ongr_cur;

  RETURN (v_code);

END sorteerkenmerk;
END kgc_ongr_00;
/

/
QUIT
