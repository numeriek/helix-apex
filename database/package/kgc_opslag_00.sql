CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPSLAG_00" IS
FUNCTION helix_persoon_info
(p_pers_id IN NUMBER DEFAULT NULL
,p_helix_id IN NUMBER DEFAULT NULL
,p_open_code IN VARCHAR2 DEFAULT NULL
,p_lengte IN VARCHAR2 DEFAULT 'LANG'
)
RETURN VARCHAR2
;
FUNCTION helix_identificatie
(p_helix_id IN NUMBER
,p_open_code IN VARCHAR2
)
RETURN VARCHAR2
;
FUNCTION opslagpositie
( p_oppo_id IN NUMBER
)
RETURN VARCHAR2;
-- parameter p_hierarchy added for Mantis 2273
FUNCTION opslagpositie
( p_open_code IN VARCHAR2
, p_helix_id IN NUMBER
, p_hierarchy VARCHAR2 DEFAULT 'N'
)
RETURN VARCHAR2;

FUNCTION opmerkingen
( p_opat_id IN NUMBER
)
RETURN VARCHAR2;

-- plaats een helix-attribuut ( fractie, monster, brief) in opslag
PROCEDURE plaats
( p_open_code IN VARCHAR2
, p_helix_id IN NUMBER
,p_aantal  IN NUMBER DEFAULT 1--Added for Mantis 2675
, x_oppo_id OUT NUMBER
, x_opslagpositie OUT VARCHAR2
);
/*Added for Mantis 2675 , ASI*/
-- plaats een helix-attribuut ( fractie van Voorrad in Cups) in opslag
PROCEDURE plaats_cup
( p_open_code IN VARCHAR2
, p_helix_id IN NUMBER
,p_aantal  IN NUMBER DEFAULT 1--Added for Mantis 2675
,p_cuty_id IN NUMBER
, x_oppo_id OUT NUMBER
, x_opslagpositie OUT VARCHAR2
);

PROCEDURE verwijder_opslag
( p_open_code IN VARCHAR2
, p_helix_id  IN NUMBER
, p_kafd_id   IN NUMBER
, p_ongr_id   IN NUMBER
);
FUNCTION voorraad
( p_frac_id IN NUMBER
, p_cuty_id IN NUMBER
)
RETURN NUMBER;
END KGC_OPSLAG_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPSLAG_00" IS
--lokaal
procedure kafd_en_ongr
( p_open_code in varchar2
, p_helix_id in number
, x_kafd_id out number
, x_ongr_id out number
)
is
  cursor frac_cur
  is
    select mons.kafd_id
    ,      mons.ongr_id
    from   kgc_monsters mons
    ,      bas_fracties frac
    where  frac.mons_id = mons.mons_id
    and    frac.frac_id = p_helix_id
    ;
  cursor mons_cur
  is
    select mons.kafd_id
    ,      mons.ongr_id
    from   kgc_monsters mons
    where  mons.mons_id = p_helix_id
    ;
  cursor brie_cur
  is
    select brie.kafd_id
    from   kgc_brieven brie
    where  brie.brie_id = p_helix_id
    ;
  cursor kwfr_cur
  is
    select mons.kafd_id
    ,      mons.ongr_id
    from   kgc_monsters mons
    ,      bas_fracties frac
    ,      kgc_kweekfracties kwfr
    where  kwfr.frac_id = frac.frac_id
    and    frac.mons_id = mons.mons_id
    and    kwfr.kwfr_id = p_helix_id
    ;


BEGIN


  if ( p_open_code = 'FRAC' )
  then
    open  frac_cur;
    fetch frac_cur
    into  x_kafd_id
    ,     x_ongr_id;
    close frac_cur;
  elsif ( p_open_code = 'MONS' )
  then
    open  mons_cur;
    fetch mons_cur
    into  x_kafd_id
    ,     x_ongr_id;
    close mons_cur;
  elsif ( p_open_code = 'BRIE' )
  then
    open  brie_cur;
    fetch brie_cur
    into  x_kafd_id;
    close brie_cur;
  elsif ( p_open_code = 'KWFR' )
  then
    open  kwfr_cur;
    fetch kwfr_cur
    into  x_kafd_id
    ,     x_ongr_id;
    close kwfr_cur;
  end if;
end kafd_en_ongr;

-- publiek
FUNCTION helix_persoon_info
( p_pers_id IN NUMBER DEFAULT NULL
, p_helix_id IN NUMBER  DEFAULT NULL
, p_open_code IN VARCHAR2 DEFAULT NULL
, p_lengte IN VARCHAR2 DEFAULT 'LANG'
)
RETURN VARCHAR2
IS
  v_pers_info VARCHAR2(2000);  /* Return value */
  v_pers_id NUMBER(10);
  v_mons_id NUMBER(10);

  CURSOR frac_cur
  IS
    SELECT mons.pers_id
    ,      mons.mons_id
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    WHERE  mons.mons_id = frac.mons_id
    AND    frac.frac_id = p_helix_id
    ;
  CURSOR mons_cur
  IS
    SELECT mons.pers_id
    FROM   kgc_monsters mons
    WHERE  mons.mons_id = p_helix_id
    ;
  CURSOR brie_cur
  IS
    SELECT brie.pers_id
    FROM   kgc_brieven brie
    WHERE  brie.brie_id = p_helix_id
    ;
  CURSOR kwfr_cur
  IS
    SELECT mons.pers_id
    ,      mons.mons_id
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    ,      kgc_kweekfracties kwfr
    WHERE  kwfr.frac_id = frac.frac_id
    and    mons.mons_id = frac.mons_id
    AND    kwfr.kwfr_id = p_helix_id
    ;
BEGIN
  IF ( p_pers_id IS NOT NULL )
  THEN
    v_pers_id   := p_pers_id;
  ELSIF ( p_helix_id IS NOT NULL )
  THEN
    IF ( p_open_code = 'FRAC' )
    THEN
      OPEN  frac_cur;
      FETCH frac_cur
      INTO  v_pers_id
      ,     v_mons_id;
      CLOSE frac_cur;
    ELSIF ( p_open_code = 'KWFR' )
    THEN
      OPEN  kwfr_cur;
      FETCH kwfr_cur
      INTO  v_pers_id
      ,     v_mons_id;
      CLOSE kwfr_cur;
    ELSIF ( p_open_code = 'BRIE' )
    THEN
      OPEN  brie_cur;
      FETCH brie_cur
      INTO  v_pers_id;
      CLOSE brie_cur;
    ELSIF ( p_open_code = 'MONS' )
    THEN
      OPEN  mons_cur;
      FETCH mons_cur
      INTO  v_pers_id;
      CLOSE mons_cur;
      v_mons_id := p_helix_id;
    END IF;
  END IF;
  IF ( v_pers_id is not null )
  then
    v_pers_info := kgc_pers_00.persoon_info
                   ( p_mons_id => v_mons_id
                   , p_onde_id => NULL
                   , p_pers_id => v_pers_id
                   , p_lengte => p_lengte
                   );
  END IF;
  RETURN (v_pers_info);
END helix_persoon_info;

FUNCTION helix_identificatie
( p_helix_id IN NUMBER
, p_open_code IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_identificatie varchar2(100);
  CURSOR frac_cur
  IS
    SELECT frac.fractienummer
    FROM   bas_fracties frac
    WHERE  frac.frac_id = p_helix_id
    ;
  CURSOR mons_cur
  IS
    SELECT mons.monsternummer
    FROM   kgc_monsters mons
    WHERE  mons.mons_id = p_helix_id
    ;
  CURSOR brie_cur
  IS
    SELECT brty.code||' '||to_char(brie.datum_print, 'dd-mm-yyyy')
    FROM   kgc_brieftypes brty
    ,      kgc_brieven brie
    WHERE  brie.brty_id = brty.brty_id
    AND    brie.brie_id = p_helix_id
    ;
  CURSOR kwfr_cur
  IS
    SELECT frac.fractienummer||' '||kwfr.code
    FROM   bas_fracties frac
    ,      kgc_kweekfracties kwfr
    WHERE  kwfr.frac_id = frac.frac_id
    AND    kwfr.kwfr_id = p_helix_id
    ;
BEGIN
  IF ( p_helix_id IS NOT NULL )
  THEN
    IF ( p_open_code = 'MONS' )
    THEN
      open  mons_cur;
      fetch mons_cur
      into  v_identificatie;
      close mons_cur;
    ELSIF ( p_open_code = 'FRAC' )
    THEN
      open  frac_cur;
      fetch frac_cur
      into  v_identificatie;
      close frac_cur;
    ELSIF ( p_open_code = 'BRIE' )
    THEN
      open  brie_cur;
      fetch brie_cur
      into  v_identificatie;
      close brie_cur;
    ELSIF ( p_open_code = 'KWFR' )
    THEN
      open  kwfr_cur;
      fetch kwfr_cur
      into  v_identificatie;
      close kwfr_cur;
    END IF;
  END IF;
  RETURN (v_identificatie);
END helix_identificatie;

--- Created by Kartiki for mantis 2273  -------

FUNCTION opslagpositie_hier
( p_oppo_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_opslagpositie  VARCHAR2(32767);

  CURSOR oppo_cur
  IS
    SELECT opel.code  code ,
           nvl( oppo.code
              , kgc_oppo_00.opslagpositie( opel.opel_id, x_positie, 'X' )||','||
                kgc_oppo_00.opslagpositie( opel.opel_id, y_positie, 'Y' )
              ) posi
    FROM  kgc_opslag_elementen opel
    ,     kgc_opslag_posities  oppo
    WHERE opel.opel_id = oppo.opel_id
    AND   oppo.oppo_id = p_oppo_id
    ;

    rec_oppo oppo_cur%ROWTYPE;

     CURSOR h_cur (p_code VARCHAR2)
     IS
      SELECT opel.omschrijving
      FROM kgc_opslag_elementen opel
      START WITH opel.code = p_code
      CONNECT BY  opel.opel_id = PRIOR opel.parent_opel_id;


    position VARCHAR2(32767);
    add_position VARCHAR2(32767);
--    first_position NUMBER := 1;
BEGIN
  OPEN  oppo_cur;
  FETCH oppo_cur
  INTO  rec_oppo;
  IF rec_oppo.code IS not NULL THEN
     OPEN h_cur (rec_oppo.code);
     LOOP
       FETCH h_cur INTO add_position;
       EXIT WHEN h_cur%NOTFOUND;
       IF position is NULL  THEN
               position := add_position;
       ELSE
           --    position := position || '; ' || add_position; -- old line before the issue raise by customer on 10/12/2012 to change the order of display
              position := add_position || '; ' || position  ; -- changed  line
       END IF;
     end LOOP;
     CLOSE h_cur;

      v_opslagpositie := rec_oppo.code || ' : ' || rec_oppo.posi ||  ' ( ' || position  ||  ' : ' || rec_oppo.posi || ' )' ;
  end IF;

  CLOSE oppo_cur;
   RETURN (v_opslagpositie);
END opslagpositie_hier;

-- Mantis 2273 ends here    ---------------------------


FUNCTION opslagpositie
( p_oppo_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_opslagpositie  VARCHAR2(32767);

  CURSOR oppo_cur
  IS
    SELECT opel.code||': '||
           nvl( oppo.code
              , kgc_oppo_00.opslagpositie( opel.opel_id, x_positie, 'X' )||','||
                kgc_oppo_00.opslagpositie( opel.opel_id, y_positie, 'Y' )
              )
    FROM  kgc_opslag_elementen opel
    ,     kgc_opslag_posities  oppo
    WHERE opel.opel_id = oppo.opel_id
    AND   oppo.oppo_id = p_oppo_id
    ;
BEGIN
  OPEN  oppo_cur;
  FETCH oppo_cur
  INTO  v_opslagpositie;
  CLOSE oppo_cur;
  RETURN( v_opslagpositie );
END opslagpositie;

-- overloaded
-- New parameter 'p_hierarchy' added by Kartiki for Mantis 2273
FUNCTION opslagpositie
( p_open_code IN VARCHAR2
, p_helix_id IN NUMBER
, p_hierarchy VARCHAR2 DEFAULT 'N'
)
RETURN VARCHAR2
IS
  v_oppo_id number;
  v_opslagpositie  VARCHAR2(32767);

  CURSOR opin_cur
  IS
  SELECT opin.oppo_id
  FROM   kgc_opslag_attributen opat
  ,      kgc_opslag_entiteiten open
  ,      kgc_opslag_inhoud     opin
  WHERE  opin.opat_id = opat.opat_id
  AND    open.open_id = opat.open_id
  AND    open.code = p_open_code
  AND    opat.helix_id = p_helix_id
  ORDER BY opat.volgnr desc -- laatste eerst
  ;
BEGIN
  OPEN  opin_cur;
  FETCH opin_cur
  INTO  v_oppo_id;
  CLOSE opin_cur;
-- Kartiki commented and added IF condition for Mantis 2273
--  v_opslagpositie := opslagpositie ( p_oppo_id => v_oppo_id );
  IF (p_hierarchy = 'N') THEN
   v_opslagpositie := opslagpositie ( p_oppo_id => v_oppo_id );
  ELSE
   v_opslagpositie := opslagpositie_hier ( p_oppo_id => v_oppo_id );
  END IF;
-- Till here for Mantis 2273

 RETURN(v_opslagpositie);
END opslagpositie;

FUNCTION opmerkingen
( p_opat_id IN NUMBER
)
RETURN VARCHAR2
IS
  cursor c_opm (p_opat_id in number)
  is
  select opmerkingen, jn_operation, jn_date_time
  from   kgc_opslaghistorie_vw
  where  opat_id = p_opat_id
  order by jn_date_time desc
  ;

  l_v_opmerkingen varchar2(4000);
begin
  for r_opm in c_opm(p_opat_id)
  loop
    if r_opm.opmerkingen is not null and l_v_opmerkingen is not null
        then
      l_v_opmerkingen := l_v_opmerkingen||chr(10)||r_opm.jn_operation||' '||to_char(r_opm.jn_date_time,'dd-mm-yyyy')||': '||r_opm.opmerkingen;
    elsif r_opm.opmerkingen is not null and l_v_opmerkingen is null
        then
      l_v_opmerkingen := r_opm.jn_operation||' '||to_char(r_opm.jn_date_time,'dd-mm-yyyy')||': '||r_opm.opmerkingen;
        end if;
  end loop;

  RETURN( substr(l_v_opmerkingen,1,2000) );
END opmerkingen;

FUNCTION genereer_opat
(p_helix_id IN NUMBER
,p_open_code IN VARCHAR2
)
RETURN NUMBER
IS
  opat_trow kgc_opslag_attributen%ROWTYPE;

  CURSOR open_cur IS
    SELECT open.open_id
    FROM   kgc_opslag_entiteiten open
    WHERE  open.code = upper( p_open_code )
    ;
  CURSOR opat_cur
  ( b_open_id in number
  )
  IS
    SELECT nvl( max( opat.volgnr ), 0 ) + 1
    FROM   kgc_opslag_attributen opat
    WHERE  opat.open_id = b_open_id
    AND    opat.helix_id = p_helix_id
    ;
BEGIN
  OPEN  open_cur;
  FETCH open_cur
  INTO  opat_trow.open_id;
  CLOSE open_cur;

  OPEN  opat_cur( b_open_id => opat_trow.open_id );
  FETCH opat_cur
  INTO  opat_trow.volgnr;
  CLOSE opat_cur;
  BEGIN
    INSERT INTO kgc_opslag_attributen
    ( opat_id
    , open_id
    , helix_id
    , volgnr
    )
  VALUES
    ( kgc_opat_seq.nextval
    , opat_trow.open_id
    , p_helix_id
    , opat_trow.volgnr
    )
  returning opat_id INTO opat_trow.opat_id
  ;
  END;
  RETURN( opat_trow.opat_id);
END genereer_opat;

PROCEDURE plaats
( p_open_code IN VARCHAR2
, p_helix_id IN NUMBER
,p_aantal  IN NUMBER DEFAULT 1--Added for Mantis 2675
, x_oppo_id OUT NUMBER
, x_opslagpositie OUT VARCHAR2
)
IS
  v_kafd_id   number;
  v_ongr_id   number;
  v_opty_code varchar2(10);
  v_opat_id   number;
  v_oppo_id   number;
  v_opin_id   number;
  v_mede_id   number;
  v_user      varchar2(40) := USER;
  v_open_code VARCHAR2(10);--Added for Mantis 2675
  v_antaal NUMBER;--Added for Mantis 2675
  v_helix_id NUMBER;--Adde for Mantis 2675
  x_opty_code  VARCHAR2(10);
  v_cup_type   VARCHAR2(4000);

  CURSOR opel_cur
  IS
    SELECT opel.opel_id
    ,      opel.code
    ,      opel.aantal_x_pos
    FROM   kgc_opslag_elementen  opel
    ,      kgc_opslag_elem_typen opet
    ,      kgc_opslag_typen      opty
    WHERE  opty.code = v_opty_code
    AND    opty.opty_id = opet.opty_id
    AND    opet.opel_id = opel.opel_id
    AND    opel.vervallen = 'N'
    AND    kgc_opau_00.mag_actie_uitvoeren( opel.opel_id
                                          , v_user
                                          , 'I' -- Inzetten
                                          ) = 1
    and    exists
           ( select null
             from   kgc_opslag_posities oppo
             where  oppo.opel_id = opel.opel_id
             and    oppo.beschikbaar = 'J'
           )
    AND    not exists
           ( select null
             from   kgc_opslag_inhoud_jn opinj
             ,      kgc_opslag_posities   oppo2
             ,      kgc_opslag_elem_typen opet2
             ,      kgc_opslag_elementen opel2
             where  opinj.oppo_id = oppo2.oppo_id
             and    oppo2.opel_id = opel2.opel_id
             and    opel2.opel_id = opet2.opel_id
             and    opet2.opty_id = opty.opty_id
             and    opel2.vervallen = 'N'
             and    opel2.code > opel.code
           )
    ORDER BY opel.code
    ;
  opel_rec opel_cur%rowtype;
      /*Added for Mantis 2675 to fetch Opslag type*/
			/*For Monsters*/
	--Changed mate.opslag_type to opty.code by Rajendra for Mantis 2675 Rw(02-07-2014)
  CURSOR mons_cur  IS
SELECT opty.code  FROM kgc_monsters mons
                                 ,kgc_materialen mate
                                 ,kgc_opslag_typen opty
                                 ,kgc_kgc_afdelingen kafd
WHERE     mate.mate_id=mons.mate_id
AND           mate.opty_id=opty.opty_id
AND           kafd.kafd_id=mate.kafd_id
-- AND           mate.vervallen='N' commented for Rework 2675-3(issue 2) Kartiki 19/12/2013
AND           opty.vervallen='N'
AND           mons.mons_id=v_helix_id ;

			/*For Fractions*/
	--Changed frty.opslag_type to opty.code by Rajendra for Mantis 2675 Rw(02-07-2014)
       CURSOR frac_cur  IS
SELECT opty.code
              FROM bas_fracties frac
                           ,bas_fractie_types frty
                           ,kgc_opslag_typen opty
                           ,kgc_kgc_afdelingen kafd
                           ,kgc_monsters mons
WHERE frac.frty_id=frty.frty_id
AND      frac.kafd_id=kafd.kafd_id
AND     frac.mons_id=mons.mons_id
AND     frty.ongr_id=mons.ongr_id
AND       frty.opty_id=opty.opty_id
-- AND       frty.vervallen='N' commented for Rework 2675-3(issue 2) Kartiki 19/12/2013
AND       opty.vervallen='N'
AND       frac.frac_id=v_helix_id;

			/* For Kweeken */
--Changed kwty.opslag_type to opty.code by Rajendra for Mantis 2675 Rw(02-07-2014)
			CURSOR kweek_cur  IS
SELECT DISTINCT   opty.code
                   FROM  kgc_kweekfracties kwfr
                                 ,kgc_kweektypes     kwty
                                 ,kgc_opslag_typen opty
                                 ,bas_fracties frac
                                 ,kgc_monsters mons
                                 ,kgc_kgc_afdelingen kafd
                                 ,kgc_materialen mate
                                ,kgc_indicatie_groepen ingr
                                 ,kgc_indicatie_teksten indt
WHERE    kwfr.kwty_id=kwty.kwty_id
AND          kwty.opty_id=opty.opty_id
AND          mons.mons_id=frac.mons_id
AND          mate.mate_id=mons.mate_id
AND          kwfr.frac_id=frac.frac_id
AND          kwty.kafd_id=kafd.kafd_id
AND          (kwty.mate_id=mate.mate_id  OR kwty.mate_id IS NULL)
AND          (mons.ongr_id=ingr.ongr_id )
AND         (kwty.indi_id=indt.indi_id OR kwty.indi_id IS NULL)
--- AND          kwty.vervallen='N' commented for Rework 2675-3(issue 2) Kartiki 19/12/2013
AND          opty.vervallen='N'
AND          kwfr.kwfr_id=  v_helix_id;


			/*For Voorraad*/
--Changed cuty.opslag_type to opty.code by Rajendra for Mantis 2675 Rw(02-07-2014)
			CURSOR voor_cur  IS
SELECT  opty.code ,cuty.code
                    FROM kgc_cup_types cuty
                        ,bas_fracties frac
                        ,bas_voorraad voor
                                 ,kgc_opslag_typen opty
WHERE      voor.frac_id=frac.frac_id
AND        voor.cuty_id=cuty.cuty_id
AND        cuty.opty_id=opty.opty_id
-- AND        cuty.vervallen='N' commented for Rework 2675-3(issue 2) Kartiki 19/12/2013
AND        opty.vervallen='N'
AND        frac.frac_id= v_helix_id;
 /*Change for Mantis ends here to fetch Opslag type */
  function volgende_vrije_positie
  ( b_opel_id in number
  , b_aantal_x_pos in number
  )
  return number -- oppo_id
  is
    l_oppo_id number := null;
    -- hoogste (ooit) gevulde positie
    cursor oppo_cur
    is
      select ( ( oppo.y_positie - 1 )
             * b_aantal_x_pos
             )
             + oppo.x_positie pos_volgnr
      from   kgc_opslag_inhoud_jn opinj
      ,      kgc_opslag_posities   oppo
      where  opinj.oppo_id = oppo.oppo_id
      and    oppo.opel_id = b_opel_id
      and    oppo.beschikbaar = 'J'
      order by 1 desc
      ;
    l_positie_volgnr number;
    -- volgende beschikbare positie
    cursor vrij_cur
    ( b_volgnr in number
    )
    is
      select oppo.oppo_id
      from   kgc_opslag_posities   oppo
      where  oppo.opel_id = b_opel_id
      and    oppo.beschikbaar = 'J'
      and    ( ( oppo.y_positie - 1 )
             * b_aantal_x_pos
             )
             + oppo.x_positie > b_volgnr
      order by ( ( oppo.y_positie - 1 )
               * b_aantal_x_pos
               )
               + oppo.x_positie asc
      ;

  begin
    open  oppo_cur;
    fetch oppo_cur
    into  l_positie_volgnr;
    close oppo_cur;
    open  vrij_cur(  nvl( l_positie_volgnr, 0 ) );
    fetch vrij_cur
    into  l_oppo_id;
    close vrij_cur;
    return( l_oppo_id );
  end volgende_vrije_positie;

BEGIN
  v_open_code:=p_open_code;--Added for Mantis 2675
  v_antaal:=p_aantal;--Added for Mantis 2675
  v_helix_id:=p_helix_id;--Added for Mantis 2675
  /*Added for rework 2675 , start ,ASI*/
      kafd_en_ongr
  ( p_open_code => p_open_code
  , p_helix_id => p_helix_id
  , x_kafd_id => v_kafd_id
  , x_ongr_id => v_ongr_id
  );
     /*Added for rework 2675 , end ,ASI*/
/*This code needs to changed as depending uopn the entity type Opslag type will be fetched*/
  -- Ophalen van het opslag element type waarin gegenereerd moet worden
/*  v_opty_code := kgc_sypa_00.systeem_waarde
                 ( p_parameter_code => 'OPSLAG_TYPE_VOOR_AUTO_VULLING'
                 , p_kafd_id => v_kafd_id
                 , p_ongr_id => v_ongr_id
                 );*/

/*This code needs to changed as depending uopn the entity type Opslag type will be fetched*/--Abhijit

IF  v_open_code='MONS'
      THEN
	     OPEN mons_cur ;
		 FETCH mons_cur INTO x_opty_code;

		 IF mons_cur%NOTFOUND
		   THEN
		         qms$errors.show_message
		 ( p_mesg => 'KGC-11190'
		 );
		  END IF;
		   v_opty_code:=x_opty_code;

 ELSIF  v_open_code='FRAC'
      THEN
	     OPEN frac_cur ;
		 FETCH frac_cur INTO x_opty_code;

		 IF frac_cur%NOTFOUND
		   THEN
		         qms$errors.show_message
		 ( p_mesg => 'KGC-11190'
		 );
		  END IF;
		   v_opty_code:=x_opty_code;

  ELSIF  v_open_code='KWFR'
      THEN
	     OPEN kweek_cur ;
		 FETCH kweek_cur INTO x_opty_code;

		 IF kweek_cur%NOTFOUND
		   THEN
		         qms$errors.show_message
		 ( p_mesg => 'KGC-11190'
		 );
		  END IF;
		   v_opty_code:=x_opty_code;

  ELSIF  v_open_code='VOOR'
      THEN

	     OPEN voor_cur ;
		 FETCH voor_cur INTO x_opty_code ,v_cup_type;
--		 v_open_code:='FRAC';--Added for rework 2675, ASI
		 IF voor_cur%NOTFOUND
		   THEN
		         qms$errors.show_message
		 ( p_mesg => 'KGC-11190'
		 );
		  END IF;
		   v_opty_code:=x_opty_code;
  END IF;
  -- Ophalen opslag_elementen met dit type voor auto vulling
  -- waarvoor gebruiker ge-autoriseerd is
FOR i IN 1..v_antaal--Added for Mantis 2675
  LOOP --Added for Mantis 2675*/

  OPEN  opel_cur;
  FETCH opel_cur
  into  opel_rec;
  v_oppo_id := volgende_vrije_positie
               ( b_opel_id => opel_rec.opel_id
               , b_aantal_x_pos => opel_rec.aantal_x_pos
               );

  IF ( v_oppo_id IS NULL )
   THEN
    -- Niet gevonden op dit element, pak het volgende (per definitie leeg)
    FETCH  opel_cur
    into   opel_rec;
    v_oppo_id  := volgende_vrije_positie
                  ( b_opel_id => opel_rec.opel_id
                  , b_aantal_x_pos => opel_rec.aantal_x_pos
                  );
  END IF;
  if ( opel_cur%isopen )
  then
    close opel_cur;
  end if;
  IF v_oppo_id IS NOT NULL
  THEN
     -- Aanmaken nieuw attribuut (volgnr)

       v_opat_id := genereer_opat
                  ( p_open_code =>p_open_code
                  , p_helix_id => p_helix_id
                  );
     v_mede_id := kgc_mede_00.medewerker_id;

     BEGIN
     INSERT INTO kgc_opslag_inhoud
       ( oppo_id
       , opat_id
       , mede_id_geplaatst
       )
       VALUES
       ( v_oppo_id
       , v_opat_id
       , v_mede_id
       );

       -- returnwaarden, id en beschrijving:
       x_oppo_id := v_oppo_id;
       x_opslagpositie := opslagpositie
                          ( p_oppo_id => v_oppo_id
                          );

       commit;
     END;
  ELSE
    -- KGC-11190: Er kan geen vrije opslag positie bepaald worden
     --            (Waarschuw de applicatiebeheerder).

     qms$errors.show_message
     ( p_mesg => 'KGC-11190'
     );

  END IF;
  END LOOP;--Added for Mantis 2675
--END LOOP;--Added for Mantis 2675


END plaats;

PROCEDURE verwijder_opslag
( p_open_code IN VARCHAR2
, p_helix_id IN NUMBER
, p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER
)
IS
  l_n_open_id               number;
  l_v_opslag_verwijderen_jn varchar2(1);
  l_v_check_opat            varchar2(1);
  l_n_kafd_id               number;
  l_n_ongr_id               number;
  l_n_opat_id               number;
  v_user                    VARCHAR2(40) := USER;

  cursor open_cur
  ( b_open_code in varchar2
  )
  is
    select open_id
    from   kgc_opslag_entiteiten open
    where  open.code = b_open_code
    ;
  cursor opin_cur
  ( b_open_id  in number
  , b_helix_id in number
  )
  is
    select 'N'
    from   kgc_opslag_attributen opat
    ,      kgc_opslag_inhoud     opin
    ,      kgc_opslag_posities   oppo
    where  opat.open_id  = b_open_id
    and    opat.helix_id = b_helix_id
    and    opat.opat_id  = opin.opat_id
    and    opin.oppo_id  = oppo.oppo_id
    and    kgc_opau_00.mag_actie_uitvoeren
           ( oppo.opel_id
           , v_user
           , 'U' -- Uithalen (ja, had D moeten zijn)
           ) = 0
    ;
  cursor opat_cur
  ( b_open_id  in number
  , b_helix_id in number
  )
  is
    select opat.opat_id
    from   kgc_opslag_attributen opat
    where  opat.open_id  = b_open_id
    and    opat.helix_id = b_helix_id
    ;
BEGIN
  kafd_en_ongr
  ( p_open_code => p_open_code
  , p_helix_id  => p_helix_id
  , x_kafd_id   => l_n_kafd_id
  , x_ongr_id   => l_n_ongr_id
  );
  -- 1. Ophalen van de systeemparameter dat bepaald of opslag verwijderd mag worden
  l_v_opslag_verwijderen_jn := kgc_sypa_00.systeem_waarde
                               ( p_parameter_code => 'OPSLAG_VERWIJDEREN'
                               , p_kafd_id => nvl(p_kafd_id,l_n_kafd_id)
                               , p_ongr_id => nvl(p_ongr_id,l_n_ongr_id)
                               );
  -- haal de OPEN_ID op die je later nodig hebt
  open  open_cur( b_open_code => p_open_code );
  fetch open_cur into l_n_open_id;
  close open_cur;
  -- 2. bepaal of er een opslag bestaat die gekoppeld is maar niet verwijderd mag worden
  open  opin_cur( b_open_id => l_n_open_id
                     , b_helix_id => p_helix_id
                );
  fetch opin_cur
  into  l_v_check_opat;
  close opin_cur;

  -- bepaal of er opslag bestaat met meegeleverde helix_id
  open  opat_cur( b_open_id => l_n_open_id
                     , b_helix_id => p_helix_id
                );
  fetch opat_cur into l_n_opat_id;
  close opat_cur;

  -- de gebruiker moet gerechtigd zijn om opslag te verwijderen:
  -- 1. De systeemparameter kan op N staan voor de gebruiker
  -- 2. De gebruiker is niet geautoriseerd volgens OPAU opslag autorisatie
  if l_v_opslag_verwijderen_jn = 'N' and l_n_opat_id is not null
  then
     -- KGC-: Opslag mag niet verwijderd worden
     qms$errors.show_message
     ( p_mesg => 'KGC-00181'
         , p_param0 => 'de systeemparameter laat dit niet toe voor deze gebruiker'
         , p_param1 => sqlerrm
         , p_errtp => 'E'
         , p_rftf => true
         );
  elsif l_v_check_opat = 'N'
  then
     -- KGC-: Opslag mag niet verwijderd worden
     qms$errors.show_message
     ( p_mesg => 'KGC-00181'
         , p_param0 => 'de opslag autorisatie laat dit niet toe voor deze gebruiker'
         , p_param1 => sqlerrm
         , p_errtp => 'E'
         , p_rftf => true
         );
  else
    -- Loop haalt alle opats op die verwijderd moeten worden
    for r_opat_cur in opat_cur( b_open_id  => l_n_open_id
                                   , b_helix_id => p_helix_id
                              )
    loop
      delete
      from   kgc_opslag_inhoud opin
      where  opin.opat_id = r_opat_cur.opat_id
      ;
      delete
      from   kgc_opslag_attributen opat
      where  opat.opat_id  = r_opat_cur.opat_id
      ;
    end loop;
  end if;

END verwijder_opslag;
FUNCTION voorraad
( p_frac_id IN NUMBER
, p_cuty_id IN NUMBER
)
RETURN NUMBER
IS
  -- oorspronkelijke voorraad
  CURSOR voor_cur
  IS
    SELECT SUM( hoeveelheid )
    FROM   bas_voorraad
    WHERE  frac_id = p_frac_id
    AND    cuty_id    = p_cuty_id
    ;
  v_voorraad NUMBER;
  -- al gereserveerde hoeveelheid
  CURSOR voor_seq_cur
  IS
   SELECT COUNT(1)
FROM   bas_voorraad          voor
              ,kgc_opslag_attributen opat
WHERE  opat.helix_id = voor.frac_id
AND    voor.frac_id = p_frac_id
AND    voor.cuty_id = p_cuty_id
    ;
  v_gereserveerd NUMBER;
BEGIN
  OPEN  voor_cur;
  FETCH voor_cur
  INTO  v_voorraad;
  CLOSE voor_cur;
  IF ( v_voorraad IS NOT NULL )
  THEN
    OPEN  voor_seq_cur;
    FETCH voor_seq_cur
    INTO  v_gereserveerd;
    CLOSE voor_seq_cur;
  END IF;
  RETURN( v_voorraad - NVL( v_gereserveerd, 0 ) );
END voorraad;
PROCEDURE plaats_cup
( p_open_code IN VARCHAR2
, p_helix_id IN NUMBER
,p_aantal  IN NUMBER DEFAULT 1--Added for Mantis 2675
,p_cuty_id IN NUMBER
, x_oppo_id OUT NUMBER
, x_opslagpositie OUT VARCHAR2
)
IS
  v_kafd_id   number;
  v_ongr_id   number;
  v_opty_code varchar2(10);
  v_opat_id   number;
  v_oppo_id   number;
  v_opin_id   number;
  v_mede_id   number;
  v_user      varchar2(40) := USER;
  v_open_code VARCHAR2(10);--Added for Mantis 2675
  v_antaal NUMBER;--Added for Mantis 2675
  v_helix_id NUMBER;--Adde for Mantis 2675
  x_opty_code  VARCHAR2(10);
  v_cup_type   VARCHAR2(4000);
  x_cuty_id       NUMBER;

  CURSOR opel_cur
  IS
    SELECT opel.opel_id
    ,      opel.code
    ,      opel.aantal_x_pos
    FROM   kgc_opslag_elementen  opel
    ,      kgc_opslag_elem_typen opet
    ,      kgc_opslag_typen      opty
    WHERE  opty.code = v_opty_code
    AND    opty.opty_id = opet.opty_id
    AND    opet.opel_id = opel.opel_id
    AND    opel.vervallen = 'N'
    AND    kgc_opau_00.mag_actie_uitvoeren( opel.opel_id
                                          , v_user
                                          , 'I' -- Inzetten
                                          ) = 1
    and    exists
           ( select null
             from   kgc_opslag_posities oppo
             where  oppo.opel_id = opel.opel_id
             and    oppo.beschikbaar = 'J'
           )
    AND    not exists
           ( select null
             from   kgc_opslag_inhoud_jn opinj
             ,      kgc_opslag_posities   oppo2
             ,      kgc_opslag_elem_typen opet2
             ,      kgc_opslag_elementen opel2
             where  opinj.oppo_id = oppo2.oppo_id
             and    oppo2.opel_id = opel2.opel_id
             and    opel2.opel_id = opet2.opel_id
             and    opet2.opty_id = opty.opty_id
             and    opel2.vervallen = 'N'
             and    opel2.code > opel.code
           )
    ORDER BY opel.code
    ;
  opel_rec opel_cur%rowtype;
      /*Added for Mantis 2675 to fetch Opslag type*/
     --Changed cuty.opslag_type to opty.code by Rajendra for Mantis 2675 Rw(02-07-2014)
		/*For Voorraad*/
			CURSOR voor_cur  IS
SELECT  opty.code ,cuty.code
                    FROM kgc_cup_types cuty
                        ,bas_fracties frac
                        ,bas_voorraad voor
                                 ,kgc_opslag_typen opty
WHERE      voor.frac_id=frac.frac_id
AND        voor.cuty_id=cuty.cuty_id
AND        cuty.opty_id=opty.opty_id
-- AND        cuty.vervallen='N' commented for Rework 2675-3(issue 2) Kartiki 26/12/2013
AND        opty.vervallen='N'
AND        voor.cuty_id=x_cuty_id
AND        frac.frac_id= v_helix_id;
 /*Change for Mantis ends here to fetch Opslag type */
  function volgende_vrije_positie
  ( b_opel_id in number
  , b_aantal_x_pos in number
  )
  return number -- oppo_id
  is
    l_oppo_id number := null;
    -- hoogste (ooit) gevulde positie
    cursor oppo_cur
    is
      select ( ( oppo.y_positie - 1 )
             * b_aantal_x_pos
             )
             + oppo.x_positie pos_volgnr
      from   kgc_opslag_inhoud_jn opinj
      ,      kgc_opslag_posities   oppo
      where  opinj.oppo_id = oppo.oppo_id
      and    oppo.opel_id = b_opel_id
      and    oppo.beschikbaar = 'J'
      order by 1 desc
      ;
    l_positie_volgnr number;
    -- volgende beschikbare positie
    cursor vrij_cur
    ( b_volgnr in number
    )
    is
      select oppo.oppo_id
      from   kgc_opslag_posities   oppo
      where  oppo.opel_id = b_opel_id
      and    oppo.beschikbaar = 'J'
      and    ( ( oppo.y_positie - 1 )
             * b_aantal_x_pos
             )
             + oppo.x_positie > b_volgnr
      order by ( ( oppo.y_positie - 1 )
               * b_aantal_x_pos
               )
               + oppo.x_positie asc
      ;

  begin
    open  oppo_cur;
    fetch oppo_cur
    into  l_positie_volgnr;
    close oppo_cur;
    open  vrij_cur(  nvl( l_positie_volgnr, 0 ) );
    fetch vrij_cur
    into  l_oppo_id;
    close vrij_cur;
    return( l_oppo_id );
  end volgende_vrije_positie;

BEGIN
  v_open_code:=p_open_code;--Added for Mantis 2675
  v_antaal:=p_aantal;--Added for Mantis 2675
  v_helix_id:=p_helix_id;--Added for Mantis 2675
  x_cuty_id:=p_cuty_id;
  /*Added for rework 2675 , start ,ASI*/
  kafd_en_ongr
  ( p_open_code => 'FRAC'
  , p_helix_id => p_helix_id
  , x_kafd_id => v_kafd_id
  , x_ongr_id => v_ongr_id
  );

     /*Added for rework 2675 , end ,ASI*/
/*This code needs to changed as depending uopn the entity type Opslag type will be fetched*/
  -- Ophalen van het opslag element type waarin gegenereerd moet worden
/*  v_opty_code := kgc_sypa_00.systeem_waarde
                 ( p_parameter_code => 'OPSLAG_TYPE_VOOR_AUTO_VULLING'
                 , p_kafd_id => v_kafd_id
                 , p_ongr_id => v_ongr_id
                 );*/

/*This code needs to changed as depending uopn the entity type Opslag type will be fetched*/--Abhijit

IF  v_open_code='VOOR'
      THEN

	     OPEN voor_cur ;
		 FETCH voor_cur INTO x_opty_code ,v_cup_type;
--		 v_open_code:='FRAC';--Added for rework 2675, ASI
		 IF voor_cur%NOTFOUND
		   THEN
		         qms$errors.show_message
		 ( p_mesg => 'KGC-11190'
		 );
		  END IF;
		   v_opty_code:=x_opty_code;
  END IF;
  -- Ophalen opslag_elementen met dit type voor auto vulling
  -- waarvoor gebruiker ge-autoriseerd is
FOR i IN 1..v_antaal--Added for Mantis 2675
  LOOP --Added for Mantis 2675*/

  OPEN  opel_cur;
  FETCH opel_cur
  into  opel_rec;
  v_oppo_id := volgende_vrije_positie
               ( b_opel_id => opel_rec.opel_id
               , b_aantal_x_pos => opel_rec.aantal_x_pos
               );

  IF ( v_oppo_id IS NULL )
   THEN
    -- Niet gevonden op dit element, pak het volgende (per definitie leeg)
    FETCH  opel_cur
    into   opel_rec;
    v_oppo_id  := volgende_vrije_positie
                  ( b_opel_id => opel_rec.opel_id
                  , b_aantal_x_pos => opel_rec.aantal_x_pos
                  );
  END IF;
  if ( opel_cur%isopen )
  then
    close opel_cur;
  end if;
  IF v_oppo_id IS NOT NULL
  THEN
     -- Aanmaken nieuw attribuut (volgnr)
         /*Added for rework 2675 , start ,ASI*/

       v_opat_id := genereer_opat
                  ( p_open_code => 'FRAC'
                  , p_helix_id => p_helix_id
                  );
     v_mede_id := kgc_mede_00.medewerker_id;

     BEGIN
       INSERT INTO kgc_opslag_inhoud
       ( oppo_id
       , opat_id
       , mede_id_geplaatst
       , opmerkingen
       )
       VALUES
       ( v_oppo_id
       , v_opat_id
       , v_mede_id
       , 'Cuptype:'||v_cup_type
       );

       -- returnwaarden, id en beschrijving:
       x_oppo_id := v_oppo_id;
       x_opslagpositie := opslagpositie
                          ( p_oppo_id => v_oppo_id
                          );

       commit;
     END;
  ELSE
    -- KGC-11190: Er kan geen vrije opslag positie bepaald worden
     --            (Waarschuw de applicatiebeheerder).

     qms$errors.show_message
     ( p_mesg => 'KGC-11190'
     );

  END IF;
  END LOOP;--Added for Mantis 2675
--END LOOP;--Added for Mantis 2675


END plaats_cup;
END KGC_OPSLAG_00;
/

/
QUIT
