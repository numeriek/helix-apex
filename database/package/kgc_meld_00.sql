CREATE OR REPLACE PACKAGE "HELIX"."KGC_MELD_00" IS
-- PL/SQL Specification
-- maak een melding voor het systeembeheer
FUNCTION maak_melding
RETURN  VARCHAR2;
END KGC_MELD_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MELD_00" IS
FUNCTION maak_melding
RETURN VARCHAR2
IS
  v_meld       VARCHAR2 (1000); -- melding uit de database
  v_tekst      VARCHAR2 (2000); -- te tonen tekst
  v_uur        NUMBER;
  v_dag        VARCHAR2 (10);
  v_database   VARCHAR2(100);

  -- welke database (test/productie)
  CURSOR db_cur
  IS
    SELECT SUBSTR(global_name,1,100)
    FROM   global_name
    ;
-- melding zoeken op dagnummer
CURSOR meld_cur
( p_dag IN VARCHAR2
)
IS
   SELECT omschrijving
   FROM   kgc_mededelingen
   WHERE  dag = p_dag
   AND    uitsluiten = 'N';

-- melding zoeken op volgnummer
CURSOR zoek_meld IS
   SELECT omschrijving
   FROM   kgc_mededelingen
   WHERE  NVL( dag, 'klein' ) NOT IN( 'MAANDAG','DINSDAG','WOENSDAG','DONDERDAG','VRIJDAG','ZATERDAG','ZONDAG')
   AND    uitsluiten = 'N'
   ORDER BY volgnummer;

-- PL/SQL Block
BEGIN
  -- bepaal database
  OPEN  db_cur;
  FETCH db_cur
  INTO  v_database;
  CLOSE db_cur;

  -- aanhef bepalen goedemorgen of middag
  v_uur := TO_NUMBER(TO_CHAR(SYSDATE,'HH24'));
  IF v_uur < 12
  THEN
    v_tekst := 'Goedenmorgen';
  ELSIF v_uur > 18
  THEN
    v_tekst := 'Goedenavond';
  ELSE
    v_tekst := 'Goedenmiddag';
  END IF;

  -- wat voor dag is het
  v_dag := TO_CHAR(SYSDATE,'D');
  IF v_dag = '2'
  THEN
    v_dag := 'MAANDAG';
  ELSIF v_dag = '3'
  THEN
    v_dag := 'DINSDAG';
  ELSIF v_dag = '4'
  THEN
    v_dag := 'WOENSDAG';
  ELSIF v_dag = '5'
  THEN
    v_dag := 'DONDERDAG';
  ELSIF v_dag = '6'
  THEN
    v_dag := 'VRIJDAG';
  ELSIF v_dag = '7'
  THEN
    v_dag := 'ZATERDAG';
  ELSIF v_dag = '1'
  THEN
    v_dag := 'ZONDAG';
  END IF;

  -- voeg naam en datum toe toe
  v_tekst := v_tekst||', '|| kgc_mede_00.medewerker_naam||'.'||CHR(10)
           ||'Het is '||LOWER(v_dag)||' '||TO_CHAR(SYSDATE,'dd-mm-yyyy');

  -- voeg database toe
  v_tekst := v_tekst||CHR(10)
           ||'U maakt gebruik van database '||NVL(v_database,'???')||'.';

  -- nieuwe regel
  v_tekst := v_tekst||CHR(10);
  -- zoeken van de tekst bij de dag en kijken of die inderdaad getoond mag worden
  -- (uitsluiten = N)
  FOR r IN meld_cur( UPPER(v_dag) )
  LOOP
    v_tekst := v_tekst || CHR(10) || r.omschrijving;
  END LOOP;

  -- Nu kijken of er extra teksten getoond moeten worden
  FOR r IN zoek_meld
  LOOP
    v_tekst := v_tekst || CHR(10) || r.omschrijving;
  END LOOP;

  RETURN (v_tekst);
END maak_melding;

END  KGC_MELD_00;
/

/
QUIT
