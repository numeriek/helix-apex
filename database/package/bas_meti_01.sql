CREATE OR REPLACE PACKAGE "HELIX"."BAS_METI_01" IS
 --Mantis 11560 SKA 26-Apr-2016 New parameter p_source_frac_id is added in procedure  protocol_aanmelden release 8.11.2.6
 --Mantis 11560 SKA 01-Feb-2016 Package created for screen BASMETI01 copy functionality release 8.9.1.4.
PROCEDURE protocol_aanmelden
( p_onmo_id IN NUMBER
, p_stgr_id IN NUMBER
, p_frac_id IN NUMBER := NULL
, p_source_frac_id IN NUMBER := NULL -- Added by SKA for mantis 11560 on 26-Apr-2016 for release 8.11.2.6
, p_standaard IN VARCHAR2 := 'N'
, p_forceer_nieuwe_meting IN VARCHAR2 := 'N'
, p_prioriteit IN VARCHAR2 := 'N'
, p_geplande_einddatum IN DATE := null
, p_snelheid IN VARCHAR2  := NULL
, p_commit IN BOOLEAN := TRUE
, p_copy       IN  VARCHAR2 := NULL -- Added by athar shaikh for the release rework 8.8.1.2 for Mantis-8147 impact on BASMETI01 module on 15/06/2015
, p_copy_aanmelden IN  VARCHAR2 := NULL   --New parameter added by SKA for Mantis 11560 additional issue (Button Stoftestgroep / protocol aanmelden working needed to change) release 8.9.1.4
, x_fout OUT BOOLEAN
, x_melding OUT VARCHAR2
);

-- Meld een stoftest aan
-- Bestaande aangemelde stoftesten worden op afgerond gezet (tenzij duplo)
-- Duplo's komen in dezelfde meting
-- Samenstellingen komen in een nieuwe meting (frac_id, onde_id)
PROCEDURE stoftest_aanmelden
( p_stof_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_frac_id IN NUMBER := NULL
, p_stan_id IN NUMBER := NULL
, p_onmo_id IN NUMBER := NULL
, p_onde_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
, p_runs_id IN NUMBER := NULL
, p_incl_voortest IN BOOLEAN := TRUE
, p_in_duplo IN BOOLEAN := FALSE
, p_commit IN BOOLEAN := TRUE
, p_copy   IN  VARCHAR2 := NULL    -- Added by SKA for Mantis-11560 release 8.9.1.4
, x_fout OUT BOOLEAN
, x_melding OUT VARCHAR2
);

-- idem, retourneer meet_id
FUNCTION stoftest_aanmelden
( p_stof_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_frac_id IN NUMBER := NULL
, p_stan_id IN NUMBER := NULL
, p_onmo_id IN NUMBER := NULL
, p_onde_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
, p_runs_id IN NUMBER := NULL
, p_incl_voortest IN BOOLEAN := TRUE
, p_in_duplo IN BOOLEAN := FALSE
, p_commit IN BOOLEAN := TRUE
, p_copy   IN  VARCHAR2 := NULL    -- Added by SKA for Mantis-11560 release 8.9.1.4
)
RETURN NUMBER;
END BAS_METI_01;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_METI_01" IS
 /******************************************************************************
      NAME:       bas_meti_01
      PURPOSE:    Used for BASMETI01 screen

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.2        29-04-2016   SKA             Mantis 11560 Cursor prst_meti_cur1 is added for exact copy when meting is having NULL protocol
                                              and parameter p_source_frac_id is added in procedure  protocol_aanmelden release 8.11.2.6
      1.1        22-03-2016   SKA             Mantis 11560 Cursor prst_meti_cur is made parameterized for BASMETI01 copy functionality release 8.11.0.2.
      1.0        01-02-2016   SKA             Mantis 11560 Package created for screen BASMETI01 copy functionality release 8.9.1.4.

   *****************************************************************************/

--
-- lokale function om te bepalen of protocol/stoftest (standaard) aangemeld
-- moet worden voor een monster met een bepaalde afnametijd
FUNCTION afnametijd_ok
( p_tijd_afname DATE
, p_voorwaarde IN VARCHAR2
)
RETURN BOOLEAN
IS
  v_return BOOLEAN;
  v_uur_afname VARCHAR2(10) := TO_CHAR(p_tijd_afname,'hh24' );
  v_voorwaarde VARCHAR2(100) := p_voorwaarde;
 BEGIN
  IF ( v_uur_afname IS NOT NULL
   AND v_voorwaarde IS NOT NULL
     )
  THEN
    IF ( v_voorwaarde NOT LIKE '<%'
     AND v_voorwaarde NOT LIKE '>%'
     AND v_voorwaarde NOT LIKE '=%'
       )
    THEN
      v_voorwaarde := '= '||v_voorwaarde;
    END IF;
    BEGIN
      IF ( kgc_util_00.dyn_exec( 'select ''J'' from dual where '||v_uur_afname||' '||v_voorwaarde ) = 'J' )
      THEN
        v_return := TRUE;
      ELSE
        v_return := FALSE;
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        v_return := FALSE;
    END;
  ELSE
    v_return := TRUE;
  END IF;
  RETURN( v_return );
END afnametijd_ok;

PROCEDURE  een_stoftest_aanmelden
 (  p_stof_id  IN  NUMBER  :=  NULL
 ,  p_meet_id  IN  NUMBER  :=  NULL
 ,  p_meti_id  IN  NUMBER  :=  NULL
 ,  p_frac_id  IN  NUMBER  :=  NULL
 ,  p_stan_id  IN  NUMBER  :=  NULL
 ,  p_onmo_id  IN  NUMBER  :=  NULL
 ,  p_onde_id  IN  NUMBER  :=  NULL
 ,  p_mede_id  IN  NUMBER  :=  NULL
 ,  p_runs_id  IN  NUMBER  :=  NULL
 ,  p_meet_id_super  IN  NUMBER  :=  NULL
 ,  p_incl_voortest  IN  BOOLEAN  :=  TRUE
 ,  p_in_duplo  IN  BOOLEAN  :=  FALSE
 ,  p_commit  IN  BOOLEAN  :=  TRUE
 ,  p_copy    IN  VARCHAR2 := NULL    -- Added by SKA for Mantis-11560 release 8.9.1.4
 ,  x_meet_id  OUT  NUMBER
 ,  x_fout  OUT  BOOLEAN
 ,  x_melding  OUT  VARCHAR2
 )
 IS
  v_stof_id NUMBER := p_stof_id;
  v_meti_id NUMBER := p_meti_id;
  v_frac_id NUMBER := p_frac_id;
  v_stan_id NUMBER := p_stan_id;
  v_onmo_id NUMBER := p_onmo_id;
  v_onde_id NUMBER := p_onde_id;

  CURSOR meet_cur
  IS
    SELECT meet.meti_id -- overrulet parameter
    ,      meet.stof_id -- overrulet parameter
    ,      meet.commentaar
    FROM   bas_meetwaarden meet
    WHERE  meet.meet_id = p_meet_id
    ;

  v_meet_commentaar VARCHAR2(2000);

  CURSOR stof_cur
  IS
    SELECT stof_id
    ,      eenheid
    ,      meet_reken
    ,      mwst_id
    ,      stof_id_voor
    ,      code
    ,      omschrijving
    ,      vervallen
    FROM   kgc_stoftesten
    WHERE  stof_id = v_stof_id
    ;
  stof_rec stof_cur%rowtype;

  -- duplo's:
  -- methode 1: stoftest opnieuw aanmelden, wordt zelfstandig gewaardeerd
  -- methode 2: stoftest wordt opnieuw aangemeld met de gegevens van de vorige aanmelden; de vorige verliest zijn waarde
  -- resultaat van volgende cursor bepaalt of er duplo's worden gemaakt volgens methode 2.
  cursor meet_duplo_cur
  is
    select meet.meet_id
    ,      stof.mwst_id
    ,      meet.volgorde
    ,      meet.spoed
    ,      meet.meetwaarde
    ,      meet.commentaar
    ,      max(mwss.aanmelden) duplo_aangemeld
    ,      mwss2.aanmelden nu_aanmelden
    from   kgc_mwst_samenstelling mwss
    ,      kgc_mwst_samenstelling mwss2
    ,      kgc_stoftesten stof
    ,      bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
    where  stof.mwst_id = mwss.mwst_id
    and    meet.stof_id = stof.stof_id
    and    meet.meet_id = mdet.meet_id
    and    mdet.volgorde = mwss.volgorde
    and    meet.meet_id = p_meet_id
    and    meet.stof_id = p_stof_id
    and    mwss.mwst_id = mwss2.mwst_id
    GROUP BY meet.meet_id
    ,        stof.mwst_id
    ,        meet.volgorde
    ,        meet.spoed
    ,        meet.meetwaarde
    ,        meet.commentaar
    ,        mwss2.aanmelden
    -- een volgende duplo moet volgens de MWSS nog mogelijk zijn, anders duplo volgens methode 1.
    HAVING MAX(mwss.aanmelden) < mwss2.aanmelden
    ORDER BY nu_aanmelden
    ;
  meet_duplo_rec meet_duplo_cur%rowtype;
  CURSOR mwss_cur
  ( b_mwst_id IN NUMBER
  , b_aanmelden IN NUMBER
  )
  IS
    SELECT mwss.volgorde
    ,      mwss.prompt
    FROM   kgc_mwst_samenstelling mwss
    WHERE  mwss.mwst_id = b_mwst_id
    AND    mwss.aanmelden > 0
    AND    mwss.aanmelden <= b_aanmelden
    ORDER BY mwss.volgorde
    ;
  CURSOR mest_cur
  IS
    SELECT mest_id
    FROM   bas_meetwaarde_statussen
    WHERE  default_goed_fout = 'F'
    AND    vervallen = 'N'
    ;
  v_mest_id_fout NUMBER;

  cursor cuty_cur
  ( b_meet_id in number
  )
  is
    select rese.cuty_id
    ,      meti.onmo_id
    ,      meti.frac_id
    ,      meet.stof_id
    from   bas_reserveringen rese
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    where  meet.meet_id = rese.meet_id
    and    meet.meti_id = meti.meti_id
    and    meet.meet_id = b_meet_id
    ;

  CURSOR frac_cur
  IS
    SELECT frac.frac_id
    ,      frac.controle
    ,      mons.tijd_afname
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    WHERE  mons.mons_id = frac.mons_id
    AND    frac_id = v_frac_id
    ;
  frac_rec frac_cur%rowtype;

  CURSOR onmo_cur
  IS
    SELECT onmo.onmo_id
    FROM   bas_fracties frac
    ,      kgc_onderzoek_monsters onmo
    WHERE  onmo.mons_id = frac.mons_id
    AND    onmo.onde_id = p_onde_id
    AND    frac.frac_id = p_frac_id
    ;

  CURSOR meti_leeg_cur
  IS
    -- leeg bij fractie
    SELECT meti.meti_id
    FROM   bas_metingen meti
    WHERE  meti.onde_id = p_onde_id
    AND    meti.frac_id = p_frac_id
    AND    meti.afgerond = 'N'
    AND    NOT EXISTS
           ( SELECT NULL
             FROM   bas_meetwaarden meet
             WHERE  meet.meti_id = meti.meti_id
           )
    UNION
    -- leeg bij standaardfractie
    SELECT meti.meti_id
    FROM   bas_metingen meti
    WHERE  meti.stan_id = p_stan_id
    AND    meti.afgerond = 'N'
    AND    NOT EXISTS
           ( SELECT NULL
             FROM   bas_meetwaarden meet
             WHERE  meet.meti_id = meti.meti_id
           )
    ORDER BY meti_id
    ;

  CURSOR meti_cur
  IS
    SELECT frac_id
    ,      stan_id
    ,      onmo_id
    ,      onde_id
    ,      stgr_id
    ,      prioriteit
    ,      snelheid
    ,      afgerond
    FROM   bas_metingen
    WHERE  meti_id = v_meti_id
    ;
  meti_rec meti_cur%rowtype;

  CURSOR prst_cur
  ( b_stgr_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  IS
    SELECT prst.volgorde
    ,      prst.eenheid
    ,      prst.afnametijd
    FROM   kgc_protocol_stoftesten prst
    WHERE  prst.stgr_id = b_stgr_id
    AND    prst.stof_id = b_stof_id
    ;
  prst_rec prst_cur%rowtype;

  -- Let op: substoftest is mogelijk al aangemeld (als element in het protocol)
  --         zonder registratie van de superstoftest
  CURSOR subs_cur
  IS
    SELECT subs.stof_id_sub
    ,      MAX(meet.meet_id) meet_id
    FROM   bas_meetwaarden meet
    ,      kgc_stoftesten stof
    ,      kgc_sub_stoftesten subs
    WHERE  subs.stof_id_sub = stof.stof_id
    AND    stof.vervallen = 'N'
    AND    subs.stof_id_super = v_stof_id
    AND    stof.stof_id = meet.stof_id
    AND    meet.meti_id = v_meti_id
    AND    meet.meet_id_super IS NULL
    GROUP BY subs.stof_id_sub
    UNION
    SELECT subs.stof_id_sub
    ,      TO_NUMBER(NULL)
    FROM   kgc_stoftesten stof
    ,      kgc_sub_stoftesten subs
    WHERE  subs.stof_id_sub = stof.stof_id
    AND    stof.vervallen = 'N'
    AND    subs.stof_id_super = v_stof_id
    AND    NOT EXISTS
           ( SELECT NULL FROM  bas_meetwaarden meet
             WHERE  meet.stof_id = stof.stof_id
             AND    meet.meti_id = v_meti_id
             AND    meet.meet_id_super IS NULL
           )
    ;

  v_normaalwaarde_ondergrens NUMBER;
  v_normaalwaarde_bovengrens NUMBER;
  v_normaalwaarde_gemiddelde NUMBER;
  v_dummy_fout BOOLEAN;
  v_dummy_melding VARCHAR2(4000);
  v_dummy_meet_id NUMBER;

  -- is er een niet-afgeronde meetwaarde voor deze stoftest bij deze fractie?
  FUNCTION niet_afgerond
  ( b_meti_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  RETURN BOOLEAN
  IS
    v_return BOOLEAN := FALSE;
    CURSOR c
    IS
      SELECT NULL
      FROM   bas_metingen meti
      ,      bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.meti_id = b_meti_id
      AND    meet.stof_id = b_stof_id
      AND    meet.afgerond = 'N'
      AND    meti.afgerond = 'N'
      ;
    l_dummy VARCHAR2(1);
   BEGIN
    IF ( b_meti_id IS NOT NULL
     AND b_stof_id IS NOT NULL
       )
    THEN
      OPEN  c;
      FETCH c
      INTO  l_dummy;
      v_return := c%found;
      CLOSE c;
    END IF;
    RETURN( v_return );
  END niet_afgerond;

  -- bouw (fout)melding op
  PROCEDURE  meld
   (  b_tekst  IN  VARCHAR2
   ,  b_fout  IN  BOOLEAN  :=  TRUE
   )
   IS
   BEGIN
    IF ( b_fout )
    THEN
      x_fout := TRUE;
    END IF;
    IF ( x_melding IS NULL )
    THEN
      x_melding := b_tekst;
    ELSE
      x_melding := x_melding||CHR(10)||b_tekst;
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END meld;
BEGIN
  -- 1. bepalingen (meetwaardestructuur, meet_reken, normaalwaarden, eenheid worden ook in db-triggers bepaald)
  -- 2. controles (controle op afgeronde meetwaarde vindt vooraf (in form) plaats)
  -- 3. aanmelding (soms ook meting)

  -- 1. bepalingen
  -- als meet_id is doorgegeven, dan wordt meti_id en stof_id bepaald
  IF ( p_meet_id IS NOT NULL )
  THEN
    OPEN  meet_cur;
    FETCH meet_cur
    INTO  v_meti_id
    ,     v_stof_id
    ,     v_meet_commentaar;
    CLOSE meet_cur;
  END IF;
  -- stof_id moet nu bekend zijn
  OPEN  stof_cur;
  FETCH stof_cur
  INTO  stof_rec;
  CLOSE stof_cur;
  IF ( stof_rec.stof_id IS NULL )
  THEN
    meld ( 'Foute aanroep: aan te melden stoftest is niet bekend!' );
    RETURN;
  ELSIF ( stof_rec.vervallen = 'J' )
  THEN
    meld ( stof_rec.omschrijving||' is vervallen!' );
    RETURN;
  END IF;

  -- in duplo: binnen dezelfde meetwaarde...
  IF ( p_in_duplo )
  THEN
    OPEN  meet_duplo_cur;
    FETCH meet_duplo_cur
    INTO  meet_duplo_rec;
    CLOSE meet_duplo_cur;
    IF ( meet_duplo_rec.duplo_aangemeld IS NOT NULL )
    THEN
      OPEN  mest_cur;
      FETCH mest_cur
      INTO  v_mest_id_fout;
      CLOSE mest_cur;
      -- duplo-aanvulling
      -- 1. nieuwe meetwaarde (zelfde meting, zelfde stoftest)
      -- 2. aanvullen met volgende set details
      -- 3. overnemen van meetwaarde, detail-waarden en ok-waarden
      -- 4. vorige meetwaarde uitschakelen
      -- 5. cuptypes overnemen (voorraadbeheer)
      --
      SAVEPOINT save_duplo;
      BEGIN
        -- even op afgerond..
        UPDATE bas_meetwaarden
        SET    afgerond = 'J'
        WHERE  meet_id = meet_duplo_rec.meet_id
        ;
        -- opnieuw aanmelden
        x_meet_id := stoftest_aanmelden
                     ( p_stof_id => p_stof_id
                     , p_meti_id => p_meti_id
                     , p_copy => p_copy -- Added by SKA for Mantis-11560 release 8.9.1.4
                     , p_in_duplo => FALSE  -- dit keer niet "in-duplo"
                     , p_commit => FALSE
                     );
        -- neem volgorde, meetwaarde, spoed en commentaar over
        update bas_meetwaarden
        set    volgorde = meet_duplo_rec.volgorde
        ,      spoed = meet_duplo_rec.spoed
        ,      meetwaarde = meet_duplo_rec.meetwaarde
        ,      commentaar = meet_duplo_rec.commentaar
        where  meet_id = x_meet_id
        ;
        -- en weer terug
        UPDATE bas_meetwaarden
        SET    afgerond = 'N'
        WHERE  meet_id = meet_duplo_rec.meet_id
        ;
        IF ( x_meet_id IS NOT NULL )
        THEN
          -- opnieuw aangemelde stoftest wordt uitgebreid met extra sets meetwaarde-details
          FOR mwss_rec IN mwss_cur( b_mwst_id => meet_duplo_rec.mwst_id
                                  , b_aanmelden => meet_duplo_rec.nu_aanmelden
                                  )
          LOOP
            INSERT INTO bas_meetwaarde_details
            ( meet_id
            , volgorde
            , prompt
            )
            VALUES
            ( x_meet_id
            , mwss_rec.volgorde
            , mwss_rec.prompt
            );
          END LOOP;
          -- neem eerst ok over
          UPDATE bas_mdet_ok mdok
          SET    mdok.ok = ( SELECT mdok0.ok
                             FROM   bas_mdet_ok mdok0
                             WHERE  mdok0.meet_id = meet_duplo_rec.meet_id
                             AND    mdok0.volgorde = mdok.volgorde
                           )
          WHERE  mdok.meet_id = x_meet_id
          AND    EXISTS
                 ( SELECT NULL
                   FROM   bas_mdet_ok mdok0
                   WHERE  mdok0.meet_id = meet_duplo_rec.meet_id
                   AND    mdok0.volgorde = mdok.volgorde
                 )
          ;
          -- neem dan details over
          UPDATE bas_meetwaarde_details mdet
          SET    mdet.waarde = ( SELECT mdet0.waarde
                                 FROM   bas_meetwaarde_details mdet0
                                 WHERE  mdet0.meet_id = meet_duplo_rec.meet_id
                                 AND    mdet0.volgorde = mdet.volgorde
                               )
          WHERE  mdet.meet_id = x_meet_id
          ;
          -- neem berekeningen over
          UPDATE bas_berekeningen bere
          SET    (uitslag,in_uitslag) = ( SELECT bere0.uitslag, bere0.in_uitslag
                                          FROM   bas_berekeningen bere0
                                          WHERE  bere0.meet_id = meet_duplo_rec.meet_id
                                          AND    bere0.bewy_id = bere.bewy_id
                                        )
          WHERE  bere.meet_id = x_meet_id
          AND    EXISTS
                 ( SELECT NULL
                   FROM   bas_berekeningen bere0
                   WHERE  bere0.meet_id = meet_duplo_rec.meet_id
                   AND    bere0.bewy_id = bere.bewy_id
                 )
          ;
          -- uitschakelen voor verder gebruik:
          -- volgende statements vorgen ervoor dat de betreffende meet-id in bas_vervallen_duploos_vw verschijnt.
          UPDATE bas_berekeningen
          SET    in_uitslag = 'N'
          WHERE  meet_id = meet_duplo_rec.meet_id
          ;
          UPDATE bas_mdet_ok
          SET    ok = 'N'
          WHERE  meet_id = meet_duplo_rec.meet_id
          ;
          UPDATE bas_meetwaarden
          SET    afgerond = 'J'
          ,      mest_id = v_mest_id_fout  -- ander status bedenken ???
          WHERE  meet_id = meet_duplo_rec.meet_id
          ;
          -- neem reserveringen over tbv. voorraadbeheer
          -- let op: na afronding vorige meetwaarde
          for r in cuty_cur( b_meet_id => meet_duplo_rec.meet_id )
          loop
            bas_rese_00.aangemeld
            ( p_onmo_id => r.onmo_id
            , p_frac_id => r.frac_id
            , p_stgr_id => NULL
            , p_stof_id => r.stof_id
            , p_cuty_id => r.cuty_id
            );
          end loop;
          -- rest hoeft/mag niet
          RETURN;
        END IF;
      EXCEPTION
        WHEN OTHERS
        THEN
          x_fout := TRUE;
          x_melding := SQLERRM;
          ROLLBACK TO SAVEPOINT save_duplo;
          RETURN;
      END;
    END IF;
  END IF;

  -- zoek een bestaande (lege) meting
  IF ( v_meti_id IS NULL )
  THEN
    OPEN  meti_leeg_cur;
    FETCH meti_leeg_cur
    INTO  v_meti_id;
    CLOSE meti_leeg_cur;
    IF ( v_meti_id IS NOT NULL )
    THEN -- ? wel of niet doen???
      UPDATE bas_metingen
      SET    datum_aanmelding = trunc(SYSDATE)
      WHERE  meti_id = v_meti_id;
    END IF;
  END IF;

  IF ( v_meti_id IS NOT NULL )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  meti_rec;
    CLOSE meti_cur;
    -- is de meting al afgerond? zo ja dan is opnieuw aanmelden onmogelijk
    IF ( meti_rec.afgerond = 'J' )
    THEN
      meld( 'Meting is afgerond. Opnieuw aanmelding is niet meer toegestaan' );
      RETURN;
    END IF;
    v_stan_id := meti_rec.stan_id;
    v_frac_id := meti_rec.frac_id;
    v_onde_id := meti_rec.onde_id;
    v_onmo_id := meti_rec.onmo_id;
  ELSIF ( v_onmo_id IS NULL
      AND p_onde_id IS NOT NULL
      AND p_frac_id IS NOT NULL
        )
  THEN
    OPEN  onmo_cur;
    FETCH onmo_cur
    INTO  v_onmo_id;
    CLOSE onmo_cur;
  END IF;

  IF ( NOT p_in_duplo
   AND niet_afgerond
       ( b_meti_id => v_meti_id
       , b_stof_id => v_stof_id
       )
     )
  THEN
    -- Stoftest is al aangemeld en nog niet afgerond
    -- Vul melding, zodat stoftest later NIET wordt meegeteld als aanmelding
    meld('Stoftest nog niet afgerond',FALSE);
    RETURN;
  END IF;

  -- fractiegegevens (status)
  IF ( v_frac_id IS NOT NULL )
  THEN
    OPEN  frac_cur;
    FETCH frac_cur
    INTO  frac_rec;
    CLOSE frac_cur;
  END IF;

  -- aanmelden
  IF ( v_meti_id IS NULL
   AND p_in_duplo
   AND v_stan_id IS NULL
     )
  THEN
    -- overige duplo altijd in dezelfde meting
    meld ( 'Geen meting gevonden. Aanmelding in duplo moet in dezelfde meting.' );
    RETURN;
  ELSIF ( v_meti_id IS NULL
      AND v_stan_id IS NULL
      AND v_frac_id IS NULL
        )
  THEN
    -- bij nieuwe meting moet frac/onde of stan bekend zijn
    meld ( 'Fractie of standaardfractie moet bekend zijn.' );
    RETURN;
  ELSIF ( v_onde_id IS NULL
      AND frac_rec.controle = 'N'
      AND v_frac_id IS NOT NULL
        )
  THEN
    -- als onderzoek niet bekend is, mag er alleen aangemeld worden bij een indexfractie
    meld ( 'Onderzoek is onbekend en de fractie is geen controle (index) fractie.' );
    RETURN;
  ELSIF ( v_meti_id IS NULL )
  THEN
    BEGIN
      SELECT bas_meti_seq.nextval
      INTO   v_meti_id
      FROM   dual;
      INSERT INTO bas_metingen
      ( meti_id
      , stan_id
      , frac_id
      , onmo_id
      , onde_id
      )
      VALUES
      ( v_meti_id
      , v_stan_id
      , v_frac_id
      , v_onmo_id
      , v_onde_id
      );
    EXCEPTION
      WHEN OTHERS
      THEN
        meld ( 'Meting niet gemaakt:'||CHR(10)||SQLERRM, TRUE );
    END;
  END IF;

  IF ( v_meti_id IS NOT NULL )
  THEN
    IF ( stof_rec.mwst_id IS NULL )
    THEN
      stof_rec.mwst_id := kgc_mwst_00.meetwaarde_structuur
                          ( p_meti_id => v_meti_id
                          , p_stof_id => v_stof_id
                          );
    END IF;
    v_normaalwaarde_ondergrens := bas_meet_00.normaalwaarde_onder( v_stof_id, v_meti_id, NULL, NULL );
    v_normaalwaarde_bovengrens := bas_meet_00.normaalwaarde_boven( v_stof_id, v_meti_id, NULL, NULL );
    v_normaalwaarde_gemiddelde := bas_meet_00.normaalwaarde_gemiddeld( v_stof_id, v_meti_id, NULL, NULL );
    IF ( v_stof_id IS NOT NULL
     AND meti_rec.stgr_id IS NOT NULL
       )
    THEN
      OPEN  prst_cur( b_stgr_id => meti_rec.stgr_id
                    , b_stof_id => v_stof_id
                    );
      FETCH prst_cur
      INTO  prst_rec;
      stof_rec.eenheid := NVL( prst_rec.eenheid, stof_rec.eenheid );
      CLOSE prst_cur;
    END IF;

    IF ( afnametijd_ok
         ( p_tijd_afname => frac_rec.tijd_afname
         , p_voorwaarde => prst_rec.afnametijd
         )
       )
    THEN
      BEGIN
        SELECT bas_meet_seq.nextval
        INTO   x_meet_id
        FROM   dual;
        INSERT INTO bas_meetwaarden
        ( meet_id
        , meti_id
        , stof_id
        , mwst_id
        , meet_reken
        , meeteenheid
        , normaalwaarde_ondergrens
        , normaalwaarde_bovengrens
        , normaalwaarde_gemiddelde
        , mede_id
        , runs_id
        , volgorde
        , meet_id_super
        , commentaar
        )
        VALUES (
          x_meet_id
        , v_meti_id
        , v_stof_id
        , stof_rec.mwst_id
        , stof_rec.meet_reken
        , stof_rec.eenheid
        , v_normaalwaarde_ondergrens
        , v_normaalwaarde_bovengrens
        , v_normaalwaarde_gemiddelde
        , p_mede_id
        , p_runs_id
        , prst_rec.volgorde
        , p_meet_id_super
        , v_meet_commentaar
        )
        ;
        meld ( 'Stoftest '||stof_rec.omschrijving||' aangemeld', FALSE );

        -- plus voortest
        IF ( p_incl_voortest
         AND stof_rec.stof_id_voor IS NOT NULL
         AND v_stan_id IS NULL
           )
        THEN
          een_stoftest_aanmelden
          ( p_stof_id => stof_rec.stof_id_voor
          , p_meti_id => v_meti_id
          , p_mede_id => p_mede_id
          , p_runs_id => p_runs_id
          , p_incl_voortest => p_incl_voortest
          , p_in_duplo => p_in_duplo
          , p_commit => FALSE
          , x_meet_id => v_dummy_meet_id
          , p_copy => p_copy  -- Added by SKA for Mantis-11560 release 8.9.1.4
          , x_fout => v_dummy_fout
          , x_melding => v_dummy_melding
          );
          IF ( v_dummy_fout )
          THEN
            meld( v_dummy_melding, FALSE );
          END IF;
        END IF;
        -- plus samenstelling
        IF ( p_copy <> 'C')
        THEN -- Added by SKA for Mantis-11560 release 8.9.1.4
            FOR r IN subs_cur
            LOOP
              IF ( r.meet_id IS NULL )
              THEN
                een_stoftest_aanmelden
                ( p_stof_id => r.stof_id_sub
                , p_meti_id => v_meti_id
                , p_mede_id => p_mede_id
                , p_runs_id => p_runs_id
                , p_meet_id_super => x_meet_id
                , p_incl_voortest => p_incl_voortest
                , p_in_duplo => p_in_duplo
                , p_commit => FALSE
                , x_meet_id => v_dummy_meet_id
                , p_copy => p_copy  -- Added by SKA for Mantis-11560 release 8.9.1.4
                , x_fout => v_dummy_fout
                , x_melding => v_dummy_melding
                );
                IF ( v_dummy_fout )
                THEN
                  meld( v_dummy_melding, FALSE );
                END IF;
              ELSE  -- al aangemelde stoftest (zowel in protocol als in samenstelling)
                UPDATE bas_meetwaarden
                SET meet_id_super = x_meet_id
                WHERE meet_id = r.meet_id;
              END IF;
            END LOOP;
        END IF; -- Added by SKA for Mantis-11560 release 8.9.1.4
      EXCEPTION
        WHEN OTHERS
        THEN
          meld ( 'Meetwaarde niet gemaakt:'||CHR(10)||SQLERRM, TRUE );
      END;
    END IF;
  END IF;
  IF ( x_fout )
  THEN
    NULL;
  ELSE -- leeg of false
    x_fout := FALSE;
  END IF;
  IF ( p_commit )
  THEN
    COMMIT;
  END IF;
END een_stoftest_aanmelden;

-- publiek

PROCEDURE  protocol_aanmelden
 (  p_onmo_id    IN  NUMBER
 ,  p_stgr_id    IN  NUMBER
 ,  p_frac_id    IN  NUMBER   :=   NULL
 ,  p_source_frac_id  IN  NUMBER   := NULL -- Added by SKA for mantis 11560 on 26-Apr-2016 for rework release 8.11.2.6
 ,  p_standaard  IN  VARCHAR2 :=  'N'
 ,  p_forceer_nieuwe_meting  IN  VARCHAR2  :=  'N'
 ,  p_prioriteit IN  VARCHAR2 :=  'N'
 ,  p_geplande_einddatum IN DATE := null
 ,  p_snelheid   IN  VARCHAR2 :=  NULL
 ,  p_commit     IN  BOOLEAN  :=  TRUE
 ,  p_copy       IN  VARCHAR2 := NULL    -- Added by athar shaikh for the release rework 8.8.1.2 for Mantis-8147 impact on BASMETI01 module on 15/06/2015
 ,  p_copy_aanmelden IN  VARCHAR2 := NULL   --New parameter added by SKA for Mantis 11560 additional issue (Button Stoftestgroep / protocol aanmelden working needed to change)  release 8.9.1.4
 ,  x_fout       OUT  BOOLEAN
 ,  x_melding    OUT  VARCHAR2
 )
 IS
  v_ok BOOLEAN := TRUE;
  v_protocol_toegevoegd BOOLEAN := FALSE;
  v_errors VARCHAR2(4000);
  v_fout BOOLEAN;
  v_melding VARCHAR2(4000);
  v_fractie_actie VARCHAR2(10); -- gedrag tav. fracties: BESTAAND,STANDAARD (fractietypes) ,DEFAULT (genereer zonodig 1 fractie)
  v_frac_id NUMBER;
  v_meti_id NUMBER;
  v_fractienr VARCHAR2(20); -- bas_fracties.fractienummer%type;
  v_forceer_nieuwe_meting VARCHAR2(1) := UPPER(p_forceer_nieuwe_meting);
  v_aantal_meet NUMBER := 0;
  l_v_snelheid kgc_stoftestgroep_gebruik.snelheid%type;

  CURSOR stgr_cur
  IS
    SELECT stgr.code
    FROM kgc_stoftestgroepen stgr
    WHERE stgr.stgr_id = p_stgr_id
    ;

  stgr_rec stgr_cur%ROWTYPE;

  CURSOR mons_cur
  IS
    SELECT onmo.mons_id
    ,      onmo.onde_id
    ,      mons.ongr_id
    ,      mons.mate_id
    ,      mons.monsternummer
    ,      mons.tijd_afname
    ,      onde.onderzoekswijze
    ,      onde.kafd_id
    ,      kafd.code kafd_code
	,      onin.indi_id
	,      onwy.onwy_id
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_monsters mons
    ,      kgc_onderzoeken onde
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_onderzoek_indicaties onin
	,      kgc_onderzoekswijzen onwy
    WHERE  onmo.onde_id = onde.onde_id
    AND    onmo.mons_id = mons.mons_id
    AND    mons.kafd_id = kafd.kafd_id
    AND    onmo.onmo_id = p_onmo_id
    AND    onde.onde_id = onin.onde_id (+)
    AND    onde.kafd_id = onwy.kafd_id (+)
    AND    onde.onderzoekswijze = onwy.code (+)
    ;
  mons_rec mons_cur%rowtype;

  CURSOR frac_cur
  IS
    SELECT frac.frac_id
    ,      frac.frty_id
    FROM   bas_fracties frac
    WHERE  frac.mons_id = mons_rec.mons_id
    AND    frac.frac_id = NVL( p_frac_id, frac.frac_id )
    AND    NVL( frac.status, 'O' ) IN ( 'A', 'O' )
    ;
  frac_rec frac_cur%rowtype;

  CURSOR stft_cur
  IS
    SELECT frty.frty_id
    ,      frty.letter_fractienr
    FROM   bas_fractie_types frty
    ,      bas_stand_fractietypes stft
    WHERE  stft.frty_id = frty.frty_id
    AND    stft.mate_id = mons_rec.mate_id
    AND    frty.ongr_id = mons_rec.ongr_id
    ;
  stft_rec stft_cur%rowtype; -- om cursor te testen op enig resultaat
  --
  CURSOR prst_cur
  IS
    SELECT prst.stof_id
    ,      prst.stgr_id
    ,      prst.volgorde
    ,      prst.afnametijd
    FROM   kgc_stoftesten stof
    ,      kgc_protocol_stoftesten prst
    WHERE  prst.stof_id = stof.stof_id
    AND    stof.vervallen = 'N'
    AND    prst.stgr_id = p_stgr_id
    AND  ( p_standaard = 'N'
        OR prst.standaard = 'J'
         )
    ORDER BY prst.stgr_id, prst.volgorde -- juiste sortering van belang!!!
    ;
  --start added by SKA for Mantis-11560 release 8.9.1.4
   /*This cursor is used for copying exact meting */
    CURSOR prst_meti_cur  (b_stgr_id NUMBER)  -- parameter b_stgr_id is added by SKA for Mantis-11560 22-03-2016 release 8.11.0.2
    IS
      SELECT prst.stof_id
      ,      prst.stgr_id
      ,      prst.volgorde
      ,      prst.afnametijd
      FROM   kgc_stoftesten stof
      ,      kgc_protocol_stoftesten prst
      WHERE  prst.stof_id = stof.stof_id
      AND    stof.vervallen = 'N'
      AND    prst.stgr_id = p_stgr_id
      AND  ( p_standaard = 'N'
          OR prst.standaard = 'J'
           )
      AND prst.stof_id IN (
                           SELECT meet.stof_id
                           FROM   bas_metingen meti
                           ,      bas_meetwaarden meet
                           WHERE  meti.onmo_id = p_onmo_id
                           AND    meti.meti_id = meet.meti_id
                           AND    meti.frac_id = p_frac_id
                           AND    meti.stgr_id = b_stgr_id   --AND  Condition is added by SKA for Mantis-11560 22-03-2016 release 8.11.0.2
                           )
      ORDER BY prst.stgr_id, prst.volgorde -- juiste sortering van belang!!!
      ;
      prst_meti_rec prst_meti_cur%rowtype;
      --end by SKA for Mantis-11560 release 8.9.1.4
     --start added by SKA for Mantis-11560 on 06-05-2016 for rework release 8.11.2.6
   /*This cursor is used for copying exact meting when meting is having NULL protocol and contains some stoftests  */
    CURSOR meet_meti_cur
    IS
      SELECT meet.stof_id
      ,      meet.volgorde
      ,      NULL afnametijd
      FROM   bas_metingen meti
      ,      bas_meetwaarden meet
      ,      kgc_onderzoek_monsters onmo
      WHERE  meti.onmo_id = onmo.onmo_id
      AND    meti.onde_id = onmo.onde_id
      AND    meti.meti_id = meet.meti_id
      AND    meti.frac_id = p_source_frac_id
      AND    meti.stgr_id IS NULL
      ORDER BY meet.stof_id, meet.volgorde;
      meet_meti_rec meet_meti_cur%rowtype;
      --end added by SKA for Mantis-11560 on 06-05-2016 for rework release 8.11.2.6
  CURSOR meti_cur
  IS
    SELECT meti_id
    FROM   bas_metingen
    WHERE  frac_id = v_frac_id
    AND    onmo_id = p_onmo_id
    AND    stgr_id = p_stgr_id
    AND    afgerond = 'N'
    UNION
    SELECT meti_id
    FROM   bas_metingen
    WHERE  frac_id = v_frac_id
    AND    onmo_id = p_onmo_id
    AND    stgr_id IS NULL
    AND    afgerond = 'N'
    AND    meti_id NOT IN
           ( SELECT meti_id
             FROM   bas_meetwaarden meet
           )
    ;

  CURSOR  ftct_cur
  ( p_frty_id IN NUMBER
  )
  IS
    SELECT cuty_id
    ,      NVL( standaard_aantal, 1 ) standaard_aantal
    FROM   bas_frty_cuty
    WHERE  frty_id = p_frty_id
    AND    NVL( standaard_aantal, 1 ) > 0
    ;

  CURSOR stgg_cur
  ( b_kafd_id IN NUMBER
  , b_stgr_id IN NUMBER
  , b_frac_id IN NUMBER
  , b_mate_id IN NUMBER
  , b_frty_id IN NUMBER
  , b_indi_id IN NUMBER
  , b_onwy_id IN NUMBER
  )
  IS
    SELECT stgg.snelheid
	,      stgg.afnametijd
    FROM   kgc_stoftestgroepen stgr
    ,      kgc_stoftestgroep_gebruik stgg
    WHERE  stgg.stgr_id = stgr.stgr_id
    AND    stgr.kafd_id = b_kafd_id
    AND    stgr.vervallen = 'N'
    AND    stgg.vervallen = 'N'
    AND    stgr.stgr_id = b_stgr_id
    AND  ( stgg.mate_id = b_mate_id
        OR stgg.mate_id IS NULL
         )
    AND  ( stgg.frty_id = b_frty_id
        OR stgg.frty_id IS NULL
         )
    AND  ( stgg.indi_id = b_indi_id
        OR stgg.indi_id IS NULL
         )
    AND  ( stgg.onwy_id = b_onwy_id
        OR stgg.onwy_id IS NULL
         )
    order by stgg.creation_date desc
  ;

   -- Added Start by athar shaikh for the MANTIS-8147 on 26-03-2015
   -- Cursor will fetch the value if on Onderzoek level stoftest group is already attached
  CURSOR dup_stgg_cur
  ( p_onde_id number
  , p_stgr_id number
  )
  IS
    SELECT stgg.stgr_id
    FROM   kgc_stoftestgroep_gebruik stgg
    WHERE  NVL(stgg.standaard, 'N') = 'J'
    AND    NVL(stgg.vervallen, 'N') != 'J'
    AND    STGR_ID IN (SELECT meti.stgr_id
                       FROM   bas_metingen meti
                       WHERE  meti.onde_id = p_onde_id
                       AND    meti.stgr_id = p_stgr_id
                     );
 -- Cursor will fetch the process onderzoek information
  cursor onde_cur
  (v_onde_id number
  )
  IS
  SELECT onde.onde_id
  ,      onde.kafd_id
  ,      onde.ongr_id
  FROM   kgc_onderzoeken onde
  WHERE  onde.onde_id = v_onde_id;

  v_dup_stgr bas_metingen.stgr_id%type;
  v_onde_id  kgc_onderzoeken.onde_id%type;
  v_kafd_id  kgc_onderzoeken.kafd_id%type;
  v_ongr_id  kgc_onderzoeken.ongr_id%type;

-- End by athar shaikh for the MANTIS-8147 on 26-03-2015
  -- lokale procedure om fouttekst op te bouwen
  PROCEDURE voeg_toe
  ( p_melding IN VARCHAR2
  )
  IS
      BEGIN
    IF ( p_melding IS NOT NULL )
    THEN
      IF ( v_errors IS NULL )
      THEN
        v_errors := SUBSTR( p_melding, 1, 1000 );
      ELSE
        v_errors := SUBSTR( v_errors||CHR(10)||p_melding, 1, 1000 );
      END IF;
    END IF;
  END voeg_toe;
--
BEGIN
  IF ( p_onmo_id IS NULL
   AND p_stgr_id IS NULL
     )
  THEN
    raise_application_error( -20000, 'Parameters zijn niet goed doorgegeven.'
                          ||CHR(10)||'Waarschuw de applicatiebeheerder!'
                           );
  END IF;
  --
  IF ( v_ok )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  mons_rec;
    IF ( mons_cur%notfound )
    THEN
      CLOSE mons_cur;
      voeg_toe( 'Onderzoek-monster is onbekend' );
      v_ok := FALSE;
    END IF;
    IF ( mons_cur%isopen )
    THEN
      CLOSE mons_cur;
    END IF;
  END IF;

  IF ( v_ok )
  THEN
    -- bepaal activiteit: bestaande fractie(s) OF nieuwe standaard fractietype(s) OF 1 nieuwe fractie
    v_fractie_actie := NULL;
    IF ( v_fractie_actie IS NULL )
    THEN
      IF ( p_frac_id IS NOT NULL )
      THEN
        v_fractie_actie := 'DEFAULT';
        v_frac_id := p_frac_id;
        OPEN  frac_cur;
        FETCH frac_cur
        INTO  frac_rec;
        CLOSE frac_cur;
      END IF;
    END IF;
    IF ( v_fractie_actie IS NULL )
    THEN
      OPEN  frac_cur;
      FETCH frac_cur
      INTO  frac_rec;
      IF ( frac_cur%found )
      THEN
        v_fractie_actie := 'BESTAAND';
        v_frac_id := frac_rec.frac_id;
      ELSE
        CLOSE frac_cur;
      END IF;
    END IF;
    IF ( v_fractie_actie IS NULL )
    THEN
      OPEN  stft_cur;
      FETCH stft_cur
      INTO  stft_rec;
      IF ( stft_cur%found )
      THEN
        v_fractie_actie := 'STANDAARD';
        v_frac_id := NULL;
      ELSE
        CLOSE stft_cur;
      END IF;
    END IF;
    IF ( v_fractie_actie IS NULL )
    THEN
      v_fractie_actie := 'DEFAULT';
      v_frac_id := NULL;
    END IF;

    LOOP
      v_meti_id := NULL;

      IF ( v_frac_id IS NULL )
      THEN
        BEGIN
          v_fractienr := kgc_nust_00.genereer_nr
                         ( p_nummertype => 'FRAC'
                         , p_ongr_id => mons_rec.ongr_id
                         , p_waarde1 => mons_rec.monsternummer
                         , p_waarde2 => stft_rec.letter_fractienr -- misschien leeg
                         );
          -- frac_id niet via TAPI; waarde is hierna nog nodig
          SELECT bas_frac_seq.nextval
          INTO   v_frac_id
          FROM   dual;
          INSERT INTO bas_fracties
          ( frac_id
          , mons_id
          , fractienummer
          , frty_id
          , commentaar
          )
          VALUES
          ( v_frac_id
          , mons_rec.mons_id
          , v_fractienr
          , stft_rec.frty_id
          , 'Gegenereerd bij aanmelding protocol'
          );
          FOR ftct_rec IN ftct_cur( p_frty_id => stft_rec.frty_id )
          LOOP
            BEGIN
              INSERT INTO bas_voorraad
              ( frac_id
              , cuty_id
              , hoeveelheid
              )
              VALUES
              ( v_frac_id
              , ftct_rec.cuty_id
              , ftct_rec.standaard_aantal
              );
            END;
          END LOOP;
        EXCEPTION
          WHEN OTHERS
          THEN
            v_ok := FALSE;
            voeg_toe( 'Fractie kan niet worden aangemaakt' );
            voeg_toe( CHR(10)||SQLERRM );
        END;
  END IF; -- fractie
  -- Added Start by athar shaikh for the MANTIS-8147 on 26-03-2015
     OPEN onde_cur(mons_rec.onde_id);
     FETCH onde_cur INTO v_onde_id, v_kafd_id, v_ongr_id;
     CLOSE onde_cur;

     OPEN dup_stgg_cur(mons_rec.onde_id, p_stgr_id);
     FETCH dup_stgg_cur INTO v_dup_stgr;
     CLOSE dup_stgg_cur;

     IF NVL(UPPER ( kgc_sypa_00.standaard_waarde ( p_parameter_code => 'AUTO_STGR_FRAC_TYPE'
                                                  ,p_kafd_id        => v_kafd_id,
                                                   p_ongr_id        => v_ongr_id) ), 'N') = 'J' AND v_dup_stgr IS NOT NULL AND p_copy is null THEN  -- P_copy condition added by athar shaikh for the release rework 8.8.1.2 for Mantis-8147 impact on BASMETI01 module on 15/06/2015

     NULL;

     ELSE

-- End by athar shaikh for the MANTIS-8147 on 26-03-2015


      IF ( v_ok )
      THEN
        IF ( v_forceer_nieuwe_meting = 'N' )
        THEN
          OPEN  meti_cur;
          FETCH meti_cur
          INTO  v_meti_id;
          CLOSE meti_cur;
        END IF;
        IF ( v_meti_id IS NULL )
        -- maak een meting aan (1x)
        THEN
          BEGIN
		    -- 1. Snelheid selecteren zoals gekozen in het scherm ...
			-- 2. Als deze waarde leeg is, zoeken naar een waarde in STGG
			-- 3. Controleren of de afnametijd correct is
 			-- 4. Als die niet bestaat alsnog de default 'K' vullen
            IF p_snelheid is null
			THEN
              FOR stgg_rec IN stgg_cur
                         ( b_kafd_id => mons_rec.kafd_id
						 , b_stgr_id => p_stgr_id
                         , b_frac_id => frac_rec.frac_id
                         , b_mate_id => mons_rec.mate_id
                         , b_frty_id => frac_rec.frty_id
                         , b_indi_id => mons_rec.indi_id
                         , b_onwy_id => mons_rec.onwy_id
                         )
              LOOP
                IF ( afnametijd_ok
                   ( p_tijd_afname => mons_rec.tijd_afname
                   , p_voorwaarde  => stgg_rec.afnametijd ) )
                THEN
                   l_v_snelheid := stgg_rec.snelheid;
                END IF;
              END LOOP;
            END IF;

            -- meti_id niet via TAPI; waarde is hierna nog nodig
            -- Mantis 287 PHE parm p_geplande_einddatum toegevoegd
            SELECT bas_meti_seq.nextval
            INTO   v_meti_id
            FROM   dual;
            INSERT INTO bas_metingen
            ( meti_id
            , frac_id
            , datum_aanmelding
            , snelheid
            , prioriteit
            , geplande_einddatum
            , stgr_id
            , onmo_id
            , onde_id
            )
            VALUES
            ( v_meti_id
            , v_frac_id
            , trunc(SYSDATE)
            , NVL( nvl(p_snelheid,l_v_snelheid), 'K' )
            , NVL( p_prioriteit, 'N' )
            , p_geplande_einddatum
            , p_stgr_id
            , p_onmo_id
            , mons_rec.onde_id
            );
          END;
          v_protocol_toegevoegd := TRUE;
      END IF; -- meting
       IF (p_copy IS NOT NULL
           AND p_copy_aanmelden IS NULL
           )
       THEN  --start by SKA for Mantis-11560 release 8.9.1.4
        /* Added by SKA for Mantis-11560 ,AND p_copy_aanmelden parameter default null condition added for stoftest\anmeldan
         button working Mantis 11560 release 8.9.1.4 */
           IF p_stgr_id IS NOT NULL
           THEN -- added by SKA for Mantis-11560 on 26-04-2016 for rework release 8.11.2.6
               FOR prst_meti_rec IN prst_meti_cur
                                                (
                                                 b_stgr_id => p_stgr_id ---- parameter b_stgr_id is added by SKA for Mantis-11560 22-03-2016 release 8.11.0.2
                                                )
               LOOP
                 IF ( afnametijd_ok
                     ( p_tijd_afname => mons_rec.tijd_afname
                     , p_voorwaarde => prst_meti_rec.afnametijd
                     )
                    )
                 THEN
                    stoftest_aanmelden
                   ( p_stof_id  => prst_meti_rec.stof_id
                   , p_meti_id  => v_meti_id
                   , p_commit   => FALSE
                   , p_copy  => p_copy
                   , x_fout => v_fout
                   , x_melding => v_melding
                   );
                   IF ( v_fout )
                   THEN
                     v_ok := FALSE;
                     voeg_toe( 'Stoftest niet aangemeld' );
                     voeg_toe( CHR(10)||v_melding );
                    ELSE
                      IF v_melding = 'Stoftest nog niet afgerond'
                      THEN
                         NULL;
                      ELSE
                         v_aantal_meet := v_aantal_meet + 1;
                      END IF;
                    END IF;
                 END IF;
               END LOOP;
           ELSE   --start added by SKA for Mantis-11560 on 06-05-2016 for rework release 8.11.2.6
               FOR meet_meti_rec IN meet_meti_cur
               LOOP
                 IF ( afnametijd_ok
                     ( p_tijd_afname => mons_rec.tijd_afname
                     , p_voorwaarde => meet_meti_rec.afnametijd
                     )
                    )
                 THEN
                    stoftest_aanmelden
                   ( p_stof_id  => meet_meti_rec.stof_id
                   , p_meti_id  => v_meti_id
                   , p_commit   => FALSE
                   , p_copy  => p_copy
                   , x_fout => v_fout
                   , x_melding => v_melding
                   );
                   IF ( v_fout )
                   THEN
                     v_ok := FALSE;
                     voeg_toe( 'Stoftest niet aangemeld' );
                     voeg_toe( CHR(10)||v_melding );
                    ELSE
                      IF v_melding = 'Stoftest nog niet afgerond'
                      THEN
                         NULL;
                      ELSE
                         v_aantal_meet := v_aantal_meet + 1;
                      END IF;
                    END IF;
                 END IF;
               END LOOP;
           END IF;   --End added by SKA for Mantis-11560 on 06-05-2016 for rework release 8.11.2.6
       ELSE  -- END added by SKA for Mantis 11560 release 8.9.1.4
            FOR prst_rec IN prst_cur
            LOOP
              IF ( afnametijd_ok
                   ( p_tijd_afname => mons_rec.tijd_afname
                   , p_voorwaarde => prst_rec.afnametijd
                   )
                 )
              THEN
                -- maak meetwaarde rij voor stoftest
                v_fout := FALSE;
                v_melding := NULL;
                stoftest_aanmelden
                ( p_stof_id  => prst_rec.stof_id
                , p_meti_id  => v_meti_id
                , p_commit   => FALSE
                , p_copy => p_copy --Added by SKA for Mantis 11560 release 8.9.1.4
                , x_fout => v_fout
                , x_melding => v_melding
                );
                IF ( v_fout )
                THEN
                  v_ok := FALSE;
                  voeg_toe( 'Stoftest niet aangemeld' );
                  voeg_toe( CHR(10)||v_melding );
                ELSE
                  IF v_melding = 'Stoftest nog niet afgerond'
                  THEN
                     NULL;
                  ELSE
                     v_aantal_meet := v_aantal_meet + 1;
                  END IF;
                END IF;
              END IF;
            END LOOP;
       END IF;--Added by SKA for the MANTIS-11560 release 8.9.1.4
      END IF;
      END IF; -- Added by athar shaikh for the MANTIS-8147 on 26-03-2015
      IF ( frac_cur%isopen )
      THEN
        FETCH frac_cur
        INTO  frac_rec;
        IF ( frac_cur%found )
        THEN
          v_frac_id := frac_rec.frac_id;
        ELSE
          CLOSE frac_cur;
          EXIT;
        END IF;
      ELSIF ( stft_cur%isopen )
      THEN
        FETCH stft_cur
        INTO  stft_rec;
        IF ( stft_cur%found )
        THEN
          v_frac_id := NULL;
        ELSE
          CLOSE stft_cur;
          EXIT;
        END IF;
      ELSE
        EXIT;
      END IF;
    END LOOP;
  END IF;
  IF ( v_ok )
  THEN
    IF ( p_commit )
    THEN
      COMMIT;
    END IF;
    x_fout := FALSE;
    OPEN stgr_cur;
    FETCH stgr_cur
    INTO stgr_rec;
    IF ( v_protocol_toegevoegd )
    THEN
      x_melding := 'Protocol ' || stgr_rec.code || ' met ' || TO_CHAR(v_aantal_meet)||' stoftest(en) aangemeld';
    ELSE
      x_melding := 'Voor protocol ' || stgr_rec.code || ' zijn ' || TO_CHAR(v_aantal_meet)||' stoftest(en) aangemeld';
    END IF;
    CLOSE stgr_cur;
  ELSE
--    ROLLBACK TO begin_aanroep;
    x_fout := TRUE;
    x_melding := v_errors;
  END IF;
END protocol_aanmelden;

PROCEDURE   stoftest_aanmelden
  (   p_stof_id   IN   NUMBER   :=   NULL
  ,   p_meet_id   IN   NUMBER   :=   NULL
  ,   p_meti_id   IN   NUMBER   :=   NULL
  ,   p_frac_id   IN   NUMBER   :=   NULL
  ,   p_stan_id   IN   NUMBER   :=   NULL
  ,   p_onmo_id   IN   NUMBER   :=   NULL
  ,   p_onde_id   IN   NUMBER   :=   NULL
  ,   p_mede_id   IN   NUMBER   :=   NULL
  ,   p_runs_id   IN   NUMBER   :=   NULL
  ,   p_incl_voortest   IN   BOOLEAN   :=   TRUE
  ,   p_in_duplo   IN   BOOLEAN   :=   FALSE
  ,   p_commit   IN   BOOLEAN   :=   TRUE
  ,   p_copy   IN  VARCHAR2 := NULL    -- Added by SKA for Mantis-11560 release 8.9.1.4
  ,   x_fout   OUT   BOOLEAN
  ,   x_melding   OUT   VARCHAR2
  )
  IS
  v_meet_id NUMBER;
  BEGIN
  een_stoftest_aanmelden
  ( p_stof_id => p_stof_id
  , p_meet_id => p_meet_id
  , p_meti_id => p_meti_id
  , p_frac_id => p_frac_id
  , p_stan_id => p_stan_id
  , p_onmo_id => p_onmo_id
  , p_onde_id => p_onde_id
  , p_mede_id => p_mede_id
  , p_runs_id => p_runs_id
  , p_incl_voortest => p_incl_voortest
  , p_in_duplo => p_in_duplo
  , p_commit => p_commit
  , p_copy => p_copy  -- Added by SKA for Mantis-11560 release 8.9.1.4
  , x_meet_id => v_meet_id
  , x_fout => x_fout
  , x_melding => x_melding
  );
END stoftest_aanmelden;

FUNCTION   stoftest_aanmelden
  (   p_stof_id   IN   NUMBER   :=   NULL
  ,   p_meet_id   IN   NUMBER   :=   NULL
  ,   p_meti_id   IN   NUMBER   :=   NULL
  ,   p_frac_id   IN   NUMBER   :=   NULL
  ,   p_stan_id   IN   NUMBER   :=   NULL
  ,   p_onmo_id   IN   NUMBER   :=   NULL
  ,   p_onde_id   IN   NUMBER   :=   NULL
  ,   p_mede_id   IN   NUMBER   :=   NULL
  ,   p_runs_id   IN   NUMBER   :=   NULL
  ,   p_incl_voortest   IN   BOOLEAN   :=   TRUE
  ,   p_in_duplo   IN   BOOLEAN   :=   FALSE
  ,   p_commit   IN   BOOLEAN   :=   TRUE
  ,   p_copy   IN  VARCHAR2 := NULL    -- Added by SKA for Mantis-11560 release 8.9.1.4
  )
  RETURN   NUMBER
  IS
  v_fout BOOLEAN;
  v_melding VARCHAR2(4000);
  v_meet_id NUMBER;
  BEGIN
  een_stoftest_aanmelden
  ( p_stof_id => p_stof_id
  , p_meet_id => p_meet_id
  , p_meti_id => p_meti_id
  , p_frac_id => p_frac_id
  , p_stan_id => p_stan_id
  , p_onmo_id => p_onmo_id
  , p_onde_id => p_onde_id
  , p_mede_id => p_mede_id
  , p_runs_id => p_runs_id
  , p_incl_voortest => p_incl_voortest
  , p_in_duplo => p_in_duplo
  , p_commit => p_commit
  , p_copy => p_copy  -- Added by SKA for Mantis-11560 release 8.9.1.4
  , x_meet_id => v_meet_id
  , x_fout => v_fout
  , x_melding => v_melding
  );
  IF ( v_fout )
  THEN
    raise_application_error( -20000, v_melding );
  ELSE
    RETURN( v_meet_id );
  END IF;
END stoftest_aanmelden;
END   bas_meti_01;
/

/
QUIT
