CREATE OR REPLACE PACKAGE "HELIX"."KGC_EENH_00" IS
-- PL/SQL Specification
-- controleer opgegeven eenheid; retourneer zo mogelijk de juiste waarde.
FUNCTION controleer
( p_eenheid IN VARCHAR2
)
RETURN VARCHAR2;


END KGC_EENH_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_EENH_00" IS
FUNCTION controleer
( p_eenheid IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(30);
  v_tmp VARCHAR2(30);

  CURSOR eenh_cur
  IS
    SELECT eenheid
    FROM   kgc_eenheden
    WHERE  UPPER(eenheid) = UPPER(p_eenheid)
    AND    vervallen = 'N'
    ORDER BY DECODE( eenheid
                   , p_eenheid, 1
                   , 2
                   )
    ;
  CURSOR eenh_cur2
  IS
    SELECT eenheid
    FROM   kgc_eenheden
    WHERE  UPPER(eenheid) LIKE UPPER(p_eenheid)||'%'
    AND    vervallen = 'N'
    ;
-- PL/SQL Block
BEGIN
  IF ( p_eenheid IS NULL )
  THEN
    v_return := NULL;
  ELSE
    -- zoek naar exacte match
    OPEN  eenh_cur;
    FETCH eenh_cur
    INTO  v_return;
    CLOSE eenh_cur;
    -- niet gevonden
    IF ( v_return IS NULL )
    THEN
      -- zoek naar redelijke match
      OPEN  eenh_cur2;
      FETCH eenh_cur2
      INTO  v_return;
      IF ( eenh_cur2%found )
      THEN
        FETCH eenh_cur2
        INTO  v_tmp;
        -- maar niet meer dan 1 match
        IF ( eenh_cur2%found )
        THEN
          v_return := NULL;
        END IF;
      END IF;
      CLOSE eenh_cur2;
    END IF;
  END IF;
  RETURN( v_return );
END controleer;

END kgc_eenh_00;
/

/
QUIT
