CREATE OR REPLACE PACKAGE "HELIX"."KGC_TRGE_00" IS
-- aantal per stoftest per standaardfractie
TYPE stof_rec_spoed IS RECORD
( meet_id NUMBER
, stof_id NUMBER
, soort_fractie NUMBER
, stof_code NUMBER
);
TYPE stof_tab_spoed_type IS TABLE OF stof_rec_spoed
INDEX BY BINARY_INTEGER; -- volgnr
stof_tab_spoed  stof_tab_spoed_type;

FUNCTION x_as
( p_trty_id IN NUMBER
, p_positie IN NUMBER
)
RETURN VARCHAR2
;
PRAGMA RESTRICT_REFERENCES( x_as, WNDS, WNPS, RNPS );

FUNCTION y_as
( p_trty_id IN NUMBER
, p_positie IN NUMBER
)
RETURN VARCHAR2
;
PRAGMA RESTRICT_REFERENCES( y_as, WNDS, WNPS, RNPS );
PROCEDURE fixeer
( p_trge_id IN NUMBER
, p_janee   IN VARCHAR2
);
-- functie tbv. matrixrapport KGCTRVU51
-- toon de positie op basis van een volgnr
FUNCTION  positie_vnr
( p_trty_id IN NUMBER
, p_volgnr IN NUMBER
)
RETURN VARCHAR2
;
PRAGMA RESTRICT_REFERENCES( positie_vnr, WNDS, WNPS, RNPS );
-- functie voor rapport BASWLMD51
-- toon de positie, bv.: "A:6(3)"
-- 1 van de 2 parameters moet opgegeven zijn
FUNCTION  positie
( p_trvu_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
)
RETURN VARCHAR2
;
PRAGMA RESTRICT_REFERENCES( positie, WNDS, WNPS, RNPS );

-- retourneer trayinformatie voor een meetwaarde
FUNCTION tray_info
( p_trvu_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
)
RETURN  VARCHAR2
;

-- initialiseer de selectie (tray-wizard)
PROCEDURE init_selectie;


PROCEDURE gewenst_aantal
( p_stof_id IN NUMBER
, p_stan_id IN NUMBER := NULL
, p_soort_fractie IN VARCHAR2
, x_aantal IN OUT NUMBER
);

FUNCTION aantal_spoed
( p_stof_id IN NUMBER
)
RETURN NUMBER;

-- bewaar selectie van aangemelde stoftesten in pl/sql-tables
PROCEDURE selectie
( p_kafd_id IN NUMBER
, p_where_1 IN VARCHAR2
, p_where_2 IN VARCHAR2
, p_max_aantal_f IN NUMBER := NULL
, p_max_aantal_s IN NUMBER := NULL
, p_excl_duplo IN VARCHAR2 := 'J'
);

-- lijst met geselecteerde stof_ids, tbv. performance (procedure kgc_trge_00.vul_tray en kgctray13.p21 en .p31)
-- NB.: alleen de selectie voor gewone fracties; de selectie van standaardfracties volgt die lijst
FUNCTION geselecteerde_stoftesten
RETURN VARCHAR2;

-- aantal geselecteerde (of gewenste) rijen (in pl/sql-table)?
FUNCTION aantal_geselecteerd
( p_stof_id IN NUMBER := NULL
, p_stan_id IN NUMBER := NULL
, p_soort_fractie IN VARCHAR2 := NULL
, p_soort IN VARCHAR2 := 'S' -- Selectie, Wens, Plaats
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( aantal_geselecteerd, WNDS, WNPS );

-- is meetwaarde geselecteerd (in pl/sql-table)?
--FUNCTION meetwaarde_geselecteerd
--( p_meet_id IN NUMBER--
--)
--RETURN VARCHAR2; -- J/N

-- zet de substoftesten ook op de tray
PROCEDURE vul_samenstelling
( p_meet_id IN NUMBER
, p_trge_id IN NUMBER := NULL
, p_x_positie IN VARCHAR2 := NULL
, p_y_positie IN VARCHAR2 := NULL
, p_werklijst_nummer IN VARCHAR2 := NULL
, p_z_positie IN NUMBER := 0
);

-- bepaal het aantal nog openstaande plaatsen op de tray
function aantal_open_plaatsen
(p_trge_id in number
,p_trty_id in number
) return number
;

PROCEDURE selectie_fracties
( p_stof_id        IN NUMBER
, p_stan_id        IN NUMBER default NULL
, p_where          IN VARCHAR2
, p_soort_fractie  IN VARCHAR2
, p_aantal_spoed   IN NUMBER default NULL
, p_aantal         IN NUMBER
, p_aantal_gewenst IN NUMBER
);

PROCEDURE vul_tray
( p_trty_id IN NUMBER
, p_trgr_id IN NUMBER
, p_trge_id IN OUT NUMBER
, p_werklijst_maken IN VARCHAR2 := 'N'
, p_standaard_tray  IN VARCHAR2 := 'N'--Added by Rajendra for Mantis 7962
);

PROCEDURE fracties_sorteren
( p_trgr_id IN NUMBER
);
END KGC_TRGE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_TRGE_00" IS
-- maximum aantal te selecteren per stoftest
-- 16-12-2003: niet maximum aantal te selecteren, maar default waarde voor gewenst.
   max_aantal_f   NUMBER := 9999999999;
   max_aantal_s   NUMBER := 9999999999;

   FUNCTION x_as (p_trty_id IN NUMBER, p_positie IN NUMBER)
      RETURN VARCHAR2
   IS
      v_return   VARCHAR2 (10);

      CURSOR trty_cur
      IS
         SELECT horizontaal, indicatie_hor
           FROM kgc_tray_types trty
          WHERE trty_id = p_trty_id
                                   --    AND trty.vervallen='N' --Added by abhijit for Mantis 2831
      ;

      trty_rec   trty_cur%ROWTYPE;
      v_num      NUMBER;
   BEGIN
      OPEN trty_cur;

      FETCH trty_cur
       INTO trty_rec;

      CLOSE trty_cur;

      IF (p_positie <= trty_rec.horizontaal)
      THEN
         BEGIN
            v_num := trty_rec.indicatie_hor;
            v_return :=
                  TO_CHAR (TO_NUMBER (trty_rec.indicatie_hor) + p_positie - 1);
         EXCEPTION
            WHEN VALUE_ERROR
            THEN
               -- alfanumeriek
               v_return :=
                          CHR (ASCII (trty_rec.indicatie_hor) + p_positie - 1);
         END;
      END IF;

      RETURN (v_return);
   END x_as;

   FUNCTION y_as (p_trty_id IN NUMBER, p_positie IN NUMBER)
      RETURN VARCHAR2
   IS
      v_return   VARCHAR2 (10);

      CURSOR trty_cur
      IS
         SELECT verticaal, indicatie_ver
           FROM kgc_tray_types trty
          WHERE trty_id = p_trty_id
                                   --    AND trty.vervallen='N' --Added by abhijit for Mantis 2831
      ;

      trty_rec   trty_cur%ROWTYPE;
      v_num      NUMBER;
   BEGIN
      OPEN trty_cur;

      FETCH trty_cur
       INTO trty_rec;

      CLOSE trty_cur;

      IF (p_positie <= trty_rec.verticaal)
      THEN
         BEGIN
            v_num := trty_rec.indicatie_ver;
            v_return :=
                  TO_CHAR (TO_NUMBER (trty_rec.indicatie_ver) + p_positie - 1);
         EXCEPTION
            WHEN VALUE_ERROR
            THEN
               -- alfanumeriek
               v_return :=
                          CHR (ASCII (trty_rec.indicatie_ver) + p_positie - 1);
         END;
      END IF;

      RETURN (v_return);
   END y_as;

   PROCEDURE fixeer (p_trge_id IN NUMBER, p_janee IN VARCHAR2)
   IS
   BEGIN
      UPDATE kgc_tray_gebruik
         SET gefixeerd = p_janee
       WHERE trge_id = p_trge_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         qms$errors.show_message
                            (p_mesg        => 'KGC-00000',
                             p_param0      => 'Fout bij het fixeren van de tray.',
                             p_param1      => SQLERRM,
                             p_errtp       => 'E',
                             p_rftf        => TRUE
                            );
   END fixeer;

   FUNCTION positie_vnr (p_trty_id IN NUMBER, p_volgnr IN NUMBER)
      RETURN VARCHAR2
   IS
      v_return   VARCHAR2 (10);

      CURSOR trty_cur
      IS
         SELECT verticaal, horizontaal
           FROM kgc_tray_types trty
          WHERE trty_id = p_trty_id
                                   --   AND trty.vervallen='N' --Added by abhijit for Mantis 2831
      ;

      trty_rec   trty_cur%ROWTYPE;
      i          NUMBER             := 0;
   BEGIN
      OPEN trty_cur;

      FETCH trty_cur
       INTO trty_rec;

      CLOSE trty_cur;

      FOR y IN 1 .. trty_rec.verticaal
      LOOP
         FOR x IN 1 .. trty_rec.horizontaal
         LOOP
            i := i + 1;

            IF (i = p_volgnr)
            THEN
               v_return :=
                     NVL (y_as (p_trty_id, y), '?')
                  || ';'
                  || NVL (x_as (p_trty_id, x), '?');
               RETURN (v_return);
            END IF;
         END LOOP;
      END LOOP;

      RETURN (v_return);
   END positie_vnr;

   FUNCTION positie (p_trvu_id IN NUMBER := NULL, p_meet_id IN NUMBER := NULL)
      RETURN VARCHAR2
   IS
      v_return   VARCHAR2 (100);

      CURSOR trvu_cur
      IS
         SELECT NVL (y_positie, '?') y_positie,
                NVL (x_positie, '?') x_positie,
                NVL (z_positie, '0') z_positie
           FROM kgc_tray_vulling
          WHERE trvu_id = NVL (p_trvu_id, trvu_id)
            AND meet_id = NVL (p_meet_id, meet_id);

      trvu_rec   trvu_cur%ROWTYPE;
   BEGIN
      IF (p_trvu_id IS NULL AND p_meet_id IS NULL)
      THEN
         RETURN (NULL);
      END IF;

      OPEN trvu_cur;

      FETCH trvu_cur
       INTO trvu_rec;

      IF (trvu_cur%FOUND)
      THEN
         v_return := trvu_rec.y_positie || ';' || trvu_rec.x_positie;

         IF (NVL (trvu_rec.z_positie, 0) <> 0)
         THEN
            v_return := v_return || '(' || trvu_rec.z_positie || ')';
         END IF;
      ELSE
         v_return := NULL;
      END IF;

      CLOSE trvu_cur;

      RETURN (SUBSTR (v_return, 1, 10));
   END positie;

   FUNCTION tray_info (p_trvu_id IN NUMBER := NULL, p_meet_id IN NUMBER
            := NULL)
      RETURN VARCHAR2
   IS
      v_return    VARCHAR2 (4000);

      CURSOR trge_cur
      IS
         SELECT trty.omschrijving trty_omschrijving, trge.datum,
                mede.naam mede_naam
           FROM kgc_tray_types trty,
                kgc_medewerkers mede,
                kgc_tray_gebruik trge,
                kgc_tray_vulling trvu
          WHERE trvu.trge_id = trge.trge_id
            AND trge.trty_id = trty.trty_id
            AND trge.mede_id = mede.mede_id
            AND trvu.trvu_id = NVL (p_trvu_id, trvu_id)
            AND trvu.meet_id = NVL (p_meet_id, meet_id)
                                                       --AND trty.vervallen='N' --Added by abhijit for Mantis 2831
      ;

      trge_rec    trge_cur%ROWTYPE;
      v_positie   VARCHAR2 (100);
   BEGIN
      IF (p_trvu_id IS NULL AND p_meet_id IS NULL)
      THEN
         RETURN (NULL);
      END IF;

      v_positie := positie (p_trvu_id => p_trvu_id, p_meet_id => p_meet_id);

      OPEN trge_cur;

      FETCH trge_cur
       INTO trge_rec;

      IF (trge_cur%FOUND)
      THEN
         v_return :=
               trge_rec.trty_omschrijving
            || ', '
            || TO_CHAR (trge_rec.datum, 'dd-mm-yyyy')
            || ', '
            || trge_rec.mede_naam
            || ' ('
            || v_positie
            || ')';
      ELSE
         v_return := NULL;
      END IF;

      CLOSE trge_cur;

      RETURN (SUBSTR (v_return, 1, 2000));
   END tray_info;

   PROCEDURE init_selectie
   IS
   BEGIN
      DELETE      kgc_tray_wizard_stof_temp
            WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');

      DELETE      kgc_tray_wizard_stan_temp
            WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');

      DELETE      kgc_tray_wizard_temp
            WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');

      COMMIT;
   END init_selectie;

   FUNCTION aantal_spoed (p_stof_id IN NUMBER)
      RETURN NUMBER
   IS
      CURSOR spoed_cur (b_stof_id IN NUMBER)
      IS
         SELECT COUNT (*)
           FROM bas_tray_meetwaarden_1_vw tmw1
          WHERE stof_id = b_stof_id
            AND spoed = 'J'
            AND EXISTS (
                   SELECT NULL
                     FROM kgc_tray_wizard_temp trws
                    WHERE trws.meet_id = tmw1.meet_id
                      AND trws.session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID'));

      v_aantal_spoed   NUMBER;
   BEGIN
      OPEN spoed_cur (b_stof_id => p_stof_id);

      FETCH spoed_cur
       INTO v_aantal_spoed;

      CLOSE spoed_cur;

      RETURN (v_aantal_spoed);
   END aantal_spoed;

   PROCEDURE gewenst_aantal (
      p_stof_id         IN       NUMBER,
      p_stan_id         IN       NUMBER,
      p_soort_fractie   IN       VARCHAR2,
      x_aantal          IN OUT   NUMBER
   )
   IS
   BEGIN
      IF (p_soort_fractie = 'F')
      THEN
         SELECT gewenst
           INTO x_aantal
           FROM kgc_tray_wizard_stof_temp
          WHERE stof_id = p_stof_id
            AND session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');
      ELSIF (p_soort_fractie = 'S')
      THEN
         SELECT gewenst
           INTO x_aantal
           FROM kgc_tray_wizard_stan_temp
          WHERE stof_id = p_stof_id
            AND stan_id = p_stan_id
            AND session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');
      END IF;
   END gewenst_aantal;

   PROCEDURE selectie (
      p_kafd_id        IN   NUMBER,
      p_where_1        IN   VARCHAR2,
      p_where_2        IN   VARCHAR2,
      p_max_aantal_f   IN   NUMBER := NULL,
      p_max_aantal_s   IN   NUMBER := NULL,
      p_excl_duplo     IN   VARCHAR2 := 'J'
   )
   IS
      v_statement   VARCHAR2 (32767);
      v_where_1     VARCHAR2 (32000) := '(1=1)';
      v_where_2     VARCHAR2 (32000) := '(1=1)';
   BEGIN
      init_selectie;

      -- vul where-clausule aan
      IF (p_where_1 IS NOT NULL)
      THEN
         v_where_1 :=
               v_where_1
            || CHR (10)
            || ' and '
            || REPLACE (p_where_1, 'BAS_TRAY_MEETWAARDEN_VW', 'tmwv');
      END IF;

      IF (p_where_1 IS NOT NULL)
      THEN
         v_where_2 :=
               v_where_2
            || CHR (10)
            || ' and '
            || REPLACE (p_where_2, 'BAS_TRAY_MEETWAARDEN_VW', 'tmwv');
      END IF;

      -- max aantal wordt gebruikt in voeg_toe
      max_aantal_f := NVL (p_max_aantal_f, 9999999999);
      max_aantal_s := NVL (p_max_aantal_s, 9999999999);
      v_statement :=
            'declare'
         || CHR (10)
         || 'v_melding varchar2(1000);'
         || CHR (10)
         || 'v_aantal  number;'
         || CHR (10)
         || 'v_meet_id number;'
         || CHR (10)
         || 'cursor c is'
         || CHR (10)
         || 'select tmwv.meet_id'
         || CHR (10)
         || ',      tmwv.stof_id'
         || CHR (10)
         || ',      tmwv.stof_code'
         || CHR (10)
         || ',      tmwv.fractienummer'
         || CHR (10)
         || ',      tmwv.stof_omschrijving'
         || CHR (10)
         || ',      tmwv.voortest_omschrijving'
         || CHR (10)
         || 'from bas_tray_meetwaarden_1_vw tmwv'
         || CHR (10)
         || 'where tmwv.kafd_id = '
         || TO_CHAR (p_kafd_id)
         || CHR (10)
         || 'and '
         || v_where_1
         || CHR (10)
         || ';'
         || CHR (10)
         || 'CURSOR meet_cur(b_meet_id in number)'
         || CHR (10)
         || 'IS'
         || CHR (10)
         || '  SELECT meet.stof_id'
         || CHR (10)
         || ',        DECODE( meti.stan_id'
         || CHR (10)
         || '               , NULL, ''F'' '
         || CHR (10)
         || '               , ''S'' '
         || CHR (10)
         || '               ) soort_fractie'
         || CHR (10)
         || '  ,      meti.stan_id'
         || CHR (10)
         || '  ,      meti.frac_id'
         || CHR (10)
         || '  ,      meti.onde_id'
         || CHR (10)
         || '  FROM   bas_metingen    meti'
         || CHR (10)
         || '  ,      bas_meetwaarden meet'
         || CHR (10)
         || '  WHERE  meet.meti_id    = meti.meti_id'
         || CHR (10)
         || '  AND    meet.meet_id    = b_meet_id'
         || CHR (10)
         || '  ;'
         || CHR (10)
         || 'meet_rec meet_cur%rowtype;'
         || CHR (10)
         || 'CURSOR duplo_cur'
         || CHR (10)
         || '( b_stof_id IN NUMBER'
         || CHR (10)
         || ', b_stan_id IN NUMBER'
         || CHR (10)
         || ', b_frac_id IN NUMBER'
         || CHR (10)
         || ', b_onde_id IN NUMBER'
         || CHR (10)
         || ')'
         || CHR (10)
         || 'IS'
         || CHR (10)
         || '  SELECT meet.meet_id'
         || CHR (10)
         || '  FROM   bas_metingen    meti'
         || CHR (10)
         || '  ,      bas_meetwaarden meet'
         || CHR (10)
         || '  WHERE  meet.meti_id    = meti.meti_id'
         || CHR (10)
         || '  AND    meet.stof_id    = b_stof_id'
         || CHR (10)
         || '  AND  ( meti.stan_id    = b_stan_id'
         || CHR (10)
         || '    OR ( meti.onde_id  = b_onde_id'
         || CHR (10)
         || '       AND meti.frac_id  = b_frac_id'
         || CHR (10)
         || '         )'
         || CHR (10)
         || '       )'
         || CHR (10)
         || '  ;'
         || CHR (10)
         || 'cursor c_meet_bestaat (b_meet_id in number)'
         || CHR (10)
         || 'is'
         || CHR (10)
         || '  select meet_id'
         || CHR (10)
         || '  from kgc_tray_wizard_temp'
         || CHR (10)
         || '  where meet_id = b_meet_id '
         || CHR (10)
         || '  and   session_id = sys_context (''USERENV'', ''SESSIONID'')'
         || CHR (10)
         || '  ;'
         || CHR (10)
         || 'cursor c_stof_gewenst (b_stof_id in number)'
         || CHR (10)
         || 'is'
         || CHR (10)
         || '  select geselecteerd'
         || CHR (10)
         || '  from   kgc_tray_wizard_stof_temp'
         || CHR (10)
         || '  where  stof_id = b_stof_id'
         || CHR (10)
         || '  and    session_id = sys_context (''USERENV'', ''SESSIONID'')'
         || CHR (10)
         || '  ;'
         || CHR (10)
         || 'begin'
         || CHR (10)
         || 'for r in c loop'
         || CHR (10)
         || 'begin'
         || CHR (10)
         || 'OPEN  meet_cur(b_meet_id => r.meet_id);'
         || CHR (10)
         || 'FETCH meet_cur INTO meet_rec;'
         || CHR (10)
         || 'CLOSE meet_cur;'
         || CHR (10)
         || 'IF ( '''
         || NVL (p_excl_duplo, 'J')
         || ''' = ''J'' '
         || CHR (10)
         || 'AND meet_rec.soort_fractie = ''F'' '
         || CHR (10)
         || '   )'
         || CHR (10)
         || 'THEN'
         || CHR (10)
         || '  FOR duplo_rec IN duplo_cur( b_stof_id => meet_rec.stof_id'
         || CHR (10)
         || '                            , b_stan_id => meet_rec.stan_id'
         || CHR (10)
         || '                            , b_frac_id => meet_rec.frac_id'
         || CHR (10)
         || '                            , b_onde_id => meet_rec.onde_id'
         || CHR (10)
         || '                            )'
         || CHR (10)
         || '  LOOP'
         || CHR (10)
         || '    open  c_meet_bestaat (b_meet_id => duplo_rec.meet_id );'
         || CHR (10)
         || '    fetch c_meet_bestaat into v_meet_id; '
         || CHR (10)
         || '    close c_meet_bestaat;'
         || CHR (10)
         || '    IF v_meet_id is not null'
         || CHR (10)
         || '    THEN'
         || CHR (10)
         || '      RETURN;'
         || CHR (10)
         || '    END IF;'
         || CHR (10)
         || '  END LOOP;'
         || CHR (10)
         || 'END IF;'
         || CHR (10)
         || 'insert into kgc_tray_wizard_temp'
         || CHR (10)
         || '( meet_id'
         || CHR (10)
         || ', stof_id'
         || CHR (10)
         || ', soort_fractie'
         || CHR (10)
         || ', stof_code'
         || CHR (10)
         || ', fractienummer'
         || CHR (10)
         || ', stof_omschrijving'
         || CHR (10)
         || ', voortest_omschrijving'
         || CHR (10)
         || ')'
         || CHR (10)
         || 'values'
         || CHR (10)
         || '( r.meet_id'
         || CHR (10)
         || ', r.stof_id'
         || CHR (10)
         || ', ''F'''
         || CHR (10)
         || ', r.stof_code'
         || CHR (10)
         || ', r.fractienummer'
         || CHR (10)
         || ', r.stof_omschrijving'
         || CHR (10)
         || ', r.voortest_omschrijving'
         || CHR (10)
         || ');'
         || CHR (10)
         || 'v_aantal := 1;'
         || CHR (10)
         || 'update kgc_tray_wizard_stof_temp'
         || CHR (10)
         || 'set    geselecteerd = geselecteerd + v_aantal'
         || CHR (10)
         || 'where  stof_id = meet_rec.stof_id'
         || CHR (10)
         || 'and    session_id = sys_context (''USERENV'', ''SESSIONID'')'
         || CHR (10)
         || ';'
         || CHR (10)
         || 'if sql%rowcount = 0'
         || CHR (10)
         || 'then'
         || CHR (10)
         || '  insert into kgc_tray_wizard_stof_temp'
         || CHR (10)
         || '  ( stof_id'
         || CHR (10)
         || '  , geselecteerd'
         || CHR (10)
         || '  , origineel_gewenst'
         || CHR (10)
         || '  , gewenst'
         || CHR (10)
         || '  , geplaatst'
         || CHR (10)
         || '  )'
         || CHR (10)
         || '  values'
         || CHR (10)
         || '  ( r.stof_id'
         || CHR (10)
         || '  , 1'
         || CHR (10)
         || '  , 0'
         || CHR (10)
         || '  , 0'
         || CHR (10)
         || '  , 0'
         || CHR (10)
         || '  );'
         || CHR (10)
         || 'end if'
         || CHR (10)
         || ';'
         || CHR (10)
         || 'open  c_stof_gewenst(b_stof_id => meet_rec.stof_id);'
         || CHR (10)
         || 'fetch c_stof_gewenst into v_aantal;'
         || CHR (10)
         || 'close c_stof_gewenst;'
         || CHR (10)
         || 'IF ( v_aantal > '
         || max_aantal_f
         || ' )'
         || CHR (10)
         || 'THEN'
         || CHR (10)
         || '  v_aantal := '
         || max_aantal_f
         || ';'
         || CHR (10)
         || 'END IF;'
         || CHR (10)
         || 'update kgc_tray_wizard_stof_temp'
         || CHR (10)
         || 'set    gewenst = v_aantal'
         || CHR (10)
         || ',      origineel_gewenst = v_aantal'
         || CHR (10)
         || 'where  stof_id = meet_rec.stof_id'
         || CHR (10)
         || 'and    session_id = sys_context (''USERENV'', ''SESSIONID'')'
         || CHR (10)
         || ';'
         || CHR (10)
         || 'exception when value_error then v_melding := ''Maximum van 1000 stoftesten is bereikt.''; raise;'
         || CHR (10)
         || ' when others then v_melding := substr(sqlerrm,1,1000); raise;'
         || CHR (10)
         || 'end;'
         || CHR (10)
         || 'end loop;'
         || CHR (10)
         || 'exception when others then'
         || CHR (10)
         || 'qms$errors.show_message'
         || CHR (10)
         || '(p_mesg => ''KGC-00000'''
         || CHR (10)
         || ',p_param0 => nvl(v_melding,''Er is een fout opgetreden'')'
         || CHR (10)
         || ',p_param1 => ''Selectie is niet (volledig) gemaakt.'''
         || CHR (10)
         || ',p_errtp => ''I'''
         || CHR (10)
         || ',p_rftf => true'
         || CHR (10)
         || ');'
         || CHR (10)
         || 'end;';

      BEGIN
         kgc_util_00.dyn_exec (v_statement);
      END;

      v_statement :=
            'declare'
         || CHR (10)
         || 'v_melding varchar2(1000);'
         || CHR (10)
         || 'v_meet_id number;'
         || CHR (10)
         || 'v_aantal  number;'
         || CHR (10)
         || 'cursor c is'
         || CHR (10)
         || 'select tmwv.meet_id'
         || CHR (10)
         || ',      tmwv.stof_id'
         || CHR (10)
         || ',      tmwv.stan_id'
         || CHR (10)
         || ',      tmwv.stof_code'
         || CHR (10)
         || ',      tmwv.fractienummer'
         || CHR (10)
         || ',      tmwv.stof_omschrijving'
         || CHR (10)
         || ',      tmwv.voortest_omschrijving'
         || CHR (10)
         || 'from bas_tray_meetwaarden_2_vw tmwv'
         || CHR (10)
         || 'where tmwv.kafd_id = '
         || TO_CHAR (p_kafd_id)
         || CHR (10)
         || 'and '
         || v_where_2
         || CHR (10)
         || 'and tmwv.stof_id in (select stof_id from kgc_tray_wizard_stof_temp where session_id = sys_context (''USERENV'', ''SESSIONID''))'
         || CHR (10)
         || ';'
         || CHR (10)
         || 'CURSOR meet_cur(b_meet_id in number)'
         || CHR (10)
         || 'IS'
         || CHR (10)
         || '  SELECT meet.stof_id'
         || CHR (10)
         || '  ,      DECODE( meti.stan_id'
         || CHR (10)
         || '               , NULL, ''F'' '
         || CHR (10)
         || '               , ''S'' '
         || CHR (10)
         || '               ) soort_fractie'
         || CHR (10)
         || '  ,      meti.stan_id'
         || CHR (10)
         || '  ,      meti.frac_id'
         || CHR (10)
         || '  ,      meti.onde_id'
         || CHR (10)
         || '  FROM   bas_metingen    meti'
         || CHR (10)
         || '  ,      bas_meetwaarden meet'
         || CHR (10)
         || '  WHERE  meet.meti_id    = meti.meti_id'
         || CHR (10)
         || '  AND    meet.meet_id    = b_meet_id'
         || CHR (10)
         || '  ;'
         || CHR (10)
         || 'meet_rec meet_cur%rowtype;'
         || CHR (10)
         || 'CURSOR duplo_cur'
         || CHR (10)
         || '( b_stof_id IN NUMBER'
         || CHR (10)
         || ', b_stan_id IN NUMBER'
         || CHR (10)
         || ', b_frac_id IN NUMBER'
         || CHR (10)
         || ', b_onde_id IN NUMBER'
         || CHR (10)
         || ')'
         || CHR (10)
         || 'IS'
         || CHR (10)
         || '  SELECT meet.meet_id'
         || CHR (10)
         || '  FROM   bas_metingen    meti'
         || CHR (10)
         || '  ,      bas_meetwaarden meet'
         || CHR (10)
         || '  WHERE  meet.meti_id    = meti.meti_id'
         || CHR (10)
         || '  AND    meet.stof_id    = b_stof_id'
         || CHR (10)
         || '  AND  ( meti.stan_id    = b_stan_id'
         || CHR (10)
         || '    OR ( meti.onde_id  = b_onde_id'
         || CHR (10)
         || '       AND meti.frac_id  = b_frac_id'
         || CHR (10)
         || '         )'
         || CHR (10)
         || '       )'
         || CHR (10)
         || '  ;'
         || CHR (10)
         || 'cursor c_meet_bestaat (b_meet_id in number)'
         || CHR (10)
         || 'is'
         || CHR (10)
         || '  select meet_id'
         || CHR (10)
         || '  from kgc_tray_wizard_temp'
         || CHR (10)
         || '  where meet_id = b_meet_id '
         || CHR (10)
         || '  and   session_id = sys_context (''USERENV'', ''SESSIONID'')'
         || CHR (10)
         || '  ;'
         || CHR (10)
         || 'begin'
         || CHR (10)
         || 'for r in c loop'
         || CHR (10)
         || 'begin'
         || CHR (10)
         || 'OPEN  meet_cur(b_meet_id => r.meet_id);'
         || CHR (10)
         || 'FETCH meet_cur INTO meet_rec;'
         || CHR (10)
         || 'CLOSE meet_cur;'
         || CHR (10)
         || 'IF ( '''
         || NVL (p_excl_duplo, 'J')
         || ''' = ''J'' '
         || CHR (10)
         || 'AND meet_rec.soort_fractie = ''F'' '
         || CHR (10)
         || '   )'
         || CHR (10)
         || 'THEN'
         || CHR (10)
         || '  FOR duplo_rec IN duplo_cur( b_stof_id => meet_rec.stof_id'
         || CHR (10)
         || '                            , b_stan_id => meet_rec.stan_id'
         || CHR (10)
         || '                            , b_frac_id => meet_rec.frac_id'
         || CHR (10)
         || '                            , b_onde_id => meet_rec.onde_id'
         || CHR (10)
         || '                            )'
         || CHR (10)
         || '  LOOP'
         || CHR (10)
         || '    open  c_meet_bestaat (b_meet_id => duplo_rec.meet_id );'
         || CHR (10)
         || '    fetch c_meet_bestaat into v_meet_id; '
         || CHR (10)
         || '    close c_meet_bestaat;'
         || CHR (10)
         || '    IF v_meet_id is not null'
         || CHR (10)
         || '    THEN'
         || CHR (10)
         || '      RETURN;'
         || CHR (10)
         || '    END IF;'
         || CHR (10)
         || '  END LOOP;'
         || CHR (10)
         || 'END IF;'
         || CHR (10)
         || 'v_aantal := 1;'
         || CHR (10)
         || 'update kgc_tray_wizard_stan_temp'
         || CHR (10)
         || 'set    geselecteerd = geselecteerd + v_aantal'
         || CHR (10)
         || ',      gewenst      = 0'
         || CHR (10)
         || 'where  stof_id = meet_rec.stof_id'
         || CHR (10)
         || 'and    stan_id = meet_rec.stan_id'
         || CHR (10)
         || 'and    session_id = sys_context (''USERENV'', ''SESSIONID'')'
         || CHR (10)
         || ';'
         || CHR (10)
         || 'if sql%rowcount = 0'
         || CHR (10)
         || 'then'
         || CHR (10)
         || '  insert into kgc_tray_wizard_stan_temp'
         || CHR (10)
         || '  ( stof_id'
         || CHR (10)
         || '  , stan_id'
         || CHR (10)
         || '  , geselecteerd'
         || CHR (10)
         || '  , origineel_gewenst'
         || CHR (10)
         || '  , gewenst'
         || CHR (10)
         || '  , geplaatst'
         || CHR (10)
         || '  )'
         || CHR (10)
         || '  values'
         || CHR (10)
         || '  ( r.stof_id'
         || CHR (10)
         || '  , r.stan_id'
         || CHR (10)
         || '  , 1'
         || CHR (10)
         || '  , 0'
         || CHR (10)
         || '  , 0'
         || CHR (10)
         || '  , 0'
         || CHR (10)
         || '  );'
         || CHR (10)
         || 'end if'
         || CHR (10)
         || ';'
         || CHR (10)
         || 'exception when value_error then v_melding := ''Maximum van 1000 stoftesten is bereikt.''; raise;'
         || CHR (10)
         || ' when others then v_melding := substr(sqlerrm,1,1000); raise;'
         || CHR (10)
         || 'end;'
         || CHR (10)
         || 'end loop;'
         || CHR (10)
         || 'exception when others then'
         || CHR (10)
         || 'qms$errors.show_message'
         || CHR (10)
         || '(p_mesg => ''KGC-00000'''
         || CHR (10)
         || ',p_param0 => nvl(v_melding,''Er is een fout opgetreden'')'
         || CHR (10)
         || ',p_param1 => ''Selectie is niet (volledig) gemaakt.'''
         || CHR (10)
         || ',p_errtp => ''I'''
         || CHR (10)
         || ',p_rftf => true'
         || CHR (10)
         || ');'
         || CHR (10)
         || 'end;';

      BEGIN
         kgc_util_00.dyn_exec (v_statement);
      END;

      COMMIT;
   END selectie;

   FUNCTION geselecteerde_stoftesten
      RETURN VARCHAR2
   IS
      CURSOR c_stof
      IS
         SELECT stof_id
           FROM kgc_tray_wizard_stof_temp
          WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');

      v_return   VARCHAR2 (32000);
   BEGIN
      FOR r_stof IN c_stof
      LOOP
         IF (v_return IS NULL)
         THEN
            v_return := r_stof.stof_id;
         ELSIF (INSTR (v_return, r_stof.stof_id) = 0)
         THEN
            v_return := v_return || ',' || r_stof.stof_id;
         END IF;
      END LOOP;

      RETURN (LTRIM (RTRIM (v_return, ','), ','));
   END geselecteerde_stoftesten;

   FUNCTION aantal_geselecteerd (
      p_stof_id         IN   NUMBER := NULL,
      p_stan_id         IN   NUMBER := NULL,
      p_soort_fractie   IN   VARCHAR2 := NULL,
      p_soort           IN   VARCHAR2
            := 'S'                -- Selectie, Wens, Plaats, Origineel gewenst
   )
      RETURN NUMBER
   IS
      v_return     NUMBER;
      v_return_f   NUMBER;
      v_return_s   NUMBER;
   BEGIN
      IF (p_stof_id IS NULL AND p_soort IN ('S', 'W'))
      THEN
         SELECT COUNT (1)
           INTO v_return
           FROM kgc_tray_wizard_temp
          WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');
      ELSIF p_soort = 'P'
      THEN
         IF p_stof_id IS NULL
         THEN
            SELECT SUM (geplaatst)
              INTO v_return
              FROM (SELECT SUM (geplaatst) geplaatst
                      FROM kgc_tray_wizard_stof_temp
                     WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
                    UNION ALL
                    SELECT SUM (geplaatst) geplaatst
                      FROM kgc_tray_wizard_stan_temp
                     WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID'));
         ELSIF (p_soort_fractie = 'F')
         THEN
            SELECT SUM (geplaatst)
              INTO v_return
              FROM kgc_tray_wizard_stof_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id;
         ELSIF (p_soort_fractie = 'S')
         THEN
            SELECT SUM (geplaatst)
              INTO v_return
              FROM kgc_tray_wizard_stan_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id
               AND stan_id = p_stan_id;
         END IF;
      ELSIF (p_soort = 'W')
      THEN
         IF (p_soort_fractie = 'S')
         THEN
            SELECT gewenst
              INTO v_return
              FROM kgc_tray_wizard_stan_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id
               AND stan_id = p_stan_id;
         ELSIF (p_soort_fractie = 'F')
         THEN
            SELECT gewenst
              INTO v_return
              FROM kgc_tray_wizard_stof_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id;
         END IF;
      ELSE
         IF (p_soort_fractie = 'F')
         THEN
            SELECT geselecteerd
              INTO v_return
              FROM kgc_tray_wizard_stof_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id;
         ELSIF (p_soort_fractie = 'S')
         THEN
            SELECT geselecteerd
              INTO v_return
              FROM kgc_tray_wizard_stan_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id
               AND stan_id = p_stan_id;
         ELSE
            SELECT geselecteerd
              INTO v_return_f
              FROM kgc_tray_wizard_stof_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id;

            SELECT geselecteerd
              INTO v_return_s
              FROM kgc_tray_wizard_stan_temp
             WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
               AND stof_id = p_stof_id
               AND stan_id = p_stan_id;

            v_return := NVL (v_return_f, 0) + NVL (v_return_s, 0);
         END IF;
      END IF;

      RETURN (NVL (v_return, 0));
   END aantal_geselecteerd;

   PROCEDURE vul_samenstelling (
      p_meet_id            IN   NUMBER,
      p_trge_id            IN   NUMBER := NULL,
      p_x_positie          IN   VARCHAR2 := NULL,
      p_y_positie          IN   VARCHAR2 := NULL,
      p_werklijst_nummer   IN   VARCHAR2 := NULL,
      p_z_positie          IN   NUMBER := 0
   )
   IS
      CURSOR trvu_cur
      IS
         SELECT trvu.trge_id, trvu.x_positie, trvu.y_positie
           FROM kgc_tray_vulling trvu
          WHERE trvu.meet_id = p_meet_id;

      trvu_rec   trvu_cur%ROWTYPE;

      CURSOR subs_cur
      IS
         SELECT   meet.meet_id, trvu.trvu_id
             FROM kgc_tray_vulling trvu, bas_meetwaarden meet
            WHERE meet.meet_id_super = p_meet_id AND meet.meet_id = trvu.meet_id(+)
         ORDER BY meet.meet_id;

      v_z        NUMBER             := p_z_positie;
   BEGIN
      IF (p_meet_id IS NULL)
      THEN
         RETURN;
      ELSIF (    p_trge_id IS NOT NULL
             AND p_x_positie IS NOT NULL
             AND p_y_positie IS NOT NULL
            )
      THEN
         trvu_rec.trge_id := p_trge_id;
         trvu_rec.x_positie := p_x_positie;
         trvu_rec.y_positie := p_y_positie;
      ELSE
         OPEN trvu_cur;

         FETCH trvu_cur
          INTO trvu_rec;

         CLOSE trvu_cur;
      END IF;

      IF (   p_meet_id IS NULL
          OR trvu_rec.trge_id IS NULL
          OR trvu_rec.x_positie IS NULL
          OR trvu_rec.y_positie IS NULL
         )
      THEN
         RETURN;
      END IF;

      FOR subs_rec IN subs_cur
      LOOP
         v_z := v_z + 1;

         BEGIN
            IF (subs_rec.trvu_id IS NOT NULL)
            THEN
               -- verplaats sub
               UPDATE kgc_tray_vulling
                  SET x_positie = trvu_rec.x_positie,
                      y_positie = trvu_rec.y_positie,
                      z_positie = v_z
                WHERE trvu_id = subs_rec.trvu_id
                      AND trge_id = trvu_rec.trge_id;
            ELSE
               INSERT INTO kgc_tray_vulling
                           (trge_id, meet_id,
                            x_positie, y_positie, z_positie
                           )
                    VALUES (trvu_rec.trge_id, subs_rec.meet_id,
                            trvu_rec.x_positie, trvu_rec.y_positie, v_z
                           );
            END IF;

            kgc_wlst_00.sync_tray_werklijst
                                     (p_actie                 => 'I',
                                      p_meet_id               => p_meet_id,
                                      p_werklijst_nummer      => p_werklijst_nummer
                                     );
         END;
      END LOOP;
   END vul_samenstelling;

   FUNCTION aantal_open_plaatsen (p_trge_id IN NUMBER, p_trty_id IN NUMBER)
      RETURN NUMBER
   IS
      CURSOR trty_cur
      IS
         SELECT horizontaal * verticaal
           FROM kgc_tray_types trty
          WHERE trty_id = p_trty_id;

      CURSOR trvu_cur
      IS
         SELECT COUNT (DISTINCT y_positie || '-' || x_positie)
           FROM kgc_tray_vulling trvu
          WHERE trge_id = p_trge_id;

      v_aantal_totaal     NUMBER (5) := 0;
      v_aantal_gebruikt   NUMBER (5) := 0;
   BEGIN
      OPEN trty_cur;

      FETCH trty_cur
       INTO v_aantal_totaal;

      CLOSE trty_cur;

      OPEN trvu_cur;

      FETCH trvu_cur
       INTO v_aantal_gebruikt;

      CLOSE trvu_cur;

      RETURN (NVL (v_aantal_totaal, 0) - NVL (v_aantal_gebruikt, 0));
   END aantal_open_plaatsen;

   PROCEDURE fracties_sorteren (p_trgr_id IN NUMBER)
   IS
      CURSOR c_sort (b_trgr_id IN NUMBER)
      IS
         SELECT   kolom, richting
             FROM kgc_tray_grid_sortering
            WHERE trgr_id = b_trgr_id AND vervallen = 'N'
         ORDER BY volgorde;

      v_statement   VARCHAR2 (32767);
      v_sortering   VARCHAR2 (4000);
      v_richting    VARCHAR2 (4);
   BEGIN
      FOR r_sort IN c_sort (b_trgr_id => p_trgr_id)
      LOOP
         IF r_sort.richting = 'A'                                        -- 2
         THEN
            v_richting := 'asc';
         ELSE
            v_richting := 'desc';
         END IF;

         IF v_sortering IS NULL
         THEN
            v_sortering := r_sort.kolom || ' ' || v_richting;
         ELSE
            v_sortering :=
                     v_sortering || ', ' || r_sort.kolom || ' ' || v_richting;
         END IF;
      END LOOP;

      IF v_sortering IS NULL
      THEN
         v_sortering := '1';
      END IF;

      -- sorteer de tabel KGC_TRAY_WIZARD_TEMP
      v_statement :=
            'declare'
         || CHR (10)
         || 'v_volgorde number := 1;'
         || CHR (10)
         || 'cursor c is'
         || CHR (10)
         || 'select meet_id'
         || CHR (10)
         || ',      session_id'
         || CHR (10)
         || 'from kgc_tray_wizard_temp'
         || CHR (10)
         || 'where session_id = '
         || SYS_CONTEXT ('USERENV', 'SESSIONID')
         || CHR (10)
         || 'order by '
         || v_sortering
         || CHR (10)
         || ';'
         || CHR (10)
         || 'begin'
         || CHR (10)
         || 'for r in c loop'
         || CHR (10)
         || 'begin'
         || CHR (10)
         || 'update kgc_tray_wizard_temp'
         || CHR (10)
         || 'set volgorde = v_volgorde'
         || CHR (10)
         || 'where meet_id = r.meet_id'
         || CHR (10)
         || 'and session_id = r.session_id;'
         || CHR (10)
         || 'v_volgorde := v_volgorde + 1;'
         || CHR (10)
         || 'end;'
         || CHR (10)
         || 'end loop;'
         || CHR (10)
         || 'exception when others then'
         || CHR (10)
         || 'qms$errors.show_message'
         || CHR (10)
         || '(p_mesg => ''KGC-00000'''
         || CHR (10)
         || ',p_param0 => ''Er is een fout opgetreden'''
         || CHR (10)
         || ',p_param1 => ''sorteer_fracties is niet juist uitgevoerd.'''
         || CHR (10)
         || ',p_errtp => ''I'''
         || CHR (10)
         || ',p_rftf => true'
         || CHR (10)
         || ');'
         || CHR (10)
         || 'end;';

      BEGIN
         kgc_util_00.dyn_exec (v_statement);
      END;

      COMMIT;
   END fracties_sorteren;

   PROCEDURE selectie_fracties (
      p_stof_id          IN   NUMBER,
      p_stan_id          IN   NUMBER DEFAULT NULL,
      p_where            IN   VARCHAR2,
      p_soort_fractie    IN   VARCHAR2,
      p_aantal_spoed     IN   NUMBER DEFAULT NULL,
      p_aantal           IN   NUMBER,
      p_aantal_gewenst   IN   NUMBER
   )
   IS
      v_statement   VARCHAR2 (32767);
      v_where       VARCHAR2 (4000)  := '(1=1)';
   BEGIN
      IF (p_where IS NOT NULL)
      THEN
         v_where :=
               v_where
            || CHR (10)
            || ' and '
            || REPLACE (p_where, 'BAS_TRAY_MEETWAARDEN_VW', 'tmwv');
      END IF;

      IF p_soort_fractie = 'F'
      THEN
         -- voeg de stoftesten toe aan de tabel KGC_TRAY_WIZARD_TEMP
         v_statement :=
               'declare'
            || CHR (10)
            || 'v_melding varchar2(1000);'
            || CHR (10)
            || 'v_aantal_spoed   number := '
            || p_aantal_spoed
            || ';'
            || CHR (10)
            || 'v_aantal         number := '
            || p_aantal
            || ';'
            || CHR (10)
            || 'v_aantal_gewenst number := '
            || p_aantal_gewenst
            || ';'
            || CHR (10)
            || 'v_session_id     number := sys_context (''USERENV'', ''SESSIONID'');'
            || CHR (10)
            || 'cursor c_spoed is'
            || CHR (10)
            || 'select tmwv.meet_id'
            || CHR (10)
            || ',      tmwv.stof_id'
            || CHR (10)
            || ',      tmwv.stof_code'
            || CHR (10)
            || ',      tmwv.fractienummer'
            || CHR (10)
            || ',      tmwv.stof_omschrijving'
            || CHR (10)
            || ',      tmwv.voortest_omschrijving'
            || CHR (10)
            || 'from bas_tray_meetwaarden_1_vw tmwv'
            || CHR (10)
            || 'where tmwv.stof_id = '
            || p_stof_id
            || CHR (10)
            || 'and   tmwv.spoed = ''J'' '
            || CHR (10)
            || 'and '
            || v_where
            || CHR (10)
            || 'order by nvl(tmwv.geplande_einddatum,to_date(''01-01-1900'',''dd-mm-yyyy'')) desc'
            || CHR (10)
            || ';'
            || CHR (10)
            || 'cursor c is'
            || CHR (10)
            || 'select tmwv.meet_id'
            || CHR (10)
            || ',      tmwv.stof_id'
            || CHR (10)
            || ',      tmwv.stof_code'
            || CHR (10)
            || ',      tmwv.fractienummer'
            || CHR (10)
            || ',      tmwv.stof_omschrijving'
            || CHR (10)
            || ',      tmwv.voortest_omschrijving'
            || CHR (10)
            || 'from bas_tray_meetwaarden_1_vw tmwv'
            || CHR (10)
            || 'where tmwv.stof_id = '
            || p_stof_id
            || CHR (10)
            || 'and   tmwv.spoed = ''N'' '
            || CHR (10)
            || 'and '
            || v_where
            || CHR (10)
            || 'order by nvl(tmwv.geplande_einddatum,to_date(''01-01-1900'',''dd-mm-yyyy'')) desc'
            || CHR (10)
            || ';'
            || CHR (10)
            || 'begin'
            || CHR (10)
            || 'delete kgc_tray_wizard_temp'
            || CHR (10)
            || 'where session_id = v_session_id'
            || CHR (10)
            || 'and   stof_id = '
            || p_stof_id
            || CHR (10)
            || 'and   soort_fractie = ''F'' '
            || CHR (10)
            || ';'
            || CHR (10)
            || 'if '
            || p_aantal_gewenst
            || ' > 0'
            || CHR (10)
            || 'then '
            || CHR (10)
            || '  update kgc_tray_wizard_stof_temp'
            || CHR (10)
            || '  set    gewenst = '
            || p_aantal_gewenst
            || CHR (10)
            || '  where  stof_id = '
            || p_stof_id
            || CHR (10)
            || '  and    session_id = v_session_id;'
            || CHR (10)
            || '  for r_spoed in c_spoed loop'
            || CHR (10)
            || '    begin'
            || CHR (10)
            || '      if v_aantal_spoed > 0 and v_aantal_gewenst > 0'
            || CHR (10)
            || '      then'
            || CHR (10)
            || '        insert into kgc_tray_wizard_temp'
            || CHR (10)
            || '        ( meet_id'
            || CHR (10)
            || '        , stof_id'
            || CHR (10)
            || '        , soort_fractie'
            || CHR (10)
            || '        , stof_code'
            || CHR (10)
            || '        , fractienummer'
            || CHR (10)
            || '        , stof_omschrijving'
            || CHR (10)
            || '        , voortest_omschrijving'
            || CHR (10)
            || '        )'
            || CHR (10)
            || '        values'
            || CHR (10)
            || '        ( r_spoed.meet_id'
            || CHR (10)
            || '        , r_spoed.stof_id'
            || CHR (10)
            || '        , '''
            || p_soort_fractie
            || ''''
            || CHR (10)
            || '        , r_spoed.stof_code'
            || CHR (10)
            || '        , r_spoed.fractienummer'
            || CHR (10)
            || '        , r_spoed.stof_omschrijving'
            || CHR (10)
            || '        , r_spoed.voortest_omschrijving'
            || CHR (10)
            || '        );'
            || CHR (10)
            || '        v_aantal_spoed := v_aantal_spoed - 1;'
            || CHR (10)
            || '        v_aantal_gewenst := v_aantal_gewenst - 1;'
            || CHR (10)
            || '      end if;'
            || CHR (10)
            || '    end;'
            || CHR (10)
            || '  end loop;'
            || CHR (10)
            || '  for r in c loop'
            || CHR (10)
            || '    begin'
            || CHR (10)
            || '      if v_aantal > 0 and v_aantal_gewenst > 0'
            || CHR (10)
            || '      then'
            || CHR (10)
            || '        insert into kgc_tray_wizard_temp'
            || CHR (10)
            || '        ( meet_id'
            || CHR (10)
            || '        , stof_id'
            || CHR (10)
            || '        , soort_fractie'
            || CHR (10)
            || '        , stof_code'
            || CHR (10)
            || '        , fractienummer'
            || CHR (10)
            || '        , stof_omschrijving'
            || CHR (10)
            || '        , voortest_omschrijving'
            || CHR (10)
            || '        )'
            || CHR (10)
            || '        values'
            || CHR (10)
            || '        ( r.meet_id'
            || CHR (10)
            || '        , r.stof_id'
            || CHR (10)
            || '        , '''
            || p_soort_fractie
            || ''''
            || CHR (10)
            || '        , r.stof_code'
            || CHR (10)
            || '        , r.fractienummer'
            || CHR (10)
            || '        , r.stof_omschrijving'
            || CHR (10)
            || '        , r.voortest_omschrijving'
            || CHR (10)
            || '        );'
            || CHR (10)
            || '        v_aantal := v_aantal - 1;'
            || CHR (10)
            || '        v_aantal_gewenst := v_aantal_gewenst - 1;'
            || CHR (10)
            || '      end if;'
            || CHR (10)
            || '    end;'
            || CHR (10)
            || '  end loop;'
            || CHR (10)
            || 'else '
            || CHR (10)
            || '  update kgc_tray_wizard_stof_temp'
            || CHR (10)
            || '  set    gewenst = '
            || p_aantal_gewenst
            || CHR (10)
            || '  where  stof_id = '
            || p_stof_id
            || CHR (10)
            || '  and    session_id = v_session_id;'
            || CHR (10)
            || 'end if;'
            || CHR (10)
            || 'exception when others then'
            || CHR (10)
            || 'qms$errors.show_message'
            || CHR (10)
            || '(p_mesg => ''KGC-00000'''
            || CHR (10)
            || ',p_param0 => nvl(v_melding,''Er is een fout opgetreden'')'
            || CHR (10)
            || ',p_param1 => ''Selectie_fracties is niet (volledig) gemaakt.'''
            || CHR (10)
            || ',p_errtp => ''I'''
            || CHR (10)
            || ',p_rftf => true'
            || CHR (10)
            || ');'
            || CHR (10)
            || 'end;';
      ELSIF p_soort_fractie = 'S'
      THEN
         -- voeg de stoftest/standaardfracties toe aan de tabel KGC_TRAY_WIZARD_TEMP
         v_statement :=
               'declare'
            || CHR (10)
            || 'v_melding varchar2(1000);'
            || CHR (10)
            || 'v_aantal         number := '
            || p_aantal
            || ';'
            || CHR (10)
            || 'v_aantal_gewenst number := '
            || p_aantal_gewenst
            || ';'
            || CHR (10)
            || 'v_session_id     number := sys_context (''USERENV'', ''SESSIONID'');'
            || CHR (10)
            || 'cursor c is'
            || CHR (10)
            || 'select tmwv.meet_id'
            || CHR (10)
            || ',      tmwv.stof_id'
            || CHR (10)
            || ',      tmwv.stof_code'
            || CHR (10)
            || ',      tmwv.stan_id'
            || CHR (10)
            || ',      tmwv.fractienummer'
            || CHR (10)
            || ',      tmwv.stof_omschrijving'
            || CHR (10)
            || ',      tmwv.voortest_omschrijving'
            || CHR (10)
            || 'from bas_tray_meetwaarden_2_vw tmwv'
            || CHR (10)
            || 'where tmwv.stof_id = '
            || p_stof_id
            || CHR (10)
            || 'and   tmwv.stan_id = '
            || p_stan_id
            || CHR (10)
            || 'and '
            || v_where
            || CHR (10)
            || ';'
            || CHR (10)
            || 'begin'
            || CHR (10)
            || '  delete kgc_tray_wizard_temp'
            || CHR (10)
            || '  where session_id = v_session_id'
            || CHR (10)
            || '  and   stof_id = '
            || p_stof_id
            || CHR (10)
            || '  and   stan_id = '
            || p_stan_id
            || CHR (10)
            || '  and   soort_fractie = ''S'' '
            || CHR (10)
            || '  ;'
            || CHR (10)
            || '  update kgc_tray_wizard_stan_temp'
            || CHR (10)
            || '  set gewenst = '
            || p_aantal_gewenst
            || CHR (10)
            || '  ,   origineel_gewenst = '
            || p_aantal_gewenst
            || CHR (10)
            || '  where stof_id = '
            || p_stof_id
            || CHR (10)
            || '  and   stan_id = '
            || p_stan_id
            || CHR (10)
            || '  and   session_id = v_session_id;'
            || CHR (10)
            || '  for r in c loop'
            || CHR (10)
            || '    begin'
            || CHR (10)
            || '      if v_aantal > 0 and v_aantal_gewenst > 0'
            || CHR (10)
            || '      then'
            || CHR (10)
            || '        insert into kgc_tray_wizard_temp'
            || CHR (10)
            || '        ( meet_id'
            || CHR (10)
            || '        , stof_id'
            || CHR (10)
            || '        , soort_fractie'
            || CHR (10)
            || '        , stof_code'
            || CHR (10)
            || '        , stan_id'
            || CHR (10)
            || '        , standaardfractienummer'
            || CHR (10)
            || '        , stof_omschrijving'
            || CHR (10)
            || '        , voortest_omschrijving'
            || CHR (10)
            || '        )'
            || CHR (10)
            || '        values'
            || CHR (10)
            || '        ( r.meet_id'
            || CHR (10)
            || '        , r.stof_id'
            || CHR (10)
            || '        , '''
            || p_soort_fractie
            || ''''
            || CHR (10)
            || '        , r.stof_code'
            || CHR (10)
            || '        , r.stan_id'
            || CHR (10)
            || '        , r.fractienummer'
            || CHR (10)
            || '        , r.stof_omschrijving'
            || CHR (10)
            || '        , r.voortest_omschrijving'
            || CHR (10)
            || '        );'
            || CHR (10)
            || '        v_aantal := v_aantal - 1;'
            || CHR (10)
            || '        v_aantal_gewenst := v_aantal_gewenst - 1;'
            || CHR (10)
            || '      end if;'
            || CHR (10)
            || '    end;'
            || CHR (10)
            || '  end loop;'
            || CHR (10)
            || 'exception when others then'
            || CHR (10)
            || '  qms$errors.show_message'
            || CHR (10)
            || '  (p_mesg => ''KGC-00000'''
            || CHR (10)
            || '  ,p_param0 => nvl(v_melding,''Er is een fout opgetreden'')'
            || CHR (10)
            || '  ,p_param1 => ''Selectie_fracties is niet (volledig) gemaakt.'''
            || CHR (10)
            || '  ,p_errtp => ''I'''
            || CHR (10)
            || '  ,p_rftf => true'
            || CHR (10)
            || '  );'
            || CHR (10)
            || 'end;';
      END IF;

      BEGIN
         kgc_util_00.dyn_exec (v_statement);
      END;

      COMMIT;
   END selectie_fracties;

   PROCEDURE vul_tray (
      p_trty_id           IN       NUMBER,
      p_trgr_id           IN       NUMBER,
      p_trge_id           IN OUT   NUMBER,
      p_werklijst_maken   IN       VARCHAR2 := 'N',
      p_standaard_tray    IN       VARCHAR2
            := 'N'                         --Added by Rajendra for Mantis 7962
   )
   IS
      /*****************************************************************************
           NAME:       vul_tray
           PURPOSE:    vul ��n of meerdere trays met de geselecteerde stoftesten

           REVISIONS:
           Ver        Date        Author           Description
           ---------  ----------  ---------------  ------------------------------------
           1.0        01-06-2006  YAR              HLX-KGCTRAY13-10905
           1.1        03-08-2006  YAR              HLX-KGCTRAY13-10905, v_stof_code niet leegmaken
           1.2        03-10-2006  MKL              Aangegeven sortering gebruiken
           1.3        04-09-2012  KRA              Fout bij het aanmaken van werklijsten/trays via Tray Wizard
           1.4        19-02-2014  RBA              Mantis 7962(Seperate tray for standard fractions)

      **************************************************************************** */
      tijd_begin                DATE;
      tijd_einde                DATE;
      tijd_verstreken           NUMBER;

      CURSOR trty_cur
      IS
         SELECT trty.kafd_id, trty.horizontaal, trty.verticaal
           FROM kgc_tray_grids trgr, kgc_tray_types trty
          WHERE trgr.trty_id = trty.trty_id
            AND trgr.trgr_id = p_trgr_id
            AND trty.trty_id = p_trty_id
            AND trty.vervallen = 'N'        --Added by abhijit for Mantis 2831
                                    ;

      trty_rec                  trty_cur%ROWTYPE;
      v_laatste_hor             VARCHAR2 (10);
      v_laatste_ver             VARCHAR2 (10);

      -- meet_cur: op basis van geselecteerde meetwaarden
      CURSOR meet_cur
      IS
         SELECT   twtp.meet_id, twtp.stan_id, twtp.stof_id,
                  twtp.soort_fractie, onde.kafd_id
             FROM kgc_tray_wizard_temp twtp,
                  kgc_onderzoeken onde,
                  bas_metingen meti,
                  bas_meetwaarden meet
            WHERE twtp.meet_id = meet.meet_id
              AND meet.meti_id = meti.meti_id
              AND meti.onde_id = onde.onde_id(+)
              AND twtp.session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
         --Added by Kartiki for Mantis 6138
         ORDER BY twtp.volgorde;

      -- tgvu_cur: op basis van grid
      CURSOR tgvu_cur
      IS
         SELECT   tgvu.y_positie, tgvu.x_positie, tgvu.inhoud,
                  trgr.stoftest_rij, trgr.richting
             FROM kgc_tray_grid_vulling tgvu, kgc_tray_grids trgr
            WHERE tgvu.trgr_id = trgr.trgr_id
              AND tgvu.inhoud IS NOT NULL
              AND trgr.trgr_id = p_trgr_id
         ORDER BY tgvu.volgorde,
                  LPAD (tgvu.y_positie, 5, '!'),
                  LPAD (tgvu.x_positie, 5, '!');

      --Added by Rajendra for Mantis 7962
      CURSOR tgvu_cur_stan (p_volgorde IN NUMBER)
      IS
         SELECT   tgvu.y_positie, tgvu.x_positie, tgvu.inhoud,
                  trgr.stoftest_rij, trgr.richting
             FROM kgc_tray_grid_vulling tgvu, kgc_tray_grids trgr
            WHERE tgvu.trgr_id = trgr.trgr_id
              AND tgvu.inhoud IS NOT NULL
              AND trgr.trgr_id = p_trgr_id
              AND tgvu.volgorde = p_volgorde
         ORDER BY tgvu.volgorde,
                  LPAD (tgvu.y_positie, 5, '!'),
                  LPAD (tgvu.x_positie, 5, '!');

      -- Volgorde afhankelijk van kgc_tray_sortering
      CURSOR meet_sel_cur (
         b_stoftest_omschrijving   IN   VARCHAR2,
         b_fractienummer           IN   VARCHAR2,
         b_soort_fractie           IN   VARCHAR2,
         b_stof_code               IN   VARCHAR2 := NULL
      )
      IS
         SELECT   twtp.meet_id, twtp.stan_id, twtp.stof_id,
                  twtp.soort_fractie, twtp.stof_code,
                  twtp.standaardfractienummer
             --Added by Rajendra for Mantis 7962
         FROM     kgc_tray_wizard_temp twtp
            WHERE UPPER (twtp.stof_omschrijving) LIKE
                                            NVL (b_stoftest_omschrijving, '%')
              AND UPPER (NVL (twtp.fractienummer, twtp.standaardfractienummer)) LIKE
                                                    NVL (b_fractienummer, '%')
              AND twtp.soort_fractie =
                                     NVL (b_soort_fractie, twtp.soort_fractie)
              AND twtp.stof_code = NVL (b_stof_code, twtp.stof_code)
              AND twtp.session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
         --Added by Kartiki for Mantis 6138
         ORDER BY twtp.soort_fractie,twtp.volgorde;

      --Added by rajendra for Mantis 7962
      CURSOR meet_sel_cur_stan (
         b_stoftest_omschrijving   IN   VARCHAR2,
         b_fractienummer           IN   VARCHAR2,
         b_soort_fractie           IN   VARCHAR2,
         b_stof_code               IN   VARCHAR2 := NULL
      )
      IS
         SELECT   twtp.meet_id, twtp.stan_id, twtp.stof_id,
                  twtp.soort_fractie, twtp.stof_code,
                  twtp.standaardfractienummer
             --Added by Rajendra for Mantis 7962
         FROM     kgc_tray_wizard_temp twtp
            WHERE UPPER (twtp.stof_omschrijving) LIKE
                                            NVL (b_stoftest_omschrijving, '%')
              AND UPPER (NVL (twtp.fractienummer, twtp.standaardfractienummer)) LIKE
                                                    NVL (b_fractienummer, '%')
              AND twtp.soort_fractie =
                                     NVL (b_soort_fractie, twtp.soort_fractie)
              AND twtp.stof_code = NVL (b_stof_code, twtp.stof_code)
              AND twtp.session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
         --Added by Kartiki for Mantis 6138
         ORDER BY twtp.soort_fractie,twtp.volgorde;

      meet_sel_rec              meet_sel_cur%ROWTYPE;
      meet_sel_null             meet_sel_cur%ROWTYPE;
      meet_sel_stan_null        meet_sel_cur_stan%ROWTYPE;
      --Added by rajendra for Mantis 7962
      meet_sel_stan_rec         meet_sel_cur_stan%ROWTYPE;
      --Added by rajendra for Mantis 7962
      v_stof_code_stan          VARCHAR2 (10);
      --Added by Rajendra for Mantis 7962
      v_stof_code               VARCHAR2 (10);      -- stoftest op actuele rij
      v_inhoud                  VARCHAR2 (100);
      tgvu_cur_stan_rec         tgvu_cur_stan%ROWTYPE;

      --Added by rajendra for Mantis 7962

      -- bepaal techniek
      CURSOR stof_cur (p_stof_trge_id IN NUMBER)
      IS
         SELECT DECODE (MIN (stof.tech_id),
                        MAX (stof.tech_id), MIN (stof.tech_id),
                        NULL
                       ) tech_id
           FROM kgc_stoftesten stof,
                bas_meetwaarden meet,
                kgc_tray_vulling trvu
          WHERE meet.stof_id = stof.stof_id
            AND meet.meet_id = trvu.meet_id
            AND trvu.trge_id = p_stof_trge_id;

      --Added by Rajendra for Mantis 7962
      CURSOR stan_cur (p_stan_num IN VARCHAR2)
      IS
         SELECT COUNT (*)
           FROM bas_standaardfracties stan
          WHERE stan.standaardfractienummer = p_stan_num;

      --Commented by rajendra for Mantis 7962
      /*
      CURSOR werk_cur (p_werk_trge_id IN NUMBER)
      IS
         SELECT trge.werklijst_nummer
           FROM kgc_tray_gebruik trge
          WHERE trge.trge_id = p_werk_trge_id;*/

      --Added by Rajendra for Mantis 7962
      CURSOR trvu_cur (p_trvu_trge_id IN NUMBER)
      IS
         SELECT COUNT (*)
           FROM kgc_tray_vulling
          WHERE trge_id = p_trvu_trge_id;

      v_tech_id                 NUMBER;
      v_werklijst_nummer        VARCHAR2 (20);
      v_fout                    BOOLEAN;
      v_melding                 VARCHAR2 (1000);
      v_gewenst                 NUMBER                      := 0;
      v_origineel_gewenst       NUMBER                      := 0;
      v_geplaatst               NUMBER                      := 0;
      v_totaal_gewenst          NUMBER                      := 0;
      v_totaal_geplaatst        NUMBER                      := 0;
      i                         BINARY_INTEGER;
      --Added by Rajendra for Mantis 7962
      v_werklijst_nummer_stan   VARCHAR2 (20);
      stan_count                NUMBER                      := 0;
      v_stan_tray               NUMBER                      := 0;
      v_tray                    NUMBER                      := 0;
      p_trge_id_next            NUMBER                      := 0;
      v_volgorde                NUMBER                      := 0;
      v_total_count             NUMBER                      := 0;
      v_total_count_stan        NUMBER                      := 0;
--End here
   BEGIN
      -- Selecteer de bijbehorende tray types, horizontaal, verticaal en kafd_id
      OPEN trty_cur;

      FETCH trty_cur
       INTO trty_rec;

      IF (trty_cur%NOTFOUND)
      THEN
         CLOSE trty_cur;

         raise_application_error (-20000, 'Onbekend traytype en/of grid');
      ELSE
         CLOSE trty_cur;

         v_laatste_hor := kgc_trge_00.x_as (p_trty_id, trty_rec.horizontaal);
         v_laatste_ver := kgc_trge_00.y_as (p_trty_id, trty_rec.verticaal);
      END IF;

      -- v_totaal_gewenst vullen
      v_totaal_gewenst := aantal_geselecteerd (p_soort => 'W');
      v_origineel_gewenst := v_totaal_gewenst;

      IF (p_trge_id IS NULL)                                              -- 1
      THEN
         -- als er de eerste ronde niets op de tray gezet wordt, dan stoppen
         WHILE v_totaal_gewenst > 0
         LOOP
            --Commented by Rajendra for Mantis 7962(21-05-2014)
               /*
               IF (p_werklijst_maken = 'J')
               THEN
                  v_werklijst_nummer :=
                     kgc_nust_00.genereer_nr (p_nummertype      => 'WERK',
                                              p_ongr_id         => NULL,
                                              p_kafd_id         => trty_rec.kafd_id
                                             );
               END IF;
               */

            --Added IF condition by rajendra for Mantis 7962
            IF p_standaard_tray = 'N'
            THEN
               --Added by Rajendra for Mantis 7962(21-05-2014)
               IF (p_werklijst_maken = 'J')
               THEN
                  v_werklijst_nummer :=
                     kgc_nust_00.genereer_nr (p_nummertype      => 'WERK',
                                              p_ongr_id         => NULL,
                                              p_kafd_id         => trty_rec.kafd_id
                                             );
               END IF;

               --End here
               SELECT kgc_trge_seq.NEXTVAL
                 INTO p_trge_id
                 FROM DUAL;

               BEGIN
                  INSERT INTO kgc_tray_gebruik
                              (trge_id, trty_id, datum,
                               mede_id, werklijst_nummer
                              )
                       VALUES (p_trge_id, p_trty_id, TRUNC (SYSDATE),
                               kgc_mede_00.medewerker_id, v_werklijst_nummer
                              );
               END;

               tijd_begin := SYSDATE;

               FOR r IN tgvu_cur
               LOOP
                  v_inhoud :=
                     TRIM
                        (kgc_util_00.strip
                            (p_string      => kgc_util_00.strip
                                                        (p_string      => r.inhoud,
                                                         p_tag_va      => '[',
                                                         p_tag_tm      => ']',
                                                         p_rest        => 'J'
                                                        ),
                             p_tag_va      => '<',
                             p_tag_tm      => '>',
                             p_rest        => 'J'
                            )
                        );

                  IF (LENGTH (v_inhoud) <> 1)
                  THEN
                     v_inhoud := NULL;
                  END IF;

                  meet_sel_rec := meet_sel_null;

                  -- probeer 1e keus
                  OPEN meet_sel_cur
                         (b_stoftest_omschrijving      => kgc_util_00.strip
                                                                    (r.inhoud,
                                                                     '[',
                                                                     ']',
                                                                     'N'
                                                                    ),
                          b_fractienummer              => kgc_util_00.strip
                                                                    (r.inhoud,
                                                                     '<',
                                                                     '>',
                                                                     'N'
                                                                    )
                                                                     --                          , b_soort_fractie => SUBSTR(r.inhoud,1,1)
                      ,
                          b_soort_fractie              => v_inhoud,
                          b_stof_code                  => v_stof_code
                         );

                  FETCH meet_sel_cur
                   INTO meet_sel_rec;

                  CLOSE meet_sel_cur;

                  IF (meet_sel_rec.meet_id IS NOT NULL)
                  THEN
                     IF (r.stoftest_rij = 'N')
                     THEN
                        v_stof_code := NULL;
                     ELSE
                        v_stof_code := meet_sel_rec.stof_code;
                     END IF;

                     BEGIN
                        INSERT INTO kgc_tray_vulling
                                    (trge_id, y_positie, x_positie,
                                     z_positie, meet_id
                                    )
                             VALUES (p_trge_id, r.y_positie, r.x_positie,
                                     0, meet_sel_rec.meet_id
                                    );

                        v_totaal_gewenst := v_totaal_gewenst - 1;

                        IF meet_sel_rec.soort_fractie = 'F'
                        THEN
                           UPDATE kgc_tray_wizard_stof_temp
                              SET geplaatst = geplaatst + 1,
                                  geselecteerd = geselecteerd - 1
                            WHERE stof_id = meet_sel_rec.stof_id
                              AND session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID');
                        ELSIF meet_sel_rec.soort_fractie = 'S'
                        THEN
                           UPDATE kgc_tray_wizard_stan_temp
                              SET geplaatst = geplaatst + 1,
                                  geselecteerd = geselecteerd - 1
                            WHERE stof_id = meet_sel_rec.stof_id
                              AND stan_id = meet_sel_rec.stan_id
                              AND session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID');
                        END IF;

                        DELETE      kgc_tray_wizard_temp
                              WHERE session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID')
                                AND meet_id = meet_sel_rec.meet_id;

                        IF (v_werklijst_nummer IS NOT NULL)
                        THEN
                           kgc_wlst_00.sync_tray_werklijst
                                    (p_actie                 => 'I',
                                     p_meet_id               => meet_sel_rec.meet_id,
                                     p_trge_id               => p_trge_id,
                                     p_werklijst_nummer      => v_werklijst_nummer
                                    );
                        END IF;

                        vul_samenstelling
                                     (p_meet_id               => meet_sel_rec.meet_id,
                                      p_trge_id               => p_trge_id,
                                      p_x_positie             => r.x_positie,
                                      p_y_positie             => r.y_positie,
                                      p_werklijst_nummer      => v_werklijst_nummer
                                     );
                     END;
                  ELSE
                     IF (    r.stoftest_rij = 'J'
                         AND (   (    r.richting = 'HOR'
                                  AND r.x_positie = v_laatste_hor
                                 )
                              OR (    r.richting = 'VER'
                                  AND r.y_positie = v_laatste_ver
                                 )
                             )
                        )
                     THEN
                        v_stof_code := NULL;
                     ELSIF (r.stoftest_rij = 'N')
                     THEN
                        v_stof_code := NULL;
                     END IF;
                  END IF;
               END LOOP;

               OPEN stof_cur (p_trge_id);

               FETCH stof_cur
                INTO v_tech_id;

               CLOSE stof_cur;

               IF (v_tech_id IS NOT NULL)
               THEN
                  BEGIN
                     UPDATE kgc_tray_gebruik
                        SET tech_id = v_tech_id
                      WHERE trge_id = p_trge_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;

               tijd_einde := SYSDATE;
               v_totaal_gewenst := aantal_geselecteerd (p_soort => 'W');

               -- Als er niets op de tray gezet kan worden, moet er uit de loop gesprongen worden,
               IF v_totaal_gewenst = v_origineel_gewenst
               THEN
                  EXIT;
               END IF;
               -- END LOOP;   --commented IF condition by rajendra for Mantis 7962
            --Added Else condition by Rajendra for Mantis 7962
            --start here
            ELSE
               FOR r IN tgvu_cur
               LOOP
                  v_inhoud :=
                     TRIM
                        (kgc_util_00.strip
                            (p_string      => kgc_util_00.strip
                                                        (p_string      => r.inhoud,
                                                         p_tag_va      => '[',
                                                         p_tag_tm      => ']',
                                                         p_rest        => 'J'
                                                        ),
                             p_tag_va      => '<',
                             p_tag_tm      => '>',
                             p_rest        => 'J'
                            )
                        );

                  IF (LENGTH (v_inhoud) <> 1)
                  THEN
                     v_inhoud := NULL;
                  END IF;

                  meet_sel_rec := meet_sel_null;

                  -- probeer 1e keus
                  OPEN meet_sel_cur
                         (b_stoftest_omschrijving      => kgc_util_00.strip
                                                                    (r.inhoud,
                                                                     '[',
                                                                     ']',
                                                                     'N'
                                                                    ),
                          b_fractienummer              => kgc_util_00.strip
                                                                    (r.inhoud,
                                                                     '<',
                                                                     '>',
                                                                     'N'
                                                                    )
                                                                     --                          , b_soort_fractie => SUBSTR(r.inhoud,1,1)
                      ,
                          b_soort_fractie              => v_inhoud,
                          b_stof_code                  => v_stof_code
                         );

                  FETCH meet_sel_cur
                   INTO meet_sel_rec;

                  CLOSE meet_sel_cur;

                  OPEN stan_cur (meet_sel_rec.standaardfractienummer);

                  FETCH stan_cur
                   INTO stan_count;

                  CLOSE stan_cur;

                  IF stan_count > 0
                  THEN
                     IF v_stan_tray = 0
                     THEN
                        IF (p_werklijst_maken = 'J')
                        THEN
                           --Added by Rajendra for Mantis 7962(21-05-2014)
                           v_werklijst_nummer_stan :=
                              kgc_nust_00.genereer_nr
                                               (p_nummertype      => 'WERK',
                                                p_ongr_id         => NULL,
                                                p_kafd_id         => trty_rec.kafd_id
                                               );
                        END IF;

                        --End Here
                        SELECT kgc_trge_seq.NEXTVAL
                          INTO p_trge_id_next
                          FROM DUAL;

                        INSERT INTO kgc_tray_gebruik
                                    (trge_id, trty_id,
                                     datum,
                                     mede_id,
                                     werklijst_nummer
                                    )
                             VALUES (p_trge_id_next, p_trty_id,
                                     TRUNC (SYSDATE),
                                     kgc_mede_00.medewerker_id,
                                     v_werklijst_nummer_stan
                                    );

                        v_stan_tray := 1;
                     END IF;

                     --Added by Rajendra for Mantis 7962
                     OPEN trvu_cur (p_trge_id_next);

                     FETCH trvu_cur
                      INTO v_total_count_stan;

                     CLOSE trvu_cur;

                     IF v_total_count_stan >=
                                  (trty_rec.horizontaal * trty_rec.verticaal
                                  )
                     THEN
                        --Added by Rajendra for Mantis 7962(21-05-2014)
                        IF (p_werklijst_maken = 'J')
                        THEN
                           v_werklijst_nummer_stan :=
                              kgc_nust_00.genereer_nr
                                               (p_nummertype      => 'WERK',
                                                p_ongr_id         => NULL,
                                                p_kafd_id         => trty_rec.kafd_id
                                               );
                        END IF;

                        --End here
                        SELECT kgc_trge_seq.NEXTVAL
                          INTO p_trge_id_next
                          FROM DUAL;

                        INSERT INTO kgc_tray_gebruik
                                    (trge_id, trty_id,
                                     datum,
                                     mede_id,
                                     werklijst_nummer
                                    )
                             VALUES (p_trge_id_next, p_trty_id,
                                     TRUNC (SYSDATE),
                                     kgc_mede_00.medewerker_id,
                                     v_werklijst_nummer_stan
                                    );

                        v_volgorde := 0;
                     END IF;

                     v_volgorde := v_volgorde + 1;

                     OPEN tgvu_cur_stan (p_volgorde => v_volgorde);

                     FETCH tgvu_cur_stan
                      INTO tgvu_cur_stan_rec;

                     IF tgvu_cur_stan%FOUND
                     THEN
                        v_inhoud :=
                           TRIM
                              (kgc_util_00.strip
                                  (p_string      => kgc_util_00.strip
                                                       (p_string      => tgvu_cur_stan_rec.inhoud,
                                                        p_tag_va      => '[',
                                                        p_tag_tm      => ']',
                                                        p_rest        => 'J'
                                                       ),
                                   p_tag_va      => '<',
                                   p_tag_tm      => '>',
                                   p_rest        => 'J'
                                  )
                              );

                        IF (LENGTH (v_inhoud) <> 1)
                        THEN
                           v_inhoud := NULL;
                        END IF;

                        meet_sel_stan_rec := meet_sel_stan_null;

                        -- probeer 1e keus
                        OPEN meet_sel_cur_stan
                               (b_stoftest_omschrijving      => kgc_util_00.strip
                                                                   (tgvu_cur_stan_rec.inhoud,
                                                                    '[',
                                                                    ']',
                                                                    'N'
                                                                   ),
                                b_fractienummer              => kgc_util_00.strip
                                                                   (tgvu_cur_stan_rec.inhoud,
                                                                    '<',
                                                                    '>',
                                                                    'N'
                                                                   )
                                                                    --                          , b_soort_fractie => SUBSTR(r.inhoud,1,1)
                            ,
                                b_soort_fractie              => v_inhoud,
                                b_stof_code                  => v_stof_code_stan
                               );

                        FETCH meet_sel_cur_stan
                         INTO meet_sel_stan_rec;

                        CLOSE meet_sel_cur_stan;

                        IF (meet_sel_stan_rec.meet_id IS NOT NULL)
                        THEN
                           IF (tgvu_cur_stan_rec.stoftest_rij = 'N')
                           THEN
                              v_stof_code_stan := NULL;
                           ELSE
                              v_stof_code_stan := meet_sel_stan_rec.stof_code;
                           END IF;

                           INSERT INTO kgc_tray_vulling
                                       (trge_id,
                                        y_positie,
                                        x_positie, z_positie,
                                        meet_id
                                       )
                                VALUES (p_trge_id_next,
                                        tgvu_cur_stan_rec.y_positie,
                                        tgvu_cur_stan_rec.x_positie, 0,
                                        meet_sel_stan_rec.meet_id
                                       );

                           IF meet_sel_stan_rec.soort_fractie = 'F'
                           THEN
                              UPDATE kgc_tray_wizard_stof_temp
                                 SET geplaatst = geplaatst + 1,
                                     geselecteerd = geselecteerd - 1
                               WHERE stof_id = meet_sel_stan_rec.stof_id
                                 AND session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID');
                           --Commented by Rajendra for Mantis 7962(02-07-2014)
                           --ELSIF meet_sel_rec.soort_fractie = 'S'
                           ELSIF meet_sel_stan_rec.soort_fractie = 'S'
                           THEN
                              UPDATE kgc_tray_wizard_stan_temp
                                 SET geplaatst = geplaatst + 1,
                                     geselecteerd = geselecteerd - 1
                               WHERE stof_id = meet_sel_stan_rec.stof_id
                                 AND stan_id = meet_sel_stan_rec.stan_id
                                 AND session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID');
                           END IF;

                           DELETE      kgc_tray_wizard_temp
                                 WHERE session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID')
                                   AND meet_id = meet_sel_stan_rec.meet_id;

                           IF (v_werklijst_nummer_stan IS NOT NULL)
                           THEN
                              kgc_wlst_00.sync_tray_werklijst
                                 (p_actie                 => 'I',
                                  p_meet_id               => meet_sel_stan_rec.meet_id,
                                  p_trge_id               => p_trge_id_next,
                                  p_werklijst_nummer      => v_werklijst_nummer_stan
                                 );
                           END IF;

                           vul_samenstelling
                                (p_meet_id               => meet_sel_stan_rec.meet_id,
                                 p_trge_id               => p_trge_id_next,
                                 p_x_positie             => tgvu_cur_stan_rec.x_positie,
                                 p_y_positie             => tgvu_cur_stan_rec.y_positie,
                                 p_werklijst_nummer      => v_werklijst_nummer_stan
                                );
                        ELSE
                           IF (    tgvu_cur_stan_rec.stoftest_rij = 'J'
                               AND (   (    tgvu_cur_stan_rec.richting = 'HOR'
                                        AND tgvu_cur_stan_rec.x_positie =
                                                                 v_laatste_hor
                                       )
                                    OR (    tgvu_cur_stan_rec.richting = 'VER'
                                        AND tgvu_cur_stan_rec.y_positie =
                                                                 v_laatste_ver
                                       )
                                   )
                              )
                           THEN
                              v_stof_code_stan := NULL;
                           ELSIF (tgvu_cur_stan_rec.stoftest_rij = 'N')
                           THEN
                              v_stof_code_stan := NULL;
                           END IF;
                        END IF;
                     END IF;

                     CLOSE tgvu_cur_stan;
                  ELSE
                     IF v_tray = 0
                     THEN
                        --Added by Rajendra for Mantis 7962(21-05-2014)
                        IF (p_werklijst_maken = 'J')
                        THEN
                           v_werklijst_nummer :=
                              kgc_nust_00.genereer_nr
                                               (p_nummertype      => 'WERK',
                                                p_ongr_id         => NULL,
                                                p_kafd_id         => trty_rec.kafd_id
                                               );
                        END IF;

                        --End here
                        SELECT kgc_trge_seq.NEXTVAL
                          INTO p_trge_id
                          FROM DUAL;

                        INSERT INTO kgc_tray_gebruik
                                    (trge_id, trty_id, datum,
                                     mede_id,
                                     werklijst_nummer
                                    )
                             VALUES (p_trge_id, p_trty_id, TRUNC (SYSDATE),
                                     kgc_mede_00.medewerker_id,
                                     v_werklijst_nummer
                                    );

                        v_tray := 1;
                     END IF;

                     --Added by Rajendra for Mantis 7962
                     OPEN trvu_cur (p_trge_id);

                     FETCH trvu_cur
                      INTO v_total_count;

                     CLOSE trvu_cur;

                     IF v_total_count >=
                                  (trty_rec.horizontaal * trty_rec.verticaal
                                  )
                     THEN
                        IF (p_werklijst_maken = 'J')
                        THEN
                           --Added by Rajendra for Mantis 7962(21-05-2014)
                           v_werklijst_nummer :=
                              kgc_nust_00.genereer_nr
                                               (p_nummertype      => 'WERK',
                                                p_ongr_id         => NULL,
                                                p_kafd_id         => trty_rec.kafd_id
                                               );
                        END IF;

                        --End here
                        SELECT kgc_trge_seq.NEXTVAL
                          INTO p_trge_id
                          FROM DUAL;

                        INSERT INTO kgc_tray_gebruik
                                    (trge_id, trty_id, datum,
                                     mede_id,
                                     werklijst_nummer
                                    )
                             VALUES (p_trge_id, p_trty_id, TRUNC (SYSDATE),
                                     kgc_mede_00.medewerker_id,
                                     v_werklijst_nummer
                                    );
                     END IF;

                     IF (meet_sel_rec.meet_id IS NOT NULL)
                     THEN
                        IF (r.stoftest_rij = 'N')
                        THEN
                           v_stof_code := NULL;
                        ELSE
                           v_stof_code := meet_sel_rec.stof_code;
                        END IF;

                        INSERT INTO kgc_tray_vulling
                                    (trge_id, y_positie, x_positie,
                                     z_positie, meet_id
                                    )
                             VALUES (p_trge_id, r.y_positie, r.x_positie,
                                     0, meet_sel_rec.meet_id
                                    );

                        IF meet_sel_rec.soort_fractie = 'F'
                        THEN
                           UPDATE kgc_tray_wizard_stof_temp
                              SET geplaatst = geplaatst + 1,
                                  geselecteerd = geselecteerd - 1
                            WHERE stof_id = meet_sel_rec.stof_id
                              AND session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID');
                        ELSIF meet_sel_rec.soort_fractie = 'S'
                        THEN
                           UPDATE kgc_tray_wizard_stan_temp
                              SET geplaatst = geplaatst + 1,
                                  geselecteerd = geselecteerd - 1
                            WHERE stof_id = meet_sel_rec.stof_id
                              AND stan_id = meet_sel_rec.stan_id
                              AND session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID');
                        END IF;

                        DELETE      kgc_tray_wizard_temp
                              WHERE session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID')
                                AND meet_id = meet_sel_rec.meet_id;

                        IF (v_werklijst_nummer IS NOT NULL)
                        THEN
                           kgc_wlst_00.sync_tray_werklijst
                                    (p_actie                 => 'I',
                                     p_meet_id               => meet_sel_rec.meet_id,
                                     p_trge_id               => p_trge_id,
                                     p_werklijst_nummer      => v_werklijst_nummer
                                    );
                        END IF;

                        vul_samenstelling
                                     (p_meet_id               => meet_sel_rec.meet_id,
                                      p_trge_id               => p_trge_id,
                                      p_x_positie             => r.x_positie,
                                      p_y_positie             => r.y_positie,
                                      p_werklijst_nummer      => v_werklijst_nummer
                                     );
                     ELSE
                        IF (    r.stoftest_rij = 'J'
                            AND (   (    r.richting = 'HOR'
                                     AND r.x_positie = v_laatste_hor
                                    )
                                 OR (    r.richting = 'VER'
                                     AND r.y_positie = v_laatste_ver
                                    )
                                )
                           )
                        THEN
                           v_stof_code := NULL;
                        ELSIF (r.stoftest_rij = 'N')
                        THEN
                           v_stof_code := NULL;
                        END IF;
                     END IF;
                  END IF;
               END LOOP;

               OPEN stof_cur (p_trge_id);

               FETCH stof_cur
                INTO v_tech_id;

               CLOSE stof_cur;

               IF (v_tech_id IS NOT NULL)
               THEN
                  BEGIN
                     UPDATE kgc_tray_gebruik
                        SET tech_id = v_tech_id
                      WHERE trge_id = p_trge_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        NULL;
                  END;
               END IF;

               ---Added by rajendra for Mantis 7962
               IF v_stan_tray = 1
               THEN
                  OPEN stof_cur (p_trge_id_next);

                  FETCH stof_cur
                   INTO v_tech_id;

                  CLOSE stof_cur;

                  IF (v_tech_id IS NOT NULL)
                  THEN
                     BEGIN
                        UPDATE kgc_tray_gebruik
                           SET tech_id = v_tech_id
                         WHERE trge_id = p_trge_id_next;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           NULL;
                     END;
                  END IF;
               END IF;

               --end here
               tijd_einde := SYSDATE;
               v_totaal_gewenst := aantal_geselecteerd (p_soort => 'W');

               -- Als er niets op de tray gezet kan worden, moet er uit de loop gesprongen worden,
               IF v_totaal_gewenst = v_origineel_gewenst
               THEN
                  EXIT;
               END IF;
            END IF;
         END LOOP;
      --End Here
      ELSE
         -- plaatsing op bstaande tray
         FOR r IN meet_cur
         LOOP
            v_gewenst :=
               aantal_geselecteerd (p_stof_id            => r.stof_id,
                                    p_stan_id            => r.stan_id,
                                    p_soort_fractie      => r.soort_fractie,
                                    p_soort              => 'W'
                                   );
            v_geplaatst :=
               aantal_geselecteerd (p_stof_id            => r.stof_id,
                                    p_stan_id            => r.stan_id,
                                    p_soort_fractie      => r.soort_fractie,
                                    p_soort              => 'P'
                                   );

            IF v_totaal_gewenst > 0
            THEN
               --commented by Rajendra for Mantis 7962(19-02-2014)
               /*
               kgc_trgr_00.vul_tray
               ( p_trge_id => p_trge_id
               , p_trgr_id => p_trgr_id
               , p_meet_id => r.meet_id
               , x_fout    => v_fout
               , x_melding => v_melding
               );*/
               --Added IF condition by Rajendra for Mantis 7962(13-02-2014)
               IF p_standaard_tray = 'J'
               THEN
                  IF r.soort_fractie = 'S'
                  THEN
                     --Commented by Rajendra for Mantis 7962(21-05-2014)
                     /*
                        OPEN werk_cur (p_trge_id);

                        FETCH werk_cur
                         INTO v_werklijst_nummer;

                        CLOSE werk_cur;*/
                     IF v_tray = 0
                     THEN
                        --Added by Rajendra for Mantis 7962(21-05-2014)
                        IF (p_werklijst_maken = 'J')
                        THEN
                           v_werklijst_nummer_stan :=
                              kgc_nust_00.genereer_nr
                                               (p_nummertype      => 'WERK',
                                                p_ongr_id         => NULL,
                                                p_kafd_id         => trty_rec.kafd_id
                                               );
                        END IF;

                        --End here
                        SELECT kgc_trge_seq.NEXTVAL
                          INTO p_trge_id_next
                          FROM DUAL;

                        INSERT INTO kgc_tray_gebruik
                                    (trge_id, trty_id,
                                     datum,
                                     mede_id,
                                     werklijst_nummer
                                    )
                             VALUES (p_trge_id_next, p_trty_id,
                                     TRUNC (SYSDATE),
                                     kgc_mede_00.medewerker_id,
                                     v_werklijst_nummer_stan
                                    );

                        v_tray := 1;
                     END IF;

                     -- Added by Rajendra for Mantis 7962
                     OPEN trvu_cur (p_trge_id_next);

                     FETCH trvu_cur
                      INTO v_total_count;

                     CLOSE trvu_cur;

                     IF v_total_count >=
                                  (trty_rec.horizontaal * trty_rec.verticaal
                                  )
                     THEN
                        --Added by Rajendra for Mantis 7962(21-05-2014)
                        IF (p_werklijst_maken = 'J')
                        THEN
                           v_werklijst_nummer_stan :=
                              kgc_nust_00.genereer_nr
                                               (p_nummertype      => 'WERK',
                                                p_ongr_id         => NULL,
                                                p_kafd_id         => trty_rec.kafd_id
                                               );
                        END IF;

                        --End here
                        SELECT kgc_trge_seq.NEXTVAL
                          INTO p_trge_id_next
                          FROM DUAL;

                        INSERT INTO kgc_tray_gebruik
                                    (trge_id, trty_id,
                                     datum,
                                     mede_id,
                                     werklijst_nummer
                                    )
                             VALUES (p_trge_id_next, p_trty_id,
                                     TRUNC (SYSDATE),
                                     kgc_mede_00.medewerker_id,
                                     v_werklijst_nummer_stan
                                    );
                     END IF;

                     kgc_trgr_00.vul_tray (p_trge_id      => p_trge_id_next,
                                           p_trgr_id      => p_trgr_id,
                                           p_meet_id      => r.meet_id,
                                           x_fout         => v_fout,
                                           x_melding      => v_melding
                                          );
                  ELSE
                     kgc_trgr_00.vul_tray (p_trge_id      => p_trge_id,
                                           p_trgr_id      => p_trgr_id,
                                           p_meet_id      => r.meet_id,
                                           x_fout         => v_fout,
                                           x_melding      => v_melding
                                          );
                  END IF;
               ELSE
                  kgc_trgr_00.vul_tray (p_trge_id      => p_trge_id,
                                        p_trgr_id      => p_trgr_id,
                                        p_meet_id      => r.meet_id,
                                        x_fout         => v_fout,
                                        x_melding      => v_melding
                                       );
               END IF;

               --End Here
               IF (v_fout)
               THEN
                  qms$errors.show_message
                       (p_mesg        => 'KGC-00000',
                        p_param0      => 'Fatale fout. Druk op help voor details',
                        p_param1      => v_melding
                       );
               END IF;

               IF (v_melding IS NULL)
               THEN
                  v_totaal_gewenst := v_totaal_gewenst - 1;

                  IF r.soort_fractie = 'F'
                  THEN
                     UPDATE kgc_tray_wizard_stof_temp
                        SET geplaatst = geplaatst + 1,
                            geselecteerd = geselecteerd - 1
                      WHERE stof_id = r.stof_id
                        AND session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');
                  ELSIF r.soort_fractie = 'S'
                  THEN
                     UPDATE kgc_tray_wizard_stan_temp
                        SET geplaatst = geplaatst + 1,
                            geselecteerd = geselecteerd - 1
                      WHERE stof_id = r.stof_id
                        AND stan_id = r.stan_id
                        AND session_id = SYS_CONTEXT ('USERENV', 'SESSIONID');
                  END IF;

                  DELETE      kgc_tray_wizard_temp
                        WHERE session_id =
                                          SYS_CONTEXT ('USERENV', 'SESSIONID')
                          AND meet_id = r.meet_id;

                  --Commented by Rajendra for Mantis 7962(19-02-2014)
                  /*
                  kgc_wlst_00.sync_tray_werklijst
                  ( p_actie            => 'I'
                  , p_meet_id          => r.meet_id
                  , p_trge_id          => p_trge_id
                  , p_werklijst_nummer => v_werklijst_nummer
                  );
                  vul_samenstelling
                  ( p_meet_id          => r.meet_id
                  , p_trge_id          => p_trge_id
                  , p_werklijst_nummer => v_werklijst_nummer
                  );*/
                  --Added IF condition by Rajendra for Mantis 7962(13-02-2014)
                  IF p_standaard_tray = 'J'
                  THEN
                     IF r.soort_fractie = 'S'
                     THEN
                        kgc_wlst_00.sync_tray_werklijst
                               (p_actie                 => 'I',
                                p_meet_id               => r.meet_id,
                                p_trge_id               => p_trge_id_next,
                                p_werklijst_nummer      => v_werklijst_nummer_stan
                               );
                     ELSE
                        kgc_wlst_00.sync_tray_werklijst
                                    (p_actie                 => 'I',
                                     p_meet_id               => r.meet_id,
                                     p_trge_id               => p_trge_id,
                                     p_werklijst_nummer      => v_werklijst_nummer
                                    );
                     END IF;
                  ELSE
                     kgc_wlst_00.sync_tray_werklijst
                                    (p_actie                 => 'I',
                                     p_meet_id               => r.meet_id,
                                     p_trge_id               => p_trge_id,
                                     p_werklijst_nummer      => v_werklijst_nummer
                                    );
                  END IF;

                  --Added IF condition by Rajendra for Mantis 7962(13-02-2014)
                  IF p_standaard_tray = 'J'
                  THEN
                     IF r.soort_fractie = 'S'
                     THEN
                        vul_samenstelling
                               (p_meet_id               => r.meet_id,
                                p_trge_id               => p_trge_id_next,
                                p_werklijst_nummer      => v_werklijst_nummer_stan
                               );
                     ELSE
                        vul_samenstelling
                                    (p_meet_id               => r.meet_id,
                                     p_trge_id               => p_trge_id,
                                     p_werklijst_nummer      => v_werklijst_nummer
                                    );
                     END IF;
                  ELSE
                     vul_samenstelling
                                    (p_meet_id               => r.meet_id,
                                     p_trge_id               => p_trge_id,
                                     p_werklijst_nummer      => v_werklijst_nummer
                                    );
                  END IF;
               END IF;
            END IF;
         END LOOP;
      END IF;

      -- v_totaal_gewenst wordt in het begin al gedaan
      v_totaal_geplaatst := aantal_geselecteerd (p_soort => 'P');
      tijd_verstreken := ROUND ((tijd_einde - tijd_begin) * 24 * 60 * 60, 0);

      -- update gewenst en zet alle geplaatste aantal terug naar 0:
      UPDATE kgc_tray_wizard_stof_temp
         SET gewenst = NVL (origineel_gewenst, 0) - geplaatst,
             geplaatst = 0
       WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
         AND geplaatst > 0;

      UPDATE kgc_tray_wizard_stof_temp
         SET gewenst = 0
       WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID') AND gewenst < 0;

      UPDATE kgc_tray_wizard_stan_temp
         SET gewenst = NVL (origineel_gewenst, 0) - geplaatst,
             geplaatst = 0
       WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID')
         AND geplaatst > 0;

      UPDATE kgc_tray_wizard_stan_temp
         SET gewenst = 0
       WHERE session_id = SYS_CONTEXT ('USERENV', 'SESSIONID') AND gewenst < 0;

      IF (v_totaal_geplaatst > 0)
      THEN
         COMMIT;
      ELSE
         ROLLBACK;
         p_trge_id := NULL;
      END IF;

      --Added by Rajendra for Mantis 7962(21-05-2014)
      IF NVL (p_trge_id_next, 0) > p_trge_id
      THEN
         p_trge_id := p_trge_id_next;
      END IF;

      IF (tijd_verstreken IS NOT NULL)
      THEN
         qms$errors.show_message (p_mesg        => 'KGC-00144',
                                  p_param0      => TO_CHAR (v_totaal_geplaatst),
                                  p_param1      => TO_CHAR
                                                          (v_origineel_gewenst),
                                  p_param2      =>    'Uitgevoerd in '
                                                   || TO_CHAR (tijd_verstreken)
                                                   || ' seconde(n).'
                                 );
      ELSE
         qms$errors.show_message (p_mesg        => 'KGC-00144',
                                  p_param0      => TO_CHAR (v_totaal_geplaatst),
                                  p_param1      => TO_CHAR
                                                          (v_origineel_gewenst)
                                 );
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         qms$errors.show_message
                             (p_mesg        => 'KGC-00000',
                              p_param0      => 'Fout bij het vullen van de tray.',
                              p_param1      => SQLERRM,
                              p_errtp       => 'E',
                              p_rftf        => TRUE
                             );
   END vul_tray;
END KGC_TRGE_00;
/

/
QUIT
