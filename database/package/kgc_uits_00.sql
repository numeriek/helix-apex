CREATE OR REPLACE PACKAGE "HELIX"."KGC_UITS_00" IS
-- zie (voorlopige) uitslagbrief DNA,CYTO:
-- DNA  : onderzoeknr / familienummer (verwijst naar papieren status)
-- CYTO : autorisator / onderzoeknr
FUNCTION referentie
( p_afdeling IN VARCHAR2
, p_onde_id IN NUMBER
, p_onderzoeknr IN VARCHAR2 -- al bekend
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( referentie, WNDS, WNPS, RNPS );

-- context informatie bij een meting (zie KGCUITS10)
FUNCTION context_info
( p_meti_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( context_info, WNDS, WNPS, RNPS );

-- standaard uitslag brieftype voor een onderzoek
-- NB.: wordt van belang bij implementatie METAB/ENZYM!!!
PROCEDURE standaard_uitslag_brieftype
( p_kafd_id IN NUMBER := NULL
, p_onde_id IN NUMBER := NULL
, x_brty_id OUT NUMBER
, x_brty_code OUT VARCHAR2
, x_brty_omschrijving OUT VARCHAR2
);
FUNCTION standaard_uitslag_brieftype
( p_onde_id IN NUMBER
)
RETURN NUMBER;

TYPE kgcuitc51_record IS RECORD
( onde_id NUMBER
, stgr_id NUMBER
, stof_id NUMBER
, kolom1 VARCHAR2(1000)
, kolom2 VARCHAR2(1000)
, kolom3 VARCHAR2(1000)
, kolom4 VARCHAR2(1000)
, kolom5 VARCHAR2(1000)
, kolom6 VARCHAR2(1000)
, kolom7 VARCHAR2(1000)
);
TYPE kgcuitc51_tabtype IS TABLE OF kgcuitc51_record
INDEX BY BINARY_INTEGER;
kgcuitc51_tab kgcuitc51_tabtype;


-- vul een pl/sql-table voor de view voor kgcuitc51 (uitslagbrief controle)
PROCEDURE vul_uitslagbrief_controle
( p_onde_id IN NUMBER
, p_reset IN BOOLEAN := FALSE -- begin met een schone tabel.
);
FUNCTION vul_uitslagbrief_controle
( p_onde_id IN NUMBER
)
RETURN NUMBER;
-- lees een pl/sql-table uit voor de view voor kgcuitc51 (uitslagbrief controle)
FUNCTION uitslagbrief_controle
( p_onde_id IN NUMBER
, p_stgr_id IN NUMBER
, p_stof_id IN NUMBER  -- -2, -1, stof_id
, p_kolom IN NUMBER -- 1..7
)
RETURN VARCHAR2;

TYPE kgcuits51metab_tpmt_record IS RECORD
( onde_id  NUMBER
, mons_id  NUMBER
, sorteer  NUMBER
, kolom1 VARCHAR2(1000)
, kolom2 VARCHAR2(1000)
, kolom3 VARCHAR2(1000)
, kolom4 VARCHAR2(1000)
);
TYPE kgcuits51metab_tpmt_tabtype IS TABLE OF kgcuits51metab_tpmt_record
INDEX BY BINARY_INTEGER;
kgcuits51metab_tpmt_tab kgcuits51metab_tpmt_tabtype;

-- vul een pl/sql-table voor de view voor kgcuits51metab_tpmt (uitslagbrief tpmt Maastricht)
PROCEDURE vul_uitslagbrief_metab_tpmt
( p_onde_id IN NUMBER
, p_reset IN BOOLEAN := FALSE -- begin met een schone tabel.
);

FUNCTION vul_uitslagbrief_metab_tpmt
( p_onde_id IN NUMBER
)
RETURN NUMBER;

-- lees een pl/sql-table voor de view voor kgcuits51metab_tpmt (uitslagbrief tpmt Maastricht)
FUNCTION uitslagbrief_metab_tpmt
( p_mons_id IN NUMBER
, p_kolom IN NUMBER
)
RETURN VARCHAR2;

-- Mantis 201, 20080910 JKO: alle meldingen in een PL/SQL-tabel teruggeven
TYPE fotc_record IS RECORD
( error_tekst VARCHAR2(2000)
, severity VARCHAR2(1)
);
TYPE fotc_tabtype IS TABLE OF fotc_record
INDEX BY BINARY_INTEGER;

PROCEDURE Foute_tekst_combinatie
( p_type        	IN  VARCHAR2 -- 'UITS' of 'METI'(METI wordt nog niet gebruikt).
, p_onde_id     	IN  NUMBER
, p_uits        	IN  VARCHAR2 := NULL
, p_toel        	IN  VARCHAR2 := NULL
, p_meti        	IN  VARCHAR2 := NULL
, x_fotc_tab	    OUT fotc_tabtype
);

PROCEDURE Wijzig_foetus_geslacht
( p_toelichting IN  VARCHAR2
, p_tekst       IN  VARCHAR2
, p_onde_id     IN  NUMBER
);

-- Mantis 1945
-- bepalen of een brief mag worden aangemaakt
-- return boolean:
-- true:  originele brief toegestaan
-- false: geen brief toegestaan
FUNCTION aanmaken_brief_toegestaan
( p_uits_id          IN  NUMBER
, x_reden_geen_brief OUT VARCHAR2
)
RETURN BOOLEAN;

-- bepalen of een brief mag worden aangemaakt
-- return varchar2:
-- 'J' originele brief toegestaan
-- 'N' geen brief toegestaan
FUNCTION aanmaken_brief_toegestaan
( p_uits_id          IN  NUMBER
)
RETURN VARCHAR2;

-- Mantis 1945
-- aanpassen tussentijdse autorisatie
PROCEDURE verwijderen_tussen_autorisatie
( p_uits_id          IN  NUMBER
, p_module           IN  VARCHAR2
);
END KGC_UITS_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_UITS_00" IS
/******************************************************************************
      NAME:       kgc_uits_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.11        01-03-2016   SKA               Mantis 9355 TO_CHAR added to  Conclusie column release 8.11.0.2
    ****************************************************************************** */
FUNCTION referentie
( p_afdeling IN VARCHAR2
, p_onde_id IN NUMBER
, p_onderzoeknr IN VARCHAR2
)
-- let op: p_afdeling en p_onderzoeknr zijn niet meer in gebruik!!!!
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR onde_cur
  IS
    SELECT onderzoeknr
    ,      kafd_id
    ,      ongr_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
  onde_rec onde_cur%rowtype;

  CURSOR auto_cur
  IS
    SELECT mede.code
    FROM   kgc_medewerkers mede
    ,      kgc_uitslagen uits
    ,      kgc_onderzoeken onde
    WHERE  uits.onde_id = onde.onde_id
    AND    mede.mede_id = NVL( onde.mede_id_autorisator, uits.mede_id4 )
    AND    onde.onde_id = p_onde_id
    ;
  -- toevoegingen
  v_familienummer VARCHAR2(20);
  v_autorisator VARCHAR2(10);
-- PL/SQL Block
BEGIN
  OPEN  onde_cur;
  FETCH onde_cur
  INTO  onde_rec;
  CLOSE onde_cur;

  BEGIN
    v_return := kgc_sypa_00.standaard_waarde
                ( p_parameter_code => 'UITSLAGBRIEF_REFERENTIE'
                , p_kafd_id => onde_rec.kafd_id
                , p_ongr_id => onde_rec.ongr_id
                );
  EXCEPTION
    WHEN OTHERS
    THEN
      v_return := NULL;
  END;
  IF ( v_return IS NULL )
  THEN
    -- standaard referentie is onderzoeknummer
    v_return := '<ONDERZOEK>';
  END IF;
  v_return := REPLACE( v_return, '<onderzoek>', '<ONDERZOEK>' );
  v_return := REPLACE( v_return, '<ONDERZOEK>', onde_rec.onderzoeknr );

  v_return := REPLACE( v_return, '<familie>', '<FAMILIE>' );
  IF ( INSTR( v_return, '<FAMILIE>' ) > 0 )
  THEN
    v_familienummer := kgc_fami_00.familie_bij_onderzoek
                       ( p_onde_id => p_onde_id
                       , p_incl_naam => 'N'
                       );
    v_return := REPLACE ( v_return, '<FAMILIE>', v_familienummer );
  END IF;

  v_return := REPLACE( v_return, '<autorisator>', '<AUTORISATOR>' );
  IF ( INSTR( v_return, '<AUTORISATOR>' ) > 0 )
  THEN
    OPEN  auto_cur;
    FETCH auto_cur
    INTO  v_autorisator;
    CLOSE auto_cur;
    v_return := REPLACE( v_return, '<AUTORISATOR>', v_autorisator );
  END IF;
  -- verwijder bekende rommel voor- en achteraan...
  v_return := LTRIM( RTRIM( v_return ) );
  v_return := LTRIM( RTRIM( v_return, '/' ), '/' );
  v_return := LTRIM( RTRIM( v_return ) );
  RETURN( v_return );
END referentie;

FUNCTION context_info
( p_meti_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  CURSOR meti_cur
  IS
    SELECT mons.monsternummer
    ,      mons.leeftijd
    ,      frac.fractienummer
    ,      meti.datum_aanmelding
    ,      stgr.code stgr_code
    ,      stgr.omschrijving stgr_omschrijving
    FROM   kgc_stoftestgroepen stgr
    ,      kgc_monsters mons
    ,      bas_fracties frac
    ,      bas_metingen meti
    WHERE  meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    meti.stgr_id = stgr.stgr_id (+)
    AND    meti.meti_id = p_meti_id
    ;
  meti_rec meti_cur%rowtype;
BEGIN
  IF ( p_meti_id IS NOT NULL )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  meti_rec;
    CLOSE meti_cur;
    v_return := 'Datum '||TO_CHAR( meti_rec.datum_aanmelding, 'dd-mm-yyyy' )
             || ', Monster '||meti_rec.monsternummer
             || ', Fractie '||meti_rec.fractienummer
              ;
    IF ( meti_rec.stgr_omschrijving IS NOT NULL )
    THEN
      v_return := v_return||' ('||meti_rec.stgr_code||')';
    END IF;
    IF ( meti_rec.leeftijd <= 0 )
    THEN
      v_return := v_return||', '||kgc_leef_00.omschrijving( p_dagen => meti_rec.leeftijd );
    END IF;
  END IF;
  RETURN( v_return );
END context_info;

PROCEDURE standaard_uitslag_brieftype
( p_kafd_id IN NUMBER := NULL
, p_onde_id IN NUMBER := NULL
, x_brty_id OUT NUMBER
, x_brty_code OUT VARCHAR2
, x_brty_omschrijving OUT VARCHAR2
)
IS
  CURSOR onde_cur
  IS
    SELECT onde.kafd_id
    ,      onde.ongr_id
    FROM   kgc_onderzoeken onde
    WHERE  onde.onde_id = p_onde_id
    ;
  onde_rec onde_cur%rowtype;

  CURSOR brty_cur
  ( b_code IN VARCHAR2
  )
  IS
    SELECT brty.brty_id
    ,      brty.omschrijving
    FROM   kgc_brieftypes brty
    WHERE  brty.code = b_code
    ;
BEGIN
  IF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  onde_rec;
    CLOSE onde_cur;
  ELSE
    onde_rec.kafd_id := p_kafd_id;
  END IF;
  IF ( onde_rec.kafd_id IS NULL )
  THEN
    onde_rec.kafd_id := kgc_mede_00.afdeling_id;
    onde_rec.ongr_id := kgc_mede_00.onderzoeksgroep_id( p_kafd_id => onde_rec.kafd_id );
  END IF;

  x_brty_code := kgc_sypa_00.standaard_waarde
                 ( p_parameter_code => 'STANDAARD_UITSLAGBRIEFTYPE'
                 , p_kafd_id => onde_rec.kafd_id
                 , p_ongr_id => onde_rec.ongr_id
                 );
  OPEN  brty_cur( b_code => x_brty_code );
  FETCH brty_cur
  INTO  x_brty_id
  ,     x_brty_omschrijving;
  CLOSE brty_cur;
END standaard_uitslag_brieftype;

FUNCTION standaard_uitslag_brieftype
( p_onde_id IN NUMBER
)
RETURN NUMBER
IS
  v_brty_id NUMBER;
  v_brty_code VARCHAR2(10);
  v_brty_omschrijving VARCHAR2(100);
BEGIN
  standaard_uitslag_brieftype
  ( p_onde_id => p_onde_id
  , x_brty_id => v_brty_id
  , x_brty_code => v_brty_code
  , x_brty_omschrijving => v_brty_omschrijving
  );
  RETURN( v_brty_id );
END standaard_uitslag_brieftype;

PROCEDURE vul_uitslagbrief_controle
( p_onde_id IN NUMBER
, p_reset IN BOOLEAN := FALSE -- begin met een schone tabel.
)
IS
  CURSOR onde_cur
  IS
    SELECT onde_id
  ,      kafd_id
  ,      ongr_id
  ,      pers_id
  FROM   kgc_onderzoeken
  WHERE  onde_id = p_onde_id
  ;
  onde_rec onde_cur%rowtype;

  CURSOR mons_cur
  -- Mantis 610: NVL's op datum_afname toegevoegd om chronologische sortering te bereiken.
  IS
    SELECT 1 sorteer
    ,      NVL( mons.datum_afname, TRUNC( mons.creation_date ) ) sorteer_datum
    ,      onmo.onmo_id
    ,      mons.monsternummer
    ,      TO_CHAR( mons.datum_afname, 'dd-mm-yyyy' ) datum
    FROM   kgc_monsters mons
    ,      kgc_onderzoek_monsters onmo
    WHERE  onmo.mons_id = mons.mons_id
    AND    onmo.onde_id = onde_rec.onde_id
    UNION
    SELECT 2
    ,      NVL( mons.datum_afname, TRUNC( mons.creation_date ) ) sorteer_datum
    ,      onmo.onmo_id
    ,      mons.monsternummer
    ,      TO_CHAR( mons.datum_afname, 'dd-mm-yyyy' ) datum
    FROM   kgc_monsters mons
    ,      kgc_onderzoek_monsters onmo
    WHERE  onmo.mons_id = mons.mons_id
    AND    mons.pers_id = onde_rec.pers_id
    AND    mons.ongr_id = onde_rec.ongr_id
    AND    onmo.onde_id <> onde_rec.onde_id
    AND    onmo.mons_id <
           ( SELECT MIN(onmo.mons_id)
             FROM   kgc_onderzoek_monsters onmo
             WHERE  onmo.onde_id = onde_rec.onde_id
           )
    ORDER BY 1 ASC
    ,        2 DESC
    ,        4 DESC
    ;
  CURSOR meet_cur
  ( b_onmo_id IN NUMBER
  )
  IS
    SELECT meet.meet_id
    ,      meti.stgr_id
    ,      meet.stof_id
    ,      stof.omschrijving stoftest
    ,      meet.meetwaarde
    ,      meet.volgorde
    FROM   kgc_stoftesten stof
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.stof_id = stof.stof_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.onmo_id = b_onmo_id
--  and    meti.afgerond <> 'N'
--  and    meet.afgerond <> 'N'
    order by meet.volgorde asc
    ,        meet.meet_id desc -- laatste eerst
  ;
  i2 BINARY_INTEGER; -- eerste kopregel
  i1 BINARY_INTEGER; -- tweede kopregel
  j BINARY_INTEGER;  -- stoftestregels

  FUNCTION tab_index
  ( b_onde_id IN NUMBER
  , b_stgr_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  RETURN BINARY_INTEGER
  IS
    l BINARY_INTEGER := i1;
  BEGIN
    LOOP
      IF ( NOT kgcuitc51_tab.exists(l) )
      THEN
        EXIT;
      END IF;
      IF ( kgcuitc51_tab(l).onde_id = b_onde_id
       AND kgcuitc51_tab(l).stgr_id = b_stgr_id
       AND kgcuitc51_tab(l).stof_id = b_stof_id
         )
      THEN
        EXIT;
      END IF;
      l := l + 1;
    END LOOP;
    RETURN( l );
  END tab_index;
BEGIN
  OPEN  onde_cur;
  FETCH onde_cur
  INTO  onde_rec;
  CLOSE onde_cur;
  -- Mantis 610: IF-statement laten vervallen zodat initialisatie per rapportuitdraai nu wel werkt.
  --IF ( onde_rec.onde_id IS NULL )
  --THEN
  --  RETURN;
  --END IF;
  IF ( p_reset )
  THEN
    kgcuitc51_tab.delete;
    i2 := 1;
    i1 := 2;
  ELSE
    i2 := NVL( tab_index( onde_rec.onde_id, -2, -2 )
             , NVL( kgcuitc51_tab.last, 0 ) + 1
             );
    i1 := NVL( tab_index( onde_rec.onde_id, -1, -1 ), i2+1 );
  END IF;
  IF ( kgcuitc51_tab.exists(i2) )
  THEN
    IF ( kgcuitc51_tab(i2).onde_id = onde_rec.onde_id )
    THEN
      RETURN; -- onderzoek is al gedaan
    END IF;
  END IF;
  kgcuitc51_tab(i2).onde_id  := onde_rec.onde_id;
  kgcuitc51_tab(i2).stgr_id  := -2;  -- eerste kopregel
  kgcuitc51_tab(i2).stof_id  := -2;  -- eerste kopregel
  kgcuitc51_tab(i2).kolom1 := 'Monsternr';
  kgcuitc51_tab(i1).onde_id  := onde_rec.onde_id;
  kgcuitc51_tab(i1).stgr_id  := -1; -- tweede kopregel
  kgcuitc51_tab(i1).stof_id  := -1; -- tweede kopregel
  kgcuitc51_tab(i1).kolom1 := 'Datum afname';
  FOR mons_rec IN mons_cur
  LOOP
    IF ( mons_cur%rowcount = 1 )
    THEN
      kgcuitc51_tab(i2).kolom2 := mons_rec.monsternummer;
      kgcuitc51_tab(i1).kolom2 := mons_rec.datum;
      FOR meet_rec IN meet_cur( mons_rec.onmo_id )
      LOOP
        j := tab_index( onde_rec.onde_id, meet_rec.stgr_id, meet_rec.stof_id );
        kgcuitc51_tab(j).onde_id := onde_rec.onde_id;
        kgcuitc51_tab(j).stgr_id := meet_rec.stgr_id;
        kgcuitc51_tab(j).stof_id := meet_rec.stof_id;
        kgcuitc51_tab(j).kolom1 := meet_rec.stoftest;
        kgcuitc51_tab(j).kolom2 := meet_rec.meetwaarde;
        kgcuitc51_tab(j).kolom7 := bas_meet_00.normaalwaarden( p_meet_id => meet_rec.meet_id
                                                             , p_met_eenheid => 'J'
                                                             );
      END LOOP;
    ELSIF ( mons_cur%rowcount = 2 )
    THEN
      kgcuitc51_tab(i2).kolom3 := mons_rec.monsternummer;
      kgcuitc51_tab(i1).kolom3 := mons_rec.datum;
      FOR meet_rec IN meet_cur( mons_rec.onmo_id )
      LOOP
        j := tab_index( onde_rec.onde_id, meet_rec.stgr_id, meet_rec.stof_id );
        kgcuitc51_tab(j).kolom3 := meet_rec.meetwaarde;
      END LOOP;
    ELSIF ( mons_cur%rowcount = 3 )
    THEN
      kgcuitc51_tab(i2).kolom4 := mons_rec.monsternummer;
      kgcuitc51_tab(i1).kolom4 := mons_rec.datum;
      FOR meet_rec IN meet_cur( mons_rec.onmo_id )
      LOOP
        j := tab_index( onde_rec.onde_id, meet_rec.stgr_id, meet_rec.stof_id );
        kgcuitc51_tab(j).kolom4 := meet_rec.meetwaarde;
      END LOOP;
    ELSIF ( mons_cur%rowcount = 4 )
    THEN
      kgcuitc51_tab(i2).kolom5 := mons_rec.monsternummer;
      kgcuitc51_tab(i1).kolom5 := mons_rec.datum;
      FOR meet_rec IN meet_cur( mons_rec.onmo_id )
      LOOP
        j := tab_index( onde_rec.onde_id, meet_rec.stgr_id, meet_rec.stof_id );
        kgcuitc51_tab(j).kolom5:= meet_rec.meetwaarde;
      END LOOP;
    ELSIF ( mons_cur%rowcount = 5 )
    THEN
      kgcuitc51_tab(i2).kolom6 := mons_rec.monsternummer;
      kgcuitc51_tab(i1).kolom6 := mons_rec.datum;
      FOR meet_rec IN meet_cur( mons_rec.onmo_id )
      LOOP
        j := tab_index( onde_rec.onde_id, meet_rec.stgr_id, meet_rec.stof_id );
        kgcuitc51_tab(j).kolom6 := meet_rec.meetwaarde;
     END LOOP;
    END IF;
  END LOOP;
  kgcuitc51_tab(i2).kolom7 := 'Normaalwaarden';
  kgcuitc51_tab(i1).kolom7 := NULL;
END vul_uitslagbrief_controle;
FUNCTION vul_uitslagbrief_controle
( p_onde_id IN NUMBER
)
RETURN NUMBER
IS
BEGIN
  vul_uitslagbrief_controle
  ( p_onde_id => p_onde_id
  , p_reset => FALSE
  );
  RETURN( kgcuitc51_tab.count );
END vul_uitslagbrief_controle;

-- lees een pl/sql-table uit voor de view voor kgcuitc51 (uitslagbrief controle)
FUNCTION uitslagbrief_controle
( p_onde_id IN NUMBER
, p_stgr_id IN NUMBER
, p_stof_id IN NUMBER
, p_kolom IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  i BINARY_INTEGER;

  FUNCTION tab_index
  ( b_onde_id IN NUMBER
  , b_stgr_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  RETURN BINARY_INTEGER
  IS
    l BINARY_INTEGER := NVL( kgcuitc51_tab.first, 0 );
  BEGIN
    LOOP
      EXIT WHEN NOT kgcuitc51_tab.exists(l);
      IF ( kgcuitc51_tab(l).onde_id = b_onde_id
       AND kgcuitc51_tab(l).stgr_id = b_stgr_id
       AND kgcuitc51_tab(l).stof_id = b_stof_id
         )
      THEN
        EXIT;
      END IF;
      l := l + 1;
    END LOOP;
    RETURN( l );
  END tab_index;
BEGIN
  i := tab_index( p_onde_id, p_stgr_id, p_stof_id );
  IF ( kgcuitc51_tab.exists(i) )
  THEN
    IF ( p_kolom = 1 )
    THEN
      v_return := kgcuitc51_tab(i).kolom1;
    ELSIF ( p_kolom = 2 )
    THEN
      v_return := kgcuitc51_tab(i).kolom2;
    ELSIF ( p_kolom = 3 )
    THEN
      v_return := kgcuitc51_tab(i).kolom3;
    ELSIF ( p_kolom = 4 )
    THEN
      v_return := kgcuitc51_tab(i).kolom4;
    ELSIF ( p_kolom = 5 )
    THEN
      v_return := kgcuitc51_tab(i).kolom5;
    ELSIF ( p_kolom = 6 )
    THEN
      v_return := kgcuitc51_tab(i).kolom6;
    ELSIF ( p_kolom = 7 )
    THEN
      v_return := kgcuitc51_tab(i).kolom7;
    END IF;
  END IF;
  RETURN( v_return );
END uitslagbrief_controle;


-- vul een pl/sql-table voor de view voor kgcuits51metab_tpmt (uitslagbrief tpmt Maastricht)
PROCEDURE vul_uitslagbrief_metab_tpmt
( p_onde_id IN NUMBER
, p_reset IN BOOLEAN := FALSE -- begin met een schone tabel.
)
IS
  CURSOR onde_cur ( b_onde_id IN NUMBER )
  IS
    SELECT onde_id
    ,      kafd_id
    ,      ongr_id
    ,      pers_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = b_onde_id
  ;
  onde_rec onde_cur%rowtype;

  CURSOR stof_cur ( b_kafd_id  IN NUMBER )
  IS
    SELECT stof.omschrijving || chr(10) ||DECODE(prst.eenheid,NULL,stof.EENHEID,prst.eenheid)  stof_header
    ,      prst.stof_id
    FROM   kgc_protocol_stoftesten prst
    ,      kgc_stoftestgroepen stgr
    ,      kgc_stoftesten stof
    WHERE  prst.stgr_id = stgr.stgr_id
    AND    stof.stof_id = prst.stof_id
    AND    stof.meet_reken = 'R'
    AND    stgr.code = 'ETPMT'
    AND    stgr.vervallen = 'N'
    AND    stgr.kafd_id = b_kafd_id
    ORDER BY prst.volgorde
  ;
  stof_rec stof_cur%rowtype;

  CURSOR meet_cur( b_onmo_id IN NUMBER
                 , b_stof_id in NUMBER)
  IS
    SELECT meet.tonen_als
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    ,      bas_meetwaarde_statussen mest
    WHERE  meet.meti_id             = meti.meti_id
    AND    mest.mest_id (+)         = meet.mest_id
    AND    meti.onmo_id             = b_onmo_id
    AND    meet.stof_id             = b_stof_id
    AND    NVL(mest.in_uitslag,'J') = 'J'
    AND    meet.afgerond            = 'J'
    AND    meet.meet_id             = (SELECT MAX(meet.meet_id)
                                       FROM   bas_metingen meti
                                       ,      bas_meetwaarden meet
                                       ,      bas_meetwaarde_statussen mest
                                       WHERE  meet.meti_id             = meti.meti_id
                                       AND    mest.mest_id (+)         = meet.mest_id
                                       AND    meti.onmo_id             = b_onmo_id
                                       AND    meet.stof_id             = b_stof_id
                                       AND    NVL(mest.in_uitslag,'J') = 'J'
                                       AND    meet.afgerond            = 'J'
                                      )
  ;
  meet_rec meet_cur%rowtype;

  CURSOR mons_cur ( b_onde_id IN NUMBER )
  IS
    SELECT onmo.onmo_id
    ,      onmo.onde_id
    ,      onmo.mons_id
    ,      pers.aanspreken        pers_aanspreken
    ,      mons.monsternummer
    FROM   kgc_monsters           mons
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_onderzoeken        onde
    ,      kgc_personen           pers
    WHERE  mons.mons_id = onmo.mons_id
    AND    pers.pers_id = onde.pers_id
    AND    onmo.onde_id = onde.onde_id
    AND    onde.onde_id = b_onde_id
  ;

  v_stof_id1  NUMBER(10);
  v_stof_id2  NUMBER(10);
  v_aantal_stof NUMBER(1);
BEGIN
  IF ( p_reset )
  THEN
    kgcuits51metab_tpmt_tab.delete;
  END IF;

  OPEN  onde_cur ( b_onde_id => p_onde_id );
  FETCH onde_cur
  INTO  onde_rec;
  CLOSE onde_cur;
  IF ( onde_rec.onde_id IS NULL )
  THEN
    RETURN;
  END IF;

  IF ( kgcuits51metab_tpmt_tab.exists(-1) )
  THEN
    IF ( kgcuits51metab_tpmt_tab(-1).onde_id = onde_rec.onde_id )
    THEN
      RETURN; -- onderzoek is al gedaan
    END IF;
  END IF;

  -- kopregel tabel
  kgcuits51metab_tpmt_tab(-1).onde_id   := onde_rec.onde_id;
  kgcuits51metab_tpmt_tab(-1).mons_id   := -1;
  kgcuits51metab_tpmt_tab(-1).kolom1    := 'Naam';
  kgcuits51metab_tpmt_tab(-1).kolom2    := 'Lab nr.';


  SELECT count('1')
    INTO   v_aantal_stof
    FROM   kgc_protocol_stoftesten prst
    ,      kgc_stoftestgroepen stgr
    ,      kgc_stoftesten stof
    WHERE  prst.stgr_id = stgr.stgr_id
    AND    stof.stof_id = prst.stof_id
    AND    stof.meet_reken = 'R'
    AND    stgr.code = 'ETPMT'
    AND    stgr.vervallen = 'N'
    AND    stgr.kafd_id = onde_rec.kafd_id
  ;

  OPEN stof_cur(b_kafd_id => onde_rec.kafd_id);
  FETCH stof_cur
  INTO stof_rec;
  kgcuits51metab_tpmt_tab(-1).kolom3 := stof_rec.stof_header;
  v_stof_id1 := stof_rec.stof_id;


  IF v_aantal_stof > 1
  THEN
    FETCH stof_cur
    INTO stof_rec;
    kgcuits51metab_tpmt_tab(-1).kolom4 := stof_rec.stof_header;
    v_stof_id2 := stof_rec.stof_id;
  END IF;

  CLOSE stof_cur;


  FOR mons_rec IN mons_cur ( b_onde_id => p_onde_id )
  LOOP
    kgcuits51metab_tpmt_tab(mons_rec.mons_id).onde_id := onde_rec.onde_id;
    kgcuits51metab_tpmt_tab(mons_rec.mons_id).mons_id := mons_rec.mons_id;
    kgcuits51metab_tpmt_tab(mons_rec.mons_id).kolom1  := mons_rec.pers_aanspreken;
    kgcuits51metab_tpmt_tab(mons_rec.mons_id).kolom2  := mons_rec.monsternummer;

    meet_rec := NULL;
    OPEN meet_cur( b_onmo_id => mons_rec.onmo_id
                 , b_stof_id => v_stof_id1
                 );
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE meet_cur;
    kgcuits51metab_tpmt_tab(mons_rec.mons_id).kolom3 := meet_rec.tonen_als;


    meet_rec := NULL;
    OPEN meet_cur( b_onmo_id => mons_rec.onmo_id
                 , b_stof_id => v_stof_id2
                 );
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE meet_cur;
    kgcuits51metab_tpmt_tab(mons_rec.mons_id).kolom4 := meet_rec.tonen_als;
  END LOOP;

  -- laatste regels tabel
  kgcuits51metab_tpmt_tab(-2).kolom1 := 'Controles (n=100)';
  kgcuits51metab_tpmt_tab(-3).kolom1 := '(mean � SD)';
  kgcuits51metab_tpmt_tab(-4).kolom1 := '(range)';


  IF v_stof_id1 IS NOT NULL
  THEN
    kgcuits51metab_tpmt_tab(-3).kolom3 := kgc_attr_00.WAARDE ('KGC_STOFTESTEN', 'MEAN_SD', v_stof_id1);
    kgcuits51metab_tpmt_tab(-4).kolom3 := kgc_attr_00.WAARDE ('KGC_STOFTESTEN', 'RANGE', v_stof_id1);
  END IF;


  IF v_stof_id2 IS NOT NULL
  THEN
    kgcuits51metab_tpmt_tab(-3).kolom4 := kgc_attr_00.WAARDE ('KGC_STOFTESTEN', 'MEAN_SD', v_stof_id2);
    kgcuits51metab_tpmt_tab(-4).kolom4 := kgc_attr_00.WAARDE ('KGC_STOFTESTEN', 'RANGE', v_stof_id2);
  END IF;
END vul_uitslagbrief_metab_tpmt;

FUNCTION vul_uitslagbrief_metab_tpmt
( p_onde_id IN NUMBER
)
RETURN NUMBER
IS
BEGIN
  vul_uitslagbrief_metab_tpmt
  ( p_onde_id => p_onde_id
  , p_reset => FALSE
  );
  RETURN( kgcuits51metab_tpmt_tab.count );
END vul_uitslagbrief_metab_tpmt;


-- lees een pl/sql-table uit voor de view voor kgcuits51metab_TPMT (uitslagbrief tpmt Maastricht)
FUNCTION uitslagbrief_metab_tpmt
( p_mons_id IN NUMBER
, p_kolom IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
BEGIN
  IF ( kgcuits51metab_tpmt_tab.exists(p_mons_id) )
  THEN
    IF ( p_kolom = 1 )
    THEN
      v_return := kgcuits51metab_tpmt_tab(p_mons_id).kolom1;
    ELSIF ( p_kolom = 2 )
    THEN
      v_return := kgcuits51metab_tpmt_tab(p_mons_id).kolom2;
    ELSIF ( p_kolom = 3 )
    THEN
      v_return := kgcuits51metab_tpmt_tab(p_mons_id).kolom3;
    ELSIF ( p_kolom = 4 )
    THEN
      v_return := kgcuits51metab_tpmt_tab(p_mons_id).kolom4;
    END IF;
  END IF;
  RETURN( v_return );
END uitslagbrief_metab_tpmt;

PROCEDURE Foute_tekst_combinatie
( p_type        IN  VARCHAR2 -- 'UITS' of 'METI'(METI wordt nog niet gebruikt).
, p_onde_id     IN  NUMBER
, p_uits        IN  VARCHAR2 := NULL
, p_toel        IN  VARCHAR2 := NULL
, p_meti        IN  VARCHAR2 := NULL
, x_fotc_tab    OUT fotc_tabtype -- Mantis 201, 20080910 JKO: alle meldingen in een PL/SQL-tabel teruggeven
)
IS
  CURSOR onde_cur (b_onde_id IN NUMBER)
  IS
    SELECT onde.kafd_id
    ,      onde.ongr_id
    FROM  kgc_onderzoeken onde
    WHERE onde.onde_id = b_onde_id
    ;
  onde_rec onde_cur%ROWTYPE;

  CURSOR meti_cur ( b_onde_id IN NUMBER)
  IS
    SELECT to_char(meti.conclusie) conclusie -- MANTIS 9355 TO_CHAR added SKA 01-03-2016 alias also added as conclusie release 8.11.0.2
    FROM bas_metingen meti
    WHERE   meti.onde_id = b_onde_id
    ;
  meti_rec meti_cur%ROWTYPE;

  CURSOR fotc_cur (b_kafd_id IN NUMBER
                  ,b_ongr_id IN NUMBER
                  )
  IS
    SELECT meti
    ,      uits
    ,      toel
    ,      strengheid
    ,      melding
    FROM kgc_foute_tekst_combinaties fotc
    WHERE fotc.kafd_id = b_kafd_id
    AND   NVL (fotc.ongr_id, b_ongr_id ) = b_ongr_id
    ORDER BY nvl( fotc.ongr_id, 9999999999 )  -- zo komt lege groep achteraan
           , DECODE ( strengheid, 'E', '1', 'W', '2' , 'I', '3' )
           -- groepen zelfde zoekcombinaties bij elkaar en daarbinnen op waarde van de zoekargumenten sorteren
           , DECODE ( DECODE ( meti, NULL, NULL, 'M' ) || DECODE ( uits, NULL, NULL, 'U' ) || DECODE ( toel, NULL, NULL, 'T' )
                    ,'MUT', '1' || meti || uits || toel
                    ,'UT' , '2' || uits || toel
                    ,'MU' , '3' || meti || uits
                    ,'MT' , '4' || meti || toel
                    )
    ;
  v_fotc_tab      fotc_tabtype;
  i_fotc          BINARY_INTEGER;
  v_indicaties    VARCHAR2(3); -- per zoek-argument een positie: 1=meti, 2=uits, 3=toel
  v_fout_gevonden BOOLEAN;
BEGIN
  v_fotc_tab.delete;
  i_fotc := 0;

  OPEN onde_cur(b_onde_id => p_onde_id);
  FETCH onde_cur
    INTO onde_rec;
  CLOSE onde_cur;

  IF p_type = 'UITS'
  THEN
    FOR fotc_rec IN fotc_cur(b_kafd_id => onde_rec.kafd_id
                            ,b_ongr_id => onde_rec.ongr_id)
    LOOP
      -- indicatoren per zoek-argument vullen (M, U of T); '-' betekent: zoek-argument niet aanwezig
      v_indicaties    := NULL;
      IF fotc_rec.meti IS NOT NULL
      THEN
        v_indicaties := 'M';
      ELSE
        v_indicaties := '-';
      END IF;

      IF fotc_rec.uits IS NOT NULL
      THEN
        v_indicaties := v_indicaties || 'U';
      ELSE
        v_indicaties := v_indicaties || '-';
      END IF;

      IF fotc_rec.toel IS NOT NULL
      THEN
        v_indicaties := v_indicaties || 'T';
      ELSE
        v_indicaties := v_indicaties || '-';
      END IF;

      -- vaststellen of METI-zoekwaarde is gevonden; dan indicatie op 'G' zetten
      IF substr(v_indicaties, 1, 1) = 'M'
      THEN
        OPEN meti_cur(b_onde_id => p_onde_id);
        FETCH meti_cur
          INTO meti_rec;
        WHILE meti_cur%FOUND
        LOOP
          IF upper(meti_rec.conclusie) LIKE
             upper('%' || fotc_rec.meti || '%')
          THEN
            v_indicaties := 'G' || substr(v_indicaties, 2, 2);
            EXIT;
          END IF;
          FETCH meti_cur
            INTO meti_rec;
        END LOOP;
        CLOSE meti_cur;
      END IF;

      -- vaststellen of UITS-zoekwaarde is gevonden; dan indicatie op 'G' zetten
      IF substr(v_indicaties, 2, 1) = 'U'
      THEN
        IF upper(p_uits) LIKE upper('%' || fotc_rec.uits || '%')
        THEN
          v_indicaties := substr(v_indicaties, 1, 1) || 'G' || substr(v_indicaties, 3, 1);
        END IF;
      END IF;

      -- vaststellen of TOEL-zoekwaarde is gevonden; dan indicatie op 'G' zetten
      IF substr(v_indicaties, 3, 1) = 'T'
      THEN
        IF upper(p_toel) LIKE upper('%' || fotc_rec.toel || '%')
        THEN
          v_indicaties := substr(v_indicaties, 1, 2) || 'G';
        END IF;
      END IF;

      -- kijken of de combinatie van zoek-waarden voorkwam (er geldt hierbij een AND-relatie)
      v_fout_gevonden := FALSE;
      IF instr(v_indicaties, '-', 1, 1) = 0
      THEN
        -- alle drie de zoek-argumenten waren aanwezig
        IF v_indicaties = 'GGG'
        THEN
          -- alleen als alle drie de zoek-waarden zijn gevonden is het een foute tekstcombinatie
          v_fout_gevonden := TRUE;
        END IF;
      ELSE
        -- twee zoek-argumenten waren aanwezig (slechts 1 is niet mogelijk)
        IF REPLACE(v_indicaties, '-', '') = 'GG'
        THEN
          -- alleen als beide zoek-waarden zijn gevonden is het een foute tekstcombinatie
          v_fout_gevonden := TRUE;
        END IF;
      END IF;
      -- indien foute tekstcombinatie, dan melding aan PL/SQL-tabel toevoegen
      IF v_fout_gevonden
      THEN
        i_fotc := i_fotc + 1;
        v_fotc_tab(i_fotc).error_tekst  := fotc_rec.melding;
        v_fotc_tab(i_fotc).severity     := fotc_rec.strengheid;
      END IF;
    END LOOP;
  END IF;

  IF p_type = 'METI'
  THEN
    -- METI wordt nog niet gebruikt
    NULL;
  END IF;

  -- PL/SQL-tabel teruggeven
  x_fotc_tab := v_fotc_tab;

END Foute_tekst_combinatie;

PROCEDURE Wijzig_foetus_geslacht
( p_toelichting IN  VARCHAR2
, p_tekst       IN  VARCHAR2
, p_onde_id     IN  NUMBER
)
IS
  CURSOR onde_cur
  IS
    SELECT kafd_id
         , ongr_id
         , foet_id
      FROM kgc_onderzoeken
     WHERE onde_id = p_onde_id;
  onde_rec onde_cur%rowtype := NULL
  ;
  v_toelichting  kgc_uitslagen.toelichting%TYPE;
  v_tekst        kgc_uitslagen.tekst%TYPE;
  v_sypa_reeks_m kgc_systeem_parameters.standaard_waarde%TYPE := NULL; -- teksten-reeks tbv. geslacht 'Mannelijk'
  v_sypa_tekst_m kgc_systeem_parameters.standaard_waarde%TYPE; -- 1 tekst uit de reeks
  v_sypa_reeks_v kgc_systeem_parameters.standaard_waarde%TYPE := NULL; -- teksten-reeks tbv. geslacht 'Vrouwelijk'
  v_sypa_tekst_v kgc_systeem_parameters.standaard_waarde%TYPE; -- 1 tekst uit de reeks
  v_delimiter    VARCHAR2(1) := ','; -- teken waarmee de teksten van elkaar zijn gescheiden in de SYPA-reeks
  v_geslacht     kgc_foetussen.geslacht%TYPE;
BEGIN
  v_toelichting := UPPER( REPLACE( REPLACE( p_toelichting, CHR(10), NULL ), ' ', NULL ) );
  v_tekst       := UPPER( REPLACE( REPLACE( p_tekst,       CHR(10), NULL ), ' ', NULL ) );
  v_geslacht    := NULL;
  IF ( v_toelichting IS NOT NULL OR
       v_tekst       IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  onde_rec;
    CLOSE onde_cur;
    IF ( onde_rec.foet_id IS NOT NULL )
    THEN
      -- Mannelijk geslacht:
      BEGIN
        v_sypa_reeks_m := kgc_sypa_00.standaard_waarde
                            ( p_parameter_code => 'UITSLAGTEKSTEN_FOETUS_M'
                            , p_kafd_id => onde_rec.kafd_id
                            , p_ongr_id => onde_rec.ongr_id
                            );
      EXCEPTION
        WHEN OTHERS
        THEN
          NULL;
      END;
      -- Nagaan of een SYPA-tekst uit de reeks bij mannelijk geslacht, voorkomt in de uitslag-toelichting of -tekst:
      v_sypa_reeks_m := RTRIM( v_sypa_reeks_m, v_delimiter ) || v_delimiter;
      WHILE ( v_sypa_reeks_m IS NOT NULL AND
              v_geslacht IS NULL )
        LOOP
          v_sypa_tekst_m := SUBSTR( v_sypa_reeks_m, 1, INSTR( v_sypa_reeks_m, v_delimiter ) - 1 );
          IF v_sypa_tekst_m IS NOT NULL
          THEN
            IF ( INSTR( v_toelichting, v_sypa_tekst_m ) > 0 OR
                 INSTR( v_tekst,       v_sypa_tekst_m ) > 0 )
            THEN
              v_geslacht := 'M';
            END IF;
          END IF;
          v_sypa_reeks_m := SUBSTR( v_sypa_reeks_m, INSTR( v_sypa_reeks_m, v_delimiter ) + 1 );
        END LOOP;

      -- Vrouwelijk geslacht:
      BEGIN
        v_sypa_reeks_v := kgc_sypa_00.standaard_waarde
                            ( p_parameter_code => 'UITSLAGTEKSTEN_FOETUS_V'
                            , p_kafd_id => onde_rec.kafd_id
                            , p_ongr_id => onde_rec.ongr_id
                            );
      EXCEPTION
        WHEN OTHERS
        THEN
          NULL;
      END;
      -- Nagaan of een SYPA-tekst uit de reeks bij vrouwelijk geslacht, voorkomt in de uitslag-toelichting of -tekst:
      v_sypa_reeks_v := RTRIM( v_sypa_reeks_v, v_delimiter ) || v_delimiter;
      WHILE ( v_sypa_reeks_v IS NOT NULL AND
              v_geslacht IS NULL )
        LOOP
          v_sypa_tekst_v := SUBSTR( v_sypa_reeks_v, 1, INSTR( v_sypa_reeks_v, v_delimiter ) - 1 );
          IF v_sypa_tekst_v IS NOT NULL
          THEN
            IF ( INSTR( v_toelichting, v_sypa_tekst_v ) > 0 OR
                 INSTR( v_tekst,       v_sypa_tekst_v ) > 0 )
            THEN
              v_geslacht := 'V';
            END IF;
          END IF;
          v_sypa_reeks_v := SUBSTR( v_sypa_reeks_v, INSTR( v_sypa_reeks_v, v_delimiter ) + 1 );
        END LOOP;
      -- Foetus-geslacht aanpassen:
      IF ( v_geslacht IS NOT NULL )
      THEN
        UPDATE kgc_foetussen
           SET geslacht = v_geslacht
         WHERE foet_id  = onde_rec.foet_id
           AND geslacht <> v_geslacht
        ;
      END IF;
    END IF;
  END IF;
END Wijzig_foetus_geslacht;

-- Mantis 1945
-- bepalen of een brief mag worden aangemaakt
-- return boolean:
-- true originele brief toegestaan
-- false geen brief toegestaan
FUNCTION aanmaken_brief_toegestaan
( p_uits_id          IN  NUMBER
, x_reden_geen_brief OUT VARCHAR2
)
RETURN BOOLEAN
IS
  CURSOR c_uits
  IS
  SELECT onde.kafd_id
       , onde.ongr_id
       , onde.onde_id
       , onde.afgerond
       , uits.brty_id
       , uits.mede_id4
       , uits.tekst
       , ramo.code
    FROM kgc_uitslagen       uits
       , kgc_onderzoeken     onde
       , kgc_brieftypes      brty
       , kgc_rapport_modules ramo
   WHERE uits.uits_id = p_uits_id
     AND uits.onde_id = onde.onde_id
     AND uits.brty_id = brty.brty_id
     AND brty.ramo_id = ramo.ramo_id;

	v_tuss_uits_toeg    varchar2(1);
  v_result BOOLEAN;
  v_melding VARCHAR2(100);
BEGIN
  v_result := true;
  v_melding := NULL;

  FOR r_uits in c_uits
  LOOP
    IF r_uits.code = 'UITSLAG'
    THEN -- volgende controles alleen voor brieftype met ramo-code "UITSLAG" uitvoeren

      -- bij een "lege" uitslag => niet printen
      IF r_uits.tekst is null
      THEN
        v_result := false;
        v_melding := 'Er is geen uitslag-tekst ingevuld.';
        EXIT;
   	  END IF;

      -- onderzoek afgerond => aanmaken toegestaan
      IF nvl(r_uits.afgerond,'N') = 'J'
      THEN
        -- klaar uit de loop
        EXIT;
      END IF;

  	 	-- de afdeling doet niet mee met tussentijdse uitslagen => niet printen
   	 	v_tuss_uits_toeg := upper( substr( kgc_sypa_00.standaard_waarde
                                              ( p_parameter_code => 'TUSSENTIJDSE_UITSLAGEN_TOEGEST'
                                              , p_kafd_id => r_uits.kafd_id
                                              , p_ongr_id => r_uits.ongr_id
                                              )
                                          ,1 ,1 )
                                        );
      IF nvl(v_tuss_uits_toeg,'N') <> 'J'
      THEN
        v_result := false;
        v_melding := 'Tussentijdse uitslagen zijn voor deze afdeling niet toegestaan.';
        EXIT;
      END IF;

      -- is er al een brief aangemaakt => niet printen
      IF kgc_brie_00.eerder_geprint(p_brty_id => r_uits.brty_id
                                   ,p_uits_id => p_uits_id) = 'J'
      THEN
        v_result := false;
        v_melding := 'Tussentijdse uitslagen zijn niet toegestaan als de uitslagbrief al is aangemaakt.';
        EXIT;
      END IF;

      -- geen tussentijdse-autorisator => niet printen
      IF r_uits.mede_id4 is null
      THEN
        v_result := false;
        v_melding := 'De tussentijdse uitslag is nog niet geautoriseerd.';
        EXIT;
      END IF;

    ELSE -- geen controles
      NULL;
    END IF;

  END LOOP;

  -- return waarden
  x_reden_geen_brief := v_melding;
  RETURN v_result;
END;

-- Mantis 1945
-- bepalen of een brief mag worden aangemaakt
-- return varchar2:
-- 'J' originele brief toegestaan
-- 'N' geen brief toegestaan
FUNCTION aanmaken_brief_toegestaan(p_uits_id IN NUMBER) RETURN VARCHAR2 IS
  v_melding VARCHAR2(100);
BEGIN
  IF aanmaken_brief_toegestaan(p_uits_id          => p_uits_id
                              ,x_reden_geen_brief => v_melding)
  THEN
    RETURN 'J';
  ELSE
    RETURN 'N';
  END IF;
END;

-- Mantis 1945
-- aanpassen tussentijdse autorisatie
PROCEDURE verwijderen_tussen_autorisatie
( p_uits_id          IN  NUMBER
, p_module           IN  VARCHAR2
)
IS
  CURSOR c_onde
  IS
    SELECT onde.kafd_id
    ,      onde.ongr_id
    ,      onde.taal_id
    FROM   kgc_onderzoeken onde
       ,   kgc_uitslagen uits
    WHERE  onde.onde_id = uits.onde_id
    AND    uits.uits_id = p_uits_id
    ;

    v_kafd_id NUMBER;
    v_ongr_id NUMBER;
    v_module VARCHAR2(1000);
BEGIN
  FOR r_onde IN c_onde
  LOOP
    v_module := nvl(kgc_rapp_00.rapport(p_ramo_code => 'UITSLAG'
                                       ,p_kafd_id => r_onde.kafd_id
                                       ,p_ongr_id => r_onde.ongr_id
                                       ,p_taal_id => r_onde.taal_id)
                   ,'KGCUITS51');
    EXIT;
  END LOOP;
  IF v_module = p_module
  THEN
    UPDATE kgc_uitslagen uits
    SET    uits.mede_id4       = NULL
          ,uits.datum_mede_id4 = NULL
    WHERE  uits.uits_id = p_uits_id
    AND    uits.mede_id4 IS NOT NULL
    AND    NOT EXISTS (SELECT 1
            FROM   kgc_onderzoeken onde
            WHERE  onde.onde_id = uits.onde_id
            AND    onde.afgerond = 'J');
  END IF;
END;
END kgc_uits_00;
/

/
QUIT
