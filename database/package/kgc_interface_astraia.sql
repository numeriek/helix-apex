CREATE OR REPLACE PACKAGE "HELIX"."KGC_INTERFACE_ASTRAIA" IS
 /******************************************************************************
    NAME:      KGC_INTERFACE_ASTRAIA
    PURPOSE:   Specifieke functionaliteit voor de Astraia-koppeling

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    5.0        18-02-2013  SOJ              Mantis 0182, modified "lees_in_astraia "
    4.0        12-08-2009  RDE              Mantis 3071 en 2870
    3.0        12-05-2009  RDE              Mantis 1710: rework.
    2.0        02-03-2009  RDE              Mantis 1710:
                                            Aanpassingen ASTRAIA Product Codes.
    1.0        18-02-2009  NtB              Mantis 740: creatie wegens
                                            ontvlechting Mosos en Astraia.
 *****************************************************************************/

   -- Package specification:

   PROCEDURE lees_in_astraia(p_imlo_id  IN NUMBER
                            ,p_filenaam IN VARCHAR2);

   -- verwerk de binnengekomen bestanden
   PROCEDURE verwerk_bestanden;
END KGC_INTERFACE_ASTRAIA;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_INTERFACE_ASTRAIA" IS
   -- Package body:
   v_hersteljob BOOLEAN := FALSE; -- zie gebuik; file niet opnieuw inlezen!

   PROCEDURE lees_in_astraia(p_imlo_id  IN NUMBER
                            ,p_filenaam IN VARCHAR2) IS
      TYPE imfoetusids IS TABLE OF VARCHAR2(20) INDEX BY BINARY_INTEGER;
      v_tabfoetusexam imfoetusids; -- de foetussen bij het enige Exam
      -- bij al deze foetussen moet evt. een monster/onderzoek toegevoegd worden!
      v_xmlp                 xmlparser.parser;
      v_xmldoc               xmldom.domdocument;
      v_xmln0                xmldom.domnode;
      v_xmln1                xmldom.domnode;
      v_xmln2                xmldom.domnode;
      v_xmlnl                xmldom.domnodelist;
      v_xmlnlrec             xmldom.domnodelist;
      v_xmlnldata            xmldom.domnodelist;
      v_xmlnldatapunctieexam xmldom.domnodelist;
      v_str                  VARCHAR2(2000);
      --    v_strAtt                VARCHAR2(2000);
      v_strnn VARCHAR2(2000);
      --    v_strVal                VARCHAR2(2000);
      --    v_strXML                VARCHAR2(32000);
      v_strxmldirfile         VARCHAR2(2000);
      v_record_id             VARCHAR2(20);
      v_parent_id             VARCHAR2(20);
      v_episode_record_id     VARCHAR2(20); -- de te selecteren episode (zwangerschap)
      v_insurance_record_id   VARCHAR2(20); -- de te selecteren insurance (zwangerschap)
      v_punctieexam_record_id VARCHAR2(20); -- tbv id onderliggende amniocentesis/cvs/fbs
      v_imfi_id               kgc_import_files.imfi_id%TYPE;
      --    v_rela_id               kgc_personen.rela_id%TYPE;
      --    v_verz_id               kgc_personen.verz_id%TYPE;
      v_imfi_opmerkingen     VARCHAR2(32000); -- verslag inlezen; indien inlezen ok, dan wordt dit overschreven door verwerking/fouten
      v_impw_externe_id      kgc_import_param_waarden.externe_id%TYPE;
      v_impw_externe_id_plus kgc_import_param_waarden.externe_id%TYPE;
      v_impw_hint            kgc_import_param_waarden.hint%TYPE;
      -- pers
      v_impe_id                  kgc_im_personen.impe_id%TYPE := NULL;
      v_impw_id_verz             kgc_im_personen.impw_id_verz%TYPE;
      v_impw_id_rela             kgc_im_personen.impw_id_rela%TYPE;
      v_impw_id_verzwijze        kgc_im_personen.impw_id_verzwijze%TYPE;
      v_impw_id_geslacht         kgc_im_personen.impw_id_geslacht%TYPE;
      v_pers_achternaam          kgc_im_personen.achternaam%TYPE;
      v_pers_voorletters         kgc_im_personen.voorletters%TYPE;
      v_pers_voorletters_dummy   kgc_im_personen.voorletters%TYPE;
      v_pers_voorvoegsel         kgc_im_personen.voorvoegsel%TYPE;
      v_pers_achternaam_partner  kgc_im_personen.achternaam%TYPE;
      v_pers_voorvoegsel_partner kgc_im_personen.voorvoegsel%TYPE;
      v_pers_zis_aanspreken      VARCHAR2(2000);
      v_patient_name_partner     VARCHAR2(2000);
      v_partner_name             VARCHAR2(2000);
      v_bsn                      VARCHAR2(2000);
      v_zisnr                    VARCHAR2(2000);
      -- onde
      v_imon_id      kgc_im_onderzoeken.imon_id%TYPE;
      v_impw_id_ongr kgc_im_onderzoeken.impw_id_ongr%TYPE;
      --    v_impw_id_onderzoekstype   kgc_im_onderzoeken.impw_id_onderzoekstype%TYPE;
      v_impw_id_herk             kgc_im_onderzoeken.impw_id_herk%TYPE;
      v_impw_id_inst             kgc_im_onderzoeken.impw_id_inst%TYPE;
      v_onde_onderzoekswijze_ext kgc_import_param_waarden.externe_id%TYPE;
      v_impw_id_onderzoekswijze  kgc_im_onderzoeken.impw_id_onderzoekswijze%TYPE;
      v_impw_id_autorisator      kgc_im_onderzoeken.impw_id_autorisator%TYPE;
      v_imon_declareren          kgc_im_onderzoeken.declareren%TYPE;
      -- zwan
      v_imzw_id_foetus              kgc_im_zwangerschappen.imzw_id%TYPE;
      v_impw_id_zekerheid_datum_lm  kgc_im_zwangerschappen.impw_id_zekerheid_datum_lm%TYPE;
      v_zwan_zekerheid_datum_lm_ext kgc_import_param_waarden.externe_id%TYPE;
      v_impw_id_berekeningswijze    kgc_im_zwangerschappen.impw_id_berekeningswijze%TYPE;
      v_zwan_berekeningswijze_ext   kgc_import_param_waarden.externe_id%TYPE;
      v_zwan_datum_lm               kgc_im_zwangerschappen.datum_lm%TYPE;
      -- foet
      v_foet_geplande_geboortedatum kgc_im_foetussen.geplande_geboortedatum%TYPE;
      -- mons
      --    v_immo_imfo_id        kgc_im_monsters.imfo_id%TYPE;
      v_punctie_mat         VARCHAR2(30);
      v_immo_id             kgc_im_monsters.immo_id%TYPE;
      v_impw_id_mate        kgc_im_monsters.impw_id_mate%TYPE;
      v_impw_id_cond        kgc_im_monsters.impw_id_cond%TYPE;
      v_aantal_buizen       kgc_im_monsters.aantal_buizen%TYPE;
      v_hoeveelheid_monster kgc_im_monsters.hoeveelheid_monster%TYPE;
      -- onderz ind
      v_impw_id_indi           kgc_im_onderzoek_indicaties.impw_id_indi%TYPE;
      v_impw_id_ingr           kgc_im_onderzoek_indicaties.impw_id_ingr%TYPE;
      v_imoi_opmerking         kgc_im_onderzoek_indicaties.opmerking%TYPE;
      v_imoi_indication_type_1 BOOLEAN;
      v_imoi_indication_type_2 BOOLEAN;
      v_imoi_history           BOOLEAN;
      --    v_cursor                 INTEGER;
      --    v_NrOfRows               NUMBER;
      v_count                NUMBER;
      v_pos                  NUMBER;
      v_sql                  VARCHAR2(32000);
      v_astraia_tag          VARCHAR2(200);
      v_eersteadres          BOOLEAN := TRUE;
      v_procedure_name       VARCHAR2(200);    -- rde 090223 mantis 1710
      v_bUG1_verwacht        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bUG2_verwacht        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bultrageluidverwerkt BOOLEAN := FALSE;
      v_bVWP_verwacht        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bVWP_verwerkt        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bVLK_verwacht        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bVLK_verwerkt        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bNSP_verwacht        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bNSP_verwerkt        BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bProcedure_dubbel    BOOLEAN := FALSE; -- rde 090223 mantis 1710
      v_bgelijk              BOOLEAN;
      --    v_ext_id_prefix          VARCHAR2(10);
      --    v_hint_prefix            VARCHAR2(100);
      --    v_hint_postfix           VARCHAR2(100);
      --    v_imoi_id_range          NUMBER;
      --    v_volgorde               NUMBER;
      v_bcdoctoralgeweest BOOLEAN := FALSE;
      v_afname_indicatie  VARCHAR2(100);
      v_land              VARCHAR2(2000);
      v_error             BOOLEAN := FALSE;

      CURSOR c_imlo IS
         SELECT imlo.imlo_id
               ,imlo.directorynaam
           FROM kgc_import_locaties imlo
          WHERE imlo.imlo_id = p_imlo_id;
      r_imlo c_imlo%ROWTYPE;
      CURSOR c_imfi IS
         SELECT imfi.imfi_id
               ,imfi.import_datum
               ,imfi.inhoud
           FROM kgc_import_files imfi
          WHERE imfi.imlo_id = p_imlo_id
            AND imfi.filenaam = p_filenaam;
      r_imfi c_imfi%ROWTYPE;
      CURSOR c_imon_check(p_imfi_id NUMBER, p_imon_id VARCHAR2) IS
         SELECT impe_id
           FROM kgc_im_onderzoeken imon
          WHERE imon.imfi_id = p_imfi_id
            AND imon.imon_id = p_imon_id;
      r_imon_check c_imon_check%ROWTYPE;
      CURSOR c_immo_check(p_imfi_id NUMBER, p_immo_id VARCHAR2) IS
         SELECT impe_id
               ,impw_id_mate
           FROM kgc_im_monsters immo
          WHERE immo.imfi_id = p_imfi_id
            AND immo.immo_id = p_immo_id;
      r_immo_check c_immo_check%ROWTYPE;
      /*  CURSOR c_imfo
        ( p_imfi_id        NUMBER
        , p_impe_id_moeder VARCHAR2
        , p_imzw_id        VARCHAR2
        )
        IS
          SELECT imfo_id
          FROM   kgc_im_foetussen imfo
          WHERE  imfo.imfi_id = p_imfi_id
          AND    imfo.impe_id_moeder = p_impe_id_moeder
          AND    imfo.imzw_id = p_imzw_id
          ;
      */
      CURSOR c_impe(p_imfi_id NUMBER, p_impe_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_personen impe
          WHERE impe.imfi_id = p_imfi_id
            AND impe.impe_id = p_impe_id;
      r_impe c_impe%ROWTYPE;
      FUNCTION getdata -- zoek in 'record' naar 'data' met specifieke naam --
      (p_xmlnldata IN xmldom.domnodelist
      ,p_strname   IN VARCHAR2
      ,p_zoekwijze IN NUMBER := 1 -- 1=waarde van value; 2=inhoud node; 3=eerst 1, indien leeg, dan 2.
       ) RETURN VARCHAR2 IS
         v_xmln1  xmldom.domnode;
         v_xmln2  xmldom.domnode;
         v_waarde VARCHAR2(32767);
      BEGIN
         FOR i IN 0 .. xmldom.getlength(p_xmlnldata) - 1
         LOOP
            v_xmln1 := xmldom.item(p_xmlnldata
                                  ,i);
            IF lower(xmldom.getattribute(xmldom.makeelement(v_xmln1)
                                        ,'name')) = p_strname
            THEN
               IF p_zoekwijze IN (1, 3)
               THEN
                  v_waarde := nvl(xmldom.getattribute(xmldom.makeelement(v_xmln1)
                                                     ,'value')
                                 ,'');
                  IF p_zoekwijze = 1
                     OR nvl(length(v_waarde)
                           ,0) > 0
                  THEN
                     IF v_waarde = '""'
                     THEN
                        RETURN NULL; -- v5 - leeg
                     END IF;
                     RETURN v_waarde;
                  END IF;
               END IF;
               -- dan de inhoud van de node proberen - in Oracle moet dit via subnode:
               v_xmln2 := xmldom.getfirstchild(v_xmln1);
               -- Mantis 3975: cdata werd genegeerd; xmldom.cdata_section_node toegevoegd
               IF xmldom.getnodetype(v_xmln2) in (xmldom.text_node, xmldom.cdata_section_node)
               THEN
                  v_waarde := nvl(xmldom.getnodevalue(v_xmln2)
                                 ,''); -- v5
                  IF v_waarde = '""'
                  THEN
                     RETURN NULL; -- v5 - leeg
                  END IF;
                  RETURN v_waarde;
               END IF;
               RETURN '';
            END IF;
         END LOOP;
         RETURN '';
      END;
      -- splitsName: splitst "Berg, van den" in 2 stukken
      -- helaas is zowel Astraia als ZIS een chaos, dus ook proberen op andere wijzen
      PROCEDURE splitsname(p_strname        IN VARCHAR2
                          ,x_strachternaam  IN OUT VARCHAR2
                          ,x_strvoorvoegsel IN OUT VARCHAR2) IS
         v_doorgaan    BOOLEAN := TRUE;
         v_vvvooraan   BOOLEAN := FALSE; -- of aan de voorkant alles eraf, of aan de achterkant. niet beide
         v_vvachteraan BOOLEAN := FALSE;
         v_pos         NUMBER;
         v_posrev      NUMBER;
         v_strsep      VARCHAR2(2);
         v_strname     VARCHAR2(2000);
         v_strvv       VARCHAR2(2000);
         v_strallevv   VARCHAR2(2000) := ' van het ter de den der in ''t aan v.d. v. d. vd v d bij op ten over uit voor ';
         -- niet alles, maar toch zeker 95%. Spaties zijn tbv zoeken
      BEGIN
         x_strachternaam  := NULL;
         x_strvoorvoegsel := NULL;
         v_pos            := instr(p_strname
                                  ,',');
         IF v_pos > 0
         THEN
            x_strachternaam  := rtrim(substr(p_strname
                                            ,1
                                            ,v_pos - 1));
            x_strvoorvoegsel := ltrim(substr(p_strname
                                            ,v_pos + 1));
         ELSE
            x_strachternaam := p_strname;
            WHILE v_doorgaan
            LOOP
               v_doorgaan      := FALSE;
               x_strachternaam := ltrim(rtrim(x_strachternaam)); -- 2 spaties kan!
               v_strname       := lower(x_strachternaam);
               v_pos           := instr(v_strname
                                       ,' ');
               v_posrev        := instr(v_strname
                                       ,' '
                                       ,-1);
               -- vv aan het begin?
               IF v_pos > 0
                  AND NOT v_vvachteraan
               THEN
                  v_strvv := substr(v_strname
                                   ,1
                                   ,v_pos); -- incl spatie!
                  IF instr(v_strallevv
                          ,v_strvv) > 0
                  THEN
                     -- hebbes
                     x_strvoorvoegsel := x_strvoorvoegsel || v_strsep || rtrim(v_strvv);
                     v_strsep         := ' ';
                     x_strachternaam  := substr(x_strachternaam
                                               ,v_pos + 1);
                     v_doorgaan       := TRUE;
                     v_vvvooraan      := TRUE;
                  END IF;
               END IF;
               -- vv aan het eind EN nog niets vervangen
               IF v_posrev > 0
                  AND NOT v_vvvooraan
                  AND NOT v_doorgaan
               THEN
                  v_strvv := substr(v_strname
                                   ,v_posrev); -- incl spatie!
                  IF instr(v_strallevv
                          ,v_strvv) > 0
                  THEN
                     -- hebbes
                     x_strvoorvoegsel := ltrim(v_strvv) || v_strsep || x_strvoorvoegsel;
                     v_strsep         := ' ';
                     x_strachternaam  := substr(x_strachternaam
                                               ,1
                                               ,v_posrev - 1);
                     v_doorgaan       := TRUE;
                     v_vvachteraan    := TRUE;
                  END IF;
               END IF;
            END LOOP;
         END IF;
      END; -- splitsName
      PROCEDURE voegtoe_imoi(p_imfi_id        IN NUMBER
                            ,p_imoi_id        IN VARCHAR2
                            ,p_imon_id        IN VARCHAR2
                            ,p_volgorde       IN NUMBER
                            ,p_impw_id_indi   IN NUMBER
                            ,p_impw_id_ingr   IN NUMBER
                            ,p_imoi_opmerking IN VARCHAR2) IS
         v_count NUMBER := 0;
         v_sql   VARCHAR2(32000);
      BEGIN
         SELECT COUNT(*)
           INTO v_count
           FROM kgc_im_onderzoek_indicaties imoi
          WHERE imoi.imfi_id = p_imfi_id
            AND imoi.imoi_id = p_imoi_id
            AND imoi.imon_id = p_imon_id;
         IF v_count = 0
         THEN
            v_sql := 'INSERT INTO kgc_im_onderzoek_indicaties ' || '( imfi_id ' || ', imoi_id ' || ', imon_id ' ||
                     ', volgorde ' || ', impw_id_indi ' || ', impw_id_ingr ' || ', opmerking ' || ') VALUES ' || '( ' ||
                     to_char(p_imfi_id) || ', ''' || p_imoi_id || '''' || ', ''' || p_imon_id || '''' || ', ''' ||
                     to_char(p_volgorde) || '''' || ', ''' || to_char(p_impw_id_indi) || '''' || ', ''' ||
                     to_char(p_impw_id_ingr) || '''' || ', ''' ||
                     REPLACE(p_imoi_opmerking
                            ,''''
                            ,'''''') || '''' || ')';
         ELSE
            -- alleen bijwerken: opmerking / indicatie
            v_sql := 'UPDATE kgc_im_onderzoek_indicaties ' || ' SET   opmerking = ''' ||
                     REPLACE(p_imoi_opmerking
                            ,''''
                            ,'''''') || '''' || ' ,     impw_id_indi = ''' || to_char(p_impw_id_indi) || '''' ||
                     ' WHERE imfi_id = ' || to_char(p_imfi_id) || ' AND   imoi_id = ''' || p_imoi_id || '''' ||
                     ' AND   imon_id = ''' || p_imon_id || '''';
         END IF;
         IF nvl(length(v_sql)
               ,0) > 0
         THEN
            BEGIN
               kgc_util_00.dyn_exec(v_sql);
            END;
         END IF;
      END; --voegtoe_imoi

      PROCEDURE bepaal_imon_afgerond(p_imfi_id        IN NUMBER
                                    ,p_imoi_parent_id IN VARCHAR2) IS
      BEGIN
         -- im_onderzoeken kunnen op afgerond gezet worden indien
         -- - nog niet afgerond
         -- - autorisator aanwezig
         -- - onderzoekswijze aanwezig
         -- - indicatie(s) aanwezig
         -- de 'like' is nodig omdat er voor iedere foetus een onderzoek is toegevoegd!
         UPDATE kgc_im_onderzoeken imon
            SET afgerond = 'J'
          WHERE imon.imfi_id = p_imfi_id
            AND imon.imon_id LIKE p_imoi_parent_id || '.%'
            AND imon.afgerond = 'N'
            AND imon.impw_id_autorisator IS NOT NULL
            AND imon.impw_id_onderzoekswijze IS NOT NULL
            AND EXISTS (SELECT NULL
                   FROM kgc_im_onderzoek_indicaties imoi
                  WHERE imoi.imon_id = imon.imon_id
                    AND imoi.imfi_id = imon.imfi_id);
      END; --bepaal_imon_afgerond

   BEGIN
      kgc_import.show_output('start kgc_interface_astraia.lees_in_astraia');

      -- Initieer globale variabele(n)
      kgc_import.g_datumformaatbron := 'yyyy-mm-dd'; -- datumformaat van astraia:

      -- lokatie gegevens ophalen
      OPEN c_imlo;
      FETCH c_imlo
         INTO r_imlo;
      IF c_imlo%NOTFOUND
      THEN
         CLOSE c_imlo;
         raise_application_error(-20000
                                ,'Import lokatie met id ' || to_char(p_imlo_id) || ' is niet gevonden!');
      END IF;
      CLOSE c_imlo;
      -- inleesrecord maken of bijwerken
      OPEN c_imfi;
      FETCH c_imfi
         INTO r_imfi;
      IF c_imfi%NOTFOUND
      THEN
         -- toevoegen!
         -- nieuw import-id uitgeven
         SELECT nvl(MAX(imfi.imfi_id)
                   ,0) + 1
           INTO v_imfi_id
           FROM kgc_import_files imfi;
         INSERT INTO kgc_import_files
            (imfi_id
            ,imlo_id
            ,filenaam
            ,import_datum)
         VALUES
            (v_imfi_id
            ,r_imlo.imlo_id
            ,p_filenaam
            ,trunc(SYSDATE));
      ELSE
         -- bijwerken
         UPDATE kgc_import_files imfi
            SET import_datum = trunc(SYSDATE)
               ,afgehandeld  = 'N'
               ,opmerkingen  = NULL
          WHERE imfi.imfi_id = r_imfi.imfi_id;
         v_imfi_id := r_imfi.imfi_id;
      END IF;
      CLOSE c_imfi;
      COMMIT;
      -- tbv error msg:
      v_strxmldirfile := r_imlo.directorynaam || ' / ' || p_filenaam;
      -- zet de inhoud van de xmlfile in KGC_IMPORT_FILES:
      IF NOT v_hersteljob
      THEN
         -- igv v_HerstelJob: astraia-file niet opnieuw inlezen!
         DECLARE
            v_unpw            VARCHAR2(200);
            v_ftperrormessage VARCHAR2(4000);
            v_ftpservername   VARCHAR2(200);
            v_ftpusername     VARCHAR2(100) := 'anonymus';
            v_ftppassword     VARCHAR2(100) := 'helix@umcn.nl';
            v_ftpsourcefile   VARCHAR2(2000);
            v_ftpremotedir    VARCHAR2(2000) := lower(r_imlo.directorynaam);
            v_ftppos          NUMBER;
            v_ftpdummykey     NUMBER;
         BEGIN
            -- un pw ophalen
            v_unpw   := kgc_sypa_00.systeem_waarde(p_parameter_code => 'ASTRAIA_FTP_UN_PW');
            v_ftppos := instr(v_unpw
                             ,'/');
            IF v_ftppos > 0
            THEN
               v_ftpusername := substr(v_unpw
                                      ,1
                                      ,v_ftppos - 1);
               v_ftppassword := substr(v_unpw
                                      ,v_ftppos + 1);
            END IF;
            v_ftpremotedir := REPLACE(v_ftpremotedir
                                     ,'ftp://'
                                     ,NULL);
            v_ftpremotedir := REPLACE(v_ftpremotedir
                                     ,'\'
                                     ,'/');
            v_ftpremotedir := rtrim(v_ftpremotedir
                                   ,'/');
            v_ftppos       := instr(v_ftpremotedir
                                   ,'/'); -- scheiding server en dir
            IF (v_ftppos > 0)
            THEN
               v_ftpservername := substr(v_ftpremotedir
                                        ,1
                                        ,v_ftppos - 1);
               v_ftpremotedir  := substr(v_ftpremotedir
                                        ,v_ftppos);
            END IF;
            v_ftpsourcefile   := v_ftpremotedir || '/' || p_filenaam;
            v_ftperrormessage := NULL;
            v_ftperrormessage := kgc_ftp.ftpgettoclob(p_servername  => v_ftpservername
                                                     ,p_port        => '21'
                                                     ,p_username    => v_ftpusername
                                                     ,p_password    => v_ftppassword
                                                     ,p_sourcefile  => v_ftpsourcefile
                                                     ,p_querytable  => 'KGC_IMPORT_FILES'
                                                     ,p_querycolumn => 'INHOUD'
                                                     ,p_querywhere  => 'WHERE IMFI_ID = ' || to_char(v_imfi_id));
            IF (nvl(length(v_ftperrormessage)
                   ,0) > 0)
            THEN
               raise_application_error(-20000
                                      ,'Error in lees_in_astraia-KGC_FTP.FTPGetToCLOB: source: ' || v_ftpsourcefile ||
                                       ' melding: ' || v_ftperrormessage || '!');
            END IF;
         END;
      END IF;
      -- imfi opnieuw ophalen
      OPEN c_imfi;
      FETCH c_imfi
         INTO r_imfi;
      CLOSE c_imfi;
      kgc_import.show_output('start parse...');
      -- parse:
      v_xmlp := xmlparser.newparser;
      xmlparser.setvalidationmode(v_xmlp
                                 ,FALSE);
      xmlparser.parseclob(v_xmlp
                         ,r_imfi.inhoud);
      -- lees document
      v_xmldoc := xmlparser.getdocument(v_xmlp);
      v_xmln0  := xmldom.makenode(xmldom.getdocumentelement(v_xmldoc));
      -- gevonden:
      IF lower(xmldom.getnodename(v_xmln0)) <> 'export'
      THEN
         raise_application_error(-20000
                                ,'Error in ' || v_strxmldirfile || ': Export keyword niet gevonden!');
      END IF;
      -- volgorde; nodig vanwege onderlinge afhankelijkheid (master-details)
      FOR n IN 0 .. 14 -- rde 090223 mantis 1710
      LOOP
         IF n = 0 -- rde 090223 mantis 1710
         THEN
            v_astraia_tag := 'Accounts'; --> tbv procedure_name die bepaalt wat er toegevoegd moet worden
         ELSIF n = 1
         THEN
            v_astraia_tag := 'Patient'; --> Persoon
         ELSIF n = 2
         THEN
            v_astraia_tag := 'Demographics'; --> Persoon
         ELSIF n = 3
         THEN
            v_astraia_tag := 'Doctor'; --> Opslaan tbv Persoon(huisarts), onderzoek (verwijzer)
         ELSIF n = 4
         THEN
            v_astraia_tag := 'Exam'; --> Dit is het bepalende onderdeel tbv zwangerschap-selectie
         ELSIF n = 5
         THEN
            v_astraia_tag := 'Episode'; --> Zwangerschap
         ELSIF n = 6
         THEN
            v_astraia_tag := 'Fetus'; --> Foetus - letop: moet NA zwangerschap door afhankelijkheid!
         ELSIF n = 7
         THEN
            v_astraia_tag := 'Exam'; --> Onderzoek (monster komt pas bij amniocentesis/
         ELSIF n = 8
         THEN
            v_astraia_tag := 'Indication_pregnancy'; --> Onderzoek > wijze; onderzoek_indicaties
         ELSIF n = 9
         THEN
            v_astraia_tag := 'Amniocentesis'; --> Vruchtwater/Amnion-punctiegegevens > monster
         ELSIF n = 10
         THEN
            v_astraia_tag := 'CVS'; --> Vlokken/Chorion-punctiegegevens > monster
         ELSIF n = 11
         THEN
            v_astraia_tag := 'FBS'; --> Navelstrengpunctiegegevens > monster
         ELSIF n = 12
         THEN
            v_astraia_tag := 'CDoctor'; --> Onderzoek > verwijzer ; 10-01-2007 RDE ook voor afname-onderzoek, dus verplaatst naar positie NA afnameverwerking!
         ELSIF n = 13
         THEN
            v_astraia_tag := 'Insurance'; --> Persoon: (evt) extra verzekeringsgegevens
         ELSIF n = 14
         THEN
            v_astraia_tag := 'Partner'; --> Persoon: (evt) partnergegevens
         END IF;
         v_xmlnl := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln0)
                                               ,v_astraia_tag);
         -- alle nodes doorlopen:
         kgc_import.show_output(to_char(xmldom.getlength(v_xmlnl)) || ' children found');
         FOR i IN 0 .. xmldom.getlength(v_xmlnl) - 1
         LOOP
            v_xmln1 := xmldom.item(v_xmlnl
                                  ,i);
            v_strnn := lower(xmldom.getnodename(v_xmln1));
            -- NN bevat patient, episode, etc.; mogelijk meerdere groepen records.
            kgc_import.show_output('lees ' || v_strnn || '...');
            v_xmlnlrec := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                     ,'record');
            FOR j IN 0 .. xmldom.getlength(v_xmlnlrec) - 1
            LOOP
               v_xmln2 := xmldom.item(v_xmlnlrec
                                     ,j);
               -- id en evt parent ophalen
               v_record_id := lower(xmldom.getattribute(xmldom.makeelement(v_xmln2)
                                                       ,'id'));
               v_parent_id := lower(xmldom.getattribute(xmldom.makeelement(v_xmln2)
                                                       ,'parent'));
               -- lijst data-regels ophalen
               v_xmlnldata := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln2)
                                                         ,'data');
               v_sql       := NULL;

               IF lower(v_strnn) = 'accounts' -- rde 090223 mantis 1710
               THEN
                  v_procedure_name := getdata(v_xmlnldata
                                             ,'procedure_name');
                  IF lower(v_procedure_name) = 'ultra geluidsonderzoek type i' THEN
                    IF v_bUG1_verwacht THEN
                      v_bProcedure_dubbel := TRUE;
                    ELSE
                      v_bUG1_verwacht := TRUE;
                    END IF;
                  ELSIF lower(v_procedure_name) = 'ultra geluidsonderzoek type ii' THEN
                    IF v_bUG2_verwacht THEN
                      v_bProcedure_dubbel := TRUE;
                    ELSE
                      v_bUG2_verwacht := TRUE;
                    END IF;
                  ELSIF lower(v_procedure_name) = 'vruchtwaterpunctie' THEN
                    IF v_bVWP_verwacht THEN
                      v_bProcedure_dubbel := TRUE;
                    ELSE
                      v_bVWP_verwacht := TRUE;
                    END IF;
                  ELSIF lower(v_procedure_name) = 'vlokkentest' THEN
                    IF v_bVLK_verwacht THEN
                      v_bProcedure_dubbel := TRUE;
                    ELSE
                      v_bVLK_verwacht := TRUE;
                    END IF;
                  ELSIF lower(v_procedure_name) = 'navelstrengpunctie' THEN
                    IF v_bNSP_verwacht THEN
                      v_bProcedure_dubbel := TRUE;
                    ELSE
                      v_bNSP_verwacht := TRUE;
                    END IF;
                  END IF;
               ELSIF lower(v_strnn) = 'patient'
               THEN
                  kgc_import.show_output('Patient met id ' || v_record_id);
                  v_impe_id := v_record_id; -- tbv vervolg: zwangerschappen/partner etc. moeten hierbij horen!
                  -- al geweest?
                  SELECT COUNT(*)
                    INTO v_count
                    FROM kgc_im_personen impe
                   WHERE impe.imfi_id = v_imfi_id
                     AND impe.impe_id = v_record_id;
                  --
                  v_impw_id_geslacht := kgc_import.getimpwid(p_imlo_id
                                                            ,kgc_import.g_cimpw_dom_geslacht
                                                            ,'V'
                                                            ,NULL
                                                            ,TRUE);
                  -- splits naam in 2
                  splitsname(getdata(v_xmlnldata
                                    ,'name')
                            ,v_pers_achternaam
                            ,v_pers_voorvoegsel);
                  v_pers_voorvoegsel := lower(v_pers_voorvoegsel); -- bah!
                  -- aanspreken wordt in veld name_partner gezet!!! Nodig voor bepaling 'gehuwd' bij partner --
                  v_patient_name_partner := getdata(v_xmlnldata
                                                   ,'name_partner');
                  -- V3: aanpassing: igv voornaam: alleen de eerste letter; Voornaam Astraia=bv Laura (1e hoofdletter, rest niet)
                  v_pers_voorletters := getdata(v_xmlnldata
                                               ,'other_names');
                  IF v_pers_voorletters = upper(v_pers_voorletters)
                  THEN
                     NULL; -- voorletters al in hoofdletters, 'dus' zijn het initialen
                  ELSE
                     v_pers_voorletters := upper(substr(v_pers_voorletters
                                                       ,1
                                                       ,1));
                  END IF;
                  -- altijd puntjes + hoofdletters!
                  v_pers_voorletters       := REPLACE(v_pers_voorletters
                                                     ,' '
                                                     ,'');
                  v_pers_voorletters       := REPLACE(v_pers_voorletters
                                                     ,'.'
                                                     ,'');
                  v_pers_voorletters_dummy := REPLACE(v_pers_voorletters
                                                     ,','
                                                     ,'');
                  v_pers_voorletters       := NULL;
                  FOR i IN 1 .. length(v_pers_voorletters_dummy)
                  LOOP
                     v_pers_voorletters := v_pers_voorletters || substr(v_pers_voorletters_dummy
                                                                       ,i
                                                                       ,1) || '.';
                  END LOOP;
                  -- rde 2013-10-08 nieuw tbv mantis 3068
                  -- 1. bsn moet overgenomen worden
                  -- 2. (tijdelijk) aanwezige zsinr moet opgehaald worden tbv omzeilen controle kgc_pers_00.controleer_bsn
                  v_bsn := getdata(v_xmlnldata
                                   ,'nhs_number');
                  v_zisnr := getdata(v_xmlnldata
                                   ,'hospital_number');
                  IF v_bsn IS NOT NULL THEN
                     DECLARE -- dit ophalen lukt alleen als de persoon al een keer in Helix gezet is; is de persoon nog niet geweest dan gaat de controle goed
                        CURSOR c_pers IS
                           SELECT zisnr
                           FROM   kgc_personen pers
                           WHERE  pers.bsn = TO_NUMBER(v_bsn)
                           AND    pers.zisnr IS NOT NULL;
                        r_pers c_pers%ROWTYPE;
                     BEGIN
                        OPEN c_pers;
                        FETCH c_pers INTO r_pers;
                        IF c_pers%FOUND THEN
                           v_zisnr := r_pers.zisnr;
                        END IF;
                        CLOSE c_pers;
                     EXCEPTION
                        WHEN OTHERS
                           THEN NULL;
                     END;
                  END IF;
                  IF v_count = 0
                  THEN
                     v_sql := 'INSERT INTO kgc_im_personen ' || '( imfi_id ' || ', impe_id ' || ', achternaam ' ||
                              ', voorletters ' || ', voorvoegsel ' || ', geboortedatum ' || ', zisnr ' ||
                              ', impw_id_geslacht, bsn,  bsn_geverifieerd) VALUES ' || '( ' || to_char(v_imfi_id) || ', ''' ||
                              v_record_id || '''' || ', ''' || REPLACE(v_pers_achternaam
                                                                      ,''''
                                                                      ,'''''') || '''' || ', ''' ||
                              REPLACE(v_pers_voorletters
                                     ,''''
                                     ,'''''') || '''' || ', ''' || REPLACE(v_pers_voorvoegsel
                                                                          ,''''
                                                                          ,'''''') || '''' || ', ''' ||
                              kgc_import.datumformaat(getdata(v_xmlnldata
                                                             ,'dob')) || '''' ||
                              ', ''' || v_zisnr || '''' ||
                              ', ' || nvl(to_char(v_impw_id_geslacht)
                                         ,'NULL') ||
                              ', ''' || v_bsn || '''' ||
                              ', ''N'')';
                  ELSE
                     v_sql := 'UPDATE kgc_im_personen ' || ' SET achternaam = ''' ||
                              REPLACE(v_pers_achternaam
                                     ,''''
                                     ,'''''') || '''' || ',    voorletters = ''' ||
                              REPLACE(v_pers_voorletters
                                     ,''''
                                     ,'''''') || '''' || ',    voorvoegsel = ''' ||
                              REPLACE(v_pers_voorvoegsel
                                     ,''''
                                     ,'''''') || '''' || ',    geboortedatum =''' ||
                              kgc_import.datumformaat(getdata(v_xmlnldata
                                                             ,'dob')) || '''' ||
                              ',    zisnr = ''' || v_zisnr || '''' ||
                              ',    impw_id_geslacht = ' || nvl(to_char(v_impw_id_geslacht)
                                                              ,'NULL') ||
                              ',    bsn = ' || '''' || v_bsn || '''' ||
                              ',    bsn_geverifieerd = ' || '''N''' ||
                              ' WHERE  imfi_id = ' || to_char(v_imfi_id) ||
                              ' AND    impe_id = ''' || v_record_id || '''';
                     IF v_hersteljob
                     THEN
                        -- even geen updates doen!
                        v_sql := NULL;
                     END IF;
                  END IF;
               ELSIF lower(v_strnn) = 'demographics'
                     AND v_eersteadres
               THEN
                  -- adresgegevens; alleen de eerste
                  v_eersteadres := FALSE;
                  kgc_import.show_output('Demographics met id ' || v_record_id);
                  -- bestaat de persoon al?
                  SELECT COUNT(*)
                    INTO v_count
                    FROM kgc_im_personen impe
                   WHERE impe.imfi_id = v_imfi_id
                     AND impe.impe_id = v_parent_id;
                  IF v_count = 0
                  THEN
                     raise_application_error(-20000
                                            ,'Error in ' || v_strxmldirfile || ': Demographics met id ' || v_record_id ||
                                             ' verwijst naar niet bestaande persoon met id ' || v_parent_id || '!');
                  ELSE
                     -- eventuele verwijzing opslaan naar insurance-record: in de file kunnen er meer zitten!!
                     v_insurance_record_id := getdata(v_xmlnldata
                                                     ,'insurance_company');
                     -- V3 bah: indien het nummer te groot wordt, dan is het hier ineens een E-macht.
                     -- het 'id' in het Insurance-record is wel goed! Dus:
                     IF instr(v_insurance_record_id
                             ,'E') > 0
                     THEN
                        -- dan: alles na de E eraf + punten verwijderen
                      /*  v_insurance_record_id := REPLACE(substr(v_insurance_record_id ,1 ,instr(v_insurance_record_id ,'E') - 1) ,'.' ,'');*/
                        v_insurance_record_id :=TO_CHAR(TO_NUMBER(REPLACE(v_insurance_record_id,'.',',')));--Modified for Mantis 7575..Code given by NUMeriek
                     END IF;
                     -- letop: v_impw_id_verz ook gebruikt bij insurance
                     v_impw_id_verz := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_verz
                                                           ,getdata(v_xmlnldata
                                                                   ,'insurance')
                                                           ,NULL
                                                           ,TRUE);
                     -- Bah. in demographics zit naast verwijzing insurance_company *SOMS(oud?)* ook tekstveld insurance
                     -- en mogelijk zijn er meerdere insurance_companies! Bah; zie LOWER(v_strNN) = 'insurance'
                     -- daar wordt het rechtgetrokken!
                     v_impw_id_rela := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_rela
                                                           ,getdata(v_xmlnldata
                                                                   ,'gp')
                                                           ,NULL -- niet de bestaande hint overschrijven
                                                           ,TRUE);
                     -- verzekeringswijze: afh van private_patient en verz
                     -- oei - in V63 is dit (voor Netherlands) vervangen door insurance_type!
                     v_str := getdata(v_xmlnldata
                                     ,'insurance_type');
                     IF nvl(length(v_str)
                           ,0) = 0
                     THEN
                        -- dan nog de andere proberen
                        IF getdata(v_xmlnldata
                                  ,'private_patient') = '1'
                        THEN
                           v_str := 'Particulier';
                        ELSIF length(nvl(getdata(v_xmlnldata
                                                ,'insurance')
                                        ,'')) > 0
                        THEN
                           v_str := 'Ziekenfonds';
                        ELSE
                           v_str := 'Onbekend';
                        END IF;
                     END IF;
                     v_impw_id_verzwijze := kgc_import.getimpwid(p_imlo_id
                                                                ,kgc_import.g_cimpw_dom_verzekeringswijze
                                                                ,v_str
                                                                ,'betekenis - zie Astraia' -- interne lijst!
                                                                ,TRUE);
                     -- v7: land omzetten
                     v_land := getdata(v_xmlnldata
                                      ,'country');
                     IF upper(v_land) = 'NED'
                     THEN
                        v_land := 'Nederland';
                     END IF;
                     v_sql := 'UPDATE kgc_im_personen ' || ' SET adres = ''' ||
                              REPLACE(getdata(v_xmlnldata
                                             ,'address')
                                     ,''''
                                     ,'''''') || '''' || ',    woonplaats = ''' ||
                              REPLACE(getdata(v_xmlnldata
                                             ,'town')
                                     ,''''
                                     ,'''''') || '''' || ',    postcode = ''' ||
                              REPLACE(getdata(v_xmlnldata
                                             ,'postcode')
                                     ,''''
                                     ,'''''') || '''' || ',    land = ''' ||
                              REPLACE(v_land
                                     ,''''
                                     ,'''''') || '''' || ',    telefoon = ''' ||
                              REPLACE(getdata(v_xmlnldata
                                             ,'home_tel')
                                     ,''''
                                     ,'''''') || ''''
                             --                || ',    work_tel = ''' || REPLACE(getData(v_XMLnldata, 'work_tel'), '''', '''''') || '''' --
                             --                || '  ,    mobile_phone = ''' || REPLACE(getData(v_XMLnldata, 'mobile_phone'), '''', '''''') || '''' --
                              || ',    telefax = ''' || REPLACE(getdata(v_xmlnldata
                                                                       ,'fax')
                                                               ,''''
                                                               ,'''''') || '''' || ',    email = ''' ||
                              REPLACE(getdata(v_xmlnldata
                                             ,'email')
                                     ,''''
                                     ,'''''') || ''''
                             --            '   || ',    occupation = ''' || REPLACE(getData(v_XMLnldata, 'occupation'), '''', '''''') || '''' --
                             --                || ',    ethnic_group = ''' || REPLACE(getData(v_XMLnldata, 'ethnic_group'), '''', '''''') || ''''' --
                              || ',    impw_id_verzwijze = ' || nvl(to_char(v_impw_id_verzwijze)
                                                                   ,'NULL') || ',    impw_id_verz = ' ||
                              nvl(to_char(v_impw_id_verz)
                                 ,'NULL') || ',    verzekeringsnr = ''' ||
                              getdata(v_xmlnldata
                                     ,'insurance_number') || '''' || ',    impw_id_rela = ' ||
                              nvl(to_char(v_impw_id_rela)
                                 ,'NULL') || ' WHERE  imfi_id = ' || to_char(v_imfi_id) || ' AND    impe_id = ''' ||
                              v_parent_id || '''';
                     IF v_hersteljob
                     THEN
                        -- even geen updates doen!
                        v_sql := NULL;
                     END IF;
                  END IF;
               ELSIF lower(v_strnn) = 'doctor'
               THEN
                  kgc_import.show_output('Doctor met id ' || v_record_id);
                  -- nog iets doen met title? category?
                  -- wellicht doctor, evt. hint toevoegen in kgc_import_param_waarden
                  v_impw_hint := NULL;
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Astraia-huisarts/verwijzer');
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Achternaam: ' || getdata(v_xmlnldata
                                                              ,'name'));
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Voornaam: ' || getdata(v_xmlnldata
                                                            ,'other_names'));
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Telefoon: ' || getdata(v_xmlnldata
                                                            ,'telephone_1'));
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Adres: ' || getdata(v_xmlnldata
                                                         ,'address'));
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Postcode: ' || getdata(v_xmlnldata
                                                            ,'postcode') || ' Woonplaats: ' ||
                                     getdata(v_xmlnldata
                                            ,'town'));
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Land: ' || getdata(v_xmlnldata
                                                        ,'country'));
                  kgc_import.addtext(v_impw_hint
                                    ,2000
                                    ,'Briefaanhef: ' || getdata(v_xmlnldata
                                                               ,'greeting'));
                  -- returnwaarde niet boeiend
                  v_impw_id_rela := kgc_import.getimpwid(p_imlo_id
                                                        ,kgc_import.g_cimpw_tab_rela
                                                        ,v_record_id
                                                        ,v_impw_hint
                                                        ,TRUE);
               ELSIF lower(v_strnn) = 'episode'
               THEN
                  kgc_import.show_output('Episode met id ' || v_record_id || ', type ' ||
                                         getdata(v_xmlnldata
                                                ,'type'));
                  IF getdata(v_xmlnldata
                            ,'type') = '1'
                     AND v_record_id = v_episode_record_id
                  THEN
                     -- juiste type! (1 = zwangerschap; v_episode_record_id = parent van aanwezige exam)
                     IF v_parent_id != v_impe_id
                     THEN
                        raise_application_error(-20000
                                               ,'Error in ' || v_strxmldirfile || ': Zwangerschap met id ' ||
                                                v_record_id || ' verwijst niet naar juiste persoon (is id ' ||
                                                v_parent_id || ', moet zijn ' || v_impe_id || ')!');
                     END IF;
                     v_zwan_zekerheid_datum_lm_ext := 'LEGE A terme datum volgens echo (->Waarschijnlijk)';
                     IF nvl(length(getdata(v_xmlnldata
                                          ,'edd_us'))
                           ,0) > 0
                     THEN
                        v_zwan_zekerheid_datum_lm_ext := 'GEVULDE A terme datum volgens echo (->Zeker)';
                     END IF;
                     v_zwan_berekeningswijze_ext := 'LEGE A terme datum volgens LM (->ECHO)';
                     IF nvl(length(getdata(v_xmlnldata
                                          ,'edd_lmp'))
                           ,0) > 0
                     THEN
                        v_zwan_berekeningswijze_ext := 'GEVULDE A terme datum volgens LM (->LM)';
                     END IF;
                     SELECT COUNT(*)
                       INTO v_count
                       FROM kgc_im_zwangerschappen imzw
                      WHERE imzw.imfi_id = v_imfi_id
                        AND imzw.imzw_id = v_record_id
                        AND imzw.impe_id = v_parent_id;
                     v_impw_id_zekerheid_datum_lm := kgc_import.getimpwid(p_imlo_id
                                                                         ,kgc_import.g_cimpw_dom_zekerheid
                                                                         ,v_zwan_zekerheid_datum_lm_ext
                                                                         ,NULL
                                                                         ,TRUE);
                     v_impw_id_berekeningswijze   := kgc_import.getimpwid(p_imlo_id
                                                                         ,kgc_import.g_cimpw_dom_berekeningswijze
                                                                         ,v_zwan_berekeningswijze_ext
                                                                         ,NULL
                                                                         ,TRUE);
                     -- datum_aterm opbergen tbv foetus(sen)
                     v_foet_geplande_geboortedatum := kgc_import.datumformaat(getdata(v_xmlnldata
                                                                                     ,'edd_us'));
                     IF v_foet_geplande_geboortedatum IS NULL
                     THEN
                        v_foet_geplande_geboortedatum := kgc_import.datumformaat(getdata(v_xmlnldata
                                                                                        ,'edd_lmp'));
                     END IF;
                     -- bevat lmp wel de lmp? Bah! Flexibel, maar er zit wel logica aan vast!
                     v_zwan_datum_lm := NULL;
                     IF getdata(v_xmlnldata
                               ,'pregnancy_dates') = '1'
                     THEN
                        v_zwan_datum_lm := kgc_import.datumformaat(getdata(v_xmlnldata
                                                                          ,'lmp'));
                     END IF;
                     IF nvl(length(v_zwan_datum_lm)
                           ,0) = 0
                     THEN
                        v_impw_id_zekerheid_datum_lm := NULL; -- ook de bijbehorende zekerheid!
                     END IF;
                     IF v_count = 0
                     THEN
                        v_sql := 'INSERT INTO kgc_im_zwangerschappen ' || '( imfi_id ' || ', imzw_id ' || ', impe_id ' ||
                                 ', nummer ' || ', impw_id_zekerheid_datum_lm ' || ', datum_lm ' ||
                                 ', impw_id_berekeningswijze ' || ', datum_aterm ' || ') VALUES ' || '( ' ||
                                 to_char(v_imfi_id) || ', ''' || v_record_id || '''' || ', ''' || v_parent_id || '''' ||
                                 ', ''' || getdata(v_xmlnldata
                                                  ,'gravida') || '''' || ', ''' || v_impw_id_zekerheid_datum_lm || '''' ||
                                 ', ''' || v_zwan_datum_lm || '''' || ', ''' || v_impw_id_berekeningswijze || '''' ||
                                 ', ''' || nvl(kgc_import.datumformaat(getdata(v_xmlnldata
                                                                              ,'edd_us'))
                                              ,kgc_import.datumformaat(getdata(v_xmlnldata
                                                                              ,'edd_lmp'))) || '''' || ')';
                     ELSE
                        -- niet bijwerken: parent_id
                        v_sql := 'UPDATE kgc_im_zwangerschappen ' || ' SET impw_id_zekerheid_datum_lm = ''' ||
                                 v_impw_id_zekerheid_datum_lm || '''' || ',    nummer = ''' ||
                                 getdata(v_xmlnldata
                                        ,'gravida') || '''' || ',    datum_lm = ''' || v_zwan_datum_lm || '''' ||
                                 ',    impw_id_berekeningswijze = ''' || v_impw_id_berekeningswijze || '''' ||
                                 ',    datum_aterm = ''' ||
                                 nvl(kgc_import.datumformaat(getdata(v_xmlnldata
                                                                    ,'edd_us'))
                                    ,kgc_import.datumformaat(getdata(v_xmlnldata
                                                                    ,'edd_lmp'))) || '''' || ' WHERE imfi_id = ' ||
                                 to_char(v_imfi_id) || ' AND   imzw_id = ''' || v_record_id || '''' ||
                                 ' AND   impe_id = ''' || v_parent_id || '''';
                        IF v_hersteljob
                        THEN
                           -- even geen updates doen!
                           v_sql := NULL;
                        END IF;
                     END IF;
                     v_imzw_id_foetus := v_record_id; --bewaren voor foetussen; er is maar 1 zwangerschap per imfi, dus dit gaat goed
                  END IF;
               ELSIF lower(v_strnn) = 'fetus'
               THEN
                  kgc_import.show_output('Foetus met id ' || v_record_id);
                  -- lastig geval: de foetusgegevens worden per exam vastgelegd!
                  -- en exam wordt vastgelegd per zwangerschap, waarvan er max 1 is per file
                  -- binnen de zwangerschap hebben ze een vast nummertje: n
                  -- Dus:
                  IF v_imzw_id_foetus IS NULL -- komt niet voor
                  THEN
                     kgc_import.show_output('Geen zwangerschap voor foetus!');
                  ELSE
                     -- opslaan tbv toev onderzoeken en monsters
                     v_tabfoetusexam(v_tabfoetusexam.COUNT + 1) := v_record_id;
                     --
                     SELECT COUNT(*)
                       INTO v_count
                       FROM kgc_im_foetussen imfo
                      WHERE imfo.imfi_id = v_imfi_id
                        AND imfo.imfo_id = v_record_id;
                     -- standaard zetten: (1) in Astraia nooit ingevuld; (2) in Helix geen default!
                     v_impw_id_geslacht := kgc_import.getimpwid(p_imlo_id
                                                               ,kgc_import.g_cimpw_dom_geslacht
                                                               ,'O'
                                                               ,NULL
                                                               ,TRUE);
                     IF v_count = 0
                     THEN
                        v_sql := 'INSERT INTO kgc_im_foetussen ' || '( imfi_id ' || ', imfo_id ' || ', impe_id_moeder ' ||
                                 ', volgnr ' || ', impw_id_geslacht ' || ', geplande_geboortedatum ' || ', imzw_id ' ||
                                 ') VALUES ' || '( ' || to_char(v_imfi_id) || ', ''' || v_record_id || '''' || ', ''' ||
                                 v_impe_id || '''' || ', ''' || getdata(v_xmlnldata
                                                                       ,'n') || '''' || ', ''' || v_impw_id_geslacht || '''' ||
                                 ', ''' || v_foet_geplande_geboortedatum || '''' || ', ''' || v_imzw_id_foetus || '''' || ')';
                     ELSE
                        -- niet bijwerken: alles behalve datum aterm
                        v_sql := 'UPDATE kgc_im_foetussen ' || ' SET geplande_geboortedatum = ''' ||
                                 v_foet_geplande_geboortedatum || '''' || ' WHERE imfi_id = ' || to_char(v_imfi_id) ||
                                 ' AND   imfo_id = ''' || v_record_id || '''';
                        IF v_hersteljob
                        THEN
                           -- even geen updates doen!
                           v_sql := NULL;
                        END IF;
                     END IF;
                  END IF;
               ELSIF lower(v_strnn) = 'exam'
               THEN
                  -- LET OP: een export wordt gemaakt per exam, dus er is er maar 1 van!!!
                  -- en deze komt 2 keer langs!
                  IF v_episode_record_id IS NULL -- dit is de eerste keer: welke episode is van belang?
                  THEN
                     v_episode_record_id := v_parent_id;
                     -- bepaal alvast de indicatie voor de afname; gebruik bij toev. afname-onderzoek
                     v_afname_indicatie := substr(getdata(v_xmlnldata
                                                         ,'invasive_indication'
                                                         ,3)
                                                 ,1
                                                 ,100);
                  ELSE
                     -- 2e keer: verwerk onderzoek
                     kgc_import.show_output('Exam met id ' || v_record_id);
                     -- Filter: is exam relevant voor Nijmegen?
                     IF nvl(getdata(v_xmlnldata
                                   ,'amniocentesis')
                           ,'0') != '0'
                     THEN
                       v_bVWP_verwacht := TRUE;
                     END IF;
                     IF nvl(getdata(v_xmlnldata
                                      ,'cvs')
                              ,'0') != '0'
                     THEN
                       v_bVLK_verwacht := TRUE;
                     END IF;
                     IF nvl(getdata(v_xmlnldata
                                      ,'fbs')
                              ,'0') != '0'
                     THEN
                       v_bNSP_verwacht := TRUE;
                     END IF;
                     IF v_bVWP_verwacht OR v_bVLK_verwacht OR v_bNSP_verwacht THEN
                        kgc_import.show_output('Punctie aanwezig (' || v_record_id || ')');
                        -- oei! Nu nog niets doen, want het kan zijn dat er bv een record amniocentesis aanwezig is,
                        -- maar dat de hoeveelheid daarvan LEEG is. Dan geen monster!
                        -- dus: verwerking uitstellen tot later... (amniocentesis / cvs / fbs)
                        v_xmlnldatapunctieexam  := v_xmlnldata; -- tbv verwerking verderop!
                        v_punctieexam_record_id := v_record_id; -- idem
                     END IF; -- rde 12-12-2006 Mantis 339: combinatie ulg+punctie komt ook voor!
                     --
                     IF v_bUG1_verwacht OR v_bUG2_verwacht
                     THEN
                        FOR iUG IN 1..2 LOOP -- zowel voor UG1 als UG2 eventueel een onderzoek toevoegen
                           -- voor iedere foetus een onderzoek maken (niet wijzigen)
                           -- onderzoeksgroep bepalen
                           kgc_import.show_output('Ultrageluid aanwezig (' || v_record_id || ')');
                           v_bultrageluidverwerkt := TRUE; -- o.a. tbv toevoegen indicaties
                           v_impw_id_ongr := NULL;
                           v_impw_id_ongr := kgc_import.getimpwid(p_imlo_id
                                                                 ,kgc_import.g_cimpw_tab_ongr
                                                                 ,'Onderzoek'
                                                                 ,NULL
                                                                 ,TRUE);
                           -- onderzoekswijze kan nu direct -- rde 090223 mantis 1710
                           v_onde_onderzoekswijze_ext := NULL;
                           v_impw_id_onderzoekswijze := NULL;
                           v_imon_declareren := 'N';
                           IF v_bUG1_verwacht AND iUG = 1 THEN
                             v_onde_onderzoekswijze_ext := 'U1';
                           ELSIF v_bUG2_verwacht AND iUG = 2 THEN
                             v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                           IF nvl(length(v_onde_onderzoekswijze_ext)
                                 ,0) > 0
                           THEN
                              -- onderzoek aanmaken!
                              v_imon_declareren          := 'J';
                              v_impw_id_onderzoekswijze := kgc_import.getimpwid(p_imlo_id
                                                                                ,kgc_import.g_cimpw_tab_onwy
                                                                                ,v_onde_onderzoekswijze_ext
                                                                                ,'Onderzoekswijze ultrageluidsonderzoek'
                                                                                ,TRUE);
                              -- aanvrager: komt elders via 'cdoctor'
                              -- herkomst
                              v_impw_id_herk := kgc_import.getimpwid(p_imlo_id
                                                                    ,kgc_import.g_cimpw_tab_herk
                                                                    ,kgc_import.g_cimpw_dummie
                                                                    ,'Herkomst gegevens'
                                                                    ,TRUE);
                              -- instelling
                              v_impw_id_inst        := kgc_import.getimpwid(p_imlo_id
                                                                           ,kgc_import.g_cimpw_tab_inst
                                                                           ,kgc_import.g_cimpw_dummie
                                                                           ,'Instelling'
                                                                           ,TRUE);
                              v_impw_id_autorisator := kgc_import.getimpwid(p_imlo_id
                                                                           ,kgc_import.g_cimpw_tab_mede
                                                                           ,getdata(v_xmlnldata
                                                                                   ,'supervisor')
                                                                           ,'Autorisator ultrageluidsonderzoek'
                                                                           ,TRUE);
                              IF v_impw_id_autorisator IS NULL
                              THEN
                                 -- dan de uitvoerder!
                                 v_impw_id_autorisator := kgc_import.getimpwid(p_imlo_id
                                                                              ,kgc_import.g_cimpw_tab_mede
                                                                              ,getdata(v_xmlnldata
                                                                                      ,'us_operator')
                                                                              ,'Autorisator ultrageluidsonderzoek'
                                                                              ,TRUE);
                              END IF;
                              -- voor alle foetussen bij dit onderzoek:
                              IF v_tabfoetusexam.COUNT > 0
                              THEN
                                 FOR j IN v_tabfoetusexam.FIRST .. v_tabfoetusexam.LAST
                                 LOOP
                                    v_imon_id := v_record_id || '.' || v_tabfoetusexam(j) || '.' || v_onde_onderzoekswijze_ext;
                                    OPEN c_imon_check(v_imfi_id
                                                     ,v_imon_id);
                                    FETCH c_imon_check
                                       INTO r_imon_check;
                                    IF c_imon_check%NOTFOUND
                                    THEN
                                       -- toevoegen
                                       v_sql := 'INSERT INTO kgc_im_onderzoeken ' || '( imfi_id ' || ', imon_id ' ||
                                                ', impw_id_ongr ' || ', impe_id ' || ', declareren ' || ', afgerond ' ||
                                                ', imfo_id ' -- foetus
                                                || ', datum_binnen ' || ', impw_id_herk ' || ', impw_id_inst '
                                                || ', impw_id_autorisator ' || ', datum_autorisatie '
                                                || ', impw_id_onderzoekswijze '
                                                || ') VALUES ' || '( ' ||
                                                to_char(v_imfi_id) || ', ''' || v_imon_id || '''' || ', ''' || v_impw_id_ongr || '''' ||
                                                ', ''' || v_impe_id || '''' || ', ''' || v_imon_declareren || ''''
                                                || ', ''N''' -- ULG is standaard NIET afgerond - tenzij wijze + indicatie ok!
                                                || ', ''' || v_tabfoetusexam(j) || '''' || ', ''' ||
                                                kgc_import.datumformaat(getdata(v_xmlnldata
                                                                               ,'us_date')) || '''' || ', ''' ||
                                                v_impw_id_herk || '''' || ', ''' || v_impw_id_inst || '''' || ', ''' ||
                                                v_impw_id_autorisator || '''' || ', ''' ||
                                                kgc_import.datumformaat(getdata(v_xmlnldata
                                                                               ,'us_date')) || ''''
                                                || ', ''' || v_impw_id_onderzoekswijze || ''''
                                                || ')';
                                    ELSE
                                       -- just checking
                                       IF r_imon_check.impe_id != v_impe_id
                                       THEN
                                          raise_application_error(-20000
                                                                 ,'Error in ' || v_strxmldirfile || ': Onderzoek met id ' ||
                                                                  v_record_id ||
                                                                  ' verwijst niet naar juiste persoon (is id ' ||
                                                                  v_parent_id || ', moet zijn ' || v_impe_id || ')!');
                                       END IF;
                                       -- WEL aanpassingen igv reeds bestaand onderzoek: het evt een verbeterde 2e poging!
                                       v_sql := 'UPDATE kgc_im_onderzoeken ' || ' SET impw_id_ongr = ''' || v_impw_id_ongr || '''' ||
                                                ',    impe_id      = ''' || v_impe_id || '''' || ',    declareren   = ''' || v_imon_declareren || '''' ||
                                                ',    afgerond     = ''N''' || ',    imfo_id      = ''' || v_tabfoetusexam(j) || '''' ||
                                                ',    datum_binnen = ''' ||
                                                kgc_import.datumformaat(getdata(v_xmlnldata
                                                                               ,'us_date')) || '''' ||
                                                ',    impw_id_herk = ''' || v_impw_id_herk || '''' ||
                                                ',    impw_id_inst = ''' || v_impw_id_inst || '''' ||
                                                ',    impw_id_autorisator = ''' || v_impw_id_autorisator || '''' ||
                                                ',    datum_autorisatie =''' || kgc_import.datumformaat(getdata(v_xmlnldata
                                                                               ,'us_date')) || '''' ||
                                                ',    impw_id_onderzoekswijze = ''' || v_impw_id_onderzoekswijze || ''''
                                                || ' WHERE  imfi_id = ' ||
                                                to_char(v_imfi_id) || ' AND    imon_id = ''' || v_imon_id || '''';
                                       IF v_hersteljob
                                       THEN
                                          -- even geen updates doen!
                                          v_sql := NULL;
                                       END IF;
                                    END IF;
                                    IF nvl(length(v_sql)
                                          ,0) > 0
                                    THEN
                                       BEGIN
                                          kgc_util_00.dyn_exec(v_sql);
                                       END;
                                    END IF;
                                    v_sql := NULL;
                                    CLOSE c_imon_check;
                                 END LOOP;
                              END IF;
                           END IF;
                        END LOOP; -- loop voor ug1 en ug2
                        -- eind v_bUG1_verwacht OR v_bUG2_verwacht
                     END IF; -- rde 12-12-2006 Mantis 339: combinatie ulg+punctie komt ook voor!
                     -- nieuw: geslacht is mogelijk bekend
                     IF v_imzw_id_foetus IS NOT NULL -- dan: zwangerschap bekend
                     THEN
                        IF getdata(v_xmlnldata
                                  ,'wants_to_know_sex') = '1'
                        THEN
                           UPDATE kgc_im_zwangerschappen imzw
                              SET imzw.info_geslacht = 'J'
                            WHERE imzw.imfi_id = v_imfi_id
                              AND imzw.imzw_id = v_imzw_id_foetus;
                        END IF;
                     END IF;
                  END IF; -- verwerk onderzoek
               ELSIF lower(v_strnn) = 'indication_pregnancy'
                     AND v_bultrageluidverwerkt -- nu worden alleen de indicaties tbv ulg opgeslagen
               THEN
                  kgc_import.show_output('Indication_pregnancy met id ' || v_record_id);
                  --
                  v_imoi_indication_type_1   := (nvl(getdata(v_xmlnldata
                                                            ,'indication_type_1')
                                                    ,'0') != '0');
                  v_imoi_indication_type_2   := (nvl(getdata(v_xmlnldata
                                                            ,'indication_type_2')
                                                    ,'0') != '0');
                  v_imoi_history := (nvl(getdata(v_xmlnldata
                                                ,'history')
                                        ,'0') != '0');
                  -- indicaties verwerken:
                  -- alle individuele indicaties opnemen (geen groepsindicaties)
                  -- voor iedere foetus!
                  -- en alleen indien een bijbehorend onderzoek aanwezig is!
                  SELECT COUNT(*)
                    INTO v_count
                    FROM kgc_im_onderzoeken imon
                   WHERE imon.imfi_id = v_imfi_id
                     AND imon.imon_id LIKE v_parent_id || '.%';
                  IF v_count > 0
                  THEN
                     -- gegevens verwerken
                     FOR i IN 1 .. 18 -- rde 090223 mantis 1710
                     LOOP
                        v_imoi_opmerking       := NULL;
                        v_impw_externe_id      := NULL;
                        v_impw_externe_id_plus := NULL;
                        v_impw_hint            := NULL;
                        v_onde_onderzoekswijze_ext := NULL;
                        -- i wordt ook gebruikt tbv id + volgorde!
                        IF i = 1
                        THEN
                           -- history_previous_pregnancy
                           IF v_imoi_indication_type_1
                              AND v_imoi_history
                           THEN
                              v_impw_externe_id := 'history_previous_pregnancy';
                              v_impw_hint       := 'Type 1 / Anamnese / Vorige zwangerschap';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 2
                        THEN
                           -- history_parents
                           IF v_imoi_indication_type_1
                              AND v_imoi_history
                           THEN
                              v_impw_externe_id := 'history_parents';
                              v_impw_hint       := 'Type 1 / Anamnese / Ouder';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 3
                        THEN
                           -- history_relative
                           IF v_imoi_indication_type_1
                              AND v_imoi_history
                           THEN
                              v_impw_externe_id := 'history_relative';
                              v_impw_hint       := 'Type 1 / Anamnese / Familie';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 4
                        THEN
                           -- insulin_dependant_diabetes
                           IF v_imoi_indication_type_1
                           THEN
                              v_impw_externe_id := 'insulin_dependant_diabetes';
                              v_impw_hint       := 'Type 1 / Insuline afhankelijke diabetes';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 5
                        THEN
                           -- anti_epileptic_drugs
                           IF v_imoi_indication_type_1
                           THEN
                              v_impw_externe_id := 'anti_epileptic_drugs';
                              v_impw_hint       := 'Type 1 / Anti-epileptica gebruik';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 6
                        THEN
                           -- drug_abuse
                           IF v_imoi_indication_type_1
                           THEN
                              v_impw_externe_id := 'drug_abuse';
                              v_impw_hint       := 'Type 1 / Drugs-gebruik';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 7
                        THEN
                           -- harmful_medicines
                           IF v_imoi_indication_type_1
                           THEN
                              v_impw_externe_id := 'harmful_medicines';
                              v_impw_hint       := 'Type 1 / Gebruik van schadelijke medicijnen';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 8
                        THEN
                           -- icsi
                           IF v_imoi_indication_type_1
                           THEN
                              v_impw_externe_id := 'icsi';
                              v_impw_hint       := 'Type 1 / Zwanger via ICSI';
                              v_onde_onderzoekswijze_ext := 'U1';
                           END IF;
                        ELSIF i = 9
                        THEN
                           -- type_1_other
                           IF v_imoi_indication_type_1
                           THEN
                              v_impw_externe_id := 'type_1_other';
                              IF upper(getdata(v_xmlnldata
                                              ,'type_1_other1')) = '1E TRIMESTER SCREENING 36 JAAR EN OUDER'
                              THEN
                                 v_impw_externe_id_plus := '1TS36JO';
                                 v_impw_hint            := 'Type 1 / Overig / 1e Trim. Scr 36jr en ouder';
                                 v_onde_onderzoekswijze_ext := 'U1';
                              ELSE
                                 v_impw_externe_id_plus := 'overig';
                                 v_impw_hint            := 'Type 1 / Overig';
                                 v_onde_onderzoekswijze_ext := 'U1';
                              END IF;
                           END IF;
                        ELSIF i = 10
                        THEN
                           -- fetalanomaly
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'fetalanomaly';
                              v_impw_hint       := 'Type 2 / Foetale afwijking';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 11
                        THEN
                           -- negative_discongruency
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'negative_discongruency';
                              v_impw_hint       := 'Type 2 / Negatieve dyscongruentie (foetale bovenbuikomtrek < P5)';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 12
                        THEN
                           -- positive_discongruency
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'positive_discongruency';
                              v_impw_hint       := 'Type 2 / Positieve dyscongruentie (foetale bovenbuikomtrek > P95)';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 13
                        THEN
                           -- oligohydramnios
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'oligohydramnios';
                              v_impw_hint       := 'Type 2 / Oligohydramnion (grootste vruchtw. comp. < 2cm)';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 14
                        THEN
                           -- ind_polyhydramnios
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'ind_polyhydramnios';
                              v_impw_hint       := 'Type 2 / Polyhydramnion (grootste vruchtw. comp. > 8cm)';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 15
                        THEN
                           -- dysrhythmia
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'dysrhythmia';
                              v_impw_hint       := 'Type 2 / Foetale hartritmestoornis';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 16
                        THEN
                           -- previous_test
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'previous_test';
                              v_impw_hint       := 'Type 2 / Afwijkende uitslag van invasieve diagnostiek';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 17
                        THEN
                           -- type_2_other
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'type_2_other';
                              v_impw_hint       := 'Type 2 / Andere';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        ELSIF i = 18 -- rde 090223 mantis 1710
                        THEN
                           -- type_2_other
                           IF v_imoi_indication_type_2
                           THEN
                              v_impw_externe_id := 'incomplete_seo';
                              v_impw_hint       := 'Type 2 / Incomplete SEO';
                              v_onde_onderzoekswijze_ext := 'U2';
                           END IF;
                        END IF;

                        -- afhandeling
                        IF v_impw_externe_id IS NOT NULL
                        THEN
                           -- dan is er iets aanwezig
                           IF getdata(v_xmlnldata
                                     ,v_impw_externe_id) <> '0'
                           THEN
                              IF v_impw_externe_id_plus IS NOT NULL
                              THEN
                                 -- er aan vast plakken
                                 v_impw_externe_id := v_impw_externe_id || ' / ' || v_impw_externe_id_plus;
                              END IF;
                              v_impw_id_indi := kgc_import.getimpwid(p_imlo_id
                                                                    ,kgc_import.g_cimpw_tab_indi
                                                                    ,v_impw_externe_id
                                                                    ,v_impw_hint
                                                                    ,TRUE);
                              -- voor alle foetussen bij dit onderzoek:
                              IF v_tabfoetusexam.COUNT > 0
                              THEN
                                 FOR j IN v_tabfoetusexam.FIRST .. v_tabfoetusexam.LAST
                                 LOOP
                                    v_imon_id := v_parent_id || '.' || v_tabfoetusexam(j) || '.' || v_onde_onderzoekswijze_ext;
                                    -- rde 12-08-2009 mantis 3071: check of verwijzing bestaat
                                    SELECT COUNT(*)
                                      INTO v_count
                                      FROM kgc_im_onderzoeken imon
                                     WHERE imon.imfi_id = v_imfi_id
                                       AND imon.imon_id = v_imon_id;
                                    IF v_count = 0
                                    THEN -- verwerking afbreken!
                                       v_error := TRUE;
                                       v_imfi_opmerkingen := 'Error in ' || v_strxmldirfile || ': Indicatie ''' || v_impw_hint || ''' hoort bij een onderzoek met onderzoekswijze ''' ||
                                                              v_onde_onderzoekswijze_ext || ''', maar hiervoor is geen procedurecode opgegeven!';
                                       EXIT;
                                    END IF;
                                    voegtoe_imoi(p_imfi_id        => v_imfi_id
                                                ,p_imoi_id        => v_record_id || '_N' || to_char(i) -- N = Nieuw: 01 tm 99 werd gebruikt in de oude situatie = situatie van voor 14-2-2006; nu worden er minder indicaties overgestuurd!
                                                ,p_imon_id        => v_imon_id
                                                ,p_volgorde       => i
                                                ,p_impw_id_indi   => v_impw_id_indi
                                                ,p_impw_id_ingr   => v_impw_id_ingr -- leeg!
                                                ,p_imoi_opmerking => v_imoi_opmerking);
                                 END LOOP; -- foetussen
                                 IF v_error
                                 THEN -- rde 12-08-2009 Mantis 3071
                                    EXIT;
                                 END IF;
                              END IF;
                           END IF;
                        END IF;
                     END LOOP;
                     IF v_error
                     THEN -- rde 12-08-2009 Mantis 3071
                        EXIT;
                     END IF;
                  END IF;
                  -- probeer im_onderzoek op afgerond te zetten (iedere keer een poging na toevoeging ind!)
                  bepaal_imon_afgerond(p_imfi_id        => v_imfi_id
                                      ,p_imoi_parent_id => v_parent_id);
                  v_sql := NULL; -- aktie is al uitgevoerd
               ELSIF lower(v_strnn) = 'cdoctor'
               THEN
                  -- tbv bepaling verwijzer
                  kgc_import.show_output('cdoctor met id ' || v_record_id);
                  -- ai ai ai: soms komen er 2 record voor met n=2, en geen met n=1
                  -- dus: niet testen op n, maar of er al 1 geweest is!
                  -- fout: IF NVL(getData(v_XMLnldata, 'n'), '0') = '1'
                  IF NOT v_bcdoctoralgeweest
                  THEN
                     -- 1e verwijzer is ok!
                     v_bcdoctoralgeweest := TRUE;
                     -- proberen toegevoegd onderzoek bij te werken
                     IF getdata(v_xmlnldata
                               ,'doctor') IS NOT NULL
                     THEN
                        v_impw_id_rela := kgc_import.getimpwid(p_imlo_id
                                                              ,kgc_import.g_cimpw_tab_rela
                                                              ,getdata(v_xmlnldata
                                                                      ,'doctor')
                                                              ,NULL -- niet de bestaande hint overschrijven!
                                                              ,TRUE);
                        -- deze moet bestaan: alle in imfi_id aanwezige doctoren zijn al toegevoegd
                        -- voor alle bijbehorende onderzoeken bijwerken (per foetus een onderzoek)
                        UPDATE kgc_im_onderzoeken imon
                           SET impw_id_rela = v_impw_id_rela
                         WHERE imon.imfi_id = v_imfi_id
                           AND imon.imon_id LIKE v_parent_id || '.%'
                           AND (imon.kgc_onde_id IS NULL OR nvl(imon.afgerond
                                                               ,'N') = 'N') -- 10-1-2007 RDE: verwerkt onderzoeken van vroeger met rust laten!
                        ;
                     END IF;
                  END IF;
                  v_sql := NULL; -- aktie is al uitgevoerd
               ELSIF (lower(v_strnn) = 'amniocentesis' AND v_bVWP_verwacht)
                     OR (lower(v_strnn) = 'cvs' AND v_bVLK_verwacht)
                     OR (lower(v_strnn) = 'fbs' AND v_bNSP_verwacht)
               THEN
                  -- punctiegegevens > monster
                  kgc_import.show_output(v_strnn || ' met id ' || v_record_id);
                  -- via foetus proberen monster met Amnion op te halen
                  -- mazzel: parent = fetus, dus in v_parent_id staat foetus-id (imfo_id)
                  v_hoeveelheid_monster := getdata(v_xmlnldata
                                                  ,'sample');
                  IF nvl(length(v_hoeveelheid_monster)
                        ,0) > 0
                  THEN
                     -- ALLEEN DAN een monster toevoegen!!!
                     IF lower(v_strnn) = 'amniocentesis' THEN
                       v_bVWP_verwerkt := TRUE;
                     ELSIF lower(v_strnn) = 'cvs' THEN
                       v_bVLK_verwerkt := TRUE;
                     ELSIF lower(v_strnn) = 'fbs' THEN
                       v_bNSP_verwerkt := TRUE;
                     END IF;
                     v_aantal_buizen := NULL;
                     IF lower(v_strnn) = 'amniocentesis'
                     THEN
                        -- 6-3-2007 RDE V6: altijd 1
                        v_aantal_buizen := 1;
                     END IF;

                     v_impw_id_cond := NULL;
                     IF nvl(length(getdata(v_xmlnldata
                                          ,'quality'))
                           ,0) > 0
                     THEN
                        v_impw_id_cond := kgc_import.getimpwid(p_imlo_id
                                                              ,kgc_import.g_cimpw_tab_cond
                                                              ,getdata(v_xmlnldata
                                                                      ,'quality')
                                                              ,'Helix monsterconditie'
                                                              ,TRUE);
                     END IF;
                     -- exam-gegevens ophalen mbv v_XMLnldataPunctieExam
                     -- onderzoeksgroep bepalen
                     v_impw_id_ongr := NULL;
                     IF (v_impw_id_ongr IS NULL)
                     THEN
                        v_impw_id_ongr := kgc_import.getimpwid(p_imlo_id
                                                              ,kgc_import.g_cimpw_tab_ongr
                                                              ,'Monsterafname'
                                                              ,NULL
                                                              ,TRUE);
                     END IF;
                     -- materiaal bepalen
                     v_punctie_mat := 'Onbekend';
                     IF lower(v_strnn) = 'amniocentesis'
                     THEN
                        v_punctie_mat := 'Amnion';
                     ELSIF lower(v_strnn) = 'cvs'
                     THEN
                        v_punctie_mat := 'Chorion';
                     ELSIF lower(v_strnn) = 'fbs'
                     THEN
                        v_punctie_mat := 'Navelstreng';
                     END IF;
                     v_impw_id_mate := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_mate
                                                           ,v_punctie_mat
                                                           ,'Materiaalcode voor ' || v_punctie_mat
                                                           ,TRUE);
                     -- inzender bepalen; de operator
                     v_impw_id_rela := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_rela
                                                           ,getdata(v_xmlnldatapunctieexam
                                                                   ,'us_operator')
                                                           ,'Uitvoerder monstername punctie/vlok'
                                                           ,TRUE);
                     -- herkomst
                     v_impw_id_herk := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_herk
                                                           ,kgc_import.g_cimpw_dummie
                                                           ,'Herkomst gegevens'
                                                           ,TRUE);

                     -- voor welke foetus? v_parent_id!
                     -- v_record_id van amniocentesis/cvs/fbs moet NIET gebruikt worden: niet uniek
                     -- het record-id van het exam moet gebruikt worden!
                     -- daarnaast kan er voor het exam, per foetus, ieder van de 3 voorkomen!
                     v_immo_id := v_punctieexam_record_id || '.' || v_parent_id || '.' ||
                                  substr(lower(v_strnn)
                                        ,1
                                        ,3);
                     /* ########## rde 12-10-2009 mantis 2870
                     onderstaande is niet meer nodig: er dient geen cyto-monster meer aangemaakt te worden
                     mogelijk tijdelijk, dus blijft het staan als commentaar!
                     OPEN c_immo_check(v_imfi_id
                                      ,v_immo_id);
                     FETCH c_immo_check
                        INTO r_immo_check;
                     IF c_immo_check%NOTFOUND
                     THEN
                        -- toevoegen
                        v_sql := 'INSERT INTO kgc_im_monsters ' || '( imfi_id ' || ', immo_id ' || ', impw_id_ongr ' ||
                                 ', impe_id ' || ', impw_id_mate ' || ', impw_id_rela ' || ', datum_afname ' ||
                                 ', imfo_id ' -- foetus
                                 || ', impw_id_herk ' || ', aantal_buizen ' || ', hoeveelheid_monster ' ||
                                 ', impw_id_cond ' || ') VALUES ' || '( ' || to_char(v_imfi_id) || ', ''' || v_immo_id || '''' ||
                                 ', ''' || v_impw_id_ongr || '''' || ', ''' || v_impe_id || '''' || ', ''' ||
                                 v_impw_id_mate || '''' || ', ''' || v_impw_id_rela || '''' || ', ''' ||
                                 kgc_import.datumformaat(getdata(v_xmlnldatapunctieexam
                                                                ,'us_date')) || '''' || ', ''' || v_parent_id || '''' ||
                                 ', ''' || v_impw_id_herk || '''' || ', ''' || v_aantal_buizen || '''' || ', ''' ||
                                 v_hoeveelheid_monster || '''' || ', ''' || v_impw_id_cond || '''' || ')';
                     ELSE
                        -- just checking
                        IF r_immo_check.impe_id != v_impe_id
                        THEN
                           raise_application_error(-20000
                                                  ,'Error in ' || v_strxmldirfile || ': Onderzoek met id ' ||
                                                   v_record_id || ' verwijst niet naar juiste persoon (is id ' ||
                                                   v_parent_id || ', moet zijn ' || v_impe_id || ')!');
                        END IF;
                        -- indien eerder toegevoegd, maar met ander materiaal: negeren
                        -- technisch is het mogelijk om bij een ingreep zowel een amniocentesis als een cvs bij
                        -- een foetus op te nemen. Logisch niet.
                        IF r_immo_check.impw_id_mate = v_impw_id_mate
                        THEN
                           -- WEL aanpassingen igv reeds bestaand monster: het evt een verbeterde 2e poging!
                           v_sql := 'UPDATE kgc_im_monsters ' || ' SET impw_id_ongr = ''' || v_impw_id_ongr || '''' ||
                                    ',    impe_id      = ''' || v_impe_id || ''''
                                   --                    || ',    impw_id_mate      = ''' || v_impw_id_mate || '''' dit wordt al afgedwongen
                                    || ',    impw_id_rela      = ''' || v_impw_id_rela || '''' ||
                                    ',    datum_afname      = ''' ||
                                    kgc_import.datumformaat(getdata(v_xmlnldatapunctieexam
                                                                   ,'us_date')) || '''' || ',    imfo_id      = ''' ||
                                    v_parent_id || '''' || ',    impw_id_herk      = ''' || v_impw_id_herk || '''' ||
                                    ',    aantal_buizen      = ''' || v_aantal_buizen || '''' ||
                                    ',    hoeveelheid_monster      = ''' || v_aantal_buizen || '''' ||
                                    ',    impw_id_cond      = ''' || v_impw_id_cond || '''' || ' WHERE  imfi_id = ' ||
                                    to_char(v_imfi_id) || ' AND    immo_id = ''' || v_immo_id || '''';
                        ELSE
                           v_sql := NULL; -- niets doen
                        END IF;
                     END IF;
                     IF nvl(length(v_sql)
                           ,0) > 0
                     THEN
                        BEGIN
                           kgc_util_00.dyn_exec(v_sql);
                        END;
                     END IF;
                     v_sql := NULL; -- aktie is al uitgevoerd
                     CLOSE c_immo_check;
                     ########## einde rde 12-10-2009 mantis 2870
                     */
                     -- *** verrichting toevoegen/wijzigen (afname)
                     -- onderzoeksgroep bepalen
                     v_impw_id_ongr := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_ongr
                                                           ,'Onderzoek'
                                                           ,NULL
                                                           ,TRUE);
                     -- onderzoekswijze bepalen
                     v_impw_id_onderzoekswijze := kgc_import.getimpwid(p_imlo_id
                                                                      ,kgc_import.g_cimpw_tab_onwy
                                                                      ,v_punctie_mat
                                                                      ,'Onderzoekswijze afname ' || v_punctie_mat
                                                                      ,TRUE);
                     -- instelling
                     v_impw_id_inst := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_inst
                                                           ,kgc_import.g_cimpw_dummie
                                                           ,'Instelling'
                                                           ,TRUE);
                     -- autorisator
                     v_impw_id_autorisator := kgc_import.getimpwid(p_imlo_id
                                                                  ,kgc_import.g_cimpw_tab_mede
                                                                  ,'Autorisator afname' -- altijd 1 per locatie!
                                                                  ,'Autorisator afname'
                                                                  ,TRUE);
                     -- verwijzer/aanvrager: vast; wordt evt nog overschreven door 'cdoctor'
                     v_impw_id_rela := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_rela
                                                           ,'Interne verwijzer afname' -- altijd 1 per locatie!
                                                           ,'Interne verwijzer afname'
                                                           ,TRUE);
                     --
                     v_imon_id := v_punctieexam_record_id || '.' || v_parent_id || '.' ||
                                  substr(lower(v_strnn)
                                        ,1
                                        ,3);
                     -- toevoeging 'substr' moet, want mogelijk is er al een ulg-onderzoek met het 1e deel van het id
                     OPEN c_imon_check(v_imfi_id
                                      ,v_imon_id);
                     FETCH c_imon_check
                        INTO r_imon_check;
                     IF c_imon_check%NOTFOUND
                     THEN
                        -- toevoegen
                        v_sql := 'INSERT INTO kgc_im_onderzoeken ' || '( imfi_id ' || ', imon_id ' || ', impw_id_ongr ' ||
                                 ', impe_id ' || ', impw_id_onderzoekswijze ' || ', afgerond ' || ', imfo_id ' -- foetus
                                 || ', datum_binnen ' || ', impw_id_herk ' || ', impw_id_inst ' ||
                                 ', impw_id_autorisator ' || ', datum_autorisatie ' || ', impw_id_rela ' || ') VALUES ' || '( ' ||
                                 to_char(v_imfi_id) || ', ''' || v_imon_id || '''' || ', ''' || v_impw_id_ongr || '''' ||
                                 ', ''' || v_impe_id || '''' || ', ''' || v_impw_id_onderzoekswijze || '''' ||
                                 ', ''N''' -- is standaard NIET afgerond - tenzij indicatie ok!
                                 || ', ''' || v_parent_id || '''' || ', ''' ||
                                 kgc_import.datumformaat(getdata(v_xmlnldatapunctieexam
                                                                ,'us_date')) || '''' || ', ''' || v_impw_id_herk || '''' ||
                                 ', ''' || v_impw_id_inst || '''' || ', ''' || v_impw_id_autorisator || '''' || ', ''' ||
                                 kgc_import.datumformaat(getdata(v_xmlnldatapunctieexam
                                                                ,'us_date')) || '''' || ', ''' || v_impw_id_rela || '''' || ')';
                     ELSE
                        -- just checking
                        IF r_imon_check.impe_id != v_impe_id
                        THEN
                           raise_application_error(-20000
                                                  ,'Error in ' || v_strxmldirfile || ': Onderzoek met id ' ||
                                                   v_record_id || ' verwijst niet naar juiste persoon (is id ' ||
                                                   v_parent_id || ', moet zijn ' || v_impe_id || ')!');
                        END IF;
                        -- WEL aanpassingen igv reeds bestaand onderzoek: het evt een verbeterde 2e poging!
                        v_sql := 'UPDATE kgc_im_onderzoeken ' || ' SET impw_id_ongr = ''' || v_impw_id_ongr || '''' ||
                                 ',    impe_id      = ''' || v_impe_id || '''' || ',    impw_id_onderzoekswijze = ''' ||
                                 v_impw_id_onderzoekswijze || '''' || ',    afgerond     = ''N''' ||
                                 ',    imfo_id      = ''' || v_parent_id || '''' || ',    datum_binnen = ''' ||
                                 kgc_import.datumformaat(getdata(v_xmlnldatapunctieexam
                                                                ,'us_date')) || '''' || ',    impw_id_herk = ''' ||
                                 v_impw_id_herk || '''' || ',    impw_id_inst = ''' || v_impw_id_inst || '''' ||
                                 ',    impw_id_autorisator = ''' || v_impw_id_autorisator || '''' ||
                                 ',    datum_autorisatie =''' ||
                                 kgc_import.datumformaat(getdata(v_xmlnldatapunctieexam
                                                                ,'us_date')) || '''' || ',    impw_id_rela      = ''' ||
                                 v_impw_id_rela || '''' || ' WHERE  imfi_id = ' || to_char(v_imfi_id) ||
                                 ' AND    imon_id = ''' || v_imon_id || '''';
                        IF v_hersteljob
                        THEN
                           -- even geen updates doen!
                           v_sql := NULL;
                        END IF;
                     END IF;
                     IF nvl(length(v_sql)
                           ,0) > 0
                     THEN
                        BEGIN
                           kgc_util_00.dyn_exec(v_sql);
                        END;
                     END IF;
                     v_sql := NULL;
                     CLOSE c_imon_check;
                     -- de indicatie van de afname staat in exam.invasive_indication!
                     -- deze is reeds in v_afname_indicatie gezet.
                     -- CDATA erafhalen: mogelijk is de inhoud '![CDATA[Risicoberekendende test geeft een verhoogd risico op chromosomale afwijkingen]]'

                     v_impw_id_indi := kgc_import.getimpwid(p_imlo_id
                                                           ,kgc_import.g_cimpw_tab_indi
                                                           ,v_afname_indicatie
                                                           ,NULL
                                                           ,TRUE);

                     IF v_impw_id_indi IS NOT NULL
                     THEN
                        voegtoe_imoi(p_imfi_id        => v_imfi_id
                                    ,p_imoi_id        => v_imon_id || '_1'
                                    ,p_imon_id        => v_imon_id
                                    ,p_volgorde       => 1
                                    ,p_impw_id_indi   => v_impw_id_indi
                                    ,p_impw_id_ingr   => NULL
                                    ,p_imoi_opmerking => NULL);
                     END IF;
                     -- probeer im_onderzoek op afgerond te zetten
                     bepaal_imon_afgerond(p_imfi_id        => v_imfi_id
                                         ,p_imoi_parent_id => v_punctieexam_record_id);
                  END IF;
               ELSIF lower(v_strnn) = 'insurance'
               THEN
                  -- Bah. in demographics zit naast verwijzing insurance_company ook tekstveld insurance
                  -- en mogelijk zijn er meerdere insurance_companies! Bah
                  -- Beide proberen
                  kgc_import.show_output('insurance met id ' || v_record_id);
                  -- oei oei - in Astraia een fout: v_record_id is bv 34192482, terwijl v_insurance_record_id 3.4192482E7 kan zijn!
                  v_bgelijk := FALSE;
                  IF v_insurance_record_id = v_record_id
                  THEN
                     v_bgelijk := TRUE;
                  ELSE
                     -- nieuwe poging
                     BEGIN
                        IF to_char(to_number(v_insurance_record_id)) = v_record_id
                        THEN
                           v_bgelijk := TRUE;
                        END IF;
                     EXCEPTION
                        WHEN OTHERS THEN
                           NULL;
                     END;
                  END IF;
                  IF v_bgelijk
                  THEN
                     -- de goede te pakken; gezet bij demographics
                     -- hint opbouwen
                     v_impw_hint := NULL;
                     kgc_import.addtext(v_impw_hint
                                       ,2000
                                       ,'Astraia-verzekeraar');
                     kgc_import.addtext(v_impw_hint
                                       ,2000
                                       ,'Naam: ' || getdata(v_xmlnldata
                                                           ,'company'));
                     kgc_import.addtext(v_impw_hint
                                       ,2000
                                       ,'Adres: ' || getdata(v_xmlnldata
                                                            ,'address'));
                     kgc_import.addtext(v_impw_hint
                                       ,2000
                                       ,'Postcode: ' || getdata(v_xmlnldata
                                                               ,'postcode'));
                     kgc_import.addtext(v_impw_hint
                                       ,2000
                                       ,'Plaats: ' || getdata(v_xmlnldata
                                                             ,'town'));
                     -- als v_impw_id_verz leeg is, dan nieuwe toevoegen
                     -- anders kijken of identieke naam; zo ja: hint bijwerken
                     IF v_impw_id_verz IS NULL
                     THEN
                        v_impw_id_verz := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                              ,p_helix_table_domain => kgc_import.g_cimpw_tab_verz
                                                              ,p_externe_id         => getdata(v_xmlnldata
                                                                                              ,'company')
                                                              ,p_hint               => v_impw_hint
                                                              ,p_toevoegen          => TRUE);
                        -- aanbrengen bij kgc_im_personen in impw_id_verz
                        UPDATE kgc_im_personen
                           SET impw_id_verz = v_impw_id_verz
                         WHERE imfi_id = v_imfi_id
                           AND impe_id = v_impe_id;
                     ELSE
                        -- hint bijwerken; niet nieuw toevoegen
                        -- in Nijmegen-Astraia lijkt het zo te zijn dat demographic.insurance gelijk is
                        -- aan insurance.company. Dan werkt het onderstaande. In de toekomst niet gelijk > dan geen ramp: wordt niet bijgewerkt!
                        v_impw_id_verz := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                              ,p_helix_table_domain => kgc_import.g_cimpw_tab_verz
                                                              ,p_externe_id         => getdata(v_xmlnldata
                                                                                              ,'company')
                                                              ,p_hint               => v_impw_hint
                                                              ,p_toevoegen          => FALSE);
                     END IF;
                  END IF;
                  v_sql := NULL; -- aktie is al uitgevoerd
               ELSIF lower(v_strnn) = 'partner'
               THEN
                  -- in persoon zit name_partner, maar deze wordt voor 'aanspreken' gebruikt!
                  kgc_import.show_output('partner met id ' || v_record_id);
                  v_partner_name := getdata(v_xmlnldata
                                           ,'name');
                  -- V7: verwerking wordt gedaan na de loop (partner-segment is niet altijd aanwezig!!!)
               ELSE
                  dbms_output.put_line(xmldom.getnodename(v_xmln1));
               END IF;
               IF nvl(length(v_sql)
                     ,0) > 0
               THEN
                  BEGIN
                     kgc_util_00.dyn_exec(v_sql);
                  END;
               END IF;
            END LOOP;
            IF v_error
            THEN -- rde 12-08-2009 Mantis 3071
               EXIT;
            END IF;
         END LOOP;
         IF v_error
         THEN -- rde 12-08-2009 Mantis 3071
            EXIT;
         END IF;
      END LOOP; -- tbv volgorde
      -- V7: kijk of partner / aanspreekgegevens aangepast moeten worden
      -- op basis van v_patient_name_partner / v_partner_name
      -- igv nijmegen: v_patient_name_partner bevat aanspreeknaam; wordt gebruikt voor bepaling std-aanspreken
      --               v_partner_name bevat de partnernaam
      -- anders: partnernaam wordt v_partner_name; indien leeg, dan v_patient_name_partner
      --         indien bovenstaande partnernaam gevuld is, dan gebruiken voor aanspreken (en dus: gehuwd!)
      IF v_imfi_id IS NOT NULL
         AND v_impe_id IS NOT NULL
      THEN
         -- persoon ophalen
         OPEN c_impe(v_imfi_id
                    ,v_impe_id);
         FETCH c_impe
            INTO r_impe; -- net toegevoegd, dus altijd gevonden!
         CLOSE c_impe;
         -- gehuwd / default_aanspreken proberen te bepalen
         -- en igv NIET-default aanspreken ook proberen aanspreken te bepalen
         -- dus alvast wat formatering uitvoeren:
         r_impe.voorletters := substr(r_impe.voorletters
                                     ,1
                                     ,30); -- substring character is changed from 10 to 30, mantis 0182, saroj
         r_impe.achternaam  := substr(r_impe.achternaam
                                     ,1
                                     ,30);
         kgc_formaat_00.formatteer(p_naamdeel => 'ACHTERNAAM'
                                  ,x_naam     => r_impe.achternaam);
         IF instr(r_imlo.directorynaam
                 ,'/RZN') > 0
         THEN
            -- Nijmegen
            v_pers_zis_aanspreken := v_patient_name_partner;
            -- v_partner_name is al ok
         ELSE
            v_partner_name        := TRIM(nvl(v_partner_name
                                             ,v_patient_name_partner));
            v_pers_zis_aanspreken := v_partner_name || ' ' || r_impe.achternaam; -- puur ter bepaling std aanspreken!
         END IF;
         -- splits naam in 2
         splitsname(v_partner_name
                   ,v_pers_achternaam_partner
                   ,v_pers_voorvoegsel_partner);
         v_pers_voorvoegsel_partner := lower(v_pers_voorvoegsel_partner); -- bah!
         -- nog meer formatering uitvoeren:
         -- voorletters niet formatteren (soms voornaam!), achternaam std, vv naar lower!
         v_pers_achternaam_partner := substr(v_pers_achternaam_partner
                                            ,1
                                            ,30);
         kgc_formaat_00.formatteer(p_naamdeel => 'ACHTERNAAM'
                                  ,x_naam     => v_pers_achternaam_partner);
         r_impe.voorvoegsel         := substr(r_impe.voorvoegsel
                                             ,1
                                             ,10);
         v_pers_voorvoegsel_partner := substr(v_pers_voorvoegsel_partner
                                             ,1
                                             ,10);
         kgc_interface.standaard_aanspreken_zis(p_voorletters         => r_impe.voorletters
                                               ,p_voorvoegsel         => r_impe.voorvoegsel
                                               ,p_achternaam          => r_impe.achternaam
                                               ,p_achternaam_partner  => v_pers_achternaam_partner
                                               ,p_voorvoegsel_partner => v_pers_voorvoegsel_partner
                                               ,p_zis_aanspreeknaam   => v_pers_zis_aanspreken
                                               ,p_geslacht            => 'V' -- kgc_im_personen is toegevoegd met v_impw_id_geslacht voor 'V'
                                               ,x_default_aanspreken  => r_impe.default_aanspreken
                                               ,x_gehuwd              => r_impe.gehuwd
                                               ,x_aanspreken          => r_impe.aanspreken);
         UPDATE kgc_im_personen
            SET achternaam_partner  = v_pers_achternaam_partner
               ,voorvoegsel_partner = v_pers_voorvoegsel_partner
               ,gehuwd              = r_impe.gehuwd
               ,default_aanspreken  = r_impe.default_aanspreken
               ,aanspreken          = r_impe.aanspreken
          WHERE imfi_id = v_imfi_id
            AND impe_id = v_impe_id;
         -- eind V7.
      END IF;
      xmldom.freedocument(v_xmldoc);
      xmlparser.freeparser(v_xmlp);
      -- laatste test: is er een monster OF declarabel onderzoek toegevoegd?
      IF v_error
      THEN -- rde 12-08-2009 Mantis 3071
         -- melding is reeds gevuld
         ROLLBACK;
      ELSIF v_bProcedure_dubbel THEN
         v_imfi_opmerkingen := v_pers_achternaam || ' - Voor deze persoon (' || v_impe_id || ' ' || v_pers_achternaam || ' ' ||
                               v_pers_voorvoegsel ||
                               ') is een dubbele productcode aangetroffen: ''' || v_procedure_name || '''; corrigeer astraia en valideer opnieuw';
         ROLLBACK;
      ELSIF (NOT v_bUG1_verwacht) AND (NOT v_bUG2_verwacht) AND (NOT v_bVWP_verwacht) AND (NOT v_bVLK_verwacht) AND (NOT v_bNSP_verwacht)
      THEN
         -- achternaam vooraan: tbv zoeken
         v_imfi_opmerkingen := v_pers_achternaam || ' - Voor deze persoon (' || v_impe_id || ' ' || v_pers_achternaam || ' ' ||
                               v_pers_voorvoegsel ||
                               ') zijn geen relevante gegevens (ingreep: vruchtwaterpuntie, vlokkentest of navelstrengpunctie; of een declarabel Ultrageluidsonderzoek (type I of II)) gevonden!';
         ROLLBACK;
      ELSIF (v_bVWP_verwacht AND NOT v_bVWP_verwerkt) OR
            (v_bVLK_verwacht AND NOT v_bVLK_verwerkt) OR
            (v_bNSP_verwacht AND NOT v_bNSP_verwerkt)
      THEN
         -- achternaam vooraan: tbv zoeken
         v_imfi_opmerkingen := v_pers_achternaam || ' - Voor deze persoon (' || v_impe_id || ' ' || v_pers_achternaam || ' ' ||
                               v_pers_voorvoegsel ||
                               ') is een procedurecode met betekenis ';
         IF (v_bVWP_verwacht AND NOT v_bVWP_verwerkt) THEN
           v_imfi_opmerkingen := v_imfi_opmerkingen || '''Vruchtwaterpunctie''';
         ELSIF (v_bVLK_verwacht AND NOT v_bVLK_verwerkt) THEN
           v_imfi_opmerkingen := v_imfi_opmerkingen || '''Vlokkentest''';
         ELSIF (v_bNSP_verwacht AND NOT v_bNSP_verwerkt) THEN
           v_imfi_opmerkingen := v_imfi_opmerkingen || '''Navelstrengbloed''';
         END IF;
         v_imfi_opmerkingen := v_imfi_opmerkingen || ' opgegeven, maar er is geen segment tbv vruchtwaterpunctie, vlokkentest of navelstrengpunctie gevonden! De verwerking is gestopt!';
         ROLLBACK;
      ELSE
         v_imfi_opmerkingen := 'OK - Bestand succesvol ingelezen!'; -- let op: in verwerk_bestanden wordt getest op de tekst "OK"!!!
      END IF;
      UPDATE kgc_import_files imfi
         SET opmerkingen = v_imfi_opmerkingen
            ,afgehandeld = 'N' -- de aanroeper bepaalt dit
      --tbv test!!!  ,      inhoud = NULL -- CLOB leegmaken!
       WHERE imfi.imfi_id = v_imfi_id;
      kgc_import.show_output('Klaar - geen fouten');
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         IF NOT xmldom.isnull(v_xmldoc)
         THEN
            xmldom.freedocument(v_xmldoc);
            xmlparser.freeparser(v_xmlp);
         END IF;
         v_str := SQLCODE;
         v_str := v_str || ' ' || substr(SQLERRM
                                        ,1
                                        ,1800);
         ROLLBACK;
         kgc_import.show_output('Error others: ' || v_str);
         UPDATE kgc_import_files imfi
            SET opmerkingen = substr(v_str
                                    ,1
                                    ,2000)
          WHERE imfi.imfi_id = v_imfi_id;
         COMMIT;
         raise_application_error(-20000
                                ,substr(v_str
                                       ,1
                                       ,255));
   END lees_in_astraia;

   --
   -- er is geen gevaar dat deze twee keer parallel gaat draaien: pas als de job klaar
   -- is, wordt de volgende gescheduled!!!
   PROCEDURE verwerk_bestanden IS
      v_unpw            VARCHAR2(200);
      v_msg             VARCHAR2(32767);
      v_errormessage    VARCHAR2(32767);
      v_ftperrormessage VARCHAR2(32767);
      v_ftpdirfiles     kgc_ftp.string_table;
      v_ftpremotedir    VARCHAR2(4000);
      v_ftpservername   VARCHAR2(200);
      v_ftpusername     VARCHAR2(100) := 'anonymus';
      v_ftppassword     VARCHAR2(100) := 'helix@umcn.nl';
      v_ftpfilename     VARCHAR2(4000);
      --    v_ftpDummyKey      NUMBER;
      v_ftppos           NUMBER;
      i                  NUMBER;
      v_imfi_opmerkingen VARCHAR2(32000);
      CURSOR c_imlo_cur IS
         SELECT imlo_id
               ,directorynaam
           FROM kgc_import_locaties
          WHERE UPPER(directorynaam) like 'FTP%'; -- rde 12-08-2009 Mantis 3071
      --    v_directorynaam VARCHAR2(2000);
      CURSOR imfi_cur(b_imlo_id IN NUMBER, b_filenaam IN VARCHAR2) IS
         SELECT imfi_id
         ,      opmerkingen
           FROM kgc_import_files
          WHERE imlo_id = b_imlo_id
            AND filenaam = b_filenaam;
      v_imfi_id NUMBER;
   BEGIN
      -- dbms_session.set_sql_trace( true ); -- 3* ************************************* TESTTESTTEST
      -- un pw ophalen
      v_unpw   := kgc_sypa_00.systeem_waarde(p_parameter_code => 'ASTRAIA_FTP_UN_PW');
      v_ftppos := instr(v_unpw
                       ,'/');
      IF v_ftppos > 0
      THEN
         v_ftpusername := substr(v_unpw
                                ,1
                                ,v_ftppos - 1);
         v_ftppassword := substr(v_unpw
                                ,v_ftppos + 1);
      END IF;
      FOR r IN c_imlo_cur
      LOOP
         -- get list of files;
         v_ftpremotedir := lower(r.directorynaam);
         v_ftpremotedir := REPLACE(v_ftpremotedir
                                  ,'ftp://'
                                  ,NULL);
         v_ftpremotedir := REPLACE(v_ftpremotedir
                                  ,'\'
                                  ,'/');
         v_ftpremotedir := rtrim(v_ftpremotedir
                                ,'/');
         v_ftppos       := instr(v_ftpremotedir
                                ,'/'); -- scheiding server en dir
         IF (v_ftppos > 0)
         THEN
            v_ftpservername := substr(v_ftpremotedir
                                     ,1
                                     ,v_ftppos - 1);
            v_ftpremotedir  := substr(v_ftpremotedir
                                     ,v_ftppos);
         END IF;
         kgc_import.show_output('vertaal_naar_helix - KGC_FTP.FTPDir - bestandenlijst ophalen: S:' || v_ftpservername ||
                                ' - UN: ' || v_ftpusername || ' - PW:' || v_ftppassword || ' RemDir:' ||
                                v_ftpremotedir);
         v_ftperrormessage := NULL;
         v_ftperrormessage := kgc_ftp.ftpdir(p_servername => v_ftpservername
                                            ,p_port       => '21'
                                            ,p_username   => v_ftpusername
                                            ,p_password   => v_ftppassword
                                            ,p_remotedir  => v_ftpremotedir
                                            ,p_dirlist    => v_ftpdirfiles);
         IF (nvl(length(v_ftperrormessage)
                ,0) > 0)
         THEN
            RAISE value_error;
         END IF;
         --    dbms_output.put_line('Aantal gevonden bestanden: ' || TO_CHAR(v_ftpDirFiles.COUNT));
         FOR i IN 1 .. v_ftpdirfiles.COUNT
         LOOP
            -- v_ftpDirFiles(I) bevat iets als
            --    '01-28-04  02:23PM       <DIR>          test'
            -- of '01-28-04  04:05PM                 6029 RZN_patient1.xml'
            v_ftpfilename := ltrim(rtrim(substr(v_ftpdirfiles(i)
                                               ,40)));
            v_ftpfilename := rtrim(v_ftpfilename
                                  ,chr(10));
            v_ftpfilename := rtrim(v_ftpfilename
                                  ,chr(13));
            --      dbms_output.put_line(v_ftpFileName);
            -- alleen *.xml lezen > geen andere of SUBDIRECTORIES!
            v_imfi_id := NULL;
            v_imfi_opmerkingen := NULL;
            IF instr(lower(v_ftpfilename)
                    ,'.xml') > 0
            THEN
               -- inlezen
               lees_in_astraia(p_imlo_id  => r.imlo_id
                              ,p_filenaam => v_ftpfilename);
               OPEN imfi_cur(r.imlo_id
                            ,v_ftpfilename);
               FETCH imfi_cur
                  INTO v_imfi_id, v_imfi_opmerkingen;
               CLOSE imfi_cur;
               -- bronbestand verwijderen
               v_ftperrormessage := v_ftperrormessage ||
                                    kgc_ftp.ftpdelete(p_servername => v_ftpservername
                                                     ,p_port       => '21'
                                                     ,p_username   => v_ftpusername
                                                     ,p_password   => v_ftppassword
                                                     ,p_sourcefile => v_ftpremotedir || '/' || v_ftpfilename);
               --        dbms_output.put_line('Verwijdering uitgevoerd / ' || v_ftpErrorMessage);
               IF (nvl(length(v_ftperrormessage)
                      ,0) > 0)
               THEN
                  RAISE value_error;
               END IF;
               -- naar helix proberen te vertalen
               -- maar ALLEEN indien de gegevens goed zijn ingelezen: lees_in_astraia zet OK... in 'opmerkingen; anders afblijven!
               -- RDE 7-5-2009 iom CVermaat
               IF (v_imfi_id IS NOT NULL) AND (SUBSTR(v_imfi_opmerkingen, 1, 2) = 'OK')
               THEN
                  kgc_import.vertaal_naar_helix(p_imfi_id => v_imfi_id
                                               ,p_online  => FALSE);
               END IF;
            END IF;
         END LOOP;
         v_ftpdirfiles.DELETE;
      END LOOP;
      COMMIT;
      -- dbms_session.set_sql_trace( false ); -- 3* ************************************* TESTTESTTEST
   EXCEPTION
      WHEN OTHERS THEN
         -- error aanvullen...
         -- RDE 7-5-2009 iom CVermaat
         v_errormessage := 'kgc_interface_astraia.verwerk_bestanden error: ';
         IF (nvl(length(v_ftperrormessage)
                ,0) > 0)
         THEN
            kgc_import.addtext(v_errormessage
                              ,32767
                              ,v_ftperrormessage);
         ELSE
            kgc_import.addtext(v_errormessage
                              ,32767
                              ,SQLERRM);
            v_msg := cg$errors.GetErrors;
            IF nvl(length(v_msg), 0) > 0 THEN
               kgc_import.addtext(v_errormessage
                                 ,32767
                                 ,'cg$errors: ' || v_msg);
            END IF;
         END IF;

         -- RDE 7-5-2009 iom CVermaat: altijd de boodschap te mailen:
         kgc_zis.post_mail_zis('ASTRAIA_MAIL_ERR'
                              ,'kgc_interface_astraia.verwerk_bestanden: Fout (database: ' || upper(database_name) || ')!'
                              ,v_errormessage);

         --    kgc_import.show_output(v_ftpErrorMessage);
         --    kgc_import.show_output(v_imfi_opmerkingen);
         IF v_imfi_id IS NOT NULL
         THEN
            UPDATE kgc_import_files
               SET opmerkingen = SUBSTR(v_errormessage, 1, 2000)
             WHERE imfi_id = v_imfi_id;
            COMMIT;
         END IF;
         -- dbms_session.set_sql_trace( false ); -- 3* ************************************* TESTTESTTEST
   END verwerk_bestanden;
BEGIN
   -- package init
   NULL;
END kgc_interface_astraia;
/

/
QUIT
