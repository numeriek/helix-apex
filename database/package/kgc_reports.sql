CREATE OR REPLACE PACKAGE "HELIX"."KGC_REPORTS" IS
-- PL/SQL Specification
FUNCTION add_where
( p_column IN VARCHAR2
, p_value IN VARCHAR2
, p_datatype IN VARCHAR2 := 'C'
, p_operator IN VARCHAR2 := '='
)
RETURN VARCHAR2;
END KGC_REPORTS;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_REPORTS" IS
-- lokale procedures
FUNCTION wildcard
( p_string  IN  VARCHAR2
)
RETURN BOOLEAN
IS
-- PL/SQL Block
BEGIN
  IF ( INSTR( p_string, '%' ) > 0
    OR INSTR( p_string, '_' ) > 0
     )
  THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END wildcard;


FUNCTION add_where
( p_column IN VARCHAR2
, p_value IN VARCHAR2
, p_datatype IN VARCHAR2 := 'C'
, p_operator IN VARCHAR2 := '='
)
RETURN VARCHAR2
IS
  v_where VARCHAR2(1000);
  v_value VARCHAR2(1000) := p_value;
  v_operator VARCHAR2(10) := ' '||p_operator||' ';
BEGIN
  IF ( p_value IS NULL )
  THEN
    RETURN( NULL );
  END IF;
  IF ( wildcard( p_value )
   AND p_operator = '='
     )
  THEN
    v_operator := ' like ';
  END IF;
  --
  IF ( p_datatype = 'N' )
  THEN
    v_value := 'to_number('||p_value||')';
  ELSIF ( p_datatype = 'D' )
  THEN
    v_value := 'to_date('''||p_value||''',''DD-MM-YYYY'')';
  ELSE -- Characters
    v_value := ''''||p_value||'''';
  END IF;
  --
  v_where := ' and ('||p_column||v_operator||v_value||')';
  --
  RETURN( v_where );
END add_where;

END kgc_reports;
/

/
QUIT
