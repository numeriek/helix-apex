CREATE OR REPLACE PACKAGE "HELIX"."KGC_MONS_00" IS
      -- tellen hoeveel monster er nog beschikbaar is voor opwerken
   function tel_hoeveelheid(p_mons_id in number) return number;
   --
   -- als geboortedatum (persoon) of aterme datum (foetus) wijzigt
   -- wordt de leeftijd bij een monster anders,
   -- waardoor de normaalwaarden bij een aangemelde stoftest opnieuw
   -- berekent moeten worden
   procedure geboortedatum_wijzigt(p_pers_id in number
                                  ,p_foet_id in number := null
                                  ,p_datum   in date
                                  ,x_fout    out boolean
                                  ,x_melding out varchar2);
   --
   function meerdere_protocollen(p_mons_id in number) return varchar2;
   --
   pragma restrict_references(meerdere_protocollen
                             ,wnds
                             ,wnps
                             ,rnps);
   --
   procedure referentie_stoftest_details(p_mons_id     in kgc_monsters.mons_id%type
                                        ,x_stof        in out kgc_stoftesten.code%type
                                        ,x_meetwaarde  in out bas_meetwaarden.meetwaarde%type
                                        ,x_meeteenheid in out bas_meetwaarden.meeteenheid%type
                                        ,p_onde_id     in kgc_onderzoeken.onde_id%type); -- Mantis 1208
   --
   function meetwaarde_referentie_stoftest(p_mons_id in kgc_monsters.mons_id%type
                                          ,p_onde_id in kgc_onderzoeken.onde_id%type) return varchar2; -- Mantis 1208
   --
   function code_referentie_stoftest(p_mons_id in kgc_monsters.mons_id%type
                                    ,p_onde_id in kgc_onderzoeken.onde_id%type) return varchar2; -- Mantis 1208
   ---
   function klinische_info(p_mons_id in kgc_monsters.mons_id%type) return varchar2;
   ---
END KGC_MONS_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MONS_00" IS
   function tel_hoeveelheid(p_mons_id in number) return number is

      cursor zoek_begin_hoev is
         select hoeveelheid_monster
           from kgc_monsters
          where mons_id = p_mons_id;

      cursor zoek_hoev is
         select nvl(sum(hoeveelheid)
                   ,0)
           from bas_opwerken
          where mons_id = p_mons_id;

      v_hoev  number;
      v_begin number;
      v_over  number;

   begin

      open zoek_begin_hoev;
      fetch zoek_begin_hoev
         into v_begin;
      close zoek_begin_hoev;

      open zoek_hoev;
      fetch zoek_hoev
         into v_hoev;
      close zoek_hoev;

      v_over := v_begin - v_hoev;

      return(v_over);

   end tel_hoeveelheid;
   --
   procedure geboortedatum_wijzigt(p_pers_id in number
                                  ,p_foet_id in number := null
                                  ,p_datum   in date
                                  ,x_fout    out boolean
                                  ,x_melding out varchar2) is
      -- wijz. 2016-03-15 RDE: de for-update selectiever gedaan: alleen als er wat te updaten valt!
      -- Wijz. 2004-12-09 ESO: automatische update van monsters verplaatst
      --                                           naarBAS_RANG_00.normaalwaarden_wijzigen
      v_geboortedatum date;
      v_aterme_datum  date;

      cursor mons_cur is
         select mons.mons_id
               ,mons.kafd_id
               ,mons.pers_id
               ,pers.geslacht
               ,mons.foet_id
               ,mons.mate_id
               ,mons.leef_id
               ,mons.leeftijd
               ,nvl(mons.datum_afname
                   ,mons.datum_aanmelding) datum_afname
           from kgc_personen pers
               ,kgc_monsters mons
          where pers.pers_id = mons.pers_id
            and mons.pers_id = p_pers_id
            and nvl(mons.foet_id
                   ,-9999999999) = nvl(p_foet_id
                                      ,-9999999999)
--            for update of mons.leeftijd, mons.leef_id nowait rde 15-03-2016 zie mantis 12100
            ;
      cursor mons_upd_cur(p_mons_id NUMBER) is
         select mons.mons_id
               ,mons.leef_id
               ,mons.leeftijd
           from kgc_monsters mons
          where mons.mons_id = p_mons_id
            for update of mons.leeftijd, mons.leef_id nowait
            ;

      v_leeftijd number;
      v_leef_id  number;

   begin

      if (p_pers_id is null or p_datum is null)
      then
         return;
      end if;
      x_fout := false;
      if (p_foet_id is not null)
      then
         v_aterme_datum := p_datum;
      else
         v_geboortedatum := p_datum;
      end if;
      for mons_rec in mons_cur
      loop
         v_leeftijd := null;
         v_leef_id  := null;
         v_leeftijd := kgc_leef_00.leeftijd(p_pers_id       => mons_rec.pers_id
                                           ,p_foet_id       => mons_rec.foet_id
                                           ,p_datum_afname  => mons_rec.datum_afname
                                           ,p_geboortedatum => v_geboortedatum
                                           ,p_datum_aterm   => v_aterme_datum);
         if (nvl(v_leeftijd
                ,-9999999999) = nvl(mons_rec.leeftijd
                                    ,-9999999999))
         then
            null; -- geen wijzigingen
         else
            v_leef_id := kgc_leef_00.leeftijdscategorie(p_kafd_id      => mons_rec.kafd_id
                                                       ,p_aantal_dagen => v_leeftijd);
            if (v_leef_id is not null)
            then
               -- rde 15-03-2016 zie mantis 12100
               for mons_upd_rec in mons_upd_cur(mons_rec.mons_id) loop
                  update kgc_monsters
                     set leeftijd = v_leeftijd
                        ,leef_id  = v_leef_id
                   where current of mons_upd_cur;
               end loop;
            end if;
         end if;
      end loop;
      x_fout := false;

   exception
      when others then
         x_fout    := true;
         x_melding := sqlerrm;

   end geboortedatum_wijzigt;
   --
   function meerdere_protocollen(p_mons_id in number) return varchar2 is
      -- let op: metingen zonder protocol tellen niet mee!
      -- precieze telmethode moet nog worden bepaald!
      v_aantal number;

      cursor meti_cur is
         select count(distinct meti.stgr_id)
           from bas_fracties frac
               ,bas_metingen meti
          where meti.frac_id = frac.frac_id
            and frac.mons_id = p_mons_id;

   begin
      open meti_cur;
      fetch meti_cur
         into v_aantal;
      close meti_cur;
      if (nvl(v_aantal
             ,0) <= 1)
      then
         return('N');
      else
         return('J');
      end if;

   end meerdere_protocollen;

   procedure referentie_stoftest_details(p_mons_id     in kgc_monsters.mons_id%type
                                        ,x_stof        in out kgc_stoftesten.code%type
                                        ,x_meetwaarde  in out bas_meetwaarden.meetwaarde%type
                                        ,x_meeteenheid in out bas_meetwaarden.meeteenheid%type
                                        ,p_onde_id     in kgc_onderzoeken.onde_id%type) is

      cursor stof_cur is
         select kgc_uk2pk.stof_id(mons.kafd_id
                                 ,kgc_sypa_00.standaard_waarde('REFERENTIE_STOFTEST'
                                                              ,mons.kafd_id)) stof_id
               ,kgc_sypa_00.standaard_waarde('REFERENTIE_STOFTEST'
                                            ,mons.kafd_id) stof_code
           from kgc_monsters mons
          where mons.mons_id = p_mons_id;

      stof_rec stof_cur%rowtype;

      cursor meet_cur(b_stof_id in number, b_mons_id in number, b_onde_id in number) is
         select meet.meet_id
               ,meet.tonen_als
               ,meet.meeteenheid
           from -- kgc_stoftesten         stof -- Mantis 1208: zinloos, cart.prod.
                kgc_monsters           mons
               ,kgc_onderzoek_monsters onmo
               ,bas_metingen           meti
               ,bas_meetwaarden        meet
          where mons.mons_id = onmo.mons_id
            and onmo.onmo_id = meti.onmo_id
            and meti.meti_id = meet.meti_id
            and meet.stof_id = b_stof_id
            and mons.mons_id = b_mons_id
            and onmo.onde_id = b_onde_id; -- Mantis 1208

      meet_rec meet_cur%rowtype;

   begin

      if p_mons_id is not null
      then

         open stof_cur;
         fetch stof_cur
            into stof_rec;
         close stof_cur;

         x_stof := stof_rec.stof_code;

         begin

            open meet_cur(b_stof_id => stof_rec.stof_id
                         ,b_mons_id => p_mons_id
                         ,b_onde_id => p_onde_id); -- Mantis 1208
            fetch meet_cur
               into meet_rec;
            close meet_cur;

            x_meetwaarde  := meet_rec.tonen_als;
            x_meeteenheid := meet_rec.meeteenheid;

         exception

            when no_data_found then
               x_meetwaarde  := null;
               x_meeteenheid := null;

            when others then
               raise;

         end;

      end if;

   end referentie_stoftest_details;

   function meetwaarde_referentie_stoftest(p_mons_id in kgc_monsters.mons_id%type
                                          ,p_onde_id in kgc_onderzoeken.onde_id%type) return varchar2 is

      v_stof        kgc_stoftesten.code%type;
      v_meetwaarde  bas_meetwaarden.meetwaarde%type;
      v_meeteenheid bas_meetwaarden.meeteenheid%type;

   begin
      referentie_stoftest_details(p_mons_id     => p_mons_id
                                 ,x_stof        => v_stof
                                 ,x_meetwaarde  => v_meetwaarde
                                 ,x_meeteenheid => v_meeteenheid
                                 ,p_onde_id     => p_onde_id); -- Mantis 1208

      return v_meetwaarde;
   end;

   function code_referentie_stoftest(p_mons_id in kgc_monsters.mons_id%type
                                    ,p_onde_id in kgc_onderzoeken.onde_id%type) return varchar2 is

      v_stof        kgc_stoftesten.code%type;
      v_meetwaarde  bas_meetwaarden.meetwaarde%type;
      v_meeteenheid bas_meetwaarden.meeteenheid%type;

   begin
      referentie_stoftest_details(p_mons_id     => p_mons_id
                                 ,x_stof        => v_stof
                                 ,x_meetwaarde  => v_meetwaarde
                                 ,x_meeteenheid => v_meeteenheid
                                 ,p_onde_id     => p_onde_id); -- Mantis 1208

      return v_stof;
   end;

   function klinische_info(p_mons_id in kgc_monsters.mons_id%type) return varchar2 is

      cursor mons_cur(pp_mons_id in number) is
         select medische_indicatie
           from kgc_monsters
          where mons_id = pp_mons_id;
      --
      v_klinische_info varchar2(2000);

   begin
      open mons_cur(p_mons_id);
      fetch mons_cur
         into v_klinische_info;
      close mons_cur;

      return v_klinische_info;
   end;
end kgc_mons_00;
/

/
QUIT
