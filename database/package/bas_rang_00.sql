CREATE OR REPLACE PACKAGE "HELIX"."BAS_RANG_00" IS
/******************************************************************************
      NAME:       bas_rang_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.2        31-03-2016   Athar Shaikh      Mantis 3195: Enzym bijlage brief: normaalwaarden  met de juiste aantallen  decimalen tonen
                                                In function normaalwaarden added parameter p_aantal_decemalen
    ****************************************************************************** */
-- Package Specification
--
-- bereken de normaalwaarden ter vergelijking met meetwaarden
-- van een bepaalde meting
PROCEDURE normaalwaarden
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
, x_onder OUT NUMBER
, x_boven OUT NUMBER
, x_gemiddeld OUT NUMBER
);

PROCEDURE doorvoeren_range
( p_stof_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_wijzig IN VARCHAR2 := 'J' -- N=bestaande normaalwaarden blijven gehandhaafd
);

-- retourneer de normaalwaarden als opgemaakte string
FUNCTION normaalwaarden
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
, p_aantal_decemalen IN NUMBER := NULL -- Added by athar shaikh for Mantis-3195 on 31-03-2016
)
RETURN VARCHAR2;

-- retourneer de lage waarde
FUNCTION laag
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
-- retourneer de hoge waarde
FUNCTION hoog
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
-- retourneer de gemiddelde waarde
FUNCTION gemiddeld
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
-- Normaalwaarders wijzigen:
PROCEDURE normaalwaarden_wijzigen
( p_mons_id  IN NUMBER := NULL
, p_kafd_id  IN NUMBER := NULL
, p_mate_id  IN NUMBER := NULL
, p_leeftijd IN NUMBER := NULL
, p_leef_id  IN NUMBER := NULL
, p_pers_id  IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
, p_frac_id  IN NUMBER := NULL
, p_frty_id  IN NUMBER := NULL
, p_entiteit IN VARCHAR2 := NULL
, x_fout    OUT BOOLEAN
, x_melding OUT VARCHAR2
);
--
END BAS_RANG_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_RANG_00" IS
 /******************************************************************************
      NAME:       bas_rang_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.2        31-03-2016   Athar Shaikh      Mantis 3195: Enzym bijlage brief: normaalwaarden  met de juiste aantallen  decimalen tonen
                                                In function normaalwaarden added parameter p_aantal_decemalen
      1.1        01-03-2016   SKA               Mantis 9355 TO_CHAR added to  Conclusie column release 8.11.0.2
    ****************************************************************************** */
PROCEDURE normaalwaarden
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
, x_onder OUT NUMBER
, x_boven OUT NUMBER
, x_gemiddeld OUT NUMBER
)
IS
  CURSOR rang_cur
  IS
    SELECT rang.laag onder
    ,      rang.hoog boven
    ,      rang.gemiddeld gemiddeld
    ,      ( leef.bovengrens - leef.ondergrens ) verschil
    FROM   bas_ranges rang
    ,      kgc_leeftijden leef
    WHERE  rang.kafd_id = p_kafd_id
    AND    rang.mate_id = p_mate_id
    AND    rang.leef_id = leef.leef_id
    AND    p_aantal_dagen BETWEEN leef.ondergrens
                              AND leef.bovengrens
    AND    leef.vervallen = 'N'
    AND    rang.stof_id = p_stof_id
    AND  ( rang.stgr_id = p_stgr_id OR rang.stgr_id IS NULL )
    AND  ( rang.frty_id = p_frty_id OR rang.frty_id IS NULL )
    AND  ( rang.bety_id = p_bety_id OR rang.bety_id IS NULL )
    AND  ( rang.geslacht = p_geslacht OR rang.geslacht IS NULL )
    AND    rang.vervallen = 'N'
    ORDER BY verschil
    ,        DECODE( rang.stgr_id
                   , p_stgr_id, 1
                   , 9
                   )
    ,        DECODE( rang.frty_id
                   , p_frty_id, 1
                   , 9
                   )
    ,        DECODE( rang.bety_id
                   , p_bety_id, 1
                   , 9
                   )
    ,        DECODE( rang.geslacht
                   , p_geslacht, 1
                   , 9
                   )
    ;
  rang_rec rang_cur%rowtype;
BEGIN
  OPEN  rang_cur;
  FETCH rang_cur
  INTO  rang_rec;
  IF ( rang_rec.onder IS NOT NULL
    OR rang_rec.boven IS NOT NULL
     )
  THEN
    x_onder := rang_rec.onder;
    x_boven := rang_rec.boven;
    x_gemiddeld := rang_rec.gemiddeld;
  END IF;
  IF ( rang_cur%isopen )
  THEN
    CLOSE rang_cur;
  END IF;
END normaalwaarden;

FUNCTION normaalwaarden
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
, p_aantal_decemalen IN NUMBER := NULL -- Added by Athar Shaikh  for Mantis-3195on 31-03-2016
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
  -- Start added by Athar Shaikh  for Mantis-3195 on 31-03-2016
  v_normaalwaarde_ondergrens varchar2(100);
  v_normaalwaarde_bovengrens varchar2(100);
  -- End added by Athar Shaikh for Mantis-3195on 31-03-2016
BEGIN
  normaalwaarden
  ( p_kafd_id => p_kafd_id
  , p_mate_id => p_mate_id
  , p_aantal_dagen => p_aantal_dagen
  , p_stof_id => p_stof_id
  , p_stgr_id => p_stgr_id
  , p_frty_id => p_frty_id
  , p_bety_id => p_bety_id
  , p_geslacht => p_geslacht
  , x_onder => v_onder
  , x_boven => v_boven
  , x_gemiddeld => v_gemiddeld
  );
  -- Start added by Athar Shaikh  for Mantis-3195 on 31-03-2016
  IF (p_aantal_decemalen) IS NULL OR (p_aantal_decemalen) = 0
  THEN
    v_normaalwaarde_ondergrens := v_onder;
    v_normaalwaarde_bovengrens := v_boven;
  ELSE
    v_normaalwaarde_ondergrens   := trim(to_char(v_onder, (('9999999990.'|| LTRIM((Power(10, p_aantal_decemalen)),1)) )));
    v_normaalwaarde_bovengrens   := trim(to_char(v_boven, (('9999999990.'|| LTRIM((Power(10, p_aantal_decemalen)),1)) )));
  END IF;
  -- End added by Athar Shaikh for Mantis-3195 on 31-03-2016
  v_return := kgc_formaat_00.normaalwaarden
              ( p_onder => v_normaalwaarde_ondergrens -- v_onder commented and added v_normaalwaarde_ondergrens by Athar Shaikh for M3195 on 31-03-2016
              , p_boven => v_normaalwaarde_bovengrens -- v_boven commented and added v_normaalwaarde_bovengrens by Athar Shaikh for M3195 on 31-03-2016
              , p_gemiddeld => v_gemiddeld
              );
  RETURN( v_return );
END normaalwaarden;

FUNCTION laag
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
BEGIN
  normaalwaarden
  ( p_kafd_id => p_kafd_id
  , p_mate_id => p_mate_id
  , p_aantal_dagen => p_aantal_dagen
  , p_stof_id => p_stof_id
  , p_stgr_id => p_stgr_id
  , p_frty_id => p_frty_id
  , p_bety_id => p_bety_id
  , p_geslacht => p_geslacht
  , x_onder => v_onder
  , x_boven => v_boven
  , x_gemiddeld => v_gemiddeld
  );
  RETURN( TO_CHAR(v_onder) );
END laag;

FUNCTION hoog
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
BEGIN
  normaalwaarden
  ( p_kafd_id => p_kafd_id
  , p_mate_id => p_mate_id
  , p_aantal_dagen => p_aantal_dagen
  , p_stof_id => p_stof_id
  , p_stgr_id => p_stgr_id
  , p_frty_id => p_frty_id
  , p_bety_id => p_bety_id
  , p_geslacht => p_geslacht
  , x_onder => v_onder
  , x_boven => v_boven
  , x_gemiddeld => v_gemiddeld
  );
  RETURN( TO_CHAR(v_boven) );
END hoog;

FUNCTION gemiddeld
( p_kafd_id IN NUMBER
, p_mate_id IN NUMBER
, p_aantal_dagen IN NUMBER
, p_stof_id IN NUMBER
, p_stgr_id IN NUMBER := NULL
, p_frty_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
BEGIN
  normaalwaarden
  ( p_kafd_id => p_kafd_id
  , p_mate_id => p_mate_id
  , p_aantal_dagen => p_aantal_dagen
  , p_stof_id => p_stof_id
  , p_stgr_id => p_stgr_id
  , p_frty_id => p_frty_id
  , p_bety_id => p_bety_id
  , p_geslacht => p_geslacht
  , x_onder => v_onder
  , x_boven => v_boven
  , x_gemiddeld => v_gemiddeld
  );
  RETURN( TO_CHAR(v_gemiddeld) );
END gemiddeld;

PROCEDURE doorvoeren_range
( p_stof_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_wijzig IN VARCHAR2 := 'J' -- N=bestaande normaalwaarden blijven gehandhaafd
)
IS
  CURSOR meet_cur
  IS
    SELECT meet.stof_id
    ,      meet.meti_id
    ,      meet.meet_id
    ,      meet.normaalwaarde_ondergrens
    ,      meet.normaalwaarde_bovengrens
    ,      meet.normaalwaarde_gemiddelde
    FROM   bas_meetwaarden meet
    WHERE  bas_meet_00.afgerond( meet.meet_id ) = 'N'
    AND    meet.stof_id = NVL( p_stof_id, meet.stof_id )
    FOR UPDATE OF normaalwaarde_ondergrens nowait
    ;
  CURSOR bere_cur
  ( p_meet_id IN NUMBER
  )
  IS
    SELECT bere.bere_id
    ,      bere.bety_id
    ,      bere.normaalwaarde_ondergrens
    ,      bere.normaalwaarde_bovengrens
    ,      bere.normaalwaarde_gemiddelde
    FROM   bas_berekeningen bere
    WHERE  bere.meet_id = p_meet_id
    AND    bere.bety_id = p_bety_id
    FOR UPDATE OF normaalwaarde_ondergrens nowait
    ;
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
BEGIN
  FOR meet_rec IN meet_cur
  LOOP
    IF ( NOT kgc_onde_00.afgerond( p_meti_id => meet_rec.meti_id, p_rftf => FALSE ) )
    THEN
      v_onder := NULL;
      v_boven := NULL;
      v_gemiddeld := NULL;
      IF ( p_wijzig = 'N' )
      THEN
        v_onder := meet_rec.normaalwaarde_ondergrens;
        v_boven := meet_rec.normaalwaarde_bovengrens;
        v_gemiddeld := meet_rec.normaalwaarde_gemiddelde;
      END IF;
      v_onder := bas_meet_00.normaalwaarde_onder
                 ( p_stof_id => meet_rec.stof_id
                 , p_meti_id => meet_rec.meti_id
                 , p_bety_id => NULL
                 , p_normaalwaarde => v_onder
                 );
      v_boven := bas_meet_00.normaalwaarde_boven
                 ( p_stof_id => meet_rec.stof_id
                 , p_meti_id => meet_rec.meti_id
                 , p_bety_id => NULL
                 , p_normaalwaarde => v_boven
                 );
      v_gemiddeld := bas_meet_00.normaalwaarde_gemiddeld
                     ( p_stof_id => meet_rec.stof_id
                     , p_meti_id => meet_rec.meti_id
                     , p_bety_id => NULL
                     , p_normaalwaarde => v_gemiddeld
                     );
      BEGIN
        UPDATE bas_meetwaarden
        SET    normaalwaarde_ondergrens = v_onder
        ,      normaalwaarde_bovengrens = v_boven
        ,      normaalwaarde_gemiddelde = v_gemiddeld
        WHERE  meet_id = meet_rec.meet_id;
      EXCEPTION
        WHEN TIMEOUT_ON_RESOURCE
        THEN
          raise_application_error
          ( -20000, 'BAS-00026' );
        WHEN OTHERS
        THEN
          NULL;
      END;
      IF ( p_bety_id IS NOT NULL )
      THEN
        FOR bere_rec IN bere_cur( meet_rec.meet_id )
        LOOP
          v_onder := NULL;
          v_boven := NULL;
          IF ( p_wijzig = 'N' )
          THEN
            v_onder := bere_rec.normaalwaarde_ondergrens;
            v_boven := bere_rec.normaalwaarde_bovengrens;
            v_gemiddeld := bere_rec.normaalwaarde_gemiddelde;
          END IF;
          v_onder := bas_meet_00.normaalwaarde_onder
                     ( p_stof_id => meet_rec.stof_id
                     , p_meti_id => meet_rec.meti_id
                     , p_bety_id => bere_rec.bety_id
                     , p_normaalwaarde => v_onder
                     );
          v_boven := bas_meet_00.normaalwaarde_boven
                     ( p_stof_id => meet_rec.stof_id
                     , p_meti_id => meet_rec.meti_id
                     , p_bety_id => bere_rec.bety_id
                     , p_normaalwaarde => v_boven
                     );
          v_gemiddeld := bas_meet_00.normaalwaarde_gemiddeld
                         ( p_stof_id => meet_rec.stof_id
                         , p_meti_id => meet_rec.meti_id
                         , p_bety_id => bere_rec.bety_id
                         , p_normaalwaarde => v_gemiddeld
                         );
          BEGIN
            UPDATE bas_berekeningen
            SET    normaalwaarde_ondergrens = v_onder
            ,      normaalwaarde_bovengrens = v_boven
            ,      normaalwaarde_gemiddelde = v_gemiddeld
            WHERE  bere_id = bere_rec.bere_id;
          EXCEPTION
           WHEN TIMEOUT_ON_RESOURCE
           THEN
             raise_application_error
             ( -20000, 'BAS-00026' );
           WHEN OTHERS
           THEN
             NULL;
          END;
        END LOOP;
      END IF;
    END IF;
  END LOOP;
  COMMIT;
END doorvoeren_range;
--
PROCEDURE normaalwaarden_wijzigen
( p_mons_id  IN NUMBER := NULL
, p_kafd_id  IN NUMBER := NULL
, p_mate_id  IN NUMBER := NULL
, p_leeftijd IN NUMBER := NULL
, p_leef_id  IN NUMBER := NULL
, p_pers_id  IN NUMBER := NULL
, p_geslacht IN VARCHAR2 := NULL
, p_frac_id  IN NUMBER := NULL
, p_frty_id  IN NUMBER := NULL
, p_entiteit IN VARCHAR2 := NULL
, x_fout    OUT BOOLEAN
, x_melding OUT VARCHAR2
)
IS
-- Zoek benodigde persoonsgegevens:
  CURSOR pers_cur
  IS
    select pers.geslacht
    from   kgc_personen pers
    where  pers.pers_id =  p_pers_id
    ;

  -- Let op: de cursoren hieronder mogen geen tabellen bevatten die bij de aanroepende trigger horen!
  -- Parameter p_mons_id als ingang (aanroep vanuit kgc_monsters)
  CURSOR meet_mons_cur
  IS
    SELECT meet.meet_id
    ,      meet.stof_id
    ,      meet.meti_id
    ,      meti.stgr_id
    ,      frac.frty_id
    ,      to_char(meti.conclusie) conclusie -- MANTIS 9355 TO_CHAR added SKA 01-03-2016 release 8.11.0.2
    ,      meet.afgerond meet_afgerond
    ,      meti.afgerond meti_afgerond
    ,      onde.afgerond onde_afgerond
    ,      meet.meetwaarde
    ,      meet.normaalwaarde_ondergrens
    ,      meet.normaalwaarde_bovengrens
    ,      meet.normaalwaarde_gemiddelde
    FROM   kgc_onderzoeken onde
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meti.frac_id = frac.frac_id
    AND    frac.mons_id = p_mons_id
    AND    EXISTS
           ( SELECT NULL FROM bas_ranges rang
             WHERE rang.stof_id = meet.stof_id -- ook mate_id en leef_id ??
           )
  FOR UPDATE OF meet.normaalwaarde_ondergrens, meet.normaalwaarde_bovengrens, meti.conclusie nowait
  ;
  -- Als vorige meet cursor, maar dan met parameter p_pers_id als ingang (aanroep vanuit kgc_personen)
  CURSOR meet_pers_cur
  IS
    SELECT meet.meet_id
    ,      meet.stof_id
    ,      meet.meti_id
    ,      meti.stgr_id
    ,      frac.frty_id
    ,      to_char(meti.conclusie) conclusie -- MANTIS 9355 TO_CHAR added SKA 01-03-2016  release 8.11.0.2
    ,      meet.afgerond meet_afgerond
    ,      meti.afgerond meti_afgerond
    ,      onde.afgerond onde_afgerond
    ,      meet.meetwaarde
    ,      meet.normaalwaarde_ondergrens
    ,      meet.normaalwaarde_bovengrens
    ,      meet.normaalwaarde_gemiddelde
    ,      mons.kafd_id
    ,      mons.mate_id
    ,      mons.leeftijd
    FROM   kgc_onderzoeken onde
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_monsters mons
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    mons.pers_id = p_pers_id
    AND    EXISTS
           ( SELECT NULL FROM bas_ranges rang
             WHERE rang.stof_id = meet.stof_id
           )
  FOR UPDATE OF meet.normaalwaarde_ondergrens, meet.normaalwaarde_bovengrens, meti.conclusie nowait
  ;
  -- Als meet cursor, maar dan met parameters p_mons_id en p_frac_id als ingang (vanuit bas_fracties)
  CURSOR meet_frac_cur
  IS
    SELECT meet.meet_id
    ,      meet.stof_id
    ,      meet.meti_id
    ,      meti.stgr_id
    ,      to_char(meti.conclusie) conclusie -- MANTIS 9355 TO_CHAR added SKA 01-03-2016  release 8.11.0.2
    ,      meet.afgerond meet_afgerond
    ,      meti.afgerond meti_afgerond
    ,      onde.afgerond onde_afgerond
    ,      meet.meetwaarde
    ,      meet.normaalwaarde_ondergrens
    ,      meet.normaalwaarde_bovengrens
    ,      meet.normaalwaarde_gemiddelde
    ,      mons.kafd_id
    ,      mons.mate_id
    ,      mons.leeftijd
    ,      pers.geslacht
    FROM   kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_monsters mons
    ,      kgc_personen pers
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    pers.pers_id = mons.pers_id
    AND    meti.frac_id = p_frac_id
    AND    mons.mons_id = p_mons_id
    AND    EXISTS
           ( SELECT NULL FROM bas_ranges rang
             WHERE rang.stof_id = meet.stof_id
           )
  FOR UPDATE OF meet.normaalwaarde_ondergrens, meet.normaalwaarde_bovengrens, meti.conclusie nowait
  ;

  v_kafd_id number;
  v_mate_id number;
  v_mons_leeftijd number;
  v_meti_id number;
  v_meet_id number;
  v_stof_id number;
  v_stgr_id number;
  v_frty_id number;
  v_normaalwaarde_ondergrens number;
  v_normaalwaarde_bovengrens number;
  v_normaalwaarde_gemiddelde number;
  v_geslacht varchar2(1);
  v_onde_afgerond varchar2(1);
  v_meti_afgerond varchar2(1);
  v_meet_afgerond varchar2(1);

  v_ondergrens   NUMBER;
  v_bovengrens 	 NUMBER;
  v_gemiddeld 	 NUMBER;
  v_meetwaarde 	 VARCHAR2(1000);
  v_resultaat 	 VARCHAR2(2000);
  v_controle_ok  BOOLEAN;
  v_strengheid 	 VARCHAR2(1);
  v_mere_melding VARCHAR2(4000);

  totaal    NUMBER := 0;
  verwerkt  NUMBER := 0;
  meet_fout NUMBER := 0;
  meti_fout NUMBER := 0;
  ongeldig  NUMBER := 0;
  onde_af   NUMBER := 0;
  meti_af   NUMBER := 0;
  meet_af   NUMBER := 0;

  PROCEDURE wijzig
  IS
    l_ondergrens number;
    l_bovengrens number;
    l_gemiddelde number;
    l_meetwaarde varchar2(1000);
    l_resultaat varchar2(2000);
    l_controle_ok boolean;
    l_strengheid varchar2(1);
    l_mere_melding varchar2(4000);

  BEGIN
    totaal := totaal + 1;
    bas_rang_00.normaalwaarden
    ( p_kafd_id => v_kafd_id
    , p_mate_id => v_mate_id
    , p_aantal_dagen => v_mons_leeftijd
    , p_stof_id => v_stof_id
    , p_stgr_id => v_stgr_id
    , p_frty_id => v_frty_id
    , p_geslacht => v_geslacht
    , x_onder => l_ondergrens
    , x_boven => l_bovengrens
    , x_gemiddeld => l_gemiddelde
    );
    IF ( NVL( l_ondergrens, -9999999999 ) = NVL( v_normaalwaarde_ondergrens, -9999999999 )
     AND NVL( l_bovengrens, -9999999999 ) = NVL( v_normaalwaarde_bovengrens, -9999999999 )
     AND NVL( l_gemiddelde, -9999999999 ) = NVL( v_normaalwaarde_gemiddelde, -9999999999 )
       )
    THEN
      NULL; -- geen wijziging
    ELSE
      IF ( v_onde_afgerond = 'J' )
      THEN
        onde_af := onde_af + 1;
      ELSIF ( v_meti_afgerond = 'J' )
      THEN
        meti_af := meti_af + 1;
      ELSIF ( v_meet_afgerond = 'J' )
      THEN
        meet_af := meet_af + 1;
      ELSE
        BEGIN
          UPDATE bas_meetwaarden
          SET normaalwaarde_ondergrens = l_ondergrens
          ,   normaalwaarde_bovengrens = l_bovengrens
          ,   normaalwaarde_gemiddelde = l_gemiddelde
          WHERE meet_id = v_meet_id
          ;
          verwerkt := verwerkt + 1;
          EXCEPTION
            WHEN OTHERS
            THEN
              meet_fout := meet_fout + 1;
        END;
        -- nieuw automatisch resultaat?
        v_resultaat := v_resultaat;
        kgc_mere_00.resultaat
        ( p_meet_id     => v_meet_id
        , p_waarde      => v_meetwaarde
        , x_tekst       => l_resultaat
        , x_controle_ok => l_controle_ok
        , x_strengheid  => l_strengheid
        , x_melding     => l_mere_melding
        );
        IF ( l_controle_ok IS NULL )
        THEN
          l_controle_ok := TRUE;
        END IF;
        IF ( l_strengheid IS NULL )
        THEN
          l_strengheid := 'I';
        END IF;
        -- melding uit controle wordt vooralsnog genegeerd.
        IF ( l_controle_ok
          OR l_strengheid <> 'E'
           )
        THEN
          IF ( NVL( l_resultaat, CHR(2) ) <> NVL( v_resultaat, CHR(2) ) )
          THEN
            BEGIN
              UPDATE bas_metingen
              SET    conclusie = l_resultaat
              WHERE meti_id = v_meti_id;
            EXCEPTION
              WHEN OTHERS
              THEN
                meti_fout := meti_fout + 1;
            END;
          END IF;
        ELSE
          ongeldig := ongeldig + 1;
        END IF;
      END IF;
    END IF;
  END wijzig;

BEGIN
  IF p_entiteit = 'MONS'
  THEN
    IF ( p_mons_id IS NULL )
    THEN
      RETURN;
    END IF;
    x_fout := FALSE;
    IF ( p_pers_id IS NOT NULL )
    THEN
      OPEN  pers_cur;
      fetch pers_cur into v_geslacht;
      close pers_cur;
    END IF;
    FOR r IN meet_mons_cur
    LOOP
      -- vul elke keer de rij opnieuw:'
      v_kafd_id := p_kafd_id;
      v_mate_id := p_mate_id;
      v_mons_leeftijd := p_leeftijd;
      v_meti_id := r.meti_id;
      v_meet_id := r.meet_id;
      v_stof_id := r.stof_id;
      v_stgr_id := r.stgr_id;
      v_frty_id := r.frty_id;
      v_normaalwaarde_ondergrens := r.normaalwaarde_ondergrens;
      v_normaalwaarde_bovengrens := r.normaalwaarde_bovengrens;
      v_normaalwaarde_gemiddelde := r.normaalwaarde_gemiddelde;
      v_geslacht := v_geslacht; -- zie cursor hiervoor
      v_onde_afgerond := r.onde_afgerond;
      v_meti_afgerond := r.meti_afgerond;
      v_meet_afgerond := r.meet_afgerond;
      v_resultaat := r.conclusie;
      v_meetwaarde := r.meetwaarde;
      wijzig;
    END LOOP;


  ELSIF p_entiteit = 'PERS'
  THEN
    IF ( p_pers_id IS NULL
       OR p_geslacht IS NULL
       )
    THEN
      RETURN;
    END IF;
    FOR r IN meet_pers_cur
    LOOP
      -- vul elke keer de rij opnieuw:'
      v_kafd_id := r.kafd_id;
      v_mate_id := r.mate_id;
      v_mons_leeftijd := r.leeftijd;
      v_meti_id := r.meti_id;
      v_meet_id := r.meet_id;
      v_stof_id := r.stof_id;
      v_stgr_id := r.stgr_id;
      v_frty_id := r.frty_id;
      v_normaalwaarde_ondergrens := r.normaalwaarde_ondergrens;
      v_normaalwaarde_bovengrens := r.normaalwaarde_bovengrens;
      v_normaalwaarde_gemiddelde := r.normaalwaarde_gemiddelde;
      v_geslacht := p_geslacht;
      v_onde_afgerond := r.onde_afgerond;
      v_meti_afgerond := r.meti_afgerond;
      v_meet_afgerond := r.meet_afgerond;
      v_resultaat := r.conclusie;
      v_meetwaarde := r.meetwaarde;
      wijzig;
    END LOOP;


  ELSIF p_entiteit = 'FRAC'
  THEN
    IF ( p_frac_id IS NULL )
    THEN
      RETURN;
    END IF;
    FOR r IN meet_frac_cur
    LOOP
      -- vul elke keer de rij opnieuw:'
      v_kafd_id := r.kafd_id;
      v_mate_id := r.mate_id;
      v_mons_leeftijd := r.leeftijd;
      v_meti_id := r.meti_id;
      v_meet_id := r.meet_id;
      v_stof_id := r.stof_id;
      v_stgr_id := r.stgr_id;
      v_frty_id := p_frty_id;
      v_normaalwaarde_ondergrens := r.normaalwaarde_ondergrens;
      v_normaalwaarde_bovengrens := r.normaalwaarde_bovengrens;
      v_normaalwaarde_gemiddelde := r.normaalwaarde_gemiddelde;
      v_geslacht := r.geslacht;
      v_onde_afgerond := r.onde_afgerond;
      v_meti_afgerond := r.meti_afgerond;
      v_meet_afgerond := r.meet_afgerond;
      v_resultaat := r.conclusie;
      v_meetwaarde := r.meetwaarde;
      wijzig;
    END LOOP;

  END IF;

  x_melding := totaal||' aangemelde stoftesten, waarvan '||verwerkt||' aangepast.';
  IF ( ongeldig > 0 )
  THEN
    x_fout := TRUE;
    x_melding := x_melding||CHR(10)
              || 'In '||ongeldig||' geval(len) was de bestaande waarde ongeldig.';
  END IF;
  IF ( onde_af > 0 )
  THEN
    x_fout := TRUE;
    x_melding := x_melding||CHR(10)
              || 'In '||onde_af||' geval(len) was onderzoek al afgerond.';
  END IF;
  IF ( meti_af > 0 )
  THEN
    x_fout := TRUE;
    x_melding := x_melding||CHR(10)
              || 'In '||meti_af||' geval(len) was meting al afgerond.';
  END IF;
  IF ( meet_af > 0 )
  THEN
    x_fout := TRUE;
    x_melding := x_melding||CHR(10)
              || 'In '||meet_af||' geval(len) was meetwaarde al afgerond.';
  END IF;
  IF ( meet_fout > 0 )
  THEN
    x_fout := TRUE;
    x_melding := x_melding||CHR(10)
              || 'In '||meet_fout||' geval(len) zijn normaalwaarden/ranges niet aangepast.';
  END IF;
  IF ( meti_fout > 0 )
  THEN
    x_fout := TRUE;
    x_melding := x_melding||CHR(10)
              || 'In '||meti_fout||' geval(len) is resultaat niet aangepast.';
  END IF;
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_melding := SQLERRM;
END normaalwaarden_wijzigen;
--
END bas_rang_00;
/

/
QUIT
