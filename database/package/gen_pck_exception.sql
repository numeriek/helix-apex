CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_EXCEPTION"
IS
   /*
   ================================================================================
   Package Name : gen_pck_exception
   Usage        : This package contains functions and procedure concerning exception
                  handling
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   FUNCTION f_get_error_message(
      i_err_code       IN gen_error_messages.err_code%TYPE
     ,i_language       IN gen_error_messages.language_code%TYPE
     ,i_show_err_code  IN VARCHAR2 DEFAULT 'Y')
      RETURN VARCHAR2;

   -- DON'T change to i_error, else we can't use it in APEX
   FUNCTION f_error_handling(p_error IN apex_error.t_error)
      RETURN apex_error.t_error_result;
   --
   FUNCTION f_get_err_message(
      i_err_code       IN gen_error_messages.err_code%TYPE
     ,i_language       IN gen_error_messages.language_code%TYPE
     ,i_show_err_code  IN VARCHAR2 DEFAULT 'Y'
     ,i_apex_error     IN VARCHAR2 DEFAULT 'Y')
      RETURN gen_error_messages.err_message%TYPE;
   --
   PROCEDURE p_json_error(i_err_code  IN gen_error_messages.err_code%TYPE
                         ,i_incl_brc  IN BOOLEAN DEFAULT TRUE
                         ,i_err_tag   IN VARCHAR2 DEFAULT 'custom_error');
   --
   PROCEDURE p_submit_feedback_auton(
      i_comment         IN VARCHAR2 DEFAULT NULL
     ,i_type            IN NUMBER DEFAULT '1'
     ,i_application_id  IN VARCHAR2 DEFAULT NULL
     ,i_page_id         IN VARCHAR2 DEFAULT NULL
     ,i_email           IN VARCHAR2 DEFAULT NULL
     ,i_screen_width    IN VARCHAR2 DEFAULT NULL
     ,i_screen_height   IN VARCHAR2 DEFAULT NULL
     ,i_attribute_01    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_02    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_03    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_04    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_05    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_06    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_07    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_08    IN VARCHAR2 DEFAULT NULL
     ,i_label_01        IN VARCHAR2 DEFAULT NULL
     ,i_label_02        IN VARCHAR2 DEFAULT NULL
     ,i_label_03        IN VARCHAR2 DEFAULT NULL
     ,i_label_04        IN VARCHAR2 DEFAULT NULL
     ,i_label_05        IN VARCHAR2 DEFAULT NULL
     ,i_label_06        IN VARCHAR2 DEFAULT NULL
     ,i_label_07        IN VARCHAR2 DEFAULT NULL
     ,i_label_08        IN VARCHAR2 DEFAULT NULL
     ,i_log_it          IN BOOLEAN DEFAULT FALSE);
END gen_pck_exception;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_EXCEPTION"
IS
   /*
   ================================================================================
   Package Name : gen_pck_exception
   Usage        : This package contains functions and procedure concerning exception
                  handling
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */


   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   --
   -- Record structure which is passed into an error handling callout function and
   -- which contains all the relevant information about the occurred error
   --type t_error is record (
   --    message                  varchar2(32767),     /* Error message which will be displayed */
   --    additional_info          varchar2(32767),     /* Only used for display_location ON_ERROR_PAGE to display additional error information */
   --    display_location         varchar2(40),        /* Use constants "used for display_location" below */
   --    association_type         varchar2(40),        /* Use constants "used for asociation_type" below */
   --    page_item_name           varchar2(255),       /* Associated page item name */
   --    region_id                number,              /* Associated tabular form region id of the primary application */
   --    column_alias             varchar2(255),       /* Associated tabular form column alias */
   --    row_num                  pls_integer,         /* Associated tabular form row */
   --    apex_error_code          varchar2(255),       /* Contains the system message code if it's an error raised by APEX */
   --    is_internal_error        boolean,             /* Set to TRUE if it's a critical error raised by the APEX engine, like an invalid SQL/PLSQL statements, ... Internal Errors are always displayed on the Error Page */
   --    is_common_runtime_error  boolean,             /* TRUE for internal authorization, session and session state errors that normally should not be masked by an error handler */
   --    original_message         varchar2(32767),     /* Contains the original error message in case it has been modified by an error handling function */
   --    original_additional_info varchar2(32767),     /* Contains the original additional error information in case it has been modified by an error handling function */
   --    ora_sqlcode              number,              /* SQLCODE on exception stack which triggered the error, NULL if the error was not raised by an ORA error */
   --    ora_sqlerrm              varchar2(32767),     /* SQLERRM which triggered the error, NULL if the error was not raised by an ORA error */
   --    error_backtrace          varchar2(32767),     /* Output of sys.dbms_utility.format_error_backtrace or sys.dbms_utility.format_call_stack */
   --    error_statement          varchar2(32767),     /* Statement that was parsed when the error occurred - only suitable when parsing caused the error */
   --    component                wwv_flow.t_component /* Component which has been processed when the error occurred */
   --    );
   --
   --type t_error_result is record (
   --    message          varchar2(32767), /* Error message which will be displayed */
   --    additional_info  varchar2(32767), /* Only used for display_location ON_ERROR_PAGE to display additional error information */
   --    display_location varchar2(40),    /* Use constants "used for display_location" below */
   --    page_item_name   varchar2(255),   /* Associated page item name */
   --    column_alias     varchar2(255)    /* Associated tabular form column alias */
   --    );
   FUNCTION f_get_error_message(
      i_err_code       IN gen_error_messages.err_code%TYPE
     ,i_language       IN gen_error_messages.language_code%TYPE
     ,i_show_err_code  IN VARCHAR2 DEFAULT 'Y')
      RETURN VARCHAR2
   IS
      l_retval  gen_error_messages.err_message%TYPE;
   BEGIN
      SELECT    DECODE(i_show_err_code, 'Y', UPPER(i_err_code) || ' - ', '')
             || eme.err_message
        INTO l_retval
        FROM gen_error_messages eme
       WHERE UPPER(eme.err_code) = UPPER(i_err_code)
         AND UPPER(eme.language_code) = UPPER(i_language);
      RETURN l_retval;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         logger.LOG('Error does not exists');

         l_retval   := NULL;
         RETURN l_retval;
   END;
   FUNCTION f_add_error_message(
      i_error_code      gen_error_messages.err_code%TYPE
     ,i_language_code   gen_error_messages.language_code%TYPE
     ,i_message         gen_error_messages.err_message%TYPE)
      RETURN VARCHAR2
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      MERGE INTO gen_error_messages err
           USING (SELECT COALESCE(i_error_code
                                 ,REPLACE(i_message
                                         ,' '
                                         ,''))
                            err_code
                        ,i_language_code language_code
                        ,i_message err_message
                    FROM DUAL) nw
              ON (nw.err_code = err.err_code
              AND nw.language_code = err.language_code)
      WHEN NOT MATCHED
      THEN
         INSERT     (err_code
                    ,language_code
                    ,err_message)
             VALUES (nw.err_code
                    ,nw.language_code
                    ,nw.err_message);
      COMMIT;
      RETURN gen_pck_exception.f_get_error_message(i_error_code
                                                  ,i_language_code
                                                  ,'N');
   END;
  -- handling errors from APEX
   FUNCTION f_error_handling(p_error IN apex_error.t_error)
      RETURN apex_error.t_error_result
   IS
      l_proc    CONSTANT VARCHAR2(61) := g_package || 'f_error_handling';
      l_retval           apex_error.t_error_result;
      l_constraint_name  gen_error_messages.err_code%TYPE;
      l_language         gen_error_messages.language_code%TYPE;
      l_used             NUMBER DEFAULT 0;

   BEGIN
      logger.log_information(
         'p_error.apex_error_code : ' || p_error.apex_error_code
        ,l_proc);
      logger.log_information('p_error.message : ' || p_error.MESSAGE
                            ,l_proc);
      logger.log_information(
         'p_error.additional_info : ' || p_error.additional_info
        ,l_proc);
      logger.log_information('p_error.ora_sqlerrm : ' || p_error.ora_sqlerrm
                            ,l_proc);
      logger.log_information(
         'p_error.page_item_name : ' || p_error.page_item_name
        ,l_proc);
      l_retval   := apex_error.init_error_result(p_error => p_error);
      -- If it's an internal error raised by APEX, like an invalid statement or
      -- code which can't be executed, the error text might contain security sensitive
      -- information. To avoid this security problem we can rewrite the error to
      -- a generic error message and log the original error message for further
      -- investigation by the help desk.
      l_language      :=
         COALESCE(v('LANGCODE')
                 ,'en');
         IF p_error.apex_error_code <> 'APEX.AUTHORIZATION.ACCESS_DENIED'
         THEN
            -- Change the message to the generic error message which doesn't expose
            -- any sensitive information.
            IF p_error.apex_error_code = 'APEX.AUTHENTICATION.UNHANDLED_ERROR'
            THEN
               l_retval.MESSAGE           :=
                  f_get_err_message(i_err_code => 'GEN-INVALID-LOGIN'
                                   ,i_language => l_language);
               l_retval.additional_info   := NULL;
               l_used                     := 1;
            END IF;
         END IF;
         IF p_error.ora_sqlcode IN (-1
                                   ,-2091
                                   ,-2290
                                   ,-2291
                                   ,-2292)
         THEN

            l_constraint_name      :=
               apex_error.extract_constraint_name(p_error => p_error);
               logger.log_information('l_constraint_name='||l_constraint_name);
            l_retval.MESSAGE      :=
               gen_pck_exception.f_get_error_message(
                  i_err_code => l_constraint_name
                 ,i_language => l_language);
            l_used   := 1;
         END IF;
         -- If an ORA error has been raised, for example a raise_application_error(-20xxx, '...')
         -- in a table trigger or in a PL/SQL package called by a process and we
         -- haven't found the error in our lookup table, then we just want to see
         -- the actual error text and not the full error stack with all the ORA error numbers.
         IF p_error.ora_sqlcode IS NOT NULL
        AND l_retval.MESSAGE = p_error.MESSAGE
         THEN
            l_constraint_name      :=
               apex_error.get_first_ora_error_text(p_error => p_error);
                logger.log_information('4. l_constraint_name='||l_constraint_name);
            -- Translate the code to a user friendly message
            l_retval.MESSAGE      :=
               COALESCE(gen_pck_exception.f_get_error_message(
                           i_err_code => l_constraint_name
                          ,i_language => l_language)
                       ,l_constraint_name);
            l_used   := 1;
         END IF;
         -- If no associated page item/tabular form column has been set, we can use
         -- apex_error.auto_set_associated_item to automatically guess the affected
         -- error field by examine the ORA error for constraint names or column names.
         IF l_retval.page_item_name IS NULL
        AND l_retval.column_alias IS NULL
         THEN
            apex_error.auto_set_associated_item(p_error => p_error
                                               ,p_error_result => l_retval);
            l_used   := 1;
         END IF;
    --  END IF;
      IF l_used = 0
      THEN
         -- CHECK if code already exists, then retreive message
         -- else insert the message
         l_retval.MESSAGE      :=
            COALESCE(gen_pck_exception.f_get_error_message(
                        i_err_code => REPLACE(p_error.MESSAGE
                                             ,' '
                                             ,'')
                       ,i_language => l_language
                       ,i_show_err_code => 'N')
                    ,REPLACE(p_error.MESSAGE
                            ,' '
                            ,''));
         IF l_retval.MESSAGE IS NULL
         THEN
            -- INSERT the message in to GEN_ERROR_MESSAGES
            l_retval.MESSAGE      :=
               f_add_error_message(i_error_code => NULL
                                  ,i_language_code => 'en'
                                  ,i_message => p_error.MESSAGE);
         END IF;
      END IF;
        logger.log_information(
         'End'
        ,l_proc);
      RETURN l_retval;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
        RAISE;
   END f_error_handling;
   --
   FUNCTION f_get_err_message(
      i_err_code       IN gen_error_messages.err_code%TYPE
     ,i_language       IN gen_error_messages.language_code%TYPE
     ,i_show_err_code  IN VARCHAR2 DEFAULT 'Y'
     ,i_apex_error     IN VARCHAR2 DEFAULT 'Y')
      RETURN gen_error_messages.err_message%TYPE
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'get_err_message';
      l_retval          gen_error_messages.err_message%TYPE;
      l_err_code        gen_error_messages.err_code%TYPE;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      l_err_code   := i_err_code;
      --      select eme.err_message
      IF i_apex_error = 'N'
      THEN
         -- maybe pl/sql error, check if it is a constraint error
         -- check if a constraint name is in the table
         BEGIN
            SELECT constraint_name
              INTO l_err_code
              FROM user_constraints
             WHERE INSTR(l_err_code
                        ,constraint_name) > 1;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
               /* PHo 23-02-2017: Don't add error
               l_retval      :=
                  f_add_error_message(i_error_code => l_err_code
                                     ,i_language_code => i_language
                                     ,i_message => l_err_code);
               */
         END;
         -- when error not from apex then json_error is used so error code must be returned
         l_retval   := l_err_code;
      ELSE
         l_retval      :=
            f_get_error_message(l_err_code
                               ,i_language
                               ,i_show_err_code);
      END IF;
      IF l_retval IS NULL
      THEN
         -- add the error and return it
         l_retval      :=
            f_add_error_message(i_error_code => l_err_code
                               ,i_language_code => i_language
                               ,i_message => i_err_code);
      END IF;
      --
      logger.LOG(p_text => l_retval
                ,p_scope => l_scope);
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
      RETURN l_retval;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_retval   := 'Unexpected error ' --" double quote in json_error procedure
                                          || i_err_code;
         logger.LOG('Error does not exist');
         RETURN l_retval;
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
         RAISE;
   END f_get_err_message;
   --
   PROCEDURE p_json_error(i_err_code  IN gen_error_messages.err_code%TYPE
                         ,i_incl_brc  IN BOOLEAN DEFAULT TRUE
                         ,i_err_tag   IN VARCHAR2 DEFAULT 'custom_error')
   IS
   BEGIN
      htp.p(   CASE
                  WHEN i_incl_brc THEN '{'
               END
            --PHO: adjusted because apex.server.process auto alerts an error in json (WHY???) || '"error":"'
            || '"'
            || COALESCE(i_err_tag
                       ,'custom_error')
            || '":"'
            ||
              CASE
                  WHEN i_err_code IS NOT NULL
                  THEN
                     gen_pck_exception.f_get_err_message(i_err_code
                                                        ,v('LANGCODE')
                                                        ,i_show_err_code => 'N')
               END
            || '"'
            || CASE
                  WHEN i_incl_brc THEN '}'
               END);
   END;
   --
   PROCEDURE p_submit_feedback_auton(
      i_comment         IN VARCHAR2 DEFAULT NULL
     ,i_type            IN NUMBER DEFAULT '1'
     ,i_application_id  IN VARCHAR2 DEFAULT NULL
     ,i_page_id         IN VARCHAR2 DEFAULT NULL
     ,i_email           IN VARCHAR2 DEFAULT NULL
     ,i_screen_width    IN VARCHAR2 DEFAULT NULL
     ,i_screen_height   IN VARCHAR2 DEFAULT NULL
     ,i_attribute_01    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_02    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_03    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_04    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_05    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_06    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_07    IN VARCHAR2 DEFAULT NULL
     ,i_attribute_08    IN VARCHAR2 DEFAULT NULL
     ,i_label_01        IN VARCHAR2 DEFAULT NULL
     ,i_label_02        IN VARCHAR2 DEFAULT NULL
     ,i_label_03        IN VARCHAR2 DEFAULT NULL
     ,i_label_04        IN VARCHAR2 DEFAULT NULL
     ,i_label_05        IN VARCHAR2 DEFAULT NULL
     ,i_label_06        IN VARCHAR2 DEFAULT NULL
     ,i_label_07        IN VARCHAR2 DEFAULT NULL
     ,i_label_08        IN VARCHAR2 DEFAULT NULL
     ,i_log_it          IN BOOLEAN DEFAULT FALSE)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      apex_util.submit_feedback(p_comment => i_comment
                               ,p_type => i_type
                               ,p_application_id => i_application_id
                               ,p_page_id => i_page_id
                               ,p_email => i_email
                               ,p_screen_width => i_screen_width
                               ,p_screen_height => i_screen_height
                               ,p_attribute_01 => i_attribute_01
                               ,p_attribute_02 => i_attribute_02
                               ,p_attribute_03 => i_attribute_03
                               ,p_attribute_04 => i_attribute_04
                               ,p_attribute_05 => i_attribute_05
                               ,p_attribute_06 => i_attribute_06
                               ,p_attribute_07 => i_attribute_07
                               ,p_attribute_08 => i_attribute_08
                               ,p_label_01 => i_label_01
                               ,p_label_02 => i_label_02
                               ,p_label_03 => i_label_03
                               ,p_label_04 => i_label_04
                               ,p_label_05 => i_label_05
                               ,p_label_06 => i_label_06
                               ,p_label_07 => i_label_07
                               ,p_label_08 => i_label_08);
      COMMIT;
      IF i_log_it
      THEN
         logger.log_error(
               'Error (app/page): '
            || i_application_id
            || ' / '
            || i_page_id
            || '  VIEW FEEDBACK'
           ,'gen_exception_apex_pkg.p_submit_feedback_auton');
      END IF;
   END;
END gen_pck_exception;
/

/
QUIT
