CREATE OR REPLACE PACKAGE "HELIX"."KGC_VERT_00" IS
 -- PL/SQL Specification
-- Haal een omschrijving op van een entiteit-rij
-- dat als basis dient voor vertalingen
FUNCTION  basis_omschrijving
 (  p_entiteit  IN  VARCHAR2
 ,  p_id  IN  NUMBER
 )
 RETURN  VARCHAR2;

-- Haal een vertaling op van een entiteit-rij
FUNCTION  vertaling
 (  p_entiteit  IN  VARCHAR2
 ,  p_id  IN  NUMBER
 ,  p_taal_id  IN  NUMBER
 )
 RETURN  VARCHAR2;

FUNCTION  vertaling
 (  p_entiteit  IN  VARCHAR2
 ,  p_id  IN  NUMBER
 ,  p_nederlands  IN  VARCHAR2
 ,  p_taal_id  IN  NUMBER
 )
 RETURN  VARCHAR2;
 PRAGMA  RESTRICT_REFERENCES(  vertaling,  WNDS,  WNPS,  RNPS  );

-- taal wordt vastgelegd bij onderzoek
FUNCTION  taal
 (  p_onde_id  IN  NUMBER  :=  NULL
 ,  p_taal_code  IN  VARCHAR2  :=  NULL
 )
 RETURN  NUMBER;
 PRAGMA  RESTRICT_REFERENCES(  taal,  WNDS,  WNPS,  RNPS  );

END  KGC_VERT_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_VERT_00" IS
FUNCTION basis_omschrijving
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000) := NULL;
  CURSOR enti_cur
  IS
    SELECT tabel
    ,      pk_kolom
    ,      omschrijving_kolommen
    FROM   kgc_entiteiten
    WHERE  code = p_entiteit
    ;
  enti_rec enti_cur%rowtype;
  v_statement VARCHAR2(1000);
-- PL/SQL Block
BEGIN
  OPEN  enti_cur;
  FETCH enti_cur
  INTO  enti_rec;
  CLOSE enti_cur;
  v_statement := 'SELECT '||enti_rec.omschrijving_kolommen||CHR(10)
              || 'INTO :result'||CHR(10)
              || 'FROM '||enti_rec.tabel||CHR(10)
              || 'WHERE '||NVL(enti_rec.pk_kolom,p_entiteit||'_ID')||' = '||TO_CHAR(p_id)||CHR(10)
               ;
  v_return := kgc_util_00.dyn_exec( v_statement );
  RETURN( v_return );
END basis_omschrijving;

FUNCTION vertaling
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_taal_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000) := NULL;
  CURSOR vert_cur
  IS
    SELECT omschrijving
    FROM   kgc_vertalingen
    WHERE  entiteit = UPPER( p_entiteit )
    AND    id = p_id
    AND    taal_id = p_taal_id
  ;
BEGIN
  IF p_id IS NOT NULL
  THEN
    OPEN  vert_cur;
    FETCH vert_cur
    INTO  v_return;
    IF ( vert_cur%notfound )
    THEN
      v_return := basis_omschrijving
                  ( p_entiteit => p_entiteit
                  , p_id => p_id
                  );
    END IF;
    CLOSE vert_cur;
  END IF;
  RETURN( v_return );
END vertaling;

FUNCTION vertaling
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_nederlands IN VARCHAR2
, p_taal_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000) := NULL;
  CURSOR vert1_cur
  IS
    SELECT omschrijving
    FROM   kgc_vertalingen
    WHERE  entiteit = UPPER( p_entiteit )
    AND    id = p_id
    AND    taal_id = p_taal_id
  ;
  CURSOR vert2_cur
  IS
    SELECT omschrijving
    FROM   kgc_vertalingen
    WHERE  entiteit = UPPER( p_entiteit )
    AND    nederlands = p_nederlands
    AND    taal_id = p_taal_id
  ;
BEGIN
  IF ( p_id IS NOT NULL )
  THEN
    OPEN  vert1_cur;
    FETCH vert1_cur
    INTO  v_return;
    IF ( vert1_cur%notfound )
    THEN
      v_return := NULL;
    END IF;
    CLOSE vert1_cur;
  ELSIF ( p_nederlands IS NOT NULL )
  THEN
    OPEN  vert2_cur;
    FETCH vert2_cur
    INTO  v_return;
    IF ( vert2_cur%notfound )
    THEN
      v_return := p_nederlands;
    END IF;
    CLOSE vert2_cur;
  END IF;
  RETURN( v_return );
END vertaling;

FUNCTION taal
( p_onde_id IN NUMBER
, p_taal_code IN VARCHAR2 := NULL
)
RETURN NUMBER
IS
  v_return NUMBER;
  CURSOR onde_cur
  IS
    SELECT taal_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
  CURSOR taal_cur
  IS
    SELECT taal_id
    FROM   kgc_talen
    WHERE  code = p_taal_code
    ;
BEGIN
  IF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_return;
    CLOSE onde_cur;
  ELSIF ( p_taal_code IS NOT NULL )
  THEN
    OPEN  taal_cur;
    FETCH taal_cur
    INTO  v_return;
    CLOSE taal_cur;
  END IF;
  RETURN( v_return );
END taal;

END kgc_vert_00;
/

/
QUIT
