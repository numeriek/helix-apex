create or replace PACKAGE KGC_INTERFACE_GENERIEK IS
-- PACKAGE HELIX.KGC_INTERFACE_GENERIEK IS
 /******************************************************************************
    NAME:      KGC_INTERFACE_GENERIEK
    PURPOSE:   Functionaliteit voor het inlezen van gegevens conform de generieke
               Helix-standaard

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    3.0        20-01-2011  JKO              Rework Mantis 3184/Dragon: insert_huisarts aangepast
    2.0        25-11-2009  RDE              Rework Mantis 3184: Interface voor Dragon
    1.0        27-08-2009  RDE              Eerste versie tbv Mantis 3184: Interface voor Dragon
 *****************************************************************************/
   FUNCTION blob_naar_bestand (p_file   IN   VARCHAR2
                              ,p_blob   IN   BLOB
                              ) RETURN VARCHAR2;

   FUNCTION delete_bestand (p_file   IN   VARCHAR2
                           ) RETURN VARCHAR2;

   FUNCTION copy_bestand (p_file_van   IN   VARCHAR2
                         ,p_file_naar  IN   VARCHAR2
                         ) RETURN VARCHAR2;

   FUNCTION lees_in(b_interface IN     VARCHAR
                   ,b_xml       IN     CLOB
                   ,b_pdf       IN     BLOB
                   ,x_verslag   IN OUT VARCHAR2
                   ,b_imfi_id   IN     NUMBER := NULL
                   ) RETURN BOOLEAN;

   PROCEDURE lees_in(b_imfi_id   IN     NUMBER
                    ,b_xml       IN     VARCHAR2
                   );

   FUNCTION annuleer(b_interface     IN     VARCHAR
                    ,b_identificatie IN     VARCHAR2
                    ,x_verslag       IN OUT VARCHAR2
                    ) RETURN BOOLEAN;

   FUNCTION aanvraag_in_behandeling(b_interface     IN     VARCHAR
                                   ,b_identificatie IN     VARCHAR2
                                   ,x_verslag       IN OUT VARCHAR2
                                   ) RETURN NUMBER;

   PROCEDURE delete_imfi(b_imfi_id IN NUMBER);

   FUNCTION persoon_im_in_zis(p_imlo_id     IN     NUMBER
                             ,x_impe_trow   IN OUT cg$kgc_im_personen.cg$row_type
                             ,x_msg         IN OUT VARCHAR2
                             ) RETURN BOOLEAN;

   PROCEDURE insert_huisarts(p_rela_trow IN OUT cg$kgc_relaties.cg$row_type);
   PROCEDURE insert_verzekeraar(p_verz_trow IN OUT cg$kgc_verzekeraars.cg$row_type);

   -- overloaded tbv aanroep vanuit Dragon... maar parameters zijn identiek, dus andere naam!
   FUNCTION lees_in2(b_interface IN     VARCHAR
                   ,b_xml       IN     CLOB
                   ,b_pdf       IN     BLOB
                   ,x_verslag   IN OUT VARCHAR2
                   ,b_imfi_id   IN     NUMBER := NULL
                   ) RETURN VARCHAR2;
   FUNCTION annuleer2(b_interface     IN     VARCHAR
                    ,b_identificatie IN     VARCHAR2
                    ,x_verslag       IN OUT VARCHAR2
                    ) RETURN VARCHAR2;

   FUNCTION FileExists ( p_file   IN     VARCHAR2
                      ) RETURN VARCHAR2;

   -- copy van kgc_word_00, maar deze was local!
   FUNCTION blob_naar_bestand_java ( p_file   IN   VARCHAR2
                                   , p_blob   IN   BLOB
                                   ) RETURN VARCHAR2
   AS
      LANGUAGE JAVA
      NAME 'BlobHandler.ExportBlob(java.lang.String, oracle.sql.BLOB) return java.lang.String';

   FUNCTION get_db_nls_lang RETURN VARCHAR2;

   FUNCTION blob2clob -- rekening houden met charactersets
   ( p_blob         IN BLOB
   , p_to_charset   IN VARCHAR2 := NULL
   , p_from_charset IN VARCHAR2 := NULL
   ) RETURN CLOB;

   FUNCTION clob2blob
   ( p_clob clob
   ) RETURN BLOB;

   FUNCTION bestand_naar_blob ( p_file   IN     VARCHAR2
                              , p_blob   IN OUT BLOB
                              ) RETURN VARCHAR2;

   FUNCTION base642blob(p_base64 clob) RETURN BLOB;

   FUNCTION base642clob(p_base64 clob) RETURN CLOB;

   FUNCTION lees_in3
   ( b_interface IN     VARCHAR
   , b_xml       IN     CLOB
   , b_pdf       IN     CLOB -- geen blob!
   , x_verslag   IN OUT VARCHAR2
   , b_imfi_id   IN     NUMBER := NULL
   ) RETURN BOOLEAN;

END KGC_INTERFACE_GENERIEK;
/
  
create or replace PACKAGE BODY KGC_INTERFACE_GENERIEK IS
-- PACKAGE BODY HELIX.KGC_INTERFACE_GENERIEK IS

   TYPE t_col_ar          IS TABLE OF VARCHAR2(4000) INDEX BY VARCHAR2(32);

-- lokale functies
   PROCEDURE xml2ar(p_xmln IN xmldom.domnode, p_col_ar IN OUT t_col_ar) IS
      v_xmlnl                xmldom.domnodelist;
      v_xmlnl2               xmldom.domnodelist;
      v_xmln1                xmldom.domnode;
      v_xmln2                xmldom.domnode;
      v_waarde               VARCHAR2(4000);
      v_att_gevonden         BOOLEAN;
   BEGIN
      p_col_ar.delete;
      v_xmlnl := xmldom.getChildNodes(p_xmln);
      IF xmldom.getLength(v_xmlnl) > 0 THEN
         FOR i IN 0 .. xmldom.getLength(v_xmlnl) - 1
         LOOP
            v_xmln1 := xmldom.item(v_xmlnl
                              ,i);
            v_xmln2 := xmldom.getFirstChild(v_xmln1);
            v_waarde := NULL;
            IF xmldom.isNull(v_xmln2)
            THEN
               v_att_gevonden := TRUE; -- leeg!
            ELSE
               IF xmldom.getNodeType(v_xmln2) = xmldom.text_node -- elementair!
               THEN
                  v_att_gevonden := TRUE;
                  v_waarde := xmldom.getNodeValue(v_xmln2);
               ELSE
                  v_att_gevonden := FALSE; -- er zijn subnodes!
               END IF;
            END IF;
            IF v_att_gevonden
            THEN
               p_col_ar(xmldom.getnodename(v_xmln1)) := v_waarde;
            END IF;
         END LOOP;
      END IF;
   END xml2ar;
   FUNCTION get_att(p_col_ar t_col_ar, p_att_naam VARCHAR2) RETURN VARCHAR2 IS
   BEGIN
      RETURN p_col_ar(p_att_naam);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_att;

   PROCEDURE merge_im_personen(p_imlo_id     IN     NUMBER
                              ,p_col_ar_pers IN     t_col_ar
                              ,x_impe_trow   IN OUT cg$kgc_im_personen.cg$row_type
                              ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_personen.slct(x_impe_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_impe_trow.zisnr               := get_att(p_col_ar_pers, 'zisnr');

      x_impe_trow.achternaam          := get_att(p_col_ar_pers, 'achternaam');
      x_impe_trow.gehuwd              := get_att(p_col_ar_pers, 'gehuwd');
      x_impe_trow.impw_id_geslacht    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                             ,p_helix_table_domain => kgc_import.g_cimpw_dom_geslacht
                                                             ,p_externe_id         => get_att(p_col_ar_pers, 'geslacht')
                                                             ,p_hint               => NULL
                                                             ,p_toevoegen          => TRUE);
      x_impe_trow.geboortedatum       := get_att(p_col_ar_pers, 'geboortedatum');
      x_impe_trow.voorletters         := get_att(p_col_ar_pers, 'voorletters');
      x_impe_trow.voorvoegsel         := get_att(p_col_ar_pers, 'voorvoegsel');

      x_impe_trow.bloedverwant        := get_att(p_col_ar_pers, 'bloedverwant');
      x_impe_trow.meerling            := get_att(p_col_ar_pers, 'meerling');
      x_impe_trow.overleden           := get_att(p_col_ar_pers, 'overleden');
      x_impe_trow.default_aanspreken  := get_att(p_col_ar_pers, 'default_aanspreken');

      x_impe_trow.impw_id_rela := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                      ,p_externe_id         => get_att(p_col_ar_pers, 'rela_id')
                                                      ,p_hint               => get_att(p_col_ar_pers, 'rela_omschrijving')
                                                      ,p_toevoegen          => TRUE);
      x_impe_trow.impw_id_verz := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_verz
                                                      ,p_externe_id         => get_att(p_col_ar_pers, 'verz_id')
                                                      ,p_hint               => get_att(p_col_ar_pers, 'verz_omschrijving')
                                                      ,p_toevoegen          => TRUE);

      x_impe_trow.part_geboortedatum  := get_att(p_col_ar_pers, 'part_geboortedatum');
      x_impe_trow.achternaam_partner  := get_att(p_col_ar_pers, 'achternaam_partner');
      x_impe_trow.voorvoegsel_partner := get_att(p_col_ar_pers, 'voorvoegsel_partner');
      x_impe_trow.adres               := get_att(p_col_ar_pers, 'adres');
      x_impe_trow.postcode            := get_att(p_col_ar_pers, 'postcode');
      x_impe_trow.woonplaats          := get_att(p_col_ar_pers, 'woonplaats');
      x_impe_trow.impw_id_provincie   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                             ,p_helix_table_domain => 'PROVINCIE'
                                                             ,p_externe_id         => get_att(p_col_ar_pers, 'provincie')
                                                             ,p_hint               => NULL
                                                             ,p_toevoegen          => TRUE);
      x_impe_trow.land                := get_att(p_col_ar_pers, 'land');
      x_impe_trow.telefoon            := get_att(p_col_ar_pers, 'telefoon');
      x_impe_trow.telefax             := get_att(p_col_ar_pers, 'telefax');
      x_impe_trow.email               := get_att(p_col_ar_pers, 'email');
      x_impe_trow.impw_id_verzwijze   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                             ,p_helix_table_domain => kgc_import.g_cimpw_dom_verzekeringswijze
                                                             ,p_externe_id         => get_att(p_col_ar_pers, 'verzekeringswijze')
                                                             ,p_hint               => NULL
                                                             ,p_toevoegen          => TRUE);
      x_impe_trow.verzekeringsnr      := get_att(p_col_ar_pers, 'verzekeringsnr');
      x_impe_trow.overlijdensdatum    := get_att(p_col_ar_pers, 'overlijdensdatum');
      x_impe_trow.hoeveelling         := get_att(p_col_ar_pers, 'hoeveelling');
      x_impe_trow.bsn                 := get_att(p_col_ar_pers, 'bsn');
      x_impe_trow.bsn_geverifieerd    := get_att(p_col_ar_pers, 'bsn_geverifieerd');
      -- nieuw, mantis 11620
      x_impe_trow.telefoon2           := get_att(p_col_ar_pers, 'telefoon2');
      x_impe_trow.anoniem             := get_att(p_col_ar_pers, 'anoniem');
      x_impe_trow.extra1              := get_att(p_col_ar_pers, 'extra1');

      IF x_impe_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_personen.ins(cg$rec => x_impe_trow
                               ,cg$ind => cg$kgc_im_personen.cg$ind_true);
      ELSE
         cg$kgc_im_personen.upd(cg$rec => x_impe_trow
                               ,cg$ind => cg$kgc_im_personen.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_personen;

   PROCEDURE merge_im_zwangerschappen(p_imlo_id     IN     NUMBER
                                     ,p_impe_id     IN     VARCHAR2
                                     ,p_col_ar_zwan IN     t_col_ar
                                     ,x_imzw_trow   IN OUT cg$kgc_im_zwangerschappen.cg$row_type
                                     ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_zwangerschappen.slct(x_imzw_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;
      x_imzw_trow.impe_id         := p_impe_id;
      x_imzw_trow.nummer          := get_att(p_col_ar_zwan, 'nummer');
      x_imzw_trow.info_geslacht   := get_att(p_col_ar_zwan, 'info_geslacht');
      x_imzw_trow.consanguiniteit := get_att(p_col_ar_zwan, 'consanguiniteit');
      x_imzw_trow.impw_id_rela    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                         ,p_externe_id         => get_att(p_col_ar_zwan, 'rela_id')
                                                         ,p_hint               => get_att(p_col_ar_zwan, 'rela_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imzw_trow.impw_id_zekerheid_datum_lm := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                                    ,p_helix_table_domain => kgc_import.g_cimpw_dom_zekerheid
                                                                    ,p_externe_id         => get_att(p_col_ar_zwan, 'zekerheid_datum_lm')
                                                                    ,p_hint               => NULL
                                                                    ,p_toevoegen          => TRUE);
      x_imzw_trow.datum_lm        := get_att(p_col_ar_zwan, 'datum_lm');
      x_imzw_trow.impw_id_berekeningswijze := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                                  ,p_helix_table_domain => kgc_import.g_cimpw_dom_berekeningswijze
                                                                  ,p_externe_id         => get_att(p_col_ar_zwan, 'berekeningswijze')
                                                                  ,p_hint               => NULL
                                                                  ,p_toevoegen          => TRUE);
      x_imzw_trow.datum_aterm     := get_att(p_col_ar_zwan, 'datum_aterm');
      x_imzw_trow.medicatie       := get_att(p_col_ar_zwan, 'medicatie');
      x_imzw_trow.opmerkingen     := get_att(p_col_ar_zwan, 'opmerkingen');
      x_imzw_trow.mola            := get_att(p_col_ar_zwan, 'mola');
      x_imzw_trow.apla            := get_att(p_col_ar_zwan, 'apla');
      x_imzw_trow.eug             := get_att(p_col_ar_zwan, 'eug');
      x_imzw_trow.para            := get_att(p_col_ar_zwan, 'para');
      x_imzw_trow.abortussen      := get_att(p_col_ar_zwan, 'abortussen');
      x_imzw_trow.abortus         := get_att(p_col_ar_zwan, 'abortus');
      x_imzw_trow.hoeveelling     := get_att(p_col_ar_zwan, 'hoeveelling');
      x_imzw_trow.progenituur     := get_att(p_col_ar_zwan, 'progenituur');

      IF x_imzw_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_zwangerschappen.ins(cg$rec => x_imzw_trow
                                      ,cg$ind => cg$kgc_im_zwangerschappen.cg$ind_true);
      ELSE
         cg$kgc_im_zwangerschappen.upd(cg$rec => x_imzw_trow
                                      ,cg$ind => cg$kgc_im_zwangerschappen.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_zwangerschappen;

   PROCEDURE merge_im_foetussen(p_imlo_id        IN NUMBER
                               ,p_impe_id_moeder IN VARCHAR2
                               ,p_imzw_id        IN VARCHAR2
                               ,p_col_ar_foet    IN t_col_ar
                               ,x_imfo_trow      IN OUT cg$kgc_im_foetussen.cg$row_type
                                ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_foetussen.slct(x_imfo_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_imfo_trow.impe_id_moeder  := p_impe_id_moeder;
      x_imfo_trow.imzw_id         := p_imzw_id;
      x_imfo_trow.volgnr          := get_att(p_col_ar_foet, 'volgnr');
      x_imfo_trow.niet_geboren    := get_att(p_col_ar_foet, 'niet_geboren');
      x_imfo_trow.impw_id_geslacht := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                          ,p_helix_table_domain => kgc_import.g_cimpw_dom_geslacht
                                                          ,p_externe_id         => get_att(p_col_ar_foet, 'geslacht')
                                                          ,p_hint               => NULL
                                                          ,p_toevoegen          => TRUE);
      x_imfo_trow.geplande_geboortedatum := get_att(p_col_ar_foet, 'geplande_geboortedatum');
      x_imfo_trow.impw_id_positie := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                          ,p_helix_table_domain => kgc_import.g_cimpw_tab_posi
                                                          ,p_externe_id         => get_att(p_col_ar_foet, 'posi_id')
                                                          ,p_hint               => get_att(p_col_ar_foet, 'posi_omschrijving')
                                                          ,p_toevoegen          => TRUE);
      -- geboren als: kan niet aangeleverd worden...
      --x_imfo_trow.impe_id_geboren_als := get_att(p_col_ar_foet, 'impe_id_geboren_als');
      x_imfo_trow.commentaar      := get_att(p_col_ar_foet, 'commentaar');

      IF x_imfo_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_foetussen.ins(cg$rec => x_imfo_trow
                                ,cg$ind => cg$kgc_im_foetussen.cg$ind_true);
      ELSE
         cg$kgc_im_foetussen.upd(cg$rec => x_imfo_trow
                                ,cg$ind => cg$kgc_im_foetussen.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_foetussen;

   PROCEDURE merge_im_monsters(p_imlo_id     IN NUMBER
                              ,p_impe_id     IN VARCHAR2
                              ,p_col_ar_mons IN t_col_ar
                              ,x_immo_trow   IN OUT cg$kgc_im_monsters.cg$row_type
                               ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_monsters.slct(x_immo_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_immo_trow.impe_id         := p_impe_id;
      x_immo_trow.monsternummer   := get_att(p_col_ar_mons, 'monsternummer');
      x_immo_trow.impw_id_kafd    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_kafd
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'kafd_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'kafd_naam')
                                                         ,p_toevoegen          => TRUE);
      x_immo_trow.impw_id_ongr    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ongr
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'ongr_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'ongr_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_immo_trow.imfo_id         := get_att(p_col_ar_mons, 'imfo_id');
      x_immo_trow.impw_id_mate    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mate
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'mate_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'mate_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_immo_trow.declareren      := get_att(p_col_ar_mons, 'declareren');
      x_immo_trow.prematuur       := get_att(p_col_ar_mons, 'prematuur');
-- niet in gebruik      x_immo_trow.bewaren         := get_att(p_col_ar_mons, 'bewaren');
-- niet in gebruik      x_immo_trow.nader_gebruiken := get_att(p_col_ar_mons, 'nader_gebruiken');
      x_immo_trow.datum_aanmelding := get_att(p_col_ar_mons, 'datum_aanmelding');
      x_immo_trow.impw_id_mede    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'mede_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'mede_naam')
                                                         ,p_toevoegen          => TRUE);
      x_immo_trow.impw_id_rela    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'rela_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'rela_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_immo_trow.impw_id_herk    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_herk
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'herk_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'herk_omschrijving')
                                                         ,p_toevoegen          => TRUE);
-- niet in gebruik      x_immo_trow.impw_id_buff    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_buff
--                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'buff_id')
--                                                         ,p_hint               => NULL
--                                                         ,p_toevoegen          => TRUE);
      x_immo_trow.impw_id_cond    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_cond
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'cond_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'cond_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_immo_trow.impw_id_leef    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_leef
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_leef
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'leef_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'leef_omschrijving')
                                                         ,p_toevoegen          => TRUE);
-- niet in gebruik      x_immo_trow.leeftijd        := get_att(p_col_ar_mons, 'leeftijd');
--      x_immo_trow.impw_id_bepl_kort := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                           ,p_helix_table_domain => kgc_import.g_cimpw_tab_bepl
--                                                           ,p_externe_id         => get_att(p_col_ar_mons, 'bepl_id_kort')
--                                                           ,p_hint               => NULL
--                                                           ,p_toevoegen          => TRUE);
-- niet in gebruik      x_immo_trow.impw_id_bepl_lang := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                           ,p_helix_table_domain => kgc_import.g_cimpw_tab_bepl
--                                                           ,p_externe_id         => get_att(p_col_ar_mons, 'bepl_id_lang')
--                                                           ,p_hint               => NULL
--                                                           ,p_toevoegen          => TRUE);
      x_immo_trow.datum_afname    := get_att(p_col_ar_mons, 'datum_afname');
      x_immo_trow.tijd_afname     := get_att(p_col_ar_mons, 'tijd_afname');
      x_immo_trow.periode_afname  := get_att(p_col_ar_mons, 'periode_afname');
-- niet in gebruik      x_immo_trow.ckcl_nummer     := get_att(p_col_ar_mons, 'ckcl_nummer');
      -- niet in gebruikx_immo_trow.kweeknummer     := get_att(p_col_ar_mons, 'kweeknummer');
      x_immo_trow.aantal_buizen   := get_att(p_col_ar_mons, 'aantal_buizen');
      x_immo_trow.datum_verwacht  := get_att(p_col_ar_mons, 'datum_verwacht');
      x_immo_trow.hoeveelheid_monster := get_att(p_col_ar_mons, 'hoeveelheid_monster');
-- niet in gebruik      x_immo_trow.hoeveelheid_buffer := get_att(p_col_ar_mons, 'hoeveelheid_buffer');
      x_immo_trow.medicatie       := get_att(p_col_ar_mons, 'medicatie');
      x_immo_trow.voeding         := get_att(p_col_ar_mons, 'voeding');
      x_immo_trow.medische_indicatie := get_att(p_col_ar_mons, 'medische_indicatie');
      x_immo_trow.commentaar      := get_att(p_col_ar_mons, 'commentaar');
      x_immo_trow.extra1          := get_att(p_col_ar_mons, 'extra1');

      x_immo_trow.bestandsnaam    := get_att(p_col_ar_mons, 'bestandsnaam');
      x_immo_trow.impw_id_btyp    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_btyp
                                                         ,p_externe_id         => get_att(p_col_ar_mons, 'btyp_id')
                                                         ,p_hint               => get_att(p_col_ar_mons, 'btyp_omschrijving')
                                                         ,p_toevoegen          => TRUE);

      IF x_immo_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_monsters.ins(cg$rec => x_immo_trow
                                ,cg$ind => cg$kgc_im_monsters.cg$ind_true);
      ELSE
         cg$kgc_im_monsters.upd(cg$rec => x_immo_trow
                                ,cg$ind => cg$kgc_im_monsters.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_monsters;

   PROCEDURE merge_im_fracties(p_imlo_id     IN NUMBER
                              ,p_col_ar_frac IN t_col_ar
                              ,x_imfr_trow   IN OUT cg$kgc_im_fracties.cg$row_type
                               ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_fracties.slct(x_imfr_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_imfr_trow.fractienummer   := get_att(p_col_ar_frac, 'fractienummer');
      x_imfr_trow.impw_id_kafd    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_kafd
                                                         ,p_externe_id         => get_att(p_col_ar_frac, 'kafd_id')
                                                         ,p_hint               => get_att(p_col_ar_frac, 'kafd_naam')
                                                         ,p_toevoegen          => TRUE);
      x_imfr_trow.controle        := get_att(p_col_ar_frac, 'controle');
      x_imfr_trow.impw_id_status  := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_dom_fractiestatus
                                                         ,p_externe_id         => get_att(p_col_ar_frac, 'status')
                                                         ,p_hint               => NULL
                                                         ,p_toevoegen          => TRUE);
      x_imfr_trow.impw_id_frty    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_frty
                                                         ,p_externe_id         => get_att(p_col_ar_frac, 'frty_id')
                                                         ,p_hint               => get_att(p_col_ar_frac, 'frty_omschrijving')
                                                         ,p_toevoegen          => TRUE);
-- nvt      x_imfr_trow.impw_id_opwe    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_opwe
--                                                         ,p_externe_id         => get_att(p_col_ar_frac, 'opwe_id')
--                                                         ,p_hint               => NULL
--                                                         ,p_toevoegen          => TRUE);
      x_imfr_trow.impw_id_cond    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_cond
                                                         ,p_externe_id         => get_att(p_col_ar_frac, 'cond_id')
                                                         ,p_hint               => get_att(p_col_ar_frac, 'cond_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imfr_trow.commentaar      := get_att(p_col_ar_frac, 'commentaar');
      x_imfr_trow.impw_id_type_afleiding := kgc_import.getimpwid(p_imlo_id     => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_dom_type_afleiding
                                                         ,p_externe_id         => get_att(p_col_ar_frac, 'type_afleiding')
                                                         ,p_hint               => NULL
                                                         ,p_toevoegen          => TRUE);
      x_imfr_trow.concentratie    := get_att(p_col_ar_frac, 'concentratie');
      x_imfr_trow.impw_id_eenh    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_eenh
                                                         ,p_externe_id         => get_att(p_col_ar_frac, 'eenh_id')
                                                         ,p_hint               => NULL
                                                         ,p_toevoegen          => TRUE);
      x_imfr_trow.datum_concentratie_bepaling := get_att(p_col_ar_frac, 'datum_concentratie_bepaling');
      x_imfr_trow.extra1          := get_att(p_col_ar_frac, 'extra1');

      IF x_imfr_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_fracties.ins(cg$rec => x_imfr_trow
                               ,cg$ind => cg$kgc_im_fracties.cg$ind_true);
      ELSE
         cg$kgc_im_fracties.upd(cg$rec => x_imfr_trow
                               ,cg$ind => cg$kgc_im_fracties.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_fracties;

   PROCEDURE merge_im_onderzoeken(p_imlo_id     IN NUMBER
                                 ,p_impe_id     IN VARCHAR2
                                 ,p_col_ar_onde IN t_col_ar
                                 ,x_imon_trow   IN OUT cg$kgc_im_onderzoeken.cg$row_type
                                 ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_onderzoeken.slct(x_imon_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;
      x_imon_trow.impe_id         := p_impe_id;
      x_imon_trow.impw_id_kafd    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_kafd
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'kafd_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'kafd_naam')
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_ongr    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ongr
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'ongr_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'ongr_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.imfo_id         := get_att(p_col_ar_onde, 'imfo_id');
      x_imon_trow.impw_id_onderzoekstype := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                                ,p_helix_table_domain => kgc_import.g_cimpw_dom_onderzoekstypen
                                                                ,p_externe_id         => get_att(p_col_ar_onde, 'onderzoekstype')
                                                                ,p_hint               => NULL
                                                                ,p_toevoegen          => TRUE);
      x_imon_trow.onderzoeknr     := get_att(p_col_ar_onde, 'onderzoeknr');
      x_imon_trow.impw_id_rela    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'rela_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'rela_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.spoed           := get_att(p_col_ar_onde, 'spoed');
      x_imon_trow.declareren      := get_att(p_col_ar_onde, 'declareren');
      x_imon_trow.afgerond        := get_att(p_col_ar_onde, 'afgerond');
      x_imon_trow.datum_binnen    := get_att(p_col_ar_onde, 'datum_binnen');
      x_imon_trow.geplande_einddatum := get_att(p_col_ar_onde, 'geplande_einddatum');
      x_imon_trow.impw_id_onderzoekswijze := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                                 ,p_helix_table_domain => kgc_import.g_cimpw_tab_onwy
                                                                 ,p_externe_id         => get_att(p_col_ar_onde, 'onderzoekswijze')
                                                                 ,p_hint               => NULL
                                                                 ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_dewy    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_dewy
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'dewy_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'dewy_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_herk    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_herk
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'herk_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'herk_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_inst    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_inst
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'inst_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'inst_naam')
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_taal    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_taal
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'taal_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'taal_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_status  := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_dom_onde_status
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'status')
                                                         ,p_hint               => NULL
                                                         ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_beoordelaar := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                             ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                             ,p_externe_id         => get_att(p_col_ar_onde, 'mede_id_beoordelaar')
                                                             ,p_hint               => get_att(p_col_ar_onde, 'mede_naam_beoordelaar')
                                                             ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_autorisator := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                             ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                             ,p_externe_id         => get_att(p_col_ar_onde, 'mede_id_autorisator')
                                                             ,p_hint               => get_att(p_col_ar_onde, 'mede_naam_autorisator')
                                                             ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_controle := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                          ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                          ,p_externe_id         => get_att(p_col_ar_onde, 'mede_id_controle')
                                                          ,p_hint               =>  get_att(p_col_ar_onde, 'mede_naam_controle')
                                                          ,p_toevoegen          => TRUE);
-- niet in gebruik      x_imon_trow.impw_id_dia_autorisator := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                                 ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
--                                                                 ,p_externe_id         => get_att(p_col_ar_onde, 'mede_id_dia_autorisator')
--                                                                 ,p_hint               => NULL
--                                                                 ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_onui    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_onui
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_onui
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'onui_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'onui_omschrijving')
                                                         ,p_toevoegen          => TRUE);
-- niet in gebruik      x_imon_trow.impw_id_coty    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_coty
--                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'coty_id')
--                                                         ,p_hint               => NULL
--                                                         ,p_toevoegen          => TRUE);

-- niet in gebruik      x_imon_trow.impw_id_diagnose_code   := get_att(p_col_ar_onde, 'diagnose_code');

-- niet in gebruik      x_imon_trow.impw_id_ermo    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ermo
--                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'ermo_id')
--                                                         ,p_hint               => NULL
--                                                         ,p_toevoegen          => TRUE);
-- niet in gebruik      x_imon_trow.impw_id_dist    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_dist
--                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'dist_id')
--                                                         ,p_hint               => NULL
--                                                         ,p_toevoegen          => TRUE);
-- niet in gebruik      x_imon_trow.impw_id_herk_dia := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                          ,p_helix_table_domain => kgc_import.g_cimpw_tab_herk
--                                                          ,p_externe_id         => get_att(p_col_ar_onde, 'herk_id_dia')
--                                                          ,p_hint               => NULL
--                                                          ,p_toevoegen          => TRUE);
      x_imon_trow.impw_id_verz    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_verz
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'verz_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'verz_omschrijving')
                                                         ,p_toevoegen          => TRUE);
-- niet in gebruik      x_imon_trow.impw_id_fare    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_fare
--                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'fare_id')
--                                                         ,p_hint               => NULL
--                                                         ,p_toevoegen          => TRUE);
-- niet in gebruik      x_imon_trow.impw_id_proj    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_proj
--                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'proj_id')
--                                                         ,p_hint               => NULL
--                                                         ,p_toevoegen          => TRUE);

-- niet in gebruik      x_imon_trow.impw_id_zekerheid_diagnose := get_att(p_col_ar_onde, 'zekerheid_diagnose');
-- niet in gebruik      x_imon_trow.impw_id_zekerheid_status := get_att(p_col_ar_onde, 'zekerheid_status');
-- niet in gebruik      x_imon_trow.impw_id_zekerheid_erfmodus := get_att(p_col_ar_onde, 'zekerheid_erfmodus');

      x_imon_trow.datum_autorisatie := get_att(p_col_ar_onde, 'datum_autorisatie');
      x_imon_trow.omschrijving    := get_att(p_col_ar_onde, 'omschrijving');
      x_imon_trow.referentie      := get_att(p_col_ar_onde, 'referentie');
      x_imon_trow.nota_adres      := get_att(p_col_ar_onde, 'nota_adres');
-- niet in gebruik      x_imon_trow.kariotypering   := get_att(p_col_ar_onde, 'kariotypering');
      x_imon_trow.immo_id         := get_att(p_col_ar_onde, 'immo_id');
      x_imon_trow.extra1          := get_att(p_col_ar_onde, 'extra1');

      x_imon_trow.bestandsnaam    := get_att(p_col_ar_onde, 'bestandsnaam');
      x_imon_trow.impw_id_btyp    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_btyp
                                                         ,p_externe_id         => get_att(p_col_ar_onde, 'btyp_id')
                                                         ,p_hint               => get_att(p_col_ar_onde, 'btyp_omschrijving')
                                                         ,p_toevoegen          => TRUE);

      IF x_imon_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_onderzoeken.ins(cg$rec => x_imon_trow
                                ,cg$ind => cg$kgc_im_onderzoeken.cg$ind_true);
      ELSE
         cg$kgc_im_onderzoeken.upd(cg$rec => x_imon_trow
                                ,cg$ind => cg$kgc_im_onderzoeken.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_onderzoeken;

   PROCEDURE merge_im_onde_kopie(p_imlo_id     IN     NUMBER
                                ,p_col_ar_onkh IN     t_col_ar
                                ,x_imok_trow   IN OUT cg$kgc_im_onde_kopie.cg$row_type
                                ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_onde_kopie.slct(x_imok_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;
      x_imok_trow.code_kopiehouder := get_att(p_col_ar_onkh, 'code_kopiehouder');
      x_imok_trow.impw_id_rela    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                         ,p_externe_id         => get_att(p_col_ar_onkh, 'rela_id')
                                                         ,p_hint               => get_att(p_col_ar_onkh, 'rela_omschrijving')
                                                         ,p_toevoegen          => TRUE);

      IF x_imok_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_onde_kopie.ins(cg$rec => x_imok_trow
                                 ,cg$ind => cg$kgc_im_onde_kopie.cg$ind_true);
      ELSE
         cg$kgc_im_onde_kopie.upd(cg$rec => x_imok_trow
                                 ,cg$ind => cg$kgc_im_onde_kopie.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_onde_kopie;


   PROCEDURE merge_im_onde_betr(p_imlo_id     IN     NUMBER
                               ,p_col_ar_onbe IN     t_col_ar
                               ,x_imob_trow   IN OUT cg$kgc_im_onderzoek_betrokkene.cg$row_type
                               ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_onderzoek_betrokkene.slct(x_imob_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_imob_trow.immo_id         := get_att(p_col_ar_onbe, 'immo_id');
      x_imob_trow.imfr_id         := get_att(p_col_ar_onbe, 'imfr_id');
      x_imob_trow.impe_id         := get_att(p_col_ar_onbe, 'impe_id');
      x_imob_trow.imfo_id         := get_att(p_col_ar_onbe, 'imfo_id');
      x_imob_trow.opmerking       := get_att(p_col_ar_onbe, 'opmerking');
      x_imob_trow.extra1          := get_att(p_col_ar_onbe, 'extra1');

      IF x_imob_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_onderzoek_betrokkene.ins(cg$rec => x_imob_trow
                                           ,cg$ind => cg$kgc_im_onderzoek_betrokkene.cg$ind_true);
      ELSE
         cg$kgc_im_onderzoek_betrokkene.upd(cg$rec => x_imob_trow
                                           ,cg$ind => cg$kgc_im_onderzoek_betrokkene.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_onde_betr;


   PROCEDURE merge_im_onderzoek_indicaties(p_imlo_id     IN NUMBER
                                          ,p_col_ar_onin IN t_col_ar
                                          ,x_imoi_trow   IN OUT cg$kgc_im_onderzoek_indicaties.cg$row_type
                                          ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_onderzoek_indicaties.slct(x_imoi_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;
      x_imoi_trow.impw_id_indi    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_indi
                                                         ,p_externe_id         => get_att(p_col_ar_onin, 'indi_id')
                                                         ,p_hint               => get_att(p_col_ar_onin, 'indi_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imoi_trow.impw_id_ingr    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ingr
                                                         ,p_externe_id         => get_att(p_col_ar_onin, 'ingr_id')
                                                         ,p_hint               => get_att(p_col_ar_onin, 'ingr_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imoi_trow.volgorde        := get_att(p_col_ar_onin, 'volgorde');
      x_imoi_trow.opmerking       := get_att(p_col_ar_onin, 'opmerking');

      x_imoi_trow.impw_id_onre    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_onre
                                                         ,p_externe_id         => get_att(p_col_ar_onin, 'onre_id')
                                                         ,p_hint               => get_att(p_col_ar_onin, 'onre_omschrijving')
                                                         ,p_toevoegen          => TRUE);

      IF x_imoi_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_onderzoek_indicaties.ins(cg$rec => x_imoi_trow
                                 ,cg$ind => cg$kgc_im_onderzoek_indicaties.cg$ind_true);
      ELSE
         cg$kgc_im_onderzoek_indicaties.upd(cg$rec => x_imoi_trow
                                 ,cg$ind => cg$kgc_im_onderzoek_indicaties.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_onderzoek_indicaties;

   PROCEDURE merge_im_metingen(p_imlo_id     IN NUMBER
                              ,p_col_ar_meti IN t_col_ar
                              ,x_imme_trow   IN OUT cg$kgc_im_metingen.cg$row_type
                              ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_metingen.slct(x_imme_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;
-- nvt  impw_id_stan           number(10),

      x_imme_trow.datum_aanmelding := get_att(p_col_ar_meti, 'datum_aanmelding');
      x_imme_trow.afgerond        := get_att(p_col_ar_meti, 'afgerond');
      x_imme_trow.herhalen        := get_att(p_col_ar_meti, 'herhalen');
      x_imme_trow.impw_id_snelheid := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_dom_meti_snelheid
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ingr
                                                         ,p_externe_id         => get_att(p_col_ar_meti, 'snelheid')
                                                         ,p_hint               => NULL
                                                         ,p_toevoegen          => TRUE);
      x_imme_trow.prioriteit      := get_att(p_col_ar_meti, 'prioriteit');
      x_imme_trow.datum_afgerond  := get_att(p_col_ar_meti, 'datum_afgerond');
      x_imme_trow.datum_conclusie := get_att(p_col_ar_meti, 'datum_conclusie');
      x_imme_trow.setnaam         := get_att(p_col_ar_meti, 'setnaam');
      x_imme_trow.opmerking_algemeen := get_att(p_col_ar_meti, 'opmerking_algemeen');
      x_imme_trow.opmerking_analyse := get_att(p_col_ar_meti, 'opmerking_analyse');
      x_imme_trow.datum_selectie_robot := get_att(p_col_ar_meti, 'datum_selectie_robot');
      x_imme_trow.datum_uitslag_robot := get_att(p_col_ar_meti, 'datum_uitslag_robot');
      x_imme_trow.declareren      := get_att(p_col_ar_meti, 'declareren');
      x_imme_trow.impw_id_stgr    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_stgr
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ingr
                                                         ,p_externe_id         => get_att(p_col_ar_meti, 'stgr_id')
                                                         ,p_hint               => get_att(p_col_ar_meti, 'stgr_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imme_trow.impw_id_mede    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ingr
                                                         ,p_externe_id         => get_att(p_col_ar_meti, 'mede_id')
                                                         ,p_hint               => get_att(p_col_ar_meti, 'mede_naam')
                                                         ,p_toevoegen          => TRUE);
      x_imme_trow.analysenr       := get_att(p_col_ar_meti, 'analysenr');
      x_imme_trow.datum_analyse   := get_att(p_col_ar_meti, 'datum_analyse');
      x_imme_trow.impw_id_mede_analyse2 := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ingr
                                                         ,p_externe_id         => get_att(p_col_ar_meti, 'mede_id_analyse2')
                                                         ,p_hint               => get_att(p_col_ar_meti, 'mede_naam_analyse2')
                                                         ,p_toevoegen          => TRUE);
      x_imme_trow.datum_analyse2  := get_att(p_col_ar_meti, 'datum_analyse2');
      x_imme_trow.impw_id_mede_controle := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
--                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ingr
                                                         ,p_externe_id         => get_att(p_col_ar_meti, 'mede_id_controle')
                                                         ,p_hint               => get_att(p_col_ar_meti, 'mede_naam_controle')
                                                         ,p_toevoegen          => TRUE);
      x_imme_trow.geplande_einddatum := get_att(p_col_ar_meti, 'geplande_einddatum');
      x_imme_trow.conclusie       := get_att(p_col_ar_meti, 'conclusie');

      IF x_imme_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_metingen.ins(cg$rec => x_imme_trow
                               ,cg$ind => cg$kgc_im_metingen.cg$ind_true);
      ELSE
         cg$kgc_im_metingen.upd(cg$rec => x_imme_trow
                               ,cg$ind => cg$kgc_im_metingen.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_metingen;

   PROCEDURE merge_im_uitslagen(p_imlo_id     IN NUMBER
                               ,p_col_ar_uits IN t_col_ar
                               ,x_imui_trow   IN OUT cg$kgc_im_uitslagen.cg$row_type
                               ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_uitslagen.slct(x_imui_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_imui_trow.impw_id_kafd    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_kafd
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'kafd_id')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'kafd_naam')
                                                         ,p_toevoegen          => TRUE);
      x_imui_trow.impw_id_rela    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'rela_id')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'rela_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imui_trow.impw_id_brty    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_brty
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'brty_id')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'brty_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imui_trow.impw_id_mede    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'mede_id')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'mede_naam')
                                                         ,p_toevoegen          => TRUE);
      x_imui_trow.datum_uitslag   := get_att(p_col_ar_uits, 'datum_uitslag');
      x_imui_trow.datum_autorisatie := get_att(p_col_ar_uits, 'datum_autorisatie');
      x_imui_trow.volledig_adres  := get_att(p_col_ar_uits, 'volledig_adres');
      x_imui_trow.tekst           := get_att(p_col_ar_uits, 'tekst');
      x_imui_trow.commentaar      := get_att(p_col_ar_uits, 'commentaar');
      x_imui_trow.toelichting     := get_att(p_col_ar_uits, 'toelichting');
      x_imui_trow.impw_id_mede2   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'mede_id2')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'mede_naam2')
                                                         ,p_toevoegen          => TRUE);
      x_imui_trow.datum_mede_id   := get_att(p_col_ar_uits, 'datum_mede_id');
      x_imui_trow.datum_mede_id2  := get_att(p_col_ar_uits, 'datum_mede_id2');
      x_imui_trow.impw_id_mede3   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'mede_id3')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'mede_naam3')
                                                         ,p_toevoegen          => TRUE);
      x_imui_trow.datum_mede_id3  := get_att(p_col_ar_uits, 'datum_mede_id3');
      x_imui_trow.impw_id_mede4   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_mede
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'mede_id4')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'mede_naam4')
                                                         ,p_toevoegen          => TRUE);
      x_imui_trow.datum_mede_id4  := get_att(p_col_ar_uits, 'datum_mede_id4');

      x_imui_trow.bestandsnaam    := get_att(p_col_ar_uits, 'bestandsnaam');
      x_imui_trow.impw_id_btyp    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_btyp
                                                         ,p_externe_id         => get_att(p_col_ar_uits, 'btyp_id')
                                                         ,p_hint               => get_att(p_col_ar_uits, 'btyp_omschrijving')
                                                         ,p_toevoegen          => TRUE);

      x_imui_trow.datum_brie_print := get_att(p_col_ar_uits, 'datum_brie_print');

      IF x_imui_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_uitslagen.ins(cg$rec => x_imui_trow
                                 ,cg$ind => cg$kgc_im_uitslagen.cg$ind_true);
      ELSE
         cg$kgc_im_uitslagen.upd(cg$rec => x_imui_trow
                                 ,cg$ind => cg$kgc_im_uitslagen.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_uitslagen;

   PROCEDURE merge_im_families(p_imlo_id     IN     NUMBER
                              ,p_col_ar_fami IN     t_col_ar
                              ,x_imfa_trow   IN OUT cg$kgc_im_families.cg$row_type
                               ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_families.slct(x_imfa_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_imfa_trow.naam           := get_att(p_col_ar_fami, 'naam');
      x_imfa_trow.archief        := get_att(p_col_ar_fami, 'archief');
      x_imfa_trow.familienummer  := get_att(p_col_ar_fami, 'familienummer');
      x_imfa_trow.opmerkingen    := get_att(p_col_ar_fami, 'opmerkingen');
      x_imfa_trow.extra1         := get_att(p_col_ar_fami, 'extra1');

      IF x_imfa_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_families.ins(cg$rec => x_imfa_trow
                                ,cg$ind => cg$kgc_im_families.cg$ind_true);
      ELSE
         cg$kgc_im_families.upd(cg$rec => x_imfa_trow
                                ,cg$ind => cg$kgc_im_families.cg$ind_true);
      END IF;
      -- eventuele exception door naar bovenliggend nivo
   END merge_im_families;

   PROCEDURE merge_im_familie_onderzoeken(p_imlo_id     IN     NUMBER
                                         ,p_imfa_id     IN     VARCHAR2
                                         ,p_col_ar_faon IN     t_col_ar
                                         ,x_imfz_trow   IN OUT cg$kgc_im_familie_onderzoeken.cg$row_type
                                         ) IS
      v_att_naam           VARCHAR2(32);
   BEGIN
      BEGIN
         cg$kgc_im_familie_onderzoeken.slct(x_imfz_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      x_imfz_trow.imfa_id         := p_imfa_id;
      x_imfz_trow.impw_id_kafd    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_kafd
                                                         ,p_externe_id         => get_att(p_col_ar_faon, 'kafd_id')
                                                         ,p_hint               => get_att(p_col_ar_faon, 'kafd_naam')
                                                         ,p_toevoegen          => TRUE);
      x_imfz_trow.impw_id_ongr    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_ongr
                                                         ,p_externe_id         => get_att(p_col_ar_faon, 'ongr_id')
                                                         ,p_hint               => get_att(p_col_ar_faon, 'ongr_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imfz_trow.impw_id_rela    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                         ,p_externe_id         => get_att(p_col_ar_faon, 'rela_id')
                                                         ,p_hint               => get_att(p_col_ar_faon, 'rela_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imfz_trow.familieonderzoeknr := get_att(p_col_ar_faon, 'familieonderzoeknr');
      x_imfz_trow.datum           := get_att(p_col_ar_faon, 'datum');
      x_imfz_trow.impw_id_onderzoekstype  := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                                 ,p_helix_table_domain => kgc_import.g_cimpw_dom_onderzoekstypen
--                                                                 ,p_helix_table_domain => kgc_import.g_cimpw_dom_onderzoekstypen
                                                                 ,p_externe_id         => get_att(p_col_ar_faon, 'onderzoekstype')
                                                                 ,p_hint               => NULL
                                                                 ,p_toevoegen          => TRUE);
      x_imfz_trow.afgerond        := get_att(p_col_ar_faon, 'afgerond');
      x_imfz_trow.spoed           := get_att(p_col_ar_faon, 'spoed');
      x_imfz_trow.impw_id_onderzoekswijze := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                                 ,p_helix_table_domain => kgc_import.g_cimpw_tab_onwy
                                                                 ,p_externe_id         => get_att(p_col_ar_faon, 'onderzoekswijze')
                                                                 ,p_hint               => NULL
                                                                 ,p_toevoegen          => TRUE);


      x_imfz_trow.geplande_einddatum := get_att(p_col_ar_faon, 'geplande_einddatum');
      x_imfz_trow.referentie      := get_att(p_col_ar_faon, 'referentie');
      x_imfz_trow.omschrijving    := get_att(p_col_ar_faon, 'omschrijving');
      x_imfz_trow.impw_id_indi    := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_indi
                                                         ,p_externe_id         => get_att(p_col_ar_faon, 'indi_id')
                                                         ,p_hint               => get_att(p_col_ar_faon, 'indi_omschrijving')
                                                         ,p_toevoegen          => TRUE);
      x_imfz_trow.extra1         := get_att(p_col_ar_faon, 'extra1');

      IF x_imfz_trow.the_rowid IS NULL
      THEN
         cg$kgc_im_familie_onderzoeken.ins(cg$rec => x_imfz_trow
                                ,cg$ind => cg$kgc_im_familie_onderzoeken.cg$ind_true);
      ELSE
         cg$kgc_im_familie_onderzoeken.upd(cg$rec => x_imfz_trow
                                ,cg$ind => cg$kgc_im_familie_onderzoeken.cg$ind_true);
      END IF;
   END merge_im_familie_onderzoeken;

   PROCEDURE merge_bestanden(x_best_trow IN OUT cg$kgc_bestanden.cg$row_type
                            ) IS
      CURSOR c_best IS
         SELECT best_id
           FROM kgc_bestanden best
          WHERE best.entiteit_code = x_best_trow.entiteit_code
            AND best.entiteit_pk = x_best_trow.entiteit_pk
            AND best.btyp_id = x_best_trow.btyp_id
            AND UPPER(best.bestand_specificatie) = UPPER(x_best_trow.bestand_specificatie)
         ;
      r_best c_best%ROWTYPE;
   BEGIN
      -- eventueel bestaande gegevens ophalen
      OPEN c_best;
      FETCH c_best INTO r_best;
      IF c_best%FOUND
      THEN
         x_best_trow.best_id := r_best.best_id;
      END IF;
      CLOSE c_best;
      BEGIN
         cg$kgc_bestanden.slct(x_best_trow); -- eventueel bestaande gegevens ophalen
      EXCEPTION
         WHEN OTHERS THEN -- no data found, maar verpakt in 20999...
            cg$errors.clear;
      END;

      IF x_best_trow.best_id IS NULL
      THEN
         cg$kgc_bestanden.ins(cg$rec => x_best_trow
                                ,cg$ind => cg$kgc_bestanden.cg$ind_true);
      ELSE
         cg$kgc_bestanden.upd(cg$rec => x_best_trow
                                ,cg$ind => cg$kgc_bestanden.cg$ind_true);
      END IF;
   END merge_bestanden;


-- globale functies
   -- wrapper voor blob_naar_bestand_java; deze geeft in forms een internal error bij compileren!
   FUNCTION blob_naar_bestand ( p_file   IN   VARCHAR2
                              , p_blob   IN   BLOB
                              ) RETURN VARCHAR2 IS
   BEGIN
      RETURN kgc_file.blob_naar_bestand(p_file, p_blob);
   END blob_naar_bestand;

   FUNCTION delete_bestand (p_file   IN   VARCHAR2
                           ) RETURN VARCHAR2 IS
   BEGIN
      RETURN kgc_file.delete_bestand(p_file);
   END delete_bestand;

   FUNCTION copy_bestand (p_file_van   IN   VARCHAR2
                         ,p_file_naar  IN   VARCHAR2
                         ) RETURN VARCHAR2 IS
   BEGIN
      RETURN kgc_file.copy_bestand(p_file_van, p_file_naar);
   END copy_bestand;

   -- vertaal xml naar kgc_im-tabellen
   FUNCTION lees_in(b_interface IN     VARCHAR
                   ,b_xml       IN     CLOB
                   ,b_pdf       IN     BLOB
                   ,x_verslag   IN OUT VARCHAR2
                   ,b_imfi_id   IN     NUMBER := NULL
                   ) RETURN BOOLEAN IS
      CURSOR c_imlo (p_code VARCHAR2) IS
         SELECT *
         FROM   kgc_import_locaties imlo
         WHERE  imlo.directorynaam = p_code;
      CURSOR c_imlo2 (p_imfi_id NUMBER) IS
         SELECT imlo.*
           FROM kgc_import_locaties imlo
              , kgc_import_files imfi
          WHERE imfi.imfi_id = p_imfi_id
            AND imfi.imlo_id = imlo.imlo_id;
      r_imlo c_imlo%ROWTYPE;
      -- let op: een generieke 'importfile' wordt geidentificeerd door het eerste stuk voor de spatie!
      CURSOR c_imfi (p_imlo_id NUMBER, p_filenaam VARCHAR2) IS
         SELECT imfi.imfi_id
         ,      imfi.import_datum
         ,      imfi.inhoud
         FROM   kgc_import_files imfi
         WHERE  imfi.imlo_id = p_imlo_id
         AND    imfi.filenaam like p_filenaam || ' %';
      r_imfi                 c_imfi%ROWTYPE;
      CURSOR c_imfi2 (p_imfi_id NUMBER) IS
         SELECT imfi.imfi_id
         ,      imfi.import_datum
         ,      imfi.inhoud
         FROM   kgc_import_files imfi
         WHERE  imfi.imfi_id = p_imfi_id;
      CURSOR c_btyp (p_code VARCHAR2) IS
         SELECT *
         FROM   kgc_bestand_types btyp
         WHERE  code = p_code
         ;
      r_btyp c_btyp%ROWTYPE;
      v_xmlp                 xmlparser.parser;
      v_xmldoc               xmldom.domdocument;
      v_xmln0                xmldom.domnode;
      v_xmln1                xmldom.domnode;
      v_xmln2                xmldom.domnode;
      v_xmlnl_pers1          xmldom.domnodelist;
      v_xmlnl_persN          xmldom.domnodelist;
      v_xmlnl_zwan1          xmldom.domnodelist;
      v_xmlnl_zwanN          xmldom.domnodelist;
      v_xmlnl_foet1          xmldom.domnodelist;
      v_xmlnl_foetN          xmldom.domnodelist;
      v_xmlnl_mons1          xmldom.domnodelist;
      v_xmlnl_monsN          xmldom.domnodelist;
      v_xmlnl_frac1          xmldom.domnodelist;
      v_xmlnl_fracN          xmldom.domnodelist;
      v_xmlnl_onde1          xmldom.domnodelist;
      v_xmlnl_ondeN          xmldom.domnodelist;
      v_xmlnl_onin1          xmldom.domnodelist;
      v_xmlnl_oninN          xmldom.domnodelist;
      v_xmlnl_onkh1          xmldom.domnodelist;
      v_xmlnl_onkhN          xmldom.domnodelist;
      v_xmlnl_onbe1          xmldom.domnodelist;
      v_xmlnl_onbeN          xmldom.domnodelist;
      v_xmlnl_meti1          xmldom.domnodelist;
      v_xmlnl_metiN          xmldom.domnodelist;
      v_xmlnl_uits1          xmldom.domnodelist;
      v_xmlnl_uitsN          xmldom.domnodelist;
      v_xmlnl_fami1          xmldom.domnodelist;
      v_xmlnl_famiN          xmldom.domnodelist;
      v_xmlnl_faon1          xmldom.domnodelist;
      v_xmlnl_faonN          xmldom.domnodelist;
      v_impe_trow            cg$kgc_im_personen.cg$row_type;
      v_impe_trow_null       cg$kgc_im_personen.cg$row_type;
      v_imzw_trow            cg$kgc_im_zwangerschappen.cg$row_type;
      v_imzw_trow_null       cg$kgc_im_zwangerschappen.cg$row_type;
      v_imfo_trow            cg$kgc_im_foetussen.cg$row_type;
      v_imfo_trow_null       cg$kgc_im_foetussen.cg$row_type;
      v_immo_trow            cg$kgc_im_monsters.cg$row_type;
      v_immo_trow_null       cg$kgc_im_monsters.cg$row_type;
      v_imfr_trow            cg$kgc_im_fracties.cg$row_type;
      v_imfr_trow_null       cg$kgc_im_fracties.cg$row_type;
      v_imon_trow            cg$kgc_im_onderzoeken.cg$row_type;
      v_imon_trow_null       cg$kgc_im_onderzoeken.cg$row_type;
      v_imoi_trow            cg$kgc_im_onderzoek_indicaties.cg$row_type;
      v_imoi_trow_null       cg$kgc_im_onderzoek_indicaties.cg$row_type;
      v_imok_trow            cg$kgc_im_onde_kopie.cg$row_type;
      v_imok_trow_null       cg$kgc_im_onde_kopie.cg$row_type;
      v_imob_trow            cg$kgc_im_onderzoek_betrokkene.cg$row_type;
      v_imob_trow_null       cg$kgc_im_onderzoek_betrokkene.cg$row_type;
      v_imme_trow            cg$kgc_im_metingen.cg$row_type;
      v_imme_trow_null       cg$kgc_im_metingen.cg$row_type;
      v_imui_trow            cg$kgc_im_uitslagen.cg$row_type;
      v_imui_trow_null       cg$kgc_im_uitslagen.cg$row_type;
      v_imfa_trow            cg$kgc_im_families.cg$row_type;
      v_imfa_trow_null       cg$kgc_im_families.cg$row_type;
      v_imfz_trow            cg$kgc_im_familie_onderzoeken.cg$row_type;
      v_imfz_trow_null       cg$kgc_im_familie_onderzoeken.cg$row_type;
      v_best_trow            cg$kgc_bestanden.cg$row_type;
      v_att_naam             VARCHAR2(32);
      v_col_ar_pers          t_col_ar;
      v_col_ar_zwan          t_col_ar;
      v_col_ar_foet          t_col_ar;
      v_col_ar_mons          t_col_ar;
      v_col_ar_frac          t_col_ar;
      v_col_ar_onde          t_col_ar;
      v_col_ar_onin          t_col_ar;
      v_col_ar_onkh          t_col_ar;
      v_col_ar_onbe          t_col_ar;
      v_col_ar_meti          t_col_ar;
      v_col_ar_uits          t_col_ar;
      v_col_ar_fami          t_col_ar;
      v_col_ar_faon          t_col_ar;
      v_imfi_found           BOOLEAN;
      v_imfi_identificatie   VARCHAR2(1000);
      v_imfi_filenaam        kgc_import_files.filenaam%TYPE;
      v_imfi_opmerkingen     kgc_import_files.opmerkingen%TYPE;
      v_imfi_specifieke_verwerking kgc_import_files.specifieke_verwerking%TYPE;
      v_locatie              kgc_bestanden.locatie%TYPE;
      v_bestandsnaam         kgc_bestanden.bestandsnaam%TYPE;
      v_file_result          VARCHAR2(32767);
      v_msg                  VARCHAR2(4000);
      v_sql                  VARCHAR2(32767);
      v_count                NUMBER;
      v_ok                   BOOLEAN;
      v_lees_in_ok           BOOLEAN := TRUE;
      v_direct_starten       BOOLEAN;
      v_blokkeer_direct_starten BOOLEAN := FALSE;
      v_debug                VARCHAR2(255);
      e_lees_in_fout         EXCEPTION;
      v_attr                 kgc_attributen.code%TYPE;
      v_atwa                 kgc_attribuut_waarden.waarde%TYPE;
   BEGIN
 
      v_debug := '1';
      v_xmlp := xmlparser.newparser;
      xmlparser.setvalidationmode(v_xmlp
                                 ,FALSE);
      -- check: interface wordt gebruikt, tenzij imfi_id is opgegeven!
      IF b_imfi_id IS NULL
      THEN
         v_debug := v_debug || 'a';
         OPEN c_imlo(p_code => b_interface);
         FETCH c_imlo INTO r_imlo;
         IF c_imlo%NOTFOUND
         THEN
            v_debug := v_debug || 'c';
            raise_application_error(-20000
                                   ,'Het opgegeven import.interface ''' || b_interface || ''' is niet in Helix bekend als import-locatie!');
         END IF;
         CLOSE c_imlo;
      ELSE
         v_debug := v_debug || 'k';
         OPEN c_imlo2(p_imfi_id => b_imfi_id);
         FETCH c_imlo2 INTO r_imlo;
         IF c_imlo2%NOTFOUND
         THEN
            v_debug := v_debug || 'm';
            raise_application_error(-20000
                                   ,'De opgegeven identificatie ''' || b_imfi_id || ''' hoort in Helix niet bij een import-locatie!');
         END IF;
         v_debug := v_debug || 'o';
         CLOSE c_imlo2;
      END IF;
      -- parse:
      v_debug := v_debug || 'z';
      xmlparser.parseclob(v_xmlp
                         ,b_xml);
      -- lees document
      v_xmldoc := xmlparser.getdocument(v_xmlp);
      v_xmln0  := xmldom.makenode(xmldom.getdocumentelement(v_xmldoc));
      -- gevonden:
      IF lower(xmldom.getnodename(v_xmln0)) <> 'import'
      THEN
         raise_application_error(-20000
                                ,'Error in xml: ''import''-tag niet gevonden!');
      END IF;
      -- identificatie ophalen
      v_debug := v_debug || '2';
      -- bv <import identificatie="ONDE1018197" fase="F1" start="N" stap="NA" attr1="ONDE_EXTRA1" atwa1="ATTR_ONDERZOEKNR_KGCN" attr2="METI_EXTRA1" atwa2="ATTR_METI_ID_KGCN">
      v_imfi_identificatie := xmldom.getattribute(xmldom.makeelement(v_xmln0)
                                                 ,'identificatie');
      IF NVL(LENGTH(v_imfi_identificatie), 0) = 0
      THEN
         raise_application_error(-20000
                                ,'Error in xml: bij ''import''-tag is geen ''idenficatie'' opgenomen!');
      END IF;
      dbms_output.put_line('> interface: ' || b_interface || '; id=' || v_imfi_identificatie);
      v_imfi_filenaam := SUBSTR(v_imfi_identificatie || ' (p=<P>; a=<A>)', 1, 100); -- let op: later wordt nog persoon en afdeling toegevoegd!
      -- specifieke verwerking - mantis 11620
      v_imfi_specifieke_verwerking := SUBSTR( xmldom.getattribute(xmldom.makeelement(v_xmln0),'fase')
                                              || '#' || xmldom.getattribute(xmldom.makeelement(v_xmln0),'start')
                                              || '#' || xmldom.getattribute(xmldom.makeelement(v_xmln0),'stap')
                                            , 1, 10);

      -- importfile opnemen/evt bijwerken
      v_debug := v_debug || '4';
      IF b_imfi_id IS NULL
      THEN
         OPEN c_imfi(r_imlo.imlo_id, v_imfi_identificatie);
         FETCH c_imfi INTO r_imfi;
         v_imfi_found := c_imfi%FOUND;
         CLOSE c_imfi;
      ELSE
         OPEN c_imfi2(p_imfi_id => b_imfi_id);
         FETCH c_imfi2 INTO r_imfi;
         v_imfi_found := c_imfi2%FOUND;
         CLOSE c_imfi2;
      END IF;
      IF NOT v_imfi_found
      THEN
         -- toevoegen!
         -- nieuw import-id uitgeven
         SELECT nvl(MAX(imfi.imfi_id)
                   ,0) + 1
         INTO   r_imfi.imfi_id
         FROM   kgc_import_files imfi;
         INSERT INTO kgc_import_files
            (imfi_id
            ,imlo_id
            ,filenaam
            ,import_datum
            ,specifieke_verwerking
            ,inhoud) ----added by Anuja against Mantis 9052
         VALUES
            (r_imfi.imfi_id
            ,r_imlo.imlo_id
            ,v_imfi_filenaam
            ,trunc(SYSDATE)
            ,v_imfi_specifieke_verwerking
            ,b_xml); ----added by Anuja against Mantis 9052
      ELSE
         -- bijwerken mag in principe WEL (bv: via scherm)
--         raise_application_error(-20000
--                                ,'Het verzoek met import.identificatie ''' || v_imfi_identificatie || ''' bestaat reeds!');
         UPDATE kgc_import_files imfi
            SET import_datum = trunc(SYSDATE)
               ,afgehandeld  = 'N'
               ,opmerkingen  = NULL
               ,filenaam     = v_imfi_filenaam
               ,specifieke_verwerking = v_imfi_specifieke_verwerking
          WHERE imfi.imfi_id = r_imfi.imfi_id;
      END IF;
      -- extra attributen?
      -- xml is bv <import identificatie="ONDE1018197" fase="F1" start="N" stap="NA" attr1="ONDE_EXTRA1" atwa1="ATTR_ONDERZOEKNR_KGCN" attr2="METI_EXTRA1" atwa2="ATTR_METI_ID_KGCN">
      FOR i_extra IN 1 .. 10 LOOP
         v_attr := xmldom.getattribute(xmldom.makeelement(v_xmln0)
                                      ,'attr' || i_extra);
         IF NVL(LENGTH(v_attr), 0) = 0
         THEN
            EXIT; -- voorbij de laatst-gevulde
         END IF;
         v_atwa := xmldom.getattribute(xmldom.makeelement(v_xmln0)
                                      ,'atwa' || i_extra);
         -- opnemen bij imfi
         kgc_attr_00.set_waarde('KGC_IMPORT_FILES', v_attr, r_imfi.imfi_id, v_atwa);
      END LOOP;

      SAVEPOINT save_imfi;
      BEGIN
         -- persoon verwerken:
         v_xmlnl_persN := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln0)
                                                    ,'personen');
         IF xmldom.getlength(v_xmlnl_persN) != 1
         THEN
            kgc_import.addtext(v_imfi_opmerkingen
                              ,2000
                              ,'Het aantal nodes met de tag ''personen'' is ' || xmldom.getlength(v_xmlnl_persn) || '; er moet er precies 1 aanwezig zijn!');
            RAISE e_lees_in_fout;
         END IF;
         v_xmln1 := xmldom.item(v_xmlnl_persn
                               ,0);
         v_xmlnl_pers1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                    ,'persoon');
         dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_pers1)) || ' pers children found');
/*         IF xmldom.getlength(v_xmlnl_pers1) != 1
         THEN
            kgc_import.addtext(v_imfi_opmerkingen
                              ,2000
                              ,'Het aantal nodes met de tag ''personen'' is ' || xmldom.getlength(v_xmlnl_pers1) || '; er moet er precies 1 aanwezig zijn!');
            RAISE e_lees_in_fout;
         END IF;
*/
         -- alle personen verwerken
         FOR i_pers IN 0 .. xmldom.getlength(v_xmlnl_pers1) - 1 LOOP -- rde 10-12-2015 mantis 11620
            --*** PERSOON verwerken
            v_debug := v_debug || '3';
            xml2ar(p_xmln => xmldom.item(v_xmlnl_pers1, i_pers), p_col_ar => v_col_ar_pers);
            v_debug := v_debug || 'a';
            IF get_att(v_col_ar_pers,'impe_id') IS NULL
            THEN -- automatisch uitgeven:
               v_col_ar_pers('impe_id') := 'PERS' || trim(to_char(i_pers + 1, '09'));
            END IF;
            -- filenaam aanpassen
            v_imfi_filenaam := SUBSTR(REPLACE(v_imfi_filenaam, '<P>', get_att(v_col_ar_pers,'impe_id')), 1, 100);

            -- persoon verwerken
            v_debug := v_debug || '5';
            v_impe_trow := v_impe_trow_null;
            v_impe_trow.imfi_id := r_imfi.imfi_id;
            v_impe_trow.impe_id := get_att(v_col_ar_pers,'impe_id');
            v_debug := v_debug || 'c';
            merge_im_personen(p_imlo_id     => r_imlo.imlo_id
                             ,p_col_ar_pers => v_col_ar_pers
                             ,x_impe_trow   => v_impe_trow
                             );
            v_msg := NULL;
            IF r_imlo.zisnr_overnemen = 'J' -- persoonsgegevens ophalen adhv zisnr?
            THEN
               IF NOT persoon_im_in_zis(p_imlo_id   => r_imlo.imlo_id
                                       ,x_impe_trow => v_impe_trow
                                       ,x_msg       => v_msg)
               THEN
                  kgc_import.addtext(v_imfi_opmerkingen
                                    ,2000
                                    ,v_msg);
                  RAISE e_lees_in_fout;
               ELSIF NVL(LENGTH(v_msg), 0) > 0 -- Rework Mantis 3184: Interface voor Dragon
               THEN
                  kgc_import.addtext(v_imfi_opmerkingen
                                    ,2000
                                    ,v_msg);
                  v_blokkeer_direct_starten := TRUE;
               END IF;
            END IF;

            --*** zwangerschap
            v_debug := v_debug || '6';
            v_imzw_trow := v_imzw_trow_null;
            v_xmlnl_foet1 := NULL; -- wordt voor iedere persoon gebruikt
            v_xmlnl_zwanN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_pers1, i_pers))
                                                        ,'zwangerschappen');
            IF NOT xmldom.isNull(v_xmlnl_zwanN)
            THEN
               IF xmldom.getlength(v_xmlnl_zwanN) > 0
               THEN
                  v_xmln1 := xmldom.item(v_xmlnl_zwanN
                                        ,0);
                  v_xmlnl_zwan1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                             ,'zwangerschap');
                  IF NOT xmldom.isNull(v_xmlnl_zwan1)
                  THEN
                     dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_zwan1)) || ' zwan children found');
                     IF xmldom.getlength(v_xmlnl_zwan1) > 0
                     THEN
                        xml2ar(p_xmln => xmldom.item(v_xmlnl_zwan1, 0), p_col_ar => v_col_ar_zwan);
                        IF get_att(v_col_ar_zwan,'imzw_id') IS NULL
                        THEN -- automatisch uitgeven:
                           v_col_ar_zwan('imzw_id') := 'ZWAN01';
                        END IF;
                        v_imzw_trow.imfi_id := r_imfi.imfi_id;
                        v_imzw_trow.imzw_id := get_att(v_col_ar_zwan,'imzw_id');
                        v_debug := v_debug || 'c';
                        merge_im_zwangerschappen(p_imlo_id     => r_imlo.imlo_id
                                                ,p_impe_id     => v_impe_trow.impe_id
                                                ,p_col_ar_zwan => v_col_ar_zwan
                                                ,x_imzw_trow   => v_imzw_trow
                                                );
                     END IF;
                     -- foetussen - dez kunnen onder de zwangerschap hangen of onder de persoon (1 van beide!)
                     v_debug := v_debug || '7';
                     v_imfo_trow := v_imfo_trow_null;
                     -- probeer foetussen te vinden onder zwangerschappen
                     v_xmlnl_foetN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_zwan1, 0))
                                                                 ,'foetussen');
                     IF NOT xmldom.isNull(v_xmlnl_foetN)
                     THEN
                        IF xmldom.getlength(v_xmlnl_foetN) > 0
                        THEN
                           v_xmln1 := xmldom.item(v_xmlnl_foetN
                                                 ,0);
                           v_xmlnl_foet1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                      ,'foetus');
                        END IF;
                     END IF;
                  END IF;
               END IF;
            END IF;

            -- probeer foetussen te vinden onder personen
            v_debug := v_debug || '8';
            IF xmldom.isNull(v_xmlnl_foet1)
            THEN
               v_xmlnl_foetN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_pers1, i_pers))
                                                           ,'foetussen');
               IF NOT xmldom.isNull(v_xmlnl_foetN)
               THEN
                  IF xmldom.getlength(v_xmlnl_foetN) > 0
                  THEN
                     v_xmln1 := xmldom.item(v_xmlnl_foetN
                                           ,0);
                     v_xmlnl_foet1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                ,'foetus');
                  END IF;
               END IF;
            END IF;

            IF NOT xmldom.isNull(v_xmlnl_foet1)
            THEN
               v_debug := v_debug || '9';
               dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_foet1)) || ' foet children found');
               IF xmldom.getlength(v_xmlnl_foet1) > 0
               THEN -- alle foetussen!
                  FOR i IN 0 .. xmldom.getlength(v_xmlnl_foet1) - 1
                  LOOP
                     v_imfo_trow := v_imfo_trow_null;
                     xml2ar(p_xmln => xmldom.item(v_xmlnl_foet1, i), p_col_ar => v_col_ar_foet);
                     IF get_att(v_col_ar_foet,'imfo_id') IS NULL
                     THEN -- automatisch uitgeven:
                        v_col_ar_foet('imfo_id') := 'FOET' || trim(to_char(i + 1, '09'));
                     END IF;
                     v_imfo_trow.imfi_id := r_imfi.imfi_id;
                     v_imfo_trow.imfo_id := get_att(v_col_ar_foet,'imfo_id');
                     v_debug := v_debug || 'c';
                     merge_im_foetussen(p_imlo_id => r_imlo.imlo_id
                                       ,p_impe_id_moeder => v_impe_trow.impe_id
                                       ,p_imzw_id => v_imzw_trow.imzw_id
                                       ,p_col_ar_foet => v_col_ar_foet
                                       ,x_imfo_trow => v_imfo_trow
                                        );
                  END LOOP;
               END IF;
            END IF;

            --*** monster
            v_debug := v_debug || 'M';
            v_xmlnl_monsN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_pers1, i_pers))
                                                        ,'monsters');
            IF NOT xmldom.isNull(v_xmlnl_monsN)
            THEN
               IF xmldom.getlength(v_xmlnl_monsN) > 0
               THEN
                  v_xmln1 := xmldom.item(v_xmlnl_monsN
                                        ,0);
                  v_xmlnl_mons1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                             ,'monster');
                  IF NOT xmldom.isNull(v_xmlnl_mons1)
                  THEN
                     dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_mons1)) || ' mons children found');
                     IF xmldom.getlength(v_xmlnl_mons1) > 0
                     THEN -- alle monsters!
                        FOR i_mons IN 0 .. xmldom.getlength(v_xmlnl_mons1) - 1
                        LOOP
                           v_immo_trow := v_immo_trow_null;
                           xml2ar(p_xmln => xmldom.item(v_xmlnl_mons1, i_mons), p_col_ar => v_col_ar_mons);
                           IF get_att(v_col_ar_mons,'immo_id') IS NULL
                           THEN -- automatisch uitgeven:
                              v_col_ar_mons('immo_id') := 'MONS' || trim(to_char(i_mons + 1, '09'));
                           END IF;
                           v_immo_trow.imfi_id := r_imfi.imfi_id;
                           v_immo_trow.immo_id := get_att(v_col_ar_mons,'immo_id');
                           v_debug := v_debug || i_mons;
                           merge_im_monsters(p_imlo_id     => r_imlo.imlo_id
                                            ,p_impe_id     => v_impe_trow.impe_id
                                            ,p_col_ar_mons => v_col_ar_mons
                                            ,x_immo_trow   => v_immo_trow
                                            );
                           -- filenaam aanpassen
                           v_imfi_filenaam := SUBSTR(REPLACE(v_imfi_filenaam, '<A>', get_att(v_col_ar_mons,'kafd_id')), 1, 100);
                           --*** mantis 11620 - alle fracties
                           v_debug := v_debug || 'F';
                           v_xmlnl_fracN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_mons1, i_mons))
                                                                       ,'fracties');
                           IF NOT xmldom.isNull(v_xmlnl_fracN)
                           THEN
                              IF xmldom.getlength(v_xmlnl_fracN) > 0
                              THEN
                                 v_xmln1 := xmldom.item(v_xmlnl_fracN
                                                       ,0);
                                 v_xmlnl_frac1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                            ,'fractie');
                                 IF NOT xmldom.isNull(v_xmlnl_frac1)
                                 THEN
                                    dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_frac1)) || ' frac children found');
                                    IF xmldom.getlength(v_xmlnl_frac1) > 0
                                    THEN -- alle fracties!
                                       FOR i_imfr IN 0 .. xmldom.getlength(v_xmlnl_frac1) - 1
                                       LOOP
                                          v_imfr_trow := v_imfr_trow_null;
                                          xml2ar(p_xmln => xmldom.item(v_xmlnl_frac1, i_imfr), p_col_ar => v_col_ar_frac);
                                          IF get_att(v_col_ar_frac,'imfr_id') IS NULL
                                          THEN -- automatisch uitgeven:
                                             v_col_ar_frac('imfr_id') := 'IMFR' || trim(to_char(i_imfr + 1, '09'));
                                          END IF;
                                          v_imfr_trow.imfi_id := r_imfi.imfi_id;
                                          v_imfr_trow.immo_id := get_att(v_col_ar_mons,'immo_id');
                                          v_imfr_trow.imfr_id := get_att(v_col_ar_frac,'imfr_id');
                                          v_debug := v_debug || i_imfr;
                                          merge_im_fracties(p_imlo_id     => r_imlo.imlo_id
                                                           ,p_col_ar_frac => v_col_ar_frac
                                                           ,x_imfr_trow   => v_imfr_trow
                                                           );
                                       END LOOP;
                                    END IF;
                                 END IF;
                              END IF;
                           END IF;
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
            END IF;

            --*** onderzoeken
            v_debug := v_debug || 'O';
            v_xmlnl_ondeN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_pers1, i_pers))
                                                        ,'onderzoeken');
            IF NOT xmldom.isNull(v_xmlnl_ondeN)
            THEN
               IF xmldom.getlength(v_xmlnl_ondeN) > 0
               THEN
                  v_xmln1 := xmldom.item(v_xmlnl_ondeN
                                        ,0);
                  v_xmlnl_onde1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                             ,'onderzoek');
                  IF NOT xmldom.isNull(v_xmlnl_onde1)
                  THEN
                     dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_onde1)) || ' onde children found');
                     IF xmldom.getlength(v_xmlnl_onde1) > 0
                     THEN -- alle onderzoeken!
                        FOR i_onde IN 0 .. xmldom.getlength(v_xmlnl_onde1) - 1
                        LOOP
                           v_imon_trow := v_imon_trow_null;
                           xml2ar(p_xmln => xmldom.item(v_xmlnl_onde1, i_onde), p_col_ar => v_col_ar_onde);
                           v_debug := v_debug || i_onde;
                           IF get_att(v_col_ar_onde,'imon_id') IS NULL
                           THEN -- automatisch uitgeven:
                              v_col_ar_onde('imon_id') := 'ONDE' || trim(to_char(i_onde + 1, '09'));
                           END IF;
                           v_debug := v_debug || 'a';
                           v_imon_trow.imfi_id := r_imfi.imfi_id;
                           v_imon_trow.imon_id := get_att(v_col_ar_onde,'imon_id');
                           v_debug := v_debug || 'b';
                           merge_im_onderzoeken(p_imlo_id     => r_imlo.imlo_id
                                               ,p_impe_id     => v_impe_trow.impe_id
                                               ,p_col_ar_onde => v_col_ar_onde
                                               ,x_imon_trow   => v_imon_trow
                                               );
                           -- filenaam aanpassen
                           v_imfi_filenaam := SUBSTR(REPLACE(v_imfi_filenaam, '<A>', get_att(v_col_ar_onde,'kafd_id')), 1, 100);
                           --*** onderzoek indicaties
                           v_debug := v_debug || 'I';
                           v_xmlnl_oninN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_onde1, i_onde))
                                                                       ,'onderzoek_indicaties');
                           IF NOT xmldom.isNull(v_xmlnl_oninN)
                           THEN
                              IF xmldom.getlength(v_xmlnl_oninN) > 0
                              THEN
                                 v_xmln1 := xmldom.item(v_xmlnl_oninN
                                                       ,0);
                                 v_xmlnl_onin1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                            ,'onderzoek_indicatie');
                                 IF NOT xmldom.isNull(v_xmlnl_onin1)
                                 THEN
                                    dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_onin1)) || ' onin children found');
                                    IF xmldom.getlength(v_xmlnl_onin1) > 0
                                    THEN -- alle onderzoekindicaties!
                                       FOR i_imoi IN 0 .. xmldom.getlength(v_xmlnl_onin1) - 1
                                       LOOP
                                          v_imoi_trow := v_imoi_trow_null;
                                          xml2ar(p_xmln => xmldom.item(v_xmlnl_onin1, i_imoi), p_col_ar => v_col_ar_onin);
                                          IF get_att(v_col_ar_onin,'imoi_id') IS NULL
                                          THEN -- automatisch uitgeven:
                                             v_col_ar_onin('imoi_id') := 'IMOI' || trim(to_char(i_imoi + 1, '09'));
                                          END IF;
                                          v_imoi_trow.imfi_id := r_imfi.imfi_id;
                                          v_imoi_trow.imon_id := get_att(v_col_ar_onde,'imon_id');
                                          v_imoi_trow.imoi_id := get_att(v_col_ar_onin,'imoi_id');
                                          v_debug := v_debug || i_imoi;
                                          merge_im_onderzoek_indicaties(p_imlo_id     => r_imlo.imlo_id
                                                                       ,p_col_ar_onin => v_col_ar_onin
                                                                       ,x_imoi_trow   => v_imoi_trow
                                                                       );
                                       END LOOP;
                                    END IF;
                                 END IF;
                              END IF;
                           END IF;

                           --*** onderzoek kopiehouders
                           v_debug := v_debug || 'K';
                           v_xmlnl_onkhN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_onde1, i_onde))
                                                                       ,'onde_kopieen');
                           IF NOT xmldom.isNull(v_xmlnl_onkhN)
                           THEN
                              IF xmldom.getlength(v_xmlnl_onkhN) > 0
                              THEN
                                 v_xmln1 := xmldom.item(v_xmlnl_onkhN
                                                       ,0);
                                 v_xmlnl_onkh1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                            ,'onde_kopie');
                                 IF NOT xmldom.isNull(v_xmlnl_onkh1)
                                 THEN
                                    dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_onkh1)) || ' onkh children found');
                                    IF xmldom.getlength(v_xmlnl_onkh1) > 0
                                    THEN -- alle onderzoekindicaties!
                                       FOR i_onkh IN 0 .. xmldom.getlength(v_xmlnl_onkh1) - 1
                                       LOOP
                                          v_imok_trow := v_imok_trow_null;
                                          xml2ar(p_xmln => xmldom.item(v_xmlnl_onkh1, i_onkh), p_col_ar => v_col_ar_onkh);
                                          IF get_att(v_col_ar_onkh,'imok_id') IS NULL
                                          THEN -- automatisch uitgeven:
                                             v_col_ar_onkh('imok_id') := 'ONKH' || trim(to_char(i_onkh + 1, '09'));
                                          END IF;
                                          v_imok_trow.imfi_id := r_imfi.imfi_id;
                                          v_imok_trow.imon_id := get_att(v_col_ar_onde,'imon_id');
                                          v_imok_trow.imok_id := get_att(v_col_ar_onkh,'imok_id');
                                          v_debug := v_debug || i_onkh;
                                          merge_im_onde_kopie(p_imlo_id     => r_imlo.imlo_id
                                                             ,p_col_ar_onkh => v_col_ar_onkh
                                                             ,x_imok_trow   => v_imok_trow
                                                             );
                                       END LOOP;
                                    END IF;
                                 END IF;
                              END IF;
                           END IF;

                           --*** onderzoek monster/betrokkenen
                           v_debug := v_debug || '3';
                           v_xmlnl_onbeN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_onde1, i_onde))
                                                                       ,'onde_betrn');
                           IF NOT xmldom.isNull(v_xmlnl_onbeN)
                           THEN
                              IF xmldom.getlength(v_xmlnl_onbeN) > 0
                              THEN
                                 v_xmln1 := xmldom.item(v_xmlnl_onbeN
                                                       ,0);
                                 v_xmlnl_onbe1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                            ,'onde_betr');
                                 IF NOT xmldom.isNull(v_xmlnl_onbe1)
                                 THEN
                                    dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_onbe1)) || ' onmo/onbe children found');
                                    IF xmldom.getlength(v_xmlnl_onbe1) > 0
                                    THEN -- alle onderzoekindicaties!
                                       FOR i_onbe IN 0 .. xmldom.getlength(v_xmlnl_onbe1) - 1
                                       LOOP
                                          v_imob_trow := v_imob_trow_null;
                                          xml2ar(p_xmln => xmldom.item(v_xmlnl_onbe1, i_onbe), p_col_ar => v_col_ar_onbe);
                                          IF get_att(v_col_ar_onbe,'imob_id') IS NULL
                                          THEN -- automatisch uitgeven:
                                             v_col_ar_onbe('imob_id') := 'ONBE' || trim(to_char(i_onbe + 1, '09'));
                                          END IF;
                                          v_imob_trow.imfi_id := r_imfi.imfi_id;
                                          v_imob_trow.imon_id := get_att(v_col_ar_onde,'imon_id');
                                          v_imob_trow.imob_id := get_att(v_col_ar_onbe,'imob_id');
                                          v_debug := v_debug || i_onbe;
                                          merge_im_onde_betr(p_imlo_id     => r_imlo.imlo_id
                                                            ,p_col_ar_onbe => v_col_ar_onbe
                                                            ,x_imob_trow   => v_imob_trow
                                                            );
                                       END LOOP;
                                    END IF;
                                 END IF;
                              END IF;
                           END IF;

                           --*** uitslagen
                           v_debug := v_debug || 'U';
                           v_xmlnl_uitsN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_onde1, i_onde))
                                                                       ,'uitslagen');
                           IF NOT xmldom.isNull(v_xmlnl_uitsN)
                           THEN
                              IF xmldom.getlength(v_xmlnl_uitsN) > 0
                              THEN
                                 v_debug := v_debug || 'a';
                                 v_xmln1 := xmldom.item(v_xmlnl_uitsN
                                                       ,0);
                                 v_xmlnl_uits1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                            ,'uitslag');
                                 IF NOT xmldom.isNull(v_xmlnl_uits1)
                                 THEN
                                    dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_uits1)) || ' uits children found');
                                    IF xmldom.getlength(v_xmlnl_uits1) > 0
                                    THEN -- alle uitslagen!
                                       v_debug := v_debug || '#=' || TO_CHAR(xmldom.getlength(v_xmlnl_uits1) - 1);
                                       FOR i_uits IN 0 .. xmldom.getlength(v_xmlnl_uits1) - 1
                                       LOOP
                                          v_imui_trow := v_imui_trow_null;
                                          xml2ar(p_xmln => xmldom.item(v_xmlnl_uits1, i_uits), p_col_ar => v_col_ar_uits);
                                          IF get_att(v_col_ar_uits,'imui_id') IS NULL
                                          THEN -- automatisch uitgeven:
                                             v_col_ar_uits('imui_id') := 'UITS' || trim(to_char(i_uits + 1, '09'));
                                          END IF;
                                          v_imui_trow.imfi_id := r_imfi.imfi_id;
                                          v_imui_trow.imon_id := get_att(v_col_ar_onde,'imon_id');
                                          v_imui_trow.imui_id := get_att(v_col_ar_uits,'imui_id');
                                          v_debug := v_debug || i_uits;
                                          merge_im_uitslagen(p_imlo_id     => r_imlo.imlo_id
                                                            ,p_col_ar_uits => v_col_ar_uits
                                                            ,x_imui_trow   => v_imui_trow
                                                            );
                                          v_debug := v_debug || 'z';
                                       END LOOP;
                                    END IF;
                                 END IF;
                              END IF;
                           END IF;

                           --*** metingen
                           v_debug := v_debug || 'M';
                           v_xmlnl_metiN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_onde1, i_onde))
                                                                       ,'metingen');
                           IF NOT xmldom.isNull(v_xmlnl_metiN)
                           THEN
                              IF xmldom.getlength(v_xmlnl_metiN) > 0
                              THEN
                                 v_xmln1 := xmldom.item(v_xmlnl_metiN
                                                       ,0);
                                 v_xmlnl_meti1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                            ,'meting');
                                 IF NOT xmldom.isNull(v_xmlnl_meti1)
                                 THEN
                                    dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_meti1)) || ' meti children found');
                                    IF xmldom.getlength(v_xmlnl_meti1) > 0
                                    THEN -- alle metingen!
                                       FOR i_meti IN 0 .. xmldom.getlength(v_xmlnl_meti1) - 1
                                       LOOP
                                          v_imme_trow := v_imme_trow_null;
                                          xml2ar(p_xmln => xmldom.item(v_xmlnl_meti1, i_meti), p_col_ar => v_col_ar_meti);
                                          IF get_att(v_col_ar_meti,'imme_id') IS NULL
                                          THEN -- automatisch uitgeven:
                                             v_col_ar_meti('imme_id') := 'METI' || trim(to_char(i_meti + 1, '09'));
                                          END IF;
                                          v_imme_trow.imfi_id := r_imfi.imfi_id;
                                          v_imme_trow.imon_id := get_att(v_col_ar_onde,'imon_id');
                                          v_imme_trow.immo_id := get_att(v_col_ar_meti,'immo_id');
                                          v_imme_trow.imfr_id := get_att(v_col_ar_meti,'imfr_id');
                                          v_imme_trow.imme_id := get_att(v_col_ar_meti,'imme_id');
                                          v_debug := v_debug || i_meti;
                                          merge_im_metingen(p_imlo_id     => r_imlo.imlo_id
                                                           ,p_col_ar_meti => v_col_ar_meti
                                                           ,x_imme_trow   => v_imme_trow
                                                           );
                                       END LOOP;
                                    END IF;
                                 END IF;
                              END IF;
                           END IF;

                        END LOOP;
                     END IF;
                  END IF;
               END IF;
            END IF;
         END LOOP; -- personen

         --*** familie
         v_debug := v_debug || 'F';
         v_imfa_trow := v_imfa_trow_null;
         v_xmlnl_famiN := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln0)
                                                     ,'families');
         IF NOT xmldom.isNull(v_xmlnl_famiN)
         THEN
            IF xmldom.getlength(v_xmlnl_famiN) > 0
            THEN
               v_xmln1 := xmldom.item(v_xmlnl_famiN
                                     ,0);
               v_xmlnl_fami1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                          ,'familie');
               IF NOT xmldom.isNull(v_xmlnl_fami1)
               THEN
                  dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_fami1)) || ' fami children found');
                  IF xmldom.getlength(v_xmlnl_fami1) > 0
                  THEN
                     xml2ar(p_xmln => xmldom.item(v_xmlnl_fami1, 0), p_col_ar => v_col_ar_fami);
                     IF get_att(v_col_ar_fami,'imfa_id') IS NULL
                     THEN -- automatisch uitgeven:
                        v_col_ar_fami('imfa_id') := 'FAMI' || trim(to_char(1, '09'));
                     END IF;
                     v_imfa_trow.imfi_id := r_imfi.imfi_id;
                     v_imfa_trow.imfa_id := get_att(v_col_ar_fami,'imfa_id');
                     v_debug := v_debug || '1';
                     merge_im_families(p_imlo_id     => r_imlo.imlo_id
                                      ,p_col_ar_fami => v_col_ar_fami
                                      ,x_imfa_trow   => v_imfa_trow
                                      );
                     --*** familieonderzoeken
                     v_debug := v_debug || 'O';
                     v_xmlnl_faonN := xmldom.getchildrenbytagname(xmldom.makeelement(xmldom.item(v_xmlnl_fami1, 0))
                                                                 ,'familie_onderzoeken');
                     IF NOT xmldom.isNull(v_xmlnl_faonN)
                     THEN
                        IF xmldom.getlength(v_xmlnl_faonN) > 0
                        THEN
                           v_xmln1 := xmldom.item(v_xmlnl_faonN
                                                 ,0);
                           v_xmlnl_faon1 := xmldom.getchildrenbytagname(xmldom.makeelement(v_xmln1)
                                                                      ,'familie_onderzoek');
                           IF NOT xmldom.isNull(v_xmlnl_faon1)
                           THEN
                              dbms_output.put_line(to_char(xmldom.getlength(v_xmlnl_faon1)) || ' faon children found');
                              IF xmldom.getlength(v_xmlnl_faon1) > 0
                              THEN -- alle familieonderzoeken!
                                 FOR i_faon IN 0 .. xmldom.getlength(v_xmlnl_faon1) - 1
                                 LOOP
                                    v_imfz_trow := v_imfz_trow_null;
                                    xml2ar(p_xmln => xmldom.item(v_xmlnl_faon1, i_faon), p_col_ar => v_col_ar_faon);
                                    IF get_att(v_col_ar_faon,'imfz_id') IS NULL
                                    THEN -- automatisch uitgeven:
                                       v_col_ar_faon('imfz_id') := 'FAON' || trim(to_char(i_faon + 1, '09'));
                                    END IF;
                                    v_imfz_trow.imfi_id := r_imfi.imfi_id;
                                    v_imfz_trow.imfz_id := get_att(v_col_ar_faon,'imfz_id');
                                    v_debug := v_debug || i_faon;
                                    merge_im_familie_onderzoeken(p_imlo_id     => r_imlo.imlo_id
                                                                ,p_imfa_id     => v_imfa_trow.imfa_id
                                                                ,p_col_ar_faon => v_col_ar_faon
                                                                ,x_imfz_trow   => v_imfz_trow
                                                                );
                                 END LOOP;
                              END IF;
                           END IF;
                        END IF;
                     END IF;
                  END IF;
               END IF;
            END IF;
         END IF;

         -- verwerken pdf:
         -- 1 koppel aan imfi
         -- 2 opslaan
         v_debug := v_debug || 'F';
         IF NVL(DBMS_LOB.GETLENGTH(b_pdf), 0) > 0
         THEN
            -- bepaal bestandstype
            OPEN c_btyp(p_code => 'IMFI_DOC');
            FETCH c_btyp into r_btyp;
            IF c_btyp%NOTFOUND
            THEN
               kgc_import.addtext(v_imfi_opmerkingen
                                 ,2000
                                 ,'Het bestandstype ''IMFI_DOC'' werd niet gevonden!');
               RAISE e_lees_in_fout;
            END IF;
            CLOSE c_btyp;
            v_best_trow.locatie := r_btyp.standaard_pad;
            IF SUBSTR(v_best_trow.locatie, -1) != '\'
            THEN
              v_best_trow.locatie := v_best_trow.locatie || '\';
            END IF;
            v_best_trow.entiteit_code := 'IMFI';
            v_best_trow.entiteit_pk := r_imfi.imfi_id;
            v_best_trow.btyp_id := r_btyp.btyp_id;
            v_best_trow.bestandsnaam := v_imfi_identificatie || '.' || REPLACE(r_btyp.standaard_extentie, '.', '');
            v_best_trow.bestand_specificatie := RTRIM(v_best_trow.locatie, '\') || '\' || v_best_trow.bestandsnaam;
            -- bestand registreren
            merge_bestanden(x_best_trow => v_best_trow);

            dbms_output.put_line('Opslaan file ' || v_best_trow.bestand_specificatie);
            -- blob schrijven moet met java... pas in versie 10 kan utl_file binary-mode aan... in text-mode wordt 0D altijd 0D 0A... bah
            v_file_result := blob_naar_bestand (p_file => v_best_trow.bestand_specificatie
                                               ,p_blob   => b_pdf);
            IF v_file_result <> 'OK'
            THEN
               kgc_import.addtext(v_imfi_opmerkingen
                                 ,2000
                                 ,'Wegschrijven file ' || v_best_trow.bestand_specificatie || ' gaat fout; melding: ' || v_file_result);
               RAISE e_lees_in_fout;
            END IF;
            dbms_output.put_line('Opslaan file klaar!');
         END IF;

      EXCEPTION
         WHEN e_lees_in_fout
         THEN
            v_lees_in_ok := FALSE;
            UTL_FILE.FCLOSE_ALL;
         WHEN OTHERS
         THEN
            UTL_FILE.FCLOSE_ALL;
            RAISE;
      END;

      -- vrijgeven:
      xmldom.freedocument(v_xmldoc);
      xmlparser.freeparser(v_xmlp);
      -- afsluiting
      IF NOT v_lees_in_ok
      THEN
         ROLLBACK TO SAVEPOINT save_imfi; -- alleen imfi mag blijven bestaan tbv de melding
         kgc_import.addtext(x_verslag
                           ,2000
                           ,'Fout in Helix: ' || v_imfi_opmerkingen);
      ELSE
         x_verslag := v_imfi_opmerkingen;
      END IF;
      UPDATE kgc_import_files imfi
         SET opmerkingen = v_imfi_opmerkingen
            ,filenaam = v_imfi_filenaam -- eventueel bijgewerkt met persoon en afdeling
            ,afgehandeld = 'N' -- altijd, want de verwerking moet nog komen!
       WHERE imfi.imfi_id = r_imfi.imfi_id;
      COMMIT;

      -- direct verwerken indien ok
      IF v_lees_in_ok
      THEN
         v_direct_starten := FALSE;
         IF v_blokkeer_direct_starten THEN -- Rework Mantis 3184: Interface voor Dragon
            NULL; -- niet direct starten, bv agv verschil in naam bij ophalen uit ZIS
         ELSIF r_imlo.start_verwerking = 'D' -- direct
         THEN
            v_direct_starten := TRUE;
         ELSIF r_imlo.start_verwerking = 'M' -- indien materiaal aanwezig
         THEN -- controleer of in dit bestand een monster aanwezig is met nummer '*KOPPEL*'...
            SELECT COUNT(*)
              INTO v_count
              FROM kgc_im_monsters immo
             WHERE immo.imfi_id = r_imfi.imfi_id
               AND immo.monsternummer = '*KOPPEL*';
            IF v_count > 0
            THEN
               v_direct_starten := TRUE;
            END IF;
         END IF;
         IF v_direct_starten
         THEN
            BEGIN
               -- indien dit een fout oplevert: niet teruggeven aan de aanroeper
               -- maar opslaan in imfi
               kgc_import.vertaal_naar_helix(p_imfi_id       => r_imfi.imfi_id
                                            ,p_online        => FALSE
                                            ,p_beheer_in_zis => 'J');
            EXCEPTION
               WHEN OTHERS THEN
                  v_imfi_opmerkingen := SQLERRM;
                  UPDATE kgc_import_files imfi
                     SET opmerkingen = v_imfi_opmerkingen
                   WHERE imfi.imfi_id = r_imfi.imfi_id;
                  COMMIT; -- verder gewoon v_lees_in_ok teruggeven!
            END;
         END IF;
      END IF;

      dbms_output.put_line('Einde kgc_interface_generiek; debug=' || v_debug);
      RETURN v_lees_in_ok;
   EXCEPTION
      WHEN OTHERS
      THEN
         dbms_output.put_line('debug=' || v_debug);
         x_verslag := x_verslag || 'SQLERM: ' || SQLERRM || ' (debug=' || v_debug || ')';
         v_msg := cg$errors.GetErrors;
         IF nvl(length(v_msg), 0) > 0
         THEN
            x_verslag := x_verslag || chr(10) || 'cg$errors: ' || v_msg;
         END IF;
         IF NOT xmldom.isnull(v_xmldoc)
         THEN
            xmldom.freedocument(v_xmldoc);
         END IF;
         xmlparser.freeparser(v_xmlp);
         RETURN FALSE;
   END lees_in;

   PROCEDURE lees_in(b_imfi_id   IN     NUMBER
                    ,b_xml       IN     VARCHAR2
                   ) IS
      -- speciaal tbv forms: kan niet omgaan met CLOB... is alleen tbv testen; dus beperking op 32K!
      v_msg VARCHAR2(4000);
      v_ok  boolean;
      v_xml CLOB := b_xml;
   BEGIN
      v_ok := kgc_interface_generiek.lees_in(b_interface => NULL
                                            ,b_xml       => v_xml
                                            ,b_pdf       => NULL
                                            ,x_verslag   => v_msg
                                            ,b_imfi_id   => b_imfi_id
                                            );
      IF NOT v_ok THEN
         raise_application_error(-20000
                                ,v_msg);
      END IF;
   END lees_in;

   FUNCTION annuleer(b_interface     IN     VARCHAR
                    ,b_identificatie IN     VARCHAR2
                    ,x_verslag       IN OUT VARCHAR2
                    ) RETURN BOOLEAN IS
      CURSOR c_imlo (p_code VARCHAR2) IS
         SELECT *
         FROM   kgc_import_locaties imlo
         WHERE  imlo.directorynaam = p_code;
      r_imlo c_imlo%ROWTYPE;
      -- let op: een generieke 'importfile' wordt geidentificeerd door het eerste stuk voor de spatie!
      CURSOR c_imfi (p_imlo_id NUMBER, p_filenaam VARCHAR2) IS
         SELECT imfi.imfi_id
         ,      imfi.import_datum
         ,      imfi.inhoud
         FROM   kgc_import_files imfi
         WHERE  imfi.imlo_id = p_imlo_id
         AND    imfi.filenaam like p_filenaam || ' %';
      r_imfi                 c_imfi%ROWTYPE;
      v_msg                  VARCHAR2(4000);
      v_res                  NUMBER;
   BEGIN
      x_verslag := NULL;
      v_res := aanvraag_in_behandeling(b_interface     => b_interface
                                      ,b_identificatie => b_identificatie
                                      ,x_verslag       => x_verslag);
      IF v_res = 1
      THEN
         x_verslag := 'Deze aanvraag is in behandeling en kan niet geannuleerd worden!';
         RETURN FALSE;
      END IF;
      -- check foutmelding uit functie
      IF v_res = 3
      THEN
         x_verslag := 'Deze aanvraag kan niet geannuleerd worden (fout: ' || x_verslag || ')!';
         RETURN FALSE;
      END IF;
      -- gegevens ophalen; al gecontroleerd door aanvraag_in_behandeling
      OPEN c_imlo(p_code => b_interface);
      FETCH c_imlo INTO r_imlo;
      CLOSE c_imlo;
      OPEN c_imfi(p_imlo_id => r_imlo.imlo_id
                 ,p_filenaam => b_identificatie);
      FETCH c_imfi INTO r_imfi;
      CLOSE c_imfi;
      delete_imfi(b_imfi_id => r_imfi.imfi_id);
      COMMIT;
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_verslag := 'Deze aanvraag kan niet geannuleerd worden: fout in kgc_import_generiek.annuleer; SQLERM: ' || SQLERRM;
         v_msg := cg$errors.GetErrors;
         IF nvl(length(v_msg), 0) > 0
         THEN
            x_verslag := x_verslag || chr(10) || 'cg$errors: ' || v_msg;
         END IF;
         RETURN FALSE;
   END annuleer;

   FUNCTION aanvraag_in_behandeling(b_interface     IN     VARCHAR
                                   ,b_identificatie IN     VARCHAR2
                                   ,x_verslag       IN OUT VARCHAR2
                                   ) RETURN NUMBER IS
      -- return: 1=in behandeling; 2=niet in behandeling; 3=er is een fout opgetreden
      CURSOR c_imlo (p_code VARCHAR2) IS
         SELECT *
         FROM   kgc_import_locaties imlo
         WHERE  imlo.directorynaam = p_code;
      r_imlo c_imlo%ROWTYPE;
      -- let op: een generieke 'importfile' wordt geidentificeerd door het eerste stuk voor de spatie!
      CURSOR c_imfi (p_imlo_id  NUMBER
                    ,p_filenaam VARCHAR2) IS
         SELECT imfi.imfi_id
         ,      imfi.filenaam
         FROM   kgc_import_files imfi
         WHERE  imfi.imlo_id = p_imlo_id
         AND    imfi.filenaam like p_filenaam || ' %';
      r_imfi                 c_imfi%ROWTYPE;
      r_imfi2                c_imfi%ROWTYPE;
      CURSOR c_impe(p_imfi_id  NUMBER) IS
         SELECT kgc_pers_id
         FROM   kgc_im_personen impe
         WHERE  impe.imfi_id = p_imfi_id;
      r_impe c_impe%ROWTYPE;
      v_msg                  VARCHAR2(4000);
   BEGIN
      x_verslag := NULL; -- dit is de foutmelding!
      -- check:
      OPEN c_imlo(p_code => b_interface);
      FETCH c_imlo INTO r_imlo;
      IF c_imlo%NOTFOUND
      THEN
         x_verslag := 'Het opgegeven interface ''' || b_interface || ''' is niet in Helix bekend als import-locatie!';
         CLOSE c_imlo;
         RETURN 3;
      END IF;
      CLOSE c_imlo;

      OPEN c_imfi(p_imlo_id => r_imlo.imlo_id
                 ,p_filenaam => b_identificatie);
      FETCH c_imfi INTO r_imfi;
      IF c_imfi%NOTFOUND
      THEN
         x_verslag := 'De opgegeven aanvraag ''' || b_identificatie || ''' is niet in Helix bekend als import-bestand!';
         CLOSE c_imfi;
         RETURN 3;
      ELSE -- check uniek:
         FETCH c_imfi INTO r_imfi2;
         IF c_imfi%FOUND THEN
            x_verslag := 'De opgegeven aanvraag ''' || b_identificatie || ''' identificeert het Helix import-bestand niet uniek (1: '
                         || r_imfi.filenaam || '; 2: ' || r_imfi2.filenaam;
            CLOSE c_imfi;
            RETURN 3;
         ELSE
            -- is er een persoon gekoppeld?
            OPEN c_impe(p_imfi_id => r_imfi.imfi_id);
            FETCH c_impe INTO r_impe;
            CLOSE c_impe;
         END IF;
      END IF;
      CLOSE c_imfi;
      IF r_impe.kgc_pers_id IS NOT NULL THEN
         RETURN 1;
      ELSE
         RETURN 2;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_verslag := x_verslag || 'SQLERM kgc_import_generiek.aanvraag_in_behandeling: ' || SQLERRM;
         v_msg := cg$errors.GetErrors;
         IF nvl(length(v_msg), 0) > 0
         THEN
            x_verslag := x_verslag || chr(10) || 'cg$errors: ' || v_msg;
         END IF;
         RETURN 3;
   END aanvraag_in_behandeling;

   PROCEDURE delete_imfi(b_imfi_id IN NUMBER) IS
      CURSOR c_best (p_imfi_id NUMBER) IS
         SELECT *
           FROM kgc_bestanden best
          WHERE best.entiteit_code = 'IMFI'
            AND best.entiteit_pk = p_imfi_id;
      v_result    VARCHAR2(2000);
   BEGIN
      -- Mantis 3184: Interface voor Dragon
      DELETE FROM kgc_im_familie_onderzoeken
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_families
      WHERE imfi_id = b_imfi_id
      ;

      -- mantis 11620: meerdere personen mogelijk!
      DELETE FROM kgc_im_metingen
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_uitslagen
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_onderzoek_betrokkenen
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_onde_kopie
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_onderzoek_indicaties
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_onderzoeken
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_fracties
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_monsters
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_foetussen
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_zwangerschappen
      WHERE imfi_id = b_imfi_id
      ;
      DELETE FROM kgc_im_personen
      WHERE imfi_id = b_imfi_id
      ;

      -- Mantis 3184: Interface voor Dragon; ook de bestanden verwijderen, zowel fysiek als in de database
      FOR r_best IN c_best (p_imfi_id => b_imfi_id) LOOP
         v_result := delete_bestand(r_best.bestand_specificatie);
         IF v_result != 'OK' THEN
           raise_application_error(-20000
                                  ,'Bestand ''' || r_best.bestand_specificatie || ''' kon niet verwijderd worden: ' || v_result);
         END IF;
         DELETE FROM kgc_bestanden best
         WHERE best.best_id = r_best.best_id
         ;
      END LOOP;


      -- Mantis 3184: Interface voor Dragon; als laatste ook de rij in kgc_import_files verwijderen:
      DELETE FROM kgc_import_files
      WHERE imfi_id = b_imfi_id
      ;

      -- commit en exception is voor de aanvrager
   END delete_imfi;

   FUNCTION persoon_im_in_zis(p_imlo_id     IN     NUMBER
                             ,x_impe_trow   IN OUT cg$kgc_im_personen.cg$row_type
                             ,x_msg         IN OUT VARCHAR2
                             ) RETURN BOOLEAN IS
      CURSOR c_pers(p_zisnr VARCHAR2) IS
         SELECT pers_id
         FROM   kgc_personen pers
         WHERE  pers.zisnr = p_zisnr
         ;
      v_pers_trow            cg$kgc_personen.cg$row_type;
      v_rela_trow            cg$kgc_relaties.cg$row_type;
      v_verz_trow            cg$kgc_verzekeraars.cg$row_type;
      v_att_naam             VARCHAR2(32);
      v_ok                   BOOLEAN;
      v_msg                  VARCHAR2(32767);
      v_naam_interface1      VARCHAR2(200);
      v_naam_interface2      VARCHAR2(200);
      v_naam_zis             VARCHAR2(200);
      v_naam_zis2            VARCHAR2(200);
      v_naam_afwijkend       BOOLEAN;
   BEGIN
      -- Interne check: imfi en impe zijn verplicht
      IF x_impe_trow.imfi_id IS NULL OR x_impe_trow.impe_id IS NULL
      THEN
         x_msg := 'Interne fout: imfi_id/impe_id is verplicht!';
         RETURN FALSE;
      END IF;
      -- ZISnr is verplicht
      IF x_impe_trow.zisnr IS NULL
      THEN
         x_msg := 'Het ZISnr is verplicht!';
         RETURN FALSE;
      END IF;
      -- eventueel de gegevens in Helix selecteren
      OPEN c_pers(p_zisnr => x_impe_trow.zisnr);
      FETCH c_pers
      INTO  v_pers_trow.pers_id;
      IF c_pers%FOUND
      THEN
         cg$kgc_personen.slct(v_pers_trow); -- bestaande gegevens ophalen
      END IF;
      CLOSE c_pers;
      -- persoon uit ZIS halen
      v_ok := kgc_zis.persoon_in_zis(p_zisnr   => x_impe_trow.zisnr
                                    ,pers_trow => v_pers_trow
                                    ,rela_trow => v_rela_trow
                                    ,verz_trow => v_verz_trow
                                    ,x_msg     => v_msg);
      IF NOT v_ok OR v_pers_trow.zisnr IS NULL
      THEN
         x_msg := 'Opvragen in ZIS van persoon met ZISnr ' || x_impe_trow.zisnr
                                 || ' geeft een fout: ' || v_msg;
         RETURN TRUE; -- doorgaan alsof het goed is! Rework Mantis 3184: Interface voor Dragon
      END IF;
      -- checkof de combinatie achternaam,voorvoegsels, voorletters, geslacht en geboortedatum overeenkomen
      v_naam_interface1 := x_impe_trow.voorletters;
      v_naam_interface2 := x_impe_trow.voorletters || x_impe_trow.achternaam || x_impe_trow.voorvoegsel;
      IF x_impe_trow.voorvoegsel IS NOT NULL
      THEN
         v_naam_interface1 := v_naam_interface1 || ' ' || x_impe_trow.voorvoegsel;
      END IF;
      v_naam_interface1 := v_naam_interface1 || ' ' || x_impe_trow.achternaam;
      v_naam_zis := v_pers_trow.voorletters;
      IF v_pers_trow.voorvoegsel IS NOT NULL
      THEN
         v_naam_zis := v_naam_zis || ' ' || v_pers_trow.voorvoegsel;
      END IF;
      v_naam_zis := v_naam_zis || ' ' || v_pers_trow.achternaam;
      -- mantis 5717
      IF v_pers_trow.achternaam_partner IS NOT NULL THEN
         v_naam_zis2 := v_pers_trow.voorletters;
         IF v_pers_trow.voorvoegsel_partner IS NOT NULL
         THEN
            v_naam_zis2 := v_naam_zis2 || ' ' || v_pers_trow.voorvoegsel_partner;
         END IF;
         v_naam_zis2 := v_naam_zis2 || ' ' || v_pers_trow.achternaam_partner;
      END IF;

      v_naam_afwijkend := TRUE;
      IF NOT kgc_util_00.verschil(UPPER(REPLACE(REPLACE(v_naam_interface1, ' ', ''), '.', ''))
                                 ,UPPER(REPLACE(REPLACE(v_naam_zis, ' ', ''), '.', '')))
      THEN
         v_naam_afwijkend := FALSE;
      ELSIF NOT kgc_util_00.verschil(UPPER(REPLACE(REPLACE(v_naam_interface2, ' ', ''), '.', ''))
                                    ,UPPER(REPLACE(REPLACE(v_naam_zis, ' ', ''), '.', '')))
      THEN
         v_naam_afwijkend := FALSE;
      ELSIF NOT kgc_util_00.verschil(UPPER(REPLACE(REPLACE(v_naam_interface1, ' ', ''), '.', ''))
                                    ,UPPER(REPLACE(REPLACE(v_naam_zis2, ' ', ''), '.', '')))
      THEN
         v_naam_afwijkend := FALSE;
      ELSIF v_naam_zis2 IS NOT NULL AND NOT kgc_util_00.verschil(UPPER(REPLACE(REPLACE(v_naam_interface2, ' ', ''), '.', ''))
                                                                ,UPPER(REPLACE(REPLACE(v_naam_zis2, ' ', ''), '.', '')))
      THEN
         v_naam_afwijkend := FALSE;
      END IF;

      IF kgc_util_00.verschil(x_impe_trow.geboortedatum
                             ,TO_CHAR(v_pers_trow.geboortedatum
                                     ,'dd-mm-yyyy'))
         OR v_naam_afwijkend
      THEN
         v_msg := 'Aangeleverd - naam: ' || v_naam_interface1 || '; geboortedatum: ' || x_impe_trow.geboortedatum ||
                  '; ZIS - naam: ' || v_naam_zis ||  '; geboortedatum: ' || TO_CHAR(v_pers_trow.geboortedatum,'dd-mm-yyyy');
         x_msg := 'De aangeleverde persoonsgegevens met ZISnr ' || x_impe_trow.zisnr
                                 || ' wijken af van de ZIS-gegevens: ' || v_msg;
--         RETURN FALSE; gewoon doorgaan! Rework Mantis 3184: Interface voor Dragon
      END IF;

      -- vertalen
      x_impe_trow.achternaam         := v_pers_trow.achternaam;
      x_impe_trow.gehuwd             := v_pers_trow.gehuwd;
      x_impe_trow.impw_id_geslacht   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                            ,p_helix_table_domain => kgc_import.g_cimpw_dom_geslacht
                                                            ,p_externe_id         => v_pers_trow.geslacht
                                                            ,p_hint               => NULL
                                                            ,p_toevoegen          => TRUE);
      x_impe_trow.bloedverwant       := v_pers_trow.bloedverwant;
      x_impe_trow.meerling           := v_pers_trow.meerling;
      x_impe_trow.overleden          := v_pers_trow.overleden;
      x_impe_trow.default_aanspreken := v_pers_trow.default_aanspreken;

      -- huisarts
      insert_huisarts(v_rela_trow);

      IF v_rela_trow.rela_id IS NULL
      THEN
         x_impe_trow.impw_id_rela := NULL;
      ELSE
         x_impe_trow.impw_id_rela := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                         ,p_externe_id         => v_rela_trow.code
--                                                         ,p_externe_id         => v_rela_trow.rela_id rde 30-12-2014: dit gaat niet goed: de rela-id kan al bekend zijn als EZIS-code van een aanvrager!
                                                         ,p_hint               => kgc_adres_00.relatie(v_rela_trow.rela_id, 'J')
                                                         ,p_helix_waarde       => v_rela_trow.rela_id
                                                         ,p_toevoegen          => TRUE);
      END IF;

      -- verzekeraar:
      insert_verzekeraar(v_verz_trow);

      IF v_verz_trow.verz_id IS NULL
      THEN
         x_impe_trow.impw_id_verz := NULL;
      ELSE
         x_impe_trow.impw_id_verz := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                         ,p_helix_table_domain => kgc_import.g_cimpw_tab_verz
                                                         ,p_externe_id         => v_verz_trow.verz_id
                                                         ,p_hint               => kgc_adres_00.verzekeraar(v_verz_trow.verz_id, 'J')
                                                         ,p_helix_waarde       => v_verz_trow.verz_id
                                                         ,p_toevoegen          => TRUE);
      END IF;

      x_impe_trow.zisnr               := v_pers_trow.zisnr;
      x_impe_trow.aanspreken          := v_pers_trow.aanspreken;
      x_impe_trow.geboortedatum       := to_char(v_pers_trow.geboortedatum
                                                ,'dd-mm-yyyy');
      x_impe_trow.part_geboortedatum  := v_pers_trow.part_geboortedatum;
      x_impe_trow.voorletters         := v_pers_trow.voorletters;
      x_impe_trow.voorvoegsel         := v_pers_trow.voorvoegsel;
      x_impe_trow.achternaam_partner  := v_pers_trow.achternaam_partner;
      x_impe_trow.voorvoegsel_partner := v_pers_trow.voorvoegsel_partner;
      x_impe_trow.adres               := v_pers_trow.adres;
      x_impe_trow.postcode            := v_pers_trow.postcode;
      x_impe_trow.woonplaats          := v_pers_trow.woonplaats;
      x_impe_trow.impw_id_provincie   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                             ,p_helix_table_domain => 'PROVINCIE'
                                                             ,p_externe_id         => v_pers_trow.provincie
                                                             ,p_hint               => NULL
                                                             ,p_helix_waarde       => v_pers_trow.provincie
                                                             ,p_toevoegen          => TRUE);
      x_impe_trow.land                := v_pers_trow.land;
      x_impe_trow.telefoon            := v_pers_trow.telefoon;
      x_impe_trow.telefax             := v_pers_trow.telefax;
      x_impe_trow.email               := v_pers_trow.email;
      x_impe_trow.impw_id_verzwijze   := kgc_import.getimpwid(p_imlo_id            => p_imlo_id
                                                             ,p_helix_table_domain => kgc_import.g_cimpw_dom_verzekeringswijze
                                                             ,p_externe_id         => v_pers_trow.verzekeringswijze
                                                             ,p_hint               => NULL
                                                             ,p_toevoegen          => TRUE);
      x_impe_trow.verzekeringsnr      := v_pers_trow.verzekeringsnr;
      x_impe_trow.overlijdensdatum    := to_char(v_pers_trow.overlijdensdatum
                                                ,'dd-mm-yyyy');
      x_impe_trow.hoeveelling         := v_pers_trow.hoeveelling;
      x_impe_trow.bsn                 := v_pers_trow.bsn;
      x_impe_trow.bsn_geverifieerd    := v_pers_trow.bsn_geverifieerd;

      cg$kgc_im_personen.upd(cg$rec => x_impe_trow
                            ,cg$ind => cg$kgc_im_personen.cg$ind_true);
      RETURN TRUE;
   END persoon_im_in_zis;

   -- huisarts toevoegen ***
   PROCEDURE insert_huisarts(p_rela_trow IN OUT cg$kgc_relaties.cg$row_type) IS
      CURSOR c_rela(b_rela_id kgc_relaties.rela_id%TYPE) IS
         SELECT rela.ROWID
           FROM kgc_relaties rela
          WHERE rela.rela_id = b_rela_id;

   BEGIN

      IF p_rela_trow.achternaam IS NOT NULL
      THEN
         -- toch wel de minste voorwaarde voor toevoeging

         OPEN c_rela(p_rela_trow.rela_id);
         FETCH c_rela
            INTO p_rela_trow.the_rowid;
         CLOSE c_rela;

         IF p_rela_trow.code IS NULL
         THEN
            -- altijd ZIS0123456 aanbrengen! rde 6 juni 2006
            SELECT kgc_rela_seq.NEXTVAL
              INTO p_rela_trow.rela_id
              FROM dual;
            p_rela_trow.code := 'ZIS' || lpad(to_char(p_rela_trow.rela_id)
                                             ,7
                                             ,'0');

         END IF;

         IF p_rela_trow.the_rowid IS NULL
         THEN
            -- nieuwe maken; er is reeds centrum-specifiek gezocht!

            p_rela_trow.relatie_type := 'HA'; -- helix-eigen; wordt niet aangeleverd!
            p_rela_trow.vervallen := nvl(p_rela_trow.vervallen, 'N');
            cg$kgc_relaties.ins(p_rela_trow
                               ,cg$kgc_relaties.cg$ind_true);

         ELSE

            NULL; -- geen actie: mogelijk zijn de aangeleverde gegevens niet OK; rde 6 juni 2006

         END IF;

      END IF;
   END insert_huisarts;

   -- verzekeraar toevoegen ***
   PROCEDURE insert_verzekeraar(p_verz_trow IN OUT cg$kgc_verzekeraars.cg$row_type) IS

      CURSOR c_verz(b_verz_id kgc_verzekeraars.verz_id%TYPE) IS
         SELECT verz.ROWID
           FROM kgc_verzekeraars verz
          WHERE verz.verz_id = b_verz_id;

   BEGIN

      IF p_verz_trow.naam IS NOT NULL
      THEN
         -- toch wel de minste voorwaarde voor toevoeging

         OPEN c_verz(p_verz_trow.verz_id);
         FETCH c_verz
            INTO p_verz_trow.the_rowid;
         CLOSE c_verz;

         IF p_verz_trow.the_rowid IS NULL
         THEN
            cg$kgc_verzekeraars.ins(p_verz_trow
                                   ,cg$kgc_verzekeraars.cg$ind_true);
         ELSE
            NULL; -- geen actie: mogelijk zijn de aangeleverde gegevens niet OK; rde 6 juni 2006
         END IF;

      END IF;
   END insert_verzekeraar;

   -- overloaded tbv aanroep vanuit Dragon... maar parameters zijn identiek, dus andere naam!
   FUNCTION lees_in2(b_interface IN     VARCHAR
                   ,b_xml       IN     CLOB
                   ,b_pdf       IN     BLOB
                   ,x_verslag   IN OUT VARCHAR2
                   ,b_imfi_id   IN     NUMBER := NULL
                   ) RETURN VARCHAR2 IS
      v_ok BOOLEAN;
   BEGIN
      v_ok := lees_in(b_interface
                     ,b_xml
                     ,b_pdf
                     ,x_verslag
                     ,b_imfi_id
                     );
      IF v_ok
      THEN
         RETURN 'J';
      ELSE
         RETURN 'N';
      END IF;
   END lees_in2;
   -- overloaded tbv aanroep vanuit Dragon... maar parameters zijn identiek, dus andere naam!
   FUNCTION annuleer2(b_interface     IN     VARCHAR
                    ,b_identificatie IN     VARCHAR2
                    ,x_verslag       IN OUT VARCHAR2
                    ) RETURN VARCHAR2 IS
      v_ok BOOLEAN;
   BEGIN
      v_ok := annuleer(b_interface
                      ,b_identificatie
                      ,x_verslag
                      );
      IF v_ok
      THEN
         RETURN 'J';
      ELSE
         RETURN 'N';
      END IF;
   END annuleer2;

   FUNCTION FileExists ( p_file   IN     VARCHAR2
                      ) RETURN VARCHAR2 IS
   BEGIN
      RETURN kgc_file.FileExists(p_file);
   END FileExists;


   FUNCTION get_db_nls_lang RETURN VARCHAR2 IS
      -- The NLS_LANG parameter is set as: <Language>_<Territory>.<Characterset> (for example, set NLS_LANG = AMERICAN_AMERICA.UTF8)
      v_nls_lang VARCHAR2(4000);
      v_str nls_database_parameters.value%TYPE;
   BEGIN
      SELECT value INTO v_str FROM nls_database_parameters WHERE parameter = 'NLS_LANGUAGE';
      v_nls_lang := v_str;
      SELECT value INTO v_str FROM nls_database_parameters WHERE parameter = 'NLS_TERRITORY';
      v_nls_lang := v_nls_lang || '_' || v_str;
      SELECT value INTO v_str FROM nls_database_parameters WHERE parameter = 'NLS_CHARACTERSET';
      v_nls_lang := v_nls_lang || '.' || v_str;
      RETURN v_nls_lang;
   END get_db_nls_lang;

   FUNCTION blob2clob
   ( p_blob         IN BLOB
   , p_to_charset   IN VARCHAR2 := NULL
   , p_from_charset IN VARCHAR2 := NULL
   ) RETURN CLOB IS
      -- typecasts BLOB to CLOB (binary conversion)
      v_pos       PLS_INTEGER  := 1;
      v_buffer    VARCHAR2( 32767 );
      v_clob      CLOB;
      v_lob_len   PLS_INTEGER  := DBMS_LOB.getLength(p_blob);
   BEGIN
      DBMS_LOB.createTemporary(v_clob, TRUE);
      DBMS_LOB.OPEN(v_clob, DBMS_LOB.LOB_ReadWrite);
      LOOP
         IF p_to_charset IS NULL THEN -- default
            v_buffer := UTL_RAW.cast_to_varchar2(DBMS_LOB.SUBSTR(p_blob, 16000, v_pos ));
         ELSE
            v_buffer := UTL_RAW.cast_to_varchar2(UTL_RAW.convert(DBMS_LOB.SUBSTR(p_blob, 16000, v_pos ), p_to_charset, p_from_charset) ); -- to, from
         END IF;
         IF LENGTH( v_buffer ) > 0 THEN
            DBMS_LOB.writeAppend(v_clob, LENGTH( v_buffer ), v_buffer );
         END IF;
         v_pos := v_pos + 16000;
         EXIT WHEN v_pos > v_lob_len;
      END LOOP;
      RETURN v_clob; -- res is OPEN here
   END blob2clob;

   FUNCTION clob2blob (p_clob clob) RETURN BLOB IS
      v_off NUMBER := 1;
      v_amt NUMBER := 4096;
      v_offWrite NUMBER := 1;
      v_amtWrite NUMBER;
      v_str VARCHAR2(32767);
      v_blob BLOB;
   BEGIN
      BEGIN
         DBMS_LOB.createTemporary(v_blob, TRUE);
         DBMS_LOB.OPEN(v_blob, DBMS_LOB.LOB_ReadWrite);
         LOOP
            dbms_lob.read(p_clob, v_amt, v_off, v_str );
            v_amtWrite := utl_raw.LENGTH(utl_raw.cast_to_raw(v_str));
            dbms_lob.write( v_blob, v_amtWrite, v_offWrite, utl_raw.cast_to_raw(v_str));
            v_offWrite := v_offWrite + v_amtWrite;
            v_off := v_off + v_amt;
            v_amt := 4096;
         END LOOP;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
         NULL;
      END;
      RETURN v_blob;
   END clob2blob;

   FUNCTION bestand_naar_blob  (p_file   IN     VARCHAR2
                             , p_blob   IN OUT BLOB
                             ) RETURN VARCHAR2 IS
   BEGIN
      RETURN kgc_file.bestand_naar_blob(p_file, p_blob);
   END bestand_naar_blob;

   FUNCTION base642blob(p_base64 clob) RETURN BLOB IS
      v_off NUMBER := 1;
      v_amt NUMBER := 65; -- hell. Since Base64 mandates a fixed line length (64 or 76 characters, depending on which version you look at), the library that produces the Base64 String should already do that (or at least have an option to do that).
      -- zie http://stackoverflow.com/questions/2871933/inserting-newline-character-after-every-76-characters-in-a-base64-string
      -- en http://wonderingmisha.blogspot.nl/2011/05/base64-encoding-in-plsql.html
      v_str VARCHAR2(32767);
      v_blob BLOB;
      v_rawData raw(32767);
   BEGIN
      IF NVL(dbms_lob.getlength(p_base64), 0) = 0 THEN
         RETURN NULL;
      END IF;
      BEGIN
         -- bepaal line length (jawel: Nijmegen gebruikt 64, Maastricht 76... en door de conversie zit daar ook nog een LF-chr(13) voor de NL-chr(10))
         v_amt := INSTR(DBMS_LOB.SUBSTR(p_base64, 100, 1), CHR(10));
         IF v_amt = 0 THEN
            v_amt := 65; -- standaard
         END IF;
         DBMS_LOB.createTemporary(v_blob, TRUE);
         DBMS_LOB.OPEN(v_blob, DBMS_LOB.LOB_ReadWrite);
         LOOP
            dbms_lob.read(p_base64, v_amt, v_off, v_str);
            v_rawData := utl_encode.BASE64_DECODE(utl_raw.CAST_TO_RAW(RTRIM(v_str))); -- op pos 65 staat een chr(10)
            dbms_lob.WRITEAPPEND( v_blob, utl_raw.LENGTH(v_rawData), v_rawData);
            v_off := v_off + v_amt;
         END LOOP;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      RETURN v_blob;
   END base642blob;

   FUNCTION base642clob(p_base64 clob) RETURN CLOB IS
      v_off NUMBER := 1;
      v_amt NUMBER := 65; -- hell. Since Base64 mandates a fixed line length (64 or 76 characters, depending on which version you look at), the library that produces the Base64 String should already do that (or at least have an option to do that).
      -- zie http://stackoverflow.com/questions/2871933/inserting-newline-character-after-every-76-characters-in-a-base64-string
      -- en http://wonderingmisha.blogspot.nl/2011/05/base64-encoding-in-plsql.html
      v_str VARCHAR2(32767);
      v_clob CLOB;
      v_rawData raw(32767);
      v_strDecoded VARCHAR2(32767);
   BEGIN
      BEGIN
         DBMS_LOB.createTemporary(v_clob, TRUE);
         DBMS_LOB.OPEN(v_clob, DBMS_LOB.LOB_ReadWrite);
         LOOP
            dbms_lob.read(p_base64, v_amt, v_off, v_str);
--               v_rawData := utl_encode.BASE64_DECODE(utl_raw.CAST_TO_RAW(RTRIM(v_str))); -- op pos 65 staat een chr(10)
            v_strDecoded := utl_raw.cast_to_varchar2(utl_encode.BASE64_DECODE(utl_raw.CAST_TO_RAW(RTRIM(v_str)))); -- op pos 65 staat een chr(10)
--               dbms_lob.WRITEAPPEND( v_clob, utl_raw.LENGTH(v_rawData), v_rawData);
            dbms_lob.WRITEAPPEND( v_clob, LENGTH(v_strDecoded), v_strDecoded);

            v_off := v_off + v_amt;
         END LOOP;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      RETURN v_clob;
   END base642clob;
      
   FUNCTION lees_in3
   ( b_interface IN     VARCHAR
   , b_xml       IN     CLOB
   , b_pdf       IN     CLOB -- geen blob!
   , x_verslag   IN OUT VARCHAR2
   , b_imfi_id   IN     NUMBER := NULL
   ) RETURN BOOLEAN IS
   BEGIN
      RETURN kgc_interface_generiek.lees_in
             ( b_interface => b_interface
             , b_xml       => base642clob(b_xml)
             , b_pdf       => base642blob(b_pdf)
             , x_verslag   => x_verslag
             , b_imfi_id   => b_imfi_id
             );
   END lees_in3;

END KGC_INTERFACE_GENERIEK;
/
