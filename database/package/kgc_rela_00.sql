CREATE OR REPLACE PACKAGE "HELIX"."KGC_RELA_00" IS
-- toevoeging bij een reatie: afdeling - instelling - specialisme
FUNCTION toevoeging
( p_rela_id IN NUMBER )
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( toevoeging, WNDS, WNPS );

-- koppel de gegevens van verschillende relaties aan 1 relatie
PROCEDURE koppel
( p_correct_rela_id IN NUMBER
, p_koppellijst IN VARCHAR2
, x_resultaat OUT VARCHAR2
);
END KGC_RELA_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_RELA_00" IS
FUNCTION toevoeging
( p_rela_id IN NUMBER )
RETURN VARCHAR2
IS
  v_toevoeging VARCHAR2(100);
  CURSOR rela_cur
  IS
    SELECT inst_id
    ,      afdg_id
    ,      spec_id
    FROM   kgc_relaties
    WHERE  rela_id = p_rela_id
    ;
  rela_rec rela_cur%rowtype;
  --
  CURSOR afdg_cur
  IS
    SELECT omschrijving
    FROM   kgc_afdelingen
    WHERE  afdg_id = rela_rec.afdg_id
    ;
  CURSOR inst_cur
  IS
    SELECT naam
    FROM   kgc_instellingen
    WHERE  inst_id = rela_rec.inst_id
    ;
  CURSOR spec_cur
  IS
    SELECT omschrijving
    FROM   kgc_specialismen
    WHERE  spec_id = rela_rec.spec_id
    ;
BEGIN
  OPEN  rela_cur;
  FETCH rela_cur
  INTO  rela_rec;
  CLOSE rela_cur;
  IF ( rela_rec.afdg_id IS NOT NULL )
  THEN
    OPEN  afdg_cur;
    FETCH afdg_cur
    INTO  v_toevoeging;
    CLOSE afdg_cur;
  ELSIF ( rela_rec.inst_id IS NOT NULL )
  THEN
    OPEN  inst_cur;
    FETCH inst_cur
    INTO  v_toevoeging;
    CLOSE inst_cur;
  ELSIF ( rela_rec.spec_id IS NOT NULL )
  THEN
    OPEN  spec_cur;
    FETCH spec_cur
    INTO  v_toevoeging;
    CLOSE spec_cur;
  END IF;
  RETURN( v_toevoeging );
END;

PROCEDURE   koppel
  (   p_correct_rela_id   IN   NUMBER
  ,   p_koppellijst   IN   VARCHAR2
  ,   x_resultaat   OUT   VARCHAR2
  )
IS
  v_statement VARCHAR2(2000);
  v_result VARCHAR2(100);
  v_gevonden_data BOOLEAN := FALSE;
  breek_af EXCEPTION;

  v_koppellijst_num VARCHAR2(1000) := p_koppellijst;
  v_koppellijst_char VARCHAR2(1000) ;
  v_rela_id NUMBER; -- 1 uit koppellijst

  CURSOR tcol_cur
  IS
    SELECT DISTINCT -- misschien meer eigenaren
           ttab.table_name
    ,      tcol.column_name
    FROM   all_tab_columns tcol
    ,      all_tables ttab
    WHERE  ttab.owner = tcol.owner
    AND    ttab.table_name = tcol.table_name
    AND    tcol.column_name LIKE '%RELA_ID%'
    AND    ttab.table_name <> 'KGC_RELATIES'
    ;

  CURSOR rela_cur
  ( p_rela_id IN NUMBER
  )
  IS
    SELECT code
    ,      aanspreken
    FROM   kgc_relaties
    WHERE  rela_id = p_rela_id
    ;
  rela_rec rela_cur%rowtype;

  v_resultaat VARCHAR2(2000);
  PROCEDURE res
  ( p_regel IN VARCHAR2
  )
  IS
    BEGIN
    IF ( p_regel IS NOT NULL )
    THEN
      IF ( v_resultaat IS NULL )
      THEN
        v_resultaat := p_regel;
      ELSE
        v_resultaat := v_resultaat||CHR(10)||p_regel;
      END IF;
    END IF;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      NULL;
  END res;
BEGIN
  -- bepaal alfanumerieke koppellijst
  if p_koppellijst is null
  then
    v_koppellijst_char := null;
  else
    v_koppellijst_char := ''''||replace(p_koppellijst,',',''',''')||'''';
  end if;
  OPEN  rela_cur( p_correct_rela_id );
  FETCH rela_cur
  INTO  rela_rec;
  IF ( rela_cur%FOUND )
  THEN
    CLOSE rela_cur;
    res( 'Aan relatie '||rela_rec.code
                 ||', '||rela_rec.aanspreken
       ||' zijn de volgende relatiegegevens gekoppeld:'
       );
  ELSE
    CLOSE rela_cur;
    res( 'Geen gegevens gevonden van de ''correcte'' relatie'
      || ' (rela_id='||TO_CHAR(p_correct_rela_id)||')'
       );
    RAISE breek_af;
  END IF;

  FOR tcol_rec IN tcol_cur
  LOOP
    v_statement := 'alter table '||tcol_rec.table_name||' disable all triggers;';
    BEGIN
      v_result := kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
    END;
    v_statement := 'begin'
        ||CHR(10)||'update '||tcol_rec.table_name
        ||CHR(10)||'set '||tcol_rec.column_name||' = '||TO_CHAR(p_correct_rela_id)
        ||CHR(10)||'where  '||tcol_rec.column_name||' in ('||v_koppellijst_num||');'
        ||CHR(10)||':result := sql%rowcount;'
        ||CHR(10)||'end;'
                 ;
    BEGIN
      v_result := kgc_util_00.dyn_exec( v_statement );
      IF ( v_result > 0 )
      THEN
        v_gevonden_data := TRUE;
        res( 'Gegevens uit tabel '||tcol_rec.table_name||' omgezet.' );
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
        RAISE breek_af;
    END;
    v_statement := 'alter table '||tcol_rec.table_name||' enable all triggers;';
    BEGIN
      v_result := kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
    END;
    COMMIT;
  END LOOP;

  -- Tabellen gerelateerd aan kgc_entiteiten:
  -- Notities
  v_statement := 'alter table KGC_NOTITIES disable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;
  v_statement := 'begin'
      ||CHR(10)||'update KGC_NOTITIES'
      ||CHR(10)||'set id = '||TO_CHAR(p_correct_rela_id)
      ||CHR(10)||'where  id in ('||v_koppellijst_num||')'
      ||CHR(10)||'and entiteit = ''RELA'';'
      ||CHR(10)||':result := sql%rowcount;'
      ||CHR(10)||'end;'
               ;
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
    IF ( v_result > 0 )
    THEN
      v_gevonden_data := TRUE;
      res( 'Gegevens uit tabel KGC_NOTITIES omgezet.' );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
      RAISE breek_af;
  END;
  v_statement := 'alter table KGC_NOTITIES enable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;
  -- Vertalingen
  v_statement := 'alter table KGC_VERTALINGEN disable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;
   v_statement := 'begin'
      ||CHR(10)||'update KGC_VERTALINGEN'
      ||CHR(10)||'set id = '||TO_CHAR(p_correct_rela_id)
      ||CHR(10)||'where  id in ('||v_koppellijst_num||')'
      ||CHR(10)||'and entiteit = ''RELA'';'
      ||CHR(10)||':result := sql%rowcount;'
      ||CHR(10)||'end;'
               ;
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
    IF ( v_result > 0 )
    THEN
      v_gevonden_data := TRUE;
      res( 'Gegevens uit tabel KGC_VERTALINGEN omgezet.' );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
      RAISE breek_af;
  END;
  v_statement := 'alter table KGC_VERTALINGEN enable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;

  -- Import param waarde  (toegevoegd t.g.v. Mantis 3344)
  v_statement := 'alter table KGC_IMPORT_PARAM_WAARDEN disable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;
   v_statement := 'begin'
      ||CHR(10)||'update KGC_IMPORT_PARAM_WAARDEN'
      ||CHR(10)||'set helix_waarde = '||TO_CHAR(p_correct_rela_id)
      ||CHR(10)||'where  helix_waarde in ('||v_koppellijst_char||')'
      ||CHR(10)||'and helix_table_domain = ''KGC_RELATIES'';'
      ||CHR(10)||':result := sql%rowcount;'
      ||CHR(10)||'end;'
               ;
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
    IF ( v_result > 0 )
    THEN
      v_gevonden_data := TRUE;
      res( 'Gegevens uit tabel KGC_IMPORT_PARAM_WAARDEN omgezet.' );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
      RAISE breek_af;
  END;
  v_statement := 'alter table KGC_IMPORT_PARAM_WAARDEN enable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;

  IF ( NOT v_gevonden_data )
  THEN
    res( 'Er waren geen onderliggende gegevens op te koppelen.' );
  END IF;

  -- verwijder gekoppelde relaties
  WHILE ( REPLACE(v_koppellijst_num,',','') IS NOT NULL )
  LOOP
    IF ( INSTR( v_koppellijst_num, ',' ) = 0 )
    THEN
      v_rela_id := v_koppellijst_num;
      v_koppellijst_num := NULL;
    ELSE
      v_rela_id := SUBSTR( v_koppellijst_num
                         , 1
                         , INSTR( v_koppellijst_num, ',' ) - 1
                         );
    END IF;
    OPEN  rela_cur( v_rela_id );
    FETCH rela_cur
    INTO  rela_rec;
    IF ( rela_cur%FOUND )
    THEN
      CLOSE rela_cur;
      res( 'Relatie '||rela_rec.code
               ||', '||rela_rec.aanspreken
         ||' verwijderen...'
         );

      v_statement := 'delete from kgc_relaties where rela_id = '||TO_CHAR(v_rela_id)||';';
      BEGIN
        kgc_util_00.dyn_exec( v_statement );
        res( 'OK.' );
      EXCEPTION
        WHEN OTHERS
        THEN
          res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
      END;
    ELSE
      CLOSE rela_cur;
      res( 'Geen gegevens gevonden van de ''gekoppelde'' relatie'
        || ' (rela_id='||TO_CHAR(p_correct_rela_id)||')'
         );
    END IF;

    v_koppellijst_num := SUBSTR( v_koppellijst_num
                           , INSTR( v_koppellijst_num, ',' ) + 1
                           );
  END LOOP;
  COMMIT;
  x_resultaat := v_resultaat;
EXCEPTION
  WHEN breek_af
  THEN
    x_resultaat := v_resultaat;
    ROLLBACK;
  WHEN OTHERS
  THEN
    x_resultaat := SUBSTR( v_resultaat||CHR(10)||SQLERRM
                         , 1, 2000
                         );
    ROLLBACK;
END koppel;
END kgc_rela_00;
/

/
QUIT
