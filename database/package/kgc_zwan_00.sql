CREATE OR REPLACE PACKAGE "HELIX"."KGC_ZWAN_00" IS
-- zwangerschapsduur op moment van onderzoek (in weken + dagen)
FUNCTION duur
( p_onde_id IN NUMBER
, p_kort_lang IN VARCHAR2 := 'K'
)
RETURN VARCHAR2;
--PRAGMA RESTRICT_REFERENCES( duur, WNDS, WNPS, RNPS );
END KGC_ZWAN_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ZWAN_00" IS
/*************************************************************************************
 * Wijziging
 *                                                                                   					*
 * Datum       Door           Wijziging                                              					*
 * mei 2007    Rob Peijster   Mantis 747
 *************************************************************************************/


FUNCTION duur
( p_onde_id IN NUMBER
, p_kort_lang IN VARCHAR2 := 'K'
)
RETURN VARCHAR2
/*************************************************************************************
 * Wijziging : Mantis 474 Het gebruik van onde.datum_binnen is onjuist en moet worden*
 *                        vervangen door mons.datum_afname / mons.datum_aanmelding   *
 *                                                                                   *
 * Datum       Door           Wijziging                                              *
 * mei 2007    Rob Peijster   Mantis 747                                             *
 *************************************************************************************/
IS
  v_return VARCHAR2(100);
  v_standaard_duur NUMBER := 280;


  CURSOR onde_cur
  IS
    SELECT foet_id
    ,      pers_id
    ,      datum_binnen
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
  onde_rec onde_cur%rowtype;


  CURSOR foet_cur
  ( b_foet_id IN NUMBER
  , b_pers_id IN NUMBER
  )
  IS
    SELECT zwan.datum_aterm
    FROM   kgc_zwangerschappen zwan
    ,      kgc_foetussen foet
    WHERE  zwan.zwan_id = foet.zwan_id
    AND    zwan.pers_id = b_pers_id
    AND    foet.foet_id = b_foet_id
    ORDER BY zwan.datum_aterm DESC
    ;

  CURSOR zwan_cur
  ( b_pers_id IN NUMBER
  , b_datum IN DATE
  )
  IS
    SELECT zwan.datum_aterm
    FROM   kgc_zwangerschappen zwan
    WHERE  zwan.pers_id = b_pers_id
    AND    b_datum BETWEEN zwan.datum_aterm - v_standaard_duur
                       AND zwan.datum_aterm
    ORDER BY zwan.datum_aterm DESC
    ;


  /* mantis 474, mons_cur toegevoegd */
  cursor mons_cur
  is
  select mons.datum_afname
  ,      mons.datum_aanmelding
  from   kgc_onderzoeken onde
  ,      kgc_monsters    mons
  ,      kgc_onderzoek_monsters om
  where  mons.mons_id = om.mons_id
  and    onde.onde_id = om.onde_id
  and    onde.foet_id is not null
  and    onde.foet_id = mons.foet_id
  and    onde.onde_id = p_onde_id
  ;


  v_datum_aterm      DATE;
  v_dagen            NUMBER;
  v_datum_afname     date;      --Mantis 747
  v_datum_aanmelding date;      --Mantis 747


BEGIN
  /* ophalen ondezoekgegevens */
  OPEN  onde_cur;
  FETCH onde_cur
  INTO  onde_rec;
  CLOSE onde_cur;



  /* via foet_cur bepalen van datum_aterm */
  IF ( onde_rec.datum_binnen IS NOT NULL )
  THEN
    IF ( onde_rec.foet_id IS NOT NULL )
    THEN
      OPEN  foet_cur( b_pers_id => onde_rec.pers_id
                    , b_foet_id => onde_rec.foet_id
                    );
      FETCH foet_cur
      INTO  v_datum_aterm;
      CLOSE foet_cur;
    ELSE
      NULL;
      -- alleen zwangerschapsduur voor onderzoeken op een foetus
      -- in ieder geval NIET bij bv. postnatale onderzoeken en zeker niet als een zwangerschap voortijdig tot een eind is gekomen
      /*
      OPEN  zwan_cur( b_pers_id => onde_rec.pers_id
                    , b_datum => onde_rec.datum_binnen
                    );
      FETCH zwan_cur
      INTO  v_datum_aterm;
      CLOSE zwan_cur;
      */
    END IF;
  END IF;


  /* bepalen v_dagen = zwan_duur, adhv datum_aterm en inde_datum_binnen */
  /* Mantis 474, verwijderd */
  IF ( v_datum_aterm IS NOT NULL )
  THEN
    v_dagen := onde_rec.datum_binnen
             - ( v_datum_aterm - v_standaard_duur )
             ;

  END IF;


  /* bepalen v_dagen = zwan_duur, adhv datum_aterm en mons_datum_afname / datum_aanmelding */
  /* Mantis 474, toegevoegd */
  IF ( v_datum_aterm IS NOT NULL )
  THEN
    /*ophalen monstergegevens*/
    OPEN   mons_cur;
    FETCH  mons_cur
     INTO  v_datum_afname
        ,  v_datum_aanmelding;
    CLOSE  mons_cur;
    /*berekenen zwanduur met monstergegevens*/
    v_dagen := nvl( v_datum_afname, v_datum_aanmelding )
             - ( v_datum_aterm - v_standaard_duur )
             ;

  END IF;



  IF ( v_dagen IS NOT NULL )
  THEN
    IF ( SUBSTR( p_kort_lang, 1, 1 ) = 'L' )
    THEN
      v_return := TO_CHAR( TRUNC( v_dagen / 7 ) )||' weken'
               || ' + '
               || TO_CHAR( TRUNC( MOD( v_dagen , 7 ) ) )||' dagen'
                ;
    ELSE
      v_return := TO_CHAR( TRUNC( v_dagen / 7 ) )
               || ' + '
               || TO_CHAR( TRUNC( MOD( v_dagen , 7 ) ) )
                ;
    END IF;
  END IF;

  RETURN( v_return );

END duur;
END  kgc_zwan_00;
/

/
QUIT
