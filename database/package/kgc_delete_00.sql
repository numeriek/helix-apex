CREATE OR REPLACE PACKAGE "HELIX"."KGC_DELETE_00" IS
-- VERWIJDER
-- Alleen controles in db-triggers zullen dat verhinderen.

FUNCTION geef_aantal_bestanden
RETURN NUMBER;

FUNCTION geef_bestandsnaam(p_volgnr NUMBER)
RETURN VARCHAR2;

PROCEDURE init
( p_forceer IN BOOLEAN := FALSE -- forceer = true, afgeronde onderzoeken en metingen toch verwijderen
);
PROCEDURE verwijder_meting
( p_meti_id IN NUMBER
, x_fout OUT BOOLEAN
, x_resultaat OUT VARCHAR2
);
PROCEDURE verwijder_fractie
( p_frac_id IN NUMBER
, x_fout OUT BOOLEAN
, x_resultaat OUT VARCHAR2
);
PROCEDURE verwijder_monster
( p_mons_id IN NUMBER
, x_fout OUT BOOLEAN
, x_resultaat OUT VARCHAR2
);
PROCEDURE verwijder_onderzoek
( p_onde_id IN NUMBER
, x_fout OUT BOOLEAN
, x_resultaat OUT VARCHAR2
);
PROCEDURE verwijder_persoon
( p_pers_id IN NUMBER
, x_fout OUT BOOLEAN
, x_resultaat OUT VARCHAR2
);
END KGC_DELETE_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DELETE_00" IS
-- de g-variabelen moeten vooraf geinitialiseerd worden met kgcdelete.init
-- wanneer meerdere aanroepen in 1 sessie gedaan worden.
g_resultaat VARCHAR2(4000); -- totaal resultaat van verwijdering
g_fout BOOLEAN := FALSE; -- verwijdering niet succesvol
g_alleen_details BOOLEAN := FALSE; -- procedure roept procedure: alleen details verwijderen
g_forceer BOOLEAN := FALSE; -- afgeronde onderzoeken en metingen toch verwijderen?

-- namen van fysieke bestanden bewaren t.b.v. het verwijderen
TYPE best_tab_typ IS TABLE OF kgc_bestanden.bestand_specificatie%TYPE INDEX BY BINARY_INTEGER;
g_best_tab best_tab_typ;
g_aantal_best NUMBER;

g_tabel VARCHAR2(30); -- tabel in verwerking
e_fout EXCEPTION; -- stop de verwijdering
e_bezet EXCEPTION;
PRAGMA EXCEPTION_INIT( e_bezet, -54 ); -- resource busy
e_childs EXCEPTION;
PRAGMA EXCEPTION_INIT( e_childs, -02292 ); -- fk_constraint

-- buitenste exception handler handelt fouten af (x_resultaat)
-- sqlerrm wordt her en der vertaalt.
FUNCTION error_text
( p_tekst IN VARCHAR2 := NULL
, p_sqlerrm IN VARCHAR2 := NULL
, p_incl_res IN BOOLEAN := TRUE
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  PROCEDURE voeg_toe
  ( p_regel IN VARCHAR2
  )
  IS
BEGIN
    IF ( p_regel IS NOT NULL )
    THEN
      IF ( v_return IS NULL )
      THEN
        v_return := p_regel;
      ELSE
        v_return := v_return||CHR(10)||p_regel;
      END IF;
    END IF;
  END voeg_toe;
BEGIN
  IF ( p_incl_res )
  THEN
    voeg_toe( g_resultaat );
  END IF;
  voeg_toe( p_tekst );
  IF ( p_sqlerrm IS NOT NULL )
  THEN
    IF ( LOWER(p_sqlerrm) LIKE '%user-defined exception%' )
    THEN
      NULL;
    ELSIF ( LOWER(p_sqlerrm) LIKE '%afgerond_trg%' )
    THEN
      voeg_toe( 'Afgeronde gegevens ('||g_tabel||') kunnen niet worden verwijderd'||CHR(10) );
    ELSE
      voeg_toe( p_sqlerrm );
    END IF;
  END IF;
  RETURN( v_return );
END error_text;

-- lokale procedure om resultaatmelding op te bouwen
PROCEDURE  meld
( p_regel  IN  VARCHAR2
, p_fout  BOOLEAN  :=  FALSE  -- true: maak g_fout true als regel gevuld
)
 IS
  v_regel VARCHAR2(1000) := RTRIM( RTRIM( p_regel ), CHR(10) );
 BEGIN
  IF ( v_regel IS NOT NULL )
  THEN
    -- altijd een CHR(10) aan het eind!
    g_resultaat := RTRIM( g_resultaat, CHR(10) );
    IF ( g_resultaat IS NULL )
    THEN
      g_resultaat := v_regel;
    ELSE
      g_resultaat := g_resultaat||CHR(10)||v_regel;
    END IF;
    IF ( p_fout )
    THEN
      g_fout := TRUE;
    END IF;
    g_resultaat := RTRIM( g_resultaat, CHR(10) ) || CHR(10);
  END IF;
EXCEPTION
  WHEN VALUE_ERROR
  THEN
    NULL;
END meld;

-- meld het aantal verwijderde rijen
PROCEDURE  meld_aantal
( p_aantal  IN  NUMBER
, p_extra_info  IN  VARCHAR2 := NULL
, p_actie IN VARCHAR2 := 'D'
)
IS
  def_regel VARCHAR2(2000);
  v_aantal NUMBER;
  v_pos_va NUMBER;
  v_regel VARCHAR2(4000);
BEGIN
  IF ( NVL(p_aantal,0) > 0 )
  THEN
    -- bouw standaard regel op
    def_regel := ' rijen uit '||LOWER(g_tabel);
    IF ( p_extra_info IS NOT NULL )
    THEN
      def_regel := def_regel||' ('||p_extra_info||')';
    END IF;
    IF ( p_actie = 'U' )
    THEN
      def_regel := def_regel||' gewijzigd.';
    ELSE
      def_regel := def_regel||' verwijderd.';
    END IF;
    -- als regel al bestaat dan het aantal ophogen!
    v_pos_va := INSTR( g_resultaat, def_regel );
    IF ( v_pos_va > 0 )
    THEN
      -- alles na de eerstvolgende chr(10) weg
      v_regel := SUBSTR( g_resultaat
                       , 1
                       , INSTR( g_resultaat, CHR(10), v_pos_va ) - 1
                       );
      -- alles tot en met laatste chr(10) weg
      v_regel := SUBSTR( v_regel
                       , INSTR( v_regel, CHR(10), -1 ) + 1
                       );
      v_aantal := TO_NUMBER( RTRIM( v_regel, def_regel ) );
      -- oude regel weg
      IF ( v_regel IS NOT NULL )
      THEN
        g_resultaat := REPLACE ( g_resultaat
                               , v_regel||CHR(10)
                               , NULL
                               );
      END IF;
    END IF;
    -- nieuwe regel erbij
    IF ( v_aantal + p_aantal > 0 )
    THEN
      meld( TO_CHAR(v_aantal+p_aantal)||' '||def_regel );
    ELSE
      meld( TO_CHAR(p_aantal) ||' '||def_regel );
    END IF;
  END IF;
EXCEPTION
  WHEN VALUE_ERROR
  THEN
    NULL;
END meld_aantal;

-- ORA-00054: resource busy afhandeling
PROCEDURE  meld_bezet
( p_tabel IN VARCHAR2 := NULL
)
IS
  v_tabel VARCHAR2(30) := NVL( p_tabel, g_tabel );
BEGIN
  meld( v_tabel||' is door iemand in gebruik voor wijziging.', TRUE );
END meld_bezet;

-- check via db-triggers werkt niet (eigenaar van package != ingelogde gebruiker)
PROCEDURE check_toegang
( p_tabel IN VARCHAR2 := NULL
, p_actie IN VARCHAR2 := 'D'
)
IS
  v_tabel VARCHAR2(30) := NVL( p_tabel, g_tabel );
BEGIN
  IF ( NOT kgc_tato_00.toegang( UPPER( v_tabel ), p_actie) )
  THEN
    IF    ( p_actie = 'D' )
    THEN
      meld( 'Niet bevoegd om te verwijderen uit '||LOWER(v_tabel)||'.', TRUE );
    ELSIF ( p_actie = 'U' )
    THEN
      meld( 'Niet bevoegd om te wijzigen in '||LOWER(v_tabel)||'.', TRUE );
    END IF;
  END IF;
END check_toegang;

-- lokale procedure om entiteit-gerelateerde gegevens
-- (notities, vertalingen, declaraties) te verwijderen
PROCEDURE  verwijder_enti
 (  p_entiteit  IN  VARCHAR2
 ,  p_id  IN  NUMBER
 )
 IS
  CURSOR noti_cur
  IS
    SELECT noti_id
    FROM   kgc_notities
    WHERE  entiteit = p_entiteit
    AND    id = p_id
    FOR UPDATE OF noti_id nowait;
  CURSOR vert_cur
  IS
    SELECT vert_id
    FROM   kgc_vertalingen
    WHERE  entiteit = p_entiteit
    AND    id = p_id
    FOR UPDATE OF vert_id nowait;
  CURSOR decl_cur (b_alles IN VARCHAR2)
  IS
    SELECT decl_id
    FROM   kgc_declaraties
    WHERE  entiteit = p_entiteit
    AND    id = p_id
    -- alleen declaraties die nog niet zijn verwerkt
    AND    ( ( b_alles = 'J' )
             OR
             ( nvl(status, '-') NOT IN ( 'V','X' ) )
           )
    FOR UPDATE OF decl_id nowait;
  CURSOR best_cur
  IS
    SELECT best.best_id
         , best.bestand_specificatie
    FROM   kgc_bestanden best
    WHERE  entiteit_code = p_entiteit
    AND    entiteit_pk = p_id
    FOR UPDATE OF best_id nowait;
  v_aantal NUMBER;
  v_alles VARCHAR2(1);
  v_later_afbreken BOOLEAN := FALSE;
BEGIN
  -- Let op: g_tabel wordt niet gezet!
  v_aantal := 0;
  BEGIN
    FOR r IN noti_cur
    LOOP
      check_toegang( 'KGC_NOTITIES' );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      DELETE FROM kgc_notities
      WHERE CURRENT OF noti_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal, 'notities' );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet( 'KGC_NOTITIES' );
  END;

  v_aantal := 0;
  BEGIN
    FOR r IN vert_cur
    LOOP
      check_toegang( 'KGC_VERTALINGEN' );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      DELETE FROM kgc_vertalingen
      WHERE CURRENT OF vert_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal, 'vertalingen' );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet( 'KGC_VERTALINGEN' );
  END;

  v_aantal := 0;
  BEGIN
    IF g_forceer
    THEN
      v_alles := 'J';
    ELSE
      v_alles := 'N';
    END IF;

    FOR r IN decl_cur(v_alles)
    LOOP
      check_toegang( 'KGC_DECLARATIES' );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      DELETE FROM kgc_declaraties
      WHERE CURRENT OF decl_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal, 'declaraties' );

    -- Mantis 1193
    -- controle op het nog aanwezig zijn van declaraties
    SELECT COUNT(1)
      INTO v_aantal
      FROM kgc_declaraties
     WHERE entiteit = p_entiteit
       AND id = p_id;
    IF v_aantal > 0
    THEN
      meld( 'Er zijn nog afgehandelde declaraties aanwezig. Maak gebruik van de forceer optie.', FALSE );
      v_later_afbreken := TRUE;
    END IF;
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet( 'KGC_DECLARATIES' );
  END;

  v_aantal := 0;
  BEGIN
    FOR r IN best_cur
    LOOP
      check_toegang( 'KGC_BESTANDEN' );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      -- bewaren bestandsnamen voor fysieke verwijdering
      g_aantal_best := g_aantal_best + 1;
      g_best_tab(g_aantal_best) := r.bestand_specificatie;

      DELETE FROM kgc_bestanden
      WHERE CURRENT OF best_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal, 'bestanden' );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet( 'KGC_BESTANDEN' );
    WHEN e_childs
    THEN
      meld( 'Er zijn nog verzonden brieven aanwezig. Maak gebruik van de forceer optie.', FALSE );
      v_later_afbreken := TRUE;
  END;

  IF ( v_later_afbreken )
  THEN -- Eerder zijn fouten gemeld, maar is nog niet afgebroken; na alle meldingen pas afbreken!
    RAISE e_fout;
  END IF;

EXCEPTION
  WHEN OTHERS
  THEN
    g_fout := TRUE;
    meld( error_text ('Fout in verwijder_enti', SQLERRM, FALSE ) );
    RAISE e_fout;
END verwijder_enti;

PROCEDURE  forceer
( p_onde_id  IN  NUMBER  :=  NULL
, p_meti_id  IN  NUMBER  :=  NULL
)
IS
  CURSOR onde_cur
  ( b_onde_id IN NUMBER
  )
  IS
    SELECT onde_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = b_onde_id
    AND    afgerond = 'J'
    FOR UPDATE OF afgerond nowait;

    /* Kartiki Cursor added for Mantis 5387 */
  CURSOR onde_cur_1
   ( b_onde_id IN NUMBER
   )
   IS
    SELECT onde_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = b_onde_id
    AND    status = 'G'
    FOR UPDATE OF status nowait;

  -- Mantis 1945: brieven mogen alleen worden verwijderd als datum verzonden niet is gevuld
  -- Via geforceerd worden deze eerst leeg gemaakt
  CURSOR brie_cur
  ( b_onde_id IN NUMBER
  )
  IS
    SELECT brie_id
    FROM   kgc_brieven
    WHERE  onde_id = b_onde_id
    AND    datum_verzonden is not null
    FOR UPDATE OF datum_verzonden nowait;
  -- METI ook niet-afgeronden vanwege controle ONDE
  CURSOR meti_cur
  IS
    SELECT meti_id
    ,      onde_id
    ,      afgerond
    FROM   bas_metingen
    WHERE  meti_id = p_meti_id
    FOR UPDATE OF afgerond nowait;
  CURSOR meet_cur
  IS
    SELECT meet_id
    FROM   bas_meetwaarden
    WHERE  meti_id = p_meti_id
    AND    afgerond = 'J'
    FOR UPDATE OF afgerond nowait;
BEGIN
  IF ( g_forceer )
  THEN
    IF ( p_onde_id IS NOT NULL )
    THEN
      BEGIN
        FOR r IN onde_cur( p_onde_id )
        LOOP
          UPDATE kgc_onderzoeken
          SET    afgerond = 'N'
          WHERE CURRENT OF onde_cur;
        END LOOP;

        /* Kartiki added FOR Loop for Mantis 5387 */
        FOR r_1 IN onde_cur_1 (p_onde_id )
        LOOP
          UPDATE kgc_onderzoeken
           SET    status = 'V'
          WHERE CURRENT OF onde_cur_1;
        END LOOP;

      EXCEPTION
        WHEN e_bezet
        THEN
          meld_bezet( 'KGC_ONDERZOEKEN' );
      END;

      -- brieven verzenddatum zetten
      BEGIN
        FOR r IN brie_cur( p_onde_id )
        LOOP
          UPDATE kgc_brieven
          SET    datum_verzonden = null
          WHERE CURRENT OF brie_cur;
        END LOOP;
      EXCEPTION
        WHEN e_bezet
        THEN
          meld_bezet( 'KGC_BRIEVEN' );
      END;

    END IF;

    IF ( p_meti_id IS NOT NULL )
    THEN
      BEGIN
        FOR r IN meti_cur
        LOOP
          -- eerst onderzoek on-afronden
          BEGIN
            forceer
            ( p_onde_id => r.onde_id
            , p_meti_id => NULL
            );
          EXCEPTION
            WHEN OTHERS
            THEN
              NULL;
          END;
          IF ( r.afgerond = 'J' )
          THEN
            UPDATE bas_metingen
            SET    afgerond = 'N'
            WHERE CURRENT OF meti_cur;
          END IF;
        END LOOP;
      EXCEPTION
        WHEN e_bezet
        THEN
          meld_bezet( 'BAS_METINGEN' );
      END;
      BEGIN
        FOR r IN meet_cur
        LOOP
          UPDATE bas_meetwaarden
          SET    afgerond = 'N'
          WHERE CURRENT OF meet_cur;
        END LOOP;
      EXCEPTION
        WHEN e_bezet
        THEN
          meld_bezet( 'BAS_MEETWAARDEN' );
      END;
    END IF;
  END IF;
END forceer;

FUNCTION geef_aantal_bestanden
RETURN NUMBER
IS
BEGIN
  RETURN g_aantal_best;
END;
FUNCTION geef_bestandsnaam(p_volgnr NUMBER)
RETURN VARCHAR2
IS
BEGIN
  IF p_volgnr < 1
  OR p_volgnr > g_aantal_best
  THEN
    RETURN NULL;
  ELSE
    RETURN g_best_tab(p_volgnr);
  END IF;
END;
-- publieke procedures
-- init eerst aanroepen voorafgaand aan 2e aanroep verwijder... in dezelfde sessie
PROCEDURE  init
( p_forceer IN BOOLEAN := FALSE
)
IS
BEGIN
  g_aantal_best := 0;
  g_resultaat := NULL;
  g_fout := FALSE;
  g_alleen_details := FALSE;
  g_forceer := p_forceer;
  IF ( p_forceer )
  THEN
    meld( 'Geforceerde verwijdering: afgeronde onderzoeken / metingen / meetwaarden, verzonden brieven en verwerkte declaraties worden toch verwijderd. Alternatieve declaratie-personen bij onderzoeken en declaraties worden toch leeg gemaakt.' );
  END IF;
END init;

-- verwijder meting en alle onderliggende gegevens
PROCEDURE  verwijder_meting
 (  p_meti_id  IN  NUMBER
 ,  x_fout  OUT  BOOLEAN
 ,  x_resultaat  OUT  VARCHAR2
 )
IS
  CURSOR meti_cur
  IS
    SELECT meti_id
    ,      frac_id
    ,      onde_id
    FROM   bas_metingen
    WHERE  meti_id = p_meti_id
    FOR UPDATE OF meti_id nowait;
  CURSOR meet_cur
  IS
    SELECT meet_id
    FROM   bas_meetwaarden
    WHERE  meti_id = p_meti_id
    FOR UPDATE OF meet_id nowait;
  CURSOR bere_cur
  IS
    SELECT bere.bere_id
    FROM   bas_berekeningen bere
    ,      bas_meetwaarden meet
    WHERE  bere.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    FOR UPDATE OF bere.bere_id nowait;
  CURSOR mdet_cur
  IS
    SELECT mdet.mdet_id
    FROM   bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
    WHERE  mdet.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    FOR UPDATE OF mdet.mdet_id nowait;
  CURSOR kary_cur
  IS
    SELECT kary.kary_id
    FROM   kgc_karyogrammen kary
    ,      bas_meetwaarden meet
    WHERE  kary.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    FOR UPDATE OF kary.kary_id nowait;
  CURSOR rese_cur
  IS
    SELECT rese.rese_id
    FROM   bas_reserveringen rese
    ,      bas_meetwaarden meet
    WHERE  rese.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    FOR UPDATE OF rese.rese_id nowait;
  CURSOR wlit_cur
  IS
    SELECT wlit.meet_id
    FROM   kgc_werklijst_items wlit
    ,      bas_meetwaarden meet
    WHERE  wlit.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    FOR UPDATE OF wlit.meet_id nowait;
  CURSOR wlia_cur
  IS
    SELECT wlia.meet_id
    FROM   kgc_werklijst_items_archief wlia
    ,      bas_meetwaarden meet
    WHERE  wlia.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    FOR UPDATE OF wlia.meet_id nowait;
  CURSOR trvu_cur
  IS
    SELECT trvu.trvu_id
    FROM   kgc_tray_vulling trvu
    ,      bas_meetwaarden meet
    WHERE  trvu.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    FOR UPDATE OF trvu.trvu_id nowait;
  v_aantal NUMBER;
  alleen_details BOOLEAN := g_alleen_details;
BEGIN
  forceer
  ( p_meti_id => p_meti_id
  );
  g_tabel := 'KGC_TRAY_VULLING';
  v_aantal := 0;
  BEGIN
    FOR r IN trvu_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'TRVU', r.trvu_id );
      DELETE FROM kgc_tray_vulling
      WHERE CURRENT OF trvu_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_WERKLIJST_ITEMS';
  v_aantal := 0;
  BEGIN
    FOR r IN wlit_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      -- NB.: geen entiteit
      DELETE FROM kgc_werklijst_items
      WHERE CURRENT OF wlit_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_WERKLIJST_ITEMS_ARCHIEF';
  v_aantal := 0;
  BEGIN
    FOR r IN wlia_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      -- NB.: geen entiteit
      DELETE FROM kgc_werklijst_items_archief
      WHERE CURRENT OF wlia_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_RESERVERINGEN';
  v_aantal := 0;
  BEGIN
    FOR r IN rese_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'RESE', r.rese_id );
      DELETE FROM bas_reserveringen
      WHERE CURRENT OF rese_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_MEETWAARDE_DETAILS';
  v_aantal := 0;
  BEGIN
    FOR r IN mdet_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'MDET', r.mdet_id );
      DELETE FROM bas_meetwaarde_details
      WHERE CURRENT OF mdet_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_KARYOGRAMMEN';
  v_aantal := 0;
  BEGIN
    FOR r IN kary_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'KARY', r.kary_id );
      DELETE FROM kgc_karyogrammen
      WHERE CURRENT OF kary_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_BEREKENINGEN';
  v_aantal := 0;
  BEGIN
    FOR r IN bere_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'BERE', r.bere_id );
      DELETE FROM bas_berekeningen
      WHERE CURRENT OF bere_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_MEETWAARDEN';
  v_aantal := 0;
  BEGIN
    FOR r IN meet_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'MEET', r.meet_id );
      DELETE FROM bas_meetwaarden
      WHERE CURRENT OF meet_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  --> Mantis 1193: van ONBE-rijen type 'fractie', de declaraties verwijderen (rijen via METI trigger)
  g_tabel := 'KGC_ONDERZOEK_BETROKKENEN';
  FOR r IN meti_cur
  LOOP
    verwijder_enti( 'ONBE', kgc_onbe_00.te_verwijderen_betrokkene(p_onde_id => r.onde_id, p_frac_id => r.frac_id, p_meti_id => r.meti_id) );
  END LOOP;
  --< Mantis 1193: van ONBE-rijen type 'fractie', de declaraties verwijderen (rijen via METI trigger)

  g_tabel := 'BAS_METINGEN';
  IF ( NOT alleen_details )
  THEN
    verwijder_enti( 'METI', p_meti_id );
    BEGIN
      FOR r IN meti_cur
      LOOP
        check_toegang;
        IF ( g_fout )
        THEN
          RAISE e_fout;
        END IF;
        DELETE FROM bas_metingen
        WHERE  CURRENT OF meti_cur;
      END LOOP;
      meld( 'meting verwijderd.' );
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  END IF;
  x_resultaat := g_resultaat;
  x_fout := FALSE;
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_resultaat := error_text( 'Fout in verwijder_meting', SQLERRM );
END verwijder_meting;

-- verwijder fractie en alle onderliggende gegevens
PROCEDURE  verwijder_fractie
 (  p_frac_id  IN  NUMBER
 ,  x_fout  OUT  BOOLEAN
 ,  x_resultaat  OUT  VARCHAR2
 )
 IS
  CURSOR frac_cur
  IS
    SELECT frac_id
    FROM   bas_fracties
    WHERE  frac_id = p_frac_id
    FOR UPDATE OF frac_id nowait;
  CURSOR voor_cur
  IS
    SELECT voor_id
    FROM   bas_voorraad
    WHERE  frac_id = p_frac_id
    FOR UPDATE OF voor_id nowait;
  CURSOR meti_cur
  IS
    SELECT meti_id
    FROM   bas_metingen
    WHERE  frac_id = p_frac_id
    FOR UPDATE OF meti_id nowait;
  CURSOR isol_cur
  IS
    SELECT frac_id
    FROM   bas_isolatielijsten
    WHERE  frac_id = p_frac_id
    FOR UPDATE OF frac_id nowait;
  v_aantal NUMBER;
  alleen_details BOOLEAN := g_alleen_details;
BEGIN
  g_tabel := 'BAS_METINGEN';
  check_toegang;
  v_aantal := 0;
  -- aanroep andere procedure, maar metingen zelf worden hier verwijderd
  g_alleen_details := TRUE;
  BEGIN
    FOR r IN meti_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_meting
      ( p_meti_id => r.meti_id
      , x_fout => g_fout
      , x_resultaat => g_resultaat
      );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      g_tabel := 'BAS_METINGEN';
      verwijder_enti( 'METI', r.meti_id );
      DELETE FROM bas_metingen
      WHERE CURRENT OF meti_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_ISOLATIELIJSTEN';
  v_aantal := 0;
  BEGIN
    FOR r IN isol_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'ISOL', 0 ); -- geen pk!
      DELETE FROM bas_isolatielijsten
      WHERE CURRENT OF isol_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_VOORRAAD';
  v_aantal := 0;
  BEGIN
    FOR r IN voor_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'VOOR', r.voor_id );
      DELETE FROM bas_voorraad
      WHERE CURRENT OF voor_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_FRACTIES';
  IF ( NOT alleen_details )
  THEN
    verwijder_enti( 'FRAC', p_frac_id );
    BEGIN
      FOR r IN frac_cur
      LOOP
        check_toegang;
        IF ( g_fout )
        THEN
          RAISE e_fout;
        END IF;
        DELETE FROM bas_fracties
        WHERE  frac_id = p_frac_id;
      END LOOP;
      meld( 'fractie verwijderd.' );
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet('Fracties' );
    END;
  END IF;

  x_resultaat := g_resultaat;
  x_fout := FALSE;
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_resultaat := error_text ('Fout in verwijder_fractie',SQLERRM);
END verwijder_fractie;

-- verwijder monster en alle onderliggende gegevens
PROCEDURE  verwijder_monster
 (  p_mons_id  IN  NUMBER
 ,  x_fout  OUT  BOOLEAN
 ,  x_resultaat  OUT  VARCHAR2
 )
 IS
  CURSOR mons_cur
  IS
    SELECT mons_id
    FROM   kgc_monsters
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF mons_id nowait;
  CURSOR frac_cur
  IS
    SELECT frac_id
    FROM   bas_fracties
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF frac_id nowait;
  CURSOR opwe_cur
  IS
    SELECT opwe_id
    FROM   bas_opwerken
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF opwe_id nowait;
  CURSOR voop_cur
  IS
    SELECT voop_id
    FROM   kgc_vooropslag
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF voop_id nowait;
  CURSOR onmo_cur
  IS
    SELECT onmo_id
    FROM   kgc_onderzoek_monsters
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF onmo_id nowait;
  CURSOR moin_cur
  IS
    SELECT moin_id
    FROM   kgc_monster_indicaties
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF moin_id nowait;
  CURSOR gemo_cur
  IS
    SELECT gemo_id
    FROM   kgc_gebe_mons
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF mons_id nowait;
  CURSOR mobw_cur
  IS
    SELECT mobw_id
    FROM   kgc_monster_bewaarredenen mobw
    WHERE  mons_id = p_mons_id
    FOR UPDATE OF mobw_id nowait;
  v_aantal NUMBER;
  alleen_details BOOLEAN := g_alleen_details;
BEGIN
  g_tabel := 'BAS_FRACTIES';
  v_aantal := 0;
  -- aanroep andere procedure, maar fracties zelf worden hier verwijderd
  g_alleen_details := TRUE;
  BEGIN
    FOR r IN frac_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_fractie
      ( p_frac_id => r.frac_id
      , x_fout => g_fout
      , x_resultaat => g_resultaat
      );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      g_tabel := 'BAS_FRACTIES';
      verwijder_enti( 'FRAC', r.frac_id );
      DELETE FROM bas_fracties
      WHERE CURRENT OF frac_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_VOOROPSLAG';
  v_aantal := 0;
  BEGIN
    FOR r IN voop_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'VOOP', r.voop_id );
      DELETE FROM kgc_vooropslag
      WHERE CURRENT OF voop_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'BAS_OPWERKEN';
  v_aantal := 0;
  BEGIN
    FOR r IN opwe_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'OPWE', r.opwe_id );
      DELETE FROM bas_opwerken
      WHERE CURRENT OF opwe_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_MONSTER_INDICATIES';
  v_aantal := 0;
  BEGIN
    FOR r IN moin_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'MOIN', r.moin_id );
      DELETE FROM kgc_monster_indicaties
      WHERE CURRENT OF moin_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_ONDERZOEK_MONSTERS';
  v_aantal := 0;
  BEGIN
    FOR r IN onmo_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'ONMO', r.onmo_id );
      DELETE FROM kgc_onderzoek_monsters
      WHERE CURRENT OF onmo_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_GEBE_MONS';
  v_aantal := 0;
  BEGIN
    FOR r IN gemo_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'GEMO', r.gemo_id );
      DELETE kgc_gebe_mons
      WHERE CURRENT OF gemo_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_MONSTER_BEWAARREDENEN';
  v_aantal := 0;
  BEGIN
    FOR r IN mobw_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'MOBW', r.mobw_id );
      DELETE kgc_monster_bewaarredenen
      WHERE CURRENT OF mobw_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_MONSTERS';
  IF ( NOT alleen_details )
  THEN
    verwijder_enti( 'MONS', p_mons_id );
    BEGIN
      FOR r IN mons_cur
      LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
        DELETE FROM kgc_monsters
        WHERE  mons_id = p_mons_id;
      END LOOP;
      meld( 'monster verwijderd.' );
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  END IF;

  x_resultaat := g_resultaat;
  x_fout := FALSE;
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_resultaat := error_text ('Fout in verwijder_monster',SQLERRM);
END verwijder_monster;

-- verwijder onderzoek en alle onderliggende gegegevens
PROCEDURE  verwijder_onderzoek
 (  p_onde_id  IN  NUMBER
 ,  x_fout  OUT  BOOLEAN
 ,  x_resultaat  OUT  VARCHAR2
 )
 IS
  CURSOR onde_cur
  IS
    SELECT onde_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF onde_id nowait;
  CURSOR meti_cur
  IS
    SELECT meti_id
    FROM   bas_metingen
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF meti_id nowait;
  CURSOR gebe_cur
  IS
    SELECT gebe.gebe_id
    FROM   kgc_gesprekken gesp
    ,      kgc_gesprek_betrokkenen gebe
    WHERE  gebe.gesp_id = gesp.gesp_id
    AND    gesp.onde_id = p_onde_id
    FOR UPDATE OF gebe.gebe_id nowait;
  CURSOR gesp_cur
  IS
    SELECT gesp_id
    FROM   kgc_gesprekken
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF gesp_id nowait;
  CURSOR onkh_cur
  IS
    SELECT onkh_id
    FROM   kgc_onderzoek_kopiehouders
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF onkh_id nowait;
  CURSOR uits_cur
  IS
    SELECT uits_id
    FROM   kgc_uitslagen
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF uits_id nowait;
  CURSOR koho_cur
  IS
    SELECT koho.koho_id
    FROM   kgc_kopiehouders koho
    ,      kgc_uitslagen uits
    WHERE  koho.uits_id = uits.uits_id
    AND    uits.onde_id = p_onde_id
    FOR UPDATE OF koho.koho_id nowait;
  CURSOR uico_cur
  IS
    SELECT uico.uico_id
    FROM   kgc_uitslag_conclusies uico
    ,      kgc_uitslagen uits
    WHERE  uico.uits_id = uits.uits_id
    AND    uits.onde_id = p_onde_id
    FOR UPDATE OF uico.uico_id nowait;
  CURSOR brie_cur
  IS
    SELECT brie_id
    FROM   kgc_brieven
    WHERE  onde_id = p_onde_id
      AND  datum_verzonden is NULL -- extra voorwaarde Mantis 1945
    FOR UPDATE OF brie_id nowait;
  CURSOR fobe_cur
  IS
    SELECT fobe_id
    FROM   kgc_faon_betrokkenen
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF fobe_id nowait;
  CURSOR onmo_cur
  IS
    SELECT onmo_id
    FROM   kgc_onderzoek_monsters
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF onmo_id nowait;
  CURSOR onin_cur
  IS
    SELECT onin_id
    FROM   kgc_onderzoek_indicaties
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF onin_id nowait;
  -- Mantis 1945 nieuwe tabel die geschoond moet worden
  CURSOR onah_cur
  IS
    SELECT onah_id
    FROM   kgc_onde_autorisatie_historie
    WHERE  onde_id = p_onde_id
    FOR UPDATE OF onah_id nowait;
  -- Mantis 1193: toegevoegd om ONBE-declaraties te verwijderen
  CURSOR onbe_cur
  IS
    SELECT onbe_id
    FROM   kgc_onderzoek_betrokkenen
    WHERE  onde_id = p_onde_id;
  -- Kartiki Cursor added for Mantis 5387
  CURSOR reon_cur
  IS
    SELECT onde_id
    FROM   KGC_GERELATEERDE_ONDERZOEKEN
    WHERE  (onde_id = p_onde_id
         OR onde_id_gerelateerde = p_onde_id)
    FOR UPDATE OF onde_id nowait;

  v_aantal NUMBER;
  alleen_details BOOLEAN := g_alleen_details;
BEGIN
  forceer
  ( p_onde_id => p_onde_id
  );
  g_tabel := 'BAS_METINGEN';
  v_aantal := 0;
  -- aanroep andere procedure, maar metingen zelf worden hier verwijderd
  g_alleen_details := TRUE;
  BEGIN
    FOR r IN meti_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_meting
      ( p_meti_id => r.meti_id
      , x_fout => g_fout
      , x_resultaat => g_resultaat
      );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      g_tabel := 'BAS_METINGEN';
      verwijder_enti( 'METI', r.meti_id );
      DELETE FROM bas_metingen
      WHERE CURRENT OF meti_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_ONDERZOEK_KOPIEHOUDERS';
  v_aantal := 0;
  BEGIN
    FOR r IN onkh_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'ONKH', r.onkh_id );
      DELETE FROM kgc_onderzoek_kopiehouders
      WHERE CURRENT OF onkh_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_GESPREK_BETROKKENEN';
  v_aantal := 0;
  BEGIN
    FOR r IN gebe_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'GEBE', r.gebe_id );
      DELETE FROM kgc_gesprek_betrokkenen
      WHERE CURRENT OF gebe_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_GESPREKKEN';
  v_aantal := 0;
  BEGIN
    FOR r IN gesp_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'GESP', r.gesp_id );
      DELETE FROM kgc_gesprekken
      WHERE CURRENT OF gesp_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_BRIEVEN';
  v_aantal := 0;
  BEGIN
    FOR r IN brie_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'BRIE', r.brie_id );
      DELETE FROM kgc_brieven
      WHERE CURRENT OF brie_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_UITSLAG_CONCLUSIES';
  v_aantal := 0;
  BEGIN
    FOR r IN uico_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'UICO', r.uico_id );
      DELETE FROM kgc_uitslag_conclusies
      WHERE CURRENT OF uico_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_KOPIEHOUDERS';
  v_aantal := 0;
  BEGIN
    FOR r IN koho_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'KOHO', r.koho_id );
      DELETE FROM kgc_kopiehouders
      WHERE CURRENT OF koho_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_UITSLAGEN';
  v_aantal := 0;
  BEGIN
    FOR r IN uits_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'UITS', r.uits_id );
      DELETE FROM kgc_uitslagen
      WHERE CURRENT OF uits_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
    WHEN e_childs
    THEN
      -- Mantis 1945: uitzonderingen voor verzonden brieven
      IF instr(SQLERRM,'KGC_BRIE_UITS_FK') > 0
      THEN
        meld( 'Er zijn nog verzonden brieven aanwezig. Maak gebruik van de forceer optie.', TRUE );
        RAISE e_fout;
      ELSE
        RAISE;
      END IF;
  END;

  g_tabel := 'KGC_FAON_BETROKKENEN';
  v_aantal := 0;
  BEGIN
    FOR r IN fobe_cur
    LOOP
      check_toegang( p_tabel => NULL, p_actie => 'U' );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      -- betrokkene blijft: alleen onderzoek verwijderd
      UPDATE kgc_faon_betrokkenen
      SET    onde_id = NULL
      WHERE CURRENT OF fobe_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal, 'adviesvraag', 'U' );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_ONDERZOEK_INDICATIES';
  v_aantal := 0;
  BEGIN
    FOR r IN onin_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'ONIN', r.onin_id );
      DELETE FROM kgc_onderzoek_indicaties
      WHERE CURRENT OF onin_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_ONDE_AUTORISATIE_HISTORIE';
  v_aantal := 0;
  BEGIN
    FOR r IN onah_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'ONAH', r.onah_id );
      DELETE FROM kgc_onde_autorisatie_historie
      WHERE CURRENT OF onah_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_ONDERZOEK_MONSTERS';
  v_aantal := 0;
  BEGIN
    FOR r IN onmo_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'ONMO', r.onmo_id );
      DELETE FROM kgc_onderzoek_monsters
      WHERE CURRENT OF onmo_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  --> Mantis 1193: van ONBE-rijen type 'persoon', de declaraties verwijderen (rijen via ONDE trigger)
  g_tabel := 'KGC_ONDERZOEK_BETROKKENEN';
  FOR r IN onbe_cur
  LOOP
    verwijder_enti( 'ONBE', r.onbe_id );
  END LOOP;
  --< Mantis 1193: van ONBE-rijen type 'persoon', de declaraties verwijderen (rijen via ONDE trigger)

  -- Added by kartiki for Mantis 5387
  g_tabel := 'KGC_GERELATEERDE_ONDERZOEKEN';
  v_aantal := 0;
  BEGIN
    FOR r IN reon_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'REON', r.onde_id );

      DELETE FROM KGC_GERELATEERDE_ONDERZOEKEN
      WHERE CURRENT OF reon_cur;

      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;
-- Till here Kartiki 5387


  g_tabel := 'KGC_ONDERZOEKEN';
  IF ( NOT alleen_details )
  THEN
    verwijder_enti( 'ONDE', p_onde_id );
    BEGIN
      FOR r IN onde_cur
      LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
        DELETE FROM kgc_onderzoeken
        WHERE  onde_id = p_onde_id;
      END LOOP;
      meld( 'onderzoek verwijderd.' );
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  END IF;

  x_resultaat := g_resultaat;
  x_fout := FALSE;
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_resultaat := error_text ('Fout in verwijder_onderzoek',SQLERRM);
END verwijder_onderzoek;

-- verwijder persoon en alle onderliggende gegevens
PROCEDURE  verwijder_persoon
 (  p_pers_id  IN  NUMBER
 ,  x_fout  OUT  BOOLEAN
 ,  x_resultaat  OUT  VARCHAR2
 )
 IS
  CURSOR pers_cur
  IS
    SELECT pers_id
    FROM   kgc_personen
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF pers_id nowait;
  CURSOR mons_cur
  IS
    SELECT mons_id
    FROM   kgc_monsters
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF mons_id nowait;
  CURSOR brie_cur
  IS
    SELECT brie_id
    FROM   kgc_brieven
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF brie_id nowait;
  CURSOR onde_cur
  IS
    SELECT onde_id
    FROM   kgc_onderzoeken
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF onde_id nowait;
  CURSOR fale_cur
  IS
    SELECT fale_id
    FROM   kgc_familie_leden
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF fale_id nowait;
  CURSOR fobe_cur
  IS
    SELECT fobe_id
    FROM   kgc_faon_betrokkenen
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF fobe_id nowait;
  CURSOR foet1_cur
  IS
    SELECT foet_id
    FROM   kgc_foetussen
    WHERE  pers_id_moeder = p_pers_id
    FOR UPDATE OF foet_id nowait;
  CURSOR foet2_cur
  IS
    SELECT foet_id
    FROM   kgc_foetussen
    WHERE  pers_id_geboren_als = p_pers_id
    FOR UPDATE OF foet_id nowait;
  CURSOR zwan_cur
  IS
    SELECT zwan_id
    FROM   kgc_zwangerschappen
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF zwan_id nowait;
  CURSOR gebe_cur
  IS
    SELECT gebe_id
    FROM   kgc_gesprek_betrokkenen
    WHERE  pers_id = p_pers_id
    FOR UPDATE OF gebe_id nowait;
  CURSOR decl_cur (b_alles IN VARCHAR2)
  IS
    SELECT decl_id
    FROM   kgc_declaraties
    WHERE  pers_id = p_pers_id
     -- alleen declaraties die nog niet zijn verwerkt -- alternatieve persoon in declaraties niet verwijderen
      AND  ( b_alles = 'J'  OR
             nvl(status, '-' ) NOT IN ( 'V', 'X' ) )
   FOR UPDATE OF decl_id nowait;
  CURSOR onde_alt_pers_cur
  IS
    SELECT onde.onde_id
         , onde.onderzoeknr
         , onde.afgerond
         , pers.aanspreken
    FROM   kgc_onderzoeken onde
         , kgc_personen pers
    WHERE  onde.pers_id_alt_decl = p_pers_id
      AND  pers.pers_id = onde.pers_id
    FOR UPDATE OF onde_id NOWAIT
    ORDER BY 4, 2;
  CURSOR decl_alt_pers_cur
  IS
    SELECT decl.decl_id
         , decode(decl.entiteit,
                    'ONDE', 'Onderzoek',
                    'ONBE', 'Betrokkene',
                    'MONS', 'Monsterafname',
                    'METI', 'Deelonderzoek (meting)',
                    'GESP', 'Counseling',
                    decl.entiteit
                 ) enti_omschr
         , decl.status
         , pers.aanspreken
    FROM   kgc_declaraties decl
         , kgc_personen pers
    WHERE  decl.pers_id_alt_decl = p_pers_id
      AND  pers.pers_id = decl.pers_id
    FOR UPDATE OF decl_id NOWAIT
    ORDER BY 4, 2;
  v_aantal NUMBER;
  v_alles VARCHAR2(1);
  alleen_details BOOLEAN := g_alleen_details;
  v_later_afbreken BOOLEAN := FALSE;
BEGIN
  g_tabel := 'KGC_FAON_BETROKKENEN';
  -- eerst fobe, omdat ze hier worden verwijderd
  -- bij verwijder_onderzoek is er alleen een update
  v_aantal := 0;
  BEGIN
    FOR r IN fobe_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'FOBE', r.fobe_id );
      DELETE FROM kgc_faon_betrokkenen
      WHERE CURRENT OF fobe_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_MONSTERS';
  v_aantal := 0;
  -- aanroep andere procedure, maar monsters zelf worden hier verwijderd
  g_alleen_details := TRUE;
  BEGIN
    FOR r IN mons_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_monster
      ( p_mons_id => r.mons_id
      , x_fout => g_fout
      , x_resultaat => g_resultaat
      );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      g_tabel := 'KGC_MONSTERS';
      verwijder_enti( 'MONS', r.mons_id );
      DELETE FROM kgc_monsters
      WHERE CURRENT OF mons_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_BRIEVEN';
  v_aantal := 0;
  g_alleen_details := TRUE;
  BEGIN
    FOR r IN brie_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'BRIE', r.brie_id );
      DELETE FROM kgc_brieven
      WHERE CURRENT OF brie_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_ONDERZOEKEN';
  v_aantal := 0;
  -- aanroep andere procedure, maar onderzoeken zelf worden hier verwijderd
  g_alleen_details := TRUE;
  BEGIN
    FOR r IN onde_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_onderzoek
      ( p_onde_id => r.onde_id
      , x_fout => g_fout
      , x_resultaat => g_resultaat
      );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      g_tabel := 'KGC_ONDERZOEKEN';
      verwijder_enti( 'ONDE', r.onde_id );
      DELETE FROM kgc_onderzoeken
      WHERE CURRENT OF onde_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet('Onderzoeken' );
  END;

  g_tabel := 'KGC_FAMILIE_LEDEN';
  v_aantal := 0;
  BEGIN
    FOR r IN fale_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'FALE', r.fale_id );
      DELETE FROM kgc_familie_leden
      WHERE CURRENT OF fale_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_FOETUSSEN';
  v_aantal := 0;
  BEGIN
    FOR r IN foet1_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'FOET', r.foet_id );
      DELETE FROM kgc_foetussen
      WHERE CURRENT OF foet1_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_FOETUSSEN';
  v_aantal := 0;
  BEGIN
    FOR r IN foet2_cur
    LOOP
      check_toegang( p_tabel => NULL, p_actie => 'U' );
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      -- foetus wordt niet verwijderd, geboren_als wordt geleegd
      UPDATE kgc_foetussen
      SET    pers_id_geboren_als = NULL
      WHERE CURRENT OF foet2_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal, 'geboren als', 'U' );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_ZWANGERSCHAPPEN';
  v_aantal := 0;
  BEGIN
    FOR r IN zwan_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'ZWAN', r.zwan_id );
      DELETE FROM kgc_zwangerschappen
      WHERE CURRENT OF zwan_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_GESPREK_BETROKKENEN';
  v_aantal := 0;
  BEGIN
    FOR r IN gebe_cur
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'GEBE', r.gebe_id );
      DELETE FROM kgc_gesprek_betrokkenen
      WHERE CURRENT OF gebe_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  g_tabel := 'KGC_DECLARATIES';
  v_aantal := 0;
  BEGIN
    IF g_forceer
    THEN
      v_alles := 'J';
    ELSE
      v_alles := 'N';
    END IF;
    FOR r IN decl_cur (v_alles)
    LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
      verwijder_enti( 'DECL', r.decl_id );
      DELETE FROM kgc_declaraties
      WHERE CURRENT OF decl_cur;
      v_aantal := v_aantal + 1;
    END LOOP;
    meld_aantal( v_aantal );
    -- Mantis 1193: controle of er achtergebleven declaraties zijn
    SELECT COUNT(1)
      INTO v_aantal
      FROM kgc_declaraties
     WHERE pers_id = p_pers_id;
    IF v_aantal > 0
    THEN
      meld( 'Er zijn nog afgehandelde declaraties aanwezig. Maak gebruik van de forceer optie.', FALSE );
      v_later_afbreken := TRUE;
    END IF;
  EXCEPTION
    WHEN e_bezet
    THEN
      meld_bezet;
  END;

  -- Mantis 1268: persoon is alternatieve persoon bij een onderzoek
  IF ( g_forceer = FALSE )
  THEN -- Controleren
    g_tabel := 'KGC_ONDERZOEKEN';
    v_aantal := 0;
    BEGIN
      FOR r IN onde_alt_pers_cur
      LOOP
        IF ( v_aantal = 0 )
        THEN
          meld( 'Persoon treedt op als alt. decl. persoon bij onderzoek(en):', FALSE );
        END IF;
        meld( ' > ' || r.onderzoeknr || ' van persoon '|| r.aanspreken, FALSE );
        v_aantal := v_aantal + 1;
      END LOOP;
      IF ( v_aantal > 0 )
      THEN
        meld( 'Maak gebruik van de forceer optie.', FALSE );
        v_later_afbreken := TRUE;
      END IF;
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  ELSE -- Verwerken
    g_tabel := 'KGC_ONDERZOEKEN';
    v_aantal := 0;
    BEGIN
      FOR r IN onde_alt_pers_cur
      LOOP
        check_toegang( p_tabel => NULL, p_actie => 'U' );
        IF ( g_fout )
        THEN
          RAISE e_fout;
        END IF;
        -- onderzoek wordt niet verwijderd, pers_id_alt_decl wordt geleegd
        IF ( r.afgerond = 'J' )
        THEN -- Tijdelijk niet-afgerond
          UPDATE kgc_onderzoeken
          SET    afgerond = 'N'
          WHERE CURRENT OF onde_alt_pers_cur;
        END IF;
        UPDATE kgc_onderzoeken
        SET    pers_id_alt_decl = NULL
        WHERE CURRENT OF onde_alt_pers_cur;
        IF ( r.afgerond = 'J' )
        THEN -- Weer wel-afgerond
          UPDATE kgc_onderzoeken
          SET    afgerond = 'J'
          WHERE CURRENT OF onde_alt_pers_cur;
        END IF;
        v_aantal := v_aantal + 1;
      END LOOP;
      meld_aantal( v_aantal, 'alt. decl. persoon', 'U' );
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  END IF;

  -- Mantis 1268: persoon is alternatieve persoon bij declaratie
  IF ( g_forceer = FALSE )
  THEN -- Controleren
    g_tabel := 'KGC_DECLARATIES';
    v_aantal := 0;
    BEGIN
      FOR r IN decl_alt_pers_cur
      LOOP
        IF ( v_aantal = 0 )
        THEN
          meld( 'Persoon treedt op als alt. decl. persoon bij declaratie(s):', FALSE );
        END IF;
        meld( ' > ' || r.decl_id || ' m.b.t. ' || r.enti_omschr || ' van persoon '|| r.aanspreken, FALSE );
        v_aantal := v_aantal + 1;
      END LOOP;
      IF ( v_aantal > 0 )
      THEN
        meld( 'Maak gebruik van de forceer optie.', FALSE );
        v_later_afbreken := TRUE;
      END IF;
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  ELSE -- Verwerken (nodig voor verwerkte declaraties)
    g_tabel := 'KGC_DECLARATIES';
    v_aantal := 0;
    BEGIN
      FOR r IN decl_alt_pers_cur
      LOOP
        check_toegang( p_tabel => NULL, p_actie => 'U' );
        IF ( g_fout )
        THEN
          RAISE e_fout;
        END IF;
        -- onderzoek wordt niet verwijderd, pers_id_alt_decl wordt geleegd
        UPDATE kgc_declaraties
        SET    pers_id_alt_decl = NULL
        WHERE CURRENT OF decl_alt_pers_cur;
        v_aantal := v_aantal + 1;
      END LOOP;
      meld_aantal( v_aantal, 'alt. decl. persoon', 'U' );
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  END IF;

  IF ( v_later_afbreken )
  THEN -- Eerder zijn fouten gemeld, maar is nog niet afgebroken; na alle meldingen pas afbreken!
    g_fout := TRUE;
    RAISE e_fout;
  END IF;

  g_tabel := 'KGC_PERSONEN';
  IF ( NOT alleen_details )
  THEN
    verwijder_enti( 'PERS', p_pers_id );
    BEGIN
      FOR r IN pers_cur
      LOOP
      check_toegang;
      IF ( g_fout )
      THEN
        RAISE e_fout;
      END IF;
        DELETE FROM kgc_personen
        WHERE  pers_id = p_pers_id;
      END LOOP;
      meld( 'persoon verwijderd.' );
    EXCEPTION
      WHEN e_bezet
      THEN
        meld_bezet;
    END;
  END IF;

  x_resultaat := g_resultaat;
  x_fout := FALSE;
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_resultaat := error_text ('Fout in verwijder_persoon',SQLERRM);
END verwijder_persoon;
END  KGC_DELETE_00;
/

/
QUIT
