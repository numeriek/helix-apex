CREATE OR REPLACE PACKAGE "HELIX"."KGC_GESP_00" IS
-- PL/SQL Specification
FUNCTION declaratie_persoon
( p_gesp_id IN NUMBER
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( declaratie_persoon, WNDS, WNPS );


END KGC_GESP_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_GESP_00" IS
FUNCTION declaratie_persoon
( p_gesp_id IN NUMBER
)
RETURN NUMBER
IS
  v_return NUMBER;
   CURSOR gebe_cur
   IS
     SELECT pers_id
     FROM   kgc_gesprek_betrokkenen
     WHERE  gesp_id = p_gesp_id
     AND    declaratie = 'J'
     ORDER BY gebe_id -- eerst ingevoerde eerst
     ;
-- PL/SQL Block
BEGIN
  OPEN gebe_cur;
  FETCH gebe_cur
  INTO v_return;
  CLOSE gebe_cur;
  RETURN( v_return );
END declaratie_persoon;

END kgc_gesp_00;
/

/
QUIT
