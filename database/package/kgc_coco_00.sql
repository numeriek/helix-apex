CREATE OR REPLACE PACKAGE "HELIX"."KGC_COCO_00" IS
v_is_locatie_leverancier constant boolean := false; -- nu capgemi
v_is_locatie_maastricht constant boolean := false;
v_is_locatie_nijmegen constant boolean := true;
v_is_locatie_utrecht constant boolean := false;
v_is_locatie_overige constant boolean := false; -- voor het geval dat!
END KGC_COCO_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_COCO_00" IS
END KGC_COCO_00;
/

/
QUIT
