CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_ENCRYPTION"
IS
   /*==============================================================================
   Package Name : gen_pck_encryption
   Usage        : This package is used to encrypt and decrypt user passwords.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   -- Function to encrypt a value, based on the DES algorithm.
   FUNCTION f_encrypt(i_str IN VARCHAR2)
      RETURN VARCHAR2;
   -- Function to decrypt a value, based on the DES algorithm.
   FUNCTION f_decrypt(i_str IN VARCHAR2)
      RETURN VARCHAR2;
   -- Function to generate a checksum, based on the MD5 algorithm.
   FUNCTION f_checksum(i_str IN VARCHAR2)
      RETURN VARCHAR2;
END gen_pck_encryption;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_ENCRYPTION"
IS
   /*==============================================================================
   Package Name : gen_pck_encryption
   Usage        : This package is used to encrypt and decrypt user passwords.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   g_str  VARCHAR2(4000);
   g_key  VARCHAR2(32) := 'A%DS%$W%F*KDLE(^SDAD*S(Fnull*D';
   -- ------------------------------------------------------------------
   -- Function to encrypt a value, based on the DES algorithm.
   -- ------------------------------------------------------------------
   FUNCTION f_encrypt(i_str IN VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      g_str   := NULL;
      dbms_obfuscation_toolkit.des3encrypt(
         input_string => RPAD(i_str
                             ,(TRUNC(LENGTH(i_str) / 8) + 1) * 8
                             ,CHR(0))
        ,key_string => g_key
        ,encrypted_string => g_str);
      RETURN g_str;
   END f_encrypt;
   -- ------------------------------------------------------------------
   -- Function to decrypt a value, based on the DES algorithm.
   -- ------------------------------------------------------------------
   FUNCTION f_decrypt(i_str IN VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      g_str   := NULL;
      dbms_obfuscation_toolkit.des3decrypt(input_string => i_str
                                          ,key_string => g_key
                                          ,decrypted_string => g_str);
      RETURN g_str;
   END f_decrypt;
   -- ---------------------------------------------------------------------------
   -- Function to generate a checksum, based on the MD5 algorithm.
   -- ---------------------------------------------------------------------------
   FUNCTION f_checksum(i_str IN VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN dbms_obfuscation_toolkit.md5(
                input => utl_raw.cast_to_raw('P' || UPPER(i_str) || 'L'));
   END f_checksum;
END gen_pck_encryption;
/

/
QUIT
