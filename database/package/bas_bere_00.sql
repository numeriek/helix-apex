CREATE OR REPLACE PACKAGE "HELIX"."BAS_BERE_00" IS
/******************************************************************************
 NAME: BAS_BERE_00
REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.4        31-03-2016   Athar Shaikh     Mantis 3195: Enzym bijlage brief: normaalwaarden  met de juiste aantallen  decimalen tonen
                                               In function normaalwaarden added parameter p_aantal_decemalen
  *****************************************************************************/
g_in_progress  BOOLEAN  :=  FALSE;
-- bereken de waarde voor berekeningstype(s)
-- voor een bepaalde stoftest (via meetwaarde)
-- 1. direct: de berekende waarde komt in bas_meetwaarden (NB.: ook in de vorm van een functie)
-- 2. de overige berekeningstypes (EIWIT, COX, CS): de berekende waarde komt in bas_berekeningen
-- wordt (ook) aangeroepen vanuit db-trigger (bas_meetwaarden AUS): geen commit
PROCEDURE berekening
( p_meet_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_bewy_id IN NUMBER := NULL
, p_form_id IN NUMBER := NULL
, p_negeer_afgerond in varchar2 := 'N'
, p_commit IN BOOLEAN := TRUE
);

-- normaalwaarden van een berekening
FUNCTION normaalwaarden
( p_bere_id IN NUMBER
, p_aantal_decemalen IN NUMBER := NULL -- Added by Athar Shaikh for Mantis-3195 on 31-03-2016
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (normaalwaarden, WNDS, WNPS, RNPS );

-- functie eigen_meetwaarde kan gebruikt worden bij formule-parameters
-- en retourneert de meetwaarde van de stoftest
FUNCTION eigen_meetwaarde
( p_meet_id IN NUMBER
)
RETURN  NUMBER;

--  Reken alle formules door die door een meetwaarde worden geraakt
PROCEDURE gewijzigde_meetwaarde
( p_meet_id  IN  bas_meetwaarden.meet_id%TYPE
);

-- voer alle berekeningen uit voor een bepaalde formule
PROCEDURE herbereken_formule
( p_form_id  IN      NUMBER
, x_fout     IN OUT  BOOLEAN
, x_melding  IN OUT  VARCHAR2
);

-- haalt afhankelijk van p_kolom gegevens over van het berekeningstype
-- bij een meting
FUNCTION uitslag
( p_meet_id IN NUMBER
, p_berekening_type IN VARCHAR2
, p_kolom IN VARCHAR2
)
RETURN  VARCHAR2;
PRAGMA    RESTRICT_REFERENCES    (uitslag,    WNDS,    WNPS,    RNPS    );

-- bepaal of het berekeningstype moet worden opgenomen in de uitslag
FUNCTION in_uitslag
( p_bety_id IN NUMBER
, p_stof_id IN NUMBER
, p_mate_id IN NUMBER
, p_frty_id IN NUMBER
, p_stgr_id IN NUMBER
)
RETURN VARCHAR2;
FUNCTION in_uitslag
( p_meet_id IN NUMBER
, p_bewy_id IN NUMBER
)
RETURN VARCHAR2;

-- als de meetwaarde buiten de rapportagegrenzen valt wordt de waarde anders getoond
procedure bepaal_tonen_als
( p_meet_id IN NUMBER
, p_meetwaarde IN VARCHAR2
, x_tonen_als out varchar2
, x_eenheid out varchar2
);
FUNCTION bepaal_tonen_als
( p_meet_id IN NUMBER
, p_meetwaarde IN VARCHAR2
)
return varchar2;

FUNCTION formatteer
(p_bere_id  IN NUMBER
,p_uitslag  IN NUMBER
)
RETURN VARCHAR2;
END BAS_BERE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_BERE_00" IS
/******************************************************************************
 NAME: BAS_BERE_00
REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.4        31-03-2016   Athar Shaikh     Mantis 3195: Enzym bijlage brief: normaalwaarden  met de juiste aantallen  decimalen tonen
                                               In function normaalwaarden added parameter p_aantal_decemalen
  *****************************************************************************/
-- =====================================================================
-- LOKAAL
--
-- de meetwaarde van een stoftest in een meting / fractie
FUNCTION meetwaarde
( p_meti_id          IN NUMBER -- of binnen meting
, p_frac_id          IN NUMBER -- of binnen fractie
, p_onmo_id          IN NUMBER -- of binnen monster
, p_frty_id          IN NUMBER -- en fractietype
, p_stof_id          IN NUMBER
)
RETURN NUMBER
IS
  v_waarde         NUMBER(38,10);
  -- alle cursoren moeten dezelfde rowtype hebben !!!
  -- stoftest binnen dezelfde meting
  CURSOR meet_cur
  IS
    SELECT meet.meetwaarde
    ,      meet.tonen_als
    ,      meet.meet_id
    FROM   bas_meetwaarden meet
    ,      bas_meetwaarde_statussen mest
    WHERE  meet.meti_id = p_meti_id
    AND    meet.stof_id = p_stof_id
    AND    meet.mest_id = mest.mest_id (+)
    AND  ( mest.in_uitslag = 'J' OR meet.mest_id IS NULL )
    ORDER BY DECODE( nvl(meet.tonen_als, meet.meetwaarde)
                   , NULL, 9
                   , 1
                   )
    ,        meet.meet_id DESC -- laatste eerst
    ;

  -- stoftest binnen dezelfde fractie (meerdere metingen)
  CURSOR meti_cur
  IS
    SELECT meet.meetwaarde
    ,      meet.tonen_als
    ,      meet.meet_id
    FROM   bas_meetwaarden meet
    ,      bas_metingen meti
    ,      bas_meetwaarde_statussen mest
    WHERE  meti.meti_id = meet.meti_id
    AND    meti.frac_id = p_frac_id
    AND    meet.stof_id = p_stof_id
    AND    meet.mest_id = mest.mest_id (+)
    AND  ( mest.in_uitslag = 'J' OR meet.mest_id IS NULL )
    ORDER BY DECODE( nvl(meet.tonen_als, meet.meetwaarde)
                   , NULL, 9
                   , 1
                   )
    ,        meet.meet_id DESC -- laatste eerst
    ;
  -- stoftest bij een fractietype binnen een hetzelfde monster (meerdere metingen)
  CURSOR frac_cur
  IS
    SELECT meet.meetwaarde
    ,      meet.tonen_als
    ,      meet.meet_id
    FROM   bas_meetwaarden meet
    ,      bas_metingen meti
    ,      bas_fracties frac
    ,      bas_meetwaarde_statussen mest
    WHERE  meti.meti_id = meet.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    meti.onmo_id = p_onmo_id
    AND    frac.frty_id = p_frty_id
    AND    meet.stof_id = p_stof_id
    AND    meet.mest_id = mest.mest_id (+)
    AND  ( mest.in_uitslag = 'J' OR meet.mest_id IS NULL )
    ORDER BY DECODE( nvl(meet.tonen_als, meet.meetwaarde)
                   , NULL, 9
                   , 1
                   )
    ,        meet.meet_id DESC -- laatste eerst
    ;
  meet_rec  meti_cur%rowtype;
BEGIN
  IF ( p_onmo_id IS NOT NULL
   AND p_frty_id IS NOT NULL
     )
  THEN
    OPEN  frac_cur;
    FETCH frac_cur
    INTO  meet_rec;
    CLOSE frac_cur;
  ELSE
    IF ( p_meti_id IS NOT NULL )
    THEN
      OPEN  meet_cur;
      FETCH meet_cur
      INTO  meet_rec;
      CLOSE meet_cur;
    END IF;

    v_waarde := meet_rec.meetwaarde; -- kgc_reken.rekenwaarde( p_meetwaarde => meet_rec.meetwaarde );
    IF ( v_waarde IS NULL )
    THEN
      OPEN  meti_cur;
      FETCH meti_cur
      INTO  meet_rec;
      CLOSE meti_cur;
    END IF;
  END IF;

  if ( meet_rec.meetwaarde <> meet_rec.tonen_als )
  then -- geen meetwaarde om mee te rekenen!
    v_waarde := to_number(null);
  else
    v_waarde := kgc_reken.rekenwaarde( p_meetwaarde => meet_rec.meetwaarde );
  end if;
  RETURN( v_waarde );

END meetwaarde;

-- bepaal normaalwaarden bij een berekening
PROCEDURE bepaal_norm
( p_meti_id    IN     NUMBER
, p_meet_id    IN     NUMBER
, p_stof_id    IN     NUMBER
, p_bety_id    IN     NUMBER
, x_norm_laag  IN OUT VARCHAR2
, x_norm_hoog  IN OUT VARCHAR2
, x_norm_gem   IN OUT VARCHAR2
)
IS
BEGIN
  x_norm_laag := bas_meet_00.normaalwaarde_onder
                 ( p_meti_id => p_meti_id
                 , p_stof_id => p_stof_id
                 , p_bety_id => p_bety_id
                 );
  x_norm_hoog := bas_meet_00.normaalwaarde_boven
                 ( p_meti_id => p_meti_id
                 , p_stof_id => p_stof_id
                 , p_bety_id => p_bety_id
                 );
  x_norm_gem  := bas_meet_00.normaalwaarde_gemiddeld
                 ( p_meti_id => p_meti_id
                 , p_stof_id => p_stof_id
                 , p_bety_id => p_bety_id
                 );
END bepaal_norm;

-- voer een formule uit
PROCEDURE bereken_formule
( p_formule_tekst    IN  VARCHAR2
, p_form_id          IN  NUMBER
, p_meet_id          IN  NUMBER
, p_meti_id          IN  NUMBER
, p_eenheid          IN  VARCHAR2
, p_bepaal_tonen_als IN  VARCHAR2
, p_meetwaarde       IN  NUMBER
, x_waarde           OUT VARCHAR2
, x_eenheid          OUT VARCHAR2
)
IS
  CURSOR fopa_cur
  ( b_parameter IN VARCHAR2
  )
  IS
    SELECT stof_id
    ,      frty_id
    ,      functie
    ,      constante
    FROM   bas_formule_parameters
    WHERE  form_id = p_form_id
    AND    parameter = b_parameter
    ;
  fopa_rec    fopa_cur%ROWTYPE;
  CURSOR meet_cur
  IS
    SELECT meet.meti_id
    ,      meet.meeteenheid
    FROM   bas_meetwaarden meet
    WHERE  meet.meet_id = p_meet_id
    ;
  meet_rec  meet_cur%ROWTYPE;
  CURSOR meti_cur
  ( b_meti_id  IN  NUMBER
  )
  IS
    SELECT meti.frac_id
    ,      meti.onmo_id
    FROM   bas_metingen meti
    WHERE  meti.meti_id = b_meti_id
    ;
  meti_rec     meti_cur%ROWTYPE;
  CURSOR form_cur
  IS
    SELECT aantal_decimalen
    FROM   bas_formules
    WHERE  form_id = p_form_id
    ;
  form_rec form_cur%ROWTYPE;
  v_parameter      VARCHAR2(30);
  v_par_waarde     NUMBER(38,10);
  v_statement      VARCHAR2(4000); -- wordt via dynamisch sql uitgevoerd
  v_waarde         NUMBER(38,10);
  v_eenheid        VARCHAR2(30);
  v_error          VARCHAR2(4000);
  v_formule_tekst  VARCHAR2(4000);

  PROCEDURE add_line
  ( p_line IN VARCHAR2
  , p_text IN OUT VARCHAR2
  )
  IS
  BEGIN
    IF ( p_line IS NOT NULL )
    THEN
      IF ( p_text IS NULL )
      THEN
        p_text := p_line;
      ELSE
        p_text := p_text||CHR(10)||p_line;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END add_line;

BEGIN
  IF ( p_formule_tekst IS NULL )
  THEN
    RETURN;
  END IF;

  v_formule_tekst := p_formule_tekst;

  IF p_meti_id IS NOT NULL
  THEN
    meet_rec.meti_id := p_meti_id;
  ELSIF p_meet_id IS NOT NULL
  THEN
    OPEN  meet_cur;
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE meet_cur;
  END IF;

  OPEN  meti_cur( b_meti_id => meet_rec.meti_id );
  FETCH meti_cur
  INTO  meti_rec;
  CLOSE meti_cur;

  -- substitueer <PARAMETER> met een waarde
  -- 1. meetwaarde van een stoftest (evt. binnen een bepaalde fractie)
  -- 2. returnwaarde van de functie
  -- 3. constante
  WHILE ( INSTR( v_formule_tekst, '<' ) > 0
      AND INSTR( v_formule_tekst, '>' ) > 0
      )
  LOOP
    v_parameter := '<'
                || kgc_util_00.strip
                   ( p_string => v_formule_tekst
                   , p_tag_va => '<'
                   , p_tag_tm => '>'
                   , p_rest => 'N'
                   )
                || '>'
                 ;
    OPEN  fopa_cur( v_parameter );
    FETCH fopa_cur
    INTO  fopa_rec;
    CLOSE fopa_cur;
    v_par_waarde := NULL;
    IF ( v_par_waarde IS NULL
     AND fopa_rec.stof_id IS NOT NULL
       )
    THEN
      v_par_waarde := meetwaarde
                      ( p_meti_id          => meet_rec.meti_id
                      , p_frac_id          => meti_rec.frac_id
                      , p_onmo_id          => meti_rec.onmo_id
                      , p_frty_id          => fopa_rec.frty_id
                      , p_stof_id          => fopa_rec.stof_id
                      );
    END IF;
    IF ( v_par_waarde IS NULL
     AND fopa_rec.functie IS NOT NULL
       )
    THEN
      IF ( SUBSTR( fopa_rec.functie, 1, 1 ) = '#' )
      THEN -- kolomnaam van bas_meetwaarden in functieveld
        v_statement := 'select '||SUBSTR( fopa_rec.functie, 2 )
                    || ' from bas_meetwaarden'
                    || ' where meet_id = '||TO_CHAR(p_meet_id)
                     ;
      ELSE
        v_statement := 'begin'
           ||CHR(10)|| '  :result := '||fopa_rec.functie||'(p_meet_id => '||TO_CHAR(p_meet_id)||');'
           ||CHR(10)|| 'end;'
                     ;
      END IF;
      BEGIN

        if p_bepaal_tonen_als = 'J'
        then
          v_par_waarde := p_meetwaarde;
        else
          v_par_waarde := kgc_util_00.dyn_exec( v_statement );
        end if;
      EXCEPTION
        WHEN OTHERS
        THEN
          add_line('Functie / kolom '||fopa_rec.functie||' is niet bekend.',v_error);
          add_line(SQLERRM,v_error);
      END;
    END IF;
    IF ( v_par_waarde IS NULL
     AND fopa_rec.constante IS NOT NULL
       )
    THEN
      v_par_waarde := fopa_rec.constante;
    END IF;
    --
    IF ( v_par_waarde IS NULL )
    THEN
      v_formule_tekst := NULL; -- berekening kan (nog) niet uitgevoerd worden
      add_line('Waarde voor '||v_parameter||' kan (nog) niet worden berekend.',v_error);
    ELSE
      v_formule_tekst := REPLACE( v_formule_tekst
                                , v_parameter
                                , TO_CHAR( v_par_waarde )
                                );
    END IF;
  END LOOP;
  IF ( v_formule_tekst IS NOT NULL )
  THEN
    v_statement := 'select '||v_formule_tekst||' from dual';
    BEGIN
      v_waarde := kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        add_line('Fout bij uitrekenen formule.',v_error);
        add_line(SQLERRM,v_error);
    END;
  END IF;

  IF ( v_waarde IS NOT NULL )
  THEN
    OPEN  form_cur;
    FETCH form_cur
    INTO  form_rec;
    CLOSE form_cur;
    IF ( form_rec.aantal_decimalen IS NOT NULL )
    THEN
      x_waarde := kgc_formaat_00.nummer( v_waarde, form_rec.aantal_decimalen );
    ELSE
      x_waarde := bas_meet_00.formatteer
                  ( p_meet_id => p_meet_id
                  , p_waarde  => v_waarde
                  );
    END IF;
  ELSE
    x_waarde := NULL;
  END IF;

  x_eenheid := NVL( p_eenheid, meet_rec.meeteenheid) ;

END bereken_formule;

PROCEDURE bereken
( p_meet_id IN NUMBER
, p_meti_id IN NUMBER
, p_form_id IN NUMBER
, x_waarde OUT NUMBER
, x_eenheid IN OUT VARCHAR2
)
IS
  CURSOR form_cur
  IS
    SELECT frmu.formule_tekst
    FROM   bas_formules   frmu
    WHERE  frmu.form_id   = p_form_id
    AND    frmu.vervallen = 'N'
    ;
  form_rec form_cur%rowtype;
BEGIN
  -- formule gegevens
  OPEN  form_cur;
  FETCH form_cur
  INTO  form_rec;
  CLOSE form_cur;

  bereken_formule
  ( p_formule_tekst    => form_rec.formule_tekst
  , p_form_id          => p_form_id
  , p_meet_id          => p_meet_id
  , p_meti_id          => p_meti_id
  , p_eenheid          => x_eenheid
  , p_bepaal_tonen_als => null
  , p_meetwaarde       => null
  , x_waarde           => x_waarde
  , x_eenheid          => x_eenheid
  );

END bereken;

PROCEDURE berekening_direct
( p_meet_id IN NUMBER
, p_form_id IN NUMBER
, x_waarde OUT NUMBER
, x_eenheid OUT VARCHAR2
)
IS
  CURSOR meet_cur
  IS
    SELECT stof_id
    ,      meti_id
    FROM   bas_meetwaarden
    WHERE  meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;
  CURSOR bety_cur
  ( p_stof_id IN NUMBER )
  IS
    SELECT bety.bety_id
    ,      bewy.eenheid
    FROM   bas_berekening_types bety
    ,      bas_berekenings_wijzen bewy
    WHERE  bewy.bety_id = bety.bety_id
    AND    bewy.form_id   = p_form_id
    AND    bewy.stof_id = p_stof_id
    AND    bety.direct = 'J'
    AND    bety.vervallen = 'N'
    ;
  bety_rec bety_cur%rowtype;
  v_waarde NUMBER(38,10);
  v_eenheid VARCHAR2(30);
BEGIN
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  meet_rec;
  CLOSE meet_cur;
  --
  OPEN  bety_cur( meet_rec.stof_id );
  FETCH bety_cur
  INTO  bety_rec;
  CLOSE bety_cur;
  IF ( bety_rec.bety_id IS NOT NULL )
  THEN
    x_eenheid := bety_rec.eenheid;
    bereken
    ( p_meet_id => p_meet_id
    , p_meti_id => NULL
    , p_form_id => p_form_id
    , x_waarde => x_waarde
    , x_eenheid => x_eenheid
    );
  END IF;
END berekening_direct;

-- zet waarde in berekening of maak berekening aan
PROCEDURE ins_upd_berekening
( p_meet_id  IN  bas_berekeningen.meet_id%TYPE
, p_meti_id  IN  bas_berekeningen.meti_id%TYPE
, p_bewy_id  IN  bas_berekeningen.bety_id%TYPE
, p_uitslag  IN  bas_berekeningen.uitslag%TYPE
, p_uitslageenheid  IN  bas_berekeningen.uitslageenheid%TYPE
, p_normaalwaarde_ondergrens  IN  bas_berekeningen.normaalwaarde_ondergrens%TYPE
, p_normaalwaarde_bovengrens  IN  bas_berekeningen.normaalwaarde_bovengrens%TYPE
, p_normaalwaarde_gemiddelde  IN  bas_berekeningen.normaalwaarde_gemiddelde%TYPE
)
IS
BEGIN
  IF ( p_meet_id IS NULL
   AND p_meti_id IS NULL
     )
  THEN
    RETURN;
  END IF;
  IF ( p_meet_id IS NOT NULL )
  THEN
    UPDATE bas_berekeningen
    SET    uitslag                  = p_uitslag
    ,      uitslageenheid           = p_uitslageenheid
    ,      normaalwaarde_ondergrens = p_normaalwaarde_ondergrens
    ,      normaalwaarde_bovengrens = p_normaalwaarde_bovengrens
    ,      normaalwaarde_gemiddelde = p_normaalwaarde_gemiddelde
    WHERE  bewy_id = p_bewy_id
    AND    meet_id = p_meet_id
    ;
  ELSIF ( p_meti_id IS NOT NULL )
  THEN
    UPDATE bas_berekeningen
    SET    uitslag                  = p_uitslag
    ,      uitslageenheid           = p_uitslageenheid
    ,      normaalwaarde_ondergrens = p_normaalwaarde_ondergrens
    ,      normaalwaarde_bovengrens = p_normaalwaarde_bovengrens
    ,      normaalwaarde_gemiddelde = p_normaalwaarde_gemiddelde
    WHERE  bewy_id = p_bewy_id
    AND    meti_id = p_meti_id
    ;
  END IF;
  IF ( sql%rowcount = 0 )
  THEN
    INSERT INTO bas_berekeningen
    ( meet_id
    , meti_id
    , bewy_id
    , uitslag
    , uitslageenheid
    , normaalwaarde_ondergrens
    , normaalwaarde_bovengrens
    , normaalwaarde_gemiddelde
    )
    VALUES
    ( p_meet_id
    , DECODE( p_meet_id
            , NULL, p_meti_id
            , NULL
            )
    , p_bewy_id
    , p_uitslag
    , p_uitslageenheid
    , p_normaalwaarde_ondergrens
    , p_normaalwaarde_bovengrens
    , p_normaalwaarde_gemiddelde
    );
  END IF;
END ins_upd_berekening;

-- =====================================================================
--  P U B L I E K
--
PROCEDURE berekening
( p_meet_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_bety_id IN NUMBER := NULL
, p_bewy_id IN NUMBER := NULL
, p_form_id IN NUMBER := NULL
, p_negeer_afgerond in varchar2 := 'N'
, p_commit IN BOOLEAN := TRUE
)
IS
  CURSOR meet_cur
  ( b_meet_id IN NUMBER := NULL
--  , b_meti_id IN NUMBER := NULL
  )
  IS
    SELECT meet.meet_id
    ,      meet.meti_id
    ,      meti.stgr_id
    ,      meet.stof_id
    ,      meet.meeteenheid
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND   ( ( meti.afgerond = 'N'
          AND meet.afgerond = 'N'
            )
         OR p_negeer_afgerond = 'J'
          )
    AND    meet.meet_id = b_meet_id -- NVL( b_meet_id, meet.meet_id )
--    AND    meti.meti_id = NVL( b_meti_id, meti.meti_id )
    ;
  meet_rec meet_cur%rowtype;

  CURSOR meti_cur  -- zelfde rowtype als hierboven
  ( b_meti_id IN NUMBER := NULL
  )
  IS
    SELECT TO_NUMBER(NULL) meet_id
    ,      meti.meti_id
    ,      meti.stgr_id
    ,      TO_NUMBER(NULL) stof_id
    ,      NULL meeteenheid
    FROM   bas_metingen meti
    WHERE ( meti.afgerond = 'N'
         OR p_negeer_afgerond = 'J'
          )
    AND    meti.meti_id = b_meti_id
    ;

  CURSOR bety_cur
  ( b_stof_id IN NUMBER
  , b_stgr_id IN NUMBER
  )
  IS
    -- berekeningen voor een stoftest, al dan niet per protocol
    SELECT bewy.bewy_id
    ,      bewy.form_id
    ,      bewy.bety_id
    ,      bety.direct
    ,      bewy.stgr_id
    ,      bewy.alle_stoftesten
    ,      bewy.stof_id
    ,      bewy.eenheid
    FROM   bas_berekening_types   bety
    ,      bas_berekenings_wijzen bewy
    WHERE  bewy.bety_id = bety.bety_id
    AND  ( bewy.stof_id = b_stof_id OR bewy.stof_id IS NULL )
    AND  ( bewy.stgr_id = b_stgr_id OR bewy.stgr_id IS NULL )
    AND    bety.bety_id = NVL( p_bety_id, bety.bety_id )
    AND    bewy.bewy_id = NVL( p_bewy_id, bewy.bewy_id )
    AND    bewy.vervallen = 'N'
    AND    bety.vervallen = 'N'
    ;

  v_waarde    NUMBER(38,10);
  v_eenheid   VARCHAR2(30);
  v_norm_laag bas_berekeningen.normaalwaarde_ondergrens%TYPE;
  v_norm_hoog bas_berekeningen.normaalwaarde_bovengrens%TYPE;
  v_norm_gem  bas_berekeningen.normaalwaarde_gemiddelde%TYPE;
  v_mede_id   NUMBER := kgc_mede_00.medewerker_id;

BEGIN
  IF g_in_progress
  THEN
    -- beveiliging tegen het rondzingen is niet meer nodig?
null; --     RETURN;
  END IF;
  g_in_progress := TRUE;
  IF ( p_meet_id IS NOT NULL )
  THEN
    OPEN  meet_cur( b_meet_id => p_meet_id );
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE meet_cur;
  ELSIF ( p_meti_id IS NOT NULL )
  THEN
    OPEN  meti_cur( b_meti_id => p_meti_id );
    FETCH meti_cur
    INTO  meet_rec;
    CLOSE meti_cur;
  END IF;
  FOR bety_rec IN bety_cur( b_stof_id => meet_rec.stof_id
                          , b_stgr_id => meet_rec.stgr_id
                          )
  LOOP
    v_eenheid := NVL( bety_rec.eenheid, meet_rec.meeteenheid );
    v_waarde  := NULL;
    IF ( bety_rec.direct = 'J' )
    THEN
      berekening_direct( p_meet_id => p_meet_id
                       , p_form_id => bety_rec.form_id
                       , x_waarde => v_waarde
                       , x_eenheid => v_eenheid
                       );
      BEGIN
        UPDATE bas_meetwaarden
        SET    meetwaarde = v_waarde
        ,      meeteenheid = v_eenheid
        ,      datum_meting = trunc(SYSDATE)
        ,      mede_id = NVL( v_mede_id, mede_id )
        WHERE  meet_id = p_meet_id
        ;
      END;
    ELSE  -- niet-direct
      IF ( p_meet_id IS NULL )
      THEN
        IF ( bety_rec.stgr_id IS NOT NULL
         AND bety_rec.alle_stoftesten = 'N'
           )
        THEN
        -- berekeningen op meti-niveau
        bereken
        ( p_meet_id => NULL
        , p_meti_id => p_meti_id
        , p_form_id => bety_rec.form_id
        , x_waarde  => v_waarde
        , x_eenheid => v_eenheid
        );
        bepaal_norm
        ( p_meti_id   => p_meti_id
        , p_meet_id   => NULL
        , p_stof_id   => NULL
        , p_bety_id   => bety_rec.bety_id
        , x_norm_laag => v_norm_laag
        , x_norm_hoog => v_norm_hoog
        , x_norm_gem  => v_norm_gem
        );
        ins_upd_berekening
        ( p_meet_id                  => NULL
        , p_meti_id                  => p_meti_id
        , p_bewy_id                  => bety_rec.bewy_id
        , p_uitslag                  => v_waarde
        , p_uitslageenheid           => v_eenheid
        , p_normaalwaarde_ondergrens => v_norm_laag
        , p_normaalwaarde_bovengrens => v_norm_hoog
        , p_normaalwaarde_gemiddelde => v_norm_gem
        );
        END IF;
      ELSE
        IF ( bety_rec.stof_id IS NOT NULL
          OR bety_rec.alle_stoftesten = 'J'
           )
        THEN
        bereken
        ( p_meet_id => p_meet_id
        , p_meti_id => NULL
        , p_form_id => bety_rec.form_id
        , x_waarde  => v_waarde
        , x_eenheid => v_eenheid
        );
        bepaal_norm
        ( p_meti_id   => meet_rec.meti_id
        , p_meet_id   => p_meet_id
        , p_stof_id   => meet_rec.stof_id
        , p_bety_id   => bety_rec.bety_id
        , x_norm_laag => v_norm_laag
        , x_norm_hoog => v_norm_hoog
        , x_norm_gem  => v_norm_gem
        );
        ins_upd_berekening
        ( p_meet_id                  => p_meet_id
        , p_meti_id                  => NULL
        , p_bewy_id                  => bety_rec.bewy_id
        , p_uitslag                  => v_waarde
        , p_uitslageenheid           => v_eenheid
        , p_normaalwaarde_ondergrens => v_norm_laag
        , p_normaalwaarde_bovengrens => v_norm_hoog
        , p_normaalwaarde_gemiddelde => v_norm_gem
        );
        END IF;
      END IF;
    END IF;
  END LOOP;
  IF ( p_commit )
  THEN
    COMMIT;
  END IF;
  g_in_progress := FALSE;

EXCEPTION
  WHEN OTHERS
  THEN
    g_in_progress := FALSE;
    RAISE;
END berekening;

FUNCTION normaalwaarden
( p_bere_id IN NUMBER
, p_aantal_decemalen IN NUMBER := NULL -- Added by Athar shaikh for Mantis-3195 on 31-03-2016
)
RETURN    VARCHAR2
IS
  v_return VARCHAR2(100);
  -- Start added by Athar Shaikh for Mantis-3195 on 31-03-2016
  v_normaalwaarde_ondergrens varchar2(100);
  v_normaalwaarde_bovengrens varchar2(100);
  -- End added by Athar Shaikh for Mantis-3195 on 31-03-2016
  CURSOR bere_cur
  IS
    SELECT normaalwaarde_ondergrens
    ,      normaalwaarde_bovengrens
    ,      normaalwaarde_gemiddelde
    FROM   bas_berekeningen
    WHERE  bere_id = p_bere_id
    ;
  bere_rec bere_cur%rowtype;
BEGIN
  OPEN  bere_cur;
  FETCH bere_cur
  INTO  bere_rec;
  CLOSE bere_cur;
  -- Start added by Athar Shaikh for Mantis-3195 on 31-03-2016
  IF (p_aantal_decemalen) IS NULL OR (p_aantal_decemalen) = 0
  THEN
    v_normaalwaarde_ondergrens   := bere_rec.normaalwaarde_ondergrens;
    v_normaalwaarde_bovengrens   := bere_rec.normaalwaarde_bovengrens;
  ELSE
    v_normaalwaarde_ondergrens   := trim(to_char(bere_rec.normaalwaarde_ondergrens, (('9999999990.'|| LTRIM((Power(10, p_aantal_decemalen)),1)) )));
    v_normaalwaarde_bovengrens   := trim(to_char(bere_rec.normaalwaarde_bovengrens, (('9999999990.'|| LTRIM((Power(10, p_aantal_decemalen)),1)) )));
  END IF;
  -- End added by Athar Shaikh for Mantis-3195 on 31-03-2016
  v_return := kgc_formaat_00.normaalwaarden
              ( p_onder => v_normaalwaarde_ondergrens -- bere_rec.normaalwaarde_ondergrens commented and added v_normaalwaarde_ondergrens by Athar Shaikh for Mantis-3195 on 31-03-2016
              , p_boven => v_normaalwaarde_bovengrens -- bere_rec.normaalwaarde_bovengrens commented and added v_normaalwaarde_bovengrens by Athar Shaikh for Mantis-3195 on 31-03-2016
              , p_gemiddeld => bere_rec.normaalwaarde_gemiddelde
              );
  RETURN( v_return );
END normaalwaarden;

FUNCTION eigen_meetwaarde
( p_meet_id IN NUMBER
)
RETURN   NUMBER
IS
  v_return NUMBER;
  CURSOR meet_cur
  IS
    SELECT meet.meti_id
    ,      meet.stof_id
    ,      meti.frac_id
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meet.meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;
BEGIN
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  meet_rec;
  CLOSE meet_cur;
  v_return := meetwaarde
              ( p_meti_id => meet_rec.meti_id
              , p_frac_id => meet_rec.frac_id
              , p_onmo_id => NULL
              , p_frty_id => NULL
              , p_stof_id => meet_rec.stof_id
              );
  RETURN( v_return );
END eigen_meetwaarde;

PROCEDURE gewijzigde_meetwaarde
( p_meet_id  IN  bas_meetwaarden.meet_id%TYPE
)
IS
  -- blijf binnen de context van een onderzoek-monster
  CURSOR meti_cur
  IS
    SELECT meti.onmo_id
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meet.meet_id = p_meet_id
    ;
  v_onmo_id NUMBER;
  -- formules uit "tonen als" - tabel:
  CURSOR tmwa_cur
  ( b_meet_id in number
  , b_onmo_id IN NUMBER
  )
  IS
    SELECT meet.meet_id
    ,      meet.meetwaarde
    FROM   kgc_toon_meetwaarde_als tmwa
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meet.stof_id = tmwa.stof_id
    AND    tmwa.vervallen = 'N'
    AND    tmwa.form_id IN
           ( SELECT frmu.form_id
             FROM   bas_meetwaarden meet
             ,      bas_formules frmu
             ,      bas_formule_parameters fopa
             WHERE  frmu.form_id = fopa.form_id
             AND    frmu.vervallen = 'N'
             AND    fopa.stof_id = meet.stof_id
             AND    meet.meet_id = b_meet_id
             UNION
             SELECT bewy.form_id
             FROM   bas_formules frmu
             ,      bas_formule_parameters fopa
             ,      bas_berekenings_wijzen bewy
             ,      bas_berekeningen bere
             WHERE  bere.bewy_id = bewy.bewy_id
             AND    bewy.vervallen = 'N'
             AND    bewy.form_id = frmu.form_id
             AND    frmu.form_id = fopa.form_id
             AND    frmu.vervallen = 'N'
             AND    fopa.functie IN ( 'BAS_BERE_00.EIGEN_MEETWAARDE', '#MEETWAARDE' )
             AND    bere.meet_id = b_meet_id
           )
    AND    meti.onmo_id = b_onmo_id
    ;
  -- formules uit berekeningswijzen:
  CURSOR bere_cur
  ( b_meet_id in number
  , b_onmo_id IN NUMBER
  )
  IS -- berekeningen op stoftest-nivo
    SELECT bere.meet_id  -- is veel sneller dan COST-base
    ,      bere.meti_id
    ,      bere.bety_id
    ,      bere.bewy_id
    ,      bewy.form_id
    FROM   bas_berekenings_wijzen bewy
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      bas_berekeningen bere
    WHERE  bere.bewy_id = bewy.bewy_id
    AND    bere.meet_id = meet.meet_id
    AND    meet.meti_id = meti.meti_id
    AND    bewy.vervallen = 'N'
    AND    meti.onmo_id = b_onmo_id
    AND    bewy.form_id IN
           ( SELECT frmu2.form_id
             FROM   bas_meetwaarden meet2
             ,      bas_formules frmu2
             ,      bas_formule_parameters fopa2
             WHERE  frmu2.form_id = fopa2.form_id
             AND    frmu2.vervallen = 'N'
             AND    fopa2.stof_id = meet2.stof_id
             AND    meet2.meet_id = b_meet_id
             UNION
             SELECT bewy2.form_id
             FROM   bas_formules frmu2
             ,      bas_formule_parameters fopa2
             ,      bas_berekenings_wijzen bewy2
             ,      bas_berekeningen bere2
             WHERE  bere2.bewy_id = bewy2.bewy_id
             AND    bewy2.vervallen = 'N'
             AND    bewy2.form_id = frmu2.form_id
             AND    frmu2.form_id = fopa2.form_id
             AND    frmu2.vervallen = 'N'
             AND    fopa2.functie IN ( 'BAS_BERE_00.EIGEN_MEETWAARDE', '#MEETWAARDE' )
			 and    bere2.meet_id = bere.meet_id
             AND    bere2.meet_id = b_meet_id
           )
    UNION  -- berekeningen op protocol-nivo
    SELECT TO_NUMBER(NULL) meet_id
    ,      bere.meti_id
    ,      bere.bety_id
    ,      bere.bewy_id
    ,      bewy.form_id
    FROM   bas_berekenings_wijzen bewy
    ,      bas_metingen meti
    ,      bas_berekeningen bere
    WHERE  bere.bewy_id = bewy.bewy_id
    AND    bere.meti_id = meti.meti_id
    AND    bewy.vervallen = 'N'
    AND    meti.onmo_id = b_onmo_id
    AND    bewy.form_id IN
           ( SELECT frmu2.form_id
             FROM   bas_meetwaarden meet2
             ,      bas_formules frmu2
             ,      bas_formule_parameters fopa2
             WHERE  frmu2.form_id = fopa2.form_id
             AND    frmu2.vervallen = 'N'
             AND    fopa2.stof_id = meet2.stof_id
             AND    meet2.meet_id = b_meet_id
             UNION
             SELECT bewy2.form_id
             FROM   bas_formules frmu2
             ,      bas_formule_parameters fopa2
             ,      bas_berekenings_wijzen bewy2
             ,      bas_berekeningen bere2
             WHERE  bere2.bewy_id = bewy2.bewy_id
             AND    bewy2.vervallen = 'N'
             AND    bewy2.form_id = frmu2.form_id
             AND    frmu2.form_id = fopa2.form_id
             AND    frmu2.vervallen = 'N'
             AND    fopa2.functie IN ( 'BAS_BERE_00.EIGEN_MEETWAARDE', '#MEETWAARDE' )
             AND    bere2.meet_id = b_meet_id
           )
    union -- directe berekeningen
    SELECT meet.meet_id
    ,      to_number(null) -- meet.meti_id ???
    ,      bewy.bety_id
    ,      bewy.bewy_id
    ,      bewy.form_id
    FROM   bas_berekening_types bety
    ,      bas_berekenings_wijzen bewy
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  bewy.bety_id = bety.bety_id
    and    bety.direct = 'J'
    and    bewy.stof_id = meet.stof_id
    and    meet.meti_id = meti.meti_id
    and    meet.meet_reken = 'R'
    AND    bewy.vervallen = 'N'
    AND    meti.onmo_id = b_onmo_id
    AND    bewy.form_id IN
           ( SELECT frmu2.form_id
             FROM   bas_meetwaarden meet2
             ,      bas_formules frmu2
             ,      bas_formule_parameters fopa2
             WHERE  frmu2.form_id = fopa2.form_id
             AND    frmu2.vervallen = 'N'
             AND    fopa2.stof_id = meet2.stof_id
             AND    meet2.meet_id = b_meet_id
             UNION
             SELECT bewy2.form_id
             FROM   bas_formules frmu2
             ,      bas_formule_parameters fopa2
             ,      bas_berekenings_wijzen bewy2
             ,      bas_berekeningen bere2
             WHERE  bere2.bewy_id = bewy2.bewy_id
             AND    bewy2.vervallen = 'N'
             AND    bewy2.form_id = frmu2.form_id
             AND    frmu2.form_id = fopa2.form_id
             AND    frmu2.vervallen = 'N'
             AND    fopa2.functie IN ( 'BAS_BERE_00.EIGEN_MEETWAARDE', '#MEETWAARDE' )
             AND    bere2.meet_id = b_meet_id
           )
    ;
BEGIN
  OPEN  meti_cur;
  FETCH meti_cur
  INTO  v_onmo_id;
  CLOSE meti_cur;

  FOR r IN tmwa_cur( p_meet_id, v_onmo_id )
  LOOP
    bas_meet_00.bepaal_tonen_als
    ( p_meet_id => r.meet_id
    , p_meetwaarde => r.meetwaarde
    );
  END LOOP;
  FOR r IN bere_cur( p_meet_id, v_onmo_id )
  LOOP
    berekening
    ( p_meet_id => r.meet_id
    , p_meti_id => r.meti_id
    , p_bety_id => r.bety_id
    , p_bewy_id => r.bewy_id
    , p_form_id => r.form_id
    , p_negeer_afgerond => 'J' -- soms worden meetwaarden ingevoerd en direct afgerond!!!
    , p_commit => FALSE
    );
  END LOOP;
END gewijzigde_meetwaarde;

PROCEDURE herbereken_formule
( p_form_id IN NUMBER
, x_fout IN OUT BOOLEAN
, x_melding IN OUT VARCHAR2
)
IS
  v_aantal NUMBER := 0;
  CURSOR tmwa_cur
  IS
    SELECT meet.meet_id
    ,      meet.meetwaarde
    FROM   kgc_toon_meetwaarde_als tmwa
    ,      kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meet.stof_id = tmwa.stof_id
    AND    tmwa.vervallen = 'N'
    AND    meet.afgerond = 'N'
    AND    meti.afgerond = 'N'
    AND    onde.afgerond = 'N'
    AND    tmwa.form_id = p_form_id
    ;
  CURSOR bewy_cur
  IS
    -- berekeningen op meetwaarden
    SELECT bewy.bety_id
    ,      bere.meet_id
    ,      meet.meti_id
    ,      bere.bewy_id
    ,      bewy.form_id
    FROM   kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      bas_berekeningen bere
    ,      bas_berekenings_wijzen bewy
    WHERE  bewy.bewy_id = bere.bewy_id
    AND    bere.meet_id = meet.meet_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meet.afgerond = 'N'
    AND    meti.afgerond = 'N'
    AND    onde.afgerond = 'N'
    AND    bewy.form_id = p_form_id
    UNION
    -- berekeningen op metingen
    SELECT bewy.bety_id
    ,      bere.meet_id
    ,      bere.meti_id
    ,      bere.bewy_id
    ,      bewy.form_id
    FROM   kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_berekeningen bere
    ,      bas_berekenings_wijzen bewy
    WHERE  bewy.bewy_id = bere.bewy_id
    AND    bewy.stof_id IS NULL
    AND    bewy.alle_stoftesten = 'N'
    AND    bere.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meti.afgerond = 'N'
    AND    onde.afgerond = 'N'
    AND    bewy.form_id = p_form_id
    UNION
    -- berekeningen op alle stoftesten bij een metingen
    SELECT bewy.bety_id
    ,      meet.meet_id
    ,      meet.meti_id
    ,      bere.bewy_id
    ,      bewy.form_id
    FROM   kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      bas_berekeningen bere
    ,      bas_berekenings_wijzen bewy
    WHERE  bewy.bewy_id = bere.bewy_id
    AND    bere.meti_id = meti.meti_id
    AND    bewy.alle_stoftesten = 'J'
    AND    meti.meti_id = meet.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meet.afgerond = 'N'
    AND    meti.afgerond = 'N'
    AND    onde.afgerond = 'N'
    AND    bewy.form_id = p_form_id
    UNION
    -- directe berekeningen
    SELECT bewy.bety_id
    ,      meet.meet_id
    ,      meet.meti_id
    ,      bewy.bewy_id
    ,      bewy.form_id
    FROM   kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      bas_berekening_types bety
    ,      bas_berekenings_wijzen bewy
    WHERE  bewy.bety_id = bety.bety_id
    AND    bety.direct = 'J'
    AND    bewy.stof_id = meet.stof_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meet.meet_reken = 'R'
    AND    meet.afgerond = 'N'
    AND    meti.afgerond = 'N'
    AND    onde.afgerond = 'N'
    AND    bewy.form_id = p_form_id
    ;
BEGIN
  FOR r IN tmwa_cur
  LOOP
    bas_meet_00.bepaal_tonen_als
    ( p_meet_id => r.meet_id
    , p_meetwaarde => r.meetwaarde
    );
    v_aantal := v_aantal + 1;
  END LOOP;

  FOR r IN bewy_cur
  LOOP
    berekening
    ( p_meet_id => r.meet_id
    , p_meti_id => r.meti_id
    , p_bety_id => r.bety_id
    , p_bewy_id => r.bewy_id
    , p_form_id => r.form_id
    , p_commit => TRUE
    );
    v_aantal := v_aantal + 1;
  END LOOP;
  x_fout    := FALSE;
  x_melding := v_aantal||' berekening(en) opnieuw uitgevoerd';
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout    := TRUE;
    x_melding := SUBSTR( SQLERRM, 1, 200 );
END herbereken_formule;

FUNCTION in_uitslag
( p_bety_id IN NUMBER
, p_stof_id IN NUMBER
, p_mate_id IN NUMBER
, p_frty_id IN NUMBER
, p_stgr_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';

  CURSOR c
  IS
    SELECT 'J'
    FROM   kgc_stand_berekeningstypes
    WHERE  bety_id = p_bety_id
    AND  ( stof_id = p_stof_id OR stof_id IS NULL OR p_stof_id IS NULL )
    AND  ( mate_id = p_mate_id OR mate_id IS NULL OR p_mate_id IS NULL )
    AND  ( frty_id = p_frty_id OR frty_id IS NULL OR p_frty_id IS NULL )
    AND  ( stgr_id = p_stgr_id OR stgr_id IS NULL OR p_stgr_id IS NULL )
    ORDER BY DECODE( stof_id, p_stof_id, 1, 9) ASC
    ,        DECODE( mate_id, p_mate_id, 1, 9) ASC
    ,        DECODE( frty_id, p_frty_id, 1, 9) ASC
    ,        DECODE( stgr_id, p_stgr_id, 1, 9) ASC
    ;
BEGIN
  OPEN  c;
  FETCH c
  INTO  v_return;
  CLOSE c;
  RETURN( v_return );
END in_uitslag;

FUNCTION in_uitslag
( p_meet_id IN NUMBER
, p_bewy_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';

  CURSOR c
  IS
    SELECT bewy.bety_id
    ,      meet.meet_id
    ,      meet.stof_id
    ,      mons.mate_id
    ,      frac.frty_id
    ,      meti.stgr_id
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      bas_berekenings_wijzen bewy
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    meet.meet_id = p_meet_id
    AND    bewy.bewy_id = p_bewy_id
    ;
  r c%rowtype;
BEGIN
  OPEN  c;
  FETCH c
  INTO  r;
  CLOSE c;
  IF ( r.meet_id IS NOT NULL )
  THEN
    v_return := in_uitslag
                ( p_bety_id => r.bety_id
                , p_stof_id => r.stof_id
                , p_mate_id => r.mate_id
                , p_frty_id => r.frty_id
                , p_stgr_id => r.stgr_id
                );
  END IF;
  RETURN( v_return );
END in_uitslag;

-- functie uitslag wordt alleen aangeroepen in enz_uitslag_ovz_..._vw
-- en is waarschijnlijk achterhaald (MK 26-02-2004)
FUNCTION  uitslag
 (  p_meet_id  IN  NUMBER
 ,  p_berekening_type  IN  VARCHAR2
 ,  p_kolom            IN  VARCHAR2
 )
 RETURN  VARCHAR2
 IS
  v_uitslag NUMBER;
  v_bereik  VARCHAR2  (100);
  v_eenheid VARCHAR2  (100);
  v_return  VARCHAR2  (100);
  v_onder   VARCHAR2  (100);
  v_boven   VARCHAR2  (100);

  CURSOR bere_cur
  IS
    SELECT uitslag,
           normaalwaarde_ondergrens||' - '||normaalwaarde_bovengrens bereik,
           normaalwaarde_ondergrens ,
           normaalwaarde_bovengrens ,
           uitslageenheid
    FROM   bas_berekeningen bere
    ,      bas_berekening_types bety
    WHERE  bere.bety_id = bety.bety_id
    AND    bety.code = UPPER( p_berekening_type )
    AND    bere.meet_id = p_meet_id
    ;
BEGIN
  OPEN  bere_cur;
  FETCH bere_cur
  INTO  v_uitslag,
        v_bereik,
        v_onder,
        v_boven,
        v_eenheid;
  CLOSE bere_cur;
  IF UPPER(p_kolom) = 'UITSLAG'
  THEN
      v_return := v_uitslag;
  ELSIF UPPER(p_kolom) = 'BEREIK'
  THEN
      v_return := v_bereik;
  ELSIF UPPER(p_kolom) = 'EENHEID'
  THEN
      v_return := v_eenheid;
  ELSIF UPPER(p_kolom) = 'ONDER'
  THEN
      v_return := v_onder;
  ELSIF UPPER(p_kolom) = 'BOVEN'
  THEN
      v_return := v_boven;
  END IF;
  RETURN( v_return );
END uitslag;

procedure bepaal_tonen_als
( p_meet_id IN NUMBER
, p_meetwaarde IN VARCHAR2
, x_tonen_als out varchar2
, x_eenheid out varchar2
)
IS
  v_tonen_als varchar2(1000);
  v_eenheid varchar2(30);
  v_num_waarde number;

CURSOR tmwa_cur(b_meetwaarde IN NUMBER -- NUMERIEK!!!!
                ) IS
  SELECT meet.meti_id
        ,tmwa.voorvoegsel
        ,tmwa.waarde        alt_waarde
        ,meet.meetwaarde    act_waarde --Added for Mantis 2775
        ,tmwa.tekst
        ,tmwa.eenheid
        ,tmwa.form_id
        ,frmu.formule_tekst
        ,tmwa.waarde_va --Added by RBA for mantis 2775
        ,tmwa.waarde_tm --Added by RBA for mantis 2775
        ,tmwa.excl_grenzen --Added by RBA for mantis 2775
  FROM   bas_formules            frmu
        ,kgc_toon_meetwaarde_als tmwa
        ,bas_metingen            meti
        ,bas_meetwaarden         meet
  WHERE  tmwa.stof_id = meet.stof_id
  AND    meet.meti_id = meti.meti_id
  AND    nvl(meti.stgr_id
            ,-9999999999) = nvl(tmwa.stgr_id
                                ,nvl(meti.stgr_id
                                    ,-9999999999))
  AND    tmwa.form_id = frmu.form_id(+)
  AND    tmwa.vervallen = 'N'
        /*AND ( ( (b_meetwaarde >= to_number(tmwa.waarde_va) or to_number(tmwa.waarde_va) is null)
         AND (b_meetwaarde <= to_number(tmwa.waarde_tm) or to_number(tmwa.waarde_tm) is null)
         AND tmwa.excl_grenzen = 'N'
           )
        or ( (b_meetwaarde > to_number(tmwa.waarde_va) or to_number(tmwa.waarde_va) is null)
         AND (b_meetwaarde < to_number(tmwa.waarde_tm) or to_number(tmwa.waarde_tm) is null)
         AND tmwa.excl_grenzen = 'J'
           )
         )*/--Commented by RBA for Mantis 2775 rework(12-11-2013)
  AND    meet.meet_id = p_meet_id;
tmwa_rec tmwa_cur%ROWTYPE;
BEGIN
IF p_meetwaarde IS NOT NULL
THEN
  BEGIN
    v_num_waarde := p_meetwaarde;
  EXCEPTION
    WHEN value_error THEN
      v_num_waarde := NULL;
  END;

  /*open  tmwa_cur( b_meetwaarde => v_num_waarde );
  fetch tmwa_cur
  into  tmwa_rec;
  close tmwa_cur;*/--Commented by RBA for Mantis 2775 rework(12-11-2013)

  /*if ( tmwa_rec.tekst is not null )
  then
    v_tonen_als := tmwa_rec.tekst;
  elsif ( tmwa_rec.form_id is not null )
  then*/ --commented by RBA for Mantis number 2775

  --Added For loop by RBA for Mantis 2775 rework(12-11-2013)

  FOR c_cur IN tmwa_cur(b_meetwaarde => v_num_waarde)
  LOOP

    IF (c_cur.form_id IS NOT NULL)
    THEN
      bereken_formule(p_formule_tekst    => c_cur.formule_tekst
                     ,p_form_id          => c_cur.form_id
                     ,p_meet_id          => p_meet_id
                     ,p_meti_id          => c_cur.meti_id
                     ,p_eenheid          => NULL
                     ,p_bepaal_tonen_als => 'J'
                      --      , p_meetwaarde => c_cur.alt_waarde
                     ,p_meetwaarde => c_cur.act_waarde --Added for Mantis 2775
                     ,x_waarde     => v_num_waarde
                     ,x_eenheid    => v_eenheid);
      /*      v_tonen_als := tmwa_rec.voorvoegsel||to_char(v_num_waarde);

        elsif ( tmwa_rec.alt_waarde is not null )
        then
          v_tonen_als := tmwa_rec.voorvoegsel||tmwa_rec.alt_waarde;
        end if;
      end if;*/ --Commented by RBA for mantis 2775
      --Added the following code for mantis 2775
      IF (((v_num_waarde >= to_number(c_cur.waarde_va) OR
         to_number(c_cur.waarde_va) IS NULL) AND
         (v_num_waarde <= to_number(c_cur.waarde_tm) OR
         to_number(c_cur.waarde_tm) IS NULL) AND
         c_cur.excl_grenzen = 'N') OR
         ((v_num_waarde > to_number(c_cur.waarde_va) OR
         to_number(c_cur.waarde_va) IS NULL) AND
         (v_num_waarde < to_number(c_cur.waarde_tm) OR
         to_number(c_cur.waarde_tm) IS NULL) AND
         c_cur.excl_grenzen = 'J'))
      THEN
        IF (c_cur.tekst IS NOT NULL)
        THEN
          v_tonen_als := c_cur.tekst;
        ELSE
          v_tonen_als := c_cur.voorvoegsel || c_cur.alt_waarde;
        END IF;
      END IF;
    ELSE
      IF (((v_num_waarde >= to_number(c_cur.waarde_va) OR
         to_number(c_cur.waarde_va) IS NULL) AND
         (v_num_waarde <= to_number(c_cur.waarde_tm) OR
         to_number(c_cur.waarde_tm) IS NULL) AND
         c_cur.excl_grenzen = 'N') OR
         ((v_num_waarde > to_number(c_cur.waarde_va) OR
         to_number(c_cur.waarde_va) IS NULL) AND
         (v_num_waarde < to_number(c_cur.waarde_tm) OR
         to_number(c_cur.waarde_tm) IS NULL) AND
         c_cur.excl_grenzen = 'J'))
      THEN
        IF (c_cur.tekst IS NOT NULL)
        THEN
          v_tonen_als := c_cur.tekst;
        ELSE
          v_tonen_als := c_cur.voorvoegsel || c_cur.alt_waarde;
        END IF;
      END IF;
    END IF;
    x_tonen_als := v_tonen_als;
    x_eenheid   := c_cur.eenheid;
  END LOOP;
END IF;
--End here for mantis 2775
end bepaal_tonen_als;
-- idem, overloaded...
FUNCTION bepaal_tonen_als
( p_meet_id IN NUMBER
, p_meetwaarde IN VARCHAR2
)
RETURN varchar2
IS
  v_tonen_als varchar2(1000) := NULL;
  v_eenheid varchar2(30); -- dummy
begin
  bepaal_tonen_als
  ( p_meet_id => p_meet_id
  , p_meetwaarde => p_meetwaarde
  , x_tonen_als => v_tonen_als
  , x_eenheid => v_eenheid
  );
  return( v_tonen_als );
end bepaal_tonen_als;

FUNCTION formatteer
( p_bere_id  IN NUMBER
, p_uitslag  IN NUMBER
)
RETURN VARCHAR2
IS
  CURSOR frmu_cur
  ( b_bere_id IN NUMBER
  )
  IS
    SELECT bere.meet_id
    ,      frmu.aantal_decimalen
    FROM   bas_berekeningen       bere
    ,      bas_berekenings_wijzen bewy
    ,      bas_formules           frmu
    WHERE  bewy.bewy_id = bere.bewy_id
    AND    frmu.form_id = bewy.form_id
    AND    bere.bere_id = b_bere_id
    ;
  frmu_rec frmu_cur%rowtype;
  v_return varchar2(1000);
BEGIN
  OPEN  frmu_cur( b_bere_id => p_bere_id );
  FETCH frmu_cur
  INTO  frmu_rec;
  CLOSE frmu_cur;

  IF ( frmu_rec.aantal_decimalen IS NOT NULL )
  THEN
    v_return := kgc_formaat_00.nummer
                ( p_waarde   => p_uitslag
                , p_decimaal => frmu_rec.aantal_decimalen
                );
  ELSE
    v_return := bas_meet_00.formatteer
                ( p_meet_id => frmu_rec.meet_id
                , p_waarde  => p_uitslag
                );
  END IF;

  RETURN ( v_return );
END formatteer;
END BAS_BERE_00;
/

/
QUIT
