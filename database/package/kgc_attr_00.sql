CREATE OR REPLACE PACKAGE KGC_ATTR_00 IS
-- PL/SQL Specification
-- retourneer de attribuutwaarde
FUNCTION waarde
( p_tabel_naam IN VARCHAR2
, p_code IN VARCHAR2
, p_id IN NUMBER
)
RETURN VARCHAR2;

PROCEDURE set_waarde
( p_tabel_naam VARCHAR2
, p_code VARCHAR2
, p_id NUMBER
, p_waarde VARCHAR2
);

END KGC_ATTR_00;
/

CREATE OR REPLACE PACKAGE BODY KGC_ATTR_00 IS
FUNCTION waarde
( p_tabel_naam IN VARCHAR2
, p_code IN VARCHAR2
, p_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR atwa_cur
  IS
    SELECT atwa.waarde
    FROM   kgc_attribuut_waarden atwa
    ,      kgc_attributen attr
    WHERE  attr.attr_id = atwa.attr_id
    AND    attr.tabel_naam = p_tabel_naam
    AND    attr.code = p_code
    and    atwa.id = p_id
    ;
-- PL/SQL Block
BEGIN
  OPEN  atwa_cur;
  FETCH atwa_cur
  INTO  v_return;
  CLOSE atwa_cur;
  RETURN( v_return );
END waarde;

PROCEDURE set_waarde(p_tabel_naam VARCHAR2, p_code VARCHAR2, p_id NUMBER, p_waarde VARCHAR2) IS
   -- zet de waarde van een attibuut voor een specifieke entiteit
   CURSOR c_attr IS
      SELECT attr.attr_id
      FROM   kgc_attributen attr
      WHERE  attr.tabel_naam = p_tabel_naam
      AND    attr.code = p_code
      ;
   r_attr c_attr%ROWTYPE;
   CURSOR c_atwa(p_attr_id NUMBER) IS
      SELECT atwa_id
      FROM   kgc_attribuut_waarden atwa
      WHERE  atwa.attr_id = p_attr_id
      AND    atwa.id = p_id
      ;
   r_atwa c_atwa%ROWTYPE;
BEGIN
   OPEN c_attr;
   FETCH c_attr INTO r_attr;
   IF c_attr%NOTFOUND THEN
      raise_application_error(-20000, 'Attribuut voor tabel ' || p_tabel_naam|| ', code ' || p_code  || ' werd niet gevonden!(waarschuw systeembeheer)');
   ELSE
      OPEN c_atwa(r_attr.attr_id);
      FETCH c_atwa INTO r_atwa;
      IF c_atwa%FOUND THEN -- update
         UPDATE kgc_attribuut_waarden atwa
         SET waarde = p_waarde
         WHERE atwa.atwa_id = r_atwa.atwa_id;
      ELSE -- insert
         INSERT INTO kgc_attribuut_waarden (attr_id, id, waarde) VALUES (r_attr.attr_id, p_id, p_waarde);
      END IF;
      CLOSE c_atwa;
   END IF;
   CLOSE c_attr;
END set_waarde;


END kgc_attr_00;
/


