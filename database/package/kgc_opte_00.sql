CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPTE_00" IS
   -- PL/SQL Specification

   -- object(record)typen
   r_meting           kgc_rec_meting DEFAULT kgc_rec_meting(NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL);

   r_omf_set          kgc_rec_meting DEFAULT kgc_rec_meting(NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL);

   r_stof_meting_null kgc_rec_stof_meting DEFAULT kgc_rec_stof_meting(NULL -- stof_id
                                                                     ,NULL -- stof_oms
                                                                     ,NULL -- t_meting_set                                                                     ,NULL -- t_meting_set
                                                                     ,NULL -- omf_set_1
                                                                     ,NULL -- omf_set_2
                                                                     ,NULL -- omf_set_3
                                                                     ,NULL -- omf_set_4
                                                                     ,NULL -- omf_set_5
                                                                     ,NULL -- omf_set_6
                                                                     ,NULL -- omf_set_7
                                                                     ,NULL -- omf_set_8
                                                                     ,NULL -- omf_set_9
                                                                     ,NULL -- omf_set_10
                                                                     ,NULL -- omf_set_11
                                                                     ,NULL -- omf_set_12
                                                                     ,NULL -- omf_set_13
                                                                     ,NULL -- omf_set_14
                                                                     ,NULL -- omf_set_15
                                                                     ,NULL -- omf_set_16
                                                                     ,NULL -- omf_set_17
                                                                     ,NULL -- omf_set_18
                                                                     ,NULL -- omf_set_19
                                                                     ,NULL -- omf_set_20
                                                                     ,NULL -- omf_set_21
                                                                     ,NULL -- omf_set_22
                                                                     ,NULL -- omf_set_23
                                                                     ,NULL -- omf_set_24
                                                                     ,NULL -- omf_set_25
                                                                     ,NULL -- omf_set_26
                                                                     ,NULL -- omf_set_27
                                                                     ,NULL -- omf_set_28
                                                                     ,NULL -- omf_set_29
                                                                     ,NULL -- omf_set_30
                                                                     ,NULL -- omf_set_31
                                                                     ,NULL -- omf_set_32
                                                                     ,NULL -- omf_set_33
                                                                     ,NULL -- omf_set_34
                                                                     ,NULL -- omf_set_35
                                                                     ,NULL -- omf_set_36
                                                                     ,NULL -- omf_set_37
                                                                     ,NULL -- omf_set_38
                                                                     ,NULL -- omf_set_39
                                                                     ,NULL -- omf_set_40
                                                                     ,NULL -- omf_set_41
                                                                     ,NULL -- omf_set_42
                                                                     ,NULL -- omf_set_43
                                                                     ,NULL -- omf_set_44
                                                                     ,NULL -- omf_set_45
                                                                     ,NULL -- omf_set_46
                                                                     ,NULL -- omf_set_47
                                                                     ,NULL -- omf_set_48
                                                                     ,NULL -- omf_set_49
                                                                     ,NULL -- omf_set_50
                                                                     ,NULL -- omf_set_51
                                                                     ,NULL -- omf_set_52
                                                                     ,NULL -- omf_set_53
                                                                     ,NULL -- omf_set_54
                                                                     ,NULL -- omf_set_55
                                                                     ,NULL -- omf_set_56
                                                                     ,NULL -- omf_set_57
                                                                     ,NULL -- omf_set_58
                                                                     ,NULL -- omf_set_59
                                                                     ,NULL -- omf_set_60
                                                                     ,NULL -- omf_set_61
                                                                     ,NULL -- omf_set_62
                                                                     ,NULL -- omf_set_63
                                                                     ,NULL -- omf_set_64
                                                                     ,NULL -- omf_set_65
                                                                     ,NULL -- omf_set_66
                                                                     ,NULL -- omf_set_67
                                                                     ,NULL -- omf_set_68
                                                                     ,NULL -- omf_set_69
                                                                     ,NULL -- omf_set_70
                                                                     ,NULL -- omf_set_71
                                                                     ,NULL -- omf_set_72
                                                                     ,NULL -- omf_set_73
                                                                     ,NULL -- omf_set_74
                                                                     ,NULL -- omf_set_75
                                                                     ,NULL -- omf_set_76
                                                                     ,NULL -- omf_set_77
                                                                     ,NULL -- omf_set_78
                                                                     ,NULL -- omf_set_79
                                                                     ,NULL -- omf_set_80
                                                                     ,NULL -- omf_set_81
                                                                     ,NULL -- omf_set_82
                                                                     ,NULL -- omf_set_83
                                                                     ,NULL -- omf_set_84
                                                                     ,NULL -- omf_set_85
                                                                     ,NULL -- omf_set_86
                                                                     ,NULL -- omf_set_87
                                                                     ,NULL -- omf_set_88
                                                                     ,NULL -- omf_set_89
                                                                     ,NULL -- omf_set_90
                                                                     ,NULL -- omf_set_91
                                                                     ,NULL -- omf_set_92
                                                                     ,NULL -- omf_set_93
                                                                     ,NULL -- omf_set_94
                                                                     ,NULL -- omf_set_95
                                                                     ,NULL -- omf_set_96
                                                                     ,NULL -- omf_set_97
                                                                     ,NULL -- omf_set_98
                                                                     ,NULL -- omf_set_99
                                                                     ,NULL -- omf_set_100
                                                                      );

   r_stof_meting      kgc_rec_stof_meting DEFAULT r_stof_meting_null;
   r_onde             kgc_rec_stof_meting DEFAULT r_stof_meting_null;
   r_mate             kgc_rec_stof_meting DEFAULT r_stof_meting_null;
   r_frac             kgc_rec_stof_meting DEFAULT r_stof_meting_null;
   r_meti             kgc_rec_stof_meting DEFAULT r_stof_meting_null;

   r_matrix_null      kgc_rec_matrix DEFAULT kgc_rec_matrix(NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL
                                                           ,NULL);
   r_matrix           kgc_rec_matrix;

   -- tabeltypen
   t_meting      kgc_tab_meting      DEFAULT kgc_tab_meting();
   t_omf_set     kgc_tab_meting      DEFAULT kgc_tab_meting();
   t_stof_meting kgc_tab_stof_meting DEFAULT kgc_tab_stof_meting();
   t_matrix      kgc_tab_matrix      DEFAULT kgc_tab_matrix();

   -- publieke globale variabelen
   g_mresult             VARCHAR2(25) DEFAULT NULL; -- was VARCHAR2(15)
   g_max_aantal_omf_sets PLS_INTEGER  DEFAULT 100; -- beter constante!?
   g_kafd_code           kgc_kgc_afdelingen.code%TYPE;

   -- legenda
   FUNCTION legenda
     ( p_afdeling IN VARCHAR2 )
   RETURN VARCHAR2;

   -- inhoud van (matrix-)veld
   FUNCTION waarde
     ( p_afdeling IN VARCHAR2
     , p_meet_id  IN NUMBER )
   RETURN VARCHAR2;

   -- 236: overload function
   -- Afvangen meet_id in varchar2
   FUNCTION waarde
     ( p_afdeling IN VARCHAR2
     , p_meet_id  IN VARCHAR2 )
   RETURN VARCHAR2;

   -- kleur van het veld wordt bepaald door de inhoud
   FUNCTION kleur
     ( p_afdeling IN VARCHAR2
     , p_waarde   IN VARCHAR2 )
   RETURN VARCHAR2;

   FUNCTION f_get_max_aantal_omf_sets
   RETURN PLS_INTEGER;

   FUNCTION f_retourneer_obj
   RETURN kgc_tab_stof_meting;

   FUNCTION f_retourneer_obj_det
   RETURN kgc_tab_meting;

   FUNCTION f_retourneer_omf_set
   RETURN kgc_tab_meting;

   FUNCTION f_retourneer_matrix
   RETURN kgc_tab_matrix;

   FUNCTION f_kafd_code
   RETURN VARCHAR2;

   PROCEDURE p_bepaal_obj --
     ( p_where        IN VARCHAR2
     , p_weergave_per IN VARCHAR2 DEFAULT 'M' -- per meting
     , p_kafd_code    IN VARCHAR2 DEFAULT NULL
     , p_waarschuwing OUT VARCHAR2 );

   PROCEDURE test;
END KGC_OPTE_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPTE_00" IS
   -- PL/SQL Body

   FUNCTION legenda(p_afdeling IN VARCHAR2) RETURN VARCHAR2 IS
      v_return VARCHAR2(2000);
   BEGIN
      BEGIN
         v_return := kgc_sypa_00.standaard_waarde(p_parameter_code => 'KGCOPTE01_LEGENDA'
                                                 ,p_afdeling       => p_afdeling);
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END;
      -- oude situatie, van voor feb'04 staat hieronder nog
      -- mag/moet tzt. weg
      IF (v_return IS NULL)
      THEN
         IF (p_afdeling = 'DNA')
         THEN
            v_return := 'J = groen = afgerond' || chr(10) || 'N = geel = niet afgerond' || chr(10) ||
                        'leeg = grijs = niet aangemeld' || chr(10) || '* = al eerder getest' || chr(10) ||
                        'M = rood = mutatie gevonden' || chr(10) || 'P = rood = polymorfisme gevonden';
         ELSE
            v_return := 'J = groen = afgerond' || chr(10) || 'N = geel = niet afgerond' || chr(10) ||
                        'leeg = grijs = niet aangemeld' || chr(10) || '* = al eerder getest';
         END IF;
      END IF;
      RETURN(v_return);

   EXCEPTION -- 236
      WHEN OTHERS THEN
         qms$errors.unhandled_exception('kgc_opte_00.legenda');
         --
   END legenda;

   FUNCTION waarde(p_afdeling IN VARCHAR2
                  ,p_meet_id  IN NUMBER) RETURN VARCHAR2 IS
      v_return VARCHAR2(2000);

      CURSOR meet_cur IS
         SELECT meet.meet_id
               ,meet.meti_id
               ,meet.stof_id
               ,meet.afgerond meet_afgerond
                -- ,meti.afgerond meti_afgerond -- 236
               ,nvl(meti.afgerond
                   ,'-') meti_afgerond -- 236
               ,meti.frac_id
               ,meti.onde_id
               ,meti.datum_aanmelding --236
               ,nvl(mdet.waarde
                   ,'-') test_ok -- 236
           FROM bas_meetwaarden        meet
               ,bas_metingen           meti
               ,bas_meetwaarde_details mdet -- 236
          WHERE meet.meti_id = meti.meti_id
            AND meet.meet_id = mdet.meet_id(+) -- 236
            AND mdet.prompt(+) = 'Test OK?' -- 236
            AND meet.meet_id = p_meet_id
          ORDER BY mdet.volgorde ASC; -- 236

      meet_rec meet_cur%ROWTYPE;

      -- stoftest voor dezelfde fractie en hetzelfde onderzoek eerder getest?
      CURSOR eerder_cur IS
         SELECT NULL
           FROM bas_meetwaarden meet
               ,bas_metingen    meti
          WHERE meet.meti_id = meti.meti_id
            AND meet.stof_id = meet_rec.stof_id
            AND meti.onde_id = meet_rec.onde_id
            AND meti.frac_id = meet_rec.frac_id
            -- AND meet.meet_id < p_meet_id;
            AND meet.meet_id <> p_meet_id; -- 236, eerder of later

      -- mutatie of polymorfisme gevonden (waarde=J...) bij een stoftest
      -- binnen dezelfde fractie en hetzelfde onderzoek
      CURSOR mdet_cur(b_prompt IN VARCHAR2) IS
         SELECT NULL
           FROM bas_meetwaarde_details mdet
               ,bas_meetwaarden        meet
               ,bas_metingen           meti
          WHERE mdet.meet_id = meet.meet_id
            AND meet.meti_id = meti.meti_id
            AND meet.stof_id = meet_rec.stof_id
            AND meti.onde_id = meet_rec.onde_id
            AND meti.frac_id = meet_rec.frac_id
            AND upper(mdet.prompt) LIKE b_prompt
            AND substr(ltrim(rtrim(mdet.waarde))
                      ,1
                      ,1) = 'J';

      -- afwijking gevonden (waarde = mutatie, polymorfisme of UV ...) bij een stoftest
      -- binnen dezelfde fractie en hetzelfde onderzoek
      CURSOR mdet_cur_afw -- 236
      (b_prompt IN VARCHAR2, b_waarde IN VARCHAR2) IS
         SELECT NULL
           FROM bas_meetwaarde_details mdet
               ,bas_meetwaarden        meet
               ,bas_metingen           meti
          WHERE mdet.meet_id = meet.meet_id
            AND meet.meti_id = meti.meti_id
            AND meet.stof_id = meet_rec.stof_id
            AND meti.onde_id = meet_rec.onde_id
            AND meti.frac_id = meet_rec.frac_id
            AND upper(mdet.prompt) LIKE b_prompt
            AND upper(mdet.waarde) LIKE b_waarde;

      v_dummy VARCHAR2(1);

   BEGIN
      IF (p_meet_id IS NOT NULL)
      THEN
         OPEN meet_cur;
         FETCH meet_cur
            INTO meet_rec;
         CLOSE meet_cur;

         IF (meet_rec.meti_afgerond = 'J')
         THEN
            v_return := 'J';
         ELSE
            v_return := meet_rec.meet_afgerond;
         END IF;

         v_return := v_return || meet_rec.test_ok; -- 236

         IF (meet_rec.meet_id IS NOT NULL)
         THEN

            -- 236 >>> check op prompt of op waarde
            IF kgc_sypa_00.standaard_waarde(p_parameter_code => 'KGCOPTE01_AFWIJKING_WEERGAVE'
                                           ,p_afdeling       => p_afdeling) = 'WAARDE'
            THEN
               -- manier 2: op waarde

               OPEN mdet_cur_afw('%AFW%'
                                ,'%POLY%');
               FETCH mdet_cur_afw
                  INTO v_dummy;
               IF (mdet_cur_afw%FOUND)
               THEN
                  v_return := v_return || 'P';
               END IF;
               CLOSE mdet_cur_afw;

               OPEN mdet_cur_afw('%AFW%'
                                ,'%MUT%');
               FETCH mdet_cur_afw
                  INTO v_dummy;
               IF (mdet_cur_afw%FOUND)
               THEN
                  v_return := v_return || 'M';
               END IF;
               CLOSE mdet_cur_afw;

               OPEN mdet_cur_afw('%AFW%'
                                ,'%UV%');
               FETCH mdet_cur_afw
                  INTO v_dummy;
               IF (mdet_cur_afw%FOUND)
               THEN
                  v_return := v_return || 'UV';
               END IF;
               CLOSE mdet_cur_afw;

            ELSE
               -- <<< einde 236
               -- manier 1: op prompt

               -- mutatie?
               OPEN mdet_cur('MUT%');
               FETCH mdet_cur
                  INTO v_dummy;
               IF (mdet_cur%FOUND)
               THEN
                  v_return := v_return || 'M';
               END IF;
               CLOSE mdet_cur;

               -- polymorfisme?
               OPEN mdet_cur('POLY%');
               FETCH mdet_cur
                  INTO v_dummy;
               IF (mdet_cur%FOUND)
               THEN
                  v_return := v_return || 'P';
               END IF;
               CLOSE mdet_cur;

               -- UV? -- 236
               OPEN mdet_cur('UV%');
               FETCH mdet_cur
                  INTO v_dummy;
               IF (mdet_cur%FOUND)
               THEN
                  v_return := v_return || 'UV';
               END IF;
               CLOSE mdet_cur;

            END IF; -- 236

            -- eerder getest
            OPEN eerder_cur;
            FETCH eerder_cur
               INTO v_dummy;
            IF (eerder_cur%FOUND)
            THEN
               v_return := v_return || '*';
            END IF;
            CLOSE eerder_cur;
         END IF;
      END IF;

      RETURN(v_return);

   EXCEPTION -- 236
      WHEN OTHERS THEN
         qms$errors.unhandled_exception('kgc_opte_00.waarde');
         --
   END waarde;

   -- 236: overload function
   -- Afvangen meet_id in varchar2
   FUNCTION waarde(p_afdeling IN VARCHAR2
                  ,p_meet_id  IN VARCHAR2) RETURN VARCHAR2 IS

      v_meet_id bas_meetwaarden.meet_id%TYPE;
      v_return  VARCHAR2(2000);

   BEGIN

      v_meet_id := to_number(p_meet_id);
      v_return  := waarde(p_afdeling => p_afdeling
                         ,p_meet_id  => v_meet_id);
      RETURN v_return;

   EXCEPTION
      WHEN OTHERS THEN
         RETURN p_meet_id;

   END;

   FUNCTION kleur(p_afdeling IN VARCHAR2
                 ,p_waarde   IN VARCHAR2)
   RETURN VARCHAR2 IS
      v_return VARCHAR2(100);
      PROCEDURE if_null(b_kleur IN VARCHAR2) IS
      BEGIN
         IF (v_return IS NULL)
         THEN
            v_return := b_kleur;
         END IF;
      END if_null;
   BEGIN
      IF (v_return IS NULL)
      THEN
         IF (p_waarde LIKE '%M%') -- Mutatie
         THEN
            if_null('ROOD');
         ELSIF (p_waarde LIKE '%P%') -- Polymorfisme
         THEN
            if_null('ROOD');
         ELSIF (p_waarde LIKE '%UV%') -- UV
         THEN
            if_null('ROOD');
         END IF;
      END IF;

      IF (v_return IS NULL)
      THEN
         IF (p_waarde LIKE 'J%') -- Afgerond
         THEN
            if_null('GROEN');
         ELSIF (p_waarde LIKE 'N%') -- Niet afgerond
         THEN
            if_null('GEEL');
         ELSE
            if_null('DEFAULT');
         END IF;
      END IF;
      RETURN(v_return);

   EXCEPTION
      WHEN OTHERS THEN
         qms$errors.unhandled_exception('kgc_opte_00.kleur');
         --
   END kleur;

   /**************************************************************************/
   FUNCTION f_get_max_aantal_omf_sets RETURN PLS_INTEGER IS
   BEGIN
      RETURN g_max_aantal_omf_sets; --_set;
   END;

   /**************************************************************************/
   FUNCTION f_retourneer_obj RETURN kgc_tab_stof_meting IS
      /*********************************************************************************
      ** Auteur: N. ter Burg
      ** Datum : 17-06-2009
      ** Actie : Creatie
      **
      ** Doel: Retourneren van vooraf bepaalde gegevens in de vorm van een geheugen
      **       tabel. Gegevens zijn bepaald door p_bepaal_obj. View
      **       .... is gebaseerd op deze functie.
      **
      **       Wijzigingshistorie
      ** Wie      Waneer        Wat
      **
      *********************************************************************************/
   BEGIN
      RETURN t_stof_meting;
   END f_retourneer_obj;

   /**************************************************************************/
   FUNCTION f_retourneer_obj_det RETURN kgc_tab_meting IS
   BEGIN
      RETURN t_meting; --_set;
   END;

   /**************************************************************************/
   FUNCTION f_retourneer_omf_set RETURN kgc_tab_meting IS
   BEGIN
      RETURN t_omf_set;
   END;

   /**************************************************************************/
   FUNCTION f_retourneer_matrix RETURN kgc_tab_matrix IS
   BEGIN
      RETURN t_matrix;
   END;

   /**************************************************************************/
   FUNCTION f_kafd_code RETURN VARCHAR2 IS
   BEGIN
      RETURN g_kafd_code;
   END;

   /**************************************************************************/
   PROCEDURE p_bepaal_obj
   (p_where        IN VARCHAR2
   ,p_weergave_per IN VARCHAR2 DEFAULT 'M' -- per meting
   ,p_kafd_code    IN VARCHAR2 DEFAULT NULL
   ,p_waarschuwing OUT VARCHAR2) IS
      /*********************************************************************************
      ** Auteur: N. ter Burg
      ** Datum : 17-06-2009
      ** Actie : Creatie
      **
      ** Doel: Ophalen van meetgegevens adhv. wgr_gid. De gegevens worden opgeslagen
      **       in een geheugentabel, welke door f_retourneer_obj weer
      **       wordt uitgelezen. Wordt gebruikt door scherm KGC_OPTE01.
      **
      **       Wijzigingshistorie
      ** Wie      Wanneer        Wat
      ** JKO      2010-10-18     Kopregel met #MET_ID# toegevoegd aan tabel t_stof_meting.
      ** JKO      2009-10-29     Collection T_MATRIX tbv. KGCOPTE51 bijhouden.
      ** JKO      2009-07-21     Ook lege metingen aan collection(s) toevoegen.
      *********************************************************************************/

      -- ref_cursor
      TYPE ref_cur IS REF CURSOR;

      rc_stom ref_cur;

      -- records
      TYPE rec_stom IS RECORD
        (
          stof_id_sel           kgc_stoftesten.stof_id%TYPE
         ,stof_oms_sel          kgc_stoftesten.omschrijving%TYPE
         ,onde_id               kgc_openstaande_testen_vw.onde_id%TYPE
         ,onderzoeknr           kgc_openstaande_testen_vw.onderzoeknr%TYPE
         ,mate_id               kgc_openstaande_testen_vw.mate_id%TYPE
         ,mate_code             kgc_openstaande_testen_vw.mate_code%TYPE
         ,frac_id               kgc_openstaande_testen_vw.frac_id%TYPE
         ,fractienummer         kgc_openstaande_testen_vw.fractienummer%TYPE
         ,meti_id               kgc_openstaande_testen_vw.meti_id%TYPE
         ,meti_datum_aanmelding kgc_openstaande_testen_vw.meti_datum_aanmelding%TYPE
         ,familienummer         kgc_openstaande_testen_vw.familienummer%TYPE
         ,in_wlit               VARCHAR2(1) -- J/N
         ,meet_id_sel           VARCHAR2(10)
        );

      r_stom rec_stom;

      -- variabelen
      l_al_aanwezig BOOLEAN         DEFAULT FALSE;
      l_query       VARCHAR2(32767) DEFAULT 'select :s from :f where :w order by :ob';
      l_sel         VARCHAR2(1000)  DEFAULT '*';
      l_from        VARCHAR2(500)   DEFAULT 'dual';
      l_where       VARCHAR2(30767) DEFAULT '1 = 2'; -- haalt niets op
      l_sort        VARCHAR2(500)   DEFAULT '1 asc';
      -- timer         TIMESTAMP       DEFAULT systimestamp;

   BEGIN
      -- Debug info tbv. fouten en performance
      qms$errors.show_debug_info('kgc_opte_00.p_bepaal_obj');
      qms$errors.show_debug_info('timer 0: ' || to_char(systimestamp,'hh24:mi:ssxff3'));

      g_kafd_code := p_kafd_code;

      -- Init: geheugentabellen leegmaken
      t_meting.DELETE;
      t_omf_set.DELETE;
      t_stof_meting.DELETE;
      t_matrix.DELETE;

      -- records expliciet nogmaals schonen
      r_stof_meting := r_stof_meting_null;
      r_onde        := r_stof_meting_null;
      r_mate        := r_stof_meting_null;
      r_frac        := r_stof_meting_null;
      r_meti        := r_stof_meting_null;

      p_waarschuwing := '';

      -- Opbouw query
      l_sel := ' nvl(stof.stof_id, -9) AS stof_id_sel ' || -- -9 betekent een fictieve stoftest (tbv. lege meting)
               ',nvl(stof.omschrijving, ''<< lege meting >>'') AS stof_oms_sel ' || --
               ',opte.onde_id ' || --
               ',opte.onderzoeknr ' || --
               ',opte.mate_id ' || --
               ',opte.mate_code ' || --
               ',opte.frac_id ' || --
               ',opte.fractienummer ' || --
               ',opte.meti_id ' || --
               ',opte.meti_datum_aanmelding ' || --
               ',opte.familienummer ' || --
               ',nvl2(wlit.wlst_id,''J'',''N'') in_wlit ' ||
               ',nvl(to_char(meet.meet_id), ''-'') AS meet_id_sel '; -- '-' betekent een fictieve meetwaarde (tbv. lege meting)
            -- ',kgc_opte_00.waarde(NULL,meet.meet_id) meet_result'; -- nu nog niet, komt later, scheelt performance

      l_from := ' kgc_openstaande_testen_vw opte' ||
                ',bas_meetwaarden           meet' ||
                ',kgc_stoftesten            stof' ||
                ',kgc_werklijst_items       wlit';

      IF p_where IS NOT NULL
      THEN
         l_where := 'meet.meti_id(+)  = opte.meti_id '    || -- voor lege metingen NULL-waarden selecteren
                    'AND meet.stof_id = stof.stof_id(+) ' || --
                    'AND meet.meet_id = wlit.meet_id(+) ' || --
                    'AND ' || p_where;
      ELSE
         l_where := 'meet.meti_id(+)  = opte.meti_id '    || -- voor lege metingen NULL-waarden selecteren
                    'AND meet.stof_id = stof.stof_id(+) ' || --
                    'AND meet.meet_id = wlit.meet_id(+) ';
      END IF;

      l_sort := ' stof.stof_id ' || --
                ',opte.onderzoeknr ' || --
                ',opte.fractienummer '; -- meest recente eerst of laatst; id of volgorde?????

      IF p_weergave_per = 'O'
      THEN -- per onderzoek
         l_sort := l_sort || -- eerst op meetwaarde, dan pas op meting
                   ',nvl(meet.afgerond, ''F'') DESC ' || -- eerst niet afgeronde, dan afgeronde, dan Fictieve stoftest/meetwaarde
                   ',meet.datum_afgerond DESC ' || -- de meest recente
                   ',in_wlit DESC ' || -- niet-op-werklijst eerst
                   ',meet.creation_date ASC ' || -- de oudste
                   ',meet.meet_id ASC '; -- de oudste, beetje overbodig, voor de zekerheid
      ELSE -- per meting
         l_sort := l_sort || --
                   ',opte.meti_datum_aanmelding DESC ' || -- geen tijdscomponent
                   ',opte.meti_id DESC ' || -- de meest recente
                   ',nvl(meet.afgerond, ''F'') DESC ' || -- eerst niet afgeronde, dan afgeronde, dan Fictieve stoftest/meetwaarde
                   ',meet.datum_afgerond DESC ' || -- de meest recente
                   ',in_wlit DESC ' || -- niet-op-werklijst eerst
                   ',meet.creation_date ASC '|| -- de oudste
                   ',meet.meet_id ASC '; -- beetje overbodig, voor de zekerheid
      END IF;

      l_query := 'select ' || l_sel || --
                 '  from ' || l_from || --
                 ' where ' || l_where || --
                 ' order by ' || l_sort;

      -- kgc_import.show_output(p_tekst => l_query);

      OPEN rc_stom FOR l_query;
      --USING l_sel, l_from, p_where, l_sort; -- werkt niet goed voor from en select
      LOOP
         -- kgc_import.show_output(p_tekst => r_stom.meti_id);

         FETCH rc_stom
            INTO r_stom;
         EXIT WHEN rc_stom%NOTFOUND;

         -- kgc_import.show_output(p_tekst => r_stom.meet_id);

         -- 2e Geheugentabel uitbreiden, indien 1e keer of geschakeld wordt naar
         -- een andere stoftest
         IF r_stof_meting.stof_id IS NULL
            OR r_stof_meting.stof_id != r_stom.stof_id_sel
         THEN
            t_stof_meting.EXTEND;

            -- 1e Geheugentabel tussentijds leegmaken voor volgende set,
            -- indien geschakeld wordt naar een andere stoftest
            t_meting.DELETE;
         END IF;

         -- 1e Geheugenobject vullen
         -- Evt. meting toevoegen voor weergave per meting. Nu gaat het per onderzoek.
         r_meting.onde_nr       := r_stom.onderzoeknr;
         r_meting.mate_code     := r_stom.mate_code;
         r_meting.frac_nr       := r_stom.fractienummer;
         r_meting.meti_dat_aanm := r_stom.meti_datum_aanmelding; -- voor weergave per meting
         r_meting.meti_id       := r_stom.meti_id; -- voor weergave per meting
         r_meting.fami_nr       := r_stom.familienummer;
         r_meting.meet_result   := r_stom.meet_id_sel; -- alleen id ter identifcatie

         -- 1e Geheugentabel vullen met object
         t_meting.EXTEND;
         t_meting(t_meting.LAST) := r_meting;

         -- Tussendoor alle mogelijke combinaties onde-mate-frac(-meti)
         -- vastleggen in aparte geheugentabel; hebben we later weer nodig;
         -- tegelijk ontdubbelen!
         r_omf_set.onde_nr         := r_meting.onde_nr;
         r_omf_set.mate_code       := r_meting.mate_code;
         r_omf_set.frac_nr         := r_meting.frac_nr;
         IF p_weergave_per = 'M'
         THEN
           r_omf_set.meti_dat_aanm := r_meting.meti_dat_aanm;
           r_omf_set.meti_id       := r_meting.meti_id;
         ELSE
           r_omf_set.meti_dat_aanm := NULL;
           r_omf_set.meti_id       := NULL;
         END IF;
         r_omf_set.fami_nr         := r_meting.fami_nr;

         -- is voorkomen al aanwezig?
         l_al_aanwezig := FALSE;

         IF t_omf_set.EXISTS(t_omf_set.FIRST)
         THEN
            FOR h IN t_omf_set.FIRST .. t_omf_set.LAST
            LOOP
               -- afh. per weergave
               IF p_weergave_per = 'M'
               THEN
                  IF t_omf_set(h).onde_nr = r_omf_set.onde_nr
                     AND t_omf_set(h).mate_code = r_omf_set.mate_code
                     AND t_omf_set(h).frac_nr = r_omf_set.frac_nr
                     AND t_omf_set(h).meti_id = r_omf_set.meti_id -- tbv. per meting
                  THEN
                     l_al_aanwezig := TRUE;
                  END IF;

               ELSE
                  -- per onderzoek
                  IF t_omf_set(h).onde_nr = r_omf_set.onde_nr
                     AND t_omf_set(h).mate_code = r_omf_set.mate_code
                     AND t_omf_set(h).frac_nr = r_omf_set.frac_nr
                  THEN
                     l_al_aanwezig := TRUE;
                  END IF;
               END IF;

            END LOOP;
         END IF;

         IF NOT l_al_aanwezig
         THEN
            IF t_omf_set.COUNT < g_max_aantal_omf_sets -- begrenzing, anders fout verderop bij aanmaken pivot-tabel
            THEN
               -- toevoegen
               t_omf_set.EXTEND;
               t_omf_set(t_omf_set.LAST) := r_omf_set;
            ELSE
               p_waarschuwing := 'Er zijn meer dan ' || g_max_aantal_omf_sets;

               IF p_weergave_per = 'M'
               THEN
                  p_waarschuwing := p_waarschuwing || ' metingen ';
               ELSE
                  p_waarschuwing := p_waarschuwing || ' onderzoek-fractie-combinaties ';
               END IF;

               p_waarschuwing := p_waarschuwing ||
                                 'gevonden! Niet alle metingen worden getoond. Verfijn eventueel de zoekopdracht.';
            END IF;
         END IF;

         -- 2e Geheugenobject vullen
         r_stof_meting.stof_id      := r_stom.stof_id_sel;
         r_stof_meting.stof_oms     := r_stom.stof_oms_sel;
         r_stof_meting.t_meting_set := t_meting;

         -- 2e Geheugentabel vullen met object
         t_stof_meting(t_stof_meting.LAST) := r_stof_meting;

      END LOOP;
      CLOSE rc_stom;

      -- kgc_import.show_output(p_tekst => 'omfsets doorlopen');
      qms$errors.show_debug_info('timer 1: ' || to_char(systimestamp,'hh24:mi:ssxff3'));

      -- 2e Geheugenobject omf_sets vullen, indien aanwezig per stoftest
      -- Per omf_set doorlopen en van rownum voorzien NA sorteren (tbv. kolomsortering)!
      FOR k IN (SELECT rownum nr
                      ,t.*
                  FROM (SELECT s.*
                          FROM TABLE(kgc_opte_00.f_retourneer_omf_set) s
                         ORDER BY s.onde_nr
                                 ,s.mate_code
                                 ,s.frac_nr
                                 ,s.meti_dat_aanm DESC
                                 ,s.meti_id DESC) t)
      LOOP
         -- Dan stoftesten erbij halen met de meting_sets
         FOR i IN t_stof_meting.FIRST .. t_stof_meting.LAST
         LOOP
            -- Init streepje voor de duidelijkheid
            g_mresult := '-';
            l_al_aanwezig := FALSE;

            -- Vervolgens binnen de meting_sets kijken of omf_set voorkomt
            FOR j IN t_stof_meting(i).t_meting_set.FIRST .. t_stof_meting(i).t_meting_set.LAST
            LOOP
               -- afh. per weergave
               IF p_weergave_per = 'M'
               THEN
                  IF t_stof_meting(i).t_meting_set(j).onde_nr = k.onde_nr
                     AND t_stof_meting(i).t_meting_set(j).mate_code = k.mate_code
                     AND t_stof_meting(i).t_meting_set(j).frac_nr = k.frac_nr
                     AND t_stof_meting(i).t_meting_set(j).meti_id = k.meti_id -- per meting
                     AND NOT l_al_aanwezig -- alleen de eerst gevonden vastleggen (de meest recente
                                           -- combinatie onde-mate-frac-meti dus)
                  THEN
                     -- Meting_result overnemen, anders leeglaten
                     g_mresult := t_stof_meting(i).t_meting_set(j).meet_result;
                     l_al_aanwezig := TRUE;
                  END IF;

               ELSE
                  -- per onderzoek
                  IF t_stof_meting(i).t_meting_set(j).onde_nr = k.onde_nr
                     AND t_stof_meting(i).t_meting_set(j).mate_code = k.mate_code
                     AND t_stof_meting(i).t_meting_set(j).frac_nr = k.frac_nr
                     AND NOT l_al_aanwezig -- alleen de eerst gevonden vastleggen (de meest recente dus)
                  THEN
                     -- Meting_result overnemen, anders leeglaten
                     g_mresult := t_stof_meting(i).t_meting_set(j).meet_result;
                     l_al_aanwezig := TRUE;
                  END IF;

               END IF;
            END LOOP;

            EXECUTE IMMEDIATE ('begin' || --
                              '    kgc_opte_00.t_stof_meting(' || i || ').omf_set_' || k.nr ||
                              '       := kgc_opte_00.g_mresult;' || --
                              ' end;');

            -- Tegelijkertijd tbv. KGCOPTE51 de matrix-collection vanuit elke niet-fictieve meetcel vullen:
            IF ( t_stof_meting(i).stof_id <> -9 OR -- Normale meetwaarde-rij
                 t_stof_meting.COUNT = 1 ) -- Fictieve stoftest-rij is de enige rij
            THEN
              r_matrix                := r_matrix_null; -- rij initialiseren
              r_matrix.stof_id        := t_stof_meting(i).stof_id;
              r_matrix.stof_oms       := t_stof_meting(i).stof_oms;
              r_matrix.onde_nr        := k.onde_nr;
              r_matrix.mate_code      := k.mate_code;
              r_matrix.frac_nr        := k.frac_nr;
              r_matrix.meti_dat_aanm  := k.meti_dat_aanm;
              r_matrix.meti_id        := k.meti_id;
              r_matrix.meet_result    := g_mresult;
              t_matrix.EXTEND;
              t_matrix(t_matrix.LAST) := r_matrix;
            END IF;

         END LOOP;

         -- De omf-gegevens vastleggen in 3 objecten/records
         -- De KGCOPTE51-header-gegevens fami_nr en meti_dat_aanm worden niet toegevoegd; deze staan alleen in de omf_set
         -- Let op: k-gegevens zijn varchar2-datatypen --> extra quootje nodig
         EXECUTE IMMEDIATE ('begin' || --
                           '    kgc_opte_00.r_onde.omf_set_' || k.nr || ' := '''  || k.onde_nr   || ''';'  ||
                           '    kgc_opte_00.r_mate.omf_set_' || k.nr || ' := '''  || k.mate_code || ''';'  ||
                           '    kgc_opte_00.r_frac.omf_set_' || k.nr || ' := '''  || k.frac_nr   || ''';'  ||
                           '    kgc_opte_00.r_meti.omf_set_' || k.nr || ' := ''#' || k.meti_id   || '#'';' ||
                           ' end;');

      END LOOP;

      -- De eventueel aanwezige fictieve stoftest-rij (de laatste) verwijderen tenzij die de enige rij is:
      IF t_stof_meting.COUNT > 1
      THEN
        IF t_stof_meting(t_stof_meting.LAST).stof_id = -9
        THEN
          t_stof_meting.DELETE(t_stof_meting.LAST);
        END IF;
      END IF;

      -- We gaan als laatste nog t_stof_meting uitbreiden met de omf-gegevens (3 rijen)
      t_stof_meting.EXTEND;
      r_onde.stof_oms := lpad('Onderzoek', 25);
      t_stof_meting(t_stof_meting.LAST) := r_onde;
      --
      t_stof_meting.EXTEND;
      r_mate.stof_oms := lpad('Materiaal', 25);
      t_stof_meting(t_stof_meting.LAST) := r_mate;
      --
      t_stof_meting.EXTEND;
      r_frac.stof_oms := lpad('Fractie', 25);
      t_stof_meting(t_stof_meting.LAST) := r_frac;
      --
      t_stof_meting.EXTEND;
      r_meti.stof_oms := lpad('#METI_ID#', 25);
      t_stof_meting(t_stof_meting.LAST) := r_meti;
      --
      qms$errors.show_debug_info('timer 2: ' || to_char(systimestamp,'hh24:mi:ssxff3'));
      qms$errors.show_debug_info('kgc_opte_00.p_bepaal_obj einde');
      --
   EXCEPTION
      WHEN OTHERS THEN
         qms$errors.unhandled_exception('kgc_opte_00.p_bepaal_obj');
         --
   END p_bepaal_obj;

   /**************************************************************************/
   PROCEDURE test IS

      teller  PLS_INTEGER DEFAULT 0;
      l_dummy VARCHAR2(2000) DEFAULT '1 = 1';

   BEGIN
      dbms_output.enable(buffer_size => 1000000);

      -- p_bepaal_obj(p_where      => l_dummy
      --             ,p_weergave_per => FALSE);

      -- Conrole vulling ---------------------------------------------------------
      dbms_output.put_line('');
      dbms_output.put_line('Controle vulling stof');
      IF t_stof_meting.EXISTS(t_stof_meting.FIRST)
      THEN
         FOR i IN t_stof_meting.FIRST .. t_stof_meting.LAST
         LOOP
            dbms_output.put_line('daaro');
            dbms_output.put_line(i || 'e Stoftest: ' || t_stof_meting(i).stof_oms);

            -- IF t_stof_meting(i).t_meting_set.EXISTS(t_stof_meting(i).t_meting_set.FIRST)
            IF t_stof_meting(i).t_meting_set.COUNT > 0
            THEN
               dbms_output.put_line('vervolgens');
               FOR j IN t_stof_meting(i).t_meting_set.FIRST .. t_stof_meting(i).t_meting_set.LAST
               LOOP
                  dbms_output.put_line('          ' || j || ' ' || t_stof_meting(i).t_meting_set(j).meet_result);
               END LOOP;
            END IF;

         END LOOP;
      END IF;

      -- Controle vulling omf_set
      dbms_output.put_line('');
      dbms_output.put_line('Vulling omf_set');
      teller := 0;

      FOR k IN (SELECT rownum nr
                      ,t.*
                  FROM (SELECT s.*
                          FROM TABLE(kgc_opte_00.f_retourneer_omf_set) s
                         ORDER BY s.onde_nr
                                 ,s.mate_code
                                 ,s.frac_nr
                                 ,s.meti_dat_aanm DESC
                                 ,s.meti_id DESC) t)

      LOOP
         teller := teller + 1;
         dbms_output.put_line(teller || 'e ' --
                              || k.nr || ' ' --
                              || k.onde_nr || ' ~ ' --
                              || k.mate_code || ' ~ ' --
                              || k.frac_nr || ' ~ ' --
                              || k.meti_dat_aanm || ' ~ ' --
                              || k.meti_id || ' ~ ' --
                              || k.fami_nr);
      END LOOP;

      qms$errors.show_debug_info('kgc_opte_00.test einde');
      --
   EXCEPTION
      WHEN OTHERS THEN
         qms$errors.unhandled_exception('kgc_opte_00.test');
         --
   END test;
END kgc_opte_00;
/

/
QUIT
