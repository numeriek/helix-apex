CREATE OR REPLACE PACKAGE "HELIX"."KGC_RAPP_00" IS
-- bepaal rapp_id op basis van binnengekomen parameters
FUNCTION rapport_id
( p_ramo_id IN NUMBER := NULL
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
, p_ramo_code IN VARCHAR2 := NULL
, p_kafd_code IN VARCHAR2 := NULL
, p_ongr_code IN VARCHAR2 := NULL
, p_taal_code IN VARCHAR2 := NULL
)
RETURN NUMBER;

-- Haal de vaste tekst voor een bepaald rapport op
-- Indien geen taal bekend is, wordt de tekst zonder taalaanduiding genomen
-- <systeemdatum> wordt vervangen door de huidige datum (dd-mm-yyyy) of de opgegeven systeemdatum
-- <waarde> wordt vervangen door de doorgegeven waarde (bv. een kolomwaarde uit een query)
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
)
RETURN VARCHAR2;

-- overload voor aanroep met waarde
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2;

-- Mantis 1945: variabele systeemdatum toegevoegd
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
, p_waarde IN VARCHAR2
, p_systeemdatum IN DATE
)
RETURN VARCHAR2;

-- Mantis 1945: variabele brieftype_tekst toegevoegd
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
, p_waarde IN VARCHAR2
, p_systeemdatum IN DATE
, p_brieftype_tekst IN VARCHAR2
)
RETURN VARCHAR2;

-- p_waarde is hier bijvoorbeeld <MONSTERNUMMER>X00/00001 <MATERIAAL>Bloed ...
FUNCTION etiket
( p_kafd_id    IN    NUMBER
, p_ongr_id IN NUMBER := NULL
, p_module    IN    VARCHAR2
, p_rapport    IN    VARCHAR2
, p_code    IN    VARCHAR2
, p_waarde    IN    VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( etiket, WNDS );

-- zet alle machtigingsindicaties van een onderzoek in een (1) veld
FUNCTION machtigingsindicatie
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( machtigingsindicatie, WNDS, WNPS, RNPS );

-- zet alle gerelateerde onderzoeken van een onderzoek in een (1) veld
FUNCTION gerelateerde_onderzoeken
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( gerelateerde_onderzoeken, WNDS, WNPS, RNPS );

-- zet alle monster-afnamedatums van een onderzoek in een (1) veld
FUNCTION mons_afnamedatums
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( mons_afnamedatums, WNDS, WNPS, RNPS );

-- zet alle monsterindicaties van een onderzoek in een (1) veld
FUNCTION  monsterindicatie
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( monsterindicatie, WNDS, WNPS, RNPS );

-- zet de uitslagtekst van UITSLAG- en EINDbrieven van een onderzoek in een (1) veld
FUNCTION uitslag_tekst
( p_onde_id    IN    NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( uitslag_tekst, WNDS, WNPS, RNPS );

-- zet de aanvullende informatie van een onderzoek in een (1) veld
-- Noot: heeft op dit moment (07/02/2001) nog geen inhoud
FUNCTION aanvullende_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( aanvullende_info, WNDS, WNPS, RNPS );

-- zet de toelichting van een uitslagbrief in een (1) veld
-- commentaar (eigenlijk voor intern gebruik) wordt hier vooralsnog voor gebruikt/misbruikt
FUNCTION toelichting
( p_uits_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( toelichting, WNDS, WNPS, RNPS );

-- zet de resultaattekst (meting) van een onderzoek in een (1) veld
FUNCTION resultaat_tekst
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( resultaat_tekst, WNDS, WNPS, RNPS );

-- zet alle cc-ontvangers van een uitslagbrief in een (1) veld
-- behalve de geadresseerde
FUNCTION cc_kopiehouders
( p_uits_id IN NUMBER
, p_rela_id IN NUMBER := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( cc_kopiehouders, WNDS, WNPS );

-- volledig_adres van relatie
FUNCTION volledig_adres_rela
( p_rela_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( volledig_adres_rela, WNDS, WNPS );

-- per persoon: laatste (afgeronde) onderzoek = 1, op een na laatste = 2, enz..
FUNCTION volgnr_onderzoek
( p_pers_id IN NUMBER
, p_onde_id IN NUMBER
, p_afgerond IN VARCHAR2 := 'J'
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( volgnr_onderzoek,    WNDS,    WNPS,    RNPS    );

-- maak een regel voor overzichten: wie heeft wat wanneer uitgedraaid
FUNCTION    uitdraai
( p_user IN VARCHAR2 := NULL
, p_module IN VARCHAR2 := NULL
, p_tijd IN DATE := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( uitdraai, WNDS, WNPS, RNPS );

-- bepaal printer
FUNCTION rapport_printer
( p_ramo_code IN VARCHAR2 := NULL
, p_ramo_id IN NUMBER := NULL
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
, p_rapport IN VARCHAR2 := NULL
)
RETURN NUMBER;
-- bepaal printernaam
FUNCTION printer_naam
( p_prin_id IN NUMBER
)
RETURN VARCHAR2;
-- bepaal printerlade
FUNCTION printer_lade
( p_prin_id IN NUMBER
, p_lade    IN NUMBER := 1 -- 1 of 2
)
RETURN VARCHAR2;

-- formatteer waarde in door streepjescode-lezer te lezen waarde
-- ofwel omvat de waarde met ! of *.
FUNCTION barcode
( p_waarde IN VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( barcode, WNDS, WNPS, RNDS, RNPS );

-- retourneer de rapportnaam (= rep-bestand dat moet worden uitgevoerd)
-- via p_entiteit ( BV. 'ONDE') en p_id kunnen andere parameters worden doorgegeven
--
FUNCTION rapport
( p_ramo_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
, p_entiteit IN VARCHAR2 := NULL
, p_id IN NUMBER := NULL
)
RETURN VARCHAR2;

-- retourneer de rapportnaam (= rep-bestand dat moet worden uitgevoerd)
-- via brieftype, afdeling enz.
--
FUNCTION rapport_van_brieftype
( p_brty_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
)
RETURN VARCHAR2;

-- in teksten kan gebruik zijn gemaakt van
-- <DATUM_MONSTER>
-- <MATERIAAL>
-- ...
FUNCTION specifieke_waarden
( p_tekst IN VARCHAR
, p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( specifieke_waarden, WNDS, WNPS, RNPS );

FUNCTION origineel
( p_rela_id IN NUMBER := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( origineel, WNDS, WNPS );

FUNCTION naam_instelling
( p_kafd_id IN NUMBER DEFAULT NULL
, p_ongr_id IN NUMBER DEFAULT NULL
) RETURN VARCHAR2;

FUNCTION naam_instelling
( p_kafd_code IN VARCHAR2 DEFAULT NULL
, p_ongr_code IN VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2;

--  Bepaal naam van te starten rapport
FUNCTION naam_rapport
( p_rapport  IN  VARCHAR2
, p_kafd_id  IN  NUMBER  DEFAULT NULL
, p_ongr_id  IN  NUMBER  DEFAULT NULL
, p_mede_id  IN  NUMBER  DEFAULT NULL
) RETURN VARCHAR2;

FUNCTION concept_tekst
(p_kafd_id IN NUMBER     DEFAULT NULL
,p_ongr_id IN NUMBER     DEFAULT NULL
,p_ramo_id IN NUMBER     DEFAULT NULL
,p_ramo_code IN VARCHAR2 DEFAULT NULL
,p_taal_id IN NUMBER     DEFAULT NULL
,p_destype IN VARCHAR2
) RETURN VARCHAR2;

-- bepalen best id op basis van vast gelegde uitslagen in tabel
FUNCTION bepaal_best_id
( p_prit_id IN NUMBER
, p_best_id IN NUMBER
, p_rapport IN VARCHAR2
, p_uits_id IN NUMBER
, p_onde_id IN NUMBER
) RETURN NUMBER;

-- Mantis 1945 nieuw: bepalen brieftype-tekst die op de uitslagbrief wordt vermeld
FUNCTION brieftype_tekst
( p_onde_afgerond IN VARCHAR2 := NULL
, p_brty_code IN VARCHAR2 := NULL
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_taal_id IN NUMBER := NULL
)
RETURN VARCHAR2;
END KGC_RAPP_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_RAPP_00" IS
-- bepaal rapp_id op basis van binnengekomen parameters
FUNCTION rapport_id
( p_ramo_id IN NUMBER := NULL
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
, p_ramo_code IN VARCHAR2 := NULL
, p_kafd_code IN VARCHAR2 := NULL
, p_ongr_code IN VARCHAR2 := NULL
, p_taal_code IN VARCHAR2 := NULL
)
RETURN NUMBER
IS
  v_rapp_id NUMBER;
  v_ramo_id NUMBER;
  v_kafd_id NUMBER;
  v_ongr_id NUMBER;
  v_taal_id NUMBER;
  CURSOR ramo_cur
  IS
    SELECT ramo_id
    FROM   kgc_rapport_modules
    WHERE  code = p_ramo_code
    ;
  CURSOR kafd_cur
  IS
    SELECT kafd_id
    FROM   kgc_kgc_afdelingen
    WHERE  code = p_kafd_code
    ;
  CURSOR ongr_cur
  IS
    SELECT ongr_id
    FROM   kgc_onderzoeksgroepen
    WHERE  kafd_id = v_kafd_id
    AND    code = p_ongr_code
    ;
  CURSOR taal_cur
  IS
    SELECT taal_id
    FROM   kgc_talen
    WHERE  code = p_taal_code
    ;
  CURSOR rapp_cur
  IS
    SELECT rapp.rapp_id
    FROM   kgc_rapport_modules ramo
    ,      kgc_rapporten rapp
    WHERE  rapp.kafd_id = v_kafd_id
    AND    rapp.ramo_id = ramo.ramo_id
    AND    ramo.ramo_id = v_ramo_id
    AND  ( rapp.ongr_id = v_ongr_id
        OR rapp.ongr_id IS NULL
         )
    AND  ( rapp.taal_id = v_taal_id
        OR rapp.taal_id IS NULL
         )
    ORDER BY rapp.ongr_id ASC -- zonder ongr als laatst
    ,        rapp.taal_id ASC -- zonder taal als laatst
    ;
BEGIN
  IF ( p_ramo_id IS NOT NULL )
  THEN
    v_ramo_id := p_ramo_id;
  ELSE
    OPEN  ramo_cur;
    FETCH ramo_cur
    INTO  v_ramo_id;
    CLOSE ramo_cur;
  END IF;
  IF ( p_kafd_id IS NOT NULL )
  THEN
    v_kafd_id := p_kafd_id;
  ELSE
    OPEN  kafd_cur;
    FETCH kafd_cur
    INTO  v_kafd_id;
    CLOSE kafd_cur;
  END IF;
  IF ( p_ongr_id IS NOT NULL )
  THEN
    v_ongr_id := p_ongr_id;
  ELSE
    OPEN  ongr_cur;
    FETCH ongr_cur
    INTO  v_ongr_id;
    CLOSE ongr_cur;
  END IF;
  IF ( p_taal_id IS NOT NULL )
  THEN
    v_taal_id := p_taal_id;
  ELSE
    OPEN  taal_cur;
    FETCH taal_cur
    INTO  v_taal_id;
    CLOSE taal_cur;
  END IF;

  OPEN  rapp_cur;
  FETCH rapp_cur
  INTO  v_rapp_id;
  CLOSE rapp_cur;
  RETURN( v_rapp_id );
END rapport_id;

-- specifieke waarden worden gebruikt in teksten
FUNCTION specifieke_waarde
( p_code IN VARCHAR2
, p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR mons_cur
  IS
    SELECT mons.datum_aanmelding
    ,      mate.omschrijving materiaal
    FROM   kgc_materialen mate
    ,      kgc_monsters mons
    ,      kgc_onderzoek_monsters onmo
    WHERE  onmo.mons_id = mons.mons_id
    AND    mons.mate_id = mate.mate_id
    AND    onmo.onde_id = p_onde_id
    ORDER BY onmo.onmo_id DESC -- laatste eerst
    ;
  mons_rec mons_cur%rowtype;
BEGIN
  IF ( p_code IN ( '<DATUM_MONSTER>', '<MATERIAAL>' ) )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  mons_rec;
    CLOSE mons_cur;
    IF ( p_code = '<DATUM_MONSTER>' )
    THEN
      v_return := TO_CHAR( mons_rec.datum_aanmelding, 'dd-mm-yyyy' );
    ELSIF ( p_code = '<MATERIAAL>' )
    THEN
      v_return := LOWER( mons_rec.materiaal );
    END IF;
  END IF;
  RETURN( v_return );
END specifieke_waarde;

-- publiek
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
)
RETURN VARCHAR2
IS
BEGIN
  RETURN formatteer(p_kafd_id, p_ongr_id, p_ramo_id, p_module, p_code, p_taal_id , null, null, null);
END;

-- publiek
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2
IS
BEGIN
  RETURN formatteer(p_kafd_id, p_ongr_id, p_ramo_id, p_module, p_code, p_taal_id , p_waarde, null, null);
END;

-- publiek
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
, p_waarde IN VARCHAR2
, p_systeemdatum IN DATE
)
RETURN VARCHAR2
IS
BEGIN
  RETURN formatteer(p_kafd_id, p_ongr_id, p_ramo_id, p_module, p_code, p_taal_id , p_waarde, null, null);
END;

-- publiek
FUNCTION formatteer
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_module IN VARCHAR2 := NULL
, p_code IN VARCHAR2
, p_taal_id IN NUMBER := NULL
, p_waarde IN VARCHAR2
, p_systeemdatum IN DATE
, p_brieftype_tekst IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(4000);
  v_rapp_id NUMBER;
  CURSOR rate_cur
  IS
    SELECT rate.tekst
    ,      rste.label
    FROM   kgc_rapport_stand_teksten rste
    ,      kgc_rapporten rapp
    ,      kgc_rapport_teksten rate
    WHERE  rate.rapp_id = rapp.rapp_id
    AND    rste.ramo_id = rapp.ramo_id
    AND    rste.code = rate.code
    AND    rapp.rapp_id = v_rapp_id
    AND    rate.code = UPPER( p_code )
    ;
  rate_rec rate_cur%rowtype;
BEGIN
  v_rapp_id := rapport_id
               ( p_ramo_code => UPPER( p_module )
               , p_ramo_id => p_ramo_id
               , p_kafd_id => p_kafd_id
               , p_ongr_id => p_ongr_id
               , p_taal_id => p_taal_id
               );
  OPEN  rate_cur;
  FETCH rate_cur
  INTO  rate_rec;
  IF ( rate_cur%notfound )
  THEN
    CLOSE rate_cur;
    -- MACHT_BU en MACHT_BEG worden door elkaar gebruikt
    -- MACHT_BEG(eleidende brief) is eigenlijk verouderd
    IF ( UPPER(p_module) = 'MACHT_BU' )
    THEN
      v_rapp_id := rapport_id
                   ( p_ramo_code => 'MACHT_BEG'
                   , p_ramo_id => p_ramo_id
                   , p_kafd_id => p_kafd_id
                   , p_ongr_id => p_ongr_id
                   , p_taal_id => p_taal_id
                   );
    ELSIF ( UPPER(p_module) = 'MACHT_BEG' )
    THEN
      v_rapp_id := rapport_id
                   ( p_ramo_code => 'MACHT_BU'
                   , p_ramo_id => p_ramo_id
                   , p_kafd_id => p_kafd_id
                   , p_ongr_id => p_ongr_id
                   , p_taal_id => p_taal_id
                   );
    END IF;
    OPEN  rate_cur;
    FETCH rate_cur
    INTO  rate_rec;
    CLOSE rate_cur;
  ELSE
    CLOSE rate_cur;
  END IF;
  v_tekst := rate_rec.tekst;
  -- substitutiecodes case insensitive
  v_tekst := REPLACE( v_tekst, '<systeemdatum>', '<SYSTEEMDATUM>' );
  v_tekst := REPLACE( v_tekst, '<waarde>', '<WAARDE>' );
  v_tekst := REPLACE( v_tekst, '<brieftype-tekst>', '<BRIEFTYPE-TEKST>' );
  -- label is 1-regelig
  IF ( rate_rec.label = 'J' )
  THEN
    v_tekst := RTRIM( LTRIM( REPLACE( v_tekst, CHR(10), ' ' ) ) );
  END IF;

  IF ( INSTR( v_tekst, '<SYSTEEMDATUM>' ) > 0 )
  THEN
    if p_systeemdatum is not null
    then
      v_tekst := REPLACE( v_tekst, '<SYSTEEMDATUM>', TO_CHAR( p_systeemdatum, 'dd-mm-yyyy' ) );
    else
      v_tekst := REPLACE( v_tekst, '<SYSTEEMDATUM>', TO_CHAR( SYSDATE, 'dd-mm-yyyy' ) );
    end if;
  END IF;

  IF ( INSTR( v_tekst, '<WAARDE>' ) > 0 )
  THEN
    v_tekst := REPLACE( v_tekst, '<WAARDE>', p_waarde );
  END IF;

  IF ( INSTR( v_tekst, '<BRIEFTYPE-TEKST>' ) > 0 )
  THEN
    v_tekst := REPLACE( v_tekst, '<BRIEFTYPE-TEKST>', p_brieftype_tekst );
  END IF;
  --
  RETURN( v_tekst );
END formatteer;

FUNCTION specifieke_waarden
( p_tekst IN VARCHAR
, p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(32767) := p_tekst;
  v_code VARCHAR2(100);
  v_waarde VARCHAR2(1000);
  v_va NUMBER;
  v_tm NUMBER;
  i NUMBER := 0; -- vangnet
BEGIN
  WHILE ( INSTR( v_tekst, '<' ) < INSTR( v_tekst, '>' )
      AND i < 10
        )
  LOOP
    i := i + 1; -- maximaal 10 keer
    v_va := INSTR( v_tekst, '<' );
    v_tm := INSTR( v_tekst, '>', v_va );
    v_code := SUBSTR( v_tekst
                    , v_va
                    , v_tm - v_va + 1
                    );
    v_waarde := specifieke_waarde
                ( p_code => UPPER( v_code )
                , p_onde_id => p_onde_id
                );
    v_tekst := REPLACE( v_tekst, v_code, v_waarde );
  END LOOP;
  RETURN( v_tekst );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( NVL( v_tekst, p_tekst ) );
END specifieke_waarden;

FUNCTION etiket
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_module IN VARCHAR2
, p_rapport IN VARCHAR2
, p_code IN VARCHAR2
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(4000);
  CURSOR rate_cur
  IS
    SELECT rate.tekst
    ,      rste.label
    FROM   kgc_rapport_modules ramo
    ,      kgc_rapport_stand_teksten rste
    ,      kgc_rapporten rapp
    ,      kgc_rapport_teksten rate
    WHERE  rate.rapp_id = rapp.rapp_id
    AND    rapp.kafd_id = p_kafd_id
    AND    rapp.ramo_id = ramo.ramo_id
    AND    rste.ramo_id = ramo.ramo_id
    AND    rste.code = rate.code
    AND    ramo.code = UPPER( p_module )
    AND    rapp.rapport = UPPER( p_rapport )
    AND    rate.code = UPPER( p_code )
    AND  ( rapp.ongr_id = p_ongr_id
        OR rapp.ongr_id IS NULL
         )
    ORDER BY rapp.ongr_id ASC -- zonder ongr als laatst
    ;
  rate_rec rate_cur%rowtype;

  PROCEDURE substitueer
  ( b_label IN VARCHAR2
  )
  IS
 --     v_waarde VARCHAR2(100) := NULL; -- old line before mantis 0182
    v_waarde VARCHAR2(4000) := NULL; -- new line after 0182 changes
    v_label_pos NUMBER(3) := NULL;
    v_lengte NUMBER(3) := NULL;
   BEGIN
    IF ( INSTR( v_tekst, b_label ) > 0 )
    THEN
      IF ( INSTR( p_waarde, b_label ) > 0 )
      THEN
        v_label_pos := INSTR( p_waarde, b_label ) + LENGTH(b_label);
        IF ( INSTR( p_waarde, '<', v_label_pos ) > 0 )
        THEN
          v_lengte := INSTR( p_waarde, '<', v_label_pos ) - v_label_pos;
        ELSE
          v_lengte := length( p_waarde ) + 1 - v_label_pos;
        END IF;
        v_waarde := SUBSTR( p_waarde
                          , v_label_pos
                          , v_lengte
                          );
      ELSE
        v_waarde := '';
      END IF;
      v_tekst := REPLACE( v_tekst, b_label, v_waarde );
    END IF;
  END substitueer;

BEGIN
  OPEN  rate_cur;
  FETCH rate_cur
  INTO  rate_rec;
  CLOSE rate_cur;
  v_tekst := rate_rec.tekst;
  --
  -- substitutiecodes case insensitive
  v_tekst := REPLACE( v_tekst, '<systeemdatum>', '<SYSTEEMDATUM>' );
  v_tekst := REPLACE( v_tekst, '<monsternummer>', '<MONSTERNUMMER>' );
  v_tekst := REPLACE( v_tekst, '<materiaal>', '<MATERIAAL>' );
  v_tekst := REPLACE( v_tekst, '<materiaal_code>', '<MATERIAAL_CODE>' );
  v_tekst := REPLACE( v_tekst, '<aanmelddatum>', '<AANMELDDATUM>' );
  v_tekst := REPLACE( v_tekst, '<afnamedatum>', '<AFNAMEDATUM>' );
  v_tekst := REPLACE( v_tekst, '<bergplaats>', '<BERGPLAATS>' );
  v_tekst := REPLACE( v_tekst, '<achternaam>', '<ACHTERNAAM>' );
  v_tekst := REPLACE( v_tekst, '<naam>', '<NAAM>' );
  v_tekst := REPLACE( v_tekst, '<zisnr>', '<ZISNR>' );
  v_tekst := REPLACE( v_tekst, '<geboortedatum>', '<GEBOORTEDATUM>' );
  v_tekst := REPLACE( v_tekst, '<geslacht>', '<GESLACHT>' );
  v_tekst := REPLACE( v_tekst, '<fractienummer>', '<FRACTIENUMMER>' );
  v_tekst := REPLACE( v_tekst, '<cuptype>', '<CUPTYPE>' );
  v_tekst := REPLACE( v_tekst, '<onderzoek>', '<ONDERZOEK>' );
  v_tekst := REPLACE( v_tekst, '<type>', '<TYPE>' );
  v_tekst := REPLACE( v_tekst, '<volgnr>', '<VOLGNR>' );
  --
  -- Substitueer mogelijke etiket waarden
  IF ( INSTR( v_tekst, '<SYSTEEMDATUM>' ) > 0 )
  THEN
    v_tekst := REPLACE( v_tekst, '<SYSTEEMDATUM>', TO_CHAR( SYSDATE, 'dd-mm-yyyy' ) );
  END IF;
  substitueer( b_label => '<MONSTERNUMMER>' );
  substitueer( b_label => '<MATERIAAL>' );
  substitueer( b_label => '<MATERIAAL_CODE>' );
  substitueer( b_label => '<AANMELDDATUM>' );
  substitueer( b_label => '<AFNAMEDATUM>' );
  substitueer( b_label => '<BERGPLAATS>' );
  substitueer( b_label => '<NAAM>' );
  substitueer( b_label => '<ACHTERNAAM>' );
  substitueer( b_label => '<ZISNR>' );
  substitueer( b_label => '<GEBOORTEDATUM>' );
  substitueer( b_label => '<GESLACHT>' );
  substitueer( b_label => '<FRACTIENUMMER>' );
  substitueer( b_label => '<CUPTYPE>' );
  substitueer( b_label => '<ONDERZOEK>' );
  substitueer( b_label => '<TYPE>' );
  substitueer( b_label => '<VOLGNR>' );
  --
  RETURN( v_tekst );
END etiket;

FUNCTION barcode
( p_waarde IN VARCHAR2
)
RETURN VARCHAR2
IS
BEGIN
  RETURN( '!'||p_waarde||'!' );
END barcode;

FUNCTION machtigingsindicatie
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
  CURSOR onin_cur
  IS
    SELECT DISTINCT ingr.machtigingsindicatie
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.ingr_id = ingr.ingr_id
    AND    onin.onde_id = p_onde_id
    ;
  v_indicatie VARCHAR2(4000) := kgc_indi_00.onderzoeksreden( p_onde_id );
BEGIN
  IF ( v_indicatie IS NOT NULL )
  THEN
    v_indicatie := ' ('||v_indicatie||')';
  END IF;
  FOR onin_rec IN onin_cur
  LOOP
    IF ( v_tekst IS NULL )
    THEN
      -- eenmalig onderzoeksreden toevoegen
      v_tekst := onin_rec.machtigingsindicatie||v_indicatie;
    ELSE
      v_tekst := v_tekst||CHR(10)||onin_rec.machtigingsindicatie;
    END IF;
  END LOOP;
  RETURN ( v_tekst );
END machtigingsindicatie;

FUNCTION gerelateerde_onderzoeken
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
  CURSOR reon_cur
  IS
    select reon.onde_id
    ,      onde.onderzoeknr
    ,      onde.datum_binnen
    from   kgc_onderzoeken onde
    ,      kgc_gerelateerde_onderzoeken reon
    where  onde.onde_id = reon.onde_id_gerelateerde
    and    reon.onde_id  = p_onde_id
    order by onde.datum_binnen
    ;
BEGIN
  FOR reon_rec IN reon_cur
  LOOP
    if (v_tekst is null)
       then
       v_tekst := reon_rec.onderzoeknr;
    else
       v_tekst := v_tekst||', '||reon_rec.onderzoeknr;
       end if;
  END LOOP;
  RETURN ( v_tekst );
END gerelateerde_onderzoeken;

FUNCTION mons_afnamedatums
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
  CURSOR mons_cur
  IS
    select nvl( to_char( mons.datum_afname, 'dd-mm-yyyy' ), rpad( ' ', 19 ) ) ||
           ' (' ||
           mons.monsternummer ||
           ') ' mons_datum_nummer
    from   kgc_onderzoeken onde
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_monsters mons
    ,      kgc_materialen mate
    where  onde.onde_id = p_onde_id
    and    onmo.onde_id = onde.onde_id
    and    mons.mons_id = onmo.mons_id
    and    mate.mate_id = mons.mate_id
    order by mate.omschrijving
          , mons.datum_afname
          , mons.monsternummer
    ;
BEGIN
  FOR mons_rec IN mons_cur
  LOOP
    if (v_tekst is null)
       then
       v_tekst := mons_rec.mons_datum_nummer;
    else
       v_tekst := v_tekst || CHR(10) || mons_rec.mons_datum_nummer;
       end if;
  END LOOP;
  RETURN ( v_tekst );
END mons_afnamedatums;

FUNCTION monsterindicatie
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(32767) := NULL;
  CURSOR moin_cur
  IS
    SELECT DISTINCT indi.code
    ,      indi.omschrijving
    ,      moin.opmerking
    FROM   kgc_indicatie_teksten indi
    ,      kgc_monster_indicaties moin
    ,      kgc_onderzoek_monsters onmo
    WHERE  moin.indi_id = indi.indi_id
    AND    onmo.mons_id = moin.mons_id
    AND    onmo.onde_id = p_onde_id
    ;
  v_regel VARCHAR2(4000);
BEGIN
  FOR moin_rec IN moin_cur
  LOOP
    v_regel := moin_rec.omschrijving||' ('||moin_rec.code||')';
    IF ( moin_rec.opmerking IS NOT NULL )
    THEN
      v_regel := v_regel||' [Opm.: '||moin_rec.opmerking||']';
    END IF;
    IF ( v_tekst IS NULL )
    THEN
      v_tekst := v_regel;
    ELSE
      v_tekst := v_tekst||CHR(10)||v_regel;
    END IF;
  END LOOP;
  RETURN ( v_tekst );
END monsterindicatie;

FUNCTION uitslag_tekst
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(32767) := NULL;
  CURSOR uits_cur
  IS
    SELECT uits.tekst -- DISTINCT mag niet vanwege ORDER BY
    FROM   kgc_uitslagen uits
    WHERE  uits.onde_id = p_onde_id
    AND    uits.tekst IS NOT NULL
    ORDER BY uits.datum_uitslag ASC
    ;
BEGIN
  FOR uits_rec IN uits_cur
  LOOP
    IF ( v_tekst IS NULL )
    THEN
      v_tekst := uits_rec.tekst;
    ELSE
      v_tekst := v_tekst||CHR(10)||uits_rec.tekst;
    END IF;
  END LOOP;
  RETURN ( v_tekst );
END uitslag_tekst;

FUNCTION aanvullende_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
BEGIN
  RETURN ( v_tekst );
END aanvullende_info;

FUNCTION toelichting
( p_uits_id IN NUMBER
)
RETURN    VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
  CURSOR uits_cur
  IS
    SELECT toelichting
    FROM   kgc_uitslagen uits
    WHERE  uits_id = p_uits_id
    ;
BEGIN
  OPEN  uits_cur;
  FETCH uits_cur
  INTO  v_tekst;
  CLOSE uits_cur;
  RETURN( v_tekst );
END toelichting;

FUNCTION resultaat_tekst
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
BEGIN
  RETURN ( kgc_onde_00.resultaat_tekst
           ( p_onde_id => p_onde_id
           )
         );
END resultaat_tekst;

FUNCTION cc_kopiehouders
( p_uits_id IN NUMBER
, p_rela_id IN NUMBER := NULL
)
RETURN    VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
  CURSOR koho_cur
  IS
    SELECT koho.rela_id
    ,      koho.volledig_adres -- verder niet gebruiken!
    FROM   kgc_kopiehouders koho
    WHERE  koho.uits_id = p_uits_id
    AND    koho.rela_id <> NVL( p_rela_id, -1 )
    ;
BEGIN
  FOR koho_rec IN koho_cur
  LOOP
    koho_rec.volledig_adres := kgc_adres_00.relatie
                               ( p_rela_id => koho_rec.rela_id
                               , p_met_naam => 'J'
                               );
    -- alles op een regel
    koho_rec.volledig_adres := REPLACE( koho_rec.volledig_adres, CHR(10), ' ' );

    IF ( v_tekst IS NULL )
    THEN
      v_tekst := koho_rec.volledig_adres;
    ELSE
      v_tekst := v_tekst||CHR(10)||koho_rec.volledig_adres;
    END IF;
  END LOOP;
  RETURN ( v_tekst );
END cc_kopiehouders;

FUNCTION origineel
( p_rela_id IN NUMBER := NULL
)
RETURN    VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
BEGIN
  v_tekst := kgc_adres_00.relatie
             ( p_rela_id => p_rela_id
             , p_met_naam => 'J'
             );
  v_tekst := REPLACE( v_tekst, CHR(10), ' ' );

  RETURN ( v_tekst );
END origineel;

FUNCTION volledig_adres_rela
( p_rela_id IN NUMBER
)
RETURN    VARCHAR2
IS
  v_adres VARCHAR2(4000);
BEGIN
  v_adres := kgc_adres_00.relatie
             ( p_rela_id => p_rela_id
             , p_met_naam => 'J'
             );
  RETURN( v_adres );
END volledig_adres_rela;

FUNCTION volgnr_onderzoek
( p_pers_id IN NUMBER
, p_onde_id IN NUMBER
, p_afgerond IN VARCHAR2 := 'J'
)
RETURN NUMBER
IS
  v_return NUMBER;
  CURSOR c
  IS
    SELECT onde_id
    FROM   kgc_onderzoeken
    WHERE  pers_id = p_pers_id
    AND    ( afgerond = p_afgerond
          OR p_afgerond IS NULL
           )
    ORDER BY datum_binnen DESC
    ;

BEGIN
  FOR r IN c
  LOOP
    IF ( r.onde_id = p_onde_id )
    THEN
      v_return := c%rowcount;
      EXIT;
    END IF;
  END LOOP;
  RETURN( v_return );
END volgnr_onderzoek;

FUNCTION uitdraai
( p_user IN VARCHAR2 := NULL
, p_module IN VARCHAR2 := NULL
, p_tijd IN DATE := NULL
)
RETURN    VARCHAR2
IS
  v_uit VARCHAR2(200);
  v_medewerker VARCHAR2(100);
BEGIN
  v_medewerker := NVL( kgc_mede_00.medewerker_naam( p_user => p_user ), p_user );
  v_uit := 'Uitdraai';
  IF ( p_module IS NOT NULL )
  THEN
    v_uit := v_uit||' van '||p_module;
  END IF;
  v_uit := v_uit||' op '||TO_CHAR( NVL(p_tijd,SYSDATE), 'dd-mm-yyyy hh24:mi:ss' )
        || ' door '||v_medewerker
        || '.'
         ;
  RETURN( v_uit );
END uitdraai;

FUNCTION  rapport_printer
( p_ramo_code IN  VARCHAR2 := NULL
, p_ramo_id IN NUMBER := NULL
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
, p_rapport IN VARCHAR2 := NULL
)
RETURN  NUMBER
IS
  v_return VARCHAR2(100);
  v_rapp_id NUMBER;
  v_mede_id NUMBER := NVL( p_mede_id, kgc_mede_00.medewerker_id );
  -- zoek printer op basis van rapport (alleen als rapport_module onbekend is)
  CURSOR rapp_cur
  IS
    SELECT rapp.rapp_id
    FROM   kgc_rapporten rapp
    ,      kgc_rapport_printers rapr
    WHERE  rapp.rapp_id = rapr.rapp_id (+)
    AND    rapp.kafd_id = p_kafd_id
    AND    UPPER(rapp.rapport) = UPPER(p_rapport)
    AND  ( rapp.ongr_id = p_ongr_id
        OR rapp.ongr_id IS NULL
         )
    AND  ( rapp.taal_id = p_taal_id
        OR rapp.taal_id IS NULL
         )
    AND  ( rapr.mede_id = v_mede_id
        OR rapr.mede_id IS NULL
         )
    ORDER BY rapr.rapr_id     -- eerst met specifieke printer
    ,        rapr.mede_id ASC -- zonder mede als laatst
    ,        rapp.ongr_id ASC -- zonder ongr als laatst
    ,        rapp.taal_id ASC -- zonder taal als laatst
    ;
  CURSOR prin_cur
  IS
    SELECT rapr.prin_id
    FROM   kgc_rapport_printers rapr
    WHERE  rapr.rapp_id = v_rapp_id
    AND  ( rapr.mede_id = v_mede_id
        OR rapr.mede_id IS NULL
         )
    ORDER BY rapr.mede_id ASC -- zonder mede als laatst
    ;
BEGIN
  v_rapp_id := rapport_id
               ( p_ramo_code => p_ramo_code
               , p_ramo_id => p_ramo_id
               , p_kafd_id => p_kafd_id
               , p_ongr_id => p_ongr_id
               , p_taal_id => p_taal_id
               );
  IF ( v_rapp_id IS NULL )
  THEN
    -- probeer een rapport te vinden op basis van "rapport"
    OPEN  rapp_cur;
    FETCH rapp_cur
    INTO  v_rapp_id;
    CLOSE rapp_cur;
  END IF;
  OPEN  prin_cur;
  FETCH prin_cur
  INTO  v_return;
  CLOSE prin_cur;
  RETURN( v_return );
END rapport_printer;

FUNCTION printer_naam
( p_prin_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR prin_cur
  IS
    SELECT prin.naam
    FROM   kgc_printers prin
    WHERE  prin.prin_id = p_prin_id
    ;
  prin_rec prin_cur%rowtype;
BEGIN
  OPEN  prin_cur;
  FETCH prin_cur
  INTO  prin_rec;
  CLOSE prin_cur;
  v_return := prin_rec.naam;
  RETURN( v_return );
END printer_naam;

FUNCTION printer_lade
( p_prin_id IN NUMBER
, p_lade IN NUMBER := 1 -- 1 of 2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR prin_cur
  IS
    SELECT prin.lade1
    ,      prin.lade2
    FROM   kgc_printers prin
    WHERE  prin.prin_id = p_prin_id
    ;
  prin_rec prin_cur%rowtype;
BEGIN
  OPEN  prin_cur;
  FETCH prin_cur
  INTO  prin_rec;
  CLOSE prin_cur;
  IF ( NVL( p_lade, 1 ) = 2
   AND prin_rec.lade2 IS NOT NULL
     )
  THEN
    v_return := prin_rec.lade2;
  ELSE
    v_return := prin_rec.lade1;
  END IF;
  RETURN( v_return );
END printer_lade;

FUNCTION rapport
( p_ramo_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
, p_entiteit IN VARCHAR2 := NULL
, p_id IN NUMBER := NULL
)
RETURN VARCHAR2
IS
  v_rapport VARCHAR2(20);
  CURSOR rapp_cur
  IS
    SELECT rapp.rapport
    FROM   kgc_rapport_modules ramo
    ,      kgc_rapporten rapp
    WHERE  rapp.ramo_id = ramo.ramo_id
    AND    ramo.code = p_ramo_code
    AND    rapp.kafd_id = p_kafd_id
    AND  ( rapp.ongr_id = p_ongr_id
        OR rapp.ongr_id IS NULL
         )
    AND  ( rapp.taal_id = p_taal_id
        OR rapp.taal_id IS NULL
         )
    ORDER BY rapp.ongr_id ASC -- zonder ongr als laatst
    ,        rapp.taal_id ASC -- zonder taal als laatst
    ;
  -- specifieke cursoren om p_id om te zetten naar kafd_id, ongr_id en taal_id
  CURSOR onde_cur
  IS
    SELECT kafd_id
    ,      ongr_id
    ,      taal_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_id
    AND    p_entiteit = 'ONDE'
    ;
  onde_rec onde_cur%rowtype;
BEGIN
  IF ( p_entiteit = 'ONDE'
   AND p_id IS NOT NULL
     )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  onde_rec;
    CLOSE onde_cur;
    -- iteratieve aanroep
    v_rapport := rapport
                 ( p_ramo_code => p_ramo_code
                 , p_kafd_id => onde_rec.kafd_id
                 , p_ongr_id => onde_rec.ongr_id
                 , p_taal_id => onde_rec.taal_id
                 );
  ELSE -- eigenlijke parameters
    OPEN  rapp_cur;
    FETCH rapp_cur
    INTO  v_rapport;
    CLOSE rapp_cur;
  END IF;
  RETURN( v_rapport );
END rapport;

FUNCTION rapport_van_brieftype
( p_brty_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_taal_id IN NUMBER := NULL
)
RETURN VARCHAR2
IS
  v_rapport VARCHAR2(20);

  CURSOR rapp_cur
  IS
    SELECT rapp.rapport
    FROM   kgc_rapporten rapp
    ,      kgc_brieftypes brty
    WHERE  brty.brty_id = p_brty_id
    AND    rapp.ramo_id = brty.ramo_id
    AND    rapp.kafd_id = p_kafd_id
    AND  ( rapp.ongr_id = p_ongr_id
        OR rapp.ongr_id IS NULL
         )
    AND  ( rapp.taal_id = p_taal_id
        OR rapp.taal_id IS NULL
         )
    ORDER BY rapp.ongr_id ASC -- zonder ongr als laatst
    ,        rapp.taal_id ASC -- zonder taal als laatst
    ;
BEGIN
  OPEN  rapp_cur;
  FETCH rapp_cur
   INTO  v_rapport;
  CLOSE rapp_cur;

  IF v_rapport IS NULL
  THEN
    SELECT decode(ramo.code,'UITSLAG','KGCUITS51','CONTROLE', 'KGCUITC51',NULL)
      INTO v_rapport
      FROM kgc_rapport_modules ramo
         , kgc_brieftypes brty
      WHERE brty.brty_id = p_brty_id
        AND brty.ramo_id = ramo.ramo_id(+);
  END IF;
  RETURN( v_rapport );
END rapport_van_brieftype;

  FUNCTION naam_instelling
  ( p_kafd_id IN NUMBER DEFAULT NULL
  , p_ongr_id IN NUMBER DEFAULT NULL
  ) RETURN VARCHAR2
  IS
  BEGIN
    RETURN kgc_sypa_00.standaard_waarde
           ( p_parameter_code => 'NAAM_INSTELLING_RAPPORT'
           , p_kafd_id        => p_kafd_id
           , p_ongr_id        => p_ongr_id
           );
  END;

  FUNCTION naam_instelling
  ( p_kafd_code IN VARCHAR2 DEFAULT NULL
  , p_ongr_code IN VARCHAR2 DEFAULT NULL
  ) RETURN VARCHAR2
  IS
  BEGIN
    RETURN kgc_rapp_00.naam_instelling
           ( p_kafd_id => kgc_uk2pk.kafd_id( p_code    => p_kafd_code )
           , p_ongr_id => kgc_uk2pk.ongr_id( p_kafd_id => kgc_uk2pk.kafd_id(p_code => p_kafd_code)
                                           , p_code    => p_ongr_code
                                           )
           );
  END;

  FUNCTION naam_rapport
  ( p_rapport  IN  VARCHAR2
  , p_kafd_id  IN  NUMBER  DEFAULT NULL
  , p_ongr_id  IN  NUMBER  DEFAULT NULL
  , p_mede_id  IN  NUMBER  DEFAULT NULL
  ) RETURN VARCHAR2
  IS
  BEGIN
    RETURN kgc_sypa_00.standaard_waarde
           ( p_parameter_code => p_rapport
           , p_kafd_id        => p_kafd_id
           , p_ongr_id        => p_ongr_id
           , p_mede_id        => NVL( p_mede_id, kgc_mede_00.medewerker_id(USER) )
           );
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN p_rapport;
  END;

  FUNCTION concept_tekst
  (p_kafd_id IN NUMBER     DEFAULT NULL
  ,p_ongr_id IN NUMBER     DEFAULT NULL
  ,p_ramo_id IN NUMBER     DEFAULT NULL
  ,p_ramo_code IN VARCHAR2 DEFAULT NULL
  ,p_taal_id IN NUMBER     DEFAULT NULL
  ,p_destype IN VARCHAR2
  ) RETURN VARCHAR2
  IS
  /*****************************************************************************
   NAME:       concept_tekst
   PURPOSE:    Bepaal de string die als concepttekst afgedrukt moet worden.

   REVISIONS:
   Ver        Date        Author       Description
   ---------  ----------  ------------ ---------------------------------------
   1.0	      13-04-2006  GWE          Melding: HLX-KGCRAPP01-11094
  *****************************************************************************/

    v_concept_tekst varchar2(1000);

  BEGIN
    -- Check eerst of sypa 'CONCEPT_TEKST' bestaat en gevuld is.
    BEGIN
      v_concept_tekst := kgc_sypa_00.systeem_waarde ( p_parameter_code => 'CONCEPT_TEKST'
                                                    , p_kafd_id => p_kafd_id
                                                    , p_ongr_id => p_ongr_id
                                                    );
    EXCEPTION WHEN OTHERS
    THEN
      v_concept_tekst := NULL;
    END;

    -- Als sypa 'CONCEPT_TEKST' niet gevuld dan ook geen controle op registratie in preview
    IF v_concept_tekst IS NOT NULL
    THEN
      IF UPPER( p_destype ) = 'PRINTER'
      OR UPPER( p_destype ) = 'FILE'
      OR kgc_brie_00.registreer_brief_in_preview  ( p_kafd_id   => p_kafd_id
                                                  , p_ongr_id   => p_ongr_id
                                                  , p_ramo_id   => p_ramo_id
                                                  , p_ramo_code => p_ramo_code
                                                  , p_taal_id   => p_taal_id
                                                  ) = 'J'
      OR ( kgc_brie_00.registreer_brief_in_preview ( p_kafd_id   => p_kafd_id
                                                   , p_ongr_id   => p_ongr_id
                                                   , p_ramo_id   => p_ramo_id
                                                   , p_ramo_code => p_ramo_code
                                                   , p_taal_id   => p_taal_id
                                                   ) = 'N'
         AND
           kgc_brie_00.direct_naar_printer ( p_kafd_id   => p_kafd_id
                                           , p_ongr_id   => p_ongr_id
                                           , p_ramo_id   => p_ramo_id
                                           , p_ramo_code => p_ramo_code
                                           , p_taal_id   => p_taal_id
                                           ) = 'N'
         )
      THEN
        v_concept_tekst := NULL;
      END IF;
    END IF;

    RETURN ( v_concept_tekst );
  END concept_tekst;

  -- bepalen best id op basis van vast gelegde uitslagen in tabel
  FUNCTION bepaal_best_id
  ( p_prit_id IN NUMBER
  , p_best_id IN NUMBER
  , p_rapport IN VARCHAR2
  , p_uits_id IN NUMBER
  , p_onde_id IN NUMBER
  ) RETURN NUMBER
  IS
   CURSOR c_prit_uits
   IS
     SELECT best_id
       FROM kgc_printset_tmp prit
      WHERE prit.prit_id = p_prit_id
        AND prit.rapport = p_rapport
        AND prit.uits_id = p_uits_id;
   CURSOR c_prit_onde
   IS
     SELECT best_id
       FROM kgc_printset_tmp prit
      WHERE prit.prit_id = p_prit_id
        AND prit.rapport = p_rapport
        AND prit.onde_id = p_onde_id;
  BEGIN
    IF p_prit_id IS NOT NULL
    THEN
      IF p_uits_id IS NOT NULL
      THEN
        FOR r_prit IN c_prit_uits
        LOOP
          RETURN r_prit.best_id;
        END LOOP;
      END IF;
      IF p_onde_id IS NOT NULL
      THEN
        FOR r_prit IN c_prit_onde
        LOOP
          RETURN r_prit.best_id;
        END LOOP;
      END IF;
      RETURN NULL;
    ELSE
      RETURN p_best_id;
    END IF;
  END bepaal_best_id;

  -- Mantis 1945 nieuw: bepalen brieftype-tekst die op de uitslagbrief wordt vermeld
  FUNCTION brieftype_tekst
  ( p_onde_afgerond IN VARCHAR2 := NULL
  , p_brty_code IN VARCHAR2 := NULL
  , p_kafd_id IN NUMBER := NULL
  , p_ongr_id IN NUMBER := NULL
  , p_module IN VARCHAR2 := NULL
  , p_taal_id IN NUMBER := NULL
  )
  RETURN VARCHAR2
  IS
  BEGIN
    IF ( p_onde_afgerond = 'J' )
    THEN -- RATE met als code de brieftype-code ophalen
      RETURN( formatteer( p_kafd_id, p_ongr_id, NULL, p_module, p_brty_code, p_taal_id, NULL, NULL, NULL ) );
    ELSE -- RATE van de fictieve brieftype-code 'UITSLAG_T' ophalen
      RETURN( formatteer( p_kafd_id, p_ongr_id, NULL, p_module, 'UITSLAG_T', p_taal_id, NULL, NULL, NULL ) );
    END IF;
  END brieftype_tekst;
END   kgc_rapp_00;
/

/
QUIT
