CREATE OR REPLACE PACKAGE "HELIX"."KGC_OBDE_00" IS
-- PL/SQL Specification
PROCEDURE maak_rijen
  ( p_onbe_id  IN  NUMBER
  );

END KGC_OBDE_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OBDE_00" IS
PROCEDURE maak_rijen
  ( p_onbe_id  IN  NUMBER
  ) IS
    CURSOR mwss_cur IS
      SELECT  mwss.volgorde
      ,       mwss.prompt
      FROM    kgc_mwst_samenstelling   mwss
      ,       kgc_meetwaardestructuur  mwst
      WHERE   mwst.mwst_id = mwss.mwst_id
      AND     mwst.code    = 'KGCONBE01'
      AND     NOT EXISTS
            ( SELECT '1'
              FROM   kgc_onbe_details  obmd
              WHERE  obmd.onbe_id      = p_onbe_id
            )
      ;
  -- PL/SQL Block
BEGIN
    IF  p_onbe_id IS NOT NULL
    THEN
      FOR  mwss_rec IN mwss_cur
      LOOP
        BEGIN
          INSERT INTO kgc_onbe_details (
                   onbe_id
          ,        volgorde
          ,        prompt
          )
          VALUES ( p_onbe_id
          ,        mwss_rec.volgorde
          ,        mwss_rec.prompt
          );
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN NULL;
          WHEN OTHERS           THEN RAISE;
        END;
      END LOOP;
    END IF;
  END maak_rijen;
END kgc_obde_00;
/

/
QUIT
