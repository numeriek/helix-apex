CREATE OR REPLACE PACKAGE "HELIX"."KGC_PATCOMP_KOPPELING" IS
/******************************************************************************
   NAME:       KGC_PATCOMP_KOPPELING
   PURPOSE:    ophalen van persoonsgegevens (incl huisarts en verzekeraar)
               voor Nijmegen via specifiek SOAP-protocol.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   6          20-01-2010  RDE              Mantis 3850: telefoon_2 toegevoegd
   5          23-07-2009  RDE              Oracle10: werking xmldom.getAttribute is anders - namspace noodzakelijk!
   4          23-04-2009  RDE              mantis 283 bsn toegevoegd
   3          13-04-2007  RDE              close_all_connections vervangen door close_connection()
   2          27-02-2007  RDE              Aanpassingen tbv Patcomp 2 - werken met sessies
                                           Gebruik sypa tbv username/password toegevoegd
                                           Aanpassingen tbv dft/rocs - 1 extra gegeven (x_pat_aangemeld)!
   1         <27-02-2007  RDE              Nog geen versie beschikbaar
***************************************************************************** */
  g_toon_debug      BOOLEAN := FALSE;
-- PL/SQL Specification
-- de naamgeving van patcomp - xml is aangehouden!
-- all
-- pat = patient
-- ha = huisarts
-- vzk = verzekering/verzekeraar
PROCEDURE haal_zis_gegevens
( p_nummer              IN  VARCHAR2 -- zisnummer; 7 pos; A... = echt; B... = pseudo
, x_faultcode           OUT VARCHAR2 -- A? indien leeg, dan ok!
, x_faultstring         OUT VARCHAR2 -- A?
, x_pat_registratienummer OUT VARCHAR2 -- A7
, x_pat_aanspreeknaam   OUT VARCHAR2 -- A30
, x_pat_naam            OUT VARCHAR2 -- A20
, x_pat_voorvoegsels    OUT VARCHAR2 -- A10
, x_pat_roepnaam        OUT VARCHAR2 -- A15
, x_pat_voorletters     OUT VARCHAR2 -- A5
, x_pat_geslacht        OUT VARCHAR2 -- A1; M/V/O/K (kind)
, x_pat_geboortedatum   OUT VARCHAR2 -- A10; dd-mm-jjjj
, x_pat_telefoonnummer  OUT VARCHAR2 -- A13
, x_pat_telefoontype    OUT VARCHAR2 -- A01; B=Buren;E=Eigen;F=Fam;G=Geheim;I=Instelling;K=Kennissen;W=Werk
, x_pat_telefoonnummer_2 OUT VARCHAR2 -- A13; mantis 3850: was afwezig!
, x_pat_partnernaam     OUT VARCHAR2 -- A20
, x_pat_partnervoorvoegsels OUT VARCHAR2 -- A10
, x_pat_partnernaamisaanspreekna OUT VARCHAR2 -- A3; ja/nee
, x_pat_overleden       OUT VARCHAR2 -- A3; ja/nee
, x_pat_vip             OUT VARCHAR2 -- A3; ja/nee
, x_pat_meerling        OUT VARCHAR2 -- A1???; ja/nee
, x_pat_datumoverleden  OUT VARCHAR2 -- A10; dd-mm-jjjj
, x_pat_registratienummermoeder OUT VARCHAR2 -- A7
, x_pat_straatmetnummer OUT VARCHAR2 -- A29
, x_pat_postcode        OUT VARCHAR2 -- A06
, x_pat_plaats          OUT VARCHAR2 -- A18
, x_pat_land            OUT VARCHAR2 -- A18
, x_pat_aangemeld       OUT BOOLEAN  --
, x_pat_bsn             OUT VARCHAR2 -- A09
, x_pat_bsn_geverifieerd OUT VARCHAR2 -- A3; ja/nee
, x_ha_code             OUT VARCHAR2 -- A06; bv 000013
, x_ha_naam             OUT VARCHAR2 -- A33
, x_ha_voorvoegsels     OUT VARCHAR2 -- A03
, x_ha_voorletters      OUT VARCHAR2 -- A04
, x_ha_geslacht         OUT VARCHAR2 -- A01; M/V/O
, x_ha_telefoonnummer   OUT VARCHAR2 -- A13
, x_ha_straatmetnummer  OUT VARCHAR2 -- A29
, x_ha_postcode         OUT VARCHAR2 -- A06
, x_ha_plaats           OUT VARCHAR2 -- A18
, x_vzk_polisnummer     OUT VARCHAR2 -- A15
, x_vzk_klasse          OUT VARCHAR2 -- A01; 1, 2 of 3
, x_vzk_ingangsdatum    OUT VARCHAR2 -- A10; dd-mm-jjjj
, x_vzk_type            OUT VARCHAR2 -- A01; P=Particulier; Z=Ziekenfonds
, x_vzk_naam            OUT VARCHAR2 -- A30
, x_vzk_code            OUT VARCHAR2 -- A04 bv 5202
, x_vzk_telefoonnummer  OUT VARCHAR2 -- A13
, x_vzk_straatmetnummer OUT VARCHAR2 -- A29
, x_vzk_postcode        OUT VARCHAR2 -- A06
, x_vzk_plaats          OUT VARCHAR2 -- A18
);
END KGC_PATCOMP_KOPPELING;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_PATCOMP_KOPPELING" IS
-- constant
  c_n               CONSTANT VARCHAR2(2) := CHR(10);
  c_msg_header      CONSTANT VARCHAR2(32767) := 'POST http://@soaphost/SOAPServer/rpcrouter HTTP/1.0' || c_n
    || 'Content-Type: text/xml; charset=utf-8' || c_n
    || 'Content-Length: @lenbody'
    || '@cookie'
    ||  c_n;
  c_msg_body        CONSTANT VARCHAR2(32767) :=
       '<?xml version=''1.0'' encoding=''UTF-8''?>' || c_n
    || '<SOAP-ENV:Envelope '
    || 'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" '
    || 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
    || 'xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
    || '<SOAP-ENV:Body>'
    || '@soapbody'
    || '</SOAP-ENV:Body>'
    || '</SOAP-ENV:Envelope>';
  c_pipenaam_soap_sessionid  CONSTANT VARCHAR2(100) := 'PATCOMP_SOAP_SESSIONID';
PROCEDURE getHostUnPw
( x_soaphost            OUT VARCHAR2
, x_username            OUT VARCHAR2
, x_password            OUT VARCHAR2
) IS
  v_standaard_waarde kgc_systeem_parameters.standaard_waarde%type;
  v_pos             NUMBER;
BEGIN
  v_standaard_waarde := kgc_sypa_00.standaard_waarde( p_parameter_code => 'PATCOMP_HOST_UN_PW', p_kafd_id => NULL);
  v_pos := INSTR(v_standaard_waarde, '/');
  IF v_pos > 0 THEN
    x_soaphost := SUBSTR(v_standaard_waarde, 1, v_pos - 1);
    v_standaard_waarde := SUBSTR(v_standaard_waarde, v_pos + 1);
    v_pos := INSTR(v_standaard_waarde, '/');
    IF v_pos > 0 THEN
      x_username := SUBSTR(v_standaard_waarde, 1, v_pos - 1);
      x_password := SUBSTR(v_standaard_waarde, v_pos + 1);
    END IF;
  END IF;
END;
PROCEDURE haal_zis_gegevens
( p_nummer              IN  VARCHAR2 -- zisnummer; 7 pos; A... = echt; B... = pseudo
, x_faultcode           OUT VARCHAR2 -- A255 indien leeg, dan ok!
, x_faultstring         OUT VARCHAR2 -- A255
, x_pat_registratienummer OUT VARCHAR2 -- A7
, x_pat_aanspreeknaam   OUT VARCHAR2 -- A30
, x_pat_naam            OUT VARCHAR2 -- A20
, x_pat_voorvoegsels    OUT VARCHAR2 -- A10
, x_pat_roepnaam        OUT VARCHAR2 -- A15
, x_pat_voorletters     OUT VARCHAR2 -- A5
, x_pat_geslacht        OUT VARCHAR2 -- A1; M/V/O/K (kind)
, x_pat_geboortedatum   OUT VARCHAR2 -- A10; dd-mm-jjjj
, x_pat_telefoonnummer  OUT VARCHAR2 -- A13
, x_pat_telefoontype    OUT VARCHAR2 -- A01; B=Buren;E=Eigen;F=Fam;G=Geheim;I=Instelling;K=Kennissen;W=Werk
, x_pat_telefoonnummer_2 OUT VARCHAR2 -- A13; mantis 3850: was afwezig!
, x_pat_partnernaam     OUT VARCHAR2 -- A20
, x_pat_partnervoorvoegsels OUT VARCHAR2 -- A10
, x_pat_partnernaamisaanspreekna OUT VARCHAR2 -- A3; ja/nee
, x_pat_overleden       OUT VARCHAR2 -- A3; ja/nee
, x_pat_vip             OUT VARCHAR2 -- A3; ja/nee
, x_pat_meerling        OUT VARCHAR2 -- A1???; ja/nee
, x_pat_datumoverleden  OUT VARCHAR2 -- A10; dd-mm-jjjj
, x_pat_registratienummermoeder OUT VARCHAR2 -- A7
, x_pat_straatmetnummer OUT VARCHAR2 -- A29
, x_pat_postcode        OUT VARCHAR2 -- A06
, x_pat_plaats          OUT VARCHAR2 -- A18
, x_pat_land            OUT VARCHAR2 -- A18
, x_pat_aangemeld       OUT BOOLEAN  -- Is de patient in ROCS aanwezig?
, x_pat_bsn             OUT VARCHAR2 -- A09 mantis 283
, x_pat_bsn_geverifieerd OUT VARCHAR2 -- A3; ja/nee
, x_ha_code             OUT VARCHAR2 -- A06; bv 000013
, x_ha_naam             OUT VARCHAR2 -- A33
, x_ha_voorvoegsels     OUT VARCHAR2 -- A03
, x_ha_voorletters      OUT VARCHAR2 -- A04
, x_ha_geslacht         OUT VARCHAR2 -- A01; M/V/O
, x_ha_telefoonnummer   OUT VARCHAR2 -- A13
, x_ha_straatmetnummer  OUT VARCHAR2 -- A29
, x_ha_postcode         OUT VARCHAR2 -- A06
, x_ha_plaats           OUT VARCHAR2 -- A18
, x_vzk_polisnummer     OUT VARCHAR2 -- A15
, x_vzk_klasse          OUT VARCHAR2 -- A01; 1, 2 of 3
, x_vzk_ingangsdatum    OUT VARCHAR2 -- A10; dd-mm-jjjj
, x_vzk_type            OUT VARCHAR2 -- A01; P=Particulier; Z=Ziekenfonds
, x_vzk_naam            OUT VARCHAR2 -- A30
, x_vzk_code            OUT VARCHAR2 -- A04 bv 5202
, x_vzk_telefoonnummer  OUT VARCHAR2 -- A13
, x_vzk_straatmetnummer OUT VARCHAR2 -- A29
, x_vzk_postcode        OUT VARCHAR2 -- A06
, x_vzk_plaats          OUT VARCHAR2 -- A18
) IS
  v_XMLp            xmlparser.Parser;
  v_XMLdoc          xmldom.DOMDocument;
  v_XMLn1           xmldom.DOMNode;
  v_XMLNL           xmldom.DOMNodeList;
  v_XMLNLad         xmldom.DOMNodeList;
  v_XMLNLha         xmldom.DOMNodeList;
  v_XMLNLvz1        xmldom.DOMNodeList;
  v_XMLNLvz2        xmldom.DOMNodeList;
  v_XMLNLpat        xmldom.DOMNodeList;
  v_XMLNLpatpn      xmldom.DOMNodeList;
  v_xmlstart        BOOLEAN := FALSE;
  v_c               utl_tcp.connection;  -- TCP/IP connection to the Web server
  v_ret_val         pls_integer;
  v_username        VARCHAR2(32);
  v_password        VARCHAR2(32);
  v_soaphost        VARCHAR2(200);
  v_faultcode       VARCHAR2(255);
  v_faultstring     VARCHAR2(255);
  v_msg_header      VARCHAR2(32767);
  v_msg_body        VARCHAR2(32767);
  v_str             VARCHAR2(32767);
  v_strAtt          VARCHAR2(2000);
  v_strVal          VARCHAR2(2000);
  v_strXML          VARCHAR2(32000);
  v_nrOf            NUMBER;
  v_soap_sessionid  VARCHAR2(100);
  v_pipe_return     INTEGER;
  v_sesid_token     VARCHAR2(100) := 'JSESSIONID'; -- ???'sesessionid=';
  v_ns_xsi          VARCHAR2(4000); -- 23-07-2009 RDE nieuw: hierin staat de namespace xsi
  PROCEDURE toon_debug (p_text VARCHAR2) IS
  BEGIN
    IF g_toon_debug THEN
      dbms_output.put_line(SUBSTR(p_text, 1, 255));
    END IF;
  END;
  PROCEDURE close_connection(p_c IN OUT utl_tcp.connection) IS
  BEGIN
    utl_tcp.close_connection(p_c);
  EXCEPTION
    WHEN OTHERS THEN
      NULL; -- bv network error: not connected; ok
  END;
BEGIN
  x_pat_aangemeld := FALSE;
  -- haal PATCOMP_HOST_UN_PW
  getHostUnPw(v_soaphost, v_username, v_password);
  -- parser definieren
  v_XMLp := xmlparser.newParser;
  xmlparser.setValidationMode(v_XMLp, FALSE);
  -- ok, lezen maar. 2 pogingen: indien de 1e poging mis gaat vanwege 'sessie verlopen'
  -- dan nog een 2e poging!
  -- 1e poging kan dus zijn:
  -- a. geen sessie-id -> ophalen
  -- b. wel sessie - proberen; wellicht niet meer geldig -> 2e poging
  FOR i IN 1..2 LOOP
    -- is er reeds een sessie beschikbaar? / pipe leegmaken
    v_pipe_return := DBMS_PIPE.RECEIVE_MESSAGE(c_pipenaam_soap_sessionid, 0); -- verwijderd het gelezene
    IF v_pipe_return = 0 THEN -- succes
      dbms_pipe.unpack_message(v_soap_sessionid);
    END IF;
    IF i = 1 AND v_soap_sessionid IS NOT NULL THEN
      NULL; -- gebruik de aanwezige sessie;
    ELSE -- bepaal nieuwe sessie:
      -- open connection; blijkbaar is dit nodig voor iedere vraag
      -- indien je bv na ophalen sessieid door wil gaan om gegevens op te vragen,
      -- dan krijg je een ORA-29260: network error: TNS:connection closed!
      v_c := utl_tcp.open_connection( remote_host => v_soaphost
                                    , remote_port =>  80
      );
      -- bericht maken:
      v_msg_body := REPLACE(c_msg_body, '@soapbody'
        ,  '<ns1:login xmlns:ns1="urn:account_service" '
        || 'SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'
        || '<gebruiker_code xsi:type="xsd:string">' || v_username || '</gebruiker_code>'
        || '<wachtwoord xsi:type="xsd:string">' || v_password || '</wachtwoord>'
        || '</ns1:login>'
        );
      toon_debug('Nieuwe sessie: v_msg_body');
      toon_debug(v_msg_body);
      v_msg_header := c_msg_header;
      v_msg_header := REPLACE(v_msg_header, '@soaphost', v_soaphost);
      v_msg_header := REPLACE(v_msg_header, '@lenbody', TO_CHAR(LENGTH(v_msg_body)));
      v_msg_header := REPLACE(v_msg_header, '@cookie', '');
      toon_debug('Nieuwe sessie: v_msg_header');
      toon_debug(v_msg_header);
      toon_debug('bericht verzenden...');
      v_ret_val := utl_tcp.write_line(v_c, v_msg_header);
      v_ret_val := utl_tcp.write_line(v_c, v_msg_body);
      toon_debug('bericht verzonden!');
      -- terugkomende gegevens ophalen:
      DECLARE
        v_str           VARCHAR2(32767);
        v_pos1          NUMBER;
        v_pos2          NUMBER;
      BEGIN
        LOOP
          v_str := utl_tcp.get_line(v_c, TRUE);
          toon_debug('Gelezen: ' || v_str);
          v_pos1 := INSTR(v_str, v_sesid_token);
          IF v_pos1 > 0 THEN
            v_pos2 := INSTR(v_str, ';', v_pos1);
            IF v_pos2 > 0 THEN
              v_soap_sessionid := SUBSTR(v_str, v_pos1 + LENGTH(v_sesid_token) + 1, v_pos2 - (v_pos1 + LENGTH(v_sesid_token)) - 1);
            ELSE
              v_soap_sessionid := TRIM(SUBSTR(v_str, v_pos1 + LENGTH(v_sesid_token) + 1));
            END IF;
          END IF;
        END LOOP;
      EXCEPTION
        WHEN utl_tcp.end_of_input THEN
          NULL; -- end of input
      END;
      toon_debug('Nieuwe sessie; session-id: ' || v_soap_sessionid);
      close_connection(v_c); --utl_tcp.close_all_connections;
    END IF;
    -- pipe weer vullen met sessionid
    DBMS_PIPE.PACK_MESSAGE(v_soap_sessionid);
    v_pipe_return := DBMS_PIPE.SEND_MESSAGE(c_pipenaam_soap_sessionid, 1); -- max 1 sec
    --
    v_c := utl_tcp.open_connection( remote_host => v_soaphost
                                  , remote_port =>  80
    );
    toon_debug('2');
    -- bericht maken:
    v_msg_body := REPLACE(c_msg_body, '@soapbody'
      ,  '<ns1:read xmlns:ns1="urn:patient_demografisch_huisarts_verzekering_service" '
      || 'SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'
      || '<patient_registratienummer xsi:type="xsd:string">' || p_nummer || '</patient_registratienummer>'
      || '</ns1:read>'
      );
    toon_debug('READ: v_msg_body');
    toon_debug(v_msg_body);
    v_msg_header := c_msg_header;
    v_msg_header := REPLACE(v_msg_header, '@soaphost', v_soaphost);
    v_msg_header := REPLACE(v_msg_header, '@lenbody', TO_CHAR(LENGTH(v_msg_body)));
    v_msg_header := REPLACE(v_msg_header, '@cookie',  c_n || 'Cookie: ' || v_sesid_token || '=' || v_soap_sessionid);
    toon_debug('READ: v_msg_header');
    toon_debug(v_msg_header);
    -- bericht verzenden:
    toon_debug('3');
    v_ret_val := utl_tcp.write_line(v_c, v_msg_header);
    toon_debug('4');
    v_ret_val := utl_tcp.write_line(v_c, v_msg_body);
    toon_debug('5');
    -- terugkomende gegevens ophalen:
    v_strXML := NULL;
    BEGIN
      v_xmlstart := FALSE;
      LOOP
        v_str := utl_tcp.get_line(v_c, TRUE);
        IF NOT v_xmlstart THEN
          IF LOWER(SUBSTR(v_str, 1, 5)) = '<?xml' THEN
            v_xmlstart := TRUE;
          END IF;
        END IF;
        IF v_xmlstart THEN
          toon_debug(v_str);
          v_strXML := v_strXML || v_str;
        END IF;
      END LOOP;
    EXCEPTION
      WHEN utl_tcp.end_of_input THEN
        NULL; -- end of input
    END;
    -- igv testbericht: bovenstaande niet uitvoeren maar direct v_strXML een waarde geven! */
    toon_debug('6');
    IF NOT xmldom.isNull(v_XMLdoc) THEN -- nodig voor de eventuele 2e omloop
      xmldom.freeDocument(v_XMLdoc);
    END IF;

    -- parse:
    toon_debug('7');
    xmlparser.parseBuffer(v_XMLp, v_strXML);
    -- lees document
    toon_debug('8');
    v_XMLdoc := xmlparser.getDocument(v_XMLp);
    v_XMLn1 := xmldom.makeNode(xmldom.getDocumentElement(v_XMLdoc));
    -- gevonden: SOAP-ENV:Envelope
    toon_debug(xmldom.getNodeName(v_XMLn1));
    -- 23-07-2009 RDE nieuw: hierin staan de namespaces - nodig en ophalen!
    v_ns_xsi := xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi', 'xmlns');
    toon_debug('8b-ns-attr=' || v_ns_xsi);
    v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
    -- gevonden: SOAP-ENV:Body
    toon_debug(xmldom.getNodeName(v_XMLn1));
    v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
    -- gevonden: ns1:zoekMetNummerResponse OF fout
    toon_debug(xmldom.getNodeName(v_XMLn1));
    v_str := xmldom.getNodeName(v_XMLn1);
    v_faultcode := NULL;
    v_faultstring := NULL;
    IF UPPER(v_str) = 'SOAP-ENV:FAULT' THEN
      -- fout ophalen
      v_XMLNL := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), 'faultcode');
      -- 1e ophalen; aanvullende navigatie naar node met de waarde!!!
      v_faultcode := SUBSTR(xmldom.getNodeValue(xmldom.getFirstChild(xmldom.item(v_XMLNL, 0))), 1, 255);
      v_XMLNL := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), 'faultstring');
      v_faultstring := SUBSTR(xmldom.getNodeValue(xmldom.getFirstChild(xmldom.item(v_XMLNL, 0))), 1, 255);
      -- mogelijk bevat dit: Exception from service object: [AC000E] Er is geen sessie. Log eerst aan.
      IF i = 1 AND INSTR(UPPER(v_faultstring), '[AC000E]') > 0 THEN -- 2e poging!
        NULL;
      ELSE -- klaar
        x_faultcode := v_faultcode;
        x_faultstring := v_faultstring;
        EXIT;
      END IF;
    ELSE
      v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
      -- gevonden: return
      toon_debug(xmldom.getNodeName(v_XMLn1));
      -- structuur: pat.gegevens
      --             |  |  - adres
      --             |  - huisarts + adres
      --             - verzekering + verzekeraar + adres
      v_XMLNL := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
      -- alle nodes doorlopen:
      toon_debug('9 #nodes:' || xmldom.getLength(v_XMLNL));
      FOR i IN 0..xmldom.getLength(v_XMLNL) - 1 LOOP
      -- kan zijn: adres / huisarts / verzekering / persoonsgegeven
        v_XMLn1 := xmldom.item(v_XMLNL, i);
--        v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
        v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
        toon_debug('10 attr:' || v_strAtt);
        IF v_strAtt = 'ns2:huisarts' THEN -- verwerk huisarts
          v_XMLNLha := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
          FOR i IN 0..xmldom.getLength(v_XMLNLha) - 1 LOOP
            -- huisarts / adresgegevens!
            v_XMLn1 := xmldom.item(v_XMLNLha, i);
--            v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
            v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
            IF v_strAtt = 'ns2:adres' THEN -- verwerk adres
              v_XMLNLad := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
              FOR i IN 0..xmldom.getLength(v_XMLNLad) - 1 LOOP
                -- alleen adresgegevens!
                v_XMLn1 := xmldom.item(v_XMLNLad, i);
--                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
                IF v_strAtt like 'xsd:%' THEN -- verwerk huisarts-adres-attribuut
                  v_str := LOWER(xmldom.getNodeName(v_XMLn1));
                  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
                  IF xmldom.isNull(v_XMLn1) THEN
                    v_strVal := '';
                  ELSE
                    v_strVal := xmldom.getNodeValue(v_XMLn1);
                  END IF;
                  IF v_str = 'straat_met_nummer' THEN
                    x_ha_straatmetnummer := v_strVal;
                  ELSIF v_str = 'postcode' THEN
                    x_ha_postcode := v_strVal;
                  ELSIF v_str = 'woonplaats' THEN
                    x_ha_plaats := v_strVal;
                  ELSIF v_str = 'land_naam' THEN
                    null; -- niet nodig?
                  ELSE
                    toon_debug('***** GEEN MATCH (1) VOOR:');
                  END IF;
                END IF;
                toon_debug('HA-adres-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
              END LOOP;
            ELSIF v_strAtt like 'xsd:%' THEN -- verwerk huisarts-attribuut
              v_str := LOWER(xmldom.getNodeName(v_XMLn1));
              v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
              IF xmldom.isNull(v_XMLn1) THEN
                v_strVal := '';
              ELSE
                v_strVal := xmldom.getNodeValue(v_XMLn1);
              END IF;
              IF v_str = 'huisarts_code' THEN
                x_ha_code := v_strVal;
              ELSIF v_str = 'huisarts_naam' THEN
                x_ha_naam  := v_strVal;
              ELSIF v_str = 'voorvoegsels' THEN
                x_ha_voorvoegsels := v_strVal;
              ELSIF v_str = 'voorletters' THEN
                x_ha_voorletters := v_strVal;
              ELSIF v_str = 'geslacht' THEN
                x_ha_geslacht := v_strVal;
              ELSIF v_str = 'telefoonnummer' THEN
                x_ha_telefoonnummer := v_strVal;
              ELSIF v_str in ('email', 'faxnummer', 'telefoontype') THEN
                null; -- niet nodig!
              ELSE
                toon_debug('***** GEEN MATCH (2) VOOR:');
              END IF;
              toon_debug('HA-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
            END IF;
          END LOOP;
        ELSIF v_strAtt = 'ns2:verzekering' THEN -- verwerk verwerk verzekering
          v_XMLNLvz1 := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
          FOR i IN 0..xmldom.getLength(v_XMLNLvz1) - 1 LOOP
            -- verzekeraar / verzekeringsgegevens!
            v_XMLn1 := xmldom.item(v_XMLNLvz1, i);
--            v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
            v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
            IF v_strAtt = 'ns2:verzekeraar' THEN -- verwerk verzekeraar
              v_XMLNLvz2 := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
              FOR i IN 0..xmldom.getLength(v_XMLNLvz2) - 1 LOOP
                -- verzekering / adresgegevens!
                v_XMLn1 := xmldom.item(v_XMLNLvz2, i);
--                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
                IF v_strAtt = 'ns2:adres' THEN -- verwerk adres
                  v_XMLNLad := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
                  FOR i IN 0..xmldom.getLength(v_XMLNLad) - 1 LOOP
                    -- alleen adresgegevens!
                    v_XMLn1 := xmldom.item(v_XMLNLad, i);
--                    v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
                    v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
                    IF v_strAtt like 'xsd:%' THEN -- verwerk adres attribuut
                      v_str := LOWER(xmldom.getNodeName(v_XMLn1));
                      v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
                      IF xmldom.isNull(v_XMLn1) THEN
                        v_strVal := '';
                      ELSE
                        v_strVal := xmldom.getNodeValue(v_XMLn1);
                      END IF;
                      IF v_str = 'straat_met_nummer' THEN
                        x_vzk_straatmetnummer := v_strVal;
                      ELSIF v_str = 'postcode' THEN
                        x_vzk_postcode := v_strVal;
                      ELSIF v_str = 'woonplaats' THEN
                        x_vzk_plaats := v_strVal;
                      ELSIF v_str = 'land_naam' THEN
                        null; -- niet nodig?
                      ELSE
                        toon_debug('***** GEEN MATCH (3) VOOR:');
                      END IF;
                      toon_debug('VZ-adres-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
                    END IF;
                  END LOOP;
                ELSIF v_strAtt like 'xsd:%' THEN -- verwerk verzekeraar-attribuut
                  v_str := LOWER(xmldom.getNodeName(v_XMLn1));
                  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
                  IF xmldom.isNull(v_XMLn1) THEN
                    v_strVal := '';
                  ELSE
                    v_strVal := xmldom.getNodeValue(v_XMLn1);
                  END IF;
                  IF v_str = 'verzekeraar_naam' THEN
                    x_vzk_naam := v_strVal;
                  ELSIF v_str = 'verzekeraar_code' THEN
                    x_vzk_code := v_strVal;
                  ELSIF v_str = 'telefoonnummer' THEN
                    x_vzk_telefoonnummer := v_strVal;
                  ELSE
                    toon_debug('***** GEEN MATCH (4) VOOR:');
                  END IF;
                  toon_debug('VZ-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
                END IF;
              END LOOP;
            ELSIF v_strAtt like 'xsd:%' THEN -- verwerk verzekering-attribuut
              v_str := LOWER(xmldom.getNodeName(v_XMLn1));
              v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
              IF xmldom.isNull(v_XMLn1) THEN
                v_strVal := '';
              ELSE
                v_strVal := xmldom.getNodeValue(v_XMLn1);
              END IF;
              IF v_str = 'polisnummer' THEN
                x_vzk_polisnummer := v_strVal;
              ELSIF v_str = 'klasse' THEN
                x_vzk_klasse := v_strVal;
              ELSIF v_str = 'startdatum' THEN
                x_vzk_ingangsdatum := v_strVal;
              ELSIF v_str = 'verzekering_type' THEN
                x_vzk_type := v_strVal;
              ELSIF v_str in ('ligklasse') THEN
                null; -- wordt niet gebruikt
              ELSE
                toon_debug('***** GEEN MATCH (5) VOOR:');
              END IF;
              toon_debug('VZ-geg-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
            END IF;
          END LOOP;
        ELSIF v_strAtt = 'ns2:patient_demografisch' THEN -- verwerk patient-attr
          v_XMLNLpat := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
          FOR i IN 0..xmldom.getLength(v_XMLNLpat) - 1 LOOP
            -- adres / partner / persoonsattributen
            v_XMLn1 := xmldom.item(v_XMLNLpat, i);
--            v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
            v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
            IF v_strAtt = 'ns2:adres' THEN -- verwerk persoon-adres
              v_XMLNLad := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
              FOR i IN 0..xmldom.getLength(v_XMLNLad) - 1 LOOP
                -- alleen adresgegevens!
                v_XMLn1 := xmldom.item(v_XMLNLad, i);
--                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
                IF v_strAtt like 'xsd:%' THEN -- verwerk persoon-adres-attribuut
                  v_str := LOWER(xmldom.getNodeName(v_XMLn1));
                  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
                  IF xmldom.isNull(v_XMLn1) THEN
                    v_strVal := '';
                  ELSE
                    v_strVal := xmldom.getNodeValue(v_XMLn1);
                  END IF;
                  IF v_str = 'straat_met_nummer' THEN
                    x_pat_straatmetnummer := v_strVal;
                  ELSIF v_str = 'postcode' THEN
                    x_pat_postcode := v_strVal;
                  ELSIF v_str = 'woonplaats' THEN
                    x_pat_plaats:= v_strVal;
                  ELSIF v_str = 'land_naam' THEN
                    x_pat_land := TRIM(v_strVal); -- duitsland wordt voorzien van veel spaties!
                  ELSE
                    toon_debug('***** GEEN MATCH (6) VOOR:');
                  END IF;
                END IF;
                toon_debug('pat-adres-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
              END LOOP;
            ELSIF v_strAtt = 'ns2:partner' THEN -- verwerk partner
              v_XMLNLpatpn := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), '*');
              FOR i IN 0..xmldom.getLength(v_XMLNLpatpn) - 1 LOOP
                -- alleen partnergegevens!
                v_XMLn1 := xmldom.item(v_XMLNLpatpn, i);
--                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'xsi:type'));
                v_strAtt := LOWER(xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 'type', v_ns_xsi));
                IF v_strAtt like 'xsd:%' THEN -- verwerk persoon-partner-attribuut
                  v_str := LOWER(xmldom.getNodeName(v_XMLn1));
                  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
                  IF xmldom.isNull(v_XMLn1) THEN
                    v_strVal := '';
                  ELSE
                    v_strVal := xmldom.getNodeValue(v_XMLn1);
                  END IF;
                  IF v_str = 'naam' THEN
                    x_pat_partnernaam := v_strVal;
                  ELSIF v_str = 'voorvoegsels' THEN
                    x_pat_partnervoorvoegsels := v_strVal;
                  ELSE
                    toon_debug('***** GEEN MATCH (7) VOOR:');
                  END IF;
                ELSE
                  toon_debug('***** GEEN MATCH (8) VOOR:');
                END IF;
                toon_debug('pat-partner-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
              END LOOP;
            ELSE -- verwerk persoon-attribuut
              v_str := LOWER(xmldom.getNodeName(v_XMLn1));
              v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
              IF xmldom.isNull(v_XMLn1) THEN
                v_strVal := '';
              ELSE
                v_strVal := xmldom.getNodeValue(v_XMLn1);
              END IF;
              IF v_str = 'patient_registratienummer' THEN
                x_pat_registratienummer := v_strVal;
              ELSIF v_str = 'aanspreeknaam' THEN
                x_pat_aanspreeknaam := v_strVal;
              ELSIF v_str = 'patientnaam' THEN
                x_pat_naam := v_strVal;
              ELSIF v_str = 'voorvoegsels' THEN
                x_pat_voorvoegsels := v_strVal;
              ELSIF v_str = 'roepnaam' THEN
                x_pat_roepnaam := v_strVal;
              ELSIF v_str = 'voorletters' THEN
                x_pat_voorletters := v_strVal;
              ELSIF v_str = 'geslacht' THEN
                x_pat_geslacht := v_strVal;
                IF UPPER(x_pat_geslacht) NOT IN ('M', 'V', 'O') THEN -- rde 2/6/05, melding 2800 (test db)
                  x_pat_geslacht := 'O';
                END IF;
              ELSIF v_str = 'geboortedatum' THEN
                x_pat_geboortedatum := v_strVal;
              ELSIF v_str = 'telefoonnummer' THEN
                x_pat_telefoonnummer := v_strVal;
              ELSIF v_str = 'telefoontype' THEN
                x_pat_telefoontype := v_strVal;
              ELSIF v_str = 'telefoonnummer_2' THEN -- mantis 3850
                x_pat_telefoonnummer_2 := v_strVal;
              ELSIF v_str = 'partnernaam_is_aanspreeknaam' THEN
                IF UPPER(v_strVal) = 'TRUE' THEN
                  x_pat_partnernaamisaanspreekna := 'JA';
                ELSE
                  x_pat_partnernaamisaanspreekna := 'NEE';
                END IF;
              ELSIF v_str = 'overleden' THEN
                IF UPPER(v_strVal) = 'TRUE' THEN
                  x_pat_overleden := 'JA';
                ELSE
                  x_pat_overleden := 'NEE';
                END IF;
              ELSIF v_str = 'vip' THEN
                IF UPPER(v_strVal) = 'TRUE' THEN
                  x_pat_vip := 'JA';
                ELSE
                  x_pat_vip := 'NEE';
                END IF;
              ELSIF v_str = 'meerling' THEN
                IF UPPER(v_strVal) = 'TRUE' THEN
                  x_pat_meerling := 'JA';
                ELSE
                  x_pat_meerling := 'NEE';
                END IF;
              ELSIF v_str = 'overlijdensdatum' THEN
                x_pat_datumoverleden := v_strVal;
              ELSIF v_str = 'moeder_registratienummer' THEN
                x_pat_registratienummermoeder := v_strVal;
              ELSIF v_str = 'aangemeld' THEN
                IF UPPER(v_strVal) = 'TRUE' THEN
                  x_pat_aangemeld := TRUE;
                ELSE
                  x_pat_aangemeld := FALSE;
                END IF;
              ELSIF v_str = 'burgerservicenummer' THEN
                x_pat_bsn := v_strVal;
              ELSIF v_str = 'burgerservicenummer_verified' THEN
                IF UPPER(v_strVal) = 'TRUE' THEN
                  x_pat_bsn_geverifieerd := 'JA';
                ELSE
                  x_pat_bsn_geverifieerd := 'NEE';
                END IF;
              ELSIF v_str IN ('patient_registratienummer_secundair') THEN
                NULL; -- is array! en wordt niet gebruikt!
              ELSE
                toon_debug('***** GEEN MATCH (9) VOOR:' || v_str);
              END IF;
            END IF;
            toon_debug('Pat-Name: ' ||  v_str || ' type: ' || v_strAtt || ' waarde: ' || v_strVal);
          END LOOP;
        ELSE
          toon_debug('***** GEEN MATCH (11) VOOR:' || v_strAtt);
        END IF;
      END LOOP;
      EXIT; -- gegevens gelezen? Klaar.
    END IF;
    close_connection(v_c); --utl_tcp.close_all_connections;
  END LOOP; -- 2 lees-pogingen
  -- afsluiten
  close_connection(v_c); --utl_tcp.close_all_connections;
  IF NOT xmldom.isNull(v_XMLdoc) THEN
    xmldom.freeDocument(v_XMLdoc);
  END IF;
  xmlparser.freeParser(v_XMLp);
EXCEPTION
WHEN OTHERS THEN
  x_faultcode := SUBSTR(SQLCODE, 1, 255);
  x_faultstring := SUBSTR(SQLERRM, 1, 255);
  close_connection(v_c); --utl_tcp.close_all_connections;
  toon_debug('Error others; laatste attribuut=' || v_strAtt || '; waarde=' || v_strVal); -- mantis 3850
  IF NOT xmldom.isNull(v_XMLdoc) THEN
    xmldom.freeDocument(v_XMLdoc);
  END IF;
  xmlparser.freeParser(v_XMLp);
END;
PROCEDURE logout IS
BEGIN
  NULL;
END logout;
END kgc_patcomp_koppeling;
/

/
QUIT
