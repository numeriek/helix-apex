CREATE OR REPLACE PACKAGE "HELIX"."KGC_MAASTRICHT" IS
  /******************************************************************************
    NAME:      KGC_MAASTRICHT
    PURPOSE:   Algemene Maastricht-specifieke functionaliteit

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    2.0        27-07-2012  RKON             Wijziging ivm aansluiting op SAP.
    1.0        18-05-2006  ????             Creatie.
 *****************************************************************************/

-- volume voor bepaalde materialen wordt afwijkend getoond
FUNCTION volume
( p_mons_id IN number
)
RETURN VARCHAR2;

-- zet alle cc-ontvangers van een uitslagbrief in een (1) veld
-- behalve de geadresseerde
FUNCTION cc_kopiehouders
( p_uits_id IN NUMBER
, p_rela_id IN NUMBER := NULL
)
RETURN VARCHAR2;

-- Onze referentie in uitslagbrieven wordt afwijkend getoond
FUNCTION referentie
( p_onde_id IN NUMBER
, p_onderzoeknr IN varchar2
)
RETURN VARCHAR2;

-- is het protocol een zgn. TPMT-protocol
-- Stoftesten vit TPMT-protocollen van het type Reken worden vermeld in de  zgn. TPMT-eindbrief
function TPMT_protocol
( p_stgr_id in number
)
return varchar2;

-- Controleer Zisnr op Colebrander-algoritme
procedure zisnr_controle
( p_zisnr in varchar2
);

-- Is het een stoftest waarvan het protocol-resultaat toch in de eindbrief (basis) moet komen,
-- ondanks dat de stoftest in de categorie CATEGORIE_EINDBRIEF staat
-- zie kgcuits51/52mametab.rdf.
function kgcuits51_urine_stoftest
( p_stof_code in varchar2
)
return varchar2 -- J/N
;
END KGC_MAASTRICHT;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MAASTRICHT" IS
FUNCTION volume
( p_mons_id IN number
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR mons_cur
  ( b_mons_id IN number
  )
  IS
    SELECT mate.code   mate_code
    ,      mate.eenheid mate_eenheid
    ,      periode_afname
    ,      hoeveelheid_monster
    FROM   kgc_materialen mate
    ,      kgc_monsters mons
    WHERE  mate.mate_id = mons.mate_id
    AND    mons.mons_id = b_mons_id;

  mons_rec mons_cur%ROWTYPE;
  v_hoeveelheid_monster VARCHAR2(50);
BEGIN
  OPEN  mons_cur(b_mons_id => p_mons_id);
  FETCH mons_cur
  INTO  mons_rec;
  CLOSE mons_cur;

  IF ( mons_rec.mate_code = 'UP' ) -- Urine portie
  THEN
    v_return := 'Portie';
  ELSIF ( mons_rec.mate_code = 'UV' ) -- Urine verzameling
  THEN
    IF ( mons_rec.hoeveelheid_monster IS NOT NULL )
    THEN
      v_hoeveelheid_monster := kgc_formaat_00.waarde( p_waarde => mons_rec.hoeveelheid_monster
                                                    , p_aantal_decimalen => NULL
                                                    );
      v_return := v_hoeveelheid_monster || ' ' || mons_rec.mate_eenheid;
      IF ( mons_rec.periode_afname IS NOT NULL )
      THEN
        v_return := v_return||'/'||mons_rec.periode_afname;
      END IF;
    ELSE
      v_return := 'Verzameling';
    END IF;
  ELSIF ( mons_rec.mate_code is not null ) -- overige materialen
  THEN
    v_hoeveelheid_monster := kgc_formaat_00.waarde( p_waarde => mons_rec.hoeveelheid_monster
                                                  , p_aantal_decimalen => NULL
                                                  );
    v_return := v_hoeveelheid_monster;
    IF v_return IS NOT NULL
    THEN
      v_return := v_return || ' ' || mons_rec.mate_eenheid;
    END IF;
  ELSE
    v_return := NULL;
  END IF;

  RETURN ( v_return );
END volume;

FUNCTION cc_kopiehouders
( p_uits_id IN NUMBER
, p_rela_id IN NUMBER := NULL
)
RETURN  VARCHAR2
IS
  v_tekst VARCHAR2(4000) := NULL;
  CURSOR koho_cur
  IS
    SELECT koho.rela_id
    ,      rela.aanspreken rela_naam
    ,      afdg.omschrijving afdg_naam
    ,      inst.naam inst_naam
    ,      nvl( inst.woonplaats, rela.woonplaats ) woonplaats
    FROM   kgc_afdelingen afdg
    ,      kgc_instellingen inst
    ,      kgc_relaties rela
    ,      kgc_kopiehouders koho
    WHERE  koho.rela_id = rela.rela_id
    and    rela.afdg_id = afdg.afdg_id (+)
    and    rela.inst_id = inst.inst_id (+)
    and    koho.uits_id = p_uits_id
    AND    koho.rela_id <> NVL( p_rela_id, -1 )
    ;
  v_een_cc varchar2(2000);
BEGIN
  FOR koho_rec IN koho_cur
  LOOP
    v_een_cc := koho_rec.rela_naam;
    if ( koho_rec.afdg_naam is not null )
    then
      v_een_cc := v_een_cc ||' - '||koho_rec.afdg_naam;
    end if;
    if ( koho_rec.inst_naam is not null )
    then
      v_een_cc := v_een_cc ||' - '||koho_rec.inst_naam;
    end if;
    if ( koho_rec.woonplaats is not null )
    then
      v_een_cc := v_een_cc ||' - '||koho_rec.woonplaats;
    end if;

    IF ( v_tekst IS NULL )
    THEN
      v_tekst := v_een_cc;
    ELSE
      v_tekst := v_tekst||CHR(10)||v_een_cc;
    END IF;
   END LOOP;
  RETURN ( v_tekst );
END cc_kopiehouders;

FUNCTION referentie
( p_onde_id IN NUMBER
, p_onderzoeknr IN varchar2
)
RETURN VARCHAR2
IS
  v_return varchar2(100) := p_onderzoeknr;
  -- aan een E-onderzoek is een B-onderzoek gerelateerd
  -- bij B-onderzoek komt te staan: Bnr - Enr.
  -- bij E-onderzoek komt te staan: Enr.
  CURSOR reon_cur
  IS
    SELECT NVL( onde.onderzoeknr, reon.externe_onderzoeknr ) onderzoek
    FROM   kgc_onderzoeken onde
    ,      kgc_gerelateerde_onderzoeken reon
    WHERE  reon.onde_id = onde.onde_id (+)
    AND    reon.onde_id_gerelateerde = p_onde_id
    ;
BEGIN
  FOR r IN reon_cur
  LOOP
    IF ( INSTR( v_return, r.onderzoek ) = 0 )
    THEN
      v_return := v_return||' - '||r.onderzoek;
    END IF;
  END LOOP;
  return( v_return );
END referentie;

function TPMT_protocol
( p_stgr_id in number
)
return varchar2
is
  v_return varchar2(1) := 'N';
  cursor stgr_cur
  ( b_stgr_id in number
  )
  is
    select kafd_id
    ,      code
    from   kgc_stoftestgroepen
    where  stgr_id = b_stgr_id
    ;
  stgr_rec stgr_cur%rowtype;
  v_sypa varchar2(1000);
begin
  open  stgr_cur( b_stgr_id => p_stgr_id );
  fetch stgr_cur
  into  stgr_rec;
  close stgr_cur;
  begin
    v_sypa := kgc_sypa_00.standaard_waarde
              ( p_parameter_code => 'TPMT_PROTOCOLLEN'
              , p_kafd_id => stgr_rec.kafd_id
              );
  exception
    when others
    then
      v_sypa := null;
  end;
  if ( v_sypa is not null )
  then
    if ( instr( upper( v_sypa ), '#'||stgr_rec.code||'#' ) > 0 )
    then
      v_return := 'J';
    end if;
  end if;
  return ( v_return );
END tpmt_protocol;

procedure zisnr_controle
( p_zisnr in varchar2
)
is
  v_totaal NUMBER := 0;
begin
  -- Controleer Zisnr op Colebrander-algoritme
  -- RKON ivm met aansluiting op SAP uitgevinkt
 /* for i in 1..7
  loop
    v_totaal := v_totaal + mod( power( 2
                                     , ( i + to_number( substr( p_zisnr, i, 1 ) ) )
                                     )
                              , 11
                              );
  end loop;
  if ( mod( v_totaal, 10 ) <> 0 )
  then
    qms$errors.show_message
    ( p_mesg => 'KGC-00201'
    );
  end if; */
 v_totaal := 0;
end zisnr_controle;

function kgcuits51_urine_stoftest
( p_stof_code in varchar2
)
return varchar2 -- J/N
is
  v_return varchar2(1);
begin
  -- hardgecodeerd omdat het zeker nooit verandert!(?)
  if ( p_stof_code = 'AZ_AAN' )
  then
    v_return := 'J';
  else
    v_return := 'N';
  end if;
  return( v_return );
end kgcuits51_urine_stoftest;
END KGC_MAASTRICHT;
/

/
QUIT
