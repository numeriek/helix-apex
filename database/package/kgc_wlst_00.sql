CREATE OR REPLACE PACKAGE "HELIX"."KGC_WLST_00" IS
-- Package Specification
-- vul kgc_werklijsten op basis van een where-clausule (zie oa. form baswerk01)



-- zet werklijst in andere volgorde (bij gelijkheid op meet_id)
-- FRACTIE: fractienummer
-- MATERIAAL: materiaalcode
-- METING: datum aanmelding van meting
-- MONSTER: monsternummer
-- ONDERZOEK: onderzoeknr
-- PERSOON: achternaam, geboortedatum
-- PRIORITEIT: spoedmetingen en - onderzoeken eerst
-- PROTOCOL: protocolcode
-- STOFTEST: stoftestcode
-- TRAY: positie op tray (vlnr /vbnb)
-- STOF_TYPE: eerst samenstellingen, dan voortesten, dan normalen en tenslotte berekenden


-- (de-)archiveer een werklijst: de items op een werklijst worden verplaatst


-- controleer bevoegdheid om te verwijderen


-- afhankelijk van afdeling moet bepaalde informatie wel of niet worden getoond op werklijsten
-- geef extra informatie (bv. tray-positie) bij een aangemelde stoftest

-- idem, function tbv. reports
-- pragma is niet (meer) nodig!


-- retourneer een waarde waarmee gesorteerd kan worden
-- waarde is  een varchar2!!!
-- dus datum als YYYYMMDD en nummers als 0000000999



-- zorg dat vulling van tray overeenkomt met bijbehorende werklijst


-- retourneer de werklijst(en) waar een aangemelde stoftest op staat



-- onder voorwaarden kan een onderzoek op een werklijst automatisch worden afgerond
-- Voorwaarden: geen afwijkingen, geen bepaalde indicaties, geen multiple protocollen
--

--

FUNCTION MAAK_WERKLIJST
 (P_WERKLIJST_NUMMER IN VARCHAR2
 ,P_KAFD_ID IN NUMBER
 ,P_WHERE IN VARCHAR2
 ,P_SORTERING IN VARCHAR2 := NULL
 ,P_TYPE_WERKLIJST IN VARCHAR2 := 'LIST'
 ,P_COMMIT IN BOOLEAN := TRUE
 ,P_SPOED_BOVENAAN IN VARCHAR2 := 'N'
 )
 RETURN NUMBER;
PROCEDURE MAAK_WERKLIJST
 (P_WERKLIJST_NUMMER IN VARCHAR2
 ,P_KAFD_ID IN NUMBER
 ,P_WHERE IN VARCHAR2
 ,P_SORTERING IN VARCHAR2 := NULL
 ,P_TYPE_WERKLIJST IN VARCHAR2 := 'LIST'
 ,P_COMMIT IN BOOLEAN := TRUE
 ,P_SPOED_BOVENAAN IN VARCHAR2 := 'N'
 );
PROCEDURE VOLGORDE
 (P_WLST_ID IN NUMBER
 ,P_SYSTEMATIEK IN VARCHAR2 := 'INIT'
 ,P_COMMIT IN BOOLEAN := TRUE
 ,P_SPOED_BOVENAAN IN VARCHAR2 := 'N'
 );
PROCEDURE ARCHIVEER
 (P_WLST_ID IN NUMBER
 ,P_IN_UIT IN VARCHAR2 := 'IN'
 ,P_ALLEEN_ITEMS IN BOOLEAN := FALSE
 ,P_COMMIT IN BOOLEAN := TRUE
 );
FUNCTION BEVOEGD
 (P_KAFD_ID IN NUMBER
 ,P_MEDE_ID IN NUMBER := NULL
 )
 RETURN BOOLEAN;
PROCEDURE EXTRA_INFO
 (P_AFDELING IN VARCHAR2
 ,P_MEET_ID IN NUMBER
 ,P_TYPE IN VARCHAR2
 ,P_ALT_INFO IN VARCHAR2
 ,X_PROMPT OUT VARCHAR2
 ,X_WAARDE OUT VARCHAR2
 );
FUNCTION EXTRA_INFO
 (P_AFDELING IN VARCHAR2
 ,P_MEET_ID IN NUMBER
 ,P_TYPE IN VARCHAR2
 ,P_ALT_INFO IN VARCHAR2
 ,P_PROMPT IN VARCHAR2 := 'N'
 )
 RETURN VARCHAR2;
FUNCTION SORTEERWAARDE
 (P_MEET_ID IN NUMBER
 ,P_SYSTEMATIEK IN VARCHAR2
 )
 RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (SORTEERWAARDE, WNDS, WNPS);
PROCEDURE SYNC_TRAY_WERKLIJST
 (P_ACTIE IN VARCHAR2
 ,P_MEET_ID IN NUMBER
 ,P_WERKLIJST_NUMMER IN VARCHAR2 := NULL
 ,P_WLST_ID IN NUMBER := NULL
 ,P_TRGE_ID IN NUMBER := NULL
 ,P_TRVU_ID IN NUMBER := NULL
 );
FUNCTION WERKLIJST_LIJST
 (P_MEET_ID IN NUMBER := NULL
 ,P_METI_ID IN NUMBER := NULL
 )
 RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES (WERKLIJST_LIJST, WNDS, WNPS, RNPS);
PROCEDURE AUTOMATISCH_AFRONDEN
 (P_WLST_ID IN NUMBER := NULL
 ,P_ONDE_ID IN NUMBER := NULL
 ,P_MEDE_ID_AUTORISATOR IN NUMBER := NULL
 ,P_MEDE_ID_ONDERTEKENAAR IN NUMBER := NULL
 ,X_FOUT OUT BOOLEAN
 ,X_AANTAL OUT NUMBER
 ,X_MELDING OUT VARCHAR2
 );
FUNCTION ALLES_AFGEROND
 (P_WLST_ID IN KGC_WERKLIJSTEN.WLST_ID%TYPE
 )
 RETURN VARCHAR2;
END KGC_WLST_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_WLST_00" IS
/******************************************************************************
      NAME:       kgc_wlst_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        01-03-2016   SKA               Mantis 9355 TO_CHAR added to  Conclusie column
    ****************************************************************************** */
-- PL/SQL Block
-- lokale procedure: voorwaardelijke commit

--

PROCEDURE DO_COMMIT
 (P_COMMIT IN BOOLEAN := TRUE
 );
FUNCTION DO_COMMIT
 (P_COMMIT IN BOOLEAN := TRUE
 )
 RETURN VARCHAR2;


PROCEDURE DO_COMMIT
 (P_COMMIT IN BOOLEAN := TRUE
 )
 IS
BEGIN
  IF ( p_commit )
  THEN
    COMMIT;
  END IF;
END do_commit;
FUNCTION DO_COMMIT
 (P_COMMIT IN BOOLEAN := TRUE
 )
 RETURN VARCHAR2
 IS

  v_return VARCHAR2(10);
BEGIN
  IF ( p_commit )
  THEN
    v_return := 'commit;';
  END IF;
  RETURN( v_return );
END do_commit;
FUNCTION MAAK_WERKLIJST
 (P_WERKLIJST_NUMMER IN VARCHAR2
 ,P_KAFD_ID IN NUMBER
 ,P_WHERE IN VARCHAR2
 ,P_SORTERING IN VARCHAR2 := NULL
 ,P_TYPE_WERKLIJST IN VARCHAR2 := 'LIST'
 ,P_COMMIT IN BOOLEAN := TRUE
 ,P_SPOED_BOVENAAN IN VARCHAR2 := 'N'
 )
 RETURN NUMBER
 IS

  v_statement VARCHAR2(32767);
  v_where VARCHAR2(4000) := '(1=1)';
  CURSOR wlst_cur
  IS
    SELECT wlst_id
    FROM   kgc_werklijsten
    WHERE  werklijst_nummer = p_werklijst_nummer
    ;
  v_wlst_id NUMBER;
BEGIN
  IF ( p_werklijst_nummer IS NULL
    OR p_kafd_id IS NULL
     )
  THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_param0 => 'Verplichte parameter(s) ontbreken'
    , p_param1 => 'Interne id voor kgc_afdeling en werklijstnummer zijn verplicht'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
  -- vul where-clausule aan
  IF ( p_where IS NOT NULL )
  THEN
    v_where := v_where||CHR(10)||' and '||LTRIM(LTRIM(LTRIM(p_where),'and'),'where');
  END IF;

  OPEN  wlst_cur;
  FETCH wlst_cur
  INTO  v_wlst_id;
  CLOSE wlst_cur;
  IF ( v_wlst_id IS NULL )
  THEN
    SELECT kgc_wlst_seq.nextval
    INTO   v_wlst_id
    FROM dual
    ;
  END IF;
  v_statement := 'declare'
     ||CHR(10)|| ' v_datum date := trunc(sysdate);'
     ||CHR(10)|| ' v_user varchar2(30) := user;'
     ||CHR(10)|| 'wlst_row kgc_werklijsten%rowtype;'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'wlst_row.wlst_id := '||TO_CHAR(v_wlst_id)||';'
     ||CHR(10)|| 'wlst_row.kafd_id := '||TO_CHAR(p_kafd_id)||';'
     ||CHR(10)|| 'wlst_row.werklijst_nummer := '''||p_werklijst_nummer||''';'
     ||CHR(10)|| 'wlst_row.type_werklijst := '''||NVL(p_type_werklijst,'LIST')||''';'
     ||CHR(10)|| 'wlst_row.gesorteerd := '''||p_sortering||''';'
     ||CHR(10)|| 'wlst_row.spoed_bovenaan := '''||p_spoed_bovenaan||''';'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'insert into kgc_werklijsten'
     ||CHR(10)|| '( wlst_id'
     ||CHR(10)|| ', kafd_id'
     ||CHR(10)|| ', werklijst_nummer'
     ||CHR(10)|| ', type_werklijst'
     ||CHR(10)|| ', oracle_uid'
     ||CHR(10)|| ', datum'
     ||CHR(10)|| ', archief'
     ||CHR(10)|| ', gesorteerd'
     ||CHR(10)|| ', spoed_bovenaan'
     ||CHR(10)|| ')'
     ||CHR(10)|| 'values'
     ||CHR(10)|| '( wlst_row.wlst_id'
     ||CHR(10)|| ', wlst_row.kafd_id'
     ||CHR(10)|| ', wlst_row.werklijst_nummer'
     ||CHR(10)|| ', wlst_row.type_werklijst'
     ||CHR(10)|| ', v_user'
     ||CHR(10)|| ', v_datum'
     ||CHR(10)|| ', ''N'''
     ||CHR(10)|| ', wlst_row.gesorteerd'
     ||CHR(10)|| ', wlst_row.spoed_bovenaan'
     ||CHR(10)|| ');'
     ||CHR(10)|| 'exception when dup_val_on_index then null;'
     ||CHR(10)|| 'when others then raise;'
     ||CHR(10)|| 'end;'
     ||CHR(10)|| do_commit( p_commit )
     ||CHR(10)|| 'exception when others then'
     ||CHR(10)|| 'raise_application_error( -20007, ''Werklijst niet aangemaakt!''||chr(10)||sqlerrm );'
     ||CHR(10)|| 'end;'
               ;
  BEGIN
    kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message
      (p_mesg => 'KGC-00000'
      ,p_param0 => 'Fout bij aanmaken werklijst!'
      ,p_param1 => SQLERRM
      ,p_errtp => 'E'
      ,p_rftf => TRUE
      );
  END;
  v_statement := 'declare'
     ||CHR(10)|| ' v_volgnr number;'
     ||CHR(10)|| 'cursor c is'
     ||CHR(10)|| 'select meet_id'
     ||CHR(10)|| ',stof_id'
     ||CHR(10)|| ',mons_id'
     ||CHR(10)|| ',ongr_id'
     ||CHR(10)|| ',mest_id'
     ||CHR(10)|| ',mede_id_controle'
     ||CHR(10)|| ',meetwaarde'
     ||CHR(10)|| ',meet_commentaar'
     ||CHR(10)|| 'from kgc_select_meet_1_vw'
     ||CHR(10)|| 'where kafd_id = '||TO_CHAR( p_kafd_id )
     ||CHR(10)|| 'and '||v_where
     ||CHR(10)|| 'order by spoed,meet_id'
     ||CHR(10)|| ';'
     ||CHR(10)|| 'wlit_row kgc_werklijst_items%rowtype;'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'wlit_row.wlst_id := '||TO_CHAR(v_wlst_id)||';'
     ||CHR(10)|| 'select nvl(max(volgnummer),0)'
     ||CHR(10)|| 'into v_volgnr'
     ||CHR(10)|| 'from kgc_werklijst_items'
     ||CHR(10)|| 'where wlst_id = wlit_row.wlst_id;'
     ||CHR(10)|| 'for r in c loop'
     ||CHR(10)|| 'v_volgnr := v_volgnr + 1;'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'insert into kgc_werklijst_items'
     ||CHR(10)|| '( wlst_id'
     ||CHR(10)|| ', volgnummer'
     ||CHR(10)|| ', meet_id'
     ||CHR(10)|| ', stof_id'
     ||CHR(10)|| ', ongr_id'
     ||CHR(10)|| ', mons_id'
     ||CHR(10)|| ', mest_id'
     ||CHR(10)|| ', mede_id_controle'
     ||CHR(10)|| ', meetwaarde'
     ||CHR(10)|| ', afgerond'
     ||CHR(10)|| ', commentaar'
     ||CHR(10)|| ')'
     ||CHR(10)|| 'values'
     ||CHR(10)|| '( wlit_row.wlst_id'
     ||CHR(10)|| ', v_volgnr'
     ||CHR(10)|| ', r.meet_id'
     ||CHR(10)|| ', r.stof_id'
     ||CHR(10)|| ', r.ongr_id'
     ||CHR(10)|| ', r.mons_id'
     ||CHR(10)|| ', r.mest_id'
     ||CHR(10)|| ', r.mede_id_controle'
     ||CHR(10)|| ', r.meetwaarde'
     ||CHR(10)|| ', ''N'''
     ||CHR(10)|| ', r.meet_commentaar'
     ||CHR(10)|| ');'
     ||CHR(10)|| 'exception when dup_val_on_index then null;'
     ||CHR(10)|| 'v_volgnr := v_volgnr - 1;'
     ||CHR(10)|| 'when others then raise;'
     ||CHR(10)|| 'end;'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| do_commit( p_commit )
     ||CHR(10)|| 'exception when others then'
     ||CHR(10)|| 'raise_application_error( -20007, ''Werklijst-items 1 niet aangemaakt!''||chr(10)||sqlerrm );'
     ||CHR(10)|| 'end;'
               ;
  BEGIN
    kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message
      (p_mesg => 'KGC-00000'
      ,p_param0 => 'Fout bij aanmaken werklijst!'
      ,p_param1 => SQLERRM
      ,p_errtp => 'E'
      ,p_rftf => TRUE
      );
  END;
  v_statement := 'declare'
     ||CHR(10)|| 'v_volgnr number;'
     ||CHR(10)|| 'cursor c is'
     ||CHR(10)|| 'select meet_id'
     ||CHR(10)|| ',stof_id'
     ||CHR(10)|| ',ongr_id'
     ||CHR(10)|| ',mest_id'
     ||CHR(10)|| ',mede_id_controle'
     ||CHR(10)|| ',meetwaarde'
     ||CHR(10)|| ',meet_commentaar'
     ||CHR(10)|| 'from kgc_select_meet_2_vw'
     ||CHR(10)|| 'where kafd_id = '||TO_CHAR( p_kafd_id )
     ||CHR(10)|| 'and '||v_where
     ||CHR(10)|| 'order by spoed, meet_id'
     ||CHR(10)|| ';'
     ||CHR(10)|| 'wlit_row kgc_werklijst_items%rowtype;'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'wlit_row.wlst_id := '||TO_CHAR(v_wlst_id)||';'
     ||CHR(10)|| 'select nvl(max(volgnummer),0)'
     ||CHR(10)|| 'into v_volgnr'
     ||CHR(10)|| 'from kgc_werklijst_items'
     ||CHR(10)|| 'where wlst_id = wlit_row.wlst_id;'
     ||CHR(10)|| 'for r in c loop'
     ||CHR(10)|| 'v_volgnr := v_volgnr + 1;'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'insert into kgc_werklijst_items'
     ||CHR(10)|| '( wlst_id'
     ||CHR(10)|| ', volgnummer'
     ||CHR(10)|| ', meet_id'
     ||CHR(10)|| ', stof_id'
     ||CHR(10)|| ', ongr_id'
     ||CHR(10)|| ', mest_id'
     ||CHR(10)|| ', mede_id_controle'
     ||CHR(10)|| ', meetwaarde'
     ||CHR(10)|| ', afgerond'
     ||CHR(10)|| ', commentaar'
     ||CHR(10)|| ')'
     ||CHR(10)|| 'values'
     ||CHR(10)|| '( wlit_row.wlst_id'
     ||CHR(10)|| ', v_volgnr'
     ||CHR(10)|| ', r.meet_id'
     ||CHR(10)|| ', r.stof_id'
     ||CHR(10)|| ', r.ongr_id'
     ||CHR(10)|| ', r.mest_id'
     ||CHR(10)|| ', r.mede_id_controle'
     ||CHR(10)|| ', r.meetwaarde'
     ||CHR(10)|| ', ''N'''
     ||CHR(10)|| ', r.meet_commentaar'
     ||CHR(10)|| ');'
     ||CHR(10)|| 'exception when dup_val_on_index then null;'
     ||CHR(10)|| 'v_volgnr := v_volgnr - 1;'
     ||CHR(10)|| 'when others then raise;'
     ||CHR(10)|| 'end;'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| do_commit( p_commit )
     ||CHR(10)|| 'exception when others then'
     ||CHR(10)|| 'raise_application_error( -20007, ''Werklijst-items 2 niet aangemaakt!''||chr(10)||sqlerrm );'
     ||CHR(10)|| 'end;'
               ;
  BEGIN
    kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message
      (p_mesg => 'KGC-00000'
      ,p_param0 => 'Fout bij aanmaken werklijst!'
      ,p_param1 => SQLERRM
      ,p_errtp => 'E'
      ,p_rftf => TRUE
      );
  END;
  IF ( p_sortering IS NOT NULL )
  THEN
    volgorde
    ( p_wlst_id => v_wlst_id
    , p_systematiek => p_sortering
    , p_commit => p_commit
    , p_spoed_bovenaan => p_spoed_bovenaan
    );
  END IF;
  RETURN( v_wlst_id );
END maak_werklijst;
PROCEDURE MAAK_WERKLIJST
 (P_WERKLIJST_NUMMER IN VARCHAR2
 ,P_KAFD_ID IN NUMBER
 ,P_WHERE IN VARCHAR2
 ,P_SORTERING IN VARCHAR2 := NULL
 ,P_TYPE_WERKLIJST IN VARCHAR2 := 'LIST'
 ,P_COMMIT IN BOOLEAN := TRUE
 ,P_SPOED_BOVENAAN IN VARCHAR2 := 'N'
 )
 IS

  v_wlst_id NUMBER;
BEGIN
  v_wlst_id := maak_werklijst
               ( p_werklijst_nummer => p_werklijst_nummer
               , p_kafd_id => p_kafd_id
               , p_where => p_where
               , p_sortering => p_sortering
               , p_type_werklijst => p_type_werklijst
               , p_commit => p_commit
               , p_spoed_bovenaan => p_spoed_bovenaan
               );
END maak_werklijst;
PROCEDURE VOLGORDE
 (P_WLST_ID IN NUMBER
 ,P_SYSTEMATIEK IN VARCHAR2 := 'INIT'
 ,P_COMMIT IN BOOLEAN := TRUE
 ,P_SPOED_BOVENAAN IN VARCHAR2 := 'N'
 )
 IS

  -- initieel/reset
  CURSOR wlit0_cur
  IS
    SELECT  wlit.meet_id
    FROM    kgc_werklijst_items wlit
    WHERE   wlit.wlst_id = p_wlst_id
    ORDER BY wlit.meet_id
    ;

  CURSOR wlit_cur
  ( b_spoed_bovenaan VARCHAR2
  , b_wlst_id NUMBER
  , b_systematiek VARCHAR2
  )
  IS
    SELECT  wlit.meet_id
    ,       decode( b_spoed_bovenaan
                       , 'J', bas_meet_00.meetwaarde_spoed( wlit.meet_id )
                                  , 'N'
                              )
            || sorteerwaarde( wlit.meet_id, b_systematiek ) sorteer
    FROM    kgc_werklijst_items wlit
    WHERE   wlit.wlst_id = b_wlst_id
    ORDER BY 2, 1
    ;

  v_volgnummer NUMBER(5,0) := 0;

  PROCEDURE zet_vnr
  ( b_meet_id IN NUMBER
  , b_volgnummer IN NUMBER
  )
  IS
    v_dummy VARCHAR2(1);
  BEGIN
    UPDATE kgc_werklijst_items
    SET    volgnummer = b_volgnummer
    WHERE  wlst_id = p_wlst_id
    AND    meet_id = b_meet_id
    ;
  EXCEPTION
    WHEN dup_val_on_index
    THEN
      zet_vnr( b_meet_id, b_volgnummer + 1 );
    WHEN OTHERS
    THEN
      NULL;
  END;
  PROCEDURE  reset
  IS
    CURSOR c
    IS
      SELECT volgnummer
      FROM   kgc_werklijst_items
      WHERE  wlst_id = p_wlst_id
      FOR UPDATE OF volgnummer nowait
      ;
  BEGIN
    FOR r IN c
    LOOP
      UPDATE kgc_werklijst_items
      SET volgnummer = volgnummer + 5000
      WHERE CURRENT OF c;
    END LOOP;
  END reset;
BEGIN
  IF ( p_systematiek = 'INIT' )
  THEN
    reset;
    FOR r IN wlit0_cur
    LOOP
      v_volgnummer := v_volgnummer + 1;
      zet_vnr( r.meet_id, v_volgnummer );
    END LOOP;
  ELSE
    reset;
    FOR r IN wlit_cur( b_spoed_bovenaan => p_spoed_bovenaan
                     , b_wlst_id => p_wlst_id
                     , b_systematiek => p_systematiek )
    LOOP
      v_volgnummer := v_volgnummer + 1;
      zet_vnr( r.meet_id, v_volgnummer );
    END LOOP;
  END IF;
  do_commit( p_commit );
END volgorde;
PROCEDURE ARCHIVEER
 (P_WLST_ID IN NUMBER
 ,P_IN_UIT IN VARCHAR2 := 'IN'
 ,P_ALLEEN_ITEMS IN BOOLEAN := FALSE
 ,P_COMMIT IN BOOLEAN := TRUE
 )
 IS

  CURSOR wlit_cur
  IS
    SELECT wlst_id
    ,      volgnummer
    ,      meet_id
    ,      afgerond
    ,      stof_id
    ,      ongr_id
    ,      mons_id
    ,      mede_id_controle
    ,      mest_id
    ,      meetwaarde
    ,      commentaar
    FROM   kgc_werklijst_items
    WHERE  wlst_id = p_wlst_id
    ;
  CURSOR wlia_cur
  IS
    SELECT wlst_id
    ,      volgnummer
    ,      meet_id
    ,      afgerond
    ,      stof_id
    ,      ongr_id
    ,      mons_id
    ,      mede_id_controle
    ,      mest_id
    ,      meetwaarde
    ,      commentaar
    FROM   kgc_werklijst_items_archief
    WHERE  wlst_id = p_wlst_id
    ;
BEGIN
  IF ( p_in_uit = 'IN' )
  THEN
    BEGIN
      IF ( NOT p_alleen_items )
      THEN
        UPDATE kgc_werklijsten
        SET    archief = 'J'
        WHERE  wlst_id = p_wlst_id
        ;
      END IF;
      FOR r IN wlit_cur
      LOOP
        BEGIN
          INSERT INTO kgc_werklijst_items_archief
          ( wlst_id
          , volgnummer
          , meet_id
          , afgerond
          , stof_id
          , ongr_id
          , mons_id
          , mede_id_controle
          , mest_id
          , meetwaarde
          , commentaar
          )
          VALUES
          ( r.wlst_id
          , r.volgnummer
          , r.meet_id
          , r.afgerond
          , r.stof_id
          , r.ongr_id
          , r.mons_id
          , r.mede_id_controle
          , r.mest_id
          , r.meetwaarde
          , r.commentaar
          );
        END;
      END LOOP;
      DELETE FROM kgc_werklijst_items
      WHERE wlst_id = p_wlst_id;
    END;
  ELSIF ( p_in_uit = 'UIT' )
  THEN
    BEGIN
      IF ( NOT p_alleen_items )
      THEN
        UPDATE kgc_werklijsten
        SET    archief = 'N'
        WHERE  wlst_id = p_wlst_id
        ;
      END IF;
      FOR r IN wlia_cur
      LOOP
        BEGIN
          INSERT INTO kgc_werklijst_items
          ( wlst_id
          , volgnummer
          , meet_id
          , afgerond
          , stof_id
          , ongr_id
          , mons_id
          , mede_id_controle
          , mest_id
          , meetwaarde
          , commentaar
          )
          VALUES
          ( r.wlst_id
          , r.volgnummer
          , r.meet_id
          , r.afgerond
          , r.stof_id
          , r.ongr_id
          , r.mons_id
          , r.mede_id_controle
          , r.mest_id
          , r.meetwaarde
          , r.commentaar
          );
        END;
      END LOOP;
      DELETE FROM kgc_werklijst_items_archief
      WHERE wlst_id = p_wlst_id;
    END;
  END IF;
  do_commit( p_commit );
END archiveer;
FUNCTION BEVOEGD
 (P_KAFD_ID IN NUMBER
 ,P_MEDE_ID IN NUMBER := NULL
 )
 RETURN BOOLEAN
 IS

  v_return BOOLEAN := TRUE;
  CURSOR spwa_cur
  ( b_mede_id IN NUMBER
  )
  IS
    SELECT NULL
    FROM   kgc_systeem_par_waarden spwa
    ,      kgc_systeem_parameters sypa
    WHERE  sypa.sypa_id = spwa.sypa_id
    AND    sypa.code = 'BEVOEGD_VERWIJDER_WERKLIJST'
    AND    spwa.kafd_id = p_kafd_id
    AND  ( spwa.mede_id = b_mede_id
        OR b_mede_id IS NULL
         )
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  spwa_cur( NULL);
  FETCH spwa_cur
  INTO  v_dummy;
  IF ( spwa_cur%found )
  THEN
    CLOSE spwa_cur;
    OPEN  spwa_cur( NVL( p_mede_id, kgc_mede_00.medewerker_id ) );
    FETCH spwa_cur
    INTO  v_dummy;
    IF ( spwa_cur%notfound )
    THEN
      CLOSE spwa_cur;
      v_return := FALSE;
    ELSE
      CLOSE spwa_cur;
      v_return := TRUE;
    END IF;
  ELSE
    CLOSE spwa_cur;
    v_return := TRUE;
  END IF;
  RETURN( v_return );
END bevoegd;
PROCEDURE EXTRA_INFO
 (P_AFDELING IN VARCHAR2
 ,P_MEET_ID IN NUMBER
 ,P_TYPE IN VARCHAR2
 ,P_ALT_INFO IN VARCHAR2
 ,X_PROMPT OUT VARCHAR2
 ,X_WAARDE OUT VARCHAR2
 )
 IS

  v_gebruik_tray VARCHAR2(1);
  v_gebruik_familie VARCHAR2(1);
  v_alt_info VARCHAR2(100) := UPPER( p_alt_info );

  FUNCTION monster
  RETURN VARCHAR2
  IS
    l_return VARCHAR2(100);
    CURSOR c
    IS
      SELECT mons.monsternummer
      FROM   kgc_monsters mons
      ,      bas_fracties frac
      ,      bas_metingen meti
      ,      bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.frac_id = frac.frac_id
      AND    frac.mons_id = mons.mons_id
      AND    meet.meet_id = p_meet_id
      ;
  BEGIN
    IF ( p_meet_id IS NOT NULL )
    THEN
      OPEN  c;
      FETCH c
      INTO  l_return;
      CLOSE c;
    END IF;
    RETURN( l_return );
  END monster;

  FUNCTION onderzoek
  RETURN VARCHAR2
  IS
    l_return VARCHAR2(100);
    CURSOR c
    IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
      ,      bas_metingen meti
      ,      bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = onde.onde_id
      AND    meet.meet_id = p_meet_id
      ;
  BEGIN
    IF ( p_meet_id IS NOT NULL )
    THEN
      OPEN  c;
      FETCH c
      INTO  l_return;
      CLOSE c;
    END IF;
    RETURN( l_return );
  END onderzoek;

  FUNCTION familie
  RETURN VARCHAR2
  IS
    l_return VARCHAR2(100);
    CURSOR c
    IS
      SELECT kgc_fami_00.familie_bij_onderzoek( meti.onde_id ) familie
      FROM   bas_metingen meti
      ,      bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meet.meet_id = p_meet_id
      ;
  BEGIN
    IF ( p_meet_id IS NOT NULL )
    THEN
      OPEN  c;
      FETCH c
      INTO  l_return;
      CLOSE c;
    END IF;
    RETURN( l_return );
  END familie;

  FUNCTION protocol
  RETURN VARCHAR2
  IS
    l_return VARCHAR2(100);
    CURSOR c
    IS
      SELECT stgr.code
      FROM   kgc_stoftestgroepen stgr
      ,      bas_metingen meti
      ,      bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.stgr_id = stgr.stgr_id
      AND    meet.meet_id = p_meet_id
      ;
  BEGIN
    IF ( p_meet_id IS NOT NULL )
    THEN
      OPEN  c;
      FETCH c
      INTO  l_return;
      CLOSE c;
    END IF;
    RETURN( l_return );
  END protocol;

  PROCEDURE info
  ( b_info IN VARCHAR2
  )
  IS
  BEGIN
    IF ( b_info = 'MONSTER' )
    THEN
      x_prompt := 'Monster';
      IF ( p_meet_id IS NOT NULL )
      THEN
        x_waarde := monster;
      END IF;
    ELSIF ( b_info = 'ONDERZOEK' )
    THEN
      x_prompt := 'Onderzoek';
      IF ( p_meet_id IS NOT NULL )
      THEN
        x_waarde := onderzoek;
      END IF;
    ELSIF ( b_info = 'FAMILIE' )
    THEN
      x_prompt := 'Familie';
      IF ( p_meet_id IS NOT NULL )
      THEN
        x_waarde := familie;
      END IF;
    ELSIF ( b_info = 'PROTOCOL' )
    THEN
      x_prompt := 'Protocol';
      IF ( p_meet_id IS NOT NULL )
      THEN
        x_waarde := protocol;
      END IF;
    ELSIF ( b_info = 'TRAYPOSITIE' )
    THEN
      x_prompt := 'Positie';
      IF ( p_meet_id IS NOT NULL )
      THEN
        x_waarde := kgc_trge_00.positie( p_meet_id => p_meet_id );
      END IF;
    ELSE
      x_waarde := p_alt_info;
    END IF;
  END info;
BEGIN
  IF ( p_type = 'TRAY' )
  THEN
    v_gebruik_tray := UPPER( SUBSTR( kgc_sypa_00.standaard_waarde
                                     ( p_parameter_code => 'GEBRUIK_TRAY'
                                     , p_afdeling => p_afdeling
                                     )
                                   , 1, 1
                                   )
                           );
    IF ( v_gebruik_tray = 'J' )
    THEN
      info( 'TRAYPOSITIE' );
    ELSE
      info( v_alt_info );
      IF ( x_waarde IS NULL )
      THEN
        x_waarde := 'Controle';
      END IF;
    END IF;
  ELSIF ( p_type = 'FAMILIE' )
  THEN
    v_gebruik_familie := UPPER( SUBSTR( kgc_sypa_00.standaard_waarde
                                        ( p_parameter_code => 'GEBRUIK_FAMILIE'
                                        , p_afdeling => p_afdeling
                                        )
                                      , 1, 1
                                      )
                              );
    IF ( v_gebruik_familie = 'J' )
    THEN
      info( 'FAMILIE' );
    ELSE
      info( v_alt_info );
    END IF;
  ELSE
    info( v_alt_info );
  END IF;
END extra_info;
FUNCTION EXTRA_INFO
 (P_AFDELING IN VARCHAR2
 ,P_MEET_ID IN NUMBER
 ,P_TYPE IN VARCHAR2
 ,P_ALT_INFO IN VARCHAR2
 ,P_PROMPT IN VARCHAR2 := 'N'
 )
 RETURN VARCHAR2
 IS

  v_prompt VARCHAR2(1000);
  v_waarde VARCHAR2(1000);
BEGIN
  extra_info
  ( p_afdeling => p_afdeling
  , p_meet_id => p_meet_id
  , p_type => p_type
  , p_alt_info => p_alt_info
  , x_prompt => v_prompt
  , x_waarde => v_waarde
  );
  IF ( p_prompt = 'J' )
  THEN
    RETURN( v_prompt );
  ELSE
    RETURN( v_waarde );
  END IF;
END extra_info;
FUNCTION SORTEERWAARDE
 (P_MEET_ID IN NUMBER
 ,P_SYSTEMATIEK IN VARCHAR2
 )
 RETURN VARCHAR2
 IS

  v_return VARCHAR2(1000);
BEGIN
  v_return := kgc_sorteren.sorteerwaarde
              ( p_meet_id => p_meet_id
              , p_systematiek => UPPER( p_systematiek )
              );
  RETURN( v_return );
END sorteerwaarde;
PROCEDURE SYNC_TRAY_WERKLIJST
 (P_ACTIE IN VARCHAR2
 ,P_MEET_ID IN NUMBER
 ,P_WERKLIJST_NUMMER IN VARCHAR2 := NULL
 ,P_WLST_ID IN NUMBER := NULL
 ,P_TRGE_ID IN NUMBER := NULL
 ,P_TRVU_ID IN NUMBER := NULL
 )
 IS

  CURSOR trge_cur
  IS
    SELECT werklijst_nummer
    FROM   kgc_tray_gebruik
    WHERE  trge_id = p_trge_id
    ;
  CURSOR trvu_cur
  IS
    SELECT trge.werklijst_nummer
    FROM   kgc_tray_gebruik trge
    ,      kgc_tray_vulling trvu
    WHERE  trge.trge_id = trvu.trge_id
    AND    trvu.trvu_id = p_trvu_id
    ;
  CURSOR wlst_cur
  ( b_werklijst_nummer IN VARCHAR2
  )
  IS
    SELECT wlst_id
    ,      werklijst_nummer -- tbv. declaratie variable
    FROM   kgc_werklijsten
    WHERE  werklijst_nummer = b_werklijst_nummer
    ;
  wlst_rec wlst_cur%rowtype;
  CURSOR meet_cur
  IS
    SELECT meet.afgerond
    ,      mons.kafd_id
    ,      mons.ongr_id
    ,      frac.mons_id
    ,      meet.stof_id
    ,      meet.meetwaarde
    ,      meet.mede_id_controle
    ,      meet.mest_id
    ,      meet.commentaar
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    meti.frac_id IS NOT NULL
    AND    meet.meet_id = p_meet_id
    UNION
    SELECT meet.afgerond
    ,      ongr.kafd_id
    ,      stan.ongr_id
    ,      TO_NUMBER(NULL)
    ,      meet.stof_id
    ,      meet.meetwaarde
    ,      meet.mede_id_controle
    ,      meet.mest_id
    ,      meet.commentaar
    FROM   kgc_onderzoeksgroepen ongr
    ,      bas_standaardfracties stan
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.stan_id = stan.stan_id
    AND    stan.ongr_id = ongr.ongr_id
    AND    meti.stan_id IS NOT NULL
    AND    meet.meet_id = p_meet_id
    ;
  meet_rec     meet_cur%rowtype;
  v_volgnummer NUMBER;
  v_user       VARCHAR2(40) := USER;
BEGIN
-- procedure is (nog) niet volledig
-- ondersteund wordt: AR-trg op TRVU
  IF ( p_meet_id IS NULL
    OR p_actie IS NULL
     )
  THEN
    RETURN;
  END IF;

  IF ( p_wlst_id IS NOT NULL )
  THEN
    wlst_rec.wlst_id := p_wlst_id;
  ELSE
    IF ( p_werklijst_nummer IS NOT NULL )
    THEN
      wlst_rec.werklijst_nummer := p_werklijst_nummer;
    ELSIF ( p_trge_id IS NOT NULL )
    THEN
      OPEN  trge_cur;
      FETCH trge_cur
      INTO  wlst_rec.werklijst_nummer;
      CLOSE trge_cur;
    ELSIF ( p_trvu_id IS NOT NULL )
    THEN
      OPEN  trvu_cur;
      FETCH trvu_cur
      INTO  wlst_rec.werklijst_nummer;
      CLOSE trvu_cur;
    END IF;
    IF ( wlst_rec.werklijst_nummer IS NOT NULL )
    THEN
      OPEN  wlst_cur( wlst_rec.werklijst_nummer );
      FETCH wlst_cur
      INTO  wlst_rec;
      CLOSE wlst_cur;
    END IF;
  END IF;
  -- wlst_id is nu bepaald

  OPEN  meet_cur;
  FETCH meet_cur
  INTO  meet_rec;
  CLOSE meet_cur;

  IF ( UPPER( p_actie ) = 'I' )
  THEN
    IF ( wlst_rec.wlst_id IS NULL )
    THEN
      IF ( wlst_rec.werklijst_nummer IS NOT NULL )
      THEN
        SELECT kgc_wlst_seq.nextval
        INTO   wlst_rec.wlst_id
        FROM   dual;
        BEGIN
          INSERT INTO kgc_werklijsten
          ( wlst_id
          , kafd_id
          , werklijst_nummer
          , type_werklijst
          , oracle_uid
          , datum
          , archief
          , gesorteerd
          )
          VALUES
          ( wlst_rec.wlst_id
          , meet_rec.kafd_id
          , wlst_rec.werklijst_nummer
          , 'LIST'
          , v_user
          , trunc(sysdate)
          , 'N'
          , 'TRAY_LR'
          );
        END;
      ELSE
        -- werklijstnummer onbekend
        RETURN;
      END IF;
    END IF;

    SELECT NVL( MAX( volgnummer ), 0 ) + 1
    INTO   v_volgnummer
    FROM   kgc_werklijst_items
    WHERE  wlst_id = wlst_rec.wlst_id
    ;
    BEGIN
      INSERT INTO kgc_werklijst_items
      ( wlst_id
      , meet_id
      , afgerond
      , volgnummer
      , stof_id
      , ongr_id
      , mons_id
      , mede_id_controle
      , mest_id
      , meetwaarde
      , commentaar
      )
      VALUES
      ( wlst_rec.wlst_id
      , p_meet_id
      , meet_rec.afgerond
      , v_volgnummer
      , meet_rec.stof_id
      , meet_rec.ongr_id
      , meet_rec.mons_id
      , meet_rec.mede_id_controle
      , meet_rec.mest_id
      , meet_rec.meetwaarde
      , meet_rec.commentaar
      );
    EXCEPTION
      WHEN dup_val_on_index
      THEN
        NULL;
    END;
  ELSIF ( UPPER( p_actie ) = 'D' )
  THEN
    BEGIN
      DELETE FROM kgc_werklijst_items
      WHERE  wlst_id = wlst_rec.wlst_id
      AND    meet_id = p_meet_id
      ;
    END;
  END IF;
END sync_tray_werklijst;
FUNCTION WERKLIJST_LIJST
 (P_MEET_ID IN NUMBER := NULL
 ,P_METI_ID IN NUMBER := NULL
 )
 RETURN VARCHAR2
 IS

  v_return VARCHAR2(2000);
  -- Wijziging 26-09-2003: niet alleen actueel, maar ook gearchiveerd (probleem 2231)
  CURSOR meet_cur
  IS
    SELECT wlst.werklijst_nummer
    FROM   kgc_werklijsten wlst
    ,      kgc_werklijst_items wlit
    WHERE  wlst.wlst_id = wlit.wlst_id
    AND    wlit.meet_id = p_meet_id
    UNION
    SELECT wlst.werklijst_nummer
    FROM   kgc_werklijsten wlst
    ,      kgc_werklijst_items_archief wlia
    WHERE  wlst.wlst_id = wlia.wlst_id
    AND    wlia.meet_id = p_meet_id
    ;
  CURSOR meti_cur
  IS
    SELECT wlst.werklijst_nummer
    FROM   kgc_werklijsten wlst
    ,      kgc_werklijst_items wlit
    ,      bas_meetwaarden meet
    WHERE  wlst.wlst_id = wlit.wlst_id
    AND    wlit.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    UNION
    SELECT wlst.werklijst_nummer
    FROM   kgc_werklijsten wlst
    ,      kgc_werklijst_items_archief wlia
    ,      bas_meetwaarden meet
    WHERE  wlst.wlst_id = wlia.wlst_id
    AND    wlia.meet_id = meet.meet_id
    AND    meet.meti_id = p_meti_id
    ;
BEGIN
  IF ( p_meet_id IS NOT NULL )
  THEN
    FOR r IN meet_cur
    LOOP
      IF ( v_return IS NULL )
      THEN
        v_return := r.werklijst_nummer;
      ELSIF ( INSTR( v_return, r.werklijst_nummer ) = 0 )
      THEN
        v_return := v_return||'; '||r.werklijst_nummer;
      END IF;
    END LOOP;
  ELSIF ( p_meti_id IS NOT NULL )
  THEN
    FOR r IN meti_cur
    LOOP
      IF ( v_return IS NULL )
      THEN
        v_return := r.werklijst_nummer;
      ELSIF ( INSTR( v_return, r.werklijst_nummer ) = 0 )
      THEN
        v_return := v_return||'; '||r.werklijst_nummer;
      END IF;
    END LOOP;
  END IF;
  RETURN( v_return );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( NULL );
END werklijst_lijst;
PROCEDURE AUTOMATISCH_AFRONDEN
 (P_WLST_ID IN NUMBER := NULL
 ,P_ONDE_ID IN NUMBER := NULL
 ,P_MEDE_ID_AUTORISATOR IN NUMBER := NULL
 ,P_MEDE_ID_ONDERTEKENAAR IN NUMBER := NULL
 ,X_FOUT OUT BOOLEAN
 ,X_AANTAL OUT NUMBER
 ,X_MELDING OUT VARCHAR2
 )
 IS

  -- allen onderzoeken zonder monsters met meerdere protocollen
  --                   zonder afwijkende indicaties
  --                   zonder afwijkende meetwaarden (leeg is nog niet goed)
  CURSOR onde_cur
  IS
    SELECT onde.onde_id
    ,      onde.kafd_id
    ,      onde.ongr_id
    ,      onde.rela_id
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_onderzoeken onde
    ,      kgc_werklijst_items wlit
    WHERE  onde.afgerond = 'N'
    AND    onde.onde_id = meti.onde_id
    AND    meti.meti_id = meet.meti_id
    AND    meet.meet_id = wlit.meet_id
    AND  ( wlit.wlst_id = p_wlst_id OR p_wlst_id IS NULL )
    AND  ( onde.onde_id = p_onde_id OR p_onde_id IS NULL )
    AND    NOT EXISTS
             ( SELECT NULL
               FROM   kgc_onderzoek_monsters onmo
               WHERE  onmo.onde_id = onde.onde_id
               AND    onmo.mons_id = wlit.mons_id
               AND    kgc_mons_00.meerdere_protocollen( onmo.mons_id ) = 'J'
             )
    AND    NOT EXISTS
           ( SELECT NULL
             FROM   kgc_indicatie_teksten indi
             ,      kgc_onderzoek_indicaties onin
             WHERE  onin.indi_id = indi.indi_id
             AND    indi.afwijking = 'J'
             AND    onin.onde_id = onde.onde_id
           )
    AND    NOT EXISTS
           ( SELECT NULL
             FROM   bas_metingen meti2
             ,      bas_meetwaarden meet2
             WHERE  meti2.onde_id = meti.onde_id
             AND    meti2.meti_id = meet2.meti_id
             AND ( ( NOT kgc_reken.numeriek(meet2.meetwaarde) BETWEEN NVL(meet2.normaalwaarde_ondergrens, meet2.meetwaarde)
                                                                              AND NVL(meet2.normaalwaarde_bovengrens, meet2.meetwaarde)
                  OR bas_mdok_00.meet_ok( meet2.meet_id ) = 'N'
                   )
                OR  meet2.meetwaarde IS NULL
                 )
           )
    AND SUBSTR( kgc_sypa_00.systeem_waarde( 'ONDERZOEK_AUTOMATISCH_AFRONDEN', onde.kafd_id, onde.ongr_id,  NULL ),1,1) = 'J'
    UNION
    SELECT onde.onde_id
    ,      onde.kafd_id
    ,      onde.ongr_id
    ,      onde.rela_id
    FROM   kgc_onderzoeken onde
    WHERE  onde.afgerond = 'N'
    AND    onde.onde_id = p_onde_id
    AND    p_wlst_id IS NULL
    ;

  CURSOR meti_cur
  ( b_onde_id IN NUMBER
  )
  IS
    SELECT meti.meti_id
    ,     to_char( meti.conclusie)conclusie   --TO_CHAR added by SKA for MANTIS 9355  2016-03-01 release 8.11.0.2
    FROM   bas_metingen meti
    WHERE  meti.onde_id = b_onde_id
    AND    meti.afgerond = 'N'
    ;

  CURSOR meet_cur
  ( b_meti_id IN NUMBER
  )
  IS
    SELECT meet.meet_id
    ,      meet.meetwaarde
    ,      meet.afgerond
    FROM   bas_meetwaarden meet
    WHERE  meet.meti_id = b_meti_id
    AND    meet.afgerond = 'N'
    ;

  CURSOR uits_cur
  ( b_onde_id IN NUMBER
  , b_brty_id IN NUMBER
  )
  IS
    SELECT uits_id
    FROM   kgc_uitslagen
    WHERE  onde_id = b_onde_id
    AND  ( brty_id = b_brty_id OR b_brty_id IS NULL )
    AND    tekst IS NOT NULL
    ;
  v_uits_id NUMBER;

  v_aantal_onde NUMBER := 0;
  v_aantal_meet NUMBER := 0;
  v_aantal_uits NUMBER := 0;
  v_aantal_af NUMBER := 0;
  v_aantal_mis NUMBER := 0;
  v_mede_id_autorisator NUMBER; -- := NVL( p_mede_id_autorisator, kgc_mede_00.medewerker_id );
  v_mede_id_ondertekenaar NUMBER; --  := NVL( p_mede_id_ondertekenaar, v_mede_id_autorisator );
  v_brty_id NUMBER;
  v_onui_id NUMBER;
  v_tekst VARCHAR2(2000);
  v_toelichting VARCHAR2(2000);
  v_commentaar VARCHAR2(2000) := 'Automatisch gegenereerd';
  v_melding VARCHAR2(4000); -- dummy
  v_controle_ok BOOLEAN; -- dummy
  v_severity VARCHAR2(1); -- dummy
  geen_uitslag_tekst EXCEPTION;
BEGIN
  IF ( p_wlst_id IS NULL
   AND p_onde_id IS NULL
     )
  THEN
    x_fout := TRUE;
    x_melding := 'Geen parameters opgegeven!';
    RETURN;
  END IF;
  -- Voortschrijdend inzicht: ondertekenaar is ook autorisator
  if ( p_mede_id_ondertekenaar is not null )
  then
    v_mede_id_ondertekenaar := p_mede_id_ondertekenaar;
    v_mede_id_autorisator := p_mede_id_ondertekenaar;
  elsif ( p_mede_id_autorisator is not null )
  then
    v_mede_id_autorisator := p_mede_id_autorisator;
    v_mede_id_ondertekenaar := p_mede_id_autorisator;
  else
    x_fout := TRUE;
    x_melding := 'Geen autorisator opgegeven!';
    RETURN;
  end if;
  x_fout := FALSE;
  FOR onde_rec IN onde_cur
  LOOP
    v_aantal_onde := v_aantal_onde + 1;
    v_onui_id := kgc_sypa_00.standaard_waarde
                 ( p_parameter_code => 'STANDAARD_UITSLAGCODE'
                 , p_kafd_id => onde_rec.kafd_id
                 , p_ongr_id => onde_rec.ongr_id
                 );
    v_brty_id := kgc_uits_00.standaard_uitslag_brieftype
                 ( p_onde_id => onde_rec.onde_id
                 );
    -- meetwaarden/metingen afronden?
    FOR meti_rec IN meti_cur( onde_rec.onde_id )
    LOOP
      FOR meet_rec IN meet_cur( meti_rec.meti_id )
      LOOP
        -- bepaal automatisch resultaat...
        kgc_mere_00.resultaat
        ( p_meet_id => meet_rec.meet_id
        , p_waarde => meet_rec.meetwaarde
        , p_afgerond => 'J' -- vooruitlopend
        , x_tekst => meti_rec.conclusie
        , x_controle_ok => v_controle_ok
        , x_strengheid => v_severity
        , x_melding => v_melding
        );
        BEGIN
          UPDATE bas_meetwaarden
          SET    afgerond = 'J'
          ,      datum_afgerond = trunc(sysdate)
          WHERE  meet_id = meet_rec.meet_id
          ;
          v_aantal_meet := v_aantal_meet + 1;
        EXCEPTION
          WHEN OTHERS
          THEN
            NULL;
        END;
      END LOOP;
      BEGIN
        UPDATE bas_metingen
        SET    conclusie = meti_rec.conclusie
        ,      afgerond = 'J'
        ,      datum_afgerond = trunc(sysdate)
        ,      mede_id = v_mede_id_autorisator
        WHERE  meti_id = meti_rec.meti_id
        ;
      EXCEPTION
        WHEN OTHERS
        THEN
          NULL;
      END;
    END LOOP;
    -- is er al een uitslag?
    v_uits_id := NULL;
    OPEN  uits_cur( b_onde_id => onde_rec.onde_id
                  , b_brty_id => v_brty_id
                  );
    FETCH uits_cur
    INTO  v_uits_id;
    CLOSE uits_cur;
    IF ( v_uits_id IS NOT NULL )
    THEN
      BEGIN
        UPDATE kgc_uitslagen
        SET    mede_id = v_mede_id_ondertekenaar
        ,      datum_uitslag = NVL( datum_uitslag, trunc(sysdate) )
        ,      datum_autorisatie = trunc(sysdate)
        WHERE  uits_id = v_uits_id
        ;

        -- aanmaken autorisatie historie
        INSERT INTO kgc_onde_autorisatie_historie
          (onde_id
          ,actie
          ,onui_id
          ,datum_autorisatie)
          SELECT onde.onde_id
                ,'A'
                ,NVL(onde.onui_id
                    ,v_onui_id)
                ,trunc(SYSDATE)
          FROM   kgc_onderzoeken onde
          WHERE  onde.onde_id = onde_rec.onde_id
          AND    onde.afgerond = 'N';

        -- onderzoek afronden
        UPDATE kgc_onderzoeken
        SET    afgerond = 'J'
        ,      mede_id_autorisator = v_mede_id_autorisator
        ,      onui_id = NVL( onui_id, v_onui_id )
        ,      datum_autorisatie = trunc(sysdate)
        WHERE  onde_id = onde_rec.onde_id
        ;
        v_aantal_af := v_aantal_af + 1;
      EXCEPTION
        WHEN OTHERS
        THEN
          v_aantal_mis := v_aantal_mis + 1;
      END;
    ELSE
      -- uitslag maken
      kgc_reui_00.uitslag
      ( p_onde_id => onde_rec.onde_id
      , x_tekst => v_tekst
      , x_toelichting => v_toelichting
      , x_melding => v_melding
      );
      IF ( v_tekst IS NOT NULL )
      THEN
        BEGIN
          UPDATE kgc_uitslagen
          SET    mede_id = v_mede_id_ondertekenaar
          ,      brty_id = v_brty_id
          ,      datum_uitslag = trunc(sysdate)
          ,      datum_autorisatie = trunc(sysdate)
          ,      tekst = v_tekst
          ,      toelichting = v_toelichting
          ,      commentaar = v_commentaar
          WHERE  onde_id = onde_rec.onde_id
          AND    brty_id = v_brty_id
          AND    v_tekst IS NOT NULL
          ;
          IF ( sql%rowcount = 0 )
          THEN
            INSERT INTO kgc_uitslagen
            ( onde_id
            , kafd_id
            , rela_id
            , volledig_adres
            , mede_id
            , brty_id
            , datum_uitslag
            , datum_autorisatie
            , tekst
            , toelichting
            , commentaar
            )
            VALUES
            ( onde_rec.onde_id
            , onde_rec.kafd_id
            , onde_rec.rela_id
            , kgc_adres_00.relatie( onde_rec.rela_id, 'J' )
            , v_mede_id_ondertekenaar
            , v_brty_id
            , trunc(sysdate)
            , trunc(sysdate)
            , v_tekst
            , v_toelichting
            , v_commentaar
            );
            v_aantal_uits := v_aantal_uits + 1;
          END IF;

        -- aanmaken autorisatie historie
        INSERT INTO kgc_onde_autorisatie_historie
          (onde_id
          ,actie
          ,onui_id
          ,datum_autorisatie)
          SELECT onde.onde_id
                ,'A'
                ,NVL(onde.onui_id
                    ,v_onui_id)
                ,trunc(SYSDATE)
          FROM   kgc_onderzoeken onde
          WHERE  onde.onde_id = onde_rec.onde_id
          AND    onde.afgerond = 'N';

       -- onderzoek afronden
          UPDATE kgc_onderzoeken
          SET    afgerond = 'J'
          ,      mede_id_autorisator = v_mede_id_autorisator
          ,      onui_id = NVL( onui_id, v_onui_id )
          ,      datum_autorisatie = trunc(sysdate)
          WHERE  onde_id = onde_rec.onde_id
          ;
          v_aantal_af := v_aantal_af + 1;
        EXCEPTION
          WHEN OTHERS
          THEN
            v_aantal_mis := v_aantal_mis + 1;
        END;
      ELSE
        -- geen tekst gemaakt en geen bestaande tekst...
        -- foutmelding alleen bij expliciet onderzoek
        v_aantal_mis := v_aantal_mis + 1;
        IF ( p_onde_id IS NOT NULL )
        THEN
          RAISE geen_uitslag_tekst;
        END IF;
      END IF;
    END IF;
  END LOOP;
  x_aantal := v_aantal_onde;
  x_melding := 'Aantal onderzoeken: '||TO_CHAR(v_aantal_onde)
   ||CHR(10)|| 'Aantal meetwaarden afgerond: '||TO_CHAR(v_aantal_meet)
   ||CHR(10)|| 'Aantal uitslagen gemaakt: '||TO_CHAR(v_aantal_uits)
   ||CHR(10)|| 'Aantal onderzoeken afgerond: '||TO_CHAR(v_aantal_af)
   ||CHR(10)|| 'Aantal onderzoeken mislukt: '||TO_CHAR(v_aantal_mis)
             ;
EXCEPTION
  WHEN geen_uitslag_tekst
  THEN
    x_melding := 'Er is geen uitslag gemaakt. Onderzoek is daarom niet afgerond.'
     ||CHR(10)
     ||CHR(10)|| 'Aantal meetwaarden afgerond: '||TO_CHAR(v_aantal_meet)
               ;
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_melding := SQLERRM;
END automatisch_afronden;
FUNCTION ALLES_AFGEROND
 (P_WLST_ID IN KGC_WERKLIJSTEN.WLST_ID%TYPE
 )
 RETURN VARCHAR2
 IS
/*****************************************************************************
   NAME:       Alles_afgerond
   PURPOSE:    Retourneren of alle onderzoeken van een werklijst afgerond zijn.

   REVISIONS:
   Ver        Date        Author       Description
   ---------  ----------  ------------ --------------------------------------
   1.1	      01/04/2010  GWE         Mantis 3381 (cursor gebaseerd op exists is sneller)
   1.0	      30/07/2008  JKO          Mantis 252
*****************************************************************************/
  CURSOR onde_cur
  ( b_wlst_id IN kgc_werklijsten.wlst_id%TYPE
  )
  IS

    SELECT NULL
    FROM DUAL
    WHERE EXISTS (SELECT NULL
                  FROM   kgc_onderzoeken onde
                  ,      bas_metingen meti
                  ,      bas_meetwaarden meet
                  ,      kgc_werklijst_items wlit -- bevat alleen rijen van werklijsten die niet gearchiveerd zijn.
                  WHERE  wlit.meet_id = meet.meet_id
                  AND    meet.meti_id = meti.meti_id
                  AND    meti.onde_id = onde.onde_id
                  AND    onde.afgerond = 'N'
                  AND    wlit.wlst_id = b_wlst_id)
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  onde_cur( p_wlst_id );
  FETCH onde_cur
  INTO  v_dummy;
  IF ( onde_cur%found )
  THEN
    RETURN ('N');
  ELSE
    -- 1. werklijst niet gearchiveerd: alle rijen geven 'onderzoek afgerond' aan.
    -- 2. werklijst wel gearchiveerd: geen rijen, maar alle onderzoeken zijn impliciet afgerond.
    RETURN ('J');
  END IF;
  CLOSE onde_cur;
END alles_afgerond;
END kgc_wlst_00;
/

/
QUIT
