CREATE OR REPLACE PACKAGE "HELIX"."KGC_VARIABELEN" IS

  function get_var_varchar
   (p_var_naam in varchar2)
  return varchar2
  ;

  procedure set_var_varchar
   (p_var_naam in varchar2
   ,p_waarde   in varchar2)
   ;

  function get_var_number
  (p_var_naam in varchar2)
  return number
  ;

  procedure set_var_number
  (p_var_naam in varchar2
  ,p_waarde   in number)
  ;
END KGC_VARIABELEN;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_VARIABELEN" IS
  /******************************************************************************
      NAME:       kgc_variabelen
      PURPOSE:    opslaan en ophalen van variabelen
                              Bij toevoeging van een variabele denk aan het volgende:
                              1. variabele als pv_... toevoegen in package body
                              2. functie get_var_varchar aanpassen
                              3. functie set_var_varchar aanpassen
      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        07-07-2006   YAR              Creatie nav melding 11389
      2.0        08-08-2006   GWE              Aangepast nav melding 11408
   *****************************************************************************/

  pv_gekozen_destype   varchar2(100);
  pv_gekozen_printer   varchar2(100);
  pv_rapportnaam       varchar2(100);
  pv_cancel_print      varchar2(1) := 'N';
  pv_batchprint        varchar2(1) := 'N';
  pv_afgerond_veranderd varchar2(1) := 'N';
  pv_cate_id           number(10);

  function get_var_varchar
  (p_var_naam in varchar2)
  return varchar2
  is
  begin
    if p_var_naam = 'pv_gekozen_destype'
    then
      return ( pv_gekozen_destype );
    elsif p_var_naam = 'pv_gekozen_printer'
    then
      return ( pv_gekozen_printer );
    elsif p_var_naam = 'pv_rapportnaam'
    then
      return ( pv_rapportnaam );
    elsif p_var_naam = 'pv_cancel_print'
    then
      return ( pv_cancel_print );
    elsif p_var_naam = 'pv_batchprint'
    then
      return ( pv_batchprint );
    elsif p_var_naam = 'pv_afgerond_veranderd'
    then
      return ( pv_batchprint );
    end if;
  end;

  procedure set_var_varchar
  (p_var_naam in varchar2
  ,p_waarde   in varchar2)
  is
  begin
    if p_var_naam = 'pv_gekozen_destype'
    then
      pv_gekozen_destype := p_waarde;
    elsif p_var_naam = 'pv_gekozen_printer'
    then
      pv_gekozen_printer := p_waarde;
    elsif p_var_naam = 'pv_rapportnaam'
    then
      pv_rapportnaam := p_waarde;
    elsif p_var_naam = 'pv_cancel_print'
    then
      pv_cancel_print := p_waarde;
    elsif p_var_naam = 'pv_batchprint'
    then
      pv_batchprint := p_waarde;
    elsif p_var_naam = 'pv_afgerond_veranderd'
    then
      pv_batchprint := p_waarde;
    end if;
  end;

  function get_var_number
  (p_var_naam in varchar2)
  return number
  is
  begin
    if p_var_naam = 'pv_cate_id'
    then
      return ( pv_cate_id );
    end if;
  end;

  procedure set_var_number
  (p_var_naam in varchar2
  ,p_waarde   in number)
  is
  begin
    if p_var_naam = 'pv_cate_id'
    then
      pv_cate_id := p_waarde;
    end if;
  end;
END kgc_variabelen;
/

/
QUIT
