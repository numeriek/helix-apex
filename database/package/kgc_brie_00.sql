CREATE OR REPLACE PACKAGE "HELIX"."KGC_BRIE_00" IS
PROCEDURE registreer
( p_datum IN DATE := NULL
, p_kopie IN VARCHAR2 := 'N'
, p_brieftype IN VARCHAR2 := NULL
, p_brty_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_pers_id IN NUMBER := NULL
, p_kafd_id IN NUMBER
, p_onde_id IN NUMBER := NULL
, p_rela_id IN NUMBER := NULL
, p_uits_id IN NUMBER := NULL
, p_zwan_id IN NUMBER := NULL
, p_onbe_id IN NUMBER := NULL
, p_obmg_id IN NUMBER := NULL
, p_decl_id IN NUMBER := NULL
, p_geadresseerde IN VARCHAR2 := NULL
, p_ramo_code IN VARCHAR2 := NULL
, p_ongr_id IN NUMBER   := NULL
, p_taal_id IN NUMBER   := NULL
, p_destype IN VARCHAR2 := 'PRINTER'
, p_best_id IN kgc_brieven.best_id%TYPE DEFAULT NULL
, p_ind_commit IN VARCHAR2 DEFAULT 'J'
);

-- Is de brief al eerder uitgedraaid?
FUNCTION  eerder_geprint
 ( p_onde_id   IN NUMBER   := NULL
 , p_brieftype IN VARCHAR2 := NULL
 , p_brty_id   IN NUMBER   := NULL
 , p_ramo_id   IN NUMBER   := NULL
 , p_uits_id   IN NUMBER   := NULL
 , p_rela_id   IN NUMBER   := NULL
 , p_decl_id   IN NUMBER   := NULL
 , p_zwan_id   IN NUMBER   := NULL
 , p_obmg_id   IN NUMBER   := NULL
 )
 RETURN  VARCHAR2;
 -- idem, maar nu voor rapport KGCUIT51(DNA)
FUNCTION  eindbrief_reeds_geprint
 (  p_uits_id  IN  NUMBER
 )
 RETURN  VARCHAR2;
 -- idem, maar nu voor rapport KGCONDE51
FUNCTION  machtiging_reeds_geprint
 (  p_decl_id  IN  NUMBER
 )
 RETURN  VARCHAR2;
 -- idem, maar nu voor rapport KGCUITS53
-- bezie alleen vertragingsbrieven na geplande einddatum van onderzoek
FUNCTION  vertraging_reeds_geprint
 (  p_onde_id  IN  NUMBER
 )
 RETURN  VARCHAR2;
 PRAGMA  RESTRICT_REFERENCES(  vertraging_reeds_geprint,  WNDS,  WNPS  );
 -- Wanneer is de brief voor het laatst uitgedraaid?
FUNCTION  print_datum
 (  p_onde_id   IN NUMBER    :=  NULL
 ,  p_brieftype IN VARCHAR2  :=  NULL
 ,  p_brty_id   IN NUMBER    :=  NULL
 ,  p_ramo_id   IN NUMBER    :=  NULL
 ,  p_obmg_id   IN NUMBER    :=  NULL
 )
 RETURN  DATE;
 PRAGMA  RESTRICT_REFERENCES(  print_datum,  WNDS,  WNPS,  RNPS  );

-- is vreugdebrief al eerder uitgedraaid voor de zwangerschap?
 FUNCTION  vreugdebrief_reeds_geprint
 (  p_zwan_id  IN  NUMBER
 )
 RETURN  VARCHAR2;

-- identificatie van een record: onderzoek, declaratie, zwangerschap
-- er is een korte en een lange omschrijving beschikbaar
FUNCTION  context
 (  p_brie_id  IN  NUMBER
 ,  p_volledig  IN  VARCHAR2  :=  'N'
 )
 RETURN  VARCHAR2;
 PRAGMA  RESTRICT_REFERENCES(  context,  WNDS,  WNPS  );

FUNCTION  herinnermg_reeds_geprint
 (  p_obmg_id     IN   NUMBER
 )
 RETURN  VARCHAR2;
FUNCTION  verzoekmg_reeds_geprint
 (  p_obmg_id     IN   NUMBER
 )
 RETURN  VARCHAR2;
FUNCTION  verzoekmg_geprint_op
 (  p_obmg_id     IN   NUMBER
 )
 RETURN  DATE;
FUNCTION  herinnermg_geprint_op
 (  p_obmg_id     IN   NUMBER
 )
 RETURN  DATE;
FUNCTION  ontvangstbevest_geprint_op
 (  p_onde_id  IN  NUMBER
 )
 RETURN  DATE;

-- Mantis 1945:
-- Bepalen datum van aanmaak van de actuele uitslag
FUNCTION  datum_huidige_brief
 (  p_uits_id     IN  NUMBER
 )
RETURN  DATE;

FUNCTION  datum_eerdere_brief
 (  p_onde_id     IN  NUMBER
 ,  p_brty_id     IN  NUMBER    := NULL
 )
 RETURN  DATE;

FUNCTION  uitslag_eerder_geprint
 ( p_uits_id  IN  NUMBER
 , p_brty_id  IN  NUMBER := NULL
 )
 RETURN VARCHAR2;

FUNCTION registreer_brief_in_preview
 ( p_kafd_id IN NUMBER   := NULL
 , p_ongr_id IN NUMBER   := NULL
 , p_ramo_id IN NUMBER   := NULL
 , p_ramo_code IN VARCHAR2 := NULL
 , p_taal_id IN NUMBER   := NULL
 )
RETURN VARCHAR2;

-- Mantis 1945:
-- Exrtra bepalen registreer brief op basis van een rapport code (ramo is niet altijd beschikbaar)
FUNCTION registreer_brief_in_preview
 ( p_kafd_id IN NUMBER   := NULL
 , p_ongr_id IN NUMBER   := NULL
 , p_taal_id IN NUMBER   := NULL
 , p_rapport IN VARCHAR2 := NULL
 )
RETURN VARCHAR2;

FUNCTION direct_naar_printer
 ( p_kafd_id IN NUMBER   := NULL
 , p_ongr_id IN NUMBER   := NULL
 , p_ramo_id IN NUMBER   := NULL
 , p_ramo_code IN VARCHAR2 := NULL
 , p_taal_id IN NUMBER   := NULL
 )
RETURN VARCHAR2;
END KGC_BRIE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_BRIE_00" IS


  /*****************************************************************************
     NAME:       registreer
     PURPOSE:

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.        22-05-2006  HZO              Aanpassingen tgv Word-koppeling:
                                            p_best_id en p_ind_commit toegevoegd

     2.        25-11-2015  SAP              Aanpassingen 'KGC_BRIE_00.EERDER AFGEDRUKTE'
                                            voor Maintis 10922

     NOTES:

  *****************************************************************************/


PROCEDURE registreer
( p_datum IN DATE := NULL
, p_kopie IN VARCHAR2 := 'N'
, p_brieftype IN VARCHAR2 := NULL
, p_brty_id IN NUMBER := NULL
, p_ramo_id IN NUMBER := NULL
, p_pers_id IN NUMBER := NULL
, p_kafd_id IN NUMBER
, p_onde_id IN NUMBER := NULL
, p_rela_id IN NUMBER := NULL
, p_uits_id IN NUMBER := NULL
, p_zwan_id IN NUMBER := NULL
, p_onbe_id IN NUMBER := NULL
, p_obmg_id IN NUMBER := NULL
, p_decl_id IN NUMBER := NULL
, p_geadresseerde IN VARCHAR2 := NULL
, p_ramo_code IN VARCHAR2 := NULL
, p_ongr_id IN NUMBER   := NULL
, p_taal_id IN NUMBER   := NULL
, p_destype IN VARCHAR2 := 'PRINTER'
, p_best_id IN kgc_brieven.best_id%TYPE DEFAULT NULL
, p_ind_commit IN VARCHAR2 DEFAULT 'J'
)
IS
  CURSOR zwan_cur
  IS
    SELECT pers_id
    FROM   kgc_zwangerschappen
    WHERE  zwan_id = p_zwan_id
    ;
  v_pers_id NUMBER;

  CURSOR brty_cur_ramo
  IS
    SELECT brty_id
    FROM   kgc_brieftypes
    WHERE ( ramo_id = p_ramo_id
         OR p_ramo_id IS NULL
          )
    AND   ( code = UPPER(p_brieftype)
         OR UPPER(omschrijving) LIKE '%'||UPPER(p_brieftype)||'%'
         OR p_brieftype IS NULL
          )
    ;
  CURSOR brty_cur
  IS
    SELECT brty_id
    FROM   kgc_brieftypes
    WHERE ( code = UPPER(p_brieftype)
         OR UPPER(omschrijving) LIKE '%'||UPPER(p_brieftype)||'%'
         OR p_brieftype IS NULL
          )
    ;
  CURSOR eind_t_cur
  IS
    SELECT brty_id
    FROM   kgc_brieftypes
    WHERE  code = 'UITSLAG_T'
    ;

  v_brty_id NUMBER;
  v_geadresseerde VARCHAR2(500);
BEGIN
  -- brieven van des type bestand nooit registreren
  IF   UPPER (p_destype) = 'FILE'
  THEN
    RETURN;
  END IF;
  -- moet er geregistreerd worden in de previewer?
  IF   UPPER (p_destype) <> 'PRINTER'
  THEN
    IF registreer_brief_in_preview ( p_kafd_id   => p_kafd_id
                                   , p_ongr_id   => p_ongr_id
                                   , p_ramo_id   => p_ramo_id
                                   , p_ramo_code => p_ramo_code
                                   , p_taal_id   => p_taal_id
                                   )  = 'N'
    THEN
      RETURN;
    END IF;
  END IF;


  IF ( p_pers_id IS NOT NULL )
  THEN
    v_pers_id := p_pers_id;
  ELSIF ( p_zwan_id IS NOT NULL )
  THEN
    OPEN  zwan_cur;
    FETCH zwan_cur
    INTO  v_pers_id;
    CLOSE zwan_cur;
  END IF;
  -- volledig adres
  IF ( p_geadresseerde IS NOT NULL )
  THEN
    v_geadresseerde := p_geadresseerde;
  ELSE
    IF ( p_rela_id IS NOT NULL )
    THEN
      v_geadresseerde := kgc_adres_00.relatie( p_rela_id => p_rela_id, p_met_naam => 'J' );
    ELSIF ( v_pers_id IS NOT NULL )
    THEN
      v_geadresseerde := kgc_adres_00.persoon( p_pers_id => v_pers_id, p_met_naam => 'J' );
    END IF;
  END IF;
  v_geadresseerde := LTRIM( RTRIM( v_geadresseerde, CHR(10) ), CHR(10) );
  -- brieftype
  -- 1. opgegeven brty_id
  -- 2. via opgegeven ramo_id (kgc_brieftypes)
  -- 3. via brieftype (code of omschrijving in kgc_brieftypes)
  -- 4. default: 1e brieftype in kgc_brieftypes
  -- Noot: als brieftype = 'UITSLAG' en onderzoek is nog niet afgerond
  -- dan wordt het brieftype UITSLAG_T (tussentijdse uitslagbrief)
  IF ( p_brieftype = 'UITSLAG'
   AND NOT kgc_onde_00.afgerond
           ( p_onde_id => p_onde_id
           , p_rftf => FALSE
           )
     )
  THEN
    OPEN  eind_t_cur;
    FETCH eind_t_cur
    INTO  v_brty_id;
    CLOSE eind_t_cur;
  END IF;
  IF ( v_brty_id IS NULL )
  THEN
    v_brty_id := p_brty_id;
  END IF;
  IF ( v_brty_id IS NULL )
  THEN
    OPEN  brty_cur_ramo;
    FETCH brty_cur_ramo
    INTO  v_brty_id;
    CLOSE brty_cur_ramo;
  END IF;
  IF ( v_brty_id IS NULL )
  THEN
    OPEN  brty_cur;
    FETCH brty_cur
    INTO  v_brty_id;
    CLOSE brty_cur;
  END IF;

  BEGIN
    INSERT INTO kgc_brieven
    ( pers_id
    , kafd_id
    , datum_print
    , kopie
    , brty_id
    , onde_id
    , rela_id
    , uits_id
    , zwan_id
    , onbe_id
    , decl_id
    , obmg_id
    , geadresseerde
    , best_id
    )
    VALUES
    ( v_pers_id
    , p_kafd_id
    , NVL( p_datum, trunc(SYSDATE) )
    , p_kopie
    , v_brty_id
    , p_onde_id
    , p_rela_id
    , p_uits_id
    , p_zwan_id
    , p_onbe_id
    , p_decl_id
    , p_obmg_id
    , v_geadresseerde
    , p_best_id
    );

    IF p_ind_commit  = 'J'
    THEN
      COMMIT;
    END IF;


  EXCEPTION
    WHEN OTHERS
    THEN
      RAISE; -- ???
  END;
END registreer;

FUNCTION  context
 (  p_brie_id  IN  NUMBER
 ,  p_volledig  IN  VARCHAR2  :=  'N'
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(2000);
  CURSOR brie_cur
  IS
    SELECT brie.onde_id
    ,      DECODE ( brie.onde_id
                  , NULL, NULL
                  , onde.onderzoeknr||' ('||ongr.omschrijving||')'
                  ) onderzoek
    ,      brie.uits_id
    ,      brie.decl_id
    ,      brie.zwan_id
    ,      brie.pers_id
    ,      brie.onbe_id
    ,      brie.best_id -- nieuw rde 01-11-2010  --Addedfor Mantis 4843,ASI
    FROM   kgc_onderzoeksgroepen ongr
    ,      kgc_onderzoeken onde
    ,      kgc_brieven brie
    WHERE  brie.onde_id = onde.onde_id (+)
    AND    onde.ongr_id = ongr.ongr_id (+)
    AND    brie.brie_id = p_brie_id
    ;
  brie_rec brie_cur%rowtype;
 CURSOR zwan_cur
  IS
    SELECT 'Zwangerschap '
        || nummer
        || ' aterm '
        || TO_CHAR( datum_aterm, 'dd-mm-yyyy' )
        || DECODE( pers_id, brie_rec.pers_id, NULL, ' van '|| kgc_pers_00.info( pers_id ) )
    FROM   kgc_zwangerschappen
    WHERE  zwan_id = brie_rec.zwan_id
    ;
  CURSOR decl_cur
  IS
    SELECT 'Declaratie '
        || DECODE( entiteit
                 , 'ONDE', 'onderzoek'
                 , 'ONBE', 'onderzoek-betrokkene'
                 , 'MONS', 'monsterafname'
                 , 'METI', 'deelonderzoek/meting'
                 , 'GESP', 'counseling'
                 , NULL
                 )
        || DECODE( brie_rec.onderzoek, NULL, NULL, ' van onderzoek '|| brie_rec.onderzoek )
        || DECODE( pers_id, brie_rec.pers_id, NULL, ' voor '|| kgc_pers_00.info( pers_id ) )
    FROM   kgc_declaraties
    WHERE  decl_id = brie_rec.decl_id
    ;
  CURSOR uits_cur
  IS
    SELECT 'Uitslag'
        || DECODE( brie_rec.onderzoek, NULL, NULL, ' van onderzoek '|| brie_rec.onderzoek )
        || DECODE( pers_id, brie_rec.pers_id, NULL, ' voor '|| kgc_pers_00.info( pers_id ) )
    FROM   kgc_onderzoeksgroepen ongr
    ,      kgc_onderzoeken onde
    ,      kgc_uitslagen uits
    WHERE  uits.onde_id = onde.onde_id
    AND    onde.ongr_id = ongr.ongr_id
    AND    uits.uits_id = brie_rec.uits_id
    ;
  CURSOR onde_cur
  IS
    SELECT 'Onderzoek '
        || DECODE( brie_rec.onderzoek, NULL, NULL, brie_rec.onderzoek )
        || DECODE( pers_id, brie_rec.pers_id, NULL, ' voor '|| kgc_pers_00.info( pers_id ) )
    FROM   kgc_onderzoeken onde
    WHERE  onde.onde_id = brie_rec.onde_id
    ;
  CURSOR onbe_cur
  IS
    SELECT 'Onderzoek '||onde.onderzoeknr||
         ', betrokkene '||kgc_pers_00.info( onbe.pers_id )
    FROM   kgc_onderzoek_betrokkenen  onbe
  ,      kgc_onderzoeken            onde
    WHERE  onbe.onbe_id = brie_rec.onbe_id
  AND    onbe.onde_id = onde.onde_id
    ;
--Change starts here for Mantis 4843,ASI
  CURSOR best_enti_cur
  IS
    SELECT enti.omschrijving
    ,      best.entiteit_code
    ,      best.entiteit_pk
    FROM   kgc_bestanden best
  ,      kgc_entiteiten  enti
   WHERE  best.best_id = brie_rec.best_id
  AND    enti.code = best.entiteit_code
    ;
  best_enti_rec  best_enti_cur%ROWTYPE;
  CURSOR mons_cur
  IS
    SELECT 'Monster '||mons.monsternummer||
          ', van '||kgc_pers_00.info( mons.pers_id )
    FROM   kgc_monsters mons
    WHERE  mons.mons_id = best_enti_rec.entiteit_pk
    ;
  CURSOR frac_cur
  IS
    SELECT 'Fractie '||frac.fractienummer||
          ', van '||kgc_pers_00.info( mons.pers_id )
    FROM   bas_fracties frac
    ,      kgc_monsters mons
    WHERE  frac.frac_id = best_enti_rec.entiteit_pk
    AND    mons.mons_id = frac.mons_id
    ;
--Change ends here for Mantis 4843,ASI
 BEGIN
  IF ( p_brie_id IS NOT NULL )
  THEN
    OPEN  brie_cur;
    FETCH brie_cur
    INTO  brie_rec;
    CLOSE brie_cur;
    IF ( brie_rec.zwan_id IS NOT NULL )
    THEN
      OPEN  zwan_cur;
      FETCH zwan_cur
      INTO  v_return;
      CLOSE zwan_cur;
    ELSIF ( brie_rec.decl_id IS NOT NULL )
    THEN
      OPEN  decl_cur;
      FETCH decl_cur
      INTO  v_return;
      CLOSE decl_cur;
    ELSIF ( brie_rec.uits_id IS NOT NULL )
    THEN
      OPEN  uits_cur;
      FETCH uits_cur
      INTO  v_return;
      CLOSE uits_cur;
    ELSIF ( brie_rec.onbe_id IS NOT NULL )
    THEN
      OPEN  onbe_cur;
      FETCH onbe_cur
      INTO  v_return;
      CLOSE onbe_cur;
    ELSIF ( brie_rec.onde_id IS NOT NULL )
    THEN
      OPEN  onde_cur;
      FETCH onde_cur
      INTO  v_return;
      CLOSE onde_cur;
-- nieuw rde 01-11-2010: voor verzonden brieven kan via het bestand bepaald worden over welke entiteit het gaat!
--Change starts here for Mantis 4843,ASI
    ELSIF ( brie_rec.best_id IS NOT NULL )
    THEN
      OPEN  best_enti_cur;
      FETCH best_enti_cur
      INTO  best_enti_rec;
      IF  best_enti_cur%NOTFOUND
      THEN
        v_return := 'Context onbekend';
      ELSE
        IF best_enti_rec.entiteit_code = 'FRAC'
        THEN
          OPEN  frac_cur;
          FETCH frac_cur
          INTO  v_return;
          CLOSE frac_cur;
        ELSIF best_enti_rec.entiteit_code = 'MONS'
        THEN
          OPEN  mons_cur;
          FETCH mons_cur
          INTO  v_return;
          CLOSE mons_cur;
        END IF;
      END IF;
      CLOSE best_enti_cur;
-- einde nieuw rde 01-11-2010
--Change ends here for Mantis 4843,ASI
    ELSE
      v_return := 'Context onbekend';
    END IF;
  END IF;
  RETURN( v_return );
END context;

FUNCTION  eerder_geprint
 ( p_onde_id   IN NUMBER   := NULL
 , p_brieftype IN VARCHAR2 := NULL
 , p_brty_id   IN NUMBER   := NULL
 , p_ramo_id   IN NUMBER   := NULL
 , p_uits_id   IN NUMBER   := NULL
 , p_rela_id   IN NUMBER   := NULL
 , p_decl_id   IN NUMBER   := NULL
 , p_zwan_id   IN NUMBER   := NULL
 , p_obmg_id   IN NUMBER   := NULL
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(1);

  CURSOR brty_cur
  (b_ramo_id IN NUMBER
  ,b_brieftype IN VARCHAR2
  )
  IS
    SELECT brty_id
    FROM   kgc_brieftypes
    WHERE ( ramo_id = b_ramo_id OR b_ramo_id IS NULL )
    AND   ( code = UPPER(b_brieftype)
        -- OR UPPER(omschrijving) LIKE '%'||UPPER(b_brieftype)||'%'    -- code commented by Sachinp on  25/11/2015 for Maintis 10922
         OR b_brieftype IS NULL
          )
    ;
  v_brty_id NUMBER;

  CURSOR uits_cur
  ( b_brty_id IN NUMBER
  , b_uits_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven brie
    WHERE  brie.brty_id = b_brty_id
    AND    brie.uits_id = b_uits_id
  ;
  CURSOR decl_cur
  ( b_brty_id IN NUMBER
  , b_decl_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    decl_id = b_decl_id
  ;
  CURSOR zwan_cur
  ( b_brty_id IN NUMBER
  , b_zwan_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    zwan_id = b_zwan_id
  ;
  CURSOR obmg_cur
  ( b_brty_id IN NUMBER
  , b_obmg_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    obmg_id = b_obmg_id
  ;
  CURSOR onde_cur
  ( b_brty_id IN NUMBER
  , b_onde_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    onde_id = b_onde_id
  ;
  CURSOR rela_cur
  ( b_brty_id IN NUMBER
  , b_rela_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    rela_id = b_rela_id
  ;

  v_dummy VARCHAR2(1);
 BEGIN
  IF ( p_brty_id IS NOT NULL )
  THEN
    v_brty_id := p_brty_id;
  ELSE
    OPEN  brty_cur (b_ramo_id   => p_ramo_id
                   ,b_brieftype => p_brieftype
                   );
    FETCH brty_cur
    INTO  v_brty_id;
    CLOSE brty_cur;
  END IF;

  if p_uits_id is not null
  then
    OPEN  uits_cur  ( b_brty_id => v_brty_id
                    , b_uits_id => p_uits_id
                    );
    FETCH uits_cur
    INTO  v_dummy;
    CLOSE uits_cur;
  elsif p_decl_id is not null
  then
    OPEN  decl_cur  ( b_brty_id => v_brty_id
                    , b_decl_id => p_decl_id
                    );
    FETCH decl_cur
    INTO  v_dummy;
    CLOSE decl_cur;
  elsif p_zwan_id is not null
  then
    OPEN  zwan_cur  ( b_brty_id => v_brty_id
                    , b_zwan_id => p_zwan_id
                    );
    FETCH zwan_cur
    INTO  v_dummy;
    CLOSE zwan_cur;
  elsif p_obmg_id is not null
  then
    OPEN  obmg_cur  ( b_brty_id => v_brty_id
                    , b_obmg_id => p_obmg_id
                    );
    FETCH obmg_cur
    INTO  v_dummy;
    CLOSE obmg_cur;
  elsif p_onde_id is not null
  then
    OPEN  onde_cur  ( b_brty_id => v_brty_id
                    , b_onde_id => p_onde_id
                    );
    FETCH onde_cur
    INTO  v_dummy;
    CLOSE onde_cur;
  elsif p_rela_id is not null
  then
    OPEN  rela_cur  ( b_brty_id => v_brty_id
                    , b_rela_id => p_rela_id
                    );
    FETCH rela_cur
    INTO  v_dummy;
    CLOSE rela_cur;
  end if;

  IF v_dummy = 1
  THEN
   v_return := 'J';
  ELSE
   v_return := 'N';
  END IF;

  RETURN( v_return );
END eerder_geprint;

-- idem, maar nu voor rapport KGCUIT51(DNA)
-- NB.: tussentijdse eindbrief wordt ook slechts eenmalig verstuurd
FUNCTION  eindbrief_reeds_geprint
 (  p_uits_id  IN  NUMBER
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(1);
  CURSOR onde_cur
  IS
    SELECT NULL
    FROM   kgc_onderzoeken onde
    ,      kgc_uitslagen uits
    WHERE  uits.onde_id = onde.onde_id
    AND    onde.afgerond = 'N'
    AND    uits.uits_id = p_uits_id
    ;

  CURSOR brty_cur
  IS
    SELECT brty.code
    FROM   kgc_uitslagen  uits
	,      kgc_brieftypes brty
    WHERE  uits.uits_id = p_uits_id
	and    uits.brty_id = brty.brty_id
    ;

  v_dummy     VARCHAR2(1);
  v_brieftype kgc_brieftypes.code%type;
 BEGIN
  OPEN  onde_cur;
  FETCH onde_cur
  INTO  v_dummy;
  IF ( onde_cur%found )
  THEN
    -- Mantis 1945: Tussentijdse uitslagen altijd beschouwen als niet eerder geprint
    v_return := 'N';

--    v_return := eerder_geprint( p_brieftype => 'UITSLAG_T'
--                              , p_uits_id   => p_uits_id
--                              );
  ELSE
    open  brty_cur;
	fetch brty_cur into v_brieftype;
	close brty_cur;

    v_return := eerder_geprint( p_brieftype => v_brieftype
                              , p_uits_id   => p_uits_id
                              );
  END IF;
  RETURN( v_return );
END eindbrief_reeds_geprint;

-- idem, maar nu voor rapport KGCONDE51
FUNCTION  machtiging_reeds_geprint
 (  p_decl_id  IN  NUMBER
 )
 RETURN  VARCHAR2
 IS
 BEGIN
   RETURN(eerder_geprint( p_brieftype => 'MACHTIGING'
                        , p_decl_id   => p_decl_id
                        )
		 );
END machtiging_reeds_geprint;

-- idem, maar nu voor rapport KGCVREU51
FUNCTION  vreugdebrief_reeds_geprint
 (  p_zwan_id  IN  NUMBER
 )
 RETURN  VARCHAR2
 IS
BEGIN
  RETURN kgc_brie_00.eerder_geprint( p_brieftype => 'VREUGDE'
                                  , p_zwan_id   => p_zwan_id
                                   );
END vreugdebrief_reeds_geprint;

FUNCTION  vertraging_reeds_geprint
 (  p_onde_id  IN  NUMBER
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(1);
  CURSOR brie_cur
  IS
    SELECT NULL
    FROM   kgc_brieven brie
    ,      kgc_brieftypes brty
    ,      kgc_onderzoeken onde
    WHERE  brie.brty_id = brty.brty_id
    AND    brty.code = 'VERTRAAGD'
    AND    brie.onde_id = onde.onde_id
    AND    onde.onde_id = p_onde_id
    AND    brie.datum_print >= onde.geplande_einddatum
    ;
  v_dummy VARCHAR2(1);
 BEGIN
  OPEN  brie_cur;
  FETCH brie_cur
  INTO  v_dummy;
  IF ( brie_cur%found )
  THEN
    v_return := 'J';
  ELSE
    v_return := 'N';
  END IF;
  CLOSE brie_cur;
  RETURN( v_return );
END vertraging_reeds_geprint;

FUNCTION  print_datum
 (  p_onde_id   IN NUMBER    :=  NULL
 ,  p_brieftype IN VARCHAR2  :=  NULL
 ,  p_brty_id   IN NUMBER    :=  NULL
 ,  p_ramo_id   IN NUMBER    :=  NULL
 ,  p_obmg_id   IN NUMBER    :=  NULL
 )
 RETURN  DATE
 IS
  v_return DATE;

  CURSOR brty_cur
  (b_ramo_id IN NUMBER
  ,b_brieftype IN VARCHAR2
  )
  IS
    SELECT brty_id
    FROM   kgc_brieftypes
    WHERE ( ramo_id = b_ramo_id OR b_ramo_id IS NULL )
    AND   ( code = UPPER(b_brieftype)
         OR UPPER(omschrijving) LIKE '%'||UPPER(b_brieftype)||'%'
         OR b_brieftype IS NULL
          )
    ;
  v_brty_id NUMBER;

  -- aparte cursoren vanwege performance
  CURSOR brie1_cur
  ( b_brty_id IN NUMBER
  , b_onde_id IN NUMBER
  , b_obmg_id IN NUMBER
  )
  IS
    SELECT MAX( datum_print )
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    onde_id = b_onde_id
    AND    obmg_id = b_obmg_id
    ;
  CURSOR brie2_cur
  ( b_brty_id IN NUMBER
  , b_onde_id IN NUMBER
  )
  IS
    SELECT MAX( datum_print )
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    onde_id = b_onde_id
    ;
  CURSOR brie3_cur
  ( b_brty_id IN NUMBER
  , b_obmg_id IN NUMBER
  )
  IS
    SELECT MAX( datum_print )
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    obmg_id = b_obmg_id
    ;
 BEGIN
  IF ( p_brty_id IS NOT NULL )
  THEN
    v_brty_id := p_brty_id;
  ELSE
    OPEN  brty_cur (b_ramo_id   => p_ramo_id
                   ,b_brieftype => p_brieftype
                   );
    FETCH brty_cur
    INTO  v_brty_id;
    CLOSE brty_cur;
  END IF;

  if ( p_onde_id is not null
   and p_obmg_id is not null
     )
  then
    OPEN  brie1_cur( b_brty_id => v_brty_id
                   , b_onde_id => p_onde_id
                   , b_obmg_id => p_obmg_id
                   );
    FETCH brie1_cur
    INTO  v_return;
    CLOSE brie1_cur;
  elsif ( p_onde_id is not null
        )
  then
    OPEN  brie2_cur( b_brty_id => v_brty_id
                   , b_onde_id => p_onde_id
                   );
    FETCH brie2_cur
    INTO  v_return;
    CLOSE brie2_cur;
  elsif ( p_obmg_id is not null
        )
  then
    OPEN  brie3_cur( b_brty_id => v_brty_id
                   , b_obmg_id => p_obmg_id
                   );
    FETCH brie3_cur
    INTO  v_return;
    CLOSE brie3_cur;
  end if;
  RETURN( v_return );
END print_datum;

FUNCTION  herinnermg_reeds_geprint
 (  p_obmg_id  IN  NUMBER
 )
 RETURN  VARCHAR2  IS
 BEGIN
  RETURN eerder_geprint( p_obmg_id   => p_obmg_id
                       , p_brieftype => 'HERINNERMG'
                       );

END herinnermg_reeds_geprint;

FUNCTION  verzoekmg_reeds_geprint
 (  p_obmg_id  IN  NUMBER
 )
 RETURN  VARCHAR2  IS
 BEGIN
  RETURN eerder_geprint( p_obmg_id   => p_obmg_id
                       , p_brieftype => 'VERZOEKMG'
                       );
END verzoekmg_reeds_geprint;

FUNCTION  verzoekmg_geprint_op
 (  p_obmg_id  IN  NUMBER
 )
 RETURN  DATE  IS
 BEGIN
  RETURN print_datum( p_obmg_id   => p_obmg_id
                    , p_brieftype => 'VERZOEKMG'
                    );
END verzoekmg_geprint_op;

FUNCTION  herinnermg_geprint_op
 (  p_obmg_id  IN  NUMBER
 )
 RETURN  DATE  IS
 BEGIN
  RETURN print_datum( p_obmg_id   => p_obmg_id
                    , p_brieftype => 'HERINNERMG'
                    );
END herinnermg_geprint_op;

FUNCTION  ontvangstbevest_geprint_op
 (  p_onde_id  IN  NUMBER
 )
 RETURN  DATE
 IS
  v_return DATE;
 BEGIN
  v_return := print_datum ( p_onde_id => p_onde_id
                           , p_brieftype => 'ONTVANGST'
                          );
  RETURN( v_return );
END ontvangstbevest_geprint_op;

-- Mantis 1945:
-- Bepalen datum van aanmaak van de actuele uitslag
FUNCTION  datum_huidige_brief
 (  p_uits_id     IN  NUMBER
 )
RETURN  DATE
IS
  cursor c_brie
  is
    SELECT nvl(min(brie.datum_print),trunc(SYSDATE)) datum_geprint
      FROM kgc_brieven brie
         , kgc_uitslagen uits
     WHERE uits.uits_id = brie.uits_id
       AND uits.brty_id = brie.brty_id
       AND uits.uits_id = p_uits_id;
BEGIN
  for r_brie in c_brie
  loop
    return r_brie.datum_geprint;
  end loop;
  return trunc(sysdate);
END;
FUNCTION  datum_eerdere_brief
 (  p_onde_id     IN  NUMBER
 ,  p_brty_id     IN  NUMBER    := NULL
 )
RETURN  DATE
IS
--******************************************************************************
--   NAME:       datum_eerdere_brief
--   PURPOSE:    Bepaal het tijdstempel dat een andere Uitslagbrief is geprint.
--
--   REVISIONS:
--   Ver        Date        Author       Description
--   ---------  ----------  ------------ ---------------------------------------
--   1.0	      04-05-2006  GWE          Melding: HLX-KGCUITS51-11320
--
--******************************************************************************/

  CURSOR brie_cur1
  (b_onde_id   IN NUMBER
  ,b_brty_id   IN NUMBER
  )
  IS
    SELECT MIN(brie.datum_print)
    FROM   kgc_brieven         brie
    ,      kgc_brieftypes      brty
    ,      kgc_rapport_modules ramo
    WHERE  brie.brty_id = brty.brty_id
    AND    ramo.ramo_id = brty.ramo_id
    AND    brie.onde_id = b_onde_id
    AND    brty.brty_id = b_brty_id
    AND    brie.kopie   = 'N'
    AND    ramo.code    = 'UITSLAG'
  ;


  CURSOR brie_cur2
  (b_onde_id     IN NUMBER
  ,b_brty_id     IN NUMBER
  ,b_datum_print IN DATE
  )
  IS
    SELECT MIN(brie.datum_print)
    FROM   kgc_brieven         brie
    ,      kgc_brieftypes      brty
    ,      kgc_rapport_modules ramo
    WHERE  brie.brty_id     =  brty.brty_id
    AND    ramo.ramo_id     =  brty.ramo_id
    AND    brie.onde_id     =  b_onde_id
    AND    ramo.code        =  'UITSLAG'
    AND    brty.brty_id     <> b_brty_id
    AND    brie.kopie       =  'N'
    AND    brie.datum_print <  b_datum_print
  ;


  v_return    DATE;
  v_datum     DATE;
BEGIN
  -- bepaal vroegste datum met 'UITSLAG' voor huidig onde_id en brty_id
  open brie_cur1(b_onde_id   => p_onde_id
                ,b_brty_id   => p_brty_id
                );
  fetch brie_cur1
  into v_datum;

  if v_datum IS NULL
  then
    v_datum := SYSDATE;
  end if;
  close brie_cur1;

  --

  -- bepaal datum eerdere brief
  open brie_cur2(b_onde_id     => p_onde_id
                ,b_brty_id     => p_brty_id
                ,b_datum_print => v_datum
                );
  fetch brie_cur2
  into v_return;

  close brie_cur2;

  RETURN ( v_return );
END datum_eerdere_brief;

FUNCTION  uitslag_eerder_geprint
 ( p_uits_id  IN  NUMBER
 , p_brty_id  IN  NUMBER := NULL
 )
RETURN VARCHAR2  IS

  CURSOR uits_cur
  ( b_uits_id IN NUMBER
  )
  IS
    SELECT brty_id
    FROM   kgc_uitslagen
    WHERE  uits_id = b_uits_id
  ;

 v_brty_id NUMBER;

BEGIN
  IF p_brty_id IS NULL
  THEN
    open uits_cur ( b_uits_id => p_uits_id );
    fetch uits_cur
    into v_brty_id;
    close uits_cur;
  ELSE
    v_brty_id := p_brty_id;
  END IF;

  RETURN eerder_geprint( p_uits_id => p_uits_id
                       , p_brty_id => v_brty_id
                       );
END uitslag_eerder_geprint;

FUNCTION registreer_brief_in_preview
 ( p_kafd_id IN NUMBER   := NULL
 , p_ongr_id IN NUMBER   := NULL
 , p_ramo_id IN NUMBER   := NULL
 , p_ramo_code IN VARCHAR2 := NULL
 , p_taal_id IN NUMBER   := NULL
 )
RETURN VARCHAR2
IS
  CURSOR rapp_cur ( b_rapp_id IN NUMBER
                  )
  IS
    SELECT registreer_via_preview
    FROM kgc_rapporten rapp
    WHERE rapp.rapp_id  = b_rapp_id
    ;

  v_registreer_via_preview VARCHAR2(1);
  v_rapp_id NUMBER;
BEGIN
  v_rapp_id := kgc_rapp_00.rapport_id
               ( p_ramo_code => UPPER( p_ramo_code )
               , p_ramo_id => p_ramo_id
               , p_kafd_id => p_kafd_id
               , p_ongr_id => p_ongr_id
               , p_taal_id => p_taal_id
               );


  IF  v_rapp_id IS NOT NULL
  THEN
    OPEN rapp_cur ( b_rapp_id => v_rapp_id );
    FETCH rapp_cur
    INTO v_registreer_via_preview;
    CLOSE rapp_cur;
  END IF;

  IF v_registreer_via_preview = 'J'
  THEN
    RETURN ('J');
  ELSE
    RETURN ('N');
  END IF;
END registreer_brief_in_preview;

FUNCTION registreer_brief_in_preview
 ( p_kafd_id IN NUMBER   := NULL
 , p_ongr_id IN NUMBER   := NULL
 , p_taal_id IN NUMBER   := NULL
 , p_rapport IN VARCHAR2 := NULL
 )
RETURN VARCHAR2
IS
  cursor rapp_cur
  ( b_rapport in varchar2
  , b_kafd_id in number
  , b_ongr_id in number
  , b_taal_id in number
  )
  is
    select rapp.registreer_via_preview
    from   kgc_rapporten        rapp
    where  rapp.rapport  = b_rapport
    and    rapp.kafd_id  = b_kafd_id
    and  ( rapp.ongr_id = nvl( b_ongr_id, rapp.ongr_id )
        or rapp.ongr_id is null
         )
    and  ( rapp.taal_id = nvl( b_taal_id, rapp.taal_id )
        or rapp.taal_id is null
         )
    order by rapp.kafd_id,rapp.ongr_id,rapp.taal_id
    ;

  v_registreer_via_preview VARCHAR2(1);
BEGIN
  v_registreer_via_preview := 'N';

  FOR r_rapp IN rapp_cur(p_rapport,p_kafd_id,p_ongr_id,p_taal_id)
  LOOP
    v_registreer_via_preview := r_rapp.registreer_via_preview;
    -- alleen eerste is relevant
    EXIT;
  END LOOP;

  RETURN v_registreer_via_preview;
END registreer_brief_in_preview;

FUNCTION direct_naar_printer
 ( p_kafd_id IN NUMBER   := NULL
 , p_ongr_id IN NUMBER   := NULL
 , p_ramo_id IN NUMBER   := NULL
 , p_ramo_code IN VARCHAR2 := NULL
 , p_taal_id IN NUMBER   := NULL
 )
RETURN VARCHAR2
IS
/******************************************************************************
   NAME:       direct_naar_printer
   PURPOSE:    Bepaal of het rapport dat gedefinieerd is voor een (rapportmodule,
               afdeling, onderzoeksgroep, taal) direct naar de printer verstuurd
			   moet worden.

   REVISIONS:
   Ver        Date        Author       Description
   ---------  ----------  ------------ ---------------------------------------
   1.0	      13-04-2006  GWE          Melding: HLX-KGCRAPP01-11094

******************************************************************************/
  CURSOR rapp_cur ( b_rapp_id IN NUMBER
                  )
  IS
    SELECT direct_naar_printer
    FROM kgc_rapporten rapp
    WHERE rapp.rapp_id  = b_rapp_id
    ;

  v_direct_naar_printer VARCHAR2(1);
  v_rapp_id NUMBER;
BEGIN
  v_rapp_id := kgc_rapp_00.rapport_id
               ( p_ramo_code => UPPER( p_ramo_code )
               , p_ramo_id => p_ramo_id
               , p_kafd_id => p_kafd_id
               , p_ongr_id => p_ongr_id
               , p_taal_id => p_taal_id
               );


  IF  v_rapp_id IS NOT NULL
  THEN
    OPEN rapp_cur ( b_rapp_id => v_rapp_id );
    FETCH rapp_cur
    INTO v_direct_naar_printer;
    CLOSE rapp_cur;
  END IF;

  IF v_direct_naar_printer = 'J'
  THEN
    RETURN ('J');
  ELSE
    RETURN ('N');
  END IF;
END direct_naar_printer;
END kgc_brie_00;
/

/
QUIT
