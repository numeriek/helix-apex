CREATE OR REPLACE PACKAGE "HELIX"."KGC_ONBE_00" IS
PROCEDURE voeg_betrokkene_toe
  (   p_onde_id   IN   NUMBER   :=   NULL
  ,   p_frac_id   IN   NUMBER   :=   NULL
  ,   p_meti_id   IN   NUMBER   :=   NULL
  );

-- 259, toegevoegd: opvoeren dummy-meting en daarmee impliciet onderzoeksbetrokkenheid
PROCEDURE voeg_betrokkene_dochter_toe
  (   p_frac_id        IN NUMBER
  ,   p_frac_id_parent IN NUMBER
  ,   p_mons_id        IN NUMBER
  );

-- 1193: geef onderzoek-betrokkene die door verwijder_betrokkene verwijderd wordt
FUNCTION te_verwijderen_betrokkene
  (   p_onde_id   IN   NUMBER
  ,   p_frac_id   IN   NUMBER
  ,   p_meti_id   IN   NUMBER   :=   NULL
  )
RETURN NUMBER;

-- verwijder fractie in onderzoek (bas_metingen) zo nodig uit onderzoek-betrokkenen
PROCEDURE verwijder_betrokkene
  (   p_onde_id   IN   NUMBER
  ,   p_frac_id   IN   NUMBER
  ,   p_meti_id   IN   NUMBER   :=   NULL
  );

-- voeg gesprek betrokkene toe aan onderzoek-betrokkenen
PROCEDURE voeg_gesp_betrokkene_toe
  (   p_gebe_id   IN   NUMBER
  );

-- verwijder gesprek betrokkene uit onderzoek-betrokkenen
PROCEDURE verwijder_gesp_betrokkene
  (   p_gesp_id   IN   NUMBER
  ,   p_pers_id   IN   NUMBER
  );

FUNCTION volgende_rol
  (   p_onde_id    IN    NUMBER
  )
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES(   volgende_rol,   WNDS,   RNPS,   WNPS   );

FUNCTION toon_persoon_rol
  (   p_pers_id    IN    NUMBER
  ,   p_onde_id    IN    NUMBER
  )
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES(   toon_persoon_rol,   WNDS,   RNPS,   WNPS   );

FUNCTION aanspreken_onbe
  (  p_onde_id   IN   NUMBER
  ,  p_rol       IN   VARCHAR2
  )
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES(   aanspreken_onbe,   WNDS,   RNPS,   WNPS   );

FUNCTION gesp_persoon_rol
  (  p_pers_id    IN    NUMBER
  ,  p_gesp_id    IN    NUMBER
  )
RETURN VARCHAR2;

PROCEDURE ins_pers_rol
  (  p_onde_id   IN   NUMBER
  ,  p_pers_id   IN   NUMBER
  ,  p_rol       IN   VARCHAR2 DEFAULT 'P01'
  ,  p_foet_id   IN   NUMBER DEFAULT NULL
  );
END KGC_ONBE_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ONBE_00" IS
PROCEDURE voeg_betrokkene_toe(p_onde_id IN NUMBER := NULL
                             ,p_frac_id IN NUMBER := NULL
                             ,p_meti_id IN NUMBER := NULL) IS
   CURSOR meti_cur IS
      SELECT DISTINCT meti.onde_id
                     ,mons.pers_id
                     ,mons.foet_id
                     ,meti.frac_id
                     ,kafd.code kafd_code
                     ,ongr.code ongr_code
                     ,onde.declareren onde_declareren
                     ,onde.datum_binnen onde_datum_binnen
                     ,onde.onderzoekstype
                     ,onde.onderzoekswijze
                     ,onde.omschrijving onde_omschrijving
                     ,onde.rela_id
                     ,frac.controle frac_controle
                     ,frac.fractienummer fractienummer -- 259
                     ,frac.frac_id_parent frac_id_parent -- 259
                     ,frac.type_afleiding -- 259
                     ,frty.code frty_code
                     ,mate.code mate_code
                     ,herk.code herk_code
        FROM kgc_kgc_afdelingen    kafd
            ,kgc_onderzoeksgroepen ongr
            ,bas_fractie_types     frty
            ,kgc_herkomsten        herk
            ,kgc_materialen        mate
            ,kgc_onderzoeken       onde
            ,kgc_monsters          mons
            ,bas_fracties          frac
            ,bas_metingen          meti
       WHERE mons.mons_id = frac.mons_id
         AND mons.mate_id = mate.mate_id
         AND mons.herk_id = herk.herk_id(+)
         AND meti.frac_id = frac.frac_id
         AND frac.frty_id = frty.frty_id(+)
         AND meti.onde_id = onde.onde_id
         AND onde.kafd_id = kafd.kafd_id
         AND onde.ongr_id = ongr.ongr_id
         AND meti.meti_id = nvl(p_meti_id
                               ,meti.meti_id)
         AND meti.frac_id = nvl(p_frac_id
                               ,meti.frac_id)
         AND meti.onde_id = nvl(p_onde_id
                               ,meti.onde_id)
         AND upper(substr(kgc_sypa_00.systeem_waarde('FRACTIE_ONDERZOEK_BETROKKEN'
                                                    ,onde.kafd_id
                                                    ,onde.ongr_id)
                         ,1
                         ,1)) = 'J'
         AND NOT EXISTS (SELECT NULL
                FROM kgc_onderzoek_betrokkenen onbe
               WHERE onbe.onde_id = meti.onde_id
                 AND onbe.frac_id = meti.frac_id);

   -- bestaat er al een declarabele ONBE, voor deze monster-persoon/foetus in dit onderzoek
   -- dan niet nog een keer declareren.
   CURSOR onbe_cur(b_onde_id IN NUMBER, b_pers_id IN NUMBER, b_foet_id IN NUMBER) IS
      SELECT NULL
        FROM kgc_monsters              mons
            ,bas_fracties              frac
            ,kgc_onderzoek_betrokkenen onbe
       WHERE onbe.declareren = 'J'
         AND onbe.onde_id = b_onde_id
         AND onbe.frac_id = frac.frac_id
         AND frac.mons_id = mons.mons_id
         AND mons.pers_id = b_pers_id
         AND nvl(mons.foet_id
                ,-9999999999) = nvl(b_foet_id
                                   ,-9999999999);

   v_dummy      VARCHAR2(1);
   v_declareren VARCHAR2(1) := 'J';

BEGIN

   FOR r IN meti_cur
   LOOP
      IF (r.onde_declareren = 'N')
      THEN
         v_declareren := 'N';
      ELSE
         OPEN onbe_cur(r.onde_id
                      ,r.pers_id
                      ,r.foet_id);
         FETCH onbe_cur
            INTO v_dummy;
         IF (onbe_cur%FOUND)
         THEN
            v_declareren := 'N';
         ELSE
            v_declareren := kgc_decl_00.declarabel(p_criteria => kgc_util_00.tag('ONBE'
                                                                                ,'entiteit') ||
                                                                 kgc_util_00.tag(r.kafd_code
                                                                                ,'kafd_code') ||
                                                                 kgc_util_00.tag(r.ongr_code
                                                                                ,'ongr_code') ||
                                                                 kgc_util_00.tag(r.onderzoekstype
                                                                                ,'onderzoekstype') ||
                                                                 kgc_util_00.tag(r.onderzoekswijze
                                                                                ,'onderzoekswijze') ||
                                                                 kgc_util_00.tag(r.onde_omschrijving
                                                                                ,'onde_omschrijving') ||
                                                                 kgc_util_00.tag(r.onde_declareren
                                                                                ,'onde_declareren') ||
                                                                 kgc_util_00.tag(r.onde_datum_binnen
                                                                                ,'datum') ||
                                                                 kgc_util_00.tag(r.mate_code
                                                                                ,'mate_code') ||
                                                                 kgc_util_00.tag(r.herk_code
                                                                                ,'herk_code') ||
                                                                 kgc_util_00.tag(r.frty_code
                                                                                ,'frty_code') ||
                                                                 kgc_util_00.tag(r.frac_controle
                                                                                ,'frac_controle') ||
                                                                 kgc_util_00.tag(r.pers_id
                                                                                ,'pers_id') ||
                                                                 kgc_util_00.tag(r.rela_id
                                                                                ,'rela_id') ||
                                                                 kgc_util_00.tag(r.onde_id
                                                                                ,'onde_id'));
         END IF;
         CLOSE onbe_cur;
      END IF;

      -- N. ter Burg, 259 >>>
      BEGIN
         -- Niet doen, indien dochterfractie van het type D, onderzoekswijze is leeg of A en
         -- fractienummer eindigt niet op A (de eerste).
         IF NOT (   r.frac_id_parent IS NOT NULL
                AND r.type_afleiding = 'D'
                AND (  r.onderzoekswijze IS NULL
                    OR r.onderzoekswijze = 'A')
                AND substr(r.fractienummer
                          ,length(rtrim(r.fractienummer))
                          ,1) != 'A')
         THEN
            -- BEGIN
            -- <<< 259

            INSERT INTO kgc_onderzoek_betrokkenen
               (onde_id
               ,pers_id
               ,frac_id
               ,declareren)
            VALUES
               (r.onde_id
               ,r.pers_id
               ,r.frac_id
               ,v_declareren);
         END IF; -- 259
      EXCEPTION
         WHEN dup_val_on_index -- onde_id, frac_id
          THEN
            NULL;
      END;
   END LOOP;

END voeg_betrokkene_toe;

-- 259: toegevoegd
PROCEDURE voeg_betrokkene_dochter_toe(p_frac_id        IN NUMBER
                                     ,p_frac_id_parent IN NUMBER
                                     ,p_mons_id        IN NUMBER) IS

  CURSOR onmo_cur
   (b_frac_id IN NUMBER -- van dochterfractie
   ,b_frac_id_parent IN NUMBER -- van moederfractie
   ,b_mons_id IN NUMBER) -- van moeder/dochterfractie; niet gebruikt
     IS
     SELECT onmo.onmo_id
           ,onmo.onde_id
       FROM kgc_onderzoek_monsters    onmo
           ,kgc_onderzoeken           onde
           ,kgc_onderzoek_betrokkenen onbe
           ,bas_fracties              frac
      WHERE 1=1
        -- criteria mbt de moederfractie: dezelfde onderzoek-monsters op grond waarvan de openstaande testen zijn verplaatst,
        -- aangevuld met het criterium dat er tevens een onderzoeks-betrokkenheid aanwezig moet zijn.
        AND onmo.mons_id = frac.mons_id
        AND onmo.onde_id = onde.onde_id
        AND onde.afgerond = 'N'
        AND onbe.onde_id = onde.onde_id
        AND onbe.frac_id = frac.frac_id
        AND frac.frac_id = b_frac_id_parent
        AND nvl(frac.status, 'A') IN ('A', 'O')
        -- criteria mbt de dochterfractie: er is door het verplaatsten van de openstaande testen geen enkele meting opgevoerd:
        AND NOT EXISTS (SELECT NULL
                          FROM bas_metingen meti
                         WHERE meti.onmo_id = onmo.onmo_id
                           AND meti.frac_id = b_frac_id)
        ;
   v_meti_id bas_metingen.meti_id%TYPE;

BEGIN
   -- Indien moederfractie betrokken is bij 1 of meerdere onderzoeken,
   -- dan moet een dochterfractie ook betrokken gemaakt worden bij die
   -- onderzoeken voor zover nog niet eerder in de transactie gebeurt is.

   -- Ophalen betrokken onderzoeken bij moederfractie waarvoor nog geen
   -- meting is opgevoerd die gerelateerd is aan dochterfractie.
   FOR onmo_rec IN onmo_cur(p_frac_id
                           ,p_frac_id_parent
                           ,p_mons_id)
   LOOP
      -- Opvoeren van een dummy-meting.
      -- Het opvoeren van een meting leidt afh. van sypa 'FRACTIE_ONDERZOEK_BETROKKEN'
      -- autom. tot het opvoeren van onderzoeksbetrokkenheid (zie trigger CG$AIS_BAS_METINGEN).
      INSERT INTO bas_metingen
         (frac_id
         ,onmo_id
         ,onde_id)
      VALUES
         (p_frac_id
         ,onmo_rec.onmo_id
         ,onmo_rec.onde_id)
      RETURNING meti_id INTO v_meti_id;
      -- Als er geen onderzoeksbetrokkenheid is opgevoerd door AIS-trigger, dan dummy-meting weer weggooien:
      DELETE bas_metingen meti
       WHERE meti.meti_id = v_meti_id
         AND NOT EXISTS (SELECT NULL
                           FROM kgc_onderzoek_betrokkenen onbe
                          WHERE onbe.onde_id = meti.onde_id
                            AND onbe.frac_id = meti.frac_id);

   END LOOP;

END voeg_betrokkene_dochter_toe;

-- 1193: geef de onderzoek-betrokkene die door verwijder_betrokkene verwijderd wordt
FUNCTION te_verwijderen_betrokkene
 (  p_onde_id  IN  NUMBER
 ,  p_frac_id  IN  NUMBER
 ,  p_meti_id  IN  NUMBER  :=  NULL
 )
RETURN NUMBER
 IS
  CURSOR meti_cur
  IS
    SELECT NULL
    FROM   bas_metingen meti
    WHERE  meti.onde_id = p_onde_id
    AND    meti.frac_id = p_frac_id
    AND    meti.meti_id <> NVL(p_meti_id,-99999999999)
    ;
  v_dummy VARCHAR2(1);

  CURSOR onbe_cur
  IS
    SELECT onbe.onbe_id
    FROM   kgc_onderzoek_betrokkenen onbe
    WHERE  onde_id = p_onde_id
    AND    frac_id = p_frac_id
   ;
  v_onbe_id kgc_onderzoek_betrokkenen.onbe_id%TYPE := NULL;

BEGIN
  OPEN  meti_cur;
  FETCH meti_cur
  INTO  v_dummy;
  IF ( meti_cur%notfound )
  THEN -- geen andere meting meer gevonden
    BEGIN
      OPEN  onbe_cur; -- maximaal 1 rij
      FETCH onbe_cur
      INTO  v_onbe_id;
      CLOSE onbe_cur;
    EXCEPTION
      WHEN OTHERS
      THEN
        v_onbe_id := NULL; -- Just-In-Case
    END;
  END IF;
  CLOSE meti_cur;
  RETURN( v_onbe_id );
END te_verwijderen_betrokkene;

-- verwijder fractie IN onderzoek (bas_metingen) zo nodig uit onderzoek-betrokkenen
PROCEDURE verwijder_betrokkene
 (  p_onde_id  IN  NUMBER
 ,  p_frac_id  IN  NUMBER
 ,  p_meti_id  IN  NUMBER  :=  NULL
 )
IS
  CURSOR meti_cur
  IS
    SELECT NULL
    FROM   bas_metingen meti
    WHERE  meti.onde_id = p_onde_id
    AND    meti.frac_id = p_frac_id
    AND    meti.meti_id <> NVL(p_meti_id,-99999999999)
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  meti_cur;
  FETCH meti_cur
  INTO  v_dummy;
  IF ( meti_cur%notfound )
  THEN -- geen andere meting meer gevonden
    BEGIN
      DELETE FROM kgc_onderzoek_betrokkenen
      WHERE onde_id = p_onde_id
      AND   frac_id = p_frac_id
      ;
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END IF;
  CLOSE meti_cur;
END verwijder_betrokkene;

PROCEDURE  voeg_gesp_betrokkene_toe
 (  p_gebe_id  IN  NUMBER
 )
 IS
  CURSOR gebe_cur( p_gebe_id  IN  NUMBER ) IS
    SELECT gesp.onde_id
    ,      gebe.pers_id
    FROM   kgc_gesprekken  gesp
    ,      kgc_gesprek_betrokkenen  gebe
    WHERE  gebe.gebe_id = p_gebe_id
    AND    gebe.gesp_id = gesp.gesp_id
    AND    NOT EXISTS
         ( SELECT '1'
             FROM   kgc_onderzoek_betrokkenen  onbe
             WHERE  onbe.onde_id = gesp.onde_id
             AND    onbe.pers_id = gebe.pers_id
           )
    ;
 BEGIN
  IF  p_gebe_id IS NOT NULL
  THEN
    FOR gebe_rec IN gebe_cur( p_gebe_id )
    LOOP
      IF  gebe_rec.pers_id IS NOT NULL
      THEN
        BEGIN
          INSERT INTO kgc_onderzoek_betrokkenen (
                   onde_id
          ,        pers_id
          )
          VALUES ( gebe_rec.onde_id
          ,        gebe_rec.pers_id
        );
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX
          THEN
            NULL;
          WHEN OTHERS
          THEN
            RAISE;
       END;
      END IF;
    END LOOP;
  END IF;
END voeg_gesp_betrokkene_toe;

PROCEDURE  verwijder_gesp_betrokkene
 (  p_gesp_id  IN  NUMBER
 ,  p_pers_id  IN  NUMBER
 )
 IS
 BEGIN
  IF  p_gesp_id IS NOT NULL
  AND p_pers_id IS NOT NULL
  THEN
    DELETE
    FROM   kgc_onderzoek_betrokkenen  onbe
    WHERE  onbe.pers_id = p_pers_id
    AND    onbe.onde_id =
         ( SELECT gesp.onde_id
           FROM   kgc_gesprekken  gesp
           WHERE  gesp.gesp_id = p_gesp_id
         )
    ;
  END IF;
EXCEPTION
  WHEN OTHERS
  THEN
    NULL;
END verwijder_gesp_betrokkene;

FUNCTION volgende_rol
( p_onde_id IN NUMBER
)
RETURN  VARCHAR2
IS
  cursor onde_cur
  is
    select kafd_id
    ,      ongr_id
    from   kgc_onderzoeken
    where  onde_id = p_onde_id;
  onde_rec onde_cur%rowtype;
  v_rol VARCHAR2(100);

  CURSOR max_cur( p_1e_teken  IN  VARCHAR2 )
  IS
    SELECT NVL( MAX( SUBSTR( onbe.rol, 2, 2 ) ), 0 )
    FROM   kgc_onderzoek_betrokkenen  onbe
    WHERE  onbe.onde_id = p_onde_id
    AND    SUBSTR( onbe.rol, 1, 1 ) = p_1e_teken
    ;
  v_p_nummer  NUMBER;
  v_return    VARCHAR2(3) := NULL;
BEGIN
  IF  p_onde_id IS NOT NULL
  THEN
    open  onde_cur;
    fetch onde_cur
    into  onde_rec;
    close onde_cur;
    BEGIN
      v_rol := kgc_sypa_00.standaard_waarde
               ( p_parameter_code => 'STANDAARD_ONDERZOEKSBETROKKENE'
               , p_kafd_id => onde_rec.kafd_id
               , p_ongr_id => onde_rec.ongr_id
               );
    EXCEPTION
      WHEN OTHERS
      THEN
        v_rol := NULL;
    END;
    IF ( upper(v_rol) like '%NUL%' ) -- dan niet!
    THEN
      v_return := null;
    ELSE
      --  Elke volgende rol is de (hoogst aanwezige + 1), 1e rol wordt 'P01'.
      OPEN  max_cur( 'P' );
      FETCH max_cur
      INTO  v_p_nummer;
      CLOSE max_cur;
      IF v_p_nummer < 99
      THEN
        v_return := 'P'||LPAD( v_p_nummer + 1, 2, '0' );
      ELSE
        v_return := null;
      END IF;
    END IF;
  END IF;
  RETURN v_return;
END volgende_rol;

FUNCTION  toon_persoon_rol(
   p_pers_id   IN   NUMBER
 ,  p_onde_id   IN   NUMBER
 )  RETURN  VARCHAR2  IS
  CURSOR onbe_cur IS
    SELECT MIN(onbe.rol)  rol
    FROM   kgc_onderzoek_betrokkenen  onbe
    WHERE  onbe.onde_id = p_onde_id
    AND    onbe.pers_id = p_pers_id
    ;
  onbe_rec  onbe_cur%ROWTYPE;
 BEGIN
  IF  p_pers_id IS NOT NULL
  AND p_onde_id IS NOT NULL
  THEN
    OPEN  onbe_cur;
    FETCH onbe_cur
    INTO  onbe_rec;
    CLOSE onbe_cur;
  END IF;
  RETURN onbe_rec.rol;
END toon_persoon_rol;

  FUNCTION  aanspreken_onbe
   (  p_onde_id   IN   NUMBER
   ,  p_rol       IN   VARCHAR2
   )  RETURN  VARCHAR2
   IS
    CURSOR onbe_cur IS
    SELECT pers.aanspreken
    FROM   kgc_personen  pers
    ,      kgc_onderzoek_betrokkenen  onbe
    WHERE  onbe.pers_id = pers.pers_id
    AND    onbe.onde_id = p_onde_id
    AND    onbe.rol     = p_rol
      ;
  onbe_rec  onbe_cur%ROWTYPE;
    CURSOR onde_cur IS
    SELECT pers.aanspreken
    FROM   kgc_personen  pers
    ,      kgc_onderzoeken onde
    WHERE  onde.pers_id = pers.pers_id
    AND    onde.onde_id = p_onde_id
      ;
  onde_rec  onde_cur%ROWTYPE;
   BEGIN
    OPEN  onbe_cur;
  FETCH onbe_cur
  INTO  onbe_rec;
  IF onbe_cur%NOTFOUND
  THEN
    IF  p_rol = 'A1'
    THEN
        IF aanspreken_onbe( p_onde_id, 'A2' ) IS NULL
        THEN
          OPEN  onde_cur;
          FETCH onde_cur
          INTO  onde_rec;
          CLOSE onde_cur;
          RETURN onde_rec.aanspreken;
        END IF;
      END IF;
    END IF;
    IF onbe_cur%ISOPEN
    THEN
      CLOSE onbe_cur;
    END IF;
    RETURN onbe_rec.aanspreken;
  END;

  FUNCTION   gesp_persoon_rol
  (  p_pers_id    IN    NUMBER
  ,  p_gesp_id    IN    NUMBER
  )   RETURN   VARCHAR2
  IS
    CURSOR gesp_cur IS
      SELECT onde_id
      FROM   kgc_gesprekken  gesp
      WHERE  gesp.gesp_id = p_gesp_id
      ;
     gesp_rec  gesp_cur%ROWTYPE;
  BEGIN
    OPEN  gesp_cur;
    FETCH gesp_cur
    INTO  gesp_rec;
    CLOSE gesp_cur;
    RETURN toon_persoon_rol( p_pers_id => p_pers_id
                           , p_onde_id => gesp_rec.onde_id
                           );
  END;

  PROCEDURE ins_pers_rol
  (  p_onde_id   IN   NUMBER
  ,  p_pers_id   IN   NUMBER
  ,  p_rol       IN   VARCHAR2
  ,  p_foet_id   IN   NUMBER DEFAULT NULL
  )
  IS
  BEGIN
    IF ( upper(p_rol) like '%NUL%' )
    THEN
      INSERT INTO kgc_onderzoek_betrokkenen(
               onde_id
      ,        pers_id
      ,        rol
      ,        foet_id
      )
      VALUES ( p_onde_id
      ,        p_pers_id
      ,        null
      ,        p_foet_id
      );
    ELSIF ( p_rol IS NOT NULL )
    THEN
      INSERT INTO kgc_onderzoek_betrokkenen(
               onde_id
      ,        pers_id
      ,        rol
      ,        foet_id
      )
      VALUES ( p_onde_id
      ,        p_pers_id
      ,        p_rol
      ,        p_foet_id
      );
    END IF;
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN NULL;
    WHEN OTHERS           THEN RAISE;
  END  ins_pers_rol;
END kgc_onbe_00;
/

/
QUIT
