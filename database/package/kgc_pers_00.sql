CREATE OR REPLACE PACKAGE HELIX.KGC_PERS_00 IS
-- FORMATTEER_ZOEKNAAM
-- Zoeknaam is een verzameling van namen van een persoon waarop (snel) gezocht
-- kan worden. Een (1) in-parameter is verplicht, de volgende optioneel.
-- Breid het aantal parameters naar hartelust uit.
-- De zoeknaam is maximaal 200 tekens lang!

/*
   Wijzigingshistorie:
   ===============

   Bevindingnr     Wie   Version    Wanneer     Wat
   ---------------------------------------------------------------------------------------------------------
   Mantis 3068     APL   2          11-10-2016  Extra functie controleer_bsn toegevoegd tbv kgc_import
   Mantis 3068     APL   1.9        30-05-2016  Reverted for Release 8.11.4.7.
   Mantis 3068     ATS   1.8        08-04-2016  P_geboortedatum change from VARCHAR2 to DATE and layout adjustment  For release rework 8.11.0.3
   Mantis 3068     ATS   1.7        17-12-2015  BSN in XML bestand oppakken en verwerken geeft voor 1 situatie een onterechte foutmelding tijdens het verwerken van de persoonsgegevens.
*/
FUNCTION formatteer_zoeknaam
( p_naam1 IN VARCHAR2
, p_naam2 IN VARCHAR2 := NULL
, p_naam3 IN VARCHAR2 := NULL
, p_naam4 IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES ( formatteer_zoeknaam, WNDS, WNPS );

-- ADMIN_MEERLING
-- Tel de administratieve meerlingen (personen met dezelfde (achter)naam en geboortedatum
-- of hetzelfde ZISnr)
PROCEDURE admin_meerling
( p_naam IN VARCHAR2 := NULL
, p_geboortedatum IN DATE := NULL
, p_zisnr IN VARCHAR2 := NULL
, x_aantal OUT NUMBER
, x_pers_id OUT NUMBER -- wordt gevuld als er precies 1 persoon gevonden is
);

-- GEBOREN (overloaded)
-- Registreer een foetus als persoon
-- De functie geeft het nieuwe pers_id terug, de procedure niet.
FUNCTION geboren
( p_foet_id IN NUMBER
, p_geboortedatum IN DATE := NULL
, p_geslacht IN VARCHAR2 := NULL
, p_voornaam IN VARCHAR2 := NULL
, p_voorvoegsel IN VARCHAR2 := NULL
, p_achternaam IN VARCHAR2  :=  NULL
, p_upd IN BOOLEAN := TRUE
)
RETURN NUMBER
;

PROCEDURE geboren
( p_foet_id IN NUMBER
, p_geboortedatum IN DATE := NULL
, p_geslacht IN VARCHAR2 := NULL
, p_voornaam IN VARCHAR2 := NULL
, p_voorvoegsel IN VARCHAR2 := NULL
, p_achternaam IN VARCHAR2  :=  NULL
, p_upd IN BOOLEAN := TRUE
);

-- Voordat een persoon wordt verwijderd
-- moet eerst een eventuele link van foetus
-- naar persoon (geboren als) worden leeggemaakt
PROCEDURE niet_geboren
( p_pers_id IN NUMBER
);

-- koppel de gegevens van verschillende personen aan 1 persoon
PROCEDURE koppel
( p_correct_pers_id IN NUMBER
, p_koppellijst IN VARCHAR2
, x_resultaat OUT VARCHAR2
);

-- Geef een fictieve geboortedatum op basis van part_geboortedatum tbv. berekeningen
FUNCTION rekendatum
( p_geboortedatum    IN  DATE
,p_part_geboortedatum IN VARCHAR2
)
RETURN DATE;
PRAGMA RESTRICT_REFERENCES( rekendatum, WNDS, WNPS ); -- Added for mantis 10927 SKA 01-04-2015
FUNCTION rekendatum
( p_pers_id IN NUMBER
)
RETURN DATE;
PRAGMA RESTRICT_REFERENCES( rekendatum, WNDS, WNPS );

-- construeer string met families waartoe persoon behoort
FUNCTION  families
( p_pers_id IN NUMBER   := NULL
, p_lengte  IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( families, WNDS, WNPS  );

-- construeer string met achternamen van de familieleden
FUNCTION  familieleden
( p_fami_id   IN   NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( familieleden, WNDS, WNPS );

-- werk hulpvelden zoekfamilie en zoekpersonen bij
PROCEDURE  zoekfamilie
( p_pers_id IN NUMBER
, p_fami_id   IN   NUMBER
);

-- Mantis 3111 2010-01-07 gwe
-- Nieuwe procedure voor updaten zoekpersonen en zoekfamilie

-- toevoegen pers id aan tabel voor updaten zoekfamilie
PROCEDURE  toevoeg_zoekfamilie
( p_pers_id IN NUMBER
);

-- werkt hulpveld zoekfamilie bij voor opgeslagen personen
PROCEDURE  update_zoekfamilie;

-- toevoegen fami id aan tabel voor updaten zoekpersonen
PROCEDURE  toevoeg_zoekpersonen
( p_fami_id IN NUMBER
);

-- werkt hulpveld zoekpersonen bij voor opgeslagen families
PROCEDURE  update_zoekpersonen;

-- is medewerkere bevoegd om identificerende persoonsgegevens te wijzigen
FUNCTION bevoegd
( p_mede_id IN NUMBER := NULL
)
RETURN BOOLEAN;

-- is persoon bij een onderzoek/monster de moeder van de foetus
PROCEDURE controleer_moeder_foetus
( p_pers_id IN NUMBER
, p_foet_id IN NUMBER
);

-- toon persoongegevens op flexibele manier
-- aangepast door JLO 01/2004
FUNCTION persoon_info
( p_mons_id IN NUMBER    := NULL
, p_onde_id IN NUMBER    := NULL
, p_pers_id IN NUMBER    := NULL
, p_lengte  IN VARCHAR2  := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( persoon_info, WNDS, WNPS, RNPS );

-- toon persoonsgegevens
FUNCTION info
( p_pers_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( info, WNDS, WNPS, RNPS );

-- Controleer Zisnr
procedure zisnr_controle
( p_zisnr in varchar2
);

-- Geef alle onderzoeken waar de persoon/foetus als betrokkene voorkomt met een van het geslacht afwijkende A-rol
FUNCTION onde_met_afw_betr_rol
( p_pers_id  IN NUMBER
, p_foet_id  IN NUMBER := NULL
, p_geslacht IN VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( onde_met_afw_betr_rol, WNDS, WNPS );

-- Controleer Burger-SofiNummer
PROCEDURE controleer_bsn
( p_pers_id              IN NUMBER := NULL
, p_bsn                  IN NUMBER
, p_bsn_geverifieerd     IN VARCHAR2
, p_controleer_uniciteit IN VARCHAR2 := 'N'
);

-- Mantis 283, toegevoegd:
-- Overloadprocedure, indien pers_id niet bekend is.
PROCEDURE controleer_bsn
(p_zisnr                IN VARCHAR2 := NULL
,p_bsn                  IN NUMBER
,p_bsn_geverifieerd     IN VARCHAR2
,p_controleer_uniciteit IN VARCHAR2 := 'N'
);

-- Geef opgemaakt Burger-SofiNummer (tbv. de rapporten)
FUNCTION bsn_opgemaakt
( p_pers_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( bsn_opgemaakt, WNDS, WNPS, RNPS );

-- Corrigeer Burger-SofiNummer uit ZIS als deze leeg is en Helix-waarde wel is gevuld
PROCEDURE corrigeer_bsn_uit_zis
( p_zis_locatie          IN VARCHAR2
, p_bsn_hlx              IN NUMBER
, x_bsn_zis              IN OUT NUMBER
, x_bsn_geverifieerd_zis IN OUT VARCHAR2
);

-- mantis 3068
PROCEDURE controleer_bsn
( p_pers_id              IN NUMBER
, p_zisnr                IN VARCHAR2
, p_bsn                  IN NUMBER
, p_bsn_geverifieerd     IN VARCHAR2
, p_controleer_uniciteit IN VARCHAR2
, p_achternaam           IN VARCHAR2
, p_geboortedatum        IN DATE
, p_geslacht             IN VARCHAR2
);

END KGC_PERS_00;
/



CREATE OR REPLACE PACKAGE BODY HELIX.KGC_PERS_00 IS
/*
   Wijzigingshistorie:
   ===============

   Bevindingnr     Wie   Version    Wanneer     Wat
   ---------------------------------------------------------------------------------------------------------
   Mantis 3068     APL   2          11-10-2016  Extra functie controleer_bsn toegevoegd tbv kgc_import
   Mantis 3068     APL   1.9        30-05-2016  Reverted for Release 8.11.4.7.
   Mantis 3068     ATS   1.8        08-04-2016  P_geboortedatum change from VARCHAR2 to DATE and layout adjustment  For release rework 8.11.0.3
   Mantis 3068     ATS   1.7        17-12-2015  BSN in XML bestand oppakken en verwerken geeft voor 1 situatie een onterechte foutmelding tijdens het verwerken van de persoonsgegevens.
*/

TYPE type_num_tab IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
g_max_zoekfamilie BINARY_INTEGER := 0;
g_tab_zoekfamilie type_num_tab;

g_max_zoekpersonen BINARY_INTEGER := 0;
g_tab_zoekpersonen type_num_tab;

FUNCTION formatteer_zoeknaam
( p_naam1 IN VARCHAR2
, p_naam2 IN VARCHAR2 := NULL
, p_naam3 IN VARCHAR2 := NULL
, p_naam4 IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_zoeknaam VARCHAR2(2000);
BEGIN
  -- NB.: verwijderen van spaties zit in kgc_formaat_00.simplify_string
  v_zoeknaam := kgc_formaat_00.simplify_string( p_naam1 );
  IF ( p_naam2 IS NOT NULL )
  THEN
    v_zoeknaam := v_zoeknaam
               || ';' || kgc_formaat_00.simplify_string( p_naam2 );
  END IF;
  IF ( p_naam3 IS NOT NULL )
  THEN
    v_zoeknaam := v_zoeknaam
               || ';' || kgc_formaat_00.simplify_string( p_naam3 );
  END IF;
  IF ( p_naam4 IS NOT NULL )
  THEN
    v_zoeknaam := v_zoeknaam
               || ';' || kgc_formaat_00.simplify_string( p_naam4 );
  END IF;
  v_zoeknaam := SUBSTR( v_zoeknaam, 1, 200 );
  RETURN( v_zoeknaam );
END formatteer_zoeknaam;

PROCEDURE  admin_meerling
(  p_naam  IN  VARCHAR2
,  p_geboortedatum  IN  DATE
,  p_zisnr  IN  VARCHAR2
,  x_aantal  OUT  NUMBER
,  x_pers_id  OUT  NUMBER
)
IS
  -- cursoren hebben identiek select-deel!
  CURSOR pers_cur_naam
  IS
    SELECT COUNT(*) aantal
    ,      MIN( pers_id ) min_pers_id
    FROM   kgc_personen
    WHERE  UPPER(p_naam) IN ( UPPER(achternaam), UPPER(aanspreken) )
    AND    geboortedatum = p_geboortedatum
    ;
  CURSOR pers_cur_zis
  IS
    SELECT COUNT(*) aantal
    ,      MIN( pers_id ) min_pers_id
    FROM   kgc_personen
    WHERE  zisnr = p_zisnr
    ;
  pers_rec pers_cur_naam%rowtype;
BEGIN
  IF ( p_zisnr IS NOT NULL )
  THEN
    OPEN  pers_cur_zis;
    FETCH pers_cur_zis
    INTO  pers_rec;
    CLOSE pers_cur_zis;
  ELSIF ( p_naam IS NOT NULL
   AND p_geboortedatum IS NOT NULL
     )
  THEN
    OPEN  pers_cur_naam;
    FETCH pers_cur_naam
    INTO  pers_rec;
    CLOSE pers_cur_naam;
  END IF;
  x_aantal := NVL( pers_rec.aantal, 0 );
  IF ( x_aantal = 1 )
  THEN
    x_pers_id := pers_rec.min_pers_id;
  END IF;
END admin_meerling;

FUNCTION geboren
 (  p_foet_id  IN  NUMBER
 ,  p_geboortedatum  IN  DATE  :=  NULL
 ,  p_geslacht  IN  VARCHAR2  :=  NULL
 ,  p_voornaam  IN  VARCHAR2  :=  NULL
 ,  p_voorvoegsel IN  VARCHAR2  :=  NULL
 ,  p_achternaam IN  VARCHAR2  :=  NULL
 ,  p_upd IN BOOLEAN := TRUE
 )
 RETURN  NUMBER
 IS
  v_pers_id NUMBER;
  CURSOR foet_cur
  IS
    SELECT foet.geslacht
    ,      foet.geplande_geboortedatum
    ,      pers.adres
    ,      pers.postcode
    ,      pers.woonplaats
    ,      pers.provincie
    ,      pers.land
    ,      pers.telefoon
    ,      pers.telefax
    ,      pers.rela_id
    ,      pers.verz_id
    ,      pers.verzekeringswijze
    ,      pers.verzekeringsnr
     FROM  kgc_personen pers
    ,      kgc_foetussen foet
    WHERE  foet.pers_id_moeder = pers.pers_id
      AND  foet.foet_id = p_foet_id
      AND  foet.pers_id_geboren_als IS NULL
      AND  foet.niet_geboren = 'N'
      FOR UPDATE OF pers_id_geboren_als
    ;
  foet_rec foet_cur%rowtype;
  v_aanspreken kgc_personen.aanspreken%type;
 BEGIN
  OPEN  foet_cur;
  FETCH foet_cur
  INTO  foet_rec;
  IF ( foet_cur%notfound )
  THEN
    CLOSE foet_cur;
    qms$errors.show_message
    ( p_mesg   => 'KGC-00035'
    , p_param0 => 'Controleer foetus gegevens'
    );
  ELSE
    kgc_formaat_00.standaard_aanspreken
      ( p_type        => 'PERS'
      , p_voorletters => p_voornaam
      , p_voorvoegsel => p_voorvoegsel
      , p_achternaam  => p_achternaam
      , p_gehuwd      => 'N'
      , p_geslacht    => NVL( p_geslacht, NVL( foet_rec.geslacht, 'O' ) )
      , p_prefix      => NULL
      , p_postfix     => NULL
      , x_aanspreken  => v_aanspreken
      );
    BEGIN
      SELECT kgc_pers_seq.nextval
        INTO v_pers_id
        FROM dual;
      INSERT INTO kgc_personen
        ( pers_id
        , achternaam
        , voorletters
        , voorvoegsel
        , aanspreken
        , geslacht
        , geboortedatum
        , adres
        , postcode
        , woonplaats
        , provincie
        , land
        , telefoon
        , telefax
        , rela_id
        , verz_id
        , verzekeringswijze
        , verzekeringsnr
        )
      VALUES
        ( v_pers_id
        , p_achternaam
        , p_voornaam
        , p_voorvoegsel
        , v_aanspreken
        , NVL( p_geslacht, NVL( foet_rec.geslacht, 'O' ) )
        , NVL( p_geboortedatum, foet_rec.geplande_geboortedatum )
        , foet_rec.adres
        , foet_rec.postcode
        , foet_rec.woonplaats
        , foet_rec.provincie
        , foet_rec.land
        , foet_rec.telefoon
        , foet_rec.telefax
        , foet_rec.rela_id
        , foet_rec.verz_id
        , foet_rec.verzekeringswijze
        , foet_rec.verzekeringsnr
        );
      IF ( p_upd )
      THEN
        UPDATE kgc_foetussen
           SET pers_id_geboren_als = v_pers_id
         WHERE CURRENT OF foet_cur;
      END IF;
      CLOSE foet_cur;
      COMMIT;
    EXCEPTION
      WHEN OTHERS
      THEN
        qms$errors.show_message
          ( p_mesg => 'KGC-00035'
          , p_param0 => SQLERRM
          );
      END;
  END IF;
  RETURN( v_pers_id );
END geboren;
--
PROCEDURE geboren
 (  p_foet_id  IN  NUMBER
 ,  p_geboortedatum  IN  DATE  :=  NULL
 ,  p_geslacht  IN  VARCHAR2  :=  NULL
 ,  p_voornaam  IN  VARCHAR2  :=  NULL
 ,  p_voorvoegsel IN  VARCHAR2  :=  NULL
 ,  p_achternaam IN  VARCHAR2  :=  NULL
 ,  p_upd IN BOOLEAN := TRUE
 )
 IS
  v_pers_id NUMBER;
 BEGIN
  v_pers_id := geboren
               ( p_foet_id       => p_foet_id
               , p_geboortedatum => p_geboortedatum
               , p_geslacht      => p_geslacht
               , p_voornaam      => p_voornaam
               , p_voorvoegsel   => p_voorvoegsel
               , p_achternaam    => p_achternaam
               , p_upd           => p_upd
               );
END geboren;

PROCEDURE niet_geboren
( p_pers_id IN NUMBER
)
IS
BEGIN
  UPDATE kgc_foetussen
  SET    pers_id_geboren_als = NULL
  WHERE  pers_id_geboren_als = p_pers_id
  ;
END niet_geboren;

PROCEDURE   koppel
  (   p_correct_pers_id   IN   NUMBER
  ,   p_koppellijst   IN   VARCHAR2
  ,   x_resultaat   OUT   VARCHAR2
  )
IS
  v_statement VARCHAR2(2000);
  v_result VARCHAR2(100);
  v_gevonden_data BOOLEAN := FALSE;
  breek_af EXCEPTION;

  v_koppellijst VARCHAR2(1000) := p_koppellijst;
  v_pers_id NUMBER; -- 1 uit koppellijst

  CURSOR tcol_cur
  IS
    SELECT DISTINCT -- misschien meer eigenaren
           ttab.table_name
    ,      tcol.column_name
    FROM   all_tab_columns tcol
    ,      all_tables ttab
    WHERE  ttab.owner = tcol.owner
    AND    ttab.table_name = tcol.table_name
    AND    tcol.column_name LIKE '%PERS_ID%'
    AND    ttab.table_name <> 'KGC_PERSONEN'
    ;

  CURSOR pers_cur
  ( p_pers_id IN NUMBER
  )
  IS
    SELECT zisnr
    ,      aanspreken
    ,      geslacht
    ,      geboortedatum
    FROM   kgc_personen
    WHERE  pers_id = p_pers_id
    ;
  pers_rec pers_cur%rowtype;

  CURSOR trig_cur
  IS
    SELECT table_name
    ,      trigger_name
    FROM   all_triggers
    WHERE  status = 'DISABLED'
    ;
  CURSOR dis_trg_cur
  IS
    SELECT COUNT(*)
    FROM   all_triggers
    WHERE  status = 'DISABLED'
    ;
  v_dis_triggers NUMBER := 0;

  v_resultaat VARCHAR2(2000);

  PROCEDURE res
  ( p_regel IN VARCHAR2
  )
  IS
    BEGIN
    IF ( p_regel IS NOT NULL )
    THEN
      IF ( v_resultaat IS NULL )
      THEN
        v_resultaat := p_regel;
      ELSE
        v_resultaat := v_resultaat||CHR(10)||p_regel;
      END IF;
    END IF;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      NULL;
  END res;
BEGIN
  OPEN  pers_cur( p_correct_pers_id );
  FETCH pers_cur
  INTO  pers_rec;
  IF ( pers_cur%FOUND )
  THEN
    CLOSE pers_cur;
    res( 'Aan persoon '||pers_rec.aanspreken
                 ||', '||pers_rec.zisnr
                 ||', '||TO_CHAR(pers_rec.geboortedatum,'dd-mm-yyyy')
                 ||'('||pers_rec.geslacht||')'
       ||' zijn de volgende persoonsgegevens gekoppeld:'
       );
  ELSE
    CLOSE pers_cur;
    res( 'Geen gegevens gevonden van de ''correcte'' persoon'
      || ' (pers_id='||TO_CHAR(p_correct_pers_id)||')'
       );
    RAISE breek_af;
  END IF;

  FOR tcol_rec IN tcol_cur
  LOOP
    v_statement := 'alter table '||tcol_rec.table_name||' disable all triggers;';
    BEGIN
      v_result := kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
    END;
    v_statement := 'begin'
        ||CHR(10)||'update '||tcol_rec.table_name
        ||CHR(10)||'set '||tcol_rec.column_name||' = '||TO_CHAR(p_correct_pers_id)
        ||CHR(10)||'where  '||tcol_rec.column_name||' in ('||v_koppellijst||');'
        ||CHR(10)||':result := sql%rowcount;'
        ||CHR(10)||'exception when dup_val_on_index then'
        ||CHR(10)||'delete from '||tcol_rec.table_name
        ||CHR(10)||'where  '||tcol_rec.column_name||' in ('||v_koppellijst||');'
        ||CHR(10)||':result := sql%rowcount;'
        ||CHR(10)||'end;'
                 ;
    BEGIN
      v_result := kgc_util_00.dyn_exec( v_statement );
      IF ( v_result > 0 )
      THEN
        v_gevonden_data := TRUE;
        res( 'Gegevens uit tabel '||tcol_rec.table_name||' omgezet.' );
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
        RAISE breek_af;
    END;
    v_statement := 'alter table '||tcol_rec.table_name||' enable all triggers;';
    BEGIN
      v_result := kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
    END;
    COMMIT;
  END LOOP;

  -- Tabellen gerelateerd aan kgc_entiteiten:
  -- Notities
  v_statement := 'alter table KGC_NOTITIES disable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;
  v_statement := 'begin'
      ||CHR(10)||'update KGC_NOTITIES'
      ||CHR(10)||'set id = '||TO_CHAR(p_correct_pers_id)
      ||CHR(10)||'where  id in ('||v_koppellijst||')'
      ||CHR(10)||'and entiteit = ''PERS'';'
      ||CHR(10)||':result := sql%rowcount;'
      ||CHR(10)||'exception when dup_val_on_index then'
      ||CHR(10)||'delete from KGC_NOTITIES'
      ||CHR(10)||'where  id in ('||v_koppellijst||');'
      ||CHR(10)||':result := sql%rowcount;'
      ||CHR(10)||'end;'
               ;
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
    IF ( v_result > 0 )
    THEN
      v_gevonden_data := TRUE;
      res( 'Gegevens uit tabel KGC_NOTITIES omgezet.' );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
      RAISE breek_af;
  END;
  v_statement := 'alter table KGC_NOTITIES enable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;
  -- Vertalingen
  v_statement := 'alter table KGC_VERTALINGEN disable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;
   v_statement := 'begin'
      ||CHR(10)||'update KGC_VERTALINGEN'
      ||CHR(10)||'set id = '||TO_CHAR(p_correct_pers_id)
      ||CHR(10)||'where  id in ('||v_koppellijst||')'
      ||CHR(10)||'and entiteit = ''PERS'';'
      ||CHR(10)||':result := sql%rowcount;'
      ||CHR(10)||'exception when dup_val_on_index then'
      ||CHR(10)||'delete from KGC_VERTALINGEN'
      ||CHR(10)||'where  id in ('||v_koppellijst||');'
      ||CHR(10)||':result := sql%rowcount;'
      ||CHR(10)||'end;'
               ;
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
    IF ( v_result > 0 )
    THEN
      v_gevonden_data := TRUE;
      res( 'Gegevens uit tabel KGC_VERTALINGEN omgezet.' );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
      RAISE breek_af;
  END;
  v_statement := 'alter table KGC_VERTALINGEN enable all triggers;';
  BEGIN
    v_result := kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
  END;

  IF ( NOT v_gevonden_data )
  THEN
    res( 'Er waren geen onderliggende gegevens op te koppelen.' );
  END IF;

  -- verwijder gekoppelde personen
  WHILE ( REPLACE(v_koppellijst,',','') IS NOT NULL )
  LOOP
    IF ( INSTR( v_koppellijst, ',' ) = 0 )
    THEN
      v_pers_id := v_koppellijst;
      v_koppellijst := NULL;
    ELSE
      v_pers_id := SUBSTR( v_koppellijst
                         , 1
                         , INSTR( v_koppellijst, ',' ) - 1
                         );
    END IF;
    OPEN  pers_cur( v_pers_id );
    FETCH pers_cur
    INTO  pers_rec;
    IF ( pers_cur%FOUND )
    THEN
      CLOSE pers_cur;
      res( 'Persoon '||pers_rec.aanspreken
               ||', '||pers_rec.zisnr
               ||', '||TO_CHAR(pers_rec.geboortedatum,'dd-mm-yyyy')
               ||'('||pers_rec.geslacht||')'
         ||' verwijderen...'
         );

      v_statement := 'delete from kgc_personen where pers_id = '||TO_CHAR(v_pers_id)||';';
      BEGIN
        kgc_util_00.dyn_exec( v_statement );
        res( 'OK.' );
      EXCEPTION
        WHEN OTHERS
        THEN
          res( 'Fout in uitvoering'||CHR(10)||SQLERRM );
          RAISE breek_af;
      END;
    ELSE
      CLOSE pers_cur;
      res( 'Geen gegevens gevonden van de ''gekoppelde'' persoon'
        || ' (pers_id='||TO_CHAR(p_correct_pers_id)||')'
         );
    END IF;

    v_koppellijst := SUBSTR( v_koppellijst
                           , INSTR( v_koppellijst, ',' ) + 1
                           );
  END LOOP;
  COMMIT;

  FOR trig_rec IN trig_cur
  LOOP
    v_statement := 'alter table '||trig_rec.table_name||' enable all triggers;';
    BEGIN
      v_result := kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END LOOP;

  OPEN  dis_trg_cur;
  FETCH dis_trg_cur
  INTO  v_dis_triggers;
  CLOSE dis_trg_cur;
  IF ( NVL( v_dis_triggers, 0 ) > 0 )
  THEN
    res( 'Er is/zijn nog '||TO_CHAR(v_dis_triggers)||' "disabled" triggers. Waarschuw de applicatiebeheerder!' );
  END IF;
  x_resultaat := v_resultaat;
EXCEPTION
  WHEN breek_af
  THEN
    x_resultaat := v_resultaat;
    ROLLBACK;
  WHEN OTHERS
  THEN
    x_resultaat := SUBSTR( v_resultaat||CHR(10)||SQLERRM
                         , 1, 2000
                         );
    ROLLBACK;
END koppel;
-- Added for mantis 10927 SKA 01-04-2015  start
FUNCTION rekendatum
( p_geboortedatum      DATE
, p_part_geboortedatum VARCHAR2
)
RETURN DATE
IS
  v_geb_datum DATE;
  v_part_datum VARCHAR2(10);
BEGIN
  IF ( p_geboortedatum IS NOT NULL )
  THEN
    v_geb_datum := p_geboortedatum;
  ELSE
    v_part_datum := p_part_geboortedatum;
    v_part_datum := REPLACE( v_part_datum, '00-', '01-' );
    BEGIN
      v_geb_datum := TO_DATE( v_part_datum, 'dd-mm-yyyy' );
    EXCEPTION
      WHEN OTHERS
      THEN
        v_geb_datum := NULL;
    END;
  END IF;
  RETURN( v_geb_datum );
END rekendatum;
-- Added for mantis 10927 SKA 01-04-2015  end
FUNCTION rekendatum
( p_pers_id IN NUMBER
)
RETURN DATE
IS
  v_geb_datum DATE;
  v_part_datum VARCHAR2(10);
  CURSOR pers_cur
  IS
    SELECT geboortedatum
    ,      part_geboortedatum
    FROM   kgc_personen
    WHERE  pers_id = p_pers_id
    ;
  pers_rec pers_cur%rowtype;
BEGIN
  OPEN  pers_cur;
  FETCH pers_cur
  INTO  pers_rec;
  CLOSE pers_cur;
  /*IF ( pers_rec.geboortedatum IS NOT NULL )
  THEN
    v_geb_datum := pers_rec.geboortedatum;
  ELSE
    v_part_datum := pers_rec.part_geboortedatum;
    v_part_datum := REPLACE( v_part_datum, '00-', '01-' );
    BEGIN
      v_geb_datum := TO_DATE( v_part_datum, 'dd-mm-yyyy' );
    EXCEPTION
      WHEN OTHERS
      THEN
        v_geb_datum := NULL;
    END;
  END IF;
  RETURN( v_geb_datum );  */   --commened for mantis 10927 SKA 01-04-2015
  RETURN rekendatum( p_geboortedatum      => pers_rec.geboortedatum
                   , p_part_geboortedatum => pers_rec.part_geboortedatum
                   ); -- Added for mantis 10927 SKA 01-04-2015

END rekendatum;

FUNCTION  familieleden
( p_fami_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  CURSOR pers_cur
  IS
    SELECT UPPER(SUBSTR(pers.achternaam,1,10)||';') naam
    FROM   kgc_personen pers
    ,      kgc_familie_leden fale
    WHERE  pers.pers_id = fale.pers_id
    AND    fale.fami_id = p_fami_id
    GROUP BY SUBSTR(pers.achternaam,1,10)
    ;
  PROCEDURE voeg_toe
  ( b_naam IN VARCHAR2
  )
  IS
  BEGIN
    IF ( b_naam IS NOT NULL )
    THEN
      IF ( v_return IS NULL )
      THEN
        v_return := b_naam;
      ELSE
        v_return := v_return||CHR(10)||b_naam;
      END IF;
    END IF;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      NULL;
  END voeg_toe;
BEGIN
  FOR r IN pers_cur
  LOOP
    voeg_toe( r.naam );
  END LOOP;
  RETURN( v_return );
END familieleden;

FUNCTION  families
( p_pers_id IN NUMBER   := NULL
, p_lengte  IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  CURSOR fami_cur
  IS
    SELECT UPPER(fami.familienummer||';'||fami.naam||';') naam, UPPER(fami.familienummer) naam_kort
    FROM   kgc_families fami
    ,      kgc_familie_leden fale
    WHERE  fami.fami_id = fale.fami_id
    AND    fale.pers_id = p_pers_id
    ORDER BY fami.naam
    ;
  PROCEDURE voeg_toe
  ( b_naam      IN VARCHAR2
  , b_naam_kort IN VARCHAR2
  )
  IS
  BEGIN
    IF ( b_naam IS NOT NULL )
    THEN
      -- in het verkort aanmeld sherm wil men bij meerdere families
      -- alleen de eerste letter te zien krijgen.
      IF p_lengte = 'K'
      THEN
        IF ( v_return IS NULL )
        THEN
          v_return := b_naam_kort;
        ELSE
          v_return := v_return||';'||b_naam_kort;
        END IF;
      ELSE
        IF ( v_return IS NULL )
        THEN
          v_return := b_naam;
        ELSE
          v_return := v_return||CHR(10)||b_naam;
        END IF;
      END IF;
    END IF;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      NULL;
  END voeg_toe;
BEGIN
  FOR r IN fami_cur
  LOOP
    voeg_toe( r.naam, r.naam_kort );
  END LOOP;
  RETURN( v_return );
END families;

PROCEDURE   zoekfamilie
( p_pers_id   IN   NUMBER
, p_fami_id   IN   NUMBER
)
IS
BEGIN
  UPDATE kgc_personen pers
  SET    zoekfamilie = SUBSTR( UPPER(kgc_pers_00.families (p_pers_id,null) ), 1, 200 )
  WHERE  pers.pers_id = p_pers_id
  ;

  UPDATE kgc_families fami
  SET    zoekpersonen = SUBSTR( UPPER(kgc_pers_00.familieleden (p_fami_id) ), 1, 749 )
  WHERE  fami.fami_id = p_fami_id
  ;
END zoekfamilie;

-- Mantis 3111 2010-01-07 gwe
-- Nieuwe procedure voor updaten zoekpersonen en zoekfamilie
PROCEDURE toevoeg_zoekfamilie
( p_pers_id   IN   NUMBER
)
IS
  l_nieuw BOOLEAN;
BEGIN
  l_nieuw := TRUE;
  FOR i IN 1 .. g_max_zoekfamilie
  LOOP
    IF g_tab_zoekfamilie(i) = p_pers_id
    THEN
      l_nieuw := FALSE;
    END IF;
  END LOOP;
  IF l_nieuw
  THEN
    g_max_zoekfamilie := g_max_zoekfamilie + 1;
    g_tab_zoekfamilie(g_max_zoekfamilie) := p_pers_id;
  END IF;
END;

PROCEDURE update_zoekfamilie
IS
BEGIN
  FOR i IN 1 .. g_max_zoekfamilie
  LOOP
    UPDATE kgc_personen pers
    SET    zoekfamilie = SUBSTR( UPPER(kgc_pers_00.families (g_tab_zoekfamilie(i),null) ), 1, 200 )
    WHERE  pers.pers_id = g_tab_zoekfamilie(i)
    ;
  END LOOP;
  -- opschonen tabel
  g_max_zoekfamilie := 0;
  g_tab_zoekfamilie.delete;
END update_zoekfamilie;

PROCEDURE toevoeg_zoekpersonen
( p_fami_id   IN   NUMBER
)
IS
  l_nieuw BOOLEAN;
BEGIN
  l_nieuw := TRUE;
  FOR i IN 1 .. g_max_zoekpersonen
  LOOP
    IF g_tab_zoekpersonen(i) = p_fami_id
    THEN
      l_nieuw := FALSE;
    END IF;
  END LOOP;
  IF l_nieuw
  THEN
    g_max_zoekpersonen := g_max_zoekpersonen + 1;
    g_tab_zoekpersonen(g_max_zoekpersonen) := p_fami_id;
  END IF;
END;

PROCEDURE update_zoekpersonen
IS
BEGIN
  FOR i IN 1 .. g_max_zoekpersonen
  LOOP
    UPDATE kgc_families fami
    SET    zoekpersonen = SUBSTR( UPPER(kgc_pers_00.familieleden (g_tab_zoekpersonen(i)) ), 1, 749 )
    WHERE  fami.fami_id = g_tab_zoekpersonen(i)
    ;
  END LOOP;
  -- opschonen tabel
  g_max_zoekpersonen := 0;
  g_tab_zoekpersonen.delete;
END update_zoekpersonen;

FUNCTION bevoegd
( p_mede_id IN NUMBER := NULL
)
RETURN BOOLEAN
IS
  v_return BOOLEAN := TRUE;
  CURSOR spwa_cur
  ( b_mede_id IN NUMBER
  )
  IS
    SELECT NULL
    FROM   kgc_systeem_par_waarden spwa
    ,      kgc_systeem_parameters sypa
    WHERE  sypa.sypa_id = spwa.sypa_id
    AND    sypa.code = 'BEVOEGD_WIJZIG_PERSOON'
    AND  ( spwa.mede_id = b_mede_id
        OR b_mede_id IS NULL
         )
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  spwa_cur( NULL);
  FETCH spwa_cur
  INTO  v_dummy;
  IF ( spwa_cur%found )
  THEN
    CLOSE spwa_cur;
    OPEN  spwa_cur( NVL( p_mede_id, kgc_mede_00.medewerker_id ) );
    FETCH spwa_cur
    INTO  v_dummy;
    IF ( spwa_cur%notfound )
    THEN
      CLOSE spwa_cur;
      v_return := FALSE;
    ELSE
      CLOSE spwa_cur;
      v_return := TRUE;
    END IF;
  ELSE
    CLOSE spwa_cur;
    v_return := TRUE;
  END IF;
  RETURN( v_return );
END bevoegd;

PROCEDURE controleer_moeder_foetus
( p_pers_id IN NUMBER
, p_foet_id IN NUMBER
)
IS
  CURSOR foet_cur
  IS
    SELECT pers_id_moeder
    FROM   kgc_foetussen
    WHERE  foet_id = p_foet_id
    ;
  v_pers_id_moeder NUMBER;
BEGIN
  IF ( p_pers_id IS NOT NULL
   AND p_foet_id IS NOT NULL
     )
  THEN
    OPEN  foet_cur;
    FETCH foet_cur
    INTO  v_pers_id_moeder;
    CLOSE foet_cur;
    IF ( v_pers_id_moeder <> p_pers_id )
    THEN
      raise_application_error
      ( -20009
      , 'Foetus en moeder horen niet bij elkaar.'
      );
    END IF;
  END IF;
END controleer_moeder_foetus;

FUNCTION info
( p_pers_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return  VARCHAR2(2000);
  v_pers_id NUMBER;
BEGIN
  v_pers_id := p_pers_id;
  IF ( v_pers_id IS NOT NULL )
  THEN
    v_return:= kgc_pers_00.persoon_info(p_pers_id => v_pers_id);
  END IF;
  RETURN( v_return );
END info;
--
FUNCTION persoon_info
( p_mons_id IN NUMBER   := NULL
, p_onde_id IN NUMBER   := NULL
, p_pers_id IN NUMBER   := NULL
, p_lengte  IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_persoon   VARCHAR2(2000);
  v_pers_id   NUMBER;
  v_foet_id   NUMBER;
  v_sypa_code VARCHAR2(100);
  v_mede_id   NUMBER;
  v_kafd_id   NUMBER;
  v_ongr_id   NUMBER;
  --
  CURSOR mons_cur
  IS
    SELECT mons_id
    ,      pers_id
    ,      foet_id
    ,      kafd_id
    ,      ongr_id
    FROM   kgc_monsters
    WHERE  mons_id = p_mons_id
    ;
  mons_rec mons_cur%rowtype;

  CURSOR onde_cur
  IS
    SELECT pers_id
    ,      foet_id
    ,      kafd_id
    ,      ongr_id
    ,      taal_id
    ,      onderzoeknr
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
  onde_rec onde_cur%rowtype;
  --
  CURSOR pers_cur
  IS
  SELECT achternaam
  ,      aanspreken
  ,      geboortedatum
  ,      part_geboortedatum
  ,      geslacht
  ,      DECODE( achternaam_partner
                 , NULL, SUBSTR(achternaam,1,3)
                 , SUBSTR(achternaam,1,3)||'-'||SUBSTR(achternaam_partner,1,3)
                 ) afkorting
  ,      DECODE( overleden
                 , 'J', 'Overleden '||TO_CHAR(overlijdensdatum,'dd-mm-yyyy')
                 , NULL
                 ) datum_overleden
  FROM   kgc_personen
  WHERE  pers_id = v_pers_id
    ;
  pers_rec pers_cur%rowtype;
  --
BEGIN
  IF ( p_mons_id IS NULL
   AND p_onde_id IS NULL
   AND p_pers_id IS NULL
     )
  THEN
    RETURN( NULL );
  END IF;
  v_mede_id := kgc_mede_00.medewerker_id;
  v_sypa_code := 'PERSOONSGEGEVENS_'||UPPER( NVL( p_lengte, 'LANG' ) );
  v_pers_id := p_pers_id;
  IF ( p_mons_id IS NOT NULL )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  mons_rec;
    v_pers_id := mons_rec.pers_id;
    v_foet_id := mons_rec.foet_id;
    v_kafd_id := mons_rec.kafd_id;
    v_ongr_id := mons_rec.ongr_id;
    CLOSE mons_cur;
  ELSIF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  onde_rec;
    v_pers_id := onde_rec.pers_id;
    v_foet_id := onde_rec.foet_id;
    v_kafd_id := onde_rec.kafd_id;
    v_ongr_id := onde_rec.ongr_id;
    CLOSE onde_cur;
  END IF;
  -- systeem waarde ophalen.
  BEGIN
    v_persoon := kgc_sypa_00.standaard_waarde
                 ( p_parameter_code => v_sypa_code
                 , p_kafd_id => v_kafd_id
                 , p_ongr_id => v_ongr_id
                 , p_mede_id => v_mede_id
                 );
  EXCEPTION
  WHEN OTHERS
  THEN
    -- als de systeem parameter niet bestaat de persoon_info van voor 2004 geven.
    v_persoon := '<AANSPREKEN> (<GESLACHT>) <GEBOORTEDATUM> <OVERLEDEN> <FOETUS> <ZWANGERSCHAP>';
  END;

  OPEN  pers_cur;
  FETCH pers_cur
  INTO  pers_rec;
  CLOSE pers_cur;
  v_persoon:= REPLACE(v_persoon, '<achternaam>', '<ACHTERNAAM>');
  v_persoon:= REPLACE(v_persoon, '<ACHTERNAAM>', pers_rec.achternaam);
  v_persoon:= REPLACE(v_persoon, '<aanspreken>', '<AANSPREKEN>');
  v_persoon:= REPLACE(v_persoon, '<AANSPREKEN>', pers_rec.aanspreken);
  v_persoon:= REPLACE(v_persoon, '<geboortedatum>', '<GEBOORTEDATUM>');
  v_persoon:= REPLACE(v_persoon, '<GEBOORTEDATUM>', NVL( TO_CHAR( pers_rec.geboortedatum, 'dd-mm-yyyy' ), pers_rec.part_geboortedatum ));
  v_persoon:= REPLACE(v_persoon, '<overleden>', '<OVERLEDEN>');
  v_persoon:= REPLACE(v_persoon, '<OVERLEDEN>', pers_rec.datum_overleden);
  v_persoon:= REPLACE(v_persoon, '<geslacht>', '<GESLACHT>');
  v_persoon:= REPLACE(v_persoon, '<GESLACHT>', kgc_vert_00.vertaling('PERS_GESL',NULL,pers_rec.geslacht,onde_rec.taal_id));
  v_persoon:= REPLACE(v_persoon, '<initialen>', '<INITIALEN>');
  v_persoon:= REPLACE(v_persoon, '<INITIALEN>', pers_rec.afkorting);
  v_persoon:= REPLACE(v_persoon, '<onderzoeknr>', '<ONDERZOEKNR>');
  v_persoon:= REPLACE(v_persoon, '<ONDERZOEKNR>', onde_rec.onderzoeknr );

  v_persoon:= REPLACE(v_persoon, '<foetus>', '<FOETUS>');
  IF INSTR( v_persoon, '<FOETUS>') > 0
  THEN
    v_persoon := REPLACE(v_persoon, '<FOETUS>', kgc_foet_00.foetus_info( p_foet_id => v_foet_id ));
  END IF;

  v_persoon:= REPLACE(v_persoon, '<zwangerschap>', '<ZWANGERSCHAP>');
  IF INSTR(v_persoon, '<ZWANGERSCHAP>') > 0
  THEN
    v_persoon := REPLACE(v_persoon, '<ZWANGERSCHAP>', kgc_leef_00.omschrijving( p_mons_id => p_mons_id ));
  END IF;

  v_persoon:= REPLACE(v_persoon, '<bsn>', '<BSN>');
  IF INSTR(v_persoon, '<BSN>') > 0
  THEN
    v_persoon := REPLACE(v_persoon, '<BSN>', kgc_pers_00.bsn_opgemaakt( p_pers_id => v_pers_id ));
  END IF;

  RETURN( LTRIM( v_persoon ) );
END persoon_info
;

procedure zisnr_controle
( p_zisnr in varchar2
)
is
  v_zis_locatie varchar2(100);
begin
  -- Voor Maastricht moet een controle op zisnummer ingebouwd worden
  begin
    v_zis_locatie := kgc_sypa_00.standaard_waarde
                      ( p_parameter_code => 'ZIS_LOCATIE'
                      , p_mede_id => kgc_mede_00.medewerker_id
                      );
  exception
    when others
    then
      v_zis_locatie := null;
  end;
  if ( v_zis_locatie = 'MAASTRICHT' )
  then
    kgc_maastricht.zisnr_controle( p_zisnr => p_zisnr );
  end if;
end zisnr_controle;

-- Geef alle onderzoeken waar de persoon/foetus als betrokkene voorkomt met een van het geslacht afwijkende A-rol
FUNCTION onde_met_afw_betr_rol
( p_pers_id  IN NUMBER
, p_foet_id  IN NUMBER := NULL
, p_geslacht IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000) := NULL;
  CURSOR onbe_cur
        ( b_pers_id  IN NUMBER
        , b_foet_id  IN NUMBER
        , b_geslacht IN VARCHAR2
        )
  IS
  SELECT DISTINCT
         onde.onderzoeknr
    FROM kgc_onderzoeken           onde
       , kgc_onderzoek_betrokkenen onbe
   WHERE onde.onde_id = onbe.onde_id
     AND onbe.pers_id = b_pers_id
     AND (
           ( b_foet_id IS     NULL AND onbe.foet_id IS NULL )
         OR
           ( b_foet_id IS NOT NULL AND onbe.foet_id = b_foet_id )
         )
     AND (
           ( b_geslacht = 'M' AND NVL( onbe.rol, '???' ) = 'A2' )
         OR
           ( b_geslacht = 'V' AND NVL( onbe.rol, '???' ) = 'A1' )
         )
   ORDER BY onde.onderzoeknr ASC
  ;
BEGIN
  FOR r IN onbe_cur ( p_pers_id, p_foet_id, p_geslacht )
  LOOP
    IF ( v_return IS NULL )
    THEN
      v_return := r.onderzoeknr;
    ELSE
      v_return := v_return||', '||r.onderzoeknr;
    END IF;
  END LOOP;
  RETURN( v_return );
END onde_met_afw_betr_rol;

PROCEDURE controleer_bsn
( p_pers_id              IN NUMBER := NULL
, p_bsn                  IN NUMBER
, p_bsn_geverifieerd     IN VARCHAR2
, p_controleer_uniciteit IN VARCHAR2 := 'N'
)
IS
BEGIN
   controleer_bsn(p_pers_id              => p_pers_id
                 ,p_zisnr                => NULL
                 ,p_bsn                  => p_bsn
                 ,p_bsn_geverifieerd     => p_bsn_geverifieerd
                 ,p_controleer_uniciteit => p_controleer_uniciteit
                 ,p_achternaam           => NULL
                 ,p_geboortedatum        => NULL
                 ,p_geslacht             => NULL
                 );

END controleer_bsn;

-- Mantis 283, toegevoegd:
-- Overloadprocedure, indien pers_id niet bekend is.
-- Deze dan zoeken op basis van zisnr, indien gevuld,
-- en anders op basis van naam, geb.datum en geslacht.
-- Wordt o.a. gebruikt in kgc_import.
PROCEDURE controleer_bsn
(p_zisnr                IN VARCHAR2 := NULL
,p_bsn                  IN NUMBER
,p_bsn_geverifieerd     IN VARCHAR2
,p_controleer_uniciteit IN VARCHAR2 := 'N'
) IS
BEGIN
   controleer_bsn(p_pers_id              => NULL
                 ,p_zisnr                => p_zisnr
                 ,p_bsn                  => p_bsn
                 ,p_bsn_geverifieerd     => p_bsn_geverifieerd
                 ,p_controleer_uniciteit => p_controleer_uniciteit
                 ,p_achternaam           => NULL
                 ,p_geboortedatum        => NULL
                 ,p_geslacht             => NULL
                 );
END controleer_bsn;

-- Geef opgemaakt Burger-SofiNummer (tbv. de rapporten)
FUNCTION bsn_opgemaakt
( p_pers_id IN NUMBER
)
RETURN VARCHAR2
IS
  CURSOR pers_cur
         ( b_pers_id IN NUMBER
         )
  IS
  SELECT pers.bsn
       , pers.bsn_geverifieerd
    FROM kgc_personen pers
   WHERE pers.pers_id = b_pers_id
  ;
  pers_rec pers_cur%ROWTYPE;
  v_return VARCHAR2(9) := NULL;
BEGIN
  OPEN  pers_cur ( p_pers_id );
  FETCH pers_cur
  INTO  pers_rec;
  IF pers_cur%FOUND
  THEN
    IF ( pers_rec.bsn IS NULL )
    THEN
      v_return := '-';
    ELSIF ( pers_rec.bsn_geverifieerd = 'N' )
    THEN
      v_return := '-';
    ELSE
      v_return := TO_CHAR( pers_rec.bsn, 'FM099999999' );
    END IF;
  END IF;
  CLOSE pers_cur;
  RETURN( v_return );
END bsn_opgemaakt;

-- Corrigeer Burger-SofiNummer uit ZIS als deze leeg is en Helix-waarde wel is gevuld
PROCEDURE corrigeer_bsn_uit_zis
( p_zis_locatie          IN VARCHAR2 -- NIJMEGEN, UTRECHT of MAASTRICHT
, p_bsn_hlx              IN NUMBER
, x_bsn_zis              IN OUT NUMBER
, x_bsn_geverifieerd_zis IN OUT VARCHAR2
)
IS
BEGIN
  IF ( x_bsn_zis IS NULL AND p_bsn_hlx IS NOT NULL )
  THEN -- BSN in ZIS leeg en in Helix gevuld
    x_bsn_geverifieerd_zis := 'N'; -- BSN_geverifieerd wordt in helix altijd op 'N' gezet
    IF p_zis_locatie IN ( 'UTRECHT' )
    THEN
      x_bsn_zis := p_bsn_hlx; -- BSN blijft in helix op oude waarde staan
    ELSE
      NULL; -- BSN wordt in helix leeg gemaakt
    END IF;
  END IF;
END corrigeer_bsn_uit_zis;

-- mantis 3068 - nieuwe overloaded procedure
-- aanroep:
-- 1. door controleer_bsn in deze package met p_achternaam, p_geboortedatum en p_geslacht NULL
-- 2. in kgc_import
PROCEDURE controleer_bsn
( p_pers_id              IN NUMBER
, p_zisnr                IN VARCHAR2
, p_bsn                  IN NUMBER     
, p_bsn_geverifieerd     IN VARCHAR2   
, p_controleer_uniciteit IN VARCHAR2
, p_achternaam           IN VARCHAR2
, p_geboortedatum        IN DATE
, p_geslacht             IN VARCHAR2
)
IS        
  CURSOR pers_cur
         ( b_bsn     IN NUMBER
         , b_pers_id IN NUMBER  
         , b_zisnr   IN VARCHAR2
         )
  IS
  SELECT pers.pers_id
       , pers.zisnr
       , pers.achternaam
       , pers.geboortedatum
       , pers.geslacht
    FROM kgc_personen pers
   WHERE pers.bsn = b_bsn
   ORDER BY DECODE( pers.pers_id
                  , p_pers_id, 1
                  , DECODE( pers.zisnr
                          , p_zisnr, 2
                          , 3 -- de rest
                          )
                  )
  ;
  
  v_bsn       VARCHAR2(4000);
  v_factor    NUMBER;
  v_totaal    NUMBER;                
  v_pers_info VARCHAR2(4000) := NULL;
  v_dubbel    VARCHAR2(4000) := NULL;
  v_char      CHAR(1);
  v_pers_ok   BOOLEAN;
  v_count_ok  NUMBER := 0;
BEGIN
  IF ( p_bsn IS NOT NULL )
  THEN
    -- Controleer of bsn meer dan 9 cijfers bevat; dan kan deze per definitie niet aan de 11-proef voldoen:
    v_bsn := TO_CHAR( p_bsn ); 
    IF LENGTH( v_bsn ) > 9
    THEN
      qms$errors.show_message
        ( p_mesg => 'KGC-11312'
        , p_param0 => v_bsn
        );
    END IF;
    -- Controleer bsn op 11-proef:      
    -- Als de 9 cijfers worden aangeduid met ABCDEFGHI, dan moet gelden: 
    -- ( 9*A + 8*B + 7*C + 6*D + 5*E + 4*F + 3*G + 2*H - I) / 11 levert rest 0. 
    v_bsn    := TO_CHAR( p_bsn, 'FM099999999' ); 
    v_factor := 9;
    v_totaal := 0;
    FOR i IN 1..8
    LOOP          
      v_totaal := v_totaal + ( v_factor * substr( v_bsn, i, 1 ) );
      v_factor := v_factor - 1;
    END LOOP; 
    v_totaal := v_totaal - substr( v_bsn, 9, 1 );
    IF ( MOD( v_totaal, 11 ) <> 0 )
    THEN
      qms$errors.show_message
        ( p_mesg => 'KGC-11312'
        , p_param0 => v_bsn
        );
    END IF;
    -- Controleer of bsn reeds bij een (andere) persoon geregistreerd is
    -- probleem hier is dat er meerdere personen kunnen zijn die aan de criteria kunnen voldoen
    -- 1 is OK, daarna echter niet meer!
    -- aangezien pers_id en zisnr uniek zijn, wordt hierop gesorteerd, deze komen eerst
    IF ( NVL( p_controleer_uniciteit, 'N' ) = 'J' )
    THEN
      FOR r IN pers_cur( p_bsn, p_pers_id, p_zisnr )
      LOOP
	    -- mantis 3068: indien p_achternaam, p_geboortedatum, p_geslacht gevuld, en overeenkomstig de gevonden persoon,
      -- dan is deze persoon bekend en moet er geen foutmelding gegeven worden (dan wordt deze namelijk gekoppeld)
      -- idem voor pers-id en ZISnr
        v_pers_ok := FALSE;
        IF p_pers_id = r.pers_id
        THEN
          v_pers_ok := TRUE;
        ELSIF p_zisnr = r.zisnr 
        THEN
          v_pers_ok := TRUE;
        ELSIF UPPER(r.achternaam) = UPPER(p_achternaam) AND 
              TRUNC(r.geboortedatum) = TRUNC(p_geboortedatum) AND 
              r.geslacht = p_geslacht
        THEN
           v_pers_ok := TRUE;
        END IF;
        IF v_count_ok = 1 OR NOT v_pers_ok THEN -- dwz: de 2e naar een correcte of iedere foute
          v_pers_info := kgc_pers_00.persoon_info
                           ( p_mons_id => NULL
                           , p_onde_id => NULL
                           , p_pers_id => r.pers_id
                           , p_lengte  => 'LANG'
                           );
          IF r.zisnr IS NOT NULL
          THEN  
             v_pers_info := v_pers_info||' [zisnr: '||r.zisnr||']'; 
          ELSE
             v_pers_info := v_pers_info||' [zisnr onbekend]'; 
          END IF;
          IF ( v_dubbel IS NULL )
          THEN
            v_dubbel := v_pers_info;
          ELSE
            v_dubbel := v_dubbel||CHR(10)||v_pers_info;
          END IF;
        ELSIF v_pers_ok THEN
          v_count_ok := v_count_ok + 1;
        END IF;
      END LOOP;
      IF v_dubbel IS NOT NULL
      THEN
        qms$errors.show_message
          ( p_mesg   => 'KGC-11314'
          , p_param0 => v_bsn
          , p_param1 => v_dubbel
          );
      END IF;
    END IF;  
  ELSE
    -- Bij leeg bsn moet bsn_geverifieerd N zijn:
    IF ( NVL( p_bsn_geverifieerd, 'N' ) = 'J' )
    THEN 
      qms$errors.show_message
        ( p_mesg => 'KGC-11313'
        );
    END IF;
  END IF;
END controleer_bsn;

END kgc_pers_00;
/
