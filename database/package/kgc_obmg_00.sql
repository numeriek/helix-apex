CREATE OR REPLACE PACKAGE "HELIX"."KGC_OBMG_00" IS
-- PL/SQL Specification
PROCEDURE zet_herinnering
  ( p_obmg_id  IN  NUMBER
  , p_weken    IN  NUMBER
  , p_datum    IN  DATE
  );
  FUNCTION referentie
  ( p_onbe_id  IN  NUMBER
  ) RETURN VARCHAR2;
  PROCEDURE verzoekmg_meld_af
  ( p_obmg_id  IN  NUMBER
  , p_weken    IN  NUMBER
  );
  PROCEDURE herinnermg_meld_af
  ( p_obmg_id  IN  NUMBER
  , p_weken    IN  NUMBER
  );

END KGC_OBMG_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OBMG_00" IS
PROCEDURE zet_herinnering
  ( p_obmg_id  IN  NUMBER
  , p_weken    IN  NUMBER
  , p_datum    IN  DATE
  ) IS
    CURSOR obmg_cur( p_obmg_id IN NUMBER ) IS
      SELECT obmg.onbe_id
      ,      obmg.rela_id
      ,      obmg.herinnering
      FROM   kgc_onbe_med_gegevens  obmg
      WHERE  obmg.obmg_id = p_obmg_id
      FOR UPDATE OF obmg.herinnering
      ;
    obmg_rec  obmg_cur%ROWTYPE;
    v_herinnering  DATE;
  -- PL/SQL Block
BEGIN
    IF  p_obmg_id      IS NOT NULL
    AND NVL(p_weken,0) >= 1
    THEN
      OPEN  obmg_cur( p_obmg_id => p_obmg_id );
      FETCH obmg_cur
      INTO  obmg_rec;
      v_herinnering := p_datum + ( p_weken * 7 );
      UPDATE kgc_onbe_med_gegevens
      SET    herinnering = TRUNC( v_herinnering )
      WHERE  CURRENT OF obmg_cur;
      CLOSE obmg_cur;
    END IF;
    COMMIT;
  END zet_herinnering;

  FUNCTION referentie
  ( p_onbe_id  IN  NUMBER
  ) RETURN VARCHAR2 IS
    CURSOR onbe_cur( p_onbe_id  IN  NUMBER ) IS
      SELECT mede.code||'/'||onde.onderzoeknr  referentie
      FROM   kgc_medewerkers            mede
      ,      kgc_onderzoeken            onde
      ,      kgc_onderzoek_betrokkenen  onbe
      WHERE  onbe.onbe_id = p_onbe_id
      AND    onbe.onde_id = onde.onde_id
      AND    mede.mede_id = onde.mede_id_beoordelaar
      ;
    onbe_rec  onbe_cur%ROWTYPE;
  BEGIN
    IF  p_onbe_id IS NOT NULL
    THEN
      OPEN  onbe_cur( p_onbe_id => p_onbe_id );
      FETCH onbe_cur
      INTO  onbe_rec;
      CLOSE onbe_cur;
    END IF;
    RETURN onbe_rec.referentie;
  END referentie;

  PROCEDURE verzoekmg_meld_af
  ( p_obmg_id  IN  NUMBER
  , p_weken    IN  NUMBER
  ) IS
    CURSOR verz_cur IS
      SELECT verz.rela_id
      ,      verz.onbe_id
      ,      verz.onde_id
      ,      verz.pers_id
      ,      verz.kafd_id
      ,      verz.geadresseerde
      ,      verz.eerder_geprint
      FROM   kgc_verzoekbrief_mg_vw  verz
      WHERE  verz.id = p_obmg_id
      ;
    verz_rec  verz_cur%ROWTYPE;
  BEGIN
    OPEN  verz_cur;
    FETCH verz_cur
    INTO  verz_rec;
    CLOSE verz_cur;
    kgc_brie_00.registreer
    ( p_obmg_id       => p_obmg_id
    , p_onde_id       => verz_rec.onde_id
    , p_onbe_id       => verz_rec.onbe_id
    , p_pers_id       => verz_rec.pers_id
    , p_kafd_id       => verz_rec.kafd_id
    , p_brieftype     => 'VERZOEKMG'
    , p_geadresseerde => verz_rec.geadresseerde
    , p_kopie         => verz_rec.eerder_geprint
    );
    zet_herinnering
    ( p_obmg_id => p_obmg_id
    , p_weken   => p_weken
    , p_datum   => kgc_brie_00.verzoekmg_geprint_op( p_obmg_id => p_obmg_id
                                                   )
    );
  END verzoekmg_meld_af;

  PROCEDURE herinnermg_meld_af
  ( p_obmg_id  IN  NUMBER
  , p_weken    IN  NUMBER
  ) IS
    CURSOR verz_cur IS
      SELECT verz.rela_id
      ,      verz.onbe_id
      ,      verz.onde_id
      ,      verz.pers_id
      ,      verz.kafd_id
      ,      verz.geadresseerde
      ,      verz.eerder_geprint
      FROM   kgc_verzoekbrief_mg_vw  verz
      WHERE  verz.id = p_obmg_id
      ;
    verz_rec  verz_cur%ROWTYPE;
  BEGIN
    OPEN  verz_cur;
    FETCH verz_cur
    INTO  verz_rec;
    CLOSE verz_cur;
    kgc_brie_00.registreer
    ( p_obmg_id       => p_obmg_id
    , p_onbe_id       => verz_rec.onbe_id
    , p_onde_id       => verz_rec.onde_id
    , p_pers_id       => verz_rec.pers_id
    , p_kafd_id       => verz_rec.kafd_id
    , p_brieftype     => 'HERINNERMG'
    , p_geadresseerde => verz_rec.geadresseerde
    , p_kopie         => verz_rec.eerder_geprint
    );
    zet_herinnering
    ( p_obmg_id => p_obmg_id
    , p_weken   => p_weken
    , p_datum   => kgc_brie_00.herinnermg_geprint_op( p_obmg_id => p_obmg_id )
    );
  END herinnermg_meld_af;

END KGC_OBMG_00;
/

/
QUIT
