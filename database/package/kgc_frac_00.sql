CREATE OR REPLACE PACKAGE "HELIX"."KGC_FRAC_00" IS
-- bepaal hoogste volgnummer van kweekfracties van een fractie
FUNCTION hoogste_volgnr
( p_frac_id IN NUMBER
, p_prefix IN VARCHAR2
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( hoogste_volgnr, WNDS, WNPS, RNPS );

-- maak fractie(s) aan op basis van standaard fractietypes
PROCEDURE standaard_fracties
( p_mons_id IN NUMBER
);

-- maak kweekfractie(s) aan op basis van standaard kweektypes
PROCEDURE standaard_kweekfracties
( p_entiteit IN varchar2 -- FRAC, ONIN, ONMO
, p_id IN NUMBER
);

-- bepaal nieuwe code voor een kweekfractie
-- Mantis 1162: parameters p_kwfr_id_parent en p_kwty_id toegevoegd
FUNCTION kwfr_code
( p_frac_id IN NUMBER
, p_code_parent IN VARCHAR2
, p_prefix IN VARCHAR2
, p_nummering_na_prefix IN VARCHAR2
, p_kwfr_id_parent IN NUMBER
, p_kwty_id IN NUMBER
)
RETURN VARCHAR2;

-- genereer kweekfracties voor een fractie
-- deze kweekfracties kunnen het gevolg zijn van het verdelen van een bestaande kweekfractie
PROCEDURE maak_kweekfracties
( p_frac_id IN NUMBER
, p_kwty_id IN NUMBER := NULL
, p_kwfr_id_parent IN NUMBER := NULL
, p_code_parent IN VARCHAR2 := NULL
, p_aantal IN varchar2 := NULL
, p_kweekduur IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
, p_datum_inzet IN DATE := NULL
);

-- genereer fractienummer tbv. zuiveringsproces (DNA)
FUNCTION fractienummer_zuivering
( p_ongr_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_waarde1 IN VARCHAR2 := NULL
, p_waarde2 IN VARCHAR2 := NULL
, p_waarde3 IN VARCHAR2 := NULL
, p_waarde4 IN VARCHAR2 := NULL
, p_waarde5 IN VARCHAR2 := NULL
)
RETURN VARCHAR2;

-- Tbv gedenormaliseerde kolom in BAS_FRACTIES
FUNCTION frac_kafd_id
( p_mons_id IN bas_fracties.mons_id%TYPE
)
RETURN NUMBER;

FUNCTION meetwaarde_stoftest_bekend
( p_frac_id  IN  NUMBER
, p_stof_id  IN  NUMBER
)
RETURN VARCHAR2;

-- default status voor een fractie
FUNCTION default_status
( p_mons_id in number := null
, p_kafd_id in number := null
, p_ongr_id in number := null
)
return varchar2;

-- kweekduur, rekening houdend met weekend-dagen
function uithaaldatum
( p_datum_inzet in date
, p_kweekduur in number
, p_kafd_id in number
, p_ongr_id in number
)
return date;

-- bepaal aantal kweekfracties eventueel adhv. formule
function aantal_kweekfracties
( p_aantal in varchar2
, p_volume in number
, p_frac_id in number := null
)
return number;

-- verwijder de fracties op basis van een onderzoekindicatie   ( Mantis 626 )
procedure verwijder_kweekfracties
  ( p_onde_id kgc_onderzoek_indicaties.onde_id%type
  , p_indi_id kgc_onderzoek_indicaties.indi_id%type
  );
END KGC_FRAC_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FRAC_00" IS
function aantal_kweekfracties
( p_aantal in varchar2
, p_volume in number
, p_frac_id in number := null
)
return number
is
  v_aantal number;
  v_volume number := p_volume;
  v_statement varchar2(100);
  CURSOR frac_cur
  IS
    select mons.hoeveelheid_monster
    from   kgc_monsters mons
    ,      bas_fracties frac
    where  frac.mons_id = mons.mons_id
    and    frac.frac_id = p_frac_id
    ;
begin
  begin
    v_aantal := p_aantal;
  exception
    when value_error
    then
      -- alfanumerieke tekens in aantal_kweken = formule
      -- enige parameter is <VOLUME>
      if ( p_volume is null
       and p_frac_id is not null
         )
      then
        open  frac_cur;
        fetch frac_cur
        into  v_volume;
        close frac_cur;
      end if;
      if ( v_volume is not null )
      then
        v_statement := 'select '||replace( upper( p_aantal ), '<VOLUME>', v_volume)||' from dual';
        v_aantal := kgc_util_00.dyn_exec( v_statement );
      else
        v_aantal := 0;
      end if;
  end;
  return( v_aantal );
end aantal_kweekfracties;

FUNCTION hoogste_volgnr
( p_frac_id IN NUMBER
, p_prefix IN VARCHAR2
)
RETURN NUMBER
IS
  v_return NUMBER;
  CURSOR kwfr_cur
  IS
    SELECT COUNT(*) aantal
    FROM   kgc_kweekfracties
    WHERE  frac_id = p_frac_id
    AND    code LIKE p_prefix||'%'
    AND    ( code <> p_prefix OR p_prefix IS NULL )
    ;
BEGIN
  OPEN  kwfr_cur;
  FETCH kwfr_cur
  INTO  v_return;
  CLOSE kwfr_cur;
  RETURN( v_return );
END hoogste_volgnr;

PROCEDURE standaard_fracties
( p_mons_id IN NUMBER
)
IS
  CURSOR mons_cur
  IS
    SELECT ongr_id
    ,      mate_id
    ,      monsternummer
    FROM   kgc_monsters
    WHERE  mons_id = p_mons_id
    AND    alleen_voor_opslag = 'N'
    ;
  mons_rec mons_cur%rowtype;

  CURSOR frty_cur
  IS
    SELECT frty.frty_id
    ,      frty.code
    ,      frty.letter_fractienr
    FROM   bas_fractie_types frty
    ,      bas_stand_fractietypes stfr
    WHERE  frty.frty_id = stfr.frty_id
    AND    stfr.ongr_id = mons_rec.ongr_id
    AND    stfr.mate_id = mons_rec.mate_id
    AND    frty.vervallen = 'N'
    ;
  v_fractienummer VARCHAR2(20);
 BEGIN
  OPEN  mons_cur;
  FETCH mons_cur
  INTO  mons_rec;
  CLOSE mons_cur;
  IF ( mons_rec.mate_id IS NOT NULL )
  THEN
    FOR r IN frty_cur
    LOOP
      BEGIN
        v_fractienummer := kgc_nust_00.genereer_nr
                           ( p_nummertype => 'FRAC'
                           , p_ongr_id => mons_rec.ongr_id
                           , p_waarde1 => mons_rec.monsternummer
                           , p_waarde2 => r.letter_fractienr
                           );
        INSERT INTO bas_fracties
        ( mons_id
        , fractienummer
        , frty_id
        , commentaar
        )
        VALUES
        ( p_mons_id
        , v_fractienummer
        , r.frty_id
        , 'Automatisch gegenereerd uit fractietype '||r.code
        );
      END;
    END LOOP;
  END IF;
END standaard_fracties;

PROCEDURE standaard_kweekfracties
( p_entiteit IN varchar2 -- FRAC, ONIN, ONMO, MONS
, p_id IN NUMBER
)
IS
  CURSOR frac_cur
  ( b_frac_id in number
  )
  IS
    SELECT frac.frac_id
    ,      frac.frty_id
    ,      mons.kafd_id
    ,      mons.mate_id
    ,      mons.alleen_voor_opslag
    ,      mons.hoeveelheid_monster
    ,      onin.indi_id
    FROM   kgc_onderzoek_indicaties onin
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_monsters mons
    ,      bas_fracties frac
    WHERE  frac.mons_id = mons.mons_id
    AND    mons.mons_id = onmo.mons_id (+)
    AND    onmo.onde_id = onin.onde_id (+)
    AND    frac.frac_id = b_frac_id
    ;
  CURSOR mons_cur
  ( b_mons_id in number
  )
  IS
    SELECT frac.frac_id
    ,      frac.frty_id
    ,      mons.kafd_id
    ,      mons.mate_id
    ,      mons.alleen_voor_opslag
    ,      mons.hoeveelheid_monster
    ,      onin.indi_id
    FROM   kgc_onderzoek_indicaties onin
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_monsters mons
    ,      bas_fracties frac
    WHERE  frac.mons_id = mons.mons_id
    AND    mons.mons_id = onmo.mons_id (+)
    AND    onmo.onde_id = onin.onde_id (+)
    AND    mons.mons_id = b_mons_id
    ;
  CURSOR onmo_cur
  ( b_onmo_id in number
  )
  IS
    SELECT frac.frac_id
    ,      frac.frty_id
    ,      mons.kafd_id
    ,      mons.mate_id
    ,      mons.alleen_voor_opslag
    ,      mons.hoeveelheid_monster
    ,      onin.indi_id
    FROM   kgc_onderzoek_indicaties onin
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_monsters mons
    ,      bas_fracties frac
    WHERE  frac.mons_id = mons.mons_id
    AND    mons.mons_id = onmo.mons_id
    AND    onmo.onde_id = onin.onde_id -- onmo is alleen tbv. onin, dus outer join niet nodig
    AND    onmo.onmo_id = b_onmo_id
    ;
  CURSOR onin_cur
  ( b_onin_id in number
  )
  IS
    SELECT frac.frac_id
    ,      frac.frty_id
    ,      mons.kafd_id
    ,      mons.mate_id
    ,      mons.alleen_voor_opslag
    ,      mons.hoeveelheid_monster
    ,      onin.indi_id
    FROM   kgc_onderzoek_indicaties onin
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_monsters mons
    ,      bas_fracties frac
    WHERE  frac.mons_id = mons.mons_id
    AND    mons.mons_id = onmo.mons_id
    AND    onmo.onde_id = onin.onde_id
    AND    onin.onin_id = b_onin_id
    ;

  procedure genereer_kweekfracties
  ( b_kafd_id in number
  , b_frac_id in number
  , b_frty_id in number
  , b_mate_id in number
  , b_indi_id in number
  , b_opslagmonster in varchar2
  , b_volume in number
  )
  is
    -- als standaard bij het kweektype is aangevinkt (dan voor alle fractietypes)
    -- of per fractietype
      -- Mantis 626: kwty.vervallen en ftkt.standaard toegevoegd
      CURSOR kwty_cur IS
        SELECT kwty.volgorde,
               kwty.code,
               kwty.kwty_id,
               kwty.prefix,
               kwty.nummering_na_prefix,
               kwty.aantal_kweken,
               kwty.kweekduur,
               kwty.omschrijving
          FROM kgc_kweektypes kwty
         WHERE kwty.kafd_id = b_kafd_id
           AND (kwty.mate_id = b_mate_id or kwty.mate_id is null)
           AND (kwty.indi_id = b_indi_id or kwty.indi_id is null)
           AND (kwty.opslagmonster = b_opslagmonster or
               kwty.opslagmonster is null)
           AND kwty.standaard = 'J'
           AND kwty.vervallen = 'N'
           AND NOT EXISTS
         (SELECT NULL
                  FROM kgc_frty_kwty ftkt
                 WHERE ftkt.frty_id = NVL(b_frty_id, ftkt.frty_id)
                   AND ftkt.kwty_id = kwty.kwty_id
                   AND ftkt.standaard = 'J')
           and not exists
         (select null
                  from kgc_kweekfracties kwfr1, kgc_kweektypes kwty1
                 where kwfr1.kwty_id = kwty1.kwty_id
                   and kwfr1.frac_id = b_frac_id
                   and kwty1.code = kwty.code)
        UNION -- niet ALL: dubbele eruit filteren!
        SELECT kwty.volgorde,
               kwty.code,
               kwty.kwty_id,
               kwty.prefix,
               kwty.nummering_na_prefix,
               kwty.aantal_kweken,
               kwty.kweekduur,
               kwty.omschrijving
          FROM kgc_kweektypes kwty, kgc_frty_kwty ftkt
         WHERE ftkt.kwty_id = kwty.kwty_id
           AND ftkt.frty_id = b_frty_id
           AND (kwty.mate_id = b_mate_id or kwty.mate_id is null)
           AND (kwty.indi_id = b_indi_id or kwty.indi_id is null)
           AND (kwty.opslagmonster = b_opslagmonster or
               kwty.opslagmonster is null)
           AND kwty.vervallen = 'N'
           AND ftkt.standaard = 'J'
           and not exists
         (select null
                  from kgc_kweekfracties kwfr1, kgc_kweektypes kwty1
                 where kwfr1.kwty_id = kwty1.kwty_id
                   and kwfr1.frac_id = b_frac_id
                   and kwty1.code = kwty.code)
         ORDER BY 1, 2
      ;
    l_kwfr_code VARCHAR2(100);
  begin
    FOR r IN kwty_cur
    LOOP
      FOR i IN 1 .. NVL( aantal_kweekfracties
                         ( p_aantal => r.aantal_kweken
                         , p_volume => b_volume
                         , p_frac_id => null -- volume is al belend
                         )
                       , 1
                       )
      LOOP
        -- Mantis 1162: parameters p_kwfr_id_parent en p_kwty_id doorgeven.
        l_kwfr_code := kwfr_code
                       ( p_frac_id => b_frac_id
                       , p_code_parent => NULL
                       , p_prefix => r.prefix
                       , p_nummering_na_prefix => r.nummering_na_prefix
                       , p_kwfr_id_parent => NULL
                       , p_kwty_id => NULL
                       );
        BEGIN
          INSERT INTO kgc_kweekfracties
          ( frac_id
          , code
          , kwty_id
          , kweekduur
          , omschrijving
          )
          VALUES
          ( b_frac_id
          , NVL( l_kwfr_code, TO_CHAR( i ) )
          , r.kwty_id
          , r.kweekduur
          , r.omschrijving
          );
        EXCEPTION
          WHEN OTHERS
          THEN
            NULL;
        END;
      END LOOP;
    END LOOP;
  end genereer_kweekfracties;

BEGIN
  -- haal voor elke entiteit op: frty_id, mate_id, indi_id, alleen_voor_opslag
  if ( p_entiteit = 'FRAC' )
  then
    for r in frac_cur( b_frac_id => p_id )
    loop
      genereer_kweekfracties
      ( b_kafd_id => r.kafd_id
      , b_frac_id => r.frac_id
      , b_frty_id => r.frty_id
      , b_mate_id => r.mate_id
      , b_indi_id => r.indi_id
      , b_opslagmonster => r.alleen_voor_opslag
      , b_volume => r.hoeveelheid_monster
      );
    end loop;
  elsif ( p_entiteit = 'ONMO' )
  then
    for r in onmo_cur( b_onmo_id => p_id )
    loop
      genereer_kweekfracties
      ( b_kafd_id => r.kafd_id
      , b_frac_id => r.frac_id
      , b_frty_id => r.frty_id
      , b_mate_id => r.mate_id
      , b_indi_id => r.indi_id
      , b_opslagmonster => r.alleen_voor_opslag
      , b_volume => r.hoeveelheid_monster
      );
    end loop;
  elsif ( p_entiteit = 'MONS' )
  then
    for r in mons_cur( b_mons_id => p_id )
    loop
      genereer_kweekfracties
      ( b_kafd_id => r.kafd_id
      , b_frac_id => r.frac_id
      , b_frty_id => r.frty_id
      , b_mate_id => r.mate_id
      , b_indi_id => r.indi_id
      , b_opslagmonster => r.alleen_voor_opslag
      , b_volume => r.hoeveelheid_monster
      );
    end loop;
  elsif ( p_entiteit = 'ONIN' )
  then
    for r in onin_cur( b_onin_id => p_id )
    loop
      genereer_kweekfracties
      ( b_kafd_id => r.kafd_id
      , b_frac_id => r.frac_id
      , b_frty_id => r.frty_id
      , b_mate_id => r.mate_id
      , b_indi_id => r.indi_id
      , b_opslagmonster => r.alleen_voor_opslag
      , b_volume => r.hoeveelheid_monster
      );
    end loop;
  end if;
END standaard_kweekfracties;

FUNCTION kwfr_code
( p_frac_id IN NUMBER
, p_code_parent IN VARCHAR2
, p_prefix IN VARCHAR2
, p_nummering_na_prefix IN VARCHAR2
, p_kwfr_id_parent IN NUMBER
, p_kwty_id IN NUMBER
)
RETURN  VARCHAR2
IS
  -- Mantis 1162: parameters p_kwfr_id_parent en p_kwty_id toegevoegd.
  -- Toelichting op parameters:
  -- ==========================
  -- p_kwfr_id_parent:  alleen bij 'verdelen' gevuld, bij 'aanmaken' leeg. Bevat de id van de te verdelen parent-kweekfractie.
  -- p_kwty_id:         alleen bij 'verdelen' gevuld, bij 'aanmaken' leeg. Bevat de id van het aan te maken detail-kweekfractie-type.
  -- p_code_parent:     alleen bij 'verdelen' gevuld als het aan te maken detail-kweekfractie-type gelijk is aan het parent-kweekfractie-type.

  v_serie_teken CONSTANT VARCHAR2(1) := '#';
  v_return VARCHAR2(20); -- mantis 626 lengte aangepast
  v_teken_1 VARCHAR2(1);
  v_teken_2 VARCHAR2(1);
  v_na_prefix VARCHAR2(100) := p_nummering_na_prefix;
  v_kwfr_code VARCHAR2(100);
  v_volgnr NUMBER;
  v_lengte_parent NUMBER;
  v_lengte NUMBER;
  v_pos_in_definitie NUMBER;
  v_master_optie BOOLEAN; -- geeft aan of bij 'verdelen', de code voorafgegaan wordt/is met de code van de 'master'
  v_master_teken CONSTANT VARCHAR2(1) := '$'; -- teken dat aangeeft dat de master-optie van kracht is
  v_prefix VARCHAR2(10);

  CURSOR kwfr_master_cur
  ( b_kwfr_id in number
  )
  IS
    SELECT code
         , kwty_id
         , kwfr_id_parent
    FROM   kgc_kweekfracties
    WHERE  kwfr_id = b_kwfr_id
    ;
  v_code_master kgc_kweekfracties.code%TYPE;
  v_kwty_id_master kgc_kweekfracties.kwty_id%TYPE;
  v_kwfr_id_parent_master kgc_kweekfracties.kwfr_id_parent%TYPE;

  v_kwfr_id_master_last kgc_kweekfracties.kwfr_id%TYPE;
  v_code_master_last kgc_kweekfracties.code%TYPE;

BEGIN
  -- indien master-optie is opgegeven, dan het master-teken verwijderen uit p_prefix
  IF INSTR( p_prefix, v_master_teken ) > 0
  THEN
    v_master_optie := TRUE;
    v_prefix := replace(p_prefix, v_master_teken, '');
  ELSE
    v_master_optie := FALSE;
    v_prefix := p_prefix;
  END IF;

  -- bepalen van v_kwfr_code die later het hoogste-volgnummer gaat bepalen
  IF p_code_parent IS NOT NULL
  THEN
    -- bij 'verdelen' waarbij kweekfractie-types van parent en detail gelijk zijn
    v_kwfr_code := p_code_parent;
  ELSIF ( p_kwfr_id_parent IS NOT NULL AND v_master_optie )
  THEN
    -- bij 'verdelen' waarbij kweekfractie-types van parent en detail ongelijk zijn, en de master-optie is opgegeven
    v_code_master := NULL;
    OPEN  kwfr_master_cur ( p_kwfr_id_parent );
    FETCH kwfr_master_cur
    INTO  v_code_master
        , v_kwty_id_master
        , v_kwfr_id_parent_master;
    CLOSE kwfr_master_cur;
    v_kwfr_code := v_code_master||v_prefix;
  ELSE
    -- bij 'verdelen' waarbij kweekfractie-types van parent en detail ongelijk zijn, en geen master-optie opgegeven en bij 'aanmaken'
    v_kwfr_code := v_prefix;
  END IF;

  -- bepaal bestaande aantal met deze code
  v_volgnr := hoogste_volgnr
              ( p_frac_id => p_frac_id
              , p_prefix => v_kwfr_code
              );

  IF p_code_parent IS NOT NULL
  THEN
    -- bij verdelen naar hetzelfde fractietype, het al eerder gebruikte voorste deel van v_na_prefix strippen:
    v_lengte_parent :=  NVL( LENGTH( v_kwfr_code ), 0 ) - NVL( LENGTH( v_prefix ), 0 );
    IF v_master_optie
    THEN
      -- in v_kwfr_code (de parent-code) kan een master-code opgenomen zijn, nl. als deze parent-kweekfractie was ontstaan uit een
      -- parent-kweekfractie van een ander kweektype. In dat geval de lengte van master-code in mindering brengen op v_lengte_parent
      v_code_master_last := NULL;
      v_kwfr_id_master_last := p_kwfr_id_parent;
      WHILE v_kwfr_id_master_last IS NOT NULL
      LOOP -- werk van onder naar boven in de hierarchie van kweekfracties totdat een ongelijk kweektype wordt gevonden
        OPEN  kwfr_master_cur ( v_kwfr_id_master_last );
        FETCH kwfr_master_cur
        INTO  v_code_master
            , v_kwty_id_master
            , v_kwfr_id_parent_master;
        IF kwfr_master_cur%FOUND
          THEN
            CLOSE kwfr_master_cur;
            IF NVL( v_kwty_id_master, -1 ) <> p_kwty_id
            THEN -- bij ongelijk kweektype stoppen; parent-code van dit niveau is gebruikt als master-code
              v_code_master_last := v_code_master;
              v_kwfr_id_master_last := NULL;
            ELSE -- bij gelijk kweektype doorgaan met de parent op het erboven liggende niveau
              v_kwfr_id_master_last := v_kwfr_id_parent_master;
            END IF;
          ELSE
            CLOSE kwfr_master_cur;
            v_kwfr_id_master_last := NULL;
        END IF;
      END LOOP;
      IF ( v_code_master_last IS NOT NULL AND INSTR( v_kwfr_code, v_code_master_last ) > 0 )
      THEN
        -- NN: master-code gevonden bij een parent in de hierarchie. INSTR: alleen als deze master-code ook voorkomt in
        -- v_kwfr_code, de lengte aanpassen. hierdoor is het mogelijk om bestaande kweekfracties die ooit aangemaakt zijn
        -- zonder een master-code, te verdelen over kweekfracties waarbij inmiddels wel een master-code is gedefinieerd.
        v_lengte_parent :=  v_lengte_parent - LENGTH( v_code_master_last );
      END IF;
    END IF;

    IF v_lengte_parent > 0
    THEN
      v_lengte := 0;
      v_pos_in_definitie := 0;
      LOOP
        v_teken_1 := SUBSTR( v_na_prefix, 1, 1 );
        v_teken_2 := SUBSTR( v_na_prefix, 2, 1 );

        IF ( v_teken_1 IS NULL )
        THEN
          EXIT; -- geen gedefinieerde nummering na prefix
        ELSIF ( v_teken_1 = v_serie_teken AND v_teken_2 IS NOT NULL )
        THEN
          -- reeks, dus lees volgende teken om reeks mee te starten
          v_na_prefix := SUBSTR( v_na_prefix, 2 );
        END IF;

        IF ( v_teken_1 = v_serie_teken AND v_teken_2 IS NOT NULL )
        THEN
          IF ( v_teken_2 IN ( '0','1','2','3','4','5','6','7','8','9') )
          THEN
            v_lengte := v_lengte + 2;
            v_pos_in_definitie := v_pos_in_definitie + 2;
          ELSIF ( v_teken_2 IS NOT NULL )
          THEN
            v_lengte := v_lengte + 1;
            v_pos_in_definitie := v_pos_in_definitie + 2;
          END IF;
        ELSE
          v_lengte := v_lengte + 1;
          v_pos_in_definitie := v_pos_in_definitie + 1;
          -- zoek verder naar volgend teken
        END IF;
        v_na_prefix := SUBSTR( v_na_prefix, 2 );
        EXIT WHEN ( v_na_prefix IS NULL OR v_lengte >= v_lengte_parent );
      END LOOP;
      v_na_prefix := SUBSTR( p_nummering_na_prefix, v_pos_in_definitie + 1 );
    END IF;
  END IF;

  -- alles achter het eerste serie-teken in v_na_prefix strippen; slechts 1 serie (='level') per keer gebruiken.
  IF INSTR( v_na_prefix, v_serie_teken, 1, 1 ) > 0
  THEN
    v_na_prefix := SUBSTR( v_na_prefix, 1, INSTR( v_na_prefix,v_serie_teken, 1, 1 ) + 1 );
  END IF;

  v_volgnr := NVL( v_volgnr, 0 ) + 1;

  -- v_kwfr_code aanvullen met de codering van de eerstvolgende serie (='level').
  LOOP
    -- zoek het eerstvolgende teken
    v_teken_1 := SUBSTR( v_na_prefix, 1, 1 );
    v_teken_2 := SUBSTR( v_na_prefix, 2, 1 );
    IF ( v_teken_1 IS NULL )
    THEN
      v_kwfr_code := v_kwfr_code||LPAD( TO_CHAR( v_volgnr ), 2, '0' );
      EXIT; -- geen gedefinieerde nummering na prefix
    ELSIF ( v_teken_1 = v_serie_teken
        AND v_teken_2 IS NOT NULL
          )
    THEN
      -- reeks, dus lees volgende teken om reeks mee te starten
      v_na_prefix := SUBSTR( v_na_prefix, 2 );
      IF (v_teken_2 IS NULL)
      THEN
        v_kwfr_code := v_kwfr_code||v_serie_teken;
        EXIT; -- indien geen teken meer na # dan exit
      END IF;
    END IF;

    IF ( v_teken_1 = v_serie_teken
     AND v_teken_2 IS NOT NULL
       )
    THEN
      IF ( v_teken_2 IN ( '0','1','2','3','4','5','6','7','8','9') )
      THEN
        v_kwfr_code := v_kwfr_code||LPAD( TO_CHAR( v_volgnr + v_teken_2 - 1 ), 2, '0' );
      ELSE
        v_kwfr_code := v_kwfr_code||CHR( ASCII( v_teken_2 ) + v_volgnr - 1 );
      END IF;
    ELSE
      v_kwfr_code := v_kwfr_code||v_teken_1;
      -- zoek verder naar volgend teken
    END IF;
    v_na_prefix := SUBSTR( v_na_prefix, 2 );
    EXIT WHEN v_na_prefix IS NULL;
  END LOOP;

  IF ( LENGTH( v_kwfr_code ) > 20 )
  THEN
    v_kwfr_code := SUBSTR( v_kwfr_code, -20 );
  END IF;
  v_return := SUBSTR( v_kwfr_code, 1, 20 );
  RETURN( v_return );
END kwfr_code;

PROCEDURE maak_kweekfracties
( p_frac_id IN NUMBER
, p_kwty_id IN NUMBER := NULL
, p_kwfr_id_parent IN NUMBER := NULL
, p_code_parent IN VARCHAR2 := NULL
, p_aantal IN varchar2 := NULL
, p_kweekduur IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
, p_datum_inzet IN DATE := NULL
)
IS
  CURSOR kwty_cur
  IS
    SELECT kwty_id
    ,      prefix
    ,      nummering_na_prefix
    ,      aantal_kweken
    ,      kweekduur
    ,      omschrijving
    FROM   kgc_kweektypes
    WHERE  kwty_id = p_kwty_id
    ;
  kwty_rec kwty_cur%rowtype;

  kwfr_trow kgc_kweekfracties%rowtype;
BEGIN
  IF ( p_frac_id IS NULL )
  THEN
    RETURN;
  END IF;
  IF ( p_kwty_id IS NULL )
  THEN
    standaard_kweekfracties
    ( p_entiteit => 'FRAC'
    , p_id => p_frac_id
    );
  ELSE
    OPEN  kwty_cur;
    FETCH kwty_cur
    INTO  kwty_rec;
    CLOSE kwty_cur;
    FOR i IN 1 .. aantal_kweekfracties
                  ( p_aantal => NVL( p_aantal, NVL( kwty_rec.aantal_kweken, 1 ) )
                  , p_volume => null
                  , p_frac_id => p_frac_id
                  )
    LOOP
      kwfr_trow.frac_id := p_frac_id;
      -- Mantis 1162: parameters p_kwfr_id_parent en p_kwty_id doorgeven.
      kwfr_trow.code := kwfr_code
                        ( p_frac_id => p_frac_id
                        , p_code_parent => p_code_parent
                        , p_prefix => kwty_rec.prefix
                        , p_nummering_na_prefix => kwty_rec.nummering_na_prefix
                        , p_kwfr_id_parent => p_kwfr_id_parent
                        , p_kwty_id => kwty_rec.kwty_id
                        );
      kwfr_trow.kwty_id := p_kwty_id;
      kwfr_trow.kweekduur := NVL( p_kweekduur, kwty_rec.kweekduur );
      kwfr_trow.datum_inzet := p_datum_inzet;
      IF ( kwfr_trow.datum_inzet IS NOT NULL )
      THEN
        kwfr_trow.mede_id_inzet := NVL( p_mede_id, kgc_mede_00.medewerker_id );
      END IF;
      IF ( kwfr_trow.kweekduur = 0 )
      THEN
        kwfr_trow.mede_id_uithaal := kwfr_trow.mede_id_inzet;
        kwfr_trow.datum_uithaal := kwfr_trow.datum_inzet;
      END IF;
      kwfr_trow.kwfr_id_parent := p_kwfr_id_parent;
      kwfr_trow.omschrijving := kwty_rec.omschrijving;

      BEGIN
        INSERT INTO kgc_kweekfracties
        ( frac_id
        , code
        , kwty_id
        , kweekduur
        , mede_id_inzet
        , datum_inzet
        , mede_id_uithaal
        , datum_uithaal
        , kwfr_id_parent
        , omschrijving
        )
        VALUES
        ( kwfr_trow.frac_id
        , kwfr_trow.code
        , kwfr_trow.kwty_id
        , kwfr_trow.kweekduur
        , kwfr_trow.mede_id_inzet
        , kwfr_trow.datum_inzet
        , kwfr_trow.mede_id_uithaal
        , kwfr_trow.datum_uithaal
        , kwfr_trow.kwfr_id_parent
        , kwfr_trow.omschrijving
        );
      EXCEPTION
        WHEN OTHERS
        THEN
          NULL;
      END;
    END LOOP;
  END IF;
END maak_kweekfracties;

FUNCTION fractienummer_zuivering
( p_ongr_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_waarde1 IN VARCHAR2 := NULL
, p_waarde2 IN VARCHAR2 := NULL
, p_waarde3 IN VARCHAR2 := NULL
, p_waarde4 IN VARCHAR2 := NULL
, p_waarde5 IN VARCHAR2 := NULL
)
RETURN  VARCHAR2
IS
  v_return VARCHAR2(20);
BEGIN
  v_return := kgc_nust_00.genereer_nr
              ( p_nummertype => 'FRAC'
              , p_kafd_id => p_kafd_id
              , p_ongr_id => p_ongr_id
              , p_waarde1 => p_waarde1  -- monsternummer
              , p_waarde2 => p_waarde2  -- fractietype
              , p_waarde3 => p_waarde3
              , p_waarde4 => p_waarde4
              , p_waarde5 => p_waarde5
              )
           || kgc_sypa_00.standaard_waarde
              ( p_parameter_code => 'FRACTIENUMMER_ZUIVERING'
              , p_kafd_id => p_kafd_id
              , p_ongr_id => p_ongr_id
              );
  RETURN( v_return );
END fractienummer_zuivering;

FUNCTION frac_kafd_id
( p_mons_id  IN  bas_fracties.mons_id%TYPE
)
RETURN NUMBER
IS
  CURSOR mons_cur
  IS
    SELECT mons.kafd_id
    FROM   kgc_monsters  mons
    WHERE  mons.mons_id = p_mons_id
    ;
  mons_rec  mons_cur%ROWTYPE;
BEGIN
  OPEN  mons_cur;
  FETCH mons_cur
  INTO  mons_rec;
  CLOSE mons_cur;
  RETURN mons_rec.kafd_id;
END;

FUNCTION meetwaarde_stoftest_bekend
( p_frac_id IN NUMBER
, p_stof_id IN NUMBER
)
RETURN VARCHAR2
IS
  CURSOR meet_cur
  IS
    SELECT 'J'
    FROM   bas_metingen     meti
    ,      bas_meetwaarden  meet
    WHERE  meti.meti_id     = meet.meti_id
    AND    meet.meetwaarde  IS NOT NULL
    AND    meti.frac_id     = p_frac_id
    AND    meet.stof_id     = p_stof_id
    ;
  v_retval VARCHAR2(1) := 'N';
BEGIN
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  v_retval;
  CLOSE meet_cur;
  RETURN v_retval;
END meetwaarde_stoftest_bekend;

-- default status voor een fractie
FUNCTION default_status
( p_mons_id in number := null
, p_kafd_id in number := null
, p_ongr_id in number := null
)
return varchar2
is
  v_return bas_fracties.status%type;
  v_kafd_id number := p_kafd_id;
  v_ongr_id number := p_ongr_id;
  cursor mons_cur
  is
    select kafd_id
    ,      ongr_id
    from   kgc_monsters
    where  mons_id = p_mons_id
    ;
begin
  if ( p_mons_id is not null )
  then
    open  mons_cur;
    fetch mons_cur
    into  v_kafd_id
    ,     v_ongr_id;
    close mons_cur;
  elsif ( p_kafd_id is null
      and p_ongr_id is null
        )
  then
    -- voorkeursafdeling en groep
    v_kafd_id := kgc_mede_00.afdeling_id;
    v_ongr_id := kgc_mede_00.onderzoeksgroep_id;
  end if;
  v_return := upper( substr( kgc_sypa_00.standaard_waarde
                             ( p_parameter_code => 'STANDAARD_FRACTIESTATUS'
                             , p_kafd_id => v_kafd_id
                             , p_ongr_id => v_ongr_id
                             )
                           , 1, 1
                           )
                   );
  return( v_return );
end default_status;

function uithaaldatum
( p_datum_inzet in date
, p_kweekduur in number
, p_kafd_id in number
, p_ongr_id in number
)
return date
is
  v_uithaaldatum date;
  v_weekend_dagen integer(1);
  v_correctie number := null;
begin
  if ( nvl( p_kweekduur, 0 ) = 0 )
  then
    v_uithaaldatum := p_datum_inzet;
  elsif ( p_datum_inzet is not null )
  then
    begin
      v_weekend_dagen := to_number( kgc_sypa_00.standaard_waarde
                                    ( p_parameter_code => 'WEEKEND_DAGEN'
                                    , p_kafd_id => p_kafd_id
                                    , p_ongr_id => p_ongr_id
                                    )
                                  );
    exception
      when others
      then
        null;
    end;
    if ( v_weekend_dagen is null )
    then
      v_weekend_dagen := 2; -- is default
    end if;

    for i in 1..p_kweekduur
    loop
      if ( to_char(p_datum_inzet + i, 'Dy' ) in ( 'Sat', 'Sun' ) )
      then
        v_correctie := 0;
      end if;
    end loop;

    if ( v_correctie is not null )
    then
      v_correctie := v_correctie + 2 - v_weekend_dagen;
    else
      v_correctie := 0;
    end if;
    v_uithaaldatum := p_datum_inzet + p_kweekduur + v_correctie;
  end if;

  return( v_uithaaldatum );
end uithaaldatum;

procedure verwijder_kweekfracties
  ( p_onde_id kgc_onderzoek_indicaties.onde_id%type
  , p_indi_id kgc_onderzoek_indicaties.indi_id%type
  )
is
  -- functionaliteit verplaatst van in de TAPI-trigger After-Delete_statement
  -- --> Mantis 626
  CURSOR kwfr_cur
  ( b_onde_id in number )
  IS
    SELECT frac.frty_id
    ,      kwfr.kwfr_id
    FROM   kgc_onderzoek_monsters onmo
    ,      kgc_monsters mons
    ,      bas_fracties frac
    ,      kgc_kweekfracties kwfr
    ,      kgc_kweektypes kwty
    WHERE  onmo.onde_id = b_onde_id
    AND    mons.mons_id = onmo.mons_id
    AND    frac.mons_id = mons.mons_id
    AND    kwfr.frac_id = frac.frac_id
    AND    kwty.kwty_id = kwfr.kwty_id
   -- Voorlopig alle rijen met gevulde _indi_id kandidaat stellen voor verwijderen (die met lege indi nooit).
   -- Zijn deze op grond van een andere indicatie nog nodig, dan wordt de delete later alsnog overgeslagen.
    AND    kwty.indi_id is not null
    -- --> Mantis 626
    and exists(
        SELECT sypa.standaard_waarde
        FROM   kgc_systeem_parameters sypa
        WHERE  sypa.code = 'AUTOM_VERWYDEREN_KWEEKFRACTIES'
        AND    TRIM(sypa.standaard_waarde) = 'J' )
    -- --< Mantis 626
    ;
BEGIN
  -- verwijder kweekfractie(s) op basis van de indicatie maar
  -- niet als kweekfractie betrokken is bij nog andere indicatie(s)
  -- Mantis 626, 20070628, JKO: query analoog aan 'genereer_kweekfracties' opgezet
  FOR kwfr_rec IN kwfr_cur(p_onde_id)
  LOOP
    DELETE FROM kgc_kweekfracties kwfr
     WHERE kwfr.kwfr_id = kwfr_rec.kwfr_id
       AND kwfr.datum_inzet IS NULL
       AND kwfr.datum_uithaal IS NULL
       AND kwfr.datum_opslaan IS NULL
       AND kwfr.datum_controle IS NULL
       AND NOT EXISTS
     (SELECT NULL
              FROM kgc_kweektypes           kwty1 -- kweektype van de te verwijderen kweekfractie
                  ,kgc_kweektypes           kwty2 -- resterende kweektype(s) uit KGC_KWEEKTYPES
                  ,kgc_onderzoek_indicaties onin2 -- resterende onderzoeksindicatie(s) van het onderzoek
             WHERE onin2.onde_id = p_onde_id
               AND kwty1.kwty_id = kwfr.kwty_id
               AND kwty2.kafd_id = kwty1.kafd_id
               AND nvl(kwty2.mate_id, 0) = nvl(kwty1.mate_id, 0)
               AND (onin2.indi_id = kwty2.indi_id OR kwty2.indi_id IS NULL)
               AND nvl(kwty2.opslagmonster, 'X') = nvl(kwty1.opslagmonster, 'X')
               AND kwty2.standaard = 'J'
               AND kwty2.vervallen = 'N'
               AND kwty2.code = kwty1.code
               AND NOT EXISTS (SELECT NULL
                      FROM kgc_frty_kwty ftkt2
                     WHERE ftkt2.kwty_id = kwty2.kwty_id
                       AND ftkt2.standaard = 'J')
            UNION
            SELECT NULL
              FROM kgc_kweektypes           kwty3 -- kweektype van de te verwijderen kweekfractie
                  ,kgc_kweektypes           kwty4 -- resterende kweektype(s) KWTY tbv FTKT
                  ,kgc_frty_kwty            ftkt4 -- resterende kweektype(s) uit KGC_FRTY_KWTY
                  ,kgc_onderzoek_indicaties onin4 -- resterende onderzoeksindicatie(s) van het onderzoek
             WHERE onin4.onde_id = p_onde_id
               AND kwty3.kwty_id = kwfr.kwty_id
               AND kwty4.kafd_id = kwty3.kafd_id
               AND nvl(kwty4.mate_id, 0) = nvl(kwty3.mate_id, 0)
               AND (onin4.indi_id = kwty4.indi_id OR kwty4.indi_id IS NULL)
               AND nvl(kwty4.opslagmonster, 'X') = nvl(kwty3.opslagmonster, 'X')
               AND kwty4.vervallen = 'N'
               AND kwty4.code = kwty3.code
               AND ftkt4.kwty_id = kwty4.kwty_id
               AND ftkt4.frty_id = kwfr_rec.frty_id
               AND ftkt4.standaard = 'J'
     );
  END LOOP;
END;
END  kgc_frac_00;
/

/
QUIT
