CREATE OR REPLACE PACKAGE "HELIX"."KGC_UK2PK" IS
--  Functies
  --
  FUNCTION  func_id(  p_code  IN  VARCHAR2  )  RETURN  NUMBER;
   PRAGMA  RESTRICT_REFERENCES(  func_id,  WNDS,  WNPS,  RNPS  );
   --
  --  Kgc Afdelingen
  FUNCTION  kafd_id(  p_code  IN  VARCHAR2  )  RETURN  NUMBER;
   PRAGMA  RESTRICT_REFERENCES(  kafd_id,  WNDS,  WNPS,  RNPS  );
   --
  --  Onderzoeksgroepen
  FUNCTION  ongr_id(  p_kafd_id   IN   NUMBER
                   ,  p_code      IN   VARCHAR2
                   )  RETURN  NUMBER;
   PRAGMA  RESTRICT_REFERENCES(  ongr_id,  WNDS,  WNPS,  RNPS  );
   --
  --  Indicatiegroepen
  FUNCTION  ingr_id(  p_kafd_id   IN   NUMBER
                   ,  p_code      IN   VARCHAR2
                   )  RETURN  NUMBER;
   PRAGMA  RESTRICT_REFERENCES(  ingr_id,  WNDS,  WNPS,  RNPS  );
   --
  --  Medewerkers
  FUNCTION  mede_id(  p_code   IN   VARCHAR2  )  RETURN  NUMBER;
   PRAGMA   RESTRICT_REFERENCES(  mede_id,   WNDS,   WNPS,   RNPS);
   --
  --  Indicatieteksten
  FUNCTION  indi_id(  p_ongr_id  IN   NUMBER
                   ,  p_code     IN   VARCHAR2
                   )  RETURN  NUMBER;
   PRAGMA   RESTRICT_REFERENCES(  indi_id,   WNDS,   WNPS,   RNPS   );
   --
  --  Gesprektypes
  FUNCTION  gety_id(  p_ongr_id  IN   NUMBER
                   ,  p_code     IN   VARCHAR2
                   )  RETURN  NUMBER;
   PRAGMA   RESTRICT_REFERENCES(  gety_id,   WNDS,   WNPS,   RNPS   );
   --
  --  Locaties
  FUNCTION  loca_id(  p_code   IN   VARCHAR2  )  RETURN  NUMBER;
   PRAGMA   RESTRICT_REFERENCES(  loca_id,   WNDS,   WNPS,   RNPS   );
   --
  --  Indicatiegroepen
  FUNCTION  ingr_id(  p_ongr_id  IN   NUMBER
                   ,  p_code     IN   VARCHAR2
                   )  RETURN  NUMBER;
   PRAGMA   RESTRICT_REFERENCES(  ingr_id,   WNDS,   WNPS,   RNPS   );
   --
  --  Brieftypes
  FUNCTION  brty_id(  p_code   IN   VARCHAR2  )  RETURN  NUMBER;
   PRAGMA   RESTRICT_REFERENCES(  brty_id,   WNDS,   WNPS,   RNPS   );
  --
  --  PC Applicaties
  FUNCTION  pcap_id( p_code  IN  VARCHAR2 ) RETURN NUMBER;
  --
  --  Stoftesten
  FUNCTION  stof_id(  p_kafd_id   IN   NUMBER
                   ,  p_code      IN   VARCHAR2
                   )  RETURN  NUMBER;

  --  Onderzoek uitslagcodes
  FUNCTION onui_id
  ( p_kafd_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER;

  --  diagnose statussen
  FUNCTION dist_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER;
  PRAGMA   RESTRICT_REFERENCES(  dist_id,   WNDS,   WNPS,   RNPS   );
  --
  --  conclusie types
  FUNCTION coty_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER;
  PRAGMA   RESTRICT_REFERENCES(  coty_id,   WNDS,   WNPS,   RNPS   );
  --
  --  erfmodi
  FUNCTION ermo_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER;
  PRAGMA   RESTRICT_REFERENCES(  ermo_id,   WNDS,   WNPS,   RNPS   );

  --  herkomsten
  FUNCTION herk_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER;
  PRAGMA   RESTRICT_REFERENCES(  herk_id,   WNDS,   WNPS,   RNPS   );

  --  Declaratiewijzen
  FUNCTION dewy_id
  ( p_kafd_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER;
  PRAGMA   RESTRICT_REFERENCES(  dewy_id,   WNDS,   WNPS,   RNPS   );

  -- Bewaarredenen
  FUNCTION bwre_id
  ( p_ongr_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER;

  --  Materialen
  FUNCTION mate_id
  ( p_kafd_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER;

  --  Relaties
  FUNCTION rela_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER;

  --  Projecten
  FUNCTION proj_id
  ( p_kafd_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER;

  --  Familierelaties
  FUNCTION fare_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER;

  -- Conclusieteksten
  FUNCTION conc_id
  ( p_kafd_id  IN NUMBER
  , p_code     IN VARCHAR2
  )  RETURN NUMBER;

  -- Stoftestgroepen
  FUNCTION  stgr_id
  ( p_kafd_id IN NUMBER
  , p_code    IN VARCHAR2
  )  RETURN NUMBER;

  -- Meetwaardestructuur
  FUNCTION mwst_id
  ( p_code IN VARCHAR2
  )
  RETURN  NUMBER;

  --  Werklijsten
  FUNCTION wlst_id
  ( p_kafd_id           IN  NUMBER
  , p_werklijst_nummer  IN  VARCHAR2
  )  RETURN NUMBER;

  -- Onderzoekrelatie types
  FUNCTION onty_id
  ( p_code IN VARCHAR2
  )
  RETURN  NUMBER;

  -- Talen
  FUNCTION  taal_id
  (  p_code  IN  VARCHAR2
  )
  RETURN   NUMBER;

  -- Berekeningstypes
  FUNCTION bety_id
  ( p_code IN VARCHAR2
  )
  RETURN NUMBER;

  -- Technieken
  FUNCTION tech_id
  ( p_code IN VARCHAR2
  )
  RETURN NUMBER;
END KGC_UK2PK;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_UK2PK" IS
FUNCTION func_id( p_code IN VARCHAR2 ) RETURN NUMBER IS
    CURSOR func_cur IS
      SELECT func.func_id
      FROM   kgc_functies  func
      WHERE  func.code    = p_code
      ;
    func_rec  func_cur%ROWTYPE;
BEGIN
    IF  p_code IS NOT NULL
    THEN
      OPEN  func_cur;
      FETCH func_cur
      INTO  func_rec;
      CLOSE func_cur;
    END IF;
    RETURN func_rec.func_id;
  END func_id;

  FUNCTION  kafd_id(  p_code  IN  VARCHAR2  )  RETURN  NUMBER  IS
    CURSOR kafd_cur IS
      SELECT kafd_id
      FROM   kgc_kgc_afdelingen
      WHERE  code = UPPER( p_code )
      ;
    v_id NUMBER;
   BEGIN
    OPEN  kafd_cur;
    FETCH kafd_cur
    INTO  v_id;
    CLOSE kafd_cur;
    RETURN( v_id );
  END kafd_id;

  FUNCTION  ongr_id(  p_kafd_id   IN   NUMBER
                   ,  p_code      IN   VARCHAR2
                   )  RETURN  NUMBER  IS
    CURSOR ongr_cur IS
      SELECT ongr.ongr_id
      FROM   kgc_onderzoeksgroepen  ongr
      WHERE  ongr.kafd_id = p_kafd_id
      AND    ongr.code    = p_code
      ;
    ongr_rec  ongr_cur%ROWTYPE;
   BEGIN
    IF  p_kafd_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  ongr_cur;
      FETCH ongr_cur
      INTO  ongr_rec;
      CLOSE ongr_cur;
    END IF;
    RETURN ongr_rec.ongr_id;
  END ongr_id;

  FUNCTION  ingr_id(
       p_kafd_id   IN   NUMBER
     ,  p_code      IN   VARCHAR2
                   )  RETURN  NUMBER  IS
    CURSOR ingr_cur IS
      SELECT ingr.ingr_id
      FROM   kgc_indicatie_groepen  ingr
      WHERE  ingr.kafd_id = p_kafd_id
      AND    ingr.code    = p_code
      ;
    ingr_rec  ingr_cur%ROWTYPE;
   BEGIN
    IF  p_kafd_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  ingr_cur;
      FETCH ingr_cur
      INTO  ingr_rec;
      CLOSE ingr_cur;
    END IF;
    RETURN ingr_rec.ingr_id;
  END ingr_id;

  FUNCTION  mede_id(  p_code   IN   VARCHAR2  )  RETURN  NUMBER  IS
    CURSOR mede_cur IS
      SELECT mede.mede_id
      FROM   kgc_medewerkers  mede
      WHERE  mede.code = p_code
      ;
    mede_rec  mede_cur%ROWTYPE;
   BEGIN
    IF  p_code IS NOT NULL
    THEN
      OPEN  mede_cur;
      FETCH mede_cur
      INTO  mede_rec;
      CLOSE mede_cur;
    END IF;
    RETURN mede_rec.mede_id;
  END mede_id;

  FUNCTION  indi_id(  p_ongr_id  IN   NUMBER
                   ,  p_code     IN   VARCHAR2
                   )  RETURN  NUMBER  IS
    CURSOR indi_cur IS
      SELECT indi.indi_id
      FROM   kgc_indicatie_teksten indi
      WHERE  indi.ongr_id = p_ongr_id
      AND    indi.code    = p_code
      ;
    indi_rec  indi_cur%ROWTYPE;
   BEGIN
    IF  p_ongr_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  indi_cur;
      FETCH indi_cur
      INTO  indi_rec;
      CLOSE indi_cur;
    END IF;
    RETURN indi_rec.indi_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END indi_id;

  FUNCTION  gety_id(  p_ongr_id  IN   NUMBER
                   ,  p_code     IN   VARCHAR2
                   )  RETURN  NUMBER  IS
    CURSOR gety_cur IS
      SELECT gety.gety_id
      FROM   kgc_gesprek_types  gety
      WHERE  gety.ongr_id = p_ongr_id
      AND    gety.code    = p_code
      ;
    gety_rec  gety_cur%ROWTYPE;
   BEGIN
    IF  p_ongr_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  gety_cur;
      FETCH gety_cur
      INTO  gety_rec;
      CLOSE gety_cur;
    END IF;
    RETURN gety_rec.gety_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END gety_id;

  FUNCTION  loca_id(  p_code   IN   VARCHAR2  )  RETURN  NUMBER  IS
    CURSOR loca_cur IS
      SELECT loca.loca_id
      FROM   kgc_locaties loca
      WHERE  loca.code    = p_code
      ;
    loca_rec  loca_cur%ROWTYPE;
   BEGIN
    IF  p_code    IS NOT NULL
    THEN
      OPEN  loca_cur;
      FETCH loca_cur
      INTO  loca_rec;
      CLOSE loca_cur;
    END IF;
    RETURN loca_rec.loca_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END loca_id;

  FUNCTION  ingr_id(  p_ongr_id  IN   NUMBER
                   ,  p_code     IN   VARCHAR2
                   )  RETURN  NUMBER  IS
    CURSOR ingr_cur IS
      SELECT ingr.ingr_id
      FROM   kgc_indicatie_groepen ingr
      WHERE  ingr.ongr_id = p_ongr_id
      AND    ingr.code    = p_code
      ;
    ingr_rec  ingr_cur%ROWTYPE;
   BEGIN
    IF  p_ongr_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  ingr_cur;
      FETCH ingr_cur
      INTO  ingr_rec;
      CLOSE ingr_cur;
    END IF;
    RETURN ingr_rec.ingr_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END ingr_id;

  FUNCTION  brty_id(  p_code   IN   VARCHAR2  )  RETURN  NUMBER  IS
    CURSOR brty_cur IS
      SELECT brty.brty_id
      FROM   kgc_brieftypes  brty
      WHERE  brty.code    = p_code
      ;
    brty_rec  brty_cur%ROWTYPE;
   BEGIN
    IF  p_code  IS NOT NULL
    THEN
      OPEN  brty_cur;
      FETCH brty_cur
      INTO  brty_rec;
      CLOSE brty_cur;
    END IF;
    RETURN brty_rec.brty_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END brty_id;

  FUNCTION  pcap_id( p_code  IN  VARCHAR2 ) RETURN NUMBER IS
    CURSOR pcap_cur IS
      SELECT pcap.pcap_id
      FROM   kgc_pc_applicaties  pcap
      WHERE  pcap.code  = p_code
      ;
    pcap_rec  pcap_cur%ROWTYPE;
   BEGIN
    IF  p_code  IS NOT NULL
    THEN
      OPEN  pcap_cur;
      FETCH pcap_cur
      INTO  pcap_rec;
      CLOSE pcap_cur;
    END IF;
    RETURN pcap_rec.pcap_id;
  EXCEPTION
    WHEN OTHERS THEN RETURN NULL;
  END pcap_id;

  FUNCTION  stof_id(  p_kafd_id   IN   NUMBER
                   ,  p_code      IN   VARCHAR2
                   )  RETURN  NUMBER  IS
    CURSOR stof_cur IS
      SELECT stof.stof_id
      FROM   kgc_stoftesten  stof
      WHERE  stof.kafd_id = p_kafd_id
      AND    stof.code    = p_code
      ;
    stof_rec  stof_cur%ROWTYPE;
   BEGIN
    IF  p_kafd_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  stof_cur;
      FETCH stof_cur
      INTO  stof_rec;
      CLOSE stof_cur;
    END IF;
    RETURN stof_rec.stof_id;
  END stof_id;

  FUNCTION onui_id
  ( p_kafd_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER
  IS
    CURSOR onui_cur IS
      SELECT onui.onui_id
      FROM   kgc_onderzoek_uitslagcodes  onui
      WHERE  onui.kafd_id = p_kafd_id
      AND    onui.code    = p_code
      ;
    onui_rec  onui_cur%ROWTYPE;
   BEGIN
    IF  p_kafd_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  onui_cur;
      FETCH onui_cur
      INTO  onui_rec;
      CLOSE onui_cur;
    END IF;
    RETURN onui_rec.onui_id;
  END onui_id;

  FUNCTION dist_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER
  IS
    CURSOR dist_cur IS
      SELECT dist.dist_id
      FROM   kgc_diagnose_statussen dist
      WHERE  dist.code    = p_code
      ;
    dist_rec  dist_cur%ROWTYPE;
   BEGIN
    IF  p_code    IS NOT NULL
    THEN
      OPEN  dist_cur;
      FETCH dist_cur
      INTO  dist_rec;
      CLOSE dist_cur;
    END IF;
    RETURN dist_rec.dist_id;
  END dist_id;

  FUNCTION coty_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER
  IS
    CURSOR coty_cur IS
      SELECT coty.coty_id
      FROM   kgc_conclusie_types coty
      WHERE  coty.code    = p_code
      ;
    coty_rec  coty_cur%ROWTYPE;
   BEGIN
    IF  p_code    IS NOT NULL
    THEN
      OPEN  coty_cur;
      FETCH coty_cur
      INTO  coty_rec;
      CLOSE coty_cur;
    END IF;
    RETURN coty_rec.coty_id;
  END coty_id;

  FUNCTION ermo_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER
  IS
    CURSOR ermo_cur IS
      SELECT ermo.ermo_id
      FROM   kgc_erfmodi ermo
      WHERE  ermo.code    = p_code
      ;
    ermo_rec  ermo_cur%ROWTYPE;
   BEGIN
    IF  p_code    IS NOT NULL
    THEN
      OPEN  ermo_cur;
      FETCH ermo_cur
      INTO  ermo_rec;
      CLOSE ermo_cur;
    END IF;
    RETURN ermo_rec.ermo_id;
  END ermo_id;

  FUNCTION herk_id
  ( p_code     IN  VARCHAR2
  )  RETURN NUMBER
  IS
    CURSOR herk_cur IS
      SELECT herk.herk_id
      FROM   kgc_herkomsten  herk
      WHERE  herk.code    = p_code
      ;
    herk_rec  herk_cur%ROWTYPE;
   BEGIN
    IF  p_code    IS NOT NULL
    THEN
      OPEN  herk_cur;
      FETCH herk_cur
      INTO  herk_rec;
      CLOSE herk_cur;
    END IF;
    RETURN herk_rec.herk_id;
  END herk_id;

  FUNCTION dewy_id
  ( p_kafd_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER
  IS
    CURSOR dewy_cur IS
      SELECT dewy.dewy_id
      FROM   kgc_declaratiewijzen dewy
      WHERE  dewy.code    = p_code
    AND    dewy.kafd_id = p_kafd_id
      ;
    dewy_rec  dewy_cur%ROWTYPE;
   BEGIN
    IF  p_code    IS NOT NULL
    THEN
      OPEN  dewy_cur;
      FETCH dewy_cur
      INTO  dewy_rec;
      CLOSE dewy_cur;
    END IF;
    RETURN dewy_rec.dewy_id;
  END dewy_id;

  FUNCTION bwre_id
  ( p_ongr_id  IN  NUMBER
  , p_code     IN  VARCHAR2
  )  RETURN NUMBER
  IS
    CURSOR bwre_cur IS
      SELECT bwre.bwre_id
      FROM   kgc_bewaarredenen  bwre
      WHERE  bwre.ongr_id = p_ongr_id
      AND    bwre.code    = p_code
      ;
    bwre_rec  bwre_cur%ROWTYPE;
   BEGIN
    IF  p_ongr_id IS NOT NULL
    AND p_code    IS NOT NULL
    THEN
      OPEN  bwre_cur;
      FETCH bwre_cur
      INTO  bwre_rec;
      CLOSE bwre_cur;
    END IF;
    RETURN bwre_rec.bwre_id;
  END bwre_id;

FUNCTION mate_id
( p_kafd_id IN NUMBER
, p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR mate_cur
  IS
    SELECT mate_id
    FROM   kgc_materialen
    WHERE  kafd_id = p_kafd_id
    AND    code = p_code
    ;
  mate_rec  mate_cur%ROWTYPE;
BEGIN
  IF ( p_kafd_id IS NOT NULL
   AND p_code IS NOT NULL
     )
  THEN
    OPEN  mate_cur;
    FETCH mate_cur
    INTO  mate_rec;
    CLOSE mate_cur;
  END IF;
  RETURN mate_rec.mate_id;
END mate_id;

FUNCTION rela_id
( p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR rela_cur
  IS
    SELECT rela_id
    FROM   kgc_relaties
    WHERE  code = p_code
    ;
  rela_rec  rela_cur%ROWTYPE;
BEGIN
  IF ( p_code IS NOT NULL
     )
  THEN
    OPEN  rela_cur;
    FETCH rela_cur
    INTO  rela_rec;
    CLOSE rela_cur;
  END IF;
  RETURN rela_rec.rela_id;
END rela_id;

FUNCTION proj_id
( p_kafd_id IN NUMBER
, p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR proj_cur
  IS
    SELECT proj_id
    FROM   kgc_projecten
    WHERE  kafd_id = p_kafd_id
    AND    code = p_code
    ;
  proj_rec  proj_cur%ROWTYPE;
BEGIN
  IF ( p_kafd_id IS NOT NULL
   AND p_code IS NOT NULL
     )
  THEN
    OPEN  proj_cur;
    FETCH proj_cur
    INTO  proj_rec;
    CLOSE proj_cur;
  END IF;
  RETURN proj_rec.proj_id;
END proj_id;

FUNCTION fare_id
( p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR fare_cur
  IS
    SELECT fare_id
    FROM   kgc_familie_relaties
    WHERE  code = p_code
    ;
  fare_rec fare_cur%ROWTYPE;
BEGIN
  IF ( p_code IS NOT NULL
     )
  THEN
    OPEN  fare_cur;
    FETCH fare_cur
    INTO  fare_rec;
    CLOSE fare_cur;
  END IF;
  RETURN fare_rec.fare_id;
END fare_id;

FUNCTION conc_id
( p_kafd_id IN NUMBER
, p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR conc_cur
  IS
    SELECT conc_id
    FROM   kgc_conclusie_teksten
    WHERE  kafd_id = p_kafd_id
    AND    code = p_code
    ;
  conc_rec conc_cur%ROWTYPE;
BEGIN
  IF ( p_kafd_id IS NOT NULL
   AND p_code IS NOT NULL
     )
  THEN
    OPEN  conc_cur;
    FETCH conc_cur
    INTO  conc_rec;
    CLOSE conc_cur;
  END IF;
  RETURN conc_rec.conc_id;
END conc_id;

FUNCTION  stgr_id
( p_kafd_id IN NUMBER
, p_code    IN VARCHAR2
)
RETURN  NUMBER
IS
  CURSOR stgr_cur IS
    SELECT stgr.stgr_id
    FROM   kgc_stoftestgroepen  stgr
    WHERE  stgr.kafd_id = p_kafd_id
    AND    stgr.code    = p_code
    ;
  stgr_rec  stgr_cur%ROWTYPE;
BEGIN
  IF ( p_kafd_id IS NOT NULL
   AND p_code    IS NOT NULL
     )
  THEN
    OPEN  stgr_cur;
    FETCH stgr_cur
    INTO  stgr_rec;
    CLOSE stgr_cur;
  END IF;
  RETURN stgr_rec.stgr_id;
END stgr_id;

FUNCTION mwst_id
( p_code IN VARCHAR2
)
RETURN  NUMBER
IS
  CURSOR mwst_cur IS
    SELECT mwst.mwst_id
    FROM   kgc_meetwaardestructuur  mwst
    WHERE  mwst.code    = p_code
    ;
  mwst_rec  mwst_cur%ROWTYPE;
BEGIN
  IF ( p_code IS NOT NULL )
  THEN
    OPEN  mwst_cur;
    FETCH mwst_cur
    INTO  mwst_rec;
    CLOSE mwst_cur;
  END IF;
  RETURN mwst_rec.mwst_id;
END mwst_id;

  FUNCTION wlst_id
  ( p_kafd_id IN NUMBER
  , p_werklijst_nummer IN VARCHAR2
  )
  RETURN NUMBER
  IS
    CURSOR wlst_cur
    IS
      SELECT wlst_id
      FROM   kgc_werklijsten
      WHERE  kafd_id = p_kafd_id
      AND    werklijst_nummer = p_werklijst_nummer
    ;
    wlst_rec  wlst_cur%ROWTYPE;
  BEGIN
    IF ( p_kafd_id IS NOT NULL
     AND p_werklijst_nummer IS NOT NULL
       )
    THEN
      OPEN  wlst_cur;
      FETCH wlst_cur
      INTO  wlst_rec;
      CLOSE wlst_cur;
    END IF;
    RETURN wlst_rec.wlst_id;
  END wlst_id;

FUNCTION onty_id
( p_code IN VARCHAR2
)
RETURN  NUMBER
IS
  CURSOR onty_cur IS
    SELECT onty.onty_id
    FROM   kgc_onderzoekrelatie_types  onty
    WHERE  onty.code    = p_code
    ;
  onty_rec  onty_cur%ROWTYPE;
BEGIN
  IF ( p_code IS NOT NULL )
  THEN
    OPEN  onty_cur;
    FETCH onty_cur
    INTO  onty_rec;
    CLOSE onty_cur;
  END IF;
  RETURN onty_rec.onty_id;
END onty_id;

FUNCTION  taal_id
 (  p_code  IN  VARCHAR2
 )
 RETURN   NUMBER
 IS
  CURSOR taal_cur IS
    SELECT taal.taal_id
    FROM   kgc_talen taal
    WHERE  taal.code    = p_code
    ;
  taal_rec  taal_cur%ROWTYPE;
 BEGIN
  IF ( p_code IS NOT NULL )
  THEN
    OPEN  taal_cur;
    FETCH taal_cur
    INTO  taal_rec;
    CLOSE taal_cur;
  END IF;
  RETURN taal_rec.taal_id;
END taal_id;

FUNCTION bety_id
( p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR bety_cur
  IS
    SELECT bety.bety_id
    FROM   bas_berekening_types bety
    WHERE  bety.code = p_code
    ;
  bety_rec  bety_cur%ROWTYPE;
BEGIN
  IF ( p_code IS NOT NULL )
  THEN
    OPEN  bety_cur;
    FETCH bety_cur
    INTO  bety_rec;
    CLOSE bety_cur;
  END IF;
  RETURN bety_rec.bety_id;
END bety_id;

FUNCTION tech_id
( p_code IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR tech_cur
  IS
    SELECT tech.tech_id
    FROM   kgc_technieken tech
    WHERE  tech.code = p_code
    ;
  tech_rec  tech_cur%ROWTYPE;
BEGIN
  IF ( p_code IS NOT NULL )
  THEN
    OPEN  tech_cur;
    FETCH tech_cur
    INTO  tech_rec;
    CLOSE tech_cur;
  END IF;
  RETURN tech_rec.tech_id;
END tech_id;
END  kgc_uk2pk;
/

/
QUIT
