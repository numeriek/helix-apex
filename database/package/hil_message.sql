CREATE OR REPLACE PACKAGE "HELIX"."HIL_MESSAGE"
 AS
   type suppress_tabtype is table of varchar2 (15) index by binary_integer;
--
-- public program units
--
  type message_rectype is record
      ( msg_code          varchar2(15)
      , severity          varchar2(1)
      , severity_desc     varchar2(100) -- Translated description of severity
      , suppr_wrng_ind    varchar2(1)
      , suppr_always_ind  varchar2(1)
      , logging_ind       varchar2(1)
      , msg_text          varchar2(2000)
      , help_text         varchar2(2000)
      , lang_code         varchar2(3)
      , raise_error       boolean
      , message_found     boolean
      -- 6.5.1.0 added 2 new fields
      , table_name        varchar2(30)
      , table_rowid       rowid
      );

   type message_tabtype is table of message_rectype index by binary_integer;

function revision return varchar2;
pragma restrict_references (revision, WNDS);
--
-- Purpose  returns the revision label of this package
--
-- Usage    -
--
-- Remarks  -
--

procedure get_suppress_messages (p_suppmesg in out hil_message.suppress_tabtype);
--
-- Purpose  Initialise stack with error that may not be displayed by retrieving
--          the records with suppr_always_ind set to 'Y'
--
-- Usage    Procedure is called during package initialisation
--
-- Remarks  -
--

procedure Pop_QMS_errorinfo (p_errorrec in out hil_message.message_rectype);
--
-- Purpose  Returns the information of the last error taken from the stack
--
-- Usage    Called from the client to retrieve the QMS error information
--          after the error has been identified as a server side error.
--          Parameter
--               p_errorrec : qms error record type
--
-- Remarks
--

procedure get_Oracle_messages (p_errortab in out hil_message.message_tabtype);
--
-- Purpose  returns all error/message information from qms_message_properties
--          and qms_message_text table for a specified oracle error message that
--           needs to be replaced at runtime.
--
-- Usage    Parameters
--              p_errorrec   : returns the error/message information in a PL/SQL record
--
-- Remarks  - Currently only ORA and FRM can be replaced.
--          - Only on the client side you can add/remove messages dynamically to the stack
--

function msg_code_exists
( p_msg_code          in  varchar2 := null
, p_constraint        in  varchar2 := null
, p_existing_msg_code out varchar2
)
return boolean;
--
-- Purpose  return true (and p_existing_msg_code then contains the message code)
--          if specified message code or constraint exists in qms_message_properties.
--
-- Usage    Either the message code or the constraint name must be specified.
--
-- Remarks  -
--


procedure Get_message ( p_errorrec in out hil_message.message_rectype
                      , p_errcode in varchar2 := null
                      , p_constraint in varchar2 := null
                      , p_language in varchar2 := hil_profile.get_profile_value ('LANGUAGE',user));
--
-- Purpose  returns all error/message information from qms_message_properties
--          and qms_message_text table for a specified error message or
--          constraint name.
--          Either the error code (p_errcode) or the constraint name must be specified
--
-- Usage    Parameters
--              p_errorrec   : returns the error/message information in a PL/SQL record
--              p_errcode    : error/message code
--              p_constraint : constraint name
--              p_language   : language in which the message text is required
--
-- Remarks  -
--

procedure set_debug_flag (p_debugflag in boolean);
--
-- Purpose  Set debug flag to TRUE or FALSE if you want to display the debug flag.
--          Setting it to TRUE will display all debug messages (show_debug_info)
--          (procedure must be called whenever the debug flag is changed on the client side)
--
-- Arguments
--    p_debugflag - TRUE or FALSE
--
-- Remarks
--

function get_debug_flag return boolean;
--
-- Purpose  Get the value for the debug flag. If TRUE, debug messages will be
--          displayed.
--
-- Arguments
--
-- Remarks
--

procedure SetDebugPipeName (p_pipename in varchar2);
--
-- Purpose  Set the name of the debug pipe to other as the default
--          (QMSDEBUGUsername).
--
-- Arguments
--       p_pipename : name of the pipe you want to use.
--
-- Remarks : Setting this to another value as the default may cause problems
--           since this setting has to be repeated for each session/connection
--           (OPEN_FORM (.., session, ..))
--

function GetDebugPipeName return varchar2;
--
-- Purpose  Returns the name of the pipe that is currently used for debuging
--
-- Arguments
--
-- Remarks
--

function ReceiveDebugPipe (p_pipename in varchar2 := hil_message.Getdebugpipename) return varchar2;
--
-- Purpose  Check the debug pipe to see if a debug message was recieved and return the
--          message. The function will return a NULL values if no message was received.
--
-- Arguments
--
-- Remarks
--

procedure SendDebugPipe ( p_message in varchar2
                        , p_pipename in varchar2 := hil_message.Getdebugpipename);
--
-- Purpose  Send a message to the debug pipe so it can be read and displayed or written to file.
--
-- Arguments
--    p_message - debug message
-- Remarks
--

procedure CreateDebugPipe (p_pipename in varchar2 := hil_message.Getdebugpipename);
--
-- Purpose  Creation of the debug pipe.
--
-- Arguments
--
-- Remarks
--

procedure RemoveDebugPipe (p_pipename in varchar2 := hil_message.Getdebugpipename);
--
-- Purpose  Close the debug pipe.
--
-- Arguments
--
-- Remarks
--

procedure add_language
( p_code       qms_message_text.msp_code%type
, p_language   qms_message_text.language%type
, p_text       qms_message_text.text%type
, p_help_text  qms_message_text.help_text%type
);
-- Purpose  Add translation in new language to existing message
--
-- Usage    From hil_message package
--
-- Remarks
--

procedure create_message
( p_code              qms_message_properties.code%type
, p_description       qms_message_properties.description%type
, p_language          qms_message_text.language%type
, p_text              qms_message_text.text%type
, p_help_text         qms_message_text.help_text%type              default null
, p_severity          qms_message_properties.severity%type         default 'E'
, p_logging_ind       qms_message_properties.logging_ind%type      default 'N'
, p_suppr_wrng_ind    qms_message_properties.suppr_wrng_ind%type   default 'N'
, p_suppr_always_ind  qms_message_properties.suppr_always_ind%type default 'N'
, p_constraint_name   qms_message_properties.constraint_name%type  default null
);
-- Purpose  Create record in message properties
--          and first record in message text
--
-- Usage    From hil_message package
--
-- Remarks
--


function get_next_msg_code
-- Purpose  Derive next unused message code within messages with prefix p_prefix
--          It is assumed that following the prefix are a '-' and then only numbers
--          up to a code length of qms$errors.c_errorlength
--
-- Usage    for instance from utilities that create new messages
--
-- Remarks
--
( p_msg_prefix in varchar2
)
return varchar2;


procedure delete_message
( p_msg_code in varchar2
);
-- Purpose  Delete message properties and all translations
-- Usage    from utilities
-- Remarks


function get_severity_string
(p_severity in varchar2)
return varchar2;
-- Purpose  Get translated description of severity (error, warning, etc.)
-- Usage    Call with message severity type as input: E, W, Q, I or M
-- Remarks
PRAGMA RESTRICT_REFERENCES (get_severity_string, WNDS);

function msg_code_language_exists
( p_msg_code    in  varchar2 := null
, p_constraint  in  varchar2 := null
, p_language    in varchar2
)
return boolean;
--
-- Purpose  return true if specified message code or constraint exists in
--          qms_message_properties and a message text exists in the specified
--          language
--
-- Usage    Either the message code or the constraint name must be specified.
--
-- Remarks  -
--

procedure update_msg_code
( p_old_msg_code   in   varchar2
, p_new_msg_code   in   varchar2
);
--
-- Purpose  Update the message code of a specific message
--
-- Usage    Called from headstart utility hsu_emsg
--
-- Remarks  -
--

--
-- 16-12-1999 WP vd Lugt Added PRAGMA RESTRICT_REFERENCES (...WNDS) for:
--                       - get_message
--                       - get_debug_flag
--                       - SendDebugPipe
--                       - GetDebugPipeName
--                       - hil_message
PRAGMA RESTRICT_REFERENCES (get_message, WNDS);
PRAGMA RESTRICT_REFERENCES (get_debug_flag, WNDS);
PRAGMA RESTRICT_REFERENCES (SendDebugPipe, WNDS);
PRAGMA RESTRICT_REFERENCES (GetDebugPipeName, WNDS);
PRAGMA RESTRICT_REFERENCES (hil_message, WNDS);

-- For some reason, we need to close the package specification here. Designer will remove it
-- when it generates the .pkb file.
end;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."HIL_MESSAGE"
 AS
/*****************************************************************************************
Created by      Luc Van der Haegen, Oracle Consulting Services,
                Advanced Development Group Oracle Belgium

Date created    03-06-1998

Change History  (most recent on top!)

When        Who                            program unit / construct
  Revision  What
------------------------------------------------------------------------------------------
18-jan-2001 Sandra Muller                  message_rectype
   6.5.1.0  Rowid extension
11-oct-2000 Marc Vahsen                    msg_code_language_exists and update_msg_code
   6.5.0.2  new functions, used by headstart utilities
29-mar-2000 Steven Davelaar                get_severity_string
   5.0.0.6  new function to get transalted severity description
16-feb-2000 Sandra Muller                  get_next_msg_code
   5.0.0.5  new function to get next free message number within message prefix
            + made revision a public function
07-feb-2000 Sandra Muller                  msg_code_exists
   5.0.0.4  new function to check if message code or constraint exists in msp
04-feb-2000 Sandra Muller                  create_message, add_language
   5.0.0.3  new procedures to insert new message with 1st translation, or add translation
18/03/1999  Steven Davelaar               package spec: record type message_rectype
   5.0.0.2  msg_text and help_text both changed to varchar2(2000)
11/03/1999  Luc Van der Haegen
   5.0.0.1  Initial version control added
*****************************************************************************************/

REVISION_LABEL   constant   varchar2(10)   := '6.5.1.0';

function revision return varchar2 is
--
-- Purpose  returns the revision label of this package
--
-- Usage    -
--
-- Remarks  -
--

begin

   return (REVISION_LABEL);

end revision;

procedure get_suppress_messages (p_suppmesg in out hil_message.suppress_tabtype) is
--
-- Purpose  Initialise stack with error that may not be displayed by retrieving
--          the records with suppr_always_ind set to 'Y'
--
-- Usage    Procedure is called during package initialisation
--
-- Remarks  -
--
begin

   qms_message.get_suppress_messages (p_suppmesg);

end get_suppress_messages;

procedure Pop_QMS_errorinfo (p_errorrec in out hil_message.message_rectype) is
--
-- Purpose  Returns the information of the last error taken from the stack
--
-- Usage    Called from the client to retrieve the QMS error information
--          after the error has been identified as a server side error.
--          Parameter
--               p_errorrec : qms error record type
--
-- Remarks
--
begin

   qms_message.Pop_QMS_errorinfo (p_errorrec);

end Pop_QMS_errorinfo;

procedure get_Oracle_messages (p_errortab in out hil_message.message_tabtype) is
--
-- Purpose  returns all error/message information from qms_message_properties
--          and qms_message_text table for a specified oracle error message that
--           needs to be replaced at runtime.
--
-- Usage    Parameters
--              p_errorrec   : returns the error/message information in a PL/SQL record
--
-- Remarks  - Currently only ORA and FRM can be replaced.
--          - Only on the client side you can add/remove messages dynamically to the stack
--
begin

   qms_message.get_Oracle_messages (p_errortab);

end get_Oracle_messages;


function msg_code_exists
( p_msg_code          in  varchar2 := null
, p_constraint        in  varchar2 := null
, p_existing_msg_code out varchar2
)
return boolean
is
--
-- Purpose  return true (and p_existing_msg_code then contains the message code)
--          if specified message code or constraint exists in qms_message_properties.
--
-- Usage    Either the message code or the constraint name must be specified.
--
-- Remarks  -
--
begin
  return qms_message.msg_code_exists(p_msg_code, p_constraint, p_existing_msg_code);
end msg_code_exists;


procedure Get_Message ( p_errorrec in out hil_message.message_rectype
                      , p_errcode in varchar2 := null
                      , p_constraint in varchar2 := null
                      , p_language in varchar2 := hil_profile.get_profile_value ('LANGUAGE', user)) is
--
-- Purpose  returns all error/message information from qms_message_properties
--          and qms_message_text table for a specified error message or
--          constraint name.
--          Either the error code (p_errcode) or the constraint name must be specified
--
-- Usage    Parameters
--              p_errorrec   : returns the error/message information in a PL/SQL record
--              p_errcode    : error/message code
--              p_constraint : constraint name
--              p_language   : language in which the message text is required
--
-- Remarks  -
--
begin

   qms_message.Get_Message ( p_errorrec
                           , p_errcode
                           , p_constraint
                           , p_language);

end Get_Message;

procedure set_debug_flag (p_debugflag in boolean) is
--
-- Purpose  Set debug flag to TRUE or FALSE if you want to display the debug flag.
--          Setting it to TRUE will display all debug messages (show_debug_info)
--          (procedure must be called whenever the debug flag is changed on the client side)
--
-- Arguments
--    p_display - target display, either 'M' (message line) or 'A' (alert box)
--
-- Remarks
--
begin

   qms_message.set_debug_flag (p_debugflag);

end set_debug_flag;

function get_debug_flag return boolean is
--
-- Purpose  Get the value for the debug flag. If TRUE, debug messages will be
--          displayed.
--
-- Arguments
--
-- Remarks
--
begin

   return (qms_message.get_debug_flag);

end get_debug_flag;

procedure SetDebugPipeName (p_pipename in varchar2) is
--
-- Purpose  Set the name of the debug pipe to other as the default
--          (QMSDEBUGUsername).
--
-- Arguments
--       p_pipename : name of the pipe you want to use.
--
-- Remarks : Setting this to another value as the default may cause problems
--           since this setting has to be repeated for each session/connection
--           (OPEN_FORM (.., session, ..))
--
begin

   qms_message.SetDebugPipeName (p_pipename);

end SetDebugPipeName;

function GetDebugPipeName return varchar2 is
--
-- Purpose  Returns the name of the pipe that is currently used for debuging
--
-- Arguments
--
-- Remarks
--
begin

   return (qms_message.GetDebugpipeName);

end GetDebugPipeName;

function ReceiveDebugPipe (p_pipename in varchar2 := hil_message.Getdebugpipename)
   return varchar2 is
--
-- Purpose  Check the debug pipe to see if a debug message was recieved and return the
--          message. The function will return a NULL values if no message was received.
--
-- Arguments
--
-- Remarks
--
begin

   return (qms_message.ReceiveDebugpipe (p_pipename));

end ReceiveDebugPipe;

procedure SendDebugPipe ( p_message in varchar2
                        , p_pipename in varchar2 := hil_message.Getdebugpipename) is
--
-- Purpose  Send a message to the debug pipe so it can be read and displayed or written to file.
--
-- Arguments
--    p_message - debug message
-- Remarks
--
begin

   qms_message.SendDebugPipe (p_message, p_pipename);

end SendDebugPipe;

procedure CreateDebugPipe (p_pipename in varchar2 := hil_message.Getdebugpipename) is
--
-- Purpose  Send a message to the debug pipe so it can be read and displayed or written to file.
--
-- Arguments
--    p_message - debug message
-- Remarks
--
begin

   qms_message.CreateDebugPipe (p_pipename);

end CreateDebugPipe;

procedure RemoveDebugPipe (p_pipename in varchar2 := hil_message.Getdebugpipename) is
--
-- Purpose  Send a message to the debug pipe so it can be read and displayed or written to file.
--
-- Arguments
--    p_message - debug message
-- Remarks
--
begin

   qms_message.RemoveDebugPipe (p_pipename);

end RemoveDebugPipe;


procedure add_language
( p_code       qms_message_text.msp_code%type
, p_language   qms_message_text.language%type
, p_text       qms_message_text.text%type
, p_help_text  qms_message_text.help_text%type
)
-- Purpose  Add translation in new language to existing message
--
-- Usage    From hil_message package
--
-- Remarks
--
is
begin
   qms_message.add_language
   ( p_code
   , p_language
   , p_text
   , p_help_text
   );
end add_language;


procedure create_message
( p_code              qms_message_properties.code%type
, p_description       qms_message_properties.description%type
, p_language          qms_message_text.language%type
, p_text              qms_message_text.text%type
, p_help_text         qms_message_text.help_text%type              default null
, p_severity          qms_message_properties.severity%type         default 'E'
, p_logging_ind       qms_message_properties.logging_ind%type      default 'N'
, p_suppr_wrng_ind    qms_message_properties.suppr_wrng_ind%type   default 'N'
, p_suppr_always_ind  qms_message_properties.suppr_always_ind%type default 'N'
, p_constraint_name   qms_message_properties.constraint_name%type  default null
)
-- Purpose  Create record in message properties
--          and first record in message text
--
-- Usage    From hil_message package
--
-- Remarks
--
is
begin
   qms_message.create_message
   ( p_code
   , p_description
   , p_language
   , p_text
   , p_help_text
   , p_severity
   , p_logging_ind
   , p_suppr_wrng_ind
   , p_suppr_always_ind
   , p_constraint_name
   );
end create_message;

function get_next_msg_code
-- Purpose  Derive next unused message code within messages with prefix p_prefix
--          It is assumed that following the prefix are a '-' and then only numbers
--          up to a code length of qms$errors.c_errorlength
--
-- Usage    for instance from utilities that create new messages
--
-- Remarks
--
( p_msg_prefix in varchar2
)
return varchar2
is
begin
   return qms_message.get_next_msg_code(p_msg_prefix);
end get_next_msg_code;


procedure delete_message
( p_msg_code in varchar2
)
-- Purpose  Delete message properties and all translations
-- Usage    from utilities
-- Remarks
is
begin
   qms_message.delete_message(p_msg_code);
end delete_message;

function get_severity_string
(p_severity in varchar2)
return varchar2
-- Purpose  Get translated description of severity (error, warning, etc.)
-- Usage    Call with message severity type as input: E, W, Q, I or M
-- Remarks
is
begin
   return qms_message.get_severity_string(p_severity);
end get_severity_string;

-- 6.5.0.2 new
function msg_code_language_exists
( p_msg_code    in  varchar2 := null
, p_constraint  in  varchar2 := null
, p_language    in  varchar2
)
return boolean
--
-- Purpose  return true if specified message code or constraint exists in
--          qms_message_properties and a message text exists in the specified
--          language
--
-- Usage    Either the message code or the constraint name must be specified.
--
-- Remarks  -
--
is
begin
  return qms_message.msg_code_language_exists
         ( p_msg_code    => p_msg_code
         , p_constraint  => p_constraint
         , p_language    => p_language
         );
end msg_code_language_exists;

-- 6.5.0.2 new
procedure update_msg_code
( p_old_msg_code   in   varchar2
, p_new_msg_code   in   varchar2
)
--
-- Purpose  Update the message code of a specific message
--
-- Usage    Called from headstart utility hsu_emsg
--
-- Remarks  -
--
is
begin
   qms_message.update_msg_code
   ( p_old_msg_code => p_old_msg_code
   , p_new_msg_code => p_new_msg_code
   );
end update_msg_code;

-- For some reason, we need to close the package body here. Designer will remove it
-- when it generates the .pkb file.
end;
/

/
QUIT
