CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_GENERAL"
IS
   /*
   ================================================================================
   Package Name : gen_pck_general
   Usage        : This package contains general procedures and functions
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   FUNCTION f_get_user_id(i_user_id  IN VARCHAR2 DEFAULT NULL
                         ,i_name     IN VARCHAR2 DEFAULT NULL)
      RETURN NUMBER;
   FUNCTION f_get_role_description(i_rle_id IN gen_appl_roles.id%TYPE)
      RETURN VARCHAR2;
   --
   FUNCTION f_get_role_code(i_rle_id IN gen_appl_roles.id%TYPE)
      RETURN VARCHAR2;
   --
   PROCEDURE p_delete_role(i_appl_role_id IN gen_appl_roles.id%TYPE);
   -- ----------------------------
   -- Function that returns a list of the emailaddresses of all users that
   -- have a certain authentication role
   -- ----------------------------
   FUNCTION f_role_mail_list(
      i_role_code       IN gen_appl_roles.role_code%TYPE
     ,i_application_id  IN gen_appl_roles.application_id%TYPE)
      RETURN VARCHAR2;
   --
   FUNCTION f_get_application_description
      RETURN VARCHAR2;
   --
   FUNCTION f_get_application_version(
      i_application_id  IN gen_change_history.application_id%TYPE DEFAULT NULL)
      RETURN VARCHAR2;
   --
   FUNCTION f_get_ir_filter_value(i_application_id         IN NUMBER
                                 ,i_page_id                IN NUMBER
                                 ,i_application_user       IN VARCHAR2
                                 ,i_condition_column_name  IN VARCHAR2
                                 ,i_condition_operator     IN VARCHAR2
                                 ,i_condition_enabled      IN VARCHAR2)
      RETURN VARCHAR2;
   --
   PROCEDURE p_initialize;
   --
   FUNCTION f_get_initial_status(
      i_category        IN gen_statuses.category%TYPE
     ,i_application_id  IN gen_statuses.application_id%TYPE DEFAULT NULL)
      RETURN NUMBER;
   FUNCTION f_get_status(
      i_category        IN gen_statuses.category%TYPE
     ,i_status_code     IN gen_statuses.status_code%TYPE
     ,i_application_id  IN gen_statuses.application_id%TYPE DEFAULT NULL)
      RETURN NUMBER;
   FUNCTION f_get_status_code(i_category   IN VARCHAR2
                             ,i_status_id  IN NUMBER)
      RETURN VARCHAR2;
   --
   FUNCTION f_get_status_color(
      i_category        IN gen_statuses.category%TYPE
     ,i_status_code     IN gen_statuses.status_code%TYPE
     ,i_application_id  IN gen_statuses.application_id%TYPE DEFAULT NULL)
      RETURN gen_statuses.color%TYPE;
   --
   FUNCTION f_get_appl_parameter_value(
      i_parameter_name  IN gen_appl_parameters.parameter_name%TYPE
     ,i_application_id  IN gen_appl_parameters.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_parameters.param_value%TYPE
      RESULT_CACHE;
   FUNCTION f_get_validation_val_low_n(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_low_n%TYPE;
   FUNCTION f_get_validation_val_high_n(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_high_n%TYPE;
   FUNCTION f_get_validation_val_low_c(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_low_c%TYPE;
   FUNCTION f_get_validation_val_high_c(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_high_c%TYPE;
   FUNCTION f_get_validation_val_low_d(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_low_d%TYPE;
   FUNCTION f_get_validation_val_high_d(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_high_d%TYPE;
   FUNCTION f_get_lookup_desc_value(
      i_lookup_type     IN gen_appl_lookups.lookup_type%TYPE
     ,i_value_short     IN gen_appl_lookups.value_short%TYPE
     ,i_application_id  IN gen_appl_lookups.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_lookups.desc_value%TYPE;
   --
   FUNCTION f_is_num(i_string IN VARCHAR)
      RETURN BOOLEAN;
   --
   FUNCTION f_is_date(i_string  IN VARCHAR
                     ,i_format  IN VARCHAR2)
      RETURN BOOLEAN;
   -- set session state for application item (first check if ai exists)
   PROCEDURE p_set_ai_default_value(i_name     VARCHAR2
                                   ,i_value    VARCHAR2
                                   ,i_app_id   NUMBER DEFAULT NULL);
   --
   PROCEDURE p_set_security_group_id;
   FUNCTION f_clob2blob(i_clob IN CLOB)
      RETURN BLOB;
   PROCEDURE p_delete_record(i_table_name  IN VARCHAR2
                            ,i_id          IN VARCHAR2);

   FUNCTION f_get_collection_rec(
      i_collection_name  IN apex_collections.collection_name%TYPE
     ,i_seq              IN apex_collections.seq_id%TYPE)
      RETURN apex_collections%ROWTYPE;

   PROCEDURE p_update_collection_rec(
      i_collection_rec  IN apex_collections%ROWTYPE);

   PROCEDURE p_cleanup_history_tables;
END gen_pck_general;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_GENERAL"
IS
   g_package  CONSTANT VARCHAR2(31) := $$plsql_unit || '.';
   -- -----------------------------------------------------------------
   -- Function to determine user id based on the given user_id or name
   -- -----------------------------------------------------------------
   FUNCTION f_get_user_id(i_user_id  IN VARCHAR2 DEFAULT NULL
                         ,i_name     IN VARCHAR2 DEFAULT NULL)
      RETURN NUMBER
   IS
      l_user_id  NUMBER;
   BEGIN
      IF i_user_id IS NOT NULL
      THEN
         SELECT aus.id
           INTO l_user_id
           FROM gen_appl_users aus
          WHERE UPPER(aus.user_id) = UPPER(i_user_id);
      ELSIF i_name IS NOT NULL
      THEN
         SELECT aus.user_id
           INTO l_user_id
           FROM gen_appl_users aus
          WHERE UPPER(aus.user_name) LIKE ('%' || UPPER(i_name) || '%');
      END IF;
      RETURN (l_user_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (NULL);
   END f_get_user_id;
   -- -------------------------------------
   -- Function returning role description
   -- -------------------------------------
   FUNCTION f_get_role_description(i_rle_id IN gen_appl_roles.id%TYPE)
      RETURN VARCHAR2
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'f_get_role_description';
      r_rle            gen_appl_roles%ROWTYPE;
   BEGIN
      SELECT rle.*
        INTO r_rle
        FROM gen_appl_roles rle
       WHERE rle.id = i_rle_id;
      --
      RETURN r_rle.description;
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
         RETURN '0';
   END f_get_role_description;
   --
   -- ----------------------------
   -- Function returning role code
   -- ----------------------------
   FUNCTION f_get_role_code(i_rle_id IN gen_appl_roles.id%TYPE)
      RETURN VARCHAR2
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'f_get_role_code';
      r_rle            gen_appl_roles%ROWTYPE;
   BEGIN
      SELECT rle.*
        INTO r_rle
        FROM gen_appl_roles rle
       WHERE rle.id = i_rle_id;
      --
      RETURN r_rle.role_code;
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
         RETURN '0';
   END f_get_role_code;
   -- ----------------------------
   -- procedure that deletes the role with id i_arl_id
   -- ----------------------------
   PROCEDURE p_delete_role(i_appl_role_id IN gen_appl_roles.id%TYPE)
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'p_delete_role';
   BEGIN
      -- remove linked authorization or authentication roles
      DELETE FROM gen_appl_roles
            WHERE id = i_appl_role_id;
      gen_pck_exception.p_json_error('');
   EXCEPTION
      WHEN OTHERS
      THEN
         gen_pck_exception.p_json_error(gen_pck_exception.f_get_err_message(
                                           SQLERRM
                                          ,v('LANGCODE')
                                          ,'N'
                                          ,'N'));
         logger.log_error(p_text => SQLERRM
                         ,p_scope => l_proc);
   END p_delete_role;
   -- ----------------------------
   -- Function that returns a list of the emailaddresses of all users that
   -- are connected to a certain authorization role
   -- ----------------------------
   FUNCTION f_role_mail_list(
      i_role_code       IN gen_appl_roles.role_code%TYPE
     ,i_application_id  IN gen_appl_roles.application_id%TYPE)
      RETURN VARCHAR2
   IS
      CURSOR c_admin
      IS
         SELECT LISTAGG(email
                       ,',')
                WITHIN GROUP (ORDER BY 1)
                   mail_list
           FROM (SELECT DISTINCT email
                   FROM gen_appl_users aus
                        INNER JOIN gen_appl_user_roles aur
                           ON (aus.id = aur.appl_user_id)
                        INNER JOIN gen_appl_role_roles arr
                           ON (aur.appl_role_id =
                                  arr.authentication_appl_role_id)
                        INNER JOIN gen_appl_roles arl
                           ON (arr.authorization_appl_role_id = arl.id)
                  WHERE UPPER(arl.role_code) = i_role_code
                    AND arl.active_ind = 'Y'
                    AND aur.active_ind = 'Y'
                    AND aus.active_ind = 'Y'
                    AND aus.email IS NOT NULL
                    AND arl.application_id = i_application_id);
      r_admin  c_admin%ROWTYPE;
   BEGIN
      OPEN c_admin;
      FETCH c_admin
         INTO r_admin;
      CLOSE c_admin;
      RETURN r_admin.mail_list;
   END f_role_mail_list;
   --
   -- ---------------------------------------------------------------------
   -- Function to retrieve application description from GEN_APPL_PARAMETERS
   -- ---------------------------------------------------------------------
   FUNCTION f_get_application_description
      RETURN VARCHAR2
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_application_description' ;
   BEGIN
      RETURN gen_pck_general.f_get_appl_parameter_value(
                i_parameter_name => 'APPL_INFO'
               ,i_application_id => v('APP_ID'));
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
         RETURN (NULL);
   END f_get_application_description;
   -- ----------------------------------------
   -- Function to retrieve application version
   -- ----------------------------------------
   FUNCTION f_get_application_version(
      i_application_id  IN gen_change_history.application_id%TYPE DEFAULT NULL)
      RETURN VARCHAR2
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'f_get_application_version';
      l_appl_version   gen_change_history.version_nr%TYPE;
      CURSOR c_version
      IS
         SELECT chh.version_nr
           FROM gen_change_history chh
          WHERE i_application_id IS NOT NULL
            AND application_id = i_application_id
            AND id = (SELECT MAX(id)
                        FROM gen_change_history
                       WHERE application_id = i_application_id)
         UNION ALL
         SELECT chh.version_nr
           FROM gen_change_history chh
          WHERE application_id IS NULL
            AND id = (SELECT MAX(id)
                        FROM gen_change_history
                       WHERE application_id IS NULL);
   BEGIN
      OPEN c_version;
      FETCH c_version
         INTO l_appl_version;
      CLOSE c_version;
      --
      RETURN (l_appl_version);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
         RETURN (NULL);
   END f_get_application_version;
   --
   FUNCTION f_get_ir_filter_value(i_application_id         IN NUMBER
                                 ,i_page_id                IN NUMBER
                                 ,i_application_user       IN VARCHAR2
                                 ,i_condition_column_name  IN VARCHAR2
                                 ,i_condition_operator     IN VARCHAR2
                                 ,i_condition_enabled      IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'f_get_ir_filter_waarde';
      CURSOR c_filter
      IS
           SELECT condition_expression
             FROM apex_application_page_ir_cond
            WHERE application_id = i_application_id
              AND page_id = i_page_id
              AND UPPER(application_user) =
                     UPPER(NVL(i_application_user, application_user))
              AND UPPER(condition_column_name) =
                     UPPER(NVL(i_condition_column_name, condition_column_name))
              AND UPPER(condition_operator) =
                     UPPER(NVL(i_condition_operator, condition_operator))
              AND UPPER(condition_enabled) =
                     UPPER(NVL(i_condition_enabled, condition_enabled))
         ORDER BY last_updated_on DESC; -- Sort last change filter
      r_filter         c_filter%ROWTYPE;
   BEGIN
      IF c_filter%ISOPEN
      THEN
         CLOSE c_filter;
      END IF;
      --
      OPEN c_filter;
      FETCH c_filter
         INTO r_filter;
      --
      IF r_filter.condition_expression IS NOT NULL
      THEN
         RETURN (r_filter.condition_expression);
      ELSE
         RETURN (NULL);
      END IF;
      --
      CLOSE c_filter;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (NULL);
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
         RETURN (NULL);
   END;
   --
   PROCEDURE p_initialize
   IS
      l_app_env                    gen_appl_parameters.param_value%TYPE;
      l_show_audit_columns         VARCHAR2(1);
      l_show_audit_columns_ir      VARCHAR2(1);
      l_show_audit_columns_tabfrm  VARCHAR2(1);
      l_date_format                VARCHAR2(1000);
      l_date_time_format           VARCHAR2(1000);
      l_user_id                    NUMBER;
      l_user_fullname              gen_appl_users.user_name%TYPE;
      l_database_msg               VARCHAR2(100) := 'Environment: ';
      l_language                   gen_named_preferences.DEFAULT_VALUE%TYPE;
      l_app_id                     NUMBER;
      l_app_user                   VARCHAR2(100);
   BEGIN
      l_app_id     := nv('APP_ID');
      l_app_user   := v('APP_USER');
      l_app_env      :=
         gen_pck_general.f_get_appl_parameter_value(
            i_parameter_name => 'DATABASE'
           ,i_application_id => l_app_id);
      BEGIN
         SELECT id
               ,user_name
           INTO l_user_id
               ,l_user_fullname
           FROM gen_appl_users
          WHERE UPPER(user_id) = UPPER(l_app_user)
            AND active_ind = 'Y';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id         := -1; --User not in user table
            l_user_fullname   := NULL;
      END;
      IF l_app_env IS NULL
      THEN
         l_database_msg   := l_database_msg || ' unknown';
      ELSE
         l_database_msg   := l_app_env;
      END IF;
      -- set value for application items after checking their existance

      p_set_ai_default_value(i_name => 'G_APP_USER_ID'
                            ,i_value => l_user_id
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_APP_USER_NAME'
                            ,i_value => l_app_user
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_USER_FULLNAME'
                            ,i_value => l_user_fullname
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_DATABASE_MSG'
                            ,i_value => l_database_msg
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(
         i_name => 'G_LDAP'
        ,i_value => CASE
                      WHEN gen_pck_general.f_get_appl_parameter_value(
                              'LDAP_ACTIVE'
                             ,l_app_id) = 'Y'
                       AND gen_pck_authentication.g_login_type = 'LDAP'
                      THEN
                         'Y'
                      ELSE
                         'N'
                   END
        ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_APP_APPLICATION_DESC'
                            ,i_value => f_get_application_description
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(
         i_name => 'G_APP_APPLICATION_VERSION'
        ,i_value => '(version: ' || f_get_application_version || ')'
        ,i_app_id => l_app_id);
      -- eventueel aanmaken van user preferences
      DECLARE
         l_pref_val  VARCHAR2(400);
      BEGIN
         FOR r_npe IN (SELECT *
                         FROM gen_named_preferences)
         LOOP
            l_pref_val      :=
               apex_util.get_preference(p_preference => r_npe.preference_name
                                       ,p_user => l_app_user);
            logger.LOG(
               l_app_user || ' ' || r_npe.preference_name || ': ' || l_pref_val);
            IF l_pref_val IS NULL
            THEN
               apex_util.set_preference(p_preference => r_npe.preference_name
                                       ,p_value => r_npe.DEFAULT_VALUE
                                       ,p_user => l_app_user);
            END IF;
            IF r_npe.preference_name = 'FSP_LANGUAGE_PREFERENCE'
            THEN
               l_language      :=
                  COALESCE(l_pref_val
                          ,r_npe.DEFAULT_VALUE);
            END IF;
            IF r_npe.preference_name = 'F_SHOW_AUDIT_COLUMNS'
            THEN
               l_show_audit_columns      :=
                  COALESCE(l_pref_val
                          ,r_npe.DEFAULT_VALUE);
            END IF;
            IF r_npe.preference_name = 'F_SHOW_AUDIT_COLUMNS_IR'
            THEN
               l_show_audit_columns_ir      :=
                  COALESCE(l_pref_val
                          ,r_npe.DEFAULT_VALUE);
            END IF;
            IF r_npe.preference_name = 'F_SHOW_AUDIT_COLUMNS_IG'
            THEN
               l_show_audit_columns_tabfrm      :=
                  COALESCE(l_pref_val
                          ,r_npe.DEFAULT_VALUE);
            END IF;
            IF r_npe.preference_name = 'F_DATE_FORMAT'
            THEN
               l_date_format      :=
                  COALESCE(l_pref_val
                          ,r_npe.DEFAULT_VALUE);
            END IF;
            IF r_npe.preference_name = 'F_DATE_TIME_FORMAT'
            THEN
               l_date_time_format      :=
                  COALESCE(l_pref_val
                          ,r_npe.DEFAULT_VALUE);
            END IF;
         END LOOP;
      END;
      p_set_ai_default_value(i_name => 'LANGCODE'
                            ,i_value => COALESCE(l_language
                                                ,'en')
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_SHOW_AUDIT_COLUMNS'
                            ,i_value => COALESCE(l_show_audit_columns
                                                ,'N')
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_SHOW_AUDIT_COLUMNS_IR'
                            ,i_value => COALESCE(l_show_audit_columns_ir
                                                ,'Y')
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_SHOW_AUDIT_COLUMNS_IG'
                            ,i_value => COALESCE(l_show_audit_columns_tabfrm
                                                ,'Y')
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_DATE_FORMAT'
                            ,i_value => COALESCE(l_date_format
                                                ,'DD-MM-YYYY')
                            ,i_app_id => l_app_id);
      p_set_ai_default_value(i_name => 'G_DATE_TIME_FORMAT'
                            ,i_value => COALESCE(l_date_time_format
                                                ,'DD-MM-YYYY HH24:MI:SS')
                            ,i_app_id => l_app_id);
   END p_initialize;
   --
   FUNCTION f_get_initial_status(
      i_category        IN gen_statuses.category%TYPE
     ,i_application_id  IN gen_statuses.application_id%TYPE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_retval  NUMBER;
      CURSOR c_sts
      IS
           SELECT id
             FROM gen_statuses sts
            WHERE sts.category = i_category
              AND sts.active_ind = 'Y'
              AND sts.initial_ind = 'Y'
              AND ((i_application_id IS NOT NULL
                AND i_application_id = sts.application_id)
                OR sts.application_id IS NULL)
         ORDER BY sts.application_id NULLS LAST
                 ,sts.sort_order;
   BEGIN
      OPEN c_sts;
      FETCH c_sts
         INTO l_retval;
      CLOSE c_sts;
      --
      RETURN l_retval;
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END f_get_initial_status;
   FUNCTION f_get_status(
      i_category        IN gen_statuses.category%TYPE
     ,i_status_code     IN gen_statuses.status_code%TYPE
     ,i_application_id  IN gen_statuses.application_id%TYPE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_retval  NUMBER;
      CURSOR c_sts
      IS
           SELECT sts.id
             FROM gen_statuses sts
            WHERE sts.category = i_category
              AND sts.active_ind = 'Y'
              AND sts.status_code = i_status_code
              AND ((i_application_id IS NOT NULL
                AND i_application_id = sts.application_id)
                OR sts.application_id IS NULL)
         ORDER BY sts.application_id NULLS LAST
                 ,sts.sort_order;
   BEGIN
      OPEN c_sts;
      FETCH c_sts
         INTO l_retval;
      CLOSE c_sts;
      --
      RETURN l_retval;
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END f_get_status;
   FUNCTION f_get_status_code(i_category   IN VARCHAR2
                             ,i_status_id  IN NUMBER)
      RETURN VARCHAR2
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'f_get_status_code';
      v_code           gen_statuses.status_code%TYPE;
   BEGIN
      SELECT status_code
        INTO v_code
        FROM gen_statuses
       WHERE UPPER(category) = UPPER(i_category)
         AND ((i_status_id IS NOT NULL
           AND id = i_status_id));
      RETURN (v_code);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
         RETURN ('Unknown');
   END;
   --
   FUNCTION f_get_status_color(
      i_category        IN gen_statuses.category%TYPE
     ,i_status_code     IN gen_statuses.status_code%TYPE
     ,i_application_id  IN gen_statuses.application_id%TYPE DEFAULT NULL)
      RETURN gen_statuses.color%TYPE
   IS
      l_retval  gen_statuses.color%TYPE;
      CURSOR c_sts
      IS
           SELECT sts.color
             FROM gen_statuses sts
            WHERE sts.category = i_category
              AND sts.active_ind = 'Y'
              AND sts.status_code = i_status_code
              AND ((i_application_id IS NOT NULL
                AND i_application_id = sts.application_id)
                OR sts.application_id IS NULL)
         ORDER BY sts.application_id NULLS LAST
                 ,sts.sort_order;
   BEGIN
      OPEN c_sts;
      FETCH c_sts
         INTO l_retval;
      CLOSE c_sts;
      --
      RETURN l_retval;
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;
   --
   -------------------------------------------------------------------------
   -- Function to retrieve application parameter value
   -------------------------------------------------------------------------
   FUNCTION f_get_appl_parameter_value(
      i_parameter_name  IN gen_appl_parameters.parameter_name%TYPE
     ,i_application_id  IN gen_appl_parameters.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_parameters.param_value%TYPE
      RESULT_CACHE
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_appl_parameter_value' ;
      l_value          gen_appl_parameters.param_value%TYPE;
      l_active         gen_appl_parameters.active_ind%TYPE;
      CURSOR c_param
      IS
           SELECT param_value
             FROM gen_appl_parameters apm
            WHERE UPPER(apm.parameter_name) = UPPER(i_parameter_name)
              AND active_ind = 'Y'
              AND ((i_application_id IS NOT NULL
                AND i_application_id = apm.application_id) -- parameter specific to current application
                OR apm.application_id IS NULL) -- or general parameter
         ORDER BY apm.application_id NULLS LAST; -- specific parameters first
   BEGIN
      -- function retrieves parameter value of parameter p_parameter_name  for p_application_id
      -- function returns null when parameters is unknown (for application)
      -- or when p_application_id is not given and more than one row exists
      OPEN c_param;
      FETCH c_param
         INTO l_value;
      CLOSE c_param;
      RETURN (l_value);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information(
               'i_parameter_name / i_application_id: '
            || i_parameter_name
            || ' / '
            || i_application_id
           ,l_proc);
         logger.log_information(dbms_utility.format_error_backtrace
                               ,l_proc);
         RETURN (NULL);
   END f_get_appl_parameter_value;
   FUNCTION f_get_appl_validation_rec(
      i_domain                   IN gen_appl_validations.domain_name%TYPE
     ,i_domain_datatype          IN gen_appl_validations.domain_datatype%TYPE
     ,i_application_id           IN gen_appl_validations.application_id%TYPE DEFAULT NULL
     ,i_low_value_included_ind   IN gen_appl_validations.low_value_included_ind%TYPE DEFAULT NULL
     ,i_high_value_included_ind  IN gen_appl_validations.high_value_included_ind%TYPE DEFAULT NULL)
      RETURN gen_appl_validations%ROWTYPE
   IS
      CURSOR c_val
      IS
           SELECT *
             FROM gen_appl_validations val
            WHERE domain_name = i_domain
              AND active_ind = 'Y'
              AND domain_datatype = i_domain_datatype
              AND (i_low_value_included_ind IS NULL
                OR low_value_included_ind = i_low_value_included_ind)
              AND (i_high_value_included_ind IS NULL
                OR high_value_included_ind = i_high_value_included_ind)
              AND ((i_application_id IS NOT NULL
                AND i_application_id = val.application_id) -- validation specific to current application
                OR val.application_id IS NULL) -- or validation parameter
         ORDER BY val.application_id NULLS LAST;
      r_val  gen_appl_validations%ROWTYPE;
   BEGIN
      OPEN c_val;
      FETCH c_val
         INTO r_val;
      CLOSE c_val;
      RETURN r_val;
   END;
   FUNCTION f_get_validation_val_low_n(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_low_n%TYPE
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_validation_val_low_n' ;
   BEGIN
      RETURN f_get_appl_validation_rec(i_domain => i_domain
                                      ,i_domain_datatype => 'N'
                                      ,i_application_id => i_application_id
                                      ,i_low_value_included_ind => 'Y').domain_value_low_n;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information('p_domain ' || i_domain
                               ,l_proc);
         logger.log_information(dbms_utility.format_error_backtrace
                               ,l_proc);
         RETURN (NULL);
   END f_get_validation_val_low_n;
   FUNCTION f_get_validation_val_high_n(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_high_n%TYPE
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_validation_val_high_n' ;
   BEGIN
      RETURN f_get_appl_validation_rec(i_domain => i_domain
                                      ,i_domain_datatype => 'N'
                                      ,i_application_id => i_application_id
                                      ,i_high_value_included_ind => 'Y').domain_value_high_n;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information('p_domain ' || i_domain
                               ,l_proc);
         logger.log_information(dbms_utility.format_error_backtrace
                               ,l_proc);
         RETURN (NULL);
   END f_get_validation_val_high_n;
   FUNCTION f_get_validation_val_low_c(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_low_c%TYPE
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_validation_val_low_c' ;
   BEGIN
      RETURN f_get_appl_validation_rec(i_domain => i_domain
                                      ,i_domain_datatype => 'C'
                                      ,i_application_id => i_application_id
                                      ,i_low_value_included_ind => 'Y').domain_value_low_c;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information('p_domain ' || i_domain
                               ,l_proc);
         logger.log_information(dbms_utility.format_error_backtrace
                               ,l_proc);
         RETURN (NULL);
   END f_get_validation_val_low_c;
   FUNCTION f_get_validation_val_high_c(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_high_c%TYPE
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_validation_val_high_c' ;
      l_value_high     gen_appl_validations.domain_value_high_c%TYPE;
   BEGIN
      RETURN f_get_appl_validation_rec(i_domain => i_domain
                                      ,i_domain_datatype => 'C'
                                      ,i_application_id => i_application_id
                                      ,i_high_value_included_ind => 'Y').domain_value_high_c;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information('i_domain ' || i_domain
                               ,l_proc);
         logger.log_information(dbms_utility.format_error_backtrace
                               ,l_proc);
         RETURN (NULL);
   END f_get_validation_val_high_c;
   FUNCTION f_get_validation_val_low_d(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_low_d%TYPE
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_validation_val_low_d' ;
   BEGIN
      RETURN f_get_appl_validation_rec(i_domain => i_domain
                                      ,i_domain_datatype => 'D'
                                      ,i_application_id => i_application_id
                                      ,i_low_value_included_ind => 'Y').domain_value_low_d;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information('p_domain ' || i_domain
                               ,l_proc);
         logger.log_information(dbms_utility.format_error_backtrace
                               ,l_proc);
         RETURN (NULL);
   END f_get_validation_val_low_d;
   FUNCTION f_get_validation_val_high_d(
      i_domain          IN gen_appl_validations.domain_name%TYPE
     ,i_application_id  IN gen_appl_validations.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_validations.domain_value_high_d%TYPE
   IS
      l_proc  CONSTANT VARCHAR2(61)
                          := g_package || 'f_get_validation_val_high_d' ;
   BEGIN
      RETURN f_get_appl_validation_rec(i_domain => i_domain
                                      ,i_domain_datatype => 'D'
                                      ,i_application_id => i_application_id
                                      ,i_high_value_included_ind => 'Y').domain_value_high_d;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information('p_domain ' || i_domain
                               ,l_proc);
         logger.log_information(dbms_utility.format_error_backtrace
                               ,l_proc);
         RETURN (NULL);
   END f_get_validation_val_high_d;
   FUNCTION f_get_lookup_desc_value(
      i_lookup_type     IN gen_appl_lookups.lookup_type%TYPE
     ,i_value_short     IN gen_appl_lookups.value_short%TYPE
     ,i_application_id  IN gen_appl_lookups.application_id%TYPE DEFAULT NULL)
      RETURN gen_appl_lookups.desc_value%TYPE
   IS
      CURSOR c_lookup
      IS
           SELECT desc_value
             FROM gen_appl_lookups lkp
            WHERE lookup_type = i_lookup_type
              AND value_short = i_value_short
              AND ((i_application_id IS NOT NULL
                AND i_application_id = lkp.application_id) -- lookup specific to current application
                OR lkp.application_id IS NULL) -- or general lookup parameter
         ORDER BY lkp.application_id NULLS LAST;
      r_lookup  c_lookup%ROWTYPE;
   BEGIN
      OPEN c_lookup;
      FETCH c_lookup
         INTO r_lookup;
      CLOSE c_lookup;
      RETURN r_lookup.desc_value;
   END f_get_lookup_desc_value;
   --
   FUNCTION f_is_num(i_string IN VARCHAR)
      RETURN BOOLEAN
   IS
      l_tst_num  NUMBER;
   BEGIN
      l_tst_num   := i_string;
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END f_is_num;
   --
   FUNCTION f_is_date(i_string  IN VARCHAR
                     ,i_format  IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_tst_dat  DATE;
   BEGIN
      l_tst_dat      :=
         TO_DATE(i_string
                ,i_format);
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END f_is_date;
   --
   -- set session state for application item (first check if ai exists)
   PROCEDURE p_set_ai_default_value(i_name     VARCHAR2
                                   ,i_value    VARCHAR2
                                   ,i_app_id   NUMBER DEFAULT NULL)
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'p_set_ai_default_value';
      l_ai_id          NUMBER;
   BEGIN
      SELECT 1
        INTO l_ai_id
        FROM apex_application_items
       WHERE UPPER(item_name) = UPPER(i_name)
         AND application_id = COALESCE(i_app_id
                                      ,nv('APP_ID'));
      --
      apex_util.set_session_state(p_name => i_name
                                 ,p_value => i_value);
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         logger.log_error('Unknown Application item ' || i_name || '.'
                         ,l_proc);
      WHEN OTHERS
      THEN
         logger.log_error(SQLERRM
                         ,l_proc);
   END p_set_ai_default_value;
   PROCEDURE p_set_security_group_id
   IS
      CURSOR c_workspace
      IS
         SELECT MAX(workspace_id)
           FROM apex_applications;
      l_workspace  NUMBER;
   BEGIN
      OPEN c_workspace;
      FETCH c_workspace
         INTO l_workspace;
      CLOSE c_workspace;
      IF l_workspace IS NULL
      THEN
         wwv_flow_api.set_security_group_id;
      ELSE
         wwv_flow_api.set_security_group_id(l_workspace);
      END IF;
   END p_set_security_group_id;
   FUNCTION f_clob2blob(i_clob IN CLOB)
      RETURN BLOB
   IS
      l_blob         BLOB;
      l_dest_offset  NUMBER := 1;
      l_src_offset   NUMBER := 1;
      l_amount       INTEGER := dbms_lob.lobmaxsize;
      l_blob_csid    NUMBER := dbms_lob.default_csid;
      l_lang_ctx     INTEGER := dbms_lob.default_lang_ctx;
      l_warning      INTEGER;
   BEGIN
      dbms_lob.createtemporary(lob_loc => l_blob
                              ,cache => TRUE);
      dbms_lob.converttoblob(l_blob
                            ,i_clob
                            ,l_amount
                            ,l_dest_offset
                            ,l_src_offset
                            ,l_blob_csid
                            ,l_lang_ctx
                            ,l_warning);
      RETURN l_blob;
   END f_clob2blob;

   PROCEDURE p_delete_record(i_table_name  IN VARCHAR2
                            ,i_id          IN VARCHAR2)
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'p_delete_record';
      l_id             NUMBER;
      CURSOR c_table
      IS
         SELECT '1'
           FROM user_tables
          WHERE table_name = UPPER(i_table_name);
      l_exists         VARCHAR2(1);
   BEGIN
      logger.log_information('Start - ' || i_table_name || '.' || i_id
                            ,l_proc);
      OPEN c_table;
      FETCH c_table
         INTO l_exists;
      CLOSE c_table;
      IF l_exists IS NULL
      THEN
         logger.log_information(i_table_name || ' does not exist.'
                               ,l_proc);
         gen_pck_exception.p_json_error(i_table_name || ' does not exist.');
      ELSE
         l_id   := TO_NUMBER(i_id);
         EXECUTE IMMEDIATE
            'delete from ' || i_table_name || ' where id = :i_id '
            USING l_id;
         IF SQL%ROWCOUNT > 0
         THEN
            gen_pck_exception.p_json_error('');
         ELSE
            gen_pck_exception.p_json_error('GEN-00002');

         END IF;
      END IF;
      logger.log_information('End'
                            ,l_proc);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_information(SQLERRM
                               ,l_proc);
         ROLLBACK;
         gen_pck_exception.p_json_error(gen_pck_exception.f_get_err_message(
                                           SQLERRM
                                          ,v('LANGCODE')
                                          ,'N'
                                          ,'N'));
   END p_delete_record;

   FUNCTION f_get_collection_rec(
      i_collection_name  IN apex_collections.collection_name%TYPE
     ,i_seq              IN apex_collections.seq_id%TYPE)
      RETURN apex_collections%ROWTYPE
   IS
      CURSOR c_col
      IS
         SELECT *
           FROM apex_collections
          WHERE collection_name = i_collection_name
            AND seq_id = i_seq;
      r_col  c_col%ROWTYPE;
   BEGIN
      OPEN c_col;
      FETCH c_col
         INTO r_col;
      CLOSE c_col;
      RETURN r_col;
   END f_get_collection_rec;
   PROCEDURE p_update_collection_rec(
      i_collection_rec  IN apex_collections%ROWTYPE)
   IS
   BEGIN
      apex_collection.update_member(
         p_collection_name => i_collection_rec.collection_name
        ,p_seq => i_collection_rec.seq_id
        ,p_c001 => i_collection_rec.c001
        ,p_c002 => i_collection_rec.c002
        ,p_c003 => i_collection_rec.c003
        ,p_c004 => i_collection_rec.c004
        ,p_c005 => i_collection_rec.c005
        ,p_c006 => i_collection_rec.c006
        ,p_c007 => i_collection_rec.c007
        ,p_c008 => i_collection_rec.c008
        ,p_c009 => i_collection_rec.c009
        ,p_c010 => i_collection_rec.c010
        ,p_c011 => i_collection_rec.c011
        ,p_c012 => i_collection_rec.c012
        ,p_c013 => i_collection_rec.c013
        ,p_c014 => i_collection_rec.c014
        ,p_c015 => i_collection_rec.c015
        ,p_c016 => i_collection_rec.c016
        ,p_c017 => i_collection_rec.c017
        ,p_c018 => i_collection_rec.c018
        ,p_c019 => i_collection_rec.c019
        ,p_c020 => i_collection_rec.c020
        ,p_c021 => i_collection_rec.c021
        ,p_c022 => i_collection_rec.c022
        ,p_c023 => i_collection_rec.c023
        ,p_c024 => i_collection_rec.c024
        ,p_c025 => i_collection_rec.c025
        ,p_c026 => i_collection_rec.c026
        ,p_c027 => i_collection_rec.c027
        ,p_c028 => i_collection_rec.c028
        ,p_c029 => i_collection_rec.c029
        ,p_c030 => i_collection_rec.c030
        ,p_c031 => i_collection_rec.c031
        ,p_c032 => i_collection_rec.c032
        ,p_c033 => i_collection_rec.c033
        ,p_c034 => i_collection_rec.c034
        ,p_c035 => i_collection_rec.c035
        ,p_c036 => i_collection_rec.c036
        ,p_c037 => i_collection_rec.c037
        ,p_c038 => i_collection_rec.c038
        ,p_c039 => i_collection_rec.c039
        ,p_c040 => i_collection_rec.c040
        ,p_c041 => i_collection_rec.c041
        ,p_c042 => i_collection_rec.c042
        ,p_c043 => i_collection_rec.c043
        ,p_c044 => i_collection_rec.c044
        ,p_c045 => i_collection_rec.c045
        ,p_c046 => i_collection_rec.c046
        ,p_c047 => i_collection_rec.c047
        ,p_c048 => i_collection_rec.c048
        ,p_c049 => i_collection_rec.c049
        ,p_c050 => i_collection_rec.c050
        ,p_n001 => i_collection_rec.n001
        ,p_n002 => i_collection_rec.n002
        ,p_n003 => i_collection_rec.n003
        ,p_n004 => i_collection_rec.n004
        ,p_n005 => i_collection_rec.n005
        ,p_d001 => i_collection_rec.d001
        ,p_d002 => i_collection_rec.d002
        ,p_d003 => i_collection_rec.d003
        ,p_d004 => i_collection_rec.d004
        ,p_d005 => i_collection_rec.d005
        ,p_clob001 => i_collection_rec.clob001
        ,p_blob001 => i_collection_rec.blob001
        ,p_xmltype001 => i_collection_rec.xmltype001);
   END;


   PROCEDURE p_cleanup_history_tables
   IS
      l_proc  CONSTANT VARCHAR2(61) := g_package || 'p_cleanup_history_tables';
   BEGIN
      /* This procedure is started by ORACLE_JOB:    */

      FOR r_ref IN (SELECT *
                      FROM gen_cleanup_history
                     WHERE active_ind = 'Y')
      LOOP
         logger.log_information('Cleaning up:' || r_ref.table_name
                               ,l_proc);
         BEGIN
            -- execute saved sql statement
            EXECUTE IMMEDIATE r_ref.sql_statement;
         EXCEPTION
            WHEN OTHERS
            THEN
               logger.log_error(p_text => dbms_utility.format_error_backtrace
                               ,p_scope => l_proc);
         END;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
   END p_cleanup_history_tables;
--
-- initialize
BEGIN
   NULL;
END gen_pck_general;
/

/
QUIT
