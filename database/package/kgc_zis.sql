CREATE OR REPLACE PACKAGE "HELIX"."KGC_ZIS" IS
   -- PACKAGE SPEC kgc_zis
   /******************************************************************************
      NAME:      KGC_ZIS
      PURPOSE:   generieke onderdelen voor de ZIS-koppelingen
      voor specifieke afhandeling: zie kgc_zis_nijmegen / kgc_zis_utrecht / kgc_zis_maastricht

      Wijzigingshistorie:
      ===================

      Bevindingnr  Wie     Wanneer
      Wat
      ---------------------------------------------------------------------------------------------------------
      ?            HBO  2012-10-09
      - Wijzigingen ivm implementatie Clinical Assistent (Nijmegen)

      ?            RKON  2012-07-27
      - Wijzigingen ivm overgang naar SAP (Maastricht)

      Mantis 3047  GWE  2010-01-06
      - lengte variabele hl7_comp gewijzigd van 2000 naar 4000

      Mantis 3185  RDE  2009-08-29 opgenomen 2010-01-07

      Mantis 283      RDE     2009-05-08
      - zie opleverdocument Rob Debets

      Mantis 283      ESC     2009-04-09
      - Aanpassing persoon_in_zis: aanroep BEH_ZIS_SIMULATIE (stub)

      Mantis 2262/63  NtB     2009-01-20
      - Versie 1.2CO(1) ingechecked om laatste MOSOS-wijzigingen vast te leggen.
        Wijzigingen tbv. 2262 / 2263 ook op vorige produktieversie aanbrengen en deze eerst opleveren.

      Mantis 2263     NtB     2009-01-20
                      RDE     2008-10-08
      - De HL7-luisterjob wil niet correct starten: job in procedure beheer_hl7
        altijd starten op instance 1.

      Mantis 2262     NtB      2009-01-20
                      RDE      2008-07-16
      - Foutmelding bij 2 keer achter elkaar een ZIS-persoon ophalen:
        loop toegevoegd in functie send_msg tbv. timeout bij openen connectie.
        Blijkbaar gaat het sluiten van deze poort in Maastricht (linux) traag,
        waardoor een vervolg-call niet in 1 keer de connectie weer kan openen.

      Mantis 740  NtB     2008-03-31
      - Procedure verwerk_hl7_bericht aangepast tav. MOSOS-berichten.
      - Code verbeterd nav. geconstateerde fouten/verbeteringen door R. Debets.

      Mantis 740  TTi/RPE  2007-10-02
      Mososkoppeling. Initiatie voor OBR,OBX,NTE,ZBR en ORC.

      ...       TTi     2007-08-28
      package integraal overgenomen van RDE

      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      4.0        02-02-2007  RDE              Mantis 503: update persoon niet OK (13-3-2007: aanpassing)
                                              indien postcode leeg moet zijn: "" -> aanroep getHL7waarde
      3.0        19-10-2006  RDE              WP55: telefoon2
                                              tevens: opschoning verstuur_dft_p03
                                              en tx_timeout op 0.5 gezet
      2.0        26-09-2006  MK               aanpassing postcode
      1.0        15-08-2006  RDE              creatie
   **************************************************************************** */

   -- een hl7-bericht kan bestaan uit meerdere segmenten (PID, MSA, ...); sepparator is g_cr
   -- een segment kan bestaan uit meerdere 'composites'; sepparator is |
   -- een 'composites' kan bestaan uit 1 of meer composite; sepparator is ~ ; voorbeeld is XPN - PID-5 Nijmegen
   -- een composite kan bestaan uit 1 of meer attributen; sepparator is ^

  -- mantis 3047 gewijzigd    TYPE hl7_comp IS TABLE OF VARCHAR2(2000);
   TYPE hl7_comp IS TABLE OF VARCHAR2(4000);
   TYPE hl7_comp_seq IS TABLE OF hl7_comp;
   TYPE hl7_seg IS TABLE OF hl7_comp_seq;
   TYPE hl7_msg IS TABLE OF hl7_seg INDEX BY VARCHAR2(10); -- nnn_PID, nnn_MSH, ...

   g_cr        VARCHAR2(1) := chr(13); -- segment-sep
   g_sob_char  VARCHAR2(1) := chr(11); -- start of block
   g_eob_char  VARCHAR2(1) := chr(28); -- end of block
   g_term_char VARCHAR2(1) := chr(13); -- teminating

   g_sypa_hl7_versie   VARCHAR2(30);
   g_log_level         VARCHAR2(200); -- not set
   g_sypa_zis_mail_err VARCHAR2(30) := 'ZIS_MAIL_ERR';

   FUNCTION getsegmentcode(p_seg_code       VARCHAR2
                          ,p_hl7_msg        hl7_msg
                          ,p_seg_code_after VARCHAR2 := NULL) RETURN VARCHAR2;

   FUNCTION formatteer_zisnr_ut(p_zisnr IN VARCHAR2) RETURN VARCHAR2; -- NtB: public gemaakt

   FUNCTION str2hl7msg(p_hlbe_id NUMBER
                      ,p_str     VARCHAR2) RETURN hl7_msg;

   FUNCTION hl7seg2str(p_seg_code VARCHAR2
                      ,p_hl7_seg  hl7_seg) RETURN VARCHAR2;

   FUNCTION init_msg(p_seg_code   VARCHAR2
                    ,p_hl7_versie VARCHAR2) RETURN hl7_seg;

   FUNCTION hl7msg2userstr(p_hl7_msg  hl7_msg
                          ,p_toonleeg BOOLEAN := FALSE) RETURN VARCHAR2;

   PROCEDURE teststr2hl7msg;

   PROCEDURE testsetsypa(p_sypa  VARCHAR2
                        ,p_value VARCHAR2);

   FUNCTION get_sypa_sywa(p_code      VARCHAR2
                         ,p_refresh   BOOLEAN
                         ,p_verplicht BOOLEAN
                         ,x_waarde    IN OUT VARCHAR2) RETURN VARCHAR2;

   PROCEDURE log(p_tekst     IN VARCHAR2
                ,p_log_level IN VARCHAR2 := NULL
                ,p_type      IN VARCHAR2 := 'M');

   PROCEDURE addtext -- plak text eraan tot maximum
   (x_text       IN OUT VARCHAR2
   ,p_max        IN NUMBER
   ,p_extra_text IN VARCHAR2
   ,p_separator  IN VARCHAR2 := chr(10));

   PROCEDURE hl7msg2pers(p_hl7_msg hl7_msg
                        ,p_extra   VARCHAR2
                        ,pers_trow IN OUT cg$kgc_personen.cg$row_type);

   PROCEDURE hl7msg2rela(p_hl7_msg hl7_msg
                        ,p_extra   VARCHAR2
                        ,rela_trow IN OUT cg$kgc_relaties.cg$row_type);

   PROCEDURE hl7msg2verz(p_hl7_msg hl7_msg
                        ,p_extra   VARCHAR2
                        ,verz_trow IN OUT cg$kgc_verzekeraars.cg$row_type);

   PROCEDURE rec2hl7msg(pers_trow IN cg$kgc_personen.cg$row_type
                       ,rela_trow IN cg$kgc_relaties.cg$row_type
                       ,verz_trow IN cg$kgc_verzekeraars.cg$row_type
                       ,p_extra   IN VARCHAR2
                       ,x_hl7_msg IN OUT hl7_msg);

   FUNCTION persoon_in_zis(p_zisnr   IN VARCHAR2
                          ,pers_trow IN OUT cg$kgc_personen.cg$row_type
                          ,rela_trow IN OUT cg$kgc_relaties.cg$row_type
                          ,verz_trow IN OUT cg$kgc_verzekeraars.cg$row_type
                          ,x_msg     OUT VARCHAR2) RETURN BOOLEAN;

   FUNCTION luister(ppoortnr  IN NUMBER
                   ,pbevestig IN STRING
                   ,pmessages IN STRING
                   ,ptimeout  IN NUMBER) RETURN STRING;

   FUNCTION testjava RETURN STRING;

   PROCEDURE beheer_hl7(p_action      IN VARCHAR2
                       ,p_check_email IN VARCHAR2);

   PROCEDURE start_hl7_luister;

   FUNCTION moet_hl7_luister_stoppen RETURN NUMBER;

   FUNCTION gethl7poort(p_sypa IN VARCHAR2) RETURN NUMBER;

   FUNCTION verwerk_hl7_bericht(p_zoveelste    NUMBER
                               ,p_hlbe_id      NUMBER
                               ,p_bericht      VARCHAR2
                               ,x_bericht_type IN OUT VARCHAR2
                               ,x_opmerkingen  OUT kgc_hl7_berichten.opmerkingen%TYPE -- NtB: nieuw, resultaat van verwerking teruggeven
                               ,x_entiteit     IN OUT VARCHAR2
                               ,x_id           IN OUT NUMBER) RETURN VARCHAR2;

   FUNCTION verwerk_adt_a08_bericht -- wijzig patient
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2;

   FUNCTION verwerk_adt_a24_bericht -- link patient
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2;

   FUNCTION verwerk_adt_a37_bericht -- unlink patient
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2;

   FUNCTION verwerk_ander_bericht -- specifiek
   (p_hlbe_id  NUMBER
   ,p_hl7_msg  kgc_zis.hl7_msg
   ,x_entiteit IN OUT VARCHAR2
   ,x_id       IN OUT NUMBER) RETURN VARCHAR2;

   FUNCTION verstuur_dft_p03(p_decl_id    IN NUMBER
                            ,p_aantal     IN NUMBER
                            ,p_check_only IN BOOLEAN
                            ,x_msg        OUT VARCHAR2) RETURN BOOLEAN;

   FUNCTION send_msg(p_host      IN VARCHAR2
                    ,p_port      IN VARCHAR2
                    ,p_strhl7in  IN VARCHAR2
                    ,p_timeout   IN NUMBER -- tienden van seconden
                    ,x_strhl7out IN OUT VARCHAR2
                    ,x_msg       IN OUT VARCHAR2) RETURN BOOLEAN;

   PROCEDURE splitsname(p_strname        IN VARCHAR2
                       ,x_strachternaam  IN OUT VARCHAR2
                       ,x_strvoorvoegsel IN OUT VARCHAR2);

   FUNCTION wijziging_geblokkeerd -- kij of een wijziging door mag gaan
   (p_pers_trow1 cg$kgc_personen.cg$row_type
   ,p_pers_trow2 cg$kgc_personen.cg$row_type) RETURN BOOLEAN;

   PROCEDURE bepaal_pers_verschil(p_pers_trow_org IN cg$kgc_personen.cg$row_type
                                 ,p_pers_trow_upd IN cg$kgc_personen.cg$row_type
                                 ,p_rela_trow_upd IN cg$kgc_relaties.cg$row_type
                                 ,p_verz_trow_upd IN cg$kgc_verzekeraars.cg$row_type
                                 ,x_stremail      IN OUT VARCHAR2
                                 ,x_strconditie   IN OUT VARCHAR2);

   FUNCTION met_puntjes(b_string IN VARCHAR2) RETURN VARCHAR2;

   PROCEDURE format_geo(x_postcode   IN OUT VARCHAR2
                       ,x_woonplaats IN OUT VARCHAR2
                       ,x_provincie  IN OUT VARCHAR2
                       ,x_land       IN OUT VARCHAR2
                       ,x_msg        IN OUT VARCHAR2);

   PROCEDURE gethl7waarde(p_hl7_waarde   IN VARCHAR2
                         ,x_helix_waarde IN OUT VARCHAR2);

   FUNCTION exehl7msg(p_strhl7 IN VARCHAR2) RETURN VARCHAR2;

   PROCEDURE post_mail_zis(p_sypa_aan  IN VARCHAR2
                          ,p_onderwerp IN VARCHAR2
                          ,p_inhoud    IN VARCHAR2
                          ,p_html      IN BOOLEAN := FALSE
                          ,p_conditie  IN VARCHAR2 := NULL
                          ,p_sypa_van  IN VARCHAR2 := 'ZIS_MAIL_AFZENDER' -- mantis 740
                           );

   FUNCTION verwerk_adt_a40_bericht -- HL7 v2.4 link patient
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2;
END KGC_ZIS;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ZIS" IS
   -- PACKAGE BODY kgc_zis

   g_jobwhat VARCHAR2(200) := 'kgc_zis.start_hl7_luister'; -- deze procedure moet bestaan!
   -- HL7

   g_sypa_listen_to_msg VARCHAR2(2000);
   g_sypa_zis_locatie   VARCHAR2(30);

   g_html_mail_head VARCHAR2(2000) := '<head><style type="text/css">' || kgc_zis.g_cr || '<!-- ' || kgc_zis.g_cr ||
                                      'p { font-family: Arial, Helvetica, sans-serif; font-size: 10pt; margin-top: 0; margin-bottom: 0.5em; padding: 0; border: 0; lineheight: 1em } ' ||
                                      kgc_zis.g_cr ||
                                      'td { font-family: Arial, Helvetica, sans-serif; font-size: 10pt; margin-top: 0; margin-bottom: 0.5em; padding: 0; border: 0; lineheight: 1em } ' ||
                                      kgc_zis.g_cr || '--> ' || kgc_zis.g_cr || '</style></head>';

   FUNCTION getondehtml(p_pers_id NUMBER
                       ,p_zisnr   VARCHAR2) RETURN VARCHAR2 IS
      CURSOR c_onde(p_pers_id NUMBER) IS
         SELECT kafd.code kafd_code
               ,ongr.code ongr_code
               ,onde.onderzoeknr
               ,onde.datum_binnen
           FROM kgc_onderzoeken       onde
               ,kgc_kgc_afdelingen    kafd
               ,kgc_onderzoeksgroepen ongr
          WHERE onde.kafd_id = kafd.kafd_id
            AND onde.ongr_id = ongr.ongr_id
            AND onde.pers_id = p_pers_id
            AND onde.onde_id = (SELECT MAX(onde2.onde_id)
                                  FROM kgc_onderzoeken onde2
                                 WHERE onde2.kafd_id = onde.kafd_id
                                   AND onde2.ongr_id = onde.ongr_id
                                   AND onde2.pers_id = onde.pers_id);
      v_html  VARCHAR2(32767);
      v_count NUMBER;
BEGIN
      v_count := 0;
      FOR r_onde IN c_onde(p_pers_id)
      LOOP
         v_count := v_count + 1;
         IF v_count = 1
         THEN
            v_html := v_html || kgc_zis.g_cr || '<p>Overzicht onderzoeken ZISnr ' || p_zisnr ||
                      ' (max 1 per onderzoeksgroep): ' || kgc_zis.g_cr;
            v_html := v_html || kgc_zis.g_cr || '<table><tr><td><b>Afdeling</b>' || '</td><td><b>Onderzoeksgroep</b>' ||
                      '</td><td><b>Onderzoeknr</b>' || '</td><td><b>Datum binnen</b>' || '</td>' || kgc_zis.g_cr;
         END IF;
         v_html := v_html || kgc_zis.g_cr || '<tr><td>' || r_onde.kafd_code || '</td><td>' || r_onde.ongr_code ||
                   '</td><td>' || r_onde.onderzoeknr || '</td><td>' ||
                   to_char(r_onde.datum_binnen
                          ,'dd-mm-yyyy') || '</td>' || kgc_zis.g_cr;
      END LOOP;
      IF v_count = 0
      THEN
         v_html := v_html || kgc_zis.g_cr || '<p>Er zijn geen onderzoeken gevonden voor ZISnr ' || p_zisnr || '!' ||
                   kgc_zis.g_cr;
      ELSE
         v_html := v_html || kgc_zis.g_cr || '</table>';
      END IF;
      RETURN v_html;
   END;

   -- formatteer zisnummer als 9.999.999
   FUNCTION formatteer_zisnr_ut(p_zisnr IN VARCHAR2) RETURN VARCHAR2 IS
      v_return VARCHAR2(10) := p_zisnr;
   BEGIN
      IF (instr(v_return
               ,'.') > 0)
      THEN
         v_return := REPLACE(p_zisnr
                            ,'.'
                            ,NULL);
      ELSE
         v_return := substr(v_return
                           ,1
                           ,1) || '.' || substr(v_return
                                               ,2
                                               ,3) || '.' || substr(v_return
                                                                   ,5);
      END IF;
      RETURN(v_return);
   EXCEPTION
      WHEN OTHERS THEN
         RETURN(p_zisnr);
   END formatteer_zisnr_ut;

   FUNCTION init_msg(p_seg_code   VARCHAR2
                    ,p_hl7_versie VARCHAR2) RETURN hl7_seg IS
      v_hl7_seg  hl7_seg := hl7_seg();
      v_nrofcomp NUMBER := 0;
      v_seg_code VARCHAR2(10) := p_seg_code;
   BEGIN
      IF length(v_seg_code) = 7
      THEN
         -- bv 001_MSH
         v_seg_code := substr(v_seg_code
                             ,5);
      END IF;
      --  dbms_output.put_line('seg_code: ' || v_seg_code || '; hl7-versie: ' || p_hl7_versie);
      -- ADT = Admission, Discharge and Transfer
      -- DFT = Detail Financial Transaction
      -- hoeveel composites heeft het segment?
      IF v_seg_code = 'EVN'
      THEN
         -- EVN-1 Event Type Code
         -- EVN-2 Recorded Date/Time
         -- EVN-3 Date/Time Planned Event
         -- EVN-4 Event Reason Code
         -- EVN-5 Operator ID
         -- EVN-6 Event Occurred
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 2;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 6;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 7;
         END IF;
         --> mantis 740
      ELSIF v_seg_code = 'ORC'
      THEN
         v_nrofcomp := 21;
      ELSIF v_seg_code = 'OBR'
      THEN
         v_nrofcomp := 39;
      ELSIF v_seg_code = 'ZBR'
      THEN
         v_nrofcomp := 3;
      ELSIF v_seg_code = 'NTE'
      THEN
--         v_nrofcomp := 3;
         v_nrofcomp := 5; -- mantisnr 8491
      ELSIF v_seg_code = 'OBX'
      THEN
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 14;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 14;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 18;-- aangepast voor ORU berichten naar EPIC mantis 8526
         END IF;
         --< mantis 740
      ELSIF v_seg_code = 'IN1'
      THEN
         -- IN1-1 Set ID - insurance
         -- IN1-2 Insurance plan ID
         -- IN1-3 Insurance company ID
         -- IN1-4 Insurance company name
         -- IN1-5 Insurance company address
         -- IN1-12 Plan effective date Ingangsdatum
         -- IN1-13 Plan expiration date Afloopdatum
         -- IN1-15 Plan type - Verzekeringstype ^ H=hoofdverzekeringB=Bijverzekering
         -- IN1-36 Policy number
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 36;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 36; -- niet in gebruik!
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 36;
         END IF;
      ELSIF v_seg_code = 'IN2'
      THEN
        v_nrofcomp := 29; -- niet gebruikt, maar kan wel aangeboden worden!
      ELSIF v_seg_code = 'FT1'
      THEN
         -- financieel!
         -- FT1-1 Set ID FT1
         -- FT1-2 Transaction ID
         -- FT1-3 Transaction Batch ID
         -- FT1-4 Transaction Date
         -- FT1-5 Transaction Posting Date
         -- FT1-6 Transaction Type
         -- FT1-7 Transaction Code
         -- FT1-8 Transaction Description
         -- FT1-9 Transaction Description - Alt
         -- FT1-10 Transaction Quantity
         -- FT1-11 Transaction Amount - Extended
         -- FT1-12 Transaction Amount ? Unit
         -- FT1-13 Department Code
         -- FT1-14 Insurance Plan ID
         -- FT1-15 Insurance Amount
         -- FT1-16 Assigned Patient Location
         -- FT1-17 Fee Schedule
         -- FT1-18 Patient Type
         -- FT1-19 Diagnosis Code
         -- FT1-20 Performed By Code
         -- FT1-21 Ordered By Code
         -- FT1-22 Unit Cost
         -- FT1-23 Filler Order Number
         -- FT1-24 Entered By Code
         -- FT1-25 Procedure Code
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 21;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 25;
         ELSIF p_hl7_versie = '2.4' -- mantisnr 8491
         THEN
            v_nrofcomp := 39;
         END IF;
      ELSIF v_seg_code = 'MRG' -- alleen in 2.4
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 7;
         END IF;
      ELSIF v_seg_code = 'MSA'
      THEN
         -- MSA-1 Acknowlegdment Code
         -- MSA-2 Message Control ID
         -- MSA-3 Text Message
         -- MSA-4 Expected Sequence Number
         -- MSA-5 Delayed ACK Type
         -- MSA-6 Error Condition
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 4;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 6;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 6;
         END IF;
      ELSIF v_seg_code = 'MSH'
      THEN
         -- MSH-1 Field Separator: standaard | (hex 0D)
         -- MSH-2 Encoding Characters: standaard ^~\& ; ^=component separator; ~=repetition separator \=escape character; &=sub-component separator
         -- MSH-3 Sending Application; 1=prod, 2=test, 4=ontw cf. AZN richtlijn
         -- MSH-4 Sending Facility
         -- MSH-5 Receiving Application
         -- MSH-6 Receiving Facility (leeg)
         -- MSH-7 Date/Time of Message
         -- MSH-8 security
         -- MSH-9 Message Type
         -- MSH-10 Message Control ID
         -- MSH-11 Processing ID
         -- MSH-12 Version ID
         -- MSH-13 Sequence Number
         -- MSH-14 Continuation Pointer
         -- MSH-15 Accept Acknowledgement Type; leeg (umcn - ack-nack.doc)
         -- MSH-16 Application Acknowledgement Type; leeg (umcn - ack-nack.doc)
         -- MSH-17 Country Code
         -- MSH-18 Character Set
         -- MSH-19 Principal Language Of Message
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 16;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 19;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 21;
         END IF;
      ELSIF v_seg_code = 'PD1'
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 21;
         END IF;
      ELSIF v_seg_code = 'PID'
      THEN
         -- PID-1 Set ID   PID
         -- PID-2 Patient ID (External ID)
         -- PID-3 Patient ID (Internal ID) -- in 2.4 ook de plek voor het BSN?!
         -- PID-4 Alternate Patient ID   PID
         -- PID-5 Patient Name
         -- PID-7 Date/Time of Birth
         -- PID-8 Sex
         -- PID-11 Patient Address
         -- PID-13 Phone Number Home
         -- PID-14 Phone Number-Business
         -- PID-15 Language Patient
         -- PID-16 Marital Status
         -- PID-17 Religion
         -- PID-21 Mother's Identifier
         -- PID-23 Birth Place
         -- PID-24 Multiple Birth Indicator
         -- PID-29 Patient Death Date
         -- PID-30 Pati? Death Indicator
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 24;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 30;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 38;
         END IF;
      ELSIF v_seg_code = 'PV1'
      THEN
         -- onderdeel DFT, niet zo boeiend
         -- Utrecht: ADTA08: 2 velden; DFT: niet gebruikt!
         -- Nijmegen: Niet in gebruik, 09-09-2013 toch wel met de invvoering van epic
         -- PV1-1 Set Id - PV1
         -- PV1-2 Patient Class
         -- PV1-3 Assigned Patient Location
         -- PV1-4 Admission Type
         -- PV1-5 Pre-Admit Number
         -- PV1-6 Prior Patient Location
         -- PV1-7 Attending Doctor
         -- PV1-8 Referring Doctor
         -- PV1-9 Consulting Doctor
         -- etc.
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 10; -- tbv ontvangst Utrecht (wordt niets mee gedaan); Maastricht?
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 52; -- maar: niet gebruikt in Nijmegen
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 52; -- maar: niet gebruikt in Nijmegen, 09-09-2013 toch wel met de invvoering van epic
         END IF;
      ELSIF v_seg_code = 'PV2'
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 47;
         END IF;
      ELSIF v_seg_code = 'QRD'
      THEN
         -- QRD-1 Query Date/Time
         -- QRD-2 Query Format Code
         -- QRD-3 Query Priority
         -- QRD-4 Query ID
         -- QRD-7 Quantity Limited Request
         -- QRD-8 Who subject filter
         -- QRD-9 What subject filter
         -- QRD-10 What Nursing unit data code
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 10;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 10; -- niet in gebruik
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 12;
         END IF;
      ELSIF v_seg_code = 'ROL'
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 12;
         END IF;
      ELSIF v_seg_code = 'STF'
      THEN
         -- STF-1 PrimaryKeyValue - zie doc
         -- STF-2 Staff ID Code
         -- STF-3 Staff name
         -- STF-4 Staff Type
         -- STF-5 Sex
         -- STF-10 Phone
         -- STF-11 Office/Home Address
         -- STF-13 Inactivation Date
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 13;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 13; -- niet in gebruik!
         END IF;
      ELSIF v_seg_code = 'ZFT' -- toegoegd nav. EPIC DFT bericht
      THEN
        v_nrofcomp := 32;
      ELSIF v_seg_code = 'ZFU'
      THEN
         -- ZFU-7 Assigned Patient Location
         -- ZFU-8 Financial treatment rule
         -- ZFU-9 Ordering Institution
         -- ZFU-10 Relation to orderer
         -- ZFU-11 Internal requestor
         -- ZFU-12 Financial treatment = Aangeboden ABW
         -- ZFU-13 Reference number
         -- ZFU-14 Transaction description
         -- ZFU-15 Transaction quantity
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 15;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 0; -- niet in gebruik
         END IF;
      ELSIF v_seg_code = 'ZPI'
      THEN
         -- ZPI-1 Set ID   PID
         -- ZPI-2 Indicatie overleden
         -- ZPI-3 Geboorteplaats
         -- ZPI-4 Naam echtgenoot
         -- ZPI-5 Verwijzend patientnummer
         -- ZPI-6 Huisarts
         -- ZPI-7 Voorletters ouder
         -- ZPI-8 Girorekening
         -- ZPI-9 Bankrekening
         -- ZPI-10 Bloedgroep
         -- ZPI-11 Datum overleden
         -- ZPI-21 VIP Indicator
         -- ZPI-22 Huistandarts
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 11;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 22;
         END IF;
--
      END IF;
      -- initialiseren:
      IF v_nrofcomp > 0
      THEN
         -- mogelijk: hele msg niet boeiend!
         FOR i IN 1 .. v_nrofcomp
         LOOP
            v_hl7_seg.EXTEND;
            v_hl7_seg(i) := hl7_comp_seq(NULL);
            v_hl7_seg(i)(1) := hl7_comp(NULL);
         END LOOP;
      END IF;
      RETURN v_hl7_seg;
   END;

   FUNCTION hl7msg2userstr -- leesbare string!
   (p_hl7_msg  hl7_msg
   ,p_toonleeg BOOLEAN := FALSE) RETURN VARCHAR2 IS
      v_str      VARCHAR2(32677);
      v_cr       VARCHAR2(10) := chr(10);
      v_seg_code VARCHAR2(10);
   BEGIN
      v_str := 'Start kgc_zis.hl7msg2userstr';
      IF p_hl7_msg.COUNT = 0
      THEN
         v_str := v_str || v_cr || 'Er zijn geen segmenten in de message aanwezig!';
      ELSE
         v_str      := v_str || v_cr || 'Er zijn ' || to_char(p_hl7_msg.COUNT) || ' segmenten in de message aanwezig!';
         v_seg_code := p_hl7_msg.FIRST; -- get subscript of first element
         WHILE v_seg_code IS NOT NULL
         LOOP
            v_str := v_str || v_cr || '  Segment ' || v_seg_code || ' (' || to_char(p_hl7_msg(v_seg_code).COUNT) ||
                     ' componenten):';
            IF p_hl7_msg(v_seg_code).COUNT > 0
            THEN
               FOR i IN 1 .. p_hl7_msg(v_seg_code).COUNT
               LOOP
                  IF p_hl7_msg(v_seg_code) (i).COUNT > 0
                  THEN
                     FOR j IN 1 .. p_hl7_msg(v_seg_code) (i).COUNT
                     LOOP
                        IF p_hl7_msg(v_seg_code) (i).COUNT > 0
                        THEN
                           FOR k IN 1 .. p_hl7_msg(v_seg_code) (i) (j).COUNT
                           LOOP
                              IF p_toonleeg
                                 OR nvl(length(p_hl7_msg(v_seg_code) (i) (j) (k))
                                       ,0) > 0
                              THEN
                                 v_str := v_str || v_cr || '    ' || v_seg_code || '(' || to_char(i) || ')(' ||
                                          to_char(j) || ')(' || to_char(k) || ') = ' || p_hl7_msg(v_seg_code) (i) (j) (k);
                              END IF;
                           END LOOP;
                        END IF;
                     END LOOP;
                  END IF;
               END LOOP;
            END IF;
            v_seg_code := p_hl7_msg.NEXT(v_seg_code); -- get subscript of next element
         END LOOP;
      END IF;
      v_str := v_str || v_cr || 'Einde kgc_zis.hl7msg2userstr';
      RETURN v_str;
   END;

   FUNCTION hl7seg2str(p_seg_code VARCHAR2
                      ,p_hl7_seg  hl7_seg) RETURN VARCHAR2 IS
      v_str VARCHAR2(32677) := p_seg_code; -- begin is altijd de segment-code!
   BEGIN
      IF p_hl7_seg.COUNT = 0
      THEN
         RETURN v_str;
      END IF;
      FOR i IN p_hl7_seg.FIRST .. p_hl7_seg.LAST
      LOOP
         IF p_seg_code <> 'MSH'
            OR -- altijd sep
            (p_seg_code = 'MSH' AND i > 2) -- pas na 2 een sep!!!
         THEN
            v_str := v_str || '|';
         END IF;
         IF p_hl7_seg(i).COUNT > 0
         THEN
            FOR j IN p_hl7_seg(i).FIRST .. p_hl7_seg(i).LAST
            LOOP
               IF j > 1
               THEN
                  v_str := v_str || '~';
               END IF;
               IF p_hl7_seg(i) (j).COUNT > 0
               THEN
                  FOR k IN p_hl7_seg(i) (j).FIRST .. p_hl7_seg(i) (j).LAST
                  LOOP
                     IF k > 1
                     THEN
                        v_str := v_str || '^';
                     END IF;
                     v_str := v_str || p_hl7_seg(i) (j) (k);
                  END LOOP;
               END IF;
            END LOOP;
         END IF;
      END LOOP;
      RETURN v_str;
   END;

   -- omdat de segmenten gecodeerd zijn ([ volgnr + _ ] + segcode) is een functie
   -- nodig om de juiste eruit te halen!
   -- teruggegeven wordt b.v. 001_MSH, zodat je p_hl7_msg kunt bewerken!
   FUNCTION getsegmentcode(p_seg_code       VARCHAR2
                          ,p_hl7_msg        hl7_msg
                          ,p_seg_code_after VARCHAR2 := NULL) RETURN VARCHAR2 IS
      v_seg_code          VARCHAR2(10);
      v_eerstvolgendeisok BOOLEAN := TRUE;
   BEGIN
      IF p_seg_code_after IS NOT NULL
      THEN
         v_eerstvolgendeisok := FALSE;
      END IF;
      v_seg_code := p_hl7_msg.FIRST; -- get subscript of first element
      WHILE v_seg_code IS NOT NULL
      LOOP
         IF substr(v_seg_code
                  ,1
                  ,3) = p_seg_code
            OR -- geen volgnummer
            substr(v_seg_code
                  ,5
                  ,3) = p_seg_code
         THEN
            -- wel volgnummer
            IF v_eerstvolgendeisok
            THEN
               RETURN v_seg_code;
            ELSIF p_seg_code_after = v_seg_code
            THEN
               v_eerstvolgendeisok := TRUE;
            END IF;
         END IF;
         v_seg_code := p_hl7_msg.NEXT(v_seg_code); -- get subscript of next element
      END LOOP;
      RETURN '';
   END;

   -- vertaal string naar HL7-msg-structuur
   FUNCTION str2hl7msg(p_hlbe_id NUMBER
                      ,p_str     VARCHAR2) RETURN hl7_msg IS
      v_hl7_msg_null      hl7_msg;
      v_hl7_msg           hl7_msg;
      v_seg_count         NUMBER := 0;
      v_pos               NUMBER;
      v_pos_next          NUMBER;
      v_pos_seg_eind      NUMBER;
      v_pos_comp_seq_eind NUMBER;
      v_comp_seq_count    NUMBER;
      v_pos_comp_eind     NUMBER;
      v_comp_count        NUMBER;
      v_pos_att_eind      NUMBER;
      v_att_count         NUMBER;
      v_seg_code          VARCHAR2(10);
      v_att_klaar         BOOLEAN;
      v_sep_field         VARCHAR2(1) := '|';
      v_sep_comp          VARCHAR2(1) := '^';
      v_sep_repeat        VARCHAR2(1) := '~';
      v_hl7_versie        VARCHAR2(10); -- deze zit in het bericht!!!
      v_versie_begin      NUMBER;
      v_versie_eind       NUMBER;
      v_versie_eind_cr    NUMBER;
      v_versie_eind_comp  NUMBER;
      v_debug             VARCHAR2(100);
      v_thisproc          VARCHAR2(100) := 'kgc_zis.str2hl7msg';
   BEGIN
      v_debug := '1';
      -- in deze functie staan een hoop foutmeldingen; na testen komen ze nooit meer voor! Dus ook niet opgenomen in beheer-doc!
      -- 1e positie: altijd g_sob_char
      IF nvl(substr(p_str
                   ,1
                   ,1)
            ,' ') <> g_sob_char
      THEN
         kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                              ,'kgc_zis.str2hl7msg: Fout (database: ' || upper(database_name) || ')!'
                              ,'Bericht in kgc_hl7_berichten met id ' || to_char(p_hlbe_id) ||
                               ' begint niet met g_sob_char (CHR(11))');
         RETURN v_hl7_msg_null;
      END IF;
      v_debug := '2';
      v_pos   := 2;
      --dbms_output.put_line('start str2hl7msg');
      WHILE TRUE
      LOOP
         -- alle segmenten
         v_debug := '3';

         --   p.l( SUBSTR(p_str, v_pos ));
         IF substr(p_str
                  ,v_pos
                  ,1) = g_eob_char
         THEN
            -- klaar, laatste segment gehad!
            v_debug := substr(p_str
                             ,v_pos
                             ,-20);
            EXIT;
         END IF;
         v_debug := '4';
         -- hmmm sommige segmenten komen vaker voor (b.v PID in A24/A37, of IN1 in A08! Dus altijd nnn_ toevoegen!
         -- ook voor het uit elkaar houden van groepen is de volgorde van belang! Zie getSegmentCode
         v_seg_count := v_seg_count + 1;
         v_seg_code  := ltrim(to_char(v_seg_count
                                     ,'099')) || '_' || substr(p_str
                                                              ,v_pos
                                                              ,3);
         --dbms_output.put_line(v_seg_code);
         v_comp_seq_count := 0;
         v_debug          := '5';
         IF v_seg_code = '001_MSH'
         THEN
            -- vreemde eend (altijd vooraan!): bevat separators
            v_debug     := v_debug || 'a';
            v_sep_field := substr(p_str
                                 ,v_pos + 3
                                 ,1); -- default |
            -- bepaal v_hl7_versie! MSH-12
            v_debug          := v_debug || 'b';
            v_versie_begin   := instr(p_str
                                     ,v_sep_field
                                     ,1
                                     ,11);
            v_versie_eind    := instr(p_str
                                     ,v_sep_field
                                     ,v_versie_begin + 1); -- de volgende
            v_versie_eind_comp := instr(p_str
                                     ,v_sep_comp
                                     ,v_versie_begin + 1); -- mantis 283: in v2.4 staat versie in MSH12-1! (en ook 12-2 is gevuld)
            v_versie_eind_cr := instr(p_str
                                     ,kgc_zis.g_cr
                                     ,v_versie_begin + 1); -- einde segment?
            IF v_versie_eind_cr > 0
               AND v_versie_eind_cr < v_versie_eind
            THEN
               v_versie_eind := v_versie_eind_cr; -- dan versie aan eind segment: begrensd door cr!
            END IF;
            -- mantis 283: in v2.4 staat versie in MSH12-1! (en ook 12-2 is gevuld)
            IF v_versie_eind_comp > 0
               AND v_versie_eind_comp < v_versie_eind
            THEN
               v_versie_eind := v_versie_eind_comp; -- dan versie als 1e in de composite
            END IF;
            v_hl7_versie := substr(p_str
                                  ,v_versie_begin + 1
                                  ,v_versie_eind - v_versie_begin - 1);
            v_debug := v_debug || 'c';
            v_hl7_msg(v_seg_code) := init_msg(v_seg_code
                                             ,v_hl7_versie);
            v_debug := v_debug || 'd';
            -- rest header
            v_hl7_msg(v_seg_code)(1)(1)(1) := v_sep_field;
            v_debug := v_debug || 'e';
            v_pos_comp_seq_eind := instr(p_str
                                        ,v_sep_field
                                        ,v_pos + 4);
            --   dbms_output.put_line('#msh2 ' || to_char(v_hl7_msg(v_seg_code)(2).COUNT));
            v_hl7_msg(v_seg_code)(2)(1)(1) := substr(p_str
                                                    ,v_pos + 4
                                                    ,v_pos_comp_seq_eind - v_pos - 4);
            v_debug := v_debug || 'f';
            v_sep_comp := substr(v_hl7_msg(v_seg_code) (2) (1) (1)
                                ,1
                                ,1);
            v_sep_repeat := substr(v_hl7_msg(v_seg_code) (2) (1) (1)
                                  ,2
                                  ,1);
            v_comp_seq_count := 2; -- de eerste 2 zijn 'special'!
            v_pos := v_pos_comp_seq_eind + 1;
         ELSE
            v_debug := v_debug || 'k';
            v_pos := v_pos + 4; -- segment-code + '|' overslaan
            v_hl7_msg(v_seg_code) := init_msg(v_seg_code
                                             ,v_hl7_versie);
            v_debug := v_debug || 'l';
         END IF;

         v_pos_seg_eind := instr(p_str
                                ,g_cr
                                ,v_pos); -- ieder segment wordt afgesloten door cr
         IF v_pos_seg_eind = 0
         THEN
            kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                                 ,'kgc_zis.str2hl7msg: Fout (database: ' || upper(database_name) || ')!'
                                 ,'Bericht in kgc_hl7_berichten met id ' || to_char(p_hlbe_id) ||
                                  ' Segment-eind (CHR(13)) niet gevonden!');
            RETURN v_hl7_msg_null;
         END IF;
         v_debug := '6';
         -- alle composites opnemen
         WHILE TRUE
         LOOP
            -- alle composite_seq's
            IF v_pos > v_pos_seg_eind
            THEN
               EXIT;
            END IF;

            v_debug             := '7';
            v_pos_comp_seq_eind := instr(p_str
                                        ,'|'
                                        ,v_pos);
            IF v_pos_comp_seq_eind = 0
               OR v_pos_comp_seq_eind > v_pos_seg_eind
            THEN
               v_pos_comp_seq_eind := v_pos_seg_eind; -- laatste composite
            END IF;
            v_debug          := '8';
            v_comp_seq_count := v_comp_seq_count + 1;
            v_comp_count     := 0;
            WHILE TRUE
            LOOP
               -- alle composites in een composite_seq
               IF v_pos > v_pos_comp_seq_eind
               THEN
                  EXIT;
               END IF;
               v_debug         := '9';
               v_pos_comp_eind := instr(p_str
                                       ,'~'
                                       ,v_pos);
               IF v_pos_comp_eind = 0
                  OR v_pos_comp_eind > v_pos_comp_seq_eind
               THEN
                  v_pos_comp_eind := v_pos_comp_seq_eind; -- laatste composite in comp_seq
               END IF;
               v_debug      := '10';
               v_comp_count := v_comp_count + 1;
               v_att_count  := 0;
               v_att_klaar  := FALSE;
               WHILE TRUE
               LOOP
                  -- alle attributen in een composite

                  v_debug        := '11';
                  v_pos_att_eind := instr(p_str
                                         ,'^'
                                         ,v_pos);
                  IF v_pos_att_eind = 0
                     OR v_pos_att_eind > v_pos_comp_eind
                  THEN
                     v_pos_att_eind := v_pos_comp_eind; -- laatste attribuut in comp
                     v_att_klaar    := TRUE;
                  END IF;
                  v_debug := '11';

                  /*        if v_seg_code like '%XXX'
                            then
                              dbms_output.put_line( substr( p_str, v_pos, v_pos_att_eind ));
                            end if;
                  */
                  v_att_count := v_att_count + 1;
                  --  dbms_output.put_line('seg attr vnr: ' || to_char(v_att_count));
                  -- is het attribuut boeiend? Niet als er geen comp-plek meer is!

                  IF v_hl7_msg(v_seg_code).COUNT >= v_comp_seq_count
                  THEN
                     -- er is plek

                     --           v_debug := '12(' || to_char(v_hl7_msg(v_seg_code).COUNT) || ')';
                     IF v_comp_count > v_hl7_msg(v_seg_code) (v_comp_seq_count).COUNT
                     THEN
                        --              v_debug := '13(' || to_char(v_comp_count) || ';' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count).COUNT) || ')';
                        v_hl7_msg(v_seg_code)(v_comp_seq_count) .EXTEND; -- alleen de eerste is er!
                        v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count) := hl7_comp(NULL);
                     END IF;

                     --           v_debug := '15(' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count).COUNT) || ')';
                     IF v_att_count > v_hl7_msg(v_seg_code) (v_comp_seq_count) (v_comp_count).COUNT
                     THEN
                        --              v_debug := '16(' || to_char(v_comp_count) || ')';
                        v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count) .EXTEND; -- alleen de eerste is er!
                     END IF;
                     /*
                     dbms_output.put_line('Attr ' || to_char(v_comp_seq_count) ||  '/' || to_char(v_comp_count) ||  '/' || to_char(v_att_count) || ': ' || SUBSTR(p_str, v_pos, v_pos_att_eind - v_pos));
                     dbms_output.put_line('#comp_seq ' || to_char(v_hl7_msg(v_seg_code).COUNT)
                      || ' #comp ' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count).COUNT)
                      || ' #attr ' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count).COUNT));
                     */
                     -- geef attribuut een waarde:
                     v_debug := '17(' || v_seg_code || ';' || to_char(v_comp_seq_count) || '<=' ||
                                to_char(v_hl7_msg(v_seg_code).COUNT) || ';' || to_char(v_comp_count) || '<=' ||
                                to_char(v_hl7_msg(v_seg_code) (v_comp_seq_count).COUNT) || ';' || to_char(v_att_count) || '<=' ||
                                to_char(v_hl7_msg(v_seg_code) (v_comp_seq_count) (v_comp_count).COUNT) || ')';
                     v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count)(v_att_count) := substr(p_str
                                                                                                 ,v_pos
                                                                                                 ,v_pos_att_eind -
                                                                                                  v_pos); -- hebbes!
                     /*           dbms_output.put_line(v_seg_code ||' - '
                                                                     || to_char(v_comp_seq_count)||' - '
                                                                     || to_char(v_comp_count)||' - '
                                                                     || to_char(v_att_count)||' - '
                                                                     || SUBSTR(p_str, v_pos, v_pos_att_eind - v_pos) );
                     */
                  ELSE
                     --            v_debug := '18(' || v_seg_code || ';'
                     --              || to_char(v_comp_seq_count) || '<=' || to_char(v_hl7_msg(v_seg_code).COUNT)
                     --              || ')';
                     NULL;
                  END IF;
                  v_pos := v_pos_att_eind + 1; -- v_pos naar 1 voorbij eind (daar staat ^ of ~ of | of cr)
                  IF v_att_klaar
                  THEN
                     EXIT;
                  END IF;
               END LOOP; -- alle alle attributen in een composite
               -- check:
               IF v_att_count = 0
               THEN
                  kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                                       ,'kgc_zis.str2hl7msg: Fout (database: ' || upper(database_name) || ')!'
                                       ,'Bericht in kgc_hl7_berichten met id ' || to_char(p_hlbe_id) ||
                                        ' Segment zonder attributen!');
                  RETURN v_hl7_msg_null;
               END IF;
            END LOOP; -- alle composites
         END LOOP; -- alle composite_seqs
      END LOOP; -- alle segementen
      --      dbms_output.put_line(v_debug);
      RETURN v_hl7_msg;
   EXCEPTION
      WHEN OTHERS THEN
         --  dbms_output.put_line(v_debug);
         raise_application_error(-20000
                                ,v_thisproc || ' debug: ' || v_debug || ' Error: ' || SQLERRM);
   END;

   -- global

   PROCEDURE teststr2hl7msg IS
      v_hl7_msg_bron hl7_msg;
      v_hl7_msg_res  hl7_msg;
      v_msg          VARCHAR2(32767);
      v_max          NUMBER;
      v_max2         NUMBER;
      v_msa          VARCHAR2(10);
      v_msh          VARCHAR2(10);
   BEGIN
      g_sypa_hl7_versie := kgc_sypa_00.systeem_waarde('HL7_VERSIE');
      v_hl7_msg_bron('MSH') := init_msg('MSH'
                                       ,g_sypa_hl7_versie);
      v_hl7_msg_bron('MSH')(1)(1)(1) := '|';
      v_hl7_msg_bron('MSH')(2)(1)(1) := '^~&';
      v_max := v_hl7_msg_bron('MSH').COUNT;
      v_hl7_msg_bron('MSH')(v_max)(1)(1) := 'Test MSH-16 laatste veld';

      v_hl7_msg_bron('MSA') := init_msg('MSA'
                                       ,g_sypa_hl7_versie);
      v_hl7_msg_bron('MSA')(1)(1)(1) := '<HELIX_ACK_CODE>';
      v_hl7_msg_bron('MSA')(1)(1) .EXTEND;
      v_hl7_msg_bron('MSA')(1)(1)(2) := 'Test comp-2';
      v_max2 := v_hl7_msg_bron('MSA').COUNT;
      v_hl7_msg_bron('MSA')(v_max2)(1)(1) := 'Test MSA-4';
      v_hl7_msg_bron('MSA')(v_max2)(1) .EXTEND;
      v_hl7_msg_bron('MSA')(v_max2)(1)(2) := 'Test MSA-4 comp-3';

      v_msg := g_sob_char || hl7seg2str('MSH'
                                       ,v_hl7_msg_bron('MSH')) || g_cr ||
               hl7seg2str('MSA'
                         ,v_hl7_msg_bron('MSA')) || g_cr || g_eob_char || g_term_char;
      log(v_msg);
      v_hl7_msg_res := str2hl7msg(1
                                 ,v_msg);
      dbms_output.put_line('Aanroep klaar!');
      v_msa := getsegmentcode('MSA'
                             ,v_hl7_msg_res);
      v_msh := getsegmentcode('MSH'
                             ,v_hl7_msg_res);
      IF v_hl7_msg_bron('MSH') (2) (1) (1) <> v_hl7_msg_res(v_msh) (2) (1) (1)
         OR v_hl7_msg_bron('MSH') (v_max) (1) (1) <> v_hl7_msg_res(v_msh) (v_max) (1) (1)
         OR v_hl7_msg_bron('MSA') (1) (1) (2) <> v_hl7_msg_res(v_msa) (1) (1) (2)
         OR v_hl7_msg_bron('MSA') (v_max2) (1) (2) <> v_hl7_msg_res(v_msa) (v_max2) (1) (2)
      THEN
         dbms_output.put_line('Test Niet OK: ' || v_hl7_msg_bron('MSH') (2) (1) (1) || ' <> ' || v_hl7_msg_res(v_msh) (2) (1)
                              (1) || ' / ' || v_hl7_msg_bron('MSH') (v_max) (1) (1) || ' <> ' || v_hl7_msg_res(v_msh)
                              (v_max) (1) (1) || ' / ' || v_hl7_msg_bron('MSA') (1) (1)
                              (2) || ' <> ' || v_hl7_msg_res(v_msa) (1) (1) (2) || ' / ' || v_hl7_msg_bron('MSA')
                              (v_max2) (1) (2) || ' <> ' || v_hl7_msg_res(v_msa) (v_max2) (1) (2));
      ELSE
         dbms_output.put_line('Test OK!');
      END IF;
   END;

   PROCEDURE testsetsypa(p_sypa  VARCHAR2
                        ,p_value VARCHAR2) IS
   BEGIN
      IF p_sypa = 'ZIS_LOCATIE'
      THEN
         g_sypa_zis_locatie := p_value;
      ELSIF p_sypa = 'HL7_VERSIE'
      THEN
         g_sypa_hl7_versie := p_value;
      END IF;
      UPDATE kgc_systeem_parameters
         SET standaard_waarde = p_value
       WHERE code = p_sypa;
      COMMIT;
   END;

   ----------------------------------------------------------------------------
   FUNCTION get_sypa_sywa(p_code      VARCHAR2
                         ,p_refresh   BOOLEAN
                         ,p_verplicht BOOLEAN
                         ,x_waarde    IN OUT VARCHAR2) RETURN VARCHAR2 IS
      v_thisproc VARCHAR2(100) := 'kgc_zis.get_sypa_sywa';
   BEGIN
      IF NOT p_refresh
      THEN
         IF x_waarde IS NOT NULL
         THEN
            RETURN x_waarde;
         END IF;
      END IF;
      x_waarde := kgc_sypa_00.systeem_waarde(p_code);
      IF p_verplicht
      THEN
         IF nvl(length(x_waarde)
               ,0) = 0
         THEN
            raise_application_error(-20000
                                   ,v_thisproc || ' Error: Systeemparameter ' || p_code || ' is leeg of afwezig!');
         END IF;
      END IF;
      RETURN x_waarde;
   END;

   PROCEDURE log(p_tekst     IN VARCHAR2
                ,p_log_level IN VARCHAR2 := NULL
                ,p_type      IN VARCHAR2 := 'M') IS
      PRAGMA AUTONOMOUS_TRANSACTION; -- dwz: indien van toepassing: altijd opslaan. Zodoende kan er ook gelogd
      v_sypa_zis_log_level VARCHAR2(30) := 'ZIS_HL7_LOG_LEVEL';
      v_pos                NUMBER;
      v_loop               NUMBER;
      v_tekst              VARCHAR2(32677) := p_tekst;
      ----------------------------------------------------------------------------
   BEGIN
      IF p_log_level IS NOT NULL
      THEN
         g_log_level := p_log_level;
      ELSIF g_log_level IS NULL
      THEN
         -- nog niet gezet - ophalen
         g_log_level := kgc_sypa_00.systeem_waarde(v_sypa_zis_log_level);
         --    dbms_output.put_line('kgc_zis.log: ' || v_sypa_zis_log_level || '; waarde: ' || g_log_level);
      END IF;
      IF substr(g_log_level
               ,1
               ,1) = '1'
      THEN
         NULL; -- ok, iets doen
      ELSE
         RETURN;
      END IF;
      IF substr(g_log_level
               ,2
               ,1) = '1'
      THEN
         v_loop := 0;
         WHILE TRUE
         LOOP
            v_loop := v_loop + 1; -- just checking!
            IF v_loop > 1000
            THEN
               dbms_output.put_line('Fout in programma!');
               EXIT;
            END IF;
            IF nvl(length(v_tekst)
                  ,0) < 100
            THEN
               dbms_output.put_line(v_tekst);
               EXIT;
            END IF;
            v_pos := instr(substr(v_tekst
                                 ,1
                                 ,100)
                          ,chr(10));
            IF v_pos > 0
            THEN
               dbms_output.put_line(substr(v_tekst
                                          ,1
                                          ,v_pos - 1));
               v_tekst := substr(v_tekst
                                ,v_pos + 1);
            ELSE
               -- zoek laatste spatie voor pos 100
               v_pos := 0;
               WHILE instr(substr(v_tekst
                                 ,1
                                 ,100)
                          ,' '
                          ,v_pos + 1) > 0
               LOOP
                  v_pos := instr(substr(v_tekst
                                       ,1
                                       ,100)
                                ,' '
                                ,v_pos + 1);
               END LOOP;
               IF v_pos = 0
               THEN
                  dbms_output.put_line(substr(v_tekst
                                             ,1
                                             ,100));
                  v_tekst := substr(v_tekst
                                   ,101);
               ELSE
                  dbms_output.put_line(substr(v_tekst
                                             ,1
                                             ,v_pos - 1));
                  v_tekst := substr(v_tekst
                                   ,v_pos + 1);
               END IF;
            END IF;
         END LOOP;
      END IF;
      IF substr(g_log_level
               ,3
               ,1) = '1'
      THEN
         INSERT INTO kgc_hl7_log
            (tekst
            ,TYPE)
         VALUES
            (substr(p_tekst
                   ,1
                   ,4000)
            ,p_type);
         COMMIT;
      END IF;
      IF substr(g_log_level
               ,4
               ,1) = '1'
      THEN
         -- positie 11 en verder bevat email
         kgc_mail.post_mail('kgc_zis.log'
                           ,substr(g_log_level
                                  ,11)
                           ,'Logging dd ' || to_char(SYSDATE
                                                    ,'dd-mm-yyyy hh24:mi:ss')
                           ,p_tekst);
      END IF;
   END;

   -- deze procedure vertaalt de generieke onderdelen uit de hl7-message
   -- naar een persoonsrij
   -- de afwijkingen kunnen hierna verwerkt worden
   -- de tegenhanger van hl7msg2pers is rec2hl7msg!
   PROCEDURE hl7msg2pers(p_hl7_msg hl7_msg
                        ,p_extra   VARCHAR2
                        ,pers_trow IN OUT cg$kgc_personen.cg$row_type) IS
      v_pid VARCHAR2(10);
   BEGIN
      v_pid := getsegmentcode('PID'
                             ,p_hl7_msg);
      IF nvl(length(v_pid)
            ,0) = 0
      THEN
         RETURN; -- niets te doen!
      END IF;
      -- *** ZISnr
      pers_trow.zisnr := p_hl7_msg(v_pid) (3) (1) (1);
      IF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         pers_trow.zisnr := formatteer_zisnr_ut(pers_trow.zisnr);
      END IF;
      -- *** geboortedatum / part_geboortedatum
      IF nvl(p_hl7_msg(v_pid) (7) (1) (1)
            ,'00000000') = '00000000'
      THEN
         pers_trow.geboortedatum := NULL;
      ELSE
         DECLARE
            v_jaar  VARCHAR2(4);
            v_maand VARCHAR2(2);
         BEGIN
            pers_trow.geboortedatum      := to_date(p_hl7_msg(v_pid) (7) (1) (1)
                                                   ,'YYYYMMDD');
            pers_trow.part_geboortedatum := NULL;
         EXCEPTION
            WHEN OTHERS THEN
               -- opbergen in part_geboortedatum
               pers_trow.geboortedatum      := NULL;
               pers_trow.part_geboortedatum := NULL;
               v_jaar                       := ltrim(rtrim(substr(p_hl7_msg(v_pid) (7) (1) (1)
                                                                 ,1
                                                                 ,4)));
               v_jaar                       := REPLACE(v_jaar
                                                      ,'0000'
                                                      ,'');
               IF nvl(length(v_jaar)
                     ,0) > 0
               THEN
                  -- iets aanwezig > gebruiken
                  pers_trow.part_geboortedatum := '00-'; -- dag
                  -- maand
                  v_maand := ltrim(rtrim(substr(p_hl7_msg(v_pid) (7) (1) (1)
                                               ,5
                                               ,2)));
                  v_maand := REPLACE(v_maand
                                    ,'00'
                                    ,'');
                  IF nvl(length(v_maand)
                        ,0) > 0
                  THEN
                     pers_trow.part_geboortedatum := pers_trow.part_geboortedatum ||
                                                     lpad(v_maand
                                                         ,2
                                                         ,'0') || '-';
                  ELSE
                     pers_trow.part_geboortedatum := pers_trow.part_geboortedatum || '00-';
                  END IF;
                  -- jaar
                  pers_trow.part_geboortedatum := pers_trow.part_geboortedatum ||
                                                  lpad(v_jaar
                                                      ,4
                                                      ,'0');
               END IF;
         END;
      END IF;
      -- *** geslacht
      IF p_hl7_msg(v_pid) (8) (1) (1) = 'M'
      THEN
         pers_trow.geslacht := 'M';
      ELSIF p_hl7_msg(v_pid) (8) (1) (1) IN ('V'
                   ,'F')
      THEN
         pers_trow.geslacht := 'V';
      ELSE
         pers_trow.geslacht := 'O';
      END IF;
      -- *** meerling
      IF p_hl7_msg(v_pid) (24) (1) (1) IN ('Y'
                ,'J')
      THEN
         pers_trow.meerling := 'J';
      ELSE
         pers_trow.meerling := 'N';
      END IF;
      -- *** adres
      pers_trow.adres := p_hl7_msg(v_pid) (11) (1) (1);
      IF nvl(length(p_hl7_msg(v_pid) (11) (1) (2))
            ,0) > 0
      THEN
         -- NL-profile: straat en huisnr gescheiden!
         pers_trow.adres := pers_trow.adres || ' ' || p_hl7_msg(v_pid) (11) (1) (2);
      END IF;
      pers_trow.woonplaats := rtrim(substr(p_hl7_msg(v_pid) (11) (1) (3)
                                          ,1
                                          ,50));
      kgc_formaat_00.formatteer(p_naamdeel => 'WOONPLAATS'
                               ,x_naam     => pers_trow.woonplaats);
      kgc_zis.gethl7waarde(rtrim(substr(p_hl7_msg(v_pid) (11) (1) (5)
                                       ,1
                                       ,7))
                          ,pers_trow.postcode); -- formattering: specifiek!!!
   END;

   PROCEDURE hl7msg2rela -- alleen voor Utrecht / Maastricht; niet Nijmegen!
   (p_hl7_msg hl7_msg
   ,p_extra   VARCHAR2
   ,rela_trow IN OUT cg$kgc_relaties.cg$row_type) IS
      v_stf          VARCHAR2(10);
      v_voorletters1 VARCHAR2(100);
      v_voorletters2 VARCHAR2(100);
      v_liszcode     VARCHAR2(100);
   BEGIN
      v_stf := getsegmentcode('STF'
                             ,p_hl7_msg);
      IF nvl(length(v_stf)
            ,0) = 0
      THEN
         -- geen STF-segment aanwezig
         RETURN;
      END IF;
      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (1) (1) (1)
                          ,v_liszcode);
      IF nvl(length(v_liszcode)
            ,0) = 15
      THEN
         -- rde 6 juni 2006: indien <> 15: niet uniek, niet gebruiken!
         rela_trow.liszcode := v_liszcode;
      END IF;
      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (3) (1) (1)
                          ,rela_trow.achternaam);

      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (3) (1) (2)
                          ,v_voorletters1);
      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (3) (1) (3)
                          ,v_voorletters2);
      rela_trow.voorletters := v_voorletters1 || v_voorletters2;

      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (3) (1) (4)
                          ,rela_trow.briefaanhef);
      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (11) (1) (1)
                          ,rela_trow.adres);
      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (11) (1) (3)
                          ,rela_trow.woonplaats);
      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (11) (1) (5)
                          ,rela_trow.postcode);
      kgc_zis.gethl7waarde(p_hl7_msg(v_stf) (10) (1) (1)
                          ,rela_trow.telefoon);

      kgc_formaat_00.standaard_aanspreken(p_type        => 'RELA'
                                         ,p_voorletters => rela_trow.voorletters
                                         ,p_voorvoegsel => rela_trow.voorvoegsel
                                         ,p_achternaam  => rela_trow.achternaam
                                         ,p_prefix      => ltrim(rela_trow.briefaanhef || ' ')
                                         ,x_aanspreken  => rela_trow.aanspreken);
      -- vervallen
      rela_trow.vervallen := nvl(rela_trow.vervallen
                                ,'N'); -- default
      IF nvl(p_hl7_msg(v_stf) (3) (1) (1)
            ,'') <> ''
      THEN
         rela_trow.vervallen := 'J';
      END IF;
      -- postcode/provincie/land: lokaal: eerst evt bestaande land ophalen
   END;

   PROCEDURE hl7msg2verz -- alleen voor Utrecht / Maastricht; niet Nijmegen!
   (p_hl7_msg hl7_msg
   ,p_extra   VARCHAR2
   ,verz_trow IN OUT cg$kgc_verzekeraars.cg$row_type) IS
      v_in1 VARCHAR2(10);
   BEGIN
      v_in1 := getsegmentcode('IN1'
                             ,p_hl7_msg);
      IF nvl(length(v_in1)
            ,0) = 0
      THEN
         -- geen IN1-segment aanwezig
         RETURN;
      END IF;
      kgc_zis.gethl7waarde(substr(ltrim(rtrim(p_hl7_msg(v_in1) (4) (1) (1)))
                                 ,1
                                 ,100)
                          ,verz_trow.naam);
      kgc_zis.gethl7waarde(p_hl7_msg(v_in1) (5) (1) (1)
                          ,verz_trow.adres);
      kgc_zis.gethl7waarde(p_hl7_msg(v_in1) (5) (1) (3)
                          ,verz_trow.woonplaats);
      kgc_zis.gethl7waarde(p_hl7_msg(v_in1) (5) (1) (5)
                          ,verz_trow.postcode);
      -- vervallen
      verz_trow.vervallen := nvl(verz_trow.vervallen
                                ,'N'); -- default
      -- machtiging_nodig
      verz_trow.machtiging_nodig := nvl(verz_trow.machtiging_nodig
                                       ,'J'); -- default
   END;

   -- generieke actie; afwijkingen in specifiek-centrum-package.proc
   -- de tegenhanger van rec2hl7msg is hl7msg2pers!
   -- indien een segment niet aanwezig is in x_hl7_msg, dan wordt deze niet gevuld!
   PROCEDURE rec2hl7msg(pers_trow IN cg$kgc_personen.cg$row_type
                       ,rela_trow IN cg$kgc_relaties.cg$row_type
                       ,verz_trow IN cg$kgc_verzekeraars.cg$row_type
                       ,p_extra   IN VARCHAR2
                       ,x_hl7_msg IN OUT hl7_msg) IS
      v_pid      VARCHAR2(10);
      v_stf      VARCHAR2(10);
      v_in1      VARCHAR2(10);
      v_debug    VARCHAR2(100);
      v_thisproc VARCHAR2(100) := 'kgc_zis.rec2hl7msg';
   BEGIN
      v_debug := '1';
      v_pid   := getsegmentcode('PID'
                               ,x_hl7_msg);
      v_stf   := getsegmentcode('STF'
                               ,x_hl7_msg);
      v_in1   := getsegmentcode('IN1'
                               ,x_hl7_msg);
      IF nvl(length(v_pid)
            ,0) > 0
      THEN
         -- *** geboortedatum / part_geboortedatum
         IF pers_trow.geboortedatum IS NOT NULL
         THEN
            x_hl7_msg(v_pid)(7)(1)(1) := to_char(pers_trow.geboortedatum
                                                ,'yyyymmdd');
         ELSIF pers_trow.part_geboortedatum IS NOT NULL
         THEN
            x_hl7_msg(v_pid)(7)(1)(1) := substr(pers_trow.part_geboortedatum
                                               ,7
                                               ,4) || substr(pers_trow.part_geboortedatum
                                                            ,4
                                                            ,2) || substr(pers_trow.part_geboortedatum
                                                                         ,1
                                                                         ,2);
         ELSE
            x_hl7_msg(v_pid)(7)(1)(1) := '00000000';
         END IF;
         -- *** geslacht - naar locatie-specifiek
         -- *** meerling - naar locatie-specifiek
      END IF;

      IF nvl(length(v_stf)
            ,0) > 0
         AND -- STF-segment aanwezig
         pers_trow.rela_id IS NOT NULL
         AND -- huisarts aanwezig
         rela_trow.rela_id IS NOT NULL
      THEN
         v_debug := v_debug || '2';
         x_hl7_msg(v_stf)(1)(1)(1) := rela_trow.liszcode;
         -- voor de vorm: nog 2 velden aanbrengen - dit wordt gebruikt om te kijken of er een huisarts aanwezig is (utrecht, ...)
         IF x_hl7_msg(v_stf) (1) (1).COUNT < 2
         THEN
            x_hl7_msg(v_stf)(1)(1) .EXTEND;
         END IF;
         IF x_hl7_msg(v_stf) (1) (1).COUNT < 3
         THEN
            x_hl7_msg(v_stf)(1)(1) .EXTEND;
         END IF;
         x_hl7_msg(v_stf)(1)(1)(3) := 'VZ';
         v_debug := v_debug || '3';
         x_hl7_msg(v_stf)(3)(1)(1) := rela_trow.achternaam;
         IF x_hl7_msg(v_stf) (3) (1).COUNT < 2
         THEN
            x_hl7_msg(v_stf)(3)(1) .EXTEND;
         END IF;
         IF x_hl7_msg(v_stf) (3) (1).COUNT < 3
         THEN
            x_hl7_msg(v_stf)(3)(1) .EXTEND;
         END IF;
         IF nvl(length(rela_trow.voorletters)
               ,0) > 0
         THEN
            IF instr(rela_trow.voorletters
                    ,'.') > 0
            THEN
               x_hl7_msg(v_stf)(3)(1)(2) := substr(rela_trow.voorletters
                                                  ,1
                                                  ,instr(rela_trow.voorletters
                                                        ,'.'));
               x_hl7_msg(v_stf)(3)(1)(3) := substr(rela_trow.voorletters
                                                  ,instr(rela_trow.voorletters
                                                        ,'.') + 1);
            ELSE
               x_hl7_msg(v_stf)(3)(1)(2) := substr(rela_trow.voorletters
                                                  ,1
                                                  ,1);
               x_hl7_msg(v_stf)(3)(1)(3) := substr(rela_trow.voorletters
                                                  ,2);
            END IF;
         END IF;
         v_debug := v_debug || '4';
         IF x_hl7_msg(v_stf) (3) (1).COUNT < 4
         THEN
            x_hl7_msg(v_stf)(3)(1) .EXTEND;
         END IF;
         x_hl7_msg(v_stf)(3)(1)(4) := rela_trow.briefaanhef;

         x_hl7_msg(v_stf)(11)(1)(1) := pers_trow.adres;
         IF x_hl7_msg(v_stf) (11) (1).COUNT < 2
         THEN
            x_hl7_msg(v_stf)(11)(1) .EXTEND;
         END IF;
         IF x_hl7_msg(v_stf) (11) (1).COUNT < 3
         THEN
            x_hl7_msg(v_stf)(11)(1) .EXTEND;
         END IF;
         x_hl7_msg(v_stf)(11)(1)(3) := rela_trow.woonplaats;
         IF x_hl7_msg(v_stf) (11) (1).COUNT < 4
         THEN
            x_hl7_msg(v_stf)(11)(1) .EXTEND;
         END IF;
         IF x_hl7_msg(v_stf) (11) (1).COUNT < 5
         THEN
            x_hl7_msg(v_stf)(11)(1) .EXTEND;
         END IF;
         IF nvl(upper(rela_trow.land)
               ,'NEDERLAND') = 'NEDERLAND'
         THEN
            x_hl7_msg(v_stf)(11)(1)(5) := REPLACE(rela_trow.postcode
                                                 ,' '
                                                 ,'');
         ELSE
            x_hl7_msg(v_stf)(11)(1)(5) := rela_trow.postcode;
         END IF;
         v_debug := v_debug || '5';
         -- land altijd leeg! x_hl7_msg(v_STF)(11)(1)(6) := rela_trow.land;

         x_hl7_msg(v_stf)(10)(1)(1) := rela_trow.telefoon;
         -- ??? JN vertalen in datum??? x_hl7_msg(v_STF)(3)(1)(1) := rela_trow.vervallen;
      END IF;

      IF nvl(length(v_in1)
            ,0) > 0
         AND -- IN1-segment aanwezig
         pers_trow.verz_id IS NOT NULL
         AND -- verzekeraar aanwezig
         verz_trow.verz_id IS NOT NULL
      THEN
         v_debug := v_debug || '6';
         x_hl7_msg(v_in1)(3)(1)(1) := verz_trow.code;
         x_hl7_msg(v_in1)(4)(1)(1) := verz_trow.naam;
         x_hl7_msg(v_in1)(5)(1)(1) := verz_trow.adres;
         v_debug := v_debug || '7';
         IF x_hl7_msg(v_in1) (5) (1).COUNT < 2
         THEN
            x_hl7_msg(v_in1)(5)(1) .EXTEND;
         END IF;
         IF x_hl7_msg(v_in1) (5) (1).COUNT < 3
         THEN
            x_hl7_msg(v_in1)(5)(1) .EXTEND;
         END IF;
         x_hl7_msg(v_in1)(5)(1)(3) := verz_trow.woonplaats;
         IF x_hl7_msg(v_in1) (5) (1).COUNT < 4
         THEN
            x_hl7_msg(v_in1)(5)(1) .EXTEND;
         END IF;
         IF x_hl7_msg(v_in1) (5) (1).COUNT < 5
         THEN
            x_hl7_msg(v_in1)(5)(1) .EXTEND;
         END IF;
         IF nvl(upper(verz_trow.land)
               ,'NEDERLAND') = 'NEDERLAND'
         THEN
            x_hl7_msg(v_in1)(5)(1)(5) := REPLACE(verz_trow.postcode
                                                ,' '
                                                ,'');
         ELSE
            x_hl7_msg(v_in1)(5)(1)(5) := verz_trow.postcode;
         END IF;
         v_debug := v_debug || '8';
         -- land altijd leeg! x_hl7_msg(v_IN1)(11)(1)(6) := verz_trow.land;
      END IF;

      -- locatie specifiek:
      v_debug := v_debug || '9';
      IF g_sypa_zis_locatie = 'NIJMEGEN'
      THEN
         kgc_zis_nijmegen.rec2hl7msg(pers_trow
                                    ,rela_trow
                                    ,verz_trow
                                    ,p_extra
                                    ,x_hl7_msg);
      ELSIF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         kgc_zis_utrecht.rec2hl7msg(pers_trow
                                   ,rela_trow
                                   ,verz_trow
                                   ,p_extra
                                   ,x_hl7_msg);
      ELSIF g_sypa_zis_locatie = 'MAASTRICHT'
      THEN
          null;


      ELSE
         raise_application_error(-20000
                                ,v_thisproc || ' Error: Onbekende waarde voor systeemparameter ZIS_LOCATIE: "' ||
                                 g_sypa_zis_locatie || '"!');
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         IF SQLCODE = -20000
         THEN
            RAISE; -- bevat al toelichting
         ELSE
            raise_application_error(-20000
                                   ,v_thisproc || ': Debug: ' || v_debug || '; Error: ' || SQLERRM);
         END IF;
   END;

   PROCEDURE addtext -- plak text eraan tot maximum
   (x_text       IN OUT VARCHAR2
   ,p_max        IN NUMBER
   ,p_extra_text IN VARCHAR2
   ,p_separator  IN VARCHAR2 := chr(10) -- '; ' --
    ) IS
      v_len     NUMBER;
      v_len_add NUMBER;
      v_sep     VARCHAR2(10) := p_separator;
   BEGIN
      v_len     := nvl(length(x_text)
                      ,0);
      v_len_add := p_max - v_len; -- beschikbare ruimte
      IF v_len_add > 0
      THEN
         IF (x_text IS NULL)
         THEN
            x_text := substr(p_extra_text
                            ,1
                            ,v_len_add);
         ELSE
            x_text := x_text || substr(v_sep || p_extra_text
                                      ,1
                                      ,v_len_add);
         END IF;
      END IF;
   END addtext;

   ----------------------------------------------------------------------------
   FUNCTION persoon_in_zis(p_zisnr   IN VARCHAR2
                          ,pers_trow IN OUT cg$kgc_personen.cg$row_type
                          ,rela_trow IN OUT cg$kgc_relaties.cg$row_type
                          ,verz_trow IN OUT cg$kgc_verzekeraars.cg$row_type
                          ,x_msg     OUT VARCHAR2) RETURN BOOLEAN IS
      ----------------------------------------------------------------------------
      v_thisproc VARCHAR2(100) := 'kgc_zis.persoon_in_zis';

   BEGIN
      log(v_thisproc || ': Start!');
      g_sypa_zis_locatie := kgc_sypa_00.systeem_waarde('ZIS_LOCATIE');
      g_sypa_hl7_versie  := kgc_sypa_00.systeem_waarde('HL7_VERSIE');
      log(v_thisproc || ': HL7_VERSIE: ' || g_sypa_hl7_versie || '; ZIS_LOCATIE: ' || g_sypa_zis_locatie);
      IF g_sypa_zis_locatie = 'NIJMEGEN'
      THEN
         RETURN kgc_zis_nijmegen.persoon_in_zis(p_zisnr   => p_zisnr
                                               ,pers_trow => pers_trow
                                               ,rela_trow => rela_trow
                                               ,verz_trow => verz_trow
                                               ,x_msg     => x_msg);
      ELSIF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         RETURN kgc_zis_utrecht.persoon_in_zis(p_zisnr   => p_zisnr
                                              ,pers_trow => pers_trow
                                              ,rela_trow => rela_trow
                                              ,verz_trow => verz_trow
                                              ,x_msg     => x_msg);
      ELSIF g_sypa_zis_locatie = 'MAASTRICHT'
      THEN
         RETURN kgc_zis_maastricht.persoon_in_zis(p_zisnr   => p_zisnr
                                                 ,pers_trow => pers_trow
                                                 ,rela_trow => rela_trow
                                                 ,verz_trow => verz_trow
                                                 ,x_msg     => x_msg);
      --ELSE
      --   -- simuleer ZIS-koppeling: gegevens worden uit de tabel BEH_ZIS_KOPPELING gehaald
      --   RETURN beh_zis_simulatie.persoon_in_zis(p_zisnr   => p_zisnr
      --                                          ,pers_trow => pers_trow
      --                                          ,rela_trow => rela_trow
      --                                          ,verz_trow => verz_trow
      --                                          ,x_msg     => x_msg);
      END IF;
   END;

   ----------------------------------------------------------------------------
   FUNCTION luister(ppoortnr  IN NUMBER
                   ,pbevestig IN STRING
                   ,pmessages IN STRING
                   ,ptimeout  IN NUMBER -- milliseconden
                    ) RETURN STRING IS
      ----------------------------------------------------------------------------
      LANGUAGE JAVA NAME 'HL7.Luister(int, java.lang.String, java.lang.String, int) return java.lang.String';

   ----------------------------------------------------------------------------
   FUNCTION testjava RETURN STRING IS
      ----------------------------------------------------------------------------
      LANGUAGE JAVA NAME 'HL7.test() return java.lang.String';

   ----------------------------------------------------------------------------
   -- beheerfunctie voor HL7 en het luisterproces
   -- p_action:
   --   B = begin job voor luisterproces
   --   E = eindig
   --   C = check of luisterproces gestart moet worden (bv bij db-startup)
   --   S = status
   PROCEDURE beheer_hl7(p_action      IN VARCHAR2
                       ,p_check_email IN VARCHAR2) IS
      v_opmerkingen VARCHAR2(32000);
      CURSOR jobs_cur(p_jobnaam VARCHAR2) IS
         SELECT *
           FROM all_jobs
          WHERE instr(lower(what)
                     ,lower(p_jobnaam)) > 0;
      jobs_rec jobs_cur%ROWTYPE;
      CURSOR c_hlbe IS
         SELECT trunc(creation_date) datum
               ,SUM(decode(status
                          ,'V'
                          ,1
                          ,0)) countv
               ,SUM(decode(status
                          ,'N'
                          ,1
                          ,0)) countn
               ,SUM(decode(status
                          ,'X'
                          ,1
                          ,0)) countx
           FROM kgc_hl7_berichten hlbe
          WHERE creation_date > trunc(SYSDATE - 7)
          GROUP BY trunc(creation_date);
      CURSOR c_sypa(p_parameter_code VARCHAR2) IS
         SELECT *
           FROM kgc_systeem_parameters
          WHERE code = upper(p_parameter_code);
      r_sypa c_sypa%ROWTYPE;
      CURSOR c_spwa(p_sypa_id NUMBER) IS
         SELECT mede.code mede_code
               ,mede.naam mede_naam
               ,ongr.code ongr_code
               ,kafd.code kafd_code
               ,spwa.waarde
           FROM kgc_systeem_par_waarden spwa
               ,kgc_kgc_afdelingen      kafd
               ,kgc_onderzoeksgroepen   ongr
               ,kgc_medewerkers         mede
          WHERE spwa.sypa_id = p_sypa_id
            AND spwa.mede_id = mede.mede_id(+)
            AND spwa.ongr_id = ongr.ongr_id(+)
            AND spwa.kafd_id = kafd.kafd_id(+);
      TYPE listsypatype IS TABLE OF VARCHAR2(100);
      v_listsypa      listsypatype;
      v_sypa          VARCHAR2(100);
      v_pipe_inh      VARCHAR2(32767);
      v_sysdate       VARCHAR2(20) := to_char(SYSDATE
                                             ,'dd-mm-yyyy hh24:mi:ss');
      v_actief        BOOLEAN := FALSE;
      v_mail_msg      VARCHAR2(32767);
      v_newline       VARCHAR2(10) := chr(10);
      v_luister       VARCHAR2(200);
      v_action        VARCHAR2(1) := upper(substr(p_action
                                                 ,1
                                                 ,1));
      v_return        INTEGER := 0;
      v_show_status   BOOLEAN := FALSE;
      v_begin_luister BOOLEAN := FALSE;
      v_job           BINARY_INTEGER;
      v_debug         VARCHAR2(100);
      v_thisproc      VARCHAR2(100) := 'kgc_zis.beheer_hl7';
   BEGIN
      dbms_output.put_line(v_thisproc || ': Start (' || to_char(SYSDATE
                                                               ,'dd-mm-yyyy hh24:mi:ss') || ')');
      IF v_action = 'C'
      THEN
         -- Check of het HL7-luisterproces opgestart moet worden
         dbms_output.put_line('Actie: Check of het HL7-luisterproces opgestart moet worden');
         v_luister := kgc_sypa_00.systeem_waarde(p_parameter_code => 'ZIS_HL7_LISTENER');
         dbms_output.put_line('Systeemparameter ZIS_HL7_LISTENER staat op ' || v_luister || '!');
         IF v_luister = 'AAN'
         THEN
            v_begin_luister := TRUE;
         ELSE
            dbms_output.put_line('Geen actie!');
         END IF;
      END IF;

      IF v_action = 'B'
         OR v_begin_luister
      THEN
         dbms_output.put_line('Actie: Start luisteren naar HL7-berichten');
         UPDATE kgc_systeem_parameters sypa
            SET standaard_waarde = 'AAN'
          WHERE code = 'ZIS_HL7_LISTENER';
         COMMIT;

         -- controleren of de job bestaat
         OPEN jobs_cur(g_jobwhat);
         FETCH jobs_cur
            INTO jobs_rec;
         IF jobs_cur%NOTFOUND
         THEN
            -- niet aanwezig? toevoegen!
            dbms_job.submit(job       => v_job
                           ,what      => g_jobwhat || ';' --'kgc_zis.start_hl7_luister;'
                           ,next_date => SYSDATE
                           ,INTERVAL  => 'SYSDATE + 1/(24*4)' -- kwartier
                           ,instance  => 1 -- Mantis 2263: De HL7-luisterjob wil niet correct starten
                            );
            COMMIT;
            dbms_output.put_line('Het HL7-luisterproces is opgestart (job "' || g_jobwhat || '", id ' ||
                                 to_char(v_job) || ')!');
         ELSE
            -- reactiveren; status wordt hierna getoond!
            dbms_job.broken
            --      DBMS_JOB.NEXT_DATE
            (job       => jobs_rec.job
            ,broken    => FALSE
            ,next_date => SYSDATE -- nu!
             );
            COMMIT;
            dbms_output.put_line('Het HL7-luisterproces is gereactiveerd (job "' || g_jobwhat || '", id ' ||
                                 to_char(jobs_rec.job) || ')!');
         END IF;
         CLOSE jobs_cur;
         v_show_status := TRUE;
      ELSIF v_action = 'E'
      THEN
         -- stop doorgeven naar luisterproces
         dbms_output.put_line('Actie: Eindig luisteren naar HL7-berichten');
         -- zet listener uit
         UPDATE kgc_systeem_parameters sypa
            SET standaard_waarde = 'UIT'
          WHERE code = 'ZIS_HL7_LISTENER';
         COMMIT;
         -- stop proces
         dbms_pipe.pack_message('Stop');
         v_return := dbms_pipe.send_message('hl7_control'
                                           ,30); -- max 30 sec wachten - zou overbodig moeten zijn!
         IF v_return = 1
         THEN
            -- 0 = succes; 1=timeout(=ok!)
            dbms_output.put_line('Timeout ontvangen');
         ELSIF v_return = 0
         THEN
            dbms_output.put_line('Luisterproces is beeindigd!');
         ELSE
            dbms_output.put_line('Onbekende returncode ontvangen: ' || to_char(v_return));
         END IF;
         dbms_lock.sleep(5); -- seconden! Job-admin even de tijd geven...
         -- job stop (evt wordt het proces (luister) weer opgestart, maar omdat de listener
         -- inmiddels op UIT staat, is dit direct afgelopen!
         OPEN jobs_cur(g_jobwhat);
         FETCH jobs_cur
            INTO jobs_rec;
         IF jobs_cur%NOTFOUND
         THEN
            -- niet aanwezig? ok
            NULL;
         ELSE
            -- stop; status wordt hierna getoond!
            dbms_job.next_date(job       => jobs_rec.job
                              ,next_date => NULL);
            COMMIT;
            dbms_output.put_line('De HL7-job is gestopt (job "' || g_jobwhat || '", id ' || to_char(jobs_rec.job) || ')!');
         END IF;
         CLOSE jobs_cur;
         v_show_status := TRUE;
      END IF;

      IF v_action = 'S'
         OR v_show_status
      THEN
         dbms_output.put_line('Actie: Toon status HL7-interface');
         dbms_output.put_line('Databasename: ' || database_name);
         -- *** overzicht status listener
         v_sypa := kgc_sypa_00.systeem_waarde('ZIS_HL7_LISTENER');
         dbms_output.put_line('De HL7-listener server staat ' || v_sypa || '; poort "' ||
                              kgc_sypa_00.systeem_waarde('ZIS_HL7_SVR_POORT_LISTENER') || '"');
         IF v_sypa = 'AAN'
         THEN
            -- interface actief? 5 pogingen...
            FOR i IN 1 .. 5
            LOOP
               dbms_pipe.pack_message('ALIVE?');
               v_return := dbms_pipe.send_message('hl7_control'
                                                 ,10); -- max 10 sec wachten - zou overbodig moeten zijn!
               -- effe het proces de kans geven...
               dbms_lock.sleep(1); -- seconden! sys must grant execute on dbms_lock to ...;
               dbms_output.put_line('HL7-listenerloop poging ' || to_char(i));
               -- lezen
               v_return := dbms_pipe.receive_message('hl7_control'
                                                    ,0); -- verwijderd het gelezene
               IF v_return = 1
               THEN
                  -- 1=timeout; dan heeft een ander proces het ingepikt - dus actief!
                  v_actief := TRUE;
                  EXIT;
               END IF;
               BEGIN
                  dbms_pipe.unpack_message(v_pipe_inh);
               EXCEPTION
                  WHEN OTHERS THEN
                     NULL;
               END;
               IF nvl(length(v_pipe_inh)
                     ,0) = 0
               THEN
                  -- leeg? dan heeft een ander proces het ingepikt - dus actief!
                  v_actief := TRUE;
                  EXIT;
               END IF;
            END LOOP;
            IF v_actief
            THEN
               -- OK
               dbms_output.put_line('HL7-listener luistert ook daadwerkelijk!');
            ELSE
               dbms_output.put_line('HL7-listener luistert echter NIET (binnen 5 keer 10 seconden)!');
            END IF;
         END IF;
         -- *** job?
         OPEN jobs_cur(g_jobwhat);
         FETCH jobs_cur
            INTO jobs_rec;
         IF jobs_cur%NOTFOUND
         THEN
            -- niet aanwezig? ok
            dbms_output.put_line('De HL7-job is niet aanwezig in de database (job "' || g_jobwhat || '")!');
         ELSE
            -- stop; status wordt hierna getoond!
            dbms_output.put_line('De HL7-job is aanwezig in de database (job "' || g_jobwhat || '", id ' ||
                                 to_char(jobs_rec.job) || ')!');
            dbms_output.put_line('> Last_date: ' || to_char(jobs_rec.last_date
                                                           ,'dd-mm-yyyy hh24:mi:ss') || ' This_date: ' ||
                                 to_char(jobs_rec.this_date
                                        ,'dd-mm-yyyy hh24:mi:ss') || ' Next_date: ' ||
                                 to_char(jobs_rec.next_date
                                        ,'dd-mm-yyyy hh24:mi:ss'));
            -- aktief?
            IF jobs_rec.this_date IS NOT NULL
               AND jobs_rec.this_date > nvl(jobs_rec.last_date
                                           ,jobs_rec.this_date)
            THEN
               dbms_output.put_line('> Job draait.');
            ELSE
               dbms_output.put_line('> Job draait niet.');
            END IF;
         END IF;
         CLOSE jobs_cur;

         -- *** overzicht status verwerkte berichten afgelopen week
         dbms_output.put_line(chr(10) || 'Overzicht verwerkte berichten afgelopen week:');
         FOR r_hlbe IN c_hlbe
         LOOP
            dbms_output.put_line(to_char(r_hlbe.datum
                                        ,'dd-mm-yyyy') || ' V=Verwerkt: ' ||
                                 to_char(r_hlbe.countv
                                        ,09999) || ' N=Later: ' || to_char(r_hlbe.countn
                                                                          ,09999) || ' X=Fout: ' ||
                                 to_char(r_hlbe.countx
                                        ,09999));
         END LOOP;
         -- *** overzicht systeemparameters
         v_listsypa := listsypatype('ZIS_LOCATIE'
                                   ,'MAIL_SERVER'
                                   ,'ZIS_MAIL_AFZENDER'
                                   ,'ZIS_MAIL_ERR'
                                   ,'HL7_VERSIE'
                                   ,'ZIS_HL7_LISTENER'
                                   ,'ZIS_HL7_SVR_POORT_LISTENER'
                                   ,'ZIS_HL7_SVR_LISTEN_TO_MSG'
                                   ,'ZIS_HL7_SVR_NAAM_QRY'
                                   ,'ZIS_HL7_SVR_POORT_QRY'
                                   ,'ZIS_HL7_SVR_NAAM_DFT'
                                   ,'ZIS_HL7_SVR_POORT_DFT'
                                   ,'ZIS_MAIL_ADTA08_INFO'
                                   ,'ZIS_ADTA08_BLOK'
                                   ,'ZIS_MAIL_ADTA08_BLOK'
                                   ,'ZIS_MAIL_ADTA24'
                                   ,'ZIS_MAIL_ADTA37'
                                   ,'ZIS_HL7_LOG_LEVEL');

         dbms_output.put_line(chr(10) || 'Systeemparameters:');
         FOR i IN v_listsypa.FIRST .. v_listsypa.LAST
         LOOP
            OPEN c_sypa(v_listsypa(i));
            FETCH c_sypa
               INTO r_sypa;
            IF c_sypa%NOTFOUND
            THEN
               dbms_output.put_line('Er is geen Systeemparameter "' || v_listsypa(i) || '" gevonden!');
            ELSE
               dbms_output.put_line('>> ' || rpad(r_sypa.code
                                                 ,31) || substr(r_sypa.omschrijving
                                                               ,1
                                                               ,45));
               dbms_output.put_line('=  ' || substr(r_sypa.standaard_waarde
                                                   ,1
                                                   ,80));
               FOR r_spwa IN c_spwa(r_sypa.sypa_id)
               LOOP
                  dbms_output.put_line('>  ' || rpad(r_spwa.mede_code
                                                    ,11) || rpad(r_spwa.ongr_code
                                                                ,11) || rpad(r_spwa.kafd_code
                                                                            ,11) ||
                                       substr(r_spwa.waarde
                                             ,1
                                             ,50));
               END LOOP;
            END IF;
            CLOSE c_sypa;
         END LOOP;
      END IF;

      IF p_check_email = 'J'
      THEN
         dbms_output.put_line(chr(10) || 'Email check...');
         v_mail_msg := '<html>' || g_html_mail_head || '<body>' || kgc_zis.g_cr ||
                       '<p>Deze mail is een check of de smtp-server werkt' || '<p>Verzonden door ' || v_thisproc ||
                       kgc_zis.g_cr || '<p><p>Succes, <p>' || v_thisproc || '</body></html>';
         BEGIN
            kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                                 ,'Helix check SMTP-server (' || upper(database_name) || ' / ' || v_sysdate || ')'
                                 ,v_mail_msg
                                 ,TRUE);
            dbms_output.put_line('Email succesvol verzonden naar personen in SYPA ' || kgc_zis.g_sypa_zis_mail_err ||
                                 '; SMTP-server ' || kgc_sypa_00.systeem_waarde('MAIL_SERVER'));
         EXCEPTION
            WHEN OTHERS THEN
               dbms_output.put_line(substr(chr(10) || 'De Email-service is NIET beschikbaar! (fout ' || SQLERRM || ')'
                                          ,1
                                          ,255));
         END;
      END IF;
      dbms_output.put_line(v_thisproc || ': Eind (' || to_char(SYSDATE
                                                              ,'dd-mm-yyyy hh24:mi:ss') || ')');
   END;

   -- luisteren naar hl7-berichten
   --
   PROCEDURE start_hl7_luister IS
      v_loop                INTEGER := 0;
      v_return              NUMBER := 0;
      v_sysdate             VARCHAR2(20);
      v_hl7_svr_poort_lstnr NUMBER;
      v_msg_ok              VARCHAR2(2000);
      v_errormessage        VARCHAR2(32000);
      v_hl7_msg             hl7_msg;
      v_luister             VARCHAR2(200);
      v_debug               VARCHAR2(100);
      v_thisproc            VARCHAR2(100) := 'kgc_zis.start_hl7_luister';
   BEGIN
      v_debug               := '1';
      v_hl7_svr_poort_lstnr := gethl7poort('ZIS_HL7_SVR_POORT_LISTENER');
      g_log_level           := NULL; -- zodat deze opnieuw wordt opgehaald!
      -- pijp leegmaken
      dbms_pipe.purge('hl7_control');
      g_sypa_listen_to_msg := get_sypa_sywa('ZIS_HL7_SVR_LISTEN_TO_MSG'
                                           ,TRUE
                                           ,TRUE
                                           ,g_sypa_listen_to_msg);
      g_sypa_zis_locatie   := get_sypa_sywa('ZIS_LOCATIE'
                                           ,TRUE
                                           ,TRUE
                                           ,g_sypa_zis_locatie);
      g_sypa_hl7_versie    := get_sypa_sywa('HL7_VERSIE'
                                           ,TRUE
                                           ,TRUE
                                           ,g_sypa_hl7_versie);
      -- maak msg ter bevestiging: MSH + MSA terugsturen
      v_debug := '2';
      v_hl7_msg('MSH') := init_msg('MSH'
                                  ,g_sypa_hl7_versie);
      v_hl7_msg('MSH')(1)(1)(1) := '|';
      v_hl7_msg('MSH')(2)(1)(1) := '^~&';
      -- Mantis 283 RDE 02-04-2009: conform spec: niet HELIX, maar overnemen uit bericht
      v_hl7_msg('MSH')(3)(1)(1) := '<HELIX_SND_APP>';
      v_hl7_msg('MSH')(4)(1)(1) := 'KGC';
      v_hl7_msg('MSH')(5)(1)(1) := '<HELIX_REC_APP>'; -- flex: wordt omgezet in java-functie
      -- Mantis 283 RDE 02-04-2009: conform spec: MSH-6 leeg laten
      v_hl7_msg('MSH')(7)(1)(1) := '<HELIX_DATETIME>';
      v_hl7_msg('MSH')(8)(1)(1) := 'KGC';
      v_hl7_msg('MSH')(9)(1)(1) := 'ACK';
      v_hl7_msg('MSH')(10)(1)(1) := '<HELIX_CONTROL_ID>';
      v_hl7_msg('MSH')(11)(1)(1) := '<HELIX_PROC_ID>';
      -- Mantis 283 RDE 02-04-2009: ook versie flexibel maken
      v_hl7_msg('MSH')(12)(1)(1) := '<HELIX_VERSION_ID>'; -- g_sypa_hl7_versie;
      IF g_sypa_zis_locatie = 'NIJMEGEN'
      THEN
         -- alleen nijmegen:
         v_hl7_msg('MSH')(17)(1)(1) := 'NL';
      END IF;

      v_debug := '3';
      v_hl7_msg('MSA') := init_msg('MSA'
                                  ,g_sypa_hl7_versie);
dbms_output.put_line('HL7-v=' || g_sypa_hl7_versie || '; MSA-count: ' || v_hl7_msg('MSA').count);
      v_hl7_msg('MSA')(1)(1)(1) := '<HELIX_ACK_CODE>';
      v_hl7_msg('MSA')(2)(1)(1) := '<HELIX_CONTROL_ID>'; -- identiek aan msh10

      v_debug  := '4';
      v_msg_ok := g_sob_char || hl7seg2str('MSH'
                                          ,v_hl7_msg('MSH')) || g_cr ||
                  hl7seg2str('MSA'
                            ,v_hl7_msg('MSA')) || g_cr || g_eob_char || g_term_char;

      v_debug := '5';
      log(v_thisproc || ' wordt gestart (ZIS_LOCATIE: ' || g_sypa_zis_locatie || '; HL7_VERSIE: ' || g_sypa_hl7_versie ||
          '; ZIS_HL7_SVR_LISTEN_TO_MSG: ' || g_sypa_listen_to_msg || '; ZIS_HL7_SVR_POORT_LISTENER: ' ||
          to_char(v_hl7_svr_poort_lstnr));
      WHILE TRUE
      LOOP
         -- deze loop is nodig omdat Luister een timeout geeft - dan gewoon doorgaan!
         v_luister := kgc_sypa_00.systeem_waarde(p_parameter_code => 'ZIS_HL7_LISTENER');
         IF nvl(v_luister
               ,'XXX') <> 'AAN'
         THEN
            -- just to be sure!
            log(v_thisproc || ': beeindigd: ZIS_HL7_LISTENER staat niet op AAN!');
            RETURN;
         END IF;
         v_sysdate := to_char(SYSDATE
                             ,'dd-mm-yyyy hh24:mi:ss');
         -- doorgaan?
         v_return := moet_hl7_luister_stoppen;
         v_debug  := '7';
         IF v_return < 0
         THEN
            -- error
            kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                                 ,v_thisproc || ': Error met pipe (database: ' || upper(database_name) || ')!'
                                 ,v_sysdate);
            raise_application_error(-20000
                                   ,v_thisproc || ': Error receiving on pipe: ' || to_char(v_return));
         END IF;
         v_debug := '8';
         IF v_return > 0
         THEN
            -- iets in de pijp - stoppen!
            log(v_thisproc || ': beeindigd op verzoek gebruiker!');
            RETURN;
         END IF;
         --  log(v_thisproc || ' Start...');

         -- Ga luisteren naar hl7-berichten:
         v_debug        := '9';
         v_errormessage := luister(v_hl7_svr_poort_lstnr
                                  ,v_msg_ok
                                  ,g_sypa_listen_to_msg
                                  ,1000);
         IF nvl(length(v_errormessage)
               ,0) > 0
         THEN
            IF instr(v_errormessage
                    ,'10048') > 0
               AND instr(v_errormessage
                        ,'bind failed') > 0
            THEN
               -- poort bezet!
               v_errormessage := 'Poort ' || to_char(v_hl7_svr_poort_lstnr) || ' is in gebruik!' ||
                                 ' Mogelijk gebruikt een ander proces deze poort reeds!' || chr(10) || 'Foutmelding:' ||
                                 chr(10) || v_errormessage;
            END IF;
            v_errormessage := 'Dit HL7-luisterproces wordt gestopt: ' || chr(10) || v_errormessage;
            kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                                 ,v_thisproc || ' Error (database: ' || upper(database_name) || ')!'
                                 ,v_errormessage);
            EXIT;
         END IF;
         v_debug := '10';
         /*  v_loop := v_loop + 1;
             IF v_loop > 100 THEN -- test: 1 minuten!
               EXIT;
             END IF;
         */
      END LOOP;
      log(v_thisproc || ' wordt beeindigd!');
   EXCEPTION
      WHEN OTHERS THEN
         -- tbv commandline
         kgc_zis.log(v_thisproc || ': debug: ' || v_debug || '; Fout: ' || SQLERRM
                    ,'1100');
         -- offline in job
         kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                              ,v_thisproc || ': debug: ' || v_debug || '; Fout (database: ' || upper(database_name) || ')!'
                              ,SQLERRM);
   END;

   -- bekijk de inhoud van de pipe; retourneer -1 igv fout; anders: lengte inhoud pipe
   FUNCTION moet_hl7_luister_stoppen RETURN NUMBER -- -1: error; andere lengte inhoud pipe
    IS
      v_return   INTEGER := 0;
      v_pipe_inh VARCHAR2(32767);
   BEGIN
      v_return := dbms_pipe.receive_message('hl7_control'
                                           ,0); -- verwijderd het gelezene
      IF v_return > 1
      THEN
         -- 0 = succes; 1=timeout(=ok!)
         RETURN - 1;
      END IF;
      BEGIN
         dbms_pipe.unpack_message(v_pipe_inh);
      EXCEPTION
         WHEN OTHERS THEN
            NULL;
      END;
      IF upper(v_pipe_inh) = 'ALIVE?'
      THEN
         v_pipe_inh := NULL; -- tbv test of pijp gelezen wordt door bv hl7-listener
      END IF;
      RETURN nvl(length(v_pipe_inh)
                ,0);
   END;

   FUNCTION gethl7poort(p_sypa IN VARCHAR2) RETURN NUMBER IS
      v_sypa_val           kgc_systeem_parameters.standaard_waarde%TYPE;
      v_database_name      VARCHAR2(4000) := upper(database_name); -- sys-function
      v_sypa_database_name VARCHAR2(4000);
      v_pos_komma          NUMBER;
      v_poort              NUMBER;
      v_thisproc           VARCHAR2(100) := 'kgc_zis.getHL7Poort';
   BEGIN
      v_sypa_val := kgc_sypa_00.systeem_waarde(p_sypa);
      IF nvl(length(v_sypa_val)
            ,0) = 0
      THEN
         raise_application_error(-20000
                                ,v_thisproc || ' Error: Systeemparameter ' || p_sypa || ' is leeg of afwezig!');
      END IF;
      v_pos_komma := instr(v_sypa_val
                          ,',');
      IF v_pos_komma <= 0
      THEN
         raise_application_error(-20000
                                ,v_thisproc || ' Error: Systeemparameter ' || p_sypa || ' ("' || v_sypa_val ||
                                 '") bevat geen komma!');
      END IF;
      BEGIN
         v_poort := to_number(substr(v_sypa_val
                                    ,1
                                    ,v_pos_komma - 1));
      EXCEPTION
         WHEN OTHERS THEN
            raise_application_error(-20000
                                   ,v_thisproc || ' Error: Systeemparameter ' || p_sypa || ' ("' || v_sypa_val ||
                                    '") bevat voor de komma niet een numeriek poortnummer!');
      END;
      v_sypa_database_name := upper(substr(v_sypa_val
                                          ,v_pos_komma + 1));
      IF v_database_name <> nvl(v_sypa_database_name
                               ,'@#@')
      THEN
         raise_application_error(-20000
                                ,v_thisproc || ' Error: Systeemparameter ' || p_sypa || ' ("' || v_sypa_val ||
                                 '") eindigt niet met de databasenaam ("' || v_database_name || '")!');
      END IF;
      RETURN v_poort;
   END;

   FUNCTION verwerk_hl7_bericht(p_zoveelste    NUMBER
                               ,p_hlbe_id      NUMBER
                               ,p_bericht      VARCHAR2
                               ,x_bericht_type IN OUT VARCHAR2
                               ,x_opmerkingen  OUT kgc_hl7_berichten.opmerkingen%TYPE -- NtB: nieuw, resultaat van verwerking teruggeven
                               ,x_entiteit     IN OUT VARCHAR2
                               ,x_id           IN OUT NUMBER) RETURN VARCHAR2 IS

      CURSOR c_hlbe(p_hlbe_id NUMBER) IS
         SELECT hlbe.bericht
               ,hlbe.status
           FROM kgc_hl7_berichten hlbe
          WHERE hlbe_id = p_hlbe_id;

      CURSOR jobs_cur(p_zoekwhat VARCHAR2) IS
         SELECT *
           FROM all_jobs
          WHERE what LIKE '%' || p_zoekwhat || '%';

      jobs_rec         jobs_cur%ROWTYPE;
      v_ret_status     kgc_hl7_berichten.status%TYPE := 'N';
      v_bericht        kgc_hl7_berichten.bericht%TYPE := p_bericht;
      v_bericht2       kgc_hl7_berichten.bericht%TYPE;
      v_hl7_msg        hl7_msg;
      v_msh            VARCHAR2(10);
      v_ander_bericht  BOOLEAN := FALSE;
      v_repeatjob_id   VARCHAR2(100) := 'UNIQUEJOBHLBEID#' || to_char(p_hlbe_id);
      v_repeatjob_what VARCHAR2(4000);
      v_job            BINARY_INTEGER;
      v_debug          VARCHAR2(100);

      v_status   kgc_hl7_berichten.status%TYPE; -- mantis 740
      v_thisproc VARCHAR2(100) := 'kgc_zis.verwerk_hl7_bericht';

   BEGIN

      v_debug := '1';
      IF p_bericht IS NULL
      THEN
         -- eerst ophalen; kan igv niet-eerste poging!
         OPEN c_hlbe(p_hlbe_id);
         FETCH c_hlbe
            INTO v_bericht, v_status;
         IF c_hlbe%NOTFOUND
         THEN
            NULL; -- FOUT!
            v_debug := v_debug || 'a';
         END IF;
         CLOSE c_hlbe;
      END IF;

      v_debug   := '2';
      v_hl7_msg := str2hl7msg(p_hlbe_id
                             ,v_bericht);
      v_debug   := '3';

      IF v_hl7_msg.COUNT = 0
      THEN
         v_ret_status := 'X'; -- geen nieuwe poging, want fout!
      ELSE

         v_msh          := getsegmentcode('MSH'
                                         ,v_hl7_msg);
         x_bericht_type := v_hl7_msg(v_msh) (9) (1) (1) || '_' || v_hl7_msg(v_msh) (9) (1) (2);

         v_debug := '3' || x_bericht_type;

         IF v_hl7_msg(v_msh) (9) (1) (1) = 'ADT'
         THEN
            x_entiteit := 'PERS';
            IF v_hl7_msg(v_msh) (9) (1) (2) IN ('A08', 'A31') -- mantis 283 tbv Nijmegen
            THEN
               v_ret_status := verwerk_adt_a08_bericht(p_hlbe_id => p_hlbe_id
                                                      ,p_hl7_msg => v_hl7_msg
                                                      ,x_id      => x_id);
            ELSIF v_hl7_msg(v_msh) (9) (1) (2) = 'A24'
            THEN
               -- voor iedereen gelijk
               v_ret_status := verwerk_adt_a24_bericht(p_hlbe_id => p_hlbe_id
                                                      ,p_hl7_msg => v_hl7_msg
                                                      ,x_id      => x_id);
            ELSIF v_hl7_msg(v_msh) (9) (1) (2) = 'A37'
            THEN
               -- alleen nijmegen
               v_ret_status := verwerk_adt_a37_bericht(p_hlbe_id => p_hlbe_id
                                                      ,p_hl7_msg => v_hl7_msg
                                                      ,x_id      => x_id);
            ELSIF v_hl7_msg(v_msh) (9) (1) (2) = 'A40' -- mantis 283: HL7 v2.4 vervanger van A24
            THEN
               -- alleen utrecht
               v_ret_status := verwerk_adt_a40_bericht(p_hlbe_id => p_hlbe_id
                                                      ,p_hl7_msg => v_hl7_msg
                                                      ,x_id      => x_id);
            ELSE
               v_ander_bericht := TRUE;
               v_debug         := v_debug || 'a';
            END IF;

            -- Mantis 740 >>>
         ELSIF upper(x_bericht_type) = 'ORM_O01'
         THEN
            -- mosos bericht (her) verwerken
            -- geen bestand, dan  bestand aanmaken
            -- werwerken van im_tabellen vertalen naar helix (indien mogelijk)
            -- Verweken via de Job met status N en wel een bestand.
            -- werwerken van im_tabellen vertalen naar helix (indien mogelijk)
            kgc_interface_mosos.routeer_hl7_berichten(p_hlbe_id => p_hlbe_id
                                                     --,p_bericht  => p_bericht
                                                     ,p_hl7_msg     => v_hl7_msg -- NtB: ipv. p_bericht
                                                     ,x_id          => x_id
                                                     ,x_entiteit    => x_entiteit
                                                     ,x_status      => v_ret_status -- NtB: v_ret_status is default 'N' voor aanroep!!!
                                                     ,x_opmerkingen => x_opmerkingen); -- NtB: teruggeven resultaat
            -- <<< Einde Mantis 740

         ELSE
            --    v_ander_bericht := TRUE; -- NtB: overbodig
            -- END IF;
            -- IF v_ander_bericht
            -- THEN
            v_ret_status := verwerk_ander_bericht(p_hlbe_id  => p_hlbe_id
                                                 ,p_hl7_msg  => v_hl7_msg
                                                 ,x_entiteit => x_entiteit
                                                 ,x_id       => x_id);
         END IF;
      END IF;

      IF v_ret_status = 'N'
      THEN
         -- opnieuw opstarten!
         -- NtB: gerangschikt
         v_repeatjob_what := 'DECLARE' || --
                             '   /* id = ' || v_repeatjob_id || ' */ ' || --
                             '   v_st    varchar2(1);' || --
                             '   v_bt    kgc_hl7_berichten.bericht_type%type;' || --
                             '   v_opm   kgc_hl7_berichten.opmerkingen%type;' || -- NtB: nieuwe dummy
                             '   v_ent   kgc_hl7_berichten.entiteit%type;' || --
                             '   v_id    kgc_hl7_berichten.id%type;' || --
                             'BEGIN' || --
                             '   v_st := kgc_zis.verwerk_hl7_bericht ' || --
                             '           (p_zoveelste    => ' || to_char(p_zoveelste + 1) || --
                             '           ,p_hlbe_id      => ' || to_char(p_hlbe_id) || --
                             '           ,p_bericht      => null' || --
                             '           ,x_bericht_type => v_bt' || --
                             '           ,x_opmerkingen  => v_opm' || -- NtB: nieuw, resultaat van verwerking teruggeven
                             '           ,x_entiteit     => v_ent' || --
                             '           ,x_id           => v_id' || --
                             '           );' || --
                             '   commit;' || --
                             'END;';
         -- NtB: opletten!!! returngegevens worden hier niet vastgelegd,
         -- behalve de status bij weggooien job

         -- hoe te herkennen? in v_repeatjob_what is het hlbe_id verwerkt, dus uniek!
         OPEN jobs_cur(v_repeatjob_id);
         FETCH jobs_cur
            INTO jobs_rec;
         IF jobs_cur%NOTFOUND
         THEN
            -- niet aanwezig? toevoegen!
            dbms_job.submit(job       => v_job
                           ,what      => v_repeatjob_what
                           ,next_date => SYSDATE + 1 / (24 * 6)
                           ,INTERVAL  => 'SYSDATE + 1/(24*6)' -- 10 minuten
                            );
            dbms_output.put_line('Verwerking van bericht ' || to_char(p_hlbe_id) || ' wordt voor de ' ||
                                 to_char(p_zoveelste + 1) || 'e keer opgestart!');
         ELSE
            -- wijzigen en reactiveren; status wordt hierna getoond!
            dbms_job.change(job       => jobs_rec.job
                           ,what      => v_repeatjob_what
                           ,next_date => SYSDATE + 1 / (24 * 6)
                           ,INTERVAL  => jobs_rec.INTERVAL -- onveranderd
                            );
            dbms_output.put_line('Verwerking van bericht ' || to_char(p_hlbe_id) || ' wordt voor de ' ||
                                 to_char(p_zoveelste + 1) || 'e keer herstart!');
         END IF;
         CLOSE jobs_cur;

      ELSE
         -- status ongelijk N
         IF p_zoveelste = 1
         THEN
            NULL; -- de aanroepende trigger zet de status
         ELSE
            -- dan is er een job aanwezig die deze functie gestart heeft
            -- aangezien de status ongelijk N is: verwijder job!
            OPEN jobs_cur(v_repeatjob_id);
            FETCH jobs_cur
               INTO jobs_rec;
            IF jobs_cur%FOUND
            THEN
               -- anwezig? weg ermee!
               dbms_job.remove(jobs_rec.job);
            END IF;
            CLOSE jobs_cur;

            -- EN: er is geen aanroepende trigger die de status goed zet, dus: zelf doen!
            UPDATE kgc_hl7_berichten
               SET status      = v_ret_status
                  ,opmerkingen = x_opmerkingen -- NtB: nieuw
             WHERE hlbe_id = p_hlbe_id;
         END IF;

      END IF;

      RETURN v_ret_status;
   EXCEPTION
      WHEN OTHERS THEN
         kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                              ,v_thisproc || ': Fout (database: ' || upper(database_name) || ')!'
                              ,v_thisproc || ' Debug: ' || v_debug || '; Error: ' || SQLERRM);
         RETURN 'X'; -- niet verder verwerken
   END;

   FUNCTION send_msg(p_host      IN VARCHAR2
                    ,p_port      IN VARCHAR2
                    ,p_strhl7in  IN VARCHAR2
                    ,p_timeout   IN NUMBER -- tienden van seconden
                    ,x_strhl7out IN OUT VARCHAR2
                    ,x_msg       IN OUT VARCHAR2) RETURN BOOLEAN IS
      -- TCP/IP connection to HL7-communicatie-engine (Cloverleaf/TDM/...):
      v_conn           utl_tcp.connection;
      v_ret_val        BOOLEAN := TRUE;
      v_ret_conn       PLS_INTEGER;
      v_data_available PLS_INTEGER;
      v_nrcharrec      PLS_INTEGER;
      v_loop           NUMBER := 0;
      v_str            VARCHAR2(32000);
      v_strantw        VARCHAR2(32000);
      v_thisproc       VARCHAR2(100) := 'kgc_zis.send_msg';
   BEGIN
      -- open connectie
      log(v_thisproc || ': Connectie ' || p_host || ', poort ' || p_port || ' wordt geopend...');
      v_loop := 0;
      WHILE TRUE -- Mantis 2262 - 2008-07-16  RDE Loop toegevoegd
      LOOP
         v_loop := v_loop + 1;
         BEGIN
            v_conn := utl_tcp.open_connection(remote_host => p_host
                                             ,remote_port => p_port
                                             ,charset     => 'WE8MSWIN1252'
                                             ,tx_timeout  => 0.1 -- v3 - seconden! Zie doc: dit is niet de tijd voor wacht-op-connectie, maar wacht bij read!!! 0=nietwachten, null=waitforever
                                              --        , tx_timeout => 5 -- seconden!
                                              );
            EXIT;
         EXCEPTION
            WHEN OTHERS THEN
               IF SQLCODE = -29260
                  AND v_loop < p_timeout -- 2262
               THEN
                  -- network error: TNS:connection closed; geen fout! zolang
                  -- effe wachten () en doorgaan
                  dbms_lock.sleep(0.1); -- seconden! sys must grant execute on dbms_lock to ...;
               ELSE
                  x_msg := v_thisproc || ': Error: de connectie met server ' || p_host || ', poort ' || p_port ||
                           ' kon niet tot stand worden gebracht; Melding: ' || SQLERRM;
                  log(x_msg);
                  utl_tcp.close_all_connections;
                  RETURN FALSE;
               END IF;
         END;
      END LOOP;
      log(v_thisproc || ': Connectie is geopend; message wordt verzonden...');
      v_ret_conn := utl_tcp.write_raw(v_conn
                                     ,utl_raw.cast_to_raw(p_strhl7in)); -- send request
      IF v_ret_conn = 0
      THEN
         -- aantal verzonden bytes
         x_msg := v_thisproc || ': Error: de connectie met server ' || p_host || ', poort ' || p_port ||
                  ' is OK, maar er kon geen bericht verzonden worden!';
         log(x_msg);
         utl_tcp.close_all_connections;
         RETURN FALSE;
      END IF;

      log(v_thisproc || ': Message is verzonden; wacht op antwoord...');
      v_loop := 0;
      WHILE TRUE
      LOOP
         v_loop           := v_loop + 1;
         v_data_available := utl_tcp.available(v_conn);
         -- terugkomende gegevens ophalen:
         IF v_data_available > 0
         THEN
            BEGIN
               LOOP
                  IF v_data_available = 0
                  THEN
                     EXIT;
                  END IF;
                  v_nrcharrec      := utl_tcp.read_text(v_conn
                                                       ,v_str
                                                       ,1024); -- zie doc:'On some platforms, this function (= available) may only return 1, to indicate that some data is available'
                  v_strantw        := v_strantw || v_str;
                  v_data_available := utl_tcp.available(v_conn); -- meer?
               END LOOP;
            EXCEPTION
               WHEN utl_tcp.end_of_input THEN
                  NULL; -- end of input; geen fout!
               WHEN OTHERS THEN
                  IF SQLCODE = -29260
                  THEN
                     NULL; -- network error: TNS:connection closed; geen fout!
                  ELSE
                     RAISE;
                  END IF;
            END;
            -- gegevens gelezen? => klaar!
            log(v_thisproc || ': Antwoord ingelezen!');
            EXIT;
         END IF;
         IF v_loop < p_timeout
         THEN
            -- effe wachten () en doorgaan
            dbms_lock.sleep(0.1); -- seconden! sys must grant execute on dbms_lock to ...;
         ELSE
            -- timeout!!!
            x_msg := 'Timeout na ' || to_char(p_timeout / 10) || ' seconden!';
            log(v_thisproc || ': ' || x_msg);
            v_ret_val := FALSE;
            EXIT;
         END IF;
      END LOOP;
      utl_tcp.close_all_connections;
      log(v_thisproc || ': Connectie dicht...');
      IF nvl(length(v_strantw)
            ,0) = 0
      THEN
         x_msg     := 'Lege reactie ontvangen!';
         v_ret_val := FALSE;
      END IF;
      x_strhl7out := v_strantw;
      RETURN v_ret_val;
   EXCEPTION
      WHEN OTHERS THEN
         -- todo: connectie errors + info!
         x_msg := v_thisproc || ': Error: ' || SQLERRM;
         log(x_msg);
         utl_tcp.close_all_connections;
         RETURN FALSE;
   END;

   -- splitsName: splitst "Berg, van den" in 2 stukken
   -- helaas is zowel Astraia als ZIS een chaos, dus ook proberen op andere wijzen
   PROCEDURE splitsname(p_strname        IN VARCHAR2
                       ,x_strachternaam  IN OUT VARCHAR2
                       ,x_strvoorvoegsel IN OUT VARCHAR2) IS
      v_doorgaan    BOOLEAN := TRUE;
      v_vvvooraan   BOOLEAN := FALSE; -- of aan de voorkant alles eraf, of aan de achterkant. niet beide
      v_vvachteraan BOOLEAN := FALSE;
      v_pos         NUMBER;
      v_posrev      NUMBER;
      v_strsep      VARCHAR2(2);
      v_strname     VARCHAR2(2000);
      v_strvv       VARCHAR2(2000);
      v_strallevv   VARCHAR2(2000) := ' van het ter de den der in ''t aan v.d. v. d. vd v d bij op ten over uit voor tn tr dn vt '; -- rde 26-08-2009 4 toevoegingen; Mantis 3185
      -- niet alles, maar toch zeker 95%. Spaties zijn tbv zoeken
   BEGIN
      x_strachternaam  := NULL;
      x_strvoorvoegsel := NULL;
      v_pos            := instr(p_strname
                               ,',');
      IF v_pos > 0
      THEN
         x_strachternaam  := rtrim(substr(p_strname
                                         ,1
                                         ,v_pos - 1));
         x_strvoorvoegsel := ltrim(substr(p_strname
--                                         ,v_pos + 1));
                                         ,v_pos + 1, 10)); -- begrensd op 10! rde 26-8-2009. Mantis 3185
      ELSE
         x_strachternaam := p_strname;
         WHILE v_doorgaan
         LOOP
            v_doorgaan      := FALSE;
            x_strachternaam := ltrim(rtrim(x_strachternaam)); -- 2 spaties kan!
            v_strname       := lower(x_strachternaam);
            v_pos           := instr(v_strname
                                    ,' ');
            v_posrev        := instr(v_strname
                                    ,' '
                                    ,-1);
            -- vv aan het begin?
            IF v_pos > 0
               AND NOT v_vvachteraan
            THEN
               v_strvv := substr(v_strname
                                ,1
                                ,v_pos); -- incl spatie!
               IF instr(v_strallevv
                       ,v_strvv) > 0
               THEN
                  -- hebbes
                  x_strvoorvoegsel := x_strvoorvoegsel || v_strsep || rtrim(v_strvv);
                  v_strsep         := ' ';
                  x_strachternaam  := substr(x_strachternaam
                                            ,v_pos + 1);
                  v_doorgaan       := TRUE;
                  v_vvvooraan      := TRUE;
               END IF;
            END IF;
            -- vv aan het eind EN nog niets vervangen
            IF v_posrev > 0
               AND NOT v_vvvooraan
               AND NOT v_doorgaan
            THEN
               v_strvv := substr(v_strname
                                ,v_posrev); -- incl spatie!
               IF instr(v_strallevv
                       ,v_strvv) > 0
               THEN
                  -- hebbes
                  x_strvoorvoegsel := ltrim(v_strvv) || v_strsep || x_strvoorvoegsel;
                  v_strsep         := ' ';
                  x_strachternaam  := substr(x_strachternaam
                                            ,1
                                            ,v_posrev - 1);
                  v_doorgaan       := TRUE;
                  v_vvachteraan    := TRUE;
               END IF;
            END IF;
         END LOOP;
      END IF;
   END; -- splitsName

   ----------------------------------------------------------------------------
   FUNCTION verwerk_adt_a08_bericht -- wijzig patient adhv een ADT A08 OF A31 bericht
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2 IS
      ----------------------------------------------------------------------------
      v_van VARCHAR2(100) := kgc_sypa_00.systeem_waarde('ZIS_MAIL_AFZENDER');
      CURSOR c_sypa(p_parameter_code VARCHAR2) IS
         SELECT *
           FROM kgc_systeem_parameters
          WHERE code = upper(p_parameter_code);
      r_sypa c_sypa%ROWTYPE;
      CURSOR c_spwa(p_sypa_id NUMBER) IS
         SELECT mede.email
               ,mede.mede_id
               ,spwa.waarde
           FROM kgc_systeem_par_waarden spwa
               ,kgc_medewerkers         mede
          WHERE spwa.sypa_id = p_sypa_id
            AND mede.mede_id = spwa.mede_id
            AND mede.email IS NOT NULL -- rde 12-05-2006
         ;
      CURSOR c_pers(p_zisnr VARCHAR2) IS
         SELECT pers.pers_id
               ,beheer_in_zis
           FROM kgc_personen pers
          WHERE pers.zisnr = p_zisnr;
      CURSOR c_hlbe_oud(p_pers_id NUMBER) IS
         SELECT COUNT(*)
           FROM kgc_hl7_berichten hlbe
          WHERE hlbe.entiteit = 'PERS'
            AND hlbe.bericht_type IN ('ADT_A08', 'ADT_A24', 'ADT_A31', 'ADT_A37') -- mantis 283
            AND hlbe.status = 'N'
            AND hlbe.id = p_pers_id
            AND hlbe.hlbe_id < p_hlbe_id -- dwz: ouder!
         ;
      CURSOR c_pers_upd(p_pers_id NUMBER) IS
         SELECT pers.pers_id
           FROM kgc_personen pers
          WHERE pers.pers_id = p_pers_id
            FOR UPDATE NOWAIT;
      r_pers_upd c_pers_upd%ROWTYPE;
      CURSOR c_onde_mede(p_mede_id NUMBER, p_pers_id NUMBER) IS
         SELECT kafd.code kafd_code
               ,ongr.code ongr_code
               ,onde.onderzoeknr
               ,onde.datum_binnen
           FROM kgc_onderzoeken       onde
               ,kgc_kgc_afdelingen    kafd
               ,kgc_onderzoeksgroepen ongr
               ,kgc_mede_kafd         meka
          WHERE onde.pers_id = p_pers_id
            AND onde.kafd_id = kafd.kafd_id
            AND onde.ongr_id = ongr.ongr_id
            AND onde.kafd_id = meka.kafd_id
            AND meka.mede_id = p_mede_id;
      r_onde_mede c_onde_mede%ROWTYPE;
      CURSOR c_onde_mede_naf(p_mede_id NUMBER, p_pers_id NUMBER) IS
         SELECT kafd.code kafd_code
               ,ongr.code ongr_code
               ,onde.onderzoeknr
               ,onde.datum_binnen
           FROM kgc_onderzoeken       onde
               ,kgc_kgc_afdelingen    kafd
               ,kgc_onderzoeksgroepen ongr
               ,kgc_mede_kafd         meka
          WHERE onde.pers_id = p_pers_id
            AND onde.kafd_id = kafd.kafd_id
            AND onde.ongr_id = ongr.ongr_id
            AND onde.kafd_id = meka.kafd_id
            AND meka.mede_id = p_mede_id
            AND onde.afgerond <> 'J';
      r_onde_mede_naf  c_onde_mede_naf%ROWTYPE;
      v_zisnr          kgc_personen.zisnr%TYPE;
      v_adt_msgtype    VARCHAR2(100);
      v_pers_trow      cg$kgc_personen.cg$row_type;
      pers_trow        cg$kgc_personen.cg$row_type;
      rela_trow        cg$kgc_relaties.cg$row_type;
      verz_trow        cg$kgc_verzekeraars.cg$row_type;
      v_found          BOOLEAN := TRUE;
      v_mail_msg       VARCHAR2(32767);
      v_count          NUMBER;
      v_sysdate        VARCHAR2(20) := to_char(SYSDATE
                                              ,'dd-mm-yyyy hh24:mi:ss');
      v_pid            VARCHAR2(10);
      v_msh            VARCHAR2(10);
      v_send           BOOLEAN;
      v_beheer_in_zis  VARCHAR2(1) := 'J';
      v_error_msg      VARCHAR2(32767);
      v_update_mislukt BOOLEAN := FALSE;
      v_stremail       VARCHAR2(32767);
      v_stremail_ps    VARCHAR2(32767);
      v_onderwerp      VARCHAR2(2000);
      v_strconditie    VARCHAR2(32767);
      v_sypa_rcg_val   kgc_systeem_parameters.standaard_waarde%TYPE;
      v_ret_status     kgc_hl7_berichten.status%TYPE := 'N';
      v_debug          VARCHAR2(100);
      v_thisproc       VARCHAR2(100) := 'kgc_zis.verwerk_ADT_A08_bericht';
   BEGIN
      -- A08 Nijmegen: MSH EVN PID [ZPI] [PV1]
      -- A08 Utr/Maas: MSH EVN PID ZPI PV1 { IN1 [IN2] } STF
      v_debug := '1';
      kgc_zis.log(v_thisproc || ' Bericht ' || to_char(p_hlbe_id) || ' wordt verwerkt!');
      g_sypa_zis_locatie := get_sypa_sywa('ZIS_LOCATIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_zis_locatie);
      g_sypa_hl7_versie  := get_sypa_sywa('HL7_VERSIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_hl7_versie);

      kgc_zis.log(v_thisproc || ': ZIS_LOCATIE: ' || g_sypa_zis_locatie || '; HL7_VERSIE: ' || g_sypa_hl7_versie);
      -- algemeen:
      v_debug := '2';
      v_pid   := getsegmentcode('PID'
                               ,p_hl7_msg);
      v_zisnr := p_hl7_msg(v_pid) (3) (1) (1);
      v_msh   := getsegmentcode('MSH'
                               ,p_hl7_msg);
      v_adt_msgtype := p_hl7_msg(v_msh) (9) (1) (2);

      IF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         v_zisnr := formatteer_zisnr_ut(v_zisnr);
      END IF;
      -- bestaat persoon?
      v_debug := '3';
      OPEN c_pers(v_zisnr);
      FETCH c_pers
         INTO x_id, v_beheer_in_zis;
      IF c_pers%NOTFOUND
      THEN
         v_found := FALSE;
      END IF;
      CLOSE c_pers;
      v_debug := '4';
      IF NOT v_found
      THEN
         kgc_zis.log(v_thisproc || ': Pers met zisnr niet gevonden: ' || v_zisnr || ' (dus: geen actie!)');
         RETURN 'V'; -- niet in Helix? geen actie = return V = verwerkt!
      END IF;
      kgc_zis.log(v_thisproc || ': Pers met zisnr gevonden: ' || v_zisnr);
      -- wel aanwezig!

      -- check of oude, nietverwerkte versies bestaan
      v_debug := '5';
      OPEN c_hlbe_oud(x_id);
      FETCH c_hlbe_oud
         INTO v_count;
      CLOSE c_hlbe_oud;
      v_debug := '5a';
      IF v_count > 0
      THEN
         -- er staan nog oudere te wachten!
         RETURN 'N';
      END IF;

      -- check beheerd in ZIS: indien N, dan geen actie!
      v_debug := '6';
      IF v_beheer_in_zis = 'N'
      THEN
         RETURN 'V';
      END IF;

      v_debug             := '7';
      v_pers_trow.pers_id := x_id;
      cg$kgc_personen.slct(v_pers_trow); -- bestaande gegevens ophalen
      v_pers_trow.zisnr := v_zisnr;
      pers_trow         := v_pers_trow; -- startpunt is de bestaande situatie!

      -- het vertalen van het bericht naar Helix is instelling-specifiek:
      v_debug := '8';
      IF g_sypa_zis_locatie = 'NIJMEGEN'
      THEN
         v_debug := v_debug || 'N';
         kgc_zis_nijmegen.hl7msg2rec(p_hl7_msg => p_hl7_msg
                                    ,pers_trow => pers_trow
                                    ,rela_trow => rela_trow
                                    ,verz_trow => verz_trow);
      ELSIF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         v_debug := v_debug || 'U';
         kgc_zis_utrecht.hl7msg2rec(p_hl7_msg => p_hl7_msg
                                   ,pers_trow => pers_trow
                                   ,rela_trow => rela_trow
                                   ,verz_trow => verz_trow);
         IF v_pers_trow.bsn IS NOT NULL AND pers_trow.bsn IS NULL THEN  -- mantis 283
           pers_trow.bsn := v_pers_trow.bsn; -- en geverifieerd staat al goed (N).
         END IF;
      ELSIF g_sypa_zis_locatie = 'MAASTRICHT'
      THEN
         v_debug := v_debug || 'M';
         null;
      ELSE
         raise_application_error(-20000
                                ,v_thisproc || ' Error: Onbekende waarde voor systeemparameter ZIS_LOCATIE: "' ||
                                 g_sypa_zis_locatie || '"!');
      END IF;

      -- insert / update
      -- *** RELATIE = HUISARTS
      v_debug := '10';
      IF rela_trow.achternaam IS NOT NULL
      THEN
         -- toch wel de minste voorwaarde!
         IF rela_trow.rela_id IS NULL
         THEN
            -- automatisch code uitgeven?
            IF rela_trow.code IS NULL
            THEN
               -- altijd ZIS0123456 aanbrengen! rde 6 juni 2006
               SELECT kgc_rela_seq.NEXTVAL
                 INTO rela_trow.rela_id
                 FROM dual;
               rela_trow.code := 'ZIS' || lpad(to_char(rela_trow.rela_id)
                                              ,7
                                              ,'0');
            END IF;
            IF rela_trow.code IS NOT NULL
            THEN
               -- toevoegen!
               rela_trow.relatie_type := 'HA'; -- helix-eigen; wordt niet aangeleverd!
               cg$kgc_relaties.ins(rela_trow
                                  ,cg$kgc_relaties.cg$ind_true);
               pers_trow.rela_id := rela_trow.rela_id;
            END IF;
         ELSE
            NULL; -- geen actie: mogelijk zijn de aangeleverde gegevens niet OK; rde 6 juni 2006
            -- evt later! cg$kgc_relaties.upd(rela_trow, cg$kgc_relaties.cg$ind_true);
         END IF;
      END IF;

      -- *** VERZEKERAARS
      v_debug := '11';
      IF verz_trow.naam IS NOT NULL
      THEN
         -- toch wel de minste voorwaarde!
         IF verz_trow.verz_id IS NULL
         THEN
            cg$kgc_verzekeraars.ins(verz_trow
                                   ,cg$kgc_verzekeraars.cg$ind_true);
            pers_trow.verz_id := verz_trow.verz_id;
         ELSE
            NULL; -- geen actie: mogelijk zijn de aangeleverde gegevens niet OK; rde 6 juni 2006
            -- evt later! cg$kgc_verzekeraars.upd(verz_trow, cg$kgc_verzekeraars.cg$ind_true);
         END IF;
      END IF;

      -- *** PERSONEN
      -- bepaal tekst wijziging tbv mail
      -- bepaal gewijzigde attributen tbv conditie
      -- bepaal sql?
      v_debug := '12';
      bepaal_pers_verschil(v_pers_trow
                          ,pers_trow
                          ,rela_trow
                          ,verz_trow
                          ,v_stremail
                          ,v_strconditie);

      v_debug := '13';
      -- zijn er wijzigingen?
      IF nvl(length(v_stremail)
            ,0) = 0
      THEN
         RETURN 'V'; -- dan valt er niets te melden! => Verwerkt!
      END IF;

      -- mail opbouwen
      v_mail_msg := '<html>' || g_html_mail_head || '<body>' || kgc_zis.g_cr;
      v_mail_msg := v_mail_msg || '<p>Er is een ADT-' || v_adt_msgtype || '-bericht ontvangen van ZIS in Helix-database ' ||
                    upper(database_name) || ' op ' || v_sysdate || kgc_zis.g_cr || '<p><p>Het betreft ZISnummer <b>' ||
                    v_pers_trow.zisnr || '</b>, ' || v_pers_trow.aanspreken || ', ' || v_pers_trow.geslacht || ', ' ||
                    to_char(v_pers_trow.geboortedatum
                           ,'dd-mm-yyyy') || '.';

      -- blokkeren?
      v_debug := '9';
      IF wijziging_geblokkeerd(v_pers_trow
                              ,pers_trow)
      THEN
         kgc_zis.log(v_thisproc || ': Wijziging wordt geblokkeerd! (ZISnr ' || v_pers_trow.zisnr || ')');

         v_mail_msg := v_mail_msg || kgc_zis.g_cr ||
                       '<p>Deze wijziging is <b>geblokkeerd</b> (= niet in Helix doorgevoerd)!' ||
                       '<p>Niet doorgevoerde wijzigingen:<br>' || v_stremail;

         v_mail_msg := v_mail_msg || kgc_zis.g_cr || kgc_zis.g_cr || '<p><p>Succes, <p>' || v_thisproc;
         v_mail_msg := v_mail_msg || '</body></html>';

         kgc_zis.post_mail_zis('ZIS_MAIL_ADTA08_BLOK'
                              ,'Helix: Bericht ADT-' || v_adt_msgtype || ' is geblokkeerd van ZIS (ZISnr ' || v_pers_trow.zisnr || ' / ' ||
                               upper(database_name) || ' / ' || v_sysdate || ')'
                              ,v_mail_msg
                              ,TRUE);
         RETURN 'X';
      END IF;

      -- check lock / ...
      BEGIN
         OPEN c_pers_upd(v_pers_trow.pers_id); -- nu wordt de lock gezet!
         CLOSE c_pers_upd; -- geen actie, want deze komt zometeen
         v_debug := v_debug || 'g';
      EXCEPTION
         WHEN OTHERS THEN
            IF SQLCODE = -54
            THEN
               -- resource busy and acquire with NOWAIT specified
               v_debug := v_debug || 'h';
               kgc_zis.log(v_thisproc || ': Pers met zisnr ' || v_pers_trow.zisnr || ' is gelocked!');
               RETURN 'N';
            END IF;
            RAISE; -- iets anders? Kan niet!
      END;
      v_debug := 'A';
      -- ************************** de update:
      DECLARE
         CURSOR c_pers_jn IS
            SELECT ROWID
              FROM kgc_personen_jn persjn
             WHERE persjn.pers_id = pers_trow.pers_id
               AND persjn.jn_operation = 'UPD'
             ORDER BY persjn.jn_date_time DESC -- de meest recente eerst
            ;
         v_persjn_rowid         ROWID;
         v_pers_last_updated_by kgc_personen.last_updated_by%TYPE := pers_trow.last_updated_by;
      BEGIN
         -- cg$kgc_personen.upd(pers_trow, cg$kgc_personen.cg$ind_true);
         -- nieuw: mantis 503
         UPDATE kgc_personen pers
            SET achternaam          = pers_trow.achternaam
               ,gehuwd              = pers_trow.gehuwd
               ,geslacht            = pers_trow.geslacht
               ,bloedverwant        = pers_trow.bloedverwant
               ,meerling            = pers_trow.meerling
               ,overleden           = pers_trow.overleden
               ,default_aanspreken  = pers_trow.default_aanspreken
               ,zoeknaam            = pers_trow.zoeknaam
               ,beheer_in_zis       = pers_trow.beheer_in_zis
               ,created_by          = pers_trow.created_by
               ,creation_date       = pers_trow.creation_date
               ,last_updated_by     = pers_trow.last_updated_by
               ,last_update_date    = pers_trow.last_update_date
               ,rela_id             = pers_trow.rela_id
               ,verz_id             = pers_trow.verz_id
               ,zisnr               = pers_trow.zisnr
               ,aanspreken          = pers_trow.aanspreken
               ,geboortedatum       = pers_trow.geboortedatum
               ,part_geboortedatum  = pers_trow.part_geboortedatum
               ,voorletters         = pers_trow.voorletters
               ,voorvoegsel         = pers_trow.voorvoegsel
               ,achternaam_partner  = pers_trow.achternaam_partner
               ,voorvoegsel_partner = pers_trow.voorvoegsel_partner
               ,adres               = pers_trow.adres
               ,postcode            = pers_trow.postcode
               ,woonplaats          = pers_trow.woonplaats
               ,provincie           = pers_trow.provincie
               ,land                = pers_trow.land
               ,telefoon            = pers_trow.telefoon
               ,telefoon2           = pers_trow.telefoon2
               ,telefax             = pers_trow.telefax
               ,email               = pers_trow.email
               ,verzekeringswijze   = pers_trow.verzekeringswijze
               ,verzekeringsnr      = pers_trow.verzekeringsnr
               ,hoeveelling         = pers_trow.hoeveelling
               ,overlijdensdatum    = pers_trow.overlijdensdatum
               ,zoekfamilie         = pers_trow.zoekfamilie
               ,bsn                 = pers_trow.bsn -- mantis 283
               ,bsn_geverifieerd    = pers_trow.bsn_geverifieerd
          WHERE pers.pers_id = pers_trow.pers_id
         RETURNING last_updated_by INTO pers_trow.last_updated_by;
         -- v4 13-3-2007: toevoeging returning: het resultaat na triggers!

         IF pers_trow.last_updated_by <> v_pers_last_updated_by
         THEN
            -- overruled door cg$KGC_PERSONEN.up_autogen_columns
            -- in journal zetten!
            OPEN c_pers_jn;
            FETCH c_pers_jn
               INTO v_persjn_rowid;
            CLOSE c_pers_jn;
            IF v_persjn_rowid IS NOT NULL
            THEN
               UPDATE kgc_personen_jn
                  SET jn_user = v_pers_last_updated_by -- deze wordt namelijk getoond in kgcpers20
                WHERE ROWID = v_persjn_rowid;
            END IF;
         END IF;
      EXCEPTION
         WHEN OTHERS THEN
            v_debug     := v_debug || '1';
            v_error_msg := cg$errors.geterrors; -- verwijdert de error!
            v_debug     := v_debug || 'a';
            IF nvl(length(v_error_msg)
                  ,0) = 0
            THEN
               v_debug     := v_debug || 'b';
               v_error_msg := v_thisproc || ': Error: ' || SQLERRM;
            END IF;
            v_debug := v_debug || '2';
            kgc_zis.log(v_thisproc || ': Fout bij wijziging (ZISnr ' || v_pers_trow.zisnr || '): ' || v_error_msg);
            -- op 'niet beheerd' zetten
            v_pers_trow.beheer_in_zis := 'N'; -- enige wijziging...

            v_debug := v_debug || '3';
            -- cg$kgc_personen.upd(v_pers_trow, cg$kgc_personen.cg$ind_true);
            -- nieuw: mantis 503
            UPDATE kgc_personen pers
               SET beheer_in_zis = v_pers_trow.beheer_in_zis
             WHERE pers.pers_id = pers_trow.pers_id;

            v_debug    := v_debug || '4';
            v_mail_msg := v_mail_msg || kgc_zis.g_cr ||
                          '<p><font size=4>Er is <b>een fout</b> opgetreden bij het doorvoeren van deze wijziging!</font>' ||
                          '<p>Foutboodschap: ' || v_error_msg ||
                          '<p>Niet doorgevoerde wijzigingen (die de oorzaak vormen voor de fout):<br>' || v_stremail ||
                          '<p><b>De status ''beheerd in ZIS'' van deze persoon is op NEE gezet!</b> ' ||
                          ' Wijzigingen voor deze persoon uit ZIS worden niet meer doorgevoerd totdat' ||
                          ' deze status handmatig op JA gezet wordt!';

            v_debug    := v_debug || '5';
            v_mail_msg := v_mail_msg || kgc_zis.g_cr || kgc_zis.g_cr || '<p><p>Succes, <p>' || v_thisproc;
            v_mail_msg := v_mail_msg || '</body></html>';
            -- mail gaat naar alle geinteresseerden! (zie verderop)
            v_update_mislukt := TRUE;
      END;

      v_debug := 'B';
      IF NOT v_update_mislukt
      THEN
         -- verslag - v_mail_msg head is reeds gevuld!
         v_mail_msg := v_mail_msg || kgc_zis.g_cr || '<p>De volgende gegevens zijn gewijzigd:<br>' || v_stremail;

         -- onderzoeken:
         v_mail_msg := v_mail_msg || getondehtml(v_pers_trow.pers_id
                                                ,v_pers_trow.zisnr);
         -- eind
         v_mail_msg := v_mail_msg || kgc_zis.g_cr || kgc_zis.g_cr || '<p><p>Succes, <p>Helix - ' || v_thisproc;
      END IF;

      v_debug := 'C';
      -- de lastigste: per gebruiker kijken of ze in aanmerking komen!
      -- sypa ophalen
      OPEN c_sypa('ZIS_MAIL_ADTA08_INFO');
      FETCH c_sypa
         INTO r_sypa;
      IF c_sypa%NOTFOUND
      THEN
         CLOSE c_sypa;
         raise_application_error(-20000
                                ,'Er is geen Systeemparameter "ZIS_MAIL_ADTA08_INFO" gevonden!');
      END IF;
      CLOSE c_sypa;
      v_debug := 'D';
      BEGIN
         FOR r_spwa IN c_spwa(r_sypa.sypa_id)
         LOOP
            -- waarde = A => altijd mail; O => Er is een onderzoek van de gebr-afd;
            -- L => Er is een lopend onderzoek van de gebr-afd;
            v_send        := FALSE;
            v_debug       := 'E';
            v_stremail_ps := '<p><font size=1>PS. U ontvangt deze mail omdat u in Helix geregistreerd staat bij systeemparameter ZIS_MAIL_ADTA08_INFO als geinteresseerde voor ';
            IF r_spwa.waarde = 'A'
            THEN
               v_debug       := v_debug || '1';
               v_send        := TRUE;
               v_stremail_ps := v_stremail_ps || 'alle wijzigingen (A).';
            ELSIF r_spwa.waarde = 'O'
            THEN
               -- zoek onderzoek bij gebruiker
               v_debug := v_debug || '2';
               OPEN c_onde_mede(r_spwa.mede_id
                               ,v_pers_trow.pers_id);
               FETCH c_onde_mede
                  INTO r_onde_mede;
               IF c_onde_mede%FOUND
               THEN
                  v_send        := TRUE;
                  v_stremail_ps := v_stremail_ps ||
                                   'alle wijzigingen van Helix-personen die een onderzoek hebben bij uw afdeling (O; b.v. ' ||
                                   r_onde_mede.kafd_code || ' / ' || r_onde_mede.ongr_code || ' / ' ||
                                   r_onde_mede.onderzoeknr || ')';
               END IF;
               CLOSE c_onde_mede;
            ELSIF r_spwa.waarde = 'L'
            THEN
               -- zoek onderzoek bij gebruiker
               v_debug := v_debug || '3';
               OPEN c_onde_mede_naf(r_spwa.mede_id
                                   ,v_pers_trow.pers_id);
               FETCH c_onde_mede_naf
                  INTO r_onde_mede_naf;
               IF c_onde_mede_naf%FOUND
               THEN
                  v_send        := TRUE;
                  v_stremail_ps := v_stremail_ps ||
                                   'alle wijzigingen Helix-personen die een niet-afgerond onderzoek hebben bij uw afdeling (L; b.v. ' ||
                                   r_onde_mede_naf.kafd_code || ' / ' || r_onde_mede_naf.ongr_code || ' / ' ||
                                   r_onde_mede_naf.onderzoeknr || ')';
               END IF;
               CLOSE c_onde_mede_naf;
            END IF;

            v_debug := v_debug || '4';
            IF v_send
            THEN
               v_debug := v_debug || '5';
               IF v_update_mislukt
               THEN
                  v_onderwerp := 'Helix: ZIS-Wijziging (ADT-' || v_adt_msgtype || ') ontvangen maar NIET verwerkt (' ||
                                 upper(database_name) || ' / ' || v_sysdate || ')';
               ELSE
                  v_onderwerp := 'Helix: ZIS-Wijziging (ADT-' || v_adt_msgtype || ') ontvangen en verwerkt (' || upper(database_name) ||
                                 ' / ' || v_sysdate || ')';
               END IF;
               v_stremail_ps := v_stremail_ps || '</font>';
               v_debug       := v_debug || '6';
               kgc_mail.post_mail(p_van       => v_van
                                 ,p_aan       => r_spwa.email
                                 ,p_onderwerp => v_onderwerp
                                 ,p_inhoud    => v_mail_msg || v_stremail_ps || '</body></html>'
                                 ,p_html      => TRUE);
            END IF;
         END LOOP;
      EXCEPTION
         WHEN OTHERS THEN
            -- probleem met de mail
            kgc_zis.log(p_tekst     => v_thisproc || ': De Email voor bericht ADT^' || v_adt_msgtype || ' - ' || to_char(p_hlbe_id) ||
                                       ' kon niet verzonden worden! ZISnr: ' || v_pers_trow.zisnr || '; Error: ' ||
                                       SQLERRM
                       ,p_log_level => NULL
                       ,p_type      => 'E');
            RETURN 'N'; -- later!!!
      END;

      IF v_update_mislukt
      THEN
         RETURN 'X';
      END IF;
      RETURN 'V'; -- verwerkt!
   EXCEPTION
      WHEN OTHERS THEN
         kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                              ,v_thisproc || ': Fout (database: ' || upper(database_name) || ')!'
                              ,'Debug: ' || v_debug || '; SQLErrm: ' || SQLERRM);
         RETURN 'X';
   END;

   ----------------------------------------------------------------------------
   FUNCTION verwerk_adt_a24_bericht -- link patient: pid_2.zisnr wordt pid.zisnr
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2 IS
      ----------------------------------------------------------------------------
      CURSOR c_pers(p_zisnr VARCHAR2) IS
         SELECT pers.pers_id
               ,pers.aanspreken
               ,pers.geboortedatum
               ,pers.geslacht
               ,pers.beheer_in_zis
           FROM kgc_personen pers
          WHERE pers.zisnr = p_zisnr;
      r_pers_pref  c_pers%ROWTYPE;
      r_pers_syn   c_pers%ROWTYPE;
      v_ret_status kgc_hl7_berichten.status%TYPE := 'N';
      v_zisnr_pref kgc_personen.zisnr%TYPE;
      v_zisnr_syn  kgc_personen.zisnr%TYPE;
      v_found_pref BOOLEAN := TRUE;
      v_found_syn  BOOLEAN := TRUE;
      v_mail_msg   VARCHAR2(32767);
      v_count      NUMBER;
      v_sysdate    VARCHAR2(20) := to_char(SYSDATE
                                          ,'dd-mm-yyyy hh24:mi:ss');
      v_pid_1      VARCHAR2(10);
      v_pid_2      VARCHAR2(10);
      v_debug      VARCHAR2(100);
      v_thisproc   VARCHAR2(100) := 'kgc_zis.verwerk_ADT_A24_bericht';
   BEGIN
      -- A24: MSH EVN PID PID
      v_debug := '1';
      kgc_zis.log(v_thisproc || ' Bericht ' || to_char(p_hlbe_id) || ' wordt verwerkt!');
      g_sypa_zis_locatie := get_sypa_sywa('ZIS_LOCATIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_zis_locatie);
      g_sypa_hl7_versie  := get_sypa_sywa('HL7_VERSIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_hl7_versie);
      --  kgc_zis.log(v_thisproc || ' Bericht seg#: '||to_char(p_hl7_msg.COUNT));
      v_pid_1 := getsegmentcode('PID'
                               ,p_hl7_msg);
      v_pid_2 := getsegmentcode('PID'
                               ,p_hl7_msg
                               ,v_pid_1);
      -- compleet?
      IF nvl(length(v_pid_1)
            ,0) = 0
         OR nvl(length(v_pid_2)
               ,0) = 0
      THEN
         kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                              ,v_thisproc || ': Fout (database: ' || upper(database_name) || ')!'
                              ,'Bericht (kgc_hl7_berichten) met id = ' || to_char(p_hlbe_id) || ' is niet compleet! (' ||
                               v_pid_1 || '/' || v_pid_2 || ')');
         RETURN 'X';
      END IF;

      -- eerst ZISnrs bepalen
      v_debug      := '2';
      v_zisnr_syn  := p_hl7_msg(v_pid_2) (3) (1) (1);
      v_zisnr_pref := p_hl7_msg(v_pid_1) (3) (1) (1);
      IF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         v_debug      := '2a';
         v_zisnr_syn  := formatteer_zisnr_ut(v_zisnr_syn);
         v_zisnr_pref := formatteer_zisnr_ut(v_zisnr_pref);
      END IF;
      v_debug := '3';
      OPEN c_pers(v_zisnr_syn);
      FETCH c_pers
         INTO r_pers_syn;
      IF c_pers%NOTFOUND
      THEN
         v_found_syn := FALSE;
      END IF;
      CLOSE c_pers;
      v_debug := '4';
      IF NOT v_found_syn
      THEN
         kgc_zis.log(v_thisproc || ': Pers met zisnr-synoniem is niet aanwezig: ' || v_zisnr_syn ||
                     ' (dus: geen actie!)');
         RETURN 'V'; -- niet in Helix? Dan geen actie = return V = verwerkt!
      END IF;
      -- wel aanwezig!
      v_debug := '5';
      x_id    := r_pers_syn.pers_id;
      kgc_zis.log(v_thisproc || ': Pers met zisnr-synoniem is aanwezig: ' || v_zisnr_syn);

      v_debug := '8';
      OPEN c_pers(v_zisnr_pref);
      FETCH c_pers
         INTO r_pers_pref;
      IF c_pers%NOTFOUND
      THEN
         v_found_pref := FALSE;
      END IF;
      CLOSE c_pers;

      -- stuur msg naar behoeftigen
      v_debug    := '7';
      v_mail_msg := '<html>' || g_html_mail_head || '<body>' || kgc_zis.g_cr;

      v_mail_msg := v_mail_msg || '<p>Er is een ADT-A24-bericht ontvangen van ZIS in Helix-database ' ||
                    upper(database_name) || ' op ' || v_sysdate || kgc_zis.g_cr ||
                    '<p><p>Het betreft synoniem ZISnummer <b>' || v_zisnr_syn || '</b>, ' || r_pers_syn.aanspreken || ', ' ||
                    r_pers_syn.geslacht || ', ' || to_char(r_pers_syn.geboortedatum
                                                          ,'dd-mm-yyyy') || '.';

      IF v_found_pref
      THEN
         v_mail_msg := v_mail_msg || kgc_zis.g_cr || '<p><p>Het bijbehorende Preferent ZISnummer ' || v_zisnr_pref ||
                       ' bestaat in Helix: ' || r_pers_pref.aanspreken || ', ' || r_pers_pref.geslacht || ', ' ||
                       to_char(r_pers_pref.geboortedatum
                              ,'dd-mm-yyyy') || '.';
      ELSE
         v_mail_msg := v_mail_msg || kgc_zis.g_cr || '<p><p>Het bijbehorende Preferent ZISnummer ' || v_zisnr_pref ||
                       ' <b>bestaat NIET in Helix</b>!' || kgc_zis.g_cr;
      END IF;

      -- onderzoeken:
      v_debug    := '8';
      v_mail_msg := v_mail_msg || getondehtml(r_pers_syn.pers_id
                                             ,v_zisnr_syn) || kgc_zis.g_cr;

      IF v_found_pref
      THEN
         v_mail_msg := v_mail_msg || kgc_zis.g_cr ||
                       '<p><b>Hint</b>: voeg de personen (na controle) samen in Helix, waarbij ' || v_zisnr_pref ||
                       ' het correcte ZISnummer is, en ' || v_zisnr_syn || ' het koppel ZISnummer!' || kgc_zis.g_cr;
      ELSE
         v_mail_msg := v_mail_msg || kgc_zis.g_cr ||
                       '<p><b>Hint</b>: wijzig (na controle in ZIS) het ZISnummer van persoon ' || v_zisnr_syn ||
                       ' naar ' || v_zisnr_pref || kgc_zis.g_cr;
      END IF;

      v_mail_msg := v_mail_msg || kgc_zis.g_cr || kgc_zis.g_cr || '<p><p>Succes, <p>Helix - ' || v_thisproc;
      v_mail_msg := v_mail_msg || '</body></html>';
      v_debug    := '9/' || to_char(nvl(length(v_mail_msg)
                                       ,0));

      BEGIN
         kgc_zis.post_mail_zis('ZIS_MAIL_ADTA24'
                              ,'Helix: ZIS-koppel-bericht ontvangen (ADT-A24=koppel / ' || upper(database_name) ||
                               ' / ' || v_sysdate || ')'
                              ,v_mail_msg
                              ,TRUE);
         v_ret_status := 'V';
      EXCEPTION
         WHEN OTHERS THEN
            -- probleem met de mail
            kgc_zis.log(p_tekst     => v_thisproc || ': De Email voor bericht ADT^A24 - ' || to_char(p_hlbe_id) ||
                                       ' kon niet verzonden worden. ZISnr Preferent: ' || v_zisnr_pref ||
                                       '; ZISnr Synoniem: ' || v_zisnr_syn || ' (dus: later nieuwe poging!)' ||
                                       '; Debug: ' || v_debug || '; Error: ' || SQLERRM
                       ,p_log_level => NULL
                       ,p_type      => 'E');
            v_ret_status := 'X'; -- niet herhalen!
      END;
      RETURN v_ret_status;
   EXCEPTION
      WHEN OTHERS THEN
         kgc_zis.log(v_thisproc || ': Debug: ' || v_debug || '; SQLErrm: ' || SQLERRM);
         kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                              ,v_thisproc || ': Fout (database: ' || upper(database_name) || ')!'
                              ,'Debug: ' || v_debug || '; SQLErrm: ' || SQLERRM);
         RETURN 'X';
   END;

   ----------------------------------------------------------------------------
   FUNCTION verwerk_adt_a37_bericht -- unlink patient
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2 IS
      ----------------------------------------------------------------------------
      CURSOR c_pers(p_zisnr VARCHAR2) IS
         SELECT pers.pers_id
               ,pers.aanspreken
               ,pers.geboortedatum
               ,pers.geslacht
               ,pers.beheer_in_zis
           FROM kgc_personen pers
          WHERE pers.zisnr = p_zisnr;
      r_pers_pref  c_pers%ROWTYPE;
      r_pers_syn   c_pers%ROWTYPE;
      v_ret_status kgc_hl7_berichten.status%TYPE := 'N';
      v_zisnr_pref kgc_personen.zisnr%TYPE;
      v_zisnr_syn  kgc_personen.zisnr%TYPE;
      v_found_pref BOOLEAN := TRUE;
      v_found_syn  BOOLEAN := TRUE;
      v_mail_msg   VARCHAR2(32767);
      v_sysdate    VARCHAR2(20) := to_char(SYSDATE
                                          ,'dd-mm-yyyy hh24:mi:ss');
      v_pid_1      VARCHAR2(10);
      v_pid_2      VARCHAR2(10);
      v_debug      VARCHAR2(100);
      v_thisproc   VARCHAR2(100) := 'kgc_zis.verwerk_ADT_A37_bericht';
   BEGIN
      -- A37: MSH EVN PID PID
      v_debug := '1';
      kgc_zis.log(v_thisproc || ' Bericht ' || to_char(p_hlbe_id) || ' wordt verwerkt!');
      g_sypa_zis_locatie := get_sypa_sywa('ZIS_LOCATIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_zis_locatie);
      g_sypa_hl7_versie  := get_sypa_sywa('HL7_VERSIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_hl7_versie);
      v_pid_1            := getsegmentcode('PID'
                                          ,p_hl7_msg);
      v_pid_2            := getsegmentcode('PID'
                                          ,p_hl7_msg
                                          ,v_pid_1);
      -- compleet?
      IF nvl(length(v_pid_1)
            ,0) = 0
         OR nvl(length(v_pid_2)
               ,0) = 0
      THEN
         kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                              ,v_thisproc || ': Fout (database: ' || upper(database_name) || ')!'
                              ,'Bericht (kgc_hl7_berichten) met id = ' || to_char(p_hlbe_id) || ' is niet compleet! (' ||
                               v_pid_1 || '/' || v_pid_2 || ')');
         RETURN 'X';
      END IF;

      -- eerst ZISnrs bepalen
      v_debug      := '2';
      v_zisnr_syn  := p_hl7_msg(v_pid_2) (3) (1) (1);
      v_zisnr_pref := p_hl7_msg(v_pid_1) (3) (1) (1);
      -- komt alleen door in Nijmegen, dus geen vertaling nodig
      v_debug := '3';
      OPEN c_pers(v_zisnr_syn);
      FETCH c_pers
         INTO r_pers_syn;
      IF c_pers%NOTFOUND
      THEN
         v_found_syn := FALSE;
      END IF;
      CLOSE c_pers;
      v_debug := '5';
      OPEN c_pers(v_zisnr_pref);
      FETCH c_pers
         INTO r_pers_pref;
      IF c_pers%NOTFOUND
      THEN
         v_found_pref := FALSE;
      END IF;
      CLOSE c_pers;

      IF NOT v_found_syn
         AND NOT v_found_pref
      THEN
         -- beide niet in Helix? Klaar
         kgc_zis.log(v_thisproc || ': Personen met zisnrs zijn niet aanwezig: ' || v_zisnr_syn || '; ' || v_zisnr_pref ||
                     ' (dus: geen actie!)');
         RETURN 'V';
      END IF;

      -- stuur msg naar behoeftigen
      v_debug    := '7';
      v_mail_msg := '<html>' || g_html_mail_head || '<body>' || kgc_zis.g_cr;

      v_mail_msg := v_mail_msg || '<p>Er is een ADT-A37-bericht ontvangen van ZIS in Helix-database ' ||
                    upper(database_name) || ' op ' || v_sysdate || kgc_zis.g_cr ||
                    '<p><p>Het betreft een unlink-bericht voor de ZISnummers <b>' || v_zisnr_syn || '</b> en <b>' ||
                    v_zisnr_pref || '</b>, waarvan er in ieder geval 1 in Helix zit!';

      v_mail_msg := v_mail_msg || kgc_zis.g_cr || kgc_zis.g_cr || '<p><p>Succes, <p>Helix - ' || v_thisproc;
      v_mail_msg := v_mail_msg || '</body></html>';

      BEGIN
         kgc_zis.post_mail_zis('ZIS_MAIL_ADTA37'
                              ,'Helix: ZIS-ontkoppel-bericht ontvangen (ADT-A37=ontkoppel / ' || upper(database_name) ||
                               ' / ' || v_sysdate || ')'
                              ,v_mail_msg
                              ,TRUE);
         v_ret_status := 'V';
      EXCEPTION
         WHEN OTHERS THEN
            -- probleem met de mail
            kgc_zis.log(p_tekst     => v_thisproc || ': De Email voor bericht ADT^A37 - ' || to_char(p_hlbe_id) ||
                                       ' kon niet verzonden worden. ZISnr Preferent: ' || v_zisnr_pref ||
                                       '; ZISnr Synoniem: ' || v_zisnr_syn || ' (dus: later nieuwe poging!)' ||
                                       '; Error: ' || SQLERRM
                       ,p_log_level => NULL
                       ,p_type      => 'E');
            v_ret_status := 'X'; -- niet herhalen!
      END;
      RETURN v_ret_status;
   END;

   ----------------------------------------------------------------------------
   FUNCTION verwerk_ander_bericht -- specifiek
   (p_hlbe_id  NUMBER
   ,p_hl7_msg  kgc_zis.hl7_msg
   ,x_entiteit IN OUT VARCHAR2
   ,x_id       IN OUT NUMBER) RETURN VARCHAR2 IS
      ----------------------------------------------------------------------------
      v_ret_status kgc_hl7_berichten.status%TYPE := 'X'; -- betere waarde dan N: N resulteert in een Job, X wordt gesignaleerd...
      v_debug      VARCHAR2(100);
      v_thisproc   VARCHAR2(100) := 'kgc_zis.verwerk_ander_bericht';
   BEGIN
      -- mantis 0009797: trigger op kgc_hl7_berichten gaat onterecht af
      g_sypa_zis_locatie := kgc_sypa_00.systeem_waarde('ZIS_LOCATIE');

      IF g_sypa_zis_locatie = 'NIJMEGEN'
      THEN
         v_debug      := v_debug || 'N';
         v_ret_status := kgc_zis_nijmegen.verwerk_ander_bericht(p_hlbe_id  => p_hlbe_id
                                                               ,p_hl7_msg  => p_hl7_msg
                                                               ,x_entiteit => x_entiteit
                                                               ,x_id       => x_id);
      ELSIF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         v_debug      := v_debug || 'U';
         v_ret_status := kgc_zis_utrecht.verwerk_ander_bericht(p_hlbe_id  => p_hlbe_id
                                                              ,p_hl7_msg  => p_hl7_msg
                                                              ,x_entiteit => x_entiteit
                                                              ,x_id       => x_id);
      ELSIF g_sypa_zis_locatie = 'MAASTRICHT'
      THEN
         v_debug      := v_debug || 'M';
         null;
      END IF;

      RETURN v_ret_status;
   END;

   FUNCTION wijziging_geblokkeerd -- kij of een wijziging door mag gaan
   (p_pers_trow1 cg$kgc_personen.cg$row_type
   ,p_pers_trow2 cg$kgc_personen.cg$row_type) RETURN BOOLEAN IS
      v_sypa_val   kgc_systeem_parameters.standaard_waarde%TYPE;
      v_leeg       VARCHAR2(10) := chr(10) || chr(11) || chr(13); -- 'onmogelijke' string tbv vgl null
      v_leeg_date  DATE := to_date('01-01-0500'
                                  ,'DD-MM-YYYY');
      v_thisproc   VARCHAR2(100) := 'kgc_zis.wijziging_geblokkeerd';
      v_count_wijz NUMBER := 0;
   BEGIN
      BEGIN
         v_sypa_val := kgc_sypa_00.systeem_waarde('ZIS_ADTA08_BLOK');
      EXCEPTION
         WHEN OTHERS THEN
            RETURN FALSE; -- niet aanwezig? niet geblokkeerd!
      END;
      IF instr(v_sypa_val
              ,'A') > 0
      THEN
         -- blokkeren indien 2 van 3 gewijzigd; { achternaam, geslacht, geboortedatum }
         IF nvl(p_pers_trow1.achternaam
               ,v_leeg) <> nvl(p_pers_trow2.achternaam
                              ,v_leeg)
         THEN
            v_count_wijz := v_count_wijz + 1;
         END IF;
         IF nvl(p_pers_trow1.geslacht
               ,v_leeg) <> nvl(p_pers_trow2.geslacht
                              ,v_leeg)
         THEN
            v_count_wijz := v_count_wijz + 1;
         END IF;
         IF nvl(p_pers_trow1.geboortedatum
               ,v_leeg_date) <> nvl(p_pers_trow2.geboortedatum
                                   ,v_leeg_date)
         THEN
            v_count_wijz := v_count_wijz + 1;
         END IF;
         IF v_count_wijz >= 2
         THEN
            -- blokkeren!!!
            RETURN TRUE;
         END IF;
      END IF;
      RETURN FALSE;
   END;

   PROCEDURE bepaal_pers_verschil(p_pers_trow_org IN cg$kgc_personen.cg$row_type
                                 ,p_pers_trow_upd IN cg$kgc_personen.cg$row_type
                                 ,p_rela_trow_upd IN cg$kgc_relaties.cg$row_type
                                 ,p_verz_trow_upd IN cg$kgc_verzekeraars.cg$row_type
                                 ,x_stremail      IN OUT VARCHAR2
                                 ,x_strconditie   IN OUT VARCHAR2) IS
      v_leeg      VARCHAR2(10) := chr(10) || chr(11) || chr(13); -- 'onmogelijke' string tbv vgl null
      v_leeg_date DATE := to_date('01-01-0500'
                                 ,'DD-MM-YYYY');
      v_datefmt   VARCHAR2(10) := 'DD-MM-YYYY';
      v_verslag   VARCHAR2(32767);
      v_kolomnaam VARCHAR2(32);
      v_sep       VARCHAR2(10);
      v_thisproc  VARCHAR2(100) := 'kgc_zis.bepaal_pers_verschil';
   BEGIN
      x_strconditie := '#';
      -- *** ZISnummer - kan niet automatisch gewijzigd worden: is identificatie!
      -- *** achternaam
      IF nvl(p_pers_trow_org.achternaam
            ,v_leeg) <> nvl(p_pers_trow_upd.achternaam
                           ,v_leeg)
      THEN
         v_kolomnaam := 'ACHTERNAAM';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.achternaam
                                           ,' ') || '->' || p_pers_trow_upd.achternaam || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** voorvoegsel
      IF nvl(p_pers_trow_org.voorvoegsel
            ,v_leeg) <> nvl(p_pers_trow_upd.voorvoegsel
                           ,v_leeg)
      THEN
         v_kolomnaam := 'VOORVOEGSEL';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.voorvoegsel
                                           ,' ') || '->' || p_pers_trow_upd.voorvoegsel || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** voorletters
      IF nvl(p_pers_trow_org.voorletters
            ,v_leeg) <> nvl(p_pers_trow_upd.voorletters
                           ,v_leeg)
      THEN
         v_kolomnaam := 'VOORLETTERS';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.voorletters
                                           ,' ') || '->' || p_pers_trow_upd.voorletters || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** achternaam_partner
      IF nvl(p_pers_trow_org.achternaam_partner
            ,v_leeg) <> nvl(p_pers_trow_upd.achternaam_partner
                           ,v_leeg)
      THEN
         v_kolomnaam := 'ACHTERNAAM_PARTNER';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.achternaam_partner
                                           ,' ') || '->' || p_pers_trow_upd.achternaam_partner || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** voorvoegsel_partner
      IF nvl(p_pers_trow_org.voorvoegsel_partner
            ,v_leeg) <> nvl(p_pers_trow_upd.voorvoegsel_partner
                           ,v_leeg)
      THEN
         v_kolomnaam := 'VOORVOEGSEL_PARTNER';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.voorvoegsel_partner
                                           ,' ') || '->' || p_pers_trow_upd.voorvoegsel_partner || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** geslacht
      IF nvl(p_pers_trow_org.geslacht
            ,v_leeg) <> nvl(p_pers_trow_upd.geslacht
                           ,v_leeg)
      THEN
         v_kolomnaam := 'GESLACHT';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.geslacht
                                           ,' ') || '->' || p_pers_trow_upd.geslacht || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** gehuwd
      IF nvl(p_pers_trow_org.gehuwd
            ,v_leeg) <> nvl(p_pers_trow_upd.gehuwd
                           ,v_leeg)
      THEN
         v_kolomnaam := 'GEHUWD';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.gehuwd
                                           ,' ') || '->' || p_pers_trow_upd.gehuwd || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** aanspreken
      IF nvl(p_pers_trow_org.aanspreken
            ,v_leeg) <> nvl(p_pers_trow_upd.aanspreken
                           ,v_leeg)
      THEN
         v_kolomnaam := 'AANSPREKEN';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.aanspreken
                                           ,' ') || '->' || p_pers_trow_upd.aanspreken || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** default_aanspreken
      IF nvl(p_pers_trow_org.default_aanspreken
            ,v_leeg) <> nvl(p_pers_trow_upd.default_aanspreken
                           ,v_leeg)
      THEN
         v_kolomnaam := 'DEFAULT_AANSPREKEN';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.default_aanspreken
                                           ,' ') || '->' || p_pers_trow_upd.default_aanspreken || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** geboortedatum
      IF nvl(p_pers_trow_org.geboortedatum
            ,v_leeg_date) <> nvl(p_pers_trow_upd.geboortedatum
                                ,v_leeg_date)
      THEN
         v_kolomnaam := 'GEBOORTEDATUM';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(to_char(p_pers_trow_org.geboortedatum
                                                   ,v_datefmt)
                                           ,' ') || '->' ||
                 to_char(p_pers_trow_upd.geboortedatum
                        ,v_datefmt) || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** part_geboortedatum
      IF nvl(p_pers_trow_org.part_geboortedatum
            ,v_leeg) <> nvl(p_pers_trow_upd.part_geboortedatum
                           ,v_leeg)
      THEN
         v_kolomnaam := 'PART_GEBOORTEDATUM';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.part_geboortedatum
                                           ,' ') || '->' || p_pers_trow_upd.part_geboortedatum || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** overlijdensdatum
      IF nvl(p_pers_trow_org.overlijdensdatum
            ,v_leeg_date) <> nvl(p_pers_trow_upd.overlijdensdatum
                                ,v_leeg_date)
      THEN
         v_kolomnaam := 'OVERLIJDENSDATUM';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(to_char(p_pers_trow_org.overlijdensdatum
                                                   ,v_datefmt)
                                           ,' ') || '->' ||
                 to_char(p_pers_trow_upd.overlijdensdatum
                        ,v_datefmt) || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** overleden
      IF nvl(p_pers_trow_org.overleden
            ,v_leeg) <> nvl(p_pers_trow_upd.overleden
                           ,v_leeg)
      THEN
         v_kolomnaam := 'OVERLEDEN';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.overleden
                                           ,' ') || '->' || p_pers_trow_upd.overleden || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** meerling
      IF nvl(p_pers_trow_org.meerling
            ,v_leeg) <> nvl(p_pers_trow_upd.meerling
                           ,v_leeg)
      THEN
         v_kolomnaam := 'MEERLING';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.meerling
                                           ,' ') || '->' || p_pers_trow_upd.meerling || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** adres
      IF nvl(p_pers_trow_org.adres
            ,v_leeg) <> nvl(p_pers_trow_upd.adres
                           ,v_leeg)
      THEN
         v_kolomnaam := 'ADRES';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.adres
                                           ,' ') || '->' || p_pers_trow_upd.adres || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** woonplaats
      IF nvl(p_pers_trow_org.woonplaats
            ,v_leeg) <> nvl(p_pers_trow_upd.woonplaats
                           ,v_leeg)
      THEN
         v_kolomnaam := 'WOONPLAATS';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.woonplaats
                                           ,' ') || '->' || p_pers_trow_upd.woonplaats || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** postcode
      IF nvl(p_pers_trow_org.postcode
            ,v_leeg) <> nvl(p_pers_trow_upd.postcode
                           ,v_leeg)
      THEN
         v_kolomnaam := 'POSTCODE';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.postcode
                                           ,' ') || '->' || p_pers_trow_upd.postcode || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** land
      IF nvl(p_pers_trow_org.land
            ,v_leeg) <> nvl(p_pers_trow_upd.land
                           ,v_leeg)
      THEN
         v_kolomnaam := 'LAND';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.land
                                           ,' ') || '->' || p_pers_trow_upd.land || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** telefoon
      IF nvl(p_pers_trow_org.telefoon
            ,v_leeg) <> nvl(p_pers_trow_upd.telefoon
                           ,v_leeg)
      THEN
         v_kolomnaam := 'TELEFOON';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.telefoon
                                           ,' ') || '->' || p_pers_trow_upd.telefoon || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** telefoon
      IF nvl(p_pers_trow_org.telefoon2
            ,v_leeg) <> nvl(p_pers_trow_upd.telefoon2
                           ,v_leeg)
      THEN
         v_kolomnaam := 'TELEFOON2';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.telefoon2
                                           ,' ') || '->' || p_pers_trow_upd.telefoon2 || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;

      ------- saroj, mantis 5587  rework TIA ---------------
      -- *** email
      IF nvl(p_pers_trow_org.EMAIL
            ,v_leeg) <> nvl(p_pers_trow_upd.EMAIL
                           ,v_leeg)
      THEN
         v_kolomnaam := 'EMAIL';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.EMAIL
                                           ,' ') || '->' || p_pers_trow_upd.EMAIL || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;


      -- saroj, mantis 5587, rework TIA completed here ---

      -- *** verzekeringsnr
      IF nvl(p_pers_trow_org.verzekeringsnr
            ,v_leeg) <> nvl(p_pers_trow_upd.verzekeringsnr
                           ,v_leeg)
      THEN
         v_kolomnaam := 'VERZEKERINGSNR';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.verzekeringsnr
                                           ,' ') || '->' || p_pers_trow_upd.verzekeringsnr || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** verzekeringswijze
      IF nvl(p_pers_trow_org.verzekeringswijze
            ,v_leeg) <> nvl(p_pers_trow_upd.verzekeringswijze
                           ,v_leeg)
      THEN
         v_kolomnaam := 'VERZEKERINGSWIJZE';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.verzekeringswijze
                                           ,' ') || '->' || p_pers_trow_upd.verzekeringswijze || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** bsn
      IF nvl(p_pers_trow_org.bsn, -1) <> nvl(p_pers_trow_upd.bsn, -1)
      THEN
         v_kolomnaam := 'BSN';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(to_char(p_pers_trow_org.bsn)
                                           ,' ') || '->' ||
                 to_char(p_pers_trow_upd.bsn) || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** bsn_geverifieerd
      IF nvl(p_pers_trow_org.bsn_geverifieerd
            ,v_leeg) <> nvl(p_pers_trow_upd.bsn_geverifieerd
                           ,v_leeg)
      THEN
         v_kolomnaam := 'BSN_GEVERIFIEERD';
         addtext(v_verslag
                ,2000
                ,v_kolomnaam || ' (' || nvl(p_pers_trow_org.bsn_geverifieerd
                                           ,' ') || '->' || p_pers_trow_upd.bsn_geverifieerd || ')'
                ,v_sep);
         v_sep         := '<br>';
         x_strconditie := x_strconditie || v_kolomnaam || '#';
      END IF;
      -- *** huisarts
      IF nvl(p_pers_trow_org.rela_id
            ,0) <> nvl(p_pers_trow_upd.rela_id
                      ,0)
      THEN
         IF p_pers_trow_upd.rela_id IS NULL
         THEN
            addtext(v_verslag
                   ,2000
                   ,'Huisarts is afwezig!'
                   ,v_sep);
         ELSE
            addtext(v_verslag
                   ,2000
                   ,'Nieuwe huisarts: ' || p_rela_trow_upd.aanspreken || ', ' || p_rela_trow_upd.woonplaats
                   ,v_sep);
         END IF;
         v_sep         := '<br>';
         x_strconditie := x_strconditie || 'RELA_ID#';
      END IF;
      -- *** verzekeraar
      IF nvl(p_pers_trow_org.verz_id
            ,0) <> nvl(p_pers_trow_upd.verz_id
                      ,0)
      THEN
         IF p_pers_trow_upd.verz_id IS NULL
         THEN
            addtext(v_verslag
                   ,2000
                   ,'Verzekeraar is afwezig!'
                   ,v_sep);
         ELSE
            addtext(v_verslag
                   ,2000
                   ,'Nieuwe verzekeraar: ' || p_verz_trow_upd.naam || ', ' || p_verz_trow_upd.woonplaats
                   ,v_sep);
         END IF;
         v_sep         := '<br>';
         x_strconditie := x_strconditie || 'VERZ_ID#';
      END IF;

      x_stremail := v_verslag;
   END;

   -- deze functie is verplaatst naar kgc_dft...
   FUNCTION verstuur_dft_p03(p_decl_id    IN NUMBER
                            ,p_aantal     IN NUMBER
                            ,p_check_only IN BOOLEAN
                            ,x_msg        OUT VARCHAR2) RETURN BOOLEAN IS
   BEGIN
      RETURN TRUE;
   END;

   FUNCTION met_puntjes(b_string IN VARCHAR2) RETURN VARCHAR2 IS
      v_return VARCHAR2(100);
      v_letter VARCHAR2(1);
   BEGIN
      FOR i IN 1 .. length(b_string)
      LOOP
         v_letter := substr(b_string
                           ,i
                           ,1);
         IF (v_letter = upper(v_letter))
         THEN
            v_return := v_return || v_letter || '.';
         ELSE
            v_return := v_return || v_letter;
         END IF;
      END LOOP;
      RETURN(v_return);
   EXCEPTION
      WHEN OTHERS THEN
         RETURN(b_string);
   END met_puntjes;

   PROCEDURE format_geo(x_postcode   IN OUT VARCHAR2
                       ,x_woonplaats IN OUT VARCHAR2
                       ,x_provincie  IN OUT VARCHAR2
                       ,x_land       IN OUT VARCHAR2
                       ,x_msg        IN OUT VARCHAR2) IS
      v_debug    VARCHAR2(100);
      v_thisproc VARCHAR2(100) := 'kgc_zis.format_geo';
   BEGIN
      x_postcode := ltrim(rtrim(x_postcode));
      IF nvl(length(x_postcode)
            ,0) < 6
         OR nvl(length(x_postcode)
               ,0) > 7
      THEN
         RETURN;
      END IF;
      -- probeer postcode te formatteren
      v_debug := v_debug || 'a';
      DECLARE
         v_postcode VARCHAR2(100) := upper(x_postcode);
         v_num      NUMBER(4
                          ,0);
         v_chr      VARCHAR2(2);
      BEGIN
         v_debug := v_debug || 'b';
         v_num   := to_number(substr(v_postcode
                                    ,1
                                    ,4));
         IF length(v_postcode) = 6
         THEN
            v_chr := substr(v_postcode
                           ,5
                           ,2);
         ELSIF length(v_postcode) = 7
               AND substr(v_postcode
                         ,5
                         ,1) = ' '
         THEN
            v_chr := substr(v_postcode
                           ,6
                           ,2);
         END IF;
         IF (v_chr NOT BETWEEN 'AA' AND 'ZZ')
         THEN
            RAISE value_error;
         END IF;
         x_postcode := v_num || ' ' || v_chr;
         v_debug    := v_debug || 'd';
         -- formaat is ok! Indien het land leeg is: Nederland toevoegen
         IF x_land IS NULL
         THEN
            x_land := 'Nederland';
         END IF;
      EXCEPTION
         WHEN value_error THEN
            -- geen goed formaat? Dan valt er niets te doen
            IF upper(x_land) = 'NEDERLAND'
            THEN
               x_msg := 'Postcode ' || x_postcode || ' heeft niet het juiste formaat voor Nederland!';
            END IF;
            RETURN;
      END;

      IF upper(nvl(x_land
                  ,'NEDERLAND')) = 'NEDERLAND'
      THEN
         v_debug := v_debug || 'e';
         -- probeer te formatteren; igv fout: aangeleverde waarden overnemen!
         DECLARE
            v_errorrec      hil_message.message_rectype;
            v_error_mess    VARCHAR2(2000);
            v_postcode      VARCHAR2(100) := x_postcode;
            v_woonplaats    kgc_personen.woonplaats%TYPE := x_woonplaats;
            v_pocd_id       NUMBER;
            v_geme_id       NUMBER;
            v_provincie     kgc_personen.provincie%TYPE;
            v_land_dummy    kgc_personen.land%TYPE;
            v_dummy_boolean BOOLEAN;
         BEGIN
            v_debug := v_debug || 'f';

            v_dummy_boolean := kgc_pocd_00.postcode_ok(p_postcode => x_postcode);
            kgc_pocd_00.gerelateerde_gegevens(p_postcode  => v_postcode
                                             ,x_gevonden  => v_dummy_boolean
                                             ,x_plaats    => v_woonplaats
                                             ,x_provincie => v_provincie
                                             ,x_land      => v_land_dummy);
            v_debug      := v_debug || 'g';
            v_error_mess := cg$errors.geterrors; -- verwijdert de error!
            IF nvl(length(v_error_mess)
                  ,0) = 0
            THEN
               -- geen fout; provincie en woonplaats overnemen
               v_debug      := v_debug || 'h';
               x_woonplaats := v_woonplaats;
               x_provincie  := v_provincie;
            END IF;
         EXCEPTION
            WHEN OTHERS THEN
               NULL;
         END;
      END IF;
   END;

   PROCEDURE gethl7waarde -- indien p_hl7_waarde = "", dan leeg maken; anders alleen overnemen indien gevuld!
   (p_hl7_waarde   IN VARCHAR2
   ,x_helix_waarde IN OUT VARCHAR2) IS
   BEGIN
      IF p_hl7_waarde = '""'
      THEN
         x_helix_waarde := NULL;
      ELSIF nvl(length(p_hl7_waarde)
               ,0) > 0
      THEN
         x_helix_waarde := p_hl7_waarde;
      END IF;
   END;

   FUNCTION exehl7msg -- testing!!! voor testdoeleinden kan de hl7-listener ook een QRY^Q01 bericht verwerken: deze procedure moet dan het antwoord geven!
   (p_strhl7 IN VARCHAR2) RETURN VARCHAR2 IS
      CURSOR c_pers(p_zisnr VARCHAR2) IS
         SELECT pers_id
           FROM kgc_personen pers
          WHERE pers.zisnr = p_zisnr;
      v_pers_id      NUMBER;
      v_hl7_msg_bron kgc_zis.hl7_msg;
      v_hl7_msg_res  kgc_zis.hl7_msg;
      v_msh          VARCHAR2(10);
      v_qrd          VARCHAR2(10);
      v_msg          VARCHAR2(32767);
      v_sysdate      VARCHAR2(15) := to_char(SYSDATE
                                            ,'yyyymmddhh24mi');
      v_hl7_err      VARCHAR2(32767) := 'MSH|^~&|HELIX|HELIX|HELIX||' || v_sysdate || '||XXX^XXX|4137|P|2.2|||||' ||
                                        kgc_zis.g_cr || 'MSA|AE|4137|';
      v_zisnr        VARCHAR2(10);
      pers_trow      cg$kgc_personen.cg$row_type;
      rela_trow      cg$kgc_relaties.cg$row_type;
      verz_trow      cg$kgc_verzekeraars.cg$row_type;
      v_debug        VARCHAR2(100);
      v_thisproc     VARCHAR2(100) := 'kgc_zis.exeHL7Msg';
   BEGIN
      v_debug := '1';
      dbms_output.put_line(v_thisproc || ': HL7-msg: ' || substr(p_strhl7
                                                                ,1
                                                                ,240));
      IF nvl(length(p_strhl7)
            ,0) = 0
      THEN
         v_debug := v_debug || 'a';
         log(v_thisproc || ': Lege boodschap ontvangen!');
         RETURN kgc_zis.g_sob_char || v_hl7_err || 'Lege boodschap!' || kgc_zis.g_cr || kgc_zis.g_eob_char || kgc_zis.g_term_char;
      END IF;

      v_debug        := '2';
      v_hl7_msg_bron := kgc_zis.str2hl7msg(0
                                          ,p_strhl7);
      v_debug        := '3';
      v_msh          := kgc_zis.getsegmentcode('MSH'
                                              ,v_hl7_msg_bron);
      IF nvl(length(v_msh)
            ,0) = 0
      THEN
         log(v_thisproc || ': Fout in HL7-bericht (MSH-segment afwezig)!');
         RETURN kgc_zis.g_sob_char || v_hl7_err || 'Fout in HL7-bericht (MSH-segment afwezig)!' || kgc_zis.g_cr || kgc_zis.g_eob_char || kgc_zis.g_term_char;
      END IF;

      v_debug := '4';
      IF v_hl7_msg_bron(v_msh) (9) (1) (1) = 'QRY'
      THEN
         v_debug := v_debug || 'a';
         IF v_hl7_msg_bron(v_msh) (9) (1) (2) = 'Q01'
         THEN
            v_debug := v_debug || '1';
            v_qrd   := kgc_zis.getsegmentcode('QRD'
                                             ,v_hl7_msg_bron);
            v_debug := v_debug || '2';
            IF nvl(length(v_qrd)
                  ,0) = 0
            THEN
               log(v_thisproc || ': Fout in HL7-bericht (QRD-segment afwezig)!');
               RETURN kgc_zis.g_sob_char || v_hl7_err || 'Fout in HL7-bericht (QRD-segment afwezig)!' || kgc_zis.g_cr || kgc_zis.g_eob_char || kgc_zis.g_term_char;
            END IF;
            v_debug := v_debug || '3';
            v_zisnr := v_hl7_msg_bron(v_qrd) (8) (1) (1);
            IF g_sypa_zis_locatie = 'UTRECHT'
            THEN
               v_debug := v_debug || 'a';
               v_zisnr := formatteer_zisnr_ut(v_zisnr);
            END IF;

            -- **** MSH
            v_debug := v_debug || '4';
            v_hl7_msg_res('MSH') := kgc_zis.init_msg('MSH'
                                                    ,v_hl7_msg_bron(v_msh) (12) (1) (1));
            v_hl7_msg_res('MSH')(1)(1)(1) := v_hl7_msg_bron(v_msh) (1) (1) (1);
            v_hl7_msg_res('MSH')(2)(1)(1) := v_hl7_msg_bron(v_msh) (2) (1) (1);
            v_hl7_msg_res('MSH')(3)(1)(1) := v_hl7_msg_bron(v_msh) (5) (1) (1);
            v_hl7_msg_res('MSH')(4)(1)(1) := 'HELIX';
            v_hl7_msg_res('MSH')(5)(1)(1) := v_hl7_msg_bron(v_msh) (3) (1) (1);
            v_hl7_msg_res('MSH')(7)(1)(1) := v_sysdate;
            v_hl7_msg_res('MSH')(9)(1)(1) := 'ADT^A19';
            v_hl7_msg_res('MSH')(10)(1)(1) := v_hl7_msg_bron(v_msh) (10) (1) (1);
            v_hl7_msg_res('MSH')(11)(1)(1) := 'P';
            v_hl7_msg_res('MSH')(12)(1)(1) := v_hl7_msg_bron(v_msh) (12) (1) (1);

            -- **** QRD
            v_debug := v_debug || '5';
            v_hl7_msg_res('QRD') := kgc_zis.init_msg('QRD'
                                                    ,v_hl7_msg_bron(v_msh) (12) (1) (1));
            v_hl7_msg_res('QRD')(1)(1)(1) := v_hl7_msg_bron(v_qrd) (1) (1) (1);
            v_hl7_msg_res('QRD')(2)(1)(1) := v_hl7_msg_bron(v_qrd) (2) (1) (1);
            v_hl7_msg_res('QRD')(3)(1)(1) := v_hl7_msg_bron(v_qrd) (3) (1) (1);
            v_hl7_msg_res('QRD')(4)(1)(1) := v_hl7_msg_bron(v_qrd) (4) (1) (1);
            v_hl7_msg_res('QRD')(8)(1)(1) := v_hl7_msg_bron(v_qrd) (8) (1) (1);
            v_hl7_msg_res('QRD')(9)(1)(1) := v_hl7_msg_bron(v_qrd) (9) (1) (1);

            -- **** MSA
            v_debug := v_debug || '6';
            v_hl7_msg_res('MSA') := kgc_zis.init_msg('MSA'
                                                    ,v_hl7_msg_bron(v_msh) (12) (1) (1));
            v_hl7_msg_res('MSA')(2)(1)(1) := v_hl7_msg_bron(v_msh) (10) (1) (1);
            -- zoek persson
            v_debug := v_debug || '7';
            OPEN c_pers(v_zisnr);
            FETCH c_pers
               INTO v_pers_id;
            IF c_pers%NOTFOUND
            THEN
               v_debug := v_debug || '8';
               v_hl7_msg_res('MSA')(1)(1)(1) := 'AE';
               v_hl7_msg_res('MSA')(3)(1)(1) := 'ZISnr ' || v_zisnr || ' niet aanwezig!';
            ELSE
               v_debug := v_debug || '9';
               v_hl7_msg_res('MSA')(1)(1)(1) := 'AA';
               v_hl7_msg_res('MSA')(3)(1)(1) := 'OK';
               -- gegevens ophalen
               pers_trow.pers_id := v_pers_id;
               cg$kgc_personen.slct(pers_trow);
               IF pers_trow.rela_id IS NOT NULL
               THEN
                  rela_trow.rela_id := pers_trow.rela_id;
                  cg$kgc_relaties.slct(rela_trow);
               END IF;
               IF pers_trow.verz_id IS NOT NULL
               THEN
                  verz_trow.verz_id := pers_trow.verz_id;
                  cg$kgc_verzekeraars.slct(verz_trow);
               END IF;
               v_debug := v_debug || 'A';
               -- segementen maken
               v_hl7_msg_res('PID') := kgc_zis.init_msg('PID'
                                                       ,v_hl7_msg_bron(v_msh) (12) (1) (1));
               v_hl7_msg_res('ZPI') := kgc_zis.init_msg('ZPI'
                                                       ,v_hl7_msg_bron(v_msh) (12) (1) (1));
               v_hl7_msg_res('IN1') := kgc_zis.init_msg('IN1'
                                                       ,v_hl7_msg_bron(v_msh) (12) (1) (1));
               v_hl7_msg_res('STF') := kgc_zis.init_msg('STF'
                                                       ,v_hl7_msg_bron(v_msh) (12) (1) (1));
               -- segmenten vullen
               v_debug := v_debug || 'B';
               rec2hl7msg(pers_trow => pers_trow
                         ,rela_trow => rela_trow
                         ,verz_trow => verz_trow
                         ,p_extra   => NULL
                         ,x_hl7_msg => v_hl7_msg_res);
               v_debug := v_debug || 'C';
               v_msg   := kgc_zis.hl7seg2str('PID'
                                            ,v_hl7_msg_res('PID')) || kgc_zis.g_cr ||
                          kgc_zis.hl7seg2str('ZPI'
                                            ,v_hl7_msg_res('ZPI')) || kgc_zis.g_cr ||
                          kgc_zis.hl7seg2str('IN1'
                                            ,v_hl7_msg_res('IN1')) || kgc_zis.g_cr ||
                          kgc_zis.hl7seg2str('STF'
                                            ,v_hl7_msg_res('STF')) || kgc_zis.g_cr;
            END IF;
            CLOSE c_pers;

            v_debug := v_debug || '7';
            v_msg   := kgc_zis.hl7seg2str('MSH'
                                         ,v_hl7_msg_res('MSH')) || kgc_zis.g_cr ||
                       kgc_zis.hl7seg2str('MSA'
                                         ,v_hl7_msg_res('MSA')) || kgc_zis.g_cr ||
                       kgc_zis.hl7seg2str('QRD'
                                         ,v_hl7_msg_res('QRD')) || kgc_zis.g_cr || v_msg;
         END IF;
      ELSE
         -- nog niet ondersteund!
         raise_application_error(-20000
                                ,v_thisproc || ': Error; Message ' || v_hl7_msg_bron('MSH') (9) (1)
                                 (1) || ' wordt niet ondersteund!');
      END IF;

      v_debug := '5';
      v_msg   := kgc_zis.g_sob_char || v_msg || kgc_zis.g_eob_char || kgc_zis.g_term_char;
      RETURN v_msg;
   EXCEPTION
      WHEN OTHERS THEN
         IF SQLCODE = -20000
         THEN
            RAISE; -- bevat al toelichting
         ELSE
            raise_application_error(-20000
                                   ,v_thisproc || ': Debug: ' || v_debug || '; Error: ' || SQLERRM);
         END IF;
   END;

   PROCEDURE post_mail_zis(p_sypa_aan  IN VARCHAR2
                          ,p_onderwerp IN VARCHAR2
                          ,p_inhoud    IN VARCHAR2
                          ,p_html      IN BOOLEAN := FALSE
                          ,p_conditie  IN VARCHAR2 := NULL
                          ,p_sypa_van  IN VARCHAR2 := 'ZIS_MAIL_AFZENDER' -- mantis 740
                           ) IS
      v_van VARCHAR2(100) := kgc_sypa_00.systeem_waarde(p_sypa_van);
      CURSOR c_sypa(p_parameter_code VARCHAR2) IS
         SELECT *
           FROM kgc_systeem_parameters
          WHERE code = upper(p_parameter_code);
      r_sypa c_sypa%ROWTYPE;
      CURSOR c_spwa(p_sypa_id NUMBER) IS
         SELECT mede.email
               ,spwa.waarde
           FROM kgc_systeem_par_waarden spwa
               ,kgc_medewerkers         mede
          WHERE spwa.sypa_id = p_sypa_id
            AND mede.mede_id = spwa.mede_id
            AND mede.email IS NOT NULL -- rde 12-05-2006
         ;
      v_send BOOLEAN;
   BEGIN
      -- sypa ophalen
      OPEN c_sypa(p_sypa_aan);
      FETCH c_sypa
         INTO r_sypa;
      IF c_sypa%NOTFOUND
      THEN
         raise_application_error(-20000
                                ,'Er is geen Systeemparameter "' || p_sypa_aan || '" gevonden!');
      END IF;
      CLOSE c_sypa;
      FOR r_spwa IN c_spwa(r_sypa.sypa_id)
      LOOP
         v_send := TRUE;
         IF p_conditie IS NOT NULL
         THEN
            IF p_conditie <> r_spwa.waarde
            THEN
               v_send := FALSE;
            END IF;
         END IF;
         IF v_send
         THEN
            kgc_mail.post_mail(p_van       => v_van
                              ,p_aan       => r_spwa.email
                              ,p_onderwerp => p_onderwerp
                              ,p_inhoud    => p_inhoud
                              ,p_html      => p_html);
         END IF;
      END LOOP;
   END post_mail_zis;

   FUNCTION verwerk_adt_a40_bericht -- link patient: pid_2.zisnr wordt pid.zisnr
   (p_hlbe_id NUMBER
   ,p_hl7_msg kgc_zis.hl7_msg
   ,x_id      IN OUT NUMBER) RETURN VARCHAR2 IS
      ----------------------------------------------------------------------------
      CURSOR c_pers(p_zisnr VARCHAR2) IS
         SELECT pers.pers_id
               ,pers.aanspreken
               ,pers.geboortedatum
               ,pers.geslacht
               ,pers.beheer_in_zis
           FROM kgc_personen pers
          WHERE pers.zisnr = p_zisnr;
      r_pers_pref  c_pers%ROWTYPE;
      r_pers_syn   c_pers%ROWTYPE;
      v_ret_status kgc_hl7_berichten.status%TYPE := 'N';
      v_zisnr_pref kgc_personen.zisnr%TYPE;
      v_zisnr_syn  kgc_personen.zisnr%TYPE;
      v_found_pref BOOLEAN := TRUE;
      v_found_syn  BOOLEAN := TRUE;
      v_mail_msg   VARCHAR2(32767);
      v_count      NUMBER;
      v_sysdate    VARCHAR2(20) := to_char(SYSDATE
                                          ,'dd-mm-yyyy hh24:mi:ss');
      v_pid        VARCHAR2(10);
      v_mrg        VARCHAR2(10);
      v_debug      VARCHAR2(100);
      v_thisproc   VARCHAR2(100) := 'kgc_zis_utrecht.verwerk_ADT_A40_bericht';
   BEGIN
      -- A40: MSH EVN PID MRG.
      v_debug := '1';
      kgc_zis.log(v_thisproc || ' Bericht ' || to_char(p_hlbe_id) || ' wordt verwerkt!');
      kgc_zis.g_sypa_zis_locatie := get_sypa_sywa('ZIS_LOCATIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_zis_locatie);
      g_sypa_hl7_versie  := get_sypa_sywa('HL7_VERSIE'
                                         ,FALSE
                                         ,TRUE
                                         ,g_sypa_hl7_versie);
      --  kgc_zis.log(v_thisproc || ' Bericht seg#: '||to_char(p_hl7_msg.COUNT));
      v_pid := getsegmentcode('PID'
                             ,p_hl7_msg);
      v_mrg := getsegmentcode('MRG'
                             ,p_hl7_msg);
      -- compleet?
      IF nvl(length(v_pid)
            ,0) = 0
         OR nvl(length(v_mrg)
               ,0) = 0
      THEN
         kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                              ,v_thisproc || ': Fout (database: ' || upper(database_name) || ')!'
                              ,'Bericht (kgc_hl7_berichten) met id = ' || to_char(p_hlbe_id) || ' is niet compleet! (' ||
                               v_pid || '/' || v_mrg || ')');
         RETURN 'X';
      END IF;

      -- eerst ZISnrs bepalen
      v_debug      := '2';
      v_zisnr_syn  := p_hl7_msg(v_mrg) (1) (1) (1);
      v_zisnr_pref := p_hl7_msg(v_pid) (3) (1) (1);
      IF g_sypa_zis_locatie = 'UTRECHT'
      THEN
         v_debug      := '2a';
         v_zisnr_syn  := formatteer_zisnr_ut(v_zisnr_syn);
         v_zisnr_pref := formatteer_zisnr_ut(v_zisnr_pref);
      END IF;
      v_debug := '3';
      OPEN c_pers(v_zisnr_syn);
      FETCH c_pers
         INTO r_pers_syn;
      IF c_pers%NOTFOUND
      THEN
         v_found_syn := FALSE;
      END IF;
      CLOSE c_pers;
      v_debug := '4';
      IF NOT v_found_syn
      THEN
         kgc_zis.log(v_thisproc || ': Pers met zisnr-synoniem is niet aanwezig: ' || v_zisnr_syn ||
                     ' (dus: geen actie!)');
         RETURN 'V'; -- niet in Helix? Dan geen actie = return V = verwerkt!
      END IF;
      -- wel aanwezig!
      v_debug := '5';
      x_id    := r_pers_syn.pers_id;
      kgc_zis.log(v_thisproc || ': Pers met zisnr-synoniem is aanwezig: ' || v_zisnr_syn);

      v_debug := '8';
      OPEN c_pers(v_zisnr_pref);
      FETCH c_pers
         INTO r_pers_pref;
      IF c_pers%NOTFOUND
      THEN
         v_found_pref := FALSE;
      END IF;
      CLOSE c_pers;

      -- stuur msg naar behoeftigen
      v_debug    := '7';
      v_mail_msg := '<html>' || kgc_zis.g_html_mail_head || '<body>' || kgc_zis.g_cr;

      v_mail_msg := v_mail_msg || '<p>Er is een ADT-A40-bericht ontvangen van ZIS in Helix-database ' ||
                    upper(database_name) || ' op ' || v_sysdate || kgc_zis.g_cr ||
                    '<p><p>Het betreft synoniem ZISnummer <b>' || v_zisnr_syn || '</b>, ' || r_pers_syn.aanspreken || ', ' ||
                    r_pers_syn.geslacht || ', ' || to_char(r_pers_syn.geboortedatum
                                                          ,'dd-mm-yyyy') || '.';

      IF v_found_pref
      THEN
         v_mail_msg := v_mail_msg || kgc_zis.g_cr || '<p><p>Het bijbehorende Preferent ZISnummer ' || v_zisnr_pref ||
                       ' bestaat in Helix: ' || r_pers_pref.aanspreken || ', ' || r_pers_pref.geslacht || ', ' ||
                       to_char(r_pers_pref.geboortedatum
                              ,'dd-mm-yyyy') || '.';
      ELSE
         v_mail_msg := v_mail_msg || kgc_zis.g_cr || '<p><p>Het bijbehorende Preferent ZISnummer ' || v_zisnr_pref ||
                       ' <b>bestaat NIET in Helix</b>!' || kgc_zis.g_cr;
      END IF;

      -- onderzoeken:
      v_debug    := '8';
      v_mail_msg := v_mail_msg || getondehtml(r_pers_syn.pers_id
                                             ,v_zisnr_syn) || kgc_zis.g_cr;

      IF v_found_pref
      THEN
         v_mail_msg := v_mail_msg || kgc_zis.g_cr ||
                       '<p><b>Hint</b>: voeg de personen (na controle) samen in Helix, waarbij ' || v_zisnr_pref ||
                       ' het correcte ZISnummer is, en ' || v_zisnr_syn || ' het koppel ZISnummer!' || kgc_zis.g_cr;
      ELSE
         v_mail_msg := v_mail_msg || kgc_zis.g_cr ||
                       '<p><b>Hint</b>: wijzig (na controle in ZIS) het ZISnummer van persoon ' || v_zisnr_syn ||
                       ' naar ' || v_zisnr_pref || kgc_zis.g_cr;
      END IF;

      v_mail_msg := v_mail_msg || kgc_zis.g_cr || kgc_zis.g_cr || '<p><p>Succes, <p>Helix - ' || v_thisproc;
      v_mail_msg := v_mail_msg || '</body></html>';
      v_debug    := '9/' || to_char(nvl(length(v_mail_msg)
                                       ,0));

      BEGIN
         kgc_zis.post_mail_zis('ZIS_MAIL_ADTA24'
                              ,'Helix: ZIS-koppel-bericht ontvangen (ADT-A40=koppel / ' || upper(database_name) ||
                               ' / ' || v_sysdate || ')'
                              ,v_mail_msg
                              ,TRUE);
         v_ret_status := 'V';
      EXCEPTION
         WHEN OTHERS THEN
            -- probleem met de mail
            kgc_zis.log(p_tekst     => v_thisproc || ': De Email voor bericht ADT^A40 - ' || to_char(p_hlbe_id) ||
                                       ' kon niet verzonden worden. ZISnr Preferent: ' || v_zisnr_pref ||
                                       '; ZISnr Synoniem: ' || v_zisnr_syn || ' (dus: later nieuwe poging!)' ||
                                       '; Debug: ' || v_debug || '; Error: ' || SQLERRM
                       ,p_log_level => NULL
                       ,p_type      => 'E');
            v_ret_status := 'X'; -- niet herhalen!
      END;
      RETURN v_ret_status;
   EXCEPTION
      WHEN OTHERS THEN
         kgc_zis.log(v_thisproc || ': Debug: ' || v_debug || '; SQLErrm: ' || SQLERRM);
         kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                              ,v_thisproc || ': Fout (database: ' || upper(database_name) || ')!'
                              ,'Debug: ' || v_debug || '; SQLErrm: ' || SQLERRM);
         RETURN 'X';
   END verwerk_adt_a40_bericht;
END kgc_zis;
/

/
QUIT
