CREATE OR REPLACE PACKAGE "HELIX"."KGC_REUI_00" IS
-- controleer waarde en bepaal uitslagtekst
PROCEDURE uitslag
( p_onde_id IN NUMBER
, x_tekst IN OUT VARCHAR2
, x_toelichting IN OUT VARCHAR2
, x_melding OUT VARCHAR2
);
END KGC_REUI_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_REUI_00" IS
 /******************************************************************************
      NAME:       kgc_reui_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.1        01-03-2016   SKA               Mantis 9355 TO_CHAR added to  Conclusie column
    ****************************************************************************** */
PROCEDURE uitslag
( p_onde_id IN NUMBER
, x_tekst IN OUT VARCHAR2
, x_toelichting IN OUT VARCHAR2
, x_melding OUT VARCHAR2
)
IS
  CURSOR onde_cur
  IS
    SELECT ongr_id
    ,      afgerond
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
  v_ongr_id NUMBER;
  v_afgerond VARCHAR2(1);

  CURSOR reui_cur
  IS
    SELECT reui_id
    ,      stgr_id
    ,      stof_id
    ,      in_tekst
    ,      in_toelichting
    ,      tekst
    ,      als_afgerond
    FROM   kgc_resultaat_uitslag reui
    WHERE  ongr_id = v_ongr_id
    AND    tekst IS NOT NULL
    AND  ( in_tekst = 'J'
        OR in_toelichting = 'J'
         )
    and  ( stgr_id is null
        or stgr_id in
           ( select stgr_id
             from   bas_metingen
             where  onde_id = p_onde_id
           )
         )
    ORDER BY volgorde ASC
    ;

  CURSOR meti_cur
  ( b_stgr_id IN NUMBER
  )
  IS
    SELECT to_char(meti.conclusie)  conclusie -- MANTIS 9355 SKA 01-03-2016 TO_CHAR added  to conclusie
    ,      meti.afgerond
    FROM   bas_metingen meti
    WHERE  meti.onde_id = p_onde_id
    AND    meti.stgr_id = nvl( b_stgr_id, meti.stgr_id )
    AND    meti.conclusie IS NOT NULL
    ORDER BY meti.meti_id ASC
    ;
  CURSOR meet_cur
  ( b_stgr_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  IS
    SELECT meet.meetwaarde
    ,      DECODE( meti.afgerond
                 , 'J', 'J'
                 , meet.afgerond
                 ) afgerond
    FROM   bas_meetwaarde_statussen mest
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.mest_id = mest.mest_id (+)
    AND    NVL( mest.in_uitslag, 'J' ) = 'J'
    AND    meet.meti_id = meti.meti_id
    AND    meti.onde_id = p_onde_id
    AND    meet.stof_id = b_stof_id
    AND  ( meti.stgr_id = b_stgr_id
        OR b_stgr_id IS NULL
         )
    AND    meet.meetwaarde IS NOT NULL
    ORDER BY meet.meet_id ASC
    ;

  v_resultaat VARCHAR2(2000);
  v_meetwaarde VARCHAR2(2000);

  PROCEDURE voeg_toe
  ( b_tekst IN OUT VARCHAR2
  , b_toevoeging IN VARCHAR2
  )
  IS
  BEGIN
    IF ( b_toevoeging IS NOT NULL )
    THEN
      IF ( b_tekst IS NULL )
      THEN
        b_tekst := b_toevoeging;
      ELSIF ( INSTR( b_tekst, b_toevoeging ) = 0 )
      THEN
        b_tekst := b_tekst
                || CHR(10)
                || b_toevoeging
                 ;
      END IF;
    END IF;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      NULL;
  END voeg_toe;
BEGIN
  IF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_ongr_id
    ,     v_afgerond;
    CLOSE onde_cur;
  END IF;

  IF ( v_ongr_id IS NULL
    OR v_afgerond = 'J'
     )
  THEN
    RETURN;
  END IF;

  FOR reui_rec IN reui_cur
  LOOP
    v_resultaat := NULL;
    v_meetwaarde := NULL;
    -- als stoftest EN stoftestgroep leeg is telt de rij voor alle stoftestgroepen!
    IF ( reui_rec.stof_id IS NULL )
    THEN
      FOR meti_rec IN meti_cur( reui_rec.stgr_id )
      LOOP
        if ( reui_rec.als_afgerond = 'N'
          or meti_rec.afgerond = 'J'
           )
        then
          voeg_toe( v_resultaat, meti_rec.conclusie );
        end if;
      END LOOP;
    ELSE  -- stoftest is not null
      FOR meet_rec IN meet_cur( reui_rec.stgr_id, reui_rec.stof_id )
      LOOP
        if ( reui_rec.als_afgerond = 'N'
          or meet_rec.afgerond = 'J'
           )
        then
          voeg_toe( v_meetwaarde, meet_rec.meetwaarde );
        end if;
      END LOOP;
    END IF;

    -- tekst wel als er geen <RESULTAAT> of <MEETWAARDE> verwijzing in staat
    if ( instr( reui_rec.tekst, '<RESULTAAT>' ) > 0
      or instr( reui_rec.tekst, '<MEETWAARDE>' ) > 0
       )
    then
      if ( v_resultaat is not null
        or v_meetwaarde is not null
         )
      then
        reui_rec.tekst := REPLACE( reui_rec.tekst, '<RESULTAAT>', v_resultaat );
        reui_rec.tekst := REPLACE( reui_rec.tekst, '<MEETWAARDE>', v_meetwaarde );
      else
        reui_rec.tekst := null;
      end if;
    else
      null; -- neem tekst over
    END IF;
    if ( reui_rec.tekst is not null )
    then
      IF ( reui_rec.in_tekst = 'J' )
      THEN
        voeg_toe( x_tekst, reui_rec.tekst );
      END IF;
      IF ( reui_rec.in_toelichting = 'J' )
      THEN
        voeg_toe( x_toelichting, reui_rec.tekst );
      END IF;
    END IF;
  END LOOP;

END uitslag;
END kgc_reui_00;
/

/
QUIT
