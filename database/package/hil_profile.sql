CREATE OR REPLACE PACKAGE "HELIX"."HIL_PROFILE" IS

-- PL/SQL Specification
/*****************************************************************************************
Purpose  Interface Package to fetch profile values.

         Currently, Headstart only supports end user preferences
         End user preferences are stored per user in table QMS_USER_OPTIONS, so the
         only parameters required are the user name and profile name.

         This interface to profiles has a number of additional parameters which
         anticipate on more sophisticated storage of end user preferences and
         profiles in general. In particular, this interface is designed to support
         the generic profiles data structure used in APPS/AOL and IAD/CFD, which allows
         to use profile values at site level, application level, user role level
         and user level.

         This powerful and flexible profile system is likely to be implemented in a
         future version.

         The advantage of having this Profiles interface in place right now is twofold:
         - The client-side template package libraries (qmslib..) can function independent
           of the underlying table structure used to store the profile values. If this
           structure changes, no modifications whatsoever are required in the
           template package libraries.
         - Headstart customers might have their own profiles system in place, and want to
           use this system instead of the QMS_USER_OPTIONS table. To use their own system
           they only need to replace the call to QMS_PROFILE.GET_PROFILE_VALUE in
           function GET_PROFILE_VALUE with a call to their own function which uses their
           own data structure.

*****************************************************************************************/

type profile_rectype is record
( name  varchar2(50)
, value varchar2(500)
);

type profile_tabtype is table of profile_rectype
     index by binary_integer;

function revision
return varchar2;
--
-- Purpose: Return revision number of the package
--

function get_profile_value
(        p_profile_name in varchar2
,        p_user_name in varchar2
)
return varchar2;
-- 16-12-1999 WP vd Lugt Added PRAGMA RESTRICT_REFERENCES (get_profile_value, WNDS)
--
PRAGMA RESTRICT_REFERENCES (get_profile_value, WNDS);

/* 16-12-1999 WP vd Lugt Removed unused function causing PRAGMA errors
function get_profile_value
(        p_profile_name in varchar2
,        p_appl_id in number default 0
,        p_role_id in number default 0
,        p_user_id in number default 0
,        p_get_lowest_value in varchar2 default 'Y'
)
return varchar2;
*/

--
-- Purpose: Overloaded function to return profile value
--          Overloading is required because current Headstart implementation of
--          user prefs through QMS_USER_OPTIONS table, uses the Oracle username as
--          the primary key. It is likely that many customers will have a
--          separate user table, with a user ID, and prefer to access profiles
--          through this ID.
--
-- Arguments:
--    p_profile_name     - name of the profile, for example HIGHLIGHT_REQUIRED_ITEMS.
--    p_user_name        - Oracle user name the profile value must be looked up for
--    p_appl_id          - Application ID that must be used to look up a profile value
--    p_role_id          - User Role ID that must be used to look up a profile value
--    p_user_id          - User ID that must be used to look up a profile value
--    p_get_lowest_value - Applicable if a profile can be stored at multiple levels
--

function get_user_profiles
(p_user_name in varchar2
)
return hil_profile.profile_tabtype;

function get_user_profiles
(p_user_id in number
)
return hil_profile.profile_tabtype;

--
-- Purpose: Overloaded function to return all user level profile settings
--          Profile values are stored in PL/SQL table
--
-- Arguments:
--    p_user_name        - Oracle user name the profile value must be looked up for
--    p_user_id          - User ID that must be used to look up a profile value
--

-- For some reason, we need to close the package specification here. Designer will remove it
-- when it generates the .pkb file.
END HIL_PROFILE;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."HIL_PROFILE" IS
-- PL/SQL Block
/*****************************************************************************************
Created by      Steven Davelaar, Oracle Custom Development Center of Excellence

Date created    4 august 1998

Change History

When        Who                            program unit / construct
            What
16-dec-1999 WP vd Lugt                     get_profile_value
  1.1       removed unused function causing PRAGMA errors
*****************************************************************************************/

REVISION_LABEL   constant   varchar2(10)   := '6.5.0.1';

function revision
return varchar2
is
begin
   return (REVISION_LABEL);
end revision;

function get_profile_value
(        p_profile_name in varchar2
,        p_user_name in varchar2
)
return varchar2
is
begin
   return qms_profile.get_profile_value(p_profile_name, p_user_name);
end get_profile_value;

/* 16-12-1999 WP vd Lugt Removed unused function causing PRAGMA errors
function get_profile_value
(        p_profile_name in varchar2
,        p_appl_id in number default 0
,        p_role_id in number default 0
,        p_user_id in number default 0
,        p_get_lowest_value in varchar2 default 'Y'
)
return varchar2
is
begin
   qms$errors.internal_error('hil_profile.get_profile_value invoked with parameter set which is not supported');
end;
*/

function get_user_profiles
(p_user_name in varchar2
)
return hil_profile.profile_tabtype
is
begin
   return qms_profile.get_user_profiles(p_user_name);
end get_user_profiles;

function get_user_profiles
(p_user_id in number
)
return hil_profile.profile_tabtype
is
begin
   qms$errors.internal_error('hil_profile.get_user_profiles invoked with parameter set which is not supported');
end get_user_profiles;

-- For some reason, we need to close the package body here. Designer will remove it
-- when it generates the .pkb file.
END HIL_PROFILE;
/

/
QUIT
