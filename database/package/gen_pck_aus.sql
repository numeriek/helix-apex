CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_AUS"
IS
   /*
   =============================================================================
   Package Name : gen_pck_aur
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_users
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   PROCEDURE p_ins_gen_appl_users(i_aus_rec IN gen_appl_users%ROWTYPE);
   --
   PROCEDURE p_del_gen_appl_users(i_aus_id  IN gen_appl_users.id%TYPE
                               ,i_json    IN BOOLEAN DEFAULT FALSE);
END gen_pck_aus;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_AUS"
IS
   /*
   =============================================================================
   Package Name : gen_pck_aur
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_users
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */

   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   --
   PROCEDURE p_ins_gen_appl_users(i_aus_rec IN gen_appl_users%ROWTYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'p_ins_gen_appl_users';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      NULL;
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   END p_ins_gen_appl_users;
   PROCEDURE p_del_gen_appl_users(i_aus_id  IN gen_appl_users.id%TYPE
                               ,i_json    IN BOOLEAN DEFAULT FALSE)
   IS
      l_scope      CONSTANT VARCHAR2(61) := g_package || 'p_del_gen_appl_users';
      e_delete_not_allowed  EXCEPTION;
      PRAGMA EXCEPTION_INIT(e_delete_not_allowed
                           ,-2292);
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      DELETE FROM gen_appl_user_roles aur
            WHERE aur.appl_user_id = i_aus_id;
      DELETE FROM gen_appl_users
            WHERE id = i_aus_id;
      IF i_json
      THEN
         gen_pck_exception.p_json_error('');
      END IF;
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN e_delete_not_allowed
      THEN
         ROLLBACK;
         logger.log_error(p_text => 'p_del_gen_appl_users'
                         ,p_scope => l_scope);
         IF i_json
         THEN
            gen_pck_exception.p_json_error(gen_pck_exception.f_get_err_message(
                                            SQLERRM
                                           ,v('LANGCODE')
                                           ,'N'
                                           ,'N'));
         ELSE
            RAISE;
         END IF;
      WHEN OTHERS
      THEN
         ROLLBACK;

         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);

         IF i_json
         THEN
            gen_pck_exception.p_json_error('GEN-00000');
         ELSE
            RAISE;
         END IF;
   END p_del_gen_appl_users;
--
END gen_pck_aus;
/

/
QUIT
