CREATE OR REPLACE PACKAGE "HELIX"."BAS_MEET_00" IS
/******************************************************************************
 NAME: BAS_MEET_00
REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.3        31-03-2016   Athar Shaikh     Mantis 3195: Enzym bijlage brief: normaalwaarden  met de juiste aantallen  decimalen tonen
                                               In function normaalwaarden added parameter p_aantal_decemalen
  *****************************************************************************/
-- ALGEMEEN:
-- toon de juiste meetwaarde of de uitslag (berekening) van een aangemelde stoftest
function toon_meetwaarde
( p_meet_id in number
, p_meet_bere in varchar2 := null -- MEET=alleen meetwaarde/tonen_als, BERE=alleen berekening, leeg=berekening, tenzij tonen_als apart is aangegegeven (als geen berekening dan alsnogmeetwaarde)
, p_type in varchar2
)
return varchar2;

-- rond de invoer van een waarde op de geeiste nauwkeurigheid af
FUNCTION formatteer
( p_meet_id IN NUMBER
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2;
-- PRAGMA RESTRICT_REFERENCES (formatteer, WNDS, WNPS, RNPS  );
-- is meetwaarde / meting afgerond?
FUNCTION afgerond
( p_meet_id IN NUMBER
)
RETURN  VARCHAR2;
PRAGMA  RESTRICT_REFERENCES  (afgerond,  WNDS,  WNPS,  RNPS  );
-- is de meetwaarde nog wijzigbaar??
FUNCTION wijzigbaar
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;
-- Haal gerelateerde stoftest van een afdeling
PROCEDURE gerelateerde_stoftest
( p_kafd_id IN NUMBER
, x_stof_id OUT NUMBER
, x_kop OUT VARCHAR2
);
-- is bijbehorende voortest succesvol verlopen?
FUNCTION voortest_ok
( p_meti_id IN NUMBER
, p_stof_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( voortest_ok, WNDS, WNPS );
-- retourneer de werklijst(en) waarop de aangemelde stoftest staat
FUNCTION werklijst
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( werklijst, WNDS, WNPS );
-- controleer of fractie/stoftest al eerder is getest
FUNCTION eerder_getest
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( eerder_getest, WNDS, WNPS );
-- vervolgactiviteiten bij een samengestelde stoftest (aanroep in AUS-trigger van basmeetwaarden)
PROCEDURE samenstelling
( p_actie IN VARCHAR2 -- AFRONDEN, AFWIJKING
, p_meti_id IN NUMBER
, p_stof_id IN NUMBER
, p_meet_id IN NUMBER := NULL -- aangemelde substoftest
);
-- retourneer J als de test in duplo is aangemeld (volgens meetwaarde-structuuur)
FUNCTION herhaling
( p_meet_id IN NUMBER
)
RETURN VARCHAR2; -- J/N
PRAGMA  RESTRICT_REFERENCES  (herhaling,  WNDS,  WNPS,  RNPS  );
-- retourneer een tekst die beschrijft hoe een protocol/stoftest is aangemeld
-- inclusief aanduiding voor kleurgebruik (zie scherm basmeti03)
FUNCTION aangemeld
( p_meti_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
)
RETURN VARCHAR2; -- J/N
PRAGMA  RESTRICT_REFERENCES  (aangemeld,  WNDS,  WNPS,  RNPS  );
-- de meetwaarde in BAS_MEETWAARDEN wordt zo mogelijk gekopieerd naar een van de details
PROCEDURE kopieer_meetwaarde
( p_meet_id IN NUMBER
, p_waarde IN VARCHAR2
);
-- soms is de meetwaarde niet betekenisvol en wordt anders uitgedrukt (bv. ND of <10)
-- vanwege het mogelijke gebruik van een formule is de werkelijke functie in bas_bere_00 gezet
PROCEDURE bepaal_tonen_als
( p_meet_id IN NUMBER
, p_meetwaarde IN VARCHAR2
);
-- als de rapportagegrenzen wijzigen, moet "tonen_als" voor nog niet afgeronde meetwaarden opnieuw worden bepaald
PROCEDURE doorrekenen_tonen_als
( p_stof_id IN NUMBER
, x_aantal OUT VARCHAR2
);

-- MEETWAARDEN:
-- Haal meetwaarde op van een stoftest bij een monster / fractie / meting
procedure meetwaarde
( p_meet_id in number
, x_waarde out varchar2 -- meetwaarde (tonen_als) of uitslag van berekening
, x_eenheid out varchar2
);
FUNCTION  meetwaarde
( p_stof_id IN NUMBER
, p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_frac_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_type    IN VARCHAR2 := NULL
)
RETURN  VARCHAR2;
-- Haal (berekende) uitslag op van een stoftest bij een aangemelde stofteste
PROCEDURE meetwaarde_uitslag
( p_meet_id IN number
, x_uitslag IN OUT varchar2
, x_uitslageenheid IN OUT varchar2
);
-- bepaal hoe een meetwaarde van een aangemelde stoftest moet worden getoond
PROCEDURE bepaal_uitslag_en_eenheid
( p_meet_id IN NUMBER
, x_uitslag OUT VARCHAR2
, x_eenheid OUT VARCHAR2
);
-- idem, overloaded, retoourneerd of de uitslag of de eenheid
FUNCTION bepaal_uitslag_of_eenheid
( p_meet_id IN NUMBER
, p_type IN VARCHAR2
)
RETURN VARCHAR2;

-- NORMAALWAARDEN:
-- bepaal de normaalwaarde (ondergrens controle range)
FUNCTION normaalwaarde_onder
( p_stof_id IN NUMBER
, p_meti_id IN NUMBER
, p_bety_id IN NUMBER := NULL
, p_normaalwaarde IN NUMBER := NULL
)
RETURN  NUMBER;
-- bepaal de normaalwaarde (bovengrens controle range)
FUNCTION normaalwaarde_boven
( p_stof_id IN NUMBER
, p_meti_id IN NUMBER
, p_bety_id IN NUMBER := NULL
, p_normaalwaarde IN NUMBER :=  NULL
)
RETURN  NUMBER;
FUNCTION normaalwaarde_gemiddeld
( p_stof_id IN NUMBER
, p_meti_id IN NUMBER
, p_bety_id IN NUMBER := NULL
, p_normaalwaarde IN NUMBER := NULL
)
RETURN NUMBER;
-- normaalwaarden van een meetwaarde
FUNCTION normaalwaarden
( p_meet_id IN NUMBER
)
RETURN  VARCHAR2;
PRAGMA  RESTRICT_REFERENCES ( normaalwaarden, WNDS, WNPS, RNPS );
-- normaalwaarden van een meetwaarde, plus extra parameter
FUNCTION normaalwaarden
( p_meet_id IN NUMBER
, p_met_eenheid IN VARCHAR2
, p_aantal_decemalen IN NUMBER := NULL -- Added by Athar Shaikh for Mantis-3195 on 31-03-2016
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES ( normaalwaarden, WNDS, WNPS, RNPS );

-- Haal meetwaarde op van een stoftest bij een monster
FUNCTION meetwaarde_bij_monster
(p_onmo_id IN NUMBER
,p_stof_id IN NUMBER
,p_type    IN VARCHAR2 -- WAARDE of EENHEID
)
RETURN VARCHAR2;

-- mantis 1145
-- overloaded functie. Ook stofgroep gebruiken.
FUNCTION meetwaarde_bij_monster
(p_onmo_id IN NUMBER
,p_stof_id IN NUMBER
,p_stgr_id in number
,p_type    IN VARCHAR2 -- MEETWAARDE, WAARDE (meetwaarde of berekening), EENHEID of ONDERGRENS of BOVENGRENS
)
RETURN VARCHAR2;

FUNCTION meetwaarde_spoed
( p_meet_id number )
RETURN varchar2;
END BAS_MEET_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_MEET_00" IS
 /******************************************************************************
 NAME: BAS_MEET_00
REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.3        31-03-2016   Athar Shaikh     Mantis 3195: Enzym bijlage brief: normaalwaarden  met de juiste aantallen  decimalen tonen
                                               In function normaalwaarden added parameter p_aantal_decemalen
      1.2        23-12-2015   SKA              mantis 3957: In procedure gerelateerde_stoftest variable
                                               v_stof_code width is changed from 10-25 chr.
  *****************************************************************************/
-- lokale procedure om meetwaarde / uitslag op te halen (gebruikt in een aantal functies)
procedure toon_meetwaarde
( p_meet_id in number
, p_meet_bere in varchar2 := null -- MEET=alleen meetwaarde/tonen_als, BERE=alleen berekening, leeg=berekening, tenzij tonen_als apart is aangegegeven (als geen berekening dan alsnogmeetwaarde)
, x_waarde out varchar2 -- meetwaarde (tonen_als) of uitslag van berekening
, x_eenheid out varchar2
)
is
  CURSOR meet_cur
  ( b_meet_id in number
  )
  IS
    SELECT meet.meetwaarde
    ,      meet.tonen_als
    ,      meet.meeteenheid  -- eenheid van stoftest
    ,      meet.eenheid  -- eenheid van "tonen als" of van stoftest
    FROM   bas_meetwaarden meet
    WHERE  meet.meet_id = b_meet_id
    ;
  meet_rec meet_cur%rowtype;
  CURSOR bere_cur
  ( b_meet_id in number
  )
  IS
    SELECT bere.uitslag
    ,      bere.uitslageenheid
    ,      bere.bere_id
    FROM   bas_berekeningen bere
    WHERE  bere.meet_id = b_meet_id
    ORDER BY bere.bere_id DESC -- pak er maar eentje (de laatste)
    ;
  bere_rec bere_cur%rowtype;
  v_meetwaarde_is_tonen_als boolean; -- als meetwaarde <> tonen_als, dan is er spraken van een "alternatieve" waarde: geen berekeningen uitvoeren
BEGIN
  if ( p_meet_id is null )
  then
    return;
  end if;
  OPEN  meet_cur( p_meet_id );
  FETCH meet_cur
  INTO  meet_rec;
  CLOSE meet_cur;
  v_meetwaarde_is_tonen_als := nvl( meet_rec.meetwaarde, chr(2) ) = nvl( meet_rec.tonen_als, chr(2) );
  if ( nvl( p_meet_bere, 'BERE' ) = 'BERE' )
  then
    if ( v_meetwaarde_is_tonen_als )
    then
      OPEN  bere_cur( p_meet_id );
      FETCH bere_cur
      INTO  bere_rec;
      IF  ( bere_cur%found )
      THEN
        x_waarde := bas_bere_00.formatteer
                    ( p_bere_id => bere_rec.bere_id
                    , p_uitslag => bere_rec.uitslag
                    );
        x_eenheid := bere_rec.uitslageenheid;
      END IF;
      CLOSE bere_cur;
    else
      x_waarde := null;
      x_eenheid := null;
    end if;
  end if;
  if ( x_waarde is null
   and nvl( p_meet_bere, 'MEET' ) = 'MEET'
     )
  then
    if ( v_meetwaarde_is_tonen_als )
    then
      x_waarde := formatteer
                  ( p_meet_id => p_meet_id
                  , p_waarde => meet_rec.tonen_als
                  );
      x_eenheid := meet_rec.meeteenheid;
    else -- tonen_als geeft alternatieve waarde
      x_waarde := meet_rec.tonen_als;
      x_eenheid := nvl( meet_rec.eenheid, meet_rec.meeteenheid );
    end if;
  END IF;
end toon_meetwaarde;

function toon_meetwaarde
( p_meet_id in number
, p_meet_bere in varchar2 := null -- MEET=alleen meetwaarde/tonen_als, BERE=alleen berekening, leeg=berekening, tenzij tonen_als apart is aangegegeven (als geen berekening dan alsnogmeetwaarde)
, p_type in varchar2  -- WAARDE of EENHEID
)
return varchar2
is
  v_waarde varchar2(1000);
  v_eenheid varchar2(30);
begin
  toon_meetwaarde
  ( p_meet_id => p_meet_id
  , p_meet_bere => p_meet_bere
  , x_waarde => v_waarde
  , x_eenheid => v_eenheid
  );
  if ( p_type = 'WAARDE' )
  then
    return( v_waarde );
  elsif ( p_type = 'EENHEID' )
  then
    return( v_eenheid );
  elsif ( v_eenheid is not null )
  then
    return( v_waarde||' '||v_eenheid );
  else
    return( v_waarde );
  end if;
end toon_meetwaarde;

-- lokale procedure om onder- en bovengrens te bepalen
PROCEDURE bereken_normaalwaarden
( p_stof_id IN NUMBER
, p_meti_id IN NUMBER
, p_bety_id IN NUMBER := NULL
, x_onder OUT NUMBER
, x_boven OUT NUMBER
, x_gemiddeld OUT NUMBER
)
IS
  CURSOR meti_cur
  IS
    SELECT mons.kafd_id
    ,      mons.mate_id
    ,      mons.leef_id
    ,      mons.datum_afname
    ,      mons.pers_id
    ,      mons.foet_id
    ,      mons.leeftijd
    ,      meti.stgr_id
    ,      frac.frty_id
    ,      pers.geslacht
    FROM   kgc_personen pers
    ,      kgc_monsters mons
    ,      bas_fracties frac
    ,      bas_metingen meti
    WHERE  meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    mons.pers_id = pers.pers_id
    AND    meti.meti_id = p_meti_id
    ;
  meti_rec meti_cur%rowtype;

  CURSOR pers_cur
  ( l_pers_id IN NUMBER )
  IS
    SELECT geboortedatum
    FROM   kgc_personen
    WHERE  pers_id = l_pers_id
    ;
  pers_rec pers_cur%rowtype;

  CURSOR zwan_cur
  ( l_foet_id IN NUMBER )
  IS
    SELECT zwan.datum_aterm
    FROM   kgc_zwangerschappen zwan
    ,      kgc_foetussen foet
    WHERE  foet.zwan_id = zwan.zwan_id
    AND    foet.foet_id = l_foet_id
    ;
  zwan_rec zwan_cur%rowtype;

  CURSOR leef_cur
  ( l_leef_id IN NUMBER )
  IS
    SELECT NVL( ondergrens, bovengrens ) norm_aantal_dagen
    FROM   kgc_leeftijden
    WHERE  leef_id = l_leef_id
    ;
  v_aantal_dagen NUMBER;
  v_onder NUMBER;
  v_boven NUMBER;
BEGIN
  OPEN  meti_cur;
  FETCH meti_cur
  INTO  meti_rec;
  CLOSE meti_cur;

  IF ( meti_rec.leeftijd IS NOT NULL )
  THEN
    v_aantal_dagen := meti_rec.leeftijd;
  ELSIF ( meti_rec.foet_id IS NOT NULL )
  THEN
    OPEN  zwan_cur( meti_rec.foet_id );
    FETCH zwan_cur
    INTO  zwan_rec;
    CLOSE zwan_cur;
    v_aantal_dagen := ( meti_rec.datum_afname - zwan_rec.datum_aterm );
  ELSE
    OPEN  pers_cur( meti_rec.pers_id );
    FETCH pers_cur
    INTO  pers_rec;
    CLOSE pers_cur;
    v_aantal_dagen := ( meti_rec.datum_afname - pers_rec.geboortedatum );
  END IF;
  IF ( v_aantal_dagen IS NULL
   AND meti_rec.leef_id IS NOT NULL
     )
  THEN
    OPEN  leef_cur( meti_rec.leef_id );
    FETCH leef_cur
    INTO  v_aantal_dagen;
    CLOSE leef_cur;
  END IF;

  IF ( v_aantal_dagen IS NOT NULL )
  THEN
    bas_rang_00.normaalwaarden
    ( p_kafd_id => meti_rec.kafd_id
    , p_mate_id => meti_rec.mate_id
    , p_aantal_dagen => v_aantal_dagen
    , p_stof_id => p_stof_id
    , p_stgr_id => meti_rec.stgr_id
    , p_frty_id => meti_rec.frty_id
    , p_bety_id => p_bety_id
    , p_geslacht => meti_rec.geslacht
    , x_onder => x_onder
    , x_boven => x_boven
    , x_gemiddeld => x_gemiddeld
    );
  END IF;
END bereken_normaalwaarden;

-- PUBLIEK:
FUNCTION formatteer
( p_meet_id IN NUMBER
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return_waarde    VARCHAR2(1000) := p_waarde;
  v_num_waarde number;
  v_aantal_decimalen NUMBER(1);
  v_formaat varchar2(50);
  CURSOR meet_cur
  IS
    SELECT meet.stof_id
    ,      meti.stgr_id
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;

  CURSOR prst_cur
  ( b_stgr_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  IS
    SELECT NVL( prst.aantal_decimalen, stof.aantal_decimalen )
    FROM   kgc_stoftesten stof
    ,      kgc_protocol_stoftesten prst
    WHERE  prst.stof_id = stof.stof_id
    AND    prst.stgr_id = b_stgr_id
    AND    prst.stof_id = b_stof_id
    ;
  CURSOR stof_cur
  ( b_stof_id IN NUMBER
  )
  IS
    SELECT stof.aantal_decimalen
    FROM   kgc_stoftesten stof
    WHERE  stof.stof_id = b_stof_id
    ;
BEGIN
  v_num_waarde := kgc_reken.numeriek( p_waarde );
  if ( v_num_waarde is null )
  then
    return( p_waarde );
  end if;
  IF ( p_waarde IS NOT NULL
   AND p_meet_id IS NOT NULL
     )
  THEN
    OPEN  meet_cur;
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE meet_cur;
  END IF;
  IF ( meet_rec.stgr_id IS NOT NULL )
  THEN
    OPEN  prst_cur( meet_rec.stgr_id, meet_rec.stof_id );
    FETCH prst_cur
    INTO  v_aantal_decimalen;
    CLOSE prst_cur;
  ELSE
    OPEN  stof_cur( meet_rec.stof_id );
    FETCH stof_cur
    INTO  v_aantal_decimalen;
    CLOSE stof_cur;
  END IF;

  v_return_waarde := kgc_formaat_00.waarde
                     ( p_waarde => p_waarde
                     , p_aantal_decimalen => v_aantal_decimalen
                     );

  RETURN( v_return_waarde );
END formatteer;

FUNCTION afgerond
( p_meet_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR c
  IS
    SELECT meti.afgerond meti_afgerond
    ,      meet.afgerond meet_afgerond
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meet.meet_id = p_meet_id
    ;
  r c%rowtype;
BEGIN
  if ( p_meet_id is null )
  then
    v_return := 'J';
  else
    OPEN  c;
    FETCH c
    INTO  r;
    CLOSE c;
    IF ( r.meti_afgerond = 'J'
     OR  r.meet_afgerond = 'J'
       )
    THEN
      v_return := 'J';
    END IF;
  END IF;
  RETURN( v_return );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( 'N' );
END afgerond;

FUNCTION wijzigbaar
( p_meet_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'J';
  CURSOR c
  IS
    SELECT meti.afgerond meti_afgerond
    ,      meet.afgerond meet_afgerond
    ,      meet.meet_reken
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meet.meet_id = p_meet_id
    ;
  r c%rowtype;
BEGIN
  OPEN  c;
  FETCH c
  INTO  r;
  CLOSE c;
  IF ( r.meet_reken = 'R'  -- directe berekeningen blijven wijzbaar
     )
  THEN
    v_return := 'J';
  ELSIF ( r.meti_afgerond = 'J'
       OR r.meet_afgerond = 'J'
        )
  THEN
    v_return := 'N';
  END IF;
  RETURN( nvl( v_return, 'J' ) );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( 'N' );
END wijzigbaar;

PROCEDURE gerelateerde_stoftest
( p_kafd_id IN NUMBER
, x_stof_id OUT NUMBER
, x_kop OUT VARCHAR2
)
IS
  v_stof_code VARCHAR2(25);    -- Increased width from 10 to 25CHR for Mantis 3957 report basmeti53 SKA -14-12-2015
  CURSOR stof_cur
  IS
    SELECT stof_id
    ,      omschrijving
    FROM   kgc_stoftesten
    WHERE  kafd_id = p_kafd_id
    AND    code = v_stof_code
    ;
  stof_rec stof_cur%rowtype;
 BEGIN
  v_stof_code := kgc_sypa_00.standaard_waarde
                 ( p_parameter_code => 'REFERENTIE_STOFTEST'
                 , p_kafd_id => p_kafd_id
                 );
  IF ( v_stof_code IS NOT NULL )
  THEN
    OPEN  stof_cur;
    FETCH stof_cur
    INTO  stof_rec;
    CLOSE stof_cur;
    x_stof_id := stof_rec.stof_id;
    x_kop := stof_rec.omschrijving;
  END IF;
END gerelateerde_stoftest;

FUNCTION voortest_ok
( p_meti_id IN NUMBER
, p_stof_id IN NUMBER
)
RETURN   VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR stof_cur
  IS
    SELECT stof_id_voor
    FROM   kgc_stoftesten
    WHERE  stof_id = p_stof_id
    ;
  stof_rec stof_cur%rowtype;
  -- meetwaarden van de voortest
  CURSOR meet_cur
  ( b_meti_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  IS
    SELECT meet_id
    ,      afgerond
    FROM   bas_meetwaarden
    WHERE  meti_id = b_meti_id
    AND    stof_id = b_stof_id
    ;
  meet_rec meet_cur%rowtype;

  v_meet_succes BOOLEAN := FALSE;
  BEGIN
  OPEN  stof_cur;
  FETCH stof_cur
  INTO  stof_rec;
  CLOSE stof_cur;
  IF ( stof_rec.stof_id_voor IS NOT NULL )
  THEN
    OPEN  meet_cur( b_meti_id => p_meti_id
                  , b_stof_id => stof_rec.stof_id_voor
                  );
    FETCH meet_cur
    INTO  meet_rec;
    -- voortest is niet aangemeld in dezelfde meting! Mag niet, maar toch OK.
    IF ( meet_cur%notfound )
    THEN
      v_meet_succes := TRUE;
    END IF;
    LOOP
      EXIT WHEN meet_cur%notfound;
      v_meet_succes := FALSE;
      IF ( meet_rec.afgerond = 'N' )
      THEN
        v_meet_succes := FALSE;
      ELSE
        -- 1 goede meetwaarde vinden is ok.
        IF ( bas_mdok_00.meet_ok( p_meet_id => meet_rec.meet_id ) = 'J' )
        THEN
          v_meet_succes := TRUE;
        END IF;
      END IF;
      IF ( v_meet_succes )
      THEN
        EXIT; -- een goeie gevonden...
      END IF;
      FETCH meet_cur
      INTO  meet_rec;
    END LOOP;
    CLOSE meet_cur;
    IF ( v_meet_succes )
    THEN
      v_return := 'J';
    ELSE
      v_return := 'N';
    END IF;
  ELSE
    v_return := 'J';
  END IF;
  RETURN( v_return );
END voortest_ok;

FUNCTION werklijst
( p_meet_id IN NUMBER
)
RETURN  VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR wliv_cur
  IS
    SELECT wlst.werklijst_nummer
    FROM   kgc_werklijsten wlst
    ,      kgc_werklijst_items_vw wliv
    WHERE  wliv.wlst_id = wlst.wlst_id
    AND    wliv.meet_id = p_meet_id
    ;
BEGIN
  FOR r IN wliv_cur
  LOOP
    IF ( v_return IS NULL )
    THEN
      v_return := r.werklijst_nummer;
    ELSE
      v_return := v_return||', '||r.werklijst_nummer;
    END IF;
  END LOOP;
  RETURN( v_return );
EXCEPTION
  WHEN VALUE_ERROR
  THEN
    v_return := SUBSTR( v_return, 1, 96 )||' ...';
    RETURN( v_return );
END werklijst;

FUNCTION eerder_getest
( p_meet_id IN NUMBER
)
RETURN  VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR meet_cur
  IS
    SELECT meet.stof_id
    ,      meti.frac_id
    ,      meti.onde_id
    ,      meti.stan_id
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meti.meti_id = meet.meti_id
    AND    meet.meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;
  -- voorlopig is het bestaan van een ouder record voldoende
  -- voor dezelfde fractie/onderzoek of voor dezelfde standaardfractie
  -- misschien uitbreiden met: meetwaarde ingevuld, afgerond, ...
  CURSOR eerder_cur1
  IS
    SELECT NULL
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meti.meti_id = meet.meti_id
    AND    meti.frac_id = meet_rec.frac_id
    AND    meti.onde_id = meet_rec.onde_id
    AND    meet.stof_id = meet_rec.stof_id
    AND    meet.meet_id < p_meet_id
    ;
  CURSOR eerder_cur2
  IS
    SELECT NULL
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meti.meti_id = meet.meti_id
    AND    meti.stan_id = meet_rec.stan_id
    AND    meet.stof_id = meet_rec.stof_id
    AND    meet.meet_id < p_meet_id
    ;
  v_dummy VARCHAR2(1);
BEGIN
  IF ( p_meet_id IS NOT NULL )
  THEN
    OPEN  meet_cur;
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE meet_cur;
    IF ( meet_rec.frac_id IS NOT NULL
     AND meet_rec.onde_id IS NOT NULL
       )
    THEN
      OPEN  eerder_cur1;
      FETCH eerder_cur1
      INTO  v_dummy;
      IF ( eerder_cur1%found )
      THEN
        v_return := 'J';
      END IF;
      CLOSE eerder_cur1;
    ELSIF ( meet_rec.stan_id IS NOT NULL )
    THEN
      OPEN  eerder_cur2;
      FETCH eerder_cur2
      INTO  v_dummy;
      IF ( eerder_cur2%found )
      THEN
        v_return := 'J';
      END IF;
      CLOSE eerder_cur2;
    END IF;
  END IF;
  RETURN( v_return );
END eerder_getest;

PROCEDURE samenstelling
( p_actie IN VARCHAR2 -- AFRONDEN, AFWIJKING
, p_meti_id IN NUMBER
, p_stof_id IN NUMBER
, p_meet_id IN NUMBER := NULL
)
IS
  -- bijbehorende (sub)stoftesten in dezelfde meting worden ook afgerond
  CURSOR subs_cur
  IS
    SELECT meet.meet_id
    FROM   kgc_sub_stoftesten subs
    ,      bas_meetwaarden meet
    WHERE  meet.stof_id = subs.stof_id_sub
    AND    subs.stof_id_super = p_stof_id
    AND    meet.meti_id = p_meti_id
    AND    meet.afgerond = 'N'
    ;
  -- afwijking bij substoftest wordt geregistreerd in samenstelling (afwijking=J)
  -- de stoftest wordt daarbij gemeld in het commentaarveld
  -- alleen de laatst ingevoerde (stoftesten worden soms vaker aangemeld in dezelfde meting)
  CURSOR super_cur
  IS
    SELECT meet.meet_id
    ,      meet.commentaar
    ,      meet.afgerond
    ,      stof.omschrijving stof_omschrijving
    FROM   kgc_stoftesten stof
    ,      kgc_sub_stoftesten subs
    ,      bas_meetwaarden meet
    WHERE  meet.stof_id = subs.stof_id_super
    AND    subs.stof_id_sub = stof.stof_id
    AND    stof.stof_id = p_stof_id
    AND    meet.meti_id = p_meti_id
    ORDER BY meet.meet_id DESC -- laatste eerst!
    ;
  super_rec super_cur%rowtype;

  v_aantal_underscores NUMBER;
  v_afwijking VARCHAR2(100);
  v_ok VARCHAR2(1) := 'J';
  v_sep CONSTANT VARCHAR2(1) := ';';
BEGIN
  IF ( UPPER( p_actie ) = 'AFRONDEN' )
  THEN
    FOR r IN subs_cur
    LOOP
      UPDATE bas_meetwaarden
      SET    afgerond = 'J'
      WHERE  meet_id = r.meet_id
      ;
    END LOOP;
  ELSIF ( UPPER( p_actie ) = 'AFWIJKING' )
  THEN
    OPEN  super_cur;
    FETCH super_cur
    INTO  super_rec;
    IF ( super_cur%found )
    THEN
      v_ok := bas_mdok_00.meet_ok( p_meet_id => p_meet_id );
      IF ( v_ok = 'N' )
      THEN
        v_aantal_underscores := kgc_util_00.num_of_occur( super_rec.stof_omschrijving, '_' );
        IF ( v_aantal_underscores = 0 )
        THEN
          v_afwijking := super_rec.stof_omschrijving || v_sep;
        ELSIF ( v_aantal_underscores = 1 )
        THEN -- alles na de 1e underscore
          v_afwijking := SUBSTR( super_rec.stof_omschrijving, INSTR( super_rec.stof_omschrijving, '_', 1, 1 ) + 1 ) || v_sep;
        ELSE -- alles na de 2e underscore
          v_afwijking := SUBSTR( super_rec.stof_omschrijving, INSTR( super_rec.stof_omschrijving, '_', 1, 2 ) + 1 ) || v_sep;
        END IF;
        IF ( super_rec.commentaar IS NULL )
        THEN
          super_rec.commentaar := v_afwijking;
        ELSIF ( INSTR( super_rec.commentaar, v_afwijking ) = 0 )
        THEN
          super_rec.commentaar := RTRIM( super_rec.commentaar, v_sep ) || v_sep || v_afwijking;
        END IF;

        -- zet afwijkend bij samenstelling op Ja en verander het commentaar
        IF ( super_rec.afgerond = 'J' )
        THEN
          -- even op niet-afgerond...
          UPDATE bas_meetwaarden
          SET    afgerond = 'N'
          WHERE  meet_id = super_rec.meet_id
          ;
        END IF;

        UPDATE bas_meetwaarden
        SET    commentaar = super_rec.commentaar
        WHERE  meet_id = super_rec.meet_id
        AND    NVL(commentaar,CHR(2)) <> NVL(super_rec.commentaar,CHR(2))
        ;
        UPDATE bas_meetwaarde_details
        SET    waarde = 'J'
        WHERE  meet_id = super_rec.meet_id
        AND    UPPER(prompt) LIKE 'AFWIJK%'
        ;
        IF ( super_rec.afgerond = 'J' )
        THEN
          -- ... en weer terug.
          UPDATE bas_meetwaarden
          SET    afgerond = 'J'
          WHERE  meet_id = super_rec.meet_id
          ;
        END IF;
      END IF;
      CLOSE super_cur;
    END IF;
  END IF;
END samenstelling;

FUNCTION herhaling
( p_meet_id IN NUMBER
)
RETURN VARCHAR2 -- J/N
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR mdet_cur
  IS
    SELECT 'J'
    FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
    WHERE  meet.mwst_id = mwss.mwst_id
    AND    meet.meet_id = mdet.meet_id
    AND    mdet.volgorde = mwss.volgorde
    AND    mwss.aanmelden > 0
    AND    meet.meet_id = p_meet_id
    ;
BEGIN
  IF ( p_meet_id IS NOT NULL )
  THEN
    OPEN  mdet_cur;
    FETCH mdet_cur
    INTO  v_return;
    CLOSE mdet_cur;
  END IF;
  RETURN( v_return );
END herhaling;

FUNCTION aangemeld
( p_meti_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  CURSOR meti_cur
  IS
    SELECT meti.meti_id
    ,      meti.prioriteit
    ,      meti.herhalen
    ,      meti.afgerond
    FROM   bas_metingen meti
    WHERE  meti.meti_id = p_meti_id
    ;
  meti_rec meti_cur%rowtype;
  CURSOR meet_cur
  IS
    SELECT meet.meet_id
    ,      DECODE( meet.spoed,'J','J',meti.prioriteit) prioriteit
    ,      meti.stgr_id
    ,      DECODE( meet.afgerond, 'J', 'J', meti.afgerond ) afgerond
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meti.meti_id = meet.meti_id
    AND    meet.meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;
  -- voorzie de kleur van een tag
  FUNCTION tag
  ( b_kleur IN VARCHAR2
  )
  RETURN VARCHAR2
  IS
  BEGIN
    RETURN( '<KLEUR>'||b_kleur||'</KLEUR>' );
  END tag;
BEGIN
  IF ( p_meet_id IS NOT NULL )
  THEN
    OPEN  meet_cur;
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE meet_cur;
    IF ( meet_rec.afgerond = 'J' )
    THEN
      v_return := tag('GROEN')||'Afgerond';
    ELSE
      IF ( meet_rec.stgr_id IS NOT NULL )
      THEN
        v_return := tag('GEEL')||'In protocol';
      ELSIF ( meet_rec.meet_id IS NOT NULL )
      THEN
        v_return := tag('GEEL')||'Aangemeld';
      END IF;
      IF ( v_return IS NOT NULL )
      THEN
        IF ( meet_rec.prioriteit = 'J' )
        THEN
          v_return := v_return || ' (!)';
        END IF;
        IF ( herhaling( p_meet_id => meet_rec.meet_id ) = 'J' )
        THEN
          v_return := v_return || ' (hh)';
        END IF;
      END IF;
    END IF;
  ELSIF ( p_meti_id IS NOT NULL )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  meti_rec;
    CLOSE meti_cur;
    IF ( meti_rec.afgerond = 'J' )
    THEN
      v_return := tag('GROEN')||'Afgerond';
    ELSE
      IF ( meti_rec.meti_id IS NOT NULL )
      THEN
        v_return := tag('GEEL')||'Aangemeld';
      END IF;
      IF ( v_return IS NOT NULL )
      THEN
        IF ( meti_rec.prioriteit = 'J' )
        THEN
          v_return := v_return || ' (!)';
        END IF;
        IF ( meti_rec.herhalen = 'J' )
        THEN
          v_return := v_return || ' (hh)';
        END IF;
      END IF;
    END IF;
  END IF;
  RETURN( v_return );
END aangemeld;

PROCEDURE kopieer_meetwaarde
( p_meet_id IN NUMBER
, p_waarde  IN VARCHAR2
)
IS
  cursor mwss_cur
  is
    select mwss.volgorde
    from   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarden meet
    where  meet.mwst_id = mwss.mwst_id
    and    meet.meet_id = p_meet_id
    and    mwss.gebruik is not null
    -- voorkomen moet worden dat als een detail wordt gewijzigd,
    -- via meetwaarde een ander detail wordt geupdate
    and    not exists
           ( select null
             from   bas_meetwaarde_details mdet
             where  mdet.meet_id = meet.meet_id
             and    mdet.volgorde <> mwss.volgorde
             and    nvl( waarde, chr(2) ) = nvl( p_waarde, chr(2) )
           )
    order by mwss.gebruik
    ;
  mwss_rec mwss_cur%rowtype;
BEGIN
  open  mwss_cur;
  fetch mwss_cur
  into  mwss_rec;
  close mwss_cur;
  if ( mwss_rec.volgorde is not null )
  then
    update bas_meetwaarde_details mdet
    set    mdet.waarde = p_waarde
    where  mdet.volgorde = mwss_rec.volgorde
    and    mdet.meet_id = p_meet_id
    and    nvl( mdet.waarde, chr(2) ) <> nvl( p_waarde, chr(2) )
    -- een niet-ok detail mag niet worden veranderd (zie bas_mdet_00.zet_meetwaarde)
    and    not exists
           ( select null
             from   bas_mdet_ok mdok
             where  mdok.mdet_id = mdet.mdet_id
             and    mdok.ok = 'N'
           )
    ;
  end if;
END kopieer_meetwaarde;

PROCEDURE bepaal_tonen_als
( p_meet_id IN NUMBER
, p_meetwaarde IN VARCHAR2
)
IS
  v_tonen_als varchar2(1000) := NULL;
  v_eenheid varchar2(30) := NULL;
  cursor eenh_cur
  is
    select nvl( prst.eenheid, stof.eenheid ) eenheid
    from   kgc_protocol_stoftesten prst
    ,      kgc_stoftesten stof
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    where  meet.stof_id = stof.stof_id
    and    meet.meti_id = meti.meti_id
    and    meti.stgr_id = prst.stgr_id (+)
    and  ( stof.stof_id = prst.stof_id
        or meti.stgr_id is null
         )
    and    meet.meet_id = p_meet_id
    ;
begin
  bas_bere_00.bepaal_tonen_als
  ( p_meet_id => p_meet_id
  , p_meetwaarde => p_meetwaarde
  , x_tonen_als => v_tonen_als
  , x_eenheid => v_eenheid
  );
--raise_application_error( -20002,  'toon als MEET_00 ta='||v_tonen_als );
  if ( v_tonen_als is not null )
  then
    update bas_meetwaarden
    set    tonen_als = v_tonen_als
    ,      eenheid = v_eenheid  -- meeteenheid blijft ongewijzigd!
    where  meet_id = p_meet_id
    and    nvl( tonen_als, chr(2) ) <> nvl( v_tonen_als, chr(2) )
    ;
  else
    open  eenh_cur;
    fetch eenh_cur
    into  v_eenheid;
    close eenh_cur;
    update bas_meetwaarden
    set    tonen_als = meetwaarde
    ,      meeteenheid = v_eenheid -- normaliter overbodig correctie!
    ,      eenheid = v_eenheid
    where  meet_id = p_meet_id
    and    nvl( tonen_als, chr(2) ) <> nvl( meetwaarde, chr(2) )
    ;
  end if;
end bepaal_tonen_als;

PROCEDURE doorrekenen_tonen_als
( p_stof_id IN NUMBER
, x_aantal OUT VARCHAR2
)
IS
  cursor meet_cur
  is
        SELECT distinct meet.meet_id meet_id
        ,      meet.meetwaarde meetwaarde
    FROM   bas_metingen meti
        ,      kgc_onderzoeken onde
    ,      bas_meetwaarden meet
    ,      kgc_toon_meetwaarde_als tmwa
    WHERE  tmwa.stof_id = meet.stof_id
    AND    meet.meti_id = meti.meti_id
    AND    nvl(meti.stgr_id,-9999999999) = nvl(tmwa.stgr_id,nvl(meti.stgr_id,-9999999999) )
        AND    meti.onde_id = onde.onde_id (+)
    AND    meet.afgerond = 'N'
    AND    meti.afgerond = 'N'
    AND    tmwa.vervallen = 'N'
        AND    nvl(onde.afgerond,'N') = 'N'
    AND    tmwa.stof_id = p_stof_id
        AND    meet.meetwaarde is not null
        ;
  v_aantal NUMBER := 0;
BEGIN
  FOR meet_rec IN meet_cur
  LOOP
    bepaal_tonen_als
    ( p_meet_id => meet_rec.meet_id
    , p_meetwaarde => meet_rec.meetwaarde
    );
    v_aantal := v_aantal + 1;
  END LOOP;
  x_aantal := v_aantal;
END doorrekenen_tonen_als;

procedure meetwaarde
( p_meet_id in number
, x_waarde out varchar2 -- meetwaarde (tonen_als)
, x_eenheid out varchar2
)
is
begin
  toon_meetwaarde
  ( p_meet_id => p_meet_id
  , p_meet_bere => 'MEET'
  , x_waarde => x_waarde
  , x_eenheid => x_eenheid
  );
end meetwaarde;

FUNCTION meetwaarde
( p_stof_id IN NUMBER
, p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_frac_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_type    IN VARCHAR2 := NULL
)
RETURN VARCHAR2 -- beschreven meetwaarde
IS
  CURSOR meti_cur
  IS
    SELECT meet.meet_id
    FROM   bas_meetwaarden meet
    WHERE  meet.meti_id = p_meti_id
    AND    meet.stof_id = p_stof_id
    ORDER BY meet.datum_meting DESC
    ,        meet.meet_id desc
    ;
  CURSOR frac_cur
  IS
    SELECT meet.meet_id
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.frac_id = p_frac_id
    AND    meet.stof_id = p_stof_id
    AND  ( meti.onde_id = p_onde_id or p_onde_id is null )
    ORDER BY meet.datum_meting DESC
    ,        meet.meet_id desc
    ;
  CURSOR mons_cur
  IS
    SELECT meet.meet_id
    FROM   bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  frac.frac_id = meti.frac_id
    AND    meet.meti_id = meti.meti_id
    AND    frac.mons_id = p_mons_id
    AND    meet.stof_id = p_stof_id
    AND  ( meti.onde_id = p_onde_id or p_onde_id is null )
    ORDER BY meet.datum_meting DESC
    ,        meet.meet_id DESC
    ;
  v_meet_id NUMBER;
BEGIN
  IF ( p_meti_id IS NOT NULL )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  v_meet_id;
    CLOSE meti_cur;
  ELSIF ( p_frac_id IS NOT NULL )
  THEN
    OPEN  frac_cur;
    FETCH frac_cur
    INTO  v_meet_id;
    CLOSE frac_cur;
  ELSIF ( p_mons_id IS NOT NULL )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  v_meet_id;
    CLOSE mons_cur;
  END IF;
  RETURN( toon_meetwaarde( p_meet_id => v_meet_id
                         , p_meet_bere => 'MEET'
                         , p_type => p_type
                         )
        );
END meetwaarde;

PROCEDURE meetwaarde_uitslag
( p_meet_id IN number
, x_uitslag IN OUT varchar2
, x_uitslageenheid IN OUT varchar2
)
IS
BEGIN
  toon_meetwaarde
  ( p_meet_id => p_meet_id
  , p_meet_bere => 'BERE'
  , x_waarde => x_uitslag
  , x_eenheid => x_uitslageenheid
  );
END meetwaarde_uitslag;

PROCEDURE bepaal_uitslag_en_eenheid -- basmeet03c
(p_meet_id IN NUMBER
,x_uitslag OUT VARCHAR2
,x_eenheid OUT VARCHAR2
)
IS
BEGIN
  toon_meetwaarde
  ( p_meet_id => p_meet_id
  , p_meet_bere => null
  , x_waarde => x_uitslag
  , x_eenheid => x_eenheid
  );
EXCEPTION
  WHEN OTHERS THEN
    x_uitslag := null;
    x_eenheid := null;
end bepaal_uitslag_en_eenheid;

FUNCTION  bepaal_uitslag_of_eenheid -- KGC_BASMEET53_MEET_VW, KGC_UITSLAG_BIJLAGE_UTMETAB_VW, kgcanme01 KGCANME51
( p_meet_id IN NUMBER
, p_type IN VARCHAR2
)
RETURN VARCHAR2
 IS
  v_uitslag VARCHAR2(2000);
  v_eenheid VARCHAR2(2000);
BEGIN
  toon_meetwaarde
  ( p_meet_id => p_meet_id
  , p_meet_bere => null
  , x_waarde => v_uitslag
  , x_eenheid => v_eenheid
  );

  if p_type = 'UITSLAG'
  then
    RETURN( v_uitslag );
  elsif p_type = 'EENHEID'
  then
    RETURN( v_eenheid );
  else
    return( null );
  end if;
END bepaal_uitslag_of_eenheid;

FUNCTION normaalwaarde_onder
( p_stof_id IN NUMBER
, p_meti_id IN NUMBER
, p_bety_id IN NUMBER := NULL
, p_normaalwaarde IN NUMBER := NULL
)
RETURN   NUMBER
IS
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
BEGIN
  IF ( p_normaalwaarde IS NOT NULL )
  THEN
    v_onder := p_normaalwaarde;
  ELSE
    bereken_normaalwaarden
    ( p_stof_id => p_stof_id
    , p_meti_id => p_meti_id
    , p_bety_id => p_bety_id
    , x_onder => v_onder
    , x_boven => v_boven
    , x_gemiddeld => v_gemiddeld
    );
  END IF;
  RETURN( v_onder );
END normaalwaarde_onder;

FUNCTION normaalwaarde_boven
( p_stof_id IN NUMBER
, p_meti_id IN NUMBER
, p_bety_id IN NUMBER := NULL
, p_normaalwaarde IN NUMBER := NULL
)
RETURN   NUMBER
IS
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
BEGIN
  IF ( p_normaalwaarde IS NOT NULL )
  THEN
    v_boven := p_normaalwaarde;
  ELSE
    bereken_normaalwaarden
    ( p_stof_id => p_stof_id
    , p_meti_id => p_meti_id
    , p_bety_id => p_bety_id
    , x_onder => v_onder
    , x_boven => v_boven
    , x_gemiddeld => v_gemiddeld
    );
  END IF;
  RETURN( v_boven );
END normaalwaarde_boven;

FUNCTION normaalwaarden
( p_meet_id IN NUMBER
)
RETURN   VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR meet_cur
  IS
    SELECT normaalwaarde_ondergrens
    ,      normaalwaarde_bovengrens
    ,      normaalwaarde_gemiddelde
    ,      meeteenheid
    FROM   bas_meetwaarden
    WHERE  meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;
BEGIN
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  meet_rec;
  CLOSE meet_cur;
  v_return := kgc_formaat_00.normaalwaarden
              ( p_onder => meet_rec.normaalwaarde_ondergrens
              , p_boven => meet_rec.normaalwaarde_bovengrens
              , p_gemiddeld => meet_rec.normaalwaarde_gemiddelde
              );
  RETURN( v_return );
END normaalwaarden;
FUNCTION normaalwaarden
( p_meet_id IN NUMBER
, p_met_eenheid IN VARCHAR2
, p_aantal_decemalen IN NUMBER := NULL -- Added by Athar Shaikh for Mantis-3195 on 31-03-2016
)
RETURN   VARCHAR2
IS
  v_return VARCHAR2(100);
  -- Start added by Athar Shaikh for M3195 on 31-03-2016
  v_normaalwaarde_ondergrens varchar2(100);
  v_normaalwaarde_bovengrens varchar2(100);
  -- End added by Athar Shaikh for Mantis-3195 on 31-03-2016
  CURSOR meet_cur
  IS
    SELECT normaalwaarde_ondergrens
    ,      normaalwaarde_bovengrens
    ,      normaalwaarde_gemiddelde
    ,      meeteenheid
    FROM   bas_meetwaarden
    WHERE  meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;
BEGIN
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  meet_rec;
  CLOSE meet_cur;
  -- Start added by Athar Shaikh for M3195 on 31-03-2016
  IF (p_aantal_decemalen) IS NULL OR (p_aantal_decemalen) = 0
  THEN
    v_normaalwaarde_ondergrens   := meet_rec.normaalwaarde_ondergrens;
    v_normaalwaarde_bovengrens   := meet_rec.normaalwaarde_bovengrens;
  ELSE
    v_normaalwaarde_ondergrens   := trim(to_char(meet_rec.normaalwaarde_ondergrens, (('9999999990.'|| LTRIM((Power(10, p_aantal_decemalen)),1)) )));
    v_normaalwaarde_bovengrens   := trim(to_char(meet_rec.normaalwaarde_bovengrens, (('9999999990.'|| LTRIM((Power(10, p_aantal_decemalen)),1)) )));
  END IF;
  -- End added by Athar Shaikh for M3195 on 31-03-2016
  v_return := kgc_formaat_00.normaalwaarden
              ( p_onder => v_normaalwaarde_ondergrens -- meet_rec.normaalwaarde_ondergrens commented and added v_normaalwaarde_ondergrens by Athar Shaikh for M3195 on 31-03-2016
              , p_boven => v_normaalwaarde_bovengrens -- meet_rec.normaalwaarde_bovengrens commented and added v_normaalwaarde_bovengrens by Athar Shaikh for M3195 on 31-03-2016
              , p_gemiddeld => meet_rec.normaalwaarde_gemiddelde
              );
  IF ( v_return IS NOT NULL
   AND meet_rec.meeteenheid IS NOT NULL
   AND UPPER(SUBSTR(p_met_eenheid,1,1)) = 'J'
     )
  THEN
    v_return := v_return||' '||meet_rec.meeteenheid;
  END IF;
  RETURN( v_return );
END normaalwaarden;

FUNCTION normaalwaarde_gemiddeld
( p_stof_id IN NUMBER
, p_meti_id IN NUMBER
, p_bety_id IN NUMBER := NULL
, p_normaalwaarde IN NUMBER := NULL
)
RETURN NUMBER
IS
  v_onder NUMBER;
  v_boven NUMBER;
  v_gemiddeld NUMBER;
BEGIN
  IF ( p_normaalwaarde IS NOT NULL )
  THEN
    v_gemiddeld := p_normaalwaarde;
  ELSE
    bereken_normaalwaarden
    ( p_stof_id => p_stof_id
    , p_meti_id => p_meti_id
    , p_bety_id => p_bety_id
    , x_onder => v_onder
    , x_boven => v_boven
    , x_gemiddeld => v_gemiddeld
    );
  END IF;
  RETURN( v_gemiddeld );
END normaalwaarde_gemiddeld;


FUNCTION meetwaarde_bij_monster
(p_onmo_id IN NUMBER
,p_stof_id IN NUMBER
,p_type    IN VARCHAR2 -- MEETWAARDE, WAARDE (meetwaarde of berekening), EENHEID of ONDERGRENS of BOVENGRENS
)
RETURN VARCHAR2
IS
  v_return  VARCHAR2(1000);

  CURSOR meet_cur
  (b_onmo_id IN NUMBER
  ,b_stof_id IN NUMBER
  )
  IS
    SELECT meet.meet_id
    ,      meet.normaalwaarde_ondergrens
    ,      meet.normaalwaarde_bovengrens
    FROM   bas_metingen  meti
    ,      bas_meetwaarden meet
    ,      bas_meetwaarde_statussen mest
    WHERE  meti.onmo_id    = b_onmo_id
    AND    meet.meti_id    = meti.meti_id
    AND    meet.stof_id    = b_stof_id
    AND    mest.mest_id(+) = meet.mest_id
    AND    NVL(mest.in_uitslag,'J') = 'J'
    ORDER BY DECODE (meet.tonen_als, NULL,1,-1)  -- eerst met waarde gevuld, dan leeg
    ,        meet.meet_id DESC
  ;

  meet_rec meet_cur%ROWTYPE;

  CURSOR bere_cur
  ( b_meet_id in number
  )
  IS
    SELECT bere.normaalwaarde_ondergrens
    ,      bere.normaalwaarde_bovengrens
    ,      bere.bere_id
    FROM   bas_berekeningen bere
    WHERE  bere.meet_id = b_meet_id
    ORDER BY bere.bere_id DESC -- pak er maar eentje (de laatste)
    ;
  bere_rec bere_cur%rowtype;

BEGIN
  OPEN meet_cur (b_onmo_id => p_onmo_id
                ,b_stof_id => p_stof_id
                ) ;
  FETCH meet_cur
  INTO meet_rec;
  CLOSE meet_cur;

  IF meet_rec.meet_id IS NOT NULL
  THEN
    IF p_type = 'WAARDE'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_type => 'WAARDE'
                                   )
                  );
    ELSIF p_type = 'MEETWAARDE'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_meet_bere => 'MEET'
                                   , p_type => 'WAARDE'
                                   )
                  );
    ELSIF p_type = 'UITSLAG'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_meet_bere => 'BERE'
                                   , p_type => 'WAARDE'
                                   )
                  );
    ELSIF p_type = 'EENHEID'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_type => 'EENHEID'
                                   )
                  );
    ELSIF p_type = 'ONDERGRENS'
    THEN
      OPEN  bere_cur( meet_rec.meet_id );
      FETCH bere_cur
      INTO  bere_rec;
      CLOSE bere_cur;
      v_return := nvl( bere_rec.normaalwaarde_ondergrens, meet_rec.normaalwaarde_ondergrens );
    ELSIF p_type = 'BOVENGRENS'
    THEN
      OPEN  bere_cur( meet_rec.meet_id );
      FETCH bere_cur
      INTO  bere_rec;
      CLOSE bere_cur;
      v_return := nvl( bere_rec.normaalwaarde_bovengrens, meet_rec.normaalwaarde_bovengrens );
    END IF;
  END IF;

  RETURN (v_return);
END meetwaarde_bij_monster;

FUNCTION meetwaarde_spoed
( p_meet_id number )
RETURN varchar2
IS
  v_return varchar2(1) := 'N';
  CURSOR spoed_cur
  IS
    SELECT  DECODE( onde.spoed
                  , 'J', 'J'
                  , DECODE( meti.prioriteit
                          , 'J','J'
                          , DECODE( meet.spoed
                                      , 'J', 'J'
                                  , 'N' )
                          )
                   )  spoed
    FROM    kgc_onderzoeken onde
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.onde_id = onde.onde_id (+)
    AND     meet.meet_id = p_meet_id
    ;
BEGIN
  open  spoed_cur;
  fetch spoed_cur into v_return;
  close spoed_cur;

  return( v_return);
END;


FUNCTION meetwaarde_bij_monster
(p_onmo_id IN NUMBER
,p_stof_id IN NUMBER
,p_stgr_id in number
,p_type    IN VARCHAR2 -- MEETWAARDE, WAARDE (meetwaarde of berekening), EENHEID of ONDERGRENS of BOVENGRENS
)
RETURN VARCHAR2
--
--     mantis 1145
--     08-08=-2008     In reports KGCUITS51ENZYM, KGCUITS51MAMETAB en KGCUITS52MAMETAB moet rekening
--                     gehouden worden met de stofgroep.
--
IS
  v_return  VARCHAR2(1000);

  CURSOR meet_cur
  (b_onmo_id IN NUMBER
  ,b_stof_id IN NUMBER
  ,b_stgr_id in number  --     mantis 1145
  )
  IS
    SELECT meet.meet_id
    ,      meet.normaalwaarde_ondergrens
    ,      meet.normaalwaarde_bovengrens
    FROM   bas_metingen  meti
    ,      bas_meetwaarden meet
    ,      bas_meetwaarde_statussen mest
    WHERE  meet.meti_id    = meti.meti_id
    AND    meti.onmo_id    = b_onmo_id
    AND    meet.stof_id    = b_stof_id
    AND    mest.mest_id(+) = meet.mest_id
    and    (  meti.stgr_id  = b_stgr_id
           or (   meti.stgr_id is null
              and b_stgr_id is null
              )
           )--     mantis 1145
    AND    NVL(mest.in_uitslag,'J') = 'J'
    ORDER BY DECODE (meet.tonen_als, NULL,1,-1)  -- eerst met waarde gevuld, dan leeg
    ,        meet.meet_id DESC
  ;

  meet_rec meet_cur%ROWTYPE;

  CURSOR bere_cur
  ( b_meet_id in number
  )
  IS
    SELECT bere.normaalwaarde_ondergrens
    ,      bere.normaalwaarde_bovengrens
    ,      bere.bere_id
    FROM   bas_berekeningen bere
    WHERE  bere.meet_id = b_meet_id
    ORDER BY bere.bere_id DESC -- pak er maar eentje (de laatste)
    ;
  bere_rec bere_cur%rowtype;

BEGIN

  OPEN meet_cur (b_onmo_id => p_onmo_id
                ,b_stof_id => p_stof_id
                ,b_stgr_id => p_stgr_id--     mantis 1145
                ) ;
  FETCH meet_cur  INTO meet_rec;
  CLOSE meet_cur;

  IF meet_rec.meet_id IS NOT NULL
  THEN
    IF p_type = 'WAARDE'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_type => 'WAARDE'
                                   )
                  );
    ELSIF p_type = 'MEETWAARDE'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_meet_bere => 'MEET'
                                   , p_type => 'WAARDE'
                                   )
                  );
    ELSIF p_type = 'UITSLAG'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_meet_bere => 'BERE'
                                   , p_type => 'WAARDE'
                                   )
                  );
    ELSIF p_type = 'EENHEID'
    THEN
      v_return := ( toon_meetwaarde( p_meet_id => meet_rec.meet_id
                                   , p_type => 'EENHEID'
                                   )
                  );
    ELSIF p_type = 'ONDERGRENS'
    THEN
      OPEN  bere_cur( meet_rec.meet_id );
      FETCH bere_cur   INTO  bere_rec;
      CLOSE bere_cur;
      v_return := nvl( bere_rec.normaalwaarde_ondergrens, meet_rec.normaalwaarde_ondergrens );
    ELSIF p_type = 'BOVENGRENS'
    THEN
      OPEN  bere_cur( meet_rec.meet_id );
      FETCH bere_cur  INTO  bere_rec;
      CLOSE bere_cur;
      v_return := nvl( bere_rec.normaalwaarde_bovengrens, meet_rec.normaalwaarde_bovengrens );
    END IF;
  END IF;

  RETURN (v_return);
END meetwaarde_bij_monster;
END  bas_meet_00;
/

/
QUIT
