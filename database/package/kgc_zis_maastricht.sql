CREATE OR REPLACE PACKAGE "HELIX"."KGC_ZIS_MAASTRICHT" IS
  /******************************************************************************
    NAME:      KGC_ZIS_MAASTRICHT
    PURPOSE:   Maastricht specifieke functionaliteit voor KGC_ZIS

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    5.5 	18-02-2013 SOJ	changed in "soapmsg2rec" procedure

    5.4        15-10-2012  JKO              CAST_LAND: trema verwijderd; literal is nu 'BELGIE'.
                                            BEH_SOAP_API gewijzigd naar KGC_SOAP_API_MAASTRICHT.
                                            SEQ_HELIX_ADT_WS gewijzigd naar KGC_SOAP_MAASTRICHT_SEQ.
    5.3        27-07-2012  RKON             Toepassen Dummy Record verzekeraar en huisarts
                           RKON             Overlijdensdatum toegevoegd.
                                            Commentaar toegevoegd
    5.2        26-07-2012  RKON             Aanpassingen verwerken verzekeraar.
    5.1        25-07-2012  RKON             Aanpassingen verwerken Relatie gegevens
                           RKON             Vullen van veld aanspreken toegevoegd
    5.0        23-07-2012  M vd Kamp        Huidige koppeling vervangen door webservices / soap xml uitwisseling
    4.0        04-06-2009  JKO              BSN: leeg retourneren aan Helix
    3.0        19-10-2006  RDE              WP55: telefoon2
    2.0        05-10-2006  RDE              HLX-KGC_ZIS01-11442: gehuwd-bepaling
    1.0        15-08-2006  RDE              creatie
 *****************************************************************************/

FUNCTION persoon_in_zis
( p_zisnr IN VARCHAR2
, pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
, x_msg        OUT VARCHAR2
) RETURN BOOLEAN;

PROCEDURE soapmsg2rec -- vertaal SOAP-message naar rec's
( p_soap_message IN     xmltype
, pers_trow IN OUT Cg$kgc_Personen.cg$row_type
, rela_trow IN OUT Cg$kgc_Relaties.cg$row_type
, verz_trow IN OUT Cg$kgc_Verzekeraars.cg$row_type
);

FUNCTION XML_EXTRACT_NO_ERRORS
(p_xml IN xmltype,
p_location IN varchar2
) RETURN VARCHAR2;

FUNCTION decl2dft_p03
( p_decl_id        IN     NUMBER
, p_aantal         IN     NUMBER
, p_check_only     IN     BOOLEAN
, p_msg_control_id IN     VARCHAR2
, x_msg            IN OUT VARCHAR2
) RETURN BOOLEAN;

FUNCTION Format_PersAanspreken ( p_achternaam       varchar2
                                ,P_VOORLETTERS      varchar2
                                ,p_voorvoegsel      varchar2
                                ,P_Achternaam_part  varchar2
                                ,P_voorvoegsel_part varchar2
                                )
RETURN varchar2;

FUNCTION CAST_LAND ( P_Land varchar2 )
  Return varchar2 ;
END KGC_ZIS_MAASTRICHT;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ZIS_MAASTRICHT" IS
 /******************************************************************************
    NAME:      KGC_ZIS_MAASTRICHT
    PURPOSE:   Maastricht specifieke functionaliteit voor KGC_ZIS

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    5.5 	18-02-2013 SOJ	changed in "soapmsg2rec" procedure

    5.4        15-10-2012  JKO              CAST_LAND: trema verwijderd; literal is nu 'BELGIE'
                                            BEH_SOAP_API gewijzigd naar KGC_SOAP_API_MAASTRICHT.
                                            SEQ_HELIX_ADT_WS gewijzigd naar KGC_SOAP_MAASTRICHT_SEQ.
    5.3        27-07-2012  RKON             Toepassen Dummy Record verzekeraar en huisarts
                           RKON             Overlijdensdatum toegevoegd.
                                            Commentaar toegevoegd
    5.2        26-07-2012  RKON             Aanpassingen verwerken verzekeraar.
    5.1        25-07-2012  RKON             Aanpassingen verwerken Relatie gegevens
                           RKON             Vullen van veld aanspreken toegevoegd
    5.0        23-07-2012  M vd Kamp        Huidige koppeling vervangen door webservices / soap xml uitwisseling
    4.0        04-06-2009  JKO              BSN: leeg retourneren aan Helix
    3.0        19-10-2006  RDE              WP55: telefoon2
    2.0        05-10-2006  RDE              HLX-KGC_ZIS01-11442: gehuwd-bepaling
    1.0        15-08-2006  RDE              creatie
 *****************************************************************************/

FUNCTION persoon_in_zis
( p_zisnr   IN     VARCHAR2
, pers_trow IN OUT Cg$kgc_Personen.cg$row_type
, rela_trow IN OUT Cg$kgc_Relaties.cg$row_type
, verz_trow IN OUT Cg$kgc_Verzekeraars.cg$row_type
, x_msg        OUT VARCHAR2
) RETURN BOOLEAN
IS
/* ---  AANGEPAST VERWERKT NU XML BERICHTEN DIE OPGEHAALD WORDEN AAN DE HAND VAN EEN
        WEB SERVICE OP BASIS VAN HL7 ADT 2.4 STANDAARD, ALLEEN IS HET BERICHT FORMAAT XML
        BEVAT WEL DEZELFDE VELDEN ALS IN HL7 2.4 ADT Q
*/
  v_msg_control_id  VARCHAR2(10); --moet ergens in 't bericht

  v_thisproc        VARCHAR2(100) := 'kgc_zis_maastricht.persoon_in_zis';

  l_request   kgc_soap_api_maastricht.t_request;
  l_response  kgc_soap_api_maastricht.t_response;

  l_url          VARCHAR2(32767);
  l_namespace    VARCHAR2(32767);
  l_method       VARCHAR2(32767);
  l_soap_action  VARCHAR2(32767);
  l_result_name  VARCHAR2(32767);
  s_message_uk number;
BEGIN
--select a unique key for message identification and debugging purposes
 select KGC_SOAP_MAASTRICHT_SEQ.nextval into s_message_uk from dual;

  --- blijft bestaan
  v_msg_control_id := SUBSTR(TO_CHAR(ABS(dbms_random.RANDOM)), 1, 10);

-- OPBOUWEN SOAP MESSAGE

  l_url         := 'pwioa404.prd.corp:8083/HelixWebservices-adt_webservice-context-root/HTTP_Port';
  l_namespace   := 'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"';
  l_method      := 'QRY_A19_Patient_Query';
  l_soap_action := 'HL7_V25_QRY_A19_Patient_Query_OutSyn';
  l_result_name := 'ADR_A19_Patient_Response';


  --Opbouwen van het bericht

  l_request := kgc_soap_api_maastricht.new_request(p_method       => l_method,
                                                   p_namespace    => l_namespace);

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<MSH>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Field_Separator',
                                        p_value   => '|');

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Encoding_Characters',
                                        p_value   => '^~\;');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Sending_Application>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Namespace_ID',
                                        p_value   => 'HELIX');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Sending_Application>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Sending_Facility>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Namespace_ID',
                                        p_value   => 'HELIX');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Sending_Facility>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Receiving_Facility>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Namespace_ID',
                                        p_value   => 'SAP');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Receiving_Facility>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Date_Time_Of_Message>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Time',
                                        p_value   => to_char(sysdate, 'YYYYMMDDHH24MI'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Date_Time_Of_Message>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Message_Type>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Message_Code',
                                        p_value   => to_char(sysdate, 'YYYYMMDDHH24MISS'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Message_Type>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Message_Control_ID',
                                        p_value   => concat(to_char(sysdate, 'YYYYMMDDHH24'), s_message_uk));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Processing_ID>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Processing_ID',
                                        p_value   => 'P');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Processing_ID>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Version_ID>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Version_ID',
                                        p_value   => '2.4');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Version_ID>'));

   kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                 p_xml => ('</MSH>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<QRD>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Query_Date_Time>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Time',
                                        p_value   => to_char(sysdate, 'YYYYMMDDHH24MI'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Query_Date_Time>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Query_Format_Code',
                                        p_value   => 'R');

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Query_Priority',
                                        p_value   => 'I');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<Who_Subject_Filter>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'ID_Number',
                                        p_value   => p_zisnr);

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</Who_Subject_Filter>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<What_Subject_Filter>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Identifier',
                                        p_value   => 'DEM');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</What_Subject_Filter>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('<What_Department_Data_Code>'));

  kgc_soap_api_maastricht.add_parameter(p_request => l_request,
                                        p_name    => 'Identifier',
                                        p_value   => 'ALL');

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</What_Department_Data_Code>'));

  kgc_soap_api_maastricht.add_complex_parameter(p_request => l_request,
                                                p_xml => ('</QRD>'));
 --Send the message
  l_response := kgc_soap_api_maastricht.invoke(p_request => l_request,
                                               p_url     => l_url,
                                               p_action  => 'QRY_A19_Patient_Query');

  delete from KGC_ZIS_MAASTRICHT_XML_TEMP;
  insert into KGC_ZIS_MAASTRICHT_XML_TEMP values(l_response.doc);
  commit;
  -- LEES WAARDEN UIT XML BERICHT UIT IN INTERNE RECORDS.
  soapmsg2rec(l_response.doc, pers_trow, rela_trow, verz_trow);
  --delete from KGC_ZIS_MAASTRICHT_XML_TEMP;

    dbms_output.put_line('Gek testje: ' || rela_trow.liszcode);

  x_msg := 'OK';

  RETURN TRUE;
/*  EXCEPTION
  WHEN OTHERS
    THEN x_msg := v_thisproc || ' Error: ' || SQLERRM;
    RETURN FALSE;
*/
END;

PROCEDURE soapmsg2rec -- vertaal SOAP-message naar rec's
( p_soap_message IN     xmltype
, pers_trow IN OUT Cg$kgc_Personen.cg$row_type
, rela_trow IN OUT Cg$kgc_Relaties.cg$row_type
, verz_trow IN OUT Cg$kgc_Verzekeraars.cg$row_type
) IS

CURSOR c_rela
  ( p_liszcode IN varchar2
  )
  IS -- in Maastricht wordt de huisarts geidentificeerd door de LISZ-code
    SELECT rela.rela_id
    FROM   KGC_RELATIES rela
    WHERE  rela.liszcode = p_liszcode;

-- Controle gegevens verzekeraars
CURSOR c_verz
  ( p_code VARCHAR2
  ) IS
    SELECT verz_id,
           vervallen
    FROM   KGC_VERZEKERAARS verz
    WHERE  verz.code = p_code
     ;
  v_rela_aanw       varchar2(1);
  v_dummy           NUMBER;
  v_verz_id         NUMBER;
  v_rela_id         NUMBER;
  v_verz_vervallen  varchar2(1);
  v_rela_aansp      varchar2(80);
  v_verz_aanw       varchar2(1);
  v_overledendat    varchar2(20);
  v_ovl_jaar        varchar2(4);
  v_ovl_mnd         varchar2(2);
  v_ovl_dag         varchar2(2);
  TMP_Verz          Cg$kgc_Verzekeraars.cg$row_type;
  v_voorletters     varchar2(50);
  v_voorletters2    varchar2(50);

BEGIN
   /* ---------------- OPHALEN PERSOONSGEGEVENS ---------------------   */
    BEGIN
    SELECT extractValue(value(ma), '/Patient_Identifier_List/ID_Number') into pers_trow.zisnr
    FROM KGC_ZIS_MAASTRICHT_XML_TEMP,
                table(XMLSequence(extract(OBJECT_VALUE, '//Cluster_1/PID/Patient_Identifier_List'))) ma
    WHERE extractValue(value(ma), '/Patient_Identifier_List/Assigning_Authority/Namespace_ID') = 'SAP';
    EXCEPTION WHEN NO_DATA_FOUND then NULL;
    WHEN OTHERS THEN null;
    END;

    BEGIN
    SELECT extractValue(value(ma), '/Patient_Identifier_List/ID_Number') into pers_trow.BSN
    FROM KGC_ZIS_MAASTRICHT_XML_TEMP,
                table(XMLSequence(extract(OBJECT_VALUE, '//Cluster_1/PID/Patient_Identifier_List'))) ma
    WHERE extractValue(value(ma), '/Patient_Identifier_List/Identifier_Type_Code') = 'NNNLD';
    EXCEPTION WHEN NO_DATA_FOUND then NULL;
    WHEN OTHERS THEN null;
    END;

    pers_trow.ACHTERNAAM          := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Name/Family_Name/Own_Surname/text()');
    v_voorletters                 := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Name/Given_Name/text()');

    pers_trow.VOORVOEGSEL         := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Name/Family_Name/Own_Surname_Prefix/text()');
    pers_trow.ACHTERNAAM_PARTNER  := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Name/Family_Name/Surname_From_Partner_Spouse/text()');
    pers_trow.VOORVOEGSEL_PARTNER := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Name/Family_Name/Surname_Prefix_From_Partner_Spouse/text()');
    pers_trow.GEBOORTEDATUM       := to_date(XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Date_Time_of_Birth/Time/text()'),'yyyymmdd');
    pers_trow.GESLACHT            := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Administrative_Sex/text()');
    pers_trow.ADRES               := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Address/Street_Address/Street_or_Mailing_Address/text()');
    pers_trow.POSTCODE            := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Address/Zip_or_Postal_Code/text()');
    pers_trow.WOONPLAATS          := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Address/City/text()');
    pers_trow.LAND                := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Address/Country/text()');
    pers_trow.TELEFOON            := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Phone_Number-Home/Telephone_Number/text()');
    pers_trow.MEERLING            := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Multiple_Birth_Indicator/text()');
    pers_trow.OVERLEDEN           := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Death_Indicator/text()');
    pers_trow.VERZEKERINGSNR      := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/Cluster_3/IN1/Policy_Number/text()');
    pers_trow.OVERLIJDENSDATUM    := To_date(substr(XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Death_Date_and_Time/Time/text()'),1,8),'yyyymmdd');
    v_voorletters2                := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/PID/Patient_Name/Second_and_Further_Given_Names_or_Initials_Thereof/text()');

    -- Formateer voorletters veld
    -- indien de voorletters minder dan 10 zijn, de hele naam ofwel het hele voorletter veld mee
    -- over nemen. Indien het aantal characters in het veld 1 of meer dan tien(dan wordt het eerste
    -- veld gesubstringed tot 1 Char dan plakt men het tweede veld uit het XML bericht hieraan
    -- vast.
    IF Length(v_voorletters) <= 30 -- condition is changed from <10 to <30 mantis 0182,saroj
    THEN
       pers_trow.VOORLETTERS := v_voorletters ;
    ELSE
       pers_trow.VOORLETTERS := substr(v_voorletters,1,1);
    END IF;

    IF LENGTH(pers_trow.VOORLETTERS) = 1 THEN
       IF LENGTH(v_voorletters2)  < 29 THEN -- condition is changed from <9 to <29, mantis 0182 saroj
           pers_trow.VOORLETTERS := pers_trow.VOORLETTERS||' '||v_voorletters2;
       END IF;
    END IF;

    -- Word niet meegegeven in de ADT Q webservice vanuit SAP
    -- Verplicht veld binnen KGC_Personen, als de achernaam van de partner ingevuld is
    -- dan is de persoon gehuwd, anders gehuwd = 'N'

    IF Trim(pers_trow.ACHTERNAAM_PARTNER) Is Not NULL
    THEN
      pers_trow.GEHUWD              := 'J';
    ELSE
      pers_trow.GEHUWD              := 'N';
    END IF;

    -- Aanspreken veld bij persoon aanmaken.
     pers_trow.AANSPREKEN := Format_PersAanspreken( pers_trow.ACHTERNAAM
                                              ,pers_trow.VOORLETTERS
                                              ,pers_trow.VOORVOEGSEL
                                              ,pers_trow.ACHTERNAAM_PARTNER
                                              ,pers_trow.VOORVOEGSEL_PARTNER
                                             ) ;

    --Toepassen Business logica controle BSN
    IF pers_trow.BSN is Null
    THEN
       pers_trow.BSN_GEVERIFIEERD := 'N';
    ELSE
       pers_trow.BSN_GEVERIFIEERD := 'J';
    END IF;

    -- Overleden veld omzetten naar juiste waarde(Engels naar NL)
    IF  pers_trow.OVERLEDEN = 'Y'
    THEN
        pers_trow.OVERLEDEN := 'J';
    ELSE
        pers_trow.OVERLEDEN := 'N';
    END IF;

    IF  pers_trow.MEERLING = 'Y'
    THEN
        pers_trow.MEERLING := 'J';
    ELSE
        pers_trow.MEERLING := 'N';
    END IF;

    -- Geslacht in het juiste formaat plaatsen
    CASE pers_trow.GESLACHT
    WHEN 'F' THEN    pers_trow.GESLACHT := 'V';
    WHEN 'M' THEN    null;
    ELSE
       pers_trow.GESLACHT := 'O';
    END CASE;

    pers_trow.land := CAST_LAND(pers_trow.land);


    IF pers_trow.DEFAULT_AANSPREKEN IS NULL OR  pers_trow.DEFAULT_AANSPREKEN <> 'N' THEN
       pers_trow.DEFAULT_AANSPREKEN := 'J';
    END IF;

/*----------------------------------------------------------- */
/*-------   OPHALEN RELATIEGEGEVENS UIT XML BERICHT --------- */
/*----------------------------------------------------------- */

    -- OPHALEN LISZCODE UIT XML BERICHT
    BEGIN
       SELECT extractValue(value(ma), '/Role_Person/ID_Number') into rela_trow.liszcode
       FROM KGC_ZIS_MAASTRICHT_XML_TEMP,
                table(XMLSequence(extract(OBJECT_VALUE, '//Cluster_1/ROL_1/Role_Person'))) ma
       WHERE extractValue(value(ma), '/Role_Person/Source_Table') = 'NGPA';
    EXCEPTION
       -- LISZCODE LEEG DAN DUMMY RECORD
       WHEN NO_DATA_FOUND
         THEN NULL;
       WHEN OTHERS
         THEN null;
    END;
    IF Trim(rela_trow.LISZCODE) IS NULL
    THEN
       rela_trow.LISZCODE := 'DIV9999999';
    END IF;
    -- CONTROLEREN OF DE LISCODE BESTAAT IN DE TABEL KGC_RELATIES
    OPEN c_rela(rela_trow.LISZCODE);
      FETCH c_rela INTO v_rela_id;
    IF c_rela%notfound
      -- INDIEN LISZCODE NIET GEVONDEN NIEUWE ARTS TOEVOEGEN
    THEN
       v_rela_id := KGC_RELA_SEQ.nextval;

        --huisarts uit xml bericht halen
        BEGIN
            SELECT extractValue(value(ma), '/Role_Person/Family_Name/Surname') into rela_trow.ACHTERNAAM
            FROM KGC_ZIS_MAASTRICHT_XML_TEMP,
                    table(XMLSequence(extract(OBJECT_VALUE, '//Cluster_1/ROL_1/Role_Person'))) ma
            WHERE extractValue(value(ma), '/Role_Person/Source_Table') = 'NGPA';
            EXCEPTION WHEN NO_DATA_FOUND then rela_trow.ACHTERNAAM := 'ACHTERNAAM HUISARTS ONBEKEND' ;
            WHEN OTHERS THEN NULL;
        END;
        BEGIN
            SELECT extractValue(value(ma), '/Role_Person/Second_and_Further_Given') into rela_trow.VOORLETTERS
            FROM KGC_ZIS_MAASTRICHT_XML_TEMP,
                    table(XMLSequence(extract(OBJECT_VALUE, '//Cluster_1/ROL_1/Role_Person'))) ma
            WHERE extractValue(value(ma), '/Role_Person/Source_Table') = 'NGPA';
            EXCEPTION WHEN NO_DATA_FOUND then NULL;
            WHEN OTHERS THEN null;
        END;
        -- OPHALEN ADRES GEGEVENS.
        rela_trow.ADRES      := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/ROL_1/Office_Home_Address_Birthplace/Street_Address/Street_or_Mailing_Address/text()');
        rela_trow.POSTCODE   := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/ROL_1/Office_Home_Address_Birthplace/Zip_or_Postal_Code/text()');
        rela_trow.WOONPLAATS := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/ROL_1/Office_Home_Address_Birthplace/City/text()');
        rela_trow.VERVALLEN  := 'N';
        rela_trow.LAND       := CAST_LAND(XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/ROL_1/Office_Home_Address_Birthplace/Country/text()'));
        -- AANSPREKEN VELD FORMATEREN NAAR JUIST FORMAAT
        v_rela_aansp         := Format_PersAanspreken(  rela_trow.ACHTERNAAM
                                                       ,rela_trow.VOORLETTERS
                                                       ,rela_trow.VOORVOEGSEL
                                                       ,null
                                                       ,null
                                                      );


        -- CODE := LISZCODE
        rela_trow.CODE       := rela_trow.LISZCODE;

        -- NIEUWE HUIARTS TOEVOEGEN
        insert into KGC_RELATIES( RELA_ID,
                                    CODE,
                                    ACHTERNAAM,
                                    VERVALLEN,
                                    relatie_type,
                                    aanspreken,
                                    CREATED_BY,
                                    CREATION_DATE,
                                    LAST_UPDATED_BY,
                                    LAST_UPDATE_DATE,
                                    LISZCODE,
                                    VOORLETTERS,
                                    ADRES,
                                    POSTCODE,
                                    WOONPLAATS,
                                    LAND)
                    values(         v_rela_id,
                                    rela_trow.liszcode,
                                    rela_trow.ACHTERNAAM,
                                    rela_trow.VERVALLEN,
                                    'HA',
                                    v_rela_aansp,
                                    'ZIS',
                                    sysdate,
                                    'ZIS',
                                    sysdate,
                                    rela_trow.LISZCODE,
                                    rela_trow.VOORLETTERS,
                                    rela_trow.ADRES,
                                    rela_trow.POSTCODE,
                                    rela_trow.WOONPLAATS,
                                    rela_trow.LAND
                             );

      rela_trow.RELA_ID := v_rela_id;
      pers_trow.RELA_ID := v_rela_id;

   ELSE
      pers_trow.RELA_ID := v_rela_id;
   END IF;
   close C_rela;

/* --------------------------------------------------------------------- */
/*----------------- BEPALEN GEGEVENS VERZEKERAAR ----------------------- */

   -- OPHALEN CODE VERZEKERAAR
   verz_trow.code        := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/Cluster_3/IN1/Insurance_Company_ID/ID_Number/text()');

   IF trim(verz_trow.code ) Is Null -- INDIEN LEEG DUMMY RECORD.
   THEN
      verz_trow.code  := 'DIV9999999';
   END IF;

     -- CONTROLE OF GEGEVENS VERZEKERAAR AL BESTAAN
     OPEN c_verz(verz_trow.code);
       FETCH c_verz INTO v_verz_id, v_verz_vervallen  ;
     IF c_verz%NOTFOUND --  NIET GEVONDEN GEGEVENS OPHALEN UIT XML
     THEN
        v_verz_id  := Kgc_Verz_Seq.Nextval ;
        verz_trow.NAAM        := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/Cluster_3/IN1/Insurance_Company_Name/Organization_Name/text()');
        verz_trow.ADRES       := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/Cluster_3/IN1/Insurance_Company_Address/Street_or_Mailing_Address/text()');
        verz_trow.POSTCODE    := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/Cluster_3/IN1/Zip_or_Postal_Code/text()');
        verz_trow.WOONPLAATS  := XML_EXTRACT_NO_ERRORS(p_soap_message,'//Cluster_1/Cluster_3/IN1/City/text()');

        -- TOEVOEGEN AAN KGC_VERZEKERAARS
        INSERT INTO Kgc_Verzekeraars (
                                       verz_id
                                      ,Code
                                      ,Naam
                                      ,Vervallen
                                      ,Created_By
                                      ,Creation_Date
                                      ,Last_Updated_By
                                      ,Last_Update_Date
                                      ,Adres
                                      ,Postcode
                                      ,Woonplaats
                                     )
           Values                    (
                                       v_verz_id
                                      ,verz_trow.CODE
                                      ,nvl(verz_trow.NAAM,'NAAM ONBEKEND OPZOEKEN')
                                      ,'N'
                                      ,'ZIS'
                                      ,sysdate
                                      ,'ZIS'
                                      ,sysdate
                                      ,verz_trow.ADRES
                                      ,verz_trow.POSTCODE
                                      ,verz_trow.WOONPLAATS
                                     ) ;


          pers_trow.VERZ_ID := v_verz_id;

     ELSE
       If v_verz_vervallen = 'J'
       THEN
          Update kgc_verzekeraars
          set    vervallen = 'N'
          Where  verz_id =  v_verz_id;
       END IF;
        verz_trow.verz_id := v_verz_id;
   --     Cg$kgc_Verzekeraars.slct(verz_trow); -- bestaande gegevens ophalen
        pers_trow.verz_id := verz_trow.verz_id; -- opnemen bij persoon!
     END IF;
   CLOSE c_verz;

END;

----------------------------------------------------------------------------


----------------------------------------------------------------------------
FUNCTION verwerk_ander_bericht -- specifiek
( p_hlbe_id   NUMBER
, p_hl7_msg   Kgc_Zis.hl7_msg
, x_entiteit  IN OUT VARCHAR2
, x_id        IN OUT NUMBER
) RETURN VARCHAR2 IS
----------------------------------------------------------------------------
  v_ret_status       KGC_HL7_BERICHTEN.status%TYPE := 'X';
  v_debug            VARCHAR2(100);
  v_thisproc         VARCHAR2(100) := 'kgc_zis_maastricht.verwerk_ander_bericht';
BEGIN
  -- nog geen specifieke berichten aanwezig voor maastricht
  RETURN v_ret_status;
END;

FUNCTION decl2dft_p03
( p_decl_id        IN     NUMBER
, p_aantal         IN     NUMBER
, p_check_only     IN     BOOLEAN
, p_msg_control_id IN     VARCHAR2
, x_msg            IN OUT VARCHAR2
) RETURN BOOLEAN IS
----------------------------------------------------------------------------
  CURSOR c_decl (p_decl_id NUMBER) IS
    SELECT *
    FROM   KGC_DECLARATIES decl
    WHERE  decl.decl_id = p_decl_id
    ;
  r_decl c_decl%ROWTYPE;
  CURSOR c_kafd (p_kafd_id NUMBER) IS
    SELECT *
    FROM   KGC_KGC_AFDELINGEN kafd
    WHERE  kafd.kafd_id = p_kafd_id
    ;
  r_kafd c_kafd%ROWTYPE;
  CURSOR c_rela (p_rela_id NUMBER) IS
    SELECT *
    FROM   KGC_RELATIES rela
    WHERE  rela.rela_id = p_rela_id
    ;
  r_rela c_rela%ROWTYPE;
  CURSOR c_onde (p_onde_id NUMBER) IS
    SELECT *
    FROM   KGC_ONDERZOEKEN onde
    WHERE  onde.onde_id = p_onde_id
    ;
  r_onde c_onde%ROWTYPE;
  CURSOR c_onbe (p_onbe_id NUMBER) IS
    SELECT *
    FROM   KGC_ONDERZOEK_BETROKKENEN onbe
    WHERE  onbe.onbe_id = p_onbe_id
    ;
  r_onbe c_onbe%ROWTYPE;
  CURSOR c_meti (p_meti_id NUMBER) IS
    SELECT *
    FROM   BAS_METINGEN meti
    WHERE  meti.meti_id = p_meti_id
    ;
  r_meti c_meti%ROWTYPE;
  v_hl7_msg_bron    Kgc_Zis.hl7_msg;
  v_pers_trow       Cg$kgc_Personen.cg$row_type;
  v_rela_trow       Cg$kgc_Relaties.cg$row_type;
  v_verz_trow       Cg$kgc_Verzekeraars.cg$row_type;
  v_onde_id         NUMBER;
  v_kafd_dft_code   VARCHAR2(100);
  v_sysdate         VARCHAR2(20) := TO_CHAR(SYSDATE, 'yyyymmddhh24miss');
  v_temp             VARCHAR2(2000);
  v_sep             VARCHAR2(2);
  v_nl              VARCHAR2(2) := CHR(10);
  v_debug            VARCHAR2(100);
  v_thisproc        VARCHAR2(100) := 'kgc_zis_maastricht.verstuur_dft_p03';
BEGIN
  RETURN FALSE; -- zie utrecht voor een uitgewerkte voorzet
EXCEPTION
  WHEN OTHERS
  THEN
    -- exception door naar aanroeper:
    RAISE_APPLICATION_ERROR(-20000, v_thisproc || ': Error; Debug: ' || v_debug || '; SQLErrm: ' || SQLERRM);
END;

FUNCTION XML_EXTRACT_NO_ERRORS
--wordt gebruikt door soapmsg2rec om het xml bericht uit te lezen, en vangt eventuele lege tags af.
(p_xml IN xmltype,
p_location IN varchar2
) RETURN VARCHAR2 AS

begin

return case when p_xml.extract(p_location) IS NOT NULL
then p_xml.extract(p_location).getstringval()
else NULL
end;


END;

FUNCTION Format_PersAanspreken ( p_achternaam       varchar2
                                ,p_VOORLETTERS      varchar2
                                ,p_voorvoegsel      varchar2
                                ,p_Achternaam_part  varchar2
                                ,p_voorvoegsel_part varchar2
                                )
/* Deze functie vult het veld aanspreken op basis cvan bovenstaande variabeleb */
RETURN varchar2
As
--   v_aanspreken       varchar2(80); -- comment by saroj mantis 0182
  v_aanspreken       varchar2(200); -- added by saroj mantis 0182
  v_Achternaam_part  varchar2(30);
  v_voorvoegsel_part varchar2(10);
  v_voorvoegsel      varchar2(10);
  v_achternaam       varchar2(30);
  v_spatie           varchar2(1) := ' ';
  E_format_aanspreken Exception ;
BEGIN
    v_voorvoegsel_part := trim(p_voorvoegsel_part);
    v_Achternaam_part  := Trim(p_Achternaam_part);
    v_voorvoegsel      := Trim(p_voorvoegsel);
    v_achternaam       := Trim(p_achternaam);

    v_aanspreken       := Trim(p_Voorletters)||v_spatie;
    -- Als het voorvoegsel leeg is dan niet toevoegen aan aanspreken.


    IF v_Achternaam_part is not null
    THEN
       IF v_voorvoegsel_part Is not null
       THEN
         v_aanspreken := v_aanspreken||
                         v_voorvoegsel_part||
                         v_spatie||v_Achternaam_part;
       ELSE
          v_aanspreken := v_aanspreken||
                          v_spatie||v_Achternaam_part;
       END IF;
       v_aanspreken := v_aanspreken||' - ';
    END IF;

    IF v_voorvoegsel Is Not NULL
    THEN
       v_aanspreken := v_aanspreken||v_voorvoegsel||
                       v_spatie||v_achternaam;
    ELSE
       v_aanspreken := v_aanspreken||v_spatie||v_achternaam;
    END IF;

   Return(v_aanspreken);

   Exception When Others then
      Raise  E_format_aanspreken;

END;

FUNCTION CAST_LAND ( P_Land varchar2 )
RETURN varchar2
AS
Begin
   CASE P_LAND
   WHEN 'NL'
     THEN return('NEDERLAND');
   WHEN 'BE'
     THEN return('BELGIE');
   WHEN 'DE'
     THEN return('DUITSLAND');
   WHEN 'FR'
     THEN return('FRANKRIJK');
   ELSE
        return(P_LAND);

   END CASE;

END;
END KGC_ZIS_MAASTRICHT;
/

/
QUIT
