CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPPO_00" IS
/* LST 13-07-2004                                                                                                                                        */
/* Package voor procedures op de tabel KGC_OPSLAG_POSITIES                                                   */
PROCEDURE chk_positie
(p_opel_id IN NUMBER
,p_x_pos IN NUMBER
,p_y_pos IN NUMBER
);

PROCEDURE insert_oppo
(p_oppo_rec IN kgc_opslag_posities%rowtype
,p_oppo_id  OUT kgc_opslag_posities.oppo_id%type
);

FUNCTION exists_oppo
(p_opel_id IN NUMBER
) RETURN BOOLEAN
;

PROCEDURE update_beschikbaar
(p_oppo_id IN NUMBER
,p_beschikbaar IN VARCHAR2
)
;

PROCEDURE kopieer_oppo
(p_opel_van IN NUMBER
,p_opel_naar IN NUMBER
)
;

/* Vertaal opslagpositie volgens as-TYPE vh opslagelement */
FUNCTION opslagpositie
(p_opel_id   IN NUMBER
,p_positie   IN NUMBER
,p_code      IN VARCHAR2
) RETURN VARCHAR2
;
END KGC_OPPO_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPPO_00" IS
/* LST 13-07-2004                                                                                                                                        */
/* Package voor procedures op de tabel KGC_OPSLAG_POSITIES                                                   */
PROCEDURE chk_positie
(p_opel_id IN NUMBER
,p_x_pos IN NUMBER
,p_y_pos IN NUMBER
)
IS
  CURSOR c_opel
  (cp_opel_id NUMBER
  ,cp_x_pos NUMBER
  ,cp_y_pos NUMBER
  ) IS
  SELECT '1'
  FROM   kgc_opslag_elementen opel
  WHERE  opel.opel_id = cp_opel_id
  AND    (opel.aantal_x_pos < cp_x_pos
          OR
      opel.aantal_y_pos < cp_y_pos
     )
  ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN c_opel(p_opel_id, p_x_pos, p_y_pos);
  FETCH c_opel INTO v_dummy;
  IF c_opel%found THEN
    qms$errors.show_message(p_mesg => 'KGC-11138'
                         ,p_errtp => 'E'
               ,p_rftf => TRUE
               );
  END IF;
  CLOSE c_opel;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opel%isopen THEN
    CLOSE c_opel;
  END IF;
  RAISE;
END;

-- insert oppo en geef pk terug
PROCEDURE insert_oppo
(p_oppo_rec IN kgc_opslag_posities%rowtype
,p_oppo_id  OUT kgc_opslag_posities.oppo_id%type
) IS
BEGIN
  INSERT INTO kgc_opslag_posities
  (oppo_id
  ,opel_id
  ,x_positie
  ,y_positie
  ,beschikbaar
  ,created_by
  ,creation_date
  ,last_updated_by
  ,last_update_date
  ,code
  ,opmerkingen
  )
  VALUES
  (kgc_oppo_seq.nextval
  ,p_oppo_rec.opel_id
  ,p_oppo_rec.x_positie
  ,p_oppo_rec.y_positie
  ,p_oppo_rec.beschikbaar
  ,p_oppo_rec.created_by
  ,p_oppo_rec.creation_date
  ,p_oppo_rec.last_updated_by
  ,p_oppo_rec.last_update_date
  ,p_oppo_rec.code
  ,p_oppo_rec.opmerkingen
  )
  returning oppo_id INTO p_oppo_id
  ;
END insert_oppo;

-- controleer of oppo al gegenereerd is
FUNCTION exists_oppo
(p_opel_id IN NUMBER
) RETURN BOOLEAN
IS
  CURSOR c_oppo
  (cp_opel_id NUMBER
  )
  IS
  SELECT '1'
  FROM   kgc_opslag_posities oppo
  WHERE  oppo.opel_id = cp_opel_id
  ;
  v_dummy  VARCHAR2(1);
  v_exists BOOLEAN := TRUE;
BEGIN
  OPEN c_oppo(p_opel_id);
  FETCH c_oppo INTO v_dummy;
  IF c_oppo%found
  THEN
    v_exists := TRUE;
  ELSE
    v_exists := FALSE;
  END IF;
  CLOSE c_oppo;
  RETURN v_exists;
EXCEPTION
  WHEN OTHERS THEN
    IF c_oppo%found THEN
      CLOSE c_oppo;
    END IF;
    RAISE;
END exists_oppo;

PROCEDURE update_beschikbaar
(p_oppo_id     IN NUMBER
,p_beschikbaar IN VARCHAR2
)
IS
BEGIN
  UPDATE kgc_opslag_posities
  SET    beschikbaar = p_beschikbaar
  WHERE  oppo_id = p_oppo_id
  ;
END update_beschikbaar;

PROCEDURE kopieer_oppo
(p_opel_van IN NUMBER
,p_opel_naar IN NUMBER
)
IS
  CURSOR c_oppo
  (cp_opel_id NUMBER
  )
  IS
  SELECT oppo.*
  FROM   kgc_opslag_posities oppo
  WHERE  oppo.opel_id = cp_opel_id
  ;
  r_oppo c_oppo%rowtype;
BEGIN
  FOR r_oppo IN c_oppo(p_opel_van)
  LOOP
    INSERT INTO kgc_opslag_posities
  (oppo_id
  ,opel_id
  ,x_positie
  ,y_positie
  ,beschikbaar
  ,created_by
  ,creation_date
  ,last_updated_by
  ,last_update_date
  ,code
  ,opmerkingen
  ) VALUES
  (kgc_oppo_seq.nextval
  ,p_opel_naar
  ,r_oppo.x_positie
  ,r_oppo.y_positie
  ,r_oppo.beschikbaar
  ,USER
  ,SYSDATE
  ,USER
  ,SYSDATE
  ,r_oppo.code
  ,r_oppo.opmerkingen
  )
  ;
  END LOOP;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    RAISE;
END kopieer_oppo;

/* Bepaal code opslaglocatie volgens as-TYPE vh opslagelement
P_CODE = X of Y
*/
FUNCTION opslagpositie
(p_opel_id   IN NUMBER
,p_positie   IN NUMBER
,p_code      IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_as_type kgc_opslag_elementen.xas_type%TYPE;
  v_return  VARCHAR2(11);
  --
  CURSOR c_opel
  (cp_opel_id NUMBER
  ,cp_code    VARCHAR2
  )
  IS
  SELECT decode (cp_code,'X',xas_type
                        ,'Y',yas_type,'N')
  FROM   kgc_opslag_elementen opel
  WHERE  opel.opel_id = cp_opel_id
  ;
BEGIN
  OPEN c_opel (p_opel_id,p_code);
  FETCH c_opel INTO v_as_type;
  CLOSE c_opel;
  v_return := to_char(p_positie);
  IF v_as_type = 'A'
  THEN
     v_return := chr(p_positie+64);
  END IF;
  RETURN(v_return);
END opslagpositie;

END KGC_OPPO_00;
/

/
QUIT
