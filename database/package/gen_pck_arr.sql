CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_ARR"
IS
   /*
   =============================================================================
   Package Name : gen_pck_arr
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_role_roles
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   PROCEDURE p_ins_gen_appl_role_roles(
      i_arr_rec  IN gen_appl_role_roles%ROWTYPE);
   --
   PROCEDURE p_del_gen_appl_role_roles(i_arr_id IN gen_appl_role_roles.id%TYPE);
--
END gen_pck_arr;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_ARR"
IS
   /*
   =============================================================================
   Package Name : gen_pck_arr
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_role_roles
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   --
   PROCEDURE p_ins_gen_appl_role_roles(
      i_arr_rec  IN gen_appl_role_roles%ROWTYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61)
                           := g_package || 'p_ins_gen_appl_role_roles' ;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      INSERT INTO gen_appl_role_roles(authentication_appl_role_id
                                     ,authorization_appl_role_id)
           VALUES (i_arr_rec.authentication_appl_role_id
                  ,i_arr_rec.authorization_appl_role_id);
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   END p_ins_gen_appl_role_roles;
   --
   PROCEDURE p_del_gen_appl_role_roles(i_arr_id IN gen_appl_role_roles.id%TYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61)
                           := g_package || 'p_del_gen_appl_role_roles' ;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      DELETE FROM gen_appl_role_roles
            WHERE id = i_arr_id;
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   END p_del_gen_appl_role_roles;
--
END gen_pck_arr;
/

/
QUIT
