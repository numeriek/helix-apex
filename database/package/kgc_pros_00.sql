CREATE OR REPLACE PACKAGE "HELIX"."KGC_PROS_00" IS
--Verwerk probes van tmp-tabel naar KGC_PROBES
PROCEDURE probes_inlezen;

-- zet een probe in de stoftesten-tabel
PROCEDURE probe_overzetten
( p_pros_id in number
, x_stof_id out number
, x_status out varchar2 -- Fout, Overgezet, Gewijzigd, Vervallen
, x_opmerkingen out varchar2
);

-- zorg dat chromosoom-aanduidingen goed worden gesorteerd: X, Y, 1,2,..22.
function sorteer_chromosoom
( p_chromosoom in varchar2
)
return number;

-- Zie KGC_PROBE_METINGEN_VW
FUNCTION aangemelde_probe
( p_meti_id in number
, p_volgnr in number
)
RETURN NUMBER;

-- zie rapport kgcpros51 (metafase/interfase).
FUNCTION aantal_lege_regels_op_rapport
( p_meti_id in number
, p_fase in varchar2
, p_max_aantal_lege_regels in number := 5
)
RETURN NUMBER;
END KGC_PROS_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_PROS_00" IS
PROCEDURE probes_inlezen
IS
/******************************************************************************
   NAME:       probes_inlezen
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.3        02-08-2011  RDE              Overnemen kleur
   1.2        29-12-2008  JKO              Mantis 1519-aanpassingen.
   1.1        15-02-2007  MKL              Verschillen in excel-bestanden weggewerkt:
                                           1 procedure voor alle instellingen
   1.0        01-09-2006  MKL              Verwerk probes van tmp-tabel naar KGC_PROBES
De volgorde van de cellen is (tussen haakjes de maximum lengte):
- Code (10)
- Name (100)
- Chromosome (5)
- Locus (100)
- FISH status (20)
- Clinical indication (100)
- Source (100)
- Selection marker (100)
- Date received (10, formaat dd-mm-yyyy)
- GENE (100)
- Mb distance vanaf (10, numeriek)
- Mb distance tot en met (10, numeriek)
- Browser version (20)
- Commercial Probe Supplier (100)
- Commercial Probe Cat nr (20)
- Kleur (20) - nieuw in versie 1.3

***************************************************************************** */
  cursor c
  is
    select trim(kolom1) kolom1
    ,      trim(kolom2) kolom2
    ,      trim(kolom3) kolom3
    ,      trim(kolom4) kolom4
    ,      trim(kolom5) kolom5
    ,      trim(kolom6) kolom6
    ,      trim(kolom7) kolom7
    ,      trim(kolom8) kolom8
    ,      trim(kolom9) kolom9
    ,      trim(kolom10) kolom10
    ,      trim(kolom11) kolom11
    ,      trim(kolom12) kolom12
    ,      trim(kolom13) kolom13
    ,      trim(kolom14) kolom14
    ,      trim(kolom15) kolom15
    ,      trim(kolom16) kolom16
    ,      trim(kolom17) kolom17
    ,      trim(kolom18) kolom18
    ,      trim(kolom19) kolom19
    ,      trim(kolom10) kolom20
    from   kgc_probes_tmp
    for update of kolom1 nowait
    ;
  v_kafd_id number := kgc_uk2pk.kafd_id(NVL(kgc_sypa_00.systeem_waarde(p_parameter_code => 'PROBES_INLEZEN_AFDELING'),'GENOOM') );/*Changes done for Mantis 5482, ASI*/
  pros_trow kgc_probes%rowtype;
  pros_null kgc_probes%rowtype;
BEGIN
  -- zet eerst de verdwenen probes op niet-gebruiken
  update kgc_probes pros
  set    pros.gebruiken = 'N'
  where  not exists
         ( select null
           from   kgc_probes_tmp prot
           where  prot.kolom2 = pros.omschrijving
         )
  ;
  commit;
  for r in c
  loop
    pros_trow := pros_null;
    if ( r.kolom2 is not null
     and r.kolom3 is not null
     and r.kolom4 is not null
       )
    then
     begin
       pros_trow.kafd_id := v_kafd_id;
       pros_trow.code := r.kolom1;
       pros_trow.omschrijving := r.kolom2;
       pros_trow.gebruiken := 'J';
       pros_trow.chromosoom := ltrim( r.kolom3, 'chr' );
       pros_trow.locus := r.kolom4;
       pros_trow.status := r.kolom5;
       pros_trow.indicatie := r.kolom6;
       pros_trow.bron := r.kolom7;
       pros_trow.selectie_marker := r.kolom8;
       pros_trow.datum_ontvangst := to_date( r.kolom9, 'dd-mm-yyyy' );
       pros_trow.gen := r.kolom10;
       pros_trow.mb_positie_va := r.kolom11;
       pros_trow.mb_positie_tm := r.kolom12;
       pros_trow.versienr := r.kolom13;
       pros_trow.leverancier := r.kolom14;
       pros_trow.catalogusnr := r.kolom15;
       pros_trow.kleur := SUBSTR(r.kolom16, 1, 20);--Changed as given by the customer for Mantis 5978 (RBA(10-09-2013))
       update kgc_probes
       set    code = pros_trow.code
       ,      gebruiken = 'J'
       ,      chromosoom = pros_trow.chromosoom
       ,      locus = pros_trow.locus
       ,      status = pros_trow.status
       ,      indicatie = pros_trow.indicatie
       ,      bron = pros_trow.bron
       ,      selectie_marker = pros_trow.selectie_marker
       ,      datum_ontvangst = pros_trow.datum_ontvangst
       ,      gen = pros_trow.gen
       ,      mb_positie_va = pros_trow.mb_positie_va
       ,      mb_positie_tm = pros_trow.mb_positie_tm
       ,      versienr = pros_trow.versienr
       ,      leverancier = pros_trow.leverancier
       ,      catalogusnr = pros_trow.catalogusnr
       ,      kleur = pros_trow.kleur --Changed as given by the customer for Mantis 5978 (RBA(10-09-2013))
       where  omschrijving = pros_trow.omschrijving
       ;
       if ( sql%rowcount = 0 )
       then
         insert into kgc_probes
         ( kafd_id
         , code
         , omschrijving
         , gebruiken
         , chromosoom
         , locus
         , status
         , indicatie
         , bron
         , selectie_marker
         , datum_ontvangst
         , gen
         , mb_positie_va
         , mb_positie_tm
         , versienr
         , leverancier
         , catalogusnr
         , kleur   --Changed as given by the customer for Mantis 5978 (RBA(10-09-2013))
         )
         values
         ( pros_trow.kafd_id
         , pros_trow.code
         , pros_trow.omschrijving
         , pros_trow.gebruiken
         , pros_trow.chromosoom
         , pros_trow.locus
         , pros_trow.status
         , pros_trow.indicatie
         , pros_trow.bron
         , pros_trow.selectie_marker
         , pros_trow.datum_ontvangst
         , pros_trow.gen
         , pros_trow.mb_positie_va
         , pros_trow.mb_positie_tm
         , pros_trow.versienr
         , pros_trow.leverancier
         , pros_trow.catalogusnr
         , pros_trow.kleur --Changed as given by the customer for Mantis 5978 (RBA(10-09-2013))
         );
        end if;
        delete from kgc_probes_tmp where current of c;
      exception
        when others
        then
          p.l( r.kolom2||': '||sqlerrm );
      end;
    end if;
  end loop;
  commit;
END probes_inlezen;

PROCEDURE probe_overzetten
( p_pros_id     in number
, x_stof_id     out number
, x_status      out varchar2
, x_opmerkingen out varchar2
)
IS
/******************************************************************************
   NAME:       probes_overzetten
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.3        02-03-2009  JKO              Mantis 1519-aanpassingen nav AT.
   1.2        29-12-2008  JKO              Mantis 1519-aanpassingen.
***************************************************************************** */
  cursor pros_cur
  ( b_pros_id in number
  )
  is
    select pros_id
    ,      kafd_id
    ,      code
    ,      omschrijving
    ,      gebruiken
    ,      stof_id
    ,      opmerkingen
    from   kgc_probes
    where  pros_id = b_pros_id
    ;
  pros_rec pros_cur%rowtype;

  -- Stoftesten-cursor op omschrijving:
  -- Aanname: stoftest-omschrijving is uniek!!!
  cursor stof_cur_oms
  ( b_kafd_id      in number
  , b_omschrijving in varchar2
  )
  is
    select stof_id
         , afgeleide_stoftest
         , vervallen
    from   kgc_stoftesten
    where  kafd_id = b_kafd_id
    and    omschrijving = b_omschrijving
    ;
  stof_rec_oms  stof_cur_oms%rowtype;
  stof_null_oms stof_cur_oms%rowtype;

  -- Stoftesten-cursor op primary-key:
  cursor stof_cur_pk
  ( b_stof_id      in number
  )
  is
    select vervallen
    from   kgc_stoftesten
    where  stof_id = b_stof_id
    ;
  stof_rec_pk  stof_cur_pk%rowtype;

  v_start_fouttekst binary_integer := 0;

  procedure meld_fout
  ( b_tekst in varchar2
  )
  is
    v_opmerkingen varchar2(4000) := x_opmerkingen;
  begin
    v_opmerkingen := substr( v_opmerkingen
                          || chr(10)
                          || 'Fout bij overzetten:'
                          || chr(10)
                          || b_tekst
                           , 1
                           , 2000
                           );
    x_opmerkingen := v_opmerkingen;
  end meld_fout;

BEGIN
  open  pros_cur( b_pros_id => p_pros_id );
  fetch pros_cur into pros_rec;
  close pros_cur;
  x_stof_id := pros_rec.stof_id;
  -- Vorige foutteksten verwijderen uit de opmerking-kolom
  v_start_fouttekst := instr( pros_rec.opmerkingen, 'Fout bij overzetten:' );
  if v_start_fouttekst > 0
  then
    x_opmerkingen := substr( pros_rec.opmerkingen, 1, v_start_fouttekst - 1 );
  else
    x_opmerkingen := pros_rec.opmerkingen;
  end if;

  if pros_rec.gebruiken = 'J'
  then -- de probe moet gekoppeld worden aan de stoftest
    if x_stof_id is not null
    then -- er is reeds een koppeling met de stoftest aanwezig, stoftest wijzigen
      begin
        if pros_rec.code is null
        then
          pros_rec.code := x_stof_id;
        end if;
        open  stof_cur_pk( x_stof_id );
        fetch stof_cur_pk
        into  stof_rec_pk;
        close stof_cur_pk;
        if stof_rec_pk.vervallen = 'N'
        then
          update kgc_stoftesten
          set    code         = nvl( code, pros_rec.code )
          ,      omschrijving = pros_rec.omschrijving

          where  stof_id = x_stof_id
          ;
          x_status := 'Gewijzigd';
        else
          update kgc_stoftesten
          set    code            = nvl( code, pros_rec.code )
          ,      omschrijving    = pros_rec.omschrijving
          ,      vervallen       = 'N'
          ,      datum_vervallen = null
          where  stof_id = x_stof_id
          ;
          x_status := 'Niet-vervallen-gezet';
        end if;
      exception
        when others
        then
          meld_fout( sqlerrm );
          x_status := 'Fout';
      end;
    else -- er is nog geen koppeling met de stoftest aanwezig, dus koppeling leggen
      stof_rec_oms := stof_null_oms;
      open  stof_cur_oms( pros_rec.kafd_id, pros_rec.omschrijving );
      fetch stof_cur_oms
      into  stof_rec_oms;
      close stof_cur_oms;
      x_stof_id := stof_rec_oms.stof_id;
      -- alleen invoeren als UK-constraint dat toelaat
      if x_stof_id is null -- er is geen bijpassende stoftest aanwezig, dus eerst de stoftest opvoeren
      then
        begin
          select kgc_stof_seq.nextval into x_stof_id from dual;
          if pros_rec.code is null
          then
            pros_rec.code := x_stof_id;
          end if;
          insert into kgc_stoftesten
          ( stof_id
          , kafd_id
          , code
          , omschrijving
          , scherm_voor_details
          , afgeleide_stoftest
          )
          values
          ( x_stof_id
          , pros_rec.kafd_id
          , pros_rec.code
          , pros_rec.omschrijving
          , 'KGCPROS20'
          , 'J'
          );
          x_status := 'Nieuw';
        exception
          when others
          then
            meld_fout( sqlerrm );
            x_status := 'Fout';
        end;
      else -- er is wel een bijpassende stoftest aanwezig
        if nvl(stof_rec_oms.afgeleide_stoftest, 'N') = 'J' -- de stoftest voldoet, dus koppelen en wijzigen
        then
          if pros_rec.code is null
          then
            pros_rec.code := x_stof_id;
          end if;
          if stof_rec_oms.vervallen = 'N'
          then
            begin
              update kgc_stoftesten
              set    code         = nvl( code, pros_rec.code )
              --,      omschrijving = pros_rec.omschrijving -- is altijd hetzelfde
              where  stof_id = x_stof_id
              ;
              x_status := 'Gekoppeld/niet-vervallen';
            exception
              when others
              then
                meld_fout( sqlerrm );
                x_status := 'Fout';
            end;
          else
            begin
              update kgc_stoftesten
              set    code            = nvl( code, pros_rec.code )
              --,      omschrijving    = pros_rec.omschrijving -- is altijd hetzelfde
              ,      vervallen       = 'N'
              ,      datum_vervallen = null
              where  stof_id = x_stof_id
              ;
              x_status := 'Gekoppeld/vervallen';
            exception
            when others
            then
              meld_fout( sqlerrm );
              x_status := 'Fout';
            end;
          end if;
        else -- de stoftest voldoet niet, dus niet koppelen
          meld_fout( 'KGC-11305 Probe niet gekoppeld, geen afgeleide stoftest.' );
          x_status := 'Niet-gekoppeld';
          x_stof_id := null;
        end if;
      end if;
    end if;
  elsif x_stof_id is not null
  then -- de stoftest moet vervallen geboekt worden
    begin
      update kgc_stoftesten
      set    vervallen       = 'J'
      ,      datum_vervallen = trunc(sysdate)
      where  stof_id = x_stof_id
      and    vervallen = 'N'
      ;
      if sql%rowcount = 0
      then
        x_status := 'Reeds-vervallen';
      else
        x_status := 'Vervallen';
      end if;
    exception
      when others
      then
        meld_fout( sqlerrm );
        x_status := 'Fout';
    end;
  else
    x_status := 'Onbruikbaar';
  end if;
END probe_overzetten;

function sorteer_chromosoom
( p_chromosoom in varchar2
)
return number
is
  v_return number(2,0);
begin
  if ( p_chromosoom is null )
  then
    v_return := 99;
  elsif ( p_chromosoom = 'X' )
  then
    v_return := -9;
  elsif ( p_chromosoom like 'X%' )
  then
    v_return := -8;
  elsif ( p_chromosoom = 'Y' )
  then
    v_return := -7;
  elsif ( p_chromosoom like 'Y%' )
  then
    v_return := -6;
  else
    v_return := kgc_reken.rekenwaarde( p_chromosoom );
  end if;
  return( v_return );
end sorteer_chromosoom;

FUNCTION aangemelde_probe
( p_meti_id in number
, p_volgnr in number
)
RETURN NUMBER
IS
/******************************************************************************
   NAME:       aangemelde_probe
   PURPOSE:    haal een meet_id op volgorde

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05-09-2006  MKL              Zie KGC_PROBE_METINGEN_VW
                                           Op volgorde of volgnummer (2e rij of volgorde=2)?

***************************************************************************** */
  v_return number;
  v_dummy number;
  cursor meet_volgorde_cur
  ( b_meti_id in number
  , b_volgorde in number
  )
  is
    select meet.meet_id
    from   kgc_probes pros
    ,      bas_meetwaarden meet
    where  meet.stof_id = pros.stof_id
    and    meet.meti_id = b_meti_id
    and    meet.volgorde = b_volgorde
    order by meet.meet_id asc
    ;
  -- voor het geval er geen volgorde is aangegeven
  cursor meet_volgnr_cur
  ( b_meti_id in number
  )
  is
    select meet.meet_id
    from   kgc_probes pros
    ,      bas_meetwaarden meet
    where  meet.stof_id = pros.stof_id
    and    meet.meti_id = b_meti_id
    and    not exists
           ( select null
             from   kgc_probes pros2
             ,      bas_meetwaarden meet2
             where  meet2.stof_id = pros2.stof_id
             and    meet2.meti_id = meet.meti_id
             and    meet2.volgorde is not null
           )
    order by meet.volgorde asc
    ,        meet.meet_id asc
    ;
BEGIN
  open  meet_volgorde_cur( p_meti_id, p_volgnr );
  fetch meet_volgorde_cur
  into  v_return;
  close meet_volgorde_cur;
  if ( v_return is null )
  then
    open  meet_volgnr_cur( p_meti_id );
    if ( p_volgnr = 1 )
    then
      fetch meet_volgnr_cur
      into  v_return;
    elsif ( p_volgnr = 2 )
    then
      fetch meet_volgnr_cur
      into  v_dummy;
      if ( meet_volgnr_cur%found )
      then
        fetch meet_volgnr_cur
        into  v_return;
      end if;
    elsif ( p_volgnr = 3 )
    then
      fetch meet_volgnr_cur
      into  v_dummy;
      if ( meet_volgnr_cur%found )
      then
        fetch meet_volgnr_cur
        into  v_dummy;
        if ( meet_volgnr_cur%found )
        then
          fetch meet_volgnr_cur
          into  v_return;
        end if;
      end if;
    else
      v_return := null;
    end if;
    close meet_volgnr_cur;
  end if;
  RETURN( v_return );
END aangemelde_probe;

FUNCTION aantal_lege_regels_op_rapport
( p_meti_id in number
, p_fase in varchar2
, p_max_aantal_lege_regels in number := 5
)
RETURN NUMBER
IS
  v_return NUMBER;

  cursor pran_cur
  is
    select count(rowid)
    from   kgc_probe_analyses pran
    where  pran.meti_id = p_meti_id
    and    pran.fase = p_fase
    ;
BEGIN
  open  pran_cur;
  fetch pran_cur
  into  v_return;
  close pran_cur;
  v_return := p_max_aantal_lege_regels - v_return; -- maximaal 5 lege regels op kgcpros51.
  if ( v_return < 0 )
  then
    v_return := 0;
  end if;
  return( v_return );
END aantal_lege_regels_op_rapport;
end kgc_pros_00;
/

/
QUIT
