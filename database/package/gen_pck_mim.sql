CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_MIM"
AS
   /*
    =============================================================================
   NAME:       gen_pck_mim
   PURPOSE:    api for table gen_menu_items
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   PROCEDURE p_del_gen_menu_items(i_mim_id IN gen_menu_items.id%TYPE);
   PROCEDURE p_print_menu_page(i_app_id      IN NUMBER
                              ,i_page_id     IN NUMBER
                              ,i_label       IN VARCHAR2
                              ,i_user_id     IN NUMBER
                              ,i_session_id  IN NUMBER
                              ,i_debug       IN VARCHAR2);
   PROCEDURE p_save_menu_item(
      io_mim_id             IN OUT gen_menu_items.id%TYPE
     ,i_application_id      IN     gen_menu_items.application_id%TYPE
     --   ,p_display_sequence   IN OUT gen_menu_items.display_sequence%TYPE
     ,i_parent_id           IN     gen_menu_items.parent_id%TYPE
     ,i_item_type           IN     gen_menu_items.item_type%TYPE
     ,i_item_icon           IN     gen_menu_items.item_icon%TYPE
     ,i_page_name           IN     gen_menu_items.page_name%TYPE
     ,i_component_function  IN     gen_menu_items.component_function%TYPE
     ,i_clearcache          IN     gen_menu_items.clearcache%TYPE
     ,i_parameters          IN     gen_menu_items.parameters%TYPE
     ,i_parameter_values    IN     gen_menu_items.parameter_values%TYPE
     ,i_printerfriendly     IN     gen_menu_items.printerfriendly%TYPE
     ,i_target_attr         IN     gen_menu_items.target_attr%TYPE
     ,i_languages           IN     wwv_flow_global.vc_arr2
     ,i_item_labels         IN     wwv_flow_global.vc_arr2
     ,i_item_descriptions   IN     wwv_flow_global.vc_arr2);
   PROCEDURE p_move_menu_item(i_move_item_id     IN gen_menu_items.id%TYPE
                             ,i_target_item_id   IN gen_menu_items.id%TYPE
                             ,i_target_position  IN VARCHAR2);
END gen_pck_mim;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_MIM"
AS
   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   PROCEDURE p_del_gen_menu_items(i_mim_id IN gen_menu_items.id%TYPE)
   IS
      l_scope  VARCHAR2(61) := g_package || 'del_gen_menu_items';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      logger.log_information(p_text => 'i_mim_id: ' || i_mim_id
                            ,p_scope => l_scope);
      DELETE FROM gen_menu_items
            WHERE id = i_mim_id;
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
         RAISE;
   END p_del_gen_menu_items;
   PROCEDURE p_print_menu_page(i_app_id      IN NUMBER
                              ,i_page_id     IN NUMBER
                              ,i_label       IN VARCHAR2
                              ,i_user_id     IN NUMBER
                              ,i_session_id  IN NUMBER
                              ,i_debug       IN VARCHAR2)
   IS
      l_scope              VARCHAR2(61) := g_package || 'p_print_menu_page';
      CURSOR c_mim
      IS
         SELECT mim.id
               ,lng.item_label
           FROM gen_menu_items mim
                INNER JOIN apex_application_pages aap
                   ON (aap.page_alias = mim.page_name
                   AND aap.application_id = i_app_id)
                INNER JOIN gen_menu_items_lang lng
                   ON (mim.id = lng.menu_item_id)
          WHERE aap.page_id = i_page_id
            AND mim.parameter_values = i_label
            AND mim.parameters = 'P' || i_page_id || '_LABEL'
            AND lng.language_code = COALESCE(v('LANGCODE')
                                            ,'en');
      l_parent_id          gen_menu_items.id%TYPE;
      l_parent_label       gen_menu_items_lang.item_label%TYPE;
      l_submenu_count      PLS_INTEGER := 0;
      l_total_left_count   PLS_INTEGER := 0;
      l_total_right_count  PLS_INTEGER := 0;
      l_current_left       BOOLEAN := TRUE;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      -- get the parent menu item
      OPEN c_mim;
      FETCH c_mim
         INTO l_parent_id
             ,l_parent_label;
      CLOSE c_mim;
      IF l_parent_id IS NOT NULL
      THEN
         FOR r_mim
            IN (           SELECT LEVEL
                                 ,id
                                 ,   '<i class="fa'
                                  || CASE
                                        WHEN COALESCE(item_type
                                                     ,'ITEM') = 'ITEM'
                                        THEN
                                           ' fa-2x'
                                        ELSE
                                           ' fa-fw'
                                     END
                                  || ' '
                                  || item_icon
                                  || ' t-MediaList-icon"></i>'
                                     item_icon
                                 ,item_label
                                 ,item_description
                                 ,item_type
                                 ,CASE
                                     WHEN page_name IS NULL
                                     THEN
                                        '" style="cursor:default" onclick="return false' -- no page connected
                                     ELSE
                                        apex_util.prepare_url(
                                              'f?p='
                                           || i_app_id
                                           || ':'
                                           || page_id
                                           || ':'
                                           || i_session_id
                                           || '::'
                                           || i_debug
                                           || ':'
                                           || clearcache
                                           || ':'
                                           || parameters
                                           || ':'
                                           || parameter_values
                                           || CASE
                                                 WHEN printerfriendly IS NOT NULL
                                                 THEN
                                                    ':' || printerfriendly
                                              END) -- prepare url for checksum
                                  END
                                     target
                                 ,target_attr
                             FROM gen_vw_user_menu_items
                       CONNECT BY PRIOR id = parent_id
                       START WITH parent_id = l_parent_id -- start with menu item of current page
                ORDER SIBLINGS BY display_sequence)
         LOOP
            IF r_mim.item_type = 'SUBMENU'
            THEN
               -- submenu, start new section
               l_submenu_count   := l_submenu_count + 1;
               IF l_submenu_count > 1
               THEN
                  -- end previous section if not the first
                  htp.p('</ul></div></section>');
               END IF;
               IF l_total_left_count > l_total_right_count
               THEN
                  l_current_left   := FALSE;
               ELSE
                  l_current_left   := TRUE;
               END IF;
               IF l_current_left
               THEN
                  l_total_left_count   := l_total_left_count + 1;
               ELSE
                  l_total_right_count   := l_total_right_count + 1;
               END IF;
               -- start new section
               htp.p(   '<section id="reg_submenu_'
                     || l_submenu_count
                     || '" class="t-Region-header sectionRegion'
                     || CASE /*MOD(l_submenu_count
                                ,2)
                           WHEN 1*/
                           WHEN l_current_left THEN 'Left'
                           ELSE 'Right'
                        END
                     || '">');
               -- set report header
               htp.p(
                     '<div class="t-Region-header"><div class="t-Region-headerItems t-Region-headerItems--title"><a href="'
                  || r_mim.target
                  || '"><h2 class="t-Region-title">'
                  || r_mim.item_icon
                  || ' '
                  || r_mim.item_label
                  || '</h2></a></div><span class="uButtonContainer"></span></div>');
               -- open report contents
               htp.p(
                  '<div class="t-Region-body"><ul class="t-MediaList  t-MediaList--showDesc t-MediaList--showIcons">');
            ELSE
               IF l_submenu_count = 0
               THEN
                  -- No submenu defined, create dummy
                  l_submenu_count   := l_submenu_count + 1;
                  htp.p(
                        '<section id="reg_submenu_'
                     || l_submenu_count
                     || '" class="uRegion uRegionNoPadding  clearfix sectionRegionLeft">');
                  htp.p(
                        '<div class="t-Region-header"><div class="t-Region-headerItems t-Region-headerItems--title">'
                     || l_parent_label
                     || '</div><span class="uButtonContainer"></span></div>');
                  -- open report contents
                  htp.p(
                     '<div class="t-Region-body"><ul class="t-MediaList  t-MediaList--showDesc t-MediaList--showIcons">');
               END IF;
               -- normal item
               htp.p(
                     '<li class="t-MediaList-item"><div class="a-MediaBlock"><a class="t-MediaList-itemWrap" href="'
                  || r_mim.target
                  || '" '
                  || CASE
                        WHEN r_mim.target_attr IS NOT NULL
                        THEN
                           'target="' || r_mim.target_attr || '"'
                     END
                  || '><div class="t-MediaList-iconWrap">'
                  || r_mim.item_icon
                  || '</div><div class="t-MediaList-body"><h3 class="t-MediaList-title">'
                  || r_mim.item_label
                  || '</h3> <div class="t-MediaList-desc">'
                  || r_mim.item_description
                  || '</div></div></a></div></li>');
               IF l_current_left
               THEN
                  l_total_left_count   := l_total_left_count + 1;
               ELSE
                  l_total_right_count   := l_total_right_count + 1;
               END IF;
            END IF;
         END LOOP;
         IF l_submenu_count > 0
         THEN
            -- end previous section
            htp.p('</ul></div></section>');
         END IF;
      END IF;
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
         RAISE;
   END p_print_menu_page;
   PROCEDURE p_save_menu_item(
      io_mim_id             IN OUT gen_menu_items.id%TYPE
     ,i_application_id      IN     gen_menu_items.application_id%TYPE
     -- ,p_display_sequence    IN OUT gen_menu_items.display_sequence%TYPE
     ,i_parent_id           IN     gen_menu_items.parent_id%TYPE
     ,i_item_type           IN     gen_menu_items.item_type%TYPE
     ,i_item_icon           IN     gen_menu_items.item_icon%TYPE
     ,i_page_name           IN     gen_menu_items.page_name%TYPE
     ,i_component_function  IN     gen_menu_items.component_function%TYPE
     ,i_clearcache          IN     gen_menu_items.clearcache%TYPE
     ,i_parameters          IN     gen_menu_items.parameters%TYPE
     ,i_parameter_values    IN     gen_menu_items.parameter_values%TYPE
     ,i_printerfriendly     IN     gen_menu_items.printerfriendly%TYPE
     ,i_target_attr         IN     gen_menu_items.target_attr%TYPE
     ,i_languages           IN     wwv_flow_global.vc_arr2
     ,i_item_labels         IN     wwv_flow_global.vc_arr2
     ,i_item_descriptions   IN     wwv_flow_global.vc_arr2)
   IS
      l_scope             VARCHAR2(61) := g_package || 'p_save_menu_item';
      l_display_sequence  gen_menu_items.display_sequence%TYPE;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      SELECT   COALESCE(MAX(display_sequence)
                       ,0)
             + 1
        INTO l_display_sequence
        FROM gen_menu_items
       WHERE COALESCE(parent_id
                     ,-1) = COALESCE(TO_NUMBER(i_parent_id)
                                    ,-1)
         AND application_id = i_application_id;
      IF io_mim_id IS NOT NULL
      THEN
         -- do an update
         UPDATE gen_menu_items
            SET parent_id            = i_parent_id
               ,item_type            = i_item_type
               ,item_icon            = i_item_icon
               ,page_name            = i_page_name
               ,clearcache           = i_clearcache
               ,parameters           = i_parameters
               ,parameter_values     = i_parameter_values
               ,printerfriendly      = i_printerfriendly
               ,component_function   = i_component_function
               ,target_attr          = i_target_attr
          WHERE id = io_mim_id;
      ELSE
         INSERT INTO gen_menu_items(parent_id
                                   ,item_type
                                   ,item_icon
                                   ,page_name
                                   ,display_sequence
                                   ,clearcache
                                   ,parameters
                                   ,parameter_values
                                   ,printerfriendly --,language
                                   ,component_function
                                   ,target_attr
                                   ,application_id)
              VALUES (i_parent_id
                     ,i_item_type
                     ,i_item_icon
                     ,i_page_name
                     ,l_display_sequence
                     ,i_clearcache
                     ,i_parameters
                     ,i_parameter_values
                     ,i_printerfriendly --,p_language
                     ,i_component_function
                     ,i_target_attr
                     ,i_application_id)
           RETURNING id
                INTO io_mim_id;
      END IF;
      FOR i IN 1 .. i_languages.COUNT
      LOOP
         MERGE INTO gen_menu_items_lang mil
              USING (SELECT io_mim_id mim_id
                           ,i_languages(i) language
                           ,i_item_labels(i) item_label
                           ,i_item_descriptions(i) item_description
                       FROM DUAL) rec
                 ON (mil.menu_item_id = rec.mim_id
                 AND mil.language_code = rec.language)
         WHEN MATCHED
         THEN
            UPDATE SET item_label         = rec.item_label
                      ,item_description   = rec.item_description
         WHEN NOT MATCHED
         THEN
            INSERT     (menu_item_id
                       ,language_code
                       ,item_label
                       ,item_description)
                VALUES (rec.mim_id
                       ,rec.language
                       ,rec.item_label
                       ,rec.item_description);
      END LOOP;
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
         RAISE;
   END p_save_menu_item;
   FUNCTION f_get_menu_item(i_item_id IN gen_menu_items.id%TYPE)
      RETURN gen_menu_items%ROWTYPE
   IS
      l_rec  gen_menu_items%ROWTYPE;
   BEGIN
      SELECT *
        INTO l_rec
        FROM gen_menu_items
       WHERE id = i_item_id;
      RETURN l_rec;
   END f_get_menu_item;
   PROCEDURE p_move_menu_item(i_move_item_id     IN gen_menu_items.id%TYPE
                             ,i_target_item_id   IN gen_menu_items.id%TYPE
                             ,i_target_position  IN VARCHAR2)
   IS
      l_target_item_rec  gen_menu_items%ROWTYPE;
      l_scope            VARCHAR2(61) := g_package || 'p_move_menu_item';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      CASE UPPER(i_target_position)
         WHEN 'INSIDE'
         THEN
            UPDATE gen_menu_items
               SET display_sequence   = display_sequence + 1
             WHERE parent_id = i_target_item_id;
            FOR r_items
               IN (SELECT id
                         ,ROWNUM display_sequence
                     FROM (SELECT id
                                 ,display_sequence + 1 display_sequence
                             FROM gen_menu_items
                            WHERE COALESCE(parent_id
                                          ,-1) = COALESCE(i_target_item_id
                                                         ,-1)
                              AND id != i_move_item_id
                           UNION
                           SELECT i_move_item_id
                                 ,1 display_sequence
                             FROM DUAL
                           ORDER BY 2))
            LOOP
               UPDATE gen_menu_items
                  SET parent_id          = i_target_item_id
                     ,display_sequence   = r_items.display_sequence
                WHERE id = r_items.id;
            END LOOP;
         WHEN 'AFTER'
         THEN
            l_target_item_rec   := f_get_menu_item(i_target_item_id);
            FOR r_items
               IN (SELECT id
                         ,ROWNUM display_sequence
                     FROM (SELECT id
                                 ,CASE
                                     WHEN display_sequence >
                                             l_target_item_rec.display_sequence
                                     THEN
                                        display_sequence + 1
                                     ELSE
                                        display_sequence
                                  END
                                     display_sequence
                             FROM gen_menu_items
                            WHERE COALESCE(parent_id
                                          ,-1) =
                                     COALESCE(l_target_item_rec.parent_id
                                             ,-1)
                              AND id != i_move_item_id
                           UNION
                           SELECT i_move_item_id
                                 ,l_target_item_rec.display_sequence + 1
                                     display_sequence
                             FROM DUAL
                           ORDER BY 2))
            LOOP
               UPDATE gen_menu_items
                  SET parent_id          = l_target_item_rec.parent_id
                     ,display_sequence   = r_items.display_sequence
                WHERE id = r_items.id;
            END LOOP;
         WHEN 'BEFORE'
         THEN
            l_target_item_rec   := f_get_menu_item(i_target_item_id);
            FOR r_items
               IN (SELECT id
                         ,ROWNUM display_sequence
                     FROM (SELECT id
                                 ,CASE
                                     WHEN display_sequence >=
                                             l_target_item_rec.display_sequence
                                     THEN
                                        display_sequence + 1
                                     ELSE
                                        display_sequence
                                  END
                                     display_sequence
                             FROM gen_menu_items
                            WHERE COALESCE(parent_id
                                          ,-1) =
                                     COALESCE(l_target_item_rec.parent_id
                                             ,-1)
                              AND id != i_move_item_id
                           UNION
                           SELECT i_move_item_id
                                 ,l_target_item_rec.display_sequence
                                     display_sequence
                             FROM DUAL
                           ORDER BY 2))
            LOOP
               UPDATE gen_menu_items
                  SET parent_id          = l_target_item_rec.parent_id
                     ,display_sequence   = r_items.display_sequence
                WHERE id = r_items.id;
            END LOOP;
      END CASE;
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
         RAISE;
   END p_move_menu_item;
END gen_pck_mim;
/

/
QUIT
