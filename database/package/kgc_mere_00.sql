CREATE OR REPLACE PACKAGE "HELIX"."KGC_MERE_00" IS
-- controleer waarde en bepaal resultaattekst
PROCEDURE resultaat
( p_meet_id IN NUMBER := NULL
, p_stof_id IN NUMBER := NULL
, p_afgerond IN VARCHAR2 := NULL
, p_waarde IN VARCHAR2
, p_meti_afgerond IN VARCHAR2 := 'N'
, x_tekst IN OUT VARCHAR2
, x_controle_ok OUT BOOLEAN
, x_strengheid OUT VARCHAR2
, x_melding OUT VARCHAR2
);
-- idem als functie, retourneer resultaattekst
FUNCTION resultaat
( p_meet_id IN NUMBER := NULL
, p_stof_id IN NUMBER := NULL
, p_afgerond IN VARCHAR2 := NULL
, p_waarde IN VARCHAR2
, p_tekst IN VARCHAR2 := NULL
, p_meti_afgerond IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
END KGC_MERE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MERE_00" IS
/******************************************************************************
      NAME:       kgc_mere_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.4        01-03-2016   SKA               Mantis 9355 TO_CHAR added to  Conclusie column release 8.11.0.2
    ****************************************************************************** */
PROCEDURE resultaat
( p_meet_id IN NUMBER := NULL
, p_stof_id IN NUMBER := NULL
, p_afgerond IN VARCHAR2 := NULL
, p_waarde IN VARCHAR2
, p_meti_afgerond IN VARCHAR2 := 'N'
, x_tekst IN OUT VARCHAR2
, x_controle_ok OUT BOOLEAN
, x_strengheid OUT VARCHAR2
, x_melding OUT VARCHAR2
)
IS
  v_meet_id NUMBER := p_meet_id;
  v_stof_id NUMBER := p_stof_id;
  v_afgerond VARCHAR2(1) := p_afgerond;
  v_waarde VARCHAR2(1000) := p_waarde;
  v_num_waarde NUMBER;
  v_string1     bas_meetwaarden.normaalwaarde_ondergrens%TYPE;--Added for Mantis 5304
  v_string2     bas_meetwaarden.normaalwaarde_bovengrens%TYPE;--Added for Mantis 5304


  CURSOR meet_cur
  IS
    SELECT meet.stof_id
    ,      meet.meetwaarde
    ,      meet.afgerond meet_afgerond
    ,      meet.normaalwaarde_ondergrens
    ,      meet.normaalwaarde_bovengrens
    ,      meet.meti_id
    ,      meti.stgr_id
    ,      to_char(meti.conclusie) resultaat_tekst   -- MANTIS 9355 TO_CHAR added SKA 01-03-2016 release 8.11.0.2
    ,      meti.afgerond meti_afgerond
    FROM   bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meti.meti_id = meet.meti_id
    AND    meet.meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;

  CURSOR mere_cur
  IS
    SELECT mere_id
    ,      UPPER( waarde_va ) waarde_va
    ,      UPPER( waarde_tm ) waarde_tm
    ,      excl_grenzen
    ,      meting_overschrijven
    ,      als_meetwaarde_afgerond
    ,      controle_functie
    ,      controle_strengheid
    ,      controle_foutmelding
    ,      tekst
    FROM   kgc_meetwaarde_resultaat
    WHERE  stof_id = v_stof_id
    AND  ( stgr_id = meet_rec.stgr_id
        OR stgr_id IS NULL
        OR meet_rec.stgr_id IS NULL
         )
    ORDER BY stgr_id ASC -- met stoftestgroep eerst
    ,        mere_id DESC -- laatst ingevoerde eerst
    ;
  mere_rec mere_cur%rowtype; -- tbv overnemen gegevens

  v_statement VARCHAR2(4000);
  v_result VARCHAR2(1); -- J=controle ok, N=controle niet ok

  FUNCTION numeriek
  ( b_waarde IN VARCHAR2
  )
  RETURN BOOLEAN
  IS
    l_num_waarde NUMBER;
  -- PL/SQL Block
BEGIN
    l_num_waarde := TO_NUMBER( b_waarde );
    RETURN( TRUE );
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN( FALSE );
  END numeriek;

BEGIN
  IF ( p_meet_id IS NOT NULL )
  THEN
    OPEN  meet_cur;
    FETCH meet_cur
    INTO  meet_rec;
    IF ( meet_cur%found )
    THEN
      v_afgerond := NVL( v_afgerond, meet_rec.meet_afgerond );
      v_stof_id := NVL( v_stof_id, meet_rec.stof_id );
      x_tekst := NVL( x_tekst, meet_rec.resultaat_tekst );
      v_string1 := meet_rec.normaalwaarde_ondergrens; --Added for Mantis 5304
      v_string2 := meet_rec.normaalwaarde_bovengrens; --Added for Mantis 5304
    END IF;
    CLOSE meet_cur;
  END IF;

  IF ( v_stof_id IS NULL
    OR NVL( meet_rec.meti_afgerond, 'N' ) = 'J'
     )
  THEN
    RETURN;
  ELSE
    IF ( kgc_onde_00.afgerond( p_meti_id => meet_rec.meti_id ) )
    THEN
      RETURN;
    END IF;
  END IF;

  -- filter [aantal] uit v_waarde (cyto: 46XY[20])
  LOOP
    EXIT WHEN ( INSTR( v_waarde, '[' ) = 0 OR INSTR( v_waarde, ']' ) = 0 );
    DECLARE
      l_pos_1 NUMBER;
      l_pos_2 NUMBER;
    BEGIN
      l_pos_1 := INSTR( v_waarde, '[' );
      IF ( l_pos_1 > 0 )
      THEN
        l_pos_2 := INSTR( v_waarde, ']', l_pos_1 );
      END IF;
      IF ( l_pos_1 > 0 AND l_pos_2 > l_pos_1 )
      THEN
        IF ( l_pos_1 = 1 )
        THEN
          v_waarde := SUBSTR( v_waarde, l_pos_2 + 1 );
        ELSE
          v_waarde := SUBSTR( v_waarde, 1, l_pos_1 - 1 )
                   || SUBSTR( v_waarde, l_pos_2 + 1 );
        END IF;
      ELSE
        EXIT;
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        EXIT;
    END;
  END LOOP;

  -- normaalwaarden / ranges zijn numeriek!
  BEGIN
    v_num_waarde := TO_NUMBER( v_waarde );
  EXCEPTION
    WHEN OTHERS
    THEN
      v_num_waarde := NULL;
  END;

  -- Zoek een geschikte rij...
  FOR r IN mere_cur
  LOOP
    IF ( r.waarde_va = '<NW_ONDER>' )
    THEN
      r.waarde_va := meet_rec.normaalwaarde_ondergrens;
    ELSIF ( r.waarde_va = '<NW_BOVEN>' )
    THEN
      r.waarde_va := meet_rec.normaalwaarde_bovengrens;
    END IF;
    IF ( r.waarde_tm = '<NW_ONDER>' )
    THEN
      r.waarde_tm := meet_rec.normaalwaarde_ondergrens;
    ELSIF ( r.waarde_tm = '<NW_BOVEN>' )
    THEN
      r.waarde_tm := meet_rec.normaalwaarde_bovengrens;
    END IF;

    IF ( r.waarde_va IS NULL
     AND r.waarde_tm IS NULL
       )
    THEN
      mere_rec := r; -- de eerste is goed genoeg
      EXIT;
    ELSE
      -- zonder waarde hoeft de rest niet
      IF ( v_waarde ) IS NULL
      THEN
        mere_rec.mere_id := NULL;
        exit;
      END IF;

      IF ( numeriek( v_waarde )
       AND numeriek( r.waarde_va )
       AND numeriek( r.waarde_tm )
         )
      THEN
        IF ( r.excl_grenzen = 'J' )
        THEN
          IF ( ( v_num_waarde > TO_NUMBER( r.waarde_va ) OR r.waarde_va IS NULL )
           AND ( v_num_waarde < TO_NUMBER( r.waarde_tm ) OR r.waarde_tm IS NULL )
             )
          THEN
            mere_rec := r;
            EXIT;
          END IF;
        ELSE
          IF ( v_num_waarde >= NVL( TO_NUMBER( r.waarde_va ), v_num_waarde )
           AND v_num_waarde <= NVL( TO_NUMBER( r.waarde_tm ), v_num_waarde )
             )
          THEN
            mere_rec := r;
            EXIT;
          END IF;
        END IF;
      ELSE -- Bij alfanumeriek vergelijken UPPER-CASE hanteren:
        IF ( r.excl_grenzen = 'J' )
        THEN
          IF ( ( UPPER( v_waarde ) > r.waarde_va OR r.waarde_va IS NULL )
           AND ( UPPER( v_waarde ) < r.waarde_tm OR r.waarde_tm IS NULL )
             )
          THEN
            mere_rec := r;
            EXIT;
          END IF;
        ELSE
          IF ( UPPER( v_waarde ) >= NVL( r.waarde_va, v_waarde )
           AND UPPER( v_waarde ) <= NVL( r.waarde_tm, v_waarde )
             )
          THEN
            mere_rec := r;
            EXIT;
          END IF;
        END IF;
      END IF;
    END IF;
  END LOOP;

  IF ( mere_cur%isopen )
  THEN
    CLOSE mere_cur;
  END IF;

  IF ( mere_rec.mere_id IS NOT NULL )
  THEN
    -- alleen tekst als meetwaarde afgerond is
    -- nb.: meetwaarde is ook afgerond als bovenliggende meting afgeron is (gaat worden)
    IF ( v_afgerond = 'N'
     AND mere_rec.als_meetwaarde_afgerond = 'J'
     AND not p_meti_afgerond = 'J'
       )
    THEN
      RETURN;
    END IF;

    -- controle
    x_strengheid := mere_rec.controle_strengheid;
    IF ( mere_rec.controle_functie IS NOT NULL )
    THEN
      v_statement := 'begin :result := '||mere_rec.controle_functie||'(p_meet_id => '||TO_CHAR( p_meet_id )||'); end;';
      v_result := 'J';
      BEGIN
        v_result := kgc_util_00.dyn_exec( v_statement );
      EXCEPTION
        WHEN OTHERS
        THEN
          RAISE; -- NULL; ??
      END;
      x_controle_ok := NVL( v_result, 'J' ) = 'J';
      IF ( NOT x_controle_ok )
      THEN
        x_melding := mere_rec.controle_foutmelding;
      END IF;
    END IF;

    IF ( NOT x_controle_ok
     AND NVL( x_strengheid, 'W' ) = 'E'
       )
    THEN
      RETURN;
    END IF;

    -- tekst
    IF ( mere_rec.tekst IS NOT NULL )
    THEN
      mere_rec.tekst := REPLACE( mere_rec.tekst, '<meetwaarde>', v_waarde );
      mere_rec.tekst := REPLACE( mere_rec.tekst, '<MEETWAARDE>', v_waarde );
      mere_rec.tekst := REPLACE( mere_rec.tekst, '<NW_ONDER>', v_string1 ); --Added for Mantis 5304
      mere_rec.tekst := REPLACE( mere_rec.tekst, '<NW_BOVEN>', v_string2 ); --Added for Mantis 5304

      IF ( mere_rec.meting_overschrijven = 'D' ) -- Default-waarde, dus alleen als tekst nog leeg is
      THEN
        IF ( x_tekst IS NULL )
        THEN
          x_tekst := mere_rec.tekst;
        END IF;
      ELSIF ( mere_rec.meting_overschrijven = 'J'
        OR x_tekst IS NULL
         )
      THEN
        x_tekst := mere_rec.tekst;
      ELSIF ( INSTR( x_tekst, mere_rec.tekst ) > 0 )
      THEN
        NULL;
      ELSE
        x_tekst := x_tekst||CHR(10)||mere_rec.tekst;
      END IF;
    END IF;
  END IF;
END resultaat;

FUNCTION resultaat
( p_meet_id IN NUMBER := NULL
, p_stof_id IN NUMBER := NULL
, p_afgerond IN VARCHAR2 := NULL
, p_waarde IN VARCHAR2
, p_tekst IN VARCHAR2 :=  NULL
, p_meti_afgerond IN VARCHAR2 := 'N'
 )
 RETURN  VARCHAR2
 IS
  v_tekst VARCHAR2(2000) := p_tekst;
  v_controle_ok BOOLEAN := TRUE;
  v_severity VARCHAR2(1) := 'I';
  v_melding VARCHAR2(200) := NULL;
 BEGIN
  resultaat
  ( p_meet_id => p_meet_id
  , p_stof_id => p_stof_id
  , p_afgerond => p_afgerond
  , p_waarde => p_waarde
  , p_meti_afgerond => p_meti_afgerond
  , x_tekst => v_tekst
  , x_controle_ok => v_controle_ok
  , x_strengheid => v_severity
  , x_melding => v_melding
  );
  IF ( NOT v_controle_ok
   AND v_melding IS NOT NULL
     )
  THEN
    qms$errors.show_message
    ( p_mesg => v_melding
    , p_errtp => NVL( v_severity, 'W' )
    );
  END IF;
  RETURN( v_tekst );
END resultaat;
END  kgc_mere_00;
/

/
QUIT
