CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_AUTHORIZATION"
IS
   /*
   ================================================================================
   Package Name : gen_pck_authorization
   Usage        : This package contains all procedures and functions which are being
                  used within the applications for authorization purposes.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   FUNCTION f_empl_appl_authorization(i_user_id IN NUMBER)
      RETURN BOOLEAN;
   FUNCTION f_appl_component_authorization(i_user_id         IN NUMBER
                                          ,i_appl_component  IN VARCHAR2)
      RETURN BOOLEAN;
   FUNCTION f_empl_role_authorization_ch(i_user_id    IN NUMBER
                                        ,i_role_code  IN VARCHAR2)
      RETURN VARCHAR2;
   FUNCTION f_empl_role_authorization(i_user_id    IN NUMBER
                                     ,i_role_code  IN VARCHAR2)
      RETURN BOOLEAN;
   FUNCTION f_chk_component_auth(i_application_id  IN NUMBER
                                ,i_user_name       IN VARCHAR2
                                ,i_role_id         IN NUMBER DEFAULT NULL
                                ,i_page_id         IN NUMBER DEFAULT NULL
                                ,i_component_type  IN VARCHAR2
                                ,i_component       IN VARCHAR2 DEFAULT NULL
                                ,i_privilege       IN VARCHAR2)
      RETURN BOOLEAN;
   FUNCTION f_chk_component_authorization(
      i_application_id  IN NUMBER
     ,i_user_id         IN NUMBER
     ,i_role_id         IN NUMBER DEFAULT NULL
     ,i_page_id         IN NUMBER DEFAULT NULL
     ,i_component_type  IN VARCHAR2
     ,i_component       IN VARCHAR2 DEFAULT NULL
     ,i_privilege       IN VARCHAR2)
      RETURN BOOLEAN;
   FUNCTION f_chk_component_auth_multi(
      i_application_id  IN NUMBER
     ,i_user_name       IN VARCHAR2
     ,i_role_id         IN NUMBER DEFAULT NULL
     ,i_page_id         IN NUMBER DEFAULT NULL
     ,i_component_type  IN VARCHAR2
     ,i_component       IN VARCHAR2 DEFAULT NULL
     ,i_privileges      IN VARCHAR2)
      RETURN BOOLEAN;
   FUNCTION f_ldap(i_username  IN VARCHAR2
                  ,i_password  IN VARCHAR2)
      RETURN BOOLEAN;
   PROCEDURE p_refresh_autorisatie_matrix(i_application_id IN NUMBER);
   PROCEDURE p_print_auth_matrix(
      i_app_id              IN apex_application_pages.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE DEFAULT NULL
     ,i_page_search_string  IN VARCHAR2 DEFAULT NULL);
   PROCEDURE p_set_select_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_select%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE);
   PROCEDURE p_set_insert_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_insert%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE);
   PROCEDURE p_set_update_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_update%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE);
   PROCEDURE p_set_delete_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_delete%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE);
   PROCEDURE p_set_privs_authorization(
      i_application_id    IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id           IN gen_appl_roles.id%TYPE
     ,i_component_page    IN gen_appl_authorizations.component_page%TYPE
     ,i_privilege_select  IN gen_appl_authorizations.privilege_select%TYPE
     ,i_privilege_insert  IN gen_appl_authorizations.privilege_insert%TYPE
     ,i_privilege_update  IN gen_appl_authorizations.privilege_update%TYPE
     ,i_privilege_delete  IN gen_appl_authorizations.privilege_delete%TYPE --,p_component_function      IN gen_appl_authorizations.component_function%TYPE
                                                                          );
END gen_pck_authorization;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_AUTHORIZATION"
IS
   /*
   ================================================================================
   Package Name : gen_pck_authorization
   Usage        : This package contains all procedures and functions which are being
                  used within the applications for authorization purposes.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */

   g_package  CONSTANT VARCHAR2(31) := $$plsql_unit || '.';
   --
   -- -----------------------------------------------------------------
   -- Function to determine if an employee is authorized to start a certain
   -- application
   -- -----------------------------------------------------------------
   FUNCTION f_empl_appl_authorization(i_user_id IN NUMBER)
      RETURN BOOLEAN
   IS
      l_found  NUMBER;
   BEGIN
      SELECT COUNT(*)
        INTO l_found
        FROM gen_appl_users aus
       WHERE id = i_user_id
         AND NVL(aus.active_ind, 'N') = 'Y';
      IF NVL(l_found, 0) > 0
      THEN
         RETURN (TRUE);
      ELSE
         RETURN (FALSE);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (FALSE);
   END f_empl_appl_authorization;
   -- -----------------------------------------------------------------
   -- Function that determines if a user has a certain role.Returning Y/N
   -- -----------------------------------------------------------------
   FUNCTION f_empl_role_authorization_ch(i_user_id    IN NUMBER
                                        ,i_role_code  IN VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      IF f_empl_role_authorization(i_user_id
                                  ,i_role_code)
      THEN
         RETURN 'Y';
      ELSE
         RETURN 'N';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END;
   -- -----------------------------------------------------------------
   -- Function that determines if a user has a certain role.Returning Boolean
   -- -----------------------------------------------------------------
   FUNCTION f_empl_role_authorization(i_user_id    IN NUMBER
                                     ,i_role_code  IN VARCHAR2)
      RETURN BOOLEAN
   IS
      CURSOR c_found
      IS
         SELECT 1
           FROM (SELECT arl.role_code
                   FROM gen_appl_users aus
                        INNER JOIN gen_appl_user_roles aur
                           ON (aus.id = aur.appl_user_id)
                        INNER JOIN gen_appl_role_roles arr
                           ON (aur.appl_role_id =
                                  arr.authentication_appl_role_id)
                        INNER JOIN gen_appl_roles arl
                           ON (arr.authorization_appl_role_id = arl.id)
                  WHERE arl.active_ind = 'Y'
                    AND aur.active_ind = 'Y'
                    AND aus.active_ind = 'Y'
                    AND aus.id = i_user_id
                 UNION
                 -- only the special roles directly connectected to the user
                 SELECT arl.role_code
                   FROM gen_appl_users aus
                        INNER JOIN gen_appl_user_roles aur
                           ON (aus.id = aur.appl_user_id)
                        INNER JOIN gen_appl_roles arl
                           ON (aur.appl_role_id = arl.id)
                  WHERE arl.active_ind = 'Y'
                    AND aur.active_ind = 'Y'
                    AND aus.active_ind = 'Y'
                    AND arl.authorization_ind = 'S'
                    AND aus.id = i_user_id)
          WHERE UPPER(role_code) = i_role_code;
      l_found   PLS_INTEGER;
      l_arl_id  gen_appl_roles.id%TYPE;
   BEGIN
      OPEN c_found;
      FETCH c_found
         INTO l_found;
      CLOSE c_found;
      IF l_found IS NOT NULL
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (FALSE);
   END f_empl_role_authorization;
   -- -----------------------------------------------------------------
   -- Function that determines if a user is allowed to see/start a
   -- certain application component
   -- -----------------------------------------------------------------
   FUNCTION f_appl_component_authorization(i_user_id         IN NUMBER
                                          ,i_appl_component  IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_found  NUMBER;
   BEGIN
      -- determine User/Role combination
      SELECT COUNT(*)
        INTO l_found
        FROM gen_appl_user_roles aur
       WHERE aur.appl_user_id = i_user_id
         AND aur.appl_role_id IN (SELECT arr.authentication_appl_role_id
                                    FROM gen_appl_role_roles arr
                                         INNER JOIN gen_appl_roles arl
                                            ON (arr.authorization_appl_role_id =
                                                   arl.id)
                                   WHERE UPPER(arl.role_code) LIKE
                                            UPPER(i_appl_component || '_%'))
         AND UPPER(aur.active_ind) = 'Y';
      IF l_found > 0
      THEN
         RETURN (TRUE);
      ELSE
         RETURN (FALSE);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (FALSE);
   END f_appl_component_authorization;
   -----------------------------------------------------------------------
   -- Function that determines if a user is authorized to approach a certain
   -- application component with a certain privilege
   -----------------------------------------------------------------------
   FUNCTION f_chk_component_auth(i_application_id  IN NUMBER
                                ,i_user_name       IN VARCHAR2
                                ,i_role_id         IN NUMBER DEFAULT NULL
                                ,i_page_id         IN NUMBER DEFAULT NULL
                                ,i_component_type  IN VARCHAR2
                                ,i_component       IN VARCHAR2 DEFAULT NULL
                                ,i_privilege       IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_user_id  NUMBER;
   BEGIN
      BEGIN
         SELECT id
           INTO l_user_id
           FROM gen_appl_users
          WHERE UPPER(user_id) = UPPER(i_user_name)
            AND active_ind = 'Y';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id   := -1; --User not in user table (general user)
      END;
      RETURN gen_pck_authorization.f_chk_component_authorization(
                i_application_id
               ,l_user_id
               ,i_role_id
               ,i_page_id
               ,i_component_type
               ,i_component
               ,i_privilege);
   END f_chk_component_auth;
   -----------------------------------------------------------------------
   -- Function that determines if a user is authorized to approach a certain
   -- application component with a certain privilege
   -----------------------------------------------------------------------
   FUNCTION f_chk_component_authorization(
      i_application_id  IN NUMBER
     ,i_user_id         IN NUMBER
     ,i_role_id         IN NUMBER DEFAULT NULL
     ,i_page_id         IN NUMBER DEFAULT NULL
     ,i_component_type  IN VARCHAR2
     ,i_component       IN VARCHAR2 DEFAULT NULL
     ,i_privilege       IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_authorized  NUMBER;
      l_user_id     NUMBER;
   BEGIN
      --return true;
      IF i_application_id IS NULL
      OR i_user_id = -1
      OR i_user_id IS NULL
      THEN
         RETURN FALSE;
      END IF;
      SELECT COUNT(*)
        INTO l_authorized
        FROM gen_appl_authorizations aas
       WHERE aas.appl_role_id IN (SELECT arr.authorization_appl_role_id
                                    FROM gen_appl_role_roles arr
                                         INNER JOIN gen_appl_roles arl
                                            ON (arr.authentication_appl_role_id =
                                                   arl.id)
                                   WHERE arl.id IN (SELECT aur.appl_role_id
                                                      FROM gen_appl_user_roles aur
                                                     WHERE aur.appl_user_id =
                                                              i_user_id
                                                       AND aur.appl_role_id =
                                                              NVL(
                                                                 i_role_id
                                                                ,aur.appl_role_id)
                                                       AND UPPER(
                                                              aur.active_ind) =
                                                              'Y'))
         AND ((UPPER(i_component_type) = 'TAB'
           AND UPPER(aas.component_tab) = UPPER(i_component)
           AND aas.component_page IS NULL
           AND aas.component_item IS NULL
           AND aas.component_function IS NULL)
           OR (UPPER(i_component_type) = 'PAGE'
           AND UPPER(aas.component_page) =
                  (SELECT UPPER(page_alias)
                     FROM apex_application_pages
                    WHERE application_id = i_application_id
                      AND page_id = i_page_id)
           AND aas.component_tab IS NULL
           AND aas.component_item IS NULL
           AND aas.component_function IS NULL)
           OR (UPPER(i_component_type) = 'ITEM'
           AND UPPER(aas.component_item) = UPPER(i_component)
           AND UPPER(aas.component_page) =
                  (SELECT UPPER(page_alias)
                     FROM apex_application_pages
                    WHERE application_id = i_application_id
                      AND page_id = i_page_id)
           AND aas.component_tab IS NULL
           AND aas.component_function IS NULL)
           OR (UPPER(i_component_type) = 'FUNCTION'
           AND UPPER(aas.component_function) = UPPER(i_component)
           AND UPPER(aas.component_page) =
                  (SELECT UPPER(page_alias)
                     FROM apex_application_pages
                    WHERE application_id = i_application_id
                      AND page_id = i_page_id)
           AND aas.component_tab IS NULL
           AND aas.component_item IS NULL))
         AND ((UPPER(i_privilege) = 'S'
           AND UPPER(aas.privilege_select) = 'Y')
           OR (UPPER(i_privilege) = 'I'
           AND UPPER(aas.privilege_insert) = 'Y')
           OR (UPPER(i_privilege) = 'U'
           AND UPPER(aas.privilege_update) = 'Y')
           OR (UPPER(i_privilege) = 'D'
           AND UPPER(aas.privilege_delete) = 'Y'));
      IF NVL(l_authorized, 0) > 0
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   END;
   -----------------------------------------------------------------------
   -- Function that determines if a user is authorized to approach a certain
   -- application component with one of certain privileges
   -----------------------------------------------------------------------
   FUNCTION f_chk_component_auth_multi(
      i_application_id  IN NUMBER
     ,i_user_name       IN VARCHAR2
     ,i_role_id         IN NUMBER DEFAULT NULL
     ,i_page_id         IN NUMBER DEFAULT NULL
     ,i_component_type  IN VARCHAR2
     ,i_component       IN VARCHAR2 DEFAULT NULL
     ,i_privileges      IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_authorized  NUMBER;
      l_user_id     NUMBER;
   BEGIN
      BEGIN
         SELECT id
           INTO l_user_id
           FROM gen_appl_users
          WHERE UPPER(user_id) = UPPER(i_user_name)
            AND active_ind = 'Y';
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN FALSE; --User not in user table (general user)
      END;
      IF i_application_id IS NULL
      OR l_user_id IS NULL
      THEN
         RETURN FALSE;
      END IF;
      SELECT COUNT(*)
        INTO l_authorized
        FROM gen_appl_authorizations aas
       WHERE aas.appl_role_id IN (SELECT arr.authorization_appl_role_id
                                    FROM gen_appl_role_roles arr
                                         INNER JOIN gen_appl_roles arl
                                            ON (arr.authentication_appl_role_id =
                                                   arl.id)
                                   WHERE arl.id IN (SELECT aur.appl_role_id
                                                      FROM gen_appl_user_roles aur
                                                     WHERE aur.appl_user_id =
                                                              l_user_id
                                                       AND aur.appl_role_id =
                                                              NVL(
                                                                 i_role_id
                                                                ,aur.appl_role_id)
                                                       AND UPPER(
                                                              aur.active_ind) =
                                                              'Y'))
         AND ((UPPER(i_component_type) = 'TAB'
           AND UPPER(aas.component_tab) = UPPER(i_component)
           AND aas.component_page IS NULL
           AND aas.component_item IS NULL
           AND aas.component_function IS NULL)
           OR (UPPER(i_component_type) = 'PAGE'
           AND UPPER(aas.component_page) =
                  (SELECT UPPER(page_alias)
                     FROM apex_application_pages
                    WHERE application_id = i_application_id
                      AND page_id = i_page_id)
           AND aas.component_tab IS NULL
           AND aas.component_item IS NULL
           AND aas.component_function IS NULL)
           OR (UPPER(i_component_type) = 'ITEM'
           AND UPPER(aas.component_item) = UPPER(i_component)
           AND UPPER(aas.component_page) =
                  (SELECT UPPER(page_alias)
                     FROM apex_application_pages
                    WHERE application_id = i_application_id
                      AND page_id = i_page_id)
           AND aas.component_tab IS NULL
           AND aas.component_function IS NULL)
           OR (UPPER(i_component_type) = 'FUNCTION'
           AND UPPER(aas.component_function) = UPPER(i_component)
           AND UPPER(aas.component_page) =
                  (SELECT UPPER(page_alias)
                     FROM apex_application_pages
                    WHERE application_id = i_application_id
                      AND page_id = i_page_id)
           AND aas.component_tab IS NULL
           AND aas.component_item IS NULL))
         AND ((INSTR(UPPER(i_privileges)
                    ,'S') > 0
           AND UPPER(aas.privilege_select) = 'Y')
           OR (INSTR(UPPER(i_privileges)
                    ,'I') > 0
           AND UPPER(aas.privilege_insert) = 'Y')
           OR (INSTR(UPPER(i_privileges)
                    ,'U') > 0
           AND UPPER(aas.privilege_update) = 'Y')
           OR (INSTR(UPPER(i_privileges)
                    ,'D') > 0
           AND UPPER(aas.privilege_delete) = 'Y'));
      IF NVL(l_authorized, 0) > 0
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   END;
   FUNCTION f_ldap(i_username  IN VARCHAR2
                  ,i_password  IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_ldap_port       NUMBER;
      l_ldap_host       VARCHAR2(100);
      l_ldap_domain     VARCHAR2(100);
      --
      l_retval          PLS_INTEGER;
      l_retval2         PLS_INTEGER;
      l_session         dbms_ldap.session;
      --
      l_application_id  NUMBER;
      l_proc   CONSTANT VARCHAR2(61) := g_package || 'f_ldap';
   BEGIN
      -- initialize l_application_id
      l_application_id   := v('APP_ID');
      --
      l_ldap_port        :=
         gen_pck_general.f_get_appl_parameter_value('LDAP_PORT'
                                                 ,l_application_id);
      l_ldap_host        :=
         gen_pck_general.f_get_appl_parameter_value('LDAP_HOST'
                                                 ,l_application_id);
      l_ldap_domain      :=
         gen_pck_general.f_get_appl_parameter_value('LDAP_DOMAIN'
                                                 ,l_application_id);
      --
      l_session          :=
         dbms_ldap.init(l_ldap_host
                       ,l_ldap_port);
      l_retval           :=
         dbms_ldap.simple_bind_s(l_session
                                ,l_ldap_domain || '\' || i_username
                                ,i_password);
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END f_ldap;
   PROCEDURE p_refresh_autorisatie_matrix(i_application_id IN NUMBER)
   IS
   BEGIN
      -- delete rows with no existant aliases
      DELETE FROM gen_appl_authorizations aas
            WHERE aas.component_page IS NOT NULL
              AND NOT EXISTS
                     (SELECT 1
                        FROM apex_application_pages aap
                       WHERE aap.application_id = i_application_id
                         AND UPPER(aap.page_alias) = aas.component_page);
      FOR r_rol IN (SELECT id
                      FROM gen_appl_roles
                     WHERE authorization_ind = 'Y')
      LOOP
         FOR r_pag IN (SELECT aap.*
                         FROM apex_application_pages aap
                        WHERE aap.application_id = i_application_id
                          AND aap.page_id NOT IN (0
                                                 ,101
                                                 ,102)
                          AND UPPER(page_alias) NOT LIKE '%MODAL%'
                          AND aap.page_alias IS NOT NULL
                          AND NOT EXISTS
                                 (SELECT 1
                                    FROM gen_appl_authorizations aas
                                   WHERE aas.component_page = aap.page_alias
                                     AND aas.appl_role_id = r_rol.id))
         LOOP
            INSERT INTO gen_appl_authorizations(component_page
                                               ,appl_role_id)
                 VALUES (r_pag.page_alias
                        ,r_rol.id);
         END LOOP;
      END LOOP;
   END;
   PROCEDURE p_print_auth_matrix(
      i_app_id              IN apex_application_pages.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE DEFAULT NULL
     ,i_page_search_string  IN VARCHAR2 DEFAULT NULL)
   IS
      CURSOR c_content(
         b_page_alias   gen_appl_authorizations.component_page%TYPE
        ,b_role_id      gen_appl_roles.id%TYPE)
      IS
         SELECT    '<input type="checkbox"  name="set-select"'
                || CASE
                      WHEN COALESCE(MAX(privilege_select)
                                   ,'N') = 'Y'
                      THEN
                         ' checked=""'
                   END
                || '/><label class="checkbox-label">Select</label>' -- ||'<br/>'
                || '<input type="checkbox"  name="set-insert"'
                || CASE
                      WHEN COALESCE(MAX(privilege_insert)
                                   ,'N') = 'Y'
                      THEN
                         ' checked=""'
                   END
                || '/><label class="checkbox-label">Insert</label>' -- ||'<br/>'
                || '<input type="checkbox" name="set-update" '
                || CASE
                      WHEN COALESCE(MAX(privilege_update)
                                   ,'N') = 'Y'
                      THEN
                         ' checked=""'
                   END
                || '/><label class="checkbox-label">Update</label>' -- ||'<br/>'
                || '<input type="checkbox" name="set-delete" '
                || CASE
                      WHEN COALESCE(MAX(privilege_delete)
                                   ,'N') = 'Y'
                      THEN
                         ' checked=""'
                   END
                || '/><label class="checkbox-label">Delete</label>'
                   contents
           FROM gen_appl_authorizations auth
          WHERE auth.appl_role_id = b_role_id
            AND auth.component_page = b_page_alias
            AND auth.application_id = i_app_id;
      r_content    c_content%ROWTYPE;
      l_ind        PLS_INTEGER := 0;
      l_num_roles  PLS_INTEGER := 0;
   BEGIN
      SELECT COUNT(*)
        INTO l_num_roles
        FROM gen_appl_roles
       WHERE authorization_ind = 'N'
         AND id = COALESCE(i_role_id
                          ,id);
      htp.p(
         '<table style="width:800px"><tr><td colspan="2" align="center" class="legend">Role</td></tr><tr><td><div class="rotate legend">Page</div></td><td>');
      htp.p(
         '<div class="auth-matrix-parent" ><table cellspacing="0" id="auth-matrix"><tbody><tr >');
      htp.p('<th class="ui-widget-header">&nbsp;</th>');
      FOR r_role IN (  SELECT *
                         FROM gen_appl_roles
                        WHERE authorization_ind = 'Y' --'N'
                          AND id = COALESCE(i_role_id
                                           ,id)
                     ORDER BY role_name)
      LOOP
         htp.p(
               '<th class="no-left-border ui-widget-header">'
            || r_role.role_name
            || '</th>');
      END LOOP;
      htp.p('</tr>');
      FOR r_page IN (  SELECT ape.page_id || ': ' || page_title page_title
                             ,page_alias
                             ,authorization_scheme
                         FROM apex_application_pages ape
                        WHERE ape.application_id = i_app_id
                          AND ape.page_alias IS NOT NULL
                          AND (i_page_search_string IS NULL
                            OR INSTR(
                                  LOWER(
                                        ape.page_id
                                     || ': '
                                     || page_title
                                     || CASE
                                           WHEN authorization_scheme IS NULL
                                           THEN
                                              ' (No Authorization)'
                                        END)
                                 ,LOWER(i_page_search_string)) > 0)
                     ORDER BY ape.page_id
                             ,ape.page_title)
      LOOP
         htp.p(
               '<tr class="'
            || CASE
                  WHEN MOD(l_ind
                          ,2) = 1
                  THEN
                     'even'
                  ELSE
                     'odd'
               END
            || '">'
            || '<td class="header ui-widget-header">'
            || r_page.page_title
            || CASE
                  WHEN r_page.authorization_scheme IS NULL
                  THEN
                     ' (No Authorization)'
               END
            || '</td>');
         FOR r_role IN (  SELECT *
                            FROM gen_appl_roles
                           WHERE authorization_ind = 'Y' --'N'
                             AND id = COALESCE(i_role_id
                                              ,id)
                        ORDER BY role_name)
         LOOP
            r_content   := NULL;
            OPEN c_content(r_page.page_alias
                          ,r_role.id);
            FETCH c_content
               INTO r_content;
            CLOSE c_content;
            htp.p(   '<td class="matrix-field" '
                  || ' data-role-id="'
                  || r_role.id
                  || '" data-role-name="'
                  || r_role.role_name
                  || '" data-page-alias="'
                  || r_page.page_alias
                  || '" data-page-title="'
                  || r_page.page_title
                  || '">'
                  || COALESCE(r_content.contents
                             ,'&nbsp;')
                  || '</td>');
         END LOOP;
         htp.p('</tr>');
         l_ind   := l_ind + 1;
      END LOOP;
      --
      --
      htp.p('</tbody>');
      htp.p('</table></div>');
      htp.p('</td></tr></table>');
   END p_print_auth_matrix;
   PROCEDURE p_set_select_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_select%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE)
   IS
   BEGIN
      MERGE INTO gen_appl_authorizations auth
           USING (SELECT i_role_id role_id
                        ,i_component_page page
                        ,i_value VALUE
                        ,i_component_function component_function
                        ,i_application_id application_id
                    FROM DUAL) priv
              ON (priv.application_id = auth.application_id
              AND priv.page = auth.component_page
              AND priv.role_id = auth.appl_role_id
              AND COALESCE(auth.component_function
                          ,'_NULL') = COALESCE(priv.component_function
                                              ,'_NULL'))
      WHEN MATCHED
      THEN
         UPDATE SET privilege_select   = priv.VALUE
         DELETE
                 WHERE auth.privilege_select = 'N'
                   AND auth.privilege_insert = 'N'
                   AND auth.privilege_update = 'N'
                   AND auth.privilege_delete = 'N'
      WHEN NOT MATCHED
      THEN
         INSERT     (auth.component_page
                    ,auth.appl_role_id
                    ,auth.privilege_select
                    ,auth.component_function
                    ,auth.application_id)
             VALUES (priv.page
                    ,priv.role_id
                    ,priv.VALUE
                    ,priv.component_function
                    ,priv.application_id);
   END p_set_select_authorization;
   PROCEDURE p_set_insert_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_insert%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE)
   IS
   BEGIN
      MERGE INTO gen_appl_authorizations auth
           USING (SELECT i_role_id role_id
                        ,i_component_page page
                        ,i_value VALUE
                        ,i_component_function component_function
                        ,i_application_id application_id
                    FROM DUAL) priv
              ON (priv.application_id = auth.application_id
              AND priv.page = auth.component_page
              AND priv.role_id = auth.appl_role_id
              AND COALESCE(auth.component_function
                          ,'_NULL') = COALESCE(priv.component_function
                                              ,'_NULL'))
      WHEN MATCHED
      THEN
         UPDATE SET privilege_insert   = priv.VALUE
         DELETE
                 WHERE auth.privilege_select = 'N'
                   AND auth.privilege_insert = 'N'
                   AND auth.privilege_update = 'N'
                   AND auth.privilege_delete = 'N'
      WHEN NOT MATCHED
      THEN
         INSERT     (auth.component_page
                    ,auth.appl_role_id
                    ,auth.privilege_insert
                    ,auth.component_function
                    ,auth.application_id)
             VALUES (priv.page
                    ,priv.role_id
                    ,priv.VALUE
                    ,priv.component_function
                    ,priv.application_id);
   END p_set_insert_authorization;
   PROCEDURE p_set_update_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_update%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE)
   IS
   BEGIN
      MERGE INTO gen_appl_authorizations auth
           USING (SELECT i_role_id role_id
                        ,i_component_page page
                        ,i_value VALUE
                        ,i_component_function component_function
                        ,i_application_id application_id
                    FROM DUAL) priv
              ON (priv.application_id = auth.application_id
              AND priv.page = auth.component_page
              AND priv.role_id = auth.appl_role_id
              AND COALESCE(auth.component_function
                          ,'_NULL') = COALESCE(priv.component_function
                                              ,'_NULL'))
      WHEN MATCHED
      THEN
         UPDATE SET privilege_update   = priv.VALUE
         DELETE
                 WHERE auth.privilege_select = 'N'
                   AND auth.privilege_insert = 'N'
                   AND auth.privilege_update = 'N'
                   AND auth.privilege_delete = 'N'
      WHEN NOT MATCHED
      THEN
         INSERT     (auth.component_page
                    ,auth.appl_role_id
                    ,auth.privilege_update
                    ,auth.component_function
                    ,auth.application_id)
             VALUES (priv.page
                    ,priv.role_id
                    ,priv.VALUE
                    ,priv.component_function
                    ,priv.application_id);
   END p_set_update_authorization;
   PROCEDURE p_set_delete_authorization(
      i_application_id      IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id             IN gen_appl_roles.id%TYPE
     ,i_component_page      IN gen_appl_authorizations.component_page%TYPE
     ,i_value               IN gen_appl_authorizations.privilege_delete%TYPE
     ,i_component_function  IN gen_appl_authorizations.component_function%TYPE)
   IS
   BEGIN
      MERGE INTO gen_appl_authorizations auth
           USING (SELECT i_role_id role_id
                        ,i_component_page page
                        ,i_value VALUE
                        ,i_component_function component_function
                        ,i_application_id application_id
                    FROM DUAL) priv
              ON (priv.application_id = auth.application_id
              AND priv.page = auth.component_page
              AND priv.role_id = auth.appl_role_id
              AND COALESCE(auth.component_function
                          ,'_NULL') = COALESCE(priv.component_function
                                              ,'_NULL'))
      WHEN MATCHED
      THEN
         UPDATE SET privilege_delete   = priv.VALUE
         DELETE
                 WHERE auth.privilege_select = 'N'
                   AND auth.privilege_insert = 'N'
                   AND auth.privilege_update = 'N'
                   AND auth.privilege_delete = 'N'
      WHEN NOT MATCHED
      THEN
         INSERT     (auth.component_page
                    ,auth.appl_role_id
                    ,auth.privilege_delete
                    ,auth.component_function
                    ,auth.application_id)
             VALUES (priv.page
                    ,priv.role_id
                    ,priv.VALUE
                    ,priv.component_function
                    ,priv.application_id);
   END p_set_delete_authorization;
   PROCEDURE p_set_privs_authorization(
      i_application_id    IN gen_appl_authorizations.application_id%TYPE
     ,i_role_id           IN gen_appl_roles.id%TYPE
     ,i_component_page    IN gen_appl_authorizations.component_page%TYPE
     ,i_privilege_select  IN gen_appl_authorizations.privilege_select%TYPE
     ,i_privilege_insert  IN gen_appl_authorizations.privilege_insert%TYPE
     ,i_privilege_update  IN gen_appl_authorizations.privilege_update%TYPE
     ,i_privilege_delete  IN gen_appl_authorizations.privilege_delete%TYPE)
   IS
   BEGIN
      MERGE INTO gen_appl_authorizations auth
           USING (SELECT i_role_id role_id
                        ,i_component_page page
                        ,i_privilege_select privilege_select
                        ,i_privilege_insert privilege_insert
                        ,i_privilege_update privilege_update
                        ,i_privilege_delete privilege_delete
                        ,i_application_id application_id
                    FROM DUAL) priv
              ON (priv.application_id = auth.application_id
              AND priv.page = auth.component_page
              AND priv.role_id = auth.appl_role_id)
      WHEN MATCHED
      THEN
         UPDATE SET auth.privilege_select   = priv.privilege_select
                   ,auth.privilege_insert   = priv.privilege_insert
                   ,auth.privilege_update   = priv.privilege_update
                   ,auth.privilege_delete   = priv.privilege_delete
         DELETE
                 WHERE auth.privilege_select = 'N'
                   AND auth.privilege_insert = 'N'
                   AND auth.privilege_update = 'N'
                   AND auth.privilege_delete = 'N'
      WHEN NOT MATCHED
      THEN
         INSERT     (auth.component_page
                    ,auth.appl_role_id
                    ,auth.privilege_select
                    ,auth.privilege_insert
                    ,auth.privilege_update
                    ,auth.privilege_delete
                    ,auth.application_id)
             VALUES (priv.page
                    ,priv.role_id
                    ,priv.privilege_select
                    ,priv.privilege_insert
                    ,priv.privilege_update
                    ,priv.privilege_delete
                    ,priv.application_id);
   END;
END gen_pck_authorization;
/

/
QUIT
