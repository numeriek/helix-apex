CREATE OR REPLACE PACKAGE "HELIX"."KGC_INDI_00" IS
-- bepaal de indicatie met de hoogste prioriteit bij onderzoek
FUNCTION indi_id
( p_onde_id IN NUMBER
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( indi_id, WNDS, WNPS, RNPS );

-- idem, retourneer indi_code
FUNCTION indi_code
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( indi_code, WNDS, WNPS, RNPS );

-- idem, retourneer indi_omschrijving
FUNCTION indi_omschrijving
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( indi_omschrijving, WNDS, WNPS, RNPS );

-- bepaal onderzoeksreden op basis van indicaties bij onderzoek (voor patienthistorie)
FUNCTION onderzoeksreden
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( onderzoeksreden, WNDS, WNPS, RNPS );

-- bepaal machtigingsindicatie op basis van indicaties bij onderzoek
FUNCTION machtigingsindicatie
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( machtigingsindicatie, WNDS, WNPS, RNPS );

-- idem, retourneer ingr_id
FUNCTION ingr_id
( p_onde_id IN NUMBER
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( ingr_id, WNDS, WNPS, RNPS );

-- idem, retourneer ingr_code
FUNCTION ingr_code
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( ingr_code, WNDS, WNPS, RNPS );

-- bepaal afgeleide gegevens van indicaties bij onderzoek
PROCEDURE afgeleiden_van_indicatie
( p_onde_id IN NUMBER
, x_indicatie OUT VARCHAR2
, x_onderzoeksreden OUT VARCHAR2
, x_machtigingsindicatie OUT VARCHAR2
, x_geplande_einddatum OUT DATE
);

-- alle onderzoeksindicaties: omschrijving (code)
FUNCTION onderzoeksindicaties
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA  RESTRICT_REFERENCES(  onderzoeksindicaties,  WNDS,  WNPS,  RNPS  );

  FUNCTION indicaties_met_opmerking
  ( p_onde_id IN NUMBER
  )
  RETURN VARCHAR2;
  PRAGMA  RESTRICT_REFERENCES( indicaties_met_opmerking, WNDS, WNPS, RNPS );

-- retourneer monster-indicatie
FUNCTION mons_indi_id
( p_mons_id IN NUMBER
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( mons_indi_id, WNDS, WNPS, RNPS );

-- idem, retourneer indi_code
FUNCTION mons_indi_code
( p_mons_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( mons_indi_code, WNDS, WNPS, RNPS );

-- idem, retourneer indi_omschrijving
FUNCTION mons_indi_omschrijving
( p_mons_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( mons_indi_omschrijving, WNDS, WNPS, RNPS );

/* toegevoegd tbv het digitaal (lab) dossier */
FUNCTION mons_indicatie_codes
( p_mons_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( mons_indicatie_codes, WNDS, WNPS, RNPS );


FUNCTION onde_indicatie_codes
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( onde_indicatie_codes, WNDS, WNPS, RNPS );

FUNCTION locus
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( locus, WNDS, WNPS, RNPS );

FUNCTION Onderzoek_indi_omschrijvingen
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
END KGC_INDI_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_INDI_00" IS
/*******************************************************************************
    NAME:       kgc_indi_00
    PURPOSE:    ophalen van indicatie teksten
    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.5        23-12-2015  SKA              Mantis 3957 : In function indi_code variable v_return variable size increased from 10 to 25 chr.
    1.4        06-07-2015  Athar Shaikh     Mantis 3957 : Code Fields increase in Helix up to 25 and the
                                                          family survey number increase to 20
    1.3        24-11-2014  Y.Arts           Mantis 8633 :
                                            KGCBEST02 toegevoegd
*******************************************************************************/
FUNCTION indi_id
( p_onde_id IN NUMBER
)
RETURN NUMBER
IS
  v_return NUMBER(10);
  CURSOR indi_cur
  IS
    SELECT indi.indi_id
    FROM   kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.indi_id = indi.indi_id
    AND    onin.onde_id = p_onde_id
    ORDER BY onin.volgorde ASC
    ,        onin.onin_id ASC
    ;
-- PL/SQL Block
BEGIN
  OPEN  indi_cur;
  FETCH indi_cur
  INTO  v_return;
  CLOSE indi_cur;
  RETURN( v_return );
END indi_id;

FUNCTION indi_code
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(25); -- from 10 to 25 changed by athar for the Mantis-3957 on 06-07-2015
  v_indi_id NUMBER;
  CURSOR indi_cur
  IS
    SELECT indi.code
    FROM   kgc_indicatie_teksten indi
    WHERE  indi.indi_id = v_indi_id
    ;
BEGIN
  v_indi_id := indi_id( p_onde_id => p_onde_id );
  IF ( v_indi_id IS NOT NULL )
  THEN
    OPEN  indi_cur;
    FETCH indi_cur
    INTO  v_return;
    CLOSE indi_cur;
  END IF;
  RETURN( v_return );
END indi_code;

FUNCTION indi_omschrijving
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  v_indi_id NUMBER;
  CURSOR indi_cur
  IS
    SELECT indi.omschrijving
    FROM   kgc_indicatie_teksten indi
    WHERE  indi.indi_id = v_indi_id
    ;
BEGIN
  v_indi_id := indi_id( p_onde_id => p_onde_id );
  IF ( v_indi_id IS NOT NULL )
  THEN
    OPEN  indi_cur;
    FETCH indi_cur
    INTO  v_return;
    CLOSE indi_cur;
  END IF;
  RETURN( v_return );
END indi_omschrijving;

FUNCTION onderzoeksreden
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(250);   ---250 made by Anuja against Mantis 8017...100 changed to 250
  v_indi_id NUMBER;
  CURSOR indi_cur
  IS
    SELECT SUBSTR( NVL( indi.omschrijving_reden, indi.omschrijving )
                 , 1
                 , 250  ---Added by anuja against Mantis 8017...100 changed to 250
                 ) omschrijving
    FROM   kgc_indicatie_teksten indi
    WHERE  indi.indi_id = v_indi_id
    ;
  CURSOR onde_cur
  IS
    SELECT SUBSTR( omschrijving
                 , 1
                 , 100
                 ) omschrijving
    FROM   kgc_onderzoeken
    WHERE  onde_id = p_onde_id
    ;
BEGIN
  v_indi_id := indi_id( p_onde_id => p_onde_id );
  IF ( v_indi_id IS NOT NULL )
  THEN
    OPEN  indi_cur;
    FETCH indi_cur
    INTO  v_return;
    CLOSE indi_cur;
  END IF;
  IF ( v_return IS NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_return;
    CLOSE onde_cur;
  END IF;
  RETURN( v_return );
END onderzoeksreden;

FUNCTION ingr_id
( p_onde_id IN NUMBER
)
RETURN NUMBER
IS
  CURSOR ingr_cur
  IS
    SELECT ingr.ingr_id
    ,      onin.volgorde
    ,      onin.onin_id
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.indi_id = indi.indi_id
    AND    indi.ingr_id = ingr.ingr_id
    AND    ingr.machtigingsindicatie IS NOT NULL
    AND    onin.onde_id = p_onde_id
    UNION
    SELECT ingr.ingr_id
    ,      onin.volgorde
    ,      onin.onin_id
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.ingr_id = ingr.ingr_id
    AND    ingr.machtigingsindicatie IS NOT NULL
    AND    onin.onde_id = p_onde_id
    ORDER BY 2 ASC
    ,        3 ASC
    ;
  ingr_rec ingr_cur%rowtype;
BEGIN
  OPEN  ingr_cur;
  FETCH ingr_cur
  INTO  ingr_rec;
  CLOSE ingr_cur;
  RETURN( ingr_rec.ingr_id );
END ingr_id;

FUNCTION ingr_code
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(10);
  v_ingr_id NUMBER;
  CURSOR ingr_cur
  IS
    SELECT code
    FROM   kgc_indicatie_groepen
    WHERE  ingr_id = v_ingr_id
    ;
BEGIN
  v_ingr_id := ingr_id( p_onde_id => p_onde_id );
  IF ( v_ingr_id IS NOT NULL )
  THEN
    OPEN  ingr_cur;
    FETCH ingr_cur
    INTO  v_return;
    CLOSE ingr_cur;
  END IF;
  RETURN( v_return );
END ingr_code;

FUNCTION machtigingsindicatie
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  v_ingr_id NUMBER;
  CURSOR ingr_cur
  IS
    SELECT machtigingsindicatie
    FROM   kgc_indicatie_groepen
    WHERE  ingr_id = v_ingr_id
    ;
BEGIN
  v_ingr_id := ingr_id( p_onde_id => p_onde_id );
  IF ( v_ingr_id IS NOT NULL )
  THEN
    OPEN  ingr_cur;
    FETCH ingr_cur
    INTO  v_return;
    CLOSE ingr_cur;
  END IF;
  RETURN( v_return );
END machtigingsindicatie;

PROCEDURE afgeleiden_van_indicatie
( p_onde_id IN NUMBER
, x_indicatie OUT VARCHAR2
, x_onderzoeksreden OUT VARCHAR2
, x_machtigingsindicatie OUT VARCHAR2
, x_geplande_einddatum OUT DATE
)
IS
  CURSOR indi_cur
  IS
    SELECT indi.omschrijving
    ,      indi.indi_id
    ,      onde.onderzoekswijze
    ,      onde.datum_binnen
    FROM   kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    ,      kgc_onderzoeken onde
    WHERE  onin.indi_id = indi.indi_id
    AND    onin.onde_id = onde.onde_id
    AND    onde.onde_id = p_onde_id
    ORDER BY onin.volgorde ASC
    ,        onin.onin_id ASC
    ;
  indi_rec indi_cur%rowtype;
BEGIN
  OPEN  indi_cur;
  FETCH indi_cur
  INTO  indi_rec;
  CLOSE indi_cur;
  IF ( indi_rec.omschrijving IS NOT NULL )
  THEN
    x_indicatie := indi_rec.omschrijving;
    x_onderzoeksreden := onderzoeksreden( p_onde_id => p_onde_id );
    x_machtigingsindicatie := machtigingsindicatie( p_onde_id => p_onde_id );
    x_geplande_einddatum := kgc_loop_00.geplande_einddatum
                            ( p_datum => indi_rec.datum_binnen
                            , p_indi_id => indi_rec.indi_id
                            , p_onderzoekswijze => indi_rec.onderzoekswijze
                            );
  END IF;
END afgeleiden_van_indicatie;

FUNCTION onderzoeksindicaties
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  cursor onde_cur
  is
    select kafd_id
        ,      ongr_id
        from   kgc_onderzoeken
        where  onde_id = p_onde_id
        ;
  onde_rec onde_cur%rowtype;

  CURSOR indi_cur
  IS
    SELECT indi.code indi_code
    ,      indi.omschrijving indi_omschrijving
    ,      onre.code onre_code
    ,      onre.omschrijving onre_omschrijving
    ,      onin.opmerking
    FROM   kgc_onderzoeksredenen onre
    ,      kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.onre_id = onre.onre_id (+)
    AND    onin.indi_id = indi.indi_id (+)
    and    onin.ingr_id is null
    AND    onin.onde_id = p_onde_id
    ORDER BY onin.volgorde ASC
    ,        onin.onin_id ASC
    ;
  indi_rec indi_cur%rowtype;
  v_indicatie_tekst varchar2(2000);
  v_indicatie_teksten varchar2(2000);
BEGIN
  open  onde_cur;
  fetch onde_cur
  into  onde_rec;
  close onde_cur;
  begin
    v_indicatie_tekst := kgc_sypa_00.standaard_waarde
                             ( p_parameter_code => 'TOON_INDICATIES_SCHERMEN'
                                                 , p_kafd_id => onde_rec.kafd_id
                                                 , p_ongr_id => onde_rec.ongr_id
                         );
  exception
    when others
    then
      null;
  end;

  if ( v_indicatie_tekst is not null )
  then
    v_indicatie_tekst := replace( v_indicatie_tekst, '<indi_code>', '<INDI_CODE>' );
    v_indicatie_tekst := replace( v_indicatie_tekst, '<indi_omschrijving>', '<INDI_OMSCHRIJVING>' );
    v_indicatie_tekst := replace( v_indicatie_tekst, '<onre_code>', '<ONRE_CODE>' );
    v_indicatie_tekst := replace( v_indicatie_tekst, '<onre_omschrijving>', '<ONRE_OMSCHRIJVING>' );
    v_indicatie_tekst := replace( v_indicatie_tekst, '<opmerking>', '<OPMERKING>' );
    if ( substr( ltrim(v_indicatie_tekst), 1, 1 ) = '['
         and substr( rtrim(v_indicatie_tekst), -1 ) = ']'
           )
        then -- toon alle indicaties
      v_indicatie_tekst := replace( v_indicatie_tekst, '[', NULL );
      v_indicatie_tekst := replace( v_indicatie_tekst, ']', NULL );
      FOR r IN indi_cur
      LOOP
            begin
          if ( v_indicatie_teksten is null )
          then
            v_indicatie_teksten := v_indicatie_tekst;
          else
            v_indicatie_teksten := v_indicatie_teksten||chr(10)||v_indicatie_tekst;
          end if;
          v_indicatie_teksten := replace( v_indicatie_teksten, '<INDI_CODE>', r.indi_code );
          v_indicatie_teksten := replace( v_indicatie_teksten, '<INDI_OMSCHRIJVING>', r.indi_omschrijving );
          v_indicatie_teksten := replace( v_indicatie_teksten, '<ONRE_CODE>', r.onre_code );
          v_indicatie_teksten := replace( v_indicatie_teksten, '<ONRE_OMSCHRIJVING>', r.onre_omschrijving );
          v_indicatie_teksten := replace( v_indicatie_teksten, '<OPMERKING>', r.opmerking );
                exception
                  when value_error
                  then
                    v_indicatie_teksten := substr( v_indicatie_teksten, 1, 1997 )||' >>';
                end;
      end loop;
      v_indicatie_tekst := v_indicatie_teksten;
        else -- toon alleen de belangrijkste indicatie
      open  indi_cur;
          fetch indi_cur
          into  indi_rec;
          close indi_cur;
      v_indicatie_tekst := replace( v_indicatie_tekst, '<INDI_CODE>', indi_rec.indi_code );
      v_indicatie_tekst := replace( v_indicatie_tekst, '<INDI_OMSCHRIJVING>', indi_rec.indi_omschrijving );
      v_indicatie_tekst := replace( v_indicatie_tekst, '<ONRE_CODE>', indi_rec.onre_code );
      v_indicatie_tekst := replace( v_indicatie_tekst, '<ONRE_OMSCHRIJVING>', indi_rec.onre_omschrijving );
      v_indicatie_tekst := replace( v_indicatie_tekst, '<OPMERKING>', indi_rec.opmerking );
        end if;
        v_return := v_indicatie_tekst;
  else
    /*
    -- oude manier
    FOR r IN indi_cur
    LOOP
      IF ( r.indi_code IS NOT NULL )
      THEN
        IF ( v_return IS NULL )
        THEN
          v_return := r.indi_omschrijving||' ('||r.indi_code||')';
        ELSE
          v_return := v_return||CHR(10)||r.indi_omschrijving||' ('||r.indi_code||')';
        END IF;
      ELSE
        IF ( v_return IS NULL )
        THEN
          v_return := r.opmerking;
        ELSE
          v_return := v_return||CHR(10)||r.opmerking;
        END IF;
      END IF;
    END LOOP;
    */
    v_return := kgc_indi_00.onderzoeksreden( p_onde_id => p_onde_id );
  end if;
  RETURN( v_return );
END onderzoeksindicaties;

  FUNCTION indicaties_met_opmerking
  ( p_onde_id IN NUMBER
  )
  RETURN VARCHAR2
  IS
    v_return VARCHAR2(4000);
    CURSOR indi_cur
    IS
      SELECT indi.code
      ,      indi.omschrijving
      ,      onin.opmerking
      FROM   kgc_indicatie_teksten indi
      ,      kgc_onderzoek_indicaties onin
      WHERE  onin.indi_id = indi.indi_id (+)
      AND    onin.onde_id = p_onde_id
      ORDER BY onin.volgorde ASC
      ,        onin.onin_id ASC
      ;
    v_cnt  NUMBER := 0;
  BEGIN

    FOR r IN indi_cur
    LOOP
      IF ( r.code IS NOT NULL )
      THEN
        IF ( v_return IS NULL )
        THEN
          v_return := r.omschrijving||' ('||r.code||')';
        ELSE
          v_return := v_return||CHR(10)||r.omschrijving||' ('||r.code||')';
        END IF;
      END IF;
      IF r.opmerking IS NOT NULL
      THEN
        IF ( v_return IS NULL )
        THEN
          v_return := r.opmerking;
        ELSE
          v_return := v_return||CHR(10)||r.opmerking;
        END IF;
      END IF;
    END LOOP;
    RETURN( v_return );
  END indicaties_met_opmerking;

FUNCTION mons_indi_id
( p_mons_id IN NUMBER
)
RETURN NUMBER
IS
  v_return NUMBER(10);
  CURSOR indi_cur
  IS
    SELECT indi.indi_id
    FROM   kgc_indicatie_teksten indi
    ,      kgc_monster_indicaties moin
    WHERE  moin.indi_id = indi.indi_id
    AND    moin.mons_id = p_mons_id
    ORDER BY moin.moin_id ASC
    ;
BEGIN
  OPEN  indi_cur;
  FETCH indi_cur
  INTO  v_return;
  CLOSE indi_cur;
  RETURN( v_return );
END mons_indi_id;

FUNCTION mons_indi_code
( p_mons_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(25); -- Mantis 3957 SKA  16-12-2015 Report KGCIUFV51 was giving error ORA-06512
  v_indi_id NUMBER;
  CURSOR indi_cur
  IS
    SELECT indi.code
    FROM   kgc_indicatie_teksten indi
    WHERE  indi.indi_id = v_indi_id
    ;
BEGIN
  v_indi_id := mons_indi_id( p_mons_id => p_mons_id );
  IF ( v_indi_id IS NOT NULL )
  THEN
    OPEN  indi_cur;
    FETCH indi_cur
    INTO  v_return;
    CLOSE indi_cur;
  END IF;
  RETURN( v_return );
END mons_indi_code;

FUNCTION mons_indi_omschrijving
( p_mons_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  v_indi_id NUMBER;
  CURSOR indi_cur
  IS
    SELECT indi.omschrijving
    FROM   kgc_indicatie_teksten indi
    WHERE  indi.indi_id = v_indi_id
    ;
BEGIN
  v_indi_id := mons_indi_id( p_mons_id => p_mons_id );
  IF ( v_indi_id IS NOT NULL )
  THEN
    OPEN  indi_cur;
    FETCH indi_cur
    INTO  v_return;
    CLOSE indi_cur;
  END IF;
  RETURN( v_return );
END mons_indi_omschrijving;

FUNCTION onde_indicatie_codes
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  CURSOR indi_cur
  IS
    SELECT indi.code
    FROM   kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.indi_id = indi.indi_id
    AND    onin.onde_id = p_onde_id
    ORDER BY onin.volgorde ASC
    ,        onin.onin_id ASC
    ;
BEGIN
  FOR r IN indi_cur
  LOOP
    IF ( v_return IS NULL )
    THEN
      v_return := r.code;
    ELSE
      v_return := v_return||', '||r.code;
    END IF;
  END LOOP;
  RETURN( v_return );
END onde_indicatie_codes;

/* toegevoegd tbv het digitaal (lab) dossier */
FUNCTION mons_indicatie_codes
( p_mons_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  CURSOR indi_cur
  IS
    SELECT indi.code
    FROM   kgc_indicatie_teksten indi
    ,      kgc_monster_indicaties moin
    WHERE  moin.indi_id = indi.indi_id
    AND    moin.mons_id = p_mons_id
    ORDER BY moin.moin_id ASC
    ;
BEGIN
  FOR r IN indi_cur
  LOOP
    IF ( v_return IS NULL )
    THEN
      v_return := r.code;
    ELSE
      v_return := v_return||', '||r.code;
    END IF;
  END LOOP;
  RETURN( v_return );
END mons_indicatie_codes;


FUNCTION locus
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  v_indi_id NUMBER;
  CURSOR indi_cur
  IS
    SELECT locus
    FROM   kgc_indicatie_teksten indi
    WHERE  indi.indi_id = v_indi_id
    ;
BEGIN
  v_indi_id := indi_id( p_onde_id => p_onde_id );
  IF ( v_indi_id IS NOT NULL )
  THEN
    OPEN  indi_cur;
    FETCH indi_cur
    INTO  v_return;
    CLOSE indi_cur;
  END IF;
  RETURN( v_return );
END locus;

FUNCTION Onderzoek_indi_omschrijvingen
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  CURSOR indi_cur
  IS
    SELECT indi.omschrijving
    FROM   kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.indi_id = indi.indi_id
    AND    onin.onde_id = p_onde_id
    ORDER BY onin.volgorde ASC
    ,        onin.onin_id ASC
    ;
BEGIN
  FOR r IN indi_cur
  LOOP
    IF ( v_return IS NULL )
    THEN
      v_return := r.omschrijving;
    ELSE
      v_return := v_return||CHR(10)||r.omschrijving;
    END IF;
  END LOOP;
  RETURN( v_return );
END Onderzoek_indi_omschrijvingen;
END kgc_indi_00;
/

/
QUIT
