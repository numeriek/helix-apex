CREATE OR REPLACE PACKAGE "HELIX"."BEH_LABV_01" is
function create_xml(p_trge_id in number) return xmltype;
procedure start_verwerking;
end beh_labv_01;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_LABV_01" is
function create_xml(p_trge_id in number) return xmltype is

v_this_prog varchar2(100) := 'beh_labv_01.create_xml';

cursor c_trge is
select *
from beh_labv_trge_xml_vw
where trge_id = p_trge_id;

r_trge c_trge%rowtype;


begin
open c_trge;
fetch c_trge into r_trge;
close c_trge;

return r_trge.trge_xml;

exception
	when others then
    return xmltype('<prog>' || v_this_prog || '</prog><fout>' || substr(SQLERRM, 1, 1000) || '</fout>');
end create_xml;

procedure start_verwerking is
v_this_prog varchar2(100) := 'beh_labv_01.start_verwerking';

v_trge_xml beh_labv_trge_xml_vw.trge_xml%type;
-- BEH_LABV pad -> \\umcstgen01\DNA$\labv
v_dir_file varchar2(500);
xml_doc dbms_xmldom.domdocument;
v_attr_code kgc_attributen.code%type := 'LABV_XML_FILE';
v_count number := 0;
v_msg varchar2(4000);
v_db_naam varchar2(20) := substr(sys_context('userenv','db_name'), 1, 4);
v_labv_xml_dir varchar2(500) := trim(kgc_sypa_00.systeem_waarde('BEH_LABV_XML_DIR'));
v_create_dir_sql varchar2(2000) := 'create or replace directory BEH_LABV as ''' || v_labv_xml_dir || '''';
v_drop_dir_sql varchar2(2000) := 'drop directory BEH_LABV';
v_file_naam varchar2(1000);

cursor c_trge is
select trge_id
from beh_labv_trge_manueel_vw
;

cursor c_attr(p_trge_id in number) is
select atwa.atwa_id
from kgc_attributen attr, kgc_attribuut_waarden atwa
where attr.attr_id = atwa.attr_id
and attr.tabel_naam = 'KGC_TRAY_GEBRUIK'
and attr.code = v_attr_code
and id = p_trge_id
;
r_attr c_attr%rowtype;

begin
execute immediate v_create_dir_sql;
	for r_trge in c_trge loop
		v_count := v_count+1;
    v_file_naam := v_db_naam || '_' || to_char(r_trge.trge_id) || '_' || to_char(sysdate, 'yyyymmddhh24miss') || '.xml';
		v_dir_file := 'BEH_LABV/' || v_file_naam;		
		v_trge_xml := beh_labv_01.create_xml(r_trge.trge_id);
		xml_doc := dbms_xmldom.newdomdocument(v_trge_xml);
		dbms_xmldom.writetofile(xml_doc, v_dir_file, 'iso-8859-1'); -- werkt ook met diacritische tekens
		
		open c_attr(r_trge.trge_id);
		fetch c_attr into r_attr;
		if c_attr%found then
		   update kgc_attribuut_waarden
		   set waarde = v_labv_xml_dir|| '\' ||v_file_naam
		   where atwa_id = r_attr.atwa_id
		   ;
		else
			insert into kgc_attribuut_waarden
			 (attr_id, id, waarde)
			 values((select attr_id from kgc_attributen where code = v_attr_code), r_trge.trge_id, v_labv_xml_dir|| '\' ||v_file_naam)
			 ;
		 end if;
		close c_attr;

	end loop;
  execute immediate v_drop_dir_sql;
  dbms_output.put_line(to_char(v_count) || ' records zijn verwerkt.');

exception
	when others then
    v_trge_xml := xmltype('<prog>' || v_this_prog || '</prog><fout>' || substr(SQLERRM, 1, 1000) || '</fout>');
    xml_doc := dbms_xmldom.newdomdocument(v_trge_xml);
    dbms_xmldom.writetofile(xml_doc, v_dir_file);
    execute immediate v_drop_dir_sql;
    raise_application_error(-20000, SQLERRM);
end start_verwerking;
end beh_labv_01;
/

/
QUIT
