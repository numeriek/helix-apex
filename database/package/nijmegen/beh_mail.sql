CREATE OR REPLACE PACKAGE "HELIX"."BEH_MAIL" as
  PROCEDURE post_mail
  ( p_van   IN VARCHAR2
  , p_aan   IN VARCHAR2
  , p_onderwerp  IN VARCHAR2
  , p_inhoud  IN VARCHAR2
  );
END;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_MAIL" is
PROCEDURE post_mail
( p_van   IN VARCHAR2
, p_aan   IN VARCHAR2
, p_onderwerp  IN VARCHAR2
, p_inhoud  IN VARCHAR2
)
IS
----------------------------------------------------------------------------
  v_EmailServer     varchar2(30) := kgc_sypa_00.standaard_waarde(p_parameter_code => 'BEH_MAIL_SERVER', p_kafd_id => NULL, p_ongr_id => NULL, p_mede_id => NULL);
    -- '143.121.16.65'; --'mail-int'; --'smtp.azu.nl';
  v_Port            number := 25;
  v_conn            UTL_SMTP.CONNECTION;
  v_crlf            varchar2(2):= CHR(13) || CHR(10);
  v_mesg            varchar2(4000);
BEGIN
  v_conn:= utl_smtp.open_connection(v_EmailServer, v_Port);
  utl_smtp.helo(v_conn, v_EmailServer);
  utl_smtp.mail(v_conn, p_van);
  utl_smtp.rcpt(v_conn, p_aan);
  v_mesg:= substr('Subject: ' || p_onderwerp || v_crlf ||
    'Date: ' || TO_CHAR(SYSDATE, 'Dy, dd Mon yyyy hh24:mi:ss') || v_crlf ||
    'From:' || p_van || v_crlf ||
    'To: ' || p_aan || v_crlf ||
    '' || v_crlf ||
    p_inhoud
    , 1, 4000);
  utl_smtp.data(v_conn, v_mesg);
  utl_smtp.quit(v_conn);
EXCEPTION WHEN OTHERS THEN
  dbms_output.put_line(substr('SQLErrm ' || SQLERRM, 1, 255));
END post_mail;
END;
/

/
QUIT
