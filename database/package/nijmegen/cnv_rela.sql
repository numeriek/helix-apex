CREATE OR REPLACE PACKAGE "HELIX"."CNV_RELA"
is
  function adres
  ( p1 in varchar2
  , p2 in varchar2 := null
  , p3 in varchar2 := null
  , p4 in varchar2 := null
  , p5 in varchar2 := null
  , p6 in varchar2 := null
  )
  return varchar2;
  pragma restrict_references (adres, WNDS, WNPS, RNDS, RNPS );
  function bekende
  ( p_code in varchar2
  , p_relatie_type in varchar2 := null
  , p_naam in varchar2 := null
  , p_adres in varchar2 := null
  , p_postcode in varchar2 := null
  , p_woonplaats in varchar2 := null
  , p_spec_id in number := null
  , p_inst_id in number := null
  )
  return number;
--  pragma restrict_references (bekende, WNDS, WNPS );
  function fmt_achternaam
  ( p_naam in varchar2
  )
  return varchar2;
--  pragma restrict_references (fmt_achternaam, WNDS, WNPS );
  function fmt_adres
  ( p_adres in varchar2
  )
  return varchar2;
  pragma restrict_references (fmt_adres, WNDS, WNPS );
end cnv_rela;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."CNV_RELA"
is
  function adres
  ( p1 in varchar2
  , p2 in varchar2 := null
  , p3 in varchar2 := null
  , p4 in varchar2 := null
  , p5 in varchar2 := null
  , p6 in varchar2 := null
  )
  return varchar2
  is
    v_adres varchar2(250);
  begin
    v_adres := p1;
    if ( p2 is not null )
    then
      v_adres := v_adres||chr(10)||p2;
    end if;
    if ( p3 is not null )
    then
      v_adres := v_adres||chr(10)||p3;
    end if;
    if ( p4 is not null )
    then
      v_adres := v_adres||chr(10)||p4;
    end if;
    if ( p5 is not null )
    then
      v_adres := v_adres||chr(10)||p5;
    end if;
    if ( p6 is not null )
    then
      v_adres := v_adres||chr(10)||p6;
    end if;
    return( v_adres );
  end adres;
  function fmt_achternaam
  ( p_naam in varchar2
  )
  return varchar2
  is
    v_achternaam varchar2(100) := p_naam;
    v_tmp_naam varchar2(100);
  begin
    v_achternaam := upper( ltrim( rtrim( v_achternaam ) ) );
    v_achternaam := replace( v_achternaam, ' - ', '-' );
    v_achternaam := replace( v_achternaam, '- ', '-' );
    v_achternaam := replace( v_achternaam, ' -', '-' );
    v_tmp_naam := v_achternaam;
    v_achternaam := replace( v_achternaam, 'DHR' );
    v_achternaam := replace( v_achternaam, 'MEVR' );
    v_achternaam := ltrim( v_achternaam, '.' );
    v_achternaam := ltrim( v_achternaam, '.' );
    while instr( v_achternaam, '.' ) > 0
    loop
      v_achternaam := replace( v_achternaam
                             , substr( v_achternaam
                                     , greatest(instr( v_achternaam, '.' ) - 1, 1 )
                                     , 2
                                     )
                             , ''
                             );
    end loop;
    if ( instr( v_achternaam,' ', -1 ) > 0 )
    then
      v_achternaam := substr( v_achternaam, instr( v_achternaam,' ', -1 ) );
    end if;
    v_achternaam := upper( ltrim( rtrim( v_achternaam ) ) );
    if ( instr( v_tmp_naam, v_achternaam ) = 0 )
    then
      v_achternaam := v_tmp_naam;
    end if;
    kgc_formaat_00.formatteer
    ( p_naamdeel => 'ACHTERNAAM'
    , x_naam => v_achternaam
    );
    return( v_achternaam );
  end fmt_achternaam;
  function fmt_adres
  ( p_adres in varchar2
  )
  return varchar2
  is
    v_adres varchar2(500) := p_adres;
  begin
    v_adres := ltrim( rtrim( upper( v_adres ) ) );
    v_adres := replace( v_adres, 'ALHIER', '' );
    v_adres := replace( v_adres, '.', ' ' );
    v_adres := replace( v_adres, 'STR ', 'STRAAT ' );
    v_adres := replace( v_adres, 'ST ', 'STRAAT ' );
    v_adres := replace( v_adres, 'WG ', 'WEG ' );
    v_adres := replace( v_adres, 'LN ', 'LAAN ' );
    v_adres := replace( v_adres, 'PLN ', 'PLEIN ' );
    v_adres := replace( v_adres, 'HF ', 'HOF ' );
    v_adres := replace( v_adres, 'DYK ', 'DIJK ' );
    return ( v_adres );
  end fmt_adres;
  function bekende
  ( p_code in varchar2
  , p_relatie_type in varchar2 := null
  , p_naam in varchar2 := null
  , p_adres in varchar2 := null
  , p_postcode in varchar2 := null
  , p_woonplaats in varchar2 := null
  , p_spec_id in number := null
  , p_inst_id in number := null
  )
  return number
  is
    v_onbe_id number := null;
    v_rela_id number := null;
    v_dummy_id number;
    v_code varchar2(10);
    v_achternaam varchar2(100);
    v_adres varchar2(250);
    -- zelfde code
    cursor c1
    is
      select rela_id
      from   kgc_relaties
      where  code = v_code
      and  ( relatie_type = p_relatie_type
          or p_relatie_type is null
           )
      ;
    -- zelfde naam, adres, woonplaats, specialisme
    cursor c2
    is
      select rela_id
      ,      adres
      from   kgc_relaties
      where  achternaam = v_achternaam
      and  ( relatie_type = p_relatie_type
          or p_relatie_type is null
           )
      and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
         and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
           )
-- performance!    and    fmt_adres( adres ) = v_adres
      and  ( spec_id = nvl( p_spec_id, spec_id ) or spec_id is null )
      and  ( inst_id = nvl( p_inst_id, inst_id ) or inst_id is null )
      ;
    -- zelfde naam, woonplaats, specialisme
    cursor c3
    is
      select rela_id
      from   kgc_relaties
      where  achternaam = v_achternaam
      and  ( relatie_type = p_relatie_type
          or p_relatie_type is null
           )
      and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
         and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
           )
      and  ( spec_id = nvl( p_spec_id, spec_id ) or spec_id is null )
      and  ( inst_id = nvl( p_inst_id, inst_id ) or inst_id is null )
      ;
    -- zelfde naam, adres, woonplaats
    cursor c4
    is
      select rela_id
      ,      adres
      from   kgc_relaties
      where  achternaam = v_achternaam
      and  ( relatie_type = p_relatie_type
          or p_relatie_type is null
           )
      and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
         and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
           )
-- performance!     and    fmt_adres( adres ) = v_adres
      ;
    -- zelfde naam, woonplaats
    cursor c5
    is
      select rela_id
      from   kgc_relaties
      where  achternaam = v_achternaam
      and  ( relatie_type = p_relatie_type
          or p_relatie_type is null
           )
      and  ( upper( nvl(woonplaats,'~')) = upper( nvl(p_woonplaats,'~') )
         and upper( nvl(postcode,'~')) = upper( nvl(p_postcode,'~') )
           )
      ;
    -- zelfde naam, geen andere gegevens
    cursor c6
    is
      select rela_id
      from   kgc_relaties
      where  achternaam = v_achternaam
      and  ( relatie_type = p_relatie_type
          or p_relatie_type is null
           )
      and    adres is null
      and    woonplaats is null
      and    postcode is null
      ;
    v_r_adres varchar2(250);
  begin
--p.l( '0tijd='||to_char(sysdate,'mi:ss') );
    v_code := upper( p_code );
    v_achternaam := fmt_achternaam( p_naam );
    v_adres := fmt_adres( p_adres );
    if ( p_code = 'ONBEKEND' )
    then
      null;
    elsif ( upper(p_naam) like '%ONBEKEND%'
      or p_naam = '?'
       )
    then
      v_rela_id := cnv_rela.bekende
                   ( p_code => 'ONBEKEND'
                   );
    end if;
    --
--p.l( '1tijd='||to_char(sysdate,'mi:ss') );
    if ( v_rela_id is null )
    then
      if ( p_code is not null )
      then
        open  c1;
        fetch c1 into v_rela_id;
        close c1;
      end if;
    end if;
--p.l( '2tijd='||to_char(sysdate,'mi:ss') );
    if ( v_rela_id is null )
    then
      if ( p_naam is not null
       and p_adres is not null
       and ( p_postcode is not null or p_woonplaats is not null )
         )
      then
        open  c2;
        fetch c2 into v_rela_id, v_r_adres;
        while c2%found loop
          if ( fmt_adres( v_r_adres ) = v_adres )
          then
            exit;
          else
            v_rela_id := null;
          end if;
          fetch c2 into v_rela_id, v_r_adres;
        end loop;
        close c2;
      end if;
    end if;
--p.l( '3tijd='||to_char(sysdate,'mi:ss') );
    if ( v_rela_id is null )
    then
      if ( p_naam is not null
       and ( p_postcode is not null or p_woonplaats is not null )
         )
      then
        open  c3;
        fetch c3 into v_rela_id;
        close c3;
      end if;
    end if;
--p.l( '4tijd='||to_char(sysdate,'mi:ss') );
    if ( v_rela_id is null )
    then
      if ( p_naam is not null
       and p_adres is not null
       and ( p_postcode is not null or p_woonplaats is not null )
         )
      then
        open  c4;
        fetch c4 into v_rela_id, v_r_adres;
        while c4%found loop
          if ( fmt_adres( v_r_adres ) = v_adres )
          then
            exit;
          else
            v_rela_id := null;
          end if;
          fetch c4 into v_rela_id, v_r_adres;
        end loop;
        close c4;
      end if;
    end if;
--p.l( '5tijd='||to_char(sysdate,'mi:ss') );
    if ( v_rela_id is null )
    then
      if ( p_naam is not null
       and ( p_postcode is not null or p_woonplaats is not null )
         )
      then
        open  c5;
        fetch c5 into v_rela_id;
        close c5;
      end if;
    end if;
--p.l( '6tijd='||to_char(sysdate,'mi:ss') );
    if ( v_rela_id is null )
    then
      if ( p_naam is not null
       and p_adres is null
       and p_postcode is null
       and p_woonplaats is null
         )
      then
        open  c6;
        fetch c6 into v_rela_id;
        close c6;
      end if;
    end if;
--p.l( '9tijd='||to_char(sysdate,'mi:ss') );
    return ( v_rela_id );
  end bekende;
end cnv_rela;
/

/
QUIT
