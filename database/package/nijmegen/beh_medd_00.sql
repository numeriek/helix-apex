CREATE OR REPLACE PACKAGE "HELIX"."BEH_MEDD_00" as
/*************************************************************
   NAME:      beh_medd_00
   PURPOSE:    wordt gebruikt  bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        28-03-2013  Cootje Vermaat      1. Created this function.


*************************************************************/
FUNCTION m_dd_categorie
(
p_meet_id IN NUMBER
)
RETURN VARCHAR2;

FUNCTION m_dd_haplogroup
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;

FUNCTION m_dd_interpretatie
( p_meet_id IN NUMBER
)RETURN VARCHAR2;

FUNCTION m_dd_varia_io
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;

FUNCTION m_dd_variatie
( p_meet_id IN NUMBER
)RETURN VARCHAR2;

FUNCTION m_d_MITODNA
( p_onde_id IN NUMBER
)RETURN varchar2;

FUNCTION m_d_MTDNA_T
( p_onde_id IN NUMBER
)RETURN varchar2;


END BEH_MEDD_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_MEDD_00" as
FUNCTION m_dd_categorie
(
p_meet_id IN NUMBER
)
RETURN VARCHAR2 IS

  v_return VARCHAR2(100);

  cursor c_mdet(p_meet_id in number) is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and stof.code <> 'HAPLOGROUP'
     and mdet.prompt = 'Categorie'
     and mwst.code = 'DDMITOCHIP'
   ;

begin

  OPEN  c_mdet(p_meet_id);
  fetch c_mdet into v_return;
  If c_mdet%notfound then
    v_return := '-';
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';
END m_DD_categorie;

FUNCTION m_dd_haplogroup
( p_meet_id IN NUMBER
)/******************************************************************************
   NAME:       beh_metab_dd_variatie
   PURPOSE:    bepaal de variatie van een meetwaarde bij onderzoeksgroep DD,
               indicatie D_MM en protocol D_MTDNA_T gebruikt bij de Ion Torrent en meetw.structuur DDMITOCHIP
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19-03-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(200) := null;

  cursor c_mdet is
  select stof.omschrijving
  ,  mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and stof.code = 'HAPLOGROUP'
     and mdet.prompt = 'Categorie'
     and mwst.code = 'DDMITOCHIP'
   ;
  r_mdet c_mdet%rowtype;

begin

  OPEN  c_mdet;
  fetch c_mdet into r_mdet;
  If c_mdet%notfound then
    v_return := null;
  else
   v_return := 'Op basis van deze bevindingen behoort deze pati�nt tot Haplogroup: '||r_mdet.waarde;
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := null;

END m_dd_haplogroup;

FUNCTION m_dd_interpretatie
( p_meet_id IN NUMBER
)/******************************************************************************
   NAME:       beh_metab_dd_interpretatie
   PURPOSE:    bepaal de interpretatie van een meetwaarde bij onderzoeksgroep DD,
               indicatie D_MM en protocolin( D_MITODNA, D_MTDNA_T) en meetw.structuur DDMITOCHIP
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        14-02-2011  Cootje Vermaat      1. Created this function.
   1.1        19-03-2013 Cootje Vermaat     aangepast voor protocol D_MTDNA_T

******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  cursor c_mdet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
          and stof.code <> 'HAPLOGROUP'
     and mdet.prompt = 'Interpretatie'
     and mwst.code = 'DDMITOCHIP'
   ;

begin

  OPEN  c_mdet;
  fetch c_mdet into v_return;
  If c_mdet%notfound then
    v_return := '-';
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END m_dd_interpretatie;
FUNCTION m_dd_varia_io
( p_meet_id IN NUMBER
)/******************************************************************************
   NAME:       beh_metab_dd_variatie
   PURPOSE:    bepaal de variatie van een meetwaarde bij onderzoeksgroep DD,
               indicatie D_MM en protocol D_MTDNA_T gebruikt bij de Ion Torrent en meetw.structuur DDMITOCHIP
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19-03-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_stof VARCHAR2(100) := null;
  v_ref VARCHAR2(100) := null;
  v_meet VARCHAR2(100) := null;
  v_return VARCHAR2(100) := null;

  cursor c_stof is
  select stof.code
     , 'm.'||substr(stof.omschrijving, 7) omschrijving
     from bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and stof.code <> 'HAPLOGROUP'
     and mwst.code = 'DDMITOCHIP'
   ;
   r_stof c_stof%rowtype;

  cursor c_ref is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and stof.code <> 'HAPLOGROUP'
     and mdet.prompt = 'Ref.'
     and mwst.code = 'DDMITOCHIP'
   ;
  cursor c_meet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and stof.code <> 'HAPLOGROUP'
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Meetw.'
     and mwst.code = 'DDMITOCHIP'
   ;
begin

  OPEN  c_stof;
  fetch c_stof into r_stof;
  If c_stof%notfound then
    v_stof := null;
  else
   v_stof := r_stof.omschrijving;
  end if;
  CLose c_stof;

  OPEN  c_ref;
  fetch c_ref into v_ref;
  If c_ref%notfound then
    v_ref := null;
  end if;
  close c_ref;

  OPEN  c_meet;
  fetch c_meet into v_meet;
  If c_meet%notfound then
    v_meet := null;
  end if;
  close c_meet;

  if v_stof is null then
    v_return := null;
  elsif (v_meet like '%ins%' or v_meet like '%del%')
  then
    v_return := v_stof||v_meet;
  else
    v_return := v_stof||v_ref||'>'||v_meet;
  end if;

  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END m_dd_varia_io;

FUNCTION m_dd_variatie
( p_meet_id IN NUMBER
)/******************************************************************************
   NAME:       beh_metab_dd_variatie
   PURPOSE:    bepaal de variatie van een meetwaarde bij onderzoeksgroep DD,
               indicatie D_MM en protocol in ( D_MITODNA, D_MTDNA_T) en meetw.structuur DDMITOCHIP
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        14-02-2011  Cootje Vermaat      1. Created this function.
   1.1        19-03-2013  cootje vermaat     aangepast voor protocol D_MTDNA_T

******************************************************************************/
RETURN VARCHAR2
IS
  v_stof VARCHAR2(100) := null;
  v_ref VARCHAR2(100) := null;
  v_meet VARCHAR2(100) := null;
  v_return VARCHAR2(100) := null;

  cursor c_stof is
  select stof.code
     , stof.omschrijving
     from bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mwst.code = 'DDMITOCHIP'
   ;

  r_stof c_stof%rowtype;


  cursor c_ref is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Ref.'
     and mwst.code = 'DDMITOCHIP'
   ;
  cursor c_meet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Meetw.'
     and mwst.code = 'DDMITOCHIP'
   ;
begin

  OPEN  c_stof;
  fetch c_stof into r_stof;
  If c_stof%notfound then
    v_stof := null;
  else
    v_stof:= r_stof.omschrijving;
  end if;

  OPEN  c_ref;
  fetch c_ref into v_ref;
  If c_ref%notfound then
    v_ref := null;
  end if;

  OPEN  c_meet;
  fetch c_meet into v_meet;
  If c_meet%notfound then
    v_meet := null;
  end if;

  v_return := v_stof||v_ref||'>'||v_meet;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END m_dd_variatie;

FUNCTION m_d_MITODNA
( p_onde_id IN NUMBER
)/******************************************************************************
   NAME:      beh_metab_d_MITODNA
   PURPOSE:    bepaal of er een meting is met de stoftestgroep  D_MITODNA
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        21-02-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN varchar2
IS
  v_return varchar2(1);

  cursor c_meti is
  select meti.meti_id
  from bas_metingen meti
  , kgc_stoftestgroepen stgr
  where meti.stgr_id = stgr.stgr_id
  and stgr.code = 'D_MITODNA'
  and METI.ONde_ID = p_onde_id
  ;
  r_meti c_meti%rowtype;

begin

  v_return := 'N';

  OPEN c_meti;
  Fetch C_meti into r_meti;
  If c_meti%FOUND then
    v_return := 'J';
  else
    v_return :='N';
  end if;
  Close c_meti;

  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN v_return := 'N';

END m_d_MITODNA;

FUNCTION m_d_MTDNA_T
( p_onde_id IN NUMBER
)/******************************************************************************
   NAME:      beh_metab_d_MTDNA_T
   PURPOSE:    bepaal of er een meting is met de stoftestgroep  D_MTDNA_T
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19-03-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN varchar2
IS
  v_return varchar2(1);

  cursor c_meti is
  select meti.meti_id
  from bas_metingen meti
  , kgc_stoftestgroepen stgr
  where meti.stgr_id = stgr.stgr_id
  and stgr.code = 'D_MTDNA_T'
  and METI.ONde_ID = p_onde_id
  ;
  r_meti c_meti%rowtype;

begin

  v_return := 'N';

  OPEN c_meti;
  Fetch C_meti into r_meti;
  If c_meti%FOUND then
    v_return := 'J';
  else
    v_return :='N';
  end if;
  Close c_meti;

  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN v_return := 'N';

END m_d_MTDNA_T;

END BEH_MEDD_00;
/

/
QUIT
