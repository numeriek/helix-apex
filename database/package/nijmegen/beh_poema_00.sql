CREATE OR REPLACE PACKAGE "HELIX"."BEH_POEMA_00" is
g_context_naam varchar2(30) := 'beh_poema_selectie_criteria';

function zet_stgr_code (p_stgr_code in varchar) return number;
function zet_indi_code (p_indi_code in varchar) return number;
end;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_POEMA_00" is

procedure zet_stgr_code (p_stgr_code in varchar) is
begin
    dbms_session.set_context(
                                g_context_naam
                              , 'STGR_CODE'
                              , p_stgr_code
                              );
end zet_stgr_code;
--###################################
function zet_stgr_code
(
p_stgr_code in varchar
)
return number
is
begin
  zet_stgr_code ( p_stgr_code => p_stgr_code);
  return 1;
exception
 when others then
    return 0;
end zet_stgr_code;
--###################################
procedure zet_indi_code (p_indi_code in varchar) is
begin
    dbms_session.set_context(
                                g_context_naam
                              , 'INDI_CODE'
                              , p_indi_code
                              );
end zet_indi_code;
--###################################
function zet_indi_code
(
p_indi_code in varchar
)
return number
is
begin
  zet_indi_code ( p_indi_code => p_indi_code);
  return 1;
exception
 when others then
    return 0;
end zet_indi_code;

end;
/

/
QUIT
