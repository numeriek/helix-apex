CREATE OR REPLACE PACKAGE "HELIX"."BEH_DECL_00" is

  /******************************************************************************
     NAAM:   onde_jaar
     DOEL:   Het jaar dat een onderzoek binnen is gekomen.
             Gebruikt om in declaratie criteria te bepalen of de
             criteria geldig zijn.
  ******************************************************************************/
  function onde_jaar(p_onde_id number) return number;
  pragma restrict_references(onde_jaar, wnds, wnps, rnps);

  /******************************************************************************
     NAAM:   stgr_type
     DOEL:   Haal de extra attribuutwaarde TYPE_TEST op van een stoftestgroep.
             Dit wordt gebruikt om declaratie criteria te filteren.
  ******************************************************************************/
  function stgr_type(p_stgr_code in kgc_stoftestgroepen.code%type)
    return kgc_attribuut_waarden.waarde%type;
  pragma restrict_references(stgr_type, wnds, wnps, rnps);


  /******************************************************************************
     NAAM:   stgr_onde_decl
     DOEL:   Controleer de extra attribuutwaarde TYPE_TEST tegen het onderzoek.
             Als 1 van de metingen van het onderzoek van een protocol is dat
             deze extra attribuutwaarde type_type gelijk heeft aan de opgegeven
             p_type_test parameter, dan geeft 'J' terug, anders 'N'
  ******************************************************************************/
  function stgr_onde_decl( p_onde_id   in kgc_onderzoeken.onde_id%type
                         , p_type_test in varchar2
                         ) return varchar2;
  pragma restrict_references(stgr_onde_decl, wnds, wnps, rnps);


  /******************************************************************************
     NAAM:   onde_ingr_decl
     DOEL:   Bepaal of het onderzoek gedeclareerd mag worden volgens de
             extra attribuutwaarde NIET_DECLAREREN op de indicatie groep(en)
             van het onderzoek. Als bij 1 van de indicatiegroepen van het
             onderzoek NIET_DECLAREREN met waarde J is gevuld mag het onderzoek
             niet gedeclareerd worden.
             Deze functie geeft J terug als het onderzoek gedeclareerd mag
             worden, of N indien anders.
  ******************************************************************************/
  function onde_ingr_decl(p_onde_id in kgc_onderzoeken.onde_id%type) return varchar2;
  pragma restrict_references(onde_ingr_decl, wnds, wnps, rnps);

END beh_decl_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DECL_00" is

  /******************************************************************************
     NAAM:   onde_jaar
     DOEL:   Het jaar dat een onderzoek binnen is gekomen.
             Gebruikt om in declaratie criteria te bepalen of de
             criteria geldig zijn.
  ******************************************************************************/
  function onde_jaar(p_onde_id number) return number is
    v_jaar_datum_binnen number := NULL;
  begin

    select to_number(to_char(onde.datum_binnen, 'YYYY')) as jaar_binnen
    into   v_jaar_datum_binnen
    from   kgc_onderzoeken onde
    where  onde.onde_id = p_onde_id;

    return v_jaar_datum_binnen;

  exception when others then

    return NULL;

  end;


  /******************************************************************************
     NAAM:   stgr_type
     DOEL:   Haal de extra attribuutwaarde TYPE_TEST op van een stoftestgroep.
             Dit wordt gebruikt om declaratie criteria te filteren.
             Let op: stgr_code is alleen beschikbaar in een declaratie op meting
             niet op onderzoek.
  ******************************************************************************/
  function stgr_type(p_stgr_code in kgc_stoftestgroepen.code%type)
    return kgc_attribuut_waarden.waarde%type is

    cursor c_stgr_type(b_stgr_code in kgc_stoftestgroepen.code%type) is
      select stgr.stgr_id
           , stgr.code as stgr_code
           , stgr.omschrijving as stgr_omschrijving
           , attr.code as attr_code
           , atwa.waarde
      from   kgc_stoftestgroepen stgr
        join kgc_attribuut_waarden atwa on atwa.id = stgr.stgr_id
        join kgc_attributen attr on attr.attr_id = atwa.attr_id
      where  attr.code = 'TYPE_TEST'
        and  stgr.code = b_stgr_code;

    r_stgr_type c_stgr_type%rowtype;
    v_stgr_type kgc_attribuut_waarden.waarde%type := 'GEEN';

  begin

    open c_stgr_type(p_stgr_code);
    fetch c_stgr_type into r_stgr_type;

    if (c_stgr_type%found) then
      v_stgr_type := r_stgr_type.waarde;
    end if;

    return v_stgr_type;

  end;


  /******************************************************************************
     NAAM:   stgr_onde_decl
     DOEL:   Controleer de extra attribuutwaarde TYPE_TEST tegen het onderzoek.
             Als 1 van de metingen van het onderzoek van een protocol is dat
             deze extra attribuutwaarde type_type gelijk heeft aan de opgegeven
             p_type_test parameter, dan geeft 'J' terug, anders 'N'
  ******************************************************************************/
  function stgr_onde_decl( p_onde_id   in kgc_onderzoeken.onde_id%type
                         , p_type_test in varchar2
                         ) return varchar2 is

    cursor c_stgr_decl( b_onde_id   in kgc_onderzoeken.onde_id%type
                      , b_type_test in varchar2
                      ) is
      select onde.onde_id
           , onde.onderzoeknr
           , meti.meti_id
           , stgr.stgr_id
           , stgr.code as stgr_code
           , stgr.omschrijving as stgr_omschrijving
           , attr.code as attr_code
           , atwa.waarde
      from   kgc_onderzoeken onde
        join bas_metingen meti on meti.onde_id = onde.onde_id
        join kgc_stoftestgroepen stgr on stgr.stgr_id = meti.stgr_id
        join kgc_attribuut_waarden atwa on atwa.id = stgr.stgr_id
        join kgc_attributen attr on attr.attr_id = atwa.attr_id
      where  onde.onde_id = b_onde_id
        and  attr.code = 'TYPE_TEST'
        and  atwa.waarde = b_type_test;

    r_stgr_decl c_stgr_decl%rowtype;

  begin

    open c_stgr_decl(p_onde_id, p_type_test);
    fetch c_stgr_decl into r_stgr_decl;

    if (c_stgr_decl%found) then
      return 'J';
    end if;

    return 'N';

  end;


  /******************************************************************************
     NAAM:   onde_ingr_decl
     DOEL:   Bepaal of het onderzoek gedeclareerd mag worden volgens de
             extra attribuutwaarde NIET_DECLAREREN op de indicatie groep(en)
             of de indicatie teksten van het onderzoek.
             Als bij 1 van de indicatiegroepen of teksten van het
             onderzoek NIET_DECLAREREN met waarde J is gevuld mag het onderzoek
             niet gedeclareerd worden.

             Deze functie geeft J terug als het onderzoek WEL gedeclareerd mag
             worden, of N indien anders.

  ******************************************************************************/
  function onde_ingr_decl(p_onde_id in kgc_onderzoeken.onde_id%type) return varchar2 is

    cursor c_onde_decl(b_onde_id in kgc_onderzoeken.onde_id%type) is
      select onde.onde_id
           , onde.onderzoeknr
           , ingr.code as ingr_code
           , ingr.omschrijving as ingr_omschrijving
           , attr_ingr.code as ingr_attr_code
           , atwa_ingr.waarde as ingr_attr_waarde
           , indi.code as indi_code
           , indi.omschrijving as indi_omschrijving
           , attr_indi.code as indi_attr_code
           , atwa_indi.waarde as indi_attr_waarde
      from   kgc_onderzoeken onde
        join kgc_onderzoek_indicaties onin on onin.onde_id = onde.onde_id
        join kgc_indicatie_teksten indi on indi.indi_id = onin.indi_id
        join kgc_indicatie_groepen ingr on ingr.ingr_id = indi.ingr_id
        left join kgc_attribuut_waarden atwa_ingr on atwa_ingr.id = ingr.ingr_id
        left join kgc_attributen attr_ingr        on attr_ingr.attr_id = atwa_ingr.attr_id
        left join kgc_attribuut_waarden atwa_indi on atwa_indi.id = indi.indi_id
        left join kgc_attributen attr_indi        on attr_indi.attr_id = atwa_indi.attr_id
      where  onde.onde_id = b_onde_id
        and  ( ( attr_ingr.code = 'NIET_DECLAREREN'
             and attr_ingr.tabel_naam = 'KGC_INDICATIE_GROEPEN'
             and nvl(upper(atwa_ingr.waarde), 'J') = 'J'
               )
             or
               ( attr_indi.code = 'NIET_DECLAREREN'
             and attr_indi.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
             and nvl(upper(atwa_indi.waarde), 'J') = 'J'
               )
             );

    r_onde_decl c_onde_decl%rowtype;
    v_declareren_ind varchar2(1) := 'J';

  begin

    open c_onde_decl(p_onde_id);
    fetch c_onde_decl into r_onde_decl;

    if (c_onde_decl%found) then
      v_declareren_ind := 'N';
    end if;

    return v_declareren_ind;

  end;


END beh_decl_00;
/

/
QUIT
