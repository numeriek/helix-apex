CREATE OR REPLACE PACKAGE "HELIX"."BEH_DNA_VOORTEST_WLST_GEEN_MQ" IS

/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 06-05-2012
Huidige versie  : 1.2.2

DOEL
Het automatisch selecteren van stoftesten, deze op trays plaatsen,
hier werklijsten van maken en vervolgens etiketten voor laten afdrukken.

Hierbij worden geen MQ op tray geplaatst. Dit is het grote verschil met
de package BEH_DNA_VOORTEST_WLST_AANMAKEN

Dit gebeurt op basis van een aantal selectie, parameters.

Door het aanroepen van ' start_werklijst_tray_etiket' met onderstaanden parameters,
wordt het proces opgestart.

De output met logging wordt achteraf naar per email verzonden.

INPUT PARAMETERS
P_F_STATUS : FRACTIE_STATUS    : De status van de fractie zoals in bas_fracties, OPGEWERKT'
P_O_STATUS : ONDERZOEKSSTATUS  : De status van een onderzoek zoals in GOEDGEKEURD'
P_PROTOCOL : PROTOCOL_CODE     : Je kunt een of meerdere stoftestgroep codes opgeven
P_O_WIJZE  : ONDERZOEKSWIJZE   : De wijze van onderzoek: SCANNING, SCREENING of NULL (beide)
P_TECHNIEK : De techniek dat toegepast wordt:  PCR
P_AFDELING : De afdeling waarop de stoftesten uitgevoerd moeten worden:  GENOOM
P_TRTY     : Het type tray waarop de stoftesten moeten komen: 96 WELLS
P_EMAIL    : Email adres waar de output van de procedure naartoe gemaild moet worden
P_VOL_WLST : Alleen Volle werklijsten Ja, Nee
P_NUMMERSTR: De te gebruiken nummerstructuur voor de naamgeving: WERK

Onderstaande tabellen / views worden in deze package gebruikt:
kgc_interface_definities
beh_hlp_tray_gebruik
kgc_tray_gebruik
kgc_tray_vulling
kgc_samples
bas_tray_meetwaarden_vw
kgc_onderzoeken
beh_hlp_werklijst_stoftesten
beh_hlp_werklijst_meetwaarde
beh_hlp_werklijst
kgc_etiket_wlst_vw
kgc_etiket_parameters
kgc_medewerkers
kgc_etiket_voorkeuren
kgc_onderzoeksgroepen
kgc_kgc_afdelingen
kgc_stoftestgroepen
kgc_tray_types
kgc_technieken
beh_hlp_storage
beh_hlp_storage_uitval
storage@storage

Onderstaande packages / procedures / functies worden gebruikt
kgc_util_00.num_of_occur
kgc_util_00.strip
kgc_nust_00.genereer_nr
kgc_sypa_00.standaard_waarde
kgc_interface.standaard_waarde
kgc_wlst_00.sync_tray_werklijst
kgc_sypa_00.systeem_waarde
kgc_ftp.login
kgc_etik_00.regel
kgc_ftp.convert_crlf
kgc_ftp.put_remote_ascii_data
kgc_ftp.logout
utl_tcp.close_all_connections

Onderstaande database links worden gebruikt
STORAGE

***********************************************************************/


GV_REVISIE_VERSIE CONSTANT VARCHAR2(10) := '1.2.3';

/* revisie historie */
FUNCTION REVISION
 RETURN VARCHAR2;

PROCEDURE START_WERKLIJST_TRAY_ETIKET
 (P_F_STATUS      IN VARCHAR2 -- OPGEWERKT
 ,P_O_STATUS      IN VARCHAR2 -- GOEDGEKEURD
 ,P_PROTOCOL      IN VARCHAR2 -- Kunnen meerdere protocols/stoftestgroepen zijn
 ,P_O_WIJZE       IN VARCHAR2 DEFAULT NULL -- SCANNING, SCREENING of NULL
 ,P_TECHNIEK      IN VARCHAR2 -- PCR
 ,P_AFDELING      IN VARCHAR2 -- GENOOM
 ,P_TRTY          IN VARCHAR2 -- 96 WELLS , code van de tray meegeven
 ,P_EMAIL         IN VARCHAR2 DEFAULT 'dnapcrfacility@gen.umcn.nl' -- aangepast door MGO , was: 'HelpdeskHelixCUKZ@cukz.umcn.nl'
 ,P_TRAY_GRD      IN VARCHAR2 DEFAULT NULL
 ,P_VOL_WLST      IN VARCHAR2 DEFAULT 'J'
 ,P_NUMMERST      IN VARCHAR2 DEFAULT 'WERK'
 );

END BEH_DNA_VOORTEST_WLST_GEEN_MQ;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DNA_VOORTEST_WLST_GEEN_MQ" IS

C_SESSION_ID   NUMBER;
C_VOLGNR       NUMBER := 1;
V_EMAIL_INHOUD VARCHAR2(32767);
C_NIEUWE_REGEL varchar2(10) := chr(10);
v_doorgaan     boolean := false;


/* revisie historie */
FUNCTION REVISION
 RETURN VARCHAR2
 IS
/***********************************************************************
Auteur/bedrijf  : Yuri Arts, YA IT-Services
Datum creatie   : 06-05-2012

BESCHRIJVING
Beschrijf hier de wijzigingen in de package.

HISTORIE

Wanneer /      Wie             Wat
Versie
------------------------------------------------------------------------
02-06-2015     Y.Arts          P_O_WIJZE, onderzoekswijze is optioneel geworden. Betekent dat scanning en screening bij elkaar op
1.2.3                                     een werklijst gezet worden.
14-04-2015     Y.Arts          P_NUMMERST  toegevoegd tbv ML werklijsten
1.2.2
23-01-2014     Y.Arts          Sortering van meetwaarden op einddatum onderzoek in maak_werklijst
1.2.1
03-04-2013     Y.Arts          1. Parameter P_VOL_WLST toegevoegd. Ja/Nee indicator waarmee bepaald kan worden of alleen volle werklijsten
                                  aangemaakt kunnen worden. Als een plaat maar halfvol dreigt te worden dan afkappen.
1.2.0                          2. Parameter P_PROTOCOL is niet meer hoofdletter gevoelig. De string moet voorkomen in de omschrijving.
                               3. Volgorde op werklijst aanpassen: einddatum onderzoek aflopend, zonder einddatum achteraan.

06-06-2012     Y.Arts          De uitvoertekst, dat in de mail gezet wordt, moet gecontroleerd worden op 32766 posities.
1.0.1                          Als het toe te voegen tekst ervoor zorgt dat het aantal hier overheen gaat, mag het niet toegevoegd worden.
                               Dit is om fouten te voorkomen.
06-05-2012     Y.Arts          Creatie
1.0.0
***********************************************************************/
BEGIN
  RETURN gv_revisie_versie;
END;

PROCEDURE gebruikers_info
 (p_tekst in varchar2)
is
begin
  if (nvl(length(v_email_inhoud),0) + nvl(length(p_tekst),0) < 32766) then
    v_email_inhoud := v_email_inhoud||p_tekst||c_nieuwe_regel;
  end if;
END;

procedure controleer_bestand
( p_bestand    in varchar2
, p_locatie    in varchar2
, p_dbs_client in varchar2
, p_rw         in varchar2
)
is
  out_file_client utl_file.file_type;
  out_file_dbs    utl_file.file_type;
begin
  -- controleer opgegeven bestand
  IF ( p_bestand IS NOT NULL )
  THEN
    BEGIN
      if ( p_dbs_client = 'CLIENT' )
      then
        out_file_client  := utl_file.fopen(location  => p_locatie
                                          ,filename  => p_bestand
                                          ,open_mode => p_rw);
      elsif ( p_dbs_client = 'DBS' )
      then
        out_file_dbs  := utl_file.fopen(location  => p_locatie
                                      ,filename  => p_bestand
                                      ,open_mode => p_rw);
      end if;

      IF ( utl_file.is_open( file => out_file_client ) )
      THEN
        utl_file.fclose( file =>  out_file_client );
      END IF;
      IF ( utl_file.is_open( file => out_file_dbs ) )
      THEN
        utl_file.fclose( file => out_file_dbs );
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        IF ( utl_file.is_open( file => out_file_client ) )
        THEN
          utl_file.fclose( file => out_file_client );
        END IF;
        IF ( utl_file.is_open( file => out_file_dbs ) )
        THEN
          utl_file.fclose( file => out_file_dbs );
        END IF;
        qms$errors.show_message
          ( p_mesg => 'KGC-00000'
          , p_param0 => 'Bestand '||p_bestand||' kan niet geopend worden'
          , p_param1 => 'Controleer locatie en naam van het bestand'
                        ||CHR(10)||SQLERRM
          , p_errtp => 'E'
          , p_rftf => TRUE
          );
    END;
  END IF;
end controleer_bestand;

--
FUNCTION select_deel
( p_idef_id         IN NUMBER
, p_datum_formaat   IN VARCHAR2
, p_formaat         IN VARCHAR2
, p_omsluitteken    IN VARCHAR2
, p_scheidingsteken IN VARCHAR2
, p_geformatteerd   IN BOOLEAN DEFAULT TRUE
, p_tabel           IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return      VARCHAR2(4000);
  v_column      VARCHAR2(1000);
  v_laatste_pos NUMBER := 1;
  v_kolom_komt_voor_in_tabel VARCHAR2(1);
  CURSOR ifss_cur (b_idef_id number)
  IS
    SELECT code
    ,      datatype
    ,      lengte
    ,      positie_va
    ,      default_waarde
    ,      functie
    FROM   kgc_interface_samenstelling
    WHERE  idef_id = b_idef_id
    AND    selecteren = 'J'
    ORDER BY volgorde
  ;

  function kolom_in_tabel
  ( b_tabel in varchar2
  , b_kolom in varchar2
  )
  return varchar2
  IS
    l_return varchar2(1) := 'N';
    l_owner varchar2(30) := KGC_SYPA_00.systeem_waarde('APPLICATIE_EIGENAAR');
    l_tabel varchar2(100);

    cursor c
    ( bl_owner in varchar2
    , bl_tabel in varchar2
    , bl_kolom in varchar2
    )
    is
      SELECT 'J'
      FROM   all_tab_columns
      WHERE  owner = bl_owner
      AND    table_name  = bl_tabel
      AND    column_name = bl_kolom
      ;
  begin
    if ( instr( b_tabel, ' ' ) > 0 )
    then
      l_tabel := substr( b_tabel, 1, instr( b_tabel, ' ' ) - 1 );
    else
        l_tabel := b_tabel;
    end if;
    open  c( bl_owner => upper(l_owner)
           , bl_tabel => upper(l_tabel)
           , bl_kolom => upper(b_kolom)
           );
    fetch c
    into  l_return;
    close c;
      return( nvl( l_return, 'N' ) );
  exception
    when others
    then
      return( 'N' );
  end kolom_in_tabel;

BEGIN
  v_return := NULL;
   FOR ifss_rec IN ifss_cur (p_idef_id)
  LOOP
    IF ( ifss_rec.datatype = 'N' )
    THEN
      v_column := 'to_char('||ifss_rec.code||')';
    ELSIF ( ifss_rec.datatype = 'D' )
    THEN
      v_column := 'to_char('||ifss_rec.code||','''||p_datum_formaat||''')';
    ELSIF ( ifss_rec.datatype = 'S' )
    THEN
      v_column := 'to_char(rownum)';
    ELSE
      v_column := ifss_rec.code;
    END IF;
    -- functie moet worden uitgevoerd
    IF ( ifss_rec.functie IS NOT NULL )
    THEN
      v_column := REPLACE( ifss_rec.functie, '<waarde>', v_column );
      v_column := REPLACE( ifss_rec.functie, '<WAARDE>', v_column );
    END IF;
    -- gebruik eventueel default waarde
    -- in ieder geval als de kolom niet voorkomt in de tabel of view
    if ( ifss_rec.datatype = 'S' )
    then
      v_kolom_komt_voor_in_tabel := 'J'; -- rownum = dummy-kolom
    else
      v_kolom_komt_voor_in_tabel := kolom_in_tabel
                                    ( b_tabel => p_tabel
                                    , b_kolom => ifss_rec.code
                                    );
    end if;
    IF ( v_kolom_komt_voor_in_tabel = 'N')
    THEN
      if ( ifss_rec.default_waarde is null )
      then
        v_column := 'NULL'; -- loze waarde
      else
        v_column := ''''||ifss_rec.default_waarde||'''';
      end if;
    else -- kolom komt wel voor in tabel/view
      IF ( ifss_rec.default_waarde IS NOT NULL )
      THEN
        v_column := 'nvl('||v_column||','''||ifss_rec.default_waarde||''')';
      END IF;
    END IF;
    IF ( ifss_rec.lengte IS NOT NULL
     and v_column <> 'NULL'
         )
    THEN
      -- bij vast formaat wordt verderop rpad functie gebruikt, daarom een eventuele spatie!
      IF ( p_formaat = 'V' )
      THEN
        v_column := 'nvl(substr('||v_column||',1,'||ifss_rec.lengte||'),'' '')';
      ELSE
        v_column := 'substr('||v_column||',1,'||ifss_rec.lengte||')';
      END IF;
    END IF;
    IF ( p_omsluitteken IS NOT NULL
     and v_column <> 'NULL'
         )
    THEN
      v_column := ''''||p_omsluitteken||'''||'||v_column||'||'''||p_omsluitteken||'''';
    END IF;
    IF ( NOT p_geformatteerd )
    THEN
      v_column := v_column||' '||ifss_rec.code; -- met alias
      IF ( v_return IS NULL )
      THEN
        v_return := v_column;
      ELSE
        v_return := v_return||CHR(10)||', '||v_column;
      END IF;
    ELSE -- geformatteerd
      IF ( p_formaat = 'N' ) -- Normaal (CSV)
      THEN
        IF ( v_return IS NULL )
        THEN
          v_return := v_column;
        ELSE
          IF ( UPPER(p_scheidingsteken) LIKE 'CHR%' )
          THEN
            v_return := v_return||CHR(10)||'||'||p_scheidingsteken||'||'||v_column;
          ELSE
            v_return := v_return||CHR(10)||'||'''||NVL(p_scheidingsteken,',')||'''||'||v_column;
          END IF;
        END IF;
      ELSIF ( p_formaat = 'V' ) -- Vaste recordstructuur
      THEN
        IF ( ifss_rec.positie_va IS NOT NULL
         AND ifss_rec.lengte IS NOT NULL
           )
        THEN
          -- voorloop- en naloop-spaties
          v_column := 'lpad('' '','||TO_CHAR(ifss_rec.positie_va - v_laatste_pos)||','' '')||rpad('||v_column||','||TO_CHAR(ifss_rec.lengte)||','' '')';
          -- laatste positie is nodig om voorloop-spaties van volgende kolom te bepalen
          v_laatste_pos := ifss_rec.positie_va + ifss_rec.lengte;
          IF ( v_return IS NULL )
          THEN
            v_return := v_column;
          ELSE
            v_return := v_return ||CHR(10)||'||'||v_column;
          END IF;
        END IF;
      END IF;
    END IF;
  END LOOP;
  RETURN ( v_return );
END select_deel;
--

FUNCTION formatteer_where
( p_voorwaarde   VARCHAR2
, p_kolomnaam    VARCHAR2
, p_datatype     VARCHAR2
, p_datumformaat VARCHAR2
)
RETURN VARCHAR2
IS
 v_voorwaarde VARCHAR2(4000);
 v_return VARCHAR2(4000);
BEGIN
  v_voorwaarde := p_voorwaarde;
  IF ( v_voorwaarde IS NOT NULL )
  THEN
    IF ( INSTR( UPPER( v_voorwaarde ), UPPER( p_kolomnaam ) ) > 0 )
    THEN -- hele voorwaarde is opgegeven
      NULL;
    ELSIF ( SUBSTR( v_voorwaarde, 1, 1 ) IN ( '=', '>', '<', '!' )
         OR SUBSTR( UPPER(v_voorwaarde), 1, 2 ) IN ( 'IN' )
         OR SUBSTR( UPPER(v_voorwaarde), 1, 4 ) IN ( 'LIKE', 'NOT ' )
         OR SUBSTR (UPPER(v_voorwaarde), 1, 7 ) IN ( 'BETWEEN')
            )
    THEN -- hele voorwaarde is opgegeven, behalve kolomnaam
      v_voorwaarde := p_kolomnaam||' '||v_voorwaarde;
    ELSE -- alleen waarde is opgegeven
      IF ( p_datatype = 'C' )
      THEN
        IF ( NOT SUBSTR( v_voorwaarde, 1, 1 ) = '''' )
        THEN
          v_voorwaarde := ''''||v_voorwaarde;
        END IF;
        IF ( NOT SUBSTR( v_voorwaarde, -1, 1 ) = '''' )
        THEN
          v_voorwaarde := v_voorwaarde||'''';
        END IF;
      ELSIF ( p_datatype = 'D' )
      THEN
        IF ( NOT SUBSTR( v_voorwaarde, 1, 1 ) = '''' )
        THEN
          v_voorwaarde := ''''||v_voorwaarde;
        END IF;
        IF ( NOT SUBSTR( v_voorwaarde, -1, 1 ) = '''' )
        THEN
          v_voorwaarde := v_voorwaarde||'''';
        END IF;
        v_voorwaarde := 'to_date('||v_voorwaarde||','''||p_datumformaat||''')';
      END IF;
      IF ( INSTR( v_voorwaarde, '%' ) > 0
        OR ( INSTR( v_voorwaarde, '_' ) > 0 and NOT substr(v_voorwaarde,1,3) = 'TO_%' )
         )
      THEN
        v_voorwaarde := p_kolomnaam||' like '||v_voorwaarde;
      ELSE
        v_voorwaarde := p_kolomnaam||' = '||v_voorwaarde;
      END IF;
    END IF;
    v_return := v_voorwaarde;
  END IF;
    RETURN (v_return);
END formatteer_where;

FUNCTION where_deel
( p_idef_id  IN NUMBER
, p_conditie IN VARCHAR2
, p_where    IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  CURSOR ifss_cur
  ( b_idef_id number
  )
  IS
    SELECT idef.datum_formaat
    ,      ifss.conditie
    ,      ifss.code
    ,      ifss.datatype
    FROM   kgc_interface_samenstelling ifss
    ,      kgc_interface_definities idef
    WHERE  idef.idef_id = b_idef_id
    AND    idef.idef_id = ifss.idef_id
    AND    ifss.conditie IS NOT NULL
    ORDER BY volgorde
    ;
BEGIN
  -- clausules per kolom (definitie)
  FOR ifss_rec IN ifss_cur (p_idef_id)
  LOOP
    IF ( v_return IS NULL )
    THEN
      v_return := formatteer_where
                  ( p_voorwaarde   => ifss_rec.conditie
                  , p_kolomnaam    => ifss_rec.code
                  , p_datatype     => ifss_rec.datatype
                  , p_datumformaat => ifss_rec.datum_formaat
                  )
      ;
    ELSE
      v_return := v_return||CHR(10)||'and '
               || formatteer_where
                  ( p_voorwaarde   => ifss_rec.conditie
                  , p_kolomnaam    => ifss_rec.code
                  , p_datatype     => ifss_rec.datatype
                  , p_datumformaat => ifss_rec.datum_formaat
                  );
    END IF;
  END LOOP;
  -- (extra) clausules vanuit een scherm (KGCIDEF10)
  IF ( p_conditie IS NOT NULL )
  THEN
    IF ( v_return IS NULL )
    THEN
      v_return := p_conditie;
    ELSE
      v_return := v_return||CHR(10)||'and '||p_conditie;
    END IF;
  END IF;
  -- extra clausules via parameter
  IF ( p_where IS NOT NULL )
  THEN
    IF ( v_return IS NULL )
    THEN
      v_return := p_where;
    ELSE
      v_return := v_return||CHR(10)||'and '||p_where;
    END IF;
  END IF;

  RETURN ( v_return );
END where_deel;
--
FUNCTION order_by_deel
( p_sortering IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
BEGIN
  IF ( p_sortering IS NOT NULL )
  THEN
    if ( upper(p_sortering) like 'ORDER BY %' )
    then
      v_return := p_sortering;
    ELSE
      v_return := 'order by '||p_sortering;
    END IF;
  END IF;

  RETURN ( v_return );
END order_by_deel;
 -- extra mogelijkheid om 'vaste' tekst aan uitvoerbestand toe te voegen:
-- ook evt. via een select-statement
function koptekst
( p_tekst in varchar2
)
return varchar2
is
  v_tekst varchar2(4000) := p_tekst;
  v_vast varchar2(4000); -- vaste tekst-deel
  v_return varchar2(4000);
  procedure zet_prompt
  ( b_tekst in out varchar2
  )
  is
    l_aantal number := kgc_util_00.num_of_occur( b_tekst, chr(10) );
  begin
    b_tekst := 'prompt '||b_tekst;
    for i in 1..l_aantal
    loop
      -- zet chr(10) tijdelijk naar <CR>
      b_tekst := replace( b_tekst, chr(10), '<CR>prompt ' );
    end loop;
    -- zet <CR> terug naar chr(10)
    while (instr( b_tekst, '<CR>' ) > 0 )
    loop
      b_tekst := replace( b_tekst, '<CR>', chr(10) );
    end loop;
  end zet_prompt;
begin
  if ( p_tekst is null )
  then
    return( null );
  end if;
  v_tekst := replace ( v_tekst, '<select>', '<SELECT>' );
  v_tekst := replace ( v_tekst, '</select>', '</SELECT>' );
  if ( instr( v_tekst, '<SELECT>' ) > 0
   and instr( v_tekst, '</SELECT>' ) > 0
     )
  then
    v_return := kgc_util_00.strip
                ( p_string => v_tekst
                , p_tag_va => '<SELECT>'
                , p_tag_tm => '</SELECT>'
                );
    v_return := rtrim(v_return, chr(10));
     if ( substr( v_return, -1 ) in ( ';', '/' ) )
    then
      null;
    else
      v_return := v_return||';';
    end if;

    -- vaste tekst voor de selectie
    v_vast := substr( v_tekst
                    , 1
                    , instr( v_tekst, '<SELECT>' ) - 1
                    );
    if ( v_vast is not null )
    then
      zet_prompt( v_vast );
      v_return := v_vast || v_return;
    end if;
     -- vaste tekst na de selectie
    v_vast := substr( v_tekst
                    , instr( v_tekst, '</SELECT>' ) + 10
                    );
    if ( v_vast is not null )
    then
      zet_prompt( v_vast );
      v_return := v_return || v_vast ;
    end if;
  else  -- geen select-statement
    v_return := p_tekst;
    zet_prompt( v_return );
  end if;
  return( v_return );
end koptekst;


PROCEDURE werklijst_interface_bestand
( p_idef_id             IN NUMBER
, p_locatie             IN VARCHAR2
, p_bestandsnaam        IN VARCHAR2
, p_trge_id             IN NUMBER   DEFAULT NULL
, p_koptekst            IN VARCHAR2 DEFAULT NULL -- afwijkende koptekst
)
IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 06-05-2012

DOEL
Het aanmaken van werklijst bestanden op de locaties zoals aangegeven bij de
systeemparameters BEH_ROBOT_WLST_DIR_01 en BEH_ROBOT_WLST_DIR_02

1. Haal de interface definitie op voor de opgegeven p_idef_id
2. Verwijder de Oracle directory mocht deze nog bestaan. Dit om fouten te voorkomen
3. formatteer de locatienaam en bestandsnaam
4. maak de Oracle directories aan waar de werklijsten moeten komen te staan
5. Bouw de query statement op dat de juiste gegevens selecteerd
6. Open het bestand
7. Voer de query uit door er doorheen te loopen. Voeg iedere regel toe aan het bestand
8. Sluit het bestand
9. verwijder de Oracle directory
10. registreer het tray gebruik in KGC_BESTANDEN

INPUT PARAMETERS
p_idef_id       NUMBER     -- Het unieke nummer van de interface definitie
p_locatie       VARCHAR2   -- De locatie op server waar het bestand, voor C_01 availability, neergezet dient
                           -- te worden. Bijvoorbeeld: \\umcfs01\CUKZ_antrg$\DNAdiagnostiek\PCRROBOT\Pending\
p_bestandsnaam  VARCHAR2   -- Naam van het werklijst bestand
p_trge_id       NUMBER     -- trge_id van de aangemaakte werklijsten
p_koptekst      VARCHAR2   -- afwijkende koptekst  -- Koptekst


!!! We maken nu gebruik van een systeemparameter voor de locatie van de werklijsten. Dit is echter tijdelijk.
!!! Normaal zou deze directory opgehaald moeten worden uit de interface definitie maar de database heeft
!!! geen toegang om hier bestanden weg te schrijven. Voorlopig doen we het zo.

Wanneer        Wie             Wat
------------------------------------------------------------------------
06-05-2012     Y.Arts          Creatie
1.0.0
***********************************************************************/

  v_dbs_locatie       VARCHAR2(100);  -- tijdelijke uitvoerlocatie op database server
  v_dbs_bestand       VARCHAR2(100);  -- tijdelijke uitvoerbestand op database server
  v_sql_bestand       VARCHAR2(100);  -- tijdelijk sql-bestand database server
  v_client_locatie    VARCHAR2(100);  -- werkelijke uitvoerlocatie
  v_client_bestand    VARCHAR2(100);  -- werkelijk uitvoerbestand

  out_file            utl_file.file_type;
  linebuf             VARCHAR2(2000);
  v_regel             VARCHAR2(2000);
  v_regel1            VARCHAR2(2000);
  v_tmp_tabel         VARCHAR2(50);
  v_column            VARCHAR2(100); -- hulpvariabele voor kolom formatteren
  v_select_deel       VARCHAR2(32767);
  v_where_deel        VARCHAR2(32767);
  v_order_by_deel     VARCHAR2(32767);
  v_statement         VARCHAR2(32767);
  v_module            varchar2(2000);
  v_connect           VARCHAR2(100);
  v_connect_string       VARCHAR2(100);
  v_database_domein      VARCHAR2(100);
  v_sqlplus              VARCHAR2(200);
  v_sqlplus_bestandsnaam varchar2(100);
  v_sqlplus_locatie      varchar2(100);
  v_statement_client     varchar2(100);
  v_statement_dbs        varchar2(100);
  v_statement_sqlplus    varchar2(100);
  v_statement_sqlplusdir varchar2(100);


  handle      NUMBER;
  dbms_return NUMBER;
  kolom       varchar(200);

TYPE cur_typ IS REF CURSOR;
    c cur_typ;


  CURSOR idef_cur
  ( b_idef_id in number
  )
  IS
    SELECT code
    ,      formaat
    ,      bestandstype
    ,      in_uit
    ,      scheidingsteken
    ,      omsluitteken
    ,      datum_formaat
    ,      procedure_naam
    ,      view_naam
    ,      tabel_naam
    ,      remote_tabel
    ,      stappen_uitsluiten
    ,      locatie
    ,      bestand
    ,      conditie
    ,      sortering
    ,      koptekst
    FROM kgc_interface_definities
    WHERE idef_id = b_idef_id
    ;

  idef_rec idef_cur%rowtype;

  CURSOR ifss_cur
  ( b_idef_id in number
  )
  IS
    SELECT code
    ,      datatype
    ,      positie_va
    ,      lengte
    ,      selecteren
    ,      default_waarde
    ,      functie
    ,      remote_kolom
    FROM   kgc_interface_samenstelling
    WHERE  idef_id = b_idef_id
    ORDER BY volgorde, positie_va, code
    ;
  -- haal bestandstype voor het geval geinsert in kgc_bestanden
  CURSOR btyp_cur
  ( b_entiteit_code       IN VARCHAR2
  , b_interface_definitie IN VARCHAR2
  )
  IS
    SELECT btyp.btyp_id
    FROM   kgc_bestand_types btyp
    WHERE  btyp.enti_code = 'TRGE'
    AND    btyp.code      = b_interface_definitie
    ;
  btyp_rec btyp_cur%rowtype;

  -- hoofdprogramma
BEGIN

  -- haal de interface definitie op
  OPEN  idef_cur (b_idef_id => p_idef_id);
  FETCH idef_cur INTO idef_rec;
  CLOSE idef_cur;

  -- verwijder de Oracle directory mocht deze nog bestaan. Dit om fouten te voorkomen
  BEGIN
    v_statement := 'drop directory clientdir';
    execute immediate v_statement;
  EXCEPTION
    when others then
      null;
  END;

  -- formatteer de locatienaam en bestandsnaam
  v_client_locatie := RTRIM( p_locatie, '\')||'\'; --'\\umcukz13\dna$\yuri';
  v_client_bestand := p_bestandsnaam;

  -- maak de Oracle directory aan waar de werklijsten moeten komen staan
  v_statement_client := 'create directory clientdir as '||''''||v_client_locatie||'''';
  execute immediate v_statement_client;

  -- Bouw de select statement op
  v_select_deel := select_deel
                     ( p_idef_id         => p_idef_id
                     , p_datum_formaat   => idef_rec.datum_formaat
                     , p_formaat         => idef_rec.formaat
                     , p_omsluitteken    => idef_rec.omsluitteken
                     , p_scheidingsteken => idef_rec.scheidingsteken
                     , p_tabel           => NVL( idef_rec.view_naam, idef_rec.tabel_naam )
                     );
  IF ( v_select_deel IS NOT NULL
  AND UPPER( v_select_deel ) NOT LIKE 'SELECT %'
     )
  THEN
    v_select_deel := 'select '||v_select_deel;
  END IF;

  v_where_deel := where_deel
                  ( p_idef_id  => p_idef_id
                  , p_conditie => idef_rec.conditie
                  , p_where    => 'where trge_id = '||p_trge_id
                  );

  v_order_by_deel := order_by_deel( p_sortering => idef_rec.sortering );

  if ( v_order_by_deel is not null )
  then
    v_where_deel := v_where_deel||chr(10)||v_order_by_deel;
  end if;

  IF INSTR( UPPER(v_where_deel), 'GROUP BY' ) > 0
  THEN
    v_where_deel := SUBSTR( v_where_deel
                          , 1
                          , INSTR( UPPER(v_where_deel), 'GROUP BY' ) - 1
                          );
  END IF;
  IF ( UPPER(v_select_deel) NOT LIKE 'SELECT DISTINCT%' )
  THEN
    v_select_deel := REPLACE( v_select_deel, 'SELECT ', 'select distinct ' );
    v_select_deel := REPLACE( v_select_deel, 'select ', 'select distinct ' );
  END IF;
  --
  IF ( NVL( idef_rec.view_naam, idef_rec.tabel_naam ) IS NOT NULL )
  THEN
    IF ( p_bestandsnaam IS NOT NULL )
    THEN
      controleer_bestand
      ( p_locatie    => 'CLIENTDIR'
      , p_bestand    => v_client_bestand
      , p_dbs_client => 'CLIENT'
      , p_rw         => 'w'
      );

      out_file  := utl_file.fopen(location    => 'CLIENTDIR'
                                , filename    => v_client_bestand
                                , open_mode   => 'w'
                                 );

      if ( idef_rec.sortering is not null )
      then
           v_statement := v_select_deel||' kolom, '||idef_rec.sortering
                    ;
      else
        v_statement := v_statement
           ||CHR(10)|| v_select_deel||' kolom'
                      ;
        if ( instr( upper( v_where_deel ), 'ORDER BY ' ) > 0 )
        then
          v_statement := v_statement
             ||CHR(10)||', '||substr( upper( v_where_deel )
                                    , instr( upper( v_where_deel ), 'ORDER BY ' ) + 9
                                    )
                       ;
        end if;
      end if;

      v_statement := v_statement
         ||CHR(10)|| 'from '||NVL( idef_rec.view_naam, idef_rec.tabel_naam )
         ||CHR(10)|| v_where_deel -- = incl. v_order_by_deel
                   ;

      -- Voer de statement uit door er doorheen te loopen. Voeg iedere regel toe aan het bestand.
      OPEN c FOR v_statement;
        LOOP
          FETCH c INTO v_regel, v_regel1;
          EXIT WHEN c%NOTFOUND;

            utl_file.put_line(file   => out_file
                             ,buffer => v_regel);


        END LOOP;
      CLOSE c;

      IF ( utl_file.is_open( file => out_file ) )
      THEN
        utl_file.fclose( file => out_file );
      END IF;

    END IF;

  END IF;

  -- registreer het tray gebruik in KGC_BESTANDEN
  OPEN  btyp_cur (b_entiteit_code       => 'TRGE'
                 ,b_interface_definitie => idef_rec.code);
  FETCH btyp_cur INTO btyp_rec;
  CLOSE btyp_cur;

  IF btyp_rec.btyp_id IS NOT NULL  -- btyp.code moet overeenkomen met idef.code, anders geen registratie!
  THEN

    BEGIN
      INSERT INTO KGC_BESTANDEN
      ( entiteit_code
      , entiteit_pk
      , btyp_id
      , locatie
      , bestandsnaam
      )
      values( 'TRGE'
      ,      p_trge_id
      ,      btyp_rec.btyp_id
      ,      v_client_locatie
      ,      v_client_bestand
      );
    EXCEPTION
      when others then
        null;
    END;


  END IF;

  IF ( utl_file.is_open( file => out_file ) )
  THEN
    utl_file.fclose( file => out_file );
  END IF;

  -- drop directories
  v_statement_client     := 'drop directory CLIENTDIR';
  execute immediate v_statement_client;

EXCEPTION
  WHEN OTHERS
  THEN
    IF ( utl_file.is_open( file => out_file ) )
    THEN
      utl_file.fclose( file => out_file );
    END IF;
    RAISE;
END werklijst_interface_bestand;



FUNCTION is_nummer
( p_as                 IN VARCHAR2
, p_waarde             IN VARCHAR2
, p_trty_indicatie_hor IN VARCHAR2 default null
, p_trty_indicatie_ver IN VARCHAR2 default null
)
RETURN NUMBER
/* (public) Positie kan alfanummeriek worden uitgedrukt; wordt hier vertaald in een nummer */
IS
  v_return NUMBER;
BEGIN
  BEGIN
    RETURN( TO_NUMBER( p_waarde ) ); -- waarde is al numeriek
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END;
  IF ( p_as = 'X' )
  THEN
    v_return := ASCII( p_waarde ) - ASCII( p_trty_indicatie_hor ) + 1;
  ELSIF ( p_as = 'Y' )
  THEN
    v_return := ASCII( p_waarde ) - ASCII( p_trty_indicatie_ver ) + 1;
  END IF;
  RETURN( v_return );
END is_nummer;

FUNCTION is_letter
( p_as                 IN VARCHAR2
, p_waarde             IN VARCHAR2
, p_trty_indicatie_hor IN VARCHAR2 default null
, p_trty_indicatie_ver IN VARCHAR2 default null
)
RETURN VARCHAR2
/* positie kan alfanummeriek worden uitgedrukt; nummer wordt hier vertaalt */
IS
  v_return VARCHAR2(3);
  v_nummer NUMBER;
BEGIN
  BEGIN
    v_nummer := TO_NUMBER( p_waarde );
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN( p_waarde ); -- waarde is niet numeriek
  END;
  IF ( p_as = 'X' )
  THEN
    IF ( p_trty_indicatie_hor = '1' )
    THEN
      v_return := p_waarde;
    ELSE
      v_return := CHR( ASCII(p_trty_indicatie_hor) + v_nummer - 1 );
    END IF;
  ELSIF ( p_as = 'Y' )
  THEN
    IF ( p_trty_indicatie_ver = '1' )
    THEN
      v_return := p_waarde;
    ELSE
      v_return := CHR( ASCII(p_trty_indicatie_ver) + v_nummer - 1 );
    END IF;
  END IF;
  RETURN( v_return );
END is_letter;

FUNCTION koptekst
( P_IDEF_ID IN NUMBER
)
RETURN varchar2
IS
  v_return    varchar2(4000);
  v_vast      varchar2(2000);
  v_parameter varchar2(100);
  cursor idef_cur
  is
    select koptekst
    from   kgc_interface_definities
    where  idef_id = p_idef_id
    ;
  v_tekst varchar2(2000);

  procedure zet_prompt
  ( b_tekst in out varchar2
  )
  is
    l_aantal number := kgc_util_00.num_of_occur( b_tekst, chr(10) );
  begin
    b_tekst := 'prompt '||b_tekst;
    for i in 1..l_aantal
    loop
      -- zet chr(10) tijdelijk naar <CR>
      b_tekst := replace( b_tekst, chr(10), '<CR>prompt ' );
    end loop;
    -- zet <CR> terug naar chr(10)
    while (instr( b_tekst, '<CR>' ) > 0 )
    loop
      b_tekst := replace( b_tekst, '<CR>', chr(10) );
    end loop;
  end zet_prompt;

BEGIN
  open  idef_cur;
  fetch idef_cur into v_tekst;
  close idef_cur;
  if ( v_tekst is null )
  then
    return( null );
  end if;

  v_tekst := replace ( v_tekst, '<select>', '<SELECT>' );
  v_tekst := replace ( v_tekst, '</select>', '</SELECT>' );
  if ( instr( v_tekst, '<SELECT>' ) > 0
   and instr( v_tekst, '</SELECT>' ) > 0
     )
  then
    v_return := kgc_util_00.strip
                ( p_string => v_tekst
                , p_tag_va => '<SELECT>'
                , p_tag_tm => '</SELECT>'
                );
    v_return := rtrim(v_return, chr(10));

    if ( substr( v_return, -1 ) in ( ';', '/' ) )
    then
      null;
    else
      v_return := v_return||';';
    end if;

    -- vaste tekst voor de selectie
    v_vast := substr( v_tekst
                    , 1
                    , instr( v_tekst, '<SELECT>' ) - 1
                    );
    if ( v_vast is not null )
    then
      zet_prompt( v_vast );
      v_return := v_vast || v_return;
    end if;

    -- vaste tekst na de selectie
    v_vast := substr( v_tekst
                    , instr( v_tekst, '</SELECT>' ) + 10
                    );
    if ( v_vast is not null )
    then
      zet_prompt( v_vast );
      v_return := v_return || v_vast ;
    end if;
  else  -- geen select-statement
    v_return := v_tekst;
    zet_prompt( v_return );
  end if;

  -- KGCIDEF12-toevoeging:
  -- parameters kunnen zijn toegevoegd in de vorm van verwijzingen naar schermvelden <KGCIDEF12.blok.item>
  while ( INSTR( v_return, '<KGCIDEF12.' ) > 0 )
  loop
    v_parameter := kgc_util_00.strip
                   ( p_string => v_return
                   , p_tag_va => '<KGCIDEF12.'
                   , p_tag_tm => '>'
                   );
    v_return := replace( v_return
                       , '<KGCIDEF12.'||v_parameter||'>'
                       , v_parameter
                       );
  end loop;
  -- einde KGCIDEF12-toevoeging

  return( v_return );
END koptekst;

PROCEDURE werklijst_interface
(p_kafd_id           in number
)
IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts, YA IT-Services
Datum onderhoud : 06-05-2012

DOEL

1. Haal de interface definitie code op voor de betreffende afdeling
2. Haal de werklijst directory op waar de werklijstbestanden terecht moeten komen
3. Selecteer de gegevens uit beh_hlp_TRAY_GEBRUIK
4. Generneer de samples
5. Maak een sample aan per trvu-rij
6. Doorloop de voorgedefinieerde interface definities.
7. Genereer een werklijst bestandsnaam
8. Output voor de gebruiker, welk bestand is aangemaakt
8. Roep de procedure WERKLIJST_INTERFACE_BESTAND aan om het bestand aan te maken.

INPUT PARAMETERS
p_kafd_id : het ID van de opgegeven afdeling

OUTPUT PARAMETERS


HISTORIE

Wanneer        Wie             Wat
------------------------------------------------------------------------
06-05-2012     Y.Arts          Creatie
1.0.0
***********************************************************************/

  v_systematiek      VARCHAR2(10) := 'TRAY_LR';
  v_koptekst         kgc_interface_definities.koptekst%type;
  v_idef_code        kgc_interface_definities.code%type;
  v_sample_name      VARCHAR2(100);
  v_bestand          VARCHAR2(100);
  v_idef_locatie     VARCHAR2(100);
  v_idef_locatie_01  VARCHAR2(100);
  v_idef_locatie_02  VARCHAR2(100);
  v_idef_locatie_03  VARCHAR2(100);

  cursor trge_cur
  is

  select trge_hlp.trge_id trge_id
  ,      trge_hlp.werklijst_nummer
  ,      trge_hlp.available
  ,      trge.datum
  ,      trge.externe_id
  ,      trge.trty_id
  ,      trge.tech_id
  ,      trge.gefixeerd
  from   beh_hlp_tray_gebruik trge_hlp
  ,      kgc_tray_gebruik     trge
  where  trge_hlp.trge_id    = trge.trge_id

  and    trge_hlp.session_id = c_session_id
  ;

  cursor idef_cur (b_idef_code in varchar2)
  is
  select idef_id      --119
  ,      code         --ROBOT_DNA
  ,      omschrijving --Definitie voor DNA robotstraat
  ,      locatie      --\\umcfs01\CUKZ_antrg$\DNAdiagnostiek\PCRROBOT\Pending\
  ,      bestand      --<NUST>BEST</NUST>
  ,      bestandstype --TXT
  from   kgc_interface_definities idef
  where  idef.code like nvl(b_idef_code,'%')
  and    idef.in_uit = 'U'
  ;

  cursor trvu_sample_cur (b_trge_id in number)
  is

  select trvu.trvu_id
  ,      trvu.meet_id
  from   kgc_tray_vulling trvu
  where  trvu.trge_id = b_trge_id
  and    not exists
         ( select null
           from   kgc_samples samp
           where  samp.meet_id = trvu.meet_id
         )
  ;

  FUNCTION genereer_bestandsnaam
  ( p_bestand    IN VARCHAR2
  , p_nummertype IN VARCHAR2
  , p_kafd_id    IN NUMBER
  , p_waarde1    IN VARCHAR2 := NULL
  , p_waarde2    IN VARCHAR2 := NULL
  )
  RETURN VARCHAR2
  IS
    v_nummerstructuur kgc_nummerstructuur.code%type;
    v_bestand kgc_interface_definities.bestand%type;
  BEGIN
    v_nummerstructuur := kgc_util_00.strip
                         ( p_string => p_bestand
                         , p_tag_va => '<NUST>'
                         , p_tag_tm => '</NUST>'
                         );
    v_bestand := kgc_nust_00.genereer_nr
                 ( p_nummertype      => p_nummertype
                 , p_nummerstructuur => v_nummerstructuur
                 , p_kafd_id         => p_kafd_id
                 , p_ongr_id         => null
                 , p_waarde1         => p_waarde1 -- werklijstnummer
                 , p_waarde2         => p_waarde2 -- externe_id
                 , p_waarde3         => null
                 , p_waarde4         => null
                 , p_waarde5         => null
                 );
    RETURN(v_bestand);
  EXCEPTION
    WHEN OTHERS THEN
      RAISE;
  END genereer_bestandsnaam;


BEGIN
  -- Haal de interface definitie code op voor de betreffende afdeling
  v_idef_code := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'ROBOTSTRAAT_INTERFACE'
                     , p_kafd_id        => p_kafd_id
                     );

  -- Haal de werklijst directory op waar de werklijstbestanden terecht moeten komen
  v_idef_locatie_01 := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'BEH_ROBOT_WLST_DIR_01'
                     , p_kafd_id        => p_kafd_id
                     );
  v_idef_locatie_02 := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'BEH_ROBOT_WLST_DIR_02'
                     , p_kafd_id        => p_kafd_id
                     );
  v_idef_locatie_03 := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'BEH_ROBOT_WLST_DIR_03'
                     , p_kafd_id        => p_kafd_id
                     );

  --Selecteer de gegevens uit BEH_HLP_TRAY_GEBRUIK
  for trge_rec in trge_cur loop

    -- genereer samples
    FOR trvu_sample_rec IN trvu_sample_cur (b_trge_id => trge_rec.trge_id) LOOP

      -- maak een sample aan per trvu-rij
      v_sample_name := kgc_interface.standaard_waarde
                       ( p_meta_type => 'SAMPLE#'
                       , p_trge_id   => trge_rec.trge_id
                       , p_trvu_id   => trvu_sample_rec.trvu_id
                       );
      BEGIN
        INSERT INTO kgc_samples
        ( meet_id
        , prompt
        , naam
        )
        VALUES
        ( trvu_sample_rec.meet_id
        , 'Sample'
        , v_sample_name
        );
      END;
    END LOOP;

    -- doorloop de voorgedefinieerde interface definities.
    for idef_rec in idef_cur (b_idef_code => v_idef_code) loop

      IF ( v_idef_locatie_01 IS NOT NULL AND
           v_idef_locatie_02 IS NOT NULL AND
           v_idef_locatie_03 IS NOT NULL AND
           idef_rec.bestand IS NOT NULL
         )
      THEN
        v_bestand := idef_rec.bestand;

        IF ( instr(idef_rec.bestand,'<NUST>') > 0 )
        THEN
          v_bestand := genereer_bestandsnaam
                         ( p_bestand    => idef_rec.bestand
                         , p_nummertype => 'BEST'
                         , p_kafd_id    => p_kafd_id
                         , p_waarde1    => trge_rec.werklijst_nummer
                         , p_waarde2    => trge_rec.externe_id
                         );
        END IF;

        v_koptekst := koptekst(p_idef_id => idef_rec.idef_id);

        -- output voor de gebruiker, welk bestand is aangemaakt
        gebruikers_info( p_tekst => 'Werklijst bestand: '||v_bestand);
        dbms_output.put_line ('Werklijst bestand: '||v_bestand);

        -- Als trge_rec.available 'C_01' is dan moet het naar locatie 01 weggeschreven worden, anders naar locatie 02.
        IF trge_rec.available = 'C_01'
        THEN
          v_idef_locatie := v_idef_locatie_01;
        ELSIF trge_rec.available = 'C_02'
        THEN
          v_idef_locatie := v_idef_locatie_02;
        ELSIF trge_rec.available = 'C_03'
        THEN
          v_idef_locatie := v_idef_locatie_03;
        END IF;

        werklijst_interface_bestand
        ( p_idef_id       => idef_rec.idef_id
        , p_locatie       => v_idef_locatie
        , p_bestandsnaam  => v_bestand
        , p_trge_id       => trge_rec.trge_id
        , p_koptekst      => v_koptekst
        );

      END IF;

    end loop;

  end loop;

EXCEPTION
  WHEN OTHERS
  THEN
    qms$errors.unhandled_exception('beh_dna_voortest_wlst_aanmaken.werklijst_interface');
END;



function maak_werklijst
 (P_PROTOCOL            IN VARCHAR2
 ,P_KAFD_ID             IN NUMBER
 ,P_TRTY_ID             IN NUMBER
 ,P_TECH_ID             IN NUMBER
 ,P_F_STATUS            IN VARCHAR2
 ,P_ONDERZOEKSSTATUS    IN VARCHAR2
 ,P_ONDERZOEKSWIJZE     IN VARCHAR2
 ,P_MEETWAARDEN_OP_TRAY IN NUMBER
 ,P_VOL_WLST            IN VARCHAR2
 ,P_NUMMERST            IN VARCHAR2
) return boolean
 is
/***********************************************************************
Auteur/bedrijf  : Y.Arts, YA IT-Services
Datum onderhoud : 06-05-2012

DOEL
Per protocol moet een werklijst aangemaakt worden. Een protocol heeft minimaal 1 werklijst
maar dit kunnen er ook meerdere zijn, afhankelijk van hoeveel stoftesten er geselecteerd worden.
Er kunnen maar 96 stoftesten op een WELL van 96 en de laatste is altijd een MQ stoftest.
Wanneer een werklijst meerdere WELLS bevat dan worden het meerdere werklijsten. Een stoftest op
een werklijst wordt ALTIJD afgesloten met een MQ echter, ook als deze verdergaat op een volgend WELL.

1. Selecteer de stoftesten op basis van de ingevoerde parameters
2. Bepaal de naam van de werklijst  KGCNUST01: code = WERK
   ** REFERENTIE -> BEHEER -> WAARDEN -> MEETWAARDE STRUCTUUR
   ** zie systeemparameter LETTER_WERK
3. Zet de stoftesten op de werklijst. Zodra de tray vol zit ga je verder met een volgend werklijst.
   Invoer in tabel KGC_TRAY_GEBRUIK

INPUT PARAMETERS
P_PROTOCOL   : Omschrijving van een stoftestgroep (protocol)
P_KAFD_ID             : Unieke identificatie van een afdeling
P_TRTY_ID             : Unieke identificatie van een tray type
P_TECH_ID             : Unieke identificatie van een techniek
P_ONDERZOEKSSTATUS    : Geeft de status van een onderzoek weer
P_ONDERZOEKSWIJZE     : Geeft de wijze van onderzoeken weer: Scanning, Screening of leeg
P_MEETWAARDEN_OP_TRAY : max aantal meetwaarden op de gekozen tray
P_VOL_WLST : Ja of Nee, Moet de werktlijst vol?
P_HOEV_TRAY_POSITIES : hoeveel posities zitten er precies op de gebruikte tray

OUTPUT PARAMETERS

TABELLEN
BAS_TRAY_MEETWAARDEN_VW
KGC_TRAY_GEBRUIK
BEH_HLP_TRAY_GEBRUIK
BEH_HLP_WERKLIJST
BEH_HLP_WERKLIJST_STOFTESTEN
BEH_HLP_WERKLIJST_MEETWAARDE
BEH_HLP_STORAGE
BEH_HLP_STORAGE_UITVAL

HISTORIE

Wanneer        Wie             Wat
------------------------------------------------------------------------
23-01-2014     Y.Arts         Sortering aangepast met einddatum onderzoek
1.0.3
17-01-2014     Y.Arts         Sortering aangepast met einddatum onderzoek
1.0.2
24-03-2013     Y.Arts         * parameter p_vol_wlst toegevoegd.
1.0.1                                   Standaard worden alleen volle werklijsten aangemaakt.
                                       * procedure verandert in een functie. Return false als de tray niet vol is en p_vol_wlst = J
06-05-2012     Y.Arts          Creatie
1.0.0
***********************************************************************/

  cursor totaal_cur (b_kafd_id           in number
                    ,b_onderzoekswijze   in varchar2
                    ,b_tech_id           in number
                    ,b_f_status          in varchar2
                    ,b_protocol          in varchar2
                    ,b_onderzoeksstatus  in varchar2
                    )
   is

   select sum(subtotaal) from
     (select count(1) subtotaal
      from   bas_tray_meetwaarden_vw tmwv
      ,      kgc_onderzoeken onde
      ,      beh_hlp_storage ste
      where  ste.id = tmwv.fractienummer
      and    ste.id not in (select sul.id
                            from beh_hlp_storage_uitval sul)
      and    ste.available = decode(b_onderzoekswijze,'S','C_01',ste.available)
      and    (tmwv.kafd_id = b_kafd_id
              and ( not exists ( select null from kgc_tray_vulling trvu
                                 where trvu.meet_id = tmwv.meet_id
                               )
                  )
             )
      and    tmwv.frac_status     = b_f_status
      and    tmwv.onderzoekswijze = nvl(b_onderzoekswijze,tmwv.onderzoekswijze)
      and    tmwv.tech_id         = b_tech_id
      and    tmwv.stof_id not in (select stof_id from beh_stof_niet_op_werklijst)
      and    tmwv.stgr_id         in (select stgr_id
                                      from   kgc_stoftestgroepen
                                      where  kafd_id      = b_kafd_id
                                      and    vervallen    = 'N'
                                      and    instr(upper(omschrijving),upper(b_protocol)) > 0)
      and    tmwv.onde_id         = onde.onde_id
      and    onde.status          = b_onderzoeksstatus
      group by stof_id) totaal
      ;

   cursor stof_cur (b_kafd_id           in number
                   ,b_onderzoekswijze   in varchar2
                   ,b_tech_id           in number
                   ,b_f_status          in varchar2
                   ,b_protocol          in varchar2
                   ,b_onderzoeksstatus  in varchar2
                   )
   is

   select stof_id
   ,      tmwv.stof_omschrijving omschrijving
   ,      count(1) subtotaal
   ,      ste.available
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage ste
   where  ste.id = tmwv.fractienummer
   and    ste.id not in (select sul.id
                         from beh_hlp_storage_uitval sul)
   and    ste.available = decode(b_onderzoekswijze,'S','C_01',ste.available)
   and    (tmwv.kafd_id = b_kafd_id
           and ( not exists ( select null from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = nvl(b_onderzoekswijze,tmwv.onderzoekswijze)
   and    tmwv.tech_id         = b_tech_id
   and    tmwv.stof_id not in (select stof_id from beh_stof_niet_op_werklijst)
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0)
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus
   group by tmwv.stof_id, tmwv.stof_omschrijving, ste.available
   order by ste.available, tmwv.stof_omschrijving asc
   ;

   cursor meet_cur (b_kafd_id           in number
                   ,b_onderzoekswijze   in varchar2
                   ,b_tech_id           in number
                   ,b_f_status          in varchar2
                   ,b_protocol in varchar2
                   ,b_stof_id           in number
                   ,b_onderzoeksstatus  in varchar2
                   ,b_available         in varchar2)
   is

   select meet_id
   ,      fractienummer
   ,      ste.available
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage ste
   where  ste.id = tmwv.fractienummer
   and    ste.id not in (select sul.id
                         from beh_hlp_storage_uitval sul)
   and    ste.available = b_available
   and    (tmwv.kafd_id = b_kafd_id
           and ( not exists ( select null
                              from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = nvl(b_onderzoekswijze,tmwv.onderzoekswijze)
   and    tmwv.tech_id         = b_tech_id
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0)
   and    tmwv.stof_id         = b_stof_id
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus
   order by tmwv.geplande_einddatum
   ;

   cursor storage_cur (b_kafd_id           in number
                      ,b_onderzoekswijze   in varchar2
                      ,b_tech_id           in number
                      ,b_f_status          in varchar2
                      ,b_protocol in varchar2
                      ,b_onderzoeksstatus  in varchar2
                      )
   is

   select tmwv.fractienummer, onde.onde_id
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   where  not exists (select ste.id
                      from   beh_hlp_storage ste
                      where  ste.id = tmwv.fractienummer
                     )
   and    (tmwv.kafd_id = b_kafd_id
           and ( not exists ( select null from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = nvl(b_onderzoekswijze,tmwv.onderzoekswijze)
   and    tmwv.tech_id         = b_tech_id
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0)
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus
   group by tmwv.fractienummer, onde.onde_id
   order by tmwv.fractienummer asc
   ;

   cursor storage_cur2 (b_kafd_id           in number
                       ,b_onderzoekswijze   in varchar2
                       ,b_tech_id           in number
                       ,b_f_status          in varchar2
                       ,b_protocol in varchar2
                       ,b_onderzoeksstatus  in varchar2
                       )
   is
   select sul.id fractienummer
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage_uitval sul
   where  sul.id = tmwv.fractienummer
   and    (tmwv.kafd_id = b_kafd_id
           and ( not exists ( select null from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = nvl(b_onderzoekswijze,tmwv.onderzoekswijze)
   and    tmwv.tech_id         = b_tech_id
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0)
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus
   group by sul.id
   order by sul.id asc
   ;

   cursor storage_cur3 (b_kafd_id       in number
                   ,b_onderzoekswijze   in varchar2
                   ,b_tech_id           in number
                   ,b_f_status          in varchar2
                   ,b_protocol in varchar2
                   ,b_onderzoeksstatus  in varchar2
                   )
   is
   select stof_id
   ,      ste.available
   ,      tmwv.onde_id
   ,      tmwv.fractienummer
   ,      tmwv.onderzoeknr
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage ste
   where  ste.id = tmwv.fractienummer
   and    ste.id not in (select sul.id from beh_hlp_storage_uitval sul)
   and    (tmwv.kafd_id = b_kafd_id
           and ( not exists ( select null from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = nvl(b_onderzoekswijze,tmwv.onderzoekswijze)
   and    tmwv.tech_id         = b_tech_id
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0)
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus
   group by tmwv.stof_id, ste.available, tmwv.onde_id, tmwv.fractienummer,tmwv.onderzoeknr
   ;

   cursor pers_cur2 (b_onde_id in number
                    ,b_onderzoekswijze   in varchar2
                    ,b_tech_id           in number
                    ,b_f_status          in varchar2
                    ,b_protocol in varchar2
                    ,b_onderzoeksstatus  in varchar2 )
   is
   select  tmwv.frac_id, mons.pers_id
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      kgc_monsters            mons
   where  tmwv.onde_id = b_onde_id
   and    tmwv.mons_id = mons.mons_id
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = nvl(b_onderzoekswijze,tmwv.onderzoekswijze)
   and    tmwv.tech_id         = b_tech_id
   and    onde.status          = b_onderzoeksstatus
   and    tmwv.onde_id         = onde.onde_id
   and    tmwv.stgr_id in (select stgr_id
                           from   kgc_stoftestgroepen
                           where  kafd_id      = onde.kafd_id
                           and    vervallen    = 'N'
                           and    instr(upper(omschrijving),upper(b_protocol)) > 0
                          )
   group by tmwv.frac_id, mons.pers_id
   ;

   cursor stof_hlp_cur (b_available in varchar2)
   is
   select stof_id
   ,      aantal
   ,      verwerkt
   from   beh_hlp_werklijst_stoftesten
   where  session_id = c_session_id
   and    available  = b_available
   order by geplande_einddatum_onderzoek asc
   ;

   cursor stof_hlp_grp_cur
   is
   select distinct available
   from   beh_hlp_werklijst_stoftesten
   where  session_id = c_session_id
   ;

   cursor meet_hlp_cur (b_stof_id in number
                       ,b_available in varchar2)
   is
   select stof_id
   ,      meet_id
   ,      meet_seq
   ,      fractienummer
   from   beh_hlp_werklijst_meetwaarde
   where    session_id = c_session_id
   and    stof_id    = b_stof_id
   and    verwerkt   = 'N'
   and    available  = b_available
   order by meet_seq
   ;

   cursor onde_cur (b_onde_id in number )
   is
   select stof_id
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage ste
   where  1=1
   and    ste.id       = tmwv.fractienummer
   and    tmwv.onde_id = onde.onde_id
   and    onde.onde_id = b_onde_id
   ;

   v_werklijst_nummer    kgc_tray_gebruik.werklijst_nummer%type;
   v_trge_id             kgc_tray_gebruik.trge_id%type;
   v_teller_meet         number;
   v_totaal_aantal       number;
   v_meet_seq            number := 1;
   v_personen_bij_onderzoek number;
   v_x_positie           number;
   v_y_positie           varchar2(2);
   v_positie_gevuld      number;
   v_fractienummer       varchar2(100);
   v_available           varchar2(10);

   -- Standaar de return waarde op false zetten. Mocht blijken dat een tray gemaakt kan worden dan wordt het daar op TRUE gezet.
   v_return              boolean := false;
   v_aantal_op_werklijst number := 0;
   v_nog_een_tray_zelfde_cyto boolean := false;
   v_totaal_teller number := 10000;
   V_GEPLANDE_EINDDATUM date;

BEGIN

  -- De volgende stoftesten konden niet op de werklijst gezet worden omdat er geen storage van bekend is
  for storage_rec  in  storage_cur(b_kafd_id           => p_kafd_id
                                  ,b_onderzoekswijze   => p_onderzoekswijze
                                  ,b_tech_id           => p_tech_id
                                  ,b_f_status          => p_f_status
                                  ,b_protocol => P_PROTOCOL
                                  ,b_onderzoeksstatus  => p_onderzoeksstatus )  loop
    if p_onderzoekswijze = 'S' then
      gebruikers_info( p_tekst => 'Fractie '||storage_rec.fractienummer||' kon niet op een werklijst gezet worden omdat er geen storage van bekend is. C_01, C_02 of C_03');

      -- andere stoftesten bij dezelfde onderzoek mogen ook niet op de werklijst
      for onde_rec in onde_cur(b_onde_id => storage_rec.onde_id)  loop

        insert into beh_stof_niet_op_werklijst (stof_id) values (onde_rec.stof_id);

      end loop;

    else
      gebruikers_info( p_tekst => 'Fractie '||storage_rec.fractienummer||' kon niet op een werklijst gezet worden omdat er geen storage van bekend is. C_01, C_02');
    end if;
  end loop;

  -- De volgende fracties komen ofwel in meerdere koelkasten voor ofwel meerdere keren geregistreerd binnen 1 koelkast
  for storage_rec2  in  storage_cur2(b_kafd_id           => p_kafd_id
                                    ,b_onderzoekswijze   => p_onderzoekswijze
                                    ,b_tech_id           => p_tech_id
                                    ,b_f_status          => p_f_status
                                    ,b_protocol => P_PROTOCOL
                                    ,b_onderzoeksstatus  => p_onderzoeksstatus )  loop
    gebruikers_info( p_tekst => 'Fractie '||storage_rec2.fractienummer||' komt meerdere keren voor binnen de Storage. Deze zal niet op de werklijst gezet kunnen worden.');
  end loop;


  -- voor screening signaleren als available C_02 of C_03 is
  if p_onderzoekswijze = 'S' then
    for storage_rec3  in  storage_cur3(b_kafd_id           => p_kafd_id
                                      ,b_onderzoekswijze   => p_onderzoekswijze
                                      ,b_tech_id           => p_tech_id
                                      ,b_f_status          => p_f_status
                                      ,b_protocol => P_PROTOCOL
                                      ,b_onderzoeksstatus  => p_onderzoeksstatus )  loop

      -- voor screening geldt dat minimaal 2 fracties bij dezelfde onderzoek bij verschillende patienten moet zitten
      v_personen_bij_onderzoek := 0;

      for pers_rec2 in pers_cur2( b_onde_id           => storage_rec3.onde_id
                                 ,b_onderzoekswijze   => p_onderzoekswijze
                                 ,b_tech_id           => p_tech_id
                                 ,b_f_status          => p_f_status
                                 ,b_protocol => P_PROTOCOL
                                 ,b_onderzoeksstatus  => p_onderzoeksstatus ) loop
        v_personen_bij_onderzoek := v_personen_bij_onderzoek + 1;
      end loop;

      -- er moeten minimaal 2 verschillende personen een fractie hebben bij het onderzoek
      if v_personen_bij_onderzoek <= 1 then
        gebruikers_info( p_tekst => 'Bij het onderzoek '||storage_rec3.onderzoeknr||' moeten minimaal 2 fracties zijn bij verschillende personen, dat is niet het geval.');
        gebruikers_info( p_tekst => 'Fractie '||storage_rec3.fractienummer||' wordt niet op de werklijst gezet.');

        insert into beh_stof_niet_op_werklijst (stof_id) values (storage_rec3.stof_id);

      end if;

    end loop;

  end if;

  -- bepaal het totaal aantal stoftesten die op werklijsten gezet moet worden.
  open totaal_cur(b_kafd_id           => p_kafd_id
                 ,b_onderzoekswijze   => p_onderzoekswijze
                 ,b_tech_id           => p_tech_id
                 ,b_f_status          => p_f_status
                 ,b_protocol          => P_PROTOCOL
                 ,b_onderzoeksstatus  => p_onderzoeksstatus );
  fetch totaal_cur into v_totaal_aantal;
  close totaal_cur;

  -- Vul eerst de hulptabellen om daarna sneller de echte tabellen te kunnen vullen
  if v_totaal_aantal > 0 then

    for stof_rec in stof_cur(b_kafd_id           => p_kafd_id
                            ,b_onderzoekswijze   => p_onderzoekswijze
                            ,b_tech_id           => p_tech_id
                            ,b_f_status          => p_f_status
                            ,b_protocol => P_PROTOCOL
                            ,b_onderzoeksstatus  => p_onderzoeksstatus ) loop

      select min(geplande_einddatum) into v_geplande_einddatum from bas_tray_meetwaarden_vw  where  stof_id = stof_rec.stof_id;

      insert into beh_hlp_werklijst_stoftesten
      (session_id
      ,stof_id
      ,aantal
      ,verwerkt
      ,omschrijving
      ,available
      ,geplande_einddatum_onderzoek) values
      (c_session_id
      ,stof_rec.stof_id
      ,stof_rec.subtotaal
      ,'N'
      ,stof_rec.omschrijving
      ,stof_rec.available
      ,v_geplande_einddatum)
      ;

      v_meet_seq := 1;

      -- doorloop de fracties/meetwaarden en zet deze op de tray
      for meet_rec in meet_cur(b_kafd_id           => p_kafd_id
                              ,b_onderzoekswijze   => p_onderzoekswijze
                              ,b_tech_id           => p_tech_id
                              ,b_f_status          => p_f_status
                              ,b_protocol => P_PROTOCOL
                              ,b_stof_id           => stof_rec.stof_id
                              ,b_onderzoeksstatus  => p_onderzoeksstatus
                              ,b_available         => stof_rec.available ) loop
        insert into beh_hlp_werklijst_meetwaarde
        (session_id
        ,stof_id
        ,meet_seq
        ,meet_id
        ,fractienummer
        ,verwerkt
        ,available
        ) values
        (c_session_id
        ,stof_rec.stof_id
        ,v_meet_seq
        ,meet_rec.meet_id
        ,meet_rec.fractienummer
        ,'N'
        ,decode(p_onderzoekswijze,'S','C_01',stof_rec.available)
        );

        update beh_hlp_werklijst_stoftesten
        set    verwerkt   = 'J'
        where  stof_id    = stof_rec.stof_id
        and    session_id = c_session_id
        and    available  = stof_rec.available
        ;

        v_meet_seq := v_meet_seq + 1;

      end loop;

    end loop;

  end if; -- einde hulptabellen vullen

  v_totaal_teller := 100;

  -- als het totaal aantal groter dan 0 is moeten er stoftesten op tray gezet worden
  while v_totaal_aantal > 0 loop  -- zet na verwerking van een stoftest v_totaal_aantal := v_totaal_aantal - verwerkte_fracties

    if p_vol_wlst = 'J' and v_totaal_aantal < p_meetwaarden_op_tray then
        -- zorg ervoor dat uit de loop gesprongen wordt. Zet v_totaal_aantal gedwongen op 0.
      v_totaal_aantal := 0;
      exit;
    elsif v_totaal_teller = 0 then
      dbms_output.put_line('excape exit uit loop');
      exit;
    else
      v_totaal_teller := v_totaal_teller - 1;

      for stof_hlp_grp_rec in stof_hlp_grp_cur loop

        -- als parameter p_vol_wlst = J dan alleen volle werklijsten aanmaken
        if p_vol_wlst = 'J' and v_totaal_aantal < p_meetwaarden_op_tray then
          -- zorg ervoor dat uit de loop gesprongen wordt. Zet v_totaal_aantal gedwongen op 0.
          v_totaal_aantal := 0;
          exit;
        else

          v_nog_een_tray_zelfde_cyto := true;

          WHILE v_nog_een_tray_zelfde_cyto Loop

            v_nog_een_tray_zelfde_cyto := false;

            -- Werklijstnummer genereren
            v_werklijst_nummer := kgc_nust_00.genereer_nr
                                        ( p_nummertype => p_nummerst
                                        , p_ongr_id    => NULL
                                        , p_kafd_id    => p_kafd_id
                                        );

            v_werklijst_nummer := substr(v_werklijst_nummer,1,11)||'ROBOT';

            select kgc_trge_seq.nextval into v_trge_id from dual;

            -- Maak een TRAY GEBRUIK aan in de tabel KGC_TRAY_GEBRUIK
            insert into kgc_tray_gebruik
            (trge_id
            ,mede_id
            ,tech_id
            ,trty_id
            ,datum
            ,gefixeerd
            ,werklijst_nummer
            )
             values
            (v_trge_id
            ,kgc_mede_00.medewerker_id
            ,p_tech_id
            ,p_trty_id
            ,sysdate
            ,'J'
            ,v_werklijst_nummer
            );

            -- Vul de hulptabel met tray gebruik
            insert into beh_hlp_tray_gebruik
            (session_id
            ,trge_id
            ,werklijst_nummer
            ,available
            )
            values
            (c_session_id
            ,v_trge_id
            ,v_werklijst_nummer
            ,stof_hlp_grp_rec.available
            );

            v_teller_meet := 0;
            v_available := stof_hlp_grp_rec.available;

            -- tijdelijke tabellen zijn al gevuld. Doorloop ze en zet op tray
            for stof_hlp_rec in stof_hlp_cur(b_available => stof_hlp_grp_rec.available) loop

--              if stof_hlp_rec.aantal > (p_meetwaarden_op_tray - v_teller_meet) then
              if (p_meetwaarden_op_tray - v_teller_meet) = 0 then

                v_nog_een_tray_zelfde_cyto := true;
                exit;
                -- het past niet meer op de tray, doe niets en probeer de volgende stoftest, wellicht heeft die wel een aantal dat past op de tray

              else

                -- doorloop de fracties/meetwaarden en zet deze op de tray
                for meet_hlp_rec in meet_hlp_cur(b_stof_id   => stof_hlp_rec.stof_id
                                                ,b_available => stof_hlp_grp_rec.available ) loop

                  if (p_meetwaarden_op_tray - v_teller_meet) = 0 then
                    v_nog_een_tray_zelfde_cyto := true;
                    exit;
                    -- het past niet meer op de tray, doe niets en probeer de volgende stoftest, wellicht heeft die wel een aantal dat past op de tray
                  else
                    v_teller_meet   := v_teller_meet + 1;
                    v_totaal_aantal := v_totaal_aantal - 1;

                    -- meetwaarden op tray zetten, huidige werklijst.
                    insert into beh_hlp_werklijst
                    (session_id
                    ,meet_id
                    ,werklijst_nummer
                    ,trge_id
                    ,sorteer_nummer
                    ,available
                    ) values
                    (c_session_id
                    ,meet_hlp_rec.meet_id
                    ,v_werklijst_nummer
                    ,v_trge_id
                    ,v_teller_meet
                    ,stof_hlp_grp_rec.available
                    );

                    update beh_hlp_werklijst_meetwaarde
                    set   verwerkt = 'J'
                    where session_id  = c_session_id
                    and   stof_id     = stof_hlp_rec.stof_id
                    and   meet_id     = meet_hlp_rec.meet_id
                    and   available   = stof_hlp_grp_rec.available
                    ;

                  end if;

                end loop;

              end if;

            end loop;

            select count(1) into v_aantal_op_werklijst from beh_hlp_werklijst where werklijst_nummer = v_werklijst_nummer;

            if v_teller_meet = 0 or (v_aantal_op_werklijst < p_meetwaarden_op_tray and p_vol_wlst = 'J')
            then
              delete beh_hlp_werklijst    where werklijst_nummer = v_werklijst_nummer;
              delete kgc_tray_gebruik     where werklijst_nummer = v_werklijst_nummer;
              delete beh_hlp_tray_gebruik where werklijst_nummer = v_werklijst_nummer;
            else
               -- Als een werklijst gemaakt kan worden dan doorgaan met tray vullen, dus TRUE retourneren
               v_return := true;
            end if;

          end loop;

        end if;

      end loop;

    end if;

  end loop;

  return v_return;

EXCEPTION
  WHEN OTHERS
  THEN
   rollback;
    qms$errors.unhandled_exception('beh_dna_voortest_wlst_aanmaken.maak_werklijst');

END;

PROCEDURE vul_tray
 ( P_TRTY_HOR_INDICATIE  IN VARCHAR2
 , P_TRTY_VER_INDICATIE  IN VARCHAR2
 , P_TRTY_HOR_NUM_TOTAAL IN NUMBER
 , P_TRTY_VER_NUM_TOTAAL IN NUMBER
 )
 IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts, YA IT-Services
Datum onderhoud : 06-05-2012

DOEL
De stoftesten van een protocol moeten op een tray gezet worden.
Iedere tray wordt een aparte werklijst.

1. Selecteer de stoftesten die op de tray gezet moeten worden ( pl/sql tabel BEH_HLP_WERKLIJST )
2. Bij het aanmaken van de werklijsten is al bepaald welke meetwaarden op een tray gezet worden.
   We doorlopen gewoon de unieke tray gebruiken in cursor WLST_CUR.
3. Vervolgens doorlopen we per tray gebruik de geselecteerde meetwaarden mbv cursor WLST_MEET_CUR.
4. Voer de meetwaarden op in tabel KGC_TRAY_VULLING
5. Houdt rekening met het type tray en hoeveel posities deze heeft.
6. Vul email variabele met de aangemaakte werklijsten


-- Zie de functie 'PLAATS' in KGCTRVU01.
-- Plaatsing is VERTICAAL
-- Vanaf A - 1
-- Maak per tray een werklijst aan. Zie ook KGCTRVU01 de functie 'MAAK'
   ** Zie KGCIDEF01 via het menu REFERENTIE -> BEHEER -> INTERFACE -> INTERFACE DEFINITIE
   ** De code moet zijn 'ROBOT_DNA'
   ** Neem deze structuur over en vul de tray
-- Maak een Robot tekstbestand aan met de vulling van de tray en zet deze op een locatie.
   ** Zie KGCIDEF12 Robotstraat -> Automatisch


INPUT PARAMETERS
P_TRTY_HOR_INDICATIE  : Geeft aan met welke indicatie de tray horizontaal begint, A of 1
P_TRTY_VER_INDICATIE  : Geeft aan met welke indicatie de tray verticaal begint, A of 1
P_TRTY_HOR_NUM_TOTAAL : Geeft het maximale indicatie aan van de horizontale as
P_TRTY_VER_NUM_TOTAAL : Geeft het maximale indicatie aan van de verticale as


HISTORIE

Wanneer/        Wie             Wat
Versie
------------------------------------------------------------------------
06-05-2012     Y.Arts          Creatie
1.0.0
***********************************************************************/

  -- selecteer de unieke tray gebruik nummers
  cursor wlst_cur
  is

  select trge_id, werklijst_nummer
  from   beh_hlp_werklijst
  where  1=1
  and    session_id = c_session_id
  group by trge_id, werklijst_nummer
  ;

  -- selecteer per tray gebruik nummer de meetwaarden die op de tray gezet moeten worden
  cursor wlst_meet_cur (b_trge_id in number)
  is

  select wlst.meet_id
  ,      wlst.werklijst_nummer
  from   beh_hlp_werklijst wlst
  where  1=1
  and    session_id = c_session_id
  and    wlst.verwerkt = 'N'
  and    wlst.trge_id  = b_trge_id
  order by wlst.trge_id, wlst.sorteer_nummer asc
  ;

  cursor stof_cur (b_trge_id in number)
  is

  select wlst.meet_id
  ,      wlst.werklijst_nummer
  ,      stof.omschrijving
  from   beh_hlp_werklijst wlst
  ,      bas_meetwaarden meet
  ,      kgc_stoftesten  stof
  where  1=1
  and    session_id    = c_session_id
  and    wlst.meet_id  = meet.meet_id
  and    meet.stof_id  = stof.stof_id
  and    wlst.verwerkt = 'N'
  and    wlst.trge_id  = b_trge_id
  order by wlst.trge_id, wlst.sorteer_nummer asc
  ;

  v_x            NUMBER  ; -- A
  v_y            NUMBER  ; -- 1
  v_x_pos_vanaf  VARCHAR2(3) ;
  v_y_pos_vanaf  VARCHAR2(3) ;
  v_y_char       VARCHAR2(3) ;
  v_teller_wlst  NUMBER := 0;

  procedure werklijst
  ( p_meet_id          IN NUMBER
  , p_actie            IN VARCHAR2
  , p_werklijst_nummer IN VARCHAR2
  )
  IS
  begin
    kgc_wlst_00.sync_tray_werklijst
    ( p_actie            => p_actie
    , p_meet_id          => p_meet_id
    , p_werklijst_nummer => p_werklijst_nummer
    );
  end;


BEGIN

   -- bepaal op basis van de invoerparameters welke nummering gehanteerd moet worden voor de tray
   v_x            := is_nummer(p_as                 => 'X'
                              ,p_waarde             => 1
                              ,p_trty_indicatie_hor => p_trty_hor_indicatie); -- A
   v_y            := is_nummer(p_as                 => 'Y'
                              ,p_waarde             => 'A'
                              ,p_trty_indicatie_ver => p_trty_ver_indicatie); --1

   v_x_pos_vanaf  := lpad(is_letter(p_as                 => 'X'
                                   ,p_waarde             => v_x
                                   ,p_trty_indicatie_hor => p_trty_hor_indicatie),3,'0');
   v_y_pos_vanaf  := lpad(is_letter(p_as                 => 'Y'
                                   ,p_waarde             => v_y
                                   ,p_trty_indicatie_ver => p_trty_ver_indicatie),3,'0');

   -- Doorloop de unieke werklijsten/tray gebruik.
   -- Bij ieder tray moet opnieuw begonnen worden met tellen van de wells
   for wlst_rec in wlst_cur loop

     v_teller_wlst := 0;

     v_x := is_nummer(p_as                 => 'X'
                     ,p_waarde             => 1
                     ,p_trty_indicatie_hor => p_trty_hor_indicatie); -- A
     v_y := is_nummer(p_as                 => 'Y'
                     ,p_waarde             => 'A'
                     ,p_trty_indicatie_ver => p_trty_ver_indicatie); --1

     -- Per werklijst/tray gebruik moeten de meetwaarden op tray gezet worden
     for wlst_meet_rec IN wlst_meet_cur(b_trge_id => wlst_rec.trge_id) loop

       v_teller_wlst := v_teller_wlst + 1;

       v_y_char := is_letter(p_as                 => 'Y'
                            ,p_waarde             => v_y
                            ,p_trty_indicatie_ver => p_trty_ver_indicatie);

       INSERT INTO kgc_tray_vulling
       ( trge_id
       , meet_id
       , x_positie
       , y_positie
       , z_positie
       )
       VALUES
       ( wlst_rec.trge_id
       , wlst_meet_rec.meet_id
       , v_x
       , v_y_char
       , 0
       );

       -- opgeven in de hulptabel dat dit record verwerkt is, dan houdt de cursor daar rekening mee.
       update beh_hlp_werklijst
       set   verwerkt = 'J'
       where session_id  = c_session_id
       and   meet_id     = wlst_meet_rec.meet_id
       ;

       -- synchroniseer de werklijst
       werklijst
       ( p_meet_id          => wlst_meet_rec.meet_id
       , p_actie            => 'I'
       , p_werklijst_nummer => wlst_meet_rec.werklijst_nummer
       );

       v_y := v_y + 1;
       -- Als verticaal helemaal vol zit, dan moet horizontaal met 1 opgehoogd worden.
       -- Je moet dan met de volgende regel verdergaan met vullen.
       IF ( v_y > p_trty_ver_num_totaal )
       THEN
         v_y := 1;
         v_x := v_x + 1;
       END IF;
       EXIT WHEN ( v_x > p_trty_hor_num_totaal AND v_y > p_trty_ver_num_totaal );

     end loop;


     if v_teller_wlst > 0 then
       gebruikers_info( p_tekst => 'Werklijst: '||wlst_rec.werklijst_nummer||' aangemaakt.');
     end if;

   end loop;

EXCEPTION
  WHEN OTHERS
  THEN
    rollback;
    qms$errors.unhandled_exception('beh_dna_voortest_wlst_aanmaken.vul_tray');

END;

function maak_werklijst_trgr
 (P_PROTOCOL            IN VARCHAR2
 ,P_KAFD_ID             IN NUMBER
 ,P_TRTY_ID             IN NUMBER
 ,P_TECH_ID             IN NUMBER
 ,P_F_STATUS            IN VARCHAR2
 ,P_ONDERZOEKSSTATUS    IN VARCHAR2
 ,P_MEETWAARDEN_OP_TRAY IN NUMBER
, P_TRAY_GRD            IN VARCHAR2
, P_VOL_WLST            IN VARCHAR2
, P_NUMMERST            IN VARCHAR2
) return boolean
 is
/***********************************************************************
Auteur/bedrijf  : Y.Arts, YA IT-Services
Datum onderhoud : 07-04-2012

DOEL
Per protocol moet een werklijst aangemaakt worden. Een protocol heeft minimaal 1 werklijst
maar dit kunnen er ook meerdere zijn, afhankelijk van hoeveel stoftesten er geselecteerd worden.
Er kunnen maar 96 stoftesten op een WELL van 96 en de laatste is altijd een MQ stoftest.
Wanneer een werklijst meerdere WELLS bevat dan worden het meerdere werklijsten. Een stoftest op
een werklijst wordt ALTIJD afgesloten met een MQ echter, ook als deze verdergaat op een volgend WELL.

1. Selecteer de stoftesten op basis van de ingevoerde parameters
2. Bepaal de naam van de werklijst  KGCNUST01: code = WERK
   ** REFERENTIE -> BEHEER -> WAARDEN -> MEETWAARDE STRUCTUUR
   ** zie systeemparameter LETTER_WERK
3. Zet de stoftesten op de werklijst. Zodra de tray vol zit ga je verder met een volgend werklijst.
   Invoer in tabel KGC_TRAY_GEBRUIK

INPUT PARAMETERS
P_PROTOCOL   : Omschrijving van een stoftestgroep (protocol)
P_KAFD_ID             : Unieke identificatie van een afdeling
P_TRTY_ID             : Unieke identificatie van een tray type
P_TECH_ID             : Unieke identificatie van een techniek
P_ONDERZOEKSSTATUS    : Geeft de status van een onderzoek weer
P_ONDERZOEKSWIJZE     : Geeft de wijze van onderzoeken weer: Scanning of Screening
P_MEETWAARDEN_OP_TRAY : max aantal meetwaarden op de gekozen tray
P_TRAY_GRD            : De te gebruiken tray grid

OUTPUT PARAMETERS

TABELLEN
BAS_TRAY_MEETWAARDEN_VW
KGC_TRAY_GEBRUIK
BEH_HLP_TRAY_GEBRUIK
BEH_HLP_WERKLIJST
BEH_HLP_WERKLIJST_STOFTESTEN
BEH_HLP_WERKLIJST_MEETWAARDE
BEH_HLP_STORAGE
BEH_HLP_STORAGE_UITVAL

HISTORIE

Wanneer        Wie             Wat
------------------------------------------------------------------------
06-05-2012     Y.Arts          Creatie
1.1.0
***********************************************************************/

  cursor totaal_cur (b_kafd_id           in number
                    ,b_tech_id           in number
                    ,b_f_status          in varchar2
                    ,b_protocol in varchar2
                    ,b_onderzoeksstatus  in varchar2
                    )
   is
   select sum(subtotaal) from
     (select count(1)+1 subtotaal
      from   bas_tray_meetwaarden_vw tmwv
      ,      kgc_onderzoeken onde
      ,      beh_hlp_storage ste
      where  ste.id = tmwv.fractienummer
      and    ste.id not in (select sul.id
                            from beh_hlp_storage_uitval sul)
      and    ste.available = ste.available
      and    (tmwv.kafd_id = b_kafd_id
              and ( not exists ( select null from kgc_tray_vulling trvu
                                 where trvu.meet_id = tmwv.meet_id
                               )
                  )
             )
      and    tmwv.frac_status     = b_f_status
      and    tmwv.onderzoekswijze = 'A'
      and    tmwv.tech_id         = b_tech_id
      and    tmwv.stof_id not in (select stof_id from beh_stof_niet_op_werklijst)
      and    tmwv.stgr_id         in (select stgr_id
                                      from   kgc_stoftestgroepen
                                      where  kafd_id      = b_kafd_id
                                      and    vervallen    = 'N'
                                      and    instr(upper(omschrijving),upper(b_protocol)) > 0)
      and    tmwv.onde_id         = onde.onde_id
      and    onde.status          = b_onderzoeksstatus
      group by stof_id) totaal
      ;

   cursor frac_cur (b_kafd_id           in number
                   ,b_tech_id           in number
                   ,b_f_status          in varchar2
                   ,b_protocol in varchar2
                   ,b_onderzoeksstatus  in varchar2
                   )
   is

   select tmwv.fractienummer
   ,      count(1)+1 subtotaal
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage ste
   where  ste.id = tmwv.fractienummer
   and    ste.id not in (select sul.id
                         from beh_hlp_storage_uitval sul)
   and    ste.available = ste.available
   and    (tmwv.kafd_id = b_kafd_id  -- 1
           and ( not exists ( select null from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status--'O'
   and    tmwv.onderzoekswijze = 'A'
   and    tmwv.tech_id         = b_tech_id -- 6
   and    tmwv.stof_id not in (select stof_id from beh_stof_niet_op_werklijst)
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0) --'Ready to use plates')
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus --'G'
   group by tmwv.fractienummer
   order by tmwv.fractienummer asc
   ;

   cursor meet_cur (b_kafd_id           in number
                   ,b_tech_id           in number
                   ,b_f_status          in varchar2
                   ,b_protocol in varchar2
                   ,b_onderzoeksstatus  in varchar2
                   ,b_fractienummer     in varchar2)
   is

   select meet_id, stof_id
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage ste
   where  ste.id = tmwv.fractienummer
   and    tmwv.fractienummer = b_fractienummer
   and    ste.id not in (select sul.id
                         from beh_hlp_storage_uitval sul)
   and    (tmwv.kafd_id = b_kafd_id --1
           and ( not exists ( select null
                              from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status --'O'
   and    tmwv.onderzoekswijze = 'A'
   and    tmwv.tech_id         = b_tech_id --6
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id --1
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0) --'Ready to use plates')
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus --'G'
   group by meet_id, stof_id
   order by stof_id
   ;

   cursor storage_cur (b_kafd_id           in number
                      ,b_tech_id           in number
                      ,b_f_status          in varchar2
                      ,b_protocol in varchar2
                      ,b_onderzoeksstatus  in varchar2
                      )
   is
   select tmwv.fractienummer, onde.onde_id
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   where  not exists (select ste.id
                      from   beh_hlp_storage ste
                      where  ste.id = tmwv.fractienummer
                     )
   and    (tmwv.kafd_id = b_kafd_id
           and ( not exists ( select null from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = 'A'
   and    tmwv.tech_id         = b_tech_id
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0)
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus
   group by tmwv.fractienummer, onde.onde_id
   order by tmwv.fractienummer asc
   ;

   cursor storage_cur2 (b_kafd_id           in number
                       ,b_tech_id           in number
                       ,b_f_status          in varchar2
                       ,b_protocol in varchar2
                       ,b_onderzoeksstatus  in varchar2
                       )
   is
   select sul.id fractienummer
   from   bas_tray_meetwaarden_vw tmwv
   ,      kgc_onderzoeken onde
   ,      beh_hlp_storage_uitval sul
   where  sul.id = tmwv.fractienummer
   and    (tmwv.kafd_id = b_kafd_id
           and ( not exists ( select null from kgc_tray_vulling trvu
                              where trvu.meet_id = tmwv.meet_id
                            )
               )
          )
   and    tmwv.frac_status     = b_f_status
   and    tmwv.onderzoekswijze = 'A'
   and    tmwv.tech_id         = b_tech_id
   and    tmwv.stgr_id         in (select stgr_id
                                   from   kgc_stoftestgroepen
                                   where  kafd_id      = b_kafd_id
                                   and    vervallen    = 'N'
                                   and    instr(upper(omschrijving),upper(b_protocol)) > 0)
   and    tmwv.onde_id         = onde.onde_id
   and    onde.status          = b_onderzoeksstatus
   group by sul.id
   order by sul.id asc
   ;

   cursor frac_hlp_grp_cur
   is
   select fractienummer
   from   beh_hlp_werklijst_fracties
   where  session_id = c_session_id
   ;

   cursor stof_hlp_cur (b_fractienummer in varchar2)
   is

   select stof_id
   ,      count(1)+1 aantal
   from   beh_hlp_werklijst_meetwaarde
   where  1=1
   and    session_id    = c_session_id
   and    fractienummer = b_fractienummer
   and    verwerkt      = 'N'
   group by stof_id
   ;

   cursor meet_hlp_cur (b_fractienummer in varchar2
                       ,b_stof_id in number)
   is

   select stof_id
   ,      meet_id
   ,      meet_seq
   ,      fractienummer
   ,      mq
   from   beh_hlp_werklijst_meetwaarde
   where  1=1
   and    session_id    = c_session_id
   and    fractienummer = b_fractienummer
   and    stof_id       = b_stof_id
   and    verwerkt      = 'N'
   order by meet_seq
   ;

   cursor trvu_cur1(b_tray_grd in varchar2
                   ,b_meet_id  in number)
   is
   select x_positie
   ,      y_positie
   from   bas_meetwaarden meet
   ,      kgc_stoftesten  stof
   ,      kgc_sub_stoftesten stof_sub
   ,      kgc_tray_grid_vulling tgvu
   ,      kgc_tray_grids trgr
   ,      bas_tray_meetwaarden_vw tmwv
   where  tmwv.meet_id            = meet.meet_id
   and    meet.stof_id            = stof_sub.stof_id_sub
   and    stof_sub.stof_id_super  = stof.stof_id
   and    trgr.trgr_id            = tgvu.trgr_id
   and    upper(stof.omschrijving) like substr(tgvu.inhoud,3,length(tgvu.inhoud)-3)
   and    substr(tgvu.inhoud,1,1) = 'S'
   and    trgr.code               = b_tray_grd
   and    meet.meet_id            = b_meet_id
   and    tmwv.stan_id is not null
   ;

   cursor trvu_cur2(b_tray_grd  in varchar2
                   ,b_trge_id   in number
                   ,b_x_positie in number
                   ,b_y_positie in varchar2)
   is

   select 1
   from   beh_hlp_werklijst_mq wlst
   ,      bas_meetwaarden meet
   ,      kgc_stoftesten  stof
   ,      kgc_sub_stoftesten stof_sub
   ,      kgc_tray_grid_vulling tgvu
   ,      kgc_tray_grids trgr
   ,      bas_tray_meetwaarden_vw tmwv
   where  1=1
   and    session_id              = c_session_id
   and    wlst.meet_id            = meet.meet_id
   and    tmwv.meet_id            = meet.meet_id
   and    meet.stof_id            = stof_sub.stof_id_sub
   and    stof_sub.stof_id_super  = stof.stof_id
   and    trgr.trgr_id            = tgvu.trgr_id
   and    upper(stof.omschrijving) like substr(tgvu.inhoud,3,length(tgvu.inhoud)-3)
   and    substr(tgvu.inhoud,1,1) = 'S'
   and    tmwv.stan_id is not null
   and    trgr.code               = b_tray_grd
   and    wlst.trge_id            = b_trge_id
   and    x_positie               = b_x_positie
   and    y_positie               = b_y_positie
   ;

   v_werklijst_nummer    kgc_tray_gebruik.werklijst_nummer%type;
   v_trge_id             kgc_tray_gebruik.trge_id%type;
   v_meet_seq            number;
   v_teller_meet         number;
   v_totaal_aantal       number;
   v_personen_bij_onderzoek number;
   v_x_positie           number;
   v_y_positie           varchar2(2);
   v_positie_gevuld      number;
   v_fractienummer       varchar2(100);
   v_available           varchar2(10);

   -- Standaard de return waarde op false zetten. Mocht blijken dat een tray gemaakt kan worden dan wordt het daar op TRUE gezet.
   v_return              boolean := false;
   v_aantal_op_werklijst number := 0;
   v_nog_een_tray_zelfde_cyto boolean := false;

BEGIN

  -- De volgende stoftesten konden niet op de werklijst gezet worden omdat er geen storage van bekend is
  for storage_rec  in  storage_cur(b_kafd_id           => p_kafd_id
                                  ,b_tech_id           => p_tech_id
                                  ,b_f_status          => p_f_status
                                  ,b_protocol => P_PROTOCOL
                                  ,b_onderzoeksstatus  => p_onderzoeksstatus )  loop
    gebruikers_info( p_tekst => 'Fractie '||storage_rec.fractienummer||' kon niet op een werklijst gezet worden omdat er geen storage van bekend is. C_01, C_02');
  end loop;

  -- De volgende fracties komen ofwel in meerdere koelkasten voor ofwel meerdere keren geregistreerd binnen 1 koelkast
  for storage_rec2  in  storage_cur2(b_kafd_id           => p_kafd_id
                                    ,b_tech_id           => p_tech_id
                                    ,b_f_status          => p_f_status
                                    ,b_protocol => P_PROTOCOL
                                    ,b_onderzoeksstatus  => p_onderzoeksstatus )  loop
    gebruikers_info( p_tekst => 'Fractie '||storage_rec2.fractienummer||' komt meerdere keren voor binnen de Storage. Deze zal niet op de werklijst gezet kunnen worden.');
  end loop;


  -- bepaal het totaal aantal stoftesten die op werklijsten gezet moet worden.
  open totaal_cur(b_kafd_id           => p_kafd_id
                 ,b_tech_id           => p_tech_id
                 ,b_f_status          => p_f_status
                 ,b_protocol => P_PROTOCOL
                 ,b_onderzoeksstatus  => p_onderzoeksstatus );
  fetch totaal_cur into v_totaal_aantal;
  close totaal_cur;

  -- Vul eerst de hulptabellen om daarna sneller de echte tabellen te kunnen vullen
  if v_totaal_aantal > 0 then

    for frac_rec in frac_cur(b_kafd_id           => p_kafd_id
                            ,b_tech_id           => p_tech_id
                            ,b_f_status          => p_f_status
                            ,b_protocol => P_PROTOCOL
                            ,b_onderzoeksstatus  => p_onderzoeksstatus ) loop

      insert into beh_hlp_werklijst_fracties
      (session_id
      ,fractienummer
      ,verwerkt
      ) values
      (c_session_id
      ,frac_rec.fractienummer
      ,'N'
      );

      v_meet_seq := 1;

      -- doorloop de fracties/meetwaarden en zet deze op de tray
      for meet_rec in meet_cur(b_kafd_id           => p_kafd_id
                              ,b_tech_id           => p_tech_id
                              ,b_f_status          => p_f_status
                              ,b_protocol => P_PROTOCOL
                              ,b_onderzoeksstatus  => p_onderzoeksstatus
                              ,b_fractienummer     => frac_rec.fractienummer ) loop
        insert into beh_hlp_werklijst_meetwaarde
        (session_id
        ,stof_id
        ,meet_seq
        ,meet_id
        ,fractienummer
        ,verwerkt
        ,available
        ) values
        (c_session_id
        ,meet_rec.stof_id
        ,v_meet_seq
        ,meet_rec.meet_id
        ,frac_rec.fractienummer
        ,'N'
        ,'C_01'
        );

        v_meet_seq := v_meet_seq + 1;

      end loop;

      update beh_hlp_werklijst_fracties
      set    verwerkt      = 'J'
      where  fractienummer = frac_rec.fractienummer
      and    session_id    = c_session_id
      ;

    end loop;

  end if; -- einde hulptabellen vullen

  -- als het totaal aantal groter dan 0 is moeten er stoftesten op tray gezet worden
  while v_totaal_aantal > 0  loop  -- zet na verwerking van een stoftest v_totaal_aantal := v_totaal_aantal - verwerkte_fracties

    if p_vol_wlst = 'J' and v_totaal_aantal < p_meetwaarden_op_tray then
        -- zorg ervoor dat uit de loop gesprongen wordt. Zet v_totaal_aantal gedwongen op 0.
      v_totaal_aantal := 0;
      exit;

    else

      for frac_hlp_grp_rec in frac_hlp_grp_cur loop

        -- als parameter p_vol_wlst = J dan alleen volle werklijsten aanmaken
        if p_vol_wlst = 'J' and v_totaal_aantal < p_meetwaarden_op_tray then
          -- zorg ervoor dat uit de loop gesprongen wordt. Zet v_totaal_aantal gedwongen op 0.
          v_totaal_aantal := 0;
          exit;
        else

          v_nog_een_tray_zelfde_cyto := true;

          while v_nog_een_tray_zelfde_cyto loop

            v_nog_een_tray_zelfde_cyto := false;

            -- Werklijstnummer genereren
            v_werklijst_nummer := kgc_nust_00.genereer_nr
                                    ( p_nummertype => p_nummerst
                                    , p_ongr_id    => NULL
                                    , p_kafd_id    => p_kafd_id
                                    );

            -- Ready To Use Plates krijgen andere werklijst nummer dan andere ROBOT werklijsten
            v_werklijst_nummer := substr(v_werklijst_nummer,1,11)||'R2U';

            select kgc_trge_seq.nextval into v_trge_id from dual;

            -- output voor de gebruiker, welke werklijst aangemaakt is
            dbms_output.put_line ('Werklijst: '||v_werklijst_nummer);

            -- Maak een TRAY GEBRUIK aan in de tabel KGC_TRAY_GEBRUIK
            insert into kgc_tray_gebruik
            (trge_id
            ,mede_id
            ,tech_id
            ,trty_id
            ,datum
            ,gefixeerd
            ,werklijst_nummer
            )
             values
            (v_trge_id
            ,kgc_mede_00.medewerker_id
            ,p_tech_id
            ,p_trty_id
            ,sysdate
            ,'J'
            ,v_werklijst_nummer
            );

            -- Vul de hulptabel met tray gebruik
            insert into beh_hlp_tray_gebruik
            (session_id
            ,trge_id
            ,werklijst_nummer
            ,available
            )
            values
            (c_session_id
            ,v_trge_id
            ,v_werklijst_nummer
            ,'C_01'
            );

            v_teller_meet := 0;

            for stof_hlp_rec in stof_hlp_cur(b_fractienummer => frac_hlp_grp_rec.fractienummer) loop

              -- doorloop de fracties/meetwaarden en zet deze op de tray
              for meet_hlp_rec in meet_hlp_cur(b_fractienummer => frac_hlp_grp_rec.fractienummer
                                              ,b_stof_id       => stof_hlp_rec.stof_id  ) loop

                v_teller_meet   := v_teller_meet + 1;
                v_totaal_aantal := v_totaal_aantal - 1;

                -- meetwaarden op tray zetten, huidige werklijst.
                insert into beh_hlp_werklijst
                (session_id
                ,meet_id
                ,werklijst_nummer
                ,trge_id
                ,sorteer_nummer
                ,available
                ) values
                (c_session_id
                ,meet_hlp_rec.meet_id
                ,v_werklijst_nummer
                ,v_trge_id
                ,v_teller_meet
                ,'C_01'
                );

                update beh_hlp_werklijst_meetwaarde
                set   verwerkt = 'J'
                where session_id  = c_session_id
                and   stof_id     = stof_hlp_rec.stof_id
                and   meet_id     = meet_hlp_rec.meet_id
                ;

              end loop;

            end loop;

            select count(1) into v_aantal_op_werklijst from beh_hlp_werklijst where werklijst_nummer = v_werklijst_nummer;

            if v_teller_meet = 0 or (v_aantal_op_werklijst < p_meetwaarden_op_tray and p_vol_wlst = 'J')
            then
              delete beh_hlp_werklijst    where werklijst_nummer = v_werklijst_nummer;
              delete kgc_tray_gebruik     where werklijst_nummer = v_werklijst_nummer;
              delete beh_hlp_tray_gebruik where werklijst_nummer = v_werklijst_nummer;
            else
               -- Als een werklijst gemaakt kan worden dan doorgaan met tray vullen, dus TRUE retourneren
               v_return := true;
            end if;

          end loop;


        end if;

      end loop;

    end if;

  end loop;

  return v_return;




EXCEPTION
  WHEN OTHERS
  THEN
   rollback;
    qms$errors.unhandled_exception('beh_dna_voortest_wlst_aanmaken.maak_werklijst_trgr');

END;

PROCEDURE vul_tray_trgr
 ( P_TRAY_GRD IN VARCHAR2
 )
 IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts, YA IT-Services
Datum onderhoud : 10-04-2012

DOEL
De stoftesten van een protocol moeten op een tray gezet worden.
Iedere tray wordt een aparte werklijst.

1. Selecteer de stoftesten die op de tray gezet moeten worden ( pl/sql tabel BEH_HLP_WERKLIJST )
2. Bij het aanmaken van de werklijsten is al bepaald welke meetwaarden op een tray gezet worden.
   We doorlopen gewoon de unieke tray gebruiken in cursor WLST_CUR.
3. Vervolgens doorlopen we per tray gebruik de geselecteerde meetwaarden mbv cursor WLST_MEET_CUR.
4. Voer de meetwaarden op in tabel KGC_TRAY_VULLING
5. Houdt rekening met het type tray en hoeveel posities deze heeft.
6. Vul email variabele met de aangemaakte werklijsten

-- Zie de functie 'PLAATS' in KGCTRVU01.
-- Plaatsing is VERTICAAL
-- Vanaf A - 1
-- Maak per tray een werklijst aan. Zie ook KGCTRVU01 de functie 'MAAK'
   ** Zie KGCIDEF01 via het menu REFERENTIE -> BEHEER -> INTERFACE -> INTERFACE DEFINITIE
   ** De code moet zijn 'ROBOT_DNA'
   ** Neem deze structuur over en vul de tray
-- Maak een Robot tekstbestand aan met de vulling van de tray en zet deze op een locatie.
   ** Zie KGCIDEF12 Robotstraat -> Automatisch


INPUT PARAMETERS
P_TRAY_GRD   : de te gebruiken Tray Grid uit kgc_tray_grids

HISTORIE

Wanneer/        Wie             Wat
Versie
------------------------------------------------------------------------
07-04-2012      Y.Arts          Creatie
1.1.0
***********************************************************************/

  -- selecteer de unieke tray gebruik nummers
  cursor wlst_cur
  is

  select trge_id, werklijst_nummer
  from   beh_hlp_werklijst
  where  1=1
  and    session_id = c_session_id
  group by trge_id, werklijst_nummer
  ;

  -- selecteer per tray gebruik nummer de meetwaarden die op de tray gezet moeten worden
  cursor wlst_meet_cur (b_trge_id in number)
  is

  select wlst.meet_id
  ,      wlst.werklijst_nummer
  from   beh_hlp_werklijst wlst
  where  1=1
  and    session_id = c_session_id
  and    wlst.verwerkt = 'N'
  and    wlst.trge_id  = b_trge_id
  order by wlst.trge_id, wlst.sorteer_nummer asc
  ;

  -- selecteer per tray gebruik nummer de meetwaarden die op de tray gezet moeten worden
  cursor wlst_tgvu_cur (b_trge_id in number
                       ,b_tray_grd in varchar2)
  is

  select wlst.meet_id
  ,      wlst.werklijst_nummer
  ,      x_positie
  ,      y_positie
  ,      tmwv.stan_id
  from   beh_hlp_werklijst wlst
  ,      bas_meetwaarden meet
  ,      kgc_stoftesten  stof
  ,      kgc_tray_grid_vulling tgvu
  ,      kgc_tray_grids trgr
  ,      bas_tray_meetwaarden_vw tmwv
  where  1=1
  and    session_id = c_session_id
  and    wlst.meet_id = meet.meet_id
  and    tmwv.meet_id = meet.meet_id
  and    meet.stof_id = stof.stof_id
  and    trgr.trgr_id = tgvu.trgr_id
  and    trgr.code    = b_tray_grd
  and    upper(stof.omschrijving) like substr(tgvu.inhoud,3,length(tgvu.inhoud)-3)
  and    wlst.verwerkt = 'N'
  and    wlst.trge_id  = b_trge_id
  and    tmwv.stan_id is null
  union all
  select wlst.meet_id
  ,      wlst.werklijst_nummer
  ,      x_positie
  ,      y_positie
  ,      tmwv.stan_id
  from   beh_hlp_werklijst wlst
  ,      bas_meetwaarden meet
  ,      kgc_stoftesten  stof
  ,      kgc_sub_stoftesten stof_sub
  ,      kgc_tray_grid_vulling tgvu
  ,      kgc_tray_grids trgr
  ,      bas_tray_meetwaarden_vw tmwv
  where  1=1
  and    session_id = c_session_id
  and    wlst.meet_id = meet.meet_id
  and    tmwv.meet_id = meet.meet_id
  and    meet.stof_id = stof_sub.stof_id_sub
  and    stof_sub.stof_id_super = stof.stof_id
  and    trgr.trgr_id = tgvu.trgr_id
  and    trgr.code    = b_tray_grd
  and    upper(stof.omschrijving) like substr(tgvu.inhoud,3,length(tgvu.inhoud)-3)
  and    substr(tgvu.inhoud,1,1) = 'S'
  and    wlst.verwerkt = 'N'
  and    wlst.trge_id  = b_trge_id
  and    tmwv.stan_id is not null
  ;

  cursor stof_cur (b_trge_id in number)
  is

  select wlst.meet_id
  ,      wlst.werklijst_nummer
  ,      stof.omschrijving
  from   beh_hlp_werklijst wlst
  ,      bas_meetwaarden meet
  ,      kgc_stoftesten  stof
  where  1=1
  and    session_id    = c_session_id
  and    wlst.meet_id  = meet.meet_id
  and    meet.stof_id  = stof.stof_id
  and    wlst.verwerkt = 'N'
  and    wlst.trge_id  = b_trge_id
  order by wlst.trge_id, wlst.sorteer_nummer asc
  ;

  cursor trvu_cur (b_stof_omschrijving in varchar2
                  ,b_tray_grd in varchar2)
  is
  select 1
  from   kgc_tray_grid_vulling tgvu
  ,      kgc_tray_grids trgr
  where  trgr.trgr_id = tgvu.trgr_id
  and    trgr.code    = b_tray_grd
  and    upper(b_stof_omschrijving) like substr(tgvu.inhoud,3,length(tgvu.inhoud)-3)
  ;

  cursor trvu_cur2 (b_trge_id in number
                   ,b_x_positie in number
                   ,b_y_positie in varchar2 )
  is
  select nvl(max(z_positie),0)+1
  from kgc_tray_vulling
  where trge_id = b_trge_id
  and   x_positie = b_x_positie
  and   y_positie = b_y_positie
  ;

  v_teller_wlst  NUMBER;
  v_trvu_dummy   NUMBER;
  v_z_positie    NUMBER;

  procedure werklijst
  ( p_meet_id          IN NUMBER
  , p_actie            IN VARCHAR2
  , p_werklijst_nummer IN VARCHAR2
  )
  IS
  begin
    kgc_wlst_00.sync_tray_werklijst
    ( p_actie            => p_actie
    , p_meet_id          => p_meet_id
    , p_werklijst_nummer => p_werklijst_nummer
    );
  end;


BEGIN

dbms_output.put_line('vul tray');

   -- Doorloop de unieke werklijsten/tray gebruik.
   -- Bij ieder tray moet opnieuw begonnen worden met tellen van de wells
   for wlst_rec in wlst_cur loop

     v_teller_wlst := 0;

dbms_output.put_line('trge_id '||wlst_rec.trge_id);

     -- eerst aangeven welke stoftest omschrijvingen niet gevonden kunnen worden
     for stof_rec in stof_cur (b_trge_id => wlst_rec.trge_id) loop

dbms_output.put_line('stoftest kan niet gevonden worden '||stof_rec.omschrijving);

       open trvu_cur (b_stof_omschrijving => stof_rec.omschrijving
                     ,b_tray_grd          => p_tray_grd);
       fetch trvu_cur into v_trvu_dummy;

       if trvu_cur%notfound then

         -- meetwaarde kon niet op tray gezet worden, geen coordinaten bekend in tray grid
         gebruikers_info( p_tekst => 'Tray grid vulling niet gevonden voor stoftest: '||stof_rec.omschrijving);
         dbms_output.put_line ('Tray grid vulling niet gevonden voor stoftest: '||stof_rec.omschrijving);

       end if;
       close trvu_cur;

     end loop;

dbms_output.put_line('meetwaarden op tray zetten ');

     -- meetwaarden op tray zetten
     for wlst_tgvu_rec IN wlst_tgvu_cur(b_trge_id  => wlst_rec.trge_id
                                       ,b_tray_grd => p_tray_grd) loop

       v_teller_wlst := v_teller_wlst + 1;

       if wlst_tgvu_rec.stan_id is not null then
         open trvu_cur2(b_trge_id   => wlst_rec.trge_id
                       ,b_x_positie => wlst_tgvu_rec.x_positie
                       ,b_y_positie => wlst_tgvu_rec.y_positie);
         fetch trvu_cur2 into v_z_positie;
         close trvu_cur2;
       end if;

       INSERT INTO kgc_tray_vulling
       ( trge_id
       , meet_id
       , x_positie
       , y_positie
       , z_positie
       )
       VALUES
       ( wlst_rec.trge_id
       , wlst_tgvu_rec.meet_id
       , wlst_tgvu_rec.x_positie
       , wlst_tgvu_rec.y_positie
       , nvl(v_z_positie,0)
       );

       v_z_positie := null;

       -- opgeven in de hulptabel dat dit record verwerkt is, dan houdt de cursor daar rekening mee.
       update beh_hlp_werklijst
       set   verwerkt = 'J'
       where session_id  = c_session_id
       and   meet_id     = wlst_tgvu_rec.meet_id
       ;

       -- synchroniseer de werklijst
       werklijst
       ( p_meet_id          => wlst_tgvu_rec.meet_id
       , p_actie            => 'I'
       , p_werklijst_nummer => wlst_tgvu_rec.werklijst_nummer
       );

     end loop;

     if v_teller_wlst > 0 then

       gebruikers_info( p_tekst => 'Werklijst: '||wlst_rec.werklijst_nummer||' aangemaakt.');
       dbms_output.put_line ('Werklijst: '||wlst_rec.werklijst_nummer||' aangemaakt.');

     end if;

   end loop;

EXCEPTION
  WHEN OTHERS
  THEN
    rollback;
    qms$errors.unhandled_exception('beh_dna_voortest_wlst_aanmaken.vul_tray_trgr');

END;


PROCEDURE etiket_interface_bestand
(p_kafd_code in varchar2
,p_kafd_id   in number
)
IS
/***********************************************************************
Auteur/bedrijf : Y.Arts, YA IT-Services
Datum onderhoud : 06-05-2012

DOEL
Per werklijst moet een etiket aangemaakt worden. Dit gebeurt door een tekst bestand
aan te maken dat op een locatie op de server gezet wordt.

Deze functie is af te leiden van het handmatige proces in de schermen KGCETIK01 via UITVOER -> ETIKETTEN

1. Verwijder eventueel openstaand Oracle directory zodat geen foutmelding ontstaat bij het creeren ervan
2. Maak de Oracle directory aan voor de uitvoerlocatie. Deze locatie staat in de interface definitie.
   Oracle moet wel toegang hebben tot deze directory.
3. Controleer of Oracle het uitvoerbestand mag aanmaken
4. Open het uitvoerbestand
5. Doorloop de etiket werklijst gegevens en voeg deze 1 voor 1 toe aan het uitvoerbestand
6. Sluit het uitvoerbestand

INPUT PARAMETERS
p_kafd_code  : de code van de opgegeven afdeling
p_kafd_id    : het ID van de opgegeven afdeling


HISTORIE

Wanneer        Wie             Wat
------------------------------------------------------------------------
06-05-2012     Y.Arts          Creatie
1.0.0
***********************************************************************/

  cursor wlst_cur (b_kafd_code in varchar2)
  is
  select wlst.*, bhwt.available
  from   kgc_etiket_wlst_vw wlst
  ,      (select distinct werklijst_nummer, available
          from beh_hlp_werklijst
          where session_id = c_session_id) bhwt
  where  kafd_code             = b_kafd_code
  and    upper(type_etiket)    = 'WLST'
  and    wlst.werklijst_nummer = bhwt.werklijst_nummer
  ;

  CURSOR etpa_cur
  ( b_kafd_id IN NUMBER
  )
  IS
    SELECT etpa.locatie  --  was  \\umcukz13\Data$\DNA\Robot\
    ,      etpa.kop
    ,      etpa.omsluitteken
    ,      etpa.scheidingsteken
    ,      etpa.regel
    ,      etpa.omschrijving
    FROM   kgc_etiket_parameters etpa
    WHERE  etpa.kafd_id     = b_kafd_id
    AND    etpa.type_etiket = 'WLST'
    AND    etpa.vervallen   = 'N'
    ORDER BY
           (SELECT etvo.creation_date
            FROM   kgc_medewerkers       mede
            ,      kgc_etiket_voorkeuren etvo
            WHERE  etvo.etpa_id = etpa.etpa_id
            AND    etvo.mede_id = mede.mede_id
            AND    mede.oracle_uid = user) desc nulls last,
           (SELECT code
            FROM   kgc_onderzoeksgroepen ongr
            WHERE  ongr.ongr_id = etpa.ongr_id) asc,
           code asc
    ;


  etpa_rec etpa_cur%rowtype;
  out_file utl_file.file_type;

  v_regel            varchar2(1000);
  v_omsluitteken     varchar2(1);
  v_scheidingsteken  varchar2(1);
  v_regelstructuur   varchar2(2000);
  v_lijst            varchar2(4000);
  v_gevonden         boolean := false;
  v_aantal           number;
  v_bestandsnaam     varchar2(100);
  v_werklijst_nummer varchar2(20);
  v_statement        varchar2(4000);
  v_statement_client varchar2(4000);
  v_client_locatie   varchar2(100);


BEGIN

  -- Verwijder de Oracle directory 'clientdir' om fouten te voorkomen
  BEGIN
    v_statement := 'drop directory clientdir';
    execute immediate v_statement;
  EXCEPTION
    when others then
    null;
  END;

  -- Haal de etiketparameters op voor de werklijsten
  OPEN  etpa_cur( b_kafd_id => p_kafd_id );
  FETCH etpa_cur
  INTO  etpa_rec;

  v_gevonden := etpa_cur%FOUND;

  CLOSE etpa_cur;

  v_omsluitteken    := SUBSTR( etpa_rec.omsluitteken, 1, 1 );
  v_scheidingsteken := NVL( SUBSTR( etpa_rec.scheidingsteken, 1, 1 ), ';' );
  v_regelstructuur  := etpa_rec.regel;
  v_client_locatie  := RTRIM( etpa_rec.locatie, '\')||'\';--'\\umcukz13\dna$\yuri';--
  v_bestandsnaam    := 'WLST'||c_session_id||TO_CHAR( SYSDATE, 'hh24miss')||'.TXT';

  -- maak etiket directory van de client aan in Oracle
  v_statement_client := 'create directory clientdir as '||''''||v_client_locatie||'''';
  execute immediate v_statement_client;

  -- Controleer of het bestand aangemaakt kan worden op de locatie van de client
  controleer_bestand
  ( p_locatie    => 'CLIENTDIR'
  , p_bestand    => v_bestandsnaam
  , p_dbs_client => 'CLIENT'
  , p_rw         => 'w'
  );

  -- maak het bestand aan op de directory
  out_file := utl_file.fopen(location  => 'CLIENTDIR'
                            ,filename  => v_bestandsnaam
                            ,open_mode => 'w');

  -- schrijf de eerste regel naar het bestand
  utl_file.put_line(file   => out_file
                   ,buffer => etpa_rec.kop);

  -- Doorloop de aangemaakte werklijsten met stoftesten.
  FOR wlst_rec IN wlst_cur(b_kafd_code => p_kafd_code) LOOP

    v_regel := NULL;
    v_regel := kgc_etik_00.regel
                (  p_type            => 'WLST'
                 , p_id              => wlst_rec.wlst_id
                 , p_omsluitteken    => v_omsluitteken
                 , p_scheidingsteken => v_scheidingsteken
                 , p_regelstructuur  => v_regelstructuur
                );

    v_regel := v_regel || v_scheidingsteken || wlst_rec.available || v_scheidingsteken || TO_CHAR( NVL( v_aantal, 1 ) );

    utl_file.put_line(file   => out_file
                     ,buffer => v_regel);
    v_aantal := v_aantal + 1;

    BEGIN
      v_lijst := v_lijst||v_regel||CHR(10);
    EXCEPTION
    WHEN OTHERS
      THEN -- als max bereikt is dan mag hier zonder fout uitgegaan worden:
      NULL;
    END;

  END LOOP;

  -- sluit het bestand
  utl_file.fclose( file => out_file );

  IF ( utl_file.is_open( file => out_file ) )
  THEN
    utl_file.fclose( file => out_file );
  END IF;

  -- drop directories
  v_statement_client     := 'drop directory CLIENTDIR';
  execute immediate v_statement_client;

EXCEPTION
  WHEN OTHERS
  THEN
    rollback;
    IF ( utl_file.is_open( file => out_file ) )
    THEN
      utl_file.fclose( file => out_file );
    END IF;
    qms$errors.unhandled_exception('BEH_DNA_VOORTEST_WLST_AANMAKEN.etiket_interface_bestand');

END;


PROCEDURE START_WERKLIJST_TRAY_ETIKET
 (P_F_STATUS   IN VARCHAR2 -- OPGEWERKT 'O'
 ,P_O_STATUS   IN VARCHAR2 -- GOEDGEKEURD 'G'
 ,P_PROTOCOL   IN VARCHAR2 -- Kunnen meerdere protocols/stoftestgroepen zijn 'Ready to use plates'
 ,P_O_WIJZE    IN VARCHAR2 DEFAULT NULL -- SCANNING 'A', SCREENING 'S' of NULL
 ,P_TECHNIEK   IN VARCHAR2 -- PCR
 ,P_AFDELING   IN VARCHAR2 -- GENOOM
 ,P_TRTY       IN VARCHAR2 -- 96 WELLS , code van de tray meegeven
 ,P_EMAIL         IN VARCHAR2 DEFAULT 'dnapcrfacility@gen.umcn.nl' -- aangepast door MGO , was: 'HelpdeskHelixCUKZ@cukz.umcn.nl'
 ,P_TRAY_GRD   IN VARCHAR2 DEFAULT NULL
 ,P_VOL_WLST   IN VARCHAR2 DEFAULT 'J'
 ,P_NUMMERST   IN VARCHAR2 DEFAULT 'WERK'
 )
 IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 06-05-2012

DOEL
Start het aanmaken van werklijsten, trays en etiketten

0. Bepaal de huidige session id en bewaar deze in een constante variabele
1. Bepaal de afdelings ID voor verder gebruik in de package op basis van de ingevoerde variabelen
2. Haal de tray type waarden op basis van de ingevoerde variabelen
3. Bepaal de techniek ID voor verder gebruik in de package op basis van de ingevoerde variabelen
4. Loop door de geselecteerde protocollen (stoftestgroepen). Voer alles uit PER protocol
5. Maak per protocol 1 of meerdere werklijsten aan en zet deze in een tijdelijke tabel beh_hlp_WERKLIJST
6. Doorloop alle werklijsten en vul de trays

INPUT PARAMETERS
P_F_STATUS : FRACTIE_STATUS    : De status van de fractie zoals in bas_fracties, OPGEWERKT'
P_O_STATUS : ONDERZOEKSSTATUS  : De status van een onderzoek zoals in GOEDGEKEURD'
P_PROTOCOL : PROTOCOL_CODE     : Je kunt een of meerdere stoftestgroep codes opgeven
P_O_WIJZE  : ONDERZOEKSWIJZE   : De wijze van onderzoek: SCANNING of SCREENING
P_TECHNIEK : De techniek dat toegepast wordt:  PCR
P_AFDELING : De afdeling waarop de stoftesten uitgevoerd moeten worden:  GENOOM
P_TRTY     : Het type tray waarop de stoftesten moeten komen: 96 WELLS
P_EMAIL    : Output van de procedure wordt naar dit email adres verzonden
P_TRAY_GRD : De te gebruiken tray grid definitie

OUTPUT PARAMETERS


Wanneer/        Wie             Wat
Versie
------------------------------------------------------------------------
14-04-2015     Y.Arts          P_NUMMERST toegevoegd tbv ML werklijsten
1.2.2
06-05-2012     Y.Arts          Creatie
1.0.0
***********************************************************************/

   cursor kafd_cur (b_afdeling in varchar2)
   is
   select kafd_id
   from   kgc_kgc_afdelingen
   where  code = b_afdeling
   ;

   cursor trty_cur (b_tray_type in varchar2
                   ,b_kafd_id   in number)
   is
   select trty_id                       -- number
   ,      horizontaal   hor_num_totaal  -- number
   ,      indicatie_hor hor_indicatie   -- varchar2(1)
   ,      verticaal     ver_num_totaal  -- number
   ,      indicatie_ver ver_indicatie   -- varchar2(1)
   from   kgc_tray_types
   where  code    = b_tray_type
   and    kafd_id = b_kafd_id
   ;

   cursor tech_cur (b_tech_code in varchar2)
   is

   select tech_id
   from   kgc_technieken tech
   where  tech.code =  b_tech_code
   and    vervallen = 'N'
   ;

   -- selecteer per tray gebruik nummer de meetwaarden die op de tray gezet moeten worden
   cursor trgr_cur (b_tray_grd in varchar2)
   is
   select trgr.trgr_id
   from   kgc_tray_grid_vulling tgvu
   ,      kgc_tray_grids trgr
   where  trgr.trgr_id = tgvu.trgr_id
   and    trgr.code    = b_tray_grd
   ;

   v_return_maak_werklijst boolean := true;
   v_return_vul_tray       boolean := true;
   trty_rec                trty_cur%rowtype;
   v_kafd_id               KGC_KGC_AFDELINGEN.KAFD_ID%type;
   v_tech_id               KGC_TECHNIEKEN.TECH_ID%type;
   v_trgr_id               number;

  -- email
  v_onderwerp    varchar2(100) := 'Werklijst-Tray-Etiket';
  e_parameters   EXCEPTION;
  e_geen_protocollen EXCEPTION;
  e_parameters_onbekend EXCEPTION;
  v_totaal_aantal_protocollen number := 0;
BEGIN

 open  trgr_cur (b_tray_grd => p_tray_grd);
 fetch trgr_cur into v_trgr_id;
 close trgr_cur;

 gebruikers_info( p_tekst => 'Start procedure Werklijst-Tray-Etiket zonder MQ' );

  -- Controleer of de tray grid bestaat. Zo niet dan melding en afbreken.
  if p_tray_grd is not null and v_trgr_id is null then
    gebruikers_info( p_tekst => 'Tray grid code is onbekend of heeft geen vulling, procedure afgebroken');
  else

   SELECT SYS_CONTEXT ('USERENV', 'SESSIONID') into c_session_id FROM DUAL;

   -- maak de hulptabel eerst leeg
   delete beh_hlp_storage;

   -- maak de hulptabel eerst leeg
   delete beh_hlp_storage_uitval;

   -- maak de hulptabel eerst leeg, deze tabel bepaald welke stoftesten niet op de werklijst mogen omdat
   -- er geen 2 fracties zijn bij dezelfde onderzoek
   delete beh_stof_niet_op_werklijst;

   -- vul de hulptabel met de gegevens uit de Storage tabel
   insert into beh_hlp_storage (select id, available from storage@storage where available in ('C_01','C_02','C_03'));

   -- vul de hulptabel met de uitval gegevens uit de beheer Storage tabel. Dit zijn de dubbele voorkomens en mogen niet verwerkt worden
   insert into beh_hlp_storage_uitval (select ste.id from beh_hlp_storage ste where ste.id is not null group by ste.id having count(1) > 1);

   open  kafd_cur (b_afdeling => p_afdeling);
   fetch kafd_cur into v_kafd_id;
   close kafd_cur;

   if v_kafd_id is null then
     gebruikers_info( p_tekst => 'Opgegeven afdeling is onbekend, procedure afgebroken');
     raise e_parameters;
   end if;

   open  trty_cur (b_tray_type => p_trty
                  ,b_kafd_id   => v_kafd_id);
   fetch trty_cur into trty_rec;
   close trty_cur;

   if trty_rec.trty_id is null then
     gebruikers_info( p_tekst => 'Opgegeven tray is onbekend, procedure afgebroken');
     raise e_parameters;
   end if;

   gebruikers_info( p_tekst => 'De tray bevat '||trty_rec.hor_num_totaal * trty_rec.ver_num_totaal||' posities.' );

   open tech_cur (b_tech_code => p_techniek);
   fetch tech_cur into v_tech_id;
   close tech_cur;

   if v_tech_id is null then
     gebruikers_info( p_tekst => 'Opgegeven techniek is onbekend, procedure afgebroken');
     raise e_parameters;
   end if;

   begin
        gebruikers_info ( p_tekst => ' ');
        -- output voor de gebruiker, voor welk protocol werklijsten worden aangemaakt
        gebruikers_info( p_tekst => 'Protocol: '||p_protocol);

        if p_tray_grd is not null then
          -- zet de werklijsten met meet_id's in een hulp tabel BEH_HLP_WLST
          -- KGCTRGE01
          gebruikers_info( p_tekst => 'Tray Grid staat aan.');

          v_doorgaan := maak_werklijst_trgr(p_protocol   => p_protocol
                                  ,p_kafd_id             => v_kafd_id
                                  ,p_trty_id             => trty_rec.trty_id
                                  ,p_meetwaarden_op_tray => (trty_rec.hor_num_totaal * trty_rec.ver_num_totaal)
                                  ,p_tech_id             => v_tech_id
                                  ,p_onderzoeksstatus    => p_o_status
                                  ,p_f_status            => p_f_status
                                  ,p_tray_grd            => p_tray_grd
                                  ,p_vol_wlst            => p_vol_wlst
                                  ,p_nummerst            => p_nummerst
                                  );

          if v_doorgaan then
             -- Vul de trays op basis van de aangemaakte werklijsten : BEH_HLP_WERKLIJST
             -- KGCTRVU01
             vul_tray_trgr (p_tray_grd => p_tray_grd);
          else
             gebruikers_info( p_tekst => 'Geen tray gevuld voor '||p_protocol||', geen werklijst of etiket bestand maken.');
          end if;

        else

          -- zet de werklijsten met meet_id's in een hulp tabel BEH_HLP_WLST
          -- KGCTRGE01
          gebruikers_info( p_tekst => 'Tray Grid staat uit. Bepaal tray vulling op basis van sortering');

          v_doorgaan := maak_werklijst(p_protocol   => p_protocol
                             ,p_kafd_id             => v_kafd_id
                             ,p_trty_id             => trty_rec.trty_id
                             ,p_meetwaarden_op_tray =>(trty_rec.hor_num_totaal * trty_rec.ver_num_totaal)
                             ,p_tech_id             => v_tech_id
                             ,p_onderzoeksstatus    => p_o_status
                             ,p_onderzoekswijze     => p_o_wijze
                             ,p_f_status            => p_f_status
                             ,p_vol_wlst            => p_vol_wlst
                             ,p_nummerst            => p_nummerst
                             );

           if v_doorgaan then
             -- Vul de trays op basis van de aangemaakte werklijsten : BEH_HLP_WERKLIJST
             -- KGCTRVU01
             vul_tray (p_trty_hor_indicatie  => trty_rec.hor_indicatie
                      ,p_trty_ver_indicatie  => trty_rec.ver_indicatie
                      ,p_trty_hor_num_totaal => trty_rec.hor_num_totaal
                      ,p_trty_ver_num_totaal => trty_rec.ver_num_totaal
                      );
           else
             gebruikers_info( p_tekst => 'Geen tray gevuld voor '||p_protocol||', geen werklijst of etiket bestand maken.');
           end if;

        end if;

        -- Maak een interface bestand aan voor de werklijsten
        -- KGCIDEF12
        werklijst_interface(p_kafd_id => v_kafd_id);

        -- Maak etiketten aan voor de trays
        etiket_interface_bestand(p_kafd_id   => v_kafd_id
                                ,p_kafd_code => p_afdeling);

        -- wijzigingen opslaan en doorgaan met de volgende
        gebruikers_info( p_tekst => 'Tray(s) en bestand(en) aangemaakt, wijzigingen opslaan.');
        commit;

   exception
     when others then
       gebruikers_info( p_tekst => 'Fout opgetreden bij het verwerken van protocol: '||p_protocol);
       rollback;
       utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                       recipients => p_email,
                       subject    => 'Fout opgetreden bij het verwerken van protocol: '||p_protocol,
                       message    => v_email_inhoud);

   end;

   if v_totaal_aantal_protocollen is null then
     gebruikers_info( p_tekst => 'Procedure heeft geen protocollen gevonden met opgegeven omschrijving: '||p_protocol);
     raise e_geen_protocollen;
   end if;

   -- verwijder de tijdelijke gegevens uit de hulptabellen
   delete beh_hlp_werklijst;

   delete beh_hlp_werklijst_mq;

   delete beh_hlp_tray_gebruik;

   delete beh_hlp_werklijst_stoftesten;

   delete beh_hlp_werklijst_meetwaarde;

   delete beh_hlp_storage;

   delete beh_hlp_storage_uitval;

   delete beh_stof_niet_op_werklijst;

   delete beh_hlp_werklijst_fracties;

 end if;

 -- We leggen pas alles vast als alles goed verlopen is.
 -- Bij een fout moet een rollback uitgevoerd worden.

 if v_email_inhoud is not null
 then
   utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                   recipients => p_email,
                   subject    => v_onderwerp,
                   message    => v_email_inhoud);
 end if;

commit;

EXCEPTION
   WHEN e_parameters_onbekend THEN
     ROLLBACK;
     gebruikers_info( p_tekst => 'Geen gegevens gevonden met opgegeven parameters: '||v_onderwerp);
     utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                     recipients => p_email,
                     subject    => 'Geen gegevens gevonden met opgegeven parameters: '||v_onderwerp,
                     message    => v_email_inhoud);
     qms$errors.unhandled_exception('beh_dna_voortest_wlst_geen_mq.start_werklijst_tray_etiket');
   WHEN e_geen_protocollen THEN
     ROLLBACK;
     gebruikers_info( p_tekst => 'Geen gegevens gevonden voor opgegeven protocol: '||v_onderwerp);
     utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                     recipients => p_email,
                     subject    => 'Geen gegevens gevonden voor opgegeven protocol: '||v_onderwerp,
                     message    => v_email_inhoud);
     qms$errors.unhandled_exception('beh_dna_voortest_wlst_geen_mq.start_werklijst_tray_etiket');
   WHEN OTHERS THEN
     gebruikers_info(p_tekst => 'Onverwachte fout opgetrden met sqlcode '|| SQLCODE ||' , foutmelding ' || SQLERRM );
     ROLLBACK;
     utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                     recipients => p_email,
                     subject    => 'EXCEPTION in procedure: '||v_onderwerp,
                     message    => v_email_inhoud);
     qms$errors.unhandled_exception('beh_dna_voortest_wlst_geen_mq.start_werklijst_tray_etiket');


END start_werklijst_tray_etiket;

END BEH_DNA_VOORTEST_WLST_GEEN_MQ;
/

/
QUIT
