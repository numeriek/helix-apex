CREATE OR REPLACE PACKAGE "HELIX"."BEH_GRP_AANMELDEN_ONDERZOEKEN" IS

/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 24-07-2011

DOEL

Voor  personen die in het .csv bestand staan onderzoek aanmelden en daarbij
de jongste opgewerkte fractie betrokken maken.
Alleen onderzoek aanmaken waarbij fractie  status op 'opgewerkt' staat
en er geen openstaand onderzoek is voor de indicatie code die moet worden aangemeld.
Zo niet signaleren.

Input:
*.csv bestand met daarin eerste kolom het  PERS_ID

Uitvoeren In Helix via scherm uitvoeren SQL

Parameters
- Locatie file ? verplicht; schrijfwijze: \\umcukz13\d$\inputfile.csv
- Afdeling (Default GENOOM) ? verplicht
- Onderzoeksgroep  (Default P) ? verplicht
- Type (Default Research) ? verplicht
- Wijze (Default scanning) ? verplicht
- Aanvrager (Code van de relatie) indien niet aanwezig in relaties code 'ONBEKEND' gebruiken. ? verplicht
- Indicatie Code ? verplicht
- Email adres, waar naartoe log file gestuurd moet worden. ? niet verplicht (indien leeg , dan geen email sturen, log file wordt alleen in Helix geopend).

Resultaat:
- Aangemaakte Onderzoeken met betrokken fracties (als het goed is worden metingen via Helix automatismen aangemeld)
- Status van onderzoeken default op 'Goedgekeurd' zetten.

Signaleringen:
1.    Personen waarbij geen fractie is gevonden die aan de eisen voldoet ? onderzoeken voor deze personen worden niet aangemaakt
2.    Personen waarbij er onderzoeken openstaan voor de opgegeven indicatie ? onderzoeken voor deze personen worden niet aangemaakt
3.    Indien de opgegeven indicatie code van andere onderzoeksgroep is, dan de opgegeven onderzoeksgroep, dan moet de verwerking stoppen. Foutmelding teruggeven.
4.    Indien de opgegeven file niet gevonden kan worden, foutmelding geven.

Fracties betrokken maken:
- Monster moet gekoppeld worden (in KGC_ONDERZOEKEN_MONSTERS),
  fracties worden dan automatisch betrokken gemaakt
  (indien sypa= FRACTIE_ONDERZOEK_BETROKKEN op J staat voor de desbetreffende afdeling/onderzoeksgroep)

INPUT PARAMETERS
P_BESTANDSLOCATIE IN VARCHAR2 -- '\\umcukz13\d$\inputfile.csv'
P_AFDELING        IN VARCHAR2 DEFAULT 'GENOOM'
P_ONDERZOEKSGROEP IN VARCHAR2 DEFAULT 'P'--
P_O_TYPE          IN VARCHAR2 DEFAULT 'R'-- RESEARCH
P_O_WIJZE         IN VARCHAR2 DEFAULT 'S'-- SCANNING
P_AANVRAGER       IN VARCHAR2 DEFAULT 'ONBEKEND'-- Code van de relatie
P_INDICATIE_CODE  IN VARCHAR2
P_EMAIL           IN VARCHAR2 DEFAULT 'HelpdeskHelixCUKZ@cukz.umcn.nl'
P_LOCATIE         IN VARCHAR2 -- waar staat het bestand
P_BESTAND         IN VARCHAR2 -- naam van het invoerbestand

Wanneer/        Wie             Wat
Versie
------------------------------------------------------------------------
24-07-2011     Y.Arts          Creatie
1.0.0
***********************************************************************/


GV_REVISIE_VERSIE CONSTANT VARCHAR2(10) := '1.0.0';
V_EMAIL_INHOUD VARCHAR2(32767);
C_NIEUWE_REGEL varchar2(10) := chr(10);


/* revisie historie */
FUNCTION REVISION
 RETURN VARCHAR2;

PROCEDURE START_AANMELDEN_ONDERZOEKEN
 (P_AFDELING        IN VARCHAR2 DEFAULT 'GENOOM'
 ,P_O_GROEP         IN VARCHAR2 DEFAULT 'P'--
 ,P_O_TYPE          IN VARCHAR2 DEFAULT 'R'-- RESEARCH
 ,P_O_WIJZE         IN VARCHAR2 DEFAULT 'S'-- SCREENING
 ,P_AANVRAAG        IN VARCHAR2 DEFAULT 'ONBEKEND'-- Code van de relatie
 ,P_INDICODE        IN VARCHAR2
 ,P_EMAIL           IN VARCHAR2 DEFAULT 'HelpdeskHelixCUKZ@cukz.umcn.nl'
 ,P_LOCATIE         IN VARCHAR2 -- waar staat het bestand
 ,P_BESTAND         IN VARCHAR2 -- naam van het invoerbestand
 );

END BEH_GRP_AANMELDEN_ONDERZOEKEN;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_GRP_AANMELDEN_ONDERZOEKEN" IS


FUNCTION REVISION
 RETURN VARCHAR2
 IS
/***********************************************************************
Auteur/bedrijf  : Yuri Arts, YA IT-Services
Datum creatie   : 24-07-2011

BESCHRIJVING
Beschrijf hier de wijzigingen in de package.

HISTORIE

Wanneer /      Wie             Wat
Versie
------------------------------------------------------------------------
24-07-2011     Y.Arts          Creatie
1.0.0
***********************************************************************/
BEGIN
  RETURN gv_revisie_versie;
END;

PROCEDURE START_AANMELDEN_ONDERZOEKEN
 (P_AFDELING        IN VARCHAR2 DEFAULT 'GENOOM'
 ,P_O_GROEP         IN VARCHAR2 DEFAULT 'P'--
 ,P_O_TYPE          IN VARCHAR2 DEFAULT 'R'-- RESEARCH
 ,P_O_WIJZE         IN VARCHAR2 DEFAULT 'S'-- SCREENING
 ,P_AANVRAAG        IN VARCHAR2 DEFAULT 'ONBEKEND'-- Code van de relatie
 ,P_INDICODE        IN VARCHAR2
 ,P_EMAIL           IN VARCHAR2 DEFAULT 'HelpdeskHelixCUKZ@cukz.umcn.nl'
 ,P_LOCATIE         IN VARCHAR2 -- waar staat het bestand
 ,P_BESTAND         IN VARCHAR2 -- naam van het invoerbestand
 )
IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 24-07-2011

DOEL

Voor  personen die in het .csv bestand staan onderzoek aanmelden en daarbij
de jongste opgewerkte fractie betrokken maken.
Alleen onderzoek aanmaken waarbij fractie  status op 'opgewerkt' staat
en er geen openstaand onderzoek is voor de indicatie code die moet worden aangemeld.
Zo niet signaleren.

Input:
*.csv bestand met daarin eerste kolom het  PERS_ID

Uitvoeren In Helix via scherm uitvoeren SQL

Parameters
- Locatie file ? verplicht; schrijfwijze: \\umcukz13\d$\inputfile.csv
- Afdeling (Default GENOOM) ? verplicht
- Onderzoeksgroep  (Default P) ? verplicht
- Type (Default Research) ? verplicht
- Wijze (Default scanning) ? verplicht
- Aanvrager (Code van de relatie) indien niet aanwezig in relaties code 'ONBEKEND' gebruiken. ? verplicht
- Indicatie Code ? verplicht
- Email adres, waar naartoe log file gestuurd moet worden. ? niet verplicht (indien leeg , dan geen email sturen, log file wordt alleen in Helix geopend).

Resultaat:
- Aangemaakte Onderzoeken met betrokken fracties (als het goed is worden metingen via Helix automatismen aangemeld)
- Status van onderzoeken default op 'Goedgekeurd' zetten.

Signaleringen:
1.    Personen waarbij geen fractie is gevonden die aan de eisen voldoet ? onderzoeken voor deze personen worden niet aangemaakt
2.    Personen waarbij er onderzoeken openstaan voor de opgegeven indicatie ? onderzoeken voor deze personen worden niet aangemaakt
3.    Indien de opgegeven indicatie code van andere onderzoeksgroep is, dan de opgegeven onderzoeksgroep, dan moet de verwerking stoppen. Foutmelding teruggeven.
4.    Indien de opgegeven file niet gevonden kan worden, foutmelding geven.

Fracties betrokken maken:
- Monster moet gekoppeld worden (in KGC_ONDERZOEKEN_MONSTERS),
  fracties worden dan automatisch betrokken gemaakt
  (indien sypa= FRACTIE_ONDERZOEK_BETROKKEN op J staat voor de desbetreffende afdeling/onderzoeksgroep)

INPUT PARAMETERS
P_BESTANDSLOCATIE IN VARCHAR2 -- '\\umcukz13\d$\inputfile.csv'
P_AFDELING        IN VARCHAR2 DEFAULT 'GENOOM'
P_ONDERZOEKSGROEP IN VARCHAR2 DEFAULT 'P'--
P_O_TYPE          IN VARCHAR2 DEFAULT 'R'-- RESEARCH
P_O_WIJZE         IN VARCHAR2 DEFAULT 'S'-- SCANNING
P_AANVRAGER       IN VARCHAR2 DEFAULT 'ONBEKEND'-- Code van de relatie
P_INDICATIE_CODE  IN VARCHAR2
P_EMAIL_LOGGING   IN VARCHAR2 DEFAULT 'HelpdeskHelixCUKZ@cukz.umcn.nl'
P_EMAIL           IN VARCHAR2 DEFAULT 'HelpdeskHelixCUKZ@cukz.umcn.nl'

Wanneer/        Wie             Wat
Versie
------------------------------------------------------------------------
24-07-2011     Y.Arts          Creatie
1.0.0
***********************************************************************/

   -- afdeling id ophalen
   cursor kafd_cur (b_afdeling in varchar2)
   is
   select kafd_id
   from   kgc_kgc_afdelingen
   where  code = b_afdeling
   ;

   -- personen die verwerkt moeten worden staan in .csv file
   cursor pers_cur  ( b_afdeling in varchar2
                    , b_o_groep  in varchar2
                    , b_o_type   in varchar2
                    , b_o_wijze  in varchar2
                    , b_aanvraag in varchar2
                    , b_indicode in varchar2
                    )
   is

   select onde.pers_id
   ,      pers.zisnr
   ,      pers.aanspreken
   ,      pers.geboortedatum
   ,      pers.geslacht
   from   beh_aanmelden_onderzoeken_pers onde
   ,      kgc_personen pers
   where  onde.verwerkt        = 'N'
   and    onde.pers_id         = pers.pers_id
   and    onde.afdeling        = b_afdeling
   and    onde.onderzoeksgroep = b_o_groep
   and    onde.onderzoekstype  = b_o_type
   and    onde.onderzoekswijze = b_o_wijze
   and    onde.aanvrager       = b_aanvraag
   and    onde.indicatie_code  = b_indicode
   ;

   cursor frac_cur (b_pers_id in number
                   ,b_kafd_id in number)
   is

   select frac.frac_id
   ,      frac.mons_id
   ,      frac.fractienummer
   from bas_fracties frac
   ,    kgc_monsters mons
   where frac.mons_id = mons.mons_id
   and   frac.status = 'O'
   and   mons.pers_id = b_pers_id
   and   mons.kafd_id = b_kafd_id
   order by frac.creation_date desc
   ;

   cursor onde_cur (b_pers_id       in number
                   ,b_indi_code     in varchar2
                   ,b_onde_afgerond in varchar2
                   ,b_kafd_id       in number)
   is

   select onde.onderzoeknr
   from kgc_indicatie_teksten    indi
   ,    kgc_onderzoek_indicaties onin
   ,    kgc_onderzoeken          onde
   ,    kgc_relaties             rela
   where onde.onde_id  = onin.onde_id
   and   onin.indi_id  = indi.indi_id
   and   onde.pers_id  = b_pers_id
   and   indi.code     = b_indi_code
   and   onde.afgerond = b_onde_afgerond
   and   onde.kafd_id  = b_kafd_id
   ;

   cursor ongr_cur (b_indi_code in varchar2
                   ,b_ongr_code in varchar2
                   ,b_kafd_id   in number)
   is

   select ongr.ongr_id, indi.ingr_id, indi.indi_id
   from  kgc_indicatie_teksten indi
   ,     kgc_onderzoeksgroepen ongr
   where indi.ongr_id = ongr.ongr_id
   and   indi.code    = b_indi_code
   and   ongr.code    = b_ongr_code
   and   ongr.kafd_id = b_kafd_id
   ;

   cursor rela_cur (b_relatie_code in varchar2)
   is
   select rela_id
   from   kgc_relaties
   where  code = b_relatie_code
   and    vervallen = 'N'
   union all
   select rela_id
   from   kgc_relaties
   where  code = 'ONBEKEND'
   and    vervallen = 'N'
   ;

   --Controleer of via de automatismen een onderzoeksbetrokkene is aangemaakt
   cursor onbe_cur (b_onde_id in number)
   is
   select onbe.onbe_id
   from kgc_onderzoek_betrokkenen onbe
   where onbe.onde_id = b_onde_id
   ;

   --Bepaal welke fractie betrokken moet blijven. frac_parent_id is leeg
   cursor frac_cur2 (b_onmo_id in number)
   is
   select max(frac_id)
   from   kgc_onderzoek_monsters onmo
   ,      bas_fracties           frac
   where  onmo.onmo_id = b_onmo_id
   and    frac.mons_id = onmo.mons_id
   and    frac_id_parent is null
   and    frac.status = 'O'
   ;

   --Als geen frac_id gevonden is dan zoeken naar fractie waarbij frac_parent_id is gevuld
   cursor frac_cur3 (b_onmo_id in number)
   is
   select max(frac_id)
   from   kgc_onderzoek_monsters onmo
   ,      bas_fracties           frac
   where  onmo.onmo_id = b_onmo_id
   and    frac.mons_id = onmo.mons_id
   and    frac_id_parent is not null
   and    frac.status = 'O'
   ;

   --
   cursor frac_cur4 (b_frac_id in number)
   is
   select frac.fractienummer
   from   bas_fracties frac
   where  frac.frac_id = b_frac_id
   ;

   v_kafd_id               KGC_KGC_AFDELINGEN.KAFD_ID%type;
   v_frac_id bas_fracties.frac_id%type;
   v_fractienummer bas_fracties.fractienummer%type;
   v_mons_id bas_fracties.mons_id%type;
   v_onderzoeknr kgc_onderzoeken.onderzoeknr%type;

   -- email
   v_onderwerp    varchar2(100) := 'Aanmelden Onderzoeken';
   v_signalering  boolean := false;
   v_fout         boolean := false;

   v_aantal_personen number := 0;
   v_ongr_id number;
   v_indi_id     number;
   v_teller      number;
   v_rela_id     number;

   v_onde_id     number;
   v_ingr_id     number;
   v_onin_id     number;
   v_onmo_id     number;

   -- inlezen van bestand
   v_bestandsnaam      varchar2(100);
   v_statement         varchar2(4000);
   v_statement_client  varchar2(4000);
   v_client_locatie    varchar2(100);
   v_invoerbestand     utl_file.file_type;
   v_regel             varchar2(4000);
   v_onbe_id           number;
   v_frac_id_betrokken number;


procedure controleer_bestand
( p_bestand    in varchar2
, p_locatie    in varchar2
, p_dbs_client in varchar2
, p_rw         in varchar2
)
is
  out_file_client utl_file.file_type;
  out_file_dbs    utl_file.file_type;
begin
  -- controleer opgegeven bestand
  IF ( p_bestand IS NOT NULL )
  THEN
    BEGIN
      if ( p_dbs_client = 'CLIENT' )
      then
        out_file_client  := utl_file.fopen(location  => p_locatie
                                          ,filename  => p_bestand
                                          ,open_mode => p_rw);
      elsif ( p_dbs_client = 'DBS' )
      then
        out_file_dbs  := utl_file.fopen(location  => p_locatie
                                      ,filename  => p_bestand
                                      ,open_mode => p_rw);
      end if;

      IF ( utl_file.is_open( file => out_file_client ) )
      THEN
        utl_file.fclose( file =>  out_file_client );
      END IF;
      IF ( utl_file.is_open( file => out_file_dbs ) )
      THEN
        utl_file.fclose( file => out_file_dbs );
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        IF ( utl_file.is_open( file => out_file_client ) )
        THEN
          utl_file.fclose( file => out_file_client );
        END IF;
        IF ( utl_file.is_open( file => out_file_dbs ) )
        THEN
          utl_file.fclose( file => out_file_dbs );
        END IF;
        qms$errors.show_message
          ( p_mesg => 'KGC-00000'
          , p_param0 => 'Bestand '||p_bestand||' kan niet geopend worden'
          , p_param1 => 'Controleer locatie en naam van het bestand'
                        ||CHR(10)||SQLERRM
          , p_errtp => 'E'
          , p_rftf => TRUE
          );
    END;
  END IF;
end controleer_bestand;


BEGIN

   dbms_output.put_line ('Aanmelden onderzoeken:');
   v_email_inhoud := v_email_inhoud ||'Aanmelden onderzoeken:';

   -- Lees eerst het bestand in de database
   if p_bestand is not null and p_locatie is not null then

     -- Verwijder de Oracle directory 'clientdir' om fouten te voorkomen
     begin
       v_statement := 'drop directory clientdir';
       execute immediate v_statement;
     exception
       when others then
       null;
     end;

     -- maak etiket directory van de client aan in Oracle
     v_statement_client := 'create directory clientdir as '||''''||p_locatie||'''';
     execute immediate v_statement_client;

     -- Controleer of het bestand aangemaakt kan worden op de locatie van de client
     controleer_bestand
     ( p_locatie    => 'CLIENTDIR'
     , p_bestand    => p_bestand
     , p_dbs_client => 'CLIENT'
     , p_rw         => 'r'
     );

     v_invoerbestand := utl_file.fopen( 'CLIENTDIR', p_bestand, 'r' );

     loop
       begin
         utl_file.get_line( v_invoerbestand, v_regel );
       exception
         when NO_DATA_FOUND then
            exit;
       end;

       insert into beh_aanmelden_onderzoeken_pers (pers_id
                                                  ,afdeling
                                                  ,onderzoeksgroep
                                                  ,onderzoekstype
                                                  ,onderzoekswijze
                                                  ,aanvrager
                                                  ,indicatie_code
                                                  ,email
                                                  ,locatie
                                                  ,bestandsnaam
                                                  ,verwerkt) values
                                                  (v_regel
                                                  ,p_afdeling
                                                  ,p_o_groep
                                                  ,p_o_type
                                                  ,p_o_wijze
                                                  ,p_aanvraag
                                                  ,p_indicode
                                                  ,p_email
                                                  ,p_locatie
                                                  ,p_bestand
                                                  ,'N');
     end loop;

     utl_file.fclose( v_invoerbestand );

    -- drop directories
     v_statement_client     := 'drop directory CLIENTDIR';
     execute immediate v_statement_client;

   end if;
   ----------------------------------------------------------------------------------------

   -- Begin met de verwerking van het bestand
   open  kafd_cur (b_afdeling => p_afdeling);
   fetch kafd_cur into v_kafd_id;
   close kafd_cur;

   ------------------------------------------------------------
   -- Indien de opgegeven indicatie code van andere onderzoeksgroep is
   -- dan de opgegeven onderzoeksgroep, dan moet de verwerking stoppen. Foutmelding teruggeven.
   ------------------------------------------------------------
   open ongr_cur (b_indi_code => p_indicode
                 ,b_ongr_code => p_o_groep
                 ,b_kafd_id   => v_kafd_id);
   fetch ongr_cur into v_ongr_id, v_ingr_id, v_indi_id;
   close ongr_cur;

   if v_ongr_id is null then

     v_fout := true;
     v_email_inhoud := v_email_inhoud ||'Indicatiecode '||p_indicode||' hoort niet bij onderzoeksgroep '||p_o_groep||'. Verwerking afgebroken.'||c_nieuwe_regel;
     dbms_output.put_line ('Indicatiecode '||p_indicode||' hoort niet bij onderzoeksgroep '||p_o_groep||'. Verwerking afgebroken.');

   end if;
   ------------------------------------------------------------

   if not v_fout then

     for pers_rec in pers_cur ( b_afdeling => p_afdeling
                              , b_o_groep  => p_o_groep
                              , b_o_type   => p_o_type
                              , b_o_wijze  => p_o_wijze
                              , b_aanvraag => p_aanvraag
                              , b_indicode => p_indicode
                              )
                               loop

       v_signalering   := false;
       v_teller        := v_teller + 1;
       v_onbe_id       := null;
       v_onderzoeknr   := null;
       v_frac_id       := null;
       v_fractienummer := null;
       v_mons_id       := null;
       ------------------------------------------------------------
       -- Personen waarbij geen fractie is gevonden die aan de eisen voldoet ? onderzoeken voor deze personen worden niet aangemaakt
       open  frac_cur(b_pers_id => pers_rec.pers_id
                     ,b_kafd_id => v_kafd_id);
       fetch frac_cur into v_frac_id, v_mons_id, v_fractienummer;
       close frac_cur;

       if v_frac_id is null then

         v_signalering := true;
         v_email_inhoud := v_email_inhoud ||'Geen fractie aanwezig voor persoon met id: '||pers_rec.pers_id||'. Onderzoek niet aangemaakt.'||c_nieuwe_regel;
         dbms_output.put_line ('Geen fractie aanwezig voor persoon met id: '||pers_rec.pers_id||'. Onderzoek niet aangemaakt.');

       end if;
       ------------------------------------------------------------

       ------------------------------------------------------------
       --Personen waarbij er onderzoeken openstaan voor de opgegeven indicatie. onderzoeken voor deze personen worden niet aangemaakt
       open  onde_cur(b_pers_id       => pers_rec.pers_id
                     ,b_indi_code     => p_indicode
                     ,b_onde_afgerond => 'N'
                     ,b_kafd_id       => v_kafd_id);
       fetch onde_cur into v_onderzoeknr;
       close onde_cur;

       if v_onderzoeknr is not null then

         --
         v_signalering := true;
         v_email_inhoud := v_email_inhoud ||'Onderzoek '||v_onderzoeknr||' voor persoon met id: '||pers_rec.pers_id||' is nog niet afgerond. Onderzoek niet aangemaakt.'||c_nieuwe_regel;
         dbms_output.put_line ('Onderzoek '||v_onderzoeknr||' voor persoon met id: '||pers_rec.pers_id||' is nog niet afgerond. Onderzoek niet aangemaakt.');

       end if;
       ------------------------------------------------------------

       ------------------------------------------------------------
       -- Als er geen signaleringen zijn dan doorgaan met onderzoek aanmaken
       ------------------------------------------------------------
       if v_signalering = false then

         -- relatie ID ophalen. Als de opgegeven aanvrager niet bestaat dan onbekende relatie nemen
         open  rela_cur(b_relatie_code => p_aanvraag);
         fetch rela_cur into v_rela_id;
         close rela_cur;

         v_onderzoeknr := kgc_nust_00.genereer_nr
                            ( p_nummertype => 'ONDE'
                            , p_ongr_id    => v_ongr_id
                            );

         select kgc_onde_seq.nextval
         into   v_onde_id
         from   dual;

         select kgc_onin_seq.nextval
         into   v_onin_id
         from   dual;

         select kgc_onmo_seq.nextval
         into   v_onmo_id
         from   dual;

         insert into kgc_onderzoeken
         (onde_id
         ,ongr_id
         ,kafd_id
         ,pers_id
         ,onderzoekstype
         ,onderzoekswijze
         ,onderzoeknr
         ,rela_id
         ,status
         ,mede_id_controle
         ) values
         (v_onde_id
         ,v_ongr_id
         ,v_kafd_id
         ,pers_rec.pers_id
         ,p_o_type
         ,p_o_wijze
         ,v_onderzoeknr
         ,v_rela_id
         ,'G'
         ,kgc_mede_00.medewerker_id
         );

         insert into kgc_onderzoek_monsters
         ( onmo_id
         , onde_id
         , mons_id
         ) values
         ( v_onmo_id
         , v_onde_id
         , v_mons_id
         );

         insert into kgc_onderzoek_indicaties
         ( onin_id
         , onde_id
         , indi_id
         ) values
         ( v_onin_id
         , v_onde_id
         , v_indi_id
         );

         v_email_inhoud := v_email_inhoud ||pers_rec.pers_id||';'||pers_rec.zisnr||';'||pers_rec.aanspreken||';'
                                          ||pers_rec.geboortedatum||';'||pers_rec.geslacht||';'||v_onderzoeknr||';'
                                          ||v_fractienummer||c_nieuwe_regel;
         dbms_output.put_line (pers_rec.pers_id||';'||pers_rec.zisnr||';'||pers_rec.aanspreken||';'
                                ||pers_rec.geboortedatum||';'||pers_rec.geslacht||';'||v_onderzoeknr||';'
                                ||v_fractienummer);



         update beh_aanmelden_onderzoeken_pers
         set   verwerkt        = 'J'
         where pers_id         = pers_rec.pers_id
         and   afdeling        = p_afdeling
         and   onderzoeksgroep = p_o_groep
         and   onderzoekstype  = p_o_type
         and   onderzoekswijze = p_o_wijze
         and   aanvrager       = p_aanvraag
         and   indicatie_code  = p_indicode
         ;

         commit;

         -- In de  AIS trigger van kgc_onderzoek_monsters wordt de procedure
         -- bas_meti_00.standaard_protocol_aanmelden aangeroepen. De teveel aangemaakte fracties dienen
         -- verwijderd te worden.
         DBMS_LOCK.sleep (seconds => 0.1);

         -- Bepaal welke frac_id betrokken moet blijven, de rest moet weg
         v_frac_id_betrokken := null;

         open  frac_cur2(b_onmo_id => v_onmo_id);
         fetch frac_cur2 into v_frac_id_betrokken;
         close frac_cur2;

         if v_frac_id_betrokken is null then
           open  frac_cur3(b_onmo_id => v_onmo_id);
           fetch frac_cur3 into v_frac_id_betrokken;
           close frac_cur3;
         end if;

         if v_frac_id_betrokken is null then
           v_frac_id_betrokken := 0;
           v_email_inhoud := v_email_inhoud ||'Fractie niet gevonden welke betrokken moet blijven'||c_nieuwe_regel;
           dbms_output.put_line ('Fractie niet gevonden welke betrokken moet blijven');
         end if;

         delete  bas_meetwaarde_details mdet
         where meet_id in ( select meet_id from  bas_meetwaarden
                            where  meti_id in (select meti.meti_id from bas_metingen meti
                                                where  meti.onde_id = v_onde_id
                                                and    meti.frac_id <> v_frac_id_betrokken
                                              )
                         );

         delete bas_meetwaarden
         where  meti_id in (select meti.meti_id from bas_metingen meti
                            where  meti.onde_id = v_onde_id
                            and    meti.frac_id <> v_frac_id_betrokken
                           );

         delete bas_metingen meti
         where  meti.onde_id = v_onde_id
         and    meti.frac_id <> v_frac_id_betrokken;


         delete kgc_onderzoek_betrokkenen onbe
         where onde_id = v_onde_id
         and   onbe.frac_id <> v_frac_id_betrokken;


         --Controleer of via de automatismen een onderzoeksbetrokkene is aangemaakt
         open  onbe_cur (b_onde_id => v_onde_id);
         fetch onbe_cur into v_onbe_id;
         close onbe_cur;

         if v_onbe_id is null then

           if v_frac_id_betrokken is not null then
             open  frac_cur4(b_frac_id => v_frac_id_betrokken);
             fetch frac_cur4 into v_fractienummer;
             close frac_cur4;

             v_frac_id := v_frac_id_betrokken;
           end if;

           v_email_inhoud := v_email_inhoud ||'Fractie '||v_fractienummer||' is betrokken gemaakt.'||c_nieuwe_regel;
           dbms_output.put_line ('Fractie '||v_fractienummer||' is betrokken gemaakt.');

           select kgc_onmo_seq.nextval
           into   v_onbe_id
           from   dual;

           insert into kgc_onderzoek_betrokkenen (onbe_id, onde_id, frac_id, pers_id)
           values (v_onbe_id, v_onde_id, v_frac_id, pers_rec.pers_id);
         end if;

       end if;
       ------------------------------------------------------------

     end loop;

     ------------------------------------------------------------
     -- Indien de opgegeven file niet gevonden kan worden, foutmelding geven.
     if v_teller = 0 then

       v_email_inhoud := v_email_inhoud ||'Het bestand is niet aanwezig of bevat geen personen (pers_id)'||c_nieuwe_regel;
       dbms_output.put_line ('Het bestand is niet aanwezig of bevat geen personen (pers_id)');

     end if;
     ------------------------------------------------------------

   end if;

   if v_email_inhoud is not null
   then
     utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                     recipients => p_email,
                     subject    => v_onderwerp,
                     message    => v_email_inhoud);
   end if;

   commit;


EXCEPTION
   WHEN OTHERS THEN
     ROLLBACK;

     utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                     recipients => p_email,
                     subject    => 'EXCEPTION in procedure: '||v_onderwerp,
                     message    => v_email_inhoud);
     qms$errors.unhandled_exception('beh_groep_aanmelden_onderzoeken.start_aanmelden_onderzoeken');

END START_AANMELDEN_ONDERZOEKEN;


END beh_grp_aanmelden_onderzoeken;
/

/
QUIT
