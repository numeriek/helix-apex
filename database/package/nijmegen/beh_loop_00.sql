CREATE OR REPLACE PACKAGE "HELIX"."BEH_LOOP_00" is

-- Alternatieve functies voor kgc_loop_00 geplande_einddatum en signaleringsdatum.
-- Deze versies houden rekening met een extra attribuutwaarde op de indicatie om
-- verschil te kunnen maken tussen binnenlands- en buitenlandse looptijden.
--
-- Mantis 12168
--
-- Bepaal de geplande einddatum van een onderzoek.
function geplande_einddatum(p_onde_id in number, p_indi_id in number) return date;
pragma restrict_references(geplande_einddatum, wnds, wnps, rnps);

-- Bepaal de signaleringsdatum van een onderzoek.
function signaleringsdatum(p_onde_id in number, p_indi_id in number) return date;
pragma restrict_references(signaleringsdatum, wnds, wnps, rnps);


-- Voor METAB moet de doorloop (geplande einddatum) aangepast worden afhankelijk
-- van snelheid (langdurend/kortdurend) op het geselecteerde protocol.
--
procedure zet_einddatum_via_snelheid(p_onde_id in number);
function  get_dlt_via_snelheid(p_onde_id in number) return number;

end beh_loop_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_LOOP_00" is

  cursor onde_cur(b_onde_id in number) is
    select datum_binnen
    ,      onderzoekswijze
    from   kgc_onderzoeken
    where  onde_id = b_onde_id;


  ------------------------------------------------------------------------------
  function geplande_einddatum(p_onde_id in number, p_indi_id in number) return date is
    v_datum                date;
    onde_rec               onde_cur%rowtype;
    v_verkort_doorlooptijd number;

  begin

    open onde_cur(b_onde_id => p_onde_id);
    fetch onde_cur
    into onde_rec;
    close onde_cur;

    --Haal de einddatum op via de standaard manier.
    v_datum := kgc_loop_00.geplande_einddatum(onde_rec.datum_binnen, p_indi_id, onde_rec.onderzoekswijze);

    --Corrigeer de einddatum alleen als het een buitenlands onderzoek betreft.
    if beh_onde_bu(p_onde_id) = 'BUITENLAND'  and p_indi_id is not null then

      --Haal extra attribuut waarde VERKORT_BU_DOORLOOPTIJD op voor de indicatie.
      begin
        select to_number(atwa.waarde) as verkort_looptijd
        into   v_verkort_doorlooptijd
        from   kgc_attributen attr
          join kgc_attribuut_waarden atwa on atwa.id = p_indi_id and atwa.attr_id = attr.attr_id
        where  attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
          and  attr.code = 'VERKORT_BU_DOORLOOPTIJD';
      exception when others then
        v_verkort_doorlooptijd := 0;
      end;

      --Corrigeer de datum.
      v_datum := v_datum - v_verkort_doorlooptijd;

    end if;

    return(v_datum);

  end geplande_einddatum;


  ------------------------------------------------------------------------------
  function signaleringsdatum(p_onde_id in number, p_indi_id in number) return date is
    v_datum                     date;
    onde_rec                    onde_cur%rowtype;
    v_verkort_signaleringsdatum number;

  begin

    open onde_cur(b_onde_id => p_onde_id);
    fetch onde_cur
    into onde_rec;
    close onde_cur;

    --Haal de signaleringsdatum op via de standaard manier.
    v_datum := kgc_loop_00.signaleringsdatum(onde_rec.datum_binnen, p_indi_id, onde_rec.onderzoekswijze);

    --Corrigeer de signaleringsdatum alleen als het een buitenlands onderzoek betreft.
    if beh_onde_bu(p_onde_id) = 'BUITENLAND' and p_indi_id is not null then

      --Haal extra attribuut waarde VERKORT_BU_SIGNAALTIJD op voor de indicatie.
      begin
        select to_number(atwa.waarde) as verkort_looptijd
        into   v_verkort_signaleringsdatum
        from   kgc_attributen attr
          join kgc_attribuut_waarden atwa on atwa.id = p_indi_id and atwa.attr_id = attr.attr_id
        where  attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
          and  attr.code = 'VERKORT_BU_SIGNAALTIJD';
      exception when others then
        v_verkort_signaleringsdatum := 0;
      end;

      --Corrigeer de datum.
      v_datum := v_datum - v_verkort_signaleringsdatum;

    end if;

    return(v_datum);

  end signaleringsdatum;


  ------------------------------------------------------------------------------
  -- Haal de doorlooptijd van een onderzoek op via de snelheid volgens
  -- de definities van METAB.
  --
  function get_dlt_via_snelheid(p_onde_id in number) return number is

    -- Cursor voor het ophalen van de metingen onder een onderzoek en per
    -- meting een eventueele overrulde doorlooptijd van op protocol nivo.
    cursor onde_dlt_cur(b_onde_id in number) is
      with stgr_dlt_attr as
      ( select to_number(atwa.id) as stgr_id
           , to_number(atwa.waarde) as attr_waarde
           , attr.code as attr_code
        from kgc_attribuut_waarden atwa
        left join kgc_attributen attr on attr.attr_id = atwa.attr_id
        where attr.tabel_naam = 'KGC_STOFTESTGROEPEN'
          and attr.code = 'DLT_PROT'
      )
      select meti.meti_id
           , meti.stgr_id
           , meti.snelheid
           , stgr.code
           , dlta.attr_waarde as dlt_waarde
      from bas_metingen meti
        join kgc_stoftestgroepen stgr on stgr.stgr_id = meti.stgr_id
        left join stgr_dlt_attr dlta on dlta.stgr_id = stgr.stgr_id
      where meti.onde_id = b_onde_id;

    v_doorlooptijd_kort number;
    v_doorlooptijd_lang number;

    v_max_doorlooptijd     number := 9999;
    v_kortste_doorlooptijd number := 9999;

  begin

    dbms_output.put_line('DLT_PROT_KORTDUREND: ' || v_doorlooptijd_kort);
    dbms_output.put_line('DLT_PROT_LANGDUREND: ' || v_doorlooptijd_lang);

    for onde_dlt_rec in onde_dlt_cur(b_onde_id => p_onde_id)  loop

      dbms_output.put_line('DLT rec: ' || onde_dlt_rec.meti_id || ' - ' || onde_dlt_rec.stgr_id || ' - ' || onde_dlt_rec.snelheid || ' - ' || onde_dlt_rec.code || ' - ' || onde_dlt_rec.dlt_waarde);

      -- Controleer meting met doorlooptijd Kort.
      if onde_dlt_rec.snelheid = 'K' and (nvl(v_doorlooptijd_kort, v_max_doorlooptijd) < v_kortste_doorlooptijd) then
        v_kortste_doorlooptijd := v_doorlooptijd_kort;
        dbms_output.put_line('IF - KORT');
      end if;

      -- Controleer meting met doorlooptijd Lang.
      if onde_dlt_rec.snelheid = 'L' and (nvl(v_doorlooptijd_lang, v_max_doorlooptijd) < v_kortste_doorlooptijd) then
        v_kortste_doorlooptijd := v_doorlooptijd_lang;
        dbms_output.put_line('IF - LANG');
      end if;

      -- Controleer meting protocol bij meting.
      if nvl(onde_dlt_rec.dlt_waarde, v_max_doorlooptijd) < v_kortste_doorlooptijd then
        v_kortste_doorlooptijd := onde_dlt_rec.dlt_waarde;
        dbms_output.put_line('IF - PROT');
      end if;
      dbms_output.put_line('DLT kostst: ' || v_kortste_doorlooptijd);

    end loop;

    --Als er een kortste doorlooptijd gevonden is, geeft deze terug.
    if v_kortste_doorlooptijd < v_max_doorlooptijd then
      return v_kortste_doorlooptijd;
    end if;

    return null;

  end;


  ------------------------------------------------------------------------------
  procedure zet_einddatum_via_snelheid(p_onde_id in number) is
    v_doorlooptijd       number;
    v_geplande_einddatum date;
    v_onderzoek_rec      cg$kgc_onderzoeken.cg$row_type;
    v_onderzoek_ind      cg$kgc_onderzoeken.cg$ind_type;

  begin

    v_doorlooptijd := get_dlt_via_snelheid(p_onde_id => p_onde_id);

    if v_doorlooptijd is not null then

      --Haal row van geselecteerd onderzoek op.
      v_onderzoek_rec.onde_id := p_onde_id;
      cg$kgc_onderzoeken.slct(v_onderzoek_rec);

      -- Bereken de geplande einddatum alleen het onderzoek nog niet is afgerond
      -- en er wel een datum binnen is ingevuld.
      --
      -- Geplande einddatum voor de datum binnen is een uitzondering.
      -- GENOOM gebruikt bv. geplande einddatum 11-11-1111 als indicatie dat er handmatig
      -- gepland wordt. METAB gaat ook zoiets gebruiken.
      --
      if  ( (nvl(v_onderzoek_rec.AFGEROND, 'N') = 'N')
        and (v_onderzoek_rec.DATUM_BINNEN is not null)
        and (nvl(v_onderzoek_rec.GEPLANDE_EINDDATUM, v_onderzoek_rec.DATUM_BINNEN) >= v_onderzoek_rec.DATUM_BINNEN)
          )
      then

        -- Overschrijf de geplande einddatum altijd, ongeacht een eventueel ingevulde einddatum.
        v_onderzoek_rec.GEPLANDE_EINDDATUM := v_onderzoek_rec.DATUM_BINNEN + v_doorlooptijd;
        v_onderzoek_ind.GEPLANDE_EINDDATUM := TRUE;

        cg$kgc_onderzoeken.upd(v_onderzoek_rec, v_onderzoek_ind);

      end if;
    end if;

  end;

end beh_loop_00;
/

/
QUIT
