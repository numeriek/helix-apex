CREATE OR REPLACE PACKAGE "HELIX"."BEH_FILE_00" IS
  --
  -- Functies en procedures voor file manipulaties vanaf de database server.
  --

  FUNCTION get_filename(p_path IN VARCHAR2) RETURN varchar2;
  FUNCTION get_dir_path(p_path IN VARCHAR2) RETURN varchar2;

  FUNCTION  create_directory_object(p_path IN VARCHAR2) RETURN varchar2;
  PROCEDURE remove_directory_object(p_dir_name IN VARCHAR2);


  -- File functies met een bool return type.
  -- Return TRUE als de actie gelukt is en FALSE als mislukt.
  FUNCTION file_exists(p_dir_file IN VARCHAR2) RETURN boolean;
  FUNCTION file_delete(p_dir_file IN VARCHAR2) RETURN boolean;
  FUNCTION file_copy(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN boolean;
  FUNCTION file_move(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN boolean;
  FUNCTION create_file_directory(p_path IN VARCHAR2) RETURN boolean;


  -- Wrappers voor de file functies zodat deze met een varchar2 return type gebruikt kunnen worden.
  -- Return 'OK' als de actie gelukt is en 'ERR' als mislukt.
  FUNCTION file_copy_01(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN varchar2;
  FUNCTION file_move_01(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN varchar2;
  FUNCTION file_delete_01(p_dir_file IN VARCHAR2) RETURN varchar2;
  FUNCTION file_exists_01(p_dir_file IN VARCHAR2) RETURN varchar2;
  FUNCTION create_file_directory_01(p_path IN VARCHAR2) RETURN varchar2;


END beh_file_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_FILE_00" IS
  --
  -- Functies en procedure voor file manipulaties op het locale filesystem van de database server.
  --

  -- Return het filename deel van het filepath wat is doorgegeven in parameter p_path.
  FUNCTION get_filename(p_path IN VARCHAR2) RETURN varchar2
  IS
    v_file VARCHAR2(30000);

  BEGIN
    -- Parse string for UNIX system
    IF instr(p_path,'/') > 0 THEN
        v_file := substr(p_path,(instr(p_path,'/',-1,1)+1),length(p_path));

    -- Parse string for Windows system
    ELSIF instr(p_path,'\') > 0 THEN
        v_file := substr(p_path,(instr(p_path,'\',-1,1)+1),length(p_path));

    -- If no slashes were found, return the original string
    ELSE
        v_file := p_path;
    END IF;

    RETURN v_file;
  END;


  -- Return het directory path van het filepath wat is doorgegeven in parameter p_path.
  FUNCTION get_dir_path(p_path IN VARCHAR2) RETURN varchar2
  IS
     v_dir VARCHAR2(30000);

  BEGIN

    -- Parse string for UNIX system
    IF instr(p_path,'/') > 0 THEN
        v_dir := substr(p_path,1,(instr(p_path,'/',-1,1)-1));

    -- Parse string for Windows system
    ELSIF instr(p_path,'\') > 0 THEN
        v_dir := substr(p_path,1,(instr(p_path,'\',-1,1)-1));

    -- If no slashes were found, return the original string
    ELSE
        v_dir := p_path;
    END IF;

    RETURN v_dir;

  END;


  -- Maak het Oracle directory object aan voor het directory path p_path en
  -- geef de naam van het gemaakte object terug.
  FUNCTION create_directory_object(p_path IN VARCHAR2) RETURN varchar2
  IS
     v_dir_name VARCHAR2(16);
  BEGIN

    v_dir_name := 'BEH_' || dbms_random.string('u', 12);

    EXECUTE IMMEDIATE 'CREATE DIRECTORY ' || v_dir_name || ' AS ''' || p_path || '''';
    --dbms_output.put_line('FUNCTION create_directory_object: created directory object ' || v_dir_name || ' for location ' || p_path || '.');

    RETURN v_dir_name;

  END;


  -- Verwijder het Oracle directory object met de opgegeven naam geeft true terug als het gelukt is.
  PROCEDURE remove_directory_object(p_dir_name IN VARCHAR2)
  IS
  BEGIN

    EXECUTE IMMEDIATE 'DROP DIRECTORY ' || p_dir_name ;
    --dbms_output.put_line('PROCEDURE remove_directory_object: droped directory object ' || p_dir_name || '.');

  END;



  -- Controleer of file bestaat en return true als de file er is.
  FUNCTION file_exists(p_dir_file IN VARCHAR2) RETURN boolean IS

    --v_file_handle  UTL_FILE.FILE_TYPE;
    v_location     varchar2(30000);
    v_file         varchar2(30000);
    v_dir_obj_name varchar2(20);

    l_length      number;
    l_block_size  number;
    l_returnvalue boolean := false;

  BEGIN

    v_location := get_dir_path(p_dir_file);
    v_file     := get_filename(p_dir_file);

    v_dir_obj_name := beh_file_00.create_directory_object(v_location);

    utl_file.fgetattr (v_dir_obj_name, v_file, l_returnvalue, l_length, l_block_size);
    beh_file_00.remove_directory_object(v_dir_obj_name);

    RETURN l_returnvalue;

  EXCEPTION WHEN OTHERS THEN
    beh_file_00.remove_directory_object(v_dir_obj_name);
    raise;

  END;


  -- Delete de file en return true als de actie gelukt is.
  FUNCTION file_delete(p_dir_file IN VARCHAR2) RETURN boolean IS

    v_location     varchar2(30000);
    v_file         varchar2(30000);
    v_dir_obj_name varchar2(20);

  BEGIN

    v_location     := get_dir_path(p_dir_file);
    v_file         := get_filename(p_dir_file);
    v_dir_obj_name := beh_file_00.create_directory_object(v_location);

    --dbms_output.put_line('FUNCTION file_delete: try to delete file ' || p_dir_file || '.');
    utl_file.fremove(v_dir_obj_name, v_file);

    beh_file_00.remove_directory_object(v_dir_obj_name);
    RETURN true;

  EXCEPTION WHEN OTHERS THEN
    beh_file_00.remove_directory_object(v_dir_obj_name);
    raise;

  END;


  -- Copy de file_from naar de file_to en return true als de actie gelukt is.
  FUNCTION file_copy(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN boolean IS

    v_location_from     varchar2(30000);
    v_file_from         varchar2(30000);
    v_dir_obj_name_from varchar2(20);

    v_location_to       varchar2(30000);
    v_file_to           varchar2(30000);
    v_dir_obj_name_to   varchar2(20);

    v_source_handle     utl_file.file_type;
    v_dest_handle       utl_file.file_type;
    v_remainingbytes    number;
    v_currentbytes      number;
    v_blocksize         number;
    v_fileexists        boolean;

    v_buffer            raw(32767);

  BEGIN

    v_location_from     := get_dir_path(p_dir_file_from);
    v_file_from         := get_filename(p_dir_file_from);
    v_dir_obj_name_from := beh_file_00.create_directory_object(v_location_from);

    v_location_to       := get_dir_path(p_dir_file_to);
    v_file_to           := get_filename(p_dir_file_to);
    v_dir_obj_name_to   := beh_file_00.create_directory_object(v_location_to);

    --dbms_output.put_line('FUNCTION file_copy: from          :' || p_dir_file_from);
    --dbms_output.put_line('FUNCTION file_copy: to            :' || p_dir_file_to);
    --dbms_output.put_line('FUNCTION file_copy: location from :' || v_location_from);
    --dbms_output.put_line('FUNCTION file_copy: location to   :' || v_location_to);
    --dbms_output.put_line('FUNCTION file_copy: file from     :' || v_file_from);
    --dbms_output.put_line('FUNCTION file_copy: file to       :' || v_file_to);
    --dbms_output.put_line('FUNCTION file_copy: dir obj from  :' || v_dir_obj_name_from);
    --dbms_output.put_line('FUNCTION file_copy: dir obj to    :' || v_dir_obj_name_to);

    v_source_handle := utl_file.fopen(v_dir_obj_name_from, v_file_from, 'RB');
    v_dest_handle   := utl_file.fopen(v_dir_obj_name_to, v_file_to, 'WB');

    utl_file.fgetattr(v_dir_obj_name_from, v_file_from, v_fileexists, v_remainingbytes, v_blocksize);

    while v_remainingbytes>0 loop

      if v_remainingbytes>=32767 then
        v_currentbytes := 32767;
      else
        v_currentbytes := v_remainingbytes;
      end if;

      utl_file.get_raw(v_source_handle, v_buffer, v_currentbytes);
      utl_file.put_raw(v_dest_handle, v_buffer, false);

      v_remainingbytes := v_remainingbytes - v_currentbytes;

    end loop;

    utl_file.fclose(v_source_handle);
    utl_file.fclose(v_dest_handle);

    -- NB: De copieer functie fcopy van utl_file package kan alleen in tekst
    --     mode kopieeren niet in binary mode. Daarom wordt deze niet gebruikt.
    --
    --    utl_file.fcopy( src_location  => v_dir_obj_name_from
    --                  , src_filename  => v_file_from
    --                  , dest_location => v_dir_obj_name_to
    --                  , dest_filename => v_file_to
    --                  );

    beh_file_00.remove_directory_object(v_dir_obj_name_from);
    beh_file_00.remove_directory_object(v_dir_obj_name_to);

    RETURN true;

  EXCEPTION WHEN OTHERS THEN
    utl_file.fclose(v_source_handle);
    utl_file.fclose(v_dest_handle);
    beh_file_00.remove_directory_object(v_dir_obj_name_from);
    beh_file_00.remove_directory_object(v_dir_obj_name_to);

    RETURN false;

  END;


  -- Move de file_from naar de file_to en return true als de actie gelukt is.
  FUNCTION file_move(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN boolean IS

    v_location_from     varchar2(30000);
    v_file_from         varchar2(30000);
    v_dir_obj_name_from varchar2(20);

    v_location_to       varchar2(30000);
    v_file_to           varchar2(30000);
    v_dir_obj_name_to   varchar2(20);

  BEGIN

    v_location_from     := get_dir_path(p_dir_file_from);
    v_file_from         := get_filename(p_dir_file_from);
    v_dir_obj_name_from := beh_file_00.create_directory_object(v_location_from);

    v_location_to       := get_dir_path(p_dir_file_to);
    v_file_to           := get_filename(p_dir_file_to);
    v_dir_obj_name_to   := beh_file_00.create_directory_object(v_location_to);

    --dbms_output.put_line('FUNCTION file_move: try to move file ' || p_dir_file_from || ' to file ' || p_dir_file_to || '.');
    utl_file.frename( src_location  => v_dir_obj_name_from
                    , src_filename  => v_file_from
                    , dest_location => v_dir_obj_name_to
                    , dest_filename => v_file_to
                    , overwrite     => true
                    );

    beh_file_00.remove_directory_object(v_dir_obj_name_from);
    beh_file_00.remove_directory_object(v_dir_obj_name_to);
    RETURN true;

  EXCEPTION WHEN OTHERS THEN
    beh_file_00.remove_directory_object(v_dir_obj_name_from);
    beh_file_00.remove_directory_object(v_dir_obj_name_to);
    raise;

  END;


  -- Maak de file directory aan via java
  FUNCTION mkdir(p_path IN varchar2) RETURN number IS
  LANGUAGE JAVA NAME 'BEH_Bestanden.mkdir(java.lang.String) return java.lang.int';

  -- Copy file via java
  -- Let op! Deze copy fuctie geeft geeft vaak OK terug als de copy actie niet uitgevoerd is!
  FUNCTION copy(p_path_from IN varchar2, p_path_to IN varchar2) RETURN varchar2 IS
  LANGUAGE JAVA NAME 'Bestanden.copy(java.lang.String, java.lang.String) return java.lang.String';

  -- Delete file via java
  FUNCTION del(p_path IN varchar2) RETURN varchar2 IS
  LANGUAGE JAVA NAME 'Bestanden.delete(java.lang.String) return java.lang.String';


  -- Maak de File directory aan
  FUNCTION create_file_directory(p_path IN VARCHAR2) RETURN boolean
  IS
  BEGIN

    --Let op java mkdirs commando geeft fout (1) terug als de directory
    --al bestaat, dit wordt hier niet als fout gezien.
    if mkdir(p_path) <= 1 then
      return true;
    else
      return false;
    end if;

  END;

  -- Wrapper voor file_copy met een varchar2 return type.
  FUNCTION file_copy_01(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN varchar2 IS
  BEGIN
    if file_copy(p_dir_file_from, p_dir_file_to) then
      return 'OK';
    end if;

    return 'ERR';
  END;

  -- Wrapper voor file_move met een varchar2 return type.
  FUNCTION file_move_01(p_dir_file_from IN VARCHAR2,  p_dir_file_to IN VARCHAR2) RETURN varchar2 IS
  BEGIN
    if file_move(p_dir_file_from, p_dir_file_to) then
      return 'OK';
    end if;

    return 'ERR';
  END;

  -- Wrapper voor file_delete met een varchar2 return type.
  FUNCTION file_delete_01(p_dir_file IN VARCHAR2) RETURN varchar2 IS
  BEGIN
    if file_delete(p_dir_file) then
      return 'OK';
    end if;

    return 'ERR';
  END;

  -- Wrapper voor file_exists met een varchar2 return type.
  FUNCTION file_exists_01(p_dir_file IN VARCHAR2) RETURN varchar2 IS
  BEGIN
    if file_exists(p_dir_file) then
      return 'OK';
    end if;

    return 'ERR';
  END;

  -- Wrapper voor create_file_directory met een varchar2 return type.
  FUNCTION create_file_directory_01(p_path IN VARCHAR2) RETURN varchar2 IS
  BEGIN
    if create_file_directory(p_path) then
      return 'OK';
    end if;

    return 'ERR';
  END;


END beh_file_00;
/

/
QUIT
