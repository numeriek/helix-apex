CREATE OR REPLACE PACKAGE "HELIX"."BEH_CLIA_00" as
g_conn       utl_tcp.connection;
g_connected  boolean := false;
g_test       boolean := false;
g_hl7_bericht varchar2(32767);

g_clhb_id beh_clia_hl7_berichten.clhb_id%type;
type t_beh_clia_orm_vw is table of  beh_clia_orm_vw%rowtype index by binary_integer;
g_meet t_beh_clia_orm_vw;
procedure log
(
    p_bericht_tekst in beh_clia_hl7_berichten.bericht_tekst%type := null
,   p_verzonden in beh_clia_hl7_berichten.verzonden%type := 'N'
,   p_bericht_antwoord in beh_clia_hl7_berichten.bericht_antwoord%type := null
,   p_log_tekst in beh_clia_hl7_berichten.log_tekst%type := null
);

procedure open_verbinding;
procedure sluit_verbinding;
function samenstellen_bericht
(
    p_i in number
,   x_ok     in out boolean
)
return varchar2;
function send_msg
(
    p_strhl7in  in varchar2
,   p_timeout   in number -- tienden van seconden
,   x_strhl7out in out varchar2
,   x_msg       in out varchar2
)
return boolean;

procedure start_verwerking
(
    p_i in number    --loop row number
);
procedure start_verwerking_via_job;
end beh_clia_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_CLIA_00"
AS
    PROCEDURE LOG (
        p_bericht_tekst        IN beh_clia_hl7_berichten.bericht_tekst%TYPE,
        p_verzonden             IN beh_clia_hl7_berichten.verzonden%TYPE,
        p_bericht_antwoord    IN beh_clia_hl7_berichten.bericht_antwoord%TYPE,
        p_log_tekst             IN beh_clia_hl7_berichten.log_tekst%TYPE)
    IS
    BEGIN
        UPDATE beh_clia_hl7_berichten
            SET bericht_tekst = TRIM (p_bericht_tekst),
                 verzonden = p_verzonden,
                 bericht_antwoord = TRIM (p_bericht_antwoord),
                 log_tekst = p_log_tekst
         WHERE clhb_id = g_clhb_id;

        COMMIT;
    END LOG;

    PROCEDURE open_verbinding
    IS
        v_hl7_svr_poort_clia   NUMBER;
        v_hl7_svr_naam_clia      kgc_systeem_parameters.standaard_waarde%TYPE;
        v_sqlerrm                  VARCHAR2 (4000);
    BEGIN
        DBMS_OUTPUT.put_line ('beh_clia_00.open_verbinding - start');

        v_hl7_svr_poort_clia := kgc_zis.gethl7poort ('HL7_SVR_POORT_CLIA');
        v_hl7_svr_naam_clia := kgc_sypa_00.systeem_waarde ('HL7_SVR_NAAM_CLIA');

        IF NVL (LENGTH (v_hl7_svr_naam_clia), 0) = 0
        THEN
            DBMS_OUTPUT.put_line ('Geen standaardwaarde gevonden voor systeemparameter HL7_SVR_NAAM_CLIA');
            RETURN;
        END IF;

        DBMS_OUTPUT.put_line ('CLIA_HL7_SVR_NAAM = ' || v_hl7_svr_naam_clia);
        DBMS_OUTPUT.
         put_line ('CLIA_HL7_SVR_POORT = ' || TO_CHAR (v_hl7_svr_poort_clia));

        IF g_connected
        THEN
             DBMS_OUTPUT.put_line (
                                    'De verbinding naar '
                                 || v_hl7_svr_naam_clia
                                 || ':'
                                 || TO_CHAR (v_hl7_svr_poort_clia)
                                 || ' is reeds geopend!');
        ELSE
            IF g_test
            THEN
                g_connected := TRUE;
            ELSE
                BEGIN
                    g_conn :=
                        UTL_TCP.
                         open_connection (remote_host   => v_hl7_svr_naam_clia,
                                                remote_port   => v_hl7_svr_poort_clia,
                                                tx_timeout      => 0.5 -- seconden! zie doc:
                                              );
                    g_connected := TRUE;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        v_sqlerrm := SQLERRM;                              -- fout opbergen

                        BEGIN                                                                  -- v2
                            UTL_TCP.close_connection (g_conn);
                        EXCEPTION
                            WHEN OTHERS
                            THEN
                                NULL;
                        END;

                        g_connected := FALSE;
                        DBMS_OUTPUT.put_line (
                                                'Fout in verbinding naar '
                                             || v_hl7_svr_naam_clia
                                             || ':'
                                             || TO_CHAR (v_hl7_svr_poort_clia)
                                             || '.');
                END;
            END IF;
        END IF;

        DBMS_OUTPUT.put_line ('beh_clia_00.open_verbinding - einde');
    EXCEPTION
        WHEN OTHERS
        THEN
            DBMS_OUTPUT.put_line ('Verbinding is niet geopend!' || SQLERRM);
    END open_verbinding;

    PROCEDURE sluit_verbinding
    IS
    BEGIN
        IF g_connected
        THEN
            g_connected := FALSE;

            IF g_test
            THEN
                NULL;
            ELSE
                UTL_TCP.close_connection (g_conn);
            END IF;
        END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
          DBMS_OUTPUT.put_line ('Verbinding is niet gesloten! '|| SQLERRM);
    END sluit_verbinding;

    FUNCTION samenstellen_bericht (
        p_i    IN number,
        x_ok            IN OUT BOOLEAN)
        RETURN VARCHAR2
    IS
        v_hl7_msg_bron      kgc_zis.hl7_msg;
        v_msg_control_id     VARCHAR2 (10);
        v_sysdate             VARCHAR2 (20)
                                     := TO_CHAR (SYSDATE, 'yyyymmddhh24miss');
        v_str_hl7             VARCHAR2 (32767);
        v_db_name varchar2(100);
    BEGIN
            SELECT global_name INTO v_db_name
            FROM GLOBAL_NAME;

            kgc_zis.g_sypa_hl7_versie := kgc_sypa_00.systeem_waarde ('HL7_VERSIE');

            if g_meet(p_i).geplande_einddatum is null then
                v_str_hl7 := 'Geplande_einddatum mag niet leeg zijn.';
                x_ok := FALSE;
            elsif g_meet(p_i).zisnr is null then
                v_str_hl7 := 'ZisNr mag niet leeg zijn.';
                x_ok := FALSE;
            else
                v_msg_control_id :=
                    SUBSTR (TO_CHAR (ABS (DBMS_RANDOM.random)), 1, 10);

                v_hl7_msg_bron ('MSH') :=
                    kgc_zis.init_msg ('MSH', kgc_zis.g_sypa_hl7_versie);
                v_hl7_msg_bron ('MSH') (1) (1) (1) := '|';
                v_hl7_msg_bron ('MSH') (2) (1) (1) := '^~\&';
                v_hl7_msg_bron ('MSH') (3) (1) (1) := 'HELI';                 --helix
                v_hl7_msg_bron ('MSH') (5) (1) (1) := 'CLIA';   -- clinical assistent
                v_hl7_msg_bron ('MSH') (7) (1) (1) := v_sysdate;
                v_hl7_msg_bron ('MSH') (9) (1) (1) := 'ORM^O01';
                v_hl7_msg_bron ('MSH') (10) (1) (1) := v_msg_control_id;
                IF v_db_name LIKE '%PROD%' THEN v_hl7_msg_bron ('MSH') (11) (1) (1) := 'P'; ELSE v_hl7_msg_bron ('MSH') (11) (1) (1) := 'T'; END IF;
                v_hl7_msg_bron ('MSH') (12) (1) (1) := kgc_zis.g_sypa_hl7_versie; --2.3?
                v_hl7_msg_bron ('MSH') (15) (1) (1) := 'NE';                 -- never
                v_hl7_msg_bron ('MSH') (16) (1) (1) := 'AL';                -- always

                v_hl7_msg_bron ('PID') :=
                    kgc_zis.init_msg ('PID', kgc_zis.g_sypa_hl7_versie);
                v_hl7_msg_bron ('PID') (3) (1) (1) := g_meet(p_i).zisnr;
                v_hl7_msg_bron ('PID') (5) (1) (1) := g_meet(p_i).achternaam;

                v_hl7_msg_bron ('ORC') :=
                    kgc_zis.init_msg ('ORC', kgc_zis.g_sypa_hl7_versie);
                v_hl7_msg_bron ('ORC') (1) (1) (1) := 'NW';               --new order
                v_hl7_msg_bron ('ORC') (2) (1) (1) := 'HELIX_' || TO_CHAR (g_meet(p_i).meet_id);
                v_hl7_msg_bron ('ORC') (9) (1) (1) := v_sysdate;
                v_hl7_msg_bron ('ORC') (13) (1) (1) := 'ANTRG';
                v_hl7_msg_bron ('ORC') (15) (1) (1) := to_char(g_meet(p_i).geplande_einddatum, 'dd-mm-yyyy');

                v_hl7_msg_bron ('OBR') :=
                    kgc_zis.init_msg ('OBR', kgc_zis.g_sypa_hl7_versie);
                v_hl7_msg_bron ('OBR') (2) (1) (1) := 'HELIX_' || TO_CHAR (g_meet(p_i).meet_id);
                v_hl7_msg_bron ('OBR') (4) (1) (1) := g_meet(p_i).stgr_code; --protocol type
                v_hl7_msg_bron ('OBR') (11) (1) (1) := g_meet(p_i).monsternummer;
                v_hl7_msg_bron ('OBR') (34) (1) (1) := g_meet(p_i).ongr_code;

                -- samenstellen string

                v_str_hl7 :=
                        kgc_zis.g_sob_char
                    || kgc_zis.hl7seg2str ('MSH', v_hl7_msg_bron ('MSH'))
                    || kgc_zis.g_cr
                    || kgc_zis.hl7seg2str ('PID', v_hl7_msg_bron ('PID'))
                    || kgc_zis.g_cr
                    || kgc_zis.hl7seg2str ('ORC', v_hl7_msg_bron ('ORC'))
                    || kgc_zis.g_cr
                    || kgc_zis.hl7seg2str ('OBR', v_hl7_msg_bron ('OBR'))
                    || kgc_zis.g_cr
                    || kgc_zis.g_eob_char
                    || kgc_zis.g_term_char;
                x_ok := TRUE;
            end if;

        RETURN v_str_hl7;
    EXCEPTION
        WHEN OTHERS
        THEN
            x_ok := FALSE;
            v_str_hl7 := ': Error: ' || SQLERRM;
            kgc_zis.LOG (v_str_hl7);
            RETURN v_str_hl7;
    END samenstellen_bericht;

    FUNCTION send_msg (p_strhl7in     IN      VARCHAR2,
                             p_timeout        IN      NUMBER,         -- tienden van seconden
                             x_strhl7out    IN OUT VARCHAR2,
                             x_msg            IN OUT VARCHAR2
                            )
        RETURN BOOLEAN
    IS
        v_ret_val             BOOLEAN := TRUE;
        v_ret_conn             PLS_INTEGER;
        v_data_available     PLS_INTEGER;
        v_nrcharrec          PLS_INTEGER;
        v_loop                 NUMBER := 0;
        v_str                  VARCHAR2 (32000);
        v_strantw             VARCHAR2 (32000);
        v_thisproc             VARCHAR2 (100) := 'beh_clia_00.send_msg';
    BEGIN
        IF NOT g_connected
        THEN
                x_msg := 'Eerst moet de verbinding geopend worden!';
        END IF;

        IF g_test
        THEN
            RETURN TRUE;
        ELSE
            kgc_zis.
             LOG (
                v_thisproc
                || ': Connectie is geopend; message wordt verzonden...');
            v_ret_conn :=
                UTL_TCP.write_raw (g_conn, UTL_RAW.cast_to_raw (p_strhl7in)); -- send request

            IF v_ret_conn = 0
            THEN
                -- aantal verzonden bytes
                x_msg :=
                    v_thisproc || ': Error: er kon geen bericht verzonden worden!';
                kgc_zis.LOG (x_msg);
                RETURN FALSE;
            END IF;

            kgc_zis.
             LOG (v_thisproc || ': Message is verzonden; wacht op antwoord...');

            LOOP
                v_loop := v_loop + 1;
                v_data_available := UTL_TCP.available (g_conn);

                -- terugkomende gegevens ophalen:
                IF v_data_available > 0
                THEN
                    BEGIN
                        LOOP
                            IF v_data_available = 0
                            THEN
                                EXIT;
                            END IF;

                            v_nrcharrec := UTL_TCP.read_text (g_conn, v_str, 1024);
                            -- zie doc:'On some platforms, this function (= available) may only return 1, to indicate that some data is available'
                            v_strantw := v_strantw || v_str;
                            v_data_available := UTL_TCP.available (g_conn);  -- meer?
                        END LOOP;
                    EXCEPTION
                        WHEN UTL_TCP.end_of_input
                        THEN
                            NULL;                                 -- end of input; geen fout!
                        WHEN OTHERS
                        THEN
                            IF SQLCODE = -29260
                            THEN
                                NULL; -- network error: TNS:connection closed; geen fout!
                            ELSE
                                RAISE;
                            END IF;
                    END;

                    kgc_zis.LOG (v_thisproc || ': Antwoord ingelezen!');
                    EXIT;
                END IF;

                IF v_loop < p_timeout
                THEN
                    DBMS_LOCK.sleep (0.1);                                        -- seconden!
                ELSE
                    -- timeout!!!
                    x_msg :=
                        'Timeout na ' || TO_CHAR (p_timeout / 10) || ' seconden!';
                    kgc_zis.LOG (v_thisproc || ': ' || x_msg);
                    v_ret_val := FALSE;
                    EXIT;
                END IF;
            END LOOP;

            IF NVL (LENGTH (v_strantw), 0) = 0 AND v_ret_val = TRUE
            THEN
                x_msg := 'Lege reactie ontvangen!';
                v_ret_val := FALSE;
            END IF;

            x_strhl7out := v_strantw;
        END IF;                                                                            -- g_test

        RETURN v_ret_val;
    EXCEPTION
        WHEN OTHERS
        THEN
            x_msg := v_thisproc || ': Error: ' || SQLERRM;
            kgc_zis.LOG (x_msg);
            RETURN FALSE;
    END send_msg;
    PROCEDURE start_verwerking (p_i IN number)
    IS
        v_str_hl7                         VARCHAR2 (32767);
        v_ok_send_msg                     BOOLEAN := FALSE;
        v_ok_samenstellen_bericht     BOOLEAN := FALSE;
        v_strantw                         VARCHAR2 (32000);
        v_msg                              VARCHAR2 (32767);
        v_log_tekst                      beh_clia_hl7_berichten.log_tekst%TYPE;
        v_clhb_id                         beh_clia_hl7_berichten.clhb_id%TYPE;
        v_verzonden                      beh_clia_hl7_berichten.verzonden%TYPE;
    BEGIN
        SELECT beh_clhb_seq.NEXTVAL INTO g_clhb_id FROM DUAL;

        INSERT INTO beh_clia_hl7_berichten (clhb_id, meet_id)
              VALUES (g_clhb_id, g_meet(p_i).meet_id);

        COMMIT;

        IF g_connected
        THEN
            v_str_hl7 :=
                samenstellen_bericht (p_i     => p_i,
                                     x_ok          => v_ok_samenstellen_bericht
                                    );

            IF v_ok_samenstellen_bericht
            THEN
                v_ok_send_msg :=
                    send_msg (p_strhl7in     => v_str_hl7,
                                 p_timeout        => 150,
                                 x_strhl7out    => v_strantw,
                                 x_msg            => v_msg
                                );

                IF v_ok_send_msg
                THEN
                    v_verzonden := 'J';
                ELSE
                    v_log_tekst := v_msg;
                    v_verzonden := 'N';
                END IF;
            ELSE
                v_log_tekst := v_str_hl7;
                v_verzonden := 'N';
            END IF;

        ELSE
            v_log_tekst := 'Kan geen verbinding maken met de server.';
            v_verzonden := 'N';
        END IF;

        LOG (p_bericht_tekst       => v_str_hl7,
              p_verzonden              => v_verzonden,
              p_bericht_antwoord   => v_strantw,
              p_log_tekst              => v_log_tekst
             );
    END start_verwerking;
procedure start_verwerking_via_job is
--v_peildatum date := beh_alfu_00.jobs_last_date('beh_clia_00.start_verwerking_via_job');

cursor c_meet is
select *
from beh_clia_orm_vw
--where peildatum >= v_peildatum
UNION -- mislukte gevallen worden voor ��n week weer meegenomen
select *
from beh_clia_orm_vw
where meet_id in
(
select meet_id
from BEH_CLIA_BERICHTEN_MISLUKT_VW
)
/*
union
select *
from beh_clia_orm_vw
where meet_id in
(
11125678
)
*/
;

r_meet c_meet%rowtype;

begin
open c_meet;
fetch c_meet bulk collect into g_meet;
close c_meet;

if g_meet.count > 0 then
    open_verbinding;
    for i in g_meet.first..g_meet.last loop
        start_verwerking(i);
        DBMS_LOCK.sleep (5);    --hoge timeout ivm clia problemen.
    end loop;
    sluit_verbinding;
end if;
exception
 when others then
    sluit_verbinding;
    dbms_output.put_line(substr(sqlerrm, 1, 2500));
end start_verwerking_via_job;
END beh_clia_00;
/

/
QUIT
