create or replace package beh_heranalyse is

  -- Hoofdprocedure voor het starten van de heranalyse.
  --
  -- Deze procedure is een wrapper om procedure verwerk_view_data en zorgt
  -- ervoor dat de resultaten d.m.v. een email verstuurd worden. 
  procedure start_heranalyse;

  -- Verwerk de eigelijke heranalyse data uit view 'beh_heranalyse_onderzoeken'.
  --
  -- De view levert meedere rijen per familieonderzoek op, deze procedure haalt
  -- de kind, moeder en vader samples hieruit. 
  --
  -- Op dit moment kunnen we alleen nog 1 kind sample en 0 of 1 sample per ouder
  -- aan maar deze procedure verwerkt wel al meerdere samples per persoon.
  procedure verwerk_view_data;

  -- Controleer en indien mogelijk verwerkt 1 heranalyse voor een familieonderzoek.
  procedure verwerk_faon( p_faon_id       in number
                        , p_tel_kind      in number
                        , p_sample_kind   in varchar2
                        , p_tel_moeder    in number
                        , p_sample_moeder in varchar2
                        , p_tel_vader     in number
                        , p_sample_vader  in varchar2
                        );

  -- Verstuur http bericht p_message naar server op p_url en geef de http response terug.
  function send_message_to_url( p_url in varchar2
                              , p_message  in CLOB
                              ) return utl_http.resp;

  -- Verstuur http bericht naar server voor heranalyse.
  -- Heranalyse wordt altijd gedaan in de context van een familie onderzoek
  -- maar er het kan voor 1 kind in dat familieonderzoek aangevraagd worden.
  -- Daarom zijn p_faon_id en p_sample_kind hier verplicht.
  function verstuur_http_bericht( p_faon_id       in number
                                , p_sample_kind   in varchar2
                                , p_sample_vader  in varchar2 := null
                                , p_sample_moeder in varchar2 := null
                                , p_email         in varchar2 := null
                                ) return number;

end beh_heranalyse;
/


create or replace package body beh_heranalyse is

  g_funct_err_cnt       number := 0; -- Tel fouten die teruggemeld moet worden naar 
                                     -- gebruikers (HERANALYSE_MAIL)

  v_crlf                CONSTANT varchar2(2) := chr(13)||chr(10);
  v_html_message        clob;
  v_count_faon_verwerkt number := 0;

  ------------------------------------------------------------------------------
  procedure log(p_text in varchar2) is
    v_text varchar2(32000);
  begin
    --dbms_output.put_line(p_text);
    v_text := p_text || v_crlf;
    v_html_message := v_html_message || v_text;
  end;


  ------------------------------------------------------------------------------
  -- Hoofdprocedure voor het starten van de heranalyse.
  --
  -- Deze procedure is een wrapper om procedure verwerk_view_data en zorgt
  -- ervoor dat de resultaten d.m.v. een email verstuurd worden. 
  procedure start_heranalyse is

    v_info_mail_sypa       kgc_systeem_parameters.code%type := 'HERANALYSE_INFO_MAIL';
    v_heranalyse_mail_sypa kgc_systeem_parameters.code%type := 'HERANALYSE_MAIL';

    v_procedure_naam  varchar2(45)  := 'beh_heranalyse.start_heranalyse'; 
    v_email_cc        varchar2(100) := 'rene.brand@radboudumc.nl';
    v_email_onderwerp varchar2(100);
    
    v_email_error       varchar2(100); -- error email naar iemand die bij fouten geinforeeard wordt.
    v_email_funct       varchar2(100); -- functionele mail naar iemand die bij een functionele fout geinforeeard wordt.
    v_email_info        varchar2(100); -- info email naar iemand die bij iedere run geinforeeard wordt.
    
    v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM' 
                                                                        , p_mede_id        => NULL
                                                                        ) , 'HelpdeskHelixCUKZ@cukz.umcn.nl' );

  begin

    --Haal het info email adres op, of null als de systeem parameter niet bestaat.
    begin
      v_email_info := kgc_sypa_00.systeem_waarde(v_info_mail_sypa);
    exception when others then
      v_email_info := null;
    end;

    --Haal het functionele email adres op, of null als de systeem parameter niet bestaat.
    begin
      v_email_funct := kgc_sypa_00.systeem_waarde(v_heranalyse_mail_sypa);
    exception when others then
      v_email_funct := null;
    end;
  
    v_email_onderwerp := 'Heranalyse aanvraag';
    v_email_error     := beh_interfaces.beh_get_user_email;
    v_html_message    := '<h3>Overzicht van aangevraagde heranalyse.</h3><ul style="list-style-type:circle">' || v_crlf;

    --
    g_funct_err_cnt := 0;
    verwerk_view_data;
    --

    log('</ul></br>Datum: ' || to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') || '</br>');
    log('Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>');
    log('</br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);

    -- Als info email sypa ingesteld is, meld alles aan <HERANALYSE_INFO_MAIL>
    if v_count_faon_verwerkt > 0 and v_email_info is not null then
      -- Stuur email over de verwerkte heranalyse aanvragen.
      send_mail_html( p_from    => v_email_afzender
                    , p_to      => v_email_info
                    , p_subject => v_email_onderwerp
                    , p_html    => v_html_message
                    );
    end if;

    -- Als er functionele fouten zijn, meld dit aan <HERANALYSE_MAIL>
    if v_count_faon_verwerkt > 0 and g_funct_err_cnt > 0 then
      if v_email_funct is not null then
        -- Stuur email over de verwerkte heranalyse aanvragen.
        send_mail_html( p_from    => v_email_afzender
                      , p_to      => v_email_funct
                      , p_cc      => v_email_cc
                      , p_subject => v_email_onderwerp
                      , p_html    => v_html_message
                      );
      else
        -- Als HERANALYSE_MAIL niet is ingesteld meld de functionele fouten
        -- aan de gebruiker die het script heeft gestart (STG mailbox).
        send_mail_html( p_from    => v_email_afzender
                      , p_to      => v_email_error
                      , p_cc      => v_email_cc
                      , p_subject => v_email_onderwerp
                      , p_html    => v_html_message
                      );
      end if;
    end if;


  exception when others then
    -- Exception mail in geval van grote fout naar STG mailbox.
    v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
    log('</ul></br><font color="red">FOUT:' || sqlerrm || '</br>'
                                       || dbms_utility.format_error_backtrace || '</br>'
                                       || dbms_utility.format_call_stack
                                       ||'</font></br></br>');

    log('</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);

    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_error
                  , p_cc      => v_email_cc
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );
    raise;

  end start_heranalyse;


  ------------------------------------------------------------------------------
  -- Verwerk de eigelijke heranalyse data uit view 'beh_heranalyse_onderzoeken'.
  --
  -- De view levert meedere rijen per familieonderzoek op, deze procedure haalt
  -- de kind, moeder en vader samples hieruit. 
  --
  -- Op dit moment kunnen we alleen nog 1 kind sample en 0 of 1 samples per ouder
  -- verwerken maar deze procedure verwerkt wel al meerdere samples per persoon.
  --
  procedure verwerk_view_data is
  
    cursor c_heranalyse is
      select faon_id, fractienummer, relatie
      from   beh_heranalyse_onderzoeken
      order by faon_id;

    v_tel_kind   NUMBER := 0;
    v_tel_moeder NUMBER := 0;
    v_tel_vader  NUMBER := 0;

    v_sample_kind   varchar2(100) := '';
    v_sample_moeder varchar2(100) := '';
    v_sample_vader  varchar2(100) := '';

    v_laatste_faon NUMBER := 0;

  begin
    v_count_faon_verwerkt := 0;

    for r_heranalyse in c_heranalyse loop
      if v_laatste_faon <> r_heranalyse.faon_id then
        if v_laatste_faon <> 0 then
          verwerk_faon(v_laatste_faon, v_tel_kind, v_sample_kind, v_tel_moeder, v_sample_moeder, v_tel_vader, v_sample_vader);
        end if;

        v_tel_kind     := 0;
        v_tel_moeder   := 0;
        v_tel_vader    := 0;
        v_sample_kind  := '';
        v_sample_moeder:= '';
        v_sample_vader := '';
        v_laatste_faon := r_heranalyse.faon_id;
      end if;

      if r_heranalyse.relatie = 'KIND' then
        v_tel_kind := v_tel_kind + 1;
        
        if v_sample_kind is not null then
          v_sample_kind := v_sample_kind || ';';
        end if;

        v_sample_kind := v_sample_kind || r_heranalyse.fractienummer;
      end if;
      
      if r_heranalyse.relatie = 'MOEDER' then
        v_tel_moeder := v_tel_moeder + 1;

        if v_sample_moeder is not null then
          v_sample_moeder := v_sample_moeder || ';';
        end if;

        v_sample_moeder := v_sample_moeder || r_heranalyse.fractienummer;
      end if;

      if r_heranalyse.relatie = 'VADER' then
        v_tel_vader := v_tel_vader + 1;

        if v_sample_vader is not null then
          v_sample_vader := v_sample_vader || ';';
        end if;

        v_sample_vader := v_sample_vader || r_heranalyse.fractienummer;
      end if;
    
    end loop;

    if v_laatste_faon <> 0 then
      verwerk_faon(v_laatste_faon, v_tel_kind, v_sample_kind, v_tel_moeder, v_sample_moeder, v_tel_vader, v_sample_vader);
    end if;
    
  end verwerk_view_data;


  ------------------------------------------------------------------------------
  -- Controleer en indien mogelijk verwerkt 1 heranalyse voor een familieonderzoek.
  procedure verwerk_faon( p_faon_id       in number
                        , p_tel_kind      in number
                        , p_sample_kind   in varchar2
                        , p_tel_moeder    in number
                        , p_sample_moeder in varchar2
                        , p_tel_vader     in number
                        , p_sample_vader  in varchar2
                        ) is
                      
    v_faon_nummer         kgc_familie_onderzoeken.familieonderzoeknr%type;
    v_bety_id_heranalyse  number;
    v_resultaat_versturen number := 0;  --Resuiltaat van het versturen. 0=OK
    
  begin

    -- Ieder verwerking van een heranalyse aanvraag wordt meegeteld
    -- ook als deze verderop fout gaat.
    v_count_faon_verwerkt := v_count_faon_verwerkt + 1;

    select bety.bety_id
    into   v_bety_id_heranalyse
    from   beh_bericht_types bety
    where  bety.code = 'HERANALYSE';
    
    begin
      select familieonderzoeknr
      into   v_faon_nummer
      from   kgc_familie_onderzoeken faon
      where  faon_id = p_faon_id;
    exception when others then
      log('<li>Fout bij aanvragen heranalyse voor familieonderzoek met faon_id ' || p_faon_id || '..<br>Familieonderzoek is onbekend!</li>' || v_crlf);
      g_funct_err_cnt := g_funct_err_cnt + 1;
      
      insert into beh_berichten (bety_id, datum_verzonden, oracle_uid, adres_verzonden, ontvanger_type, entiteit, entiteit_id, bericht_tekst, log_tekst, verzonden)
      values (v_bety_id_heranalyse, sysdate, user, 'Niet verzonden', 'SYSTEEM', 'FAON', p_faon_id, 'Niet verzonden', 'TVF: Familieonderzoek is onbekend!', 'N');
      commit;
      
      return;
    end;

    if p_tel_kind < 1 then
      log('<li>Fout bij aanvragen heranalyse voor familieonderzoek ' || v_faon_nummer || '..<br>Voor heranalyse aanvraag moet er een kind sample aanwezig zijn.</li>' || v_crlf);
      g_funct_err_cnt := g_funct_err_cnt + 1;

      insert into beh_berichten (bety_id, datum_verzonden, oracle_uid, adres_verzonden, ontvanger_type, entiteit, entiteit_id, bericht_tekst, log_tekst, verzonden)
      values (v_bety_id_heranalyse, sysdate, user, 'Niet verzonden', 'SYSTEEM', 'FAON', p_faon_id, 'Niet verzonden', 'TVF: Geen kind fractie!', 'N');
      commit;
      
      return;
    end if;
    
    if p_tel_kind > 1 then
      log('<li>Fout bij aanvragen heranalyse voor familieonderzoek ' || v_faon_nummer || '..<br>Voor heranalyse aanvraag mag maximaal 1 kind sample meegegeven worden.</li>' || v_crlf);
      g_funct_err_cnt := g_funct_err_cnt + 1;

      insert into beh_berichten (bety_id, datum_verzonden, oracle_uid, adres_verzonden, ontvanger_type, entiteit, entiteit_id, bericht_tekst, log_tekst, verzonden)
      values (v_bety_id_heranalyse, sysdate, user, 'Niet verzonden', 'SYSTEEM', 'FAON', p_faon_id, 'Niet verzonden', 'TVF: Meerdere kind fracties!', 'N');
      commit;
      
      return;
    end if;
    
    if p_tel_moeder > 1 or p_tel_vader > 1 then
      log('<li>Fout bij aanvragen heranalyse voor familieonderzoek ' || v_faon_nummer || '.<br>Voor heranalyse aanvraag mag maximaal 1 vader en 1 moeder sample meegegeven worden.</li>' || v_crlf);
      g_funct_err_cnt := g_funct_err_cnt + 1;

      insert into beh_berichten (bety_id, datum_verzonden, oracle_uid, adres_verzonden, ontvanger_type, entiteit, entiteit_id, bericht_tekst, log_tekst, verzonden)
      values (v_bety_id_heranalyse, sysdate, user, 'Niet verzonden', 'SYSTEEM', 'FAON', p_faon_id, 'Niet verzonden', 'TVF: Meerdere vader of moeder fracties!', 'N');
      commit;
      
      return;
    end if;

    -- Alles OK, dus het bericht kan verstuurd worden.
    v_resultaat_versturen := verstuur_http_bericht( p_faon_id       => p_faon_id
                                                  , p_sample_kind   => p_sample_kind
                                                  , p_sample_moeder => p_sample_moeder
                                                  , p_sample_vader  => p_sample_vader
                                                  );
    if v_resultaat_versturen = 0 then
      log( '<li>Heranalyse succesvol aangevraagd voor familieonderzoek ' || v_faon_nummer 
        || '. Kind sample is: '   || nvl(p_sample_kind, '-')
        || ', moeder sample is: ' || nvl(p_sample_moeder, '-')
        || ', vader sample is: '  || nvl(p_sample_vader, '-')
        || '</li>' || v_crlf
         );
    else
      log( '<li>Heranalyse aanvraag mislukt voor familieonderzoek ' || v_faon_nummer 
        || '. Kind sample is: '   || nvl(p_sample_kind, '-')
        || ', moeder sample is: ' || nvl(p_sample_moeder, '-')
        || ', vader sample is: '  || nvl(p_sample_vader, '-')
        || '. Controleer de pipeline voor foutmeldingen op de betreffende samples.</li>' || v_crlf
         );
      g_funct_err_cnt := g_funct_err_cnt + 1;
    end if;
    
  end verwerk_faon;
  
  
  ------------------------------------------------------------------------------
  -- Verstuur http bericht naar server voor heranalyse
  function verstuur_http_bericht( p_faon_id       in number
                                , p_sample_kind   in varchar2
                                , p_sample_vader  in varchar2 := null
                                , p_sample_moeder in varchar2 := null
                                , p_email         in varchar2 := null
                                ) return number is

    v_url    varchar2(500) := kgc_sypa_00.systeem_waarde ('BEH_HERANALYSE_SERVER');
    v_secret varchar2(500) := kgc_sypa_00.systeem_waarde ('BEH_HERANALYSE_SECRET');

    v_response           utl_http.resp;
    v_request_text       clob := '';

    v_bety_id_heranalyse number;
    v_beri_id            number;
    
    errorcode            varchar2(10) := '';
    errormessage         varchar2(2000) := '';

    v_hash_raw           raw(256);
    v_hash               varchar2(64);
    v_random             varchar2(64);

    v_return_code        number := 0;  --Return code van functie 0=OK
 
  begin

    v_random   := to_char(abs(dbms_crypto.RandomInteger));
    v_hash_raw := dbms_crypto.hash(utl_raw.cast_to_raw(v_secret || v_random), dbms_crypto.hash_sh1);
    v_hash     := lower(rawtohex(v_hash_raw));

    select bety.bety_id
    into   v_bety_id_heranalyse
    from   beh_bericht_types bety
    where  bety.code = 'HERANALYSE';
    
    --JSON versie van het bericht (wordt niet gebruikt)
    --v_request_text := '{"VADER":"' || p_sample_vader || '","MOEDER":"' || p_sample_moeder || '","KIND":"' || p_sample_kind || '"}';
    
    --Bericht in HTTP POST parameters
    v_request_text := 'KIND='   || p_sample_kind || chr(38);
    
    if p_sample_vader is not null then
      v_request_text := v_request_text || 'VADER='  || p_sample_vader || chr(38);
    end if;

    if p_sample_moeder is not null then
      v_request_text := v_request_text || 'MOEDER=' || p_sample_moeder || chr(38);
    end if;

    if p_email is not null then
      v_request_text := v_request_text || 'EMAIL='  || p_email || chr(38);
    end if;

    v_request_text := v_request_text || 'hash='   || v_hash || chr(38)
                                     || 'random=' || v_random;
    
    insert into beh_berichten (bety_id, datum_verzonden, oracle_uid, adres_verzonden, ontvanger_type, entiteit, entiteit_id, bericht_tekst)
    values (v_bety_id_heranalyse, sysdate, user, v_url, 'SYSTEEM', 'FAON', p_faon_id, v_request_text)
    returning beri_id into v_beri_id;

    if p_faon_id is null then
      raise_application_error(-20101, 'Familie onderzoek id mag niet leeg zijn bij aanvraag heranalyse! Bericht wordt niet gestuurd!');
    end if;
    
    if p_sample_kind is null then
      raise_application_error(-20102, 'Sample van kind mag niet leeg zijn bij aanvraag heranalyse! Bericht wordt niet gestuurd!');
    end if;
    
    v_response := send_message_to_url(v_url, v_request_text);

    if v_response.status_code = '200' then
      update beh_berichten
      set    log_tekst = 'OK ('||v_response.status_code||'): ' || v_response.reason_phrase
           , verzonden = 'J'
      where  beri_id   = v_beri_id;
    elsif v_response.status_code = '404' or v_response.status_code = '400' then --Bad request: De door Helix meegegeven fractie is niet gevonden
      update beh_berichten
      set    log_tekst = 'TVF: Fractie niet gevonden ('||v_response.status_code||'): ' || v_response.reason_phrase
      where  beri_id   = v_beri_id;
      v_return_code := 1;
    elsif v_response.status_code = '401' then
      update beh_berichten
      set    log_tekst = 'Login hash is niet geaccepteerd ('||v_response.status_code||'): ' || v_response.reason_phrase
      where  beri_id   = v_beri_id;
      v_return_code := 1;
    else
      update beh_berichten
      set    log_tekst = 'Error ('||v_response.status_code||'): ' || v_response.reason_phrase
      where  beri_id   = v_beri_id;
      v_return_code := 1;
    end if;
    commit;
    
    return  v_return_code;
    
  exception when others then
    errormessage := SQLERRM;

    update beh_berichten
    set    log_tekst = 'Exception! SQL error: ' || errormessage || ', http responce: (' || v_response.status_code || '): ' || v_response.reason_phrase
    where  beri_id   = v_beri_id;
    v_return_code := 1;
    commit;
    
    return v_return_code;
      
  end;  


  ------------------------------------------------------------------------------
  -- Verstuur http bericht p_message naar server op p_url en geef de http response terug.
  function send_message_to_url( p_url      in varchar2
                              , p_message  in CLOB
                              ) return utl_http.resp is

    v_request       utl_http.req;
    v_response      utl_http.resp;

    v_amount        pls_integer := 2000;
    v_offset        pls_integer := 1;
    v_req_length    binary_integer;
    v_buffer        VARCHAR2(32767);
    v_return_text   VARCHAR2(32767);

  begin

    utl_http.set_transfer_timeout(60);
    v_request := utl_http.begin_request(url => p_url, method => 'POST');  
  
    utl_http.set_header(r => v_request, name => 'user-agent',     value => 'Helix');
    utl_http.set_header(r => v_request, name => 'content-type',   value => 'application/x-www-form-urlencoded');
    utl_http.set_header(r => v_request, name => 'content-length', value => dbms_lob.getlength(p_message));
  
    v_req_length := dbms_lob.getlength (p_message);
  
    while (v_offset < v_req_length) loop
      dbms_lob.read(p_message, v_amount, v_offset, v_buffer);
      utl_http.write_text(r => v_request, data => v_buffer);
      v_offset := v_offset + v_amount;
    end loop;

    v_response := utl_http.get_response(v_request);

    begin
      loop
        utl_http.read_text(v_response, v_return_text, 32767);
        -- dbms_output.put_line('Response: ' || v_return_text);
        -- dbms_lob.writeappend (v_http_return, length(v_return_text), v_return_text);
      end loop; 
    exception when utl_http.end_of_body then
      utl_http.end_response(v_response);
    end;
    
    return v_response;
  end; 

end beh_heranalyse;
/

begin
  kgc_util_00.reconcile( p_object => 'beh_heranalyse' );
end;
/
