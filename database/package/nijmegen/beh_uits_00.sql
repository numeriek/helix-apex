CREATE OR REPLACE PACKAGE "HELIX"."BEH_UITS_00"
AS
FUNCTION rela_is_intern
(
p_rela_id number
) return varchar;
function bepaal_mediatype
(
p_best_id number
) return varchar2
;
function get_uits_id
(p_onde_id in number)
return number
;
function heeft_brief_rec
(p_uits_id in number
)
return varchar2
;
function heeft_al_bestand
(p_uits_id in number,
p_rela_id in number,
p_btyp_id in number
)
return varchar2
;
function heeft_bijlagen
(p_uits_id in number
)
return varchar2
;
function check_ramo_code
(
p_report_file in varchar2,
p_kafd_id in number,
p_ongr_id in number,
p_taal_id in number
)
return varchar2
;
PROCEDURE run_rapport
(
p_report            IN VARCHAR2 := NULL ,
p_desname           IN VARCHAR2 := NULL ,
p_onderzoeknr       IN VARCHAR2 := NULL ,
p_reden             IN VARCHAR2 := NULL ,
p_uits_id           IN NUMBER := NULL ,
p_incl_kopiehouders IN VARCHAR2 := NULL ,
p_kopie             IN VARCHAR2 := NULL ,
p_module            IN VARCHAR2 := NULL ,
p_taal              IN VARCHAR2 := NULL ,
p_rela_id           IN NUMBER := NULL )
;
function verwerk_bijlagen
(
p_uits_id in number,
p_uits_file in varchar2,
p_onde_id in number,
p_draft in varchar2 := 'N'
)
return varchar2
;
function voorlopige_versie
(
p_uits_id in number
)
return varchar2
;
procedure maak_uits_bestand
(p_onde_id in kgc_onderzoeken.onde_id%type,
p_uits_id in kgc_uitslagen.uits_id%type := null,
p_rela_id in kgc_relaties.rela_id%type  := null,
p_draft in varchar2 := 'N',
x_uits_dir_file out varchar2
)
;
procedure verwijder_uits_bestand
(p_onde_id in kgc_onderzoeken.onde_id%type)
;
procedure start_verwerking_via_job;
END beh_uits_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_UITS_00"
AS
-- als adres van een relatie 'alhier' is, wordt het beschouwd als
-- een interne relatie.
-- mediatype van een interne relatie is EPD
FUNCTION rela_is_intern
(
p_rela_id number
) return varchar is
  cursor c_rela  (p_rela_id in number)
  is
    select decode(instr(upper(nvl(rela.adres,'ONB')),'ALHIER'), 0, 'N', 'J') alhier
    from   kgc_relaties rela
    where  rela.rela_id = p_rela_id
    ;

  r_rela c_rela%ROWTYPE;

  v_tekst  varchar2(2);

BEGIN
  v_tekst   := 'N';
  IF p_rela_id is not null THEN
    OPEN c_rela (p_rela_id);
    FETCH c_rela INTO r_rela;
    IF c_rela%FOUND THEN
      v_tekst := r_rela.alhier;
    END IF;
    CLOSE c_rela;
  END IF;
  RETURN v_tekst;
END rela_is_intern;
--########################################################
--MEDIATYPE bepaald hoe een uitslag met een relatie gecommuniceerd:
-- I : relatie is intern en uitslagen worden via epd gecommuniceerd
-- er zijn twee uitzonderingen
-- 1 - uitslagen van reserch onderzoeken
-- 2 - uitslagen van patienten zonder zisnr
-- Z : relatie heeft een zorgmail uitslagen worden via mail (zorgmail) gecommuniceerd
-- O: alle andere relaties. Uitslagen worden eerst geprint en via post bezorgd.
--########################################################
function bepaal_mediatype
(
p_best_id number
) return varchar2 is
v_rela_is_intern varchar2(1);
v_rela_heeft_zorgmail varchar2(1);
v_mediatype varchar2(1);

cursor c_best is
select *
from kgc_bestanden
where best_id = p_best_id
and   entiteit_code = 'UITS'
;

r_best c_best%rowtype;

cursor c_uits (p_uits_id number) is
select *
from kgc_uitslagen
where uits_id = p_uits_id
;

r_uits c_uits%rowtype;

cursor c_onde (p_onde_id number) is
select *
from kgc_onderzoeken
where onde_id = p_onde_id
;
r_onde c_onde%rowtype;

cursor c_pers (p_pers_id number) is
select *
from kgc_personen
where pers_id = p_pers_id
;
r_pers c_pers%rowtype;

begin
-- I = Intern; gaat via EPD
-- O = Overig; gaat via Printen
-- Z = Zorgmail; gaat via zorgmail
open c_best;
fetch c_best into r_best;
close c_best;


v_rela_heeft_zorgmail := beh_zoma_00.rela_heeft_zorgmail (r_best.rela_id);

if v_rela_heeft_zorgmail = 'J' then
  v_mediatype := 'Z';
else
  v_rela_is_intern := beh_uits_00.rela_is_intern (r_best.rela_id);
  if v_rela_is_intern = 'J' then
    v_mediatype := 'I';

    --
    -- U I T Z O N D E R I N G E N: --
    --
    --1. Research onderzoeken
    open c_uits (r_best.entiteit_pk);
    fetch c_uits into r_uits;
    close c_uits;

    open c_onde (r_uits.onde_id);
    fetch c_onde into r_onde;
    close c_onde;

    if r_onde.onderzoekstype = 'R' then
      v_mediatype := 'O'; -- Overig
    end if;

    --2. patienten zonder zisnr
    if v_mediatype = 'I' then
      open c_pers( r_onde.pers_id);
      fetch c_pers into r_pers;
      close c_pers;

      if r_pers.zisnr is null then
        v_mediatype := 'O'; -- Overig
      end if;
    end if;

    --3. Interne relaties zonder EPD toegang
    if v_mediatype = 'I' then
      if upper(kgc_attr_00.waarde('KGC_RELATIES', 'AFW_MEDIATYPE', r_best.rela_id))  = 'O' then
        v_mediatype := 'O'; -- Overig
      end if;
    end if;
  else
    v_mediatype := 'O';
  end if;
end if;

return v_mediatype;
end bepaal_mediatype;
--###################################################
-- ophalen van de meest recent gewijzigde uits_id
-- Bij het aanmaken van een uitslagbestand wordt niet altijd een speciefieke
-- uits_id doorgegeven. In dat geval gaan we ervan uit dat men de meest recent
-- gewijzigde uits_id  bedoeld
--###################################################
function get_uits_id
(
p_onde_id in number
)
return NUMBER is
v_uits_id KGC_UITSLAGEN.uits_id%type;

cursor c_uits is
select uits_id
from KGC_UITSLAGEN uits
where uits.onde_id= p_onde_id
order by uits.last_update_date desc
;

begin

  v_uits_id := null;

  IF ( p_onde_id IS NOT NULL ) THEN
    OPEN  c_uits;
    FETCH c_uits INTO v_uits_id;
    CLOSE c_uits;
  END IF;

  RETURN( v_uits_id );

exception
 when others then
    return null;
end get_uits_id;
--###################################################
-- Heeft uits_id al een geregistreerde brief?
-- zo ja, het automatische verwijderen van de uitslagbestand gaat niet door
--###################################################
function heeft_brief_rec
(
p_uits_id in number
)
return varchar2 is

cursor c_brie is
select count(*) aantal
from kgc_brieven brie,
kgc_brieftypes brty,
kgc_rapport_modules ramo
where  brie.brty_id = brty.brty_id
and    brty.ramo_id = ramo.ramo_id
and    upper(ramo.code) in ('UITSLAG', 'CONTROLE')
and    uits_id = p_uits_id
;

r_brie c_brie%rowtype;

begin
open c_brie;
fetch c_brie into r_brie;
close c_brie;

if r_brie.aantal > 0 then
    return 'J';
else
    return 'N';
end if;
end heeft_brief_rec;
--###################################################
-- Heeft uits_id al een uitslagbestand?
--Zo ja: er wordt geen tweede bestand aangemaakt;
-- tenzij datum wijziging van de uitslag,  jonger is dan datum aanmaak van de uitslagbestand.
--Dit houd in dat de uitslagbestand verouderd is en nog een keer aangemaakt moet worden.
--###################################################
function heeft_al_bestand
(
p_uits_id in number,
p_rela_id in number,
p_btyp_id in number
)
return varchar2 is

v_heeft_al_bestand varchar2(1000) := 'N';

cursor c_best is
select *
from kgc_bestanden
where entiteit_code = 'UITS'
and    entiteit_pk = p_uits_id
and   rela_id = p_rela_id
and   btyp_id = p_btyp_id
 -- datum aanmaak bestand moet na datum aanmaak/update uitslag zijn
and   creation_date > ( select last_update_date
                        from kgc_uitslagen
                        where uits_id = p_uits_id)
;

begin
for r_best in c_best loop
    if (beh_alfu_00.file_exists(r_best.bestand_specificatie) = 'J') then
        v_heeft_al_bestand := 'J';
        exit;
    end if;
end loop;
return v_heeft_al_bestand;
end heeft_al_bestand;
--###################################################
-- Heeft uits_id nog bijlagen?
--###################################################
function heeft_bijlagen
(p_uits_id in number
)
return varchar2 is
v_this_prog varchar2(100) := 'beh_uits_00.heeft_bijlagen';
cursor c_best is
select best.*
from kgc_bestanden best, kgc_bestand_types btyp
where best.btyp_id = btyp.btyp_id
and    entiteit_code = 'UITS'
and    btyp.code like 'BIJLAGE%'
and    best.vervallen = 'N'
and    best.verwijderd = 'N'
and    entiteit_pk = p_uits_id
;

begin
for r_best in c_best loop
    return 'J';
end loop;

return 'N';

end heeft_bijlagen;
--###################################################
-- Komt de rapport in aanmerking voor een uitslagbestand?
-- Alleen repporten komen in aanmerking voor een uitslagbestand die
-- gekoppeld zijn aan UITSLAG of CONTROLE modules.
--###################################################
function check_ramo_code
(
p_report_file in varchar2,
p_kafd_id in number,
p_ongr_id in number,
p_taal_id in number
)
return varchar2 is

v_error_txt varchar2(4000);
v_this_prog varchar2(100) := 'beh_uits_00.check_ramo_code';

cursor c_rapp is
select rapp.*
from  kgc_rapporten rapp
,     kgc_rapport_modules ramo
,     kgc_talen taal
,     kgc_kgc_afdelingen kafd
,     kgc_onderzoeksgroepen ongr
where rapp.ramo_id = ramo.ramo_id
and   rapp.kafd_id = kafd.kafd_id
and   nvl(rapp.taal_id, (select taal_id from kgc_talen where code = 'NED') ) = taal.taal_id
and   rapp.ongr_id = ongr.ongr_id(+)
and   ramo.code in ('UITSLAG', 'CONTROLE')
and   kafd.kafd_id = p_kafd_id
and   (rapp.taal_id = p_taal_id or rapp.taal_id is null)
and   (ongr.ongr_id = p_ongr_id or ongr.ongr_id is null)
and   upper(rapp.rapport) = upper(p_report_file)
;

begin
for r_rapp in c_rapp loop
  return 'J';
end loop;
  return 'N';
exception
when others then
   v_error_txt := v_this_prog || ' => ' || SUBSTR(SQLERRM, 1, 2000);
   dbms_output.put_Line(v_error_txt);
return 'N';
end check_ramo_code;
--##################################################
--SRW is een Oracle package (geintalleerd onder SYS) waarmee kun je
-- rapport server vanuit binnen database aanroepen.
--##################################################
PROCEDURE run_rapport(
    p_report            IN VARCHAR2 := NULL ,
    p_desname           IN VARCHAR2 := NULL ,
    p_onderzoeknr       IN VARCHAR2 := NULL ,
    p_reden             IN VARCHAR2 := NULL ,
    p_uits_id           IN NUMBER := NULL ,
    p_incl_kopiehouders IN VARCHAR2 := NULL ,
    p_kopie             IN VARCHAR2 := NULL ,
    p_module            IN VARCHAR2 := NULL ,
    p_taal              IN VARCHAR2 := NULL ,
    p_rela_id           IN NUMBER := NULL )
IS
  v_this_prog varchar2(100) := 'beh_uits_00.run_rapport';

  myPlist SYS.SRW_PARAMLIST;
  myIdent SRW.Job_Ident;
  v_error_txt varchar2(4000);

  v_rapport_server_url kgc_systeem_parameters.standaard_waarde%type :=
         kgc_sypa_00.systeem_waarde('BEH_RAPPORT_SERVER_URL');

  v_rapport_server_naam kgc_systeem_parameters.standaard_waarde%type :=
         kgc_sypa_00.systeem_waarde('REPORTS_SERVER');

  v_rapport_server_user kgc_systeem_parameters.standaard_waarde%type :=
         kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER');
BEGIN
  myPlist := SRW_PARAMLIST(SRW_PARAMETER('',''));

  v_rapport_server_user := beh_cryptit.decrypt_data(v_rapport_server_user);
--#####################################
/*
                  UTL_MAIL.SEND(  sender   => 'cukz124@umcn.nl',
                      recipients => 'hamid.bourchi@radboudumc.nl',
                      subject    => 'Aanmaken Uitslagbestan',
                      message    =>
                                  'GATEWAY => ' || v_rapport_server_url ||
                                  ' SERVER => ' || v_rapport_server_naam ||
                                  ' REPORT => ' || p_report ||
                                  ' USERID => ' || v_rapport_server_user ||
                                  ' DESNAME => ' ||  p_desname ||
                                  ' P_ONDERZOEKNR => ' || p_onderzoeknr ||
                                  ' P_REDEN => '   || p_reden ||
                                  ' P_UITS_ID => '   || to_char(p_uits_id) ||
                                 ' P_INCL_KOPIEHOUDERS => '  || p_incl_kopiehouders ||
                                 ' P_KOPIE => '  || p_kopie ||
                                 ' P_MODULE => '  || p_module ||
                                 ' P_TAAL => '  || p_taal ||
                                 ' P_RELA_ID => '  || to_char(p_rela_id)
                      );
                      */
--#####################################
  srw.add_parameter(myPlist, 'GATEWAY', v_rapport_server_url);
  srw.add_parameter(myPlist, 'SERVER', v_rapport_server_naam);
  srw.add_parameter(myPlist, 'REPORT', p_report);
  srw.add_parameter(myPlist, 'USERID', v_rapport_server_user);
  srw.add_parameter(myPlist, 'DESTYPE', 'file');
  srw.add_parameter(myPlist, 'DESFORMAT', 'PDF');
  srw.add_parameter(myPlist, 'DESNAME', p_desname);
  srw.add_parameter(myPlist, 'P_ONDERZOEKNR', p_onderzoeknr);
  srw.add_parameter(myPlist, 'P_REDEN', p_reden);
  srw.add_parameter(myPlist, 'P_UITS_ID', to_char(p_uits_id));
  srw.add_parameter(myPlist, 'P_INCL_KOPIEHOUDERS', p_incl_kopiehouders);
  srw.add_parameter(myPlist, 'P_KOPIE', p_kopie);
  srw.add_parameter(myPlist, 'P_MODULE', p_module);
  srw.add_parameter(myPlist, 'P_TAAL', p_taal);
  srw.add_parameter(myPlist, 'P_RELA_ID', to_char(p_rela_id) );

  myIdent := srw.run_report(myPlist);

EXCEPTION
WHEN OTHERS THEN
    v_error_txt := v_this_prog || ' => ' || SUBSTR(SQLERRM, 1, 2000);
    dbms_output.put_Line(v_error_txt);
    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
      values('UITS', p_uits_id, 'MISLUKT', v_error_txt);

END run_rapport;
--##################################################
-- Als uits_id nog bijlagen heeft, moeten deze bijlagen eerst ge-converteerd worden
-- naar pdf en vervolgens gemerged worden met de uitslagbestand.
-- deze wordt geregeld in de soap programatuur.
--##################################################
function verwerk_bijlagen
(
p_uits_id in number,
p_uits_file in varchar2,
p_onde_id in number,
p_draft in varchar2 := 'N'
)
return varchar2 is

v_this_prog varchar2(100) := 'beh_uits_00.verwerk_bijlagen';

v_count number := 0;
v_bijlagen varchar2(32767);
v_error_txt varchar2(4000);
v_persoon_info varchar2(2500);

paramlist beh_SOAP_PARAMLIST;
v_soap_service varchar2(100) := 'verwerkbijlagen';
v_handshake kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER'));
v_return varchar2(4000);

cursor c_best is
select best.*
from kgc_bestanden best, kgc_bestand_types btyp
where best.btyp_id = btyp.btyp_id
and    entiteit_code = 'UITS'
and    btyp.code like 'BIJLAGE%'
and    best.vervallen = 'N'
and    best.verwijderd = 'N'
and    entiteit_pk = p_uits_id
order by best.volgorde, best.creation_date
;

cursor c_onde is
select onde.onde_id, onde.pers_id, taal.code taal_code
from kgc_onderzoeken onde, kgc_talen taal
where onde.taal_id = taal.taal_id(+)
and   onde_id = p_onde_id
;
r_onde c_onde%rowtype;

begin

open c_onde;
fetch c_onde into r_onde;
close c_onde;

v_persoon_info := kgc_pers_00.persoon_info( null, r_onde.onde_id, r_onde.pers_id, 'VERVOLGVEL' );

for r_best in c_best loop
    v_count := v_count+1;
    if v_count = 1 then
        v_bijlagen := r_best.bestand_specificatie;
    else
       v_bijlagen := v_bijlagen || ';' || r_best.bestand_specificatie;
    end if;
end loop;

--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));

-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'param_01', sys_context('userenv','db_name'));
beh_soap_00.add_parameter(paramlist, 'param_02', v_handshake);
beh_soap_00.add_parameter(paramlist, 'param_03', '<![CDATA[' || p_uits_file|| ']]>');
beh_soap_00.add_parameter(paramlist, 'param_04', '<![CDATA[' || v_bijlagen|| ']]>');
beh_soap_00.add_parameter(paramlist, 'param_05', nvl(r_onde.taal_code, 'NED'));
beh_soap_00.add_parameter(paramlist, 'param_06', p_draft);
beh_soap_00.add_parameter(paramlist, 'param_07', '<![CDATA[' || v_persoon_info|| ']]>');

--aanroep soapservice
v_return := beh_soap_00.call_soap_service(v_soap_service, paramlist);

return v_return;
exception
    when others then
    v_error_txt := v_this_prog || ' => ' || ':' || SUBSTR(SQLERRM, 1, 2000);

    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
    values('UITS', p_uits_id, 'MISLUKT', v_error_txt);
    return v_error_txt;
    dbms_output.put_Line(v_error_txt);

end verwerk_bijlagen;
--##################################################
-- als het om een voorlopige versie gaat (p_draft='J') moet
-- een watermark image aan de achtergrond van de uitslagbrief toegevoed
-- worden. Om aan te geven dat het om een voorlopige versie gaat en niet
-- de definitieve uitslagbestand.
-- dit gebeurt in de soap programmatuur.
-- Deze functie wordt aangeroepen vanuit helix schermen.
--##################################################
function voorlopige_versie
(
p_uits_id in number
)
return varchar2 is

v_uits_dir_file varchar2(500);

cursor c_uits is
select *
from kgc_uitslagen
where uits_id = p_uits_id
;

r_uits c_uits%rowtype;
begin
open c_uits;
fetch c_uits into r_uits;
close c_uits;

beh_uits_00.maak_uits_bestand
    (
      p_onde_id => r_uits.onde_id,
      p_uits_id => r_uits.uits_id,
      p_rela_id => r_uits.rela_id,
      p_draft => 'J',
      x_uits_dir_file => v_uits_dir_file
    );
return v_uits_dir_file;
end voorlopige_versie;
--####################################################
-- uitslag pdf bestand kan (afhankelijk van de paramters)
-- per uit_id en rela_id aangemaakt worden
-- behandelde situaties zijn:
-- 1 - uits_id => leeg, rela_id => leeg
-- wordt voor alle relaties (aanvrager en de kopiehouders) van de laatste
-- gewijzigde uits_id bestanden aangemaakt.
-- 2 - uits_id => niet leeg, rela_id => niet leeg
-- wordt voor de opgegeven rela_id en uits_id bestand aangemaakt.
-- 3 - uits_id => niet leeg, rela_id => leeg
-- wordt voor alle relaties (aanvrager en de kopiehouders) van de
-- opgegeven uits_id bestanden aangemaakt.
-- 4 - uits_id => leeg, rela_id => niet leeg
-- wordt voor de opgegeven relatie en de  laatste gewijzigde uits_id
-- bestanden aangemaakt.
--####################################################
procedure maak_uits_bestand
(p_onde_id in kgc_onderzoeken.onde_id%type,
p_uits_id in kgc_uitslagen.uits_id%type := null,
p_rela_id in kgc_relaties.rela_id%type  := null,
p_draft in varchar2 := 'N',
x_uits_dir_file out varchar2
)
is

  v_bestandsnaam varchar2(200);
  v_zisnr  kgc_personen.zisnr%type;
  v_report_file       varchar2(200);
  v_desname  kgc_bestanden.bestand_specificatie%type;
  v_this_prog varchar2(100) := 'beh_uits_00.maak_uits_bestand';
  v_count number := 0;
  v_alle_rela_ophalen varchar(1);
  v_file_exists varchar2(1000) := 'N';
  v_heeft_brief_rec varchar2(1000);
  v_heeft_al_bestand varchar2(1000) := 'N';
  v_rela_id kgc_relaties.rela_id%type := p_rela_id;
  v_check_ramo_code_ok varchar2(1);
  v_uits_id kgc_uitslagen.uits_id%TYPE := p_uits_id;
  v_ramo_code  kgc_rapport_modules.code%type;
  v_error_txt varchar2(4000);
  v_uits_file_met_bijlagen varchar2(2500);

  v_debug_stap varchar2(10);

  MISLUKT_BIJLAGEN exception;

  cursor c_onde is
  select pers.zisnr
  ,      onde.onderzoeknr
  ,      onde.ongr_id
  ,      onde.kafd_id
  ,      upper(kafd.code) kafd_code
  ,      upper(ongr.code) ongr_code
  ,      nvl(taal.code, 'NED') taal_code
  from kgc_onderzoeken onde
  ,    kgc_personen pers
  ,    kgc_kgc_afdelingen kafd
  ,    kgc_onderzoeksgroepen ongr
  ,    kgc_talen taal
  where onde.perS_id = pers.perS_id
  and onde.onde_id = p_onde_id
  and onde.kafd_id = kafd.kafd_id
  and onde.ongr_id = ongr.ongr_id
  and onde.taal_id = taal.taal_id(+)
  ;

  r_onde c_onde%rowtype;

  cursor c_uits (p2_uits_id in number) is
  select *
  from kgc_uitslagen
  where onde_id = p_onde_id
  and uits_id = p2_uits_id
  ;

  cursor c_avkh (p2_uits_id in number, p2_rela_id in number, p_alle_rela_ophalen in varchar2) is
  select rela_id
  ,      aanv_code
  ,      koho_id
  ,      decode(kopie, 'N', 'N', 'J') kopie
  ,      kafd_id
  ,      ongr_id
  ,      taal_id
  ,      onderzoeknr
  from kgc_uitslagbrief_vw
  where uits_id = p2_uits_id
  and (rela_id = p2_rela_id or p_alle_rela_ophalen = 'J')
  ;

  cursor c_btyp (p_report_file in varchar2) is
  select *
  from kgc_bestand_types
  where upper(code) = upper(p_report_file)
  ;
  r_btyp c_btyp%rowtype;
begin
     v_debug_stap := '1';

     open c_onde;
     fetch c_onde into r_onde;
     close c_onde;

     v_debug_stap := '2';

     if (v_uits_id is null) then
      v_uits_id := beh_uits_00.get_uits_id(p_onde_id); -- meest recente uits_id ophalen
     else
      v_uits_id := p_uits_id;
     end if;

     v_debug_stap := '3';

   -- voor de aanvrager en apart voor elke kopiehouder een pdf aanmaken en een bestand record aanmaken
    if (v_rela_id is null) then
      v_alle_rela_ophalen := 'J'; -- dus alle relaties ophalen
    else
      v_alle_rela_ophalen := 'N'; -- Alleen deze rela_id ophalen
    end if;

    v_debug_stap := '4';
    for r_uits in c_uits (v_uits_id) loop

       v_ramo_code := beh_epd_00.zoek_uitslag_ramo_code(r_uits.uits_id);

       v_report_file := kgc_rapp_00.rapport(
                          v_ramo_code,
                          null,
                          null,
                          null,
                          'ONDE',
                          p_onde_id );
        open c_btyp (v_report_file);
        fetch c_btyp into r_btyp;
        close c_btyp;

        for r_avkh in c_avkh(r_uits.uits_id, v_rela_id, v_alle_rela_ophalen) loop

         v_debug_stap := '5';

         -- komt rapport moduel in aanmerking voor een uitslagbestand?
         v_check_ramo_code_ok := beh_uits_00.check_ramo_code
                                    (
                                      p_report_file => v_report_file,
                                      p_kafd_id => r_avkh.kafd_id,
                                      p_ongr_id => r_avkh.ongr_id,
                                      p_taal_id => r_avkh.taal_id
                                    );
         v_debug_stap := '6';

         -- alleen als uits_id, rela_id, btyp_id combinatie nog geen uitslagbrief heeft
         -- maken we een nieuwe bestand anders niet.
         if (v_check_ramo_code_ok = 'J') then

           v_debug_stap := '7';
           if p_draft = 'N' then -- bij voorlopige versies, dit is niet interessant; dus skippen
               v_heeft_al_bestand := beh_uits_00.heeft_al_bestand (
                                    p_uits_id => r_uits.uits_id,
                                    p_rela_id => r_avkh.rela_id,
                                    p_btyp_id => r_btyp.btyp_id
                 );
            end if;

            if (v_heeft_al_bestand = 'N' or p_draft = 'J') then
              v_debug_stap := '8';

                v_bestandsnaam := kgc_nust_00.genereer_nr
                                  ( p_nummertype => 'BEST'
                                  , p_nummerstructuur => 'B_UITSLAG'
                                  , p_ongr_id => r_onde.ongr_id
                                  , p_kafd_id => r_onde.kafd_id
                                  , p_waarde3 => r_onde.zisnr
                                  , p_waarde4 => to_char(r_uits.uits_id)
                                  , p_waarde5 => to_char(r_avkh.rela_id)
                                  );

              if p_draft = 'J' then
                v_desname := kgc_sypa_00.systeem_waarde('BEH_PAD_UITS_FILE_VV')||
                                    v_bestandsnaam|| '_' ||
                                    sys_context('userenv','db_name') || '_' ||
                                    replace(user, '.', '_') || '_' ||
                                    to_char(sysdate, 'hh24miss') || '.' ||
                                    r_btyp.standaard_extentie
                                    ;
              else
                v_desname := r_btyp.standaard_pad ||v_bestandsnaam||'.'||r_btyp.standaard_extentie;
              end if;
              x_uits_dir_file := v_desname;
              v_debug_stap := '9';

                -- 5 keer proberen..
                while v_count <= 5 loop
                    v_count := v_count + 1;
                    run_rapport (p_report      => v_report_file || '.rdf'
                                 , p_desname     => v_desname
                                 , p_onderzoeknr => r_onde.onderzoeknr
                                 , p_reden       => 'pdf_aanmaken'
                                 , p_uits_id     => r_uits.uits_id
                                 , p_incl_kopiehouders => r_avkh.kopie
                                 , p_kopie       => 'N'
                                 , p_module      => v_ramo_code
                                 , p_taal        => r_onde.taal_code
                                 , p_rela_id     => r_avkh.rela_id
                              );
--#########################################
/*
                  UTL_MAIL.SEND(  sender   => 'cukz124@umcn.nl',
                      recipients => 'hamid.bourchi@radboudumc.nl',
                      subject    => 'Aanmaken Uitslagbestand Mislukt('||r_avkh.onderzoeknr ||')',
                      message    =>
                                  'p_report => ' || v_report_file || '.rdf' ||
                                  ' p_desname => ' || v_desname ||
                                  ' p_onderzoeknr => ' || r_onde.onderzoeknr ||
                                  ' p_reden => ' || 'pdf_aanmaken' ||
                                  ' p_uits_id => ' ||  to_char(r_uits.uits_id) ||
                                  ' p_incl_kopiehouders => ' || r_avkh.kopie ||
                                  ' p_kopie => '   || 'N' ||
                                  ' p_module => '   || v_ramo_code ||
                                 ' p_rela_id => '  || to_char(r_avkh.rela_id)
                      );
*/
--#########################################
                      v_file_exists := beh_alfu_00.file_exists(v_desname);

                      if (v_file_exists = 'J') then
                       exit;
                      end if;

                end loop;

                v_debug_stap := '10';

                -- alleen als bestand is aangemaakt
                if (v_file_exists = 'J') then

                  if (beh_uits_00.heeft_bijlagen(r_uits.uits_id) = 'J' or p_draft = 'J') then

                    v_uits_file_met_bijlagen := beh_uits_00.verwerk_bijlagen(
                                        p_uits_id => r_uits.uits_id,
                                        p_uits_file => v_desname,
                                        p_onde_id => p_onde_id,
                                        p_draft => p_draft
                                        );

                    if upper(substr(v_uits_file_met_bijlagen, -3, 3)) = 'PDF' then
                      null;
                    else
                      raise MISLUKT_BIJLAGEN;
                    end if;
                  end if;

                  if p_draft = 'N' then
                    insert into kgc_bestanden (
                      best_id,
                      entiteit_code,
                      entiteit_pk,
                      btyp_id,
                      locatie,
                      bestandsnaam,
                      rela_id
                      )
                      values(
                      kgc_best_seq.nextval
                    , r_btyp.enti_code
                    , r_uits.uits_id
                    , r_btyp.btyp_id
                    , r_btyp.standaard_pad
                    , v_bestandsnaam||'.'||r_btyp.standaard_extentie
                    , r_avkh.rela_id
                    );
                  end if;
                v_debug_stap := '11';

                else
                  v_debug_stap := '12';

                  v_error_txt := v_this_prog || ' => Aanmaken van uitslag file voor uits_id: ' || to_char(r_uits.uits_id) || ' is mislukt.';

                  insert into kgc_notities(entiteit, id, code, omschrijving)
                  values('UITS', v_uits_id, 'MISLUKT', v_error_txt);

                  if sys_context('userenv','db_name') like 'PROD%' then
                      UTL_MAIL.SEND(  sender   => 'cukz124@umcn.nl',
                      recipients => 'cukz124@umcn.nl',
                      subject    => 'Aanmaken Uitslagbestand Mislukt('||r_avkh.onderzoeknr ||')',
                      message    => v_error_txt)
                      ;
                  end if;

                end if;
         end if;
      end if;
     end loop;
   end loop;
exception
  when MISLUKT_BIJLAGEN then
    v_error_txt := v_this_prog || ' => toevoegen van bijlagen is mislukt: ' || ' => ' || v_uits_file_met_bijlagen;
    dbms_output.put_Line(v_error_txt);
    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
      values('UITS', v_uits_id, 'MISLUKT', v_error_txt);

    when others then
    v_error_txt := v_this_prog || ' => debug_stap=' || ':' || v_debug_stap || ' => ' || SUBSTR(SQLERRM, 1, 2000);
    dbms_output.put_Line(v_error_txt);
    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
      values('ONDE', p_onde_id, 'MISLUKT', v_error_txt);

end maak_uits_bestand;
--###################################################
-- Bij de-autoriseren van een onderzoek worden de uitslagbestanden verwijderd
-- mits ze nog niet gecommuniceerd zijn (=geen record hebben in kgc_brieven tabel)
--####################################################
procedure verwijder_uits_bestand
(p_onde_id in kgc_onderzoeken.onde_id%type)
is
v_uits_id kgc_uitslagen.uits_id%type := beh_uits_00.get_uits_id(p_onde_id);
v_this_prog varchar2(100) := 'beh_uits_00.verwijder_uits_bestand';
v_error_txt varchar2(4000);
v_heeft_brief_rec varchar2(1000);

cursor c_best (p_uits_id in kgc_uitslagen.uits_id%type) is
select best.*
from  kgc_bestanden best, kgc_bestand_types btyp
where best.btyp_id = btyp.btyp_id
and   best.entiteit_code = 'UITS'
and   best.entiteit_pk = p_uits_id
and   btyp.code = kgc_rapp_00.rapport(
                          beh_epd_00.zoek_uitslag_ramo_code(v_uits_id),
                          null,
                          null,
                          null,
                          'ONDE',
                          p_onde_id )
;

begin

v_heeft_brief_rec := beh_uits_00.heeft_brief_rec (p_uits_id => v_uits_id);

if v_heeft_brief_rec = 'J' then
    null;
else
  for r_best in c_best (v_uits_id) loop

    update kgc_bestanden
    set vervallen = 'J'
    , btyp_id = (select btyp_id from kgc_bestand_types where code = 'VERVALLEN-UITSLAG')
    where best_id = r_best.best_id
    ;

    end loop;
end if;
exception
    when others then
    v_error_txt := v_this_prog || ' => ' || ':' || SUBSTR(SQLERRM, 1, 2000);

    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
    values('ONDE', p_onde_id, 'MISLUKT', v_error_txt);

    dbms_output.put_Line(v_error_txt);
end verwijder_uits_bestand;
--####################################################
-- Deze procedure wordt (iedere 5 minuten) vanuit de database job
-- MAAK_UIT_BESTAND aangeroepen
-- Er worden alleen onderzoeken geselecteerd die in de laatste uur
-- geautoriseerd zijn. Er wordt ook een extra check gedaan of de opgehaalde
-- onderzoeken niet vervolgens weer gedeautoriseerd zijn
--####################################################
procedure start_verwerking_via_job is

v_this_prog varchar2(100) := 'beh_uits_00.start_verwerking_via_job';
v_error_txt varchar2(4000);
v_onde_id kgc_onderzoeken.onde_id%type;
v_uits_dir_file kgc_bestanden.bestand_specificatie%type;
v_file_delete varchar2(1);

cursor c_onah is
select onah.onde_id
from kgc_onde_autorisatie_historie onah, kgc_onderzoeken onde
where onde.onde_id = onah.onde_id
and onah.creation_date > (sysdate - (1/24) ) -- 1 uur
and onah.actie = 'A' --autoriseren
and onde.afgerond = 'J'
and not exists
(
select null
from kgc_onde_autorisatie_historie
where onde_id = onah.onde_id
and creation_date > onah.creation_date
and actie = 'D' --deautoriseren
)
order by onah.creation_date
;

cursor c_onah_check (p_onde_id kgc_onderzoeken.onde_id%type) is
select onah.onde_id
from kgc_onde_autorisatie_historie onah, kgc_onderzoeken onde
where onde.onde_id = onah.onde_id
and onah.actie = 'A' --autoriseren
and onde.afgerond = 'J'
and onde.onde_id = p_onde_id
and not exists
(
select null
from kgc_onde_autorisatie_historie
where onde_id = onah.onde_id
and creation_date > onah.creation_date
and actie = 'D' --deautoriseren
)
;
r_onah_check c_onah_check%rowtype;

begin

for r_onah in c_onah loop
  commit;
  v_onde_id := r_onah.onde_id;

  beh_uits_00.maak_uits_bestand(
        p_onde_id => v_onde_id,
        x_uits_dir_file => v_uits_dir_file
        );

  /*
  checken of het onderzoek, nog steeds
  aan de selectie voorwaarden voldoet en niet
  ondertussen ge-deautoriseerd is.
  in dat geval wordt aller teruggedraaid.
  */
  open c_onah_check (v_onde_id);
  fetch c_onah_check into r_onah_check;

  if c_onah_check%notfound then
    rollback;
    if length(v_uits_dir_file) > 0 then
      v_file_delete := beh_alfu_00.del_file(v_uits_dir_file);
    end if;
    v_error_txt := 'Onderzoeknr voldoet niet meer aan de voorwaarden. Uitslag bestand (' || v_uits_dir_file || ') is verwijderd.)';
    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
    values('ONDE', v_onde_id, 'MISLUKT', v_error_txt);
  end if;

  close c_onah_check;

end loop;

exception
    when others then
    v_error_txt := v_this_prog || ' => ' || ':' || SUBSTR(SQLERRM, 1, 2000);

    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
    values('ONDE', v_onde_id, 'MISLUKT', v_error_txt);

    dbms_output.put_Line(v_error_txt);

end start_verwerking_via_job;
END beh_uits_00;
/

/
QUIT
