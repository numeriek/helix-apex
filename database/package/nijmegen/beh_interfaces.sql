CREATE OR REPLACE PACKAGE "HELIX"."BEH_INTERFACES" is
procedure konelab_inlezen;
procedure konemps_inlezen;
procedure atwa_inlezen;
procedure uozk_up_inlezen;
procedure mito_dna_inlezen;
procedure cnv_wes_inlezen;
procedure genoom_cbv_codes;
function slkv_inlezen
(
p_directory in varchar2,
p_bestand in varchar2
)
return varchar2;
procedure zet_oorspr_aanvragers;
procedure kreat_inlezen
(
p_key in varchar2
);
function kasp_allel
(p_marker_name in  char
, p_ALLEL CHAR)
return varchar;
procedure KASP_inlezen;
procedure onde_aanvraag_bestanden;

function beh_get_user_email return varchar2;
procedure teksten_inlezen(p_entiteit in varchar2);
procedure stoftesten_inlezen(p_kafd_code in varchar2);

function unc_locatie(p_locatie in varchar2, p_kafd_id in kgc_kgc_afdelingen.kafd_id%type) return varchar2;
procedure quanlynx_inlezen(p_locatie in varchar2, p_bestand in varchar2);

end beh_interfaces;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_INTERFACES" is

CURSOR c_beh_prot_inlees IS
SELECT FRAGM            as CODE
     , CODE             as GEN
     , STOF_CODE        as GROEP_CODE
     , FRAGM_NUMMER     as GROEP_OMSCHR
     , STOF_OMSCHR      as PROTOCOL
     , MEET_REKEN       as VERVALLEN
     , TECH_CODE        as INDICATIE_CODE
     , MWST_CODE        as MATERIAAL1
     , STOF_VOOR_CODE   as ONDERZ_WIJZE
     , STOF_VOOR_OMSCHR as MATERIAAL2
     , ONDERZ_GROEP
FROM   beh_stoftesten_inlees
WHERE  upper(SOORT) = 'PROTOCOL'
  AND  upper(FRAGM) <> 'CODE'
  AND  upper(FRAGM) <> 'FRAGMENTEN'
ORDER BY STOF_CODE;

CURSOR c_beh_stoftest_inlees IS
SELECT SOORT
     , FRAGM
     , CODE
     , STOF_CODE
     , LEN_OK
     , FRAGM_NUMMER
     , STOF_OMSCHR
     , MEET_REKEN
     , TECH_CODE
     , MWST_CODE
     , STOF_VOOR_CODE
     , STOF_VOOR_OMSCHR
     , ONDERZ_WIJZE as SAMENSTELLING
FROM   beh_stoftesten_inlees
WHERE  upper(SOORT) = 'STOFTEST'
  AND  nvl(upper(FRAGM),'NULL') not in ('CODE', 'FRAGMENTEN')
  AND  LEN_OK = 'OK'
ORDER BY decode(UPPER(MEET_REKEN), 'SAMENSTELLING', 0, 'VOORTEST', 1, 2), STOF_CODE;



procedure konelab_inlezen is
-- zie mantisnr 0003899 voor documentatie.
type t_mdet is table of bas_meetwaarde_details%rowtype index by binary_integer;
mdet t_mdet;
mdet_leeg t_mdet;

v_stof_id kgc_stoftesten.stof_id%type;
v_aantal_decimalen kgc_stoftesten.aantal_decimalen%type;
v_mdet_id bas_meetwaarde_details.mdet_id%type;
v_waarde bas_meetwaarde_details.waarde%type;

-- ophalen de ingelezen gegevens
cursor c_ingelezen_gegevens is
select  substr(trim(upper(kolom01)), 1, 12) fractienummer,
        trim(kolom03) konelab_code,
        replace(trim(kolom05), ',', '.') mdet_waarde
        from beh_konelab_inlees
where exists (select * from bas_fracties frac where upper(frac.fractienummer) = substr(trim(upper(kolom01)), 1, 12))
order by trim(upper(kolom01)), trim(kolom03)
;

-- konelab codes die in de bestand opgenomen zijn
-- zijn in Helix aan stoftest gekopeld via
-- attributen. Deze worden hier opgehaald.
cursor c_stof (p_attr_waarde varchar2) is
select stof.stof_id stof_id, stof.code stof_code, atwa.waarde atwa_waarde
from kgc_stoftesten stof,
     kgc_attributen attr,
     kgc_attribuut_waarden atwa
where stof.stof_id = atwa.id
and attr.attr_id = atwa.attr_id
and   upper(attr.code) like (upper('KONELAB_CODE%'))
and   upper(attr.tabel_naam) = upper('KGC_STOFTESTEN')
and   upper(atwa.waarde) = upper(p_attr_waarde)
and   stof.mwst_id = (  select mwst_id
                        from   kgc_meetwaardestructuur
                        where  upper(code) = upper('GEM6'))
;

cursor c_meet (p_fractienummer varchar2, p_stof_id number) is
select meet.meet_id
from bas_fracties frac
,    bas_metingen meti
,    bas_meetwaarden meet
,    kgc_stoftesten stof
where frac.frac_id = meti.frac_id
and   meti.meti_id = meet.meti_id
and   meet.stof_id = stof.stof_id
and   upper(frac.fractienummer) = upper(p_fractienummer)
and   upper(meti.afgerond) = upper('N')
and   upper(meet.afgerond) = upper('N')
and   stof.stof_id = p_stof_id
order by meet.meet_id
;

-- ophalen aantal decimale op basis van meet_id
cursor c_aantal_decimalen (p_meet_id number) is
select nvl(nvl(prst.aantal_decimalen, stof.aantal_decimalen),0) aantal_decimalen
from    bas_meetwaarden meet,
        bas_metingen meti,
        kgc_stoftesten stof,
        kgc_stoftestgroepen stgr,
        kgc_protocol_stoftesten prst
where meet.stof_id = stof.stof_id
and   stof.stof_id = prst.stof_id(+)
and   prst.stgr_id = stgr.stgr_id
and   meet.meti_id = meti.meti_id
and   meti.stgr_id = stgr.stgr_id
and   meet.meet_id = p_meet_id
;
r_aantal_decimalen c_aantal_decimalen%rowtype;

-- alle meetwaarde_details ophalen.
-- op basis van de volgorde van de laatste ingevoerde
-- meetwaarde_details.
-- Bijv. als meetwaarde_details 4 ingevoerd
-- is, dan wordt vanaf volgorde 5 opgehaald.
cursor c_mdet (p_meet_id number) is
select *
from bas_meetwaarde_details mdet
where mdet.meet_id = p_meet_id
and   upper(mdet.prompt) <> upper('Gemiddelde')
and   mdet.waarde is null
and   mdet.volgorde > nvl(
                    (  select max(mdet2.volgorde)
                    from bas_meetwaarde_details mdet2
                    where mdet2.meet_id = mdet.meet_id
                    and   upper(mdet2.prompt) <> upper('Gemiddelde')
                    and   mdet2.waarde is not null)
                    ,0)
order by mdet.volgorde
;
       begin
       for r_ingelezen_gegevens in c_ingelezen_gegevens loop

    for r_stof in c_stof(r_ingelezen_gegevens.konelab_code) loop

        for r_meet in c_meet(r_ingelezen_gegevens.fractienummer, r_stof.stof_id) loop

            -- array wordt eerst gereset (leeg gemaakt)
            mdet := mdet_leeg;
            -- alle meetwaarde_details wordt in een array gestopt.
            -- deze omdat we deze met elkaar willen kunnen vergelijken.

            open c_mdet (r_meet.meet_id);
            fetch c_mdet bulk collect into mdet;
            close c_mdet;

            if mdet.count > 0 then

                for i in mdet.first..mdet.last loop
                    v_mdet_id := null;
                    v_waarde := r_ingelezen_gegevens.mdet_waarde;
                    -- eeste mdet is makkelik;
                    -- 1 - als eerst record van een duplo is
                    -- (duplo's zijn: 1 en 2, 4 en 5, 6 en 7)
                    -- 2 - en als een volgende record heeft
                    -- (volgorde 1, moet altijd een volgorde 2 hebben
                    -- en volgorde 4 moet altijd een volgorde 5 hebben etc)
                    -- dan wegschrijven!
                    if mdet(i).volgorde in (1, 4, 6) then
                        if mdet.exists(mdet.next(i)) then
                            v_mdet_id := mdet(i).mdet_id;
                            exit;
                        end if;
                    else
                    -- bij tweede record van een duplo
                    -- (duplo's zijn: 1 en 2, 4 en 5, 6 en 7)
                    -- moeten meer dingen gecheckt worden:

                    -- 1 - ze moeten de laatste ingevoerde duplo zijn:
                    -- dus in onderstaande voorbeeld wordt de waarde bij
                    -- volgorde 2 weggeshcreven:
                    -- bestaande records zijn:
                    -- volgorde 1 is not null
                    -- volgorde 2 is null
                        if not mdet.exists(mdet.next(i)) then
                            v_mdet_id := mdet(i).mdet_id;
                            exit;
                    -- 2 - als er nog meer duplo's zijn dan wordt de waarde
                    -- bij de eerste volgorde van de laatste ingevoerde
                    -- duplo weggeschreven:
                    -- dus in onderstaande voorbeeld wordt de waarde bij
                    -- volgorde 4 weggeshcreven en niet bij volgorde 2:

                    -- volgorde 1 is not null
                    -- volgorde 2 is null
                    -- volgorde 4 is null
                    -- volgorde 5 is null
                        else
                            if mdet(mdet.next(i)).volgorde in(4, 6) then
                                v_mdet_id := mdet(mdet.next(i)).mdet_id;
                                exit;
                            end if;
                        end if;
                    end if;
                    end loop;

                -- aantal decimalen ophalen
                open c_aantal_decimalen (r_meet.meet_id);
                fetch c_aantal_decimalen into r_aantal_decimalen;
                close c_aantal_decimalen;

                -- decimalen te verwerken
                v_waarde := KGC_FORMAAT_00.WAARDE(p_waarde => v_waarde, p_aantal_decimalen => r_aantal_decimalen.aantal_decimalen);

                -- wegschrijven van de uiteindelijke waarde in bas_meetwaarde_details:
                update bas_meetwaarde_details
                set    waarde = v_waarde
                where  mdet_id = v_mdet_id
                ;
                -- wegschrijven van de waarde, als meetwaarde, bij bas_meetwaarden. Tegelijkertijd
                -- worden de eventuele berekeningen (afhankelijk van meetwaarde structuur)
                -- gedaan.
                bas_mdet_00.zet_meetwaarde(p_mdet_id => v_mdet_id, p_meet_id => r_meet.meet_id);

                commit;
            end if;

        end loop;

    end loop;

end loop;
end konelab_inlezen;
procedure KoneMPS_inlezen is
-- Kolom01 bevat monsternummer
-- Kolom 2 tm 4 en kolom 5 tm 7 bevatten resp DBT en DMB gegevens
-- van de set van 3 kolommen is altijd maar 1 kolom voorzien van gegevens

-- Logging van verwerking wordt weggeschreven in naar BEH_KONEMPS_VERWERKT

-- DECLARE
   v_logging beh_konemps_verwerkt.status%type;
   v_meet_id_DBT bas_meetwaarden.meet_id%type;
   v_meet_id_DMB bas_meetwaarden.meet_id%type;
   v_aantal_dec_DBT number(1);
   v_aantal_dec_DMB number(1);
   v_count_kgc_monsters number(1);
   v_count_ingelezen_monsters number(2);
   v_count_meetwaarden_dbt number(2);
   v_count_meetwaarden_dmb number(2);
   v_count_meetwaarden_dbt_null number(1);
   v_count_meetwaarden_dmb_null number(1);

-- inlezen mogelijke monsternummers uit beh_konemps_inlees
CURSOR c_ingelezen_gegevens IS
SELECT upper(trim(kolom01)) monsternummer,
            replace(trim(nvl(nvl(kolom02,kolom03),kolom04)), ',', '.') dbt_waarde,
            replace(trim(nvl(nvl(kolom05,kolom06),kolom07)), ',', '.') dmb_waarde,
            decode(kolom02, null, 0, 1) kolom01_dbt_waarde,
            decode(kolom03, null, 0, 1) kolom02_dbt_waarde,
            decode(kolom04, null, 0, 1) kolom03_dbt_waarde,
            decode(kolom05, null, 0, 1) kolom01_dmb_waarde,
            decode(kolom06, null, 0, 1) kolom02_dmb_waarde,
            decode(kolom07, null, 0, 1) kolom03_dmb_waarde
       FROM beh_konemps_inlees
       WHERE     kolom01 is not null and
                      length(kolom01) <= 20 and
                      kolom01 not like 'Konelab%';
       r_ingelezen_gegevens c_ingelezen_gegevens%ROWTYPE;

-- Bepalen bestaan van bijbehorende regels en eventuele meet_id in BAS_meetwaarden
 cursor c_meetwaarden (p_monsternummer kgc_monsters.monsternummer%type, p_stof_code kgc_stoftesten.code%type) is
  select (meet.meet_id)
                            from kgc_monsters mons,
                                    bas_fracties frac,
                                    bas_metingen meti,
                                    bas_meetwaarden meet,
                                    kgc_stoftesten stof,
                                    kgc_stoftestgroepen stgr
                             where
                                    mons.mons_id = frac.mons_Id and
                                    frac.frac_id = meti.frac_id and
                                    meti.meti_id = meet.meti_id and
                                    meti.stgr_id = stgr.stgr_id and
                                    meet.stof_id = stof.stof_id and
                                    mons.monsternummer =  p_monsternummer and
                                    meet.afgerond = 'N' and
                                    meet.stof_id = (select stof.stof_id from kgc_stoftesten stof where code = p_stof_code); --MPSDBT  of MPSDMB

BEGIN
     for r_ingelezen_gegevens in c_ingelezen_gegevens loop
     v_meet_id_DBT := null;
     v_meet_id_DMB := null;
           --controle of monsternummer meerdere keren is ingelezen
           select count(*) into v_count_ingelezen_monsters from beh_konemps_inlees where upper(trim(kolom01)) = r_ingelezen_gegevens.monsternummer;
           if v_count_ingelezen_monsters = 1 then
                --controle bestaand monsternummer
                select count(*) into v_count_kgc_monsters from kgc_monsters where monsternummer = r_ingelezen_gegevens.monsternummer;
                 if v_count_kgc_monsters = 1 then
                        -- Als juist aantal aangeleverde DBT- en DMB- waarden
                        if ((r_ingelezen_gegevens.kolom01_dbt_waarde+r_ingelezen_gegevens.kolom02_dbt_waarde+r_ingelezen_gegevens.kolom03_dbt_waarde) = 1) and
                           ((r_ingelezen_gegevens.kolom01_dmb_waarde+r_ingelezen_gegevens.kolom02_dmb_waarde+r_ingelezen_gegevens.kolom03_dmb_waarde) = 1)
                                 then
                                 -- Controle of benodigde regels in BAS_meetwaarden bestaan
                                    v_count_meetwaarden_DBT := Beh_hefu_00.aantal_mons_stoftesten( p_monsternummer => r_ingelezen_gegevens.monsternummer, p_stof_code => 'MPSDBT');
                                    v_count_meetwaarden_DMB := Beh_hefu_00.aantal_mons_stoftesten( p_monsternummer => r_ingelezen_gegevens.monsternummer, p_stof_code => 'MPSDMB');
                                  --als benodigde regels in BAS_meetwaarden voor DBT en DMB bestaan, dan
                               if v_count_meetwaarden_DBT = 1 and v_count_meetwaarden_DMB = 1
                               then
                                      -- Bepalen bijbehorende Meet_ID van betreffende monsternummer voor MPSDBT en MPSDMB.
                                      open c_meetwaarden  (r_ingelezen_gegevens.monsternummer, 'MPSDBT');
                                      fetch c_meetwaarden into v_meet_id_DBT;
                                      close c_meetwaarden;
                                      open c_meetwaarden (r_ingelezen_gegevens.monsternummer, 'MPSDMB');
                                      fetch c_meetwaarden into v_meet_id_DMB;
                                      close c_meetwaarden;
                                      -- Controle of meetwaarden leeg zijn
                                       select count(*) into v_count_meetwaarden_dbt_null from bas_meetwaarden meet where meet.meet_id = v_meet_id_DBT and meet.meetwaarde is null;
                                       select count(*) into v_count_meetwaarden_dmb_null from bas_meetwaarden meet where meet.meet_id = v_meet_id_DMB and meet.meetwaarde is null;
                                      if v_count_meetwaarden_dbt_null = 1 and v_count_meetwaarden_dmb_null = 1
                                      then
                                            -- Bepalen aantal decimalen DBT en DMB
                                            -- aantal decimalen ophalen
                                             v_aantal_dec_DBT := Beh_hefu_00.aantal_decimalen( p_meet_id => v_meet_id_DBT);
                                             v_aantal_dec_DMB := Beh_hefu_00.aantal_decimalen( p_meet_id => v_meet_id_DMB);
                                            -- decimalen te verwerken
                                             r_ingelezen_gegevens.dbt_waarde := KGC_FORMAAT_00.WAARDE(p_waarde => r_ingelezen_gegevens.dbt_waarde, p_aantal_decimalen => v_aantal_dec_DBT);
                                             r_ingelezen_gegevens.dmb_waarde := KGC_FORMAAT_00.WAARDE(p_waarde => r_ingelezen_gegevens.dmb_waarde, p_aantal_decimalen => v_aantal_dec_DMB);
                                            -- update waarde DBT
                                                update bas_meetwaarden meet
                                                   set meet.meetwaarde = r_ingelezen_gegevens.dbt_waarde
                                                    where meet.meet_id = v_meet_id_DBT and meet.meetwaarde is null;
                                                    -- update waarde DMB
                                                update bas_meetwaarden meet
                                                   set meet.meetwaarde = r_ingelezen_gegevens.dmb_waarde
                                                    where meet.meet_id = v_meet_id_DMB and meet.meetwaarde is null;
                                                    -- Logging verwerkte waarden
                                                v_logging := 'Waarden verwerkt';
                                      commit;
                                      else
                                           v_logging := 'DBT- en\of DMB-waarde zijn niet leeg. Wijzigen alleen handmatig toegestaan.';
                                      end if;
                               else
                                        -- Logging als regels in BAS_meetwaarden niet bestaan.
                                      if (v_count_meetwaarden_DBT > 1 or v_count_meetwaarden_DMB > 1) then
                                           v_logging := 'DBT en\of DMB hebben ' || v_count_meetwaarden_DBT || ' regels beschikbaar in meetwaarden-tabel. Maximaal 1 regel toegestaan.';
                                      else
                                           v_logging := 'Geen regels om MPSDx-waarden in op te slaan. (Geen protocol of meeting afgerond)';
                                      end if;
                               end if;
                        else
                            -- Logging als waarden niet correct
                                       if (r_ingelezen_gegevens.kolom01_dbt_waarde+r_ingelezen_gegevens.kolom02_dbt_waarde+r_ingelezen_gegevens.kolom03_dbt_waarde) < 1 then r_ingelezen_gegevens.dbt_waarde := 'Geen DBT_waarde';
                                       else
                                              if (r_ingelezen_gegevens.kolom01_dbt_waarde+r_ingelezen_gegevens.kolom02_dbt_waarde+r_ingelezen_gegevens.kolom03_dbt_waarde) > 1 then r_ingelezen_gegevens.dbt_waarde := 'Teveel DBT_waarden';
                                              end if;
                                       end if;
                                       if (r_ingelezen_gegevens.kolom01_dmb_waarde+r_ingelezen_gegevens.kolom02_dmb_waarde+r_ingelezen_gegevens.kolom03_dmb_waarde) < 1 then r_ingelezen_gegevens.dmb_waarde := 'Geen DMB_waarde' ;
                                       else
                                              if (r_ingelezen_gegevens.kolom01_dmb_waarde+r_ingelezen_gegevens.kolom02_dmb_waarde+r_ingelezen_gegevens.kolom03_dmb_waarde) > 1 then r_ingelezen_gegevens.dmb_waarde := 'Teveel DMB_waarden';
                                              end if;
                                       end if;
                                       v_logging := 'Foutieve DBT- en\of DMB-waarden';
                        end if;
                 else
                           -- Logging als Monsternummer niet bestaat
                           v_logging := 'Monsternummer onbekend';
                 end if;
           else
                   -- Logging als Monsternummer meerdere keren wordt ingelezen
                   v_logging := 'Waarden NIET verwerkt. Monsternummer meerdere keren in batch aangeboden';
           end if;
     Insert INTO BEH_KONEMPS_VERWERKT (MONSTERNUMMER, MEET_ID_DBT, MEET_ID_DMB, WAARDE_DBT, WAARDE_DMB, STATUS)
            VALUES(r_ingelezen_gegevens.monsternummer, v_meet_id_DBT, v_meet_id_DMB, r_ingelezen_gegevens.dbt_waarde, r_ingelezen_gegevens.dmb_waarde, v_logging);
     commit;
     end loop;
END KoneMPS_inlezen;
procedure atwa_inlezen is
v_log_tekst varchar2(4000);
v_atwa_id varchar2(10);

cursor c_attr (p_tabel_naam varchar2, p_attr_code varchar2) is
select attr_id
from   kgc_attributen
where upper(tabel_naam) = p_tabel_naam
and   upper(code) = p_attr_code
;
r_attr c_attr%rowtype;

cursor c_ingelezen is
select  trim(upper(attr_tabel_naam)) tabel_naam
,       trim(upper(attr_code)) attr_code
,       to_number(trim(atwa_id)) atwa_id
,       trim(atwa_waarde) atwa_waarde
from beh_atwa_inlees
order by to_number(trim(atwa_id))
;

cursor c_atwa (p_atwa_id number, p_attr_id number) is
select *
from kgc_attribuut_waarden
where attr_id = p_attr_id
and  id = p_atwa_id
;
r_atwa c_atwa%rowtype;

begin
for r_ingelezen in c_ingelezen loop

    v_atwa_id := to_char(r_ingelezen.atwa_id);

    open c_attr (r_ingelezen.tabel_naam, r_ingelezen.attr_code);
    fetch c_attr into r_attr;

    if c_attr%found then

        open c_atwa(r_ingelezen.atwa_id, r_attr.attr_id);
        fetch c_atwa into r_atwa;

        if c_atwa%found then
            update kgc_attribuut_waarden
            set waarde = r_ingelezen.atwa_waarde
            where atwa_id = r_atwa.atwa_id
            ;
        else
            insert into kgc_attribuut_waarden
            (attr_id, id, waarde)
            values(r_attr.attr_id, r_ingelezen.atwa_id, r_ingelezen.atwa_waarde)
            ;
        end if;

        close c_atwa;
    end if;
    close c_attr;

end loop;

commit;

exception
  when others then
  v_log_tekst := SQLERRM;

  rollback;

  update beh_atwa_inlees
  set log_tekst = v_log_tekst
  where atwa_id = v_atwa_id
  ;
  commit;
  DBMS_OUTPUT.put_line(v_log_tekst);
end atwa_inlezen;
--############################################################################
-- uozk_up bestanden inlezen. Een normale regel in inlees bestand ziet zo eruit:
-- 31,BU12_00831$UOZK_UP_S.D,B12_2309,azelainezuur,8.384
-- volgnr, monsternummer$stgr_code_S.D, onderzoeknr, stof_omsch, meetwaarde
-- Verder geen bijzonderheden. alleen:
-- 1 - De meetwaarden van dubbel voorkomende stoftesten, moeten opgeteld worden
-- 2 - Bij het wegscrhijving van meetwaarden moet een ROUND functie toegepast worden.
--############################################################################
procedure uozk_up_inlezen is
cursor c_inge is
select  max(trim(upper(replace(kolom03,'_', '/')))) onderzoeknr
,		replace(substr(kolom02, 1, instr(kolom02, '$')-1 ), '_', '/') monsternummer
,		substr(kolom02, instr(kolom02, '$')+1, instr(kolom02, '_', -1)-instr(kolom02, '$')-1 ) stgr_code
,   trim(upper(kolom04)) stof_omsch
-- Let Op! "replace" is alleen nodig als je het via sqlplus draait!
--,   sum(to_number( trim( replace(kolom05, '.', ',' ) ) ) ) meetwaarde
,   sum(to_number( trim(kolom05) ) ) meetwaarde
from beh_konelab_inlees
where exists
(
select *
from kgc_stoftesten stof, kgc_kgc_afdelingen kafd
where stof.kafd_id = kafd.kafd_id
and   upper(omschrijving) = trim(upper(kolom04))
and   kafd.code = 'METAB'
)
group by
		substr(kolom02, 1, instr(kolom02, '$')-1 )
,		substr(kolom02, instr(kolom02, '$')+1, instr(kolom02, '_', -1)-instr(kolom02, '$')-1 )
,   trim(upper(kolom04))
;
cursor c_meet (p_onderzoeknr varchar2, p_monsternummer varchar2, p_stgr_code varchar2, p_stof_omsch varchar2) is
select meet_id
from  kgc_onderzoeken onde
,     kgc_onderzoek_monsters onmo
,     kgc_monsters mons
,     bas_fracties frac
,     bas_metingen meti
,     bas_meetwaarden meet
,     kgc_stoftestgroepen stgr
,     kgc_stoftesten stof
where onde.onde_id = onmo.onde_id
and   onmo.mons_id = mons.mons_id
and   mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   onde.onde_id = meti.onde_id(+)
and   meti.meti_id = meet.meti_id
and   meti.stgr_id = stgr.stgr_id
and   meet.stof_id = stof.stof_id
and   onde.onderzoeknr = p_onderzoeknr
and   mons.monsternummer = p_monsternummer
and   stgr.code = p_stgr_code
and   upper(stof.omschrijving) = p_stof_omsch
and   meti.afgerond = 'N'
and   meet.afgerond = 'N'
;

begin
for r_inge in c_inge loop

  for r_meet in c_meet(r_inge.onderzoeknr, r_inge.monsternummer, r_inge.stgr_code, r_inge.stof_omsch)
  loop
    update bas_meetwaarden
    set meetwaarde = to_char(round(r_inge.meetwaarde))
    where meet_id = r_meet.meet_id
    ;
  end loop;

end loop;
commit;

exception
when others then
  rollback;
  dbms_output.put_line(substr (sqlerrm, 1, 2500));
end uozk_up_inlezen;
procedure mito_dna_inlezen is
v_stof_code kgc_stoftesten.code%type;
v_mwst_code kgc_meetwaardestructuur.code%type := 'DDMITOCHIP';
v_mede_id kgc_medewerkers.mede_id%type;
v_stof_prefix varchar2(5) := 'ZDM_';
v_referentie varchar2(2500);
v_meetwaarde varchar2(2500);
v_waarde varchar2(2500);
v_prompt kgc_mwst_samenstelling.prompt%type := 'ALLES';
cursor c_mwst is
select mwst_id
from kgc_meetwaardestructuur
where upper(code) = upper(v_mwst_code)
;
r_mwst c_mwst%rowtype;

cursor c_stof (p_stof_code varchar2) is
select *
from kgc_stoftesten
where upper(code) = upper(p_stof_code)
;
r_stof c_stof%rowtype;

cursor c_mwss (p_prompt varchar2) is
    select trim(upper(mwss.prompt)) prompt, mwss.volgorde
    from   kgc_meetwaardestructuur mwst, kgc_mwst_samenstelling mwss
    where  mwst.mwst_id = mwss.mwst_id
    and    upper(mwst.code) = upper(v_mwst_code)
    and    (upper(prompt) = upper(p_prompt) or upper(p_prompt) = 'ALLES')
    order by mwss.volgorde
;

cursor c_inge is
    select trim(kolom1) onderzoeknr
    ,      trim(kolom2) monsternummer
    ,      trim(kolom3) fractienummer
    ,      trim(kolom4) stgr_code
    ,      trim(kolom5) input_string
    ,      trim(kolom6) categorie
    ,      trim(kolom7) interpretatie
    ,      decode(trim(upper(kolom4)), 'D_MTDNA_T', '.', ' ') separator
    from   beh_mito_dna_tmp
    order by kolom1
 ;

cursor c_meti (p_onderzoeknr varchar2, p_monsternummer varchar2, p_fractienummer varchar2, p_stgr_code varchar2)is
select meti.*
from kgc_onderzoeken onde
,   kgc_onderzoek_monsters onmo
,   kgc_monsters mons
,   bas_fracties frac
,   bas_metingen meti
,   kgc_stoftestgroepen stgr
where onde.onde_id = onmo.onde_id
and   onmo.mons_id = mons.mons_id
and   mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   meti.stgr_id = stgr.stgr_id
and   meti.onde_id = onde.onde_id(+)
and   onde.onderzoeknr = p_onderzoeknr
and   mons.monsternummer = p_monsternummer
and   frac.fractienummer = p_fractienummer
and   stgr.code = upper(p_stgr_code)
and   meti.afgerond = 'N'
;
r_meti c_meti%rowtype;

cursor c_meet (p_onderzoeknr varchar2, p_monsternummer varchar2, p_fractienummer varchar2, p_stgr_code varchar2, p_stof_code varchar2) is
select meet.*
from kgc_onderzoeken onde
,   kgc_onderzoek_monsters onmo
,   kgc_monsters mons
,   bas_fracties frac
,   bas_metingen meti
,   bas_meetwaarden meet
,   kgc_stoftesten stof
,   kgc_stoftestgroepen stgr
where onde.onde_id = onmo.onde_id
and   onmo.mons_id = mons.mons_id
and   mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   meti.meti_id = meet.meti_id
and   meti.onde_id = onde.onde_id(+)
and   meet.stof_id = stof.stof_id
and   meti.stgr_id = stgr.stgr_id
and   onde.onderzoeknr = p_onderzoeknr
and   mons.monsternummer = p_monsternummer
and   frac.fractienummer = p_fractienummer
and   stgr.code = upper(p_stgr_code)
and   upper(stof.code) = upper(p_stof_code)
and   meti.afgerond = 'N'
and   meet.afgerond = 'N'
;
r_meet c_meet%rowtype;

cursor c_mdet (p_onderzoeknr varchar2, p_monsternummer varchar2, p_fractienummer varchar2, p_stgr_code varchar2, p_stof_code varchar2, p_prompt varchar2) is
    select mdet.*
    from kgc_onderzoeken onde
    ,   kgc_onderzoek_monsters onmo
    ,   kgc_monsters mons
    ,   bas_fracties frac
    ,   bas_metingen meti
    ,   bas_meetwaarden meet
    ,   bas_meetwaarde_details mdet
    ,   kgc_stoftesten stof
    ,   kgc_stoftestgroepen stgr
    where onde.onde_id = onmo.onde_id
    and   onmo.mons_id = mons.mons_id
    and   mons.mons_id = frac.mons_id
    and   frac.frac_id = meti.frac_id
    and   meti.meti_id = meet.meti_id
    and   meti.onde_id = onde.onde_id(+)
    and   meet.meet_id = mdet.meet_id
    and   meet.stof_id = stof.stof_id
    and   meti.stgr_id = stgr.stgr_id
    and   onde.onderzoeknr = p_onderzoeknr
    and   mons.monsternummer = p_monsternummer
    and   frac.fractienummer = p_fractienummer
    and   upper(stgr.code) = upper(p_stgr_code)
    and   upper(stof.code) = upper(p_stof_code)
    and   upper(mdet.prompt) = upper(p_prompt)
    and   meti.afgerond = 'N'
    and   meet.afgerond = 'N'
;
r_mdet c_mdet%rowtype;

begin
    open c_mwst;
	fetch c_mwst into r_mwst;
	close c_mwst;

    v_mede_id := kgc_mede_00.medewerker_id(user);
    -- loop begint met het ophalen van de kolommen van meetwaarde structuur zoals: Ref., meetw., categorie en interpretatie
--    for r_mwss in c_mwss loop
    -- Vervolgens alle regels van de tussen tabel lezen
        for r_inge in c_inge loop
    -- kolom 5 van tussen tabel bevat stoftest code, referentie en meetwaarde. Deze halen we hier uit elkaar.

            if r_inge.input_string = 'Haplogroup' then
              v_stof_code	:= upper(trim(r_inge.input_string));
              v_prompt := upper('Categorie');
            else
              v_stof_code  := trim(v_stof_prefix || substr(r_inge.input_string, instr(r_inge.input_string, r_inge.separator) + 1, 5));
              v_prompt := 'ALLES';
            end if;

            v_referentie := trim(substr(r_inge.input_string, instr(r_inge.input_string, r_inge.separator) + 6, 1));
            v_meetwaarde := trim(substr(r_inge.input_string, instr(r_inge.input_string, r_inge.separator) + 8));

  -- Afhanelijk van over welke kolom van meetwaarde structuur gaat, wordt de waarde bepaald.

            for r_mwss in c_mwss(v_prompt) loop

            if r_inge.input_string = 'Haplogroup' then
              v_waarde := r_inge.interpretatie;
            else
              v_waarde :=
                case r_mwss.prompt
                    when upper('Ref.') then v_referentie
                    when upper('Meetw.') then v_meetwaarde
                    when upper('Categorie') then r_inge.categorie
                    when upper('Interpretatie') then r_inge.interpretatie
                    else '?'
                end;
           end if;
			-- Haplogroep stoftest is een uitzondering
      -- waarde van een Haplogroep wordt in mwdet.prompt "Categorie" weggeschreven.
            open c_mdet(r_inge.onderzoeknr, r_inge.monsternummer, r_inge.fractienummer, r_inge.stgr_code, v_stof_code, r_mwss.prompt);
            fetch c_mdet into r_mdet;
			-- bas_meetwaarde record niet bestaat: aanmaken.
            if (c_mdet%notfound) then

				open c_meti(r_inge.onderzoeknr, r_inge.monsternummer, r_inge.fractienummer, r_inge.stgr_code);
				fetch c_meti into r_meti;
				close c_meti;

				open c_stof(v_stof_code);
				fetch c_stof into r_stof;
				close c_stof;
                if (
                        r_meti.meti_id is not null and
                        r_stof.stof_id is not null and
                        r_mwst.mwst_id is not null and
                        ( (r_mwss.volgorde = 1) or (v_stof_code = upper('Haplogroup')) )
                   ) then
                        insert into bas_meetwaarden (meti_id, stof_id, mwst_id, datum_meting)
                        values(r_meti.meti_id, r_stof.stof_id, r_mwst.mwst_id, sysdate);

                end if;
            end if;
			close c_mdet;
			-- Stoftesten (bas_meetwaarden) zijn nu ingevoerd (dus ook meetwaardestructuren --> bas_meetwaarde_details) we kunnen nu de ingelezen waardes
			-- wegschijven in kolommen van meetwaarde details..
            open c_mdet(r_inge.onderzoeknr, r_inge.monsternummer, r_inge.fractienummer, r_inge.stgr_code, v_stof_code, r_mwss.prompt);
            fetch c_mdet into r_mdet;
			close c_mdet;

			update bas_meetwaarde_details
            set waarde = trim(v_waarde)
            where mdet_id = r_mdet.mdet_id
			;

        end loop;
    end loop;

    -- De net ingevoerde stoftesten moeten nog afgerond worden en dit gebeurt als laatste stap
    for r_inge in c_inge loop
		if r_inge.input_string = 'Haplogroup' then
			v_stof_code := upper(r_inge.input_string);
		else
			v_stof_code := trim(v_stof_prefix || substr(r_inge.input_string, instr(r_inge.input_string, r_inge.separator) + 1, 5));
		end if;

        open c_meet(r_inge.onderzoeknr, r_inge.monsternummer, r_inge.fractienummer, r_inge.stgr_code, v_stof_code);
		fetch c_meet into r_meet;
		close c_meet;

        update bas_meetwaarden
            set afgerond = 'J'
            ,   mede_id = v_mede_id
            ,   mede_id_controle = v_mede_id
            ,   datum_afgerond = sysdate
        where meet_id = r_meet.meet_id
		;
    end loop;
    commit;

exception
    when others then
        rollback;
        dbms_output.put_line(substr('SQLErrm ' || sqlerrm, 1, 255));
end mito_dna_inlezen;
procedure cnv_wes_inlezen
is
-- zie mantisnr 8353 voor toelichting
v_stgr_code kgc_stoftestgroepen.code%type := 'WES_CON_01';
v_mwst_code kgc_meetwaardestructuur.code%type := 'CNV_WES';
v_waarde varchar2(500);

cursor c_inge
  is
    select trim(kolom1) interpretatie
    ,      trim(kolom2) segmentid
    ,      trim(kolom3) chromosoom
    ,      trim(kolom4) startband
    ,      trim(kolom5) eindband
    ,      trim(kolom6) startpositie
    ,      trim(kolom7) eindpositie
    ,      trim(kolom8) lengte
    ,      trim(kolom9) soort
    ,      trim(kolom10) waarde
    ,      trim(kolom11) genecount
    ,      trim(kolom12) exoncount
    ,      trim(kolom13) baitcount
    ,      upper(trim(kolom14)) fractienr
    ,      upper(trim(kolom15)) stoftest
    from   beh_array_tmp
 ;

cursor c_mwss is
  select trim(upper(mwss.prompt)) prompt
  from   kgc_meetwaardestructuur mwst, kgc_mwst_samenstelling mwss
  where  mwst.mwst_id = mwss.mwst_id
  and    upper(mwst.code) = v_mwst_code
  order by mwss.volgorde
 ;

cursor c_mdet (p_fractienr varchar2, p_stoftest varchar2, p_prompt varchar2) is
  select mdet.mdet_id
  from bas_fracties frac
  ,    bas_metingen meti
  ,    bas_meetwaarden meet
  ,    bas_meetwaarde_details mdet
  ,    kgc_stoftesten stof
  ,    kgc_stoftestgroepen stgr
  where frac.frac_id = meti.frac_id
  and   meti.meti_id = meet.meti_id
  and   meet.meet_id = mdet.meet_id
  and   meti.stgr_id = stgr.stgr_id
  and   meet.stof_id = stof.stof_id
  and   meet.afgerond = 'N'
  and   meti.afgerond = 'N'
  and   stgr.code = v_stgr_code
  and   frac.fractienummer = p_fractienr
  and   stof.code = p_stoftest
  and   upper(mdet.prompt) = p_prompt
;

r_mdet c_mdet%rowtype;

begin
  for r_mwss in c_mwss loop
    for r_inge in c_inge loop
      v_waarde :=
        case r_mwss.prompt
          when 'INTERPRETATIE' then r_inge.interpretatie
          when 'SEGMENTID' then r_inge.segmentid
          when 'CHROMOSOOM' then r_inge.chromosoom
          when 'STARTBAND' then r_inge.startband
          when 'EINDBAND' then r_inge.eindband
          when 'STARTPOSITIE' then r_inge.startpositie
          when 'EINDPOSITIE' then r_inge.eindpositie
          when 'LENGTE' then r_inge.lengte
          when 'SOORT' then r_inge.soort
          when 'WAARDE' then r_inge.waarde
          when 'GENE COUNT' then r_inge.genecount
          when 'EXON COUNT' then r_inge.exoncount
          when 'BAIT COUNT' then r_inge.baitcount
          when 'FRACTIENR' then r_inge.fractienr
          when 'STOFTEST' then r_inge.stoftest
          else '?'
        end;

      open c_mdet (r_inge.fractienr, r_inge.stoftest, r_mwss.prompt);
      fetch c_mdet into r_mdet;
      if c_mdet%found then
        update bas_meetwaarde_details
        set waarde = v_waarde
        where mdet_id = r_mdet.mdet_id
        ;
      else
        null;
      end if;
      close c_mdet;
     end loop;
  end loop;
commit;
exception
  when others then
    rollback;
    dbms_output.put_line(substr('SQLErrm ' || sqlerrm, 1, 255));
end cnv_wes_inlezen;
procedure genoom_cbv_codes is
v_cate_code kgc_categorieen.code%type;

cursor c_cbv is
select *
from beh_genoom_bu_cbv_codes
;

cursor c_cate (p_code varchar2) is
select *
from kgc_categorieen
where caty_id = (select caty_id from kgc_categorie_types where code = 'CBV')
and   code = p_code
;
r_cate c_cate%rowtype;

cursor c_vic is
select *
from kgc_categorieen
where caty_id = (select caty_id from kgc_categorie_types where code = 'VIC')
and   code = 'DNA'
and kafd_id = (select kafd_id from kgc_kgc_afdelingen where code = 'GENOOM')
;
r_vic c_vic%rowtype;

cursor c_cawa(p_cate_id number, p_waarde varchar2) is
select waarde
from kgc_categorie_waarden
where cate_id = p_cate_id
and upper(waarde) = upper(p_waarde)
;
r_cawa c_cawa%rowtype;

begin
delete
from kgc_categorie_waarden
where cate_id in
(
select cate_id
from kgc_categorieen
where code in ('POST_SCAN', 'POST_SCREE', 'PRE_SCAN', 'PRE_SCREE')
and caty_id = (select caty_id from kgc_categorie_types where code = 'CBV')
)
;
commit;

open c_vic;
fetch c_vic into r_vic;
close c_vic;

for r_cbv in c_cbv loop
	v_cate_code := '';
  -- iedere Genoom indicatie mag 4 cbv code hebben:
  -- 1 - post scanning cbv code
  -- 2 - post screening cbv code
  -- 3 - pre scanning cbv code
  -- 4 - post screening cbv code
	if upper(r_cbv.postpre) = 'POST' then
		if upper(r_cbv.scanscreen) = 'SCANNING' then
			v_cate_code := 'POST_SCAN';
		elsif upper(r_cbv.scanscreen) = 'SCREENING' then
			v_cate_code := 'POST_SCREE';
		end if;
	elsif upper(r_cbv.postpre) = 'PRE' then
		if upper(r_cbv.scanscreen) = 'SCANNING' then
			v_cate_code := 'PRE_SCAN';
		elsif upper(r_cbv.scanscreen) = 'SCREENING' then
			v_cate_code := 'PRE_SCREE';
		end if;
	end if;

	if (length(v_cate_code)) > 0 then
		open c_cate(v_cate_code);
		fetch c_cate into r_cate;
		close c_cate;

    -- indicatie code is de waarde en cbv_code de omschrijving!
		insert into kgc_categorie_waarden
		(cate_id, waarde, beschrijving)
		values
		(r_cate.cate_id, r_cbv.indi_code, r_cbv.cbv_code)
		;
    -- toevoegen aan vic categorie
    -- anders loopt de de declaraties niet goed
    open c_cawa(r_vic.cate_id, r_cbv.cbv_code);
    fetch c_cawa into r_cawa;
    if c_cawa%notfound then
      insert into kgc_categorie_waarden
      (cate_id, waarde, beschrijving)
      values
      (r_vic.cate_id, r_cbv.cbv_code, r_cbv.omschrijving)
      ;
    end if;
    close c_cawa;

	end if;
end loop;
commit;

exception
when others then
	rollback;
	raise;
end genoom_cbv_codes;
/*
procedure slkv_inlezen is
--mantisnr 8279
v_waarde_c22 bas_meetwaarden.meetwaarde%type;
v_waarde_c24 bas_meetwaarden.meetwaarde%type;
v_waarde_c26 bas_meetwaarden.meetwaarde%type;
v_waarde_c26um bas_meetwaarden.meetwaarde%type;
v_waarde_c24_22 bas_meetwaarden.meetwaarde%type;
v_waarde_c26_22 bas_meetwaarden.meetwaarde%type;
--v_waarde bas_meetwaarden.meetwaarde%type;
-- alle rijen met een monsternummmer (en protocol code)
-- met hun rownr (en next rownr) erbij.
cursor c_mons is
select *
from
(
select vw.*,
LEAD(rownr, 1, (select count(*) from beh_slkv_vw)) OVER (ORDER BY rownr) rownr_next
from beh_slkv_vw vw
where vw.monsnr <> '?'
)
where (rownr_next-rownr) > 15
;

cursor c_stof (p_rownr_van number, p_rownr_tm number) is
select *
from beh_slkv_vw
where rownr >= p_rownr_van and rownr < p_rownr_tm
;

cursor c_meet (p_monsternummer varchar2, p_stgr_code varchar2) is
select meet_id, stof.code stof_code, stof.aantal_decimalen
from  kgc_monsters mons
,     bas_fracties frac
,     bas_metingen meti
,     bas_meetwaarden meet
,     kgc_stoftestgroepen stgr
,     kgc_stoftesten stof
where mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   meti.meti_id = meet.meti_id
and   meti.stgr_id = stgr.stgr_id
and   meet.stof_id = stof.stof_id
and   mons.monsternummer = p_monsternummer
and   stgr.code = p_stgr_code
and   stof.code in ('C24_C22', 'C26_C22', 'C26UM')
and   meet.afgerond = 'N'
and   meti.afgerond = 'N'
;

begin
for r_mons in c_mons loop

  for r_stof in c_stof(r_mons.rownr, r_mons.rownr_next) loop
    if r_stof.stof_code <> '?' then

      if r_stof.stof_code = 'C22' then
        v_waarde_c22 := r_stof.waarde_area;
      elsif r_stof.stof_code = 'C24' then
        v_waarde_c24 := r_stof.waarde_area;
      elsif r_stof.stof_code = 'C26' then
        v_waarde_c26 := r_stof.waarde_area;
        v_waarde_c26um := r_stof.waarde_conc;
      end if;

    end if;

  end loop;

  if (v_waarde_c26 is not null and v_waarde_c24 is not null and v_waarde_c22 is not null) then

    for r_meet in c_meet (r_mons.monsnr, r_mons.stgr_code) loop
      if r_meet.stof_code = 'C24_C22' then
        v_waarde_c24_22 := to_char(round(v_waarde_c24/v_waarde_c22, r_meet.aantal_decimalen));
        update bas_meetwaarden
        set meetwaarde = v_waarde_c24_22
        where meet_id = r_meet.meet_id
        ;

      elsif r_meet.stof_code = 'C26_C22' then
        v_waarde_c26_22 := to_char(round(v_waarde_c26/v_waarde_c22, r_meet.aantal_decimalen));
        update bas_meetwaarden
        set meetwaarde = v_waarde_c26_22
        where meet_id = r_meet.meet_id
        ;

      elsif r_meet.stof_code = 'C26UM' then
        update bas_meetwaarden
        set meetwaarde = v_waarde_c26um
        where meet_id = r_meet.meet_id
        ;
      end if;
    end loop;
  end if;
end loop;
commit;
exception
when others then
	rollback;
	raise;
end slkv_inlezen;
*/
function slkv_inlezen
   (
   p_directory in varchar2,
   p_bestand in varchar2
   )
return varchar2 is

--paramlist beh_soap_paramlist;
paramlist beh_SOAP_PARAMLIST;
v_soap_service varchar2(100) := 'slkv';
v_dir_file varchar2(1000) := p_directory || p_bestand;

v_handshake kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER'));

v_return varchar2(4000);

begin
--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));

-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'dirFile', '<![CDATA['||v_dir_file||']]>');
beh_soap_00.add_parameter(paramlist, 'dbName', sys_context('userenv','db_name'));
beh_soap_00.add_parameter(paramlist, 'handshake', v_handshake);

--aanroep soapservice
-- goede antwoord kan J of N zijn.
v_return := beh_soap_00.call_soap_service(v_soap_service, paramlist);

return v_return;
exception
    when others then
    raise;
end slkv_inlezen;
procedure zet_oorspr_aanvragers is
v_this_prog varchar2(500) := $$PLSQL_UNIT  || '.zet_oorspr_aanvragers';
v_email_from kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_STGEN_EMAIL_FROM'));
--v_email_to varchar2(200) := v_email_from || ';Annet.Simons@radboudumc.nl;Wilmien.Heys@radboudumc.nl';
v_email_to varchar2(200) := 'Annet.Simons@radboudumc.nl;Wilmien.Heys@radboudumc.nl';
--v_email varchar2(32000) := '<table align=center border=1>' ||
v_email clob := '<table align=center border=1>' ||
                           '<tr>' ||
                           '<td>Nr</td>' ||
                           '<td>Onderzoeknr</td>' ||
                           '<td>Aanvrager</td>' ||
                           '<td>Oorspr. aanvrager (old)</td>' ||
                           '<td>Oorspr. aanvrager (new)</td>' ||
                           '<td>Log</td>' ||
                           '</tr>';
v_email_body varchar2(32000);
v_email_footer varchar2(32000);

v_color varchar(10);
v_count number := 0;
v_sql_disable varchar2(2500) :=
	'alter trigger KGC_ONDE_AFGEROND_TRG disable';
v_sql_enable varchar2(2500) :=
	'alter trigger KGC_ONDE_AFGEROND_TRG enable';

-- tabel beh_oorspr_aanvragers wordt gevuld via
-- helix interface herstel_oorspr_aanvragers door de gebruikers
cursor c_inge is
select  upper(trim(inge.onderzoeknr)) onderzoeknr
,       upper(trim(inge.rela_code)) inge_rela_code
,       onde.onde_id
,       rela_aanvrager.rela_id rela_id_aanvrager
,       rela_aanvrager.code rela_code_aanvrager
,       rela_oorspr_old.rela_id rela_id_oorspr_old
,       rela_oorspr_old.code rela_code_oorspr_old
,       rela_oorspr_new.rela_id rela_id_oorspr_new
,       rela_oorspr_new.code rela_code_oorspr_new
,       case
          when onde.onde_id is null then
            case
              when rela_oorspr_new.code is null then
                'Onderzoeknr: ' || inge.onderzoeknr || ' en rela_code: ' || inge.rela_code || ' niet gevonden.'
              else
                'Onderzoeknr: ' || inge.onderzoeknr || ' niet gevonden.'
            end
          else
            case
              when rela_oorspr_new.rela_id is null then
                'rela_code: ' || inge.rela_code || ' niet gevonden.'
              else
                'OK'
            end
        end log_tekst
from beh_oorspr_aanvragers inge
  left join kgc_onderzoeken onde on upper(trim(inge.onderzoeknr)) = onde.onderzoeknr
  left join kgc_relaties rela_oorspr_new on upper(trim(inge.rela_code)) = rela_oorspr_new.code
  left join kgc_relaties rela_oorspr_old on onde.rela_id_oorsprong = rela_oorspr_old.rela_id
  left join kgc_relaties rela_aanvrager on onde.rela_id = rela_aanvrager.rela_id
  where    inge.log_tekst is null
  order by inge.last_update_date
  for update of inge.log_tekst
;

begin

execute immediate v_sql_disable;

for r_inge in c_inge loop

  v_count := v_count + 1;

  if r_inge.log_tekst = 'OK' then
      v_color := 'green';
      update kgc_onderzoeken
      set rela_id_oorsprong = r_inge.rela_id_oorspr_new
      where onderzoeknr = r_inge.onderzoeknr
      ;
  else
    v_color := 'red';
  end if;

  update beh_oorspr_aanvragers
  set log_tekst = r_inge.log_tekst
  where current of c_inge
  ;

   v_email_body :=
      '<tr>' ||
      '<td>'|| v_count ||'</td>' ||
      '<td>' || r_inge.onderzoeknr || '</td>' ||
      '<td>' || r_inge.rela_code_aanvrager || '</td>' ||
      '<td>' || r_inge.rela_code_oorspr_old || '</td>' ||
      '<td>' || r_inge.rela_code_oorspr_new || '</td>' ||
      '<td><font color='|| v_color ||'>' || r_inge.log_tekst ||'</font></td>' ||
      '</tr>';

      dbms_lob.append (v_email, v_email_body);

end loop;

commit;

execute immediate v_sql_enable;

v_email_footer := '</table><br><br><br>
Deze email is automatisch door Helix procedure: ' || v_this_prog || ' gegenereerd en verzonden.<br><br><br>
Met vriendelijke groeten,<br>
Serviceteam Genetica<br>';

dbms_lob.append (v_email, v_email_footer);

send_mail_html(
  p_from => v_email_from
  , p_to => v_email_to
  , p_subject => 'Herstel oorspr. aanvragers'
  , p_html => v_email
);

exception
	when others then
	rollback;
	execute immediate v_sql_enable;
  v_email := substr (sqlerrm, 1, 2500);
  send_mail_html(
    p_from => v_email_from
  , p_to => v_email_to
  , p_subject => v_this_prog
  , p_html => v_email
  );

end zet_oorspr_aanvragers;
PROCEDURE kreat_inlezen(
    p_key IN VARCHAR2 )
IS
  v_this_prog varchar2(500) := $$PLSQL_UNIT  || '.kreat_inlezen';
  v_email_from kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_STGEN_EMAIL_FROM'));
  v_email_to   VARCHAR2(1000)   := 'monsterontvangsttml@radboudumc.nl';

  v_email      VARCHAR2(32000) := '<table align=center border=1>
<b><tr><td>VolgNr</td><td>Glims ordernr</td><td>Monsnr</td><td>Meet_id</td><td>Waarde</td><td>Status</td></tr></b>
';
  v_status     VARCHAR2(500);
  v_fileName   VARCHAR2(1000);
  v_color      VARCHAR2(20);
  v_count      NUMBER := 1;
  CURSOR c_inge
  IS
    SELECT kolom02 glims_ordernr,
      kolom03 monsnr,
      kolom05 waarde,
      kolom06 fileName
    FROM beh_temp_inlees
    WHERE kolom01 = p_key
    ORDER BY kolom03 ;
  CURSOR c_meet (p_monsnr IN VARCHAR2)
  IS
    SELECT meet.meet_id,
      NVL(prst.aantal_decimalen, stof.aantal_decimalen) aantal_decimalen,
      meet.meetwaarde
    FROM kgc_monsters mons ,
      bas_fracties frac ,
      bas_metingen meti ,
      bas_meetwaarden meet ,
      kgc_stoftestgroepen stgr ,
      kgc_stoftesten stof,
      kgc_protocol_stoftesten prst
    WHERE mons.mons_id     = frac.mons_id
    AND frac.frac_id       = meti.frac_id
    AND meti.meti_id       = meet.meti_id
    AND meti.stgr_id       = stgr.stgr_id
    AND meet.stof_id       = stof.stof_id
    AND stof.stof_id       = prst.stof_id
    AND stgr.stgr_id       = prst.stgr_id
    AND mons.monsternummer = p_monsnr
    AND stgr.code          ='KREAT_UP'
    AND stof.code          = 'KREAT'
    AND meet.afgerond      = 'N'
    AND meti.afgerond      = 'N'
    ORDER BY meet_id FOR UPDATE OF meet.meetwaarde ;
  r_meet c_meet%rowtype;
BEGIN
  FOR r_inge IN c_inge
  LOOP
    v_fileName := r_inge.filename;
    OPEN c_meet (r_inge.monsnr);
    FETCH c_meet INTO r_meet;
    IF c_meet%notfound THEN
      v_status := 'FOUT: monsnr/meet_id niet gevonden.';
      v_color  := 'red';
      r_meet   := NULL;
    ELSE
      IF (beh_alfu_00.is_nummer(r_inge.waarde) = 'J') THEN
        IF r_meet.meetwaarde is null then
          v_status                              := 'GOED';
          v_color                               := 'green';
          UPDATE bas_meetwaarden
          SET meetwaarde = TO_CHAR(ROUND(r_inge.waarde, r_meet.aantal_decimalen))
          WHERE CURRENT OF c_meet;
        ELSE
          v_status := 'FOUT: stoftest bevat al waarde.';
          v_color  := 'red';
        END IF;
      ELSE
        v_status := 'FOUT: ongeldige meetwaarde.';
        v_color  := 'red';
      END IF;
    END IF;
    v_email := v_email || '<tr><td>'||v_count||'</td><td>'||r_inge.glims_ordernr||'</td><td>'||r_inge.monsnr||'</td><td>'||TO_CHAR(r_meet.meet_id)||'</td><td>'||r_inge.waarde||'</td><td><font color='||v_color||'>'|| v_status|| '</font></td></tr>';
    CLOSE c_meet;
    v_count := v_count + 1;
  END LOOP;
  v_email := v_email || '</table><br><br><br>';
  v_email := v_email || 'Deze email is automatisch door Helix procedure: ' || v_this_prog ||' gegenereerd en verzonden.<br><br><br>';
  v_email := v_email || 'Met vriendelijke groeten,<br>';
  v_email := v_email || 'Serviceteam Genetica<br>';
  send_mail_html( p_from => v_email_from, p_to => v_email_to, p_subject => v_fileName, p_html => v_email );
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  v_email := v_email || '<tr><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>'||SUBSTR('SQLErrm ' || sqlerrm, 1, 2500)|| '</td></tr></table>';
  send_mail_html( p_from => v_email_from, p_to => v_email_to, p_subject => v_this_prog, p_html => v_fileName || ' => '|| v_email );
  raise;
END kreat_inlezen;
function kasp_allel
(p_marker_name in char,
p_ALLEL CHAR
)
return varchar is

  cursor c_allel_1
  is
  select allel_1
  from beh_kasp_markers
  where marker_name = p_marker_name
  ;

  cursor c_allel_2
  is
  select allel_2
  from beh_kasp_markers
  where marker_name = p_marker_name
  ;

  v_return varchar2(1) := null;

begin

  if upper(p_allel) = 'ALLEL_1' then
    open c_allel_1;
    fetch  c_allel_1 into v_return;
    if c_allel_1%notfound then
      v_return := null;
    end if;
    close c_allel_1;

  elsif upper(p_allel) = 'ALLEL_2' then
    open c_allel_2;
    fetch  c_allel_2 into v_return;
    if c_allel_2%notfound then
      v_return := null;
    end if;
    close c_allel_2;

  end if;
  return v_return;
exception
  when others then
    v_return := null;
    return v_return;
end kasp_allel;

procedure KASP_inlezen is

v_aantal        number;
v_meet_id       number;
v_fractienummer varchar2(50);
v_volgnummer    varchar2(4);
v_marker_name   varchar2(50);
v_waarde        varchar2(500);
v_log           varchar2(2000);
v_os_user       varchar2(50);
v_session_user  varchar2(50);
v_sysdate       varchar2(25);
v_sender        varchar2(100) := trim(kgc_sypa_00.systeem_waarde('BEH_STGEN_EMAIL_FROM'));
v_recipient     varchar2(100):= 'Simone.vandenHeuvel@radboudumc.nl';
v_title         varchar2(20) := 'Inlezen KASP data';
v_message       varchar2(2000);
v_cc            varchar2(50) := v_sender;

    cursor c_kasp
    is
    select trim(kain.K_well)                                               volgnummer
         , trim(kain.k_sample_name)                                        sample_name
         , substr(kain.k_sample_name, 1, (instr(k_sample_name, '_')- 1))   fractienummer
         , trim(substr(kain.k_sample_name, (instr(k_sample_name, '_')+1))) meet_id
         , trim(k_marker_name)                                             marker_name
         , trim(kain.k_call)                                               k_call
         , decode(upper(trim(kain.k_call)), 'BOTH', kasp_allel(kain.k_MARKER_NAME,'ALLEL_1'),'A','A','C','C','G','G','T','T', null) allel_1
         , decode(upper(trim(kain.k_call)), 'BOTH', kasp_allel(kain.k_MARKER_NAME,'ALLEL_2'),'A','A','C','C','G','G','T','T', null) allel_2
         , decode(upper(trim(kain.k_call)), 'BOTH', trim(kain.k_quality_value)
                                          , 'A', trim(kain.k_quality_value)
                                          , 'C', trim(kain.k_quality_value)
                                          , 'G', trim(kain.k_quality_value)
                                          , 'T', trim(kain.k_quality_value)
                                          ,  null) quality_value
         , decode(upper(trim(kain.k_call)), 'BOTH', trim(kain.k_call_type)
                                          , 'A', trim(kain.k_call_type)
                                          , 'C', trim(kain.k_call_type)
                                          , 'G', trim(kain.k_call_type)
                                          , 'T', trim(kain.k_call_type)
                                          ,  null) k_call_type
    from BEH_KASP_INLEES kain
    where substr(k_sample_name, 1, 3) = 'DNA'
    ;

    r_kasp c_kasp%rowtype;

  cursor c_mdet (v_meet_id in number) is
  select mdet_id
  , volgorde
  , prompt
  , waarde
  from bas_meetwaarde_details
  where meet_id = v_meet_id
  and prompt in ('Allel_1', 'Allel_2', 'Call', 'Quality Value', 'Call Type')
  for update of waarde;

  r_mdet c_mdet%rowtype;

  -- van te voren te tesetn fouten
  e_geen_data_in_file   exception; -- als er geen waarde is in de kolom call
  e_geen_data_in_call   exception; -- als er geen fracties zijn in de hele in leestabel

  -- fouten die je onderweg tegenkomt
  e_reeds_data_aanwezig exception; -- als er al meetwaarde details aanweizg zijn
  e_frac_meet_fout      exception; -- de fractie heeft deze meet_id niet

begin
  v_cc  := null;
  -- context gegevens ophalen
  select sys_context('userenv', 'os_user') into v_os_user from dual;
  select sys_context('userenv', 'session_user') into v_session_user from dual;

  select to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss') into v_sysdate from dual;


  -- bepaal of er wel gegevens in de inlees tabel zijn om in te lezen
  select count(*) into v_aantal
    from BEH_KASP_INLEES
    where substr(k_sample_name, 1, 3) = 'DNA';
insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values('begin'||v_log, v_sysdate, v_os_user, v_session_user);

  if v_aantal = 0 then
    raise e_geen_data_in_file;
  else

      for r_kasp in c_kasp loop

        v_fractienummer := r_kasp.fractienummer ;
        v_meet_id := r_kasp.meet_id;
        v_volgnummer := r_kasp.volgnummer;

        --als de kolom call geen waarde heeft is er een fout in de file,
        if nvl(r_kasp.k_call,'ONB') = 'ONB' then
          raise e_geen_data_in_call;

        else

          --als bij deze fractie deze meet_id niet gevonden wordt dan is er een fout
          select count(*) into v_aantal --from bas_meetwaarden_details where meet_id = v_meet_id and prompt in ('Allel_1', 'Allel_2', 'Call', 'Quality Value', 'Call Type');
                from bas_fracties frac
          ,    bas_metingen meti
          ,    bas_meetwaarden meet
          where frac.frac_id = meti.frac_id
          and   meti.meti_id = meet.meti_id
          and   meet.afgerond = 'N'
          and   meti.afgerond = 'N'
          and   frac.fractienummer = v_fractienummer
          and   meet.meet_id  = v_meet_id;

          if v_aantal = 0 then
            raise e_frac_meet_fout;

          else

              for r_mdet in c_mdet ( v_meet_id)loop

                if nvl(r_mdet.waarde, 'ONB') <> 'ONB' then
                  raise e_reeds_data_aanwezig;

                elsif r_mdet.prompt = 'Allel_1' then
                    update bas_meetwaarde_details
                    set waarde = r_kasp.allel_1
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Allel_2' then
                    update bas_meetwaarde_details
                    set waarde = r_kasp.allel_2
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Call' then
                    update bas_meetwaarde_details
                    set waarde = r_kasp.k_call
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Quality Value' then
                    update bas_meetwaarde_details
                    set waarde = r_kasp.quality_value
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Call Type' then
                    update bas_meetwaarde_details
                    set waarde = r_kasp.k_call_type
                    where current of c_mdet;

                else
                  null;

                end if;

              end loop;


          end if;

        end if;

      end loop;

  end if;
  --verzamelen logtekst en versturen

  insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user)
    values('Verwerking van file succesvol', v_sysdate, v_os_user, v_session_user);

  v_log := 'Verwerking van file succesvol'||chr(10)||'Verwerkingsdatum en tijd:= '||v_sysdate||chr(10)||'SESSION_user: '||v_session_user;

  v_message := substr(v_log, 1, 400);

  UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);


  exception

  when e_reeds_data_aanwezig then
    rollback;
    v_log := ' -ERROR- Verwerking van file afgebroken en teruggedraaid:'||chr(10)||'Reeds data aanwezig bij volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id||chr(10);

    insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values(v_log, v_sysdate, v_os_user, v_session_user);

    v_message := v_log||chr(10)||' Datum en tijd:= '||v_sysdate||chr(10)||'SESSION_user: '||v_session_user;
    v_message := substr(v_message, 1, 400);

    UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);


  when e_geen_data_in_call then
    rollback;
    v_log := ' -ERROR- Verwerking van file afgebroken en teruggedraaid:'||chr(10)||'De waarde van de kolom CALL is leeg bij volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id;
    insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values(v_log, v_sysdate, v_os_user, v_session_user);
    v_message := v_log||chr(10)||' Datum en tijd:= '||v_sysdate||chr(10)||'SESSION_user: '||v_session_user;
    v_message := substr(v_message, 1, 400);

    UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);

  when e_geen_data_in_file then
    rollback;
    v_log := ' -ERROR- Verwerking van file is mislukt. In deze file zijn geen fracties gevonden';
    insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values(v_log, v_sysdate, v_os_user, v_session_user);
    v_message := v_log||chr(10)||' Datum en tijd:= '||v_sysdate||chr(10)||'SESSION_user: '||v_session_user;
    v_message := substr(v_message, 1, 400);

    UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);

  when e_frac_meet_fout then
    rollback;
    v_log := ' -ERROR- Verwerking van deze regel is mislukt. '||chr(10)||'Deze meet_id is niet gevonden bij deze fractie. Volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id;
    insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values(v_log, v_sysdate, v_os_user, v_session_user);
    v_message := v_log||chr(10)||' Datum en tijd:= '||v_sysdate||chr(10)||'SESSION_user: '||v_session_user;
    v_message := substr(v_message, 1, 400);

    UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);
  when others then
    rollback;
    v_log := '-ERROR- Verwerking van file is mislukt.'||SQLCODE||'  '||SUBSTR(SQLERRM, 1, 200);
    insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values(v_log, v_sysdate, v_os_user, v_session_user);
    v_message := v_log||chr(10)||' Datum en tijd:= '||v_sysdate||chr(10)||'SESSION_user: '||v_session_user;
    v_message := substr(v_message, 1, 400) ;

    UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);
end KASP_inlezen;
procedure onde_aanvraag_bestanden is
/*
deze procedure genereert een lijst van alle aanvraag bestanden van NGS onderzoeken
dit gebeurt op basis van de ingelezen fractienummers
in overizicht is ook een hyperlink opgenomen naar het bestand
*/
  v_this_prog varchar2(500) := $$PLSQL_UNIT  || '.onde_aanvraag_bestanden';
  v_email_from kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_STGEN_EMAIL_FROM'));
  v_email_to   VARCHAR2(1000)   := 'Margot.Reijnders@radboudumc.nl';
  v_email      clob := '<table align=center border=1>
<b><tr><td>VolgNr</td><td>Onderzoeknr</td><td>Monsnr</td><td>FractieNr</td><td>Bestand</td></tr></b>';

v_count number;

v_href VARCHAR2(1000);

cursor c_best is
with w_kgc_bestanden as
(
  select best.*
  from kgc_bestanden best, kgc_bestand_types btyp
  where best.btyp_id = btyp.btyp_id
  and btyp.code = 'AANVRAAG'
  and best.entiteit_code = 'ONDE'
),
w_kgc_onderzoeken as
(
  select onde.onde_id, onde.onderzoeknr, mons.monsternummer, frac.fractienummer
  from kgc_onderzoeken onde, kgc_kgc_afdelingen kafd, kgc_onderzoeksgroepen ongr,
  kgc_onderzoek_monsters onmo, kgc_monsters mons, bas_fracties frac
  where onde.kafd_id = kafd.kafd_id
  and   onde.ongr_id = ongr.ongr_id
  and   onde.onde_id = onmo.onde_id
  and   onmo.mons_id = mons.mons_id
  and   mons.mons_id = frac.mons_id
  and   kafd.code = 'GENOOM'
  and   ongr.code = 'NGS'
),
w_beh_temp_inlees as
(
select upper(trim(kolom01)) fractienummer
from beh_temp_inlees
)
select onde.onderzoeknr, onde.monsternummer, inge.fractienummer, best.bestandsnaam, nvl(best.bestand_specificatie, 'nvt') bestand_specificatie
from w_beh_temp_inlees inge
  left join w_kgc_onderzoeken onde on inge.fractienummer = onde.fractienummer
  left join w_kgc_bestanden best on onde.onde_id = best.entiteit_pk
order by onde.fractienummer
;
begin
/* limiet wordt veroorzaakt door variabel lengte (v_email) */
/*
select count(*) into v_count from beh_temp_inlees;

if v_count > 100 then
    v_email :=  v_email || '<tr><td colspan=5>Per inlees actie kan alleen 100 records verwerkt worden.</td></tr>';
else
*/
  v_count := 0;
  for r_best in c_best loop
    if r_best.bestand_specificatie != 'nvt' then
      v_href := '<a href="' || r_best.bestand_specificatie || '">' || r_best.bestandsnaam || '</a>';
    else
      v_href := r_best.bestand_specificatie;
    end if;
    v_count := v_count+1;
    v_email :=  v_email || '<tr><td>'||
                v_count ||'</td><td>' ||
                r_best.onderzoeknr || '</td><td>' ||
                r_best.monsternummer || '</td><td>' ||
                r_best.fractienummer || '</td><td>' ||
                v_href ||'</td></tr>';
  end loop;
--end if;
v_email := v_email || '</table><br><br><br>';
v_email := v_email || 'Deze email is automatisch door Helix procedure: ' || v_this_prog || ' gegenereerd en verzonden.<br><br><br>';
v_email := v_email || 'Met vriendelijke groeten,<br>';
v_email := v_email || 'Serviceteam Genetica<br>';

send_mail_html(
  p_from => v_email_from
, p_to => v_email_to
, p_subject => v_this_prog
, p_html => v_email
);
exception
	when others then
  v_email := substr (sqlerrm, 1, 2500);
  send_mail_html(
    p_from => v_email_from
  , p_to => v_email_to
  , p_subject => v_this_prog
  , p_html => v_email
  );

end onde_aanvraag_bestanden;



--############################################################################
-- Functie beh_get_user_email haalt het email adres van de huidige gebruiker op.
--############################################################################
FUNCTION beh_get_user_email RETURN varchar2 IS

  v_email_van varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                , p_mede_id        => NULL
                                                                ) , 'HelpdeskHelixCUKZ@cukz.umcn.nl');
  v_email_aan varchar2(100);

BEGIN
  select nvl(kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_MIJN_HELIX_EMAIL_ADRES', p_mede_id => kgc_mede_00.medewerker_id),decode(mede.vms_username, null, '', mede.vms_username || '@radboudumc.nl'))
  into   v_email_aan
  from   kgc_medewerkers mede
  where  mede.oracle_uid = user;

  return v_email_aan;

EXCEPTION WHEN OTHERS THEN
  return v_email_van;

END beh_get_user_email;



--############################################################################
-- Procedure teksten_inlezen te gebruiken om nederlandse en engelse teksten in te lezen.
-- Parameter: p_entiteit kan waarde RETE or CONC hebben.
--            RETE: Lees resultaat teksten in tabel KGC_RESULTAAT_TEKSTEN.
--            CONC: Lees conclusie teksten in tabel KGC_CONCLUSIE_TEKSTEN.
-- De bijbehorende engelse tekst wordt altijd ingelezen in tabel KGC_VERTALINGEN.
--############################################################################
PROCEDURE teksten_inlezen(p_entiteit in varchar2) is

  v_procedure_naam varchar2(40) := 'beh_interfaces.teksten_inlezen';

  CURSOR c_tis IS
  SELECT code, tekst_nl, tekst_eng
  FROM   beh_teksten_inlees
  WHERE  upper(code)<>'CODE'
  ORDER BY code;

  v_kafd_id_gen  kgc_kgc_afdelingen.kafd_id%type;
  v_taal_id_eng  kgc_talen.taal_id%type;
  v_id           kgc_vertalingen.id%type;

  v_html_message    clob;
  v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                      , p_mede_id        => NULL
                                                                      )
                                        , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                        );
  v_email_aan       varchar2(100);
  v_email_onderwerp varchar2(100);
  v_job_onderwerp   varchar2(50);
  v_crlf            varchar2(2) := chr(13)||chr(10);

BEGIN

  v_kafd_id_gen  := kgc_uk2pk.kafd_id('GENOOM');
  v_taal_id_eng  := kgc_uk2pk.taal_id('ENG');
  v_email_aan    := beh_get_user_email;

  IF p_entiteit = 'RETE' THEN
    v_job_onderwerp := 'resultaat teksten';
  ELSIF p_entiteit = 'CONC' THEN
    v_job_onderwerp := 'conclusie teksten';
  END IF;

  v_email_onderwerp := 'Helix overzicht ' || upper(v_job_onderwerp) || ' inlezen';
  v_html_message    := 'Dit is een automatisch gegenereerd rapport over het inlezen van ' || v_job_onderwerp || '.</br></br>' || v_crlf;
  v_html_message    := v_html_message || 'Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Uitgevoerd door: ' || user || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Uitgevoerd op server: ' || upper(sys_context('USERENV','SERVER_HOST')) || '</br></br>' || v_crlf;
  v_html_message    := v_html_message || '<table border="1">' || v_crlf;
  v_html_message    := v_html_message || '<tr><td><b>Melding</b></td><td><b>Code</b></td><td><b>Tekst NL</b></td><td><b>Melding m.b.t. tekst ENG</b></td><td><b>Tekst ENG</b></td></tr>' || v_crlf;

  --dbms_output.put_line('Start teksten_inlezen voor entiteit: ' ||p_entiteit);

  FOR r_tis IN c_tis LOOP

    v_html_message   := v_html_message || '<tr>';

    CASE p_entiteit
      ----- Resultaat teksten verwerken -----
      WHEN 'RETE' THEN

        BEGIN
          INSERT INTO kgc_resultaat_teksten (kafd_id, code, omschrijving)
          VALUES (v_kafd_id_gen, r_tis.code, r_tis.tekst_nl);

          v_html_message   := v_html_message || '<td>Code toegevoegd</td>';
          --dbms_output.put_line('Resultaat tekst aangemaakt voor code: ' || r_tis.code);

        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
          --Op dit moment worden er geen updates uitgevoerd, als een code al bestaat
          --wordt dit gezien als een fout.
          --
          --UPDATE kgc_resultaat_teksten
          --SET    omschrijving = r_tis.tekst_nl
          --WHERE  code         = r_tis.code
          --  AND  kafd_id      = v_kafd_id_gen;

          v_html_message   := v_html_message || '<td><font color="red">Code bestaat al</font></td>';
          --dbms_output.put_line('Resultaat tekst bestaat al voor code: ' || r_tis.code);

        END;

        v_html_message   := v_html_message || '<td>' || r_tis.code || '</td>';
        v_html_message   := v_html_message || '<td>' || r_tis.tekst_nl || '</td>';

        select rete_id into v_id from kgc_resultaat_teksten where kafd_id=v_kafd_id_gen and code=r_tis.code;


      ----- Conclusie teksten verwerken -----
      WHEN 'CONC' THEN

        BEGIN
          INSERT INTO kgc_conclusie_teksten (kafd_id, code, omschrijving)
          VALUES (v_kafd_id_gen, r_tis.code, r_tis.tekst_nl);

          v_html_message   := v_html_message || '<td>Code toegevoegd</td>';
          --dbms_output.put_line('Conclusie tekst aangemaakt voor code: ' || r_tis.code);

        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
          --Op dit moment worden er geen updates uitgevoerd, als een code al bestaat
          --wordt dit gezien als een fout.
          --
          --UPDATE kgc_conclusie_teksten
          --SET    omschrijving = r_tis.tekst_nl
          --WHERE  code         = r_tis.code
          --  AND  kafd_id      = v_kafd_id_gen;

          v_html_message   := v_html_message || '<td><font color="red">Code bestaat al</font></td>';
          --dbms_output.put_line('Conclusie tekst bestaat al voor code: ' || r_tis.code);

        END;

        v_html_message   := v_html_message || '<td>' || r_tis.code || '</td>';
        v_html_message   := v_html_message || '<td>' || r_tis.tekst_nl || '</td>';

        select conc_id into v_id  from kgc_conclusie_teksten where kafd_id=v_kafd_id_gen and code=r_tis.code;


      ----------
      ELSE
        null;
        --dbms_output.put_line('Entiteit ' || p_entiteit || ' wordt niet ondersteund voor code ' || r_tis.code);


    END CASE;

    ----- Vertaling verwerken -----
    IF v_id is not NULL THEN
      BEGIN
        INSERT INTO KGC_VERTALINGEN (entiteit, taal_id, id, omschrijving)
        VALUES ( p_entiteit, v_taal_id_eng, v_id, r_tis.tekst_eng );

        v_html_message   := v_html_message || '<td>Vertaling ENG toegevoegd</td>';
        v_html_message   := v_html_message || '<td>' || r_tis.tekst_eng || '</td>';
        --dbms_output.put_line('Vertaling ENG aangemaakt voor entiteit ' || p_entiteit || ' en code ' || r_tis.code);

      EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
        --Op dit moment worden er geen updates uitgevoerd, als een code al bestaat
        --wordt dit gezien als een fout.
        --
        --UPDATE KGC_VERTALINGEN
        --SET    omschrijving = r_tis.tekst_eng
        --WHERE  entiteit     = p_entiteit
        --  AND  taal_id      = v_taal_id_eng
        --  AND  id           = v_id;
        v_html_message   := v_html_message || '<td><font color="red">Vertaling ENG bestaat al</font></td>';
        v_html_message   := v_html_message || '<td>' || r_tis.tekst_eng || '</td>';
        --dbms_output.put_line('Vertaling ENG bestaat al voor entiteit ' || p_entiteit || ' en code ' || r_tis.code);

      END;
    END IF;

    v_html_message   := v_html_message || '</tr>' || v_crlf;

  END LOOP;

  v_html_message   := v_html_message || '</table>';
  v_html_message   := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

  --dbms_output.put_line('VAN: ' || v_email_afzender);
  --dbms_output.put_line('AAN: ' || v_email_aan);
  --dbms_output.put_line('ONDERW: ' || v_email_onderwerp);
  --dbms_output.put_line('TEKST: ' || v_html_message);

  send_mail_html( p_from    => v_email_afzender
                , p_to      => v_email_aan
                , p_subject => v_email_onderwerp
                , p_html    => v_html_message
                );

  COMMIT;


EXCEPTION WHEN OTHERS THEN

  ROLLBACK;

  v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
  v_html_message    := 'FOUT:<font color="red">' || sqlerrm || '</font></br></br>' || v_crlf || v_html_message;
  v_html_message    := v_html_message || '</table>';
  v_html_message    := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: beh_interfaces.teksten_inlezen';

  --dbms_output.put_line('VAN: ' || v_email_afzender);
  --dbms_output.put_line('AAN: ' || v_email_aan);
  --dbms_output.put_line('ONDERW: ' || v_email_onderwerp);
  --dbms_output.put_line('TEKST: ' || v_html_message);

  send_mail_html( p_from    => v_email_afzender
                , p_to      => v_email_aan
                , p_subject => v_email_onderwerp
                , p_html    => v_html_message
                );

END teksten_inlezen;


--############################################################################
-- Maak een protocol (stoftestgroep en stoftestgroep gebruik) aan.
-- De velden voor het aan te maken protocol zitten in record p_prot.
--
-- Parameter: p_prot         : het aan te maken protocol
-- Parameter: p_kafd_id      : is het id van de afdeling waarvoor ingelezen wordt.
-- Parameter: p_kafd_code    : is de code van de afdeling waarvoor ingelezen wordt.
-- Parameter: p_html_message : OUT parameter voor bericht richting gebruiker.
--
--############################################################################
PROCEDURE beh_stft_verwerk_prot( p_prot c_beh_prot_inlees%rowtype
                               , p_kafd_id in kgc_kgc_afdelingen.kafd_id%type
                               , p_kafd_code in kgc_kgc_afdelingen.code%type
                               , p_html_message in out clob
                               ) IS

  v_crlf varchar2(2) := chr(13)||chr(10);
  v_mate_id kgc_materialen.mate_id%type;
  v_indi_id kgc_indicatie_teksten.indi_id%type;
  v_ongr_id kgc_onderzoeksgroepen.ongr_id%type;

BEGIN
  p_html_message := p_html_message || 'Verwerk protocol ' || p_prot.groep_code || ' (' || p_prot.groep_omschr || ')</br>' || v_crlf;
  p_html_message := p_html_message || '<ul>';

  -- Maak stoftestgroep aan
  BEGIN
    INSERT INTO kgc_stoftestgroepen (kafd_id, vervallen, code, omschrijving, protocol)
    VALUES ( p_kafd_id
           , p_prot.vervallen
           , p_prot.groep_code
           , p_prot.groep_omschr
           , p_prot.protocol
           );
    p_html_message := p_html_message || '<li>Stoftestgroep ' || p_prot.groep_code || ' aangemaakt</li>' || v_crlf;

  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    p_html_message := p_html_message || '<li>Stoftestgroep ' || p_prot.groep_code || ' bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;
  END;

  --
  -- Stoftestgroep gebruik nr. 1 is hetzelfde voor alle afdelingen: Materiaal
  --
  BEGIN
    v_mate_id := kgc_uk2pk.mate_id(p_kafd_id, p_prot.materiaal2);

    if v_mate_id is not null then
      INSERT INTO kgc_stoftestgroep_gebruik (kafd_id, stgr_id, mate_id)
      VALUES ( p_kafd_id
             , (select stgr_id from kgc_stoftestgroepen where kafd_id=p_kafd_id and code=p_prot.groep_code)
             , v_mate_id
             );
      p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 1 (materiaal: ' || p_prot.materiaal2 || ') aangemaakt</li>' || v_crlf;
    else
      p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 1: Materiaal: ' || p_prot.materiaal2 || ' niet gevonden <font color="red">(gebruik niet aangemaakt)</font></li>' || v_crlf;
    end if;

  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 1 (materiaal: ' || p_prot.materiaal2 || ') bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;

  END;

  --Als de onderzoeksgroep niet gevonden kan worden, stop met het maken van de overige 2 stoftestgroep gebruiken. Deze hebben beide een onderzoeksgroep.
  v_ongr_id := kgc_uk2pk.ongr_id(p_kafd_id, p_prot.onderz_groep);
  if v_ongr_id is null then
    p_html_message := p_html_message || '<li>Stoftestgroep gebruik kan niet aangemaakt worden, <font color="red">onderzoeksgroep ' || p_prot.onderz_groep || ' is onbekend</font></li>' || v_crlf;
    p_html_message := p_html_message || '</ul>';
    return;
  end if;

  v_indi_id := kgc_uk2pk.indi_id(v_ongr_id, p_prot.indicatie_code);

  --
  -- Stoftestgroep gebruik nr. 2 verschilt per afdeling, deze wordt afdelingsspecifiek aangemaakt.
  --
  CASE p_kafd_code

    WHEN 'METAB' THEN
      -- Maak stoftestgroepgebruik nr. 2 voor METAB aan: Indicatie
      BEGIN
        if v_indi_id is not null then
          INSERT INTO kgc_stoftestgroep_gebruik (kafd_id, standaard, stgr_id, indi_id)
          VALUES ( p_kafd_id
                 , 'N'
                 , (select stgr_id from kgc_stoftestgroepen where kafd_id=p_kafd_id and code=p_prot.groep_code)
                 , v_indi_id
                 );
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2 (indicatie: ' || p_prot.indicatie_code || ') aangemaakt</li>' || v_crlf;
        else
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2: Indicatie: ' || p_prot.indicatie_code || ' niet gevonden <font color="red">(gebruik niet aangemaakt)</font></li>' || v_crlf;
        end if;

      EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
        p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2 (indicatie: ' || p_prot.indicatie_code || ') bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;

      END;


    WHEN 'GENOOM' THEN
      -- Maak stoftestgroepgebruik nr. 2a voor GENOOM aan: Indicatie met onderzoekswijze A
      BEGIN
        if v_indi_id is not null then
          INSERT INTO kgc_stoftestgroep_gebruik (kafd_id, standaard, stgr_id, onwy_id, indi_id)
          VALUES ( p_kafd_id
                 , 'N'
                 , (select stgr_id from kgc_stoftestgroepen where kafd_id=p_kafd_id and code=p_prot.groep_code)
                 , (select onwy_id from kgc_onderzoekswijzen where code = 'A' and kafd_id=p_kafd_id)
                 , v_indi_id
                 );
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2a (onderzoekswijze: A, indicatie: ' || p_prot.indicatie_code || ') aangemaakt</li>' || v_crlf;
        else
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2a: Onderzoekswijze: A, indicatie: ' || p_prot.indicatie_code || ' niet gevonden <font color="red">(gebruik niet aangemaakt)</font></li>' || v_crlf;
        end if;

      EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
        p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2a (onderzoekswijze: A, indicatie: ' || p_prot.indicatie_code || ') bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;

      END;

      -- Maak stoftestgroepgebruik nr. 2s voor GENOOM aan: Indicatie met onderzoekswijze S
      BEGIN
        if v_indi_id is not null then
          INSERT INTO kgc_stoftestgroep_gebruik (kafd_id, standaard, stgr_id, onwy_id, indi_id)
          VALUES ( p_kafd_id
                 , 'N'
                 , (select stgr_id from kgc_stoftestgroepen where kafd_id=p_kafd_id and code=p_prot.groep_code)
                 , (select onwy_id from kgc_onderzoekswijzen where code = 'S' and kafd_id=p_kafd_id)
                 , v_indi_id
                 );
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2s (onderzoekswijze: S, indicatie: ' || p_prot.indicatie_code || ') aangemaakt</li>' || v_crlf;
        else
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2s: Onderzoekswijze: S, indicatie: ' || p_prot.indicatie_code || ' niet gevonden <font color="red">(gebruik niet aangemaakt)</font></li>' || v_crlf;
        end if;

      EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
        p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 2s (onderzoekswijze: S, indicatie: ' || p_prot.indicatie_code || ') bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;

      END;

  END CASE;

  --
  -- Stoftestgroep gebruik nr. 3 verschilt per afdeling, deze wordt afdelingsspecifiek aangemaakt.
  --
  CASE p_kafd_code

    WHEN 'METAB' THEN
      -- Maak stoftestgroepgebruik nr. 3 voor METAB aan: Fractietype DNA, onderzoekswijze A, Indicatie
      BEGIN
        INSERT INTO kgc_stoftestgroep_gebruik (kafd_id, standaard, stgr_id, onwy_id, frty_id, indi_id)
        VALUES ( p_kafd_id
               , 'J'
               , (select stgr_id from kgc_stoftestgroepen where kafd_id=p_kafd_id and code=p_prot.groep_code)
               , (select onwy_id from kgc_onderzoekswijzen where code = 'A' and kafd_id=p_kafd_id)
               , (select frty_id from bas_fractie_types where code='DNA' and ongr_id=v_ongr_id)
               , v_indi_id
               );
        p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 3 (indicatie: ' || p_prot.indicatie_code || ', fractietype: DNA, onderzoekswijze: A) aangemaakt</li>' || v_crlf;
      EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
        p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 3 (indicatie: ' || p_prot.indicatie_code || ', fractietype: DNA, onderzoekswijze: A) bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;
      END;

    WHEN 'GENOOM' THEN
      -- Maak stoftestgroepgebruik nr. 3 voor GENOOM aan: Fractietype DNA, onderzoekswijze A, Indicatie

      v_mate_id := kgc_uk2pk.mate_id(p_kafd_id, p_prot.materiaal1);

      IF v_mate_id is not null THEN
        BEGIN
          INSERT INTO kgc_stoftestgroep_gebruik (kafd_id, standaard, stgr_id, onwy_id, mate_id, indi_id)
          VALUES ( p_kafd_id
                 , 'J'
                 , (select stgr_id from kgc_stoftestgroepen where kafd_id=p_kafd_id and code=p_prot.groep_code)
                 , (select onwy_id from kgc_onderzoekswijzen where code = 'A' and kafd_id=p_kafd_id)
                 , v_mate_id
                 , v_indi_id
                 );
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 3 (indicatie: ' || p_prot.indicatie_code || ', materiaal: ' || p_prot.materiaal1 || ', onderzoekswijze: A) aangemaakt</li>' || v_crlf;
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
          p_html_message := p_html_message || '<li>Stoftestgroep gebruik nr. 3 (indicatie: ' || p_prot.indicatie_code || ', materiaal: ' || p_prot.materiaal1 || ', onderzoekswijze: A) bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;
        END;
     END IF;

  END CASE;

  p_html_message := p_html_message || '</ul>';

EXCEPTION WHEN OTHERS THEN
  p_html_message := p_html_message || '<li><font color="red">Algemene fout tijdens verwerken protocol: ' || sqlerrm || ' </font></li>' || v_crlf;
  p_html_message := p_html_message || '</ul>';

END;


--############################################################################
-- Maak een stoftest aan.
-- De velden voor de aan te maken stoftest zitten in record p_prot.
--
-- Parameter: p_stoftest     : de aan te maken stoftest
-- Parameter: p_kafd_id      : is het id van de afdeling waarvoor ingelezen wordt.
-- Parameter: p_html_message : OUT parameter voor bericht richting gebruiker.
--
--############################################################################
PROCEDURE beh_stft_verwerk_stof( p_stoftest c_beh_stoftest_inlees%rowtype
                               , p_kafd_id in kgc_kgc_afdelingen.kafd_id%type
                               , p_html_message in out clob) IS
  v_crlf varchar2(2) := chr(13)||chr(10);
  v_tech_id        kgc_technieken.tech_id%type;
  v_mwst_id        kgc_meetwaardestructuur.mwst_id%type;
  v_stof_voor_id   kgc_stoftesten.stof_id%type;
  v_stof_samen_id  kgc_stoftesten.stof_id%type;
  v_stof_id        kgc_stoftesten.stof_id%type;

BEGIN
  v_tech_id       := kgc_uk2pk.tech_id(p_stoftest.tech_code);
  v_mwst_id       := kgc_uk2pk.mwst_id(p_stoftest.mwst_code);
  v_stof_voor_id  := kgc_uk2pk.stof_id(p_kafd_id, p_stoftest.stof_voor_code);
  v_stof_samen_id := kgc_uk2pk.stof_id(p_kafd_id, p_stoftest.samenstelling);

  IF v_tech_id is null and p_stoftest.tech_code is not null THEN
    p_html_message := p_html_message || '<li>Stoftest ' || p_stoftest.stof_code || ': Techniek ' || p_stoftest.tech_code || ' niet gevonden <font color="red">(stoftest niet aangemaakt)</font></li>' || v_crlf;
    return;
  END IF;

  IF v_mwst_id is null and p_stoftest.mwst_code is not null THEN
    p_html_message := p_html_message || '<li>Stoftest ' || p_stoftest.stof_code || ': Meetwaardestructuur ' || p_stoftest.mwst_code || ' niet gevonden <font color="red">(stoftest niet aangemaakt)</font></li>' || v_crlf;
    return;
  END IF;

  IF v_stof_voor_id is null and p_stoftest.stof_voor_code is not null THEN
    p_html_message := p_html_message || '<li>Stoftest ' || p_stoftest.stof_code || ': Voortest ' || p_stoftest.stof_voor_code || ' niet gevonden <font color="red">(stoftest niet aangemaakt)</font></li>' || v_crlf;
    return;
  END IF;

  IF v_stof_samen_id is null and p_stoftest.samenstelling is not null THEN
    p_html_message := p_html_message || '<li>Stoftest ' || p_stoftest.stof_code || ': Samenstelling ' || p_stoftest.stof_voor_code || ' niet gevonden <font color="red">(stoftest niet aangemaakt)</font></li>' || v_crlf;
    return;
  END IF;

  BEGIN
    INSERT INTO kgc_stoftesten (kafd_id, Code, omschrijving, meet_reken, tech_id, mwst_id, stof_id_voor)
    VALUES( p_kafd_id
          , p_stoftest.stof_code
          , p_stoftest.stof_omschr
          , decode(upper(p_stoftest.meet_reken), 'VOORTEST', 'V', 'METING', 'M', 'BEREKENING', 'R', 'SAMENSTELLING', 'S')
          , v_tech_id
          , v_mwst_id
          , v_stof_voor_id
          );

    p_html_message := p_html_message || '<li>Stoftest ' || p_stoftest.stof_code || ' (' || p_stoftest.stof_omschr || ') aangemaakt</li>' || v_crlf;

  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    p_html_message := p_html_message || '<li>Stoftest ' || p_stoftest.stof_code || ' (' || p_stoftest.stof_omschr || ') bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;
  END;

  IF v_stof_samen_id is not null THEN
    -- Als de samenstellingscode is opgegeven, maak de samenstelling aan.
    BEGIN

      v_stof_id :=  kgc_uk2pk.stof_id(p_kafd_id, p_stoftest.stof_code);
      INSERT INTO helix.kgc_sub_stoftesten (stof_id_super, stof_id_sub)
      VALUES (v_stof_samen_id, v_stof_id);

      p_html_message := p_html_message || '<li>Stoftest samenstelling ' || p_stoftest.stof_code || ' met ' || p_stoftest.samenstelling || ' toegevoegd</li>' || v_crlf;

    EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
      p_html_message := p_html_message || '<li>Stoftest samenstelling ' || p_stoftest.stof_code || ' met ' || p_stoftest.samenstelling || ' bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;

    END;

  END IF;


EXCEPTION WHEN OTHERS THEN
  p_html_message := p_html_message || '<li><font color="red">Algemene fout tijdens verwerken stoftest ' || p_stoftest.stof_code || ': ' || sqlerrm || '</font></li>' || v_crlf;

END;


--############################################################################
-- Koppel een stoftest aan een stoftestgroep (protocol)
--
-- Parameter: p_stoftest     : Record met de te koppelen stoftest
-- Parameter: p_prot         : Record het te koppelen protocol
-- Parameter: p_kafd_id      : is het id van de afdeling waarvoor ingelezen wordt.
-- Parameter: p_html_message : OUT parameter voor bericht richting gebruiker.
--
--############################################################################
PROCEDURE beh_stft_koppel_stof_prot( p_stoftest c_beh_stoftest_inlees%rowtype
                                   , p_prot c_beh_prot_inlees%rowtype
                                   , p_kafd_id in kgc_kgc_afdelingen.kafd_id%type
                                   , p_html_message in out clob) IS
  v_crlf varchar2(2) := chr(13)||chr(10);
BEGIN
  -- Speciale uitzondering voor Robot protocol ION. Hier worden de metingen niet aan de groep gekoppeld, alleen de
  -- voortesten en de samenstelling worden gekoppeld.
  IF    substr(p_prot.groep_code, -2) = '_I'      -- Groepcode eindigd op _I
    AND instr(p_prot.groep_omschr, 'ION') > 0     -- Groepnaam bevat ION
    AND upper(p_stoftest.meet_reken) = 'METING'   -- Stoftest is meting
    AND upper(p_stoftest.tech_code) = 'ION'       -- Stoftest techniek is ION
  THEN
    p_html_message := p_html_message || '<li>Koppeling meting ' || p_stoftest.stof_code || ' aan ION protocol ' || p_prot.groep_code || ' overgeslagen</li>' || v_crlf;
    return;
  END IF;

  BEGIN
    INSERT INTO kgc_protocol_stoftesten (standaard, stgr_id, stof_id)
    VALUES ( 'J'
           , (select stgr_id from kgc_stoftestgroepen where code = p_prot.groep_code and kafd_id=p_kafd_id)
           , (select stof_id from kgc_stoftesten where code = upper(p_stoftest.stof_code) and kafd_id=p_kafd_id)
           );

    p_html_message := p_html_message || '<li>Stoftest ' || p_stoftest.stof_code || ' is gekoppeld aan protocol ' || p_prot.groep_code || '</li>' || v_crlf;

  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    p_html_message := p_html_message || '<li>Koppeling stoftest ' || p_stoftest.stof_code || ' aan protocol ' || p_prot.groep_code || ' bestond al <font color="orange">(niet aangemaakt)</font></li>' || v_crlf;
  END;

EXCEPTION WHEN OTHERS THEN
  p_html_message := p_html_message || '<li><font color="red">Algemene fout tijdens koppelen van stoftest ' || p_stoftest.stof_code || 'aan protocol ' || p_prot.groep_code || ': ' || sqlerrm || '</font></li>' || v_crlf;

END;


--############################################################################
-- Procedure stoftesten_inlezen om stoftestgroepen, stoftest-groep gebruik,
-- stoftesten en protocol stoftesten in te lezen via tabel BEH_STOFTESTEN_INLEES.
-- De tabel wordt gevuld door Helix interface definitie: BEH_STOFTESTEN_INLEES.
--
-- Parameter: p_kafd_code is de code van de afdeling waarvoor ingelezen wordt.
--
--############################################################################
PROCEDURE stoftesten_inlezen(p_kafd_code in varchar2) is

  v_procedure_naam varchar2(40) := 'beh_interfaces.stoftesten_inlezen';

  v_kafd_id         kgc_kgc_afdelingen.kafd_id%type;

  v_html_message    clob;
  v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                      , p_mede_id        => NULL
                                                                      )
                                        , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                        );
  v_email_aan       varchar2(100);
  v_email_onderwerp varchar2(100);
  v_job_onderwerp   varchar2(50);
  v_crlf            varchar2(2) := chr(13)||chr(10);

BEGIN

  v_kafd_id   := kgc_uk2pk.kafd_id(p_kafd_code);
  v_email_aan := beh_get_user_email;

  v_email_onderwerp := 'Helix overzicht STOFTESTEN inlezen voor ' || p_kafd_code;
  v_html_message    := 'Dit is een automatisch gegenereerd rapport over het inlezen van stoftesten, stoftestgroepen en protocollen voor afdeling ' || p_kafd_code || '.</br></br>' || v_crlf;

  if v_kafd_id is null then
    raise_application_error(-20000, 'Onbekende afdeling opgegeven.');
  end if;

  v_html_message    := v_html_message || 'Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Uitgevoerd door: ' || user || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Uitgevoerd in database: ' || ora_database_name || '</br></br>' || v_crlf;

  v_html_message := v_html_message || '<b>Verwerken protocollen</b></br>' || v_crlf;
  for r_prot in  c_beh_prot_inlees loop
    beh_stft_verwerk_prot(r_prot, v_kafd_id, p_kafd_code, v_html_message);
  end loop;

  v_html_message := v_html_message || '<b>Verwerken stoftesten</b>' || v_crlf;
  v_html_message := v_html_message || '<ul>';
  for r_stoftest in  c_beh_stoftest_inlees loop
    beh_stft_verwerk_stof(r_stoftest, v_kafd_id, v_html_message);
  end loop;
  v_html_message := v_html_message || '</ul>';

  v_html_message := v_html_message || '<b>Koppelen stoftesten aan protocollen</b>' || v_crlf;
  v_html_message := v_html_message || '<ul>';
  for r_stoftest in  c_beh_stoftest_inlees loop
    for r_prot in  c_beh_prot_inlees loop
      beh_stft_koppel_stof_prot(r_stoftest, r_prot, v_kafd_id, v_html_message);
    end loop;
  end loop;
  v_html_message := v_html_message || '</ul>';

  v_html_message   := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

  send_mail_html( p_from    => v_email_afzender
                , p_to      => v_email_aan
                , p_subject => v_email_onderwerp
                , p_html    => v_html_message
                );

  COMMIT;

EXCEPTION WHEN OTHERS THEN

  ROLLBACK;

  v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
  v_html_message    := 'FOUT:<font color="red">' || sqlerrm || '</font></br></br>' || v_crlf || v_html_message;
  v_html_message    := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

  send_mail_html( p_from    => v_email_afzender
                , p_to      => v_email_aan
                , p_subject => v_email_onderwerp
                , p_html    => v_html_message
                );

END stoftesten_inlezen;




--############################################################################
--# Maak een UNC path van een driveletter. Hiervoor wordt systeemparameter
--# met code UNC-<driveletter> gebruikt. Deze moeten afdelingsspecifiek
--# ingesteld zijn.
--############################################################################
FUNCTION unc_locatie(p_locatie in varchar2, p_kafd_id in kgc_kgc_afdelingen.kafd_id%type) return varchar2 is

  v_unc_path kgc_systeem_par_waarden.waarde%type;
  v_locatie  varchar2(1000);

BEGIN

  v_locatie := p_locatie;

  if substr(p_locatie, 2,2)=':\' then
    begin
      v_unc_path := kgc_sypa_00.standaard_waarde(p_parameter_code => 'UNC-'||upper(substr(p_locatie, 1,1)), p_kafd_id => p_kafd_id);
      v_locatie := v_unc_path || substr(p_locatie, 3);
    exception when others then
      null;
    end;
  end if;

  return v_locatie;

END;


--############################################################################
--# Zoek een regel met tekst: 'Quantify Compound Summary Report'.
--# Dit is de start van het report.
--############################################################################
PROCEDURE quanlynx_zoek_start(p_regel in varchar, p_html_message in out clob, p_state in out number) is
  v_crlf            varchar2(2) := chr(13)||chr(10);
BEGIN
  if trim(p_regel) = 'Quantify Compound Summary Report' then
    p_state := 1; --Begin gevonden; Start met zoeken naar 'Compound'
    --p_html_message := p_html_message || '<li>Juiste bestandsheader gevonden</li>' || v_crlf;
  end if;
END;


--############################################################################
--# Zoek een regel die begint met Compound... en lees daar de compound_name
--# van in. Zoek met deze naam of er een extra attribuutwaarde voor tabel
--# KGC_STOFTESTEN bestaat met deze naam.
--############################################################################
PROCEDURE quanlynx_zoek_compound( p_regel in varchar
                                , p_html_message in out clob
                                , p_state in out number
                                , p_compound_naam in out varchar
                                , p_stoftest_omschrijving in out bas_robot_uitslagen.stoftest_omschrijving%type
                                ) is
  v_crlf      varchar2(2) := chr(13)||chr(10);
  v_find_pos  number;

  cursor c_quanlynx_comp_code(b_compound_naam varchar2) is
  select stof.stof_id
       , stof.code         as stof_code
       , stof.omschrijving as stof_omschrijving
       , atwa_stof.waarde  as quanlynx_code_stof
  from   kgc_stoftesten        stof
    left join kgc_attributen   attr_stof on attr_stof.code = 'QUANLYNX_CODE' and attr_stof.tabel_naam = 'KGC_STOFTESTEN'
    join kgc_attribuut_waarden atwa_stof on atwa_stof.id = stof.stof_id and atwa_stof.attr_id = attr_stof.attr_id
  where  upper(atwa_stof.waarde) = upper(b_compound_naam);

  r_quanlynx_comp_code c_quanlynx_comp_code%rowtype;

BEGIN
  if trim(p_regel) like 'Compound%' then
    v_find_pos := instr(p_regel, ':');
    if v_find_pos > 0 then
      p_compound_naam := trim(substr(p_regel, v_find_pos+1));

      open c_quanlynx_comp_code(p_compound_naam);
      fetch c_quanlynx_comp_code into r_quanlynx_comp_code;
      if c_quanlynx_comp_code%found then

        p_state := 2; --Compound gevonden; Start met zoeken naar metingen.
        p_stoftest_omschrijving := r_quanlynx_comp_code.stof_omschrijving;
        p_html_message := p_html_message || '<li>Compound gevonden: <b>' || p_compound_naam || '</b> als stoftest '
                                         || p_stoftest_omschrijving || ' (' || r_quanlynx_comp_code.stof_code
                                         || ')</li></br>' || v_crlf;

      else

        p_html_message := p_html_message || '<li><font color="orange">' || ' De Counpound ''' || p_compound_naam
                                         || ''' is niet gevonden als extra attribuut op een stoftest en wordt niet verwerkt.</font></li></br>'
                                         || v_crlf;

      end if;
      close c_quanlynx_comp_code;

    end if;
  end if;
END;


--############################################################################
PROCEDURE quanlynx_ververk_meting( p_regel in varchar
                                 , p_html_message in out clob
                                 , p_state in out number
                                 , p_stoftest_omschrijving in out bas_robot_uitslagen.stoftest_omschrijving%type
                                 , p_session_id in number
                                 ) is

  v_crlf            varchar2(2) := chr(13)||chr(10);
  v_sep1            varchar2(1) := chr(9);
  v_sep2            varchar2(1) := '^';

  v_tekst_col1  varchar2(100);
  v_tekst_col2  varchar2(100);
  v_tekst_col3  varchar2(100);
  v_tekst_col3a varchar2(100);
  v_tekst_col3b varchar2(100);
  v_tekst_col3c varchar2(100);
  v_tekst_col4  varchar2(100);

  v_onderzoek   bas_robot_uitslagen.onderzoeknr%type;
  v_monster     bas_robot_uitslagen.monsternummer%type;
  v_protocol    bas_robot_uitslagen.protocol_code%type;
  v_waarde      bas_robot_uitslagen.meetwaarde_1%type;

  v_count_onderzoek number;
  v_count_monster   number;
  v_protocol_code   kgc_stoftestgroepen.code%type := null;

BEGIN

  -- Als we metingen aan het zoeken zijn en er staat een niet-lege regel,
  -- ga verder met het verwerken van die regel.
  if (p_state = 2) and (p_regel is not null) then
    p_state := 3;
    p_html_message := p_html_message || '<ul>';
  end if;

  -- Als we metingen aan het verwerken zijn en er we vinden een lege regel, ga weer
  -- terug naar het zoeken van een compound.
  if (p_state = 3) and (p_regel is null) then
    p_state := 1;
    p_html_message := p_html_message || '</ul></br>' || v_crlf;
    return;
  end if;

  --Kolommen in het bestand zijn verdeeld d.m.v. tab teken.
  v_tekst_col1 := regexp_substr(p_regel, '[^' || v_sep1 || ']+', 1, 1);
  v_tekst_col2 := regexp_substr(p_regel, '[^' || v_sep1 || ']+', 1, 2);
  v_tekst_col3 := regexp_substr(p_regel, '[^' || v_sep1 || ']+', 1, 3);
  v_tekst_col4 := regexp_substr(p_regel, '[^' || v_sep1 || ']+', 1, 4);

  --Kolom 3 is intern verdeeld d.m.v. ^ teken.
  v_tekst_col3a := regexp_substr(v_tekst_col3, '[^' || v_sep2 || ']+', 1, 1);
  v_tekst_col3b := regexp_substr(v_tekst_col3, '[^' || v_sep2 || ']+', 1, 2);
  v_tekst_col3c := regexp_substr(v_tekst_col3, '[^' || v_sep2 || ']+', 1, 3);

  if (v_tekst_col2 is not null) and (v_tekst_col3a is not null) and (v_tekst_col3b is not null) and (v_tekst_col4 is not null) then

    v_onderzoek := substr(translate(v_tekst_col2,  '_', '/'), 1, 20);
    v_monster   := substr(translate(v_tekst_col3a, '_', '/'), 1, 20);
    v_protocol  := substr(v_tekst_col3b, 1, 25);
    v_waarde    := substr(v_tekst_col4, 1, 100);

    select count(*)
    into   v_count_onderzoek
    from   kgc_onderzoeken
    where  onderzoeknr = v_onderzoek;

    select count(*)
    into   v_count_monster
    from   kgc_monsters
    where  monsternummer = v_monster;

    begin
      select code
      into   v_protocol_code
      from   kgc_stoftestgroepen
      where  code like v_protocol;
    exception when others then
      null;
    end;

    if (v_count_onderzoek=1) and (v_count_monster=1) and (v_protocol_code is not null)  then

      p_html_message := p_html_message || '<li> Meting voor Onderzoek:' || v_onderzoek
                                       || ', Monster:' || v_monster  ||', Protocol:' || v_protocol_code
                                       || ', Waarde:' || v_waarde || ' verwerkt</li>' || v_crlf;

      insert into bas_robot_uitslagen (STOFTEST_OMSCHRIJVING, ONDERZOEKNR, MONSTERNUMMER, PROTOCOL_CODE, MEETWAARDE_1, SESSIE)
      values (p_stoftest_omschrijving, v_onderzoek, v_monster, v_protocol_code, v_waarde, p_session_id);

    else

      p_html_message := p_html_message || '<li><font color="orange"> Meting voor Onderzoek:' || v_onderzoek  || ', Monster:' || v_monster  || ', Protocol:' || v_protocol  || ', Waarde:' || v_waarde || ' NIET verwerkt</font>';
      if (v_count_onderzoek<>1)    then p_html_message := p_html_message || '</br><font color="red">Onbekend onderzoek</font>' || v_crlf; end if;
      if (v_count_monster<>1)      then p_html_message := p_html_message || '</br><font color="red">Onbekend monster</font>' || v_crlf;   end if;
      if (v_protocol_code is null) then p_html_message := p_html_message || '</br><font color="red">Onbekend protocol</font>' || v_crlf;  end if;
      p_html_message := p_html_message || '</li>' || v_crlf;

    end if;

  end if;

END;


--############################################################################
PROCEDURE quanlynx_inlezen(p_locatie in varchar2, p_bestand in varchar2) is

  v_procedure_naam varchar2(40) := 'beh_interfaces.quanlynx_inlezen';

  v_kafd_id         kgc_kgc_afdelingen.kafd_id%type;

  v_html_message    clob;
  v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                      , p_mede_id        => NULL
                                                                      )
                                        , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                        );
  v_email_aan       varchar2(100);
  v_email_cc        varchar2(100) := 'rene.brand@radboudumc.nl';
  v_email_onderwerp varchar2(100);
  v_job_onderwerp   varchar2(50);
  v_session_id      number;
  v_crlf            varchar2(2) := chr(13)||chr(10);

  v_locatie         varchar2(1000);
  v_bestandsnaam    varchar2(1000);
  v_file_handle     UTL_FILE.FILE_TYPE;
  v_dir_obj_name    varchar2(20);
  v_regel           varchar2(32000);
  v_compound_naam   varchar2(100);
  v_state           number:=0;
  --State O=Init
  --    , 1=Zoek Compound
  --    , 2=Zoek metingen
  --    , 3=Verwerk metingen
  --    , 4=Error
  e_state_exception exception; -- Raised als State is Error

  v_stoftest_omschrijving  bas_robot_uitslagen.stoftest_omschrijving%type;

BEGIN

  v_kafd_id   := kgc_uk2pk.kafd_id('METAB');
  v_email_aan := beh_get_user_email;
  v_locatie   := unc_locatie(p_locatie, v_kafd_id);

  select sys_context('USERENV', 'SESSIONID')
  into   v_session_id
  from   dual;

  v_email_onderwerp := 'Helix overzicht QUANLYNX inlezen';
  v_html_message    := 'Dit is een automatisch gegenereerd rapport over het inlezen van Quanlynx gegevens.</br></br>' || v_crlf;

  v_html_message    := v_html_message || 'Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Uitgevoerd door: ' || user || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Sessie ID: ' || to_number(v_session_id) || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Bestandsnaam: ' || p_bestand || '</br>' || v_crlf;
  v_html_message    := v_html_message || 'Locatie: ' || v_locatie || '</br></br>' || v_crlf;

  begin
    v_dir_obj_name := beh_file_00.create_directory_object(v_locatie);
    v_file_handle := utl_file.fopen(v_dir_obj_name, p_bestand, 'R');
  exception when others then
    null;
  end;

  if utl_file.is_open(v_file_handle) then
    --Input bestand is geopend, start de verwerking.

    --Tabel opschonen.
    delete from bas_robot_uitslagen
    where  SESSIE = v_session_id;

    loop
      begin
        utl_file.get_line(v_file_handle, v_regel, 32000);

        case
          when v_state = 0 then
            quanlynx_zoek_start(v_regel, v_html_message, v_state);
          when v_state = 1 then
            quanlynx_zoek_compound(v_regel, v_html_message, v_state, v_compound_naam, v_stoftest_omschrijving);
          when v_state = 2 or v_state = 3 then
            quanlynx_ververk_meting(v_regel, v_html_message, v_state, v_stoftest_omschrijving, v_session_id);
          else
            --Raise exception als er een error state is.
            raise e_state_exception;
        end case;

      exception when no_data_found then
        exit;
      end;
    end loop;

    v_html_message := v_html_message || '</ul>' || v_crlf;

    -- Bij juiste verwerking eindig de hoofd loop met het verwerken van metingen (v_state=2)
    -- Geef een melding als dit niet het geval is.
    if (v_state = 0) then
      v_html_message := v_html_message || 'FOUT:<font color="red">' || ' De verwachtte bestandsheader is niet gevonden!</font></br></br>' || v_crlf;
    elsif (v_state = 1) and (v_compound_naam is null) then
      v_html_message := v_html_message || 'FOUT:<font color="red">' || ' De Counpound is niet gevonden!</font></br></br>' || v_crlf;
    else
      --De origineele QUANLYNX_VAN interface draaide de duplo procedure na het laden.
      --Dit is overgenomen naar de nieuwe versie (maar nu met sessie_id)
      kgc_idef_00.duplo(v_session_id);
    end if;

  else
    v_html_message := v_html_message || 'FOUT:<font color="red">' || ' Bestand ''' || p_bestand || ''' kan niet geopend worden op locatie ''' || v_locatie || '''.</font></br></br>' || v_crlf;
  end if;

  v_html_message := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

  utl_file.fclose(v_file_handle);
  beh_file_00.remove_directory_object(v_dir_obj_name);

  send_mail_html( p_from    => v_email_afzender
                , p_to      => v_email_aan
                , p_cc      => v_email_cc
                , p_subject => v_email_onderwerp
                , p_html    => v_html_message
                );

  COMMIT;

EXCEPTION WHEN OTHERS THEN

  ROLLBACK;

  utl_file.fclose(v_file_handle);
  beh_file_00.remove_directory_object(v_dir_obj_name);

  v_html_message    := v_html_message || '</ul>';
  v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
  v_html_message    := v_html_message || 'FOUT:<font color="red">' || sqlerrm || '</font></br></br>' || v_crlf;
  v_html_message    := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

  send_mail_html( p_from    => v_email_afzender
                , p_to      => v_email_aan
                , p_cc      => v_email_cc
                , p_subject => v_email_onderwerp
                , p_html    => v_html_message
                );

END quanlynx_inlezen;


END beh_interfaces;
/

/
QUIT
