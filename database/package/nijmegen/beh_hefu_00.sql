CREATE OR REPLACE PACKAGE "HELIX"."BEH_HEFU_00" is
--algemene plsql (HE)lix (FU)ncties/procedures
-- hier zijn functies opgenomen die een directe
-- verband hebben met Helix data en afleidingen hiervan
function aantal_decimalen
(
p_meet_id in number
) return number;

FUNCTION aantal_mons_stoftesten
( p_monsternummer in varchar2
, p_stof_code in varchar2
) return number;
function set_context_value
(
p_context in varchar,
p_attribute in varchar,
p_value in varchar
)
return number;
end beh_hefu_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_HEFU_00" is
FUNCTION aantal_decimalen
( p_meet_id IN number)
RETURN  number
IS
v_aantal_decimalen number;
BEGIN
-- Bepalen aantal decimalen voor monster
select nvl(nvl(prst.aantal_decimalen, stof.aantal_decimalen), 0)
    into v_aantal_decimalen
from    bas_meetwaarden meet,
        bas_metingen meti,
        kgc_stoftesten stof,
        kgc_stoftestgroepen stgr,
        kgc_protocol_stoftesten prst
where meet.stof_id = stof.stof_id
and stof.stof_id = prst.stof_id(+)
and prst.stgr_id = stgr.stgr_id
and meet.meti_id = meti.meti_id
and meti.stgr_id = stgr.stgr_id
and meet.meet_id = p_meet_id
 ;
return v_aantal_decimalen;
end aantal_decimalen;
FUNCTION aantal_mons_stoftesten
( p_monsternummer IN varchar2
, p_stof_code IN varchar2
)
RETURN  number
IS
v_aantal number;
BEGIN
select count(*) into v_aantal
from
kgc_monsters mons,
bas_fracties frac,
bas_metingen meti,
bas_meetwaarden meet,
kgc_stoftesten stof,
kgc_stoftestgroepen stgr
where
 mons.mons_id = frac.mons_Id
and frac.frac_id = meti.frac_id
and meti.meti_id = meet.meti_id
and meti.stgr_id = stgr.stgr_id
and meet.stof_id = stof.stof_id
and upper(mons.monsternummer) =  upper(p_monsternummer)
and upper(meet.afgerond) = upper('N')
and meet.stof_id = (select stof_id from kgc_stoftesten where upper(code) = upper(p_stof_code))
;
return v_aantal;
END aantal_mons_stoftesten;
function set_context_value
(
p_context in varchar,
p_attribute in varchar,
p_value in varchar
)
return number
is
begin
    dbms_session.set_context(
                                upper(p_context)
                              , upper(p_attribute)
                              , p_value
                               );
return 1;
exception
 when others then
    return 0;
end set_context_value;
end beh_hefu_00;
/

/
QUIT
