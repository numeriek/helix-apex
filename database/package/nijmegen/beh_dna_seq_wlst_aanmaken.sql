CREATE OR REPLACE PACKAGE "HELIX"."BEH_DNA_SEQ_WLST_AANMAKEN" IS

/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 25-03-2013

DOEL
Start het aanmaken van werklijsten in Helix die van de Robot naar Lims gaan

INPUT PARAMETERS
P_TECHNIEK  : De gebruikte techniek: SEQ
P_AFDELING  : De afdeling waarvoor de gegevens uit de Robot gehaald moeten worden:  GENOOM
P_TRTY      : Gebruikte tray type: 96-WELL
P_LOGFILE   : DEFAULT '\\umcseqfac01\TEMP_IONTORRENT_LOG$'

OUTPUT PARAMETERS

***********************************************************************/


GV_REVISIE_VERSIE CONSTANT VARCHAR2(10) := '1.0.7';

/* revisie historie */
FUNCTION REVISION
 RETURN VARCHAR2;

PROCEDURE START_ROBOT_WERKLIJST_AANMAKEN
 (P_TECHNIEK   IN VARCHAR2 -- PCR
 ,P_AFDELING   IN VARCHAR2 DEFAULT 'GENOOM'
 ,P_TRTY       IN VARCHAR2 -- 96-WELL , code van de tray meegeven
 ,P_LOGFILE    IN VARCHAR2 DEFAULT '\\umcseqfac01\TEMP_IONTORRENT_LOG$'
 );

END BEH_DNA_SEQ_WLST_AANMAKEN;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DNA_SEQ_WLST_AANMAKEN" IS

C_SESSION_ID   NUMBER;
C_VOLGNR       NUMBER := 1;
V_LOGFILE_INHOUD CLOB;
C_NIEUWE_REGEL   varchar2(10) := chr(10);


/* revisie historie */
FUNCTION REVISION
 RETURN VARCHAR2
 IS
/***********************************************************************
Auteur/bedrijf  : Yuri Arts, YA IT-Services
Datum creatie   : 25-02-2012

BESCHRIJVING
Beschrijf hier de wijzigingen in de package.

HISTORIE

Wanneer /      Wie             Wat
Versie
------------------------------------------------------------------------
09-09-2013     Y.Arts          Logging wegschrijven naar bestand ipv verzenden via email
1.0.7
25-03-2013     Y.Arts          1. afhandeling package aanpassen zodat iedere plate afzonderlijk afgehandeld en opgeslagen wordt.
1.0.6                          2. De helixstatus updaten naar AFGEROND als het onderzoek ik afgerond.
                               3. De helixstatus pas updaten als alles is overgezet, achteraf controle inbouwen.
                               4. Voortesten niet opnieuw aanmelden, controle query toevoegen.
                               5. SEQWerklijst weggschrijven in Sequencefacility tabel in de kolom HELIX_worklist.
                               6. Stoftesten altijd aanmelden, controle verwijderen of deze al op een werklijst staat.
                               7. MeetID 99 invullen als onderzoek al is afgerond. Query toevoegen en update statement achteraf.
                               8. Foutafhandeling (exception) uitbreiden. Op verschillende niveaus controles inbouwen en rapporteren als er iets foutgaat.
                                  We gaan ook per plaat verwerken, per plaat rapporteren.
                               9. email afzender nu ophalen uit systeemparameter BEH_MIJN_HELIX_EMAIL_ADRES

06-06-2012     Y.Arts          De uitvoertekst, dat in de mail gezet wordt, moet gecontroleerd worden op 32766 posities.
1.0.5                          Als het toe te voegen tekst ervoor zorgt dat het aantal hier overheen gaat, mag het niet toegevoegd worden.
                               Dit is om fouten te voorkomen.
04-06-2012     Y.Arts          Bij het tijdelijk aanmaken van de werklijst records moet archief op N gezet worden.
1.0.4                          Mocht er niets op de werklijst gezet kunnen worden dan moet de werklijst verwijderd worden.
                               Voor het verwijderd kan worden moet archief op 'J' gezet worden.
25-02-2012     Y.Arts /        Procedure losgemaakt van package BEH_KGC_WERKLIJST_TRAY_ETIKET
1.0.3          M.Golebioswska
14-07-2011     Y.Arts          1. Rekening houden met afdeling bij de selecties van meetwaarden
1.0.2                          2. Default 'GENOOM' meegeven aan procedure
                               3. De plates pas verwerken als alle gegevens op de plate dezelfde status hebben (seqf_grp_cur)
27-03-2011     Y.Arts          Controles en email ingebouwd, het kan voorkomen dat een pcr_meetid in de sequencefacility tabel leeg is.
1.0.1
05-12-2010     Y.Arts          Creatie
1.0.0
***********************************************************************/
BEGIN
  RETURN gv_revisie_versie;
END;


procedure controleer_bestand
( p_bestand    in varchar2
, p_locatie    in varchar2
, p_dbs_client in varchar2
, p_rw         in varchar2
)
is
  out_file_client utl_file.file_type;
  out_file_dbs    utl_file.file_type;
begin
  -- controleer opgegeven bestand
  IF ( p_bestand IS NOT NULL )
  THEN
    BEGIN
      if ( p_dbs_client = 'CLIENT' )
      then
        out_file_client  := utl_file.fopen(location  => p_locatie
                                          ,filename  => p_bestand
                                          ,open_mode => p_rw);
      elsif ( p_dbs_client = 'DBS' )
      then
        out_file_dbs  := utl_file.fopen(location  => p_locatie
                                      ,filename  => p_bestand
                                      ,open_mode => p_rw);
      end if;

      IF ( utl_file.is_open( file => out_file_client ) )
      THEN
        utl_file.fclose( file =>  out_file_client );
      END IF;
      IF ( utl_file.is_open( file => out_file_dbs ) )
      THEN
        utl_file.fclose( file => out_file_dbs );
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        IF ( utl_file.is_open( file => out_file_client ) )
        THEN
          utl_file.fclose( file => out_file_client );
        END IF;
        IF ( utl_file.is_open( file => out_file_dbs ) )
        THEN
          utl_file.fclose( file => out_file_dbs );
        END IF;
        qms$errors.show_message
          ( p_mesg => 'KGC-00000'
          , p_param0 => 'Bestand '||p_bestand||' kan niet geopend worden'
          , p_param1 => 'Controleer locatie en naam van het bestand'
                        ||CHR(10)||SQLERRM
          , p_errtp => 'E'
          , p_rftf => TRUE
          );
    END;
  END IF;
end controleer_bestand;

procedure clob_to_file( p_dir in varchar2,
                        p_file in varchar2,
                        p_clob in clob )
as
  out_file utl_file.file_type;
  l_amt    number default 32000;
  l_offset number default 1;
  l_length number default nvl(dbms_lob.getlength(p_clob),0);

  v_statement         VARCHAR2(32767);
  v_statement_client  VARCHAR2(100);
  v_client_locatie    VARCHAR2(100);  -- werkelijke uitvoerlocatie
  v_client_bestand    VARCHAR2(100);  -- werkelijk uitvoerbestand

BEGIN

  -- verwijder de Oracle directory mocht deze nog bestaan. Dit om fouten te voorkomen
  BEGIN
    v_statement := 'drop directory clientdir';
    execute immediate v_statement;
  EXCEPTION
    when others then
      null;
  END;

  -- formatteer de locatienaam en bestandsnaam
  v_client_locatie := RTRIM( p_dir, '\')||'\'; --'\\umcseqfac01\';
  v_client_bestand := p_file;

  -- maak de Oracle directory aan waar de logfile moet komen staan
  v_statement_client := 'create directory clientdir as '||''''||v_client_locatie||'''';
  execute immediate v_statement_client;

  IF ( p_file IS NOT NULL )
  THEN
    controleer_bestand
    ( p_locatie    => 'CLIENTDIR'
    , p_bestand    => v_client_bestand
    , p_dbs_client => 'CLIENT'
    , p_rw         => 'w'
    );

    out_file  := utl_file.fopen(location    => 'CLIENTDIR'
                              , filename    => v_client_bestand
                              , open_mode   => 'w'
                              , max_linesize => 32760 );

    while ( l_offset < l_length )
    loop
      utl_file.put(out_file,
                 dbms_lob.substr(p_clob,l_amt,l_offset) );
      utl_file.fflush(out_file);
      l_offset := l_offset + l_amt;
    end loop;
    utl_file.new_line(out_file);
    utl_file.fclose(out_file);
  END IF;

  -- drop directories
  v_statement_client     := 'drop directory CLIENTDIR';
  execute immediate v_statement_client;

EXCEPTION
  WHEN OTHERS
  THEN
    IF ( utl_file.is_open( file => out_file ) )
    THEN
      utl_file.fclose( file => out_file );
    END IF;
    RAISE;

end;

PROCEDURE gebruikers_info (p_tekst in varchar2) is
begin
  v_logfile_inhoud := v_logfile_inhoud || to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') || ': ' || p_tekst || c_nieuwe_regel;
--  v_logfile_inhoud := v_logfile_inhoud||p_tekst||c_nieuwe_regel;
--  dbms_output.put_line(p_tekst);
END;

PROCEDURE START_ROBOT_WERKLIJST_AANMAKEN
 (P_TECHNIEK   IN VARCHAR2 -- SEQ
 ,P_AFDELING   IN VARCHAR2 DEFAULT 'GENOOM'
 ,P_TRTY       IN VARCHAR2 -- 96-WELL
 ,P_LOGFILE    IN VARCHAR2 DEFAULT '\\umcseqfac01\TEMP_IONTORRENT_LOG$'
 )
 IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 05-12-2010

DOEL
Start het aanmaken van werklijsten in Helix die van de Robot naar Lims gaan

1.  Verwijder alles uit de tijdelijke tabel voor deze sessie
2.  Bepaal de afdelings ID op basis van P_AFDELING
3.  Haal de tray type ID op voor de tray vulling
4.  Haal de tech_id op
5.  Bepaal op basis van de sequencefacility@storage tabel de unieke werklijstnummers
6.  Genereer een nieuw werklijstnummer op basis van de nummerstructuur ROBOT_WERK
7.  Haal de mede_id op van de ROBOT medewerker
8.  Als de medewerker ROBOT niet bestaat dan geef hiervan een melding, ga verder met mede_id = 1
9.  Maak een TRAY GEBRUIK aan in de tabel KGC_TRAY_GEBRUIK
10. Vul de hulptabel met tray gebruik
11. Maak een werklijst aan in Helix voor ieder uniek voorkomen, sequenceplateid = werklijstnummer
12. Selecteer alle stoftesten die in Helix op een nieuwe werklijst/tray gezet moeten worden
13. stof_id bepalen van seq op basis van omschrijving in seq facility tabel
14. Als de stoftest niet bestaat in Helix dan geef hiervan een melding en ga door met de volgende
15. Meestal is de meting al aangemeld in Helix. Selecteer de betreffende meet_id in Helix bas_meetwaarden
16. Als de meting nog niet is aangemeld dan moet dit alsnog gedaan worden
17. Vul de werklijst met de gevonden stoftest
18. Update de sequencefacility tabel, zet helixstatus = LOGGED en vul de meetid met de nieuwe meet_id uit Helix
19. Tray vullen
20. Per werklijst/tray gebruik moeten de meetwaarden op tray gezet worden
21. synchroniseer de werklijst


INPUT PARAMETERS
P_TECHNIEK  : De gebruikte techniek: SEQ
P_AFDELING  : De afdeling waarvoor de gegevens uit de Robot gehaald moeten worden:  GENOOM
P_TRTY      : Gebruikte tray type: 96-WELL
P_LOGFILE   : DEFAULT '\\umcseqfac01\TEMP_IONTORRENT_LOG$'

OUTPUT PARAMETERS


Wanneer/       Wie             Wat
Versie
------------------------------------------------------------------------
09-09-2013     Y.Arts          Logging wegschrijven naar bestand ipv verzenden via email
1.0.8
25-03-2013     Y.Arts          1. afhandeling package aanpassen zodat iedere plate afzonderlijk afgehandeld en opgeslagen wordt.
1.0.6                          2. De helixstatus updaten naar AFGEROND als het onderzoek ik afgerond.
                               3. De helixstatus pas updaten als alles is overgezet, achteraf controle inbouwen.
                               4. Voortesten niet opnieuw aanmelden, controle query toevoegen.
                               5. Stoftesten altijd aanmelden, controle verwijderen of deze al op een werklijst staat.
                               6. MeetID 99 invullen als onderzoek al is afgerond. Query toevoegen en update statement achteraf.
                               7. Foutafhandeling (exception) uitbreiden. Op verschillende niveaus controles inbouwen en rapporteren als er iets foutgaat.
                                  We gaan ook per plaat verwerken, per plaat rapporteren.
                               8. email afzender nu ophalen uit systeemparameter BEH_MIJN_HELIX_EMAIL_ADRES

04-06-2012     Y.Arts          Bij het tijdelijk aanmaken van de werklijst records moet archief op N gezet worden.
1.0.4                          Mocht er niets op de werklijst gezet kunnen worden dan moet de werklijst verwijderd worden.
                               Voor het verwijderd kan worden moet archief op 'J' gezet worden.
23-03-2012     Y.Arts          Bij het aanmaken van werklijst items moet de waarde van het veld 'afgerond' altijd 'N' zijn.
1.0.3
14-07-2011     Y.Arts          1. Rekening houden met afdeling bij de selecties van meetwaarden
1.0.2                          2. Default 'GENOOM' meegeven aan procedure
                               3. De plates pas verwerken als alle gegevens op de plate dezelfde status hebben (seqf_grp_cur)
27-03-2011     Y.Arts          Controles en email ingebouwd, het kan voorkomen dat een pcr_meetid in de sequencefacility tabel leeg is.
1.0.1
05-12-2010     Y.Arts          Creatie
1.0.0
***********************************************************************/

  cursor trty_cur (b_tray_type in varchar2
                  ,b_kafd_id   in number)
  is
  select trty_id                       -- number
  ,      horizontaal   hor_num_totaal  -- number
  ,      indicatie_hor hor_indicatie   -- varchar2(1)
  ,      verticaal     ver_num_totaal  -- number
  ,      indicatie_ver ver_indicatie   -- varchar2(1)
  from   kgc_tray_types
  where  code    = b_tray_type
  and    kafd_id = b_kafd_id
  ;

  cursor tech_cur (b_tech_code in varchar2)
  is
  select tech_id
  from   kgc_technieken tech
  where  tech.code = b_tech_code
  and    vervallen = 'N'
  ;

  cursor kafd_cur (b_afdeling in varchar2)
  is
  select kafd_id
  from   kgc_kgc_afdelingen
  where  code = b_afdeling
  ;

  cursor seqf_grp_cur (b_kafd_id in number)
  is

  select distinct seqf1.sequenceplateid werklijstnr, seqf1.helixstatus
  from   sequencefacility@storage seqf1
  ,      bas_meetwaarden meet
  ,      bas_metingen    meti
  ,      kgc_onderzoeken onde
  where  seqf1.helixstatus = 'INITIAL'
  and    seqf1.pcr_meetid is not null
  and    seqf1.pcr_meetid in (  select seqf.pcr_meetid
                                from   sequencefacility@storage seqf
                                ,      bas_meetwaarden meet
                                ,      bas_metingen    meti
                                ,      kgc_onderzoeken onde
                                ,      kgc_onderzoek_monsters onmo
                                where  seqf.helixstatus     = 'INITIAL'
                                and    seqf.sequenceplateid = seqf1.sequenceplateid
                                and    seqf.pcr_meetid      = meet.meet_id
                                and    meet.meti_id         = meti.meti_id
                                and    meti.onde_id         = onde.onde_id
                                and    onde.onde_id         = onmo.onde_id
                                and    onde.afgerond        = 'N')
  and  seqf1.sequenceplateid not in (select seqf2.sequenceplateid
                                     from   sequencefacility@storage seqf2
                                     where  (nvl(seqf2.helixstatus,1) <> seqf1.helixstatus -- nvl met 1 omdat anders lege waarden niet gezien worden
                                             or  (seqf2.pcr_meetid is null and UPPER(seqf2.dna_id) NOT IN ('PGEM', 'CONTROLE')) --- MGOL Helix moet alles controleren behalve PGEM en Controle fracties
                                            )
                                     and    seqf2.sequenceplateid = seqf1.sequenceplateid
                                    )
  and    seqf1.pcr_meetid      = meet.meet_id
  and    meet.meti_id         = meti.meti_id
  and    meti.onde_id         = onde.onde_id
  and    onde.kafd_id         = 1--b_kafd_id
  and    onde.afgerond        = 'N' -- toegevoegd op 08-05-2012
  group by sequenceplateid,seqf1.helixstatus
  order by seqf1.sequenceplateid
  ;

  cursor meet_id_not_in_helix_cur (b_kafd_id in number)
  is

  select pcr_meetid
  ,      dna_id
  ,      seqprimerid
  ,      investigation_id
  from   sequencefacility@storage seqf1
  where  1=1
  and    seqf1.helixstatus = 'INITIAL'
  and    seqf1.pcr_meetid is not null
  and    seqf1.pcr_meetid not in (  select seqf.pcr_meetid
                                from   sequencefacility@storage seqf
                                ,      bas_meetwaarden meet
                                ,      bas_metingen    meti
                                ,      kgc_onderzoeken onde
                                ,      kgc_onderzoek_monsters onmo
                                where  seqf.helixstatus     = 'INITIAL'
                                and    seqf.sequenceplateid = seqf1.sequenceplateid
                                and    seqf.pcr_meetid      = meet.meet_id
                                and    meet.meti_id         = meti.meti_id
                                and    meti.onde_id         = onde.onde_id
                                and    onde.onde_id         = onmo.onde_id
                                and    onde.afgerond        = 'N'
                                and    onde.kafd_id         = b_kafd_id)
  ;


  cursor seqf_cur(b_sequenceplateid in varchar2
                 ,b_kafd_id         in number)
  is

  select distinct seqf.pcr_meetid pcr_meetid
  ,      meet.stof_id          stof_id
  ,      onde.ongr_id          ongr_id
  ,      frac.mons_id          mons_id
  ,      meet.mede_id_controle mede_id_controle
  ,      meet.mest_id          mest_id
  ,      'Lims werklijstnr'    commentaar
  ,      meet.meti_id          meti_id
  ,      meet.afgerond         afgerond
  ,      meti.frac_id          frac_id
  ,      onde.onde_id          onde_id
  ,      seqf.seqprimerid      seqprimerid
  ,      seqf.primer           primer
  ,      stof.code stoftest_code
  ,      STOF.OMSCHRIJVING stoftest_omschrijving
  from   sequencefacility@storage seqf
  ,      bas_meetwaarden meet
  ,      bas_metingen    meti
  ,      kgc_onderzoeken onde
  ,      bas_fracties    frac
  ,      kgc_stoftesten  stof
  where  1=1
  and    seqf.helixstatus     = 'INITIAL'
  and    seqf.sequenceplateid = b_sequenceplateid
  and    seqf.pcr_meetid      = meet.meet_id
  and    meet.meti_id         = meti.meti_id
  and    meti.onde_id         = onde.onde_id
  and    meti.frac_id         = frac.frac_id
  and    onde.afgerond        = 'N'
  and    onde.kafd_id         = b_kafd_id
  and    stof.stof_id         = meet.stof_id
  order by seqf.pcr_meetid
  ;

  cursor seq_stof_cur (b_omschrijving in varchar2
                      ,b_kafd_id      in number)
  is
  select stof_id
  from   kgc_stoftesten
  where  omschrijving = b_omschrijving
  and    kafd_id      = b_kafd_id
  and    vervallen    = 'N'
  ;

  cursor mede_cur (b_code in varchar2)
  is
  select mede_id
  from kgc_medewerkers

  where code = b_code
  ;

  -- selecteer de unieke wlst_id
  cursor wlst_cur
  is

  select wlst.wlst_id
  ,      trge.trge_id
  ,      wlst.werklijst_nummer
  ,      trge.sequenceplateid
  from   beh_hlp_tray_gebruik trge
  ,      kgc_werklijsten      wlst
  where  trge.session_id       = c_session_id
  and    trge.werklijst_nummer = wlst.werklijst_nummer
  ;

  -- selecteer de meet_id's die nog op tray gezet moeten worden
  -- selecteer meteen de positie waarop ze op de tray moeten komen
  cursor wlst_meet_cur (b_wlst_id in number
                       ,b_sequenceplateid in varchar2)
  is
  select wlst.meet_id
  ,      wlst_id
  ,      substr(seqf.seq_positionid,1,1) y_positie
  ,      substr(seqf.seq_positionid,2,2) x_positie
  from   kgc_werklijst_items wlst
  ,      sequencefacility@storage seqf
  where  wlst.wlst_id = b_wlst_id
  and    wlst.meet_id = seqf.meetid
  and    seqf.sequenceplateid = b_sequenceplateid
  order by y_positie, x_positie
  ;


  cursor meet_cur (b_stof_id in number
                  ,b_meti_id in number)
  is

  select meet.meet_id
  from bas_meetwaarden meet
  where meet.stof_id = b_stof_id
  and   meet.meti_id = b_meti_id
  and   meet.afgerond = 'N'
  and not exists (select 1 from kgc_tray_vulling trvu where trvu.meet_id = meet.meet_id )
  and not exists (select 1 from kgc_werklijst_items wlit where wlit.meet_id = meet.meet_id)
  ;

  -- Selecteerd sequenceplateid waarbij helixstatus = 'INITIAL' en pcr_meetid is leeg
  cursor seqf_uitval_cur
  is
  select distinct dna_id
  ,      seqprimerid
  ,      investigation_id
  from   sequencefacility@storage
  where  helixstatus = 'INITIAL'
  and    pcr_meetid is null
  ;

  --selecteerd de op te pakken records uit de storage maar waarvan het onderzoek is afgerond.
  cursor onde_afgerond_cur (b_kafd_id in number)
  is

  select seqf.pcr_meetid pcr_meetid
  ,      seqf.sequenceplateid
  ,      seqf.primer           primer
  from   sequencefacility@storage seqf
  ,      bas_meetwaarden meet
  ,      bas_metingen    meti
  ,      kgc_onderzoeken onde
  ,      kgc_onderzoek_monsters onmo
  where  seqf.helixstatus     <> 'AFGEROND'
  and    seqf.pcr_meetid      = meet.meet_id
  and    meet.meti_id         = meti.meti_id
  and    meti.onde_id         = onde.onde_id
  and    onde.onde_id         = onmo.onde_id
  and    onde.afgerond        = 'J'
  and    onde.kafd_id         = b_kafd_id
  order by seqf.pcr_meetid
  ;

  cursor wlst_niet_gevuld_cur (b_wlst_id in number)
  is
  select 1
  from   kgc_werklijst_items
  where  wlst_id = b_wlst_id
  ;

  v_kafd_id          kgc_kgc_afdelingen.kafd_id%type;
  v_wlst_id          kgc_werklijsten.wlst_id%type;
  v_meet_id          bas_meetwaarden.meet_id%type;
  v_seq_meet_id      bas_meetwaarden.meet_id%type;
  v_volgnummer       number;
  v_melding          varchar2(4000);
  v_werklijst_nummer kgc_werklijsten.werklijst_nummer%type;
  v_seq_stof_id      kgc_stoftesten.stof_id%type;
  v_mede_id          kgc_medewerkers.mede_id%type;
  v_fout             boolean;

  v_tech_id          kgc_tray_gebruik.tech_id%type;
  v_trty_id          kgc_tray_gebruik.trty_id%type;
  v_trge_id          kgc_tray_gebruik.trge_id%type;
  v_wlst_aanwezig    number := 0;

  -- variabelen tbv de tray vulling
  v_y_positie        varchar2(1); -- a
  v_x_positie        number; -- 1

  trty_rec           trty_cur%rowtype;

  -- logfile
  v_onderwerp    varchar2(100) := 'Genoom sequence werklijsten';

  -- exceptions
  ex_verkeerde_parameters exception;
  ex_werklijstnummer      exception;

  procedure werklijst
  ( p_meet_id          IN NUMBER
  , p_actie            IN VARCHAR2
  , p_werklijst_nummer IN VARCHAR2
  )
  is
  begin
    kgc_wlst_00.sync_tray_werklijst
    ( p_actie            => p_actie
    , p_meet_id          => p_meet_id
    , p_werklijst_nummer => p_werklijst_nummer
    );
  end;

  function stoftest_opnieuw_aanmelden
  ( p_meti_id in number
  , p_stof_id in number
  , p_frac_id in number
  , p_onde_id in number
  ) return number
  is
    v_fout boolean;
    v_melding varchar2(4000);

  begin

    v_meet_id := bas_meti_00.stoftest_aanmelden
    ( p_meti_id  => p_meti_id
    , p_stof_id  => p_stof_id
    , p_frac_id  => p_frac_id
    , p_onde_id  => p_onde_id
    , p_in_duplo => true
    , p_commit   => false
    , p_incl_voortest => false
    );

    return v_meet_id;
  end
  ;

BEGIN

  gebruikers_info( p_tekst => 'Start robot werklijst aanmaken');
  gebruikers_info( p_tekst => to_char(sysdate,'DayDD Month RRRR HH24:MI') );
  gebruikers_info( p_tekst => ' ' );
  gebruikers_info( p_tekst => 'p_techniek         : '||p_techniek);
  gebruikers_info( p_tekst => 'p_afdeling         : '||p_afdeling);
  gebruikers_info( p_tekst => 'p_trty, tray type  : '||p_trty);
  gebruikers_info( p_tekst => 'p_logfile          : '||p_logfile);
  gebruikers_info( p_tekst => ' ');

   -- bepaal de sessionid voor gebruik in tijdelijke tabellen.
   select sys_context ('USERENV', 'SESSIONID') into c_session_id from dual;

   --1. Verwijder alles uit de tijdelijke tabel voor deze sessie
   -- De procedure kan meerdere keren gestart zijn natuurlijk
   delete beh_hlp_tray_gebruik where session_id = c_session_id;

    --2. Bepaal de afdelings ID op basis van P_AFDELING
   open  kafd_cur (b_afdeling => p_afdeling);
   fetch kafd_cur into v_kafd_id;
   close kafd_cur;

   if v_kafd_id is null then
     gebruikers_info(p_tekst => 'De afdeling kan niet gevonden worden met ingegeven parameter '||p_afdeling);
     RAISE ex_verkeerde_parameters;
   end if;

   --3. Haal de tray type ID op voor de tray vulling
   open  trty_cur (b_tray_type => p_trty
                  ,b_kafd_id   => v_kafd_id);
   fetch trty_cur into trty_rec;
   close trty_cur;
   if trty_rec.trty_id is null then
     gebruikers_info(p_tekst => 'De tray type kan niet bepaald worden met ingegeven parameter '||p_trty);
     RAISE ex_verkeerde_parameters;
   end if;

   --4. Haal de tech_id op
   open  tech_cur (b_tech_code => p_techniek);
   fetch tech_cur into v_tech_id;
   close tech_cur;
   if v_tech_id is null then
     gebruikers_info(p_tekst => 'De techniek kan niet bepaald worden met ingegeven parameter '||p_techniek);
     RAISE ex_verkeerde_parameters;
   end if;

   -- 7. Haal de mede_id op van de ROBOT medewerker
   open  mede_cur (b_code => 'ROBOT');
   fetch mede_cur into v_mede_id;
   close mede_cur;

   -- 8. Als de medewerker ROBOT niet bestaat dan geef hiervan een melding, ga verder met mede_id = 1
   if v_mede_id is null
   then
     gebruikers_info(p_tekst => 'ROBOT medewerker bestaat niet. Ingelogde medewerker ID gebruiken.');
     v_mede_id := kgc_mede_00.medewerker_id;
   end if;

   -- Controleer of er pcr_meetid's gevuld zijn voor de werklijst
   for seqf_uitval_rec in seqf_uitval_cur loop
     gebruikers_info(p_tekst => 'pcr_meetid leeg, '||seqf_uitval_rec.dna_id||', '||seqf_uitval_rec.seqprimerid||', '||seqf_uitval_rec.investigation_id);
   end loop;

   --Deze melding in de logfile kost erg veel tijd!
   --for meet_id_not_in_helix_rec in meet_id_not_in_helix_cur(b_kafd_id => v_kafd_id) loop
   --  gebruikers_info(p_tekst => 'pcr_meetid ' ||meet_id_not_in_helix_rec.pcr_meetid||' niet in Helix: '||meet_id_not_in_helix_rec.dna_id||' ,'||meet_id_not_in_helix_rec.seqprimerid||', '||meet_id_not_in_helix_rec.investigation_id);
   --end loop;

   --5. Bepaal op basis van de sequencefacility@storage tabel de unieke werklijstnummers
   for seqf_grp_rec in seqf_grp_cur(b_kafd_id => v_kafd_id) loop

      BEGIN
        gebruikers_info(p_tekst => 'Begin plaatverwerking : '||seqf_grp_rec.werklijstnr);

        v_werklijst_nummer := null;
        v_wlst_aanwezig    := null;

        select kgc_wlst_seq.nextval into v_wlst_id from dual;

        -- 6. Genereer een nieuw werklijstnummer op basis van de nummerstructuur ROBOT_WERK
        v_werklijst_nummer := kgc_nust_00.genereer_nr
                                    ( p_nummertype => 'WERK'
                                    , p_ongr_id    => NULL
                                    , p_kafd_id    => v_kafd_id
                                    ) ;

        if v_werklijst_nummer is null then
          gebruikers_info(p_tekst => 'Werklijstnummer kon niet gemaakt worden met nummertype WERK');
          RAISE ex_werklijstnummer;
        end if;

        -- werklijst van de ROBOT is niet afhankelijk van de ingelogde medewerker
        v_werklijst_nummer := substr(v_werklijst_nummer,1,11)||'ROBOT';

        select kgc_trge_seq.nextval into v_trge_id from dual;

        --9. Maak een TRAY GEBRUIK aan in de tabel KGC_TRAY_GEBRUIK
        insert into kgc_tray_gebruik
        (trge_id
        ,mede_id
        ,tech_id
        ,trty_id
        ,datum
        ,gefixeerd
        ,werklijst_nummer
        ,externe_id
        )
         values
        (v_trge_id
        ,v_mede_id
        ,v_tech_id
        ,trty_rec.trty_id
        ,sysdate
        ,'J'
        ,v_werklijst_nummer
        ,'U'||seqf_grp_rec.werklijstnr
        );

        --10. Vul de hulptabel met tray gebruik
        insert into beh_hlp_tray_gebruik
        (session_id
        ,trge_id
        ,werklijst_nummer
        ,sequenceplateid
        )
        values
        (c_session_id
        ,v_trge_id
        ,v_werklijst_nummer
        ,seqf_grp_rec.werklijstnr
        );

        --11. Maak een werklijst aan in Helix voor ieder uniek voorkomen, sequenceplateid = werklijstnummer
        insert into kgc_werklijsten
        (wlst_id
        ,kafd_id
        ,werklijst_nummer
        ,type_werklijst
        ,oracle_uid
        ,datum
        ,archief
        ,gesorteerd
        ,spoed_bovenaan)
        values
        (v_wlst_id
        ,v_kafd_id
        ,v_werklijst_nummer
        ,'STOF'
        ,user
        ,trunc(sysdate)
        ,'N'
        ,null
        ,'N')
        ;

        -- ieder werklijst_item krijgt een volgnummer, begin bij 1
        v_volgnummer := 1;

        gebruikers_info(p_tekst => 'werklijst items toevoegen');
        --12. Selecteer alle stoftesten die in Helix op een nieuwe werklijst/tray gezet moeten worden
        for seqf_rec in seqf_cur (b_sequenceplateid => seqf_grp_rec.werklijstnr
                                 ,b_kafd_id         => v_kafd_id ) loop

          v_seq_meet_id := null;
          v_seq_stof_id := null;

          ---------------------------------------------------
          -- SEQUENCE stoftest aanmelden en toevoegen aan werklijst
          ---------------------------------------------------
          --
          --13. stof_id bepalen van seq op basis van omschrijving in seq facility tabel
          open  seq_stof_cur (b_omschrijving => seqf_rec.seqprimerid||' '||seqf_rec.primer
                             ,b_kafd_id => v_kafd_id);
          fetch seq_stof_cur into v_seq_stof_id;
          close seq_stof_cur;

          --14. Als de stoftest niet bestaat in Helix dan geef hiervan een melding en ga door met de volgende
          if v_seq_stof_id is null
          then
            gebruikers_info(p_tekst => 'Stoftest niet gevonden voor: '||seqf_rec.seqprimerid||' '||seqf_rec.primer);
          else
            --15. Meestal is de meting al aangemeld in Helix. Selecteer de betreffende meet_id in Helix bas_meetwaarden
            open  meet_cur(b_meti_id => seqf_rec.meti_id
                          ,b_stof_id => v_seq_stof_id);
            fetch meet_cur into v_seq_meet_id;
            close meet_cur;

            --16. Als de meting nog niet is aangemeld dan moet dit alsnog gedaan worden
            if v_seq_meet_id is null
            then
              -- seq stoftest aanmelden
              --15. Stoftest altijd opnieuw aanmelden. Eerder werd gecontroleerd of de stoftest al op een werklijst staat in Helix, dat hoeft niet meer.
              v_seq_meet_id := stoftest_opnieuw_aanmelden
                 ( p_stof_id => v_seq_stof_id
                 , p_meti_id => seqf_rec.meti_id
                 , p_frac_id => seqf_rec.frac_id
                 , p_onde_id => seqf_rec.onde_id
                 );

            end if;

            if v_seq_stof_id is null
            then
              gebruikers_info(p_tekst => 'Stoftest niet aangemeld/gevonden voor: '||seqf_rec.seqprimerid||' '||seqf_rec.primer);
            elsif v_seq_meet_id is not null
            then
              gebruikers_info(p_tekst => 'werklijst item invoeren met meetid: '||v_seq_meet_id);
              --17. Vul de werklijst met de gevonden stoftest
              insert into kgc_werklijst_items
              (wlst_id
              ,meet_id
              ,afgerond
              ,volgnummer
              ,stof_id
              ,ongr_id
              ,mons_id
              ,mede_id_controle
              ,mest_id
              ,meetwaarde
              ,meti_id)
              values
              (v_wlst_id
              ,v_seq_meet_id
              ,'N'--seqf_rec.afgerond
              ,v_volgnummer
              ,v_seq_stof_id--seqf_rec.stof_id
              ,seqf_rec.ongr_id
              ,seqf_rec.mons_id
              ,null--seqf_rec.mede_id_controle
              ,null--seqf_rec.mest_id
              ,null
              ,seqf_rec.meti_id
              );

              v_volgnummer := v_volgnummer + 1;

              --18. Update de sequencefacility tabel, zet helixstatus = LOGGED en vul de meetid met de nieuwe meet_id uit Helix
              update sequencefacility@storage
              set helixstatus = 'LOGGED'
              ,   meetid      = v_seq_meet_id
              where sequenceplateid = seqf_grp_rec.werklijstnr
              and pcr_meetid        = seqf_rec.pcr_meetid
              and primer            = seqf_rec.primer
              and helixstatus       = 'INITIAL'
              ;

            end if;

          end if;

        end loop;

        --18A. Update de sequencefacility tabel, zet helixstatus = LOGGED voor pGEM en Controle fracties   -- MGOL
        update sequencefacility@storage
        set helixstatus = 'LOGGED'
        where sequenceplateid = seqf_grp_rec.werklijstnr
        and UPPER(dna_id)     IN ('PGEM', 'CONTROLE')
        and helixstatus       = 'INITIAL'
        ;

        v_wlst_aanwezig := 0;

        open  wlst_niet_gevuld_cur (b_wlst_id => v_wlst_id);
        fetch wlst_niet_gevuld_cur into v_wlst_aanwezig;
        close wlst_niet_gevuld_cur;

        -- als niets bij de werklijst items is toegevoegd dan werklijst weer verwijderen
        if v_wlst_aanwezig = 0 then
          update kgc_werklijsten set archief = 'J' where wlst_id = v_wlst_id;
          delete kgc_werklijsten      where wlst_id = v_wlst_id;
          delete kgc_tray_gebruik     where trge_id = v_trge_id;
          delete beh_hlp_tray_gebruik where trge_id = v_trge_id;
        end if;

        -- set sequenceplateid opslaan voor verder te gaan met volgende set.
        commit;

        gebruikers_info(p_tekst => 'Succesvol verwerkte sequenceplateid!' );
        gebruikers_info(p_tekst => 'Gereed voor werklijst : '||seqf_grp_rec.werklijstnr);

      EXCEPTION
        when ex_werklijstnummer then
          dbms_output.put_line('Fout bij aanmaken van werklijstnummer');
          gebruikers_info(p_tekst => 'Fout bij aanmaken van werklijstnummer' );
          rollback;
        when others then
          dbms_output.put_line('Fout bij aanmaken van de werklijst, zie logging');
          gebruikers_info(p_tekst => ' Onverwachte fout opgetrden met sqlcode '|| SQLCODE ||' , foutmelding ' || SQLERRM );
          rollback;
      END;

   end loop;

   -- Doorloop de sequencefacility tabel waarbij de helixstatus 'INITIAL' is maar het onderzoek afgerond is
   for onde_afgerond_rec in onde_afgerond_cur(b_kafd_id => v_kafd_id) loop

      --Update de sequencefacility tabel, zet helixstatus = AFGEROND
       update sequencefacility@storage
       set    helixstatus = 'AFGEROND'
       where sequenceplateid = onde_afgerond_rec.sequenceplateid
       and pcr_meetid        = onde_afgerond_rec.pcr_meetid
       and primer            = onde_afgerond_rec.primer
       and helixstatus       in ('INITIAL','LOGGED')  --29-01-2013 LOGGED toegevoegd
       ;

       update sequencefacility@storage
       set    meetid         = 99
       where sequenceplateid = onde_afgerond_rec.sequenceplateid
       and pcr_meetid        = onde_afgerond_rec.pcr_meetid
       and primer            = onde_afgerond_rec.primer
       and helixstatus       = 'AFGEROND'
       and meetid            is null
       ;

   end loop;

   -- output voor de gebruiker, welke werklijst aangemaakt is
   gebruikers_info(p_tekst => 'Sequence facility tabel is geupdate, statussen op AFGEROND gezet en meetid eventueel op 99.' );

   commit;

   -----------------------
   -- Nu de TRAY vullen --
   -----------------------

   --19. Tray vullen
   --Doorloop de unieke werklijsten/tray gebruik.
   --Bij ieder tray moet opnieuw begonnen worden met tellen van de wells
   for wlst_rec in wlst_cur loop

     -- output voor de gebruiker, welke werklijst aangemaakt is
     gebruikers_info(p_tekst => 'Werklijst aangemaakt, tray vullen voor werklijst '||wlst_rec.werklijst_nummer);

     begin

       --20. Per werklijst/tray gebruik moeten de meetwaarden op tray gezet worden
       for wlst_meet_rec IN wlst_meet_cur(b_wlst_id => wlst_rec.wlst_id
                                        ,b_sequenceplateid => wlst_rec.sequenceplateid) loop

         insert into kgc_tray_vulling
         ( trge_id
         , meet_id
         , x_positie
         , y_positie
         , z_positie
         , mede_id
         )
         values
         ( wlst_rec.trge_id
         , wlst_meet_rec.meet_id
         , wlst_meet_rec.x_positie
         , wlst_meet_rec.y_positie
         , 0
         , v_mede_id
         );
         --21. synchroniseer de werklijst
         werklijst
         ( p_meet_id          => wlst_meet_rec.meet_id
         , p_actie            => 'I'
         , p_werklijst_nummer => wlst_rec.werklijst_nummer
         );

       end loop;

     exception
       when others then
         gebruikers_info(p_tekst => 'Fout bij vullen van de tray, zie logging');
         gebruikers_info(p_tekst => 'Onverwachte fout opgetreden bij het vullen van de tray, sqlcode '|| SQLCODE ||' , foutmelding ' || SQLERRM );
         rollback;
     end;

   end loop;

   -- verwijder de tijdelijke waarden in hulptabel
   delete beh_hlp_tray_gebruik where session_id = c_session_id;

   commit;

  gebruikers_info(p_tekst => 'Succesvol verwerkte run!' );
  gebruikers_info(p_tekst => 'Gereed!' );

  clob_to_file( p_dir  => p_logfile
              , p_file => 'BEH_DNA_SEQ_WLST_AANMAKEN_'||to_char(sysdate,'YYYYMMDD HH24.MI')||'.log'
              , p_clob => v_logfile_inhoud );

exception
   when ex_verkeerde_parameters then
     gebruikers_info(p_tekst => 'Verkeerde parameters!' );
     dbms_output.put_line('Fout, verkeerde parameters');
     qms$errors.unhandled_exception('beh_dna_seq_wlst_aanmaken.start_robot_werklijst_aanmaken 1');
   when others then
     dbms_output.put_line('Fout, zie logfile');
     gebruikers_info(p_tekst => 'Onverwachte fout opgetrden met sqlcode '|| SQLCODE ||' , foutmelding ' || SQLERRM );
     rollback;
     qms$errors.unhandled_exception('beh_dna_seq_wlst_aanmaken.start_robot_werklijst_aanmaken 2');

END start_robot_werklijst_aanmaken;

END BEH_DNA_SEQ_WLST_AANMAKEN;
/

/
QUIT
