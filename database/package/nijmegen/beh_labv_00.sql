CREATE OR REPLACE PACKAGE "HELIX"."BEH_LABV_00" as

g_sobe_id beh_soap_berichten.sobe_id%type;
g_start_tag   constant varchar2(10) := CHR (38) || 'lt;'; -- &lt; (<)
g_end_tag     constant varchar2(10) := CHR (38) || 'gt;';-- &gt;  (>)
g_quot        constant varchar2(10) := CHR (38) || 'quot;';-- &quot;  (")

function getconnectionid
return varchar2
;
function clearconnection
(
p_connectionid varchar2
) return boolean
;
function checkconnection
(
p_connectionid varchar2
) return varchar2
;
function extract_plateid
(
p_http_return clob
) return varchar2
;
function processaction
(
p_trge_id number
)
return clob
;
function getsqldataset
(
p_query varchar2
)
return xmltype
;
function create_xml_processaction
(
p_trge_id number
)
return clob
;
procedure call_processaction
(
p_trge_id number
)
;
end beh_labv_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_LABV_00"
AS
   FUNCTION getconnectionid
      RETURN VARCHAR2
   IS
      v_request       soap_api.t_request;
      v_response      soap_api.t_response;

      v_databaseid    VARCHAR2 (100)
                         := kgc_sypa_00.systeem_waarde ('BEH_LABV_DB_NAAM');

      v_userid        VARCHAR2 (100)
                         := kgc_sypa_00.systeem_waarde ('BEH_LABV_SOAP_USER');

      v_password      VARCHAR2 (100)
         := beh_cryptit.
             decrypt_data (kgc_sypa_00.systeem_waarde ('BEH_LABV_SOAP_PASS'));

      v_return        VARCHAR2 (4000);

      v_url           VARCHAR2 (500)
         := kgc_sypa_00.systeem_waarde ('BEH_LABV_SOAP_SERVER');

      v_namespace     VARCHAR2 (500) := 'xmlns="' || v_url || '"';
      v_method        VARCHAR2 (100) := 'getConnectionId';
      v_soap_action   VARCHAR2 (500) := v_url || '/' || v_method;
      v_result_name   VARCHAR2 (32767) := 'getConnectionIdReturn';

      v_error         VARCHAR2 (4000);
      v_prog_naam varchar2(200) := 'beh_labv_00.getconnectionid';
   BEGIN
      v_request :=
         soap_api.
          new_request (p_method => v_method, p_namespace => v_namespace);

      soap_api.ADD_PARAMETER (p_request   => v_request,
                              p_name      => 'databaseid',
                              p_type      => 'xsd:string',
                              p_value     => v_databaseid);

      soap_api.ADD_PARAMETER (p_request   => v_request,
                              p_name      => 'userid',
                              p_type      => 'xsd:string',
                              p_value     => v_userid);

      soap_api.ADD_PARAMETER (p_request   => v_request,
                              p_name      => 'password',
                              p_type      => 'xsd:string',
                              p_value     => v_password);

      v_response :=
         soap_api.
          invoke (p_request   => v_request,
                  p_url       => v_url,
                  p_action    => v_soap_action);

      v_return :=
         soap_api.
          get_return_value (p_response    => v_response,
                            p_name        => v_result_name,
                            p_namespace   => NULL);

      RETURN v_return;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_error := v_prog_naam || ':' || SUBSTR (SQLERRM, 1, 2500);

         update beh_soap_berichten
         set log_tekst = log_tekst || ';' || v_error
         where sobe_id = g_sobe_id
         ;
            commit;
         DBMS_OUTPUT.PUT_LINE (v_error);
         RETURN v_error;
   END getconnectionid;
-- verbinding met labv verbreken
  FUNCTION clearconnection (p_connectionid varchar2)
      RETURN boolean
   IS
      v_request       soap_api.t_request;
      v_response      soap_api.t_response;

      v_return        VARCHAR2 (4000);

      v_url           VARCHAR2 (500)
         := kgc_sypa_00.systeem_waarde ('BEH_LABV_SOAP_SERVER');

      v_namespace     VARCHAR2 (500) := 'xmlns="' || v_url || '"';
      v_method        VARCHAR2 (100) := 'clearConnection';
      v_soap_action   VARCHAR2 (500) := v_url || '/' || v_method;
      v_result_name   VARCHAR2 (100) := 'clearConnectionResponse';

      v_error         VARCHAR2 (4000);
      v_prog_naam varchar2(200) := 'beh_labv_00.clearconnection';
      v_checkconnection varchar2(4000);
   BEGIN
      v_request :=
         soap_api.
          new_request (p_method => v_method, p_namespace => v_namespace);

      soap_api.ADD_PARAMETER (p_request   => v_request,
                              p_name      => 'connectionid',
                              p_type      => 'xsd:string',
                              p_value     => trim(p_connectionid));

      v_response :=
         soap_api.
          invoke (p_request   => v_request,
                  p_url       => v_url,
                  p_action    => v_soap_action);
        -- checken of de verbinding nog bestaat
      v_checkconnection := beh_labv_00.checkconnection(p_connectionid);

      if  v_checkconnection = 'false' then
        return true;
     else
        return false;
     end if;

   EXCEPTION
      WHEN OTHERS
      THEN
         v_error := v_prog_naam || ':' || SUBSTR (SQLERRM, 1, 2500);

         update beh_soap_berichten
         set log_tekst = log_tekst || ';' || v_error
         where sobe_id = g_sobe_id
         ;
          commit;
         DBMS_OUTPUT.PUT_LINE (v_error);

         v_checkconnection := beh_labv_00.checkconnection(p_connectionid);

         if  v_checkconnection = 'false' then
            return true;
        else
            return false;
        end if;
   END clearconnection;
  -- checken of de verbinding nog bestaat
  FUNCTION checkconnection (p_connectionid varchar2)
      RETURN varchar2
   IS
      v_request       soap_api.t_request;
      v_response      soap_api.t_response;

      v_return        VARCHAR2 (4000);

      v_url           VARCHAR2 (500)
         := kgc_sypa_00.systeem_waarde ('BEH_LABV_SOAP_SERVER');

      v_namespace     VARCHAR2 (500) := 'xmlns="' || v_url || '"';
      v_method        VARCHAR2 (100) := 'checkConnection';
      v_soap_action   VARCHAR2 (500) := v_url || '/' || v_method;
      v_result_name   VARCHAR2 (100) := 'multiRef';

      v_error         VARCHAR2 (4000);
      v_prog_naam varchar2(200) := 'beh_labv_00.checkconnection';
   BEGIN
   /* na labv8 upgrade werkt dit niet meer */
   /*
      v_request :=
         soap_api.
          new_request (p_method => v_method, p_namespace => v_namespace);

      soap_api.ADD_PARAMETER (p_request   => v_request,
                              p_name      => 'connectionid',
                              p_type      => 'xsd:string',
                              p_value     => trim(p_connectionid));

      v_response :=
         soap_api.
          invoke (p_request   => v_request,
                  p_url       => v_url,
                  p_action    => v_soap_action);

    v_return :=
         soap_api.
          get_return_value (p_response    => v_response,
                            p_name        => v_result_name,
                            p_namespace   => NULL);

      RETURN v_return;
 */
  RETURN 'false';
   EXCEPTION
      WHEN OTHERS
      THEN
         v_error := v_prog_naam || ':' || SUBSTR (SQLERRM, 1, 2500);

         update beh_soap_berichten
         set log_tekst = log_tekst || ';' || v_error
         where sobe_id = g_sobe_id
         ;
          commit;
         DBMS_OUTPUT.PUT_LINE (v_error);
         RETURN v_error;
   END checkconnection;

function extract_plateid (p_http_return clob)
return varchar2 is

v_http_replaced clob;
v_http_xml  XMLTYPE;
v_error varchar2(4000);
v_plateid varchar2(500);
v_prog_naam varchar2(200) := 'beh_labv_00.extract_plateid';
begin
    v_http_replaced := replace(replace(replace(p_http_return, g_start_tag, '<'), g_end_tag,'>'), g_quot, '"');

    v_http_xml := xmltype.createxml (v_http_replaced);
    -- 5e propery bevat plateid
    v_plateid := v_http_xml.EXTRACT ('//propertylist/property[5]/text()').getstringval ();
    -- regexp is gevoelig voor blokhaaktjes! vandaar is hier gewone replace gebruikt.
    v_plateid :=REPLACE (REPLACE (v_plateid, '<![CDATA['), ']]>');

    RETURN v_plateid;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_error := v_prog_naam || ':' || SUBSTR (SQLERRM, 1, 2500);

         update beh_soap_berichten
         set log_tekst = log_tekst || ';' || v_error
         where sobe_id = g_sobe_id
         ;

         DBMS_OUTPUT.PUT_LINE (v_error);
             commit;
         RETURN v_error;
end extract_plateid;

  FUNCTION processaction (p_trge_id IN NUMBER)
      RETURN CLOB
   IS
      v_actionid          VARCHAR2 (50) := 'HelixInterface';
      v_actionversionid   VARCHAR2 (5) := '1';
      v_prog_naam varchar2(200) := 'beh_labv_00.processaction';
      v_connectionid      VARCHAR2 (4000) := beh_labv_00.getconnectionid ();
      v_clearconnection boolean;
      v_xml_data CLOB:= BEH_LABV_00.CREATE_XML_PROCESSACTION (p_trge_id);

      v_kleiner_dan       VARCHAR2 (10) := CHR (38) || 'lt;';
      v_groter_dan        VARCHAR2 (10) := CHR (38) || 'gt;';
      v_quot              VARCHAR2 (10) := CHR (38) || 'quot;';
      v_return             CLOB;
      v_url               VARCHAR2 (500)
         := kgc_sypa_00.systeem_waarde ('BEH_LABV_SOAP_SERVER');
      v_propertyListXML clob :=
         '<![CDATA[<propertylist><property id="isdata" type="simple">YES</property><property id="filename" type="simple">'
         || v_xml_data
         || '</property></propertylist>]]>';
       /*
  V_SOAP_REQUEST      XMLTYPE := XMLTYPE(
'<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
    <SOAP-ENV:Body>
        <processAction xmlns="'|| v_url ||'" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
        <connectionid xsi:type="xsd:string">' || v_connectionid||'</connectionid>
        <actionid xsi:type="xsd:string">' ||v_actionid || '</actionid>
        <actionversionid xsi:type="xsd:string">' || v_actionversionid || '</actionversionid>
        <propertyListXML xsi:type="xsdstring">' ||v_propertyListXML || ' </propertyListXML>
</processAction>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>');
*/
 V_SOAP_REQUEST      XMLTYPE := XMLTYPE(
'<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservices.sapphire.labvantage.com">
   <soapenv:Header/>
   <soapenv:Body>
      <web:processAction soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <connectionid xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' || v_connectionid||'</connectionid>
         <actionid xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' ||v_actionid || '</actionid>
         <actionversionid xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' || v_actionversionid || '</actionversionid>
         <propertyListXML xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">' ||v_propertyListXML || '</propertyListXML>
      </web:processAction>
   </soapenv:Body>
</soapenv:Envelope>');
  V_SOAP_REQUEST_TEXT CLOB := V_SOAP_REQUEST.getClobVal();
  V_REQUEST           UTL_HTTP.REQ;
  V_RESPONSE          UTL_HTTP.RESP;

  v_http_return             CLOB;
  v_return_text           VARCHAR2(32767);

  v_amount              PLS_INTEGER := 2000;
  v_offset              PLS_INTEGER := 1;
  v_req_length        BINARY_INTEGER;
  v_buffer            VARCHAR2 (32767);

  v_plateid varchar2(500);
BEGIN
   DBMS_LOB.createtemporary (v_http_return, FALSE);

    update beh_soap_berichten
    set request = V_SOAP_REQUEST_TEXT
    where sobe_id = g_sobe_id
    ;
    commit;

  utl_http.set_transfer_timeout(120);
  V_REQUEST := UTL_HTTP.BEGIN_REQUEST(URL => v_url, METHOD => 'POST');

  UTL_HTTP.SET_HEADER (V_REQUEST, 'SOAPAction', 'processAction');
  UTL_HTTP.SET_HEADER(V_REQUEST, 'User-Agent', 'Mozilla/4.0');
  V_REQUEST.METHOD := 'POST';
  UTL_HTTP.SET_HEADER (R => V_REQUEST, NAME => 'Content-Length', VALUE => DBMS_LOB.GETLENGTH(V_SOAP_REQUEST_TEXT));

  v_req_length := DBMS_LOB.getlength (V_SOAP_REQUEST_TEXT);

  WHILE (v_offset < v_req_length) LOOP
     DBMS_LOB.read (V_SOAP_REQUEST_TEXT, v_amount, v_offset, v_buffer);
     UTL_HTTP.WRITE_TEXT (R => V_REQUEST, DATA => v_buffer);
    v_offset := v_offset + v_amount;
 END LOOP;

  V_RESPONSE := UTL_HTTP.GET_RESPONSE(V_REQUEST);
--  hier wordt http respons in een clob weggeschreven.
 BEGIN
   LOOP
     UTL_HTTP.read_text(V_RESPONSE, v_return_text, 32767);
     DBMS_LOB.writeappend (v_http_return, LENGTH(v_return_text), v_return_text);
    END LOOP;
 EXCEPTION
   WHEN UTL_HTTP.end_of_body THEN
     UTL_HTTP.end_response(V_RESPONSE);
 END;

  v_clearconnection := beh_labv_00.clearconnection (v_connectionid);

  update beh_soap_berichten
  set response = v_http_return
  where sobe_id = g_sobe_id
  ;
  commit;

  if (V_RESPONSE.STATUS_CODE = '200' and V_RESPONSE.REASON_PHRASE = 'OK') then

      v_plateid := beh_labv_00.extract_plateid(v_http_return);
      return v_plateid;
  else
     return v_http_return;
  end if;
EXCEPTION
  WHEN UTL_HTTP.END_OF_BODY THEN
    UTL_HTTP.END_RESPONSE(V_RESPONSE);
    v_clearconnection := beh_labv_00.clearconnection (v_connectionid);
    v_return := v_prog_naam ||':' || SUBSTR (SQLERRM, 1, 2500);

     update beh_soap_berichten
     set log_tekst = log_tekst || ';' || v_return
     where sobe_id = g_sobe_id
     ;
     commit;
    DBMS_OUTPUT.PUT_LINE (v_return);
    RETURN V_SOAP_REQUEST_TEXT || ' error => ' || v_return;
END processaction;
FUNCTION getsqldataset(p_query IN VARCHAR2)
  RETURN xmltype
IS
  v_actionid        VARCHAR2 (50)   := 'getSqlDataSet';
  v_prog_naam       VARCHAR2(200)   := 'beh_labv_00.getSqlDataSet';
  v_connectionid    VARCHAR2 (4000) := beh_labv_00.getconnectionid ();
  v_clearconnection BOOLEAN;
  v_return CLOB;
  v_url VARCHAR2 (500)     := kgc_sypa_00.systeem_waarde ('BEH_LABV_SOAP_SERVER');
  V_SOAP_REQUEST XMLTYPE   := XMLTYPE( '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
      <SOAP-ENV:Body>
        <getSqlDataSet xmlns="'|| v_url ||'" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
          <connectionid xsi:type="xsd:string">' || v_connectionid||'</connectionid>
          <sql xsi:type="xsd:string"><![CDATA[' || p_query ||']]></sql>
          </getSqlDataSet>
      </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>');
  V_SOAP_REQUEST_TEXT CLOB := V_SOAP_REQUEST.getClobVal();
  V_REQUEST UTL_HTTP.REQ;
  V_RESPONSE UTL_HTTP.RESP;
  v_http_return CLOB;
  v_return_text VARCHAR2(32767);
  v_http_replaced CLOB;
  v_http_xml xmltype;
  v_amount PLS_INTEGER := 2000;
  v_offset PLS_INTEGER := 1;
  v_req_length BINARY_INTEGER;
  v_buffer VARCHAR2 (32767);
BEGIN
  DBMS_LOB.createtemporary (v_http_return, FALSE);

  V_REQUEST := UTL_HTTP.BEGIN_REQUEST(URL => v_url, METHOD => 'POST');

  UTL_HTTP.SET_HEADER (V_REQUEST, 'SOAPAction', 'getSqlDataSet');
  UTL_HTTP.SET_HEADER(V_REQUEST, 'User-Agent', 'Mozilla/4.0');
  V_REQUEST.METHOD := 'POST';
  UTL_HTTP.SET_HEADER (R => V_REQUEST, NAME => 'Content-Length', VALUE => DBMS_LOB.GETLENGTH(V_SOAP_REQUEST_TEXT));
  v_req_length   := DBMS_LOB.getlength (V_SOAP_REQUEST_TEXT);

  WHILE (v_offset < v_req_length)
  LOOP
    DBMS_LOB.read (V_SOAP_REQUEST_TEXT, v_amount, v_offset, v_buffer);
    UTL_HTTP.WRITE_TEXT (R => V_REQUEST, DATA => v_buffer);
    v_offset := v_offset + v_amount;
  END LOOP;
  V_RESPONSE := UTL_HTTP.GET_RESPONSE(V_REQUEST);
  BEGIN
    LOOP
      UTL_HTTP.read_text(V_RESPONSE, v_return_text, 32767);
      DBMS_LOB.writeappend (v_http_return, LENGTH(v_return_text), v_return_text);
    END LOOP;
  EXCEPTION
  WHEN UTL_HTTP.end_of_body THEN
    UTL_HTTP.end_response(V_RESPONSE);
  END;
  v_clearconnection := beh_labv_00.clearconnection (v_connectionid);

  IF (V_RESPONSE.STATUS_CODE = '200' AND V_RESPONSE.REASON_PHRASE = 'OK') THEN
    v_http_replaced         := REPLACE(REPLACE(REPLACE(v_http_return, g_start_tag, '<'), g_end_tag,'>'), g_quot, '"');
    v_http_replaced         := REPLACE (REPLACE (v_http_replaced, '<![CDATA['), ']]>');

    RETURN xmltype.createxml (v_http_replaced);
  ELSE
    RETURN xmltype.createxml(v_http_return);
  END IF;
EXCEPTION
WHEN UTL_HTTP.END_OF_BODY THEN
  UTL_HTTP.END_RESPONSE(V_RESPONSE);
  v_clearconnection := beh_labv_00.clearconnection (v_connectionid);
  v_return          := v_prog_naam ||':' || SUBSTR (SQLERRM, 1, 2500);

  DBMS_OUTPUT.PUT_LINE (v_return);

  RETURN xmltype.createxml (V_SOAP_REQUEST_TEXT || ' error => ' || v_return);
END getsqldataset;
   FUNCTION create_xml_processaction (p_trge_id NUMBER)
      RETURN CLOB
   IS
      v_error        VARCHAR2 (4000);
      v_prog_naam varchar2(200) := 'beh_labv_00.create_xml_processaction';

      v_xml_header   VARCHAR2 (4000) :=
            g_start_tag || '?xml version="1.0" encoding="iso-8859-1"?'|| g_end_tag;

      v_xml_trge     CLOB;

      CURSOR c_trge
      IS
         SELECT *
           FROM beh_labv_trge_xml_vw
          WHERE trge_id = p_trge_id;

      r_trge c_trge%rowtype;
   BEGIN
      open c_trge;
      fetch c_trge into r_trge;
      if c_trge%found then

        v_xml_trge := v_xml_header || replace(replace(r_trge.trge_xml.getClobVal(), '>', g_end_tag), '<', g_start_tag);

      ELSE
        v_xml_trge := 'Geen relevante gegevens voor trge_id: ' || to_char(p_trge_id) || ' gevonden.';
      END IF;
      close c_trge;
      return v_xml_trge;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_error := v_prog_naam ||':' || SUBSTR (SQLERRM, 1, 2500);

         update beh_soap_berichten
         set log_tekst = log_tekst || ';' || v_error
         where sobe_id = g_sobe_id
         ;
         commit;
         DBMS_OUTPUT.PUT_LINE (v_error);
         RETURN v_error;
   END create_xml_processaction;
  procedure call_processaction (p_trge_id NUMBER)
   IS
      v_error         VARCHAR2 (4000);
      v_return        CLOB;
      v_prog_naam varchar2(200) := 'beh_labv_00.call_processaction';

   BEGIN

      g_sobe_id := null;

      SELECT beh_sobe_seq.NEXTVAL INTO g_sobe_id FROM DUAL;

      INSERT INTO beh_soap_berichten (sobe_id, entiteit_id, entiteit_code, client, server)
              VALUES (g_sobe_id, p_trge_id, 'TRGE', 'HELIX', 'LABV');

      COMMIT;

      v_return := trim(beh_labv_00.processaction (p_trge_id));

--      if (v_return is not null and length(v_return) <= 20) then
      if (length(v_return) <= 20 and substr(v_return, 1, 3) = 'PT-') then
        update kgc_tray_gebruik
        set externe_id = v_return
        where trge_id = p_trge_id
        ;
      else
        update kgc_tray_gebruik
        set externe_id = '-999999',
        opmerkingen = substr(v_return, 0, 2000)
        where trge_id = p_trge_id
        ;
      end if;
     commit;
   EXCEPTION
      WHEN OTHERS THEN
         v_error := v_prog_naam ||':' || SUBSTR (SQLERRM, 1, 2500);

         update beh_soap_berichten
         set log_tekst = log_tekst || ';' || v_error
         where sobe_id = g_sobe_id
         ;
         commit;
         DBMS_OUTPUT.PUT_LINE (v_error);
   END call_processaction;

END beh_labv_00;
/

/
QUIT
