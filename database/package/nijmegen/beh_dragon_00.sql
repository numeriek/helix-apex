CREATE OR REPLACE PACKAGE "HELIX"."BEH_DRAGON_00" as

function beh_haal_zisnr_op
(
p_zisnr in kgc_personen.zisnr%type,
p_scheidingsteken in varchar2 := '$'
)
return varchar2;

end beh_dragon_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DRAGON_00" as
function beh_haal_zisnr_op
(
p_zisnr in kgc_personen.zisnr%type,
p_scheidingsteken in varchar2 := '$'
)
return varchar2
is
-- via deze functie worden zis-gegevens van patienten die nog
-- noet in Helix bestaan maar wel in ROCS naar dragon applicatie
-- gestuurd.
  pers_trow cg$kgc_personen.cg$row_type;
  rela_trow cg$kgc_relaties.cg$row_type;
  verz_trow cg$kgc_verzekeraars.cg$row_type;

  v_scheidingsteken varchar2(1) := p_scheidingsteken;
  v_return varchar2(2000) := v_scheidingsteken;
begin
  kgc_interface.persoon_in_zis
  ( p_zisnr => p_zisnr
  , pers_trow => pers_trow
  , rela_trow => rela_trow
  , verz_trow => verz_trow
  );

  v_return := v_return || pers_trow.aanspreken || v_scheidingsteken;
  v_return := v_return || pers_trow.achternaam  || v_scheidingsteken;
  v_return := v_return || pers_trow.adres || v_scheidingsteken;

  v_return := v_return || to_char(pers_trow.geboortedatum, 'dd-mm-yyyy') || v_scheidingsteken;
  v_return := v_return || pers_trow.gehuwd || v_scheidingsteken;
  v_return := v_return || pers_trow.geslacht || v_scheidingsteken;

  v_return := v_return || pers_trow.land || v_scheidingsteken;

  v_return := v_return || pers_trow.overleden || v_scheidingsteken;
  v_return := v_return || to_char(pers_trow.overlijdensdatum, 'dd-mm-yyyy') || v_scheidingsteken;

  v_return := v_return || pers_trow.provincie || v_scheidingsteken;
  v_return := v_return || pers_trow.postcode || v_scheidingsteken;

  v_return := v_return || pers_trow.telefoon || v_scheidingsteken;

  v_return := v_return || pers_trow.voorletters || v_scheidingsteken;
  v_return := v_return || pers_trow.voorvoegsel || v_scheidingsteken;

  v_return := v_return || pers_trow.woonplaats || v_scheidingsteken;

  return v_return;
   exception
   when others then -- als niks gevonden is of als iets fout gegaan is dan geeven we null terug.
      v_return := v_return || '0' || v_scheidingsteken;
      return v_return;
end beh_haal_zisnr_op;

end beh_dragon_00;
/

/
QUIT
