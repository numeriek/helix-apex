CREATE OR REPLACE PACKAGE "HELIX"."BEH_NOTIF_VERZEND_MONSTER_00" AS
/******************************************************************************
   NAME:       BEH_NOTIF_VERZEND_MONSTER_00
   PURPOSE: Deze package wordt wordt gebruikt om emails te versturen naar de centra waar monsters naartoe verzonden worden, bijvoorbeeld Nijmegen ontvang materiaal, deze is vooor MUMC bestemd. Deze materiaal wordt in Helix geregistreerd met de monster indicatie Verzonden naar MUMC

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8-4-2014      Z268164       1. Created this package.
******************************************************************************/

procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
);

procedure verwerking_mail_monster;

end BEH_NOTIF_VERZEND_MONSTER_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_NOTIF_VERZEND_MONSTER_00" AS
/******************************************************************************
   NAME:       BEH_NOTIF_VERZEND_MONSTER_00
   PURPOSE: Deze package wordt wordt gebruikt om emails te versturen naar de centra waar monsters naartoe verzonden worden, bijvoorbeeld Nijmegen ontvang materiaal, deze is vooor MUMC bestemd. Deze materiaal wordt in Helix geregistreerd met de monster indicatie 'MUMC'

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8-4-2014      Z268164       1. Created this package.
******************************************************************************/

procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) is

v_bety_id beh_bericht_types.bety_id%type;
v_datum_verzonden date := sysdate;
v_oracle_uid kgc_medewerkers.oracle_uid%type;

begin

  select bety_id into v_bety_id
  from beh_bericht_types
  where code = 'EMAIL'
  and upper(vervallen) = upper('N')
  ;

  select sys_context('USERENV', 'SESSION_USER')
  into v_oracle_uid
  from dual
  ;

  insert into beh_berichten
  (  bety_id,
  verzonden,
  datum_verzonden,
  oracle_uid,
  ontvanger_id_intern,
  ontvanger_id_extern,
  ontvanger_type,
  adres_verzonden,
  log_tekst,
  entiteit,
  entiteit_id,
  bericht_tekst
  )
  values
  (v_bety_id,
  p_verzonden,
  v_datum_verzonden,
  v_oracle_uid,
  p_ontvanger_id_intern,
  p_ontvanger_id_extern,
  p_ontvanger_type,
  p_adres_verzonden,
  p_log_tekst,
  'MOIN',
  p_entiteit_id,
  p_bericht_tekst
  )
  ;

commit;

end verwerk_log;

function verstuur_email
(
p_afzender  varchar,
p_ontvanger varchar,
p_cc        varchar,
p_bcc       varchar,
p_titel     varchar,
p_boodschap varchar
)
return varchar is

begin
  UTL_MAIL.SEND(  sender   => p_afzender,
                recipients => p_ontvanger,
                cc         => p_cc,
                bcc        => p_bcc,
                subject    => p_titel,
                message    => p_boodschap);
  return 'OK';
exception
 when others then
    return 'NOK';
end verstuur_email;

procedure verwerking_mail_monster is

v_sender varchar2(500) := 'gen@umcn.nl';
v_recipients varchar2(500);
v_cc varchar2(500) := ''; -- komma gescheiden
v_bcc varchar2(500) :=''; -- komma gescheiden
v_subject varchar2(1000);
v_message varchar2(4000);
v_newline varchar2(10) := chr(10);
v_db_name varchar2(100);
v_verzonden varchar2(10)   := null;


CURSOR moin_cur
IS
    select MOIN.MOIN_ID,
            MONS.MONSTERNUMMER,
            INTE.CODE as LOCATIE,
            INTE.LOCUS as RECIPIENTS
    from kgc_monsters mons, kgc_monster_indicaties moin, kgc_indicatie_teksten inte, kgc_indicatie_groepen ingr
    where MONS.MONS_ID = MOIN.MONS_ID
        and MOIN.INDI_ID = INTE.INDI_ID
        and INTE.INGR_ID = INGR.INGR_ID
        and INGR.CODE = 'VERSTUURD'
        and MOIN.MOIN_ID not in (select beri.entiteit_id from beh_berichten beri where BERI.ENTITEIT = 'MOIN')
        and INTE.LOCUS is not null
    ;

moin_rec moin_cur%ROWTYPE;

BEGIN

    SELECT global_name INTO v_db_name
    FROM GLOBAL_NAME;

    OPEN moin_cur;
    FETCH moin_cur INTO moin_rec;
    CLOSE moin_cur;

    FOR moin_rec  IN  moin_cur
    LOOP
        IF moin_rec.monsternummer is not null AND v_db_name NOT LIKE '%PROD%'
        THEN v_subject := 'DIT IS EEN TEST! Monster met nummer: ' || moin_rec.monsternummer ||' - wordt/is verzonden naar uw locatie';
                 v_recipients := moin_rec.RECIPIENTS;
        ELSE
            IF moin_rec.monsternummer is not null AND v_db_name LIKE '%PROD%'
            THEN v_subject := 'Monster met nummer: ' || moin_rec.monsternummer ||' - wordt/is verzonden naar uw locatie';
                     v_recipients := moin_rec.RECIPIENTS;
            END IF;
        END IF;

        v_message := 'Geachte heer/mevrouw,'
            || v_newline
            || v_newline
            || 'Monster met nummer: ' || moin_rec.monsternummer ||' - wordt/is verzonden naar uw locatie: ' || moin_rec.LOCATIE
            || v_newline
            || v_newline
            || 'Indien u vragen heeft, dan kunt u altijd contact met ons opnemen.'
            || v_newline
            || v_newline
            || 'met vriendelijke groeten,'
            || v_newline
            || 'Monsterontvangst Genetica RadboudUMC'
            || v_newline
            || 'tel: 024-36 55736'
            ;

        UTL_MAIL.SEND(  sender => v_sender,
                        recipients => v_recipients,
                        cc => v_cc,
                        bcc => v_bcc,
                        subject => v_subject,
                        message => v_message);

        v_verzonden := 'J' ;


        verwerk_log(  v_verzonden
                            , NULL
                            , NULL
                            , 'NULL'
                            , v_recipients
                            , v_subject
                            , moin_rec.moin_id
                            , v_message
                         );
     COMMIT;
     END LOOP;

END verwerking_mail_monster;


END BEH_NOTIF_VERZEND_MONSTER_00;
/

/
QUIT
