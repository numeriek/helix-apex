CREATE OR REPLACE PACKAGE "HELIX"."BEH_NIPT_00" as
-- extra informatie, zie mantis 10100
procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
);

procedure write2file
;
end BEH_NIPT_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_NIPT_00"
as
--  MG: gemaakt om werklijsten van NIPT in een csv bestand aan te bieden aan NIPT robot
-- extra informatie, zie mantis 10100
-- wijz cv, 03-04-2015, zie mantis 0011171: directory aanpassen voor NIPT werklijst
procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) is

v_bety_id beh_bericht_types.bety_id%type;
v_datum_verzonden date := sysdate;
v_oracle_uid kgc_medewerkers.oracle_uid%type;

begin

  select bety_id into v_bety_id
  from beh_bericht_types
  where code = 'NIPT_WLST'
  and upper(vervallen) = upper('N')
  ;

  select sys_context('USERENV', 'SESSION_USER')
  into v_oracle_uid
  from dual
  ;

  insert into beh_berichten
  (  bety_id,
  verzonden,
  datum_verzonden,
  oracle_uid,
  ontvanger_id_intern,
  ontvanger_id_extern,
  ontvanger_type,
  adres_verzonden,
  log_tekst,
  entiteit,
  entiteit_id,
  bericht_tekst
  )
  values
  (v_bety_id,
  p_verzonden,
  v_datum_verzonden,
  v_oracle_uid,
  p_ontvanger_id_intern,
  p_ontvanger_id_extern,
  p_ontvanger_type,
  p_adres_verzonden,
  p_log_tekst,
  'WLST',
  p_entiteit_id,
  p_bericht_tekst
  )
  ;

commit;

end verwerk_log;

procedure write2file
is

v_create_dir_sql varchar2(2000);
v_drop_dir_sql varchar2(2000):= 'drop directory BEH_TEMP_WLST';
v_file_handler utl_file.file_type;
v_file_name varchar2(100);
v_dir_sql varchar2(250);

v_aantal number;
v_werklijst_naam varchar2 (1000);
v_wlst_id number;
v_verzonden varchar2 (1000);
v_db_name varchar2 (50);

cursor c_nipt_wlst is
select TEKST
from  BEH_NIPT_WLST_BESTAND_VW
where WERKLIJST_NUMMER = v_werklijst_naam
;

begin

select count(*) into v_aantal
from  BEH_NIPT_WLST_BESTAND_VW;

if v_aantal > 0 then
    SELECT global_name INTO v_db_name
    FROM GLOBAL_NAME;


    If  v_db_name like '%PROD%' then
        v_create_dir_sql := 'create directory BEH_TEMP_WLST as ''\\umcstgenp14\digitaaldossier$\Genoom\NIPT\Werklijsten\PROD''';
        v_dir_sql := '\\umcstgenp14\digitaaldossier$\Genoom\NIPT\Werklijsten\PROD';
    else
    -- aangepast zie mantis 0011171: directory aanpassen voor NIPT werklijst
        v_create_dir_sql := 'create directory BEH_TEMP_WLST as ''\\umcstgenp14\digitaaldossier$\Genoom\NIPT\Werklijsten\ACCI''';
        v_dir_sql := '\\umcstgenp14\digitaaldossier$\Genoom\NIPT\Werklijsten\ACCI';
    --    v_create_dir_sql := 'create directory BEH_TEMP_WLST as ''\\umcsullyp01\samplesheets\NIPT\ACCI''';
     --   v_dir_sql := '\\umcsullyp01\samplesheets\NIPT\ACCI';


    end if;

    select distinct WERKLIJST_NUMMER into v_werklijst_naam
    from BEH_NIPT_WLST_BESTAND_VW
    where rownum = 1;

    select distinct WLST_ID into v_wlst_id
    from BEH_NIPT_WLST_BESTAND_VW
    where WERKLIJST_NUMMER = v_werklijst_naam;

    execute immediate v_create_dir_sql;
    v_file_name:= v_werklijst_naam ||'.csv';
    v_file_handler := UTL_FILE.FOPEN('BEH_TEMP_WLST', v_file_name, 'W');

    utl_file.put_line( v_file_handler, 'Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,Sample_Project,Description,Sasi_BC');

    for r_nipt_wlst in c_nipt_wlst loop
        utl_file.put_line( v_file_handler, r_nipt_wlst.tekst);
        utl_file.fflush(v_file_handler);
    end loop;

    v_verzonden := 'J' ;
    verwerk_log(  v_verzonden
                                    , NULL
                                    , NULL
                                    , 'NULL'
                                    , v_dir_sql
                                    , 'werklijst bestand is aangemaakt'
                                    , v_wlst_id
                                    , 'werklijst bestand is aangemaakt voor werklijst met nummer: ' ||  v_werklijst_naam
                                 );

    UTL_FILE.FCLOSE(v_file_handler);
    execute immediate v_drop_dir_sql;
else
    null;
end if;

    exception
    when others then
      UTL_FILE.FCLOSE(v_file_handler);
      execute immediate v_drop_dir_sql;
      raise_application_error(-20000, SQLERRM);

end write2file;
end BEH_NIPT_00;
/

/
QUIT
