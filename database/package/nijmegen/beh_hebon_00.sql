CREATE OR REPLACE PACKAGE "HELIX"."BEH_HEBON_00" is

procedure zet_pers_id
    ( p_pers_id IN number
    , x_hebon_un out varchar2
    , x_hebon_pw out varchar2
    );

PROCEDURE run_rapport
(
p_report            IN VARCHAR2 := NULL ,
p_desname           IN VARCHAR2 := NULL ,
p_pers_id           IN NUMBER := NULL ,
p_onde_id           IN NUMBER := NULL ,
p_hebon_un          IN VARCHAR2 := NULL ,
p_hebon_pw          IN VARCHAR2 := NULL ,
x_out              out varchar2
);



procedure maak_uitnodiging;

end beh_hebon_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_HEBON_00" as

procedure zet_pers_id
    ( p_pers_id IN number
    , x_hebon_un out varchar2
    , x_hebon_pw out varchar2
    )
is
  cursor c_hewa IS
  select hebon_un
  , hebon_pw
  , pers_id
  from beh_hebon_waarden hewa
  where pers_id is null
  and rownum = 1
  for update of pers_id;

  r_hewa c_hewa%rowtype;

  v_this_prog varchar2(100) := 'beh_hebon_00.zet_pers_id';
  v_error_txt varchar2(2000) := null;
begin

  if p_pers_id is not null then
    open  c_hewa;
    fetch c_hewa into r_hewa;
    If c_hewa%found then
      update BEH_HEBON_WAARDEN
      set pers_id = p_pers_id
      where current of c_hewa;
      x_hebon_un := r_hewa.hebon_un;
      x_hebon_pw := r_hewa.hebon_pw;
      commit;
    end if;
    close c_hewa;
  end if;

exception
  when others then
    v_error_txt := v_this_prog || ' => ' || SQLCODE||', '||SUBSTR(SQLERRM, 1, 1500);
    dbms_output.put_Line(v_error_txt);
    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
      values('PERS', p_pers_id, 'HEBON_UITN', substr(v_error_txt, 1, 2000));

end zet_pers_id;

PROCEDURE run_rapport
(p_report            IN VARCHAR2 := NULL ,
p_desname           IN VARCHAR2 := NULL ,
p_pers_id           IN NUMBER := NULL ,
p_onde_id           IN NUMBER := NULL ,
p_hebon_un          IN VARCHAR2 := NULL ,
p_hebon_pw          IN VARCHAR2 := NULL ,
x_out                out varchar2
)
is
  v_this_prog varchar2(100) := 'beh_hebon_00.run_rapport';

  myPlist SYS.SRW_PARAMLIST;
  myIdent SRW.Job_Ident;
  v_error_txt varchar2(2000);

  v_rapport_server_url kgc_systeem_parameters.standaard_waarde%type :=
         kgc_sypa_00.systeem_waarde('BEH_RAPPORT_SERVER_URL');

  v_rapport_server_naam kgc_systeem_parameters.standaard_waarde%type :=
         kgc_sypa_00.systeem_waarde('REPORTS_SERVER');

  v_rapport_server_user kgc_systeem_parameters.standaard_waarde%type :=
         kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER');


begin
  myPlist := SRW_PARAMLIST(SRW_PARAMETER('',''));

  v_rapport_server_user := beh_cryptit.decrypt_data(v_rapport_server_user);


  srw.add_parameter(myPlist, 'GATEWAY', v_rapport_server_url);
  srw.add_parameter(myPlist, 'SERVER', v_rapport_server_naam);
  srw.add_parameter(myPlist, 'REPORT', p_report);
  srw.add_parameter(myPlist, 'USERID', v_rapport_server_user);
  srw.add_parameter(myPlist, 'DESTYPE', 'file');
  srw.add_parameter(myPlist, 'DESFORMAT', 'PDF');
  srw.add_parameter(myPlist, 'DESNAME', p_desname);
  srw.add_parameter(myPlist, 'P_PERS_ID', p_pers_id);
  srw.add_parameter(myPlist, 'P_ONDE_ID', p_onde_id);
  srw.add_parameter(myPlist, 'P_HEBON_UN', p_hebon_un);
  srw.add_parameter(myPlist, 'P_HEBON_PW', p_hebon_pw);

  myIdent := srw.run_report(myPlist);

  x_out:= 'OK';

EXCEPTION
WHEN OTHERS THEN
    x_out:= 'NOK';
    v_error_txt := v_this_prog || ' => ' || SUBSTR(SQLERRM, 1, 1500);
    dbms_output.put_Line(v_error_txt);
    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
      values('PERS', p_pers_id, 'HEBON_UITN', substr(v_error_txt, 1, 2000));

end run_rapport;



procedure maak_uitnodiging
--(
--)
is
  cursor c_onde is
  select onde_id
  , pers_id
  , rela_id
  from beh_hebon_onde_uitn_vw
  ;

  r_onde c_onde%rowtype;

  cursor c_hewa (p_pers_id in number) is
  select hebon_un
  ,  hebon_pw
  , datum_uitnodiging
  , uitnodiging_gelukt
  from beh_hebon_waarden
  where pers_id = p_pers_id
  for update of uitnodiging_gelukt
  ;

  r_hewa c_hewa%rowtype;

  v_hebon_un varchar2(30) := null;
  v_hebon_pw varchar2(30) := null;
  v_hebon_pdf_ok boolean := FALSE;

  v_error_txt varchar2(2000) := null;
  v_this_prog varchar2(100) := 'beh_hebon_00.maak_uitnodiging';

  heeft_al_unpw varchar2(1) := 'O';

  v_locatie varchar2(100) := null;
  v_bestandsnaam varchar2(100) := null;
  v_desname varchar2(200) := null;
  v_report  varchar2(100) := null;
  v_datum   varchar2(20)  := null;
  v_btyp_id  number  := null;
  v_bcat_id  number  := null;

  p_report_ok  varchar2(10) := null;
  v_debug_txt varchar2(1000) := null;

begin
-- 1   pak een recent geautoriseerd onderzoek
  open c_onde;
  fetch c_onde into r_onde;
  while c_onde%found
  loop

    -- debug 1
    if v_debug_txt is null then
      v_debug_txt := 'debug 1';
    else
      v_debug_txt := v_debug_txt||', 1';
    end if;
   -- 2 bepaal hebon_un en hebon_pw
   -- zoek in de tabel hebon waarden of er een record is met deze pers_id
    open c_hewa(r_onde.pers_id);
    fetch c_hewa into r_hewa;

    if c_hewa%found then
      heeft_al_unpw := 'J';
      if (  (nvl(r_hewa.uitnodiging_gelukt, 'N') <> 'J') ) then
       -- 2 a er is een record gevonden pak hebon_un en hebon _pw in variabelen om te gebruiken in de pdf
        -- debug 2a
        if v_debug_txt is null then
          v_debug_txt := 'debug 2a';
        else
          v_debug_txt := v_debug_txt||',  2a';
        end if;
        v_hebon_un := r_hewa.hebon_un;
        v_hebon_pw := r_hewa.hebon_pw;
      end if;
    else
       -- 2b   er is geen record gevonden:
        -- debug 2b
        if v_debug_txt is null then
          v_debug_txt := 'debug 2b';
        else
          v_debug_txt := v_debug_txt||',  2b';
        end if;
         heeft_al_unpw := 'N';
    end if;


    close c_hewa;
   -- 2c Als er geen record is gevonden, doe dan een update met pers_id,  pak hebon_un en hebon _pw in variabelen om te gebruiken in de pdf
    if heeft_al_unpw = 'N' then
        zet_pers_id(r_onde.pers_id, v_hebon_un, v_hebon_pw);
        -- debug 2c
        if v_debug_txt is null then
          v_debug_txt := 'debug 2c';
        else
          v_debug_txt := v_debug_txt||',  2c';
        end if;


    end if;

   -- 3 maak de uinodiging PDF
    if ( (v_hebon_un is not null) and (v_hebon_pw is not null) ) then

--      maak_hebon_uitn_pdf(r_onde.pers_id, r_onde.onde_id, v_hebon_un, v_hebon_pw, v_hebon_pdf_ok);
          v_report := 'behhebonuit2.rdf';
          -- bepaalde bestandsnaam , nu even zo, straks een nummerstructuur voor maken?
          select to_char(sysdate, 'yyyy-mm-dd-hh24-mi-ss') into v_datum from dual;
          select standaard_pad into v_locatie from kgc_bestand_types where code = 'HEBON';
          v_bestandsnaam := 'test_hebon_uitnodiging'||r_onde.pers_id||'_'||v_datum||'.pdf';
          v_desname := v_locatie||'test_hebon_uitnodiging'||r_onde.pers_id||'_'||v_datum||'.pdf';
          -- doe run_report om de pdf te maken en weg te zetten
          if ( (v_report is not null)
             and (v_desname is not null)
             and (r_onde.pers_id is not null)
             and (r_onde.onde_id is not null)
             and (v_hebon_un is not null)
             and (v_hebon_pw is not null)
              )
          then
            run_rapport(v_report, v_desname, r_onde.pers_id, r_onde.onde_id, v_hebon_un, v_hebon_pw , p_report_ok);
        -- debug 3
        if v_debug_txt is null then
          v_debug_txt := 'debug 3';
        else
          v_debug_txt := v_debug_txt||',  3';
        end if;
          end if;

        if p_report_ok = 'OK' then

       --4 doe insert in de bestanden tabel
       select btyp_id into v_btyp_id from kgc_bestand_types where code = 'HEBON';
--       select bcat_id into v_bcat_id from kgc_bestand_categorieen where code = 'CORR_UIT';
       insert into kgc_bestanden(btyp_id
         , entiteit_code
         , entiteit_pk
         , locatie
         , bestandsnaam
--         , bcat_id
         )
       values(v_btyp_id
         , 'PERS'
         , r_onde.PERS_id
         , v_locatie
         , v_bestandsnaam
--         , v_bcat_id
         );
        -- debug 4
        if v_debug_txt is null then
          v_debug_txt := 'debug 4';
        else
          v_debug_txt := v_debug_txt||',  4, '||r_onde.pers_id||', '||v_locatie||', '||v_bestandsnaam;
        end if;

       -- 5 werk de gegevens bij in de tabel beh_hebon_waarden
       update BEH_HEBON_WAARDEN
       set uitnodiging_gelukt = 'J'
       , onde_id = r_onde.onde_id
       , datum_uitnodiging = sysdate
       where pers_id = r_onde.pers_id;
        -- debug 5
        if v_debug_txt is null then
          v_debug_txt := 'debug 5';
        else
          v_debug_txt := v_debug_txt||',  5';
        end if;
       else
         null;
         --doe  wat je moet doen als het rapport niet wordt aangemaakt, op dit moment niets.
       end if;

    end if;

     fetch c_onde into r_onde;
     dbms_output.put_line(v_debug_txt);
     v_debug_txt := null;
  end loop;
  close c_onde;


exception
  when others then
    v_error_txt := v_this_prog || ' => ' || SQLCODE||', '||SUBSTR(SQLERRM, 1, 1500);
    dbms_output.put_Line(v_error_txt);
    insert into KGC_NOTITIES(entiteit, id, code, omschrijving)
      values('PERS', r_onde.pers_id, 'HEBON_UITN', substr(v_error_txt, 1, 2000));


end maak_uitnodiging;

end beh_hebon_00;
/

/
QUIT
