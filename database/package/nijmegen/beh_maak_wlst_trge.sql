set define off
CREATE OR REPLACE PACKAGE "HELIX"."BEH_MAAK_WLST_TRGE" IS
  --
  -- Package voor het aanmaken van traygebruik en werklijsten.
  --

  -- Record met alle data om 1 trayvulling in de context van een tray_gebruik
  -- en 1 werklijstitem in de context van een werklijst te maken.
  TYPE trvu_wlst_rec IS RECORD
  ( meet_id           bas_meetwaarden.meet_id%TYPE
  , stof_id           kgc_stoftesten.stof_id%type
  , meti_id           bas_metingen.meti_id%type
  , mons_id           kgc_monsters.mons_id%type
  , stof_code         kgc_stoftesten.code%type
  , stof_omschrijving kgc_stoftesten.omschrijving%type
  , fractienummer     bas_fracties.fractienummer%type
  );

  TYPE TrayWlstVulling IS TABLE OF trvu_wlst_rec NOT NULL INDEX BY BINARY_INTEGER;

  procedure filter_en_plaats_op_werklijst( p_kafd_code       in kgc_kgc_afdelingen.code%type
                                         , p_tech_code       in kgc_technieken.code%type
                                         , p_trty_code       in kgc_tray_types.code%type
                                         , p_trgr_code       in kgc_tray_grids.code%type
                                         , p_stoftest_filter in kgc_stoftesten.code%type
                                         , p_html_message    in out clob
                                         );

  procedure plaats_op_werklijst( p_email_aan       in varchar2
                               , p_kafd_code       in kgc_kgc_afdelingen.code%type
                               , p_tech_code       in kgc_technieken.code%type
                               , p_trty_code       in kgc_tray_types.code%type
                               , p_trgr_code       in kgc_tray_grids.code%type
                               , p_stoftest_filter in kgc_stoftesten.code%type
                               );

END beh_maak_wlst_trge;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_MAAK_WLST_TRGE" IS

  v_crlf  CONSTANT varchar2(2) := chr(13)||chr(10);


  ------------------------------------------------------------------------------
  -- Maak tray_gebruik aan en geeft het aangemaakte trge_id terug.
  function create_trge( p_werklijst_nummer in kgc_tray_gebruik.werklijst_nummer%type
                      , p_mede_id in kgc_medewerkers.mede_id%type
                      , p_tech_id in kgc_technieken.tech_id%type
                      , p_trty_id in kgc_tray_types.trty_id%type
                      ) return kgc_tray_gebruik.trge_id%type is

    v_trge_id kgc_tray_gebruik.trge_id%type := NULL;

  begin
    select kgc_trge_seq.nextval into v_trge_id from dual;

    -- Maak een TRAY GEBRUIK aan in de tabel KGC_TRAY_GEBRUIK
    insert into kgc_tray_gebruik
    ( trge_id
    , mede_id
    , tech_id
    , trty_id
    , datum
    , gefixeerd
    , werklijst_nummer
    )
    values
    ( v_trge_id
    , p_mede_id
    , p_tech_id
    , p_trty_id
    , sysdate
    , 'J'
    , p_werklijst_nummer
    );

    return v_trge_id;

  end;


  ------------------------------------------------------------------------------
  -- Maak genereer een werklijstnummer d.vm.v. de Werklijstnummer structuur.
  function create_werklijst_nummer return varchar2 is

    v_werklijst_nummer varchar2(100);

  begin
    v_werklijst_nummer := kgc_nust_00.genereer_nr
    ( p_nummertype => 'WERK'
    , p_nummerstructuur => 'WERK-MEDE'
    , p_kafd_id => 1
    , p_ongr_id => null
    );

    return v_werklijst_nummer;
  end;


  ------------------------------------------------------------------------------
  -- Maak tray_vulling aan op basis van de gegevens in de p_vulling_tabel tabel.
  procedure create_trvu( p_trge_id      in out kgc_tray_gebruik.trge_id%type
                       , p_trty_id          in kgc_tray_types.trty_id%type
                       , p_trgr_id          in kgc_tray_grids.trgr_id%type
                       , p_vulling_tabel    in TrayWlstVulling
                       , p_html_message in out clob) is

    v_session_id number;
    v_rowcount   number;
    v_bericht    varchar2(500);

  begin

    v_session_id := SYS_CONTEXT ('USERENV', 'SESSIONID');

    DELETE from kgc_tray_wizard_temp twtp
    where twtp.session_id = v_session_id;

    DELETE from kgc_tray_wizard_stof_temp twst
    where twst.session_id = v_session_id;

    for idx in p_vulling_tabel.first..p_vulling_tabel.last loop

      v_bericht := 'Plaats stoftest nr. ' || to_char(idx)
                || ' fractienummer: ' || p_vulling_tabel(idx).fractienummer
                || ' stof_code: ' || p_vulling_tabel(idx).stof_code
                || ' stof_omschrijving: ' || p_vulling_tabel(idx).stof_omschrijving
                || ' (meet_id: ' || p_vulling_tabel(idx).meet_id || ')';

      p_html_message := p_html_message || v_bericht || '</br>' || v_crlf;
      --dbms_output.put_line(v_bericht);

      insert into kgc_tray_wizard_temp
      ( session_id
      , username
      , meet_id
      , stof_id
      , stof_code
      , stof_omschrijving
      , fractienummer
      , soort_fractie
      , volgorde
      )
      values
      ( v_session_id
      , user
      , p_vulling_tabel(idx).meet_id
      , p_vulling_tabel(idx).stof_id
      , p_vulling_tabel(idx).stof_code
      , p_vulling_tabel(idx).stof_omschrijving
      , p_vulling_tabel(idx).fractienummer
      , 'F' --Soort fractie F of S
      , idx
      );

      -- Update de kgc_tray_wizard_stof_temp tabel met het aantal
      -- geselecteerde stoftesten.
      begin

        update kgc_tray_wizard_stof_temp
        set origineel_gewenst = origineel_gewenst + 1
          , gewenst = gewenst + 1
          , geselecteerd = geselecteerd + 1
        where session_id = v_session_id
          and username = user
          and stof_id = p_vulling_tabel(idx).stof_id;

        v_rowcount := sql%rowcount;

        --Als er geen records is om te updaten, maak een nieuw record aan.
        if v_rowcount = 0 then

          insert into kgc_tray_wizard_stof_temp
          (	session_id
          , username
          , stof_id
          , origineel_gewenst, gewenst, geplaatst, geselecteerd
          )
          values
          ( v_session_id
          , user
          , p_vulling_tabel(idx).stof_id
          , 1, 1, 0, 1
          );

        end if;

      end;

    end loop;

    p_html_message := p_html_message || 'Vul de tray met de geplaatste meetwaarden.</br>' || v_crlf;

    kgc_trge_00.vul_tray
    ( p_trty_id         => p_trty_id
    , p_trgr_id         => p_trgr_id
    , p_trge_id         => p_trge_id
    , p_werklijst_maken => 'N'
    , p_standaard_tray  => 'N'
    );

  end;


  ------------------------------------------------------------------------------
  -- Filter de meetwaarden en plaats deze op een tray en een werklijst.
  procedure filter_en_plaats_op_werklijst( p_kafd_code       in kgc_kgc_afdelingen.code%type
                                         , p_tech_code       in kgc_technieken.code%type
                                         , p_trty_code       in kgc_tray_types.code%type
                                         , p_trgr_code       in kgc_tray_grids.code%type
                                         , p_stoftest_filter in kgc_stoftesten.code%type
                                         , p_html_message    in out clob
                                         ) is

    cursor c_tray_meetwaarden( b_kafd_id kgc_kgc_afdelingen.kafd_id%type
                             , b_stoftest_filter kgc_stoftesten.code%type ) is
    select tmwv.meet_id
         , tmwv.stof_id
         , tmwv.meti_id
         , tmwv.mons_id
         , stof.code
         , stof.omschrijving
         , tmwv.fractienummer
    from kgc_stoftesten stof
      join bas_tray_meetwaarden_vw tmwv on tmwv.stof_id = stof.stof_id
    where stof.omschrijving like b_stoftest_filter
      and stof.kafd_id = b_kafd_id
      and stof.vervallen = 'N'
      and tmwv.kafd_id = stof.kafd_id
      and tmwv.voortest_ok = 'J'
      and stan_id is null
      and nvl(frac_status,'O') in ('O','P')
      and not exists
          ( select null
            from kgc_tray_vulling trvu
            where trvu.meet_id = tmwv.meet_id
          );

    v_vulling_tabel     TrayWlstVulling;

    v_mede_id           kgc_medewerkers.mede_id%type := null;
    v_werklijst_nummer  kgc_werklijsten.werklijst_nummer%type;
    v_trge_id           kgc_tray_gebruik.trge_id%type := null;
    v_trty_id           kgc_tray_types.trty_id%type := null;
    v_trgr_id           kgc_tray_grids.trgr_id%type := null;
    v_tech_id           kgc_technieken.tech_id%type := null;
    v_kafd_id           kgc_kgc_afdelingen.kafd_id%type;

  begin
    v_tech_id := kgc_uk2pk.tech_id(p_tech_code);

    --v_mede_id := kgc_mede_00.medewerker_id(user);
    v_mede_id := kgc_uk2pk.mede_id('ROBOT');

    v_werklijst_nummer := create_werklijst_nummer();
    v_werklijst_nummer := substr(v_werklijst_nummer,1,11)||'ROBOT';


    p_html_message := p_html_message || 'Werklijst nummer: '
                                     || v_werklijst_nummer
                                     || '</br>' || v_crlf;

    v_kafd_id := kgc_uk2pk.kafd_id(p_kafd_code);

    begin

      select trty.trty_id
      into   v_trty_id
      from   kgc_tray_types trty
      where  trty.code = p_trty_code
        and  trty.kafd_id = v_kafd_id;

    exception when no_data_found then
      p_html_message := p_html_message || '<font color="red">Traytype code ' || p_trty_code || ' niet gevonden.</font></br>' || v_crlf;
      return;
    end;

    --Zoek TrayGrid voor afdeling en TrayType.
    begin

      select trgr_id
      into   v_trgr_id
      from   kgc_tray_grids trgr
      where  trgr.trty_id = v_trty_id
        and  trgr.kafd_id = v_kafd_id
        and  trgr.code = p_trgr_code;

    exception when no_data_found then
      p_html_message := p_html_message || '<font color="red">Traygrid code ' || p_trgr_code || ' niet gevonden.</font></br>' || v_crlf;
      return;
    end;

    p_html_message := p_html_message || 'Traytype ' || p_trty_code
                                     || ' geselecteerd met grid ' || p_trgr_code
                                     || '</br>' || v_crlf;

    p_html_message := p_html_message || 'Gebruikte techniek: ' || p_tech_code
                                     || '</br>' || v_crlf;

    --Maak Traygebruik record aan.
    v_trge_id := create_trge( v_werklijst_nummer
                            , v_mede_id
                            , v_tech_id
                            , v_trty_id );

    p_html_message := p_html_message || 'Filter stoftesten op naam '
                                     || p_stoftest_filter
                                     || ' voor afdeling '
                                     || p_kafd_code || '</br>' || v_crlf;

    -- Haal alle rijen van de cursor op en vul de pl/sql tabel v_vulling_tabel.
    open c_tray_meetwaarden(v_kafd_id, p_stoftest_filter);
    fetch c_tray_meetwaarden bulk collect into v_vulling_tabel;

    if v_vulling_tabel.count > 0 then
      p_html_message := p_html_message || 'Aantal stoftesten gevonden: '
                                       || to_char(v_vulling_tabel.count)
                                       || '</br></br>' || v_crlf;
      --dbms_output.put_line('Aantal stoftesten gevonden: ' || to_char(v_vulling_tabel.count));

      create_trvu( v_trge_id
                 , v_trty_id
                 , v_trgr_id
                 , v_vulling_tabel
                 , p_html_message);

      commit;

    else
      p_html_message := p_html_message || 'Geen stoftesten gevonden die voldoen aan dit filter!</br>' || v_crlf;
      --dbms_output.put_line('Geen stoftesten gevonden die voldoen aan dit filter!');
      rollback;
    end if;

  end;


  ------------------------------------------------------------------------------
  procedure plaats_op_werklijst( p_email_aan       in varchar2
                               , p_kafd_code       in kgc_kgc_afdelingen.code%type
                               , p_tech_code       in kgc_technieken.code%type
                               , p_trty_code       in kgc_tray_types.code%type
                               , p_trgr_code       in kgc_tray_grids.code%type
                               , p_stoftest_filter in kgc_stoftesten.code%type
                               ) is

    v_procedure_naam  varchar2(45) := 'beh_maak_wlst_trge.plaats_op_werklijst';
    v_html_message    clob;
    v_now             date;
    v_email_onderwerp varchar2(100);
    v_email_aan       varchar2(100);
    v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                        , p_mede_id        => NULL
                                                                        )
                                          , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                          );

  begin

    v_now := sysdate;
    --v_email_aan := beh_interfaces.beh_get_user_email;
    v_email_aan := p_email_aan || ';rene.brand@radboudumc.nl';

    v_email_onderwerp := 'Helix overzicht automatisch stoftesten op werklijst plaatsen';
    v_html_message    := 'Dit is een automatisch gegenereerd rapport over het plaatsen van meetwaarden op werklijsten '
                      || 'en het aanmaken van traygebruik gebaseerd op geselecteerde stoftesten.</br></br>' || v_crlf;

    v_html_message    := v_html_message || 'Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf;
    v_html_message    := v_html_message || 'Uitgevoerd door: ' || user || '</br>' || v_crlf;
    v_html_message    := v_html_message || 'Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>' || v_crlf;
    v_html_message    := v_html_message || 'Draai datum: ' || to_char(v_now) || '</br></br>' || v_crlf;

    --dbms_output.put_line('Afdeling: ' || p_kafd_code);
    --dbms_output.put_line('Technologie: ' ||p_tech_code);
    --dbms_output.put_line('Traytype: ' || p_trty_code);
    --dbms_output.put_line('Traygrid: ' || p_trgr_code);
    --dbms_output.put_line('Stoftest: ' || p_stoftest_filter);

    v_html_message    := v_html_message || '<ul></br>';

    filter_en_plaats_op_werklijst( p_kafd_code       => p_kafd_code
                                 , p_tech_code       => p_tech_code
                                 , p_trty_code       => p_trty_code
                                 , p_trgr_code       => p_trgr_code
                                 , p_stoftest_filter => p_stoftest_filter
                                 , P_html_message    => v_html_message
                                 );

    v_html_message := v_html_message || '</ul>';
    v_html_message := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );

    --dbms_output.put_line('Sent mail: ' || v_html_message);

  exception when others then
    v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
    v_html_message    := v_html_message || '</br><font color="red">FOUT:' || sqlerrm || '</br>'
                                                                          || dbms_utility.format_error_backtrace || '</br>'
                                                                          || dbms_utility.format_call_stack
                                                                          ||'</font></br></br>' || v_crlf;
    v_html_message    := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan
                  , p_cc      => v_email_afzender
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );

    --dbms_output.put_line('Exception: ' || v_html_message);

  end;


END beh_maak_wlst_trge;
/

