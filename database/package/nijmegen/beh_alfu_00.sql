CREATE OR REPLACE PACKAGE "HELIX"."BEH_ALFU_00" is
--(AL)gemene plsql (FU)ncties/procedures
-- voor onderstaande functie (explode) moet je eerste deze script draaien:
--CREATE OR REPLACE TYPE listtable AS TABLE OF VARCHAR2 (255);
function explode
(
p_seperator in varchar2,
p_string in varchar2
) return beh_listtable;

function is_datum_goed
(
p_datum in varchar2,
p_formaat in varchar2 := 'dd-mm-yyyy'
)
return boolean;
function is_nl_postcode
(
p_postcode in varchar2
)
return boolean;
function is_nummer_goed
(
p_nummer in varchar2
)
return boolean;
function is_nummer
(
p_nummer in varchar2
)
return varchar2;
function is_telnr_goed
(
p_telefoon varchar2
)
return boolean;
function is_mobielnr_goed
(
p_mobielnr varchar2
)
return boolean;
function karakter_teller
(
--p_string in varchar2,
p_string in clob,
p_pattern in varchar2
)
return number;
function verstuur_sms
(
p_afzender  in varchar2,
p_ontvanger in varchar2,
p_bericht   in varchar2,
x_message   out varchar2
)
return boolean;
function calc_string
(
p_string in varchar2
)
return number;
function jobs_last_date
(
p_job_naam varchar2
) return date;
function get_filename
   (p_path IN VARCHAR2)
RETURN varchar2;
function get_dir_path
   (p_path IN VARCHAR2)
RETURN varchar2;
function file_exists
   (p_dir_file IN VARCHAR2)
RETURN varchar2;
function del_file
   (p_dir_file IN VARCHAR2)
RETURN varchar2;
function file_copy
   (p_dir_file_from IN VARCHAR2,
   p_dir_file_to IN VARCHAR2)
RETURN varchar2;
function file_move
   (p_dir_file_from IN VARCHAR2,
   p_dir_file_to IN VARCHAR2)
RETURN varchar2;
End beh_alfu_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_ALFU_00" is
-----------------------------------------------------------------------
------ een string met een separator, wordt als meerdere records
------ teruggegevens. Iedere string tussen separators, wordt als een record
------ beschouwd bijv. 'cyto, dina, metab', wordt als drie records teruggegeven.
------ zie Metab_zoekvraag_18.sql voor voorbeeld.
----------------------------------------------------------------------
function explode
(
p_seperator in varchar2,
p_string in varchar2
)
   return beh_listtable as
   l_string   long          default p_string || p_seperator;
   l_data     beh_listtable := beh_listtable ();
   n          number;
begin
   loop
      exit when l_string is null;
      n := instr (l_string, p_seperator);
      l_data.extend;
      l_data (l_data.count) := ltrim (rtrim (substr (l_string, 1, n - 1)));
      l_string := substr (l_string, n + 1);
   end loop;

   return l_data;
end;
--##########################
function is_datum_goed
(
p_datum in varchar2,
p_formaat in varchar2 := 'dd-mm-yyyy'
)
return boolean
is
v_datum date;

begin
select to_date(p_datum, p_formaat) into v_datum from dual;
return true;
exception
 when others then
    return false;
end is_datum_goed;
-----------------------------------------------------------------------
------ checken of p_postcode een geldige NL postcode formaat heeft
----------------------------------------------------------------------
function is_nl_postcode
(
p_postcode in varchar2
)
return boolean
is

v_postcode kgc_instellingen.postcode%type := upper(trim(replace(p_postcode, ' ')));
v_karakter_voor_check varchar2(1);
begin
if (length (v_postcode) = 6) then

    for i in 1..length(v_postcode) loop

        v_karakter_voor_check := substr(v_postcode, i, 1);
        if i <= 4 then -- eerste 4 karakters moeten cijfers zijn
            if not((ascii(v_karakter_voor_check)>=48) and  --0 t/m 9
                   (ascii(v_karakter_voor_check)<=57)) then
                return false;
            end if;
        else -- laatste 2 karakters moeten letters zijn
            if not((ascii(v_karakter_voor_check)>=65) and  -- Aa t/m zZ
                   (ascii(v_karakter_voor_check)<=122)) then
                return false;
            end if;
        end if;
    end loop;
    return true;
else
    return false;
end if;
end is_nl_postcode;
--######################
function is_nummer_goed
(
p_nummer in varchar2
)
return boolean
is
v_nummer number;

begin
select to_number(p_nummer) into v_nummer from dual;
return true;
exception
 when others then
    return false;
end is_nummer_goed;
function is_nummer
(
p_nummer in varchar2
)
return varchar2
is
v_nummer number;

begin
select to_number(p_nummer) into v_nummer from dual;
return 'J';
exception
 when others then
    return 'N';
end is_nummer;
--##########################
function is_telnr_goed
(
p_telefoon varchar2
) return boolean is
v_telefoon kgc_personen.telefoon%type := regexp_replace(p_telefoon, '[^[:digit:]]');

begin

if length(v_telefoon) = 10 then
    if substr(v_telefoon, 0, 1) = '0' then
      if substr(v_telefoon, 0, 2) = '00' then
        return false;
      else
        return true;
      end if;
    else
        return false;
    end if;
else
    return false;
end if;
end is_telnr_goed;
--##########################
function is_mobielnr_goed
(
p_mobielnr varchar2
) return boolean is
v_mobielnr kgc_personen.telefoon%type := regexp_replace(p_mobielnr, '[^[:digit:]]');

begin

if length(v_mobielnr) = 10 then
    if substr(v_mobielnr, 0, 2) = '06' then
        return true;
    else
        return false;
    end if;
else
    return false;
end if;
end is_mobielnr_goed;
-----------------------------------------------------------------------
------ Hoe vaak komt een pattern/karakter in een string voor?
----------------------------------------------------------------------
function karakter_teller
(
p_string clob,
--p_string varchar2,
p_pattern varchar2
)return number is

v_aantal number := 0;
v_next_index number := 1;
--v_string varchar2(32000);
v_string clob;
v_pattern varchar2(32000);

begin
v_string := lower(p_string);
v_pattern := lower(p_pattern);

for i in 1 .. length(v_string) loop
  if (length(v_pattern) <= length(v_string)-v_next_index+1)
  and (substr(v_string,v_next_index,length(v_pattern)) = v_pattern) then
    v_aantal := v_aantal+1;
  end if;
v_next_index := v_next_index+1;
end loop;
return v_aantal;
end karakter_teller;
-----------------------------------------------------------------------
------ Versturen van SMS vanuit Helix db
------ voor meer info zie:
------ http://www.mollie.nl/support/documentatie/sms-diensten/sms/http/
----------------------------------------------------------------------
function verstuur_sms
  ( p_afzender  in varchar2 -- maximaal 14 cijfers of 11 tekens
  , p_ontvanger in varchar2 -- nummers scheiden met komma
  , p_bericht   in varchar2
  , x_message   out varchar2
  )
return boolean is

v_mollie_username constant varchar2(10) := kgc_sypa_00.systeem_waarde ('BEH_SMS_PROVIDER_USERNAME');
v_mollie_password constant varchar2(50) :=
        beh_cryptit.decrypt_data(kgc_sypa_00.systeem_waarde ('BEH_SMS_PROVIDER_WACHTWOORD'));

v_mollie_url constant varchar2(100) := kgc_sypa_00.systeem_waarde ('BEH_SMS_PROVIDER_URL');

v_request      utl_http.req;
v_post_text     varchar2(500);
v_response     utl_http.resp;
v_response_text varchar2(2000);

begin

----------------------------------------------------------------------------
-- Opbouw van http opdracht
----------------------------------------------------------------------------
v_post_text :=
    'username='     ||Utl_Url.escape(v_mollie_username, true)     ||chr(38)||
    'password='     ||Utl_Url.escape(v_mollie_password, true)     ||chr(38)||
    'originator='   ||Utl_Url.escape(p_afzender, true)            ||chr(38)||
    'recipients='   ||Utl_Url.escape(p_ontvanger, true)           ||chr(38)||
    'message='      ||Utl_Url.escape(p_bericht, true);

----------------------------------------------------------------------------
-- Verzenden van SMS via een http opdracht.
----------------------------------------------------------------------------
v_request := Utl_Http.begin_request
    ( url    => v_mollie_url
    , method => 'POST'
    );

Utl_Http.set_header
    ( r     => v_request
    , name  => 'Content-Type'
    , value => 'application/x-www-form-urlencoded'
    );

Utl_Http.set_header
    ( r     => v_request
    , name  => 'Content-Length'
    , value => length(v_post_text)
    );

Utl_Http.write_text
    ( r    => v_request
    , data => v_post_text
    );

v_response := Utl_Http.get_response(v_request);

-- code 200 betekent http opdracht is goed uitgevoerd
if v_response.status_code = '200' then
    Utl_Http.read_text(v_response, v_response_text);
    x_message := v_response_text;
else
    x_message := 'HTTP status: '||v_response.status_code||'-'||v_response.reason_phrase;
end if;

Utl_Http.end_response(v_response);

if x_message like '%<resultmessage>Message successfully sent.</resultmessage>%' then
    return true;
else
    return false;
end if;

exception
when others then
  x_message := substr(sqlerrm, 1, 4000);
  return false;
end verstuur_sms;
function calc_string
(
p_string varchar2
) return number is
n number;
begin
execute immediate 'select '||p_string||' from dual' into n;
return round(n, 2);
exception
when others then
  return -99999999;
end calc_string;
--########################
function jobs_last_date
(
p_job_naam varchar2
) return date
is
v_peildatum date;

v_message varchar2(2500);
v_db_naam varchar(100) := sys_context('USERENV', 'DB_NAME');

v_sender varchar(200) :=
        kgc_sypa_00.standaard_waarde('BEH_MIJN_HELIX_EMAIL_ADRES', null, null, 'HELIX');
v_recipients varchar(200) :=
        kgc_sypa_00.standaard_waarde('BEH_MIJN_HELIX_EMAIL_ADRES', null, null, 'HELIX');
v_subject varchar(100) := v_db_naam || ';' || p_job_naam;
v_boodschap varchar(2500) :='job ' || p_job_naam || ' niet gevonden!';

cursor c_jobs is
select max(log_date) as log_date
from DBA_SCHEDULER_JOB_LOG
where instr(UPPER(job_name), UPPER(p_job_naam)) > 0
AND STATUS = 'SUCCEEDED'
;
r_jobs c_jobs%rowtype;

begin
open c_jobs;
fetch c_jobs into r_jobs;

if c_jobs%found then
    v_peildatum := nvl(r_jobs.log_date, sysdate);
else
    UTL_MAIL.SEND(  sender => v_sender,
                    recipients => v_recipients,
                    subject => v_subject,
                    message => v_boodschap);
end if;

close c_jobs;

return v_peildatum;

end jobs_last_date;
function get_filename
   (p_path IN VARCHAR2)
RETURN varchar2

IS
     v_file VARCHAR2(500);

BEGIN
    -- Parse string for UNIX system
    IF instr(p_path,'/') > 0 THEN
        v_file := substr(p_path,(instr(p_path,'/',-1,1)+1),length(p_path));

    -- Parse string for Windows system
    ELSIF instr(p_path,'\') > 0 THEN
        v_file := substr(p_path,(instr(p_path,'\',-1,1)+1),length(p_path));

    -- If no slashes were found, return the original string
    ELSE
        v_file := p_path;
    END IF;

    RETURN v_file;

END;
function get_dir_path
   (p_path IN VARCHAR2)
RETURN varchar2

IS
     v_dir VARCHAR2(1500);

BEGIN

    -- Parse string for UNIX system
    IF instr(p_path,'/') > 0 THEN
        v_dir := substr(p_path,1,(instr(p_path,'/',-1,1)-1));

    -- Parse string for Windows system
    ELSIF instr(p_path,'\') > 0 THEN
        v_dir := substr(p_path,1,(instr(p_path,'\',-1,1)-1));

    -- If no slashes were found, return the original string
    ELSE
        v_dir := p_path;
    END IF;

    RETURN v_dir;

END;
function file_exists
   (p_dir_file IN VARCHAR2)
RETURN varchar2
IS
-- deze functie kan checken of een file bestaat of niet
-- p_dir_file bevat het pad + file naam. Bijv.
-- \\umcstgen01\f$\DataExchange\UITSLAGEN\TEST\DNA\DNA_UITS_2245907_33035X_001.pdf
paramlist beh_SOAP_PARAMLIST;
v_soap_service varchar2(100) := 'file_exists';
v_dir_file varchar2(1000):= trim(p_dir_file);
v_return varchar2(1000);

BEGIN
--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));

-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'param_01', '<![CDATA[' || v_dir_file || ']]>');

--aanroep soapservice
-- goede antwoord kan J of N zijn.
v_return := beh_soap_00.call_soap_service(v_soap_service,paramlist);

return v_return;
END file_exists;
function del_file
   (p_dir_file IN VARCHAR2)
RETURN varchar2
IS
-- deze functie kan een file op een netwerk directory verwijderen.
-- p_dir_file bevat het pad + file naam die verwijder moeten worden. Bijv.
-- \\umcstgen01\f$\DataExchange\UITSLAGEN\TEST\DNA\DNA_UITS_2245907_33035X_001.pdf
paramlist beh_SOAP_PARAMLIST;
v_soap_service varchar2(100) := 'del_file';
v_dir_file varchar2(1000):= trim(p_dir_file);
v_handshake kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER'));
v_return varchar2(1000);

BEGIN
--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));

-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'param_01', v_handshake);
beh_soap_00.add_parameter(paramlist, 'param_02', '<![CDATA[' || v_dir_file || ']]>');
beh_soap_00.add_parameter(paramlist, 'param_03', sys_context('userenv','db_name'));

--aanroep soapservice
-- goede antwoord kan J of N zijn.
v_return := beh_soap_00.call_soap_service(v_soap_service,paramlist);

return v_return;
END del_file;
function file_copy
   (p_dir_file_from IN VARCHAR2,
   p_dir_file_to IN VARCHAR2)
RETURN varchar2
is
-- deze functie kan een file op een netwerk directory
-- naar een andere netwerk directory copieren.
-- p_dir_file_from bevat het pad + file naam die gecopieerd  moet worden. Bijv.
-- \\umcstgen01\f$\DataExchange\UITSLAGEN\TEST\DNA\DNA_UITS_2245907_33035X_001.pdf
-- p_dir_file_to bevat het pad + file naam waarnaar gecopieerd  moet worden. Bijv.
-- \\umcstgen01\f$\DataExchange\UITSLAGEN\ACPT\DNA\DNA_UITS_2245907_33035X_001.pdf
-- xhlxias moet toegant hebben tot beide files.
paramlist beh_SOAP_PARAMLIST;
v_soap_service varchar2(100) := 'file_copy';

begin
--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));

-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'param_01', trim(kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER')));
beh_soap_00.add_parameter(paramlist, 'param_02', sys_context('userenv','db_name'));
beh_soap_00.add_parameter(paramlist, 'param_03', '<![CDATA[' || p_dir_file_from || ']]>');
beh_soap_00.add_parameter(paramlist, 'param_04', '<![CDATA[' || p_dir_file_to || ']]>');

--aanroep soapservice
-- goede antwoord = OK.
return beh_soap_00.call_soap_service(v_soap_service,paramlist);

end file_copy;
function file_move
   (p_dir_file_from IN VARCHAR2,
   p_dir_file_to IN VARCHAR2)
RETURN varchar2
is
-- deze functie kan een file op een netwerk directory
-- naar een andere netwerk directory verplaatsen.
-- p_dir_file_from bevat het pad + file naam die gecopieerd  moet worden. Bijv.
-- \\umcstgen01\f$\DataExchange\UITSLAGEN\TEST\DNA\DNA_UITS_2245907_33035X_001.pdf
-- p_dir_file_to bevat het pad + file naam waarnaar gecopieerd  moet worden. Bijv.
-- \\umcstgen01\f$\DataExchange\UITSLAGEN\ACPT\DNA\DNA_UITS_2245907_33035X_001.pdf
-- xhlxias moet toegant hebben tot beide files.
paramlist beh_SOAP_PARAMLIST;
v_soap_service varchar2(100) := 'file_move';

begin
--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));

-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'param_01', trim(kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER')));
beh_soap_00.add_parameter(paramlist, 'param_02', sys_context('userenv','db_name'));
beh_soap_00.add_parameter(paramlist, 'param_03', '<![CDATA[' || p_dir_file_from || ']]>');
beh_soap_00.add_parameter(paramlist, 'param_04', '<![CDATA[' || p_dir_file_to || ']]>');

--aanroep soapservice
-- goede antwoord = OK.
return beh_soap_00.call_soap_service(v_soap_service,paramlist);

end file_move;
End beh_alfu_00;
/

/
QUIT
