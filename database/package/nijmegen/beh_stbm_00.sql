CREATE OR REPLACE PACKAGE "HELIX"."BEH_STBM_00" as
g_scheidingsteken varchar2(2) := ', ';
function haal_alle_fami_op
(
p_pers_id in number
)
return varchar2
;

function haal_alle_indi_op
(
p_onde_id in number
)
return varchar2
;
function haal_alle_indi_omsch_op
(
p_onde_id in number
)
return varchar2
;
function haal_alle_onde_op
(
p_pers_id in number
)
return varchar2
;

function haal_alle_mdet_op
(
p_onde_id in number,
p_frac_id in number,
p_kolom_pass in varchar,
p_met_prompt in varchar := 'N'
)
return varchar2
;
function haal_alle_frac_op
(
p_pers_id in number
)
return varchar2
;
function haal_alle_klingen_onde_op
(
p_pers_id in number
)
return varchar2
;

end beh_stbm_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_STBM_00" as
function haal_alle_fami_op
(p_pers_id in number)

return varchar2
is
v_return varchar2(20000);

cursor c_fami is
select pers.pers_id, fami.fami_id, fami.familienummer
from kgc_personen pers, kgc_families fami, kgc_familie_leden fale
where pers.pers_id = fale.pers_id
and   fale.fami_id = fami.fami_id
and   pers.pers_id = p_pers_id
;

begin
for r_fami in c_fami loop

    if v_return is null then
        v_return := r_fami.familienummer;
    else
        v_return := v_return || g_scheidingsteken || r_fami.familienummer;
    end if;

end loop;
return trim(v_return);
end haal_alle_fami_op;

function haal_alle_indi_op
(p_onde_id in number)

return varchar2
is
v_return varchar2(20000);

cursor c_indi is
-- pattern = underscore(_) + 2 {2} digits ([0-9]) aan het eind($) van indi.code
select regexp_replace(indi.code, '_[0-9]{2}$') code
from kgc_onderzoek_indicaties onin, kgc_indicatie_teksten indi
where onin.indi_id = indi.indi_id
and   onin.onde_id = p_onde_id
order by onin.volgorde, onin.creation_date
;

begin
for r_indi in c_indi loop

    if v_return is null then
        v_return := r_indi.code;
    else
        v_return := v_return || g_scheidingsteken || r_indi.code;
    end if;

end loop;
return trim(v_return);
end haal_alle_indi_op;
function haal_alle_indi_omsch_op
(p_onde_id in number)

return varchar2
is
v_return varchar2(20000);

cursor c_indi is
select indi.omschrijving
from kgc_onderzoek_indicaties onin, kgc_indicatie_teksten indi
where onin.indi_id = indi.indi_id
and   onin.onde_id = p_onde_id
order by onin.volgorde, onin.creation_date
;

begin
for r_indi in c_indi loop

    if v_return is null then
        v_return := r_indi.omschrijving;
    else
        v_return := v_return || g_scheidingsteken || r_indi.omschrijving;
    end if;

end loop;
return trim(v_return);
end haal_alle_indi_omsch_op;
function haal_alle_onde_op
(p_pers_id in number)

return varchar2
is
v_return varchar2(20000);

cursor c_onde is
select onde.onderzoeknr
from kgc_personen pers, kgc_onderzoeken onde
where pers.pers_id = onde.pers_id
and   pers.pers_id = p_pers_id
;

begin
for r_onde in c_onde loop

    if v_return is null then
        v_return := r_onde.onderzoeknr;
    else
        v_return := v_return || g_scheidingsteken || r_onde.onderzoeknr;
    end if;

end loop;
return trim(v_return);
end haal_alle_onde_op;
function haal_alle_mdet_op
(
p_onde_id in number,
p_frac_id in number,
p_kolom_pass in varchar,
p_met_prompt in varchar := 'N'
)
return varchar2
is

v_vorige_prompt beh_stbm_alle_mdet_vw.prompt%type;
v_waarde beh_stbm_alle_mdet_vw.waarde%type;
v_leesteken varchar2(4) := ' => ';
v_return varchar2(20000);

cursor c_mdet is
select *
from beh_stbm_alle_mdet_vw
where frac_id = p_frac_id
and   onde_id = p_onde_id
and   upper(kolom_pass) = upper(p_kolom_pass)
order by prompt, sortering
;

begin
    for r_mdet in c_mdet loop
        v_waarde := nvl(r_mdet.waarde, r_mdet.db_waarde);
        if upper(p_met_prompt) = upper('J') then
            if upper(r_mdet.prompt) = upper(v_vorige_prompt) then
                if v_return is null then
                    v_return := r_mdet.prompt || v_leesteken || v_waarde;
                else
                    v_return := v_return || g_scheidingsteken || v_waarde;
                end if;
            else
                if v_return is null then
                    v_return := r_mdet.prompt || v_leesteken || v_waarde;
                else
                    v_return := v_return || g_scheidingsteken || r_mdet.prompt || v_leesteken || v_waarde;
                end if;
            end if;
            v_vorige_prompt := r_mdet.prompt;
        else
            if v_return is null then
                v_return := trim(v_waarde);
            else
                v_return := trim(v_return || g_scheidingsteken || v_waarde);
            end if;
        end if;
    end loop;
    return trim(v_return);
    end haal_alle_mdet_op;
function haal_alle_frac_op
    (p_pers_id in number)

    return varchar2
    is
    v_return varchar2(4000);

    cursor c_frac is
    select frac.fractienummer
    from kgc_monsters mons
    , bas_fracties frac
    where mons.mons_id = frac.mons_id
    and   mons.pers_id = p_pers_id
    ;

    begin
    for r_frac in c_frac loop

        if v_return is null then
            v_return := r_frac.fractienummer;
        else
            v_return := substr(v_return || g_scheidingsteken || r_frac.fractienummer, 1, 4000);
        end if;

    end loop;
    return trim(v_return);
    end haal_alle_frac_op;
function haal_alle_klingen_onde_op
(p_pers_id in number)

return varchar2
is
v_return varchar2(20000);

cursor c_onde is
select onde.onderzoeknr
from kgc_personen pers,
     kgc_onderzoeken onde,
     kgc_kgc_afdelingen kafd
where pers.pers_id = onde.pers_id
and   pers.pers_id = p_pers_id
and   onde.kafd_id = kafd.kafd_id
and   upper(kafd.code) = upper('KLINGEN')
and   upper(substr(onde.onderzoeknr, 1, 1)) = upper('P')
;

begin
for r_onde in c_onde loop

    if v_return is null then
        v_return := r_onde.onderzoeknr;
    else
        v_return := trim(v_return || g_scheidingsteken || r_onde.onderzoeknr);
    end if;

end loop;
return trim(v_return);
end haal_alle_klingen_onde_op;
    end beh_stbm_00;
/

/
QUIT
