CREATE OR REPLACE PACKAGE "HELIX"."BEH_MAIL_NOTIFICATIE_00" AS
/******************************************************************************
   NAME:       BEH_MAIL_NOTIFICATIE_00
   PURPOSE: Deze package wordt gebruikt om notitificatie mails te versturen. Dit is nu gemaakt voor RFD onderzoeksgroep. Indien er een onderzoek voor RFD onderzoeksgroep in Helix aangemaakt wordt, dan moet er notificatie mail gestuurd worden naar deze groep.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2016      M.Golebiowska       1. Created this package.
******************************************************************************/

procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
);

procedure verwerking_mail_nieuw_onde;

end BEH_MAIL_NOTIFICATIE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_MAIL_NOTIFICATIE_00" AS
/******************************************************************************
   NAME:       BEH_MAIL_NOTIFICATIE_00
   PURPOSE: Deze package wordt gebruikt om notitificatie mails te versturen. Dit is nu gemaakt voor RFD onderzoeksgroep. Indien er een onderzoek voor RFD onderzoeksgroep in Helix aangemaakt wordt, dan moet er notificatie mail gestuurd worden naar deze groep.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2016      M.Golebiowska       1. Created this package.
******************************************************************************/

procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) is

v_bety_id beh_bericht_types.bety_id%type;
v_datum_verzonden date := sysdate;
v_oracle_uid kgc_medewerkers.oracle_uid%type;

begin

  select bety_id into v_bety_id
  from beh_bericht_types
  where code = 'NIEUW_ONDE'
  and upper(vervallen) = upper('N')
  ;

  select sys_context('USERENV', 'SESSION_USER')
  into v_oracle_uid
  from dual
  ;

  insert into beh_berichten
  (  bety_id,
  verzonden,
  datum_verzonden,
  oracle_uid,
  ontvanger_id_intern,
  ontvanger_id_extern,
  ontvanger_type,
  adres_verzonden,
  log_tekst,
  entiteit,
  entiteit_id,
  bericht_tekst
  )
  values
  (v_bety_id,
  p_verzonden,
  v_datum_verzonden,
  v_oracle_uid,
  p_ontvanger_id_intern,
  p_ontvanger_id_extern,
  p_ontvanger_type,
  p_adres_verzonden,
  p_log_tekst,
  'ONDE',
  p_entiteit_id,
  p_bericht_tekst
  )
  ;

commit;

end verwerk_log;

function verstuur_email
(
p_afzender  varchar,
p_ontvanger varchar,
p_cc        varchar,
p_bcc       varchar,
p_titel     varchar,
p_boodschap varchar
)
return varchar is

begin
  UTL_MAIL.SEND(  sender   => p_afzender,
                recipients => p_ontvanger,
                cc         => p_cc,
                bcc        => p_bcc,
                subject    => p_titel,
                message    => p_boodschap);
  return 'OK';
exception
 when others then
    return 'NOK';
end verstuur_email;

procedure verwerking_mail_nieuw_onde is

v_sender varchar2(500) := 'gen@umcn.nl';
v_recipients varchar2(500);
v_cc varchar2(500) := ''; -- komma gescheiden
v_bcc varchar2(500) :=''; -- komma gescheiden
v_subject varchar2(1000);
v_message varchar2(4000);
v_newline varchar2(10) := chr(10);
v_db_name varchar2(100);
v_verzonden varchar2(10)   := null;


CURSOR onde_cur
IS
Select ONDE.ONDE_ID,
        ONDE.ONDERZOEKNR,
        ATWA.WAARDE as RECIPIENTS
from kgc_onderzoeken onde, kgc_onderzoeksgroepen ongr, kgc_attributen attr, kgc_attribuut_waarden atwa
where ONDE.ONGR_ID = ONGR.ONGR_ID
    and ONGR.ONGR_ID = ATWA.ID
    and ATWA.ATTR_ID = ATTR.ATTR_ID
    and ATTR.CODE = 'NOTIFICATIE_MAIL'
    and ONDE.CREATION_DATE > sysdate - 1
    and ONDE.ONDE_ID not in (select beri.entiteit_id
                                            from beh_berichten beri, beh_bericht_types bety
                                            where BERI.ENTITEIT = 'ONDE'
                                                and BERI.BETY_ID = BETY.BETY_ID
                                                and BETY.CODE = 'NIEUW_ONDE')
;

onde_rec onde_cur%ROWTYPE;

BEGIN

    SELECT global_name INTO v_db_name
    FROM GLOBAL_NAME;

    OPEN onde_cur;
    FETCH onde_cur INTO onde_rec;
    CLOSE onde_cur;

    FOR onde_rec  IN  onde_cur
    LOOP
        IF onde_rec.onderzoeknr is not null AND v_db_name NOT LIKE '%PROD%'
        THEN v_subject := 'DIT IS EEN TEST! Onderzoek met nummer: ' || onde_rec.onderzoeknr ||' is in Helix aangemaakt';
                 v_recipients := onde_rec.RECIPIENTS;
        ELSE
            IF onde_rec.onderzoeknr is not null AND v_db_name LIKE '%PROD%'
            THEN v_subject := 'Onderzoek met nummer: ' || onde_rec.onderzoeknr ||' is in Helix aangemaakt';
                     v_recipients := onde_rec.RECIPIENTS;
            END IF;
        END IF;

        v_message := 'Geachte heer/mevrouw,'
            || v_newline
            || v_newline
            || 'Onderzoek met nummer: ' || onde_rec.onderzoeknr ||' is in Helix aangemaakt.'
            || v_newline
            || v_newline
            || 'Indien u vragen heeft, dan kunt u altijd contact met ons opnemen.'
            || v_newline
            || v_newline
            || 'met vriendelijke groeten,'
            || v_newline
            || 'Secretariaat Genetica'
            || v_newline
            || 'tel: 024-36 13799'
            ;

        UTL_MAIL.SEND(  sender => v_sender,
                        recipients => v_recipients,
                        cc => v_cc,
                        bcc => v_bcc,
                        subject => v_subject,
                        message => v_message);

        v_verzonden := 'J' ;


        verwerk_log(  v_verzonden
                            , NULL
                            , NULL
                            , 'NULL'
                            , v_recipients
                            , v_subject
                            , onde_rec.onde_id
                            , v_message
                         );
     COMMIT;
     END LOOP;

END verwerking_mail_nieuw_onde;


END BEH_MAIL_NOTIFICATIE_00;
/

/
QUIT
