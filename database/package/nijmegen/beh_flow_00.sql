CREATE OR REPLACE PACKAGE "HELIX"."BEH_FLOW_00" as
--===================================
procedure insert_beh_flow_onde_data
(
p_onde_id in number := null,
p_flow_id in number := null,
x_message out varchar2
);
function insert_beh_flow_onde_data
(
p_onde_id in number := null,
p_flow_id in number := null
) return varchar;
procedure insert_flod_via_job
;
end beh_flow_00;

/
create or replace PACKAGE BODY       "BEH_FLOW_00" as
-- als alles opnieuw berekend moet worden. kun je direct deze procedure
-- aanroepen
procedure insert_beh_flow_onde_data
( p_onde_id in number  := null
, p_flow_id in number := null
, x_message out varchar2
)
is

cursor c_paden is
-- MG 2017-07-26 onderstaande statement gewijzigd nav probleem dat de onderzoeken heel snel in 'rood' kwamen
-- na enige speurwerk denk ik dat de dlt_herijkt_dagen_cum moet de som zijn van de dagen van de stappen die na de desbetreffende stap komen, dus exclusief de desbetreffende stap zelf
--select  onde_id
--,       flow_id
--,       flde_id_voorg_stap
--,       max(dlt_herijkt_dagen) dlt_herijkt_dagen
--,       max(dlt_herijkt_dagen_cum) dlt_herijkt_dagen_cum
--,       max(geplande_einddatum_check) param_01_check
--from beh_flow_paden_dlt_vw
--group by onde_id, flow_id, flde_id_voorg_stap
--;
select  pade.onde_id
,       pade.flow_id
,       pade.flde_id_voorg_stap
,       max(pade.dlt_herijkt_dagen) dlt_herijkt_dagen
,       max(nvl(pade1.dlt_herijkt_dagen_cum, 0)) dlt_herijkt_dagen_cum
,       max(pade.geplande_einddatum_check) param_01_check
from beh_flow_paden_dlt_vw pade, beh_flow_paden_dlt_vw pade1
where pade.PADNR_PLUS_FLDE_ID_VOLG_STAP = pade1.PADNR_PLUS_FLDE_ID_VOORG_STAP (+) -- join om dlt_herijkt_dagen_cum van volgende stap te achterhalen
group by pade.onde_id, pade.flow_id, pade.flde_id_voorg_stap
;

cursor c_flows is
select *
from beh_flows
where vervallen = 'N'
;
/*
cursor c_onde is
select *
from kgc_onderzoeken
where (
        (afgerond = 'N') or
        (afgerond = 'J' and datum_autorisatie > (add_months(sysdate, -2))) -- sysdate min 2 maanden
      )
order by onde_id
;
*/
cursor c_onde is
select onde.onde_id, flko.flow_id
from kgc_onderzoeken onde, beh_flow_kafd_ongr flko
where onde.kafd_id = flko.kafd_id
and   onde.ongr_id = flko.ongr_id
and   (
        (onde.afgerond = 'N') or
        (onde.afgerond = 'J' and onde.datum_autorisatie > (add_months(sysdate, -2))) -- sysdate min 2 maanden
      )
and flko.vervallen = 'N'
order by onde.onde_id
;

function set_context (p_entiteit_id number, p_attribuut varchar2)
return boolean is
v_return number;
begin
    select beh_hefu_00.set_context_value('BEH_FLOW_CONTEXT', upper(p_attribuut), p_entiteit_id) into v_return
    from dual;

    if v_return = 1 then
        return true;
    else
        return false;
    end if;
end;
procedure insert_data is
begin
for r_paden in c_paden loop
    insert into beh_flow_onde_data
    (
        onde_id
     ,  flow_id
     ,  flde_id
     ,  dlt_herijkt_dagen
     ,  dlt_herijkt_dagen_cum
     ,  param_01_check
    )
    values
    (
        r_paden.onde_id
     ,  r_paden.flow_id
     ,  r_paden.flde_id_voorg_stap
     ,  r_paden.dlt_herijkt_dagen
     ,  r_paden.dlt_herijkt_dagen_cum
     ,  r_paden.param_01_check
     );
end loop;
commit;
end;
procedure delete_data (p_onde_id number, p_flow_id number) is
begin
    delete
    from beh_flow_onde_data
    where onde_id = p_onde_id
    and   flow_id = p_flow_id;
    commit;
end;

begin

if p_onde_id is not null then
    if (set_context(p_onde_id, 'ONDE_ID')) then
        if p_flow_id is not null then
            if (set_context(p_flow_id, 'FLOW_ID')) then
                delete_data(p_onde_id, p_flow_id);
                insert_data;
            else
                x_message := 'Instellen van BEH_FLOW_CONTEXT, attribuut: FLOW_ID is mislukt.';
            end if;
        else
           for r_flows in c_flows loop
                if (set_context(r_flows.flow_id, 'FLOW_ID')) then
                    delete_data(p_onde_id, r_flows.flow_id);
                    insert_data;
                else
                    x_message := 'Instellen van BEH_FLOW_CONTEXT, attribuut: FLOW_ID is mislukt.';
                end if;
           end loop;
        end if;
    else
       x_message := 'Instellen van BEH_FLOW_CONTEXT, attribuut: ONDE_ID is mislukt.';
    end if;
else
    for r_onde in c_onde loop
        if (set_context(r_onde.onde_id, 'ONDE_ID')) then
            if p_flow_id is not null then
                if (set_context(p_flow_id, 'FLOW_ID')) then
                    delete_data(r_onde.onde_id, p_flow_id);
                    insert_data;
                else
                    x_message := 'Instellen van BEH_FLOW_CONTEXT, attribuut: FLOW_ID is mislukt.';
                end if;
            else
               for r_flows in c_flows loop
                    if (set_context(r_flows.flow_id, 'FLOW_ID')) then
                        delete_data(r_onde.onde_id, r_flows.flow_id);
                        insert_data;
                    else
                        x_message := 'Instellen van BEH_FLOW_CONTEXT, attribuut: FLOW_ID is mislukt.';
                    end if;
               end loop;
            end if;
        else
            x_message := 'Instellen van BEH_FLOW_CONTEXT, attribuut: ONDE_ID is mislukt.';
        end if;
    end loop;

end if;
exception
when others then
    rollback;
    x_message := substr(sqlerrm, 1, 2500);
    dbms_output.put_line(x_message);
end insert_beh_flow_onde_data;
function insert_beh_flow_onde_data
(
p_onde_id in number := null,
p_flow_id in number := null
) return varchar
is
v_message varchar2(2500);
PRAGMA AUTONOMOUS_TRANSACTION;

begin
beh_flow_00.insert_beh_flow_onde_data
            ( p_onde_id => p_onde_id
            , p_flow_id => p_flow_id
            , x_message => v_message
            );
return v_message;
exception
 when others then
    v_message := substr(sqlerrm, 1, 2500);
    return v_message;
end insert_beh_flow_onde_data;
procedure insert_flod_via_job is

v_peildatum date;

v_message varchar2(2500);
v_db_naam varchar(100) := sys_context('USERENV', 'DB_NAME');
--v_job_naam varchar(100) := 'beh_flow_00.insert_flod_via_job'; -- MGOL, vervangen door onderstaand lijn nav overzetten jobs naar scheduled jobs
v_job_naam varchar(100) := 'FLOW_FLOW_ONDE_DATA';

v_sender varchar(100) := 'h.bourchi@sb.umcn.nl';
v_recipients varchar(2500) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
v_subject varchar(100) := v_db_naam || ';' || v_job_naam;
--v_boodschap varchar(2500) := 'job beh_flow_00.insert_flod_via_job niet gevonden!'; -- MGOL, vervangen door onderstaand lijn nav overzetten jobs naar scheduled jobs
v_boodschap varchar(2500) := 'FLOW_FLOW_ONDE_DATA niet gevonden';
/*
cursor c_jobs is
select max(log_date) as log_date
from DBA_SCHEDULER_JOB_LOG
where instr(UPPER(job_name), UPPER(v_job_naam)) > 0
AND STATUS = 'SUCCEEDED'
;
*/
/*
  1 - job draait iedere 5 minuten en selecteerd onderzoeken die in afgelopen 
  5 minuten geautoriseerd zijn.
  2 - iedere uur wordt wordt 1 keer geautoriseerde onderzoeken in afgelopen uur geselecteerd
  3 - 1 keer per dag worden alle onderzoeken die in de hele dag gautoriseerd zijn geselecteerd.
*/
cursor c_jobs is
select log_minuut, next_log_minuut, log_uur, next_log_uur, next_log_dag, 
case 
  when next_log_minuut >= next_log_uur then 
    case 
      when next_log_minuut >= next_log_dag then
        log_dag
      else 
        log_uur  
      end
  else 
   log_minuut
  end log_date
from
(
select
to_date(to_char(max(log_date), 'dd-mm-yyyy HH24:MI:SS'),'dd-mm-yyyy HH24:MI:SS') log_minuut,-- de laatste run minuut
to_date(to_char(max(log_date), 'dd-mm-yyyy HH24:MI:SS'),'dd-mm-yyyy HH24:MI:SS')+((1/12)/24) next_log_minuut, --log_minuut + 5 minuten
to_date(to_char(max(log_date), 'dd-mm-yyyy HH24'),'dd-mm-yyyy HH24') log_uur, -- de laatste run uur
to_date(to_char(max(log_date), 'dd-mm-yyyy HH24'),'dd-mm-yyyy HH24')+(1/24) next_log_uur,
to_date(to_char(max(log_date), 'dd-mm-yyyy'),'dd-mm-yyyy') log_dag, -- de laatste run dag
to_date(to_char(max(log_date), 'dd-mm-yyyy'),'dd-mm-yyyy')+1 next_log_dag 
from DBA_SCHEDULER_JOB_LOG
where instr(UPPER(job_name), UPPER(v_job_naam)) > 0
AND STATUS = 'SUCCEEDED'
)
;
r_jobs c_jobs%rowtype;

cursor c_onde (p_peildatum date) is
select onde.onde_id, flko.flow_id
from kgc_onderzoeken onde, beh_flow_kafd_ongr flko
where onde.kafd_id = flko.kafd_id
and   onde.ongr_id = flko.ongr_id
and   (
        (onde.afgerond = 'N') or
        (onde.afgerond = 'J' and onde.datum_autorisatie > (add_months(sysdate, -2))) -- sysdate min 2 maanden
      )
and flko.vervallen = 'N'
and onde.last_update_date >= p_peildatum
order by onde.onde_id
;

begin

open c_jobs;
fetch c_jobs into r_jobs;
if c_jobs%notfound then -- niet aanwezig? kan niet - stop!
    close c_jobs;

    UTL_MAIL.SEND(  sender => v_sender,
                    recipients => v_recipients,
                    subject => v_subject,
                    message => v_boodschap);
    return;

end if;
close c_jobs;

v_peildatum := nvl(r_jobs.log_date, sysdate);
--v_peildatum := (v_peildatum - 10/(24*60*60) ); -- 10 seconden overlap tijd voor zekerheid!

for r_onde in c_onde(v_peildatum) loop
    beh_flow_00.insert_beh_flow_onde_data
                ( p_onde_id => r_onde.onde_id
                , p_flow_id => r_onde.flow_id
                , x_message => v_message
                );

end loop;
exception
 when others then
    rollback;
    v_message := v_message || ' ' || substr(sqlerrm, 1, 2500);
    dbms_output.put_line(v_message);
end insert_flod_via_job;
end beh_flow_00;
/
