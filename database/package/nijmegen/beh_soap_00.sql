CREATE OR REPLACE PACKAGE "HELIX"."BEH_SOAP_00"
AS
procedure add_parameter (
  p_paramList     in out beh_SOAP_ParamList,
  p_name          in VarChar2,
  p_value         in VarChar2
);

function call_soap_service
(
p_service_name in varchar2,
p_paramlist in beh_SOAP_ParamList
) return varchar
;
END beh_soap_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_SOAP_00"
AS
Procedure add_parameter (
  p_paramList in out beh_SOAP_ParamList,
  p_name      in VarChar2,
  p_value     in VarChar2
)
is
  n_last_element number;
  n_counter number;
  b_added boolean := false;
begin
  if p_paramList.count != 0 then
    for n_counter in 1 .. p_paramList.count -- loop trough parameterlist
    loop
      -- compare saved with secified name
      if p_ParamList(n_counter).name = p_name then
          p_paramList(n_counter).value := p_value;
          b_added := true;
      end if;
    end loop;
  end if;
  if not b_added then
    p_paramlist.extend;
    n_last_element := p_paramList.last;           -- add new parameter

    p_paramList(n_last_element) := beh_SOAP_Parameter(p_name,p_value);

  end if;
end add_parameter;
function call_soap_service
(
p_service_name in varchar2,
p_paramlist in beh_SOAP_ParamList
)
return varchar is

  v_soap_url varchar2(1000) :=
    kgc_sypa_00.systeem_waarde ('BEH_HELIX_SOAP_URL');

  l_request   soap_api.t_request;
  l_response  soap_api.t_response;
  l_return    VARCHAR2(32767);

  l_url          VARCHAR2(32767) :=
    v_soap_url || p_service_name ||'/' || p_service_name || '.php';
  l_namespace    VARCHAR2(32767) :=
     'xmlns="'|| v_soap_url || p_service_name || '/' ||p_service_name || '.wsdl' ||'"';
  l_method       VARCHAR2(32767) := p_service_name || '_operation';
  l_soap_action  VARCHAR2(32767) := l_url;
  l_result_name  VARCHAR2(32767) := p_service_name || '_response_part';

BEGIN
--********************begin***************
-- weghalen na oplossen openoffice probleem
/*
if p_service_name = 'verwerkbijlagen' and user <> 'H.BOURCHI' then
  v_soap_url := 'http://umcukz15.umcn.nl/soap/';
end if;
  l_url := v_soap_url || p_service_name ||'/' || p_service_name || '.php';
  l_namespace :=
     'xmlns="'|| v_soap_url || p_service_name || '/' ||p_service_name || '.wsdl' ||'"';
  l_soap_action := l_url;
  */
--*************************end**********
  l_request := soap_api.new_request(p_method       => l_method,
                                    p_namespace    => null);

  for i in 1..p_paramList.count loop
        if p_paramList(i).name is not null then
            soap_api.add_parameter(p_request => l_request,
                         p_name => p_paramList(i).name,
                         p_type   => 'xsd:string',
                         p_value  => p_paramList(i).value
                         );
        end if;
  end loop;

  l_response := soap_api.invoke(p_request => l_request,
                                p_url     => l_url,
                                p_action  => l_soap_action);

  l_return := soap_api.get_return_value(p_response  => l_response,
                                        p_name      => l_result_name,
                                        p_namespace => NULL);
  return l_return;

END call_soap_service;
end beh_soap_00;
/

/
QUIT
