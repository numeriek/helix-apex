CREATE OR REPLACE PACKAGE HELIX.BEH_DIGIDOS_BACKUP IS

  --##### PUBLIC VOOR TEST DOELEINDEN, MOET PRIVATE WORDEN #####--
  --FUNCTION backup_files( p_changed_within_minutes IN number 
  --                     , p_path_werkbestanden     IN varchar2
  --                     , p_path_backup            IN varchar2
  --                     , p_subpath_backup         IN varchar2
  --                     , p_verbose_log            IN varchar2
  --                     ) RETURN number;

  --##### PUBLIC VOOR TEST DOELEINDEN, MOET PRIVATE WORDEN #####--
  --function maak_backup_subpath( p_start_path  IN varchar2
  --                            , p_full_path   IN varchar2
  --                            ) return varchar2;

  --##### PUBLIC VOOR TEST DOELEINDEN, MOET PRIVATE WORDEN #####--
  --function maak_path_voor_vorige_periode( p_path             IN varchar2
  --                                      , p_folder_structuur IN varchar2  --Q:Kwartaal / Y:Jaar
  --                                      ) return varchar2;

  function normaliseer_path(p_path IN varchar2) return varchar2;

  procedure maak_backup( p_changed_within_minutes IN number 
                       , p_path_werkbestanden     IN varchar2
                       , p_path_backup            IN varchar2
                       );
                       
  procedure maak_backup_btyp( p_changed_within_minutes  IN number 
                            , p_path_btyp_werkbestanden IN varchar2
                            , p_path_backup             IN varchar2
                            );

  procedure log(p_text in varchar2);
  
END BEH_DIGIDOS_BACKUP;
/

CREATE OR REPLACE PACKAGE BODY HELIX.BEH_DIGIDOS_BACKUP IS

  v_crlf                  CONSTANT varchar2(2) := chr(13)||chr(10);
  v_html_message          clob;

  ------------------------------------------------------------------------------
  -- Voeg regel tekst toe aan de logging tekst die uiteindelijk verstuurd wordt.
  -- Deze procedure wordt ook vanuit de Java class backupFiles aangeroepen.
  procedure log(p_text in varchar2) is
    v_text varchar2(32000);
  begin
    -- dbms_output.put_line(p_text);
    v_text := p_text || v_crlf;
    v_html_message := v_html_message || v_text;
  end;


  ------------------------------------------------------------------------------
  -- Java class die de eigelijke backup uitvoert.
  --
  FUNCTION backup_files( p_changed_within_minutes IN number 
                       , p_path_werkbestanden     IN varchar2
                       , p_path_backup            IN varchar2
                       , p_subpath_backup         IN varchar2
                       , p_verbose_log            IN varchar2
                       ) RETURN number IS
  LANGUAGE JAVA NAME 'BEH_Backup.backupFiles(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String) return int';


  ------------------------------------------------------------------------------
  -- Maak het backup subpath aan. Dit is het deel van het volledige path wat
  -- onder het start path valt.
  function maak_backup_subpath( p_start_path  IN varchar2
                              , p_full_path   IN varchar2
                              ) return varchar2 is
    v_lengte number;
    
  begin
    v_lengte := length(p_start_path);
    
    if (lower(substr(p_full_path, 1, v_lengte)) = lower(p_start_path)) then
      return substr(p_full_path, v_lengte+1);
    end if;
    
    return NULL;
  end;
  

  ------------------------------------------------------------------------------
  -- Reformat een path zodat alle slashes enkele backslashes zijn en het path 
  -- eindigd met een backslash.
  function normaliseer_path(p_path IN varchar2) return varchar2 is
    v_nieuw_path varchar2(512);
  begin
    v_nieuw_path := translate(lower(p_path), '/' , '\');
    v_nieuw_path := regexp_replace(v_nieuw_path, '\\+' , '\\', 2);
    
    v_nieuw_path := replace(v_nieuw_path, '<creationyear>', to_char(sysdate, 'YYYY'));
     
    if (substr(v_nieuw_path, length(v_nieuw_path), 1) <> '\') then
      v_nieuw_path := v_nieuw_path || '\';
    end if;
    
    return v_nieuw_path;
  end;
  
  
  ------------------------------------------------------------------------------
  -- Reformat een path met een gegeven folder structuur naar het path van de
  -- voorliggende periode.
  function maak_path_voor_vorige_periode( p_path             IN varchar2
                                        , p_folder_structuur IN varchar2  --Q:Kwartaal / Y:Jaar
                                        ) return varchar2 is
                                        
    v_nieuw_path varchar2(512);
    v_jaar       number;
    v_kwartaal   number;
    v_lengte     number;
    
  begin
  
    v_lengte := length(p_path);

    case p_folder_structuur
      when 'Q' then
        begin
          v_kwartaal := to_number(substr(p_path, v_lengte-1, 1));
          v_jaar     := to_number(substr(p_path, v_lengte-7, 4));
          
          v_kwartaal := v_kwartaal - 1;
          
          if (v_kwartaal=0) then
            v_kwartaal := 4;
            v_jaar     := v_jaar - 1;
          end if;
          
          v_nieuw_path := substr(p_path, 1, v_lengte-8) || trim(to_char(v_jaar, '0999')) || '\q' || to_char(v_kwartaal) || '\';
        exception when others then
          v_nieuw_path := p_path;
        end;
        
      when 'Y' then
        begin
          v_jaar := to_number(substr(p_path, v_lengte-4, 4));
          v_jaar := v_jaar - 1;
          
          v_nieuw_path := substr(p_path, 1, v_lengte-5) || trim(to_char(v_jaar, '0999')) || '\';
        exception when others then
          v_nieuw_path := p_path;
        end;
        
    end case;

    return v_nieuw_path;
  end;


  ------------------------------------------------------------------------------
  -- Maak een backup van de directory <p_path_werkbestanden> naar <p_path_backup>.
  -- Er wordt alleen gekeken naar bestanden die minder dan <p_changed_within_minutes>
  -- aangepast zijn.
  -- De bestanden moeten voorkomen in Helix tabel beh_bestanden.
  --
  procedure maak_backup( p_changed_within_minutes IN number 
                       , p_path_werkbestanden     IN varchar2
                       , p_path_backup            IN varchar2
                       ) is
  
    v_info_mail_sypa    kgc_systeem_parameters.code%type := 'DD_BCK_INFO_MAIL';

    v_return_val      number;
    v_procedure_naam  varchar2(45) := 'beh_digidos_backup.maak_backup'; 
    v_info_email      kgc_systeem_parameters.standaard_waarde%type;
    v_email_cc        varchar2(100) := 'rene.brand@radboudumc.nl';
    v_email_onderwerp varchar2(100);
    v_email_aan       varchar2(100);
    v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM' 
                                                                        , p_mede_id        => NULL
                                                                        ) , 'HelpdeskHelixCUKZ@cukz.umcn.nl' );

  begin
    v_email_onderwerp := 'Backup digitaal dossier';
    v_email_aan       := beh_interfaces.beh_get_user_email;
    v_html_message    := '<h3>Overzicht van gekopieerde bestanden tijdens standaard digitaal dossier backup.</h3></br>' || v_crlf;

    --Haal het info email adres op, of null als de systeem parameter niet bestaat.
    begin
      v_info_email := kgc_sypa_00.systeem_waarde(v_info_mail_sypa);
    exception when others then
      v_info_email := null;
    end;

    v_return_val      := beh_digidos_backup.backup_files( p_changed_within_minutes
                                                        , normaliseer_path(p_path_werkbestanden)
                                                        , normaliseer_path(p_path_backup)  
                                                        , null
                                                        , 'J'
                                                        );
    
    log('Uitgevoerd door: ' || user || '</br>' || v_crlf);
    log('Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>');

    log('</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);

    -- Als info_email sypa gedefinieerd is, altijd mail ter info.
    if v_info_email is not null then
      send_mail_html( p_from    => v_email_afzender
                    , p_to      => v_info_email
                    , p_subject => v_email_onderwerp
                    , p_html    => v_html_message
                    );
    end if;

  exception when others then
    -- Exception mail in geval van grote fout naar STG mailbox.
    v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
    log('</br><font color="red">FOUT:' || sqlerrm || '</br>'
                                       || dbms_utility.format_error_backtrace || '</br>'
                                       || dbms_utility.format_call_stack
                                       ||'</font></br></br>');
                                                                          
    log('</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);
    
    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan
                  , p_cc      => v_email_cc
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );
    raise;
    
  end;


  ------------------------------------------------------------------------------
  -- Maak een backup van de directories die voorlomen in tabel kgc_bestand_types
  -- en die beginnen met <p_path_btyp_werkbestanden>. De backup wordt geschreven
  -- naar <p_path_backup>.
  -- Voor directories die per kwartaal of jaar zijn wordt er ook een bepaalde tijd
  -- terug gekeken. Alleen bestanden die minder dan <p_changed_within_minutes>
  -- minuten geleden aangepast zijn worden mee genomen in het process.
  -- De bestanden moeten voorkomen in Helix tabel beh_bestanden.
  --
  procedure maak_backup_btyp( p_changed_within_minutes  IN number 
                            , p_path_btyp_werkbestanden IN varchar2
                            , p_path_backup             IN varchar2
                            ) is
  
    cursor c_btyp_paden(b_path_btyp_werkbestanden in varchar2) is 
      select distinct beh_digidos_backup.normaliseer_path(btyp.standaard_pad) as path
           , case when REGEXP_LIKE(translate(lower(btyp.standaard_pad), '/' , '\'), '\\20[12][0123456789]\\q[1234]\\?$') then 'Q' --Pad eindigt op \yyyy\qn
                  when REGEXP_LIKE(translate(lower(btyp.standaard_pad), '/' , '\'), '\\20[12][0123456789]\\?$') then 'Y'          --Pad eindigt op \yyyy 
                  when REGEXP_LIKE(translate(lower(btyp.standaard_pad), '/' , '\'), '<creationyear>') then 'Y'                    --<creationyear> wordt vertaald naar het huidig jaar door normaliseer!            
                  else 'N'
             end as folder_structuur
      from   kgc_bestand_types btyp
      where  btyp.vervallen = 'N'
        and  lower(btyp.standaard_pad) like lower(b_path_btyp_werkbestanden)||'%'     --\\umcms33\kgcn_dossiershelix$\def\  of '\\umcms005\gen_stg$\helix\'
      order by 1;

    v_info_mail_sypa    kgc_systeem_parameters.code%type := 'DD_BCK_INFO_MAIL';

    v_subpath_backup   varchar2(512);
    v_path_werkbestand varchar2(512);
    v_path_btyp_werkbestanden varchar2(512);
    v_loop             number;
    
    v_return_val      number;
    v_procedure_naam  varchar2(45) := 'beh_digidos_backup.maak_backup_btyp'; 
    v_info_email      kgc_systeem_parameters.standaard_waarde%type;
    v_email_cc        varchar2(100) := 'rene.brand@radboudumc.nl';
    v_email_onderwerp varchar2(100);
    v_email_aan       varchar2(100);
    v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM' 
                                                                        , p_mede_id        => NULL
                                                                        ) , 'HelpdeskHelixCUKZ@cukz.umcn.nl' );

  begin
    v_email_onderwerp := 'Backup digitaal dossier';
    v_email_aan       := beh_interfaces.beh_get_user_email;
    v_html_message    := '<h3>Overzicht van gekopieerde bestanden tijdens digitaal dossier backup via bestand types.</h3></br>' || v_crlf;

    v_path_btyp_werkbestanden := normaliseer_path(p_path_btyp_werkbestanden);

    --Haal het info email adres op, of null als de systeem parameter niet bestaat.
    begin
      v_info_email := kgc_sypa_00.systeem_waarde(v_info_mail_sypa);
    exception when others then
      v_info_email := null;
    end;

    -- Loop over alle matchende paden uit de cursor.
    for r_btyp_paden in c_btyp_paden(v_path_btyp_werkbestanden) loop
    
      v_path_werkbestand := normaliseer_path(r_btyp_paden.path);
      
      case r_btyp_paden.folder_structuur
        when 'Q' then --Per kwartaal. Verwerk het huidige kwartaal en een 3 vorige kwartalen.
          log('<b>Backup kwartalen structuur voor: ' || v_path_werkbestand || '</b></br></br>');
          for v_loop in 1..4 loop -- Aantal perioden in kwartalen (4)
            v_subpath_backup := maak_backup_subpath(v_path_btyp_werkbestanden, v_path_werkbestand);
            v_return_val     := beh_digidos_backup.backup_files( p_changed_within_minutes
                                                               , v_path_werkbestand
                                                               , normaliseer_path(p_path_backup)
                                                               , v_subpath_backup
                                                               , 'N'
                                                               );
            v_path_werkbestand := maak_path_voor_vorige_periode(v_path_werkbestand, 'Q');
          end loop;

        when 'Y' then --Per jaar. Verwerkt het huidige jaar en het vorige jaar.
          log('<b>Backup jaren structuur voor: ' || v_path_werkbestand || '</b></br></br>');
          for v_loop in 1..2 loop -- Aantal perioden in jaren (2)
            v_subpath_backup := maak_backup_subpath(v_path_btyp_werkbestanden, v_path_werkbestand);
            v_return_val     := beh_digidos_backup.backup_files( p_changed_within_minutes
                                                               , v_path_werkbestand
                                                               , normaliseer_path(p_path_backup)
                                                               , v_subpath_backup
                                                               , 'N'
                                                               );
            v_path_werkbestand := maak_path_voor_vorige_periode(v_path_werkbestand, 'Y');
          end loop;

        else -- Geen structuur (kijk geen periode terug)
          log('<b>Standaard backup voor: ' || v_path_werkbestand || '</b></br></br>');
          v_subpath_backup   := maak_backup_subpath(v_path_btyp_werkbestanden, v_path_werkbestand);
          v_return_val       := beh_digidos_backup.backup_files( p_changed_within_minutes
                                                               , v_path_werkbestand
                                                               , normaliseer_path(p_path_backup)
                                                               , v_subpath_backup
                                                               , 'N'
                                                               );
      end case;
    end loop;
    
    log('Uitgevoerd door: ' || user || '</br>' || v_crlf);
    log('Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>');

    log('</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);

    -- Als info_email sypa gedefinieerd is, altijd mail ter info.
    if v_info_email is not null then
      send_mail_html( p_from    => v_email_afzender
                    , p_to      => v_info_email
                    , p_subject => v_email_onderwerp
                    , p_html    => v_html_message
                    );
    end if;

  exception when others then
    -- Exception mail in geval van grote fout naar STG mailbox.
    v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
    log('</br><font color="red">FOUT:' || sqlerrm || '</br>'
                                       || dbms_utility.format_error_backtrace || '</br>'
                                       || dbms_utility.format_call_stack
                                       ||'</font></br></br>');
                                                                          
    log('</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);
    
    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan
                  , p_cc      => v_email_cc
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );
    raise;
    
  end;

END BEH_DIGIDOS_BACKUP;
/

begin
  kgc_util_00.reconcile( p_object => 'BEH_DIGIDOS_BACKUP' );
end;
/
