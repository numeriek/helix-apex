CREATE OR REPLACE PACKAGE "HELIX"."BEH_EPD_00" as

procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) ;



procedure verwerking_alert_uitslag;
function insert_best
(
p_kafd_id in number,
p_ongr_id in number,
p_uits_id in number,
p_module  in varchar,
p_zisnr in varchar
)
return varchar;
function samenstellen_bestandsnaam
(
p_kafd_id in number,
p_ongr_id in number,
p_zisnr in varchar,
p_module in varchar
)
return varchar;

procedure raadplegen
(      p_entiteit_id             IN NUMBER   := NULL
     , p_entiteit                IN varchar2 := NULL
     , p_datum                   IN date     := NULL
     , p_applicatie_vanuit       IN varchar2 := NULL
     , p_programma_waarmee       IN varchar2 := NULL
     , p_viewer_type             IN varchar2 := NULL
     , p_viewer_interne_id       IN NUMBER   := NULL
     , p_viewer_externe_id       IN varchar2 := NULL
     , p_viewer_extra_info       IN varchar2 := NULL
);
-- loggen van het bekijken/raadplegen van gegevens bv uitslagen via het epd
--
-- tevens  in voeren bij notities van het betreffende onderzoek
function kafd_epd_tonen
(
p_ongr_id number
)
return varchar2;
FUNCTION ongr_epd_tonen
 (
 p_ongr_id number,
 p_onde_id number
 ) return varchar2;

function onderzoek_EPD_afgerond
(
P_onde_id number
)
return varchar;
FUNCTION zoek_uitslag
(
p_onde_id number,
p_hoev number
) return varchar;

FUNCTION zoek_uitslag_ramo_code
(
p_uits_id kgc_uitslagen.uits_id%type
) return varchar;

FUNCTION zoek_uitslag_datum
(
p_onde_id number,
p_hoev number
) return varchar;
FUNCTION zoek_uitslag_bestand
(
p_uits_id number,
p_rela_id number
) return varchar;
FUNCTION buiten_cons
(
p_onde_id number
) return varchar;

FUNCTION INGR_CODE
(
p_onde_id number
) return varchar;
end BEH_EPD_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_EPD_00" as
--mantis 11259 wijz 24-04-2015 cv ER MOET GEEN geen EmbArgo termmijnGELDEN voor kopie houder die bij klingen ALHIER werkt
-- ManTIS 12116 OPSLAG ONDERZOEKEN KRIJGEN EEN ANDERE ALERT EMAIL TEKST


procedure verwerk_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) is

v_bety_id beh_bericht_types.bety_id%type;
v_datum_verzonden date := sysdate;
v_oracle_uid kgc_medewerkers.oracle_uid%type;

begin

  select bety_id into v_bety_id
  from beh_bericht_types
  where code = 'EMAIL'
  and upper(vervallen) = upper('N')
  ;

  select sys_context('USERENV', 'SESSION_USER')
  into v_oracle_uid
  from dual
  ;

  insert into beh_berichten
  (  bety_id,
  verzonden,
  datum_verzonden,
  oracle_uid,
  ontvanger_id_intern,
  ontvanger_id_extern,
  ontvanger_type,
  adres_verzonden,
  log_tekst,
  entiteit,
  entiteit_id,
  bericht_tekst
  )
  values
  (v_bety_id,
  p_verzonden,
  v_datum_verzonden,
  v_oracle_uid,
  p_ontvanger_id_intern,
  p_ontvanger_id_extern,
  p_ontvanger_type,
  p_adres_verzonden,
  p_log_tekst,
  'UITS',
  p_entiteit_id,
  p_bericht_tekst
  )
  ;

commit;

end verwerk_log;

function verstuur_email
(
p_afzender  varchar,
p_ontvanger varchar,
p_cc        varchar,
p_bcc       varchar,
p_titel     varchar,
p_boodschap varchar
)
return varchar is

begin
  UTL_MAIL.SEND(  sender   => p_afzender,
                recipients => p_ontvanger,
                cc         => p_cc,
                bcc        => p_bcc,
                subject    => p_titel,
                message    => p_boodschap);
  return 'OK';
exception
 when others then
    return 'NOK';
end verstuur_email;

procedure verwerking_alert_uitslag is

  cursor c_uits is --uitslagen die onlangs zijn aangemaakt en/of ge-update
  select     uits_id,
    creation_date,
    last_update_date,
    onde_id,
    kafd_code,
    ongr_code,
    onderzoeknr,
    pers_id
  from beh_uitslag_alert_vw
    ;

  r_uits c_uits%Rowtype;

  cursor c_epvw (p_onde_id in number) is --het onderzoek wordt getoond in het EPD
    select *
    from beh_epd_vw epvw
    where  epvw.onde_id = p_onde_id
   and epvw.afgerond = 'J'
  ;

  r_epvw c_epvw%rowtype;

  cursor c_rela (p_uits_id in number) IS -- interne relaties
  select rela.rela_id
  , koho
  from beh_aanv_koho_vw aako
  , kgc_relaties rela
  where aako.uits_id = p_uits_id
  and aako.rela_id = rela.rela_id
  and instr(upper(rela.adres),  'ALHIER')>0
  and rela.liszcode  is not null
  and RELA.VERVALLEN = 'N'
  ;
  r_rela c_rela%rowtype;


  cursor c_beri (p_uits_id in number, p_rela_id in number, p_last_update_date date) is -- er is al een bericht succesvol verstuurd
  select beri.bety_id
  , beri.last_update_date
  from beh_berichten beri
  , beh_bericht_types bety
  where beri.entiteit= 'UITS'
  and beri.entiteit_id = p_uits_id
  and beri.bety_id = bety.bety_id
  and bety.code = 'EMAIL'
  and beri.verzonden = 'J'
  and beri.ontvanger_type = 'RELA'
  and beri.ontvanger_id_intern = p_rela_id
  and beri.datum_verzonden > p_last_update_date
  order by beri.last_update_date desc
  ;
  r_beri C_beri%rowtype;

 -- best_id ophalen
  cursor c_best (p_uits_id in number, p_rela_id in number) is
  select best.best_id
  from kgc_bestanden best
  , KGC_BESTAND_TYPES btyp
  where entiteit_pk = p_uits_id
  and rela_id = p_rela_id
  and best.entiteit_code = 'UITS'
  and best.btyp_id = btyp.btyp_id
  and btyp.ENTI_CODE = 'UITS'
  AND btyp.CODE <> 'BIJLAGE'
  ;

  r_best c_best%rowtype;


  v_klingen   number         := null; --mantis 11259

  V_AANTAL    NUMBER         := nULL; -- MANTIS 12116
  v_afzender  varchar2(100)  := null;
  v_ontvanger varchar2(500)  := null;
  v_boodschap varchar2(2500) := null;
  v_titel     varchar2(200)  := null;


  v_log_tekst varchar2(10)   := null;
  v_verzonden varchar2(10)   := null;

begin

  open c_uits;  -- uitslage die onlangs zijn aangemaakt/ge-update
  fetch c_uits into r_uits;
  while c_uits%found loop

  select count(*) into v_aantal -- mantis 12116
  from kgc_indicatie_teksten INDI
  , KGC_ONDERZOEK_INDICATIES ONIN
  where ONIN.ONDE_ID = R_UITS.ONDE_ID
  AND ONIN.INDI_ID = INDI.INDI_ID
  and upper(code) like upper('%OPSLAG%');


    open  c_epvw(r_uits.onde_id); -- onderzoeken die ook zichtbaar zijn in het epd
    Fetch c_epvw into r_epvw;
    IF c_epvw%FOUND THEN


      open c_rela(r_uits.uits_id);    -- voor alle interne aanvragers en kopiehouders bij de onderzoeken/uitslagen
      fetch c_rela into r_rela;
      while c_rela%found
      loop


        -- ophalen best_id
        open  c_best(r_uits.uits_id, r_rela.rela_id);
        Fetch c_best into r_best;
        if c_best%found then


          if (beh_uits_00.bepaal_mediatype(r_best.best_id) = 'I') then -- alleen interne uitslagen/bestanden

              v_afzender  := 'KGCN';
              IF V_AANTAL >0 THEN   -- MANTIS 12116
                v_titel     := kgc_sypa_00.standaard_waarde('BEH_ALERT_TITEL_BIJ_OPSLAG', r_uits.kafd_code, null,null);
                v_boodschap := kgc_sypa_00.standaard_waarde('BEH_ALERT_TEKST_BIJ_OPSLAG', r_uits.kafd_code, null,null);
              ELSE
                v_titel     := kgc_sypa_00.standaard_waarde('BEH_EPD_TITEL_BERICHT_AUTOR', r_uits.kafd_code, null,null);
                v_boodschap := kgc_sypa_00.standaard_waarde('BEH_EPD_TEKST_BERICHT_AUTOR', r_uits.kafd_code, null,null);
              END IF;
              v_titel     := replace( v_titel, '<persoonaanspreken>', beh_pers_info_email(r_uits.pers_id));
              v_ontvanger := beh_interne_relatie_email(r_rela.rela_id);
           --  v_ontvanger := 'Z763130@umcn.nl';
            -- v_ontvanger := 'Z878110@umcn.nl';
              v_boodschap:= replace( v_boodschap, '<onderzoeknummer>', r_uits.onderzoeknr);
              v_boodschap:= replace( v_boodschap, '<persoonaanspreken>', kgc_pers_00.info(r_uits.pers_id));
              v_log_tekst:= 'NOK';

              open  c_beri(r_uits.uits_id, r_rela.rela_id, r_uits.last_update_date); -- mails die al aan hen zijn gestuurd over deze uitslag
              Fetch c_beri into r_beri;

              -- er is nog geen bericht verstuurd
              --of
              -- er is wel een bericht , maar de uitslag is daarna nog aangepast
            IF ( (c_beri%notFOUND)
                          or
                          ( (c_beri%FOUND) AND (r_beri.last_update_date < r_uits.last_update_date))
                          )

            THEN
                -- als er nog embargo is dan geen mail naar kopiehouder
                -- is de kopiehouder een medewerker van klingen? dan toch email ondanks embargo --mantis 11259
              select count(*) aantal into v_klingen
                         from kgc_relaties rela
                         , kgc_afdelingen afdg
                         where rela_id = r_rela.rela_id
                         and afdg.afdg_id = rela.afdg_id
                         and afdg.code = 'KLIN_836'
                         and instr(upper(rela.adres), 'ALHIER') > 0;
              if ( (r_rela.koho = 'J')  and (r_epvw.datum_beschikbaar_vanaf> sysdate)  and  (v_klingen =  0) ) then
                        null;
              else


                -- IF v_ontvanger in('Z763130@umcn.nl','Z878110@umcn.nl','Z980118@umcn.nl', 'Z337078@umcn.nl') then
                IF v_ontvanger is not null then

                  if  (    v_afzender  is not null
                            and v_boodschap is not null
                            and v_titel     is not null)
                  then
                   -- -- --    er moeten nu drie dingen worden gedaan:
                   -- -- --    stap 1- verstuur alert e-mail
                   -- -- --    stap 2- alleen als dat gelukt is: registreer dan een brief
                   -- -- --    stap 3- verwerk de logging in de berichten tabel. Dit gebeurt altijd, ook als het versturen niet is gelukt

                   -- -- -- STAP 1 verstuur alert e-mail
                           if (sys_context('userenv','db_name') like 'PROD%' )  then
                             v_log_tekst:= verstuur_email(v_afzender
                                                   , v_ontvanger
                                                   , null
                                                   , null
                                                   , v_titel
                                                   , v_boodschap
                                                   );
                         else
                             v_log_tekst := 'OK';
                           end if;    -- omgeving is productie?

                           IF v_log_tekst = 'OK' then

                             v_verzonden := 'J' ;


                            -- -- -- STAP 2
                             -- het verzenden van email is succesvul geweest.
                             -- dus het wordt als brief geregistreerd.
                             insert into kgc_brieven
                                          ( brie_id
                                          , kafd_id
                                          , pers_id
                                          , datum_print
                                          , kopie
                                          , brty_id
                                          , onde_id
                                          , rela_id
                                          , uits_id
                                          , datum_verzonden
                                          , geadresseerde
                                          , best_id
                                          , aanmaakwijze
                                          ) values
                                          ( kgc_brie_seq.nextval
                                          , r_epvw.kafd_id
                                          , r_epvw.pers_id
                                          , sysdate
                                          , 'N'
                                          , (select brty_id from kgc_uitslagen where uits_id = r_uits.uits_id)
                                          , r_uits.onde_id
                                          , r_rela.rela_id
                                          , r_uits.uits_id
                                          , sysdate
                                          , kgc_adres_00.relatie (r_rela.rela_id, 'J')
                                          , r_best.best_id
                                          , 'EPD'
                                          );

                           ELSE
                             v_verzonden := 'N' ;
                           END IF; -- logtekst ok?
                     -- -- -- STAP 3
                           -- procedure verwerk_log
                           verwerk_log(  v_verzonden
                                          , r_rela.rela_id
                                          , NULL
                                          , 'RELA'
                                          , v_ontvanger
                                          , v_log_tekst
                                          , r_uits.uits_id
                                          , v_boodschap
                                           );


                  end if; -- afzender , boodschap en titel geveuld?

                end if; -- v_ontvanger gevuld?

              end if; --koho en embargo en medewerker klingen

            end if; --c_beri%notFOUND
            close c_beri;

          end if; --beh_uits_00.bepaal_mediatype

        end if; --c_best%found
        close c_best;


        fetch c_rela into r_rela;
      end loop;   --c_rela%found
      Close c_rela;

    end if; --c_epvw%FOUND
    close c_epvw;

    fetch c_uits into r_uits;
  end loop; -- c_uits
  close c_uits;

end verwerking_alert_uitslag;


function insert_best
(
p_kafd_id number,
p_ongr_id number,
p_uits_id number,
p_module  varchar,
p_zisnr varchar
)
return varchar is

v_kafd_id kgc_kgc_afdelingen.kafd_id%type := p_kafd_id;
v_ongr_id kgc_onderzoeksgroepen.ongr_id%type := p_ongr_id;
v_uits_id kgc_uitslagen.uits_id%type := p_uits_id;
v_module kgc_bestand_types.code%type := upper(p_module);
v_zisnr kgc_personen.zisnr%type := upper(p_zisnr);

v_bestandsnaam kgc_bestanden.bestandsnaam%type;
v_return varchar(2000);

cursor c_best is
select *
from kgc_bestand_types
where upper(code) = upper(v_module)
;

r_best c_best%rowtype;


begin
open c_best;
fetch c_best into r_best;
close c_best;

v_bestandsnaam := kgc_nust_00.genereer_nr
                          ( p_nummertype => 'BEST'
                          , p_nummerstructuur => 'B_UITSLAG'
                          , p_ongr_id => v_ongr_id
                          , p_kafd_id => v_kafd_id
                          , p_waarde3 => v_zisnr
                          );

insert into kgc_bestanden
(
entiteit_code,
entiteit_pk,
btyp_id,
locatie,
bestandsnaam,
volgorde
)
values
(
r_best.enti_code,
v_uits_id,
r_best.btyp_id,
r_best.standaard_pad,
v_bestandsnaam||'.'||r_best.standaard_extentie,
1
);
v_return := 'OK';
return v_return;

exception
when others then
  v_return := SUBSTR(SQLERRM, 1, 2000);
  return v_return;
end insert_best;

function samenstellen_bestandsnaam
(
p_kafd_id in number,
p_ongr_id in number,
p_zisnr in varchar,
p_module  varchar
) return varchar is

v_kafd_id kgc_kgc_afdelingen.kafd_id%type := p_kafd_id;
v_ongr_id kgc_onderzoeksgroepen.ongr_id%type := p_ongr_id;
v_zisnr kgc_personen.zisnr%type := upper(p_zisnr);
v_module kgc_bestand_types.code%type := upper(p_module);

v_bestandsnaam kgc_bestanden.bestandsnaam%type;
v_return varchar(2000);

cursor c_best is
select *
from kgc_bestand_types
where upper(code) = upper(v_module)
;

r_best c_best%rowtype;

begin
open c_best;
fetch c_best into r_best;
close c_best;

v_bestandsnaam := kgc_nust_00.genereer_nr
                          ( p_nummertype => 'BEST'
                          , p_nummerstructuur => 'B_UITSLAG'
                          , p_ongr_id => v_ongr_id
                          , p_kafd_id => v_kafd_id
                          , p_waarde3 => v_zisnr
                          );

v_return := r_best.standaard_pad ||
            v_bestandsnaam||
            '.'||
            r_best.standaard_extentie;

return v_return;

exception
when others then
  v_return := SUBSTR(SQLERRM, 1, 2000);
  return v_return;
end samenstellen_bestandsnaam;

PROCEDURE raadplegen
(      p_entiteit_id             IN NUMBER   := NULL
     , p_entiteit                IN varchar2 := NULL
     , p_datum                   IN date     := NULL
     , p_applicatie_vanuit       IN varchar2 := NULL
     , p_programma_waarmee       IN varchar2 := NULL
     , p_viewer_type             IN varchar2 := NULL
     , p_viewer_interne_id       IN NUMBER   := NULL
     , p_viewer_externe_id       IN varchar2 := NULL
     , p_viewer_extra_info       IN varchar2 := NULL
)
-- loggen van het bekijken/raadplegen van gegevens bv uitslagen via het epd
--
-- tevens  in voeren bij notities van het betreffende onderzoek
IS
  cursor c_aans is
   select aanspreken||decode(vervallen, 'J', ' (vervallen)', 'N', Null) aanspreken
   from kgc_relaties
--   where upper(liszcode) = upper('Z878110')
   where upper(liszcode) = upper(p_viewer_externe_id)
   ;

   r_aans c_aans%rowtype;



v_aanspreken varchar2(200);
v_datum date;

BEGIN

 select sysdate into v_datum from dual;

   INSERT INTO helix.beh_raadpleeg_log
     ( entiteit_id
     , entiteit
     , datum_bekeken
     , applicatie_vanuit
     , programma_waarmee
     , viewer_type
     , viewer_interne_id
     , viewer_externe_id
     , viewer_extra_info
     )
     values(
       p_entiteit_id
     , p_entiteit
     , v_datum
     , p_applicatie_vanuit
     , p_programma_waarmee
     , p_viewer_type
     , p_viewer_interne_id
     , p_viewer_externe_id
     , p_viewer_extra_info
     )
    ;
  COMMIT;
--toevoegen aan notities indien uitslag bekeken via epd

  v_aanspreken := null;

  OPEN c_aans;
  FETCH c_aans INTO r_aans;
  WHILE c_aans%FOUND LOOP
--dbms_output.put_line('4_0: r_aanspreken'||r_aans.aanspreken);
    IF v_aanspreken is null then
       v_aanspreken := p_viewer_externe_id||',  Hoort bij relatie '||r_aans.aanspreken;
    ELSE
      v_aanspreken := v_aanspreken||', '||r_aans.aanspreken;
    END IF;
    FETCH c_aans INTO r_aans;
  END LOOP;
  CLOSE c_aans;
--dbms_output.put_line('4: v_aanspreken'||v_aanspreken);


  if p_entiteit= 'UITS' and p_applicatie_vanuit = 'EPD' then
        INSERT INTO kgc_notities
          (entiteit
          , id
          , code
          , omschrijving
          )
          values('UITS'
          ,  p_entiteit_id
          , 'EPD'
          , 'Bekeken op: '||to_char(v_datum, 'dd-mm-yyyy HH24:mi:ss')||', door: '||nvl(v_aanspreken , p_viewer_externe_id)||', extra info: '||p_viewer_extra_info
--          , 'Bekeken op: '||to_char(v_datum, 'dd-mm-yyyy HH24:mi:ss')||', door: '||p_viewer_externe_id||', extra info: '||p_viewer_extra_info
          )
          ;
        commit;
--dbms_output.put_line('5: notities');

  end if;
EXCEPTION
  WHEN OTHERS THEN
    NULL;

END raadplegen;
function kafd_epd_tonen
(
p_ongr_id number
)
return varchar2 is
 cursor c_atwa ( p_ongr_id in number)is
    select atwa.waarde
    from   kgc_attributen attr
    ,      kgc_attribuut_waarden atwa
    where  attr.attr_id = atwa.attr_id
    and    atwa.id = p_ongr_id
    and    attr.code = 'KAFD_TONEN_IN_EPD_ALS'
    and    tabel_naam = 'KGC_ONDERZOEKSGROEPEN' ;

  r_atwa c_atwa%ROWTYPE;

  cursor c_kafd ( p_ongr_id in number)is
    select ongr.code groep
           , kafd.code afdeling
    from   kgc_onderzoeksgroepen ongr
    ,      kgc_kgc_afdelingen kafd
    where  ongr.kafd_id = kafd.kafd_id
    and    ongr.ongr_id = p_ongr_id    ;

  r_kafd c_kafd%ROWTYPE;


  v_kafd varchar2(100);

BEGIN
  v_kafd := null;
  OPEN C_atwa(p_ongr_id);
  FETCH c_atwa into r_atwa;
  IF c_atwa%FOUND
   THEN v_kafd := r_atwa.waarde;
  END IF;
  CLOSE c_atwa;
  IF v_kafd is null then --er is geen alternatieve waarde voor de afdeling of deze is leeg.
    OPEN c_kafd(p_ongr_id);
    FETCH c_kafd INTO r_kafd;
    IF c_kafd%FOUND
      THEN
      v_kafd := r_kafd.afdeling;
    end if;
    CLOSE c_kafd;
  END IF;
  RETURN v_kafd;
END kafd_epd_tonen;

 FUNCTION ongr_epd_tonen
 (
 p_ongr_id number,
 p_onde_id number
 ) return varchar2 is

  cursor c_ongr is
    select atwa.waarde
    from   kgc_attribuut_waarden atwa
    ,      kgc_attributen attr
    where  attr.code = 'TONEN_IN_EPD_ALS'
    and    attr.tabel_naam = 'KGC_ONDERZOEKSGROEPEN'
    and    atwa.attr_id = attr.attr_id
    and    atwa.id = p_ongr_id    ;

  r_ongr c_ongr%ROWTYPE;
  v_ongr varchar2(1000);

  cursor c_onde is
    select afgerond
    , kafd.code kafd_code
    , ongr.code ongr_code
    from kgc_onderzoeken onde
    , kgc_kgc_afdelingen kafd
    , kgc_onderzoeksgroepen ongr
    where onde.onde_id = p_onde_id
    and onde.kafd_id = kafd.kafd_id
    and onde.ongr_id = ongr.ongr_id
    ;

  r_onde c_onde%ROWTYPE;

BEGIN
  v_ongr := null;
  OPEN c_ongr;
  FETCH C_ongr INTO r_ongr;
  IF c_ongr%FOUND
      THEN
      IF r_ongr.waarde = 'INDICATIES' then
        OPEN C_onde;
        FETCH C_ONDE INTO R_ONDE;
        IF C_onde%FOUND then
           IF ( r_onde.afgerond = 'N'
                and ( substr(r_onde.kafd_code, 1,4) = 'DNA-' or r_onde.ongr_code in ('DD') or r_onde.kafd_code in ('GENOOM', 'CYTO'))
                )
           then
             v_ongr := null;
           else
             v_ongr := beh_onde_indi_omschrijvingen (p_onde_id);
           END IF;
        ELSE
          v_ongr := null;
        END IF;
        Close C_onde;
      ELSE
        v_ongr := r_ongr.waarde;
      END IF;
  END IF;
  CLOSE c_ongr;
  RETURN v_ongr;
END ongr_epd_tonen;

function onderzoek_EPD_afgerond
(
P_onde_id number
)
return varchar is
/******************************************************************************
   NAME:       beh_onderzoek_epd_afgerond
   PURPOSE:    bepaal of het onderzoek is afgerond volgens de criteria van het epd
      om te gebruiken in de view van het epd: beh_epd_view
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        23-03-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
    cursor c_afro (p_onde_id in number)
    is
    select audl_id
    , datum_tijd
    from kgc_audit_log
    where tabel_naam   = 'KGC_ONDERZOEKEN'
    and kolom_naam   = 'AFGEROND'
    and aktie        = 'UPD'
    and waarde_oud   = 'N'
    and waarde_nieuw = 'J'
    and id = p_onde_id
    order by audl_id desc;

    r_afro c_afro%ROWTYPE;

    cursor c_onde( p_onde_id in number)
    is
    select afgerond
    ,      datum_autorisatie
    from kgc_onderzoeken
    where onde_id = p_onde_id
    ;
    r_onde c_onde%ROWTYPE;

  v_datum_tijd date;

  v_afgerond varchar2(1);
  v_datum_autorisatie date;
  v_vertraging number := null;

BEGIN
  v_datum_tijd        := null;
  v_datum_autorisatie := null;
  v_afgerond          := 'N';

  IF  p_onde_id is not null  THEN
    OPEN c_ONDE(p_onde_id);
    FETCH c_ONDE INTO r_ONDE;
    IF c_ONDE%FOUND THEN
      v_afgerond := r_onde.afgerond;
      v_datum_autorisatie := r_onde.datum_autorisatie;
    END IF;
    CLOSE c_onde;
  END IF;

  IF ( v_afgerond ='N'
       or
       ( v_afgerond = 'J' and v_datum_autorisatie <= to_date('08-06-2006','dd-mm-yyyy')
        )
      )
  THEN
    RETURN v_afgerond;

  ELSE

    IF  v_afgerond = 'J' and v_datum_autorisatie > to_date('08-06-2006','dd-mm-yyyy') THEN
      OPEN c_afro(p_onde_id);
      FETCH c_afro INTO r_afro;
      IF c_afro%FOUND THEN

        select to_number(nvl(standaard_waarde, '30') )into v_vertraging from kgc_systeem_parameters where code = 'BEH_EPD_VERTRAGING_AUTORISATIE';

        v_datum_tijd := r_afro.datum_tijd;
      END IF;
      CLOSE c_afro;
    END IF;

    IF v_datum_tijd is  not null THEN

      IF ( v_datum_tijd < (sysdate - (1/(24*(60/v_vertraging))) )
          and v_afgerond = 'J'
         )
      THEN
        v_afgerond := 'J';
      ELSE
        v_afgerond := 'N';

      END IF;
    END IF;
  END IF;

  RETURN v_afgerond;

END onderzoek_EPD_afgerond;
FUNCTION zoek_uitslag
(
p_onde_id number,
p_hoev number
)
return varchar is
/******************************************************************************
   NAME:       beh_zoek_uitslag
   PURPOSE:    zoek de n-de uitslag bij een onderzoek te beginnen met de meest recente
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18-02-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_uits is
    select uits.uits_id
    from   kgc_uitslagen  uits
    where  uits.onde_id = p_onde_id
    order by uits.last_update_date desc
    ;

  r_uits c_uits%ROWTYPE;
  v_hoev number;
  v_uits_id varchar2(30);
  v_teller  number;

BEGIN
  v_teller  := 0;
  v_uits_id := null;
  v_hoev    := P_hoev;
  OPEN c_uits;
  Fetch C_uits INTO r_uits;
  While  ( c_uits%FOUND and v_teller < v_hoev)
  loop
    v_teller := v_teller  + 1;
    v_uits_id := r_uits.uits_id;
    Fetch C_uits INTO r_uits;
  END loop;
  CLOSE c_uits;
  IF v_teller < v_hoev THEN v_uits_id := NULL; END IF;
  RETURN v_uits_id;
end zoek_uitslag;

FUNCTION zoek_uitslag_ramo_code
(
p_uits_id kgc_uitslagen.uits_id%type
) return varchar is
/******************************************************************************
   NAME:       beh_zoek_uitslag_ramo_ocde
   PURPOSE:    zoek de ramo_code bij de n-de uitslag van een onderzoek (te beginnen met de meest recente) uitslag
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10-03-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_uits is
    select uits.uits_id
    ,      decode(brty.code, 'CONTROLE', 'CONTROLE', 'UITSLAG') ramo_code
    from   kgc_uitslagen  uits
    ,      kgc_brieftypes brty
    where  uits.uits_id = p_uits_id
    and    uits.brty_id = brty.brty_id
    ;

  r_uits c_uits%ROWTYPE;

BEGIN
  OPEN c_uits;
  Fetch c_uits INTO r_uits;
  close c_uits;

  RETURN r_uits.ramo_code;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
   return NULL;
  WHEN OTHERS THEN
    null;
END zoek_uitslag_ramo_code;
FUNCTION zoek_uitslag_datum
(
p_onde_id number,
p_hoev number
) return varchar is
/******************************************************************************
   NAME:       beh_zoek_uitslag_datum
   PURPOSE:    zoek de uitslag _datum bij de n-de uitslag bij een onderzoek te beginnen met de meest recente
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18-02-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_uits is
    select uits.uits_id
    ,      uits.datum_uitslag
    from   kgc_uitslagen  uits
    where  uits.onde_id = p_onde_id
    order by uits.last_update_date desc
    ;

  r_uits c_uits%ROWTYPE;
  v_hoev number;
  v_datum varchar2(30);
  v_teller  number;

BEGIN
  v_teller  := 0;
  v_datum := null;
  v_hoev    := P_hoev;
  OPEN c_uits;
  Fetch C_uits INTO r_uits;
  While  ( c_uits%FOUND and v_teller < v_hoev)
  loop
    v_teller := v_teller  + 1;
    v_datum := to_char(r_uits.datum_uitslag, 'dd-mm-yyyy');
    Fetch C_uits INTO r_uits;
  END loop;
  CLOSE c_uits;
  IF v_teller < v_hoev THEN v_datum := NULL; END IF;
  RETURN v_datum;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
           v_datum := NULL;
       WHEN OTHERS THEN
             null;
END zoek_uitslag_datum;
FUNCTION zoek_uitslag_bestand
(
p_uits_id number,
p_rela_id number
) return varchar is
/******************************************************************************
   NAME:       beh_zoek_uitslag_bestand
   PURPOSE:    zoek het bestand bij de uitslag
               om te gebruiken in het epd

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09-11-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
    cursor c_best ( p_uits_id in number)is
    select bestand_specificatie
    from   kgc_bestanden  best
    ,      kgc_bestand_types btyp
    where  best.btyp_id = btyp.btyp_id
     -- als er geen rela_id is ingevoerd, wordt rela_id van de aanvrager gebruikt.
    and    nvl(best.rela_id, (select rela_id from kgc_uitslagen where uits_id = p_uits_id) ) = p_rela_id
    and    best.entiteit_pk = p_uits_id
    and    best.entiteit_code = 'UITS'
    and    btyp.code like 'KGCUIT%'
    order by BEST.LAST_UPDATE_DATE desc
    ;

  v_bestand varchar2(2000);
BEGIN
v_bestand := null;

OPEN c_best(p_uits_id) ;
Fetch c_best INTO v_bestand;
if c_best%notfound then
    v_bestand := null;
end if;
close c_best;
RETURN v_bestand;
EXCEPTION
 WHEN NO_DATA_FOUND THEN
   v_bestand := NULL;
 WHEN OTHERS THEN
   null;
END zoek_uitslag_bestand;
FUNCTION buiten_cons
(
p_onde_id number
) return varchar is
/******************************************************************************
   NAME:       beh_buiten_cons
   PURPOSE:   valt de aanvrager van het onderzoek onder de buitenland constructie?
              om te kunen bepalen of het getoond moet worden  in het epd
              buitenlanmd cosntructie = J, dan niet tonen in het epd
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        01-03-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
  v_return VARCHAR2(100):= null;
  v_rela   varchar2(10) := null;

  cursor c_onde is
    select rela.code
    from kgc_onderzoeken onde
    , KGC_RELATIES rela
    , kgc_attributen attr
    , kgc_attribuut_waarden atwa
    where onde.rela_id = rela.rela_id
    and   onde.onde_id = p_onde_id
    and   atwa.attr_id = attr.attr_id
    and   atwa.id = rela.rela_id
    and   atwa.waarde = 'J'
    and   ATTR.TABEL_NAAM = 'KGC_RELATIES'
    and   attr.code = 'EPD_BUITEN_CONS'
    ;

BEGIN

  IF ( p_onde_id   IS NOT NULL )
  THEN
    OPEN  c_onde;
    FETCH c_onde
    INTO  v_rela;
    CLOSE c_onde;
  END IF;
  if v_rela is not null then
    v_return := 'J';
  else
    v_return := 'N';
  ENd if;
  RETURN( v_return );

END buiten_cons;

FUNCTION INGR_CODE
(
p_onde_id number
) return varchar is
/******************************************************************************
   NAME:       ingr_code
   PURPOSE:    zoek de code van de indicatiegroep bij een onderzoek ,
               om te gebruiken in select scripts,
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31-05-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  v_return VARCHAR2(100):= null;

  v_ingr_id NUMBER := NULL;

  CURSOR ingr_cur
  IS
    SELECT code
    FROM   kgc_indicatie_groepen
    WHERE  ingr_id = v_ingr_id
    ;

  ingr_rec ingr_cur%rowtype;

  CURSOR ingr_id_cur
  IS
    SELECT ingr.ingr_id
--    ,      onin.volgorde
--    ,      onin.onin_id
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.indi_id = indi.indi_id
    AND    indi.ingr_id = ingr.ingr_id
    AND    onin.onde_id = p_onde_id
    UNION
    SELECT ingr.ingr_id
--    ,      onin.volgorde
--    ,      onin.onin_id
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.ingr_id = ingr.ingr_id
    AND    ingr.machtigingsindicatie IS NOT NULL
    AND    onin.onde_id = p_onde_id
--    ORDER BY 2 ASC
--    ,        3 ASC
    ;

  ingr_id_rec ingr_id_cur%rowtype;


BEGIN

  IF ( p_onde_id IS NOT NULL )

    THEN
    OPEN  ingr_id_cur;
    FETCH ingr_id_cur     INTO v_ingr_id;
    While ingr_id_cur%found loop

      OPEN  ingr_cur;
      FETCH ingr_cur      INTO  ingr_rec;
      IF INGR_CUR%FOUND THEN
        if v_return  is null then
          V_RETURN := ''''||INGR_REC.CODE||'''';
        else
         V_RETURN := V_RETURN||','''||INGR_REC.CODE||'''';
        end if;
      END IF;
      CLOSE ingr_cur;


      FETCH ingr_id_cur     INTO  v_ingr_id;
    end loop;
    CLOSE ingr_id_cur;
  END IF;
  RETURN( v_return );
END INGR_CODE;
--####################
end BEH_EPD_00;
/

/
QUIT
