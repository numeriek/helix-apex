CREATE OR REPLACE PACKAGE "HELIX"."BEH_SOHI_00" as
procedure log_object
(
p_object_name varchar2,
p_object_type varchar2
)
;
procedure write2file
(
p_version varchar2,
p_object_name varchar2
)
;
end beh_sohi_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_SOHI_00"
as
-- zie ook mantisnr 8293
-- disable trigger beh_sohi_trg_acr v��r hercompileren (en commiten!), anders wordt een foutmelding gegeven.
procedure log_object
(
p_object_name varchar2,
p_object_type varchar2
)
is
/*
v_email_afzender varchar2(100) := nvl(kgc_sypa_00.standaard_waarde
                           ( p_parameter_code => 'BEH_MIJN_HELIX_EMAIL_ADRES'
                           , p_mede_id        => kgc_mede_00.medewerker_id('HELIX')
                         ),'HelpdeskHelixCUKZ@cukz.umcn.nl');
*/
v_email_afzender varchar2(100) := nvl(kgc_sypa_00.systeem_waarde ('BEH_STGEN_EMAIL_FROM'),'HelpdeskHelixCUKZ@cukz.umcn.nl');
v_version varchar2(50) := to_char(sysdate, 'ddmmyyyyhh24miss');
v_db_name beh_source_hist.db_name%type := sys_context('userenv','db_name');
v_subject varchar2(500) := p_object_name || ' (' || v_db_name || ')';
v_user varchar2(100) := user || ';' || sys_context('USERENV','OS_USER');
v_newline varchar2(10) := chr(10);
/*
v_msg varchar2(32767) :=
        '
            <font>
            Beste beheerders,
            <br>
            <br>
            ' ||
            p_object_type || ': ' ||
            p_object_name ||
            ' is opnieuw gecompileerd en misschien ook aangepast. De oude versie is onder versienr: ' ||
            v_version || ', in tabel BEH_SOURCE_HIST opgeslagen.
            </font>
            <br>
            <br>
            <br>
            Deze e-mail is automatisch, door Helix trigger: "beh_sohi_trg_acr" verzonden.
        ';
*/
v_msg varchar2(32767) :=
        'Beste beheerders,'
        || v_newline
        || v_newline
        || p_object_type || ': '
        || p_object_name
        || ' is opnieuw gecompileerd en misschien ook aangepast. De oude versie is onder versieNr: '
        || v_version || ', in tabel BEH_SOURCE_HIST opgeslagen.'
        || v_newline
        || v_newline
        || v_newline
        || 'Deze e-mail is automatisch, door Helix trigger: "beh_sohi_trg_acr" verzonden.'
        ;

cursor c_cawa is
select cawa.*
from kgc_categorieen cate
,       kgc_categorie_types caty
,       kgc_categorie_waarden cawa
where cate.caty_id = caty.caty_id
and    cate.cate_id = cawa.cate_id
and    caty.code = 'MONITOSOUR'
;

begin
if p_object_type in ('PROCEDURE', 'FUNCTION', 'PACKAGE', 'PACKAGE BODY', 'TYPE', 'TRIGGER', 'JAVA SOURCE') then
    insert into beh_source_hist
    select us.*, v_version, v_db_name, v_user, sysdate, v_user, sysdate
    from user_source us
    where us.type = p_object_type
    and us.name = p_object_name
    ;
elsif p_object_type = 'VIEW' then
  insert into beh_source_hist
  select view_name, p_object_type, 1 line, to_lob(text), v_version, v_db_name, v_user, sysdate, v_user, sysdate
  from all_views
  where view_name = p_object_name
  ;
  -- Matrialized views
elsif p_object_type = 'SNAPSHOT' then
  insert into beh_source_hist
  select mview_name, p_object_type, 1 line, to_lob(query), v_version, v_db_name, v_user, sysdate, v_user, sysdate
  from all_mviews
  where owner = 'HELIX'
  and mview_name = p_object_name
  ;
end if;

for r_cawa in c_cawa loop
    if p_object_name = upper(r_cawa.waarde) and r_cawa.beschrijving is not null then
    /*
        send_mail_html(p_from => v_email_afzender,
                        p_to => r_cawa.beschrijving,
                        p_subject => v_subject,
                        p_html => v_msg
                        );
*/
        UTL_MAIL.SEND(  sender => v_email_afzender,
                recipients => r_cawa.beschrijving,
                subject => v_subject,
                message => v_msg);
    end if;
end loop;
exception
when others then
    raise_application_error(-20000, SQLERRM);
end log_object;
procedure write2file
(
p_version varchar2,
p_object_name varchar2
)
--voorbeeld aanroep script:
--begin
--beh_sohi_00.write2file('07012014114815', 'BEH_DECLARATIES_VW');
--end;
is
v_create_dir_sql varchar2(2000) := 'create directory BEH_TEMP as ''D:/temp''';
v_drop_dir_sql varchar2(2000) := 'drop directory BEH_TEMP';

v_file_handler utl_file.file_type;
v_file_name varchar2(50) := p_object_name||'_'|| p_version ||'.sql';
cursor c_sohi is
select *
from beh_source_hist
where version = p_version
and upper(name) = upper(p_object_name)
order by line
;
begin
execute immediate v_create_dir_sql;
--beh_temp = d:/temp op db server
v_file_handler := UTL_FILE.FOPEN('BEH_TEMP', v_file_name, 'W');

for r_sohi in c_sohi loop
  utl_file.putf(v_file_handler, r_sohi.text);
  utl_file.fflush(v_file_handler);
end loop;

UTL_FILE.FCLOSE(v_file_handler);
execute immediate v_drop_dir_sql;
exception
when others then
  UTL_FILE.FCLOSE(v_file_handler);
  execute immediate v_drop_dir_sql;
  raise_application_error(-20000, SQLERRM);
end write2file;
end beh_sohi_00;
/

/
QUIT
