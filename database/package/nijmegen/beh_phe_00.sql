CREATE OR REPLACE PACKAGE "HELIX"."BEH_PHE_00"
AS
FUNCTION meetwaarde_doorgeven
( p_tekst in varchar
) return varchar ;

FUNCTION bepaal_voorletters
(
p_pers_voorletters varchar
) return varchar;

FUNCTION bepaal_sms_tekst
(
p_sypa_code in varchar,
p_pers_voorletters varchar,
p_pers_geboortejaar varchar,
p_mons_datum_afname varchar,
p_meet_tonen_als varchar,
p_meet_eenheid varchar
) return varchar;


FUNCTION bepaal_sms_vervangende_tekst
(
p_sypa_code in varchar,
p_pers_voorletters varchar,
p_pers_geboortejaar varchar,
p_mons_datum_afname varchar
) return varchar;


procedure verwerk_sms_phe_log
(
p_verzonden in varchar,
p_pers_id_ontvanger in number,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
);

PROCEDURE verwerk_sms
;

END beh_phe_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_PHE_00"
AS
FUNCTION meetwaarde_doorgeven
( p_tekst in varchar
) return varchar is
/******************************************************************************
 name:         meetwaarde_doorgeven
 purpose:     bepalen of meetwaarde wel of niet doorgegeven moet worden
 revisions:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        17-05-2013  Cootje Vermaat      1. Created this function.
******************************************************************************/
v_ok varchar2(1) := 'J';
i number:= 1;
begin
  if p_tekst  is null then v_ok := 'N';
  else
      while (i < (length(p_tekst) + 1)) and (v_ok = 'J')
      loop
    --    DBMS_OUTPUT.PUT_LINE(substr(v_tekst, i, 1));
        if substr(p_tekst, i, 1) NOT IN ('1','2','3', '4','5','6','7','8','9','0','>','<',' ')
        THEN
          V_OK:= 'N';
    --    DBMS_OUTPUT.PUT_LINE(v_ok);
        END IF;
        i:= i + 1;
      end loop;
  end if;
  return  v_ok;
end meetwaarde_doorgeven;


FUNCTION bepaal_voorletters
(
p_pers_voorletters varchar
) return varchar is
/******************************************************************************
   NAME:       bepaal_voorletters
   PURPOSE:    indnien d evoornaam staat vermeld  en niet de voorletters, van de voornaam voorletters maken
              voor gebruk in sms tekst
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        15-01-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/


  v_voorletters  varchar2(20);

BEGIN

  v_voorletters := p_pers_voorletters;

  if instr(v_voorletters, '.') < 1
  then
    v_voorletters := substr(Upper(v_voorletters), 1, 1)||'.' ;
  end if;
  RETURN v_voorletters;
END bepaal_voorletters;

FUNCTION bepaal_sms_tekst
(
p_sypa_code in varchar,
p_pers_voorletters varchar,
p_pers_geboortejaar varchar,
p_mons_datum_afname varchar,
p_meet_tonen_als varchar,
p_meet_eenheid varchar
) return varchar is
/******************************************************************************
   NAME:       bepaal_sms_tekst
   PURPOSE:  bepaald de sms tekst a.d.h.v. de sypacode  en de te vervangen waarden
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08-11-2012  Cootje Vermaat      1. Created this function.


******************************************************************************/


  v_tekst  varchar2(200);

BEGIN

  select  standaard_waarde into v_tekst from kgc_systeem_parameters where code = 'BEH_SMS_PHE_TEKST';

   v_tekst := replace(v_tekst, '<pers_voorletters>' , p_pers_voorletters);
   v_tekst := replace(v_tekst, '<pers_geboortejaar>', p_pers_geboortejaar);
   v_tekst := replace(v_tekst, '<mons_datum_afname>', nvl(p_mons_datum_afname, 'onbekend'));
   v_tekst := replace(v_tekst, '<meet_tonen_als>'   , replace(replace(p_meet_tonen_als, '<',' kleiner dan '), '>',' groter dan ') );
   v_tekst := replace(v_tekst, '<meet_eenheid>'     , p_meet_eenheid);

  RETURN v_tekst;
END bepaal_sms_tekst;

FUNCTION bepaal_sms_vervangende_tekst
(
p_sypa_code in varchar,
p_pers_voorletters varchar,
p_pers_geboortejaar varchar,
p_mons_datum_afname varchar
) return varchar is
/******************************************************************************
   NAME:       bepaal_sms_vervangende_tekst
   PURPOSE:  bepaald de vervangende sms tekst a.d.h.v. de sypacode  en de te vervangen waarden indien de meetwaarde leeg is
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-01-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/


  v_tekst  varchar2(200);

BEGIN

  select  standaard_waarde into v_tekst from kgc_systeem_parameters where code = 'BEH_SMS_PHE_VERVANGENDE_TEKST';

   v_tekst := replace(v_tekst, '<pers_voorletters>' , p_pers_voorletters);
   v_tekst := replace(v_tekst, '<pers_geboortejaar>', p_pers_geboortejaar);
   v_tekst := replace(v_tekst, '<mons_datum_afname>', nvl(p_mons_datum_afname, 'onbekend'));

  RETURN v_tekst;
END bepaal_sms_vervangende_tekst;

procedure verwerk_sms_phe_log
(
p_verzonden in varchar,
p_pers_id_ontvanger in number,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) is
v_bety_id beh_bericht_types.bety_id%type;
v_datum_verzonden date := sysdate;
v_oracle_uid kgc_medewerkers.oracle_uid%type;

begin

    select bety_id into v_bety_id
    from beh_bericht_types
    where upper(code) = upper('SMS')
    and upper(vervallen) = upper('N')
    ;

    select sys_context('USERENV', 'SESSION_USER')
    into v_oracle_uid
    from dual
    ;

    insert into beh_berichten
    (
    bety_id,
    verzonden,
    datum_verzonden,
    oracle_uid,
    ontvanger_id_intern,
    adres_verzonden,
    log_tekst,
    entiteit,
    entiteit_id,
    bericht_tekst,
    ontvanger_type
    )
    values
    (
    v_bety_id,
    p_verzonden,
    v_datum_verzonden,
    v_oracle_uid,
    p_pers_id_ontvanger,
    p_adres_verzonden,
    p_log_tekst,
    'METI',
    p_entiteit_id,
    p_bericht_tekst,
    'PERS'
    )
    ;

    commit;

end verwerk_sms_phe_log;

PROCEDURE verwerk_sms

is
/******************************************************************************
   NAME:    verwerk_sms
   PURPOSE: verwerk de sms
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08-11-2012  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_SMPH is
  select *
  from BEH_SMS_PHE_VW
--  where onderzoeknr = 'B13/0001'
--  where onderzoeknr in ('B13/0022')
  ;

  r_SMPH c_smph%rowtype;

  cursor c_beri (p_meti_id in number, p_pers_id in number) is
  select beri.beri_id
  from beh_berichten beri
  , beh_bericht_types bety
  where bety.bety_id = beri.bety_id
  and bety.code = 'SMS'
  and beri.entiteit = 'METI'
  and beri.entiteit_id = p_meti_id
  and beri.ontvanger_type = 'PERS'
  and beri.ontvanger_id_intern = p_pers_id
  and beri.verzonden = 'J'
  and beri.log_tekst = 'OK'
  and beri.creation_date > to_date('09-11-2011','dd-mm-yyyy')
  ;

  r_beri c_beri%rowtype;

  v_sms_ok        boolean;
  v_bericht_tekst varchar2(2000);
  v_afzender      varchar2(200);
  v_sms_log       varchar2(2000);
  v_telefoon      varchar2(20);
  v_verzonden     varchar2(20);
  v_log_tekst     varchar2(2000);


begin

  for r_smph in c_smph
  loop
    DBMS_output.put_line(r_smph.onderzoeknr);

    -- is er al een bericht succesvol verzonden?
    -- ja : doe niets
    -- nee: doe drie dingen: 1 verstuur bericht, 2. log in de berichten tabel, en als het versturen goed gaat  3. log in de notities

    open c_beri(r_smph.meti_id, r_smph.pers_id);
    fetch c_beri into r_beri;
    if c_beri%found then
      null;
    else
     -- acties:
     -- 1. verstuur bericht
      v_telefoon      := r_smph.telefoon;
      v_bericht_tekst := r_SMPH.sms_tekst;
      if SUBSTR(v_telefoon, 1, 5) = '+49 1' then
        v_afzender  := '+31243614567';
      else
        v_afzender  := 'KGCN';
      end if;
      v_sms_ok:= FALSE;


      if  v_bericht_tekst  is not null   then

--         v_telefoon := '0622257225'; -- telefoon van Leo
--         v_telefoon := '0650545927'; --van cootje
--         v_telefoon := '0640927289'; --van Hanneke
--         v_telefoon := '0652421504'; --   van Marleen


         if  (sys_context('userenv','db_name') like 'PROD%' )
              --or
             -- v_telefoon in ( '0622257225', '0650545927', '0652421504', '0640927289')

           then
              v_sms_ok := TRUE;
                v_sms_ok := beh_alfu_00.verstuur_sms
                        ( p_afzender    => v_afzender
                        , p_ontvanger   => v_telefoon
                        , p_bericht     => v_bericht_tekst
                        , x_message     => v_sms_log
                        );


         end if;


        if v_sms_ok then
            v_verzonden := 'J';
        else
            v_verzonden := 'N';
        end if;
        v_log_tekst := v_sms_log;

       -- 2. loggen van het versturen van het bericht
          if v_verzonden = 'J' then
           -- versturen bericht is geslaagd
            v_log_tekst := 'OK';
          else
           -- versturen bericht is niet  geslaagd
            v_log_tekst := 'NOK';
          end if;



dbms_output.put_line(' p_verzonden=         '|| v_verzonden);
dbms_output.put_line(' p_pers_id_ontvanger= '|| r_smph.pers_id);
dbms_output.put_line(' p_adres_verzonden= '  || v_telefoon);
dbms_output.put_line(' p_log_tekst= '        || v_log_tekst); -- in varchar,
dbms_output.put_line(' p_entiteit_id= '      || r_smph.METI_id);-- in number,
dbms_output.put_line(' p_bericht_tekst= '    || v_bericht_tekst);-- in varchar

            verwerk_SMS_PHE_log(v_verzonden, r_smph.PERS_id, v_telefoon, v_log_tekst, r_smph.METI_id, v_bericht_tekst);

             -- 3 invoeren in notities,  alleen als hetversturen van het bericht is geslaagd!!!
                  if ( v_verzonden = 'J'
                      and r_smph.meti_id is not null ) then

                        INSERT INTO kgc_notities
                          (entiteit
                          , id
                          , code
                          , omschrijving
                          )
                          values('METI'
                          ,  r_smph.meti_id
                          , 'SMS'
                          , 'SMS tekst: "'||v_bericht_tekst||'" Verstuurd op: '||to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')||', aan: '||r_smph.voorletters||' '||r_SMPH.geboortejaar
                          )
                          ;
                        commit;

                      -- klaar

                    end if;


      end if;

   end if;
   close c_beri;


  end loop;


end VERWERK_SMS;

END beh_phe_00;
/

/
QUIT
