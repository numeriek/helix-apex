CREATE OR REPLACE PACKAGE "HELIX"."BEH_BRIE_00" IS
FUNCTION  uitslag_eerder_geprint
 ( p_uits_id  IN  NUMBER
 , p_brty_id  IN  NUMBER := NULL
 , p_rela_id  IN  NUMBER := NULL
 )
 RETURN VARCHAR2;
-- Is de brief al eerder uitgedraaid?
FUNCTION  eerder_geprint
 ( p_onde_id   IN NUMBER   := NULL
 , p_brieftype IN VARCHAR2 := NULL
 , p_brty_id   IN NUMBER   := NULL
 , p_ramo_id   IN NUMBER   := NULL
 , p_uits_id   IN NUMBER   := NULL
 , p_rela_id   IN NUMBER   := NULL
 , p_decl_id   IN NUMBER   := NULL
 , p_zwan_id   IN NUMBER   := NULL
 , p_obmg_id   IN NUMBER   := NULL
 )
 RETURN  VARCHAR2;

END BEH_BRIE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_BRIE_00" IS
FUNCTION  eerder_geprint
 ( p_onde_id   IN NUMBER   := NULL
 , p_brieftype IN VARCHAR2 := NULL
 , p_brty_id   IN NUMBER   := NULL
 , p_ramo_id   IN NUMBER   := NULL
 , p_uits_id   IN NUMBER   := NULL
 , p_rela_id   IN NUMBER   := NULL
 , p_decl_id   IN NUMBER   := NULL
 , p_zwan_id   IN NUMBER   := NULL
 , p_obmg_id   IN NUMBER   := NULL
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(1);

  CURSOR brty_cur
  (b_ramo_id IN NUMBER
  ,b_brieftype IN VARCHAR2
  )
  IS
    SELECT brty_id
    FROM   kgc_brieftypes
    WHERE ( ramo_id = b_ramo_id OR b_ramo_id IS NULL )
    AND   ( code = UPPER(b_brieftype)
         OR UPPER(omschrijving) LIKE '%'||UPPER(b_brieftype)||'%'
         OR b_brieftype IS NULL
          )
    ;
  v_brty_id NUMBER;

  CURSOR uits_cur
  ( b_brty_id IN NUMBER
  , b_uits_id IN NUMBER
  , b_rela_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven brie
    WHERE  brie.brty_id = b_brty_id
    AND    brie.uits_id = b_uits_id
    AND    brie.rela_id = b_rela_id
  ;
  CURSOR decl_cur
  ( b_brty_id IN NUMBER
  , b_decl_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    decl_id = b_decl_id
  ;
  CURSOR zwan_cur
  ( b_brty_id IN NUMBER
  , b_zwan_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    zwan_id = b_zwan_id
  ;
  CURSOR obmg_cur
  ( b_brty_id IN NUMBER
  , b_obmg_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    obmg_id = b_obmg_id
  ;
  CURSOR onde_cur
  ( b_brty_id IN NUMBER
  , b_onde_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    onde_id = b_onde_id
  ;
  CURSOR rela_cur
  ( b_brty_id IN NUMBER
  , b_rela_id IN NUMBER
  )
  IS
    SELECT 1
    FROM   kgc_brieven
    WHERE  brty_id = b_brty_id
    AND    rela_id = b_rela_id
  ;

  v_dummy VARCHAR2(1);
 BEGIN
  IF ( p_brty_id IS NOT NULL )
  THEN
    v_brty_id := p_brty_id;
  ELSE
    OPEN  brty_cur (b_ramo_id   => p_ramo_id
                   ,b_brieftype => p_brieftype
                   );
    FETCH brty_cur
    INTO  v_brty_id;
    CLOSE brty_cur;
  END IF;

  if p_uits_id is not null
  then
    OPEN  uits_cur  ( b_brty_id => v_brty_id
                    , b_uits_id => p_uits_id
                    , b_rela_id => p_rela_id
                    );
    FETCH uits_cur
    INTO  v_dummy;
    CLOSE uits_cur;
  elsif p_decl_id is not null
  then
    OPEN  decl_cur  ( b_brty_id => v_brty_id
                    , b_decl_id => p_decl_id
                    );
    FETCH decl_cur
    INTO  v_dummy;
    CLOSE decl_cur;
  elsif p_zwan_id is not null
  then
    OPEN  zwan_cur  ( b_brty_id => v_brty_id
                    , b_zwan_id => p_zwan_id
                    );
    FETCH zwan_cur
    INTO  v_dummy;
    CLOSE zwan_cur;
  elsif p_obmg_id is not null
  then
    OPEN  obmg_cur  ( b_brty_id => v_brty_id
                    , b_obmg_id => p_obmg_id
                    );
    FETCH obmg_cur
    INTO  v_dummy;
    CLOSE obmg_cur;
  elsif p_onde_id is not null
  then
    OPEN  onde_cur  ( b_brty_id => v_brty_id
                    , b_onde_id => p_onde_id
                    );
    FETCH onde_cur
    INTO  v_dummy;
    CLOSE onde_cur;
  elsif p_rela_id is not null
  then
    OPEN  rela_cur  ( b_brty_id => v_brty_id
                    , b_rela_id => p_rela_id
                    );
    FETCH rela_cur
    INTO  v_dummy;
    CLOSE rela_cur;
  end if;

  IF v_dummy = 1
  THEN
   v_return := 'J';
  ELSE
   v_return := 'N';
  END IF;

  RETURN( v_return );
END eerder_geprint;
FUNCTION  uitslag_eerder_geprint
 ( p_uits_id  IN  NUMBER
 , p_brty_id  IN  NUMBER := NULL
 , p_rela_id  IN  NUMBER := NULL
 )
RETURN VARCHAR2  IS

  CURSOR uits_cur
  ( b_uits_id IN NUMBER
  )
  IS
    SELECT brty_id
    FROM   kgc_uitslagen
    WHERE  uits_id = b_uits_id
  ;

 v_brty_id NUMBER;

BEGIN
  IF p_brty_id IS NULL
  THEN
    open uits_cur ( b_uits_id => p_uits_id );
    fetch uits_cur
    into v_brty_id;
    close uits_cur;
  ELSE
    v_brty_id := p_brty_id;
  END IF;

  RETURN eerder_geprint( p_uits_id => p_uits_id
                       , p_brty_id => v_brty_id
                       , p_rela_id => p_rela_id
                       );
END uitslag_eerder_geprint;
END BEH_BRIE_00;
/

/
QUIT
