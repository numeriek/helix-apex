CREATE OR REPLACE PACKAGE "HELIX"."BEH_CRYPTIT" is
Function encrypt_data( p_data IN VARCHAR2 ) Return RAW DETERMINISTIC;
Function decrypt_data( p_data IN RAW ) Return VARCHAR2 DETERMINISTIC;
End beh_cryptit;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_CRYPTIT" is
V_Key       RAW(128) := UTL_RAW.cast_to_raw('helixnijmegen');          -- Key

Function encrypt_data( p_data IN VARCHAR2 ) Return RAW DETERMINISTIC IS
l_data RAW(2048) := utl_raw.cast_to_raw(p_data);
l_encrypted RAW(2048);
BEGIN
NULL;
l_encrypted := dbms_crypto.encrypt                        -- Algorithm
    (
        src => l_data,
        typ => DBMS_CRYPTO.DES_CBC_PKCS5,
        key => V_KEY
    );

Return l_encrypted;
END encrypt_data;

Function decrypt_data( p_data IN RAW ) Return VARCHAR2 DETERMINISTIC IS
l_decrypted RAW(2048);
BEGIN
l_decrypted := dbms_crypto.decrypt                              -- Algorithm
    (
        src => p_data,
        typ => DBMS_CRYPTO.DES_CBC_PKCS5,
        key => V_KEY
    );

Return utl_raw.cast_to_varchar2(l_decrypted);
END decrypt_data;
End beh_cryptit;
/

/
QUIT
