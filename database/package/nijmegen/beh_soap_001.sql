CREATE OR REPLACE PACKAGE "HELIX"."BEH_SOAP_001" as
g_scheidingsteken1 varchar2(1) := ';';
g_scheidingsteken2 varchar2(1) := '$';
function haal_alle_indi_op
(
p_onde_id in number
)
return varchar2
;
function haal_alle_frac_op
(
p_onde_id in number,
p_fractienummer in varchar2
)
return varchar2
;
end beh_soap_001;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_SOAP_001" as
function haal_alle_indi_op
(p_onde_id in number)

return varchar2
is
v_return varchar2(10000);

cursor c_indi is
select indi.code, indi.omschrijving
from kgc_onderzoek_indicaties onin, kgc_indicatie_teksten indi
where onin.indi_id = indi.indi_id
and   onin.onde_id = p_onde_id
order by onin.volgorde, onin.creation_date
;

begin
for r_indi in c_indi loop

    if v_return is null then
        v_return := r_indi.code || g_scheidingsteken1 || r_indi.omschrijving;
    else
        v_return := v_return || g_scheidingsteken2 || r_indi.code || g_scheidingsteken1 || r_indi.omschrijving;
    end if;

end loop;
return v_return;
end haal_alle_indi_op;
--###########################
function haal_alle_frac_op
(
p_onde_id in number,
p_fractienummer in varchar2
)

return varchar2 is

v_return varchar2(10000);

cursor c_frac is
select frac.fractienummer
from kgc_onderzoeken onde
,    kgc_onderzoek_monsters onmo
,    kgc_monsters mons
,    bas_fracties frac
where onde.onde_id = onmo.onde_id
and   onmo.mons_id = mons.mons_id
and   mons.mons_id = frac.mons_id
and   onde.onde_id = p_onde_id
and   frac.fractienummer <> p_fractienummer
;

begin
for r_frac in c_frac loop

    if v_return is null then
        v_return := r_frac.fractienummer;
    else
        v_return := v_return || g_scheidingsteken1 || r_frac.fractienummer;
    end if;

end loop;
return v_return;
end haal_alle_frac_op;
end beh_soap_001;
/

/
QUIT
