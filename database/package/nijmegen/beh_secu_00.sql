CREATE OR REPLACE PACKAGE "HELIX"."BEH_SECU_00" AUTHID CURRENT_USER IS

PROCEDURE create_role_appl
;


procedure grant_role_appl;

PROCEDURE revoke_grants
(  p_object IN VARCHAR2
);

PROCEDURE create_grants
(  p_object IN VARCHAR2
)
;

procedure set_roles
;
-------------------------------------------------------------------------------------


procedure bewaar_log_in_gegevens
(p_toegestaan_tegengehouden in varchar2
)
;

procedure check_bevoegdheid
;




-------------------------------------------------------------------------------------
END beh_secu_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_SECU_00"
  is

procedure create_role_appl
IS
  v_statement VARCHAR2(4000);
BEGIN
    v_statement := 'CREATE ROLE HELIX_APPLICATIE IDENTIFIED USING helix.beh_secu_00;' ;-- eventueel identified by ... toevoegen

    BEGIN
      kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;

END create_role_appl;

procedure grant_role_appl
is

  cursor c_user is
    select username
    from all_users -- users
    where instr(username, '.') >0
    and username not in ( 'A.ARIG', 'C.VERMAAT');

  r_user c_user%rowtype;
  v_statement VARCHAR2(4000);

begin
    for r_user in c_user
    loop
        v_statement := 'GRANT  HELIX_APPLICATIE TO "'||r_user.username||'";' ;
        BEGIN
          kgc_util_00.dyn_exec( v_statement );
        EXCEPTION
          WHEN OTHERS
          THEN
            NULL;
        END;
    end loop;


end grant_role_appl;

PROCEDURE revoke_grants
(  p_object IN VARCHAR2
)
IS
  CURSOR obj_cur
  IS
    SELECT object_name
    ,      object_type
    FROM   user_objects
    WHERE  object_name LIKE NVL( UPPER(p_object), '%' )
    AND    object_type IN ( 'TABLE', 'VIEW', 'SEQUENCE', 'PACKAGE', 'PROCEDURE', 'FUNCTION' )
    ;
  v_statement VARCHAR2(4000);
  v_grants VARCHAR2(200);
BEGIN
  FOR r IN obj_cur
  LOOP
    IF ( r.object_type = 'TABLE' )
    THEN
      v_grants := 'SELECT,INSERT,UPDATE,DELETE';
    ELSIF ( r.object_type IN ( 'PACKAGE', 'PROCEDURE', 'FUNCTION' ) )
    THEN
      v_grants := 'EXECUTE';
    ELSE
      v_grants := 'SELECT';
    END IF;
    v_statement := 'REVOKE '||v_grants||' ON '||r.object_name||' FROM PUBLIC;'
                 ;
    BEGIN
      kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END LOOP;
END revoke_grants;


PROCEDURE create_grants
-- rechten geven aan rol helix_applicatie
(  p_object IN VARCHAR2
)
IS
  CURSOR obj_cur
  IS
    SELECT object_name
    ,      object_type
    FROM   user_objects
    WHERE  object_name LIKE NVL( UPPER(p_object), '%' )
    AND    object_type IN ( 'TABLE', 'VIEW', 'SEQUENCE', 'PACKAGE', 'PROCEDURE', 'FUNCTION' )
    ;
  v_statement VARCHAR2(4000);
  v_grants VARCHAR2(200);
BEGIN
  FOR r IN obj_cur
  LOOP
    IF ( r.object_type = 'TABLE' )
    THEN
      v_grants := 'SELECT,INSERT,UPDATE,DELETE';
    ELSIF ( r.object_type IN ( 'PACKAGE', 'PROCEDURE', 'FUNCTION' ) )
    THEN
      v_grants := 'EXECUTE';
    ELSE
      v_grants := 'SELECT';
    END IF;
    v_statement := 'GRANT '||v_grants||' ON '||r.object_name||' TO HELIX_APPLICATIE;'
                 ;
    BEGIN
      kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END LOOP;
END create_grants;

procedure set_roles

is

--
begin
  null;
end set_roles;

procedure bewaar_log_in_gegevens
(p_toegestaan_tegengehouden in varchar2
)
is

  v_action                        VARCHAR2(200):= sys_context('USERENV', 'ACTION');
  v_audited_cursorid              VARCHAR2(200):= sys_context('USERENV', 'AUDITED_CURSORID');
  v_authenticated_identity        VARCHAR2(200):= sys_context('USERENV', 'AUTHENTICATED_IDENTITY');
  v_authentication_data            VARCHAR2(200):= sys_context('USERENV', 'AUTHENTICATION_DATA');
  v_authentication_method         VARCHAR2(200):= sys_context('USERENV', 'AUTHENTICATION_METHOD');
  v_bg_job_id                     VARCHAR2(200):= sys_context('USERENV', 'BG_JOB_ID');
  v_client_identifier             VARCHAR2(200):= sys_context('USERENV', 'CLIENT_IDENTIFIER');
  v_client_info                   VARCHAR2(200):= sys_context('USERENV', 'CLIENT_INFO');
  v_current_bind                  VARCHAR2(200):= sys_context('USERENV', 'CURRENT_BIND');
  v_current_schema                VARCHAR2(200):= sys_context('USERENV', 'CURRENT_SCHEMA');
  v_current_schemaid             VARCHAR2(200):= sys_context('USERENV', 'CURRENT_SCHEMAID');
  v_current_sql                   VARCHAR2(200):= sys_context('USERENV', 'CURRENT_SQL');
  v_current_sql_length            VARCHAR2(200):= sys_context('USERENV', 'CURRENT_SQL_LENGTH');
  v_db_domain                     VARCHAR2(200):= sys_context('USERENV', 'DB_DOMAIN');
  v_db_name                       VARCHAR2(200):= sys_context('USERENV', 'DB_NAME');
  v_db_unique_name                VARCHAR2(200):= sys_context('USERENV', 'DB_UNIQUE_NAME');
  v_entryid                       VARCHAR2(200):= sys_context('USERENV', 'ENTRYID');
  v_enterprise_identity           VARCHAR2(200):= sys_context('USERENV', 'ENTERPRISE_IDENTITY');
  v_fg_job_id                     VARCHAR2(200):= sys_context('USERENV', 'FG_JOB_ID');
  v_global_context_memory         VARCHAR2(200):= sys_context('USERENV', 'GLOBAL_CONTEXT_MEMORY');
  v_global_uid                    VARCHAR2(200):= sys_context('USERENV', 'GLOBAL_UID');
  v_HOST                          VARCHAR2(200):= sys_context('USERENV', 'HOST');
  v_identification_type           VARCHAR2(200):= sys_context('USERENV', 'IDENTIFICATION_TYPE');
  v_instance                      VARCHAR2(200):= sys_context('USERENV', 'INSTANCE');
  v_instance_name                 VARCHAR2(200):= sys_context('USERENV', 'INSTANCE_NAME');
  v_ip_address                    VARCHAR2(200):= sys_context('USERENV', 'IP_ADDRESS');
  v_ISDBA                         VARCHAR2(200):= sys_context('USERENV', 'ISDBA');
  v_lang                          VARCHAR2(200):= sys_context('USERENV', 'LANG');
  v_language                      VARCHAR2(200):= sys_context('USERENV', 'LANGUAGE');
  v_MODULE                        VARCHAR2(200):= sys_context('USERENV', 'MODULE');
  v_network_protocol              VARCHAR2(200):= sys_context('USERENV', 'NETWORK_PROTOCOL');
  v_nls_calendar                  VARCHAR2(200):= sys_context('USERENV', 'NLS_CALENDAR');
  v_nls_currency                  VARCHAR2(200):= sys_context('USERENV', 'NLS_CURRENCY');
  v_nls_date_format               VARCHAR2(200):= sys_context('USERENV', 'NLS_DATE_FORMAT');
  v_nls_date_language             VARCHAR2(200):= sys_context('USERENV', 'NLS_DATE_LANGUAGE');
  v_nls_sort                      VARCHAR2(200):= sys_context('USERENV', 'NLS_SORT');
  v_nls_territory                 VARCHAR2(200):= sys_context('USERENV', 'NLS_TERRITORY');
  v_OS_USER                       VARCHAR2(200):= sys_context('USERENV', 'OS_USER');
  v_policy_invoker                VARCHAR2(200):= sys_context('USERENV', 'POLICY_INVOKER');
--  v_proxy_enterprise_id_entity    VARCHAR2(200):= sys_context('USERENV', 'PROXY_ENTERPRISE_ID_ENTITY');
--  v_PROXY_global_uid               VARCHAR2(200):= sys_context('USERENV', 'PROXY_GLOBAL_UID');
  v_PROXY_USER                    VARCHAR2(200):= sys_context('USERENV', 'PROXY_USER');
  v_PROXY_USERid                  VARCHAR2(200):= sys_context('USERENV', 'PROXY_USERID');
  v_server_host                   VARCHAR2(200):= sys_context('USERENV', 'SERVER_HOST');
  v_service_name                  VARCHAR2(200):= sys_context('USERENV', 'SERVICE_NAME');
  v_SESSION_USER                  VARCHAR2(200):= sys_context('USERENV', 'SESSION_USER');
  v_SESSION_USERID                VARCHAR2(200):= sys_context('USERENV', 'SESSION_USERID');
  v_SESSIONID                     VARCHAR2(200):= sys_context('USERENV', 'SESSIONID');
  v_SID                           VARCHAR2(200):= sys_context('USERENV', 'SID');
  v_Statementid                   VARCHAR2(200):= sys_context('USERENV', 'STATEMENTID');
  v_TERMINAL                      VARCHAR2(200):= sys_context('USERENV', 'TERMINAL');


BEGIN
-- check op users
  if  v_SESSION_USER  in ('DBSNMP','HELIX','SYS','SYSTEM','SYSMAN','PUBLIC','DISCO'
                         ,'DISCO_CURSUS') then
    null;

  elsif upper(v_module)  in ('FRMWEB.EXE','PERL.EXE','OMS','EMAGENT.EXE') then
    null;

  ELSE
   --bewaar de login gegevens
      insert into helix.BEH_LOG_IN_GEGEVENS(
      k_action
      ,k_audited_cursorid
      ,k_authenticated_identity
      ,k_authentication_data
      ,k_authentication_method
      ,k_bg_job_id
      ,k_client_identifier
      ,k_client_info
      ,k_current_bind
      ,k_current_schema
      ,k_current_schemaid
      ,k_current_sql
      ,k_current_sql_length
      ,k_db_domain
      ,k_db_name
      ,k_db_unique_name
      ,k_entryid
      ,k_enterprise_identity
      ,k_fg_job_id
      ,k_global_context_memory
      ,k_global_uid
      ,k_HOST
      ,k_identification_type
      ,k_instance
      ,k_instance_name
      ,k_ip_address
      ,k_ISDBA
      ,k_lang
      ,k_language
      ,k_MODULE
      ,k_network_protocol
      ,k_nls_calendar
      ,k_nls_currency
      ,k_nls_date_format
      ,k_nls_date_language
      ,k_nls_sort
      ,k_nls_territory
      ,k_OS_USER
      ,k_policy_invoker
    --  ,k_proxy_enterprise_id_entity
    --  ,k_PROXY_global_uid
      ,k_PROXY_USER
      ,k_PROXY_USERid
      ,k_server_host
      ,k_service_name
      ,k_SESSION_USER
      ,k_SESSION_USERID
      ,k_SESSIONID
      ,k_SID
      ,k_Statementid
      ,k_TERMINAL
      ,toegestaan_tegengehouden
    )
       values (  v_action
      ,v_audited_cursorid
      ,v_authenticated_identity
      ,v_authentication_data
      ,v_authentication_method
      ,v_bg_job_id
      ,v_client_identifier
      ,v_client_info
      ,v_current_bind
      ,v_current_schema
      ,v_current_schemaid
      ,v_current_sql
      ,v_current_sql_length
      ,v_db_domain
      ,v_db_name
      ,v_db_unique_name
      ,v_entryid
      ,v_enterprise_identity
      ,v_fg_job_id
      ,v_global_context_memory
      ,v_global_uid
      ,v_HOST
      ,v_identification_type
      ,v_instance
      ,v_instance_name
      ,v_ip_address
      ,v_ISDBA
      ,v_lang
      ,v_language
      ,v_MODULE
      ,v_network_protocol
      ,v_nls_calendar
      ,v_nls_currency
      ,v_nls_date_format
      ,v_nls_date_language
      ,v_nls_sort
      ,v_nls_territory
      ,v_OS_USER
      ,v_policy_invoker
    --  ,v_proxy_enterprise_id_entity
    --  ,v_PROXY_global_uid
      ,v_PROXY_USER
      ,v_PROXY_USERid
      ,v_server_host
      ,v_service_name
      ,v_SESSION_USER
      ,v_SESSION_USERID
      ,v_SESSIONID
      ,v_SID
      ,v_Statementid
      ,v_TERMINAL
      ,p_toegestaan_tegengehouden
    );
  end if;
  commit;
  exception
  when others then
    raise_application_error(-20000, 'Fout in helix.beh_secu_00.bewaar_log_in_gegevens: ' || SQLERRM);

end bewaar_log_in_gegevens;

procedure check_bevoegdheid

is
  v_not_authorized   exception;
  v_module           varchar2(200) := null;
  v_session_user     varchar2(200) := null;
  v_cnt_session_user         number        := 0;
  v_cnt_module       number        := 0;
  v_cnt_session_user_module  number        := 0;
  v_toegestaan_tegengehouden varchar2(20) := null;


--
begin

  v_module       := UPPER(sys_context('USERENV', 'MODULE'));
  v_session_user := UPPER(sys_context('USERENV', 'SESSION_USER'));


/*  --check op gebruik van module
  select count(*) into v_cnt_module from beh_bevoegde_GEBR_prgr where  k_module = v_module and k_session_user = 'ALLE';

  IF v_cnt_MODULE = 0 then

    --check op gebruiker
    select count(*) into v_cnt_session_user from beh_bevoegde_GEBR_prgr where k_module = 'ALLE' and k_session_user = v_session_user;

    IF  v_cnt_session_user = 0 then

      --check op combinatie gebruiker en module
      select count(*) into v_cnt_session_user_module from beh_bevoegde_GEBR_prgr where k_module = v_module and k_session_user = v_session_user;

      if  v_cnt_session_user_module  = 0 then

        v_toegestaan_tegengehouden := 'TEGENGEHOUDEN';
        bewaar_log_in_gegevens(v_toegestaan_tegengehouden);
        raise v_not_authorized;

      end if;



    end if;


  end if;
 -- registreer overige logins
  v_toegestaan_tegengehouden := 'TOEGESTAAN';
  bewaar_log_in_gegevens(v_toegestaan_tegengehouden);

*/
exception

  when v_not_authorized then
    raise_application_error(-20000, 'U bent niet bevoegd om op deze manier in te loggen. Neem contact op met het Service Team Genetica: ' );

  when others then
    raise_application_error(-20000, 'Fout in helix.beh_secu_00.bewaar_log_in_gegevens: ' || SQLERRM);

end check_bevoegdheid;

END beh_secu_00;
/

/
QUIT
