CREATE OR REPLACE PACKAGE "HELIX"."BEH_EAS_00" is
-- ten behoeve van EAS
--
--
function bepaal_omimnr
--aan de hand van de url het omimnr bepalen
(
p_url in varchar2
)
return varchar2;
end beh_eas_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_EAS_00" is
function bepaal_omimnr
(
  p_url in varchar2
)
  return    varchar2 is

  v_omimnr varchar2(30) := null;
  t number := 5;
begin

  if  beh_alfu_00.is_nummer(substr(p_url, length(p_url)- t)) = 'J'
  then v_omimnr :=  substr(p_url, length(p_url)- t);
  end if;
  return v_omimnr;
  exception
  when others
  then return 'X';
end;

end beh_eas_00;
/

/
QUIT
