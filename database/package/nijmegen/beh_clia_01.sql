CREATE OR REPLACE PACKAGE "HELIX"."BEH_CLIA_01" is

TYPE hl7_comp IS TABLE OF CLOB;
TYPE hl7_comp_seq IS TABLE OF hl7_comp;
TYPE hl7_seg IS TABLE OF hl7_comp_seq;
TYPE hl7_msg IS TABLE OF hl7_seg INDEX BY VARCHAR2(10); -- nnn_PID, nnn_MSH, ...

g_cr        VARCHAR2(1) := chr(13); -- segment-sep
g_sob_char  VARCHAR2(1) := chr(11); -- start of block
g_eob_char  VARCHAR2(1) := chr(28); -- end of block
g_term_char VARCHAR2(1) := chr(13);

g_sypa_hl7_versie   VARCHAR2(30);
g_sypa_zis_mail_err VARCHAR2(30) := 'ZIS_MAIL_ERR';

g_log_tekst beh_clia_hl7_berichten.log_tekst%type;
g_clhb_id beh_clia_hl7_berichten.clhb_id%type;
g_bericht_tekst beh_clia_hl7_berichten.bericht_tekst%type;
procedure insert_bericht
(
p_bericht_tekst in clob
);
function str2hl7msg
(
p_hlbe_id number,
p_str clob
)
return  hl7_msg
;
FUNCTION init_msg
(
p_seg_code   VARCHAR2,
p_hl7_versie VARCHAR2
) RETURN hl7_seg
;
  FUNCTION getsegmentcode(p_seg_code       VARCHAR2
                          ,p_hl7_msg        hl7_msg
                          ,p_seg_code_after VARCHAR2 := NULL) RETURN VARCHAR2;
procedure start_verwerking
;
procedure start_verwerking_via_job
;
end beh_clia_01;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_CLIA_01" is
procedure insert_bericht
-- wordt vanuit merith connect aangeroepen
(
p_bericht_tekst in clob
) is
begin
insert into beh_clia_hl7_berichten
(
clhb_id,
meet_id,
bericht_tekst,
bericht_type
)
values
(
beh_clhb_seq.nextval,
-99,
(chr(11) || p_bericht_tekst || chr(13) ||chr(28)),
'ORU'
)
;
end insert_bericht;
-- gecopieerd van KGC_ZIS.INIT_MSG
-- verschil zit datatype van het bericht.
-- hier wordt met datatype clob gewerkt.
-- bijv.   v_hl7_seg  hl7_seg
-- dit omdat clia berichten soms veel te groot zijn
-- voor varchar datatype.
FUNCTION init_msg(p_seg_code   VARCHAR2
                    ,p_hl7_versie VARCHAR2) RETURN hl7_seg IS
      v_hl7_seg  hl7_seg := hl7_seg();
      v_nrofcomp NUMBER := 0;
      v_seg_code VARCHAR2(10) := p_seg_code;
   BEGIN
      IF length(v_seg_code) = 7
      THEN
         -- bv 001_MSH
         v_seg_code := substr(v_seg_code
                             ,5);
      END IF;
      --  dbms_output.put_line('seg_code: ' || v_seg_code || '; hl7-versie: ' || p_hl7_versie);
      -- ADT = Admission, Discharge and Transfer
      -- DFT = Detail Financial Transaction
      -- hoeveel composites heeft het segment?
      IF v_seg_code = 'EVN'
      THEN
         -- EVN-1 Event Type Code
         -- EVN-2 Recorded Date/Time
         -- EVN-3 Date/Time Planned Event
         -- EVN-4 Event Reason Code
         -- EVN-5 Operator ID
         -- EVN-6 Event Occurred
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 2;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 6;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 7;
         END IF;
         --> mantis 740
      ELSIF v_seg_code = 'ORC'
      THEN
--         v_nrofcomp := 17;
         v_nrofcomp := 21; -- aangepast door hbou ter test van hl7 clia
      ELSIF v_seg_code = 'OBR'
      THEN
--         v_nrofcomp := 32;
         v_nrofcomp := 39;  -- aangepast door hbou ter test van hl7 clia
      ELSIF v_seg_code = 'ZBR'
      THEN
         v_nrofcomp := 3;
      ELSIF v_seg_code = 'NTE'
      THEN
         v_nrofcomp := 3;
      ELSIF v_seg_code = 'OBX'
      THEN
--         v_nrofcomp := 14;
                  v_nrofcomp := 17;
         --< mantis 740
      ELSIF v_seg_code = 'IN1'
      THEN
         -- IN1-1 Set ID - insurance
         -- IN1-2 Insurance plan ID
         -- IN1-3 Insurance company ID
         -- IN1-4 Insurance company name
         -- IN1-5 Insurance company address
         -- IN1-12 Plan effective date Ingangsdatum
         -- IN1-13 Plan expiration date Afloopdatum
         -- IN1-15 Plan type - Verzekeringstype ^ H=hoofdverzekeringB=Bijverzekering
         -- IN1-36 Policy number
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 36;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 36; -- niet in gebruik!
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 36;
         END IF;
      ELSIF v_seg_code = 'IN2'
      THEN
        v_nrofcomp := 29; -- niet gebruikt, maar kan wel aangeboden worden!
      ELSIF v_seg_code = 'FT1'
      THEN
         -- financieel!
         -- FT1-1 Set ID FT1
         -- FT1-2 Transaction ID
         -- FT1-3 Transaction Batch ID
         -- FT1-4 Transaction Date
         -- FT1-5 Transaction Posting Date
         -- FT1-6 Transaction Type
         -- FT1-7 Transaction Code
         -- FT1-8 Transaction Description
         -- FT1-9 Transaction Description - Alt
         -- FT1-10 Transaction Quantity
         -- FT1-11 Transaction Amount - Extended
         -- FT1-12 Transaction Amount ? Unit
         -- FT1-13 Department Code
         -- FT1-14 Insurance Plan ID
         -- FT1-15 Insurance Amount
         -- FT1-16 Assigned Patient Location
         -- FT1-17 Fee Schedule
         -- FT1-18 Patient Type
         -- FT1-19 Diagnosis Code
         -- FT1-20 Performed By Code
         -- FT1-21 Ordered By Code
         -- FT1-22 Unit Cost
         -- FT1-23 Filler Order Number
         -- FT1-24 Entered By Code
         -- FT1-25 Procedure Code
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 21;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 25;
         END IF;
      ELSIF v_seg_code = 'MRG' -- alleen in 2.4
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 7;
         END IF;
      ELSIF v_seg_code = 'MSA'
      THEN
         -- MSA-1 Acknowlegdment Code
         -- MSA-2 Message Control ID
         -- MSA-3 Text Message
         -- MSA-4 Expected Sequence Number
         -- MSA-5 Delayed ACK Type
         -- MSA-6 Error Condition
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 4;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 6;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 6;
         END IF;
      ELSIF v_seg_code = 'MSH'
      THEN
         -- MSH-1 Field Separator: standaard | (hex 0D)
         -- MSH-2 Encoding Characters: standaard ^~\& ; ^=component separator; ~=repetition separator \=escape character; &=sub-component separator
         -- MSH-3 Sending Application; 1=prod, 2=test, 4=ontw cf. AZN richtlijn
         -- MSH-4 Sending Facility
         -- MSH-5 Receiving Application
         -- MSH-6 Receiving Facility (leeg)
         -- MSH-7 Date/Time of Message
         -- MSH-8 security
         -- MSH-9 Message Type
         -- MSH-10 Message Control ID
         -- MSH-11 Processing ID
         -- MSH-12 Version ID
         -- MSH-13 Sequence Number
         -- MSH-14 Continuation Pointer
         -- MSH-15 Accept Acknowledgement Type; leeg (umcn - ack-nack.doc)
         -- MSH-16 Application Acknowledgement Type; leeg (umcn - ack-nack.doc)
         -- MSH-17 Country Code
         -- MSH-18 Character Set
         -- MSH-19 Principal Language Of Message
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 16;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 19;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 19;
--            v_nrofcomp := 21; veranderd door hbo
         END IF;
      ELSIF v_seg_code = 'PD1'
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 21;
         END IF;
      ELSIF v_seg_code = 'PID'
      THEN
         -- PID-1 Set ID   PID
         -- PID-2 Patient ID (External ID)
         -- PID-3 Patient ID (Internal ID) -- in 2.4 ook de plek voor het BSN?!
         -- PID-4 Alternate Patient ID   PID
         -- PID-5 Patient Name
         -- PID-7 Date/Time of Birth
         -- PID-8 Sex
         -- PID-11 Patient Address
         -- PID-13 Phone Number Home
         -- PID-14 Phone Number-Business
         -- PID-15 Language Patient
         -- PID-16 Marital Status
         -- PID-17 Religion
         -- PID-21 Mother's Identifier
         -- PID-23 Birth Place
         -- PID-24 Multiple Birth Indicator
         -- PID-29 Patient Death Date
         -- PID-30 Pati? Death Indicator
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 24;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 30;
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 38; -- veranderd door hbo
--            v_nrofcomp := 38;
         END IF;
      ELSIF v_seg_code = 'PV1'
      THEN
         -- onderdeel DFT, niet zo boeiend
         -- Utrecht: ADTA08: 2 velden; DFT: niet gebruikt!
         -- Nijmegen: Niet in gebruik
         -- PV1-1 Set Id - PV1
         -- PV1-2 Patient Class
         -- PV1-3 Assigned Patient Location
         -- PV1-4 Admission Type
         -- PV1-5 Pre-Admit Number
         -- PV1-6 Prior Patient Location
         -- PV1-7 Attending Doctor
         -- PV1-8 Referring Doctor
         -- PV1-9 Consulting Doctor
         -- etc.
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 10; -- tbv ontvangst Utrecht (wordt niets mee gedaan); Maastricht?
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 52; -- maar: niet gebruikt in Nijmegen
         END IF;
      ELSIF v_seg_code = 'PV2'
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 47;
         END IF;
      ELSIF v_seg_code = 'QRD'
      THEN
         -- QRD-1 Query Date/Time
         -- QRD-2 Query Format Code
         -- QRD-3 Query Priority
         -- QRD-4 Query ID
         -- QRD-7 Quantity Limited Request
         -- QRD-8 Who subject filter
         -- QRD-9 What subject filter
         -- QRD-10 What Nursing unit data code
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 10;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 10; -- niet in gebruik
         ELSIF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 12;
         END IF;
      ELSIF v_seg_code = 'ROL'
      THEN
         IF p_hl7_versie = '2.4'
         THEN
            v_nrofcomp := 12;
         END IF;
      ELSIF v_seg_code = 'STF'
      THEN
         -- STF-1 PrimaryKeyValue - zie doc
         -- STF-2 Staff ID Code
         -- STF-3 Staff name
         -- STF-4 Staff Type
         -- STF-5 Sex
         -- STF-10 Phone
         -- STF-11 Office/Home Address
         -- STF-13 Inactivation Date
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 13;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 13; -- niet in gebruik!
         END IF;
      ELSIF v_seg_code = 'ZFU'
      THEN
         -- ZFU-7 Assigned Patient Location
         -- ZFU-8 Financial treatment rule
         -- ZFU-9 Ordering Institution
         -- ZFU-10 Relation to orderer
         -- ZFU-11 Internal requestor
         -- ZFU-12 Financial treatment = Aangeboden ABW
         -- ZFU-13 Reference number
         -- ZFU-14 Transaction description
         -- ZFU-15 Transaction quantity
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 15;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 0; -- niet in gebruik
         END IF;
      ELSIF v_seg_code = 'ZPI'
      THEN
         -- ZPI-1 Set ID   PID
         -- ZPI-2 Indicatie overleden
         -- ZPI-3 Geboorteplaats
         -- ZPI-4 Naam echtgenoot
         -- ZPI-5 Verwijzend patientnummer
         -- ZPI-6 Huisarts
         -- ZPI-7 Voorletters ouder
         -- ZPI-8 Girorekening
         -- ZPI-9 Bankrekening
         -- ZPI-10 Bloedgroep
         -- ZPI-11 Datum overleden
         -- ZPI-21 VIP Indicator
         -- ZPI-22 Huistandarts
         IF p_hl7_versie = '2.2'
         THEN
            v_nrofcomp := 11;
         ELSIF p_hl7_versie = '2.3'
         THEN
            v_nrofcomp := 22;
         END IF;
      END IF;
      -- initialiseren:
      IF v_nrofcomp > 0
      THEN
         -- mogelijk: hele msg niet boeiend!
         FOR i IN 1 .. v_nrofcomp
         LOOP
            v_hl7_seg.EXTEND;
            v_hl7_seg(i) := hl7_comp_seq(NULL);
            v_hl7_seg(i)(1) := hl7_comp(NULL);
         END LOOP;
      END IF;
      RETURN v_hl7_seg;
   END;
-- gecopieerd van KGC_ZIS.str2hl7msg
-- verschil zit datatype van het bericht.
-- hier wordt met datatype clob gewerkt.
-- bijv.   v_hl7_seg  hl7_seg
-- dit omdat clia berichten soms veel te groot zijn
-- voor varchar datatype.
FUNCTION str2hl7msg(p_hlbe_id NUMBER
                      ,p_str     clob)
RETURN hl7_msg IS
      v_hl7_msg_null      hl7_msg;
      v_hl7_msg           hl7_msg;
      v_seg_count         NUMBER := 0;
      v_pos               NUMBER;
      v_pos_next          NUMBER;
      v_pos_seg_eind      NUMBER;
      v_pos_comp_seq_eind NUMBER;
      v_comp_seq_count    NUMBER;
      v_pos_comp_eind     NUMBER;
      v_comp_count        NUMBER;
      v_pos_att_eind      NUMBER;
      v_att_count         NUMBER;
      v_seg_code          VARCHAR2(10);
      v_att_klaar         BOOLEAN;
      v_sep_field         VARCHAR2(1) := '|';
      v_sep_comp          VARCHAR2(1) := '^';
      v_sep_repeat        VARCHAR2(1) := '~';
      v_hl7_versie        VARCHAR2(10); -- deze zit in het bericht!!!
      v_versie_begin      NUMBER;
      v_versie_eind       NUMBER;
      v_versie_eind_cr    NUMBER;
      v_versie_eind_comp  NUMBER;
      v_debug             VARCHAR2(4000);
      v_thisproc          VARCHAR2(100) := 'beh_clia_01.str2hl7msg';

   BEGIN
      v_debug := '1';
      -- in deze functie staan een hoop foutmeldingen; na testen komen ze nooit meer voor! Dus ook niet opgenomen in beheer-doc!
      -- 1e positie: altijd g_sob_char
      IF nvl(substr(p_str
                   ,1
                   ,1)
            ,' ') <> g_sob_char
      THEN
         kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                              ,'beh_clia_01.str2hl7msg: Fout (database: ' || upper(database_name) || ')!'
                              ,'Bericht in kgc_hl7_berichten met id ' || to_char(p_hlbe_id) ||
                               ' begint niet met g_sob_char (CHR(11))');
         RETURN v_hl7_msg_null;
      END IF;
      v_debug := '2';
      v_pos   := 2;
      --dbms_output.put_line('start str2hl7msg');
      WHILE TRUE
      LOOP
         -- alle segmenten
         v_debug := '3';

         --   p.l( SUBSTR(p_str, v_pos ));
         IF substr(p_str
                  ,v_pos
                  ,1) = g_eob_char
         THEN
            -- klaar, laatste segment gehad!
            v_debug := substr(p_str
                             ,v_pos
                             ,-20);
            EXIT;
         END IF;
         v_debug := '4';
         -- hmmm sommige segmenten komen vaker voor (b.v PID in A24/A37, of IN1 in A08! Dus altijd nnn_ toevoegen!
         -- ook voor het uit elkaar houden van groepen is de volgorde van belang! Zie getSegmentCode
         v_seg_count := v_seg_count + 1;
         v_seg_code  := ltrim(to_char(v_seg_count
                                     ,'099')) || '_' || substr(p_str
                                                              ,v_pos
                                                              ,3);
         --dbms_output.put_line(v_seg_code);
         v_comp_seq_count := 0;
         v_debug          := '5';

         IF v_seg_code = '001_MSH'
         THEN
            -- vreemde eend (altijd vooraan!): bevat separators
            v_debug     := v_debug || 'a';
            v_sep_field := substr(p_str
                                 ,v_pos + 3
                                 ,1); -- default |
            -- bepaal v_hl7_versie! MSH-12
            v_debug          := v_debug || 'b';
            v_versie_begin   := instr(p_str
                                     ,v_sep_field
                                     ,1
                                     ,11);
            v_versie_eind    := instr(p_str
                                     ,v_sep_field
                                     ,v_versie_begin + 1); -- de volgende
            v_versie_eind_comp := instr(p_str
                                     ,v_sep_comp
                                     ,v_versie_begin + 1); -- mantis 283: in v2.4 staat versie in MSH12-1! (en ook 12-2 is gevuld)
            v_versie_eind_cr := instr(p_str
                                     ,g_cr
                                     ,v_versie_begin + 1); -- einde segment?
            IF v_versie_eind_cr > 0
               AND v_versie_eind_cr < v_versie_eind
            THEN
               v_versie_eind := v_versie_eind_cr; -- dan versie aan eind segment: begrensd door cr!
            END IF;
            -- mantis 283: in v2.4 staat versie in MSH12-1! (en ook 12-2 is gevuld)
            IF v_versie_eind_comp > 0
               AND v_versie_eind_comp < v_versie_eind
            THEN
               v_versie_eind := v_versie_eind_comp; -- dan versie als 1e in de composite
            END IF;
            v_hl7_versie := substr(p_str
                                  ,v_versie_begin + 1
                                  ,v_versie_eind - v_versie_begin - 1);
            v_debug := v_debug || 'c';
            v_hl7_msg(v_seg_code) := init_msg(v_seg_code
                                             ,v_hl7_versie);
            v_debug := v_debug || 'd';
            -- rest header
            v_hl7_msg(v_seg_code)(1)(1)(1) := v_sep_field;
            v_debug := v_debug || 'e';
            v_pos_comp_seq_eind := instr(p_str
                                        ,v_sep_field
                                        ,v_pos + 4);
            --   dbms_output.put_line('#msh2 ' || to_char(v_hl7_msg(v_seg_code)(2).COUNT));
            v_hl7_msg(v_seg_code)(2)(1)(1) := substr(p_str
                                                    ,v_pos + 4
                                                    ,v_pos_comp_seq_eind - v_pos - 4);
            v_debug := v_debug || 'f';
            v_sep_comp := substr(v_hl7_msg(v_seg_code) (2) (1) (1)
                                ,1
                                ,1);
            v_sep_repeat := substr(v_hl7_msg(v_seg_code) (2) (1) (1)
                                  ,2
                                  ,1);
            v_comp_seq_count := 2; -- de eerste 2 zijn 'special'!
            v_pos := v_pos_comp_seq_eind + 1;
         ELSE
            v_debug := v_debug || 'k';
            v_pos := v_pos + 4; -- segment-code + '|' overslaan
            v_hl7_msg(v_seg_code) := init_msg(v_seg_code
                                             ,v_hl7_versie);
            v_debug := v_debug || 'l';
         END IF;

         v_pos_seg_eind := instr(p_str
                                ,g_cr
                                ,v_pos); -- ieder segment wordt afgesloten door cr
         IF v_pos_seg_eind = 0
         THEN
            kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                                 ,'beh_clia_01.str2hl7msg: Fout (database: ' || upper(database_name) ||')!'
                                 ,'Bericht in kgc_hl7_berichten met id ' || to_char(p_hlbe_id) ||
                                  ' Segment-eind (CHR(13)) niet gevonden!');
            RETURN v_hl7_msg_null;
         END IF;
         v_debug := '6';
         -- alle composites opnemen
         WHILE TRUE
         LOOP
            -- alle composite_seq's
            IF v_pos > v_pos_seg_eind
            THEN
               EXIT;
            END IF;

            v_debug             := '7';
            v_pos_comp_seq_eind := instr(p_str
                                        ,'|'
                                        ,v_pos);
            IF v_pos_comp_seq_eind = 0
               OR v_pos_comp_seq_eind > v_pos_seg_eind
            THEN
               v_pos_comp_seq_eind := v_pos_seg_eind; -- laatste composite
            END IF;
            v_debug          := '8';
            v_comp_seq_count := v_comp_seq_count + 1;
            v_comp_count     := 0;
            WHILE TRUE
            LOOP
               -- alle composites in een composite_seq
               IF v_pos > v_pos_comp_seq_eind
               THEN
                  EXIT;
               END IF;
               v_debug         := '9';
               v_pos_comp_eind := instr(p_str
                                       ,'~'
                                       ,v_pos);

               IF v_pos_comp_eind = 0
                  OR v_pos_comp_eind > v_pos_comp_seq_eind
               THEN

                  v_pos_comp_eind := v_pos_comp_seq_eind; -- laatste composite in comp_seq

               END IF;
               v_debug      := '10';

               v_comp_count := v_comp_count + 1;
               v_att_count  := 0;
               v_att_klaar  := FALSE;
               WHILE TRUE
               LOOP
                  -- alle attributen in een composite

                  v_debug        := '11';
                  v_pos_att_eind := instr(p_str
                                         ,'^'
                                         ,v_pos);
                  IF v_pos_att_eind = 0
                     OR v_pos_att_eind > v_pos_comp_eind
                  THEN
                     v_pos_att_eind := v_pos_comp_eind; -- laatste attribuut in comp
                     v_att_klaar    := TRUE;
                  END IF;
                  v_debug := '11';

                  /*        if v_seg_code like '%XXX'
                            then
                              dbms_output.put_line( substr( p_str, v_pos, v_pos_att_eind ));
                            end if;
                  */
                  v_att_count := v_att_count + 1;
                  --  dbms_output.put_line('seg attr vnr: ' || to_char(v_att_count));
                  -- is het attribuut boeiend? Niet als er geen comp-plek meer is!

                  IF v_hl7_msg(v_seg_code).COUNT >= v_comp_seq_count
                  THEN
                     -- er is plek

                     --           v_debug := '12(' || to_char(v_hl7_msg(v_seg_code).COUNT) || ')';
                     IF v_comp_count > v_hl7_msg(v_seg_code) (v_comp_seq_count).COUNT
                     THEN
                        --              v_debug := '13(' || to_char(v_comp_count) || ';' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count).COUNT) || ')';
                        v_hl7_msg(v_seg_code)(v_comp_seq_count) .EXTEND; -- alleen de eerste is er!
                        v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count) := hl7_comp(NULL);
                     END IF;

                     --           v_debug := '15(' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count).COUNT) || ')';
                     IF v_att_count > v_hl7_msg(v_seg_code) (v_comp_seq_count) (v_comp_count).COUNT
                     THEN
                        --              v_debug := '16(' || to_char(v_comp_count) || ')';
                        v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count) .EXTEND; -- alleen de eerste is er!
                     END IF;
                     /*
                     dbms_output.put_line('Attr ' || to_char(v_comp_seq_count) ||  '/' || to_char(v_comp_count) ||  '/' || to_char(v_att_count) || ': ' || SUBSTR(p_str, v_pos, v_pos_att_eind - v_pos));
                     dbms_output.put_line('#comp_seq ' || to_char(v_hl7_msg(v_seg_code).COUNT)
                      || ' #comp ' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count).COUNT)
                      || ' #attr ' || to_char(v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count).COUNT));
                     */
                     -- geef attribuut een waarde:
                     v_debug := '17(' || v_seg_code || ';' || to_char(v_comp_seq_count) || '<=' ||
                                to_char(v_hl7_msg(v_seg_code).COUNT) || ';' || to_char(v_comp_count) || '<=' ||
                                to_char(v_hl7_msg(v_seg_code) (v_comp_seq_count).COUNT) || ';' || to_char(v_att_count) || '<=' ||
                                to_char(v_hl7_msg(v_seg_code) (v_comp_seq_count) (v_comp_count).COUNT) || ')';

                     v_hl7_msg(v_seg_code)(v_comp_seq_count)(v_comp_count)(v_att_count) := substr(p_str
                                                                                                 ,v_pos
                                                                                                 ,v_pos_att_eind -
                                                                                                  v_pos); -- hebbes!
                     /*           dbms_output.put_line(v_seg_code ||' - '
                                                                     || to_char(v_comp_seq_count)||' - '
                                                                     || to_char(v_comp_count)||' - '
                                                                     || to_char(v_att_count)||' - '
                                                                     || SUBSTR(p_str, v_pos, v_pos_att_eind - v_pos) );
                     */
                  ELSE
                     --            v_debug := '18(' || v_seg_code || ';'
                     --              || to_char(v_comp_seq_count) || '<=' || to_char(v_hl7_msg(v_seg_code).COUNT)
                     --              || ')';
                     NULL;
                  END IF;
                  v_pos := v_pos_att_eind + 1; -- v_pos naar 1 voorbij eind (daar staat ^ of ~ of | of cr)
                  IF v_att_klaar
                  THEN
                     EXIT;
                  END IF;
               END LOOP; -- alle alle attributen in een composite
               -- check:
               IF v_att_count = 0
               THEN
                  kgc_zis.post_mail_zis(g_sypa_zis_mail_err
                                       ,'beh_clia_01.str2hl7msg: Fout (database: ' || upper(database_name) || ')!'
                                       ,'Bericht in kgc_hl7_berichten met id ' || to_char(p_hlbe_id) ||
                                        ' Segment zonder attributen!');
                  RETURN v_hl7_msg_null;
               END IF;
            END LOOP; -- alle composites
         END LOOP; -- alle composite_seqs
      END LOOP; -- alle segementen
      --      dbms_output.put_line(v_debug);
      RETURN v_hl7_msg;
   EXCEPTION
      WHEN OTHERS THEN
         --  dbms_output.put_line(v_debug);
         raise_application_error(-20000
                                ,v_thisproc || ' debug: ' || v_debug || ' Error: ' || SQLERRM);
   END str2hl7msg;
-- gecopieerd van KGC_ZIS.getsegmentcode
-- verschil zit datatype van het bericht.
-- hier wordt met datatype clob gewerkt.
-- bijv.   v_hl7_seg  hl7_seg
-- dit omdat clia berichten soms veel te groot zijn
-- voor varchar datatype.
   FUNCTION getsegmentcode(p_seg_code       VARCHAR2
                          ,p_hl7_msg        hl7_msg
                          ,p_seg_code_after VARCHAR2 := NULL) RETURN VARCHAR2 IS
      v_seg_code          VARCHAR2(10);
      v_eerstvolgendeisok BOOLEAN := TRUE;
   BEGIN
      IF p_seg_code_after IS NOT NULL
      THEN
         v_eerstvolgendeisok := FALSE;
      END IF;
      v_seg_code := p_hl7_msg.FIRST; -- get subscript of first element
      WHILE v_seg_code IS NOT NULL
      LOOP
         IF substr(v_seg_code
                  ,1
                  ,3) = p_seg_code
            OR -- geen volgnummer
            substr(v_seg_code
                  ,5
                  ,3) = p_seg_code
         THEN
            -- wel volgnummer
            IF v_eerstvolgendeisok
            THEN
               RETURN v_seg_code;
            ELSIF p_seg_code_after = v_seg_code
            THEN
               v_eerstvolgendeisok := TRUE;
            END IF;
         END IF;
         v_seg_code := p_hl7_msg.NEXT(v_seg_code); -- get subscript of next element
      END LOOP;
      RETURN '';
   END;
procedure start_verwerking
is
loggen EXCEPTION;

v_hl7_msg    beh_clia_01.hl7_msg;

v_obr            VARCHAR2(10);
v_obx            VARCHAR2(10);

kary_trow kgc_karyogrammen%rowtype;
meet_trow bas_meetwaarden%rowtype;

v_serie varchar2(500);
v_modality varchar2(100);
v_opmerking varchar2(100);
v_keywords varchar2(100);
v_validation varchar2(1);

v_count number := 0;
v_meet_kwfr_cell_id varchar2(500);

v_afkomstig_van_mpf boolean := false;

v_meet_id_char         varchar2(200);
v_kwfr_code kgc_kweekfracties.code%type;
v_monsternummer kgc_monsters.monsternummer%type;
v_cel_char varchar2(100);
v_znr_mede_id varchar2(100);
v_znr_mede_id_controle varchar2(100);

v_samenvatting bas_metingen.conclusie%type;
v_obx_loop_count number := 1;
v_obx_geldig_count number := 0;
v_obx_volgnr varchar2(10);
v_obx_aantal number := beh_alfu_00.karakter_teller(g_bericht_tekst, 'OBX|');
v_debug varchar2(100);
v_data_bijwerken boolean := false;
v_obr_11_1_1 varchar2(500);
v_count_inner number := 0;

cursor c_list(str varchar2, separator varchar2) is
select *
from table(cast(beh_alfu_00.explode(separator, str) as beh_listtable))
;

cursor c_mede(p_znr varchar2) is
select *
from kgc_medewerkers
where upper(vms_username) = upper(p_znr)
;
r_mede c_mede%rowtype;

cursor c_kwfr
(p_monsternummer varchar2, p_meet_id number, p_kwfr_code varchar2) is
select kwfr.*
from kgc_monsters mons
,      bas_fracties frac
,      bas_metingen meti
,     bas_meetwaarden meet
,     kgc_kweekfracties kwfr
where mons.mons_id = frac.mons_id
and    frac.frac_id = meti.frac_id
and    meti.meti_id = meet.meti_id
and    frac.frac_id = kwfr.frac_id
and   mons.monsternummer = p_monsternummer
and   meet.meet_id = p_meet_id
and    kwfr.code =p_kwfr_code
;

r_kwfr c_kwfr%rowtype;

cursor c_meet (p_meet_id number) is
select *
from bas_meetwaarden
where meet_id = p_meet_id
;
r_meet c_meet%rowtype;

cursor c_afgerond (p_meet_id number) is
select *
from beh_pers_tm_meet_vw
where meet_meet_id = p_meet_id
;
r_afgerond c_afgerond%rowtype;

cursor c_kary(p_meet_id number, p_kwfr_id number, p_cel number) is
select count(*) aantal
from kgc_karyogrammen
where meet_id = p_meet_id
and     kwfr_id = p_kwfr_id
and    cel = p_cel
;
r_kary c_kary%rowtype;

begin
g_log_tekst := '';
-- het bericht wordt weggeschreven in allerlei variabellen
v_hl7_msg := beh_clia_01.str2hl7msg( g_clhb_id, g_bericht_tekst);
v_obr   := beh_clia_01.getsegmentcode('OBR' ,v_hl7_msg);

v_debug := '1';

--v_monsternummer := v_hl7_msg(v_obr) (11) (1) (1);
-- voorbeeld waarde: M14-05204 (14763391)
v_obr_11_1_1 := v_hl7_msg(v_obr) (11) (1) (1);
v_monsternummer := trim(substr(v_obr_11_1_1, 0, instr(v_obr_11_1_1, ' ')));
v_znr_mede_id   := v_hl7_msg(v_obr) (34) (1) (1);
v_znr_mede_id_controle  := v_hl7_msg(v_obr) (32) (1) (3);
v_meet_id_char := v_hl7_msg(v_obr) (3) (1) (1);

v_debug := '2';

meet_trow.meet_id :=
        to_number(substr(v_meet_id_char, instr(v_meet_id_char, '_')+1));

v_debug := '3';

open c_meet(meet_trow.meet_id) ;
fetch c_meet into r_meet;
close c_meet;

--voorbeeld waarde van v_znr_mede_id_controle:
--z268164 17-11-2011 15:21:47
-- we hebben alleen het znr nodig
v_debug := '4';
if r_meet.meet_id is not null then

    open c_afgerond(meet_trow.meet_id) ;
    fetch c_afgerond into r_afgerond;
    close c_afgerond;

    if r_afgerond.onde_afgerond = 'J' then
        g_log_tekst := g_log_tekst || '$1-onderzoeknr: ' || r_afgerond.onde_onderzoeknr || ' is al afgerond.';
    end if;
    if r_afgerond.meti_afgerond = 'J' then
        g_log_tekst :=  g_log_tekst || '$2-Protocol: ' || r_afgerond.stgr_code || ' (' || to_char(r_afgerond.meti_meti_id) || ')  ' || 'is al afgerond.';
    end if;
else
        g_log_tekst := g_log_tekst || '$3-meet_id: ' || to_char(meet_trow.meet_id) || ' niet gevonden.';
end if;
v_debug := '5';
open c_mede(v_znr_mede_id);
fetch c_mede into r_mede;
close c_mede;

meet_trow.mede_id := r_mede.mede_id;
v_debug := '6';
if meet_trow.mede_id is null then
    g_log_tekst := g_log_tekst || '$4-znr: ' || v_znr_mede_id || ' niet gevonden.';
end if;
v_debug := '7';
v_znr_mede_id_controle :=
    trim(substr(v_znr_mede_id_controle, 0, instr(v_znr_mede_id_controle, ' ')));
v_debug := '8';
open c_mede(v_znr_mede_id_controle);
fetch c_mede into r_mede;
close c_mede;

meet_trow.mede_id_controle := r_mede.mede_id;
v_debug := '9';
if meet_trow.mede_id_controle is null then
    g_log_tekst := g_log_tekst || '$5-znr: ' || v_znr_mede_id_controle || ' niet gevonden.';
end if;

if g_log_tekst is not null then
    raise loggen;
end if;
v_debug := '10';
-- alle validations checken.
-- om records in Helix bij te werken, moeten alle valdaties F zijn.
while v_obx_loop_count <= v_obx_aantal loop
    v_validation := '';
    if v_obx_loop_count = 1 then
        v_obx   :=  beh_clia_01.getsegmentcode('OBX' ,v_hl7_msg);
    else
       v_obx   :=  beh_clia_01.getsegmentcode('OBX' ,v_hl7_msg, v_obx);
    end if;
    v_validation := upper(v_hl7_msg(v_obx) (11) (1) (1));
    if v_validation != 'F' then
        exit;
   end if;
    v_obx_loop_count := v_obx_loop_count+1;
end loop;
v_debug := '11';
-- loop op basis van aantal OBX segmenten
-- alleen als validation = 'F'
 if v_validation = 'F' then

    v_obx_loop_count := 1;
    v_debug := '12';
    while v_obx_loop_count <= v_obx_aantal loop

        v_debug := '12_' || to_char(v_obx_loop_count);
        v_count := 0;
        v_kwfr_code := '';
        v_cel_char := '';
        kary_trow.cel := null;

       if v_obx_loop_count = 1 then
         v_obx := beh_clia_01.getsegmentcode('OBX' ,v_hl7_msg);
       else
          v_obx := beh_clia_01.getsegmentcode('OBX' ,v_hl7_msg, v_obx);
       end if;

        if ( ( upper(v_hl7_msg(v_obx) (3) (1) (2) ) like '%ANALYSECEL%') or
             ( upper(v_hl7_msg(v_obx) (3) (1) (2) ) like '%EQAS%') ) then -- toegestane modalities

             v_obx_geldig_count := v_obx_geldig_count+1;
             v_debug := '12_' || to_char(v_obx_loop_count) || '_' || to_char(v_obx_geldig_count);

            if v_obx_geldig_count = 1 then

             if r_meet.afgerond = 'J' then
                 update bas_meetwaarden
                 set afgerond = 'N'
                 where meet_id = r_meet.meet_id
                 ;
            end if;
            v_data_bijwerken := true; -- alleen data bijwerken als tenminste ��n OBX is verwerkt.
           end if;
            v_obx_volgnr  := v_hl7_msg(v_obx) (1) (1) (1);
            -- clia gegevens
            v_serie := v_hl7_msg(v_obx) (3) (1) (1);
            v_modality := v_hl7_msg(v_obx) (3) (1) (2);
            v_opmerking := v_hl7_msg(v_obx) (3) (1) (3);
            -- enters en new line feeds worden weggehaald
--            v_opmerking := replace ( replace (v_opmerking, chr(10) ), chr(13));

            v_keywords := v_hl7_msg(v_obx) (4) (1) (1);
--            kary_trow.tekst_chromosomen := v_hl7_msg(v_obx) (5) (1) (1);
            -- enters en new line feeds worden weggehaald
            kary_trow.tekst_chromosomen := replace ( replace (v_hl7_msg(v_obx) (5) (1) (1), chr(10), ' ' ), chr(13), ' ');

            for r_list in c_list(v_opmerking, '-') loop
                v_count := v_count+1;
                if v_count = 1 then
                    v_kwfr_code := trim(r_list.column_value);
                elsif v_count = 2 then
                    v_cel_char := trim(r_list.column_value);
                    kary_trow.cel := to_number(substr(v_cel_char, 5));
                else
                    null;
                end if;
            end loop;

            -- voorbeeld waarde van v_serie (als afkomstig van mpf is) :
            -- 9479857.445100.382
            if (beh_alfu_00.karakter_teller(v_serie, '.') = 2) then
                v_afkomstig_van_mpf := true;
            else
                v_afkomstig_van_mpf := false;
            end if;

            if (v_afkomstig_van_mpf) then
                v_count := 0;
                    -- gegevens, afkomstig van mpf bevatten de kwfr_id...
                for r_list in c_list(v_serie, '.') loop
                    v_count := v_count+1;
                   if v_count = 2 then
------------------------begin mantisnr 13538
                        -- voorbeeld: 19846238.843463_639634.760
                        if instr(r_list.column_value, '_') > 0 then
                          v_count_inner := 0;
                          for r_list_inner in (select * from table(cast(beh_alfu_00.explode('_', r_list.column_value) as beh_listtable)) ) loop
                            v_count_inner := v_count_inner + 1;
                            if v_count_inner = 2 then
                              kary_trow.kwfr_id := to_number(r_list_inner.column_value);
                            end if;
                          end loop;
                          -- voorbeeld: 19846238.639634.760
                        else
                          kary_trow.kwfr_id := to_number(r_list.column_value);
                        end if;
-----------------------eind aanpassing mantisnr 13538
                    end if;
                end loop;
            else
                    -- ...maar handmatig ingevoerde gegevens in clia hebben geen kwfr_id
                    -- deze moet eerst opgehaald worden.
                    open c_kwfr(v_monsternummer, r_meet.meet_id, v_kwfr_code) ;
                    fetch c_kwfr into r_kwfr;
                        kary_trow.kwfr_id := r_kwfr.kwfr_id;
                    close c_kwfr;
            end if;

            if upper(v_keywords) = 'KARYOGRAM' then
                kary_trow.opname_k := 'J';
            elsif upper(v_opmerking) = 'METAFASE' then
                kary_trow.opname := 'J';
            else
                kary_trow.opname_k := '';
                kary_trow.opname := '';
                null;
            end if;
            --voorbeeld van relevante waarde =  EQAS: 1-2
            -- alle andere waarden zijn niet interessant
            if (beh_alfu_00.karakter_teller(v_modality, ':') = 1) then
                kary_trow.kwaliteit  := trim(substr(v_modality, instr(v_modality, ':')+1));
            else
                kary_trow.kwaliteit  := '';
            end if;

            open c_kary(r_meet.meet_id, kary_trow.kwfr_id, kary_trow.cel) ;
            fetch c_kary into r_kary;
            close c_kary;

            if r_kary.aantal > 0 then

                update kgc_karyogrammen
                set opname = nvl(kary_trow.opname, 'N'),
                kwaliteit =  kary_trow.kwaliteit,
                opname_k = nvl(kary_trow.opname_k, 'N'),
                tekst_chromosomen =  kary_trow.tekst_chromosomen
                where meet_id =r_meet.meet_id
                and     kwfr_id = kary_trow.kwfr_id
                and    cel =  kary_trow.cel
                ;

            else
                if ( (r_meet.meet_id is null) or (kary_trow.kwfr_id is null) or (kary_trow.cel is null) ) then
                    g_log_tekst := g_log_tekst ||
                                        ' update/insert op KARY is niet mogelijk. ��n van de onderstaande kolommen heeft geen waarde:  ' ||
                                        ', kolom meet_id: waarde => ' || to_char(r_meet.meet_id) ||
                                        ', kolom kwfr_id: waarde => ' || to_char(kary_trow.kwfr_id) ||
                                        ', kolom cel: waarde => ' || to_char(kary_trow.cel);
                     raise loggen;
               else
                    insert into kgc_karyogrammen
                    (
                    meet_id,
                    kwfr_id,
                    cel,
                    opname,
                    kwaliteit,
                    opname_k,
                    tekst_chromosomen
                    )
                    values
                    (
                    r_meet.meet_id,
                    kary_trow.kwfr_id,
                    kary_trow.cel,
                    nvl(kary_trow.opname, 'N'),
                    kary_trow.kwaliteit,
                    nvl(kary_trow.opname_k, 'N'),
                    kary_trow.tekst_chromosomen
                    )
                    ;
                end if;
            end if;

        end if;
       v_obx_loop_count := v_obx_loop_count+1;
    end loop;

    if v_data_bijwerken = true then
        v_samenvatting := beh_kgc_kary_00.samenvatting(r_meet.meet_id);

        update bas_meetwaarden
        set mede_id = nvl(meet_trow.mede_id, mede_id),
        mede_id_controle = nvl(meet_trow.mede_id_controle, mede_id_controle),
        meetwaarde =  v_samenvatting/*, -- op verzoek van Marzena (in overleg met gebruiker becomentarieerd.
        afgerond = 'J'*/
        where meet_id = r_meet.meet_id
        ;
        if  v_samenvatting is not null then
            update bas_metingen
            set conclusie = v_samenvatting
            where meti_id = r_meet.meti_id
            ;
        end if;
        g_log_tekst :=  g_log_tekst || v_validation || ' bericht: Database (kary, meet, meti) bijgewerkt.';
    else
        g_log_tekst :=  g_log_tekst || v_validation || ' valt niks te verwerken: geen geldige modality.';
    end if;

elsif v_validation = 'X' then

    if r_meet.afgerond = 'J' then
        update bas_meetwaarden
        set afgerond = 'N'
        where meet_id = r_meet.meet_id
        ;
    else
        null;
    end if;
    g_log_tekst :=  g_log_tekst || v_validation || ' bericht: Database (meet) bijgewerkt.';
else -- noch 'F' en noch 'X'
    g_log_tekst :=  g_log_tekst || v_validation || ' bericht: Database niet bijgewerkt.';
end if;
-- gegevens zijn verwerkt, dit wordt ook in beh_clia_hl7_berichten verwerkt.

update beh_clia_hl7_berichten
set verzonden = 'J',
meet_id = r_meet.meet_id,
log_tekst = log_tekst || g_log_tekst
where clhb_id = g_clhb_id
;
commit;
--###################################
exception
    when loggen then
        rollback;

        update beh_clia_hl7_berichten
        set log_tekst =  v_debug || log_tekst || g_log_tekst
        where clhb_id = g_clhb_id
        ;
        commit;
    when others then
        g_log_tekst :=  to_char(sysdate, 'dd-mm-yyyy HH24:MI:SS') || ' => 1- ' || trim(substr(sqlerrm, 1, 2500)) || ';' || v_debug || '$';
        rollback;

        update beh_clia_hl7_berichten
        set log_tekst = v_debug || log_tekst || g_log_tekst
        where clhb_id = g_clhb_id
        ;
        commit;
end start_verwerking;
procedure start_verwerking_via_job is
--v_peildatum date := beh_alfu_00.jobs_last_date('beh_clia_01.start_verwerking_via_job');
--v_peildatum date := to_date('07-03-2012 08:00:00', 'dd-mm-yyyy HH24:MI:SS');

cursor c_clhb is
select *
from beh_clia_hl7_berichten
where upper(bericht_type) = 'ORU'
and verzonden = 'N'
and log_tekst is null
--and (creation_date >= v_peildatum or log_tekst is null)
/*
union
select *
from beh_clia_hl7_berichten
where clhb_id in(108702, 108714)
*/
;

begin

for r_clhb in c_clhb loop

    g_clhb_id := r_clhb.clhb_id;

    g_bericht_tekst := r_clhb.bericht_tekst;

    start_verwerking;

end loop;

exception
 when others then
     g_log_tekst  :=  g_log_tekst || '$' ||  to_char(sysdate, 'dd-mm-yyyy HH24:MI:SS') || ' => 0 - ' || trim(substr(sqlerrm, 1, 2500)) || '$';

    update beh_clia_hl7_berichten
    set log_tekst = log_tekst || g_log_tekst
    where clhb_id = g_clhb_id
    ;
    commit;
end start_verwerking_via_job;
end beh_clia_01;
/

/
QUIT
