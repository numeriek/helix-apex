create or replace PACKAGE beh_auto_uits is

  -- bennodigdheden voor dirlist
   TYPE t_filedesc IS RECORD
   ( naam VARCHAR2(1000)
   , type VARCHAR2(1) -- D=Directory, F=File, anders ?
   , datum DATE
   , lengte NUMBER);
   TYPE t_tab_filedesc IS TABLE OF t_filedesc;

   FUNCTION dirlist_java( p_dir     IN  VARCHAR2
                        , p_filter  IN  VARCHAR2
                        , p_getattr IN  VARCHAR2
                        , x_dirlist IN OUT OAARGTABLE
                        ) RETURN VARCHAR2 IS
   LANGUAGE JAVA
   NAME 'Bestanden2.DirList(java.lang.String, java.lang.String, java.lang.String, oracle.sql.ARRAY[]) return java.lang.String';

   FUNCTION dirlist( p_dir       IN  VARCHAR2
                   , p_filter    IN  VARCHAR2 := NULL
                   , p_getattr   IN  VARCHAR2 := 'J'
                   , p_sypa_unpw IN  VARCHAR2 := NULL
                   , x_dirlist OUT t_tab_filedesc
                   ) RETURN VARCHAR2;

  -- copy uit https://github.com/mortenbra/alexandria-plsql-utils, package csv_util_pkg
  type t_str_array is table of varchar2(4000);
  g_default_separator            constant varchar2(1) := ',';
  -- convert CSV line to array of values
  function csv_to_array (p_csv_line in varchar2,
                         p_separator in varchar2 := g_default_separator) return t_str_array;


  --############################################################################
  --# Lees het opgegeven bestand met JSI meetwaarden in.
  --############################################################################
  procedure jsi_meetwaarden_inlezen
  ( p_locatie in varchar2
  , p_bestand in varchar2
  , p_kafd_code in varchar2 := null
  );

  --############################################################################
  --# Scan de opgegeven directory <map> en verwerk de gevonden jsi meetwaarden
  --# bestanden. Na het verwerken wordt het bestand verplaatst naar directory 
  --# <map>\verwerkt als het bestand correct verwerkt is en <map>\fout als er
  --# een fout is opgetreden.
  --############################################################################
  procedure scan_directory
  ( p_path in varchar2
  , p_kafd_code in varchar2 := null
  );

end beh_auto_uits;
/
create or replace PACKAGE BODY beh_auto_uits is

  v_crlf varchar2(2) := chr(13)||chr(10);
  v_sep  varchar2(1) := chr(9);



  -- copy uit https://github.com/mortenbra/alexandria-plsql-utils, package csv_util_pkg
  function csv_to_array (p_csv_line in varchar2,
                         p_separator in varchar2 := g_default_separator) return t_str_array
  as
    l_returnvalue      t_str_array     := t_str_array();
    l_length           pls_integer     := length(p_csv_line);
    l_idx              binary_integer  := 1;
    l_quoted           boolean         := false;
    l_quote  constant  varchar2(1)     := '"';
    l_start            boolean := true;
    l_current          varchar2(1 char);
    l_next             varchar2(1 char);
    l_position         pls_integer := 1;
    l_current_column   varchar2(32767);
  
    --Set the start flag, save our column value
    procedure save_column is
    begin
      l_start := true;
      l_returnvalue.extend;
      l_returnvalue(l_idx) := l_current_column;
      l_idx := l_idx + 1;
      l_current_column := null;
    end save_column;
  
    --Append the value of l_current to l_current_column
    procedure append_current is
    begin
      l_current_column := l_current_column || l_current;
    end append_current;
  begin
  
    /*
  
    Purpose:      convert CSV line to array of values
  
    Remarks:      based on code from http://www.experts-exchange.com/Database/Oracle/PL_SQL/Q_23106446.html
  
    Who     Date        Description
    ------  ----------  --------------------------------
    MBR     31.03.2010  Created
    KJS     20.04.2011  Modified to allow double-quote escaping
    MBR     23.07.2012  Fixed issue with multibyte characters, thanks to Vadi..., see http://code.google.com/p/plsql-utils/issues/detail?id=13
  
    */
  
    while l_position <= l_length loop
  
      --Set our variables with the current and next characters
      l_current := substr(p_csv_line, l_position, 1);
      l_next := substr(p_csv_line, l_position + 1, 1);
  
      if l_start then
        l_start := false;
        l_current_column := null;
  
        --Check for leading quote and set our flag
        l_quoted := l_current = l_quote;
  
        --We skip a leading quote character
        if l_quoted then goto loop_again; end if;
      end if;
  
      --Check to see if we are inside of a quote
      if l_quoted then
  
        --The current character is a quote - is it the end of our quote or does
        --it represent an escaped quote?
        if l_current = l_quote then
  
          --If the next character is a quote, this is an escaped quote.
          if l_next = l_quote then
  
            --Append the literal quote to our column
            append_current;
  
            --Advance the pointer to ignore the duplicated (escaped) quote
            l_position := l_position + 1;
  
          --If the next character is a separator, current is the end quote
          elsif l_next = p_separator then
  
            --Get out of the quote and loop again - we will hit the separator next loop
            l_quoted := false;
            goto loop_again;
  
          --Ending quote, no more columns
          elsif l_next is null then
  
            --Save our current value, and iterate (end loop)
            save_column;
            goto loop_again;
  
          --Next character is not a quote
          else
            append_current;
          end if;
        else
  
          --The current character is not a quote - append it to our column value
          append_current;
        end if;
  
      -- Not quoted
      else
  
        --Check if the current value is a separator, save or append as appropriate
        if l_current = p_separator then
          save_column;
        else
          append_current;
        end if;
      end if;
  
      --Check to see if we've used all our characters
      if l_next is null then
        save_column;
      end if;
  
      --The continue statement was not added to PL/SQL until 11g. Use GOTO in 9i.
      <<loop_again>> l_position := l_position + 1;
    end loop ;
  
    return l_returnvalue;
  end csv_to_array;

   FUNCTION dirlist( p_dir       IN  VARCHAR2
                   , p_filter    IN  VARCHAR2 := NULL -- bv *.pdf
                   , p_getattr   IN  VARCHAR2 := 'J' -- J/N: igv J wordt niet alleen de naam, maar ook type, datum en size gevuld
                   , p_sypa_unpw IN  VARCHAR2 := NULL -- evt sypa met un/pw voor toegang tot p_dir (bv igv ftp)
                   , x_dirlist OUT t_tab_filedesc
                   ) RETURN VARCHAR2 IS
      -- geeft lijst met bestandsnamen + evt attr terug in x_dirlist
      -- return OK igv alles goed; anders foutmelding
      v_ret       VARCHAR2(32767);
      v_dirlist   OAARGTABLE; -- table of vc(32K)
      v_filter_ok BOOLEAN;
      v_fileinfo  VARCHAR2(32767);
      v_file      VARCHAR2(32767);
      v_pos1      NUMBER;
      v_pos2      NUMBER;
      v_pos3      NUMBER;
      v_pos4      NUMBER;
      v_tab_filedesc t_tab_filedesc := t_tab_filedesc();
      v_msg       VARCHAR2(32767);
      v_msg2      VARCHAR2(32767);
   BEGIN
         v_dirlist := OAARGTABLE();
         v_ret := dirlist_java(p_dir, p_filter, p_getattr, v_dirlist);
         IF v_ret != 'OK' THEN
            RETURN v_ret;
         END IF;
         -- vertaal v_dirlist naar t_tab_filedesc
         -- v_dirlist: <filenaam>,<type>,<datum-tijd>,<lengte>;<filenaam etc.
         FOR i IN 1 .. v_dirlist.COUNT LOOP
            v_fileinfo := v_dirlist(i);
            -- verwerk v_fileinfo
            v_pos2 := INSTR(v_fileinfo, '>');
            IF v_pos2 > 0 THEN -- file
              v_file := SUBSTR(v_fileinfo, 1, v_pos2 - 1);
            ELSE
              v_file := v_fileinfo;
            END IF;
            v_tab_filedesc.extend;
            v_tab_filedesc(v_tab_filedesc.COUNT).naam := v_file;

            v_pos3 := INSTR(v_fileinfo, '>', v_pos2 + 1);
            IF v_pos3 > 0 THEN -- type
              v_tab_filedesc(v_tab_filedesc.COUNT).type := SUBSTR(v_fileinfo, v_pos2 + 1, 1);
              v_pos4 := INSTR(v_fileinfo, '>', v_pos3 + 1);
              IF v_pos4 > 0 THEN -- datum tijd
                v_tab_filedesc(v_tab_filedesc.COUNT).datum := TO_DATE(SUBSTR(v_fileinfo, v_pos3 + 1, v_pos4 - v_pos3 - 1), 'YYYYMMDD HH24MISS');
                -- size
                v_tab_filedesc(v_tab_filedesc.COUNT).lengte := TO_NUMBER(SUBSTR(v_fileinfo, v_pos4 + 1));
              END IF;
            END IF;
         END LOOP;
         x_dirlist := v_tab_filedesc;
      RETURN 'OK';
   END dirlist;


  --############################################################################
  --# Lees het opgegeven bestand met JSI meetwaarden in.
  --############################################################################
  PROCEDURE jsi_meetwaarden_inlezen
  ( p_locatie in varchar2
  , p_bestand in varchar2
  , p_kafd_code in varchar2 := null
  ) is
    v_sypa_mail       varchar2(100) := 'BEH_AUTO_UITS_MAIL';
    cursor c_mede is
      select mede.email
      from   kgc_systeem_parameters sypa
      ,      kgc_systeem_par_waarden spwa
      ,      kgc_medewerkers mede
      where  sypa.code = v_sypa_mail
      and    sypa.sypa_id = spwa.sypa_id
      and    spwa.mede_id = mede.mede_id
      and    mede.email is not null
      ;
    cursor c_frac(p_fractienummer varchar2) is
      select frac.frac_id
      from   bas_fracties frac
      where  frac.fractienummer = p_fractienummer
      ;
    r_frac c_frac%rowtype;
    cursor c_meet(p_meet_id number) is
      select meet.meet_id
      ,      meet.meti_id
      ,      stof.omschrijving
      from   bas_meetwaarden meet
      ,      kgc_stoftesten stof
      where  meet.meet_id = p_meet_id
      and    meet.stof_id = stof.stof_id
      ;
    r_meet c_meet%rowtype;
    type r_pw is record -- propmp/waarde
    ( prompt bas_meetwaarde_details.prompt%type
    , waarde bas_meetwaarde_details.waarde%type
    );
    type t_pw is table of r_pw;
    v_pw t_pw := t_pw();
  
    v_procedure_naam  varchar2(40) := 'beh_auto_uits.jsi_meetwaarden_inlezen'; 
    v_kafd_id         number := kgc_uk2pk.kafd_id(nvl(p_kafd_code, 'DNA'));
    v_html_message    clob;
    v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM' 
                                                                        , p_mede_id        => NULL
                                                                        )
                                          , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                          );
    v_email_onderwerp varchar2(100);
    v_job_onderwerp   varchar2(50);
  
    v_locatie         varchar2(1000);
    v_oradir_created  boolean := FALSE;
    v_bestandsnaam    varchar2(1000); 
    v_file_handle     UTL_FILE.FILE_TYPE;
    v_dir_obj_name    varchar2(20);
    v_regel           varchar2(32000);
    v_count_regels    number := 0;
    v_str_array       t_str_array;
    v_str_array_head  t_str_array;
    v_pos1            number;
    v_pos2            number;
    v_fractienummer   bas_fracties.fractienummer%type;
    v_meet_id         bas_meetwaarden.meet_id%type;
    v_duplo           varchar2(10);
    v_variant_volgnr  number := 0;
    v_current_gene    varchar2(100);
    v_current_roi     varchar2(100);
    v_waarde          bas_meetwaarde_details.waarde%type;
    v_ok              boolean;
    v_locatie_new     varchar2(1000);
    v_column_nr       number;
    function getHeaderColumnNr(p_tekst varchar2) return number is
    begin
      for i in 1 .. v_str_array_head.count loop
        if v_str_array_head(i) = p_tekst then
          return i;
        end if;
      end loop;
      raise_application_error(-20000, 'Fout: in de eerste regel kon tekst ' || p_tekst || ' niet gevonden worden!');
    end getHeaderColumnNr;
    procedure add_tekst(p_tekst varchar2) is
    begin
      dbms_lob.writeappend(v_html_message, length(p_tekst), p_tekst);
    end add_tekst;
  
    procedure upd_mdet
    ( p_meet_id number 
    , p_meet_waarde varchar2
    , p_meet_prompt varchar2
    ) is
      v_count number;
      v_err_b varchar2(100);
      v_err_e varchar2(100);
    begin
      update bas_meetwaarde_details mdet
      set    waarde = p_meet_waarde
      where  mdet.meet_id = p_meet_id
      and    mdet.prompt = p_meet_prompt;
      v_count := SQL%ROWCOUNT;
      if v_count <> 1 then
        v_err_b := '<font color="red">';
        v_err_e := '</font>';
      end if;
      add_tekst('<div class="tab2">waarde "' || p_meet_prompt || '" gevuld met ' || p_meet_waarde || '; ' || v_err_b || v_count || ' records bijgewerkt...' || v_err_e || '</div>' || v_crlf);
    end;
  
    --############################################################################
    --# verwerk de meetwaarden van de MIP-stoftest
    --############################################################################
    procedure verwerk_mip_stoftest
    ( p_omschrijving_lang varchar2
    , p_duplo             varchar2
    , p_meti_id           number
    , p_pw                t_pw
    ) is
      -- bepaal stoftest via omschrijving lang (=tab(2)) + omschrijving (moet IMP1/2 bevatten):
      cursor c_stof(p_omschrijving_lang varchar2, p_duplo varchar2) is
        select stof.stof_id
        ,      stof.code
        ,      stof.omschrijving
        ,      stof.omschrijving_lang
        from   kgc_stoftesten stof
        where  upper(stof.omschrijving_lang) like upper(p_omschrijving_lang)
        and    instr(upper(stof.omschrijving), p_duplo) > 0
        ;
      r_stof c_stof%ROWTYPE;
      cursor c_meet(p_meti_id number, p_stof_id number) is
        select meet.meet_id
        from   bas_meetwaarden meet
        where  meet.meti_id = p_meti_id
        and    meet.stof_id = p_stof_id
        ;
      r_meet c_meet%rowtype;
    begin
      open c_stof(p_omschrijving_lang => p_omschrijving_lang, p_duplo => p_duplo);
      fetch c_stof into r_stof;
      if c_stof%notfound then
         add_tekst('<li>Stoftest niet gevonden: ' || p_omschrijving_lang || ' (omschrijving lang) en ' || p_duplo || '(in omschrijving); deze wordt genegeerd!</li>' || v_crlf);
      else
         add_tekst('<li>Stoftest gevonden: ' || r_stof.code || '/' || r_stof.omschrijving || ' via ' || p_omschrijving_lang || ' (omschrijving lang) en ' || p_duplo || '(in omschrijving)' || v_crlf);
    -- zoek aangemelde stoftest
         open c_meet(p_meti_id => p_meti_id, p_stof_id => r_stof.stof_id);
         fetch c_meet into r_meet;
         if c_meet%notfound then
            add_tekst('<div class="tab1">aangemelde stoftest NIET gevonden!</div>' || v_crlf);
         else
            add_tekst('<div class="tab1">aangemelde stoftest gevonden: ' || r_meet.meet_id || '</div>' || v_crlf);
    
            for i in p_pw.first .. p_pw.last loop
              upd_mdet
              ( p_meet_id     => r_meet.meet_id
              , p_meet_waarde => p_pw(i).waarde
              , p_meet_prompt => p_pw(i).prompt
              );
            end loop;
         end if;
         close c_meet;
      end if;
      close c_stof;
    end verwerk_mip_stoftest;
    
    --############################################################################
    --# verwerk de meetwaarden van de variant-stoftest
    --############################################################################
    procedure verwerk_variant_stoftest
    ( p_duplo          varchar2
    , p_variant_volgnr number
    , p_meti_id        number
    , p_pw             t_pw
    ) is
      v_stof_omschrijving kgc_stoftesten.omschrijving%type := p_duplo || '_V' || trim(to_char(p_variant_volgnr, '09'));
      cursor c_stof(p_omschrijving varchar2) is
        select stof.stof_id
        ,      stof.code
        ,      stof.omschrijving
        ,      stof.omschrijving_lang
        from   kgc_stoftesten stof
        where  upper(stof.omschrijving) like upper(p_omschrijving)
        ;
      r_stof c_stof%ROWTYPE;
      cursor c_meet(p_meti_id number, p_stof_id number) is
        select meet.meet_id
        from   bas_meetwaarden meet
        where  meet.meti_id = p_meti_id
        and    meet.stof_id = p_stof_id
        ;
      r_meet c_meet%rowtype;
    begin
      open c_stof(p_omschrijving => v_stof_omschrijving);
      fetch c_stof into r_stof;
      if c_stof%notfound then
         add_tekst('<li>Stoftest niet gevonden: ' || v_stof_omschrijving || ' (omschrijving); deze wordt genegeerd!</li>' || v_crlf);
      else
         add_tekst('<li>Stoftest gevonden: ' || r_stof.code || '/' || r_stof.omschrijving || ' via ' || v_stof_omschrijving || ' (omschrijving)' || v_crlf);
    -- zoek aangemelde stoftest
         open c_meet(p_meti_id => p_meti_id, p_stof_id => r_stof.stof_id);
         fetch c_meet into r_meet;
         if c_meet%notfound then
            add_tekst('<div class="tab1">aangemelde stoftest NIET gevonden!</div>' || v_crlf);
         else
            add_tekst('<div class="tab1">aangemelde stoftest gevonden: ' || r_meet.meet_id || '</div>' || v_crlf);
    
            for i in p_pw.first .. p_pw.last loop
              upd_mdet
              ( p_meet_id     => r_meet.meet_id
              , p_meet_waarde => p_pw(i).waarde
              , p_meet_prompt => p_pw(i).prompt
              );
            end loop;
         end if;
         close c_meet;
      end if;
      close c_stof;
    end verwerk_variant_stoftest;
  
  BEGIN
    v_locatie   := beh_interfaces.unc_locatie(p_locatie, v_kafd_id);
    
    v_email_onderwerp := 'Helix overzicht van JSI_MEETWAARDEN_INLEZEN inlezen (database ' || substr(ora_database_name, 1, 20) || ')';
  
    dbms_lob.createtemporary(v_html_message, TRUE);
    add_tekst(
      '<style type="text/css">
       <!--
         .tab1 { margin-left: 40px; }
         .tab2 { margin-left: 80px; }
       -->
       </style>
       Dit is een automatisch gegenereerd rapport over het inlezen van jsi meetwaarden voor het automatisch aanmaken van uitslagen.</br></br>' || v_crlf);
  
    add_tekst('Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf);
    add_tekst('Uitgevoerd door: ' || user || '</br>' || v_crlf);
    add_tekst('Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>' || v_crlf);
    add_tekst('Bestandsnaam: ' || p_bestand || '</br>' || v_crlf);
    add_tekst('Locatie: ' || v_locatie || '</br></br>' || v_crlf);
    
    -- bestand = fractie + meet-id; Voorbeeld filenaam: DNA17-00243_20523484_VAR.csv
    -- fractienummer
    v_pos1 := instr(p_bestand, '_');
    if v_pos1 = 0 then
      raise_application_error(-20000, 'Fout in naam bestand: 1e ''_'' kon niet gevonden worden!');
    end if;
    v_fractienummer := SUBSTR(p_bestand, 1, v_pos1 - 1);
    open c_frac(p_fractienummer => v_fractienummer);
    fetch c_frac into r_frac;
    if c_frac%notfound then
      close c_frac;
      raise_application_error(-20000, 'Fractienummer ' || v_fractienummer || ' kon niet in Helix gevonden worden!');
    end if;
    close c_frac;
    add_tekst('<li>Bestaande fractie gevonden: ' || v_fractienummer || '</li>' || v_crlf);
  
    -- meet-id
    v_pos2 := instr(p_bestand, '_', v_pos1 + 1);
    if v_pos2 = 0 then
      raise_application_error(-20000, 'Fout in naam bestand: 2e ''_'' kon niet gevonden worden!');
    end if;
    v_meet_id := SUBSTR(p_bestand, v_pos1 + 1, v_pos2 - v_pos1 - 1);
    open c_meet(p_meet_id => v_meet_id);
    fetch c_meet into r_meet;
    if c_meet%notfound then
      close c_meet;
      raise_application_error(-20000, 'Meetwaarde ' || v_meet_id || ' kon niet in Helix gevonden worden!');
    end if;
    close c_meet;
    add_tekst('<li>Bestaande meetwaarde gevonden: ' || v_meet_id || '/' || r_meet.omschrijving || '</li>' || v_crlf);
    -- bepaal welke meting dit is van de duplo-aanmelding (1 of 2)
    if instr(UPPER(r_meet.omschrijving), 'MIP1') > 0 then
      v_duplo := 'MIP1';
    elsif instr(UPPER(r_meet.omschrijving), 'MIP2') > 0 then
      v_duplo := 'MIP2';
    else
      raise_application_error(-20000, 'Meetwaarde ' || v_meet_id || '/' || r_meet.omschrijving || ': duplo-aanmelding kon niet bepaald worden: geen MIP1 of MIP2 in de stoftest-omschrijving!');
    end if;
  
    -- open map en file
    begin
      v_dir_obj_name := beh_file_00.create_directory_object(v_locatie);
      v_oradir_created := TRUE;
    exception when others then
      raise_application_error(-20000, '<br><br>Locatie kon niet gemaakt worden: ' || SQLERRM);
    end;
    begin
      v_file_handle := utl_file.fopen(v_dir_obj_name, p_bestand, 'R');
    exception when others then
      raise_application_error(-20000, 'Bestand kon niet geopend worden: ' || SQLERRM);
    end;
  
    if utl_file.is_open(v_file_handle) then
      --Input bestand is geopend, start de verwerking.
      loop
        begin
          utl_file.get_line(v_file_handle, v_regel, 32000);
          v_count_regels := v_count_regels + 1;
          v_str_array := csv_to_array(p_csv_line=> v_regel, p_separator => CHR(9));
  
          if v_count_regels = 1 then -- heading
             if v_str_array.COUNT < 10 then
               raise_application_error(-20000, 'Eerste regel (heading) bevat minder dan 10 gegevens (nl: ' || v_str_array.COUNT || '); dit is te weinig!');
             end if;
             if v_str_array(1) <> 'Gene' then
               raise_application_error(-20000, 'Eerste regel (heading) bevat geen ''Gene'' (maar: ' || v_str_array(1) || ')!');
             end if;
             add_tekst('<li>Juiste bestandsheader gevonden</li>' || v_crlf);
             v_str_array_head := v_str_array;
          else -- inhoud
            if v_str_array(1) is not null then
              v_current_gene := v_str_array(getHeaderColumnNr('Gene'));
              v_current_roi := v_str_array(getHeaderColumnNr('ROI'));
              -- *** MIP Sotftest ***
              v_pw.delete;
              v_pw.extend(2);
              v_column_nr := getHeaderColumnNr('Coverage Info');
              v_pw(1).prompt := v_str_array_head(v_column_nr);
              v_pw(1).waarde := v_str_array(v_column_nr);
              v_pw(2).prompt := '%Req.Coverage';
              v_pw(2).waarde := v_str_array(getHeaderColumnNr('Req. Cov'));
              v_pos1 := instr(v_pw(2).waarde, '%');
              if v_pos1 > 0 then
                v_pw(2).waarde := substr(v_pw(2).waarde, 1, v_pos1 - 1);
              end if;
              if v_pw(2).waarde = '100' then -- test ok op J zetten
                v_pw.extend(1);
                v_pw(3).prompt := 'Test OK?';
                v_pw(3).waarde := 'J';
              end if;
              verwerk_mip_stoftest
              ( p_omschrijving_lang => v_str_array(2)
              , p_duplo             => v_duplo
              , p_meti_id           => r_meet.meti_id
              , p_pw                => v_pw
              );
            else -- Variant stoftest
              v_pw.delete;
              v_pw.extend(10);
  
              v_variant_volgnr := v_variant_volgnr + 1;
              v_pw(1).prompt := 'Gen';
              v_pw(1).waarde := v_current_gene;
              --kolom mut Effect (=5):	gegevens dienen als volgt in Helix ingelezen worden. Voorbeeld class 1 (08/12) -> class 1 in meetwaarde Class, 08/12 komt in meetwaarde DatumClass
              v_pw(2).prompt := 'Class';
              v_pw(2).waarde := v_str_array(getHeaderColumnNr('mut Effect'));
              v_pos1 := instr(v_pw(2).waarde, '(');
              v_pw(2).waarde := trim(substr(v_pw(2).waarde, 1, v_pos1 - 1));
--              v_pw(3).prompt := 'DatumClass'; -- wijziging Lonneke 28-03-2017
              v_pw(3).prompt := 'DatumClass(MM/YY)';
              v_pw(3).waarde := trim(')' from substr(getHeaderColumnNr('mut Effect'), v_pos1 + 1));
              --kolom mut Coverage:	gegevens dienen als volgt in Helix ingelezen worden. Voorbeeld 50% (171)   [50% (87) / 49% (84)]  ->  50% komt in meetwaarde &VariantCoverage, 171 (eerste getal tussen hakjes) komt in UniekeReadsVariant
              v_pw(4).prompt := '%VariantCoverage';
              v_pw(4).waarde := v_str_array(getHeaderColumnNr('mut Coverage'));
              v_pos1 := instr(v_pw(4).waarde, '%');
              v_pw(4).waarde := substr(v_pw(4).waarde, 1, v_pos1 - 1);
              v_pw(5).prompt := 'UniekeReadsVariant';
              v_pw(5).waarde := trim('(' from trim(substr(getHeaderColumnNr('mut Coverage'), v_pos1 + 1)));
              v_pos1 := instr(v_pw(5).waarde, ')');
              v_pw(5).waarde := substr(v_pw(5).waarde, 1, v_pos1 - 1);
              --kolom Pos.: gegevens komen in meetwaarde g.HGVS
              v_pw(6).prompt := 'g.HGVS';
              v_pw(6).waarde := v_str_array(getHeaderColumnNr('Pos.'));
              v_pos1 := instr(v_pw(6).waarde, '[');
              v_pos2 := instr(v_pw(6).waarde, '(', v_pos1);
              if v_pos1 > 0 and v_pos2 > 0 then
                v_pw(6).waarde := trim(substr(v_pw(6).waarde, v_pos1 + 1, v_pos2 - v_pos1 - 1));
              else
                v_pw(6).waarde := null;
              end if;
              
              --kolom c.HGVS: gegevens komen in meetwaarde c.HGVS
              v_pw(7).prompt := 'c.HGVS';
              v_pw(7).waarde := v_str_array(getHeaderColumnNr('c. HGVS'));
              -- kolom p.HGVS: gegevens komen in meetwaarde p.HGVS
              v_column_nr := getHeaderColumnNr('p.HGVS');
              v_pw(8).prompt := v_str_array_head(v_column_nr);
              v_pw(8).waarde := v_str_array(v_column_nr);
              -- kolom Transcript ID: gegevens komen in meetwaarde ENST nr.
              v_pw(9).prompt := 'ENST nr';
              v_pw(9).waarde := v_str_array(getHeaderColumnNr('Transcript ID'));
              v_pw(10).prompt := 'ROI';
              v_pw(10).waarde := v_current_roi;
  
              verwerk_variant_stoftest
              ( p_duplo             => v_duplo
              , p_variant_volgnr    => v_variant_volgnr
              , p_meti_id           => r_meet.meti_id
              , p_pw                => v_pw
              );
            end if;
          end if;
        exception when no_data_found then
          exit;
        end;
      end loop;
  
      add_tekst('<li>Aantal gevonden regels: ' || v_count_regels || '</li></br></br>' || v_crlf);
    else
      add_tekst('<br><br>FOUT: <font color="red">' || ' Bestand ''' || p_bestand || ''' kan niet geopend worden op locatie ''' || v_locatie || '''.</font></br></br>' || v_crlf);
    end if;
  
    add_tekst('</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);
  
    if utl_file.is_open(v_file_handle) then
      utl_file.fclose(v_file_handle);
    end if;
    if v_oradir_created then
      beh_file_00.remove_directory_object(v_dir_obj_name);
      v_oradir_created := false; -- wellicht komt er nog een exception, dan niet nog een keer doen!
    end if;
  
    -- niet overschrijven in "verwerkt"-map
    v_locatie_new := rtrim(v_locatie, '\') || '\Verwerkt\';
    if beh_file_00.file_exists(p_dir_file => v_locatie_new || p_bestand) then
      raise_application_error(-20000, 'Bestand ' || p_bestand || ' moet verplaatst worden naar map ''' || v_locatie_new || ''', maar daar bestaat het al!');
    end if;
  
    v_ok := beh_file_00.file_move
    ( p_dir_file_from => rtrim(v_locatie, '\') || '\' || p_bestand
    , p_dir_file_to   => v_locatie_new || p_bestand
    );
  
    for r_mede in c_mede loop
      send_mail_html( p_from    => v_email_afzender
                    , p_to      => r_mede.email
                    , p_subject => v_email_onderwerp
                    , p_html    => v_html_message
                    );
    end loop;
    dbms_lob.freetemporary(v_html_message);
  
    COMMIT;
  
  EXCEPTION WHEN OTHERS THEN
  
    ROLLBACK;
  
    if utl_file.is_open(v_file_handle) then
      utl_file.fclose(v_file_handle);
    end if;
    if v_oradir_created then
      beh_file_00.remove_directory_object(v_dir_obj_name);
    end if;
    
    v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
    add_tekst('<br><br>FOUT: <font color="red">' || sqlerrm || '</font></br></br>' || v_crlf);
    add_tekst('</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam);
    
    begin
      v_ok := beh_file_00.file_move
      ( p_dir_file_from => rtrim(v_locatie, '\') || '\' || p_bestand
      , p_dir_file_to   => rtrim(v_locatie, '\') || '\Fout\' || p_bestand
      );
    exception
      when others then
        add_tekst('</br></br>' || v_crlf || 'Nog een ERROR: verplaatsen van bestand ' || rtrim(v_locatie, '\') || '\' || p_bestand || ' naar submap Fout ging niet goed: ' || SQLERRM);
    end;
  
    for r_mede in c_mede loop
      send_mail_html( p_from    => v_email_afzender
                    , p_to      => r_mede.email
                    , p_subject => v_email_onderwerp
                    , p_html    => v_html_message
                    );
    end loop;
    dbms_lob.freetemporary(v_html_message);
   
  END jsi_meetwaarden_inlezen;
  
  
  --############################################################################
  --# Scan de opgegeven directory <map> en verwerk de gevonden jsi meetwaarden
  --# bestanden. Na het verwerken wordt het bestand verplaatst naar directory 
  --# <map>\verwerkt als het bestand correct verwerkt is en <map>\fout als er
  --# een fout is opgetreden.
  --############################################################################
  procedure scan_directory
  ( p_path in varchar2
  , p_kafd_code in varchar2 := null
  ) is
    v_sypa_mail       varchar2(100) := 'BEH_AUTO_UITS_MAIL';
    cursor c_mede is
      select mede.email
      from   kgc_systeem_parameters sypa
      ,      kgc_systeem_par_waarden spwa
      ,      kgc_medewerkers mede
      where  sypa.code = v_sypa_mail
      and    sypa.sypa_id = spwa.sypa_id
      and    spwa.mede_id = mede.mede_id
      and    mede.email is not null
      ;
    v_procedure_naam     varchar2(100) := 'beh_auto_uits.scan_directory'; 
    v_kafd_id            number := kgc_uk2pk.kafd_id(nvl(p_kafd_code, 'DNA'));
    v_email_afzender     varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM' 
                                                                        , p_mede_id        => NULL
                                                                        )
                                          , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                          );
    v_tab_filedesc       beh_auto_uits.t_tab_filedesc := beh_auto_uits.t_tab_filedesc();
    v_ret                VARCHAR2 (32767);
    v_path               varchar2(1000) := beh_interfaces.unc_locatie(p_path, v_kafd_id);
    v_errmsg             varchar2(32767);
  begin
    v_ret := dirlist(p_dir => v_path, x_dirlist => v_tab_filedesc);
    if v_ret != 'OK' then
      raise_application_error(-20000, 'ERROR: dirlist voor map ''' || v_path || ''' leverde een fout: ' || v_ret);
    end if;
    if v_tab_filedesc.count > 0 then
      for I in v_tab_filedesc.first .. v_tab_filedesc.last loop
        -- verwerk bestanden
        if v_tab_filedesc(i).type = 'F' then -- File
          jsi_meetwaarden_inlezen
          ( p_locatie   => v_path
          , p_bestand   => v_tab_filedesc(i).naam
          , p_kafd_code => p_kafd_code
          );
        end if;
      end loop;
    end if;
  exception
    when others then
      for r_mede in c_mede loop
        send_mail_html( p_from    => v_email_afzender
                      , p_to      => r_mede.email
                      , p_subject => 'Error in ' || v_procedure_naam || ' (database ' || substr(ora_database_name, 1, 20) || ')'
                      , p_html    => 'ERROR: ' || SQLERRM
                      );
      end loop;
  end scan_directory;


END beh_auto_uits;
/

/
QUIT
