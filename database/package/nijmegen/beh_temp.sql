CREATE OR REPLACE PACKAGE "HELIX"."BEH_TEMP" as
  PROCEDURE check_pers_zisnrs;
  PROCEDURE upd_pers(p_pers_trow IN cg$KGC_PERSONEN.cg$row_type);
  PROCEDURE upd_rela(p_rela_trow IN cg$KGC_RELATIES.cg$row_type);
  PROCEDURE upd_verz(p_verz_trow IN cg$KGC_VERZEKERAARS.cg$row_type);
END;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_TEMP" is
PROCEDURE check_pers_zisnrs
IS
  -- mogelijk wordt een persoon via kgczis01 gekoppeld aan een ZIS-persoon
  -- dan worden aanwezige gegevens in het adres niet overschreven
  -- tot aanpassing kgczis01: zelf bijwerken!
  v_doUpdates BOOLEAN := TRUE; -- indien TRUE, dan worden er updates gedaan
  cursor c_helix(p_peildatum date) is
    select pers_id
    ,      rela_id
    ,      verz_id
    ,      zisnr
    ,      achternaam
    ,      geboortedatum
    ,      geslacht
    ,      postcode
    ,      beheer_in_zis
    from   kgc_personen pers
    where  zisnr is not null
    and    beheer_in_zis = 'J'
    and    last_update_date >= p_peildatum -- wellicht krijg je ook de personen van de laatste update, maar deze zijn dan gelijk!
  ;
  cursor c_glob is
    select global_name
    from   global_name
    ;
  r_glob c_glob%rowtype;
  CURSOR c_jobs
  ( p_jobnaam VARCHAR2
  ) IS
    SELECT *
    FROM   all_jobs
    WHERE  INSTR(LOWER(what), LOWER(p_jobnaam)) > 0
    ;
  r_jobs c_jobs%ROWTYPE;
  v_pers_trow cg$KGC_PERSONEN.cg$row_type;
  v_rela_trow cg$KGC_RELATIES.cg$row_type;
  v_verz_trow cg$KGC_VERZEKERAARS.cg$row_type;
  v_pers_trow_null cg$KGC_PERSONEN.cg$row_type;
  v_rela_trow_null cg$KGC_RELATIES.cg$row_type;
  v_verz_trow_null cg$KGC_VERZEKERAARS.cg$row_type;
  v_pers_trow_zis cg$KGC_PERSONEN.cg$row_type;
  v_rela_trow_zis cg$KGC_RELATIES.cg$row_type;
  v_verz_trow_zis cg$KGC_VERZEKERAARS.cg$row_type;
  v_strEmail         VARCHAR2(32767);
  v_strConditie      VARCHAR2(32767);
  v_errnr       NUMBER := 0;
  v_errmsg      VARCHAR2(4000);
  v_msg         VARCHAR2(4000);
  v_count       number := 0;
  v_count_err   number := 0;
  v_count_diff  number := 0;
  v_count_BIZJ  number := 0;
  v_count_BIZN  number := 0;
  v_count_UPD   number := 0;
  v_peildatum   DATE;
  v_mail_body varchar2(32767);
  procedure dbms_output_put_line (p_text varchar2) is -- vervanger van dbms_output.put_line
  begin
    v_mail_body := v_mail_body || chr(10) || p_text;
  end;
begin
  open c_glob;
  fetch c_glob into r_glob;
  if c_glob%notfound then
    kgc_zis.post_mail_zis('ZIS_MAIL_ERR', 'beh_temp.check_pers_zisnrs: Fout: databasenaam niet gevonden!', '');
    return;
  end if;
  close c_glob;
  dbms_output.disable; -- kgc_zis.persoon_in_zis produceert nogal wat!
  dbms_output_put_line('Dit is de output van job beh_temp.check_pers_zisnr die draait onder user ' || USER || '!');
  dbms_output_put_line(chr(10) || 'Start Vergelijk Helix met ZIS (' || to_char(sysdate, 'dd-mm-yyyy hh:mi:ss') || '); Database: ' || r_glob.global_name);
  -- bepaling begin en eindtijd adhv job
  open c_jobs('beh_temp.check_pers_zisnrs');
  fetch c_jobs into r_jobs;
  IF c_jobs%NOTFOUND THEN -- niet aanwezig? kan niet - stop!
    close c_jobs;
    kgc_zis.post_mail_zis('ZIS_MAIL_ERR', 'beh_temp.check_pers_zisnrs: Fout: job beh_temp.check_pers_zisnrs niet gevonden!', '');
    return;
  end if;
  close c_jobs;
  v_peildatum := nvl(r_jobs.last_date, sysdate);
--  v_peildatum := sysdate - (1/96);
  dbms_output_put_line('Peildatum = ' || to_char(v_peildatum, 'dd-mm-yyyy hh24:mi:ss'));
  if v_doUpdates then
    v_msg := 'De persoonsgegevens worden vergeleken met ZIS; evt. wordt er een update gedaan van de persoon!';
  else
    v_msg := 'De persoonsgegevens worden alleen vergeleken met ZIS; er worden GEEN updates gedaan!';
  end if;
  dbms_output_put_line(chr(10) || v_msg || chr(10));
  for r_helix in c_helix(v_peildatum) loop
    v_count := v_count + 1;
    v_pers_trow := v_pers_trow_null;
    v_rela_trow := v_rela_trow_null;
    v_verz_trow := v_verz_trow_null;
    v_errnr := 0;
    v_errmsg := NULL;
    v_strEmail := null;
    -- helix gegevens ophalen
    v_pers_trow.pers_id := r_helix.pers_id;
    cg$kgc_personen.slct(v_pers_trow);
    if r_helix.rela_id is not null then
      v_rela_trow.rela_id := r_helix.rela_id;
      cg$kgc_relaties.slct(v_rela_trow);
    end if;
    if r_helix.verz_id is not null then
      v_verz_trow.verz_id := r_helix.verz_id;
      cg$kgc_verzekeraars.slct(v_verz_trow);
    end if;
    -- ZIS-gegevens gelijk maken aan Helix, muv rela/verz!!!
    v_pers_trow_zis := v_pers_trow;
    v_pers_trow_zis.rela_id := NULL;
    v_pers_trow_zis.verz_id := NULL;
    v_rela_trow_zis := v_rela_trow_null;
    v_verz_trow_zis := v_verz_trow_null;
    -- ZIS gegevens ophalen
    begin
      kgc_interface.persoon_in_zis
      ( p_zisnr => r_helix.zisnr
      , pers_trow => v_pers_trow_zis
      , rela_trow => v_rela_trow_zis
      , verz_trow => v_verz_trow_zis
      );
    exception
      when others then
        v_errnr := SQLCODE;
        v_errmsg := SQLERRM;
        IF INSTR(v_errmsg, 'CG$ERRORS') > 0 THEN -- detailfout ophalen
          v_errmsg := cg$errors.GetErrors;
        END IF;
    end;
    if v_errnr <> 0 then
      dbms_output_put_line(substr('>> ZISnr ' || r_helix.zisnr || ': ERROR: ' || v_errmsg, 1, 255));
      v_count_err := v_count_err + 1;
    elsif v_pers_trow_zis.zisnr is null then
      dbms_output_put_line(substr('>> ZISnr ' || r_helix.zisnr || ' kon niet gevonden worden in ZIS!', 1, 255));
      v_count_err := v_count_err + 1;
    elsif v_pers_trow_zis.zisnr <> v_pers_trow.zisnr then
      dbms_output_put_line(substr('>> ZISnr ' || r_helix.zisnr || ' (Helix) heeft in ZIS een ander ZISnr: ' || v_pers_trow_zis.zisnr || '!', 1, 255));
      v_count_err := v_count_err + 1;
    else
      kgc_zis.bepaal_pers_verschil(v_pers_trow, v_pers_trow_zis, v_rela_trow_zis, v_verz_trow_zis, v_strEmail, v_strConditie);
      IF NVL(LENGTH(v_strEmail), 0) = 0 THEN -- gegevens zijn gelijk!
        dbms_output_put_line(substr('>> ZISnr ' || r_helix.zisnr || ': de gegevens zijn identiek', 1, 255));
        v_count_BIZJ := v_count_BIZJ + 1;
      ELSE -- overhalen
        dbms_output_put_line(substr('>> ZISnr ' || r_helix.zisnr || ': de gegevens zijn NIET identiek; gegevens overgehaald!', 1, 255));
        dbms_output_put_line(substr('      ' || v_strEmail, 1, 255));
        IF v_rela_trow_zis.rela_id is null and v_rela_trow_zis.achternaam is not null then
          dbms_output_put_line(substr('      nieuwe rela: ' || v_rela_trow_zis.code || '/' || v_rela_trow_zis.achternaam, 1, 255));
        END IF;
        IF v_verz_trow_zis.verz_id is null and v_verz_trow_zis.naam is not null then
          dbms_output_put_line(substr('      nieuwe verz: ' || v_verz_trow_zis.code || '/' || v_verz_trow_zis.naam, 1, 255));
        END IF;
        IF v_doUpdates THEN --*** gegevens in Helix opnemen
          -- *** RELATIE = HUISARTS
          IF v_rela_trow_zis.rela_id is null and v_rela_trow_zis.achternaam is not null then -- toch wel de minste voorwaarde!
            -- automatisch code uitgeven?
            IF v_rela_trow_zis.code is null THEN
              -- altijd ZIS0123456 aanbrengen! rde 6 juni 2006
              select kgc_rela_seq.nextval
              into   v_rela_trow_zis.rela_id
              from   dual;
              v_rela_trow_zis.code := 'ZIS' || lpad(to_char(v_rela_trow_zis.rela_id), 7, '0');
            END IF;
            v_rela_trow_zis.relatie_type := 'HA'; -- helix-eigen; wordt niet aangeleverd!
            v_rela_trow_zis.vervallen := 'N';
            cg$kgc_relaties.ins(v_rela_trow_zis, cg$kgc_relaties.cg$ind_true);
            v_pers_trow_zis.rela_id := v_rela_trow_zis.rela_id;
          END IF;
          -- *** VERZEKERAARS
          v_verz_trow_zis.verz_id := nvl(v_pers_trow_zis.verz_id, v_verz_trow_zis.verz_id); -- omissie in prod.kgc_zis_nijmegen
          IF v_verz_trow_zis.verz_id is null and v_verz_trow_zis.naam is not null then -- toch wel de minste voorwaarde!
            v_verz_trow_zis.vervallen := 'N';
            v_verz_trow_zis.machtiging_nodig := 'J';
            cg$kgc_verzekeraars.ins(v_verz_trow_zis, cg$kgc_verzekeraars.cg$ind_true);
            v_pers_trow_zis.verz_id := v_verz_trow_zis.verz_id;
          END IF;
          -- *** PERSONEN
          begin
            beh_temp.upd_pers(v_pers_trow_zis);
            commit;
          exception
            when others then
              v_errnr := SQLCODE;
              v_errmsg := SQLERRM;
              IF INSTR(v_errmsg, 'CG$ERRORS') > 0 THEN -- detailfout ophalen
                v_errmsg := cg$errors.GetErrors;
              END IF;
              dbms_output_put_line(substr('>> ZISnr ' || r_helix.zisnr || ': ERROR: ' || v_errmsg, 1, 255));
              v_count_err := v_count_err + 1;
              rollback;
          end;
          IF v_errnr = 0 THEN
            v_count_BIZN := v_count_BIZN + 1;
          END IF;
        END IF;
      END IF;
    END IF;
    IF v_count > 100 THEN
      null;
--      exit;
    END IF;
  END LOOP;
  dbms_output_put_line(chr(10) || 'Gecontroleerd: ' || to_char(v_count) || '; fout: '  || to_char(v_count_err));
  dbms_output_put_line('Er zijn ' || to_char(v_count_BIZJ) || ' personen ongewijzigd!');
  dbms_output_put_line('Er zijn ' || to_char(v_count_BIZN) || ' personen gewijzigd met de gegevens uit ZIS!');
  dbms_output_put_line('Einde Vergelijk Helix met ZIS (' || to_char(sysdate, 'dd-mm-yyyy hh:mi:ss') || ')');
  -- mail, maar alleen als er iets gevonden is!
--  if v_count_BIZN + v_count_err + v_count_BIZJ > 0 then
    kgc_zis.post_mail_zis( 'ZIS_MAIL_ERR'
                         , 'Verslag run check_pers_zisnrs'
                         , v_mail_body);
--  end if;
EXCEPTION WHEN OTHERS THEN
  dbms_output_put_line(substr('SQLErrm ' || SQLERRM, 1, 255));
  -- mail!
  v_count := 0;
  kgc_zis.post_mail_zis( 'ZIS_MAIL_ERR'
                       , 'Verslag run check_pers_zisnrs'
                       , v_mail_body);
END;
PROCEDURE upd_pers(p_pers_trow IN cg$KGC_PERSONEN.cg$row_type) IS
BEGIN
  UPDATE KGC_PERSONEN
  SET
    ACHTERNAAM = p_pers_trow.ACHTERNAAM
    ,GEHUWD = p_pers_trow.GEHUWD
    ,GESLACHT = p_pers_trow.GESLACHT
    ,BLOEDVERWANT = p_pers_trow.BLOEDVERWANT
    ,MEERLING = p_pers_trow.MEERLING
    ,OVERLEDEN = p_pers_trow.OVERLEDEN
    ,DEFAULT_AANSPREKEN = p_pers_trow.DEFAULT_AANSPREKEN
    ,ZOEKNAAM = p_pers_trow.ZOEKNAAM
    ,BEHEER_IN_ZIS = p_pers_trow.BEHEER_IN_ZIS
    ,CREATED_BY = p_pers_trow.CREATED_BY
    ,CREATION_DATE = p_pers_trow.CREATION_DATE
    ,LAST_UPDATED_BY = p_pers_trow.LAST_UPDATED_BY
    ,LAST_UPDATE_DATE = p_pers_trow.LAST_UPDATE_DATE
    ,RELA_ID = p_pers_trow.RELA_ID
    ,VERZ_ID = p_pers_trow.VERZ_ID
    ,ZISNR = p_pers_trow.ZISNR
    ,AANSPREKEN = p_pers_trow.AANSPREKEN
    ,GEBOORTEDATUM = p_pers_trow.GEBOORTEDATUM
    ,PART_GEBOORTEDATUM = p_pers_trow.PART_GEBOORTEDATUM
    ,VOORLETTERS = p_pers_trow.VOORLETTERS
    ,VOORVOEGSEL = p_pers_trow.VOORVOEGSEL
    ,ACHTERNAAM_PARTNER = p_pers_trow.ACHTERNAAM_PARTNER
    ,VOORVOEGSEL_PARTNER = p_pers_trow.VOORVOEGSEL_PARTNER
    ,ADRES = p_pers_trow.ADRES
    ,POSTCODE = p_pers_trow.POSTCODE
    ,WOONPLAATS = p_pers_trow.WOONPLAATS
    ,PROVINCIE = p_pers_trow.PROVINCIE
    ,LAND = p_pers_trow.LAND
    ,TELEFOON = p_pers_trow.TELEFOON
    ,TELEFOON2 = p_pers_trow.TELEFOON2
    ,TELEFAX = p_pers_trow.TELEFAX
    ,EMAIL = p_pers_trow.EMAIL
    ,VERZEKERINGSWIJZE = p_pers_trow.VERZEKERINGSWIJZE
    ,BSN = p_pers_trow.BSN
    ,BSN_GEVERIFIEERD = p_pers_trow.BSN_GEVERIFIEERD
    ,VERZEKERINGSNR = p_pers_trow.VERZEKERINGSNR
    ,HOEVEELLING = p_pers_trow.HOEVEELLING
    ,OVERLIJDENSDATUM = p_pers_trow.OVERLIJDENSDATUM
    ,ZOEKFAMILIE = p_pers_trow.ZOEKFAMILIE
  WHERE  PERS_ID = p_pers_trow.PERS_ID;
END upd_pers;
PROCEDURE upd_rela(p_rela_trow IN cg$KGC_RELATIES.cg$row_type) IS
BEGIN
  NULL;
END upd_rela;
PROCEDURE upd_verz(p_verz_trow IN cg$KGC_VERZEKERAARS.cg$row_type) IS
BEGIN
  NULL;
END upd_verz;
END;
/

/
QUIT
