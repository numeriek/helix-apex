CREATE OR REPLACE PACKAGE "HELIX"."BEH_OPSLAG_00" as
g_scheidingsteken varchar2(1) := '$';
--###############################################
--####Zie mantisnr 4070 voor documentatie
--###############################################
procedure verwerk_log
(
p_log_tekst in varchar,
p_opve_id in number
)
;
function genereer_opat
(
p_helix_id in number,
p_open_code in varchar2
)
return number;
procedure start_verwerking;
end beh_opslag_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_OPSLAG_00" as
procedure verwerk_log
(
p_log_tekst in varchar,
p_opve_id number
) is
begin

update  beh_opslag_verwerk
set   	inlees_log = inlees_log || g_scheidingsteken || p_log_tekst
where   opve_id = p_opve_id
;
end;
function genereer_opat
(
p_helix_id in number,
p_open_code in varchar2
)
--overnegomen van package kgc_opslag_00
return number
is
  opat_trow kgc_opslag_attributen%rowtype;

  cursor open_cur is
    select open.open_id
    from   kgc_opslag_entiteiten open
    where  open.code = upper( p_open_code )
    ;
  cursor opat_cur
  ( b_open_id in number
  )
  is
    select nvl( max( opat.volgnr ), 0 ) + 1
    from   kgc_opslag_attributen opat
    where  opat.open_id = b_open_id
    and    opat.helix_id = p_helix_id
    ;
begin
  open  open_cur;
  fetch open_cur
  into  opat_trow.open_id;
  close open_cur;

  open  opat_cur( b_open_id => opat_trow.open_id );
  fetch opat_cur
  into  opat_trow.volgnr;
  close opat_cur;
  begin
    insert into kgc_opslag_attributen
    ( opat_id
    , open_id
    , helix_id
    , volgnr
    )
  values
    ( kgc_opat_seq.nextval
    , opat_trow.open_id
    , p_helix_id
    , opat_trow.volgnr
    )
  returning opat_id into opat_trow.opat_id
  ;
  end;
  return( opat_trow.opat_id);
end genereer_opat;
procedure start_verwerking is
cursor c_ingelezen_gegevens is
select  trim(upper(kolom01)) opve_id,
        trim(upper(kolom02)) fractienummer,
		trim(kolom03) opmerking,
		trim(upper(kolom04)) element,
		trim(upper(kolom05)) positie,
		trim(upper(substr(kolom05, 0, instr(kolom05, ',')-1))) x_positie,
        decode(
               trim(upper(substr(kolom05, instr(kolom05, ',')+1))),
               'A', '1',
               'B', '2',
               'C', '3',
               'D', '4',
               'E', '5',
               'F', '6',
               'G', '7',
               'H', '8',
               'I', '9',
               'J', '10',
               'K', '11',
               'L', '12',
               'M', '13',
               'N', '14',
               'O', '15',
               'P', '16',
               'Q', '17',
               'R', '18',
               'S', '19',
               'T', '20',
               'U', '21',
               'V', '22',
               'W', '23',
               'X', '24',
               'Y', '25',
               'Z', '26',
               trim(upper(substr(kolom05, instr(kolom05, ',')+1)))
           ) y_positie,
        nvl(upper(trim(kolom06)), 'J') inlezen_in_helix
from beh_opslag_inlees
where nvl(upper(trim(kolom06)), 'J') = 'J' --inlezen in helix = j
order by kolom01 -- sleutel
;

cursor c_frac (p_fractienummer varchar2) is
select *
from bas_fracties
where upper(fractienummer) = upper(p_fractienummer)
;
r_frac c_frac%rowtype;

cursor c_opel (p_element varchar2) is
select *
from kgc_opslag_elementen
where upper(code) = upper(p_element)
and upper(vervallen) = upper('N')
;
r_opel c_opel%rowtype;

cursor c_oppo (p_opel_id number, p_x_positie number, p_y_positie number) is
select *
from kgc_opslag_posities
where opel_id = p_opel_id
and   x_positie = p_x_positie
and   y_positie = p_y_positie
and   upper(beschikbaar) = upper('J')
;
r_oppo c_oppo%rowtype;

cursor c_opin (p_oppo_id number) is
select *
from kgc_opslag_inhoud
where oppo_id = p_oppo_id
;
r_opin c_opin%rowtype;

v_open_id kgc_opslag_entiteiten.open_id%type;
v_mede_id kgc_medewerkers.mede_id%type;
v_autorisatie_ok number;
v_zoek_sleutel varchar2(100);
v_datum_inlees date := sysdate;
v_opve_id beh_opslag_verwerk.opve_id%type;
v_opat_id kgc_opslag_attributen.opat_id%type;
v_log_tekst varchar2(2000);

v_afzender varchar2(100);
v_ontvanger varchar2(500);
v_boodschap  varchar2(2000);
v_newline varchar2(10) := chr(10);
v_db_naam varchar2(10);
v_titel varchar2(100);

begin
v_zoek_sleutel := upper(user) || '_' || to_char(sysdate, 'ddmmyyyyhh24miss');
v_mede_id := kgc_mede_00.medewerker_id;

for r_ingelezen_gegevens in c_ingelezen_gegevens loop

    v_opve_id := to_number(r_ingelezen_gegevens.opve_id);

    insert into beh_opslag_verwerk
    (
    opve_id,
    zoek_sleutel,
    datum_inlees,
    mede_id,
    fractienummer,
    opmerking,
    element,
    positie,
    inlezen_in_helix
    )
    values
    (
    v_opve_id,
    v_zoek_sleutel,
    v_datum_inlees,
    v_mede_id,
    r_ingelezen_gegevens.fractienummer,
    r_ingelezen_gegevens.opmerking,
    r_ingelezen_gegevens.element,
    r_ingelezen_gegevens.positie,
    r_ingelezen_gegevens.inlezen_in_helix
    );

	open c_frac(r_ingelezen_gegevens.fractienummer);
	fetch c_frac into r_frac;
	if c_frac%found then

		open c_opel(r_ingelezen_gegevens.element);
		fetch c_opel into r_opel;
		if c_opel%found then

            open c_oppo(
                            r_opel.opel_id,
                            to_number(r_ingelezen_gegevens.x_positie),
                            to_number(r_ingelezen_gegevens.y_positie)
                       );

            fetch c_oppo into r_oppo;
            if c_oppo%found then
                open c_opin(r_oppo.oppo_id);
                fetch c_opin into r_opin;
                if c_opin%found then
                    v_log_tekst := '5000 - Deze positie (' || r_ingelezen_gegevens.positie || ') is al bezet.';
                    verwerk_log (v_log_tekst, v_opve_id);
                else
                    v_autorisatie_ok := kgc_opau_00.mag_actie_uitvoeren
                                            ( r_opel.opel_id
                                            , USER
                                            , 'I' -- Inzetten
                                            );

                     if v_autorisatie_ok = 1 then

                        v_opat_id := genereer_opat(r_frac.frac_id, 'FRAC');

                        insert into kgc_opslag_inhoud
                            (oppo_id, opat_id, mede_id_geplaatst, opmerkingen)
                            values(r_oppo.oppo_id, v_opat_id, v_mede_id, r_ingelezen_gegevens.opmerking)
                            ;
                        v_log_tekst := '6000 - Opslag posities zijn succesvul ingevoerd.';
                        verwerk_log (v_log_tekst, v_opve_id);
                    else
                        v_log_tekst := '4000 - U heeft geen autorisatie voor deze element/actie.';
                        verwerk_log (v_log_tekst, v_opve_id);
                    end if;
                end if;
                close c_opin;
            else
                v_log_tekst := '3000 - Deze positie (' || r_ingelezen_gegevens.positie || ') bestaat in Helix niet.';
                verwerk_log (v_log_tekst, v_opve_id);
            end if;
            close c_oppo;

		else
            v_log_tekst := '2000 - Dit element (' || r_ingelezen_gegevens.element || ') bestaat in Helix niet.';
            verwerk_log (v_log_tekst, v_opve_id);
		end if;
		close c_opel;

	else
        v_log_tekst := '1000 - Fractienummer (' || r_ingelezen_gegevens.fractienummer || ') bestaat in Helix niet.';
        verwerk_log (v_log_tekst, v_opve_id);
	end if;
	close c_frac;
end loop;
commit;
-- versturen van raportage email
select sys_context ('USERENV', 'DB_NAME') into v_db_naam from dual
;

select 	upper(trim(spwa.waarde)) into v_afzender
from 	kgc_systeem_parameters sypa, kgc_systeem_par_waarden spwa
where 	sypa.sypa_id = spwa.sypa_id
and   	upper(sypa.code) = upper('BEH_MIJN_HELIX_EMAIL_ADRES')
and 	spwa.mede_id  in (	select mede_id
							from kgc_medewerkers
							where upper(oracle_uid) = upper('HELIX')
						 )
;

select 	upper(trim(spwa.waarde)) into v_ontvanger
from 	kgc_systeem_parameters sypa, kgc_systeem_par_waarden spwa
where 	sypa.sypa_id = spwa.sypa_id
and   	upper(sypa.code) = upper('BEH_MIJN_HELIX_EMAIL_ADRES')
and 	spwa.mede_id  in (	select mede_id
							from kgc_medewerkers
                            where upper(oracle_uid) = upper(user)
						 )
;

v_titel := v_zoek_sleutel || ' Opslag controle sleutel';

v_boodschap := 'Beste ' || user
        || v_newline
        || v_newline
        || 'Op: ' || to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS')
        || ' heb je een bestand in Helix ' || v_db_naam || ' ingelezen.'
        || v_newline
        || v_newline
        || ' Je sleutel is:' || v_zoek_sleutel
        || v_newline
        || v_newline
        || 'Voor vragen en opmerkingen kun je contact opnemen met '
        || 'Helix systeembeheer.';

UTL_MAIL.SEND(  sender => v_afzender,
                recipients => v_ontvanger, -- komma gescheiden
                cc => v_afzender,  -- komma gescheiden
                bcc => null,
                subject => v_titel,
                message => v_boodschap);

exception
when others then
--    rollback;
    v_log_tekst := to_char(sqlcode) || sqlerrm;

    if instr(v_log_tekst, 'CG$ERRORS') > 0 then -- detailfout ophalen
        v_log_tekst := cg$errors.GetErrors;
    end if;

	verwerk_log (v_log_tekst, v_opve_id);
end start_verwerking;
end beh_opslag_00;
/

/
QUIT
