CREATE OR REPLACE PACKAGE "HELIX"."BEH_DIPO_00" as
function sms_overzichten
( p_dagen_terug in number
, x_msg in out varchar2
) return boolean;
procedure verwerk_log
(
p_verzonden in varchar,
p_pers_id_ontvanger in number,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
);
function koppel_digipoli
( p_bsn                     NUMBER
, p_zisnr                   VARCHAR2
, p_koppel                  BOOLEAN
, x_message             OUT VARCHAR2
) return boolean;
procedure start_verwerking;
end beh_dipo_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DIPO_00" as
function sms_overzichten
(p_dagen_terug in number, x_msg in out varchar2)
return boolean is
v_conn UTL_SMTP.connection;
v_smtp_host kgc_systeem_parameters.standaard_waarde%type :=
            kgc_sypa_00.systeem_waarde ('MAIL_SERVER');

v_verzender_email varchar2(100) := 'cukz124@umcn.nl';
--v_ontvanger_email varchar2(1000):= 'dna@umcn.nl,cukz124@umcn.nl';
v_ontvanger_email varchar2(1000):= 'dna@umcn.nl';

v_boundary varchar2(20) := 'KGCNNIJMEGENBOUND';
v_datum_overzicht varchar2(100) :=  to_char(trunc(sysdate)-p_dagen_terug, 'dd-mm-yyyy');
v_subject varchar2(500) := 'SMS overzichten van: ' || v_datum_overzicht;

cursor c_rcpt is
select *
from table(cast(beh_alfu_00.explode(',', v_ontvanger_email) as beh_listtable))
;

cursor c_beri is
select
    pers.aanspreken
,    onde.onderzoeknr
,     to_char(beri.creation_date_laatste_keer, 'dd-mm-yyyy hh24:mi:ss') datum_verzonden
,     beri.adres_verzonden_laatste_keer telnr
,     beri.verzonden_laatste_keer verzonden
from
kgc_personen pers,
kgc_onderzoeken onde,
kgc_zwangerschappen zwan,
kgc_foetussen foet,
kgc_kgc_afdelingen kafd,
kgc_onderzoeksgroepen ongr,
(
select
    ROW_NUMBER () OVER (PARTITION BY beri2.entiteit_id ORDER BY beri2.creation_date) volgnr,
    beri2.entiteit_id zwan_id,
    MAX (beri2.creation_date) KEEP (DENSE_RANK LAST ORDER BY beri2.creation_date) over (PARTITION BY beri2.entiteit_id) creation_date_laatste_keer,
    MAX (beri2.verzonden) KEEP (DENSE_RANK LAST ORDER BY beri2.creation_date) over (PARTITION BY beri2.entiteit_id) verzonden_laatste_keer,
    MAX (beri2.log_tekst) KEEP (DENSE_RANK LAST ORDER BY beri2.creation_date) over (PARTITION BY beri2.entiteit_id) log_tekst_laatste_keer,
    MAX (beri2.adres_verzonden) KEEP (DENSE_RANK LAST ORDER BY beri2.creation_date) over (PARTITION BY beri2.entiteit_id) adres_verzonden_laatste_keer
from beh_berichten beri2, beh_bericht_types bety
where beri2.bety_id = bety.bety_id
and upper (beri2.entiteit) = 'ZWAN'
and upper (beri2.ontvanger_type) = 'PERS'
and upper(bety.code) = 'SMS'
order by beri2.beri_id, beri2.creation_date
) beri
where pers.pers_id = onde.pers_id
and   onde.foet_id = foet.foet_id
and   foet.zwan_id = zwan.zwan_id
and   zwan.zwan_id = beri.zwan_id
and   onde.kafd_id = kafd.kafd_id
and   onde.ongr_id = ongr.ongr_id
and   kafd.code in ('GENOOM', 'CYTO')
and   ongr.code like 'PRE%'
and   beri.volgnr = 1
and trunc(beri.creation_date_laatste_keer) = (trunc(sysdate)-p_dagen_terug)
order by onde.creation_date
;
cursor c_tel is
select pers.aanspreken, onde.onderzoeknr, pers.telefoon
from kgc_personen pers,
kgc_onderzoeken onde,
kgc_zwangerschappen zwan,
kgc_foetussen foet,
kgc_kgc_afdelingen kafd,
kgc_onderzoeksgroepen ongr
where pers.pers_id = onde.pers_id
and onde.foet_id = foet.foet_id
and foet.zwan_id = zwan.zwan_id
and  onde.kafd_id = kafd.kafd_id
and  onde.ongr_id = ongr.ongr_id
and  kafd.code in ('GENOOM', 'CYTO')
and  ongr.code like 'PRE%'
and nvl( kgc_attr_00.waarde ('KGC_ZWANGERSCHAPPEN', 'SMS_VERZENDEN', zwan.zwan_id), 'J') != 'N'
and trunc(onde.creation_date) = (trunc(sysdate)-p_dagen_terug)
and not exists -- onderzoeken met afwijkende indicaties krgijgen geen sms dus worden ook niet gecheckt
(
    select *
    from kgc_onderzoeken onde2,
    kgc_onderzoek_indicaties onin2,
    kgc_indicatie_teksten indi2,
    kgc_kgc_afdelingen kafd2,
    kgc_onderzoeksgroepen ongr2
    where onde2.onde_id = onin2.onde_id
    and onin2.indi_id = indi2.indi_id
    and onde2.onde_id = onde.onde_id
    and kafd2.kafd_id = indi2.kafd_id
    and ongr2.ongr_id = indi2.ongr_id
    and kafd2.code in ('GENOOM' , 'CYTO')
    and  ongr2.code like 'PRE%'
    and indi2.afwijking = 'J'
)
order by onde.creation_date
;
BEGIN
if (sys_context ('USERENV', 'DB_NAME') like 'PROD%') then
    v_conn:= UTL_SMTP.OPEN_CONNECTION(host => v_smtp_host);
    UTL_SMTP.HELO(v_conn, v_smtp_host);
    UTL_SMTP.MAIL(v_conn, v_verzender_email);

    -- loop om alle email adressen als ontvanger door te geven (in rcpt)
    -- komma is de separator
    for r_rcpt in c_rcpt loop
        UTL_SMTP.RCPT(v_conn, r_rcpt.column_value);
    end loop;

    UTL_SMTP.OPEN_DATA(v_conn);

    -- header hoofd bericht
    UTL_SMTP.WRITE_DATA(v_conn, 'From: <' || v_verzender_email || '>' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'To: <' || v_ontvanger_email || '>' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Subject: '|| v_subject || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'MIME-Version: 1.0' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Content-Type: multipart/mixed; boundary= "' || v_boundary || '"' || UTL_TCP.CRLF || UTL_TCP.CRLF);

    -- sub bericht body
    UTL_SMTP.WRITE_DATA(v_conn, '--' || v_boundary|| UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Content-Type: text/html; charset=UTF-8' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Content-Transfer-Encoding: 8bit'|| UTL_TCP.CRLF || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<i>Deze e-mail is automatisch gegenereerd door Helix (beh_dipo_00.sms_overzichten).</i><br><br>'));
    -- Eerste overzicht: verstuurde sms
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<table border="1" align="center">'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr><td align="center" colspan="5"><font color="green" size="4">Verzonden smsjes op ' || v_datum_overzicht || '</td></tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Aanspreken</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Onderzoeknr</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Datum verzonden</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Telnr</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Verzonden</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));
    for r_beri in c_beri loop

        if (upper(r_beri.verzonden) != 'J') then
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr bgcolor="red">'));
        else
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
        end if;
        UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_beri.aanspreken ||'</td>'));
        UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_beri.onderzoeknr||'</td>'));
        UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_beri.datum_verzonden||'</td>'));
        UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_beri.telnr||'</td>'));
        UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center">'||r_beri.verzonden||'</td>'));
        UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));

    end loop;
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</table><br><br>'));
    -- tweede overzicht: ongeldige telefoonrs
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<table border="1" align="center">'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr><td align="center" colspan="3"><font color="green" size="4">Ongeldige mobielnrs, ingevoerd op ' || v_datum_overzicht || '</td></tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Aanspreken</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Onderzoeknr</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Telefoon</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));
    for r_tel in c_tel loop
        if not(beh_alfu_00.is_mobielnr_goed(nvl(regexp_replace(r_tel.telefoon, '[^[:digit:]]'), 'Geen TelNr.'))) then
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_tel.aanspreken ||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_tel.onderzoeknr||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_tel.telefoon||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));
        end if;
    end loop;
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</table>'));

    UTL_SMTP.WRITE_DATA(v_conn, UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, UTL_TCP.CRLF);
    -- afsluiten van smtp connectie
    UTL_SMTP.CLOSE_DATA(v_conn);
    UTL_SMTP.QUIT(v_conn);
    return true;
end if;
EXCEPTION
WHEN OTHERS THEN
    UTL_SMTP.QUIT(v_conn);
    x_msg := ': Error: ' || SQLERRM;
    return false;
end sms_overzichten;
procedure verwerk_log
(
p_verzonden in varchar,
p_pers_id_ontvanger in number,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) is
v_bety_id beh_bericht_types.bety_id%type;
v_datum_verzonden date := sysdate;
v_oracle_uid kgc_medewerkers.oracle_uid%type;
begin
select bety_id into v_bety_id
from beh_bericht_types
where upper(code) = upper('SMS')
and upper(vervallen) = upper('N')
;
select sys_context('USERENV', 'SESSION_USER')
into v_oracle_uid
from dual
;
insert into beh_berichten
(
bety_id,
verzonden,
datum_verzonden,
oracle_uid,
ontvanger_id_intern,
adres_verzonden,
log_tekst,
entiteit,
entiteit_id,
bericht_tekst,
ontvanger_type
)
values
(
v_bety_id,
p_verzonden,
v_datum_verzonden,
v_oracle_uid,
p_pers_id_ontvanger,
p_adres_verzonden,
p_log_tekst,
'ZWAN',
p_entiteit_id,
p_bericht_tekst,
'PERS'
)
;
commit;
end verwerk_log;
function koppel_digipoli
/******************************************************************************
   NAME:       beh_koppel_digipoli
   PURPOSE:    bericht naar digipoli sturen tbv beschikbaar stellen Helix
               voor Nijmegen via specifiek SOAP-protocol.
   RESULT:     indien OK, dan TRUE, anders FALSE. Nadere info in x_message
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1          03-05-2010  RDE              Eerste versie
***************************************************************************** */
( p_bsn                     NUMBER
, p_zisnr                   VARCHAR2
, p_koppel                  BOOLEAN
, x_message             OUT VARCHAR2
) RETURN BOOLEAN IS
  c  utl_tcp.connection;  -- TCP/IP connection to the Web server
  ret_val pls_integer;
  v_digipolihost    VARCHAR2(200);
  v_msg             VARCHAR2(32000);
  v_msg_tmp         VARCHAR2(32000);
  v_header          VARCHAR2(32000);
  v_content         VARCHAR2(32000);
  v_str             VARCHAR2(2000);
  v_strAtt          VARCHAR2(2000);
  v_strVal          VARCHAR2(2000);
  v_strXML          VARCHAR2(32000);
  v_sep             VARCHAR2(2) := CHR(10);
  v_n               VARCHAR2(2) := CHR(10);
  v_r               VARCHAR2(2) := CHR(13);
  v_pos             NUMBER;
  v_koppel          VARCHAR2(10);
  v_XMLp            xmlparser.Parser := xmlparser.newParser;
  v_XMLdoc          xmldom.DOMDocument;
  v_XMLn1           xmldom.DOMNode;
  v_XMLNL           xmldom.DOMNodeList;
  v_ns_xsi          VARCHAR2(4000);
  v_ok              BOOLEAN;
  v_toon_debug      BOOLEAN := false;
  PROCEDURE toon_debug (p_text VARCHAR2) IS
  BEGIN
    IF v_toon_debug THEN
      dbms_output.put_line(SUBSTR(p_text, 1, 255));
    END IF;
  END;
BEGIN
  v_digipolihost := kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_DIGIPOLI_HOST', p_kafd_id => NULL);
  -- open connection
  c := utl_tcp.open_connection(remote_host => v_digipolihost
                              ,remote_port =>  81
--                              ,tx_timeout  => 5
                              );
  IF p_koppel THEN
    v_koppel := 'true';
  ELSE
    v_koppel := 'false';
  END IF;
  v_content :=
    '<?xml version=''1.0'' encoding=''UTF-8''?>' || v_n ||
    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">' || v_sep ||
    '   <soapenv:Header/>' || v_sep ||
    '   <soapenv:Body>' || v_sep ||
    '      <tem:CoupleUnCouplePatientToRole>' || v_sep ||
    '         <tem:koppelen>' || v_koppel || '</tem:koppelen>' || v_sep ||
    '         <tem:bsnNr>' || p_bsn || '</tem:bsnNr>' || v_sep ||
    '         <tem:radboudNr>' || p_zisnr || '</tem:radboudNr>' || v_sep ||
    '         <tem:digipoli>PND</tem:digipoli>' || v_sep ||
    '         <tem:mijnDossier>false</tem:mijnDossier>' || v_sep ||
    '      </tem:CoupleUnCouplePatientToRole>' || v_sep ||
    '   </soapenv:Body>' || v_sep ||
    '</soapenv:Envelope>';
  v_header := 'POST /PatientAdministrationService/PatientAdministrationService.svc HTTP/1.0' || v_sep
    || 'Accept-Encoding: gzip,deflate' || v_sep
    || 'Content-Type: text/xml;charset=UTF-8' || v_sep
    || 'SOAPAction: "http://tempuri.org/IPatientAdministration/CoupleUnCouplePatientToRole"' || v_sep
    || 'Host: ' || v_digipolihost || v_sep
    || 'Content-Length: ' || LENGTH(v_content) || v_sep;
  v_msg := v_header || v_sep || v_content;
  v_msg_tmp := replace(replace(v_msg, chr(10), '[\n]'|| chr(10)), chr(13), '[\r]');
  toon_debug(substr(v_msg_tmp, 1, 255));
  toon_debug(substr(v_msg_tmp, 256, 255));
  ret_val := utl_tcp.write_line(c, v_msg); -- send HTTP request
  -- terugkomende gegevens ophalen:
  BEGIN
    v_strXML := utl_tcp.get_text(c, 32000);
    toon_debug('lengte: ' || to_char(length(v_strXML)));
    toon_debug(substr(v_strXML,1,255));
    toon_debug(substr(v_strXML,256,255));
    toon_debug(substr(v_strXML,511,255));
    toon_debug(substr(v_strXML,766,255));
  EXCEPTION
    WHEN utl_tcp.end_of_input THEN
      NULL; -- end of input
  END;
  utl_tcp.close_connection(c);
  v_ok := TRUE;
--  v_XMLp := xmlparser.newParser;
  xmlparser.setValidationMode(v_XMLp, FALSE);
  -- eerste stuk eraf:
  v_pos := INSTR(v_strXML, '<');
  IF v_pos > 0 THEN
    v_strXML := SUBSTR(v_strXML, v_pos);
  END IF;
  -- parse:
  toon_debug('7-parse');
  xmlparser.parseBuffer(v_XMLp, v_strXML);
  -- lees document
  toon_debug('8-lees doc');
  v_XMLdoc := xmlparser.getDocument(v_XMLp);
  v_XMLn1 := xmldom.makeNode(xmldom.getDocumentElement(v_XMLdoc));
  -- gevonden: s:Envelope
  toon_debug('9-envelope=' || xmldom.getNodeName(v_XMLn1));
  -- namespaces - nodig en ophalen!
  v_ns_xsi := xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 's', 'xmlns');
  toon_debug('10-ns-attr=' || v_ns_xsi);
  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
  -- gevonden: s:Body
  toon_debug('11-body=' || xmldom.getNodeName(v_XMLn1));
  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
  -- gevonden: CoupleUnCouplePatientToRoleResponse
  toon_debug('12-respons=' || xmldom.getNodeName(v_XMLn1));
  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
  -- gevonden: CoupleUnCouplePatientToRoleResult
  toon_debug('13-result=' || xmldom.getNodeName(v_XMLn1));
  v_str := xmldom.getNodeName(v_XMLn1);
  -- onderliggende message en status ophalen
  v_XMLNL := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), 'Message');
  -- 1e ophalen; aanvullende navigatie naar node met de waarde!!!
  x_message := SUBSTR(xmldom.getNodeValue(xmldom.getFirstChild(xmldom.item(v_XMLNL, 0))), 1, 2000);
  toon_debug('14-message=' || SUBSTR(x_message, 1, 240));
  v_XMLNL := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), 'Status');
  v_str := SUBSTR(xmldom.getNodeValue(xmldom.getFirstChild(xmldom.item(v_XMLNL, 0))), 1, 255);
  toon_debug('15-status=' || SUBSTR(v_str, 1, 240));
  IF UPPER(v_str) in ('SUCCESS', 'ALREADYCOUPLED') THEN
    v_ok := TRUE;
  ELSE
    v_ok := FALSE;
  END IF;
  IF NOT xmldom.isNull(v_XMLdoc) THEN
    xmldom.freeDocument(v_XMLdoc);
  END IF;
  xmlparser.freeParser(v_XMLp);
  RETURN v_ok;
EXCEPTION
WHEN OTHERS THEN
  x_message := SUBSTR(SQLERRM, 1, 4000);
  utl_tcp.close_all_connections;
  IF NOT xmldom.isNull(v_XMLdoc) THEN
    xmldom.freeDocument(v_XMLdoc);
  END IF;
  xmlparser.freeParser(v_XMLp);
  RETURN FALSE;
END koppel_digipoli;
   PROCEDURE start_verwerking
   IS
      v_koppel_ok       BOOLEAN;
      v_koppel_log      VARCHAR2 (4000);
      v_sms_ok          BOOLEAN;
      v_sms_log         VARCHAR2 (4000);
      v_verzonden       beh_berichten.verzonden%TYPE := 'N';
      v_log_tekst       beh_berichten.log_tekst%TYPE;

      v_afzender        VARCHAR2 (10) := 'NPDN';
      v_telefoon        kgc_personen.telefoon%TYPE;
      v_bericht_tekst   beh_berichten.bericht_tekst%TYPE;
      v_brty_id         KGC_BRIEFTYPES.brty_id%type;
      v_global_name     varchar2(20);

      CURSOR c_pers
      IS
         SELECT *
           FROM beh_digipoli_vreugde_vw dipo
          WHERE NOT EXISTS
                       (SELECT *
                          FROM beh_berichten beri, beh_bericht_types bety
                         WHERE     beri.bety_id = bety.bety_id
                               AND beri.ontvanger_id_intern = dipo.pers_id
                               AND beri.entiteit_id = dipo.zwan_id
                               AND UPPER (beri.entiteit) = UPPER ('ZWAN')
                               AND UPPER (beri.ontvanger_type) =
                                      UPPER ('PERS')
                               AND UPPER (beri.verzonden) = UPPER ('J')
                               AND bety.code = UPPER ('SMS'))
         UNION                    -- mislukte gevallen worden vaker meegenomen
         SELECT *
           FROM beh_digipoli_vreugde_vw dipo
          WHERE EXISTS
                   (SELECT *
                      FROM beh_dipo_berichten_mislukt_vw mislukt
                     WHERE mislukt.entiteit_id = dipo.zwan_id);
   BEGIN

      select brty_id into v_brty_id from kgc_brieftypes where code = 'VREUGDE';

      FOR r_pers IN c_pers
      LOOP
         -- lege spatie in beide kanten worden weggehaald
         -- als eerst telnr leeg is, dan pakken we de tweede!
         --    v_telefoon := trim(nvl(r_pers.telefoon, r_pers.telefoon2));
         -- alleen getallen blijven over; alle andere karakters
         -- worden weggehaald
         v_telefoon := REGEXP_REPLACE (r_pers.telefoon, '[^[:digit:]]');
         -- een leeg telnr wordt gezien als 'Geen TelNr.'!
         v_telefoon := NVL (v_telefoon, 'Geen TelNr.');
         /*
             -- Ophalen van SMS tekst
             v_bericht_tekst := kgc_sypa_00.standaard_waarde
                               ( p_parameter_code => 'BEH_SMS_TEKST_VREUGDE'
                               , p_kafd_id => null);
         */
         v_bericht_tekst := r_pers.sms_tekst;

         -- als SMS tekst opgehaald is...
         IF v_bericht_tekst IS NOT NULL
         THEN
            -- ... en als mobiele nummer een geldige NL mobielenr is...
            IF beh_alfu_00.is_mobielnr_goed (v_telefoon)
            THEN
               /*
                           -- en als BSN (of ZISNr) gevuld is..
                           if r_pers.bsn is not null then
                               -- koppel de persoon in digipoli database.
                               -- de koppeling kan alleen via BSN of ZISnr
                               -- geinitieerd wordt dus niet beide!
                               -- als beide parameters gevuld zijn, geeft de web service
                               -- van digitale poli een foutmelding!
                               -- paramter p_koppel kan true (koppelen) of false
                               -- (ontkoppelen) zijn.
                               -- return is een boolean
                               v_koppel_ok :=  koppel_digipoli
                                               ( p_bsn      => r_pers.bsn
                                               , p_zisnr    => null
                                               , p_koppel   => true
                                               , x_message  => v_koppel_log
                                               );
                           else
                               -- als BSN leeg is dan word het met ZISNr geprobeerd.
                               if r_pers.zisnr is not null then
                                   v_koppel_ok :=  koppel_digipoli
                                                   ( p_bsn      => null
                                                   , p_zisnr    => r_pers.zisnr
                                                   , p_koppel   => true
                                                   , x_message  => v_koppel_log
                                                   );
                                -- Als beid (BSN en ZISNr) leeg zijn, dan loggen we
                                -- een foutmelding; koppelen is mislukt!
                                else
                                   v_koppel_ok := false;
                                   v_koppel_log := 'BSN en ZisNr zijn leeg.';
                                end if;
                           end if;
                           -- alleen bij een succesvulle koppeling, sturen we een SMS
                           if v_koppel_ok then
               */
               -- zie beh_alfu_00.verstuur_sms voor meer info
               -- over de parameters.
               -- return is een boolean
               select  global_name into v_global_name from global_name;
               if ((v_global_name like 'PROD%')
                   or (v_telefoon in('0650545927','0633796258', '0610151921')))
                  then
              -- alleen versturen in prod  of aan bepaalde telefoonnr's
                   v_sms_ok :=
                      beh_alfu_00.verstuur_sms (p_afzender    => v_afzender,
                                                p_ontvanger   => v_telefoon,
                                                p_bericht     => v_bericht_tekst,
                                                x_message     => v_sms_log);
              end If;
               IF v_sms_ok
               THEN
                  v_verzonden := 'J';

                  -- alleen als hetversturen van het bericht is geslaagd :
                            if v_verzonden = 'J' then
                             -- . registreer als  vreugdebrief nav invoeren NIPT mantis nr 9503

                              if ( v_brty_id is not null
                                 )then
                                 insert into kgc_brieven(
                                   kafd_id
                                 , pers_id
                                 , datum_print
                                 , kopie
                                 , brty_id
                                 , zwan_id
                                 , datum_verzonden
                                 , geadresseerde
                                 , aanmaakwijze
                                 )
                                 values(
                                   r_pers.kafd_id
                                 , r_pers.pers_id
                                 , sysdate
                                 , 'N'
                                 , v_brty_id
                                 , r_pers.zwan_id
                                 , sysdate
                                 , r_pers.telefoon
                                 , 'VRGD_SMS'
                                 );
                                 commit;

                              -- klaar
                              end if;

                            end if;






               ELSE
                  v_verzonden := 'N';
               END IF;

               v_log_tekst := v_sms_log;
            /*
                        else
                            v_verzonden := 'N';
                            v_log_tekst := v_koppel_log;
                        end if;
            */
            ELSE
               v_verzonden := 'N';
               v_log_tekst := 'Ongeldige telefoonnummer.';
            END IF;
         ELSE
            v_verzonden := 'N';
            v_log_tekst := 'Kolom SMS_TEKST is leeg.';
         --        v_log_tekst := 'sypa BEH_SMS_TEKST_VREUGDE is leeg.';
         END IF;

         -- de uitkomst (goed of fout) wordt gelogd.
         verwerk_log (v_verzonden,
                      r_pers.pers_id,
                      v_telefoon,
                      v_log_tekst,
                      r_pers.zwan_id,
                      v_bericht_tekst);
      END LOOP;
   END start_verwerking;

end beh_dipo_00;
/

/
QUIT
