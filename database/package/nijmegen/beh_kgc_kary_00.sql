CREATE OR REPLACE PACKAGE "HELIX"."BEH_KGC_KARY_00" IS
-- PL/SQL Specification
-- PL/SQL Specification
-- geformatteerde string van een chromosoom van 1 cel

FUNCTION formatteer
( p_chromosoom IN VARCHAR2
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( formatteer, WNDS, WNPS, RNPS );
-- samengestelde string van de chromosomen van 1 cel

FUNCTION chromosomen
( p_meet_id IN NUMBER
, p_cel IN NUMBER
, p_kwfr_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( chromosomen, WNDS, WNPS, RNPS );
-- maak een samenvatting van het karyogram

PROCEDURE  samenvatting
 (  p_meet_id  IN  NUMBER
 ,  p_kafd_id   IN NUMBER := NULL
 ,  p_ongr_id   IN NUMBER := NULL
 ,  x_resultaat  OUT  VARCHAR2
 );
 -- idem, als functie
FUNCTION samenvatting
( p_meet_id IN NUMBER
)
RETURN  VARCHAR2;


END BEH_KGC_KARY_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_KGC_KARY_00" IS
-- Formatteer de invoer conform de afspraken
-- Voorbeelden, chromosoom 5
-- invoer           resultaat
-- ---------------- ------------------------
-- +                +5
-- -                -5
-- +?               +?5
-- ?                ?5
-- ?                ?5
-- ?p               ?5p
-- -,-              -5,-5
-- del              del(5)
-- der add          der add(5)
-- del,der          del(5),der(5)
-- del(p12)         del(5)(p12)
-- t(5;15)          t(5;15)
-- t(;15)           t(5;15)
-- t(;15)(p12;q23)  t(5;15)(p12;q23)
-- Vpoorbeeld marker:
-- +1,+2            +mar1,+mar2


FUNCTION formatteer
( p_chromosoom IN VARCHAR2
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  v_waarde VARCHAR2(100) := p_waarde;
  v_een_waarde VARCHAR2(100);
  v_pos NUMBER;
  v_vast VARCHAR2(100);
-- PL/SQL Block
BEGIN
  -- komma scheidt aparte observaties van een chromosoom: formatteer per observatie
  WHILE ( v_waarde IS NOT NULL )
  LOOP
    v_een_waarde := NULL;
    IF ( INSTR( v_waarde, ',' ) > 0 )
    THEN
      v_een_waarde := LTRIM( RTRIM( SUBSTR( v_waarde
                                          , 1
                                          , INSTR( v_waarde, ',' ) - 1
                                          )
                                  )
                           );
      v_waarde := LTRIM( RTRIM( SUBSTR( v_waarde
                                      , INSTR( v_waarde, ',' ) + 1
                                      )
                              )
                       );
    ELSE
      v_een_waarde := v_waarde;
      v_waarde := NULL;
    END IF;

    -- deel tussen haakjes (niet t...) staat vast en wordt straks achteraan gezet, bv (p12;q23)
    v_vast := NULL;
    v_pos := NVL( INSTR( v_een_waarde, 't(' ) + 2, 0 );
    IF ( INSTR( v_een_waarde, '(' ) > v_pos )
    THEN
      v_vast := v_een_waarde;
      v_vast := LTRIM( RTRIM( SUBSTR( v_vast
                                    , INSTR( v_vast, '(', v_pos )
                                    )
                               )
                        );
      v_vast := LTRIM( RTRIM( SUBSTR( v_vast
                                    , 1
                                    , INSTR( v_vast, ')', v_pos )
                                    )
                            )
                     );
    END IF;
    v_een_waarde := REPLACE( v_een_waarde, v_vast );

    -- bij translokaties staat de chromosoomaanduiding tussen de haken
    -- bij + en - en ? wordt de chromosoomaanduiding direkt erachter geplaatst
    -- anders wordt de chromosoomaanduiding tussen haken erachter geplaatst
    IF ( INSTR( v_een_waarde, 't(' ) > 0 )
    THEN
      IF ( INSTR( v_een_waarde, 't(;' ) > 0 )
      THEN
        v_een_waarde := REPLACE( v_een_waarde, 't(;', 't('||p_chromosoom||';' );
      END IF;
    ELSIF ( v_een_waarde LIKE '%+%' )
    THEN
      v_een_waarde := REPLACE( v_een_waarde, '+', '+'||p_chromosoom );
    ELSIF ( v_een_waarde LIKE '%-%' )
    THEN
      v_een_waarde := REPLACE( v_een_waarde, '-', '-'||p_chromosoom );
    ELSIF ( v_een_waarde LIKE '%?%' )
    THEN
      v_een_waarde := REPLACE( v_een_waarde, '?', '?'||p_chromosoom );
    ELSE
      v_een_waarde := v_een_waarde||'('||p_chromosoom||')';
    END IF;
    IF ( v_return IS NULL )
    THEN
      v_return := v_een_waarde||v_vast;
    ELSE
      v_return := v_return||','||v_een_waarde||v_vast;
    END IF;
  END LOOP;
  RETURN( v_return );
END formatteer;

FUNCTION  chromosomen
 (  p_meet_id  IN  NUMBER
 ,  p_cel  IN  NUMBER
 ,  p_kwfr_id  IN  NUMBER
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2( 32767 );
  CURSOR kary_cur
  IS
    SELECT aantal
    ,      xy
    ,      c1
    ,      c2
    ,      c3
    ,      c4
    ,      c5
    ,      c6
    ,      c7
    ,      c8
    ,      c9
    ,      c10
    ,      c11
    ,      c12
    ,      c13
    ,      c14
    ,      c15
    ,      c16
    ,      c17
    ,      c18
    ,      c19
    ,      c20
    ,      c21
    ,      c22
    ,      c_extra
    ,      tekst_chromosomen
    FROM   kgc_karyogrammen
    WHERE  meet_id = p_meet_id
    AND    cel = p_cel
    AND  kwfr_id = p_kwfr_id
    ;
  kary_rec kary_cur%rowtype;

  PROCEDURE voeg_toe
  ( p_tekst IN VARCHAR2
  )
  IS
   BEGIN
    IF ( p_tekst IS NOT NULL )
    THEN
      IF ( v_return IS NULL )
      THEN
        v_return := p_tekst;
      ELSE
        v_return := v_return||','||p_tekst;
      END IF;
    END IF;
  END voeg_toe;
BEGIN
  OPEN  kary_cur;
  FETCH kary_cur
  INTO  kary_rec;
  CLOSE kary_cur;
  IF kary_rec.tekst_chromosomen IS NULL
  THEN
    voeg_toe( kary_rec.aantal||','||kary_rec.xy );
    voeg_toe( formatteer( '1', kary_rec.c1 ) );
    voeg_toe( formatteer( '2', kary_rec.c2 ) );
    voeg_toe( formatteer( '3', kary_rec.c3 ) );
    voeg_toe( formatteer( '4', kary_rec.c4 ) );
    voeg_toe( formatteer( '5', kary_rec.c5 ) );
    voeg_toe( formatteer( '6', kary_rec.c6 ) );
    voeg_toe( formatteer( '7', kary_rec.c7 ) );
    voeg_toe( formatteer( '8', kary_rec.c8 ) );
    voeg_toe( formatteer( '9', kary_rec.c9 ) );
    voeg_toe( formatteer( '10', kary_rec.c10 ) );
    voeg_toe( formatteer( '11', kary_rec.c11 ) );
    voeg_toe( formatteer( '12', kary_rec.c12 ) );
    voeg_toe( formatteer( '13', kary_rec.c13 ) );
    voeg_toe( formatteer( '14', kary_rec.c14 ) );
    voeg_toe( formatteer( '15', kary_rec.c15 ) );
    voeg_toe( formatteer( '16', kary_rec.c16 ) );
    voeg_toe( formatteer( '17', kary_rec.c17 ) );
    voeg_toe( formatteer( '18', kary_rec.c18 ) );
    voeg_toe( formatteer( '19', kary_rec.c19 ) );
    voeg_toe( formatteer( '20', kary_rec.c20 ) );
    voeg_toe( formatteer( '21', kary_rec.c21 ) );
    voeg_toe( formatteer( '22', kary_rec.c22 ) );
--  voeg_toe( formatteer( 'mar', kary_rec.c_extra ) ); --
    voeg_toe( formatteer( NULL, REPLACE(kary_rec.c_extra,'+','+mar') ) );
  ELSE
    voeg_toe( kary_rec.tekst_chromosomen);
  END IF;
  RETURN( v_return );
END chromosomen;


PROCEDURE samenvatting
( p_meet_id IN NUMBER
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, x_resultaat OUT VARCHAR2
)
IS
  v_kafd_id number;
  v_ongr_id number;
  cursor meet_cur
  is
    select onde.kafd_id
    ,      onde.ongr_id
    from   kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    where  meet.meti_id = meti.meti_id
    and    meti.onde_id = onde.onde_id
    and    meet.meet_id = p_meet_id
    ;
  CURSOR kary_cur
  ( b_aantal IN NUMBER
  , b_aantal_normaal IN NUMBER
  )
  IS
    SELECT beh_kgc_kary_00.chromosomen( meet_id, cel, kwfr_id ) chromosomen
    ,      COUNT(*) aantal
    FROM   kgc_karyogrammen
    WHERE  meet_id = p_meet_id
    GROUP BY beh_kgc_kary_00.chromosomen( meet_id, cel, kwfr_id )
    HAVING COUNT(*) > DECODE( beh_kgc_kary_00.chromosomen( meet_id, cel, kwfr_id)
                            , '46,XX', b_aantal_normaal - 1
                            , '46,XY', b_aantal_normaal - 1
                            , b_aantal - 1
                            )
    ORDER BY DECODE( beh_kgc_kary_00.chromosomen( meet_id, cel, kwfr_id)
                   , '46,XX', 9 -- normale uitslag altijd achteraan
                   , '46,XY', 9 -- normale uitslag altijd achteraan
                   , 1
                   )
    ,        COUNT(*) DESC
    ;
  v_samenvatting VARCHAR2(2000) := NULL;
  v_aantal NUMBER := 2;
  v_aantal_normaal NUMBER := 2;
  --
BEGIN
  if ( p_kafd_id is not null
   and p_ongr_id is not null
     )
  then
    v_kafd_id := p_kafd_id;
    v_ongr_id := p_ongr_id;
  else
    open  meet_cur;
    fetch meet_cur
    into  v_kafd_id
    ,     v_ongr_id;
    close meet_cur;
  end if;
  BEGIN
    v_aantal_normaal := TO_NUMBER( kgc_sypa_00.standaard_waarde
                                   ( p_parameter_code => 'MIN_AANTAL_NORMALE_CELLEN'
                                   , p_kafd_id => v_kafd_id
                                   , p_ongr_id => v_ongr_id
                                   )
                                 );
  EXCEPTION
    WHEN OTHERS
    THEN
      v_aantal_normaal := NULL;
  END;
  IF ( v_aantal_normaal IS NULL )
  THEN
    v_aantal_normaal := 2;
  END IF;
  --
  FOR kary_rec IN kary_cur
                  ( b_aantal => v_aantal
                  , b_aantal_normaal => v_aantal_normaal
                  )
  LOOP
    IF ( kary_rec.chromosomen IS NOT NULL )
    THEN
      IF ( v_samenvatting IS NULL )
      THEN
        v_samenvatting := kary_rec.chromosomen||'['||kary_rec.aantal||']';
      ELSE
        BEGIN
          v_samenvatting := v_samenvatting||'/'||kary_rec.chromosomen||'['||kary_rec.aantal||']';
        EXCEPTION
          WHEN VALUE_ERROR
          THEN
            v_samenvatting := SUBSTR( v_samenvatting||'/'||kary_rec.chromosomen||'['||kary_rec.aantal||']', 1, 1996 )||' ...';
        END;
      END IF;
    END IF;
  END LOOP;
  x_resultaat := v_samenvatting;
END samenvatting;

FUNCTION  samenvatting
 (  p_meet_id  IN  NUMBER
 )
 RETURN   VARCHAR2
 IS
  v_resultaat VARCHAR2(2000);
 BEGIN
  samenvatting
  ( p_meet_id => p_meet_id
  , p_kafd_id => NULL
  , p_ongr_id => NULL
  , x_resultaat => v_resultaat
  );
  RETURN( v_resultaat );
END samenvatting;


END  beh_kgc_kary_00;
/

/
QUIT
