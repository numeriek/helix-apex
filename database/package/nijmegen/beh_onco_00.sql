CREATE OR REPLACE PACKAGE "HELIX"."BEH_ONCO_00" AS

  procedure laad_onco;

END BEH_ONCO_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_ONCO_00" as

g_scheidingsteken varchar2(2) := ', ';

-- Cursor laad_onco laadt de onco gegevens uit PASS via de PASS database link.
-- De database link definitie bepaald welke PASS omgeving er gebruikt wordt.
--
-- bv:
--
-- DROP database link "PASS";
--
-- CREATE database link "PASS"
-- connect to "PASSGeneticaBDUser"
-- identified by "password"
-- using 'PASS_TEST';
--    of...
-- using 'PASS_ACPT';
--    of...
-- using 'PASS_PROD';
--
cursor c_laad_onco is
select srce_onco."cust_CustomerID" as cust_CustomerID
     , srce_onco."cust_Gender" as cust_Gender
     , srce_onco."cust_BirthDate" as cust_BirthDate
     , srce_onco."cust_Deceased" as cust_Deceased
     , srce_onco."cust_DeceasedDate" as cust_DeceasedDate
     , srce_onco."cust_Indexed" as cust_Indexed
     , srce_onco."cust_ExternalID" as cust_ExternalID
     , srce_onco."t_kanker" as t_kanker
     , srce_onco."t_1_dici_DiagDesc" as t_1_dici_DiagDesc
     , srce_onco."t_1_cudi_Age" as t_1_cudi_Age
     , srce_onco."t_1_dilo_LocationDesc" as t_1_dilo_LocationDesc
     , srce_onco."t_1_dipa_PathologyDesc" as t_1_dipa_PathologyDesc
     , srce_onco."t_1_cudi_Verified" as t_1_cudi_Verified
     , srce_onco."t_2_dici_DiagDesc" as t_2_dici_DiagDesc
     , srce_onco."t_2_cudi_Age" as t_2_cudi_Age
     , srce_onco."t_2_dilo_LocationDesc" as t_2_dilo_LocationDesc
     , srce_onco."t_2_dipa_PathologyDesc" as t_2_dipa_PathologyDesc
     , srce_onco."t_2_cudi_Verified" as t_2_cudi_Verified
     , srce_onco."t_3_dici_DiagDesc" as t_3_dici_DiagDesc
     , srce_onco."t_3_cudi_Age" as t_3_cudi_Age
     , srce_onco."t_3_dilo_LocationDesc" as t_3_dilo_LocationDesc
     , srce_onco."t_3_dipa_PathologyDesc" as t_3_dipa_PathologyDesc
     , srce_onco."t_3_cudi_Verified" as t_3_cudi_Verified
     , srce_onco."t_4_dici_DiagDesc" as t_4_dici_DiagDesc
     , srce_onco."t_4_cudi_Age" as t_4_cudi_Age
     , srce_onco."t_4_dilo_LocationDesc" as t_4_dilo_LocationDesc
     , srce_onco."t_4_dipa_PathologyDesc" as t_4_dipa_PathologyDesc
     , srce_onco."t_4_cudi_Verified" as t_4_cudi_Verified
     , srce_onco."t_5_dici_DiagDesc" as t_5_dici_DiagDesc
     , srce_onco."t_5_cudi_Age" as t_5_cudi_Age
     , srce_onco."t_5_dilo_LocationDesc" as t_5_dilo_LocationDesc
     , srce_onco."t_5_dipa_PathologyDesc" as t_5_dipa_PathologyDesc
     , srce_onco."t_5_cudi_Verified" as t_5_cudi_Verified
     , srce_onco."pedi_PedigreeID" as pedi_PedigreeID
     , srce_onco."pedi_LastUpdated" as pedi_LastUpdated
     , srce_onco."pedi_PedigreeName" as pedi_PedigreeName
     , srce_onco."pedi_ExternalID" as pedi_ExternalID
from   "beh_onco01_vw"@PASS srce_onco;


--####################
function check_dna_opslag(p_pers_id in number) return varchar2 is

  v_return varchar2(10000);

  cursor c_opslag is
  select count(*) aantal
  from kgc_monsters mons
  ,       bas_fracties frac
  ,        bas_fractie_types frty
  where mons.pers_id = p_pers_id
  and   mons.mons_id = frac.mons_id
  and   frac.frty_id = frty.frty_id
  and   frty.code in ('DNA', 'DPA', 'PAN', 'PAT')
  and   upper(frac.status) = upper('O');

  r_opslag c_opslag%rowtype;

begin

  open c_opslag;
  fetch c_opslag
  into r_opslag;
  close c_opslag;

  if r_opslag.aantal > 0 then
      v_return := 'J';
  else
      v_return := 'N';
  end if;

  return v_return;

end check_dna_opslag;


--#####################
function check_indi(p_pers_id in number, p_indi_code in varchar) return varchar2 is

  v_return varchar2(1);

  cursor c_indi is
  select count(*) aantal
  from kgc_onderzoeken onde,
       kgc_onderzoek_indicaties onin,
       kgc_indicatie_teksten indi
  where onde.onde_id = onin.onde_id
  and   onin.indi_id = indi.indi_id
  and   onde.pers_id = p_pers_id
  and   upper(indi.code) = upper(trim(p_indi_code));

  r_indi c_indi%rowtype;

begin

  open c_indi;
  fetch c_indi
  into r_indi;
  close c_indi;

  if r_indi.aantal > 0 then
      v_return := 'J';
  else
      v_return := 'N';
  end if;

  return v_return;

end check_indi;


--#####################
function haal_alle_frac_op(p_pers_id in number) return varchar2 is

  v_return varchar2(10000);

  cursor c_frac is
  select frac.fractienummer, frac.status
  from kgc_monsters mons
  ,       bas_fracties frac
  ,       bas_fractie_types frty
  where mons.pers_id = p_pers_id
  and   mons.mons_id = frac.mons_id
  and   frac.frty_id = frty.frty_id
  and   frty.code in ('DNA', 'DPA', 'PAN', 'PAT');

begin

  for r_frac in c_frac loop

      if v_return is null then

          if upper(r_frac.status) = upper('O') then
              v_return := r_frac.fractienummer || ' (beschikbaar)';
          else
              v_return := r_frac.fractienummer;
          end if;
      else
          if upper(r_frac.status) = upper('O') then
              v_return := v_return || g_scheidingsteken || r_frac.fractienummer || ' (beschikbaar)';
          else
              v_return := v_return || g_scheidingsteken || r_frac.fractienummer;
          end if;
      end if;

  end loop;

  -- Truncate on the max length of the DNA_FRACTIES field.
  -- Some rows are getting to large.
  return substr(v_return, 1, 2000);

end haal_alle_frac_op;



--####################
function haal_onui_omsch_op(p_pers_id in number, p_indi_code in varchar) return varchar2 is

  v_return varchar2(10000);

  cursor c_onui is
  select onui.*
  from    kgc_onderzoeken onde,
          kgc_onderzoek_uitslagcodes onui,
          kgc_onderzoek_indicaties onin,
          kgc_indicatie_teksten indi
  where   onde.onui_id = onui.onui_id
  and     onde.onde_id = onin.onde_id
  and     onin.indi_id = indi.indi_id
  and     onde.pers_id = p_pers_id
  and     upper(indi.code) = upper(trim(p_indi_code));

begin

  for r_onui in c_onui loop

    if v_return is null then
      v_return := r_onui.omschrijving;
    else
      v_return := v_return || g_scheidingsteken || r_onui.omschrijving;
    end if;

  end loop;

  return v_return;

end haal_onui_omsch_op;



--####################
function haal_mdet_op(p_pers_id in number, p_stof_code in varchar, p_prompt in varchar) return varchar2 is

  v_return varchar2(10000);

  cursor c_mdet is
  select distinct(mdet.waarde) waarde
  from kgc_onderzoeken onde,
       kgc_monsters mons,
       kgc_onderzoek_monsters onmo,
       bas_fracties frac,
       bas_metingen meti,
       bas_meetwaarden meet,
       bas_meetwaarde_details mdet,
       kgc_stoftesten stof
  where onde.onde_id = onmo.onde_id
  and   onmo.mons_id = mons.mons_id
  and   mons.mons_id = frac.mons_id
  and   frac.frac_id = meti.frac_id
  and   meti.meti_id = meet.meti_id
  and   onde.onde_id = meti.onde_id
  and   meet.meet_id = mdet.meet_id
  and   mons.pers_id = p_pers_id
  and   upper(stof.code) = upper(p_stof_code)
  and   upper(mdet.prompt) = upper(p_prompt);

begin

  for r_mdet in c_mdet loop

    if v_return is null then
      v_return := r_mdet.waarde;
    else
      v_return := v_return || g_scheidingsteken || r_mdet.waarde;
    end if;

  end loop;

  return v_return;

end haal_mdet_op;



--####################
function haal_onde_muta_mdet_op(p_pers_id in number, p_kolom in varchar) return varchar2 is

  type c_type is ref cursor;

  r c_type;
  v_kolom varchar2(2000);
  v_return varchar2(10000);
  v_stmt varchar2(10000) :=
    'select distinct(' || p_kolom || ') from beh_stbm_mdet_attr_vw where onde_pers_id = ' || to_char(p_pers_id);

begin

  open r for v_stmt;
    loop
      fetch r into v_kolom;
      exit when r%notfound;

      if v_return is null then
        v_return := v_kolom;
      else
        v_return := v_return || g_scheidingsteken || v_kolom;
      end if;
    end loop;
  close r;

  return v_return;

end haal_onde_muta_mdet_op;



--####################
function haal_alle_onui_omsch_op(p_pers_id in number) return varchar2 is

  v_return varchar2(10000);

  cursor c_onui is
  select  onde.onderzoeknr    ||
          ' ('                ||
          gen.gen   ||
          ', '   ||
          onui.omschrijving   ||
          ')' waarde
  from    kgc_onderzoeken onde,
          kgc_onderzoek_uitslagcodes onui,
          (
            select distinct(gen) gen, onde_onderzoeknr onderzoeknr
            from beh_stbm_mdet_attr_vw
            where onde_pers_id = p_pers_id
          ) gen
  where   onde.onui_id = onui.onui_id
  and     onde.pers_id = p_pers_id
  and     onde.onderzoeknr = gen.onderzoeknr(+);

begin
  for r_onui in c_onui loop

    if v_return is null then
      v_return := r_onui.waarde;
    else
      v_return := v_return || g_scheidingsteken || r_onui.waarde;
    end if;

  end loop;

  return v_return;

end haal_alle_onui_omsch_op;



--####################
function haal_alle_onui_code_op(p_pers_id in number) return varchar2 is

  v_return varchar2(10000);

  cursor c_onui is
  select  onde.onderzoeknr    ||
          ' ('                ||
          onui.code   ||
          ')' waarde
  from    kgc_onderzoeken onde,
          kgc_onderzoek_uitslagcodes onui
  where   onde.onui_id = onui.onui_id
  and     onde.pers_id = p_pers_id;

begin

  for r_onui in c_onui loop

    if v_return is null then
      v_return := r_onui.waarde;
    else
      v_return := v_return || g_scheidingsteken || r_onui.waarde;
    end if;

  end loop;

  return v_return;

end haal_alle_onui_code_op;



--####################
-- Verwerk 1 row met beh_onco data.
-- Parameter p_laad_onco bevat de data die opgeslagen moet worden.
-- Als de insert indicator true is dan wordt er geinsert, anders wordt er geupdate.
--
procedure laad_onco_verwerk_row( p_laad_onco        IN c_laad_onco%rowtype
                               , p_insert_indicator IN boolean
                               , p_datum_import     IN date
                               , p_count_upd        IN OUT number
                               , p_count_ins        IN OUT number
                               ) is

  v_beh_onco_row  beh_onco%rowtype;
  v_pers_id       kgc_personen.pers_id%type;
  v_pers_zisnr    kgc_personen.zisnr%type;
  v_pers_bsn      kgc_personen.bsn%type;

begin

  v_beh_onco_row.cust_customerid := p_laad_onco.cust_customerid;
  v_beh_onco_row.cust_gender := p_laad_onco.cust_gender;

  v_beh_onco_row.cust_birthdate := p_laad_onco.cust_birthdate;
  v_beh_onco_row.cust_Deceased := p_laad_onco.cust_Deceased;
  v_beh_onco_row.cust_DeceasedDate := p_laad_onco.cust_DeceasedDate;
  v_beh_onco_row.cust_ExternalID := p_laad_onco.cust_ExternalID;

  v_beh_onco_row.cust_indexed := p_laad_onco.cust_indexed;
  v_beh_onco_row.cust_externalid := p_laad_onco.cust_externalid;
  v_beh_onco_row.t_kanker := p_laad_onco.t_kanker;

  v_beh_onco_row.t_1_dici_DiagDesc := p_laad_onco.t_1_dici_DiagDesc;
  v_beh_onco_row.t_1_cudi_Age := p_laad_onco.t_1_cudi_Age;
  v_beh_onco_row.t_1_dilo_LocationDesc := p_laad_onco.t_1_dilo_LocationDesc;
  v_beh_onco_row.t_1_dipa_PathologyDesc := p_laad_onco.t_1_dipa_PathologyDesc;
  v_beh_onco_row.t_1_cudi_Verified := p_laad_onco.t_1_cudi_Verified;

  v_beh_onco_row.t_2_dici_DiagDesc := p_laad_onco.t_2_dici_DiagDesc;
  v_beh_onco_row.t_2_cudi_Age := p_laad_onco.t_2_cudi_Age;
  v_beh_onco_row.t_2_dilo_LocationDesc := p_laad_onco.t_2_dilo_LocationDesc;
  v_beh_onco_row.t_2_dipa_PathologyDesc := p_laad_onco.t_2_dipa_PathologyDesc;
  v_beh_onco_row.t_2_cudi_Verified := p_laad_onco.t_2_cudi_Verified;

  v_beh_onco_row.t_3_dici_DiagDesc := p_laad_onco.t_3_dici_DiagDesc;
  v_beh_onco_row.t_3_cudi_Age := p_laad_onco.t_3_cudi_Age;
  v_beh_onco_row.t_3_dilo_LocationDesc := p_laad_onco.t_3_dilo_LocationDesc;
  v_beh_onco_row.t_3_dipa_PathologyDesc := p_laad_onco.t_3_dipa_PathologyDesc;
  v_beh_onco_row.t_3_cudi_Verified := p_laad_onco.t_3_cudi_Verified;

  v_beh_onco_row.t_4_dici_DiagDesc := p_laad_onco.t_4_dici_DiagDesc;
  v_beh_onco_row.t_4_cudi_Age := p_laad_onco.t_4_cudi_Age;
  v_beh_onco_row.t_4_dilo_LocationDesc := p_laad_onco.t_4_dilo_LocationDesc;
  v_beh_onco_row.t_4_dipa_PathologyDesc := p_laad_onco.t_4_dipa_PathologyDesc;
  v_beh_onco_row.t_4_cudi_Verified := p_laad_onco.t_4_cudi_Verified;

  v_beh_onco_row.t_5_dici_DiagDesc := p_laad_onco.t_5_dici_DiagDesc;
  v_beh_onco_row.t_5_cudi_Age := p_laad_onco.t_5_cudi_Age;
  v_beh_onco_row.t_5_dilo_LocationDesc := p_laad_onco.t_5_dilo_LocationDesc;
  v_beh_onco_row.t_5_dipa_PathologyDesc := p_laad_onco.t_5_dipa_PathologyDesc;
  v_beh_onco_row.t_5_cudi_Verified := p_laad_onco.t_5_cudi_Verified;

  v_beh_onco_row.pedi_pedigreeID := p_laad_onco.pedi_pedigreeID;
  v_beh_onco_row.pedi_LastUpdated := p_laad_onco.pedi_LastUpdated;
  v_beh_onco_row.pedi_PedigreeName := trim(p_laad_onco.pedi_PedigreeName);
  v_beh_onco_row.pedi_ExternalID := p_laad_onco.pedi_ExternalID;

  v_beh_onco_row.datum_import := p_datum_import;

  begin
    select pers_id, zisnr, bsn
    into v_pers_id, v_pers_zisnr, v_pers_bsn
    from kgc_personen
    where pers_id = p_laad_onco.cust_ExternalID;
  exception when others then
    v_pers_id := null;
  end;

  if v_pers_id is not null then

    v_beh_onco_row.zisnr := v_pers_zisnr;
    v_beh_onco_row.bsn   := v_pers_bsn;

    v_beh_onco_row.familienummers := beh_stbm_00.haal_alle_fami_op (v_pers_id);
    v_beh_onco_row.p_nummers := beh_stbm_00.haal_alle_klingen_onde_op (v_pers_id);

    v_beh_onco_row.diag_2_1 := beh_onco_00.check_indi(v_pers_id, '2.1');
    v_beh_onco_row.diag_2_2 := beh_onco_00.check_indi(v_pers_id, '2.2');
    v_beh_onco_row.diag_2_3 := beh_onco_00.check_indi(v_pers_id, '2.3');
    v_beh_onco_row.diag_2_4 := beh_onco_00.check_indi(v_pers_id, '2.4');
    v_beh_onco_row.diag_2_5 := beh_onco_00.check_indi(v_pers_id, '2.5');
    v_beh_onco_row.diag_2_6 := beh_onco_00.check_indi(v_pers_id, '2.6');
    v_beh_onco_row.diag_2_7 := beh_onco_00.check_indi(v_pers_id, '2.7');

    v_beh_onco_row.t_nummers_intern := beh_onco_00.haal_mdet_op(v_pers_id, 'MSI_005', 'T-intern');
    v_beh_onco_row.t_nummers_extern := beh_onco_00.haal_mdet_op(v_pers_id, 'MSI_005', 'T-extern');

    v_beh_onco_row.dna_aanwezig := beh_onco_00.check_dna_opslag(v_pers_id);
    v_beh_onco_row.dna_fracties := beh_onco_00.haal_alle_frac_op(v_pers_id);

    v_beh_onco_row.msi := beh_onco_00.check_indi(v_pers_id, 'MI');
    v_beh_onco_row.Uitslag_MSI := beh_onco_00.haal_onui_omsch_op(v_pers_id, 'MI');
    v_beh_onco_row.Weefsel_MSI := beh_onco_00.haal_mdet_op(v_pers_id, 'MSI_005', 'Materiaal');
    v_beh_onco_row.hypermeth := beh_onco_00.check_indi(v_pers_id, 'MLH1-P');

    v_beh_onco_row.IMH_MLH1 := beh_onco_00.haal_mdet_op(v_pers_id, 'MLH1_060', 'Kleuring');
    v_beh_onco_row.IMH_MSH2 := beh_onco_00.haal_mdet_op(v_pers_id, 'MSH2_057', 'Kleuring');
    v_beh_onco_row.IMH_MSH6 := beh_onco_00.haal_mdet_op(v_pers_id, 'MSH6_001', 'Kleuring');
    v_beh_onco_row.IMH_PMS2 := beh_onco_00.haal_mdet_op(v_pers_id, 'PMS2_001', 'Kleuring');

    v_beh_onco_row.gen := beh_onco_00.haal_onde_muta_mdet_op(v_pers_id, 'gen');

    v_beh_onco_row.pathogene_mutatie := beh_onco_00.haal_onde_muta_mdet_op(v_pers_id, 'pathogene_mutatie');

    v_beh_onco_row.uvii := beh_onco_00.haal_onde_muta_mdet_op(v_pers_id, 'uvii');

    v_beh_onco_row.uviii := beh_onco_00.haal_onde_muta_mdet_op(v_pers_id, 'uviii');

    v_beh_onco_row.overige_resultaten := beh_onco_00.haal_onde_muta_mdet_op(v_pers_id, 'Overige_resultaten');

    v_beh_onco_row.alle_onui_omsch := beh_onco_00.haal_alle_onui_omsch_op(v_pers_id);

    if ( v_beh_onco_row.pathogene_mutatie is not null
      or v_beh_onco_row.uvii is not null
      or v_beh_onco_row.uviii is not null
      or v_beh_onco_row.overige_resultaten is not null
       )
    then
      v_beh_onco_row.mutatieonderzoek := 'J';
    else
      v_beh_onco_row.mutatieonderzoek := 'N';
    end if;

    v_beh_onco_row.alle_uits_codes := beh_onco_00.haal_alle_onui_code_op(v_pers_id);

  end if;


  if p_insert_indicator then
    -- Insert een nieuwe regel in de BEH_ONCO tabel.

    insert into helix.beh_onco
    ( cust_customerid, cust_gender, cust_birthdate, cust_deceased, cust_deceaseddate, cust_indexed
    , cust_externalid, t_kanker, t_1_dici_diagdesc, t_1_cudi_age, t_1_dilo_locationdesc, t_1_dipa_pathologydesc
    , t_1_cudi_verified, t_2_dici_diagdesc, t_2_cudi_age, t_2_dilo_locationdesc, t_2_dipa_pathologydesc, t_2_cudi_verified
    , t_3_dici_diagdesc, t_3_cudi_age, t_3_dilo_locationdesc, t_3_dipa_pathologydesc, t_3_cudi_verified, t_4_dici_diagdesc
    , t_4_cudi_age, t_4_dilo_locationdesc, t_4_dipa_pathologydesc, t_4_cudi_verified, t_5_dici_diagdesc, t_5_cudi_age
    , t_5_dilo_locationdesc, t_5_dipa_pathologydesc, t_5_cudi_verified, pedi_pedigreeid, pedi_lastupdated, pedi_pedigreename
    , pedi_externalid, zisnr, bsn, familienummers, p_nummers, diag_2_1
    , diag_2_2, diag_2_3, diag_2_4, diag_2_5, diag_2_6, diag_2_7
    , t_nummers_intern, t_nummers_extern, dna_fracties, dna_aanwezig, msi, uitslag_msi
    , weefsel_msi, hypermeth, imh_mlh1, imh_msh2, imh_msh6, imh_pms2
    , mutatieonderzoek, gen, pathogene_mutatie, uvii, uviii, overige_resultaten
    , fhat, myriad, alle_onui_omsch, alle_informed_consents, datum_import, alle_uits_codes
    )
    values
    ( v_beh_onco_row.cust_customerid
    , v_beh_onco_row.cust_gender
    , v_beh_onco_row.cust_birthdate
    , v_beh_onco_row.cust_deceased
    , v_beh_onco_row.cust_deceaseddate
    , v_beh_onco_row.cust_indexed
    , v_beh_onco_row.cust_externalid
    , v_beh_onco_row.t_kanker
    , v_beh_onco_row.t_1_dici_diagdesc
    , v_beh_onco_row.t_1_cudi_age
    , v_beh_onco_row.t_1_dilo_locationdesc
    , v_beh_onco_row.t_1_dipa_pathologydesc
    , v_beh_onco_row.t_1_cudi_verified
    , v_beh_onco_row.t_2_dici_diagdesc
    , v_beh_onco_row.t_2_cudi_age
    , v_beh_onco_row.t_2_dilo_locationdesc
    , v_beh_onco_row.t_2_dipa_pathologydesc
    , v_beh_onco_row.t_2_cudi_verified
    , v_beh_onco_row.t_3_dici_diagdesc
    , v_beh_onco_row.t_3_cudi_age
    , v_beh_onco_row.t_3_dilo_locationdesc
    , v_beh_onco_row.t_3_dipa_pathologydesc
    , v_beh_onco_row.t_3_cudi_verified
    , v_beh_onco_row.t_4_dici_diagdesc
    , v_beh_onco_row.t_4_cudi_age
    , v_beh_onco_row.t_4_dilo_locationdesc
    , v_beh_onco_row.t_4_dipa_pathologydesc
    , v_beh_onco_row.t_4_cudi_verified
    , v_beh_onco_row.t_5_dici_diagdesc
    , v_beh_onco_row.t_5_cudi_age
    , v_beh_onco_row.t_5_dilo_locationdesc
    , v_beh_onco_row.t_5_dipa_pathologydesc
    , v_beh_onco_row.t_5_cudi_verified
    , v_beh_onco_row.pedi_pedigreeid
    , v_beh_onco_row.pedi_lastupdated
    , v_beh_onco_row.pedi_pedigreename
    , v_beh_onco_row.pedi_externalid
    , v_beh_onco_row.zisnr
    , v_beh_onco_row.bsn
    , v_beh_onco_row.familienummers
    , v_beh_onco_row.p_nummers
    , v_beh_onco_row.diag_2_1
    , v_beh_onco_row.diag_2_2
    , v_beh_onco_row.diag_2_3
    , v_beh_onco_row.diag_2_4
    , v_beh_onco_row.diag_2_5
    , v_beh_onco_row.diag_2_6
    , v_beh_onco_row.diag_2_7
    , v_beh_onco_row.t_nummers_intern
    , v_beh_onco_row.t_nummers_extern
    , v_beh_onco_row.dna_fracties
    , v_beh_onco_row.dna_aanwezig
    , v_beh_onco_row.msi
    , v_beh_onco_row.uitslag_msi
    , v_beh_onco_row.weefsel_msi
    , v_beh_onco_row.hypermeth
    , v_beh_onco_row.imh_mlh1
    , v_beh_onco_row.imh_msh2
    , v_beh_onco_row.imh_msh6
    , v_beh_onco_row.imh_pms2
    , v_beh_onco_row.mutatieonderzoek
    , v_beh_onco_row.gen
    , v_beh_onco_row.pathogene_mutatie
    , v_beh_onco_row.uvii
    , v_beh_onco_row.uviii
    , v_beh_onco_row.overige_resultaten
    , v_beh_onco_row.fhat
    , v_beh_onco_row.myriad
    , v_beh_onco_row.alle_onui_omsch
    , v_beh_onco_row.alle_informed_consents
    , v_beh_onco_row.datum_import
    , v_beh_onco_row.alle_uits_codes
    );

    p_count_ins := p_count_ins + 1;

  else
    -- Update een bestaande regel in de BEH_ONCO tabel.

    update beh_onco
    set    cust_gender            = v_beh_onco_row.cust_gender,
           cust_birthdate         = v_beh_onco_row.cust_birthdate,
           cust_deceased          = v_beh_onco_row.cust_deceased,
           cust_deceaseddate      = v_beh_onco_row.cust_deceaseddate,
           cust_indexed           = v_beh_onco_row.cust_indexed,
           cust_externalid        = v_beh_onco_row.cust_externalid,
           t_kanker               = v_beh_onco_row.t_kanker,
           t_1_dici_diagdesc      = v_beh_onco_row.t_1_dici_diagdesc,
           t_1_cudi_age           = v_beh_onco_row.t_1_cudi_age,
           t_1_dilo_locationdesc  = v_beh_onco_row.t_1_dilo_locationdesc,
           t_1_dipa_pathologydesc = v_beh_onco_row.t_1_dipa_pathologydesc,
           t_1_cudi_verified      = v_beh_onco_row.t_1_cudi_verified,
           t_2_dici_diagdesc      = v_beh_onco_row.t_2_dici_diagdesc,
           t_2_cudi_age           = v_beh_onco_row.t_2_cudi_age,
           t_2_dilo_locationdesc  = v_beh_onco_row.t_2_dilo_locationdesc,
           t_2_dipa_pathologydesc = v_beh_onco_row.t_2_dipa_pathologydesc,
           t_2_cudi_verified      = v_beh_onco_row.t_2_cudi_verified,
           t_3_dici_diagdesc      = v_beh_onco_row.t_3_dici_diagdesc,
           t_3_cudi_age           = v_beh_onco_row.t_3_cudi_age,
           t_3_dilo_locationdesc  = v_beh_onco_row.t_3_dilo_locationdesc,
           t_3_dipa_pathologydesc = v_beh_onco_row.t_3_dipa_pathologydesc,
           t_3_cudi_verified      = v_beh_onco_row.t_3_cudi_verified,
           t_4_dici_diagdesc      = v_beh_onco_row.t_4_dici_diagdesc,
           t_4_cudi_age           = v_beh_onco_row.t_4_cudi_age,
           t_4_dilo_locationdesc  = v_beh_onco_row.t_4_dilo_locationdesc,
           t_4_dipa_pathologydesc = v_beh_onco_row.t_4_dipa_pathologydesc,
           t_4_cudi_verified      = v_beh_onco_row.t_4_cudi_verified,
           t_5_dici_diagdesc      = v_beh_onco_row.t_5_dici_diagdesc,
           t_5_cudi_age           = v_beh_onco_row.t_5_cudi_age,
           t_5_dilo_locationdesc  = v_beh_onco_row.t_5_dilo_locationdesc,
           t_5_dipa_pathologydesc = v_beh_onco_row.t_5_dipa_pathologydesc,
           t_5_cudi_verified      = v_beh_onco_row.t_5_cudi_verified,
           pedi_lastupdated       = v_beh_onco_row.pedi_lastupdated,
           pedi_pedigreename      = v_beh_onco_row.pedi_pedigreename,
           pedi_externalid        = v_beh_onco_row.pedi_externalid,
           zisnr                  = v_beh_onco_row.zisnr,
           bsn                    = v_beh_onco_row.bsn,
           familienummers         = v_beh_onco_row.familienummers,
           p_nummers              = v_beh_onco_row.p_nummers,
           diag_2_1               = v_beh_onco_row.diag_2_1,
           diag_2_2               = v_beh_onco_row.diag_2_2,
           diag_2_3               = v_beh_onco_row.diag_2_3,
           diag_2_4               = v_beh_onco_row.diag_2_4,
           diag_2_5               = v_beh_onco_row.diag_2_5,
           diag_2_6               = v_beh_onco_row.diag_2_6,
           diag_2_7               = v_beh_onco_row.diag_2_7,
           t_nummers_intern       = v_beh_onco_row.t_nummers_intern,
           t_nummers_extern       = v_beh_onco_row.t_nummers_extern,
           dna_fracties           = v_beh_onco_row.dna_fracties,
           dna_aanwezig           = v_beh_onco_row.dna_aanwezig,
           msi                    = v_beh_onco_row.msi,
           uitslag_msi            = v_beh_onco_row.uitslag_msi,
           weefsel_msi            = v_beh_onco_row.weefsel_msi,
           hypermeth              = v_beh_onco_row.hypermeth,
           imh_mlh1               = v_beh_onco_row.imh_mlh1,
           imh_msh2               = v_beh_onco_row.imh_msh2,
           imh_msh6               = v_beh_onco_row.imh_msh6,
           imh_pms2               = v_beh_onco_row.imh_pms2,
           mutatieonderzoek       = v_beh_onco_row.mutatieonderzoek,
           gen                    = v_beh_onco_row.gen,
           pathogene_mutatie      = v_beh_onco_row.pathogene_mutatie,
           uvii                   = v_beh_onco_row.uvii,
           uviii                  = v_beh_onco_row.uviii,
           overige_resultaten     = v_beh_onco_row.overige_resultaten,
           fhat                   = v_beh_onco_row.fhat,
           myriad                 = v_beh_onco_row.myriad,
           alle_onui_omsch        = v_beh_onco_row.alle_onui_omsch,
           alle_informed_consents = v_beh_onco_row.alle_informed_consents,
           datum_import           = v_beh_onco_row.datum_import,
           alle_uits_codes        = v_beh_onco_row.alle_uits_codes
    where  cust_customerid        = v_beh_onco_row.cust_customerid
      and  pedi_pedigreeid        = v_beh_onco_row.pedi_pedigreeid;

    p_count_upd := p_count_upd + 1;

  end if;

end;



--####################
procedure laad_onco is

  v_procedure_naam   varchar2(40) := 'beh_onco_00.laad_onco';
  v_count_rows       number;
  v_insert_indicator boolean;
  v_datum_import     date;

  v_count_upd        number := 0;
  v_count_ins        number := 0;

  v_html_message    clob;
  v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                      , p_mede_id        => NULL
                                                                      )
                                        , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                        );

  --email adres voor als het goed is gegaan. Maak het adres leeg om geen mail te sturen.
  v_email_aan_ok    varchar2(100) := 'rene.brand@radboudumc.nl';

  --email adres voor als het fout is gegaan.
  v_email_aan_err   varchar2(100) := 'rene.brand@radboudumc.nl;' || v_email_afzender;

  v_email_onderwerp varchar2(100);
  v_job_onderwerp   varchar2(50);
  v_crlf            varchar2(2) := chr(13)||chr(10);

begin

  v_datum_import := sysdate;

  v_email_onderwerp := 'Helix overzicht ONCO interface';

  v_html_message := 'Dit is een automatisch gegenereerd rapport over het inlezen van ONCO gegevens.</br></br>' || v_crlf;
  v_html_message := v_html_message || 'Uitgevoerd met import datum: ' || to_char(v_datum_import, 'YYYY-MM-DD HH24:MI:SS') || '</br>' || v_crlf;
  v_html_message := v_html_message || 'Uitgevoerd door: ' || user || '</br>' || v_crlf;
  v_html_message := v_html_message || 'Uitgevoerd in database: ' || ora_database_name || '</br></br>' || v_crlf;

  for r_laad_onco in c_laad_onco loop

    --Controleer of het record al bestaat
    select count(*)
    into   v_count_rows
    from   BEH_ONCO
    where  cust_customerid = r_laad_onco.cust_customerid
      and  pedi_PedigreeID = r_laad_onco.pedi_pedigreeid;

    --Als het record nog niet bestaat, insert een nieuwe, anders update bestaande.
    v_insert_indicator := (v_count_rows = 0);

    laad_onco_verwerk_row(r_laad_onco, v_insert_indicator, v_datum_import, v_count_upd, v_count_ins);

  end loop;

  v_html_message := v_html_message || '<ul>';
  v_html_message := v_html_message || '<li>Aantal records toegevoegd: ' || to_char(v_count_ins) || '</li>' || v_crlf;
  v_html_message := v_html_message || '<li>Aantal records bijgewerkt: ' || to_char(v_count_upd) || '</li>' || v_crlf;

  -- Verwijder nu all regels die niet geupdate of geinsert zijn.
  delete from BEH_ONCO where  datum_import != v_datum_import;
  v_html_message := v_html_message || '<li>Aantal records verwijderd: ' || to_char(sql%rowcount) || '</li>' || v_crlf;
  v_html_message := v_html_message || '</ul>';

  commit;

  v_html_message := v_html_message || '</br>' || v_crlf || 'Successvol afgerond op tijdstip: ' || to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') || '</br>' || v_crlf;

  v_html_message := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

  if v_email_aan_ok is not null then
    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan_ok
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );
  end if;


exception when others then

  rollback;

  v_email_onderwerp := v_email_onderwerp || ' (FOUT)';

  v_html_message := v_html_message || 'Fout tijdens ' || v_procedure_naam || ' wijzigingen teruggedraaid.' || '</br>' || v_crlf;
  v_html_message := v_html_message || '</br><font color="red">Foutmelding: '
                                   || sqlerrm || '</br>'
                                   || dbms_utility.format_error_backtrace || '</br>'
                                   || dbms_utility.format_call_stack
                                   ||'</font></br></br>' || v_crlf;
  v_html_message := v_html_message || 'Tijd: ' || to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') || '</br>' || v_crlf;

  v_html_message := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

  if v_email_aan_err is not null then
    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan_err
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );
  end if;

end laad_onco;


end beh_onco_00;
/

/
QUIT
