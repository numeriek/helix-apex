CREATE OR REPLACE PACKAGE "HELIX"."BEH_DIGIDOS_ORIGINELEN" IS
  --
  -- Package voor het opslaan van originele documnenten in digitaal dossier.
  -- De originele documenten worden aangemaakt voor alle bestandstypen waar
  -- indicatie kolom origineel_bewaren = 'Y'.
  --

  function get_btyp_origineel return kgc_bestand_types.btyp_id%type;
  function get_bcat_origineel return kgc_bestand_categorieen.bcat_id%type;

  --Plak het ID nummer achter aan de bestandsnaam
  function voeg_id_toe( p_path in kgc_bestanden.bestand_specificatie%type
                      , p_id in number
                      ) return kgc_bestanden.bestand_specificatie%type;

  --Het kan voorkomen dat de aangemaakte bestandsnaam te lang wordt voor het Windows OS (260 tekens)
  --Deze functie probeert de bestandsnaam in te korten.
  function maak_verkorte_bestandsnaam( p_werk_kopie_bestand in kgc_bestanden.bestand_specificatie%type
                                     , p_id in number
                                     ) return kgc_bestanden.bestand_specificatie%type;

  --Controle of er een origineel bewaard moet worden voor dit bestandstype
  function check_bewaar_origineel(p_btyp_id in kgc_bestand_types.btyp_id%type) return boolean;

  --Bereken het path voor het origineel uit het path van de werkkopie.
  function maak_origineel_path(p_path in kgc_bestanden.bestand_specificatie%type) return kgc_bestanden.bestand_specificatie%type;

  -- Sla het origineel op voor het opgegeven bestand.
  procedure bewaar_origineel( p_bestand_specificatie in kgc_bestanden.bestand_specificatie%type
                            , p_best_id              in kgc_bestanden.best_id%type
                            , p_commentaar           in kgc_bestanden.commentaar%type
                            );

  --Verwerk alle bestanden die aangemaakt zijn na datum <p_from_date> waarvoor
  --nog geen originelen aangemaakt zijn.
  procedure verwerk_originelen( p_from_date in date
                              , p_html_message in out clob
                              , p_aantal in out number
                              , p_errors in out number
                              , p_skip_niet_bestaand in varchar2
                              );

  function check_and_mark_bestand(p_best_id in kgc_bestanden.best_id%type) return boolean;

  -- Verwerk alle bestanden die aangemaakt zijn na laatste draaidatum en
  -- waarvoor originelen aangemaakt moeten terwijl deze er niet zijn.
  procedure verwerk_originelen(p_consolidatie_run in varchar2 := 'N');

  -- Tel het aantal originelen wat in de afgelopen 24 uur is aangemaakt.
  -- Geef een waarschuwing als er 24 uur lang geen originelen aangemaakt zijn.
  -- Deze controle is bedoeld om aan het eind van iedere werkdag te lopen.
  procedure controleer_originelen;

END beh_digidos_originelen;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DIGIDOS_ORIGINELEN" IS

  --Bestandstype voor het originele bestand.
  c_btyp_code_origineel   CONSTANT kgc_bestand_types.code%type := 'ORIGINEEL';

  --Bestandscategorie voor het originele bestand.
  c_bcat_omschr_origineel CONSTANT kgc_bestand_categorieen.omschrijving%type := 'ORIGINELEN';

  v_crlf                  CONSTANT varchar2(2) := chr(13)||chr(10);

  --Locaties worden omgereken d.m.v. een REGEXP_REPLACE(from, to).
  --Voor de METAB bestanden wordt voor <from> de v_regexp_from_location_metab
  --waarde gebruikt, voor GENOOM de v_regexp_from_location_genoom etc.
  v_regexp_from_location_metab  varchar(100) := '(\\\\umcms33\\kgcn_dossiershelix\$)\\(def(_.{4})?)\\(.+)';
  v_regexp_from_location_genoom varchar(100) := '(\\\\umcms005\\gen_stg)\$\\helix\\(.{4}())\\(.+)';
  v_regexp_from_location_dragon varchar(100) := '(\\\\umcms005\\gen_stg)\$\\dragon\\(.{4}())\\(.+)';

  v_regexp_to_location          varchar(100) := '\\\\umcms005\\gen_stg$\helixDigidosOriginelen\\\2\\\4';
  v_regexp_to_location_dragon   varchar(100) := '\\\\umcms005\\gen_stg$\helixDigidosOriginelen\\\2\\digidos\\\4';

  v_filter_location_metab       varchar(100) := '\\umcms33\kgcn_dossiershelix\kgcn_dossiershelix$\def%';
  v_filter_location_genoom      varchar(100) := '\\umcms005\gen_stg$\helix%';
  v_filter_location_dragon      varchar(100) := '\\umcms005\gen_stg$\dragon%';

  e_geen_lokatie EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_geen_lokatie, -20105);
  e_geen_file EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_geen_file,-20104);
  e_file_bestaat EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_file_bestaat,-20103);
  e_directory EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_directory,-20102);
  e_kopie EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_kopie,-20101);

  ------------------------------------------------------------------------------
  function get_btyp_origineel return kgc_bestand_types.btyp_id%type is
    v_btyp_id_org kgc_bestand_types.btyp_id%type := NULL;
  begin
    select btyp_id
    into   v_btyp_id_org
    from   kgc_bestand_types
    where  code = c_btyp_code_origineel
      and  vervallen = 'N';

    return v_btyp_id_org;

  exception when others then
    return null;

  end;


  ------------------------------------------------------------------------------
  function get_bcat_origineel return kgc_bestand_categorieen.bcat_id%type is
    v_bcat_id_org kgc_bestand_categorieen.bcat_id%type := NULL;
  begin
    select bcat_id
    into   v_bcat_id_org
    from   kgc_bestand_categorieen
    where  omschrijving = c_bcat_omschr_origineel
      and  vervallen = 'N';

    return v_bcat_id_org;

  exception when others then
    return null;

  end;


  ------------------------------------------------------------------------------
  --Controleer of voor dit bestandstype originelen opgeslagen moeten worden.
  function check_bewaar_origineel(p_btyp_id in kgc_bestand_types.btyp_id%type) return boolean is
    v_org_count number;
  begin

    select count(*)
    into   v_org_count
    from   kgc_bestand_types btyp
    where  btyp.btyp_id = p_btyp_id
      and  btyp.code <> c_btyp_code_origineel --Sla nooit een origineel op voor een origineel
      and  btyp.origineel_bewaren = 'J'
      and  btyp.vervallen = 'N';

    if (v_org_count>0) then
      return true;
    end if;

   return false;

  end;


  ------------------------------------------------------------------------------
  --Plak het ID nummer achter aan de bestandsnaam
  function voeg_id_toe( p_path in kgc_bestanden.bestand_specificatie%type
                      , p_id in number
                      ) return kgc_bestanden.bestand_specificatie%type is

    v_path kgc_bestanden.bestand_specificatie%type;
    v_pos  number;
    v_len  number;

  begin
    v_pos := instr(p_path, '.',  -1);
    v_len := length(p_path);

    if v_pos>0 and (v_len-v_pos)<5 then
      v_path := substr(p_path, 1, v_pos-1) || '-' || to_char(p_id)  || '-' || to_char(sysdate, 'YYYYMMDDHH24MISS') || '.' || substr(p_path, v_pos+1);
    else
      v_path := p_path;
    end if;

    return v_path;
  end;


  ------------------------------------------------------------------------------
  --Het kan voorkomen dat de aangemaakte bestandsnaam te lang wordt voor het Windows OS (260 tekens)
  --Deze functie probeert de bestandsnaam in te korten.
  --De parameter p_werk_kopie_bestand
  function maak_verkorte_bestandsnaam( p_werk_kopie_bestand in kgc_bestanden.bestand_specificatie%type
                                     , p_id in number
                                     ) return kgc_bestanden.bestand_specificatie%type is

    v_path      kgc_bestanden.bestand_specificatie%type;
    v_pos_slash number;
    v_pos_dot   number;
    v_len       number;

    v_verkorte_lengte number := 225;

  begin

    --Bereken de nieuwe locatie voor het originele bestand en voeg best_id toe.
    v_path := maak_origineel_path(p_werk_kopie_bestand);

    -- Zoek postitie van laatste \ en laatste .
    v_pos_slash := instr(v_path, '\',  -1);
    v_pos_dot   := instr(v_path, '.',  -1);
    v_len       := length(v_path);

    -- Als: Er is een laatste backslash gevonden
    --  en: Er is een laatste punt gevonden en deze staat na de laatste backslash
    --  en: De laatste backslash staat nog voor de verkorte lengte
    --  en: De punt staat minder dan 5 tekens van het einde
    -- Dan: Genereer de verkorte naam en geef deze terug.
    if v_pos_slash>0 and v_pos_dot>v_pos_slash  and v_pos_slash<(v_verkorte_lengte-1) and (v_len-v_pos_dot)<5 then
      v_path := substr(v_path, 1, least(v_verkorte_lengte, v_pos_dot-1)) || '-' || to_char(p_id)  || '-' || to_char(sysdate, 'YYYYMMDDHH24MISS') || '.' || substr(v_path, v_pos_dot+1);
    end if;

    return v_path;
  end;




  ------------------------------------------------------------------------------
  --Bereken het path voor het origineel uit het path van de werkkopie.
  --
  function maak_origineel_path(p_path in kgc_bestanden.bestand_specificatie%type) return kgc_bestanden.bestand_specificatie%type is

    v_path                kgc_bestanden.bestand_specificatie%type;
    v_path_voor_origineel kgc_bestanden.bestand_specificatie%type;

  begin

    v_path := translate(lower(p_path), '/' , '\');
    v_path_voor_origineel := NULL;

    --Bereken de nieuwe locatie voor het originele bestand.
    if v_path like v_filter_location_metab then
      --METAB locatie.  Kopieer VAN: \\umcms33\kgcn_dossiershelix$\def\
      --                       NAAR: \\umcms005\gen_stg$\helixDocs\def\
      v_path_voor_origineel := REGEXP_REPLACE(v_path, v_regexp_from_location_metab, v_regexp_to_location);

    elsif v_path like v_filter_location_genoom then
      --GENOOM locatie. Kopieer VAN: \\umcms005\gen_stg$\helix\prod\
      --                       NAAR: \\umcms005\gen_stg$\helixDocs\prod\
      v_path_voor_origineel := REGEXP_REPLACE(v_path, v_regexp_from_location_genoom, v_regexp_to_location);

    elsif v_path like v_filter_location_dragon then
      --DRAGON locatie. Kopieer VAN: \\umcms005\gen_stg$\dragon\prod\aanvragen
      --                       NAAR: \\umcms005\gen_stg$\helixDocs\prod\digidos\aanvragen
      v_path_voor_origineel := REGEXP_REPLACE(v_path, v_regexp_from_location_dragon, v_regexp_to_location_dragon);
    end if;

    --Als het path niet gewijzigt kan het niet correct zijn.
    if (p_path = v_path_voor_origineel) then
      v_path_voor_origineel := NULL;
    end if;

    --Beveiling tegen schrijven op productie paden.
    if (ora_database_name not like '%PRODILIX%') and (v_path_voor_origineel like '%\prod\%') then
      v_path_voor_origineel := NULL;
    end if;

    return v_path_voor_origineel;

  end;


  ------------------------------------------------------------------------------
  -- Markeer het de bestandsregistratie als een niet bestaande file.
  -- Return TRUE als het bestand al gemarkeerd is anders markeer het
  -- bestand en return FALSE
  --
  function check_and_mark_bestand(p_best_id in kgc_bestanden.best_id%type) return boolean is

    v_commentaar kgc_bestanden.bestand_specificatie%type;
    v_mark       varchar2(20) := '[FILE BESTAAT NIET]';

  begin

    select commentaar
    into   v_commentaar
    from   kgc_bestanden
    where  best_id = p_best_id;

    if (instr(v_commentaar, v_mark)>0) then
      return true;
    else
      v_commentaar := v_mark || ' ' || substr(v_commentaar, 1, 180);

      update kgc_bestanden
      set    commentaar = v_commentaar
      where  best_id = p_best_id;

      return false;
    end if;

  exception when others then
    return false;

  end;


  ------------------------------------------------------------------------------
  -- Controleer of de markering [FILE BESTAAT NIET] voorkomt en verwijder
  -- deze indien gevonden.
  --
  procedure check_bestand_and_remove_mark(p_best_id in kgc_bestanden.best_id%type) is

    v_commentaar kgc_bestanden.bestand_specificatie%type;
    v_mark       varchar2(20) := '[FILE BESTAAT NIET]';
    v_loc_mark   number;
    v_len_mark   number;

  begin

    select commentaar
    into   v_commentaar
    from   kgc_bestanden
    where  best_id = p_best_id;

    v_loc_mark := instr(v_commentaar, v_mark);

    if (v_loc_mark>0) then

      v_len_mark   := length(v_mark);
      v_commentaar := trim(substr(v_commentaar, 1, v_loc_mark-1) || substr(v_commentaar, v_loc_mark+v_len_mark));

      update kgc_bestanden
      set    commentaar = v_commentaar
      where  best_id = p_best_id;

    end if;

  end;



  ------------------------------------------------------------------------------
  -- Deze versie van bewaar_origineel is speciaal om aan te roepen vanuit de trigger.
  -- Alle info van een bestand die nodig is wordt meegegeven als parameter zodat
  -- er geen extra query nodig is.
  procedure bewaar_origineel( p_bestand_specificatie in kgc_bestanden.bestand_specificatie%type
                            , p_best_id              in kgc_bestanden.best_id%type
                            , p_commentaar           in kgc_bestanden.commentaar%type
                            )

  is

    v_werk_kopie_bestand     kgc_bestanden.bestand_specificatie%type;
    v_werk_kopie_locatie     kgc_bestanden.locatie%type;
    v_origineel_bestand      kgc_bestanden.bestand_specificatie%type;      --Complete bestandslocatie origineel
    v_originele_locatie      kgc_bestanden.locatie%type;                   --Locatie origineel
    v_originele_bestandsnaam kgc_bestanden.bestandsnaam%type;

    v_btyp_id_org            kgc_bestand_types.btyp_id%type := NULL;       --Type van het origineel.
    v_bcat_id_org            kgc_bestand_categorieen.bcat_id%type := NULL; --Categorie van het origineel.

    v_dummy                  boolean;

  begin
    v_btyp_id_org := get_btyp_origineel;
    v_bcat_id_org := get_bcat_origineel;

    v_werk_kopie_bestand := translate(lower(p_bestand_specificatie), '/' , '\');

    --Bereken de nieuwe locatie voor het originele bestand en voeg best_id toe.
    v_origineel_bestand := maak_origineel_path(v_werk_kopie_bestand);
    v_origineel_bestand := voeg_id_toe(v_origineel_bestand, p_best_id);

    --Bereken het nieuwe path naam (locatie) en de bestandsnaam.
    v_originele_locatie      := beh_file_00.get_dir_path(v_origineel_bestand);
    v_originele_bestandsnaam := beh_file_00.get_filename(v_origineel_bestand);

    --De locatie van het origineel is succesvol berekend.
    if (v_origineel_bestand is null) then
      -- Geen bestand op METAB of GENOOM locatie of locatie v_origineel_bestand
      -- is gelijk aan werk kopie, dat mag niet!
      -- Dit laatste kan gebeuren als de REGEXP_REPLACE de locatie niet kan matchen.
      raise_application_error(-20105, 'Originele document locatie kan niet bepaald worden voor werkkopie [' || v_werk_kopie_bestand || '] en berekend origineel [' || v_origineel_bestand || ']');
    end if;

    if not(beh_file_00.file_exists(v_werk_kopie_bestand)) then
      --Werkkopie bestand bestaat niet! Er is (blijkbaar) wel een registratie, maar het bestand is niet aanwezig (of mag niet gelezen worden)

      --Markeer het bestand en geef foutmelding.
      v_dummy := check_and_mark_bestand(p_best_id);
      raise_application_error(-20104, 'Werk kopie bestand [' || v_werk_kopie_bestand || '] bestaat niet!');
    else
      --Werkkopie bestand bestaat! controleer of een eventueele markering verwijderd moet worden.
      check_bestand_and_remove_mark(p_best_id);
    end if;

    if beh_file_00.file_exists(v_origineel_bestand) then
      --Origineel bestand bestaat al en mag niet overscheven worden!
      raise_application_error(-20103, 'Origineel bestand [' || v_origineel_bestand || '] bestaat al!');
    end if;

    if not(beh_file_00.create_file_directory(v_originele_locatie)) then
      -- Directory kan niet gemaakt worden.
      raise_application_error(-20102, 'File directory [' || v_originele_locatie || '] kan niet gemaakt worden');
    end if;

    if not(beh_file_00.file_copy(v_werk_kopie_bestand, v_origineel_bestand)) then
      -- Kopie actie mislukt. Mischien is de bestandsnaam te lang, verkort de naam en probeer het nog eens.
      v_origineel_bestand := maak_verkorte_bestandsnaam(v_werk_kopie_bestand, p_best_id);

      if beh_file_00.file_exists(v_origineel_bestand) then
        --Het verkorte originele bestand bestaat al en mag niet overscheven worden!
        raise_application_error(-20106, 'Origineel bestand na verkorting [' || v_origineel_bestand || '] bestaat al!');
      end if;

      if not(beh_file_00.file_copy(v_werk_kopie_bestand, v_origineel_bestand)) then
        raise_application_error(-20101, 'Origineel bestand [' || v_origineel_bestand || '] kan niet gekopieerd worden vanaf [' || v_werk_kopie_bestand || '].');
      end if;
    end if;

    --Bestand is gekopieerd, maak nieuwe registratie aan in kgc_bestanden tabel.
    insert into kgc_bestanden
    ( entiteit_code, entiteit_pk, btyp_id, bestand_specificatie
    , volgorde, commentaar, locatie, bestandsnaam, rela_id, bcat_id
    )
    values
    ( 'BEST', p_best_id, v_btyp_id_org, v_origineel_bestand
    , null, p_commentaar, v_originele_locatie, v_originele_bestandsnaam, null, v_bcat_id_org
    );

  end;


  ------------------------------------------------------------------------------
  -- Verwerk alle bestanden die aangemaakt zijn na de opgegeven datum en
  -- waarvoor originelen aangemaakt moeten terwijl deze er niet zijn
  --
  -- Als parameter p_skip_niet_bestaand = J dan worden bestanden die eerder zijn
  -- gemerkt met [FILE BESTAAT NIET] overgeslagen (default).
  --
  procedure verwerk_originelen( p_from_date in date
                              , p_html_message in out clob
                              , p_aantal in out number
                              , p_errors in out number
                              , p_skip_niet_bestaand in varchar2
                              ) is

    --Voor welke bestanden zou een origineel opgeslagen moeten worden.
    cursor c_org_best(b_from_date in date, b_skip_niet_bestaand in varchar2) is
    with best_originelen as
    ( select best.best_id
           , best.entiteit_pk as best_werk_id
      from   kgc_bestanden best
        join kgc_bestand_types btyp on btyp.btyp_id = best.btyp_id
      where btyp.code = c_btyp_code_origineel
        and best.entiteit_code = 'BEST'
    )
    select btyp.code
         , best.best_id
         , best.bestand_specificatie
         , best.locatie
         , best.bestandsnaam
         , best.commentaar
         , best_org.best_id as best_org_id
    from   kgc_bestand_types btyp
      join kgc_bestanden best on best.btyp_id = btyp.btyp_id
      left join best_originelen best_org on best_org.best_werk_id = best.best_id
    where  btyp.origineel_bewaren = 'J'
      and  btyp.vervallen = 'N'
      and  best_org.best_org.best_id is null
      and  ((b_skip_niet_bestaand = 'N') or ((b_skip_niet_bestaand = 'J') and (nvl(best.commentaar, 'LEEG') not like '[FILE BESTAAT NIET]%')))
      and  best.creation_date >= b_from_date;

  begin

    p_aantal := 0;

    for r_org_best in c_org_best(p_from_date, p_skip_niet_bestaand) loop

      if (dbms_lob.getlength(p_html_message) > 0) then
        p_html_message := p_html_message || '<li>Maak origineel bestand aan voor best_id [' || to_char(r_org_best.best_id)
                                         || '], bestand_specificatie [' || r_org_best.bestand_specificatie
                                         || ']</li>'
                                         || v_crlf;
      end if;

      begin

        bewaar_origineel
        ( p_bestand_specificatie => r_org_best.bestand_specificatie
        , p_best_id              => r_org_best.best_id
        , p_commentaar           => r_org_best.commentaar
        );

      exception when e_geen_lokatie
                  or e_geen_file
                  or e_file_bestaat
                  or e_directory
                  or e_kopie THEN

         p_html_message := p_html_message || '</br><font color="red">Fout bij bewaren origineel: ' || sqlerrm || '</font></br>' || v_crlf;
         p_errors := p_errors +1;

      end;

      p_aantal := p_aantal +1;

    end loop;

  end;


  ------------------------------------------------------------------------------
  -- Verwerk alle bestanden die aangemaakt zijn na laatste draaidatum en
  -- waarvoor originelen aangemaakt moeten terwijl deze er niet zijn
  --
  -- Als parameter p_consolidatie_run = J dan wordt er een consolidatie run
  -- van deze batch uitgevoerd. Ongeacht de datum van de laatste verwerking,
  -- wordt er een vast tijd (maand) terug gekeken en wordt en worden best records
  -- die verwijzen naar niet bestannde bestanden nog eens gecontroleerd
  -- (p_skip_niet_bestaand=N)
  --
  procedure verwerk_originelen( p_consolidatie_run in varchar2 := 'N' ) is

    v_procedure_naam  varchar2(45) := 'beh_digidos_originelen.verwerk_originelen';
    v_html_message    clob;
    v_email_cc        varchar2(100) := 'rene.brand@radboudumc.nl';
    v_email_onderwerp varchar2(100);
    v_email_aan       varchar2(100);
    v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                        , p_mede_id        => NULL
                                                                        )
                                          , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                          );

    v_last_verwerk_date kgc_systeem_parameters.standaard_waarde%type;
    v_info_email        kgc_systeem_parameters.standaard_waarde%type;
    v_now               date;
    v_from_date         date;

    v_last_verwerk_sypa kgc_systeem_parameters.code%type := 'DD_ORG_LAATSTE_CHECK_DATUM';
    v_info_mail_sypa    kgc_systeem_parameters.code%type := 'DD_ORG_INFO_MAIL';
    v_sypa_format       varchar2(25)                     := 'DD-MM-YYYY HH24:MI:SS';

    v_aantal            number := 0;
    v_errors            number := 0;

  begin

    v_now := sysdate;
    v_email_aan := beh_interfaces.beh_get_user_email;

    v_email_onderwerp := 'Helix overzicht aanmaken originelen voor digitaal dossier';
    v_html_message    := 'Dit is een automatisch gegenereerd rapport over het aanmaken van missende originele documenten in het digitaal dossier.</br></br>' || v_crlf;

    v_html_message    := v_html_message || 'Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf;
    v_html_message    := v_html_message || 'Uitgevoerd door: ' || user || '</br>' || v_crlf;
    v_html_message    := v_html_message || 'Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>' || v_crlf;
    v_html_message    := v_html_message || 'Draai datum: ' || to_char(v_now, v_sypa_format) || '</br>' || v_crlf;

    --Haal de datum/tijd op waneer de laatste controle is uitgevoerd.
    v_last_verwerk_date := kgc_sypa_00.systeem_waarde(v_last_verwerk_sypa);

    --Haal het info email adres op, of null als de systeem parameter niet bestaat.
    begin
      v_info_email := kgc_sypa_00.systeem_waarde(v_info_mail_sypa);
    exception when others then
      v_info_email := null;
    end;

    if (p_consolidatie_run = 'N') then
      --+++ Voer een normale (geen consolidatie) run uit +++--

      v_html_message    := v_html_message || 'Vorige draai datum: ' || v_last_verwerk_date || '</br></br>' || v_crlf;
      v_html_message    := v_html_message || '<ul>';

      if (v_last_verwerk_date is not null) then

        -- Verwerk de originelen vanaf tijdstip laatste verwerking minus 1/2 uur.
        -- De consolidatie run gaat hierna nog regelmatig proberen de bestanden aan te
        -- maken indien dit nog steeds niet gelukt is.
        v_from_date := to_date(v_last_verwerk_date, v_sypa_format) - (1/48);

        verwerk_originelen(v_from_date, v_html_message, v_aantal, v_errors, p_skip_niet_bestaand => 'J');

        --Update de datum/tijd waneer de laatste controle is uitgevoerd is me
        update kgc_systeem_parameters
        set    standaard_waarde = to_char(v_now, v_sypa_format)
        where  code = v_last_verwerk_sypa;

      else
        v_html_message := v_html_message || 'FOUT:<font color="red">' || 'Laatste verwerkdatum is NULL</font></br></br>' || v_crlf;

      end if;

    else
      --+++ Voer de consolidatie run uit +++--

      -- Verwerk de originelen vanaf vorige maand onafhankelijk van tijdstip laatste verwerking.
      v_from_date := add_months(sysdate, -1);

      v_html_message    := v_html_message || '</br><b>Consolidatie</b> run vanaf datum: ' || v_from_date || '</br></br>' || v_crlf;
      v_html_message    := v_html_message || 'De consolidatie run probeert alle bestanden die eerder een fout opleverde nog eens te verwerken. '
                                          || 'Het is te verwachten dat hier nog steeds fouten tussen staan.</br></br>' || v_crlf;
      v_html_message    := v_html_message || '<ul>';

      verwerk_originelen(v_from_date, v_html_message, v_aantal, v_errors, p_skip_niet_bestaand => 'N');

      --Update de datum/tijd waneer de laatste controle is uitgevoerd is me
      update kgc_systeem_parameters
      set    standaard_waarde = to_char(v_now, v_sypa_format)
      where  code = v_last_verwerk_sypa;

    end if;

    v_html_message := v_html_message || '</ul>';
    v_html_message := v_html_message || '</br>Tijdsduur: ' || to_char(round((sysdate - v_now)*(60*60*24), 2)) || ' sec.</br>' || v_crlf;
    v_html_message := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

    --Verstuur mail alleen als er documenten verwerkt zijn.
    if v_aantal>0 then
      --Verstuur error mail als er fouten zijn.
      if v_errors>0 then
        v_email_onderwerp := v_email_onderwerp || ' (FOUT)';

        -- Error mail in geval van kleine fout naar STG mailbox.
        send_mail_html( p_from    => v_email_afzender
                      , p_to      => v_email_aan
                      , p_cc      => v_email_cc
                      , p_subject => v_email_onderwerp
                      , p_html    => v_html_message
                      );

      end if;
    end if;

    -- Als info_email sypa gedefinieerd is, altijd mail ter info.
    if v_info_email is not null then

      send_mail_html( p_from    => v_email_afzender
                    , p_to      => v_info_email
                    , p_subject => v_email_onderwerp
                    , p_html    => v_html_message
                    );

    end if;



  exception when others then
    -- Exception mail in geval van grote fout naar STG mailbox.
    v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
    v_html_message    := v_html_message || '</br><font color="red">FOUT:' || sqlerrm || '</br>'
                                                                          || dbms_utility.format_error_backtrace || '</br>'
                                                                          || dbms_utility.format_call_stack
                                                                          ||'</font></br></br>' || v_crlf;
    v_html_message    := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan
                  , p_cc      => v_email_cc
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );

    raise;

  end;


  ------------------------------------------------------------------------------
  procedure controleer_originelen is

    v_procedure_naam  varchar2(45) := 'beh_digidos_originelen.controleer_originelen';
    v_html_message    clob;
    v_email_cc        varchar2(100) := 'rene.brand@radboudumc.nl';
    v_email_onderwerp varchar2(100);
    v_email_aan       varchar2(100);
    v_email_afzender  varchar2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM'
                                                                        , p_mede_id        => NULL
                                                                        ) , 'HelpdeskHelixCUKZ@cukz.umcn.nl' );

    v_aantal            number;

  begin
    v_email_onderwerp := 'Controleer het aanmaken van originelen voor digitaal dossier';
    v_email_aan       := beh_interfaces.beh_get_user_email;

    select count(*)
    into   v_aantal
    from   kgc_bestanden best
      join kgc_bestand_types btyp on btyp.btyp_id = best.btyp_id
    where  btyp.code = 'ORIGINEEL'
      and  best.entiteit_code = 'BEST'
      and  best.creation_date > (sysdate - 1);

    if (v_aantal > 0) then
      v_email_onderwerp := 'Helix originelen voor digitaal dossier correct aangemaakt';
      v_html_message    := 'Er zijn in de afgelopen 24 uur ' || to_char(v_aantal) || ' originelen aangemaakt.</br>' || v_crlf;
      v_html_message    := v_html_message || 'Omdat de originelen aangemaakt worden is de controle OK en er is geen verdere actie noodzakelijk.</br></br>' || v_crlf;
    else
      v_email_onderwerp := 'WAARSCHUWING: Er zijn de afgelopen 24 uur GEEN originelen aangemaakt';
      v_html_message    := 'Er zijn in de afgelopen 24 uur GEEN originelen aangemaakt.</br>' || v_crlf;
      v_html_message    := v_html_message || 'Controleer of de job BEH_DIGIDOS_MAAK_ORIGINELEN nog draait op de database.</br></br>' || v_crlf;
    end if;

    v_html_message := v_html_message || 'Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf;
    v_html_message := v_html_message || 'Uitgevoerd door: ' || user || '</br>' || v_crlf;
    v_html_message := v_html_message || 'Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>' || v_crlf;

    v_html_message := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );


  exception when others then
    -- Exception mail in geval van grote fout naar STG mailbox.
    v_email_onderwerp := v_email_onderwerp || ' (FOUT)';
    v_html_message    := v_html_message || '</br><font color="red">FOUT:' || sqlerrm || '</br>'
                                                                          || dbms_utility.format_error_backtrace || '</br>'
                                                                          || dbms_utility.format_call_stack
                                                                          ||'</font></br></br>' || v_crlf;

    v_html_message    := v_html_message || '</br></br>' || v_crlf || 'Deze e-mail is automatisch verzonden door Helix procedure: ' || v_procedure_naam;

    send_mail_html( p_from    => v_email_afzender
                  , p_to      => v_email_aan
                  , p_cc      => v_email_cc
                  , p_subject => v_email_onderwerp
                  , p_html    => v_html_message
                  );
    raise;

  end;




END beh_digidos_originelen;
/

/
QUIT
