create or replace PACKAGE BEH_ARCHIEF2DA IS

  PROCEDURE verwerk_bestanden;

END BEH_ARCHIEF2DA;
/
create or replace PACKAGE BODY BEH_ARCHIEF2DA IS

  v_crlf varchar2(2) := chr(13)||chr(10);
  v_sep  varchar2(1) := chr(9);


  --############################################################################
  --# Scan de directory uit sypa BEH_ARCHIEF2DA_PADEN en verwerk de gevonden
  --# bestanden. Na het verwerken wordt het bestand verplaatst naar directory 
  --# BTYP-ARCHIEF2DA / standaard-pad als het bestand correct verwerkt is en 
  --# de fout-map uit de sypa als er een fout is opgetreden.
  --############################################################################
  PROCEDURE verwerk_bestanden IS
    v_sypa_mail       VARCHAR2(100) := 'BEH_ARCHIEF2DA_MAIL';
    CURSOR c_btyp(p_code VARCHAR2) IS
      SELECT NVL (mobe.standaard_pad, btyp.standaard_pad) standaard_pad
      ,      btyp.btyp_id
      ,      btyp.enti_code
      FROM   kgc_bestand_types btyp
      ,      kgc_model_bestanden mobe
      WHERE  btyp.code = 'ARCHIEF2DA'
      AND    btyp.btyp_id = mobe.btyp_id(+)
      ;
    r_btyp c_btyp%ROWTYPE;
    CURSOR c_mede(p_waarde VARCHAR2) IS
      SELECT mede.email
      FROM   kgc_systeem_parameters sypa
      ,      kgc_systeem_par_waarden spwa
      ,      kgc_medewerkers mede
      WHERE  sypa.code = v_sypa_mail
      AND    sypa.sypa_id = spwa.sypa_id
      AND    spwa.mede_id = mede.mede_id
      AND    mede.email IS NOT NULL
      AND    INSTR(spwa.waarde, p_waarde) > 0
      ;
    v_procedure_naam     VARCHAR2(100) := 'beh_archief2da.verwerk_bestanden'; 
    v_html_message       CLOB;
    v_email_afzender     VARCHAR2(100) := nvl( kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_STGEN_EMAIL_FROM' 
                                                                        , p_mede_id        => NULL
                                                                        )
                                          , 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                                          );
    v_email_onderwerp    VARCHAR2(100);
    v_tab_filedesc       beh_auto_uits.t_tab_filedesc := beh_auto_uits.t_tab_filedesc();
    v_tab_filedesc2      beh_auto_uits.t_tab_filedesc := beh_auto_uits.t_tab_filedesc();
    v_ret                VARCHAR2 (32767);
    v_waarde             kgc_systeem_par_waarden.waarde%TYPE;
    v_pos                NUMBER;
    v_map_scan           VARCHAR2(1000);
    v_map_scan_dir_obj   VARCHAR2(20);
    v_map_fout           VARCHAR2(1000);
    v_map_fout_dir_obj   VARCHAR2(20);
    v_ok                 BOOLEAN;
    PROCEDURE add_tekst(p_tekst VARCHAR2) IS
    BEGIN
      dbms_lob.writeappend(v_html_message, length(p_tekst), p_tekst);
    END add_tekst;
      
    FUNCTION getFamiUitBestand(p_bestand VARCHAR2, x_errmsg IN OUT VARCHAR2) RETURN VARCHAR2 IS
      v_pos1         NUMBER;
      v_pos2         NUMBER;
      v_familienummer VARCHAR2(100);
    BEGIN
      v_pos1 := INSTR(p_bestand, '_');
      v_pos2 := INSTR(p_bestand, '_', v_pos1 + 1);
      IF v_pos1 = 0 OR v_pos2 = 0 THEN
        x_errmsg := 'ERROR: in de naam van bestand ' || p_bestand || ' kon het familienummer niet gevonden worden (tussen 2 "_")!';
        RETURN NULL;
      END IF;
      v_familienummer := SUBSTR(p_bestand, v_pos1 + 1, v_pos2 - v_pos1 - 1);
      IF v_familienummer IS NULL THEN
        x_errmsg := 'ERROR: in de naam van bestand ' || p_bestand || ' kon het familienummer niet gevonden worden!';
        RETURN NULL;
      END IF;
      RETURN v_familienummer;
    END getFamiUitBestand;
    FUNCTION getFamiActueel(p_familienummer VARCHAR2, x_fami_id IN OUT NUMBER, x_okmsg IN OUT VARCHAR2, x_errmsg IN OUT VARCHAR2) RETURN VARCHAR2 IS
      -- x_okmsg: hierin staat de zoektocht igv gekoppelde families, tevens nodig voor detectie oneindige loop
      CURSOR c_fami(p_familienummer VARCHAR2) IS
        SELECT fami.*
        ,      (select count(*)
                from   kgc_familie_leden fale
                where  fale.fami_id = fami.fami_id
               ) count_fale
        FROM   kgc_families fami
        WHERE  fami.familienummer = p_familienummer
        ;
      r_fami c_fami%ROWTYPE;
      v_familienummer_nw kgc_families.naam%TYPE;
    BEGIN
      OPEN c_fami(p_familienummer);
      FETCH c_fami INTO r_fami;
      IF c_fami%NOTFOUND THEN
        CLOSE c_fami;
        x_errmsg := x_okmsg || ' > ' || 'Familienummer ' || p_familienummer || ' bestaat niet!';
        RETURN NULL;
      END IF;
      CLOSE c_fami;
      IF r_fami.count_fale = 0 THEN
        x_okmsg := x_okmsg || ' > ' || 'Familienummer ' || p_familienummer || ' bevat geen leden';
        v_familienummer_nw := RTRIM(r_fami.naam, ' .,*)');
        v_pos := INSTR(v_familienummer_nw, ' ', -1);
        IF v_pos = 0 THEN
          x_errmsg := x_okmsg || ' > ' || 'Uit familienaam "' || p_familienummer || '" kon geen nieuw familienummer bepaald worden!!';
          RETURN NULL;
        ELSE
          v_familienummer_nw := SUBSTR(v_familienummer_nw, v_pos + 1);
          IF INSTR(x_okmsg, v_familienummer_nw) > 0 THEN -- loop-check
            x_errmsg := x_okmsg || ' > ' || 'ERROR: Gekoppeld aan ' || v_familienummer_nw || '... oneindige loop van koppelingen!!';
            RETURN NULL;
          ELSE
            x_okmsg := x_okmsg || ' > ' || 'Gekoppeld aan ' || v_familienummer_nw;
            RETURN getFamiActueel(v_familienummer_nw, x_fami_id, x_okmsg, x_errmsg);
          END IF;
        END IF;
      END IF;
      x_fami_id := r_fami.fami_id;
      RETURN r_fami.familienummer;
    END getFamiActueel;
    --############################################################################
    --# verwerk_doosfile: lees file en registreer inhoud in beh_archief2DA_dossiers
    --# indien fout: naar foutmap + verslagfile
    --############################################################################
    PROCEDURE verwerk_doosfile(p_bestand VARCHAR2) IS
      CURSOR c_b_ad 
      ( p_doos VARCHAR2
      , p_dossier VARCHAR2
      ) IS
        SELECT *
        FROM   beh_archief2DA_dossiers b_ad
        WHERE  b_ad.doos = p_doos
        AND    b_ad.dossier = p_dossier
        ;
      r_b_ad c_b_ad%ROWTYPE;

      v_pos1         NUMBER;
      v_pos2         NUMBER;
      v_errmsg       VARCHAR2(32767);
      v_okmsg        VARCHAR2(32767);
      e_normale_fout EXCEPTION;
      v_familienummer VARCHAR2(100);
      v_familienummer_act VARCHAR2(100);
      v_jaar         VARCHAR2(100);
      v_fami_id      NUMBER;
      v_bestand_z_ext VARCHAR2(1000) := p_bestand;
      v_bestand_ext  VARCHAR2(1000);
      v_bestand_fout VARCHAR2(1000);
      v_bestand_fout_verslag VARCHAR2(1000);

      v_scanfile_handle utl_file.file_type;
      v_oradir_created  boolean := FALSE;
      v_regel           varchar2(32000);
      v_count_regels    number := 0;
      v_count_err       number := 0;
      v_count_ins       number := 0;
      v_count_ok        number := 0;
      v_str_array       beh_auto_uits.t_str_array;
      v_map_doel        VARCHAR2(1000) := RTRIM(REPLACE(LOWER(r_btyp.standaard_pad), '<creationyear>', ''), '\');
      v_b_ad_id         number;
      v_errmsg_det      CLOB;
      v_datum_intake    DATE;
      v_datum_verwerkt  DATE;
      v_datum_controle  DATE;
      v_debug           VARCHAR2(100);
      PROCEDURE add_tekst_det(p_tekst VARCHAR2) IS
      BEGIN
        dbms_lob.writeappend(v_errmsg_det, length(p_tekst), p_tekst);
      END add_tekst_det;
    BEGIN
      v_debug := 'A';
      dbms_lob.createtemporary(v_errmsg_det, TRUE);
      v_pos1 := INSTR(p_bestand, '.', -1);
      IF v_pos1 > 0 THEN
        v_bestand_z_ext := SUBSTR(p_bestand, 1, v_pos1 - 1);
        v_bestand_ext := SUBSTR(p_bestand, v_pos1 + 1);
      END IF;
      v_bestand_fout := v_bestand_z_ext || '_' || TO_CHAR(SYSDATE, 'yyyymmdd_hh24miss') || '.' || v_bestand_ext;
      v_bestand_fout_verslag := v_bestand_z_ext || '_' || TO_CHAR(SYSDATE, 'yyyymmdd_hh24miss') || '.txt';
      -- open file
      begin
        v_scanfile_handle := utl_file.fopen(v_map_scan_dir_obj, p_bestand, 'R');
      exception when others then
        raise_application_error(-20000, 'Bestand ' || p_bestand || ' in map ' || v_map_scan || ' kon niet geopend worden: ' || SQLERRM);
      end;
  
      if utl_file.is_open(v_scanfile_handle) then
        loop
          begin
            utl_file.get_line(v_scanfile_handle, v_regel, 32000);
            v_count_regels := v_count_regels + 1;
            v_debug := '#' || v_count_regels;
            v_str_array := beh_auto_uits.csv_to_array(p_csv_line=> v_regel, p_separator => ';');
 
            if v_count_regels = 1 then -- heading, negeren
              null;
            elsif v_str_array.COUNT < 3 then -- voeg toe aan foutverslag
              v_count_err := v_count_err + 1;
              add_tekst_det('ERROR: op regel ' || v_count_regels || ' zijn slechts ' || v_str_array.COUNT || ' kolommen gevuld (minimaal 3 nodig)!' || v_crlf);
            else 
              v_datum_intake := NULL;
              v_datum_verwerkt := NULL;
              v_datum_controle := NULL;
              v_errmsg := NULL;
              -- formaat: Archief2dA_15-08289_28032017.pdf OF 15-08289
              IF LENGTH(v_str_array(2)) <= 20 AND INSTR(v_str_array(2), '_') = 0 THEN
                v_familienummer := v_str_array(2);
              ELSE
                v_familienummer := getFamiUitBestand(p_bestand => v_str_array(2), x_errmsg => v_errmsg);
              END IF;
              IF v_errmsg IS NOT NULL THEN
                v_count_err := v_count_err + 1;
                add_tekst_det('ERROR: op regel ' || v_count_regels || ' werd dossier ' || v_str_array(2)
                  || ' gevonden, maar hier kon geen familie bij bepaald worden: ' || v_errmsg || v_crlf);
              ELSE
                v_okmsg := NULL;
                v_fami_id := NULL;
                v_familienummer_act := getFamiActueel(p_familienummer => v_familienummer, x_fami_id => v_fami_id, x_okmsg => v_okmsg, x_errmsg => v_errmsg);
                IF v_familienummer_act IS NULL THEN
                  v_count_err := v_count_err + 1;
                  add_tekst_det('ERROR: op regel ' || v_count_regels || ' werd familienummer ' || v_familienummer
                    || ' gevonden, maar hier werd geen actuele familie bij gevonden (evt via gekoppelde families): ' || v_errmsg || v_crlf);
                END IF;
                v_pos1 := INSTR(v_str_array(3), ' ');
                v_debug := v_debug || 'H=' || v_pos1;
                v_datum_intake := TO_DATE(SUBSTR(v_str_array(3), 1, v_pos1 - 1), 'dd-mm-yyyy');
                -- bijwerken in tabel
                OPEN c_b_ad
                ( p_doos         => v_str_array(1)
                , p_dossier      => v_familienummer
                );
                IF v_str_array.count > 3 THEN
                  IF v_str_array(4) IS NOT NULL THEN
                    v_pos1 := INSTR(v_str_array(4), ' ');
                    v_debug := v_debug || 'I=' || v_pos1;
                    v_datum_verwerkt := TO_DATE(SUBSTR(v_str_array(4), 1, v_pos1 - 1), 'dd-mm-yyyy');
                  END IF;
                END IF;
                IF v_str_array.count > 4 THEN
                  IF v_str_array(5) IS NOT NULL THEN
                    v_debug := v_debug || 'J=' || v_pos1;
                    v_pos1 := INSTR(v_str_array(5), ' ');
                    v_datum_controle := TO_DATE(SUBSTR(v_str_array(5), 1, v_pos1 - 1), 'dd-mm-yyyy');
                  END IF;
                END IF;
                FETCH c_b_ad INTO r_b_ad;
                IF c_b_ad%NOTFOUND THEN -- toevoegen
                  SELECT NVL(MAX(b_ad_id), 0) + 1
                  INTO   v_b_ad_id
                  FROM   beh_archief2DA_dossiers;
                  INSERT INTO beh_archief2DA_dossiers
                  ( b_ad_id
                  , doos
                  , dossier
                  , datum_intake
                  , datum_verwerkt
                  , datum_controle
                  , familienummer
                  ) VALUES
                  ( v_b_ad_id
                  , v_str_array(1)
                  , v_familienummer
                  , v_datum_intake
                  , v_datum_verwerkt
                  , v_datum_controle
                  , v_familienummer_act
                  );
                  v_count_ins := v_count_ins + 1;
                ELSE
                  IF NVL(r_b_ad.familienummer, CHR(2)) <> NVL(v_familienummer_act, CHR(2)) OR 
                    v_datum_verwerkt IS NOT NULL OR
                    v_datum_controle IS NOT NULL
                  THEN -- bijwerken!
                    UPDATE beh_archief2DA_dossiers
                    SET    familienummer = NVL(v_familienummer_act, familienummer)
                    ,      datum_verwerkt = NVL(v_datum_verwerkt, datum_verwerkt)
                    ,      datum_controle = NVL(v_datum_controle, datum_controle)
                    WHERE  b_ad_id = v_b_ad_id;
                  END IF;
                  v_count_ok := v_count_ok + 1;
                END IF;
                CLOSE c_b_ad;
              END IF;

            end if;
          exception 
            when no_data_found then
              exit;
            when others then
--  dbms_output.put_line('1='|| v_str_array(1) || '; 2=' || v_str_array(2) || '; 3=' || v_str_array(3) || '; 4=' || v_str_array(4) || '; 5=' || v_str_array(5));
--  dbms_output.put_line('debug='|| v_debug);
--  dbms_output.put_line('a3='|| SUBSTR(v_str_array(3), 1, v_pos - 1));
              raise;
          end;
        end loop;
--        add_tekst('<li>Aantal gevonden regels: ' || v_count_regels || '</li></br></br>' || v_crlf);
      else
        add_tekst('<br><br>FOUT: <font color="red">' || ' Bestand ''' || p_bestand || ''' kan niet geopend worden in map ''' || v_map_scan || '''.</font></br></br>' || v_crlf);
      end if;
  
      if utl_file.is_open(v_scanfile_handle) then
        utl_file.fclose(v_scanfile_handle);
      end if;
  
      COMMIT;
      IF v_count_err > 0 THEN
        RAISE e_normale_fout;
      END IF;

      -- verplaats bestand
      BEGIN
        v_ok := beh_file_00.file_move
        ( p_dir_file_from => rtrim(v_map_scan, '\') || '\' || p_bestand
        , p_dir_file_to   => rtrim(v_map_doel, '\') || '\' || p_bestand
        );
      EXCEPTION
        WHEN OTHERS THEN
          v_errmsg := 'ERROR: verplaatsen van bestand ' || p_bestand || ' van ' || v_map_scan || ' naar ' || v_map_doel || ' ging niet goed: ' || SQLERRM;
          RAISE e_normale_fout;
      END;
      -- alles OK:
      add_tekst('<li>CSV ' || p_bestand || ' is correct verwerkt (#ins=' || v_count_ins || '; #ok=' || v_count_ok || '); verplaatst naar de doel-directory</li>' || v_crlf);
      dbms_lob.freetemporary(v_errmsg_det);
      COMMIT;
    EXCEPTION
      WHEN e_normale_fout THEN -- schrijf bestand en verslag
        ROLLBACK;
        UTL_FILE.FCLOSE_ALL;
        BEGIN
          v_ok := beh_file_00.file_move
          ( p_dir_file_from => rtrim(v_map_scan, '\') || '\' || p_bestand
          , p_dir_file_to   => rtrim(v_map_fout, '\') || '\' || v_bestand_fout
          );
        EXCEPTION
          WHEN OTHERS THEN -- afbreken
            raise_application_error(-20000, 'ERROR1: ' || v_errmsg || v_crlf || 'ERROR2: verplaatsen van bestand ' || p_bestand || ' van ' || v_map_scan || ' naar ' || v_map_fout || ' ging niet goed: ' || SQLERRM);
        END;
        DECLARE
          v_foutfile_handle       utl_file.file_type;
        BEGIN
          -- maak foutbestand v_bestand_fout_verslag
          v_foutfile_handle := utl_file.fopen(v_map_fout_dir_obj, v_bestand_fout_verslag, 'W');
          FOR i IN 1 .. (round(DBMS_LOB.GETLENGTH(v_errmsg_det) / 1024) + 1) LOOP
            utl_file.put(v_foutfile_handle, DBMS_LOB.SUBSTR(v_errmsg_det, 1024, ((i - 1) * 1024) + 1));
            IF MOD(i, 16) = 0 THEN
              UTL_FILE.FFLUSH(v_foutfile_handle);
            END IF;
          END LOOP;
          UTL_FILE.FFLUSH(v_foutfile_handle);
          utl_file.fclose(v_foutfile_handle);
        EXCEPTION
          WHEN OTHERS THEN -- afbreken
            raise_application_error(-20000, 'ERROR1: ' || v_errmsg || v_crlf || 'ERROR2: aanmaken van bestand ' || v_bestand_fout_verslag || ' in map ' || v_map_fout || ' ging niet goed: ' || SQLERRM);
        END;
        add_tekst('<li>CSV ' || p_bestand || ' is NIET correct verwerkt (#ins=' || v_count_ins || '; #ok=' || v_count_ok || '; #err=' || v_count_err || ', zie ' || v_bestand_fout_verslag || '); verplaatst naar fout-directory</li>' || v_crlf);
        dbms_lob.freetemporary(v_errmsg_det);
      WHEN OTHERS THEN -- afbreken
        ROLLBACK;
        add_tekst('<li>CSV ' || p_bestand || ' is NIET correct verwerkt; error=' || SQLERRM || '</li>' || v_crlf);
        dbms_lob.freetemporary(v_errmsg_det);
        RAISE; -- want: niet voorzien!
    END verwerk_doosfile;
    --############################################################################
    --# verwerk_dossier: bepaal familienummer, verplaats en koppel bestand.
    --# indien fout: naar foutmap + verslagfile
    --############################################################################
    PROCEDURE verwerk_dossier(p_map VARCHAR2, p_bestand VARCHAR2) IS
      CURSOR c_b_ad( p_dossier VARCHAR2) IS
        SELECT *
        FROM   beh_archief2DA_dossiers
        WHERE  dossier = p_dossier
        ;
      v_pos1         NUMBER;
      v_pos2         NUMBER;
      v_errmsg       VARCHAR2(32767);
      v_okmsg        VARCHAR2(32767);
      e_normale_fout EXCEPTION;
      v_familienummer VARCHAR2(100);
      v_familienummer_act VARCHAR2(100);
      v_jaar         VARCHAR2(100);
      v_map_doel     VARCHAR2(1000);
      v_fami_id      NUMBER;
      v_bestand_z_ext VARCHAR2(1000) := p_bestand;
      v_bestand_ext  VARCHAR2(1000);
      v_bestand_fout VARCHAR2(1000);
      v_bestand_fout_verslag VARCHAR2(1000);
      v_max_date     date;
      v_count_b_ad   number;
      v_open_reg_found boolean;
      v_best_id      number;
    BEGIN
      v_pos1 := INSTR(p_bestand, '.', -1);
      IF v_pos1 > 0 THEN
        v_bestand_z_ext := SUBSTR(p_bestand, 1, v_pos1 - 1);
        v_bestand_ext := SUBSTR(p_bestand, v_pos1 + 1);
      END IF;
      v_bestand_fout := v_bestand_z_ext || '_' || TO_CHAR(SYSDATE, 'yyyymmdd_hh24miss') || '.' || v_bestand_ext;
      v_bestand_fout_verslag := v_bestand_z_ext || '_' || TO_CHAR(SYSDATE, 'yyyymmdd_hh24miss') || '.txt';
      -- bepaal familienr - �ARCHIEF2DA_<familienummer>_<sysdate/tijd>.pdf�
      IF UPPER(p_bestand) NOT LIKE 'ARCHIEF2DA%' THEN
        v_errmsg := 'ERROR: de naam van bestand ' || p_bestand || ' begint niet met ARCHIEF2DA!';
        RAISE e_normale_fout;
      END IF;
      v_familienummer := getFamiUitBestand(p_bestand => p_bestand, x_errmsg => v_errmsg);
      IF v_errmsg IS NOT NULL THEN
        RAISE e_normale_fout;
      END IF;

      v_okmsg := NULL;
      v_familienummer_act := getFamiActueel(p_familienummer => v_familienummer, x_fami_id => v_fami_id, x_okmsg => v_okmsg, x_errmsg => v_errmsg);
      IF v_familienummer_act IS NULL THEN
        v_errmsg := 'ERROR: in de naam van bestand ' || p_bestand || ' werd familienummer ' || v_familienummer
          || ' gevonden, maar hier werd geen actuele familie bij gevonden (evt via gekoppelde families):' || v_crlf || v_errmsg;
        RAISE e_normale_fout;
      END IF;
      -- bepaal doel-map (afh van familienr)
      v_pos1 := INSTR(v_familienummer_act, '-');
      IF v_pos1 = 0 THEN
        v_jaar := '1980';
      ELSE
        v_jaar := SUBSTR(v_familienummer_act, 1, v_pos1 - 1);
        IF LENGTH(v_jaar) != 2 THEN
          v_errmsg := 'ERROR: in de naam van bestand ' || p_bestand || ' werd familienummer ' || v_familienummer
            || ' gevonden, met als actuele familie ' || v_familienummer_act || '. Hieruit kon geen correct jaar (2 lang) bepaald worden!';
          RAISE e_normale_fout;
        END IF;
        DECLARE
          v_num NUMBER;
        BEGIN
          v_num := TO_NUMBER(v_jaar);
        EXCEPTION
          WHEN OTHERS THEN
            v_errmsg := 'ERROR: in de naam van bestand ' || p_bestand || ' werd familienummer ' || v_familienummer
              || ' gevonden, met als actuele familie ' || v_familienummer_act || '. Hieruit kon geen correct jaar (2 lang en numeriek) bepaald worden!';
            RAISE e_normale_fout;
        END;
        IF TO_NUMBER(v_jaar) > TO_NUMBER(TO_CHAR(SYSDATE, 'YY')) THEN
          v_jaar := '19' || v_jaar;
        ELSE
          v_jaar := '20' || v_jaar;
        END IF;
      END IF;
      -- registreer bestand
      v_map_doel := REPLACE(LOWER(r_btyp.standaard_pad), '<creationyear>', v_jaar);
      -- evt aanmaken
      v_ok := beh_file_00.create_file_directory(v_map_doel);

      -- niet overschrijven in "doel"-map
      IF beh_file_00.file_exists(p_dir_file => rtrim(v_map_doel, '\') || '\' || p_bestand) THEN
        v_errmsg := 'ERROR: Bestand ' || p_bestand || ' moet verplaatst worden naar map ''' || v_map_doel || ''', maar daar bestaat het al!' || v_crlf;
        RAISE e_normale_fout;
      END IF;
      
      -- check relatie met doos-bestand
      SELECT MAX(creation_date)
      INTO   v_max_date
      FROM   beh_archief2DA_dossiers
      WHERE  bestandsnaam = p_bestand;
      IF v_max_date IS NOT NULL THEN
        v_errmsg := 'ERROR: Bestand ' || p_bestand || ' is reeds geregistreerd in beh_archief2DA_dossiers (op: ' || TO_CHAR(v_max_date, 'dd-mm-yyyy') || ')!' || v_crlf;
        RAISE e_normale_fout;
      END IF;

      -- registreren
      INSERT INTO KGC_BESTANDEN
      ( entiteit_code
      , entiteit_pk
      , btyp_id
      , locatie
      , bestandsnaam
      , bestand_specificatie
      ) VALUES
      ( r_btyp.enti_code
      , v_fami_id
      , r_btyp.btyp_id
      , v_map_doel
      , p_bestand
      , RTRIM(v_map_doel, '\') || '\' || p_bestand
      ) RETURNING best_id INTO v_best_id;
      
      v_count_b_ad := 0;
      v_open_reg_found := false;
      FOR r_b_ad IN c_b_ad( p_dossier      => v_familienummer) LOOP
        v_count_b_ad := v_count_b_ad + 1;
        IF r_b_ad.bestandsnaam IS NULL THEN -- gevonden!
          UPDATE beh_archief2DA_dossiers
          SET    familienummer = v_familienummer_act
          ,      bestandsnaam  = p_bestand
          ,      best_id       = v_best_id
          ,      datum_bestand = SYSDATE
          WHERE  b_ad_id = r_b_ad.b_ad_id;
          v_open_reg_found := true;
          EXIT;
        END IF;
      END LOOP;
      IF NOT v_open_reg_found THEN
        v_errmsg := 'ERROR: Bij bestand ' || p_bestand || ' kon geen doos gevonden worden in beh_archief2DA_dossiers met een leeg bestand (voor deze familie werden wel ' || v_count_b_ad || ' rijen met gevuld bestand gevonden)!' || v_crlf;
        RAISE e_normale_fout;
      END IF;
      
      -- verplaats bestand
      BEGIN
        v_ok := beh_file_00.file_move
        ( p_dir_file_from => rtrim(v_map_scan, '\') || '\' || p_map || p_bestand
        , p_dir_file_to   => rtrim(v_map_doel, '\') || '\' || p_bestand
        );
      EXCEPTION
        WHEN OTHERS THEN
          v_errmsg := 'ERROR: verplaatsen van bestand ' || p_bestand || ' van ' || v_map_scan || ' ' || p_map || ' naar ' || v_map_doel || ' ging niet goed: ' || SQLERRM;
          RAISE e_normale_fout;
      END;
      -- alles OK:
      add_tekst('<li>PDF ' || p_bestand || ' gekoppeld aan familie ' || v_familienummer_act || ' en correct verplaatst naar map ' || v_map_doel || '</li>' || v_crlf);
      COMMIT;
    EXCEPTION
      WHEN e_normale_fout THEN -- schrijf bestand en verslag
        ROLLBACK;
        BEGIN
          v_ok := beh_file_00.file_move
          ( p_dir_file_from => rtrim(v_map_scan, '\') || '\' || p_map || p_bestand
          , p_dir_file_to   => rtrim(v_map_fout, '\') || '\' || v_bestand_fout
          );
        EXCEPTION
          WHEN OTHERS THEN -- afbreken
            raise_application_error(-20000, 'ERROR1: ' || v_errmsg || v_crlf || 'ERROR2: verplaatsen van bestand ' || p_bestand || ' van ' || v_map_scan || ' ' || p_map || ' naar ' || v_map_fout || ' ging niet goed: ' || SQLERRM);
        END;
        DECLARE
          v_foutfile_handle       utl_file.file_type;
        BEGIN
          -- maak foutbestand v_bestand_fout_verslag
          v_foutfile_handle := utl_file.fopen(v_map_fout_dir_obj, v_bestand_fout_verslag, 'W');
          utl_file.put(v_foutfile_handle, v_errmsg);
          UTL_FILE.FFLUSH(v_foutfile_handle);
          utl_file.fclose(v_foutfile_handle);
        EXCEPTION
          WHEN OTHERS THEN -- afbreken
            raise_application_error(-20000, 'ERROR1: ' || v_errmsg || v_crlf || 'ERROR2: aanmaken van bestand ' || v_bestand_fout_verslag || ' in map ' || v_map_fout || ' ging niet goed: ' || SQLERRM);
        END;
        add_tekst('<li>PDF ' || p_bestand || ' NIET correct verwerkt: ' || v_errmsg || '</li>' || v_crlf);
      WHEN OTHERS THEN -- afbreken
        ROLLBACK;
        add_tekst('<li>PDF ' || p_bestand || ' is NIET correct verwerkt; error=' || SQLERRM || '</li>' || v_crlf);
        RAISE; -- want: niet voorzien!
    END verwerk_dossier;
  BEGIN
    v_email_onderwerp := 'Helix overzicht van inlezen dossiers ARCHIEF2DA (database ' || substr(ora_database_name, 1, 20) || ')';
  
    dbms_lob.createtemporary(v_html_message, TRUE);
    add_tekst(
      '<style type="text/css">
       <!--
         .tab1 { margin-left: 40px; }
         .tab2 { margin-left: 80px; }
       -->
       </style>
       <br>Dit is een automatisch gegenereerde mail over het inlezen dossiers ARCHIEF2DA.</br></br>' || v_crlf);
  
    add_tekst('<br>Uitgevoerd op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf);
    add_tekst('<br>Uitgevoerd door: ' || user || '</br>' || v_crlf);
    add_tekst('<br>Uitgevoerd op database: ' || substr(ora_database_name, 1, 20) || '</br>' || v_crlf);
    
    -- bepaal bestanden
    v_waarde := kgc_sypa_00.systeem_waarde ('BEH_ARCHIEF2DA_PADEN');
    v_pos := INSTR(v_waarde, '|');
    v_map_scan := SUBSTR(v_waarde, 1, v_pos - 1);
    v_map_fout := SUBSTR(v_waarde, v_pos + 1);
    IF v_map_scan IS NULL THEN
      raise_application_error(-20000, 'ERROR: uit sypa BEH_ARCHIEF2DA_PADEN kon geen bron-directory worden bepaald!');
    END IF;
    IF v_map_fout IS NULL THEN
      raise_application_error(-20000, 'ERROR: uit sypa BEH_ARCHIEF2DA_PADEN kon geen fout-directory worden bepaald!');
    END IF;
    -- btyp
    OPEN c_btyp(p_code => 'ARCHIEF2DA');
    FETCH c_btyp INTO r_btyp;
    CLOSE c_btyp;
    IF r_btyp.standaard_pad IS NULL THEN
      raise_application_error(-20000, 'ERROR: uit btyp ARCHIEF2DA kon geen standaard-pad worden bepaald!');
    END IF;
    
    add_tekst('Bron-directory: ' || v_map_scan || '</br>' || v_crlf);
    add_tekst('Doel-directory: ' || r_btyp.standaard_pad || '</br>' || v_crlf);
    add_tekst('Fout-directory: ' || v_map_fout || '</br></br>' || v_crlf);
    v_map_scan_dir_obj := beh_file_00.create_directory_object(v_map_scan);
    v_map_fout_dir_obj := beh_file_00.create_directory_object(v_map_fout);

    -- stap1: csv
    v_ret := beh_auto_uits.dirlist( p_dir     => v_map_scan
                                  , p_filter  => '*.csv'
                                  , x_dirlist => v_tab_filedesc
                                  );
    if v_ret != 'OK' then
      raise_application_error(-20000, 'ERROR: dirlist voor map ''' || v_map_scan || ''' leverde een fout: ' || v_ret);
    END IF;
    IF v_tab_filedesc.COUNT = 0 THEN
      add_tekst('<br>Stap 1: geen CSV-bestanden gevonden</br>' || v_crlf);
    ELSE
      add_tekst('<br>Stap 1: ' || v_tab_filedesc.COUNT || ' CSV-bestanden gevonden</br>' || v_crlf);
      FOR i IN v_tab_filedesc.FIRST .. v_tab_filedesc.LAST LOOP
        -- verwerk bestanden
        IF v_tab_filedesc(i).type = 'F' THEN -- File
--          add_tekst('Bestandsnaam: ' || v_tab_filedesc(i).naam || '</br>' || v_crlf);
          verwerk_doosfile(p_bestand => v_tab_filedesc(i).naam);
        END IF;
      END LOOP;
    END IF;
    -- stap2: pdf
    v_ret := beh_auto_uits.dirlist( p_dir     => v_map_scan
                                  , p_filter  => NULL
                                  , x_dirlist => v_tab_filedesc
                                  );
    if v_ret != 'OK' then
      raise_application_error(-20000, 'ERROR: dirlist voor map ''' || v_map_scan || ''' leverde een fout: ' || v_ret);
    END IF;
    IF v_tab_filedesc.COUNT = 0 THEN
      add_tekst('<br>Stap 2: geen mappen/bestanden gevonden</br>' || v_crlf);
    ELSE
      add_tekst('<br>Stap 2: ' || v_tab_filedesc.COUNT || ' mappen/bestanden gevonden</br>' || v_crlf);
      FOR i IN v_tab_filedesc.FIRST .. v_tab_filedesc.LAST LOOP
        -- verwerk bestanden
        IF v_tab_filedesc(i).type = 'F' THEN -- File
--          add_tekst('Bestandsnaam: ' || v_tab_filedesc(i).naam || '</br>' || v_crlf);
          verwerk_dossier(p_map => NULL, p_bestand => v_tab_filedesc(i).naam);
        ELSIF v_tab_filedesc(i).type = 'D' THEN -- Directory
          v_ret := beh_auto_uits.dirlist( p_dir     => RTRIM(v_map_scan, '\') || '\' || v_tab_filedesc(i).naam
                                        , p_filter  => '*.pdf'
                                        , x_dirlist => v_tab_filedesc2
                                        );
          IF v_ret != 'OK' THEN
            raise_application_error(-20000, 'ERROR: dirlist voor map ''' || RTRIM(v_map_scan, '\') || '\' || v_tab_filedesc(i).naam || ''' leverde een fout: ' || v_ret);
          END IF;
          IF v_tab_filedesc2.COUNT = 0 THEN
            add_tekst('<br>Stap 2: geen PDF-bestanden gevonden in map ' || v_tab_filedesc(i).naam || '</br>' || v_crlf);
          ELSE
            add_tekst('<br>Stap 2: ' || v_tab_filedesc2.COUNT || ' PDF-bestanden gevonden in map ' || v_tab_filedesc(i).naam || '</br>' || v_crlf);
            FOR j IN v_tab_filedesc2.FIRST .. v_tab_filedesc2.LAST LOOP
              -- verwerk bestanden
              IF v_tab_filedesc2(j).type = 'F' THEN -- File
      --          add_tekst('Bestandsnaam: ' || v_tab_filedesc(i).naam || '</br>' || v_crlf);
                verwerk_dossier(p_map => v_tab_filedesc(i).naam || '\', p_bestand => v_tab_filedesc2(j).naam);
              END IF;
            END LOOP;
          END IF;
          -- verwijder map - mogelijk lukt dit niet omdat er nog iets anders in staat als een pdf... dan niet erg.
          host_command('rmdir "' || RTRIM(v_map_scan, '\') || '\' || v_tab_filedesc(i).naam || '"');
        END IF;
      END LOOP;
    END IF;    -- verslag
    add_tekst('<br>Gereed op datum: ' || to_char(sysdate, 'DD-MM-YYYY HH24:MI') || '</br>' || v_crlf);
    FOR r_mede IN c_mede(p_waarde => 'A') LOOP
      send_mail_html( p_from    => v_email_afzender
                    , p_to      => r_mede.email
                    , p_subject => v_email_onderwerp
                    , p_html    => v_html_message
                    );
    END LOOP;
    beh_file_00.remove_directory_object(v_map_scan_dir_obj);
    v_map_scan_dir_obj := NULL;
    beh_file_00.remove_directory_object(v_map_fout_dir_obj);
    v_map_fout_dir_obj := NULL;
    dbms_lob.freetemporary(v_html_message);
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      IF v_map_scan_dir_obj IS NOT NULL THEN
        beh_file_00.remove_directory_object(v_map_scan_dir_obj);
      END IF;
      IF v_map_fout_dir_obj IS NOT NULL THEN
        beh_file_00.remove_directory_object(v_map_fout_dir_obj);
      END IF;

      FOR r_mede IN c_mede(p_waarde => 'E') LOOP
        send_mail_html( p_from    => v_email_afzender
                      , p_to      => r_mede.email
                      , p_subject => 'Error in ' || v_email_onderwerp
                      , p_html    => 'ERROR: ' || SQLERRM || v_crlf || v_crlf || v_html_message
                      );
      END LOOP;
      dbms_lob.freetemporary(v_html_message);
  END verwerk_bestanden;


END BEH_ARCHIEF2DA;
/

/
QUIT
