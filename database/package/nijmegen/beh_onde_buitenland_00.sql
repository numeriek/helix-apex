CREATE OR REPLACE PACKAGE "HELIX"."BEH_ONDE_BUITENLAND_00" is
/******************************************************************************
   NAME:       beh_onde_buitenland_00
   PURPOSE:    bepaal of het onderzoek buitenlands is, dwz de aanvrager zit in het buitenland.
               om te gebruiken in select scripts, bv jaarverslag van DNA

   REVISIONS:
   Date        Author           Description
   ----------  ---------------  ----------------------------------------------
   19-01-2016  Rene Brand       Package aangemaakt als refactoring van functie
                                beh_onde_bu. Deze functie roept nu dit package
                                aan.
   17-10-2016  Rene Brand       Functies beh_rela_is_intern en beh_onde_rela_is_intern
                                toegevoegd om te controleren of een onderzoek /
                                relatie intern is voor RadboudUMC.

******************************************************************************/

-- Bepaal het land van een relatie.
function get_rela_land(p_rela_id in number) return varchar;
pragma restrict_references(get_rela_land, wnds, wnps, rnps);

-- Bepaal het land van een onderzoek via de relatie.
function beh_onde_bu(p_onde_id number) return varchar;
pragma restrict_references(beh_onde_bu, wnds, wnps, rnps);

-- Bepaal of een relatie intern is.
-- Geeft 'J' terug als de relatie intern is, anders 'N'.
function beh_rela_is_intern(p_rela_id number) return varchar;
pragma restrict_references(beh_rela_is_intern, wnds, wnps, rnps);

-- Bepaal of de relatie van een onderzoek intern of extern is.
-- Geeft 'J' terug als de relatie intern is, anders 'N'.
function beh_onde_rela_is_intern(p_onde_id number) return varchar;
pragma restrict_references(beh_onde_rela_is_intern, wnds, wnps, rnps);

end beh_onde_buitenland_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_ONDE_BUITENLAND_00" is


-- Bepaal het land van een relatie.
function get_rela_land(p_rela_id in number) return varchar is

  --Refactored van oude beh_onde_bu() functie.
  cursor c_rela(p_rela_id in number) is
  select decode( nvl(to_char(rela.inst_id), 'LEEG')
               , 'LEEG', (decode( upper(nvl(rela.land, 'NEDERLAND')), 'NEDERLAND', 'NEDERLAND', 'BUITENLAND' ))
               , (decode( upper(nvl(inst.land, 'NEDERLAND')), 'NEDERLAND', 'NEDERLAND', 'BUITENLAND' ))
               ) land
  from   kgc_relaties rela
  ,      kgc_instellingen inst
  where  rela.inst_id = inst.inst_id(+)
    and  rela.rela_id = p_rela_id;

  r_rela  c_rela%rowtype;
  v_tekst varchar2(100);

begin
  v_tekst   := null;

  open c_rela(p_rela_id);
  fetch c_rela into r_rela;

  if c_rela%found then
    v_tekst := r_rela.land;
  else
    v_tekst := 'Relatie bestaat niet';
  end if;

  close c_rela;

  return v_tekst;
end;



-- Bepaal het land van een onderzoek via de relatie.
function beh_onde_bu(p_onde_id number) return varchar is

  cursor c_onde (p_onde_id in number) is
  select rela_id
  from   kgc_onderzoeken
  where  onde_id = p_onde_id;

  r_onde  c_onde%rowtype;
  v_tekst varchar2(100);

begin
  v_tekst   := null;

  open  c_onde(p_onde_id);
  fetch c_onde into r_onde;

  if c_onde%found then
    v_tekst := get_rela_land(r_onde.rela_id);
  else
    v_tekst := 'Onderzoek bestaat niet';
  end if;

  close c_onde;

  return v_tekst;
end;



-- Bepaal of een relatie intern of extern is.
function beh_rela_is_intern(p_rela_id number) return varchar is

  cursor c_rela (b_rela_id in number)is
    select rela.liszcode
    from   kgc_relaties rela
    where  rela.rela_id = b_rela_id
      and  rela.liszcode is not null;

  r_rela c_rela%ROWTYPE;
  v_relatie_intern varchar2(1);

begin
  v_relatie_intern := 'N';

  open c_rela(p_rela_id);
  fetch c_rela into r_rela;

  if c_rela%found then
    v_relatie_intern := 'J';
  end if;

  close c_rela;

  return v_relatie_intern;
end;



-- Bepaal of de relatie van een onderzoek intern of extern is.
function beh_onde_rela_is_intern(p_onde_id number) return varchar is

  cursor c_onde_rela(b_onde_id in number)is
    select onde.onde_id, onde.rela_id, rela.liszcode
    from   kgc_onderzoeken onde
      join kgc_relaties rela on rela.rela_id = onde.rela_id
    where  onde.onde_id = b_onde_id
      and  rela.liszcode is not null;

  r_onde_rela c_onde_rela%ROWTYPE;
  v_relatie_intern varchar2(1);

begin
  v_relatie_intern := 'N';

  open c_onde_rela(p_onde_id);
  fetch c_onde_rela into r_onde_rela;

  if c_onde_rela%found then
    v_relatie_intern := 'J';
  end if;

  close c_onde_rela;

  return v_relatie_intern;
end;


end beh_onde_buitenland_00;
/

/
QUIT
