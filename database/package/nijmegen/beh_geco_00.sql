CREATE OR REPLACE PACKAGE "HELIX"."BEH_GECO_00" as
FUNCTION gen_code_inst
(
p_inst_naam in kgc_instellingen.naam%type,
p_inst_woonplaats in kgc_instellingen.woonplaats%type,
p_inst_postcode in kgc_instellingen.postcode%type := null
)
return varchar2;
FUNCTION gen_code_rela
(
p_rela_achternaam in kgc_relaties.achternaam%type,
p_rela_voorletters in kgc_relaties.voorletters%type,
p_rela_woonplaats in kgc_relaties.woonplaats%type,
p_rela_postcode in kgc_relaties.postcode%type := null,
p_rela_land in kgc_relaties.land%type := null
)
return varchar2;
end BEH_GECO_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_GECO_00" as
function gen_code_inst
(
p_inst_naam in kgc_instellingen.naam%type,
p_inst_woonplaats in kgc_instellingen.woonplaats%type,
p_inst_postcode in kgc_instellingen.postcode%type := null
)
return varchar2
is

v_inst_naam kgc_instellingen.naam%type := upper(p_inst_naam);
v_inst_woonplaats kgc_instellingen.woonplaats%type := upper(p_inst_woonplaats);
v_vulling_karakter varchar2(1) := 'X';
v_bestaat_code_al number := 0;
v_voorstel_code kgc_instellingen.code%type;

cursor c_filter_inst_naam is
select filter_string
from   beh_kgc_instellingen_filters
where  filter_inst_naam = 'J'
order by filter_inst_naam_volgnr
;

r_filter_inst_naam c_filter_inst_naam%rowtype;

cursor c_filter_inst_woonplaats is
select filter_string
from   beh_kgc_instellingen_filters
where  filter_inst_woonplaats = 'J'
order by filter_inst_woonplaats_volgnr
;

r_filter_inst_woonplaats c_filter_inst_woonplaats%rowtype;

begin
-- karakters die weggehaald moeten worden, worden hier weggehaald
for r_filter_inst_naam in c_filter_inst_naam loop
    v_inst_naam := trim(replace(v_inst_naam, r_filter_inst_naam.filter_string));
end loop;

for r_filter_inst_woonplaats in c_filter_inst_woonplaats loop
    v_inst_woonplaats := trim(replace(v_inst_woonplaats, r_filter_inst_woonplaats.filter_string));
end loop;

-- line feeds (chr(10)) en carrage returns (chr(13)) worden weggehaald
v_inst_naam := replace(replace(v_inst_naam, chr(10)), chr(13));
v_inst_woonplaats := replace(replace(v_inst_woonplaats, chr(10)), chr(13));

-- als het om een NL postcode gaat dan wordt de code zo samengesteld:
-- eerste 4 letters van woonplaats (met X aanvullen als het niet groot genoeg is) + 6 letters van instelling naam(met X aanvullen als het niet groot genoeg is)
if (beh_alfu_00.is_nl_postcode(p_inst_postcode)) then
 v_inst_naam := rpad(substr(trim(nvl(v_inst_naam,v_vulling_karakter)), 0, 6), 6, v_vulling_karakter);
 v_inst_woonplaats := rpad(substr(trim(nvl(v_inst_woonplaats,v_vulling_karakter)), 0, 4), 4, v_vulling_karakter);
-- als het om een Buitenlandse postcode gaat dan wordt de code zo samengesteld:
-- X + eerste 4 letters van woonplaats (met X aanvullen als het niet groot genoeg is) + 5 letters van instelling naam(met X aanvullen als het niet groot genoeg is)
else
 v_inst_naam := rpad(substr(trim(nvl(v_inst_naam,v_vulling_karakter)), 0, 5), 5, v_vulling_karakter);
 v_inst_woonplaats := v_vulling_karakter || rpad(substr(trim(nvl(v_inst_woonplaats,v_vulling_karakter)), 0, 4), 4, v_vulling_karakter);

end if;
-- diacritische tekens weghalen
v_voorstel_code := convert(v_inst_woonplaats || v_inst_naam, 'US7ASCII');
-- de code moet uniek zijn; dit wordt hier gechekt. Zo nee wordt de laatste karakter vervangen door een sequentce (1, 2, 3, etc)
for i in 1..1000 loop
    select count(*) into v_bestaat_code_al from kgc_instellingen where upper(code) = v_voorstel_code;

    if v_bestaat_code_al > 0 then
        v_voorstel_code := substr(v_voorstel_code,1, (length(v_voorstel_code) - length(i))) || to_char(i);
    else
        exit;
    end if;

end loop;

return v_voorstel_code;
end gen_code_inst;
function gen_code_rela
(
p_rela_achternaam in kgc_relaties.achternaam%type,
p_rela_voorletters in kgc_relaties.voorletters%type,
p_rela_woonplaats in kgc_relaties.woonplaats%type,
p_rela_postcode in kgc_relaties.postcode%type := null,
p_rela_land in kgc_relaties.land%type := null
)
return varchar2
is

v_vulling_karakter varchar2(1) := 'X';
v_bestaat_code_al number := 0;
v_voorstel_code kgc_relaties.code%type;

v_gevonden boolean := true;
v_volgnr number := 0;
v_nl_relatie varchar2(1) := 'J';

v_rela_achternaam kgc_relaties.achternaam%type := trim(upper(p_rela_achternaam));
v_rela_woonplaats kgc_relaties.woonplaats%type := trim(upper(p_rela_woonplaats));
v_rela_voorletters kgc_relaties.voorletters%type := trim(upper(p_rela_voorletters));
v_rela_postcode kgc_relaties.postcode%type := trim(upper(p_rela_postcode));
v_rela_land kgc_relaties.land%type := trim(upper(p_rela_land));

cursor c_filter_rela_achternaam is
select filter_string
from   beh_kgc_instellingen_filters
where  filter_inst_naam = 'J'
order by filter_inst_naam_volgnr
;

r_filter_rela_achternaam c_filter_rela_achternaam%rowtype;

cursor c_filter_rela_woonplaats is
select filter_string
from   beh_kgc_instellingen_filters
where  filter_inst_woonplaats = 'J'
order by filter_inst_woonplaats_volgnr
;

r_filter_rela_woonplaats c_filter_rela_woonplaats%rowtype;

cursor c_rela (p_code varchar2) is
select *
from kgc_relaties
where code =p_code
;
r_rela c_rela%rowtype;

begin
-- karakters die weggehaald moeten worden, worden hier weggehaald
for r_filter_rela_achternaam in c_filter_rela_achternaam loop
    v_rela_achternaam := trim(replace(v_rela_achternaam, r_filter_rela_achternaam.filter_string));
end loop;

for r_filter_rela_woonplaats in c_filter_rela_woonplaats loop
    v_rela_woonplaats := trim(replace(v_rela_woonplaats, r_filter_rela_woonplaats.filter_string));
    v_rela_land := trim(replace(v_rela_land, r_filter_rela_woonplaats.filter_string));
end loop;

-- line feeds (chr(10)) en carrage returns (chr(13)) worden weggehaald
v_rela_achternaam := replace(replace(v_rela_achternaam, chr(10)), chr(13));
v_rela_woonplaats := replace(replace(v_rela_woonplaats, chr(10)), chr(13));
v_rela_land := replace(replace(v_rela_land, chr(10)), chr(13));

-- eerste 5 letters van achternaam
 v_rela_achternaam := rpad(substr(trim(nvl(v_rela_achternaam, v_vulling_karakter)), 1, 5), 5, v_vulling_karakter);
 -- als het om een BI postcode gaat, zo wordt de code samengesteld:
 -- eerste 5 karakters van achternaam + eerste 1 karakter van voorletters + eerste 4 karakters van woonplaats
if length(trim(p_rela_postcode)) > 0 then
  if (beh_alfu_00.is_nl_postcode(p_rela_postcode)) then
    v_nl_relatie := 'J';
  else
    v_nl_relatie := 'N';
  end if;
else
  if upper(nvl(v_rela_land, 'NEDERLAND')) = 'NEDERLAND' then
    v_nl_relatie := 'J';
  else
    v_nl_relatie := 'N';
  end if;
end if;

 if v_nl_relatie = 'J' then
   v_rela_woonplaats := rpad(substr(trim(nvl(v_rela_woonplaats,v_vulling_karakter)), 1, 4), 4, v_vulling_karakter);
   v_rela_voorletters := rpad(substr(trim(nvl(v_rela_voorletters,v_vulling_karakter)), 1, 1), 1, v_vulling_karakter);
   v_voorstel_code := convert(v_rela_achternaam || v_rela_voorletters || v_rela_woonplaats, 'US7ASCII');
   -- als het om een BU postcode gaat, zo wordt de code samengesteld:
   -- 'X' + eerste 5 karakters van achternaam + eerste 4 karakters van land
  else
    v_rela_land := rpad(substr(trim(nvl(v_rela_land,v_vulling_karakter)), 1, 4), 4, v_vulling_karakter);
    v_voorstel_code := convert(v_vulling_karakter || v_rela_achternaam || v_rela_land, 'US7ASCII');
 end if;

-- zorgen dat de v_voostel_code uniek is. als de v_voorstel_code niet uniek is
-- dan word(en) de laatste karakter(s) vervangen met een volgnr
while true loop
    open c_rela(v_voorstel_code);
    fetch c_rela into r_rela;

    if c_rela%notfound then
        exit;
   else
        v_volgnr := v_volgnr+1;
        v_voorstel_code := substr(r_rela.code, 1, (length(r_rela.code) - length(v_volgnr))) || to_char(v_volgnr);
    end if;

    close c_rela;

end loop;
return v_voorstel_code;
end gen_code_rela;
end BEH_GECO_00;
/

/
QUIT
