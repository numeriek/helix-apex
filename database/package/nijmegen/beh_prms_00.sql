CREATE OR REPLACE PACKAGE "HELIX"."BEH_PRMS_00" as
-- extra informatie, zie mantis 11308

procedure maak_file
;
end BEH_PRMS_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_PRMS_00"
as
-------------------------------------------------------------------------------
--  CV gemaakt om digitaal gegevens van agora monsters aan promise aan te leveren
--  extra informatie, zie mantis 11308
-------------------------------------------------------------------------------

procedure maak_file
is

v_aantal number;

v_file_handler    utl_file.file_type;
v_file_name       varchar2(100) ;
v_dir             varchar2(250);
--\\Umcfs005\ebhdata$\REPRO\Helix
v_create_dir_sql  varchar2(2000) := 'create directory V_DIR as ''\\Umcfs005\ebhdata$\REPRO\Helix''';
v_drop_dir_sql    varchar2(2000) := 'drop directory V_DIR';
v_header          varchar2(250);
v_db_name         varchar2 (50);

  cursor c_agra is
  select frac.fractienummer
||'; '||FRTY.CODE
||'; '||mate.code
||'; '||mons.monsternummer
||'; '||indi.code
||'; '||mons.datum_aanmelding
||'; '||mons.datum_afname
||'; '||replace(mons.commentaar, chr(10), ' ')
||'; '||herk.code
||'; '||ongr.code
||'; '||pers.pers_id
||'; '||pers.zisnr
||'; '||pers.geboortedatum
||'; '||pers.geslacht
||'; '||pers.voorletters
||'; '||decode(
       instr((pers.achternaam_partner||' '||pers.voorvoegsel_partner||' - '),'-')
       ,3
       ,null
       ,pers.achternaam_partner||' '||pers.voorvoegsel_partner||' - ')
       ||pers.achternaam||' '||pers.voorvoegsel
||'; '||decode(fare.code, 'VAD', 'is vader van pers_id: '||repe.pers_id_relatie
                          , 'MOE', 'is moeder van pers_id: '||repe.pers_id_relatie
                          , 'DOC', 'is dochter van pers_id: '||repe.pers_id_relatie
                          , 'ZOO', 'is zoon van pers_id: '||repe.pers_id_relatie
                          ,  null)  tekst
from kgc_personen pers
, kgc_kgc_afdelingen kafd
, kgc_monsters mons
, kgc_materialen mate
, bas_fracties frac
, BAS_FRACTIE_TYPES frty
, kgc_herkomsten herk
, kgc_onderzoeksgroepen ongr
, kgc_monster_indicaties moin
, kgc_indicatie_teksten indi
, kgc_relaties_van_persoon repe
, kgc_familie_relaties fare
, kgc_personen pers2
where mons.perS_id = pers.perS_id
and mons.kafd_id = kafd.kafd_id
and kafd.code = 'DNA-S'
and mons.mate_id = mate.mate_id
and mons.mons_id = moin.mons_id(+)
and moin.indi_id = indi.indi_id(+)
and frac.frty_id = frty.frty_id(+)
and mons.mons_id = frac.mons_id(+)
and mons.herk_id = herk.herk_id(+)
and mons.ongr_id = ongr.ongr_id
and pers.pers_id = REPE.PERS_ID(+)
and repe.fare_id = fare.fare_id(+)
and repe.pers_id_relatie = pers2.pers_id(+)
and ongr.code = 'AGORA'
--and ongr.code in ('AGORA', 'MOLGEN', 'MULTIFACT')
and mons.datum_aanmelding  between to_date('01-01-2016', 'dd-mm-yyyy') and to_date('01-04-2016', 'dd-mm-yyyy')
order by mons.monsternummer;



begin
DBMS_OUTPUT.PUT_LINE('begin');
 --   execute immediate v_drop_dir_sql;

select count(*) into v_aantal
from  KGC_TALEN;

if v_aantal > 0 then
    SELECT global_name INTO v_db_name
    FROM GLOBAL_NAME;
DBMS_OUTPUT.PUT_LINE(v_db_name);

    select to_char(sysdate,'dd-mm-yyyy-HH24-mi-ss') into v_file_name
    from dual;

    v_header := 'fractienummer; fractietype; materiaal; monsternummer; indicatie; datum_aanmelding; datum_afname; commentaar; herkomst; onderzoeksgroep; pers_id; zisnr; geboortedatum; geslacht; voorletters; Achternaam; familie_relatie';

DBMS_OUTPUT.PUT_LINE(v_file_name);

    v_file_name := 'PROMISE_AGORA_'||v_file_name||'.csv';

DBMS_OUTPUT.PUT_LINE(v_file_name);


    execute immediate v_create_dir_sql;

DBMS_OUTPUT.PUT_LINE('na v_create_dir_sql');

    v_file_handler := UTL_FILE.FOPEN('V_DIR', v_file_name, 'W');

DBMS_OUTPUT.PUT_LINE('na v_file_handler');

    utl_file.put_line( v_file_handler, v_header);

DBMS_OUTPUT.PUT_LINE('voor de loop');
    for r_agra in c_agra loop
        utl_file.put_line( v_file_handler, r_agra.tekst);
        utl_file.fflush(v_file_handler);

    end loop;

        DBMS_OUTPUT.PUT_LINE('na de loop');

    UTL_FILE.FCLOSE(v_file_handler);
    execute immediate v_drop_dir_sql;
    DBMS_OUTPUT.PUT_LINE('na drop dir');
else
    null;
end if;
DBMS_OUTPUT.PUT_LINE('einde');

    exception
    when others then
      UTL_FILE.FCLOSE(v_file_handler);
      raise_application_error(-20000, SQLERRM);

end maak_file;
end BEH_PRMS_00;
/

/
QUIT
