SET DEFINE OFF
CREATE OR REPLACE PACKAGE "HELIX"."BEH_ZOMA_00" as
function create_zoma_file
(
    p_uits_dir_files in varchar2
,   p_ontvangers_zorgmail in varchar2
,   p_subject in varchar2
,   p_bericht in varchar2
)
return varchar;

function rela_heeft_zorgmail
(
p_rela_id in number
)
return varchar;
procedure verwerk_zorgmail_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
);
procedure verwerk_zorgmail -- procedure om zorgmail te versturen
;
END;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_ZOMA_00" is

function create_zoma_file
(
    p_uits_dir_files in varchar2
,   p_ontvangers_zorgmail in varchar2
,   p_subject in varchar2
,   p_bericht in varchar2
)
return varchar is
v_this_prog varchar2(100) := 'beh_zoma_00.create_zoma_file';

v_error_txt varchar2(4000);

paramlist beh_SOAP_PARAMLIST;

v_soap_service varchar2(100) := 'createzomafile';
v_handshake kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER'));
v_verzender_email kgc_systeem_par_waarden.waarde%type :=
        trim(kgc_sypa_00.systeem_waarde('BEH_ZORGM_AFZENDER_EMAIL'));
v_dest_dir kgc_systeem_parameters.standaard_waarde%type :=
        trim(kgc_sypa_00.systeem_waarde('BEH_ZOMA_FILE_DEST_DIR'));
v_subject kgc_systeem_parameters.standaard_waarde%type :=
        CONVERT (p_subject, 'US7ASCII');
v_bericht kgc_systeem_parameters.standaard_waarde%type :=
        CONVERT (p_bericht, 'US7ASCII');
v_return varchar2(4000);
begin
--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));
-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'param_01', sys_context('userenv','db_name'));
beh_soap_00.add_parameter(paramlist, 'param_02', v_handshake);
beh_soap_00.add_parameter(paramlist, 'param_03', '<![CDATA['||p_uits_dir_files||']]>');
beh_soap_00.add_parameter(paramlist, 'param_04', '<![CDATA['||v_dest_dir||']]>');
beh_soap_00.add_parameter(paramlist, 'param_05', v_verzender_email);
beh_soap_00.add_parameter(paramlist, 'param_06', p_ontvangers_zorgmail);
beh_soap_00.add_parameter(paramlist, 'param_07', '<![CDATA['||v_subject||']]>');
beh_soap_00.add_parameter(paramlist, 'param_08', '<![CDATA['||v_bericht||']]>');

--aanroep soapservice
v_return := beh_soap_00.call_soap_service(v_soap_service, paramlist);

return v_return;

exception
    when others then
    v_error_txt := v_this_prog || ' => ' || ':' || SUBSTR(SQLERRM, 1, 2000);
    dbms_output.put_Line(v_error_txt);
    return v_error_txt;

end create_zoma_file;
function rela_heeft_zorgmail
(
p_rela_id in number
)
return varchar is

  v_zorgmail varchar2(200);

  cursor c_rela is
    select atwa.waarde
    from kgc_relaties rela
    , kgc_attributen attr
    , kgc_attribuut_waarden atwa
    where rela.rela_id = p_rela_id
    and   atwa.attr_id = attr.attr_id
    and   atwa.id = rela.rela_id
    and   ATTR.TABEL_NAAM = 'KGC_RELATIES'
    and   attr.code = 'ZORGMAIL'
    ;

begin

  v_zorgmail:= null;

  IF ( p_rela_id   IS NOT NULL )  THEN
    OPEN  c_rela;
    FETCH c_rela INTO  v_zorgmail;
    CLOSE c_rela;
  END IF;
  if v_zorgmail is not null then
    v_zorgmail := 'J';
  else
    v_zorgmail := 'N';
  end if;
  RETURN( v_zorgmail );
exception
 when others then
    return 'N';
end rela_heeft_zorgmail;

procedure verwerk_zorgmail_log
(
p_verzonden in varchar,
p_ontvanger_id_intern in number,
p_ontvanger_id_extern in varchar,
p_ontvanger_type in varchar,
p_adres_verzonden varchar,
p_log_tekst in varchar,
p_entiteit_id in number,
p_bericht_tekst in varchar
) is

v_bety_id beh_bericht_types.bety_id%type;
v_datum_verzonden date := sysdate;
v_oracle_uid kgc_medewerkers.oracle_uid%type;

begin

  select bety_id into v_bety_id
  from beh_bericht_types
  where code = 'ZORGMAIL'
  and upper(vervallen) = upper('N')
  ;

  select sys_context('USERENV', 'SESSION_USER')
  into v_oracle_uid
  from dual
  ;

  insert into beh_berichten
  (  bety_id,
  verzonden,
  datum_verzonden,
  oracle_uid,
  ontvanger_id_intern,
  ontvanger_id_extern,
  ontvanger_type,
  adres_verzonden,
  log_tekst,
  entiteit,
  entiteit_id,
  bericht_tekst
  )
  values
  (v_bety_id,
  p_verzonden,
  v_datum_verzonden,
  v_oracle_uid,
  p_ontvanger_id_intern,
  p_ontvanger_id_extern,
  p_ontvanger_type,
  p_adres_verzonden,
  p_log_tekst,
  'BEST',
  p_entiteit_id,
  p_bericht_tekst
  )
  ;

commit;

end verwerk_zorgmail_log;

procedure verwerk_zorgmail -- procedure om zorgmail te versturen

is
-- recent geautoriseerde onderzoeken (bv de afgelopen zeven dagen)
  cursor c_onah is
  select   onah.onde_id
  ,      onde.onderzoeknr
  ,      to_char(onah.last_update_date, 'dd-mm-yyyy:hh24:mi:ss') datum
  ,      onah.onah_id
  ,      onah.actie
  ,      onde.kafd_id
  ,      onde.pers_id
  ,      pers.aanspreken
  ,      to_char(pers.geboortedatum, 'dd-mm-yyyy') geboortedatum
  ,      nvl(onui.code, 'onb') uitslagcode
  from kgc_onde_autorisatie_historie onah
  ,    kgc_onderzoeken onde
  ,    kgc_personen pers
  ,    KGC_ONDERZOEK_UITSLAGCODES onui
  ,    kgc_kgc_afdelingen kafd
  ,    kgc_onderzoeksgroepen ongr
  where onah.last_update_date > ( sysdate - 7)
  and   onde.onde_id = onah.onde_id
  and   onde.pers_id = pers.pers_id
  and   onde.onui_id = onui.onui_id (+)
  and   kafd.kafd_id = onde.kafd_id
  and   ongr.ongr_id = onde.ongr_id
  AND   NVL (
          INSTR (
            kgc_sypa_00.
            standaard_waarde ('BEH_EPD_UITSLCODES_UITSLUITEN',
                        kafd.code,
                        ongr.code,
                        NULL
                        ),
            NVL (onui.code, 'NVL')),
          0) = 0
  and   onah.actie = 'A'
  and   BEH_EPD_00.onderzoek_epd_afgerond(onah.onde_id) = 'J'
  and   onah.last_update_date = ( select  max(onah2.last_update_date)
                                  from  kgc_onde_autorisatie_historie onah2
                                  where onah.onde_id = onah2.onde_id
                                  group by onah2.onde_id
                                 )
  order by onah.onde_id
  ,        onde.onderzoeknr
  ;


  r_onah c_onah%rowtype;

  v_uits_id kgc_uitslagen.uits_id%TYPE;
  v_brty_id kgc_uitslagen.brty_id%TYPE;

-- onderzoek afgerond  in het epd
  cursor c_epdf (p_onde_id  in number) is
  select epvw.onde_id
  from   beh_epd_vw epvw
  where  epvw.onde_id = p_onde_id
  and    epvw.afgerond = 'J'
  ;

  r_epdf c_epdf%rowtype;

-- uitslag
   cursor c_uits (p_onde_id  in number, v_uits_id in number) is
   select brty_id
   , last_update_date
   from kgc_uitslagen
   where onde_id = p_onde_id
   and uits_id = v_uits_id
   ;

   r_uits c_uits%rowtype;

-- onderzoeken met een bestand (pdf) bij de meest recente uitslag voor deze relatie
   cursor c_best ( v_uits_id in number, v_rela_id in number)
   is
   select best.best_id
   , bestand_specificatie
   from kgc_bestanden best
   where best.entiteit_code = 'UITS'
   and   best.entiteit_pk = v_uits_id
   and   best.rela_id = v_rela_id
   order by best.best_id desc
   ;
   r_best c_best%rowtype;
   v_best_id kgc_bestanden.best_id%type;

-- alleen aanvragers en kopiehouders met een zorgmail adres
  cursor c_avkh ( p_uits_id in number) is
  select  avkh.rela_id
  ,       avkh.koho
  ,       atwa.waarde zorgmail
  ,       rela.aanspreken
  from beh_aanv_koho_vw avkh
  , kgc_attributen attr
  , kgc_attribuut_waarden atwa
  , kgc_relaties rela
  where avkh.uits_id = p_uits_id
  and avkh.rela_id = atwa.id
  and atwa.attr_id = attr.attr_id
  and attr.tabel_naam = 'KGC_RELATIES'
  and attr.code = 'ZORGMAIL'
  and avkh.rela_id = rela.rela_id
  and instr(upper(ATWA.WAARDE), '@ZORGMAIL.NL') > 0
  ;

-- alleen als er nog geen bericht is verstuurd
  cursor c_beri (p_best_id in number, p_rela_id in number) is
  select beri.beri_id
  from beh_berichten beri
  , beh_bericht_types bety
  where bety.bety_id = beri.bety_id
  and bety.code = 'ZORGMAIL'
  and beri.entiteit = 'BEST'
  and beri.entiteit_id = p_best_id
  and beri.ontvanger_type = 'RELA'
  and beri.ontvanger_id_intern = p_rela_id
  and beri.verzonden = 'J'
--  and beri.log_tekst = 'OK'
  ;

  r_beri c_beri%rowtype;

--aantal berichten die al eerder over deze uitslag succesvol zijn verstuurd
  cursor c_aantal (p_uits_id in number, p_rela_id in number) is
  select count(*)
  from beh_berichten beri
  , beh_bericht_types bety
  where bety.bety_id = beri.bety_id
  and bety.code = 'ZORGMAIL'
  and beri.entiteit = 'BEST'
  and beri.ontvanger_type = 'RELA'
  and beri.ontvanger_id_intern = p_rela_id
  and beri.verzonden = 'J'
  and instr(beri.log_tekst, 'UITS_ID = ')  >0
  and substr(beri.log_tekst, 11)  = to_char(p_uits_id)
  ;

  v_aantal number(2) := 0 ;
  v_afzender  varchar2(100)  := null;
  v_log_tekst varchar2(4000)   := null;
  v_verzonden varchar2(10)   := null;
  v_brie_id   kgc_brieven.brie_id%type    := null;
  v_attr_id   kgc_attributen.attr_id%type := null;
  -- variabelen voor zorgmail versturen
  v_ok_verzend_zorgmail varchar2(4000);
  v_msg varchar2(32767) := null;
  v_bijlagen varchar2(10000) := null;
  v_ontvangers_zorgmail varchar2(2000) := null;
  v_bericht varchar2(10000):= null;
  v_onderwerp varchar2(500) := null;
  v_omschrijving varchar2(1000) := null;

  -- om een mail te sturen aan helixbeheer als het fout gaat
  V_ontvanger_FOUT varchar2(200) := 'cukz124@umcn.nl' ;
  V_titel_FOUT     varchar2(100) := 'ZORGMAIL versturen mislukt';
  V_boodschap_FOUT varchar2(3000) := null;
  v_global_name varchar2(100) := null;

begin
-- recent geautoriseerde onderzoeken

  for r_onah in c_onah loop
    v_uits_id   := null;
    v_uits_id := beh_uits_00.get_uits_id(r_onah.onde_id);
    if v_uits_id is not null then
      v_onderwerp := null;
      v_onderwerp := 'KGCN uitslag <versie>'||r_onah.onderzoeknr||', van '||r_onah.aanspreken||' '||r_onah.geboortedatum;

  -- onderzoeken afgerond in het epd
    open c_epdf (r_onah.onde_id);
    fetch c_epdf into r_epdf;
    close c_epdf;
    if r_epdf.onde_id is not null then

    --bepaal brieftype bij de uitslag. Dit is nodig voor het registreren van de brief

    v_brty_id:= null;
    open c_uits(r_onah.onde_id, v_uits_id);
    fetch c_uits into r_uits;
    v_brty_id:= r_uits.brty_id ;
    close c_uits;

    -- alleen aanvragers en kopiehouders met een zorgmail adres

      for r_avkh in c_avkh(v_uits_id) loop


       -- is er een bestand bij deze uitslag voor deze relatie?
       open c_best( v_uits_id, r_avkh.rela_id);
       fetch c_best into r_best;
       If c_best%found then
         v_best_id := r_best.best_id;
       else
         v_best_id := null;
       end if;
       close c_best;

       if v_best_id  is not null then

          -- alleen als er nog geen bericht is verstuurd
            open c_beri(v_best_id, r_avkh.rela_id);
            fetch c_beri into r_beri;
            if c_beri%found then -- er is al een bericht verstuurd , klaar.
              null;
            else
           -- is er al eerder een bericht over deze uitslag verstuurd aan deze relatie? zo ja , dan wordt dit een volgende versie
           open c_aantal(v_uits_id, r_avkh.rela_id);
           fetch c_aantal into v_aantal;
           if c_aantal%notfound then v_aantal := 0; end if;
           close c_aantal;
           if  v_aantal= 0 then v_onderwerp:= replace(v_onderwerp, '<versie>', ' ');
           else
             v_aantal := v_aantal + 1;
             v_onderwerp:= replace(v_onderwerp, '<versie>', v_aantal||'e versie ');
           end if  ;
                        -- acties:
                        -- 1. verstuur bericht

                              v_afzender  := 'KGCN';

                              v_ontvangers_zorgmail := upper(r_avkh.zorgmail);



                              select standaard_waarde into v_bericht
                                from kgc_systeem_parameters
                                where code = 'BEH_ZOMA_EMAIL_TEKST_BERICHT';



                              v_ok_verzend_zorgmail:= 'N';



                        --      IF v_ontvangers_zorgmail is not null then


                                if  (    v_bericht  is not null
                                     and r_best.bestand_specificatie is not null
                                     )
                                then
                              select  global_name into v_global_name from global_name;
                              if ((v_global_name like 'PROD%')
                                 or (v_ontvangers_zorgmail in(UPPER('helixdummy@zorgmail.nl'), UPPER('acpthelixzorgmail@zorgmail.nl'), UPPER('mm.kuipers@Zorgmail.nl') ))
                                 )
                                 then
                                -- v_ok_verzend_zorgmail := 'FOUT'; -- voor test doeleinden

                                  v_ok_verzend_zorgmail:= beh_zoma_00.create_zoma_file(
                                                              p_uits_dir_files => r_best.bestand_specificatie
                                                              , p_ontvangers_zorgmail => v_ontvangers_zorgmail
                                                              , p_subject => v_onderwerp
                                                              , p_bericht => v_bericht
                                                                );


                                   IF v_ok_verzend_zorgmail <> 'OK' then
                                      v_verzonden := 'N' ;
                                      -- versturen bericht is niet  geslaagd
                                      v_log_tekst := v_ok_verzend_zorgmail;
                                    -- STUUR EEN MAIL ALS HET VERSTUREN MISLUKT:
                                      v_afzender:= 'HELIX_DB';
                                      v_titel_fout:= v_global_name||', '||r_onah.onderzoeknr;
                                      v_boodschap_fout := 'Omgeving '||v_global_name||' Fout bij versturen zorgmail d.m.v. beh_zoma_00.verwerk_zorgmail vanwege: '||substr(v_log_tekst, 1, 3000);

                                      UTL_MAIL.SEND(  sender   => v_afzender,
                                                        recipients => v_ontvanger_fout,
                                                        subject    => v_titel_fout,
                                                        message    => v_boodschap_fout);


                                    -- de fout ook loggen in de notities

                                      v_omschrijving := substr(v_log_tekst, 1, 1000);

                                      INSERT INTO kgc_notities
                                          (entiteit
                                          , id
                                          , code
                                          , omschrijving
                                          )
                                          values('UITS'
                                          ,  v_uits_id
                                          , 'ZORGMAIL'
                                          , v_omschrijving
                                          )
                                          ;
                                        commit;

                                   ELSE
                                      v_verzonden := 'J' ;
                                      -- versturen bericht is geslaagd
                                      v_log_tekst := 'UITS_ID = '||v_uits_id;
                                   END IF;

                        -- 2. loggen van het versturen van het bericht
                                   verwerk_zorgmail_log(  p_verzonden =>  v_verzonden
                                               , p_ontvanger_id_intern => r_avkh.rela_id
                                               , p_ontvanger_id_extern => NULL
                                               , p_ontvanger_type => 'RELA'
                                               , p_adres_verzonden => v_ontvangers_zorgmail
                                               , p_log_tekst => substr(v_log_tekst, 1, 2000)
                                               , p_entiteit_id => v_BEST_id
                                               , p_bericht_tekst => v_onderwerp||', '||v_bericht
                                               );


                        -- alleen als hetversturen van het bericht is geslaagd :
                            if v_verzonden = 'J' then
                             -- 3. registreer als brief

                             -- de pk is later nodig , dus die moet eerst opgehaald worden
                                select kgc_brie_seq.nextval into v_brie_id from dual;

                                 insert into kgc_brieven(
                                   brie_id
                                 , kafd_id
                                 , pers_id
                                 , datum_print
                                 , kopie
                                 , brty_id
                                 , onde_id
                                 , rela_id
                                 , uits_id
                                 , datum_verzonden
                                 , geadresseerde
                                 , best_id
                                 , aanmaakwijze
                                 )
                                 values(
                                 v_brie_id
                                 , r_onah.kafd_id
                                 , r_onah.pers_id
                                 , sysdate
                                 , r_avkh.koho
                                 , v_brty_id
                                 , r_onah.onde_id
                                 , r_avkh.rela_id
                                 , v_uits_id
                                 , sysdate
                                 , kgc_adres_00.relatie (r_avkh.rela_id, 'J')
                                 , r_best.best_id
                                 , 'ZORGMAIL'
                                 );
                                 commit;


                             -- 4 invoeren in notities
                             if v_aantal = 0 then
                               v_omschrijving := 'Verstuurd op: '||to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')||', aan: '||r_avkh.aanspreken;
                             else
                               v_omschrijving := 'Versie '||v_aantal||' verstuurd op: '||to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss')||', aan: '||r_avkh.aanspreken;
                             end if;
                                INSERT INTO kgc_notities
                                  (entiteit
                                  , id
                                  , code
                                  , omschrijving
                                  )
                                  values('UITS'
                                  ,  v_uits_id
                                  , 'ZORGMAIL'
                                  , v_omschrijving
                                  )
                                  ;
                                commit;

                              -- klaar

                            end if;

                          end if;
                        end if;

           end if;
           close c_beri;
         end if;
      end loop; -- c_avkh

      end if;  -- c_epdf
    end if;  -- v_uits_id is not null
  end loop;  -- c_onah

end verwerk_zorgmail;

END;
/

/
