CREATE OR REPLACE PACKAGE "HELIX"."BEH_RAPP_00" IS


FUNCTION zwan_locatie
(
P_zwan_id in number
)
return varchar;

FUNCTION zwan_ziekenhuisnaam
(
p_zwan_id number
)
return varchar2 ;

function zoek_locatie
(
P_onde_id number
)
return varchar;

FUNCTION ziekenhuisnaam
(
p_onde_id number
)
return varchar2 ;

FUNCTION metab_dd_categorie
( p_meet_id IN NUMBER
)
--   PURPOSE:    bepaal de categorie van een meetwaarde bij onderzoeksgroep DD,
--               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN VARCHAR2;

FUNCTION metab_dd_interpretatie
( p_meet_id IN NUMBER
)
--   PURPOSE:    bepaal de interpretatie van een meetwaarde bij onderzoeksgroep DD,
--               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN VARCHAR2;



FUNCTION metab_dd_variatie
( p_meet_id IN NUMBER
)
--   PURPOSE:    bepaal de variatie van een meetwaarde bij onderzoeksgroep DD,
--               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN VARCHAR2;

FUNCTION metab_d_MITODNA
( p_onde_id IN NUMBER
)
--   PURPOSE:    bepaal of er een meting is met de stoftestgroep  D_MITODNA
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN varchar2;

FUNCTION DATUM_AUTO_ZWAN_ONDE
( p_zwan_id IN NUMBER
)
--   PURPOSE:    bepaal de meets recente autorisetie datum van onderzoeken bij een zwangerpschap
--      om te gebruiken bij het rapport kgcvreu51 akties
RETURN VARCHAR2;

FUNCTION pers_info_taal
(p_pers_id number
, p_taal_id number
)
return varchar ;
--   PURPOSE:    geeft de persoons gegevens met een vertaling van het geslach
--              wordt gebruikt in het rapport kgcuits51M_DD



end BEH_RAPP_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_RAPP_00" AS
/******************************************************************************
   NAME:       beh_rapp_00
   PURPOSE: alle beh ... functies en procedures die gebruikt worden in rapporten
   het betreft aanpassingen van het helix beheer nijmegen

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19-3-2011      z878110       1. Created this package.
******************************************************************************/

FUNCTION zwan_locatie(P_zwan_id number) return varchar is
--   PURPOSE:    zoek de locatienaam van het ziekenhuis bij een zwangerschap
--              om te gebruiken in de vreugdebrief van prenataal
    cursor c_herk (p_zwan_id in varchar)
    is
    select herk.code
    from kgc_foetussen foet
    ,    kgc_monsters mons
    ,    kgc_herkomsten herk
    where  foet.zwan_id = p_zwan_id
    and    foet.foet_id = mons.foet_id
    and    mons.herk_id = herk.herk_id(+)
    ;

    r_herk c_herk%ROWTYPE;

  v_herk varchar2(1000);


BEGIN

  v_herk := null;

  IF  p_zwan_id is not null  THEN

    OPEN c_herk(p_zwan_id);
    FETCH c_herk INTO r_herk;
    IF c_herk%FOUND THEN
      v_herk := r_herk.code;
    END IF;
    CLOSE c_herk;

  END IF;


  RETURN v_herk;


end zwan_locatie;


FUNCTION zwan_ziekenhuisnaam (
p_zwan_id number
)
return varchar2
is
--   PURPOSE:    ziekenhuis naam die getoond wordt in de vreugedebrieven
  cursor c_atwa ( p_zwan_id in number)is
    select atwa.waarde
    from   kgc_attributen         attr
    ,      kgc_attribuut_waarden  atwa
    ,      kgc_foetussen          foet
    ,      kgc_monsters           mons
    ,      kgc_herkomsten         herk
    where  attr.attr_id = atwa.attr_id
    and    atwa.id = herk.herk_id
    and    mons.herk_id = herk.herk_id
    and    mons.foet_id = foet.foet_id
    and    foet.zwan_id = p_zwan_id
    and    attr.code = 'ZIEKENHUISNAAM'
    and    tabel_naam = 'KGC_HERKOMSTEN' ;

  r_atwa c_atwa%ROWTYPE;

  v_herk varchar2(100);

BEGIN
  v_herk := null;
  OPEN C_atwa(p_zwan_id);
  FETCH c_atwa into r_atwa;
  IF c_atwa%FOUND THEN
    v_herk := r_atwa.waarde;
  ELSE
    select atwa.waarde into v_herk  -- geef de ziekenhuisnaam van Obst_nijm
        from kgc_attributen attr
        , kgc_attribuut_waarden atwa
        , kgc_herkomsten herk
        where atwa.attr_id = attr.attr_id
        and atwa.id = herk.herk_id
        and attr.code = 'ZIEKENHUISNAAM'
        and attr.tabel_naam= 'KGC_HERKOMSTEN'
        and herk.code = 'OBST_NIJM';
  END IF;
  CLOSE c_atwa;
  RETURN v_herk;
END zwan_ziekenhuisnaam;


function zoek_locatie(P_onde_id number) return varchar is
--   PURPOSE:    zoek de locatienaam van het ziekenhuis bij een herkomstcode van prenataal
--               op basis van de onde_id van het onderzoek
--      om te gebruiken in de uitslagbrief van prenataal
  cursor c_herk (p_onde_id in varchar)
    is
    select herk.code
    from kgc_onderzoek_monsters onmo
    ,    kgc_monsters mons
    ,    kgc_herkomsten herk
    where  onmo.onde_id = p_onde_id
    and    onmo.mons_id = mons.mons_id
    and    mons.herk_id = herk.herk_id(+)
    ;

    r_herk c_herk%ROWTYPE;

  v_herk varchar2(1000);


BEGIN

  v_herk := null;

  IF  p_onde_id is not null  THEN

    OPEN c_herk(p_onde_id);
    FETCH c_herk INTO r_herk;
    IF c_herk%FOUND THEN
      v_herk := r_herk.code;
    END IF;
    CLOSE c_herk;

  END IF;

  RETURN v_herk;

END zoek_locatie;

FUNCTION ziekenhuisnaam (p_onde_id number) return varchar2 is
--   PURPOSE:    ziekenhuis naam die getoond wordt in het rapport voor uitslagen van PRE-onderzoeksgroepen
  cursor c_atwa ( p_onde_id in number)is
    select atwa.waarde
    from   kgc_attributen         attr
    ,      kgc_attribuut_waarden  atwa
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_onderzoeken onde
    ,      kgc_monsters           mons
    ,      kgc_herkomsten         herk
    where  attr.attr_id = atwa.attr_id
    and    atwa.id = herk.herk_id
    and    mons.herk_id = herk.herk_id
    and    onmo.mons_id = mons.mons_id
    and    onde.onde_id = onmo.onde_id
    and    onde.onde_id = p_onde_id
    and    attr.code = 'ZIEKENHUISNAAM'
    and    tabel_naam = 'KGC_HERKOMSTEN' ;

  r_atwa c_atwa%ROWTYPE;

  v_herk varchar2(100);

BEGIN
  v_herk := null;
  OPEN C_atwa(p_onde_id);
  FETCH c_atwa into r_atwa;
  IF c_atwa%FOUND THEN
    v_herk := r_atwa.waarde;
  ELSE
    select atwa.waarde into v_herk  -- geef de ziekenhuisnaam van Obst_nijm
        from kgc_attributen attr
        , kgc_attribuut_waarden atwa
        , kgc_herkomsten herk
        where atwa.attr_id = attr.attr_id
        and atwa.id = herk.herk_id
        and attr.code = 'ZIEKENHUISNAAM'
        and attr.tabel_naam= 'KGC_HERKOMSTEN'
        and herk.code = 'OBST_NIJM';
  END IF;
  CLOSE c_atwa;
  RETURN v_herk;
END ziekenhuisnaam;

FUNCTION metab_dd_categorie
( p_meet_id IN NUMBER
)
--   PURPOSE:    bepaal de categorie van een meetwaarde bij onderzoeksgroep DD,
--               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  cursor c_mdet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Categorie'
     and mwst.code = 'DDMITOCHIP'
   ;

begin

  OPEN  c_mdet;
  fetch c_mdet into v_return;
  If c_mdet%notfound then
    v_return := '-';
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END metab_dd_categorie;

FUNCTION metab_dd_interpretatie
( p_meet_id IN NUMBER
)
--   PURPOSE:    bepaal de interpretatie van een meetwaarde bij onderzoeksgroep DD,
--               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  cursor c_mdet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Interpretatie'
     and mwst.code = 'DDMITOCHIP'
   ;

begin

  OPEN  c_mdet;
  fetch c_mdet into v_return;
  If c_mdet%notfound then
    v_return := '-';
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END metab_dd_interpretatie;




FUNCTION metab_dd_variatie
( p_meet_id IN NUMBER
)
--   PURPOSE:    bepaal de variatie van een meetwaarde bij onderzoeksgroep DD,
--               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN VARCHAR2
IS
  v_stof VARCHAR2(100) := null;
  v_ref VARCHAR2(100) := null;
  v_meet VARCHAR2(100) := null;
  v_return VARCHAR2(100) := null;

  cursor c_stof is
  select stof.omschrijving
     from bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mwst.code = 'DDMITOCHIP'
   ;
  cursor c_ref is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Ref.'
     and mwst.code = 'DDMITOCHIP'
   ;
  cursor c_meet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Meetw.'
     and mwst.code = 'DDMITOCHIP'
   ;
begin

  OPEN  c_stof;
  fetch c_stof into v_stof;
  If c_stof%notfound then
    v_stof := null;
  end if;

  OPEN  c_ref;
  fetch c_ref into v_ref;
  If c_ref%notfound then
    v_ref := null;
  end if;

  OPEN  c_meet;
  fetch c_meet into v_meet;
  If c_meet%notfound then
    v_meet := null;
  end if;

  v_return := v_stof||v_ref||'>'||v_meet;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END metab_dd_variatie;

FUNCTION metab_d_MITODNA
( p_onde_id IN NUMBER
)
--   PURPOSE:    bepaal of er een meting is met de stoftestgroep  D_MITODNA
--               om te gebruiken bij het rapport kgcuits51M_DD
RETURN varchar2
IS
  v_return varchar2(1);

  cursor c_meti is
  select meti.meti_id
  from bas_metingen meti
  , kgc_stoftestgroepen stgr
  where meti.stgr_id = stgr.stgr_id
  and stgr.code = 'D_MITODNA'
  and METI.ONde_ID = p_onde_id
  ;
  r_meti c_meti%rowtype;

begin

  v_return := 'N';

  OPEN c_meti;
  Fetch C_meti into r_meti;
  If c_meti%FOUND then
    v_return := 'J';
  else
    v_return :='N';
  end if;
  Close c_meti;

  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN v_return := 'N';

END  metab_d_MITODNA;


FUNCTION DATUM_AUTO_ZWAN_ONDE
( p_zwan_id IN NUMBER
)
--   PURPOSE:    bepaal de meets recente autorisetie datum van onderzoeken bij een zwangerpschap
--      om te gebruiken bij het rapport kgcvreu51 akties
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR c_onde
  IS
    SELECT to_char(onde.datum_autorisatie, 'dd-mm-yyyy') datum_autorisatie
    FROM   kgc_onderzoeken onde
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onde.onde_id
    AND    zwon.zwan_id = p_zwan_id
    and not exists (select *     -- er mogen geen niet afgronde of niet afgeautoriseerd onderzoeken zijn bij deze zwangerschap
                    from kgc_onderzoeken onde2
                    ,    kgc_prenataal_zwan_onde_vw zwon2
                    where zwon2.onde_id = onde2.onde_id
                    and  zwon2.onde_id = p_zwan_id
                    and ( onde.afgerond <>'J'
                        or onde.datum_autorisatie is null)
                    )
    ORDER BY onde.datum_autorisatie desc
    ;
  r_onde c_onde%rowtype;
BEGIN

  OPEN c_onde;
  FETCH c_onde INTO r_onde;
  IF c_onde%FOUND THEN
        v_return := r_onde.datum_autorisatie;
  ELSE
        v_return := 'Onbekend';
  END IF;
  CLOSE c_onde;
  return (v_return);
EXCEPTION
      WHEN VALUE_ERROR
      THEN
        NULL;
END DATUM_AUTO_ZWAN_ONDE;

FUNCTION pers_info_taal(p_pers_id number, p_taal_id number) return varchar is
--   PURPOSE:    geeft de persoons gegevens met een vertaling van het geslach
--              wordt gebruikt in het rapport kgcuits51M_DD
  cursor c_pers is
      SELECT achternaam
      ,      aanspreken
      ,      geboortedatum
      ,      zisnr
      ,      kgc_vert_00.vertaling('PERS_GESL',null,geslacht,p_taal_id) geslacht
      ,      BSN
      FROM   kgc_personen
      WHERE  pers_id = p_pers_id
      ;
  r_pers c_pers%ROWTYPE;

  v_return varchar2(200);
BEGIN
  OPEN c_pers;
  Fetch c_pers INTO r_pers;
  IF   c_pers%FOUND then
   v_return := r_pers.aanspreken||' ('||r_pers.geboortedatum||', '||r_pers.geslacht||') BSN: '||r_pers.BSN;
  ELSE
   v_return := 'Onbekend';
  END if;
  CLOSE c_pers;
  RETURN v_return;
END pers_info_taal;



END beh_rapp_00;
/

/
QUIT
