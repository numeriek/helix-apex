CREATE OR REPLACE PACKAGE "HELIX"."BEH_MOLGEN_00" as
g_scheidingsteken varchar2(1) := '$';
procedure verwerk_log
(
p_log_tekst in varchar,
p_move_id in number
);
--===================================
function is_bsn_goed
(
p_bsn in number
)
return boolean;
--===================================
function voer_pers_in
(
p_achternaam in varchar2,
p_voorvoegsel in varchar2,
p_achternaam_partner in varchar2,
p_voorvoegsel_partner in varchar2,
p_gehuwd in varchar2,
p_voorletters in varchar2,
p_geboortedatum in varchar2,
p_geslacht in varchar2,
p_adres in varchar2,
p_postcode in varchar2,
p_woonplaats in varchar2,
p_zisnr in varchar2,
p_bsn in varchar2,
p_beheer_in_zis in varchar2 := 'N'
)
return number;
--===================================
function voer_mons_in
(
p_ongr_id in number,
p_kafd_id in number,
p_pers_id in number,
p_mate_id in number,
p_nader_gebruiken in varchar2,
p_aantal_buizen in number,
p_datum_afname varchar2,
p_commentaar in varchar2,
p_herk_id in number
)
return number;
--===================================
function voer_frac_in
(
p_mons_id in number,
p_kafd_id in number,
p_ongr_id in number,
p_frty_id in number
)
return number;
--===================================
function verwerk_pers
(
p_move_id in number,
p_achternaam in varchar2,
p_voorvoegsel in varchar2,
p_achternaam_partner in varchar2,
p_voorvoegsel_partner in varchar2,
p_gehuwd in varchar2,
p_voorletters in varchar2,
p_geboortedatum in varchar2,
p_geslacht in varchar2,
p_adres in varchar2,
p_postcode in varchar2,
p_woonplaats in varchar2,
p_zisnr in varchar2,
p_bsn in varchar2
)
return varchar2;
--===================================
function verwerk_mons
(
p_move_id in number,
p_pers_id in kgc_personen.pers_id%type,
p_ongr_code in varchar2,
p_kafd_code in varchar2,
p_mate_code in varchar2,
p_nader_gebruiken in varchar2,
p_aantal_buizen in varchar2,
p_datum_afname in varchar2,
p_commentaar in varchar2,
p_forceer_mons in varchar2,
p_herk_code in varchar2
)
return varchar2;
--===================================
function verwerk_frac
(
p_move_id in number,
p_mons_id in kgc_monsters.mons_id%type,
p_kafd_code in varchar2,
p_ongr_code in varchar2,
p_frty_code in varchar2,
p_forceer_frac in varchar2
)
return varchar2;
--===================================
procedure start_verwerking;
end beh_molgen_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_MOLGEN_00" as
--#####################################
procedure verwerk_log
(
p_log_tekst in varchar,
p_move_id in number
) is
begin

update  beh_molgen_verwerk
set   	inlees_log = inlees_log || g_scheidingsteken || p_log_tekst
where   move_id = p_move_id
;
end verwerk_log;
function is_bsn_goed
(
p_bsn in number
)
return boolean
is
begin
kgc_pers_00.controleer_bsn(  p_zisnr        		=> null
							,p_bsn 			        => p_bsn
							,p_bsn_geverifieerd     => 'N'
							,p_controleer_uniciteit => 'N');
return true;
exception
 when others then
    return false;
end is_bsn_goed;

function voer_pers_in
(
p_achternaam in varchar2,
p_voorvoegsel in varchar2,
p_achternaam_partner in varchar2,
p_voorvoegsel_partner in varchar2,
p_gehuwd in varchar2,
p_voorletters in varchar2,
p_geboortedatum in varchar2,
p_geslacht in varchar2,
p_adres in varchar2,
p_postcode in varchar2,
p_woonplaats in varchar2,
p_zisnr in varchar2,
p_bsn in varchar2,
p_beheer_in_zis in varchar2 := 'N'
)
return number
is
v_pers_id kgc_personen.pers_id%type;

v_achternaam kgc_personen.achternaam%type := p_achternaam;
v_voorvoegsel kgc_personen.voorvoegsel%type := p_voorvoegsel;
v_achternaam_partner kgc_personen.achternaam_partner%type := p_achternaam_partner;
v_voorvoegsel_partner kgc_personen.voorvoegsel_partner%type := p_voorvoegsel_partner;
v_voorletters kgc_personen.voorletters%type := p_voorletters;
v_woonplaats kgc_personen.woonplaats%type := p_woonplaats;

v_aanspreken kgc_personen.aanspreken%type;

v_postcode kgc_personen.postcode%type := p_postcode;

begin
select kgc_pers_seq.nextval into v_pers_id from dual;

kgc_formaat_00.formatteer (p_naamdeel => 'ACHTERNAAM', x_naam => v_achternaam);
kgc_formaat_00.formatteer (p_naamdeel => 'VOORVOEGSEL', x_naam => v_voorvoegsel);
kgc_formaat_00.formatteer (p_naamdeel => 'ACHTERNAAM_PARTNER', x_naam => v_achternaam_partner);
kgc_formaat_00.formatteer (p_naamdeel => 'VOORVOEGSEL_PARTNER', x_naam => v_voorvoegsel_partner);
kgc_formaat_00.formatteer (p_naamdeel => 'VOORLETTERS', x_naam => v_voorletters);
kgc_formaat_00.formatteer (p_naamdeel => 'WOONPLAATS', x_naam => v_woonplaats);

kgc_formaat_00.standaard_aanspreken (   p_voorletters => v_voorletters,
                                        p_voorvoegsel => v_voorvoegsel,
                                        p_achternaam => v_achternaam,
                                        p_achternaam_partner => v_achternaam_partner,
                                        p_voorvoegsel_partner => v_voorvoegsel_partner,
                                        p_gehuwd => upper(p_gehuwd),
                                        p_geslacht => upper(p_geslacht),
                                        x_aanspreken => v_aanspreken);

v_postcode := kgc_formaat_00.formatteer_postcode (p_postcode => p_postcode);

insert into kgc_personen
  (
    pers_id,
    achternaam,
    gehuwd,
    voorvoegsel,
    achternaam_partner,
    voorvoegsel_partner,
    voorletters,
    geboortedatum,
    geslacht,
    aanspreken,
    adres,
    postcode,
    woonplaats,
    zisnr,
    beheer_in_zis,
    bsn
  )
  values
  (
    v_pers_id,
    v_achternaam,
    upper(p_gehuwd),
    v_voorvoegsel,
    v_achternaam_partner,
    v_voorvoegsel_partner,
    v_voorletters,
    to_date(substr(p_geboortedatum, 1, 10), 'dd-mm-yyyy'),
    upper(p_geslacht),
    v_aanspreken,
    p_adres,
    upper(v_postcode),
    v_woonplaats,
    p_zisnr,
    upper(p_beheer_in_zis),
    to_number(p_bsn)
  );
return v_pers_id;
end voer_pers_in;

function voer_mons_in
(
p_ongr_id in number,
p_kafd_id in number,
p_pers_id in number,
p_mate_id in number,
p_nader_gebruiken varchar2,
p_aantal_buizen in number,
p_datum_afname in varchar2,
p_commentaar in varchar2,
p_herk_id in number
)
return number
is
v_mons_id kgc_monsters.mons_id%type;
v_monsternummer kgc_monsters.monsternummer%type;
v_mede_id kgc_medewerkers.mede_id%type;
v_leeftijd kgc_monsters.leeftijd%type;
v_leef_id kgc_monsters.leef_id%type;
begin

select kgc_mons_seq.nextval into v_mons_id
from dual
;

v_monsternummer := 	kgc_nust_00.genereer_nr
					(p_nummertype => 'MONS',
					p_ongr_id => p_ongr_id,
					p_kafd_id => p_kafd_id)
;

for r in 	(
				select mede_id
				from kgc_medewerkers
				where upper(oracle_uid) = upper(user)
			)
	loop
		v_mede_id := r.mede_id;
	end loop;

v_leeftijd := 	kgc_leef_00.leeftijd
				( p_pers_id => p_pers_id,
				p_datum_afname => to_date(p_datum_afname, 'dd-mm-yyyy'))
;

v_leef_id := kgc_leef_00.leeftijdscategorie
			(p_kafd_id => p_kafd_id,
			p_aantal_dagen => v_leeftijd)
;

insert into kgc_monsters
  (
    mons_id,
    monsternummer,
    ongr_id,
    kafd_id,
    pers_id,
    mate_id,
    nader_gebruiken,
    mede_id,
    leef_id,
    leeftijd,
    aantal_buizen,
    datum_afname,
    commentaar,
    herk_id
  )
values
  (
    v_mons_id,
    v_monsternummer,
    p_ongr_id,
    p_kafd_id,
    p_pers_id,
    p_mate_id,
    upper(p_nader_gebruiken),
    v_mede_id,
    v_leef_id,
    v_leeftijd,
    p_aantal_buizen,
    p_datum_afname,
    p_commentaar,
    p_herk_id
  );

return v_mons_id;
end voer_mons_in;
--#####################################
function voer_frac_in
(
p_mons_id in number,
p_kafd_id in number,
p_ongr_id in number,
p_frty_id in number
)
return number
is
v_monsternummer kgc_monsters.monsternummer%type;
v_frac_id bas_fracties.frac_id%type;
v_fractienummer bas_fracties.fractienummer%type;
v_frty_code bas_fractie_types.code%type;
v_letter_fractienr bas_fractie_types.letter_fractienr%type;
v_standaard_waarde kgc_systeem_parameters.standaard_waarde%type;
begin

select monsternummer into v_monsternummer
from kgc_monsters
where mons_id = p_mons_id
;

select code into v_frty_code
from bas_fractie_types
where frty_id = p_frty_id
and   upper(vervallen) = upper('N')
;

select letter_fractienr into v_letter_fractienr
from bas_fractie_types
where ongr_id = p_ongr_id
and   code = upper(v_frty_code)
and   upper(vervallen) = upper('N')
;

v_standaard_waarde := kgc_sypa_00.standaard_waarde
                  (p_parameter_code => 'NUMMERSTRUCTUUR_FRAC',
                   p_kafd_id => p_kafd_id,
                   p_ongr_id => p_ongr_id)
;

select bas_frac_seq.nextval into v_frac_id from dual;

v_fractienummer := kgc_nust_00.genereer_nr(
                    p_nummertype => 'FRAC',
                    p_nummerstructuur => v_standaard_waarde,
                    p_ongr_id => p_ongr_id,
                    p_kafd_id => p_kafd_id,
                    p_waarde1 => v_monsternummer,
                    p_waarde2 => v_letter_fractienr,
                    p_waarde3 => to_char(sysdate,'dd-mm-yyyy hh24:mi:ss')
              )
;

insert into bas_fracties
  (
    frac_id,
    mons_id,
    fractienummer,
    kafd_id,
    frty_id
  )
values
  (
    v_frac_id,
    p_mons_id,
    v_fractienummer,
    p_kafd_id,
    p_frty_id
  );

return v_frac_id;
end voer_frac_in;
--#####################################
function verwerk_pers
(
p_move_id in number,
p_achternaam in varchar2,
p_voorvoegsel in varchar2,
p_achternaam_partner in varchar2,
p_voorvoegsel_partner in varchar2,
p_gehuwd in varchar2,
p_voorletters in varchar2,
p_geboortedatum in varchar2,
p_geslacht in varchar2,
p_adres in varchar2,
p_postcode in varchar2,
p_woonplaats in varchar2,
p_zisnr in varchar2,
p_bsn in varchar2
)
return varchar2
is
v_postcode_bestaat number;
v_woonplaats_bestaat number;
v_zisnr_bestaat number;
v_count_pers number := 0;
v_pers_id kgc_personen.pers_id%type;
v_zisnr kgc_personen.zisnr%type := p_zisnr;
v_log_tekst varchar2(2000);

pers_trow cg$kgc_personen.cg$row_type;
rela_trow cg$kgc_relaties.cg$row_type;
verz_trow cg$kgc_verzekeraars.cg$row_type;

cursor c_pers_zis (p_zisnr_2 in varchar2) is
select *
from   kgc_personen
where  upper(zisnr) = p_zisnr_2
;
r_pers_zis c_pers_zis%rowtype;

cursor c_pers_nav is
select *
from   kgc_personen
where  upper(achternaam) = upper(p_achternaam)
and    geboortedatum = to_date(p_geboortedatum, 'dd-mm-yyyy')
and    upper(geslacht) = upper(p_geslacht)
;

r_pers_nav c_pers_nav%rowtype;

begin

-- achternaam is een verplichte kolomn
if p_achternaam is null then
  v_log_tekst := '1010 - Achternaam is een verplichte kolom.';
end if;
-- geslacht controleren
if p_geslacht is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken || '1020 - Geslacht is een verplichte kolom.';
else
  if upper(p_geslacht) not in ('M', 'V', 'O') then
    v_log_tekst := v_log_tekst || g_scheidingsteken ||
					'1030 - Mogelijke waarden voor geslacht zijn: M, V, O.';
  end if;
end if;
-- gehuwd?
if nvl(upper(p_gehuwd), 'N') not in ('N', 'J') then
    v_log_tekst := v_log_tekst || g_scheidingsteken ||
					'1035 - Mogelijke waarden voor gehuwd zijn: J, N en leeg.';
end if;
-- heeft de ingvoerde datum juiste formaat?
if p_geboortedatum is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken ||
					'1040 - Geboortedatum is een verplichte kolom.';
else
  if  not beh_alfu_00.is_datum_goed(p_geboortedatum) then
    v_log_tekst :=  v_log_tekst || g_scheidingsteken ||
            '1050 - De ingevoerde geboortedatum ('|| p_geboortedatum || ') is niet goed.';
  end if;
end if;

if  not beh_alfu_00.is_nummer_goed(v_zisnr) then
  v_log_tekst :=  v_log_tekst || g_scheidingsteken ||
          '1060 - De ingevoerde ZISnr (' || v_zisnr || ') is niet een numeriek waarde.';
else
	if length(v_zisnr) < 7 then
		v_zisnr := lpad(v_zisnr, 7, '0'); -- min lengte van zisnr is 7 en als het korter is dan aanvullen moet voornullen.
	end if;
end if;

if not beh_alfu_00.is_nummer_goed(p_bsn) then
  v_log_tekst := v_log_tekst || g_scheidingsteken ||
				'1070 - De ingevoerde BSNnr (' || p_bsn || ') is niet een numeriek waarde.';
elsif not is_bsn_goed(to_number(p_bsn)) then
	v_log_tekst := v_log_tekst || g_scheidingsteken ||
				'1080 - De ingevoerde BSNnr (' || p_bsn || ') heeft niet de juiste formaat.';
end if;

if v_log_tekst is not null then
  verwerk_log (v_log_tekst, p_move_id);
  return v_log_tekst;
end if;
--zoeken op basis van zisnr
if v_zisnr is not null then
    open c_pers_zis(v_zisnr);
        fetch c_pers_zis into r_pers_zis;
            -- pers is gevonden nu nav vergelijken
        if c_pers_zis%found then
            if (upper(p_achternaam) = upper(r_pers_zis.achternaam) and
               upper(p_geslacht) = upper(r_pers_zis.geslacht) and
               to_date(p_geboortedatum, 'dd-mm-yyyy') = r_pers_zis.geboortedatum) then
                -- is bsn leeg? dan invoeren
				if (
				  p_bsn is not null and
				  r_pers_zis.bsn is not null and
				  p_bsn <> r_pers_zis.bsn
				  ) then
					v_log_tekst := '1090 - BSNrs van de bestand (' || p_bsn || ') en helix (' ||
								r_pers_zis.bsn ||  ') || wijken af.';
					verwerk_log (v_log_tekst, p_move_id);

					update 	beh_molgen_verwerk
					set 	achternaam_helix = r_pers_zis.achternaam,
							voorletters_helix = r_pers_zis.voorletters,
							geboortedatum_helix = r_pers_zis.geboortedatum,
							geslacht_helix = r_pers_zis.geslacht
					where 	move_id = p_move_id
					;

					return v_log_tekst;
				else
				  return to_char(r_pers_zis.pers_id);
				end if;
            else
				v_log_tekst := '1100 - zisnr (' || v_zisnr || ') is in Helix gevonden maar NAV-gegevens wijken af.';
				verwerk_log (v_log_tekst, p_move_id);

				update 	beh_molgen_verwerk
				set 	achternaam_helix = r_pers_zis.achternaam,
						voorletters_helix = r_pers_zis.voorletters,
						geboortedatum_helix = r_pers_zis.geboortedatum,
						geslacht_helix = r_pers_zis.geslacht
				where 	move_id = p_move_id
				;

				return v_log_tekst;
            end if;
        else
            -- zisnr is wel ingevoerd maar ik kan niks hiermee vinden; dan zoeken op basis van nav
            select count(*) into v_count_pers
            from kgc_personen
            where upper(p_achternaam) = upper(achternaam)
            and upper(p_geslacht) = upper(geslacht)
            and to_date(p_geboortedatum, 'dd-mm-yyyy') = geboortedatum;
                -- 1 persoon gevonden? voer monster in!
            if v_count_pers = 1 then
                for r_pers_nav in c_pers_nav loop
                    if v_zisnr is null then
						if (
						  p_bsn is not null and
						  r_pers_nav.bsn is not null and
						  p_bsn <> r_pers_nav.bsn
						  ) then
							v_log_tekst :=
							  '1110 - BSNrs van de bestand (' || p_bsn || ') en helix (' ||
							  r_pers_nav.bsn ||  ') || wijken af.';
							  verwerk_log (v_log_tekst, p_move_id);

							update 	beh_molgen_verwerk
							set 	achternaam_helix = r_pers_nav.achternaam,
									voorletters_helix = r_pers_nav.voorletters,
									geboortedatum_helix = r_pers_nav.geboortedatum,
									geslacht_helix = r_pers_nav.geslacht
							where 	move_id = p_move_id
							;

							return v_log_tekst;
						else
						  return to_char(r_pers_zis.pers_id);
						end if;
                    else
						v_log_tekst :=
						  '1120 - NAV-gegevens komen overeen maar Helix-persoon heeft geen ZISNr,' ||
						  ' terwijl import-persoon heeft wel een ZISNr.';
						verwerk_log (v_log_tekst, p_move_id);

						update 	beh_molgen_verwerk
						set 	achternaam_helix = r_pers_nav.achternaam,
								voorletters_helix = r_pers_nav.voorletters,
								geboortedatum_helix = r_pers_nav.geboortedatum,
								geslacht_helix = r_pers_nav.geslacht
						where 	move_id = p_move_id
						;

						return v_log_tekst;
                    end if;
				end loop;
                -- meerdere personen gevonden? geef foutmelding
            elsif v_count_pers > 1 then
				v_log_tekst := '1130 - Er zijn meerdere personen met dezelfde NAV-gegevens.';
				verwerk_log (v_log_tekst, p_move_id);
				return v_log_tekst;
            else
          -- niks gevonden? haal zis gegevens op
                begin
                    kgc_interface.persoon_in_zis
                    ( p_zisnr => v_zisnr
                    , pers_trow => pers_trow
                    , rela_trow => rela_trow
                    , verz_trow => verz_trow
                    );
                        if (upper(p_achternaam) = upper(pers_trow.achternaam)
                        and upper(p_geslacht) = upper(pers_trow.geslacht)
                        and to_date(p_geboortedatum, 'dd-mm-yyyy') = to_date(substr(to_char(pers_trow.geboortedatum),1, 10), 'dd-mm-yyyy') ) then
                            if (
                                p_bsn is not null and
                                pers_trow.bsn is not null and
                                pers_trow.bsn <> p_bsn
                                ) then
                                    v_log_tekst :=
										'1140 - zisnr bestaat in zis-db maar bsnnrs van de bestand (' ||
										p_bsn || ') en zis-db (' || pers_trow.bsn || ') wijken af.';
                                    verwerk_log (v_log_tekst, p_move_id);

									update 	beh_molgen_verwerk
									set 	achternaam_helix = pers_trow.achternaam,
											voorletters_helix = pers_trow.voorletters,
											geboortedatum_helix = to_date(substr(pers_trow.geboortedatum, 1, 10), 'dd-mm-yyyy'),
											geslacht_helix = pers_trow.geslacht
									where 	move_id = p_move_id
									;

									return v_log_tekst;
                            else
                                v_pers_id := voer_pers_in
                                    (
                                        p_achternaam => pers_trow.achternaam,
                                        p_voorvoegsel => pers_trow.voorvoegsel,
                                        p_achternaam_partner => pers_trow.achternaam_partner,
                                        p_voorvoegsel_partner => pers_trow.voorvoegsel_partner,
                                        p_gehuwd => nvl(pers_trow.gehuwd, 'N'),
                                        p_voorletters => pers_trow.voorletters,
                                        p_geboortedatum => substr(to_char(pers_trow.geboortedatum),1, 10),
                                        p_geslacht => pers_trow.geslacht,
                                        p_adres => pers_trow.adres,
                                        p_postcode => pers_trow.postcode,
                                        p_woonplaats => pers_trow.woonplaats,
                                        p_zisnr => pers_trow.zisnr,
                                        p_bsn => to_char(pers_trow.bsn),
										p_beheer_in_zis => 'J'
                                    );
                                return to_char(v_pers_id);
                            end if;
                        else
                            v_log_tekst := '1150 - ZISnrs (' || pers_trow.zisnr ||
										') komen overeen maar NAV-gegevens wijken af.';
                            verwerk_log (v_log_tekst, p_move_id);

							update 	beh_molgen_verwerk
							set 	achternaam_helix = pers_trow.achternaam,
									voorletters_helix = pers_trow.voorletters,
                                    geboortedatum_helix = to_date(substr(pers_trow.geboortedatum, 1, 10), 'dd-mm-yyyy'),
                                    geslacht_helix = upper(pers_trow.geslacht)
							where 	move_id = p_move_id
							;

							return v_log_tekst;
                        end if;
                        exception
                            when others then
                                v_log_tekst := to_char(sqlcode) || sqlerrm;
                                if instr(v_log_tekst, 'CG$ERRORS') > 0 then -- detailfout ophalen
                                    v_log_tekst := cg$errors.GetErrors;
                                end if;
								verwerk_log ('1160 - ' || v_log_tekst, p_move_id);
					return v_log_tekst;
				end;
            end if;
        end if;
    close c_pers_zis;
else
    -- zisnr is niet ingevoerd? zoek op basis van NAV
    select count(*) into v_count_pers
    from kgc_personen
    where upper(p_achternaam) = upper(achternaam)
    and upper(p_geslacht) = upper(geslacht)
    and to_date(p_geboortedatum, 'dd-mm-yyyy') = geboortedatum
	;

    if v_count_pers = 1 then
        for r_pers_nav in c_pers_nav loop
		  if (
			p_bsn is not null and
			r_pers_nav.bsn is not null and
			p_bsn <> r_pers_nav.bsn
			) then
			  v_log_tekst := '1170 - NAV-gegevens komen overeen maar BSNrs van de bestand (' ||
					  p_bsn || ') en helix (' || r_pers_nav.bsn ||  ') || wijken af.';
				verwerk_log (v_log_tekst, p_move_id);

				update 	beh_molgen_verwerk
				set 	achternaam_helix = r_pers_nav.achternaam,
				voorletters_helix = r_pers_nav.voorletters,
				geboortedatum_helix = r_pers_nav.geboortedatum,
				geslacht_helix = r_pers_nav.geslacht
				where 	move_id = p_move_id
				;

			  return v_log_tekst;
		  else
			return to_char(r_pers_nav.pers_id);
		  end if;
        end loop;
    elsif v_count_pers > 1 then
		  v_log_tekst := '1180 - Er zijn meerdere personen met dezelfde NAV-gegevens.';
		  verwerk_log (v_log_tekst, p_move_id);
		  return v_log_tekst;
    else
        v_pers_id := voer_pers_in
            (
                p_achternaam => p_achternaam,
				p_voorvoegsel => p_voorvoegsel,
				p_achternaam_partner => p_achternaam_partner,
				p_voorvoegsel_partner => p_voorvoegsel_partner,
				p_gehuwd => p_gehuwd,
				p_voorletters => p_voorletters,
				p_geboortedatum => p_geboortedatum,
				p_geslacht => p_geslacht,
				p_adres => p_adres,
				p_postcode => p_postcode,
				p_woonplaats => p_woonplaats,
				p_zisnr => v_zisnr,
				p_bsn => p_bsn,
				p_beheer_in_zis => 'N'
            );
        return to_char(v_pers_id);
    end if;
end if;
end verwerk_pers;

function verwerk_mons
(
p_move_id in number,
p_pers_id in kgc_personen.pers_id%type,
p_ongr_code in varchar2,
p_kafd_code in varchar2,
p_mate_code in varchar2,
p_nader_gebruiken in varchar2,
p_aantal_buizen in varchar2,
p_datum_afname in varchar2,
p_commentaar in varchar2,
p_forceer_mons in varchar2,
p_herk_code in varchar2
)
return varchar2
is
v_ongr_id kgc_onderzoeksgroepen.ongr_id%type;
v_kafd_id kgc_kgc_afdelingen.kafd_id%type;
v_mate_id kgc_materialen.mate_id%type;
v_count_mons number := 0;
v_mons_id kgc_monsters.mons_id%type;
v_log_tekst varchar2(2000);
v_geboortedatum kgc_personen.geboortedatum%type;
v_herk_id kgc_herkomsten.herk_id%type;

cursor c_kafd is
select *
from kgc_kgc_afdelingen
where upper(code) = upper(p_kafd_code)
and upper(naam) not like upper('%VERVALLEN%')
;
r_kafd c_kafd%rowtype;

cursor c_ongr is
select *
from kgc_onderzoeksgroepen
where upper(code) = upper(p_ongr_code)
and   kafd_id = ( 	select kafd_id
					from kgc_kgc_afdelingen
					where upper(code) = upper(p_kafd_code))
and upper(vervallen) = upper('N')
;
r_ongr c_ongr%rowtype;

cursor c_mate is
select *
from kgc_materialen
where upper(code) = upper(p_mate_code)
and   kafd_id = ( 	select kafd_id
					from kgc_kgc_afdelingen
					where upper(code) = upper(p_kafd_code))
and upper(vervallen) = upper('N')
;
r_mate c_mate%rowtype;

cursor c_herk is
select *
from kgc_herkomsten
where upper(code) = upper(p_herk_code)
and upper(vervallen) = upper('N')
;
r_herk c_herk%rowtype;

begin

if p_kafd_code is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken || '2010 - kafd_code is een verplichte kolom.';
else
    open c_kafd;
        fetch c_kafd into r_kafd;
        if c_kafd%found then
			v_kafd_id := r_kafd.kafd_id;
		else
			v_log_tekst := v_log_tekst || g_scheidingsteken ||
				'2020 - kafd_code (' || p_kafd_code || ') bestaat in Helix niet.';
		end if;
	close c_kafd;
end if;

if p_ongr_code is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken || '2030 - ongr_code is een verplichte kolom.';
else
    open c_ongr;
        fetch c_ongr into r_ongr;
        if c_ongr%found then
			v_ongr_id := r_ongr.ongr_id;
		else
			v_log_tekst := v_log_tekst || g_scheidingsteken ||
					'2040 - ongr_code (' || p_ongr_code || ') bestaat in Helix niet.';
		end if;
	close c_ongr;
end if;

if p_mate_code is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken || '2050 - mate_code is een verplichte kolom.';
else
    open c_mate;
        fetch c_mate into r_mate;
        if c_mate%found then
			v_mate_id := r_mate.mate_id;
		else
			v_log_tekst := v_log_tekst || g_scheidingsteken ||
				'2060 - mate_code (' || p_mate_code || ') bestaat in Helix niet.';
		end if;
	close c_mate;
end if;

if upper(p_nader_gebruiken) not in ('J', 'N') then
	v_log_tekst := v_log_tekst || g_scheidingsteken ||
				'2070 - Mogelijke waarden voor nader_gebruiken zijn: J, N.';
end if;

if not beh_alfu_00.is_nummer_goed(p_aantal_buizen) then
	v_log_tekst := v_log_tekst || g_scheidingsteken ||
				'2080 - De ingevoerde aantal buizen (' || p_aantal_buizen || ') niet een numerieke waarde.';
end if;

if p_datum_afname is not null then
    -- is datum formaat goed?
    if beh_alfu_00.is_datum_goed(p_datum_afname) then
        select geboortedatum into v_geboortedatum
        from kgc_personen
        where pers_id = p_pers_id
        ;
        -- datum_afname mag niet kleiner dan geboortedatum zijn.
        if to_date(p_datum_afname, 'dd-mm-yyyy') < v_geboortedatum then
            v_log_tekst := v_log_tekst || g_scheidingsteken ||
                       '2090 - datum_afname (' || p_datum_afname ||
                       ') is kleiner dan geboortedatum (' ||
                       to_char(v_geboortedatum, 'dd-mm-yyyy') || ')';
        end if;
    else
        v_log_tekst := v_log_tekst || g_scheidingsteken ||
                        '2095 - De ingevoerde afname datum (' || p_datum_afname || ') is niet goed.';
    end if;
end if;

if v_log_tekst is not null then
    verwerk_log (v_log_tekst, p_move_id);
	return v_log_tekst;
end if;

if p_herk_code is not null then
    open c_herk;
        fetch c_herk into r_herk;
        if c_herk%found then
			v_herk_id := r_herk.herk_id;
		else
			v_log_tekst := v_log_tekst || g_scheidingsteken ||
				'2096 - Waarschuwing:herkomst (' || p_herk_code || ') bestaat in Helix niet.';
           		verwerk_log (v_log_tekst, p_move_id);
		end if;
	close c_herk;
end if;

select count(*) into v_count_mons
from kgc_monsters
where pers_id = p_pers_id
and   kafd_id = v_kafd_id
and   ongr_id = v_ongr_id
and   mate_id = v_mate_id
and   datum_afname = to_date(p_datum_afname, 'dd-mm-yyyy');

if v_count_mons > 0 then
	if upper(p_forceer_mons) = 'J' then
		v_mons_id := voer_mons_in(
					v_ongr_id,
					v_kafd_id,
					p_pers_id,
					v_mate_id,
					p_nader_gebruiken,
					p_aantal_buizen,
					p_datum_afname,
					p_commentaar,
                    v_herk_id
					);

		return to_char(v_mons_id);
	else
		v_log_tekst := '2100 - monster bestaat al.';
		verwerk_log (v_log_tekst, p_move_id);
		return v_log_tekst;
	end if;
else
	v_mons_id := voer_mons_in(
                v_ongr_id,
                v_kafd_id,
                p_pers_id,
                v_mate_id,
                p_nader_gebruiken,
                p_aantal_buizen,
                p_datum_afname,
                p_commentaar,
                v_herk_id
               );
    return to_char(v_mons_id);
end if;

end verwerk_mons;
--##########################################
function verwerk_frac
(
p_move_id in number,
p_mons_id in kgc_monsters.mons_id%type,
p_kafd_code in varchar2,
p_ongr_code in varchar2,
p_frty_code in varchar2,
p_forceer_frac in varchar2
)
return varchar2
is
v_frty_id bas_fractie_types.frty_id%type;
v_count_frac number := 0;
v_frac_id bas_fracties.frac_id%type;
v_kafd_id kgc_kgc_afdelingen.kafd_id%type;
v_ongr_id kgc_onderzoeksgroepen.ongr_id%type;
v_log_tekst varchar2(2000);

cursor c_kafd is
select *
from kgc_kgc_afdelingen
where upper(code) = upper(p_kafd_code)
;
r_kafd c_kafd%rowtype;

cursor c_ongr is
select *
from kgc_onderzoeksgroepen
where upper(code) = upper(p_ongr_code)
and   kafd_id = ( 	select kafd_id
					from kgc_kgc_afdelingen
					where upper(code) = upper(p_kafd_code))
and upper(vervallen) = upper('N')
;
r_ongr c_ongr%rowtype;

cursor c_frty (p_ongr_id_2 in number) is
select *
from bas_fractie_types
where upper(code) = upper(p_frty_code)
and   ongr_id = p_ongr_id_2
and   upper(vervallen) = upper('N')
;
r_frty c_frty%rowtype;


begin
if p_kafd_code is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken || '3010 - kafd_code is een verplichte kolom.';
else
    open c_kafd;
        fetch c_kafd into r_kafd;
        if c_kafd%found then
			v_kafd_id := r_kafd.kafd_id;
		else
			v_log_tekst := v_log_tekst || g_scheidingsteken ||
					'3020 - kafd_code (' || p_kafd_code || ') bestaat in Helix niet.';
		end if;
	close c_kafd;
end if;

if p_ongr_code is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken || '3030 - ongr_code is een verplichte kolom.';
else
    open c_ongr;
        fetch c_ongr into r_ongr;
        if c_ongr%found then
			v_ongr_id := r_ongr.ongr_id;
		else
			v_log_tekst := v_log_tekst || g_scheidingsteken || '3040 - ongr_code (' || p_ongr_code || ') bestaat in Helix niet.';
		end if;
	close c_ongr;
end if;

if p_frty_code is null then
  v_log_tekst := v_log_tekst || g_scheidingsteken || '3050 - frty_code is een verplichte kolom.';
else
    open c_frty(v_ongr_id);
        fetch c_frty into r_frty;
        if c_frty%found then
			v_frty_id := r_frty.frty_id;
		else
			v_log_tekst := v_log_tekst || g_scheidingsteken ||
					'3060 - frty_code (' || p_frty_code || ') bestaat in Helix niet.';
		end if;
	close c_frty;
end if;
if v_log_tekst is not null then
  verwerk_log (v_log_tekst, p_move_id);
  return v_log_tekst;

end if;

select count(*) into v_count_frac
from bas_fracties
where kafd_id = v_kafd_id
and   mons_id = p_mons_id
and   frty_id = v_frty_id;

if v_count_frac > 0 then
  if p_forceer_frac = 'J' then
    v_frac_id := voer_frac_in (
                  p_mons_id,
                  v_kafd_id,
                  v_ongr_id,
                  v_frty_id
                  );
    return to_char(v_frac_id);
  else
    v_log_tekst := '3070 - fractie bestaat al.';
    verwerk_log (v_log_tekst, p_move_id);

    return v_log_tekst;
  end if;
else
  v_frac_id := voer_frac_in (
                p_mons_id,
                v_kafd_id,
                v_ongr_id,
                v_frty_id
                );
  return to_char(v_frac_id);
end if;
end verwerk_frac;
--#####################################
procedure start_verwerking is

cursor c_ingelezen_gegevens is
select  trim(kolom01) achternaam,
		trim(kolom02) voorvoegsel,
		trim(kolom03) achternaam_partner,
		trim(kolom04) voorvoegsel_partner,
		nvl(trim(kolom05), 'N') gehuwd,
		trim(kolom06) voorletters,
		trim(kolom07) geboortedatum,
		trim(kolom08) geslacht,
        trim(kolom09) adres,
		trim(kolom10) postcode,
        trim(kolom11) woonplaats,
		trim(kolom12) zisnr,
		trim(kolom13) bsn,
		trim(kolom14) ongr_code,
		trim(kolom15) kafd_code,
		trim(kolom16) mate_code,
		nvl(trim(kolom17), 'N') nader_gebruiken,
		trim(kolom18) aantal_buizen,
		trim(kolom19) datum_afname,
        trim(kolom20) commentaar,
		trim(kolom21) frty_code,
		trim(kolom22) inlezen_in_helix,
		trim(kolom23) forceer_pers,
		trim(kolom24) forceer_mons,
		trim(kolom25) forceer_frac,
		trim(kolom26) herk_code
from beh_molgen_inlees
where upper(trim(kolom22)) = 'J' --inlezen in helix = j
and   upper(trim(kolom01)) not like (upper('%achternaam%')) -- eerste rij van de bestand
order by kolom30 -- seqence en volgorde
;
r_ingelezen_gegevens c_ingelezen_gegevens%rowtype;

cursor c_mede is
select mede_id
from kgc_medewerkers
where upper(oracle_uid) = upper(user)
;

r_mede c_mede%rowtype;

cursor c_pers (p_pers_id_2 number) is
select *
from kgc_personen
where pers_id = p_pers_id_2
;

r_pers c_pers%rowtype;

cursor c_mons (p_mons_id_2 number) is
select *
from kgc_monsters
where mons_id = p_mons_id_2
;
r_mons c_mons%rowtype;

cursor c_frac (p_frac_id_2 number) is
select *
from bas_fracties
where frac_id = p_frac_id_2
;
r_frac c_frac%rowtype;

v_move_id kgc_pc_applicaties.pcap_id%type;
v_pers_id varchar2(2000);
v_mons_id varchar2(2000);
v_frac_id varchar2(2000);
v_log_tekst varchar2(2000);
v_zoek_sleutel varchar2(100);
v_mede_id kgc_medewerkers.mede_id%type;
v_datum_inlees date := sysdate;

v_afzender varchar2(100);
v_ontvanger varchar2(500);
v_boodschap  varchar2(2000);
v_newline varchar2(10) := chr(10);
v_db_naam varchar2(10);
v_titel varchar2(100);

begin
v_zoek_sleutel := upper(user) || '_' || to_char(sysdate, 'ddmmyyyyhh24miss');
-- wie leest de bestand in?
for r_mede in c_mede loop
	v_mede_id := r_mede.mede_id;
end loop;

for r_ingelezen_gegevens in c_ingelezen_gegevens loop
  -- log record in beh_molgen_verwerk
  select beh_move_seq.nextval into v_move_id from dual;

  insert into beh_molgen_verwerk
  (
    move_id,
    zoek_sleutel,
    datum_inlees,
    mede_id,
	achternaam_inlees,
	voorletters_inlees,
	geboortedatum_inlees,
	geslacht_inlees,
    inlezen_in_helix,
    forceer_pers,
    forceer_mons,
    forceer_frac
   )
  values
  (
    v_move_id,
    v_zoek_sleutel,
    v_datum_inlees,
    v_mede_id,
    r_ingelezen_gegevens.achternaam,
    r_ingelezen_gegevens.voorletters,
    r_ingelezen_gegevens.geboortedatum,
    r_ingelezen_gegevens.geslacht,
    r_ingelezen_gegevens.inlezen_in_helix,
    r_ingelezen_gegevens.forceer_pers,
    r_ingelezen_gegevens.forceer_mons,
    r_ingelezen_gegevens.forceer_frac
  );

  v_pers_id := verwerk_pers(
                v_move_id,
                r_ingelezen_gegevens.achternaam,
                r_ingelezen_gegevens.voorvoegsel,
                r_ingelezen_gegevens.achternaam_partner,
                r_ingelezen_gegevens.voorvoegsel_partner,
                r_ingelezen_gegevens.gehuwd,
                r_ingelezen_gegevens.voorletters,
                r_ingelezen_gegevens.geboortedatum,
                r_ingelezen_gegevens.geslacht,
                r_ingelezen_gegevens.adres,
                r_ingelezen_gegevens.postcode,
                r_ingelezen_gegevens.woonplaats,
                r_ingelezen_gegevens.zisnr,
                r_ingelezen_gegevens.bsn
              );

  if beh_alfu_00.is_nummer_goed(v_pers_id) then
    v_log_tekst := '1000 - pers_id: ' || v_pers_id || ' is gevonden/ingevoerd.';
    verwerk_log (v_log_tekst, v_move_id);

    for r_pers in c_pers(to_number(v_pers_id)) loop
      update beh_molgen_verwerk
      set pers_id = r_pers.pers_id,
          achternaam_helix = r_pers.achternaam,
          voorvoegsel = r_pers.voorvoegsel,
          achternaam_partner = r_pers.achternaam_partner,
          voorvoegsel_partner = r_pers.voorvoegsel_partner,
          voorletters_helix = r_pers.voorletters,
          geboortedatum_helix = r_pers.geboortedatum,
          geslacht_helix = r_pers.geslacht,
          adres = r_pers.adres,
          postcode = r_pers.postcode,
          woonplaats = r_pers.woonplaats,
          zisnr = r_pers.zisnr,
          bsn = r_pers.bsn
      where move_id = v_move_id
      ;
    end loop;

    v_mons_id := verwerk_mons(
                  v_move_id,
                  to_number(trim(v_pers_id)),
                  r_ingelezen_gegevens.ongr_code,
                  r_ingelezen_gegevens.kafd_code,
                  r_ingelezen_gegevens.mate_code,
                  r_ingelezen_gegevens.nader_gebruiken,
                  r_ingelezen_gegevens.aantal_buizen,
                  r_ingelezen_gegevens.datum_afname,
                  r_ingelezen_gegevens.commentaar,
                  r_ingelezen_gegevens.forceer_mons,
                  r_ingelezen_gegevens.herk_code
                );
    if beh_alfu_00.is_nummer_goed(v_mons_id) then
      v_log_tekst := '2000 - mons_id: ' || v_mons_id || ' is ingevoerd.';
      verwerk_log (v_log_tekst, v_move_id);

      for r_mons in c_mons(to_number(v_mons_id)) loop
      update beh_molgen_verwerk
      set mons_id = r_mons.mons_id,
          monsternummer = r_mons.monsternummer,
          ongr_id = r_mons.ongr_id,
          kafd_id = r_mons.kafd_id,
          mate_id = r_mons.mate_id,
          nader_gebruiken = r_mons.nader_gebruiken,
          aantal_buizen = r_mons.aantal_buizen,
          datum_afname = r_mons.datum_afname,
          commentaar = r_mons.commentaar,
          herk_id = r_mons.herk_id
      where move_id = v_move_id
      ;
    end loop;

      v_frac_id := verwerk_frac(
                    v_move_id,
                    to_number(trim(v_mons_id)),
                    r_ingelezen_gegevens.kafd_code,
                    r_ingelezen_gegevens.ongr_code,
                    r_ingelezen_gegevens.frty_code,
                    r_ingelezen_gegevens.forceer_frac
                  );

      if beh_alfu_00.is_nummer_goed(v_frac_id) then
        v_log_tekst := '3000 - frac_id: ' || v_frac_id || ' is ingevoerd.';
        verwerk_log (v_log_tekst, v_move_id);

        for r_frac in c_frac(to_number(v_frac_id)) loop
          update beh_molgen_verwerk
          set frac_id = r_frac.frac_id,
              fractienummer = r_frac.fractienummer,
              frty_id = r_frac.frty_id
          where move_id = v_move_id
          ;
        end loop;
      else
        v_log_tekst := '3999 - frac is niet verwerkt.';
        verwerk_log (v_log_tekst, v_move_id);
      end if;
    else
      v_log_tekst := '2999 - mons is niet verwerkt.';
      verwerk_log (v_log_tekst, v_move_id);
    end if;
  else
    v_log_tekst := '1999 - pers is niet verwerkt.';
    verwerk_log (v_log_tekst, v_move_id);
  end if;

end loop;
commit;
-- versturen van raportage email
select sys_context ('USERENV', 'DB_NAME') into v_db_naam from dual
;

v_titel := v_zoek_sleutel || ' Molgen controle sleutel';

select 	upper(trim(spwa.waarde)) into v_afzender
from 	kgc_systeem_parameters sypa, kgc_systeem_par_waarden spwa
where 	sypa.sypa_id = spwa.sypa_id
and   	upper(sypa.code) = upper('BEH_MIJN_HELIX_EMAIL_ADRES')
and 	spwa.mede_id  in (	select mede_id
							from kgc_medewerkers
							where upper(oracle_uid) = upper('HELIX')
						 )
;

select 	upper(trim(spwa.waarde)) into v_ontvanger
from 	kgc_systeem_parameters sypa, kgc_systeem_par_waarden spwa
where 	sypa.sypa_id = spwa.sypa_id
and   	upper(sypa.code) = upper('BEH_MIJN_HELIX_EMAIL_ADRES')
and 	spwa.mede_id  in (	select mede_id
							from kgc_medewerkers
                            where upper(oracle_uid) = upper(user)
						 )
;

v_boodschap :=  'Beste ' || user
        || v_newline
        || v_newline
        || 'Op: ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
        || ' heb je een bestand in Helix ' || v_db_naam || ' ingelezen.'
        || v_newline
        || v_newline
        || ' Je sleutel is:' || v_zoek_sleutel
        || v_newline
        || v_newline
        || 'Voor vragen en opmerkingen kun je contact opnemen met '
        || 'Helix systeembeheer.';

UTL_MAIL.SEND(  sender => v_afzender,
                recipients => v_ontvanger, -- komma gescheiden
                cc => v_afzender,  -- komma gescheiden
                bcc => null,
                subject => v_titel,
                message => v_boodschap);

end start_verwerking;

end beh_molgen_00;
/

/
QUIT
