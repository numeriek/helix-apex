CREATE OR REPLACE PACKAGE "HELIX"."BEH_DNA_VOORTEST_OPN_AANMELDEN" IS

/***********************************************************************
Auteur/bedrijf  : Yuri Arts, YA IT-Services
Datum creatie   : 25-02-2012

BESCHRIJVING
Start het opnieuw aanmelden van stoftesten zodat ze nog een keer op een werklijst
gezet worden.

Als basis is de package BEH_KGC_WERKLIJST_TRAY_ETIKET genomen van 08-07-2009

***********************************************************************/


GV_REVISIE_VERSIE CONSTANT VARCHAR2(10) := '1.0.5';

/* revisie historie */
FUNCTION REVISION
 RETURN VARCHAR2;

PROCEDURE START_ROBOT_STOFTEST_AANMELDEN
 (P_AFDELING   IN VARCHAR2 DEFAULT 'GENOOM'
 ,P_EMAIL      IN VARCHAR2 DEFAULT 'HelpdeskHelixCUKZ@cukz.umcn.nl'
 );

END BEH_DNA_VOORTEST_OPN_AANMELDEN;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DNA_VOORTEST_OPN_AANMELDEN" IS

v_inhoud       varchar2(32766);
c_nieuwe_regel varchar2(10) := chr(10);

/* revisie historie */
FUNCTION REVISION
 RETURN VARCHAR2
 IS
/***********************************************************************
Auteur/bedrijf  : Yuri Arts, YA IT-Services
Datum creatie   : 25-02-2012

BESCHRIJVING
Start het opnieuw aanmelden van stoftesten zodat ze nog een keer op een werklijst
gezet worden.

HISTORIE

Wanneer /      Wie                      Wat
Versie
------------------------------------------------------------------------
06-06-2012     Y.Arts          De uitvoertekst, dat in de mail gezet wordt, moet gecontroleerd worden op 32766 posities.
1.0.5                          Als het toe te voegen tekst ervoor zorgt dat het aantal hier overheen gaat, mag het niet toegevoegd worden.
                               Dit is om fouten te voorkomen.
25-02-2012     Y.Arts /        Procedure losgemaakt van package BEH_KGC_WERKLIJST_TRAY_ETIKET
1.0.4          M.Golebioswska
06-12-2011     Y.Arts          meetwaarde details wordt de TEST_OK? vlag op N gezet bij afronden
1.0.3
14-07-2011     Y.Arts          1. Rekening houden met afdeling bij de selecties van meetwaarden
1.0.2                          2. Na x keer proberen niet afkeuren maar aanmelden bij manueel protocol
                               3. Default 'GENOOM' meegeven aan procedure
27-03-2011     Y.Arts          Controles en email ingebouwd, het kan voorkomen dat een pcr_meetid in de sequencefacility tabel leeg is.
1.0.1
07-12-2010     Y.Arts          Creatie
1.0.0
***********************************************************************/

BEGIN
  RETURN gv_revisie_versie;
END;

PROCEDURE gebruikers_info
 (P_TEKST IN VARCHAR2)
IS
BEGIN
  if (nvl(length(v_inhoud),0) + nvl(length(p_tekst),0) < 32766) then
    v_inhoud := v_inhoud||p_tekst||c_nieuwe_regel;
  end if;
END;


PROCEDURE start_robot_stoftest_aanmelden
 (P_AFDELING   IN VARCHAR2 DEFAULT 'GENOOM'
 ,P_EMAIL      IN VARCHAR2 DEFAULT 'HelpdeskHelixCUKZ@cukz.umcn.nl'
)
 IS
/***********************************************************************
Auteur/bedrijf  : Y.Arts  YA IT-Services
Datum onderhoud : 07-12-2010

DOEL
Start het opnieuw aanmelden van stoftesten zodat ze nog een keer op een werklijst
gezet worden.

1.  Haal de afdelings id op
2.  Ophalen van systeemparameter dat bepaald hoevaak een stoftest opnieuw aangemald mag worden
3.  Bepaal welke metingen niet goed zijn in de postrobot tabel: testok = Nee en helix_verwerkt = Nee
4.  haal de stoftest en onderzoek op van de betreffende meet_id
5.  bepaal hoevaak de stoftest voor het onderzoek is aangemeld
6.  Als de stoftest vaker of net zo vaak is aangemeld als de systeemparameter aangeeft, dan mag het niet opnieuw
6.1.  Als vaker dan x keer aangemeld is moet de stoftest aangemeld worden bij de manuele protocol
6.2.  Als de stoftest niet opnieuw kon worden aangemeld dat geef daarvan een melding
6.3.  Postrobot tabel updaten: helix_verwerkt = Ja
7.  Opnieuw aanmelden stoftest
8.  Als de stoftest niet opnieuw kon worden aangemeld dat geef daarvan een melding
9.  Postrobot tabel updaten: helix_verwerkt = Ja
10. Bepaal welke metingen GOED zijn in de postrobot tabel: testok = Ja en helix_verwerkt = Nee
11. haal de stoftest en onderzoek op van de betreffende meet_id
12  afronden alleen als het nog niet afgerond is
13. Postrobot tabel updaten: helix_verwerkt = Ja

INPUT PARAMETERS
P_AFDELING : De afdeling waarvoor de gegevens uit de Robot gehaald moeten worden:  GENOOM

OUTPUT PARAMETERS

Wanneer/       Wie             Wat
Versie
------------------------------------------------------------------------
06-06-2012     Y.Arts          Extra controles toegevoegd zodat een meetwaarde detail of meetwaarde niet op afgerond gezet kan worden
1.0.5                          als het onderzoek al afgerond is. Het zou niet mogen voorkomen.
06-06-2012     Y.Arts          De uitvoertekst, dat in de mail gezet wordt, moet gecontroleerd worden op 32766 posities.
1.0.5                          Als het toe te voegen tekst ervoor zorgt dat het aantal hier overheen gaat, mag het niet toegevoegd worden.
                               Dit is om fouten te voorkomen.
25-02-2012     Y.Arts /        Procedure losgemaakt van package BEH_KGC_WERKLIJST_TRAY_ETIKET
1.0.4          M.Golebioswska
06-12-2011     Y.Arts          meetwaarde details wordt de TEST_OK? vlag op N gezet bij afronden
1.0.3
14-07-2011     Y.Arts          1. Rekening houden met afdeling bij de selecties van meetwaarden
1.0.2                          2. Na x keer proberen niet afkeuren maar aanmelden bij manueel protocol
                               3. Default 'GENOOM' meegeven aan procedure
27-03-2011     Y.Arts          Controles en email ingebouwd, het kan voorkomen dat een pcr_meetid in de sequencefacility tabel leeg is.
1.0.1
07-12-2010     Y.Arts          Creatie
1.0.0
***********************************************************************/


   --1. Haal de afdelings id op
   cursor kafd_cur (b_afdeling in varchar2)
   is
   select kafd_id
   from   kgc_kgc_afdelingen
   where  code = b_afdeling
   ;

   cursor mede_cur (b_code in varchar2)
   is
   select mede_id
   from kgc_medewerkers
   where code = b_code
   ;

   --3. Bepaal welke metingen niet goed zijn in de postrobot tabel: testok = Nee en helix_verwerkt = Nee
   --   Houdt rekening met de gegevens voor de meegegeven afdeling
   cursor prbt_cur_nok (b_kafd_id in number)
   is

   select pcrplatebarcode
   ,      meetid
   ,      testok
   ,      helix_verwerkt
   ,      dna_id
   from   postrobot@storage prbt
   where  testok = 'N'
   and    helix_verwerkt = 'N'
   and    (meetid in ( select meet.meet_id
                       from   bas_meetwaarden meet
                       ,      bas_metingen    meti
                       ,      kgc_onderzoeken onde
                       ,      kgc_stoftesten  stof
                       where  meet.meti_id = meti.meti_id
                       and    meti.onde_id = onde.onde_id
                       and    meet.stof_id = stof.stof_id
                       and    onde.kafd_id = b_kafd_id
                     )
           or meetid is null
          )
   ;

   --3. Bepaal welke metingen GOED zijn in de postrobot tabel: testok = Ja en helix_verwerkt = Nee
   cursor prbt_cur_ok (b_kafd_id in number)
   is

   select pcrplatebarcode
   ,      meetid
   ,      testok
   ,      helix_verwerkt
   ,      dna_id
   from   postrobot@storage prbt
   where  testok = 'J'
   and    helix_verwerkt = 'N'
   and    meetid in ( select meet.meet_id
                      from   bas_meetwaarden meet
                      ,      bas_metingen    meti
                      ,      kgc_onderzoeken onde
                      ,      kgc_stoftesten  stof
                      where  meet.meti_id = meti.meti_id
                      and    meti.onde_id = onde.onde_id
                      and    meet.stof_id = stof.stof_id
                      and    onde.kafd_id = b_kafd_id
                    )
   ;

   --4. haal de stoftest en onderzoek op van de betreffende meet_id
   cursor meet_stof_cur (b_meet_id in number)
   is

   select meet.stof_id      stof_id
   ,      meti.onde_id      onde_id
   ,      meti.frac_id      frac_id
   ,      meti.meti_id      meti_id
   ,      onde.onderzoeknr  onderzoeknr
   ,      stof.omschrijving stof_omschrijving
   ,      stgr.code         stgr_code
   from   bas_meetwaarden meet
   ,      bas_metingen    meti
   ,      kgc_onderzoeken onde
   ,      kgc_stoftesten  stof
   ,      kgc_stoftestgroepen stgr
   where  meet.meet_id = b_meet_id
   and    meet.meti_id = meti.meti_id
   and    meti.onde_id = onde.onde_id
   and    meet.stof_id = stof.stof_id
   and    meti.stgr_id = stgr.stgr_id(+)
   and    onde.afgerond = 'N'
   ;

   --5. bepaal hoevaak de stoftest voor het onderzoek is aangemeld
   cursor stof_cur(b_stof_id in number
                  ,b_onde_id in number)
   is

   select count(1)
   from   bas_metingen    meti
   ,      bas_meetwaarden meet
   where  meet.stof_id  = b_stof_id
   and    meet.meti_id  = meti.meti_id
   and    meti.onde_id  = b_onde_id
   ;


   cursor stgr_manueel_cur ( b_stgr_code in varchar2 )
   is

   select stgr_id, code
   from   kgc_stoftestgroepen
   where  code = substr(b_stgr_code,1,instr(b_stgr_code,'_')-1)||'_M'
   ;


   cursor meti_cur (b_onde_id in number
                   ,b_stgr_id in number)
   is
   select meti_id
   from bas_metingen
   where afgerond = 'N'
   and   onde_id  = b_onde_id
   and   stgr_id  = b_stgr_id
   ;


  v_kafd_id           kgc_kgc_afdelingen.kafd_id%type;
  v_wlst_id           number;
  v_stof_aantal       number;
  v_stof_aantal_sypa  number;
  v_stof_id           number;
  v_onde_id           number;
  v_onderzoeknr       kgc_onderzoeken.onderzoeknr%type;
  v_stof_omschrijving kgc_stoftesten.omschrijving%type;
  v_frac_id           number;
  v_melding           varchar2(4000);
  v_meti_id           number;
  v_stgr_id_m         number;
  v_stgr_code_m       varchar2(100);
  v_meti_id_m         number;
  v_stgr_code         varchar2(100);

  v_mede_id          kgc_medewerkers.mede_id%type;
  v_fout             boolean;

  -- email
  v_onderwerp    varchar2(1000) := 'Robot-Stoftest-Aanmelden';
  v_inhoud       varchar2(32767);
  v_nieuwe_regel varchar2(10) := chr(10);


  -- controleer of er nog een niet-afgeronde meetwaarde is
  function niet_afgerond (p_meti_id in number
                         ,p_stof_id in number)
  return boolean
  is
    v_return boolean ;
    cursor meet_cur
    is
      select null
      from   bas_meetwaarden
      where  meti_id = p_meti_id
      and    stof_id = p_stof_id
      and    afgerond = 'N'
      ;
    v_dummy varchar2(1);
  begin
    open  meet_cur;
    fetch meet_cur into v_dummy;
    v_return := meet_cur%found;
    close meet_cur;
    return( v_return );
  end niet_afgerond;


  procedure stoftest_opnieuw_aanmelden
  ( p_meti_id in number
  , p_stof_id in number
  , p_frac_id in number
  , p_onde_id in number
  , x_melding out varchar2
  , x_fout    out boolean
  )
  is
    v_fout boolean;
    v_melding varchar2(4000);

  begin
    -- opnieuw aanmelden kan alleen als dezelfde stoftest al is afgerond, het onderzoek nog niet is afgerond
    -- en de meetwaarde is wijzigbaar.
    if ( niet_afgerond (p_meti_id => p_meti_id
                       ,p_stof_id => p_stof_id ) ) AND
       ( NOT kgc_onde_00.afgerond( p_meti_id => p_meti_id ) )
    then
      begin

        update bas_meetwaarde_details mdet
        set waarde = 'N'
        where upper(prompt) = 'TEST OK?'
        and mdet.meet_id in (select meet.meet_id
                             from bas_meetwaarden meet where meet.meti_id = p_meti_id and meet.stof_id = p_stof_id)
        --and bas_meet_00.wijzigbaar( mdet.meet_id ) = 'J'
        ;

        update bas_meetwaarden
        set    afgerond = 'J'
        where  meti_id = p_meti_id
        and    stof_id = p_stof_id
        and    afgerond = 'N'
        --and    bas_meet_00.wijzigbaar( meet_id ) = 'J'
        ;
      end;
    end if;

    bas_meti_00.stoftest_aanmelden
    ( p_meti_id  => p_meti_id
    , p_stof_id  => p_stof_id
    , p_frac_id  => p_frac_id
    , p_onde_id  => p_onde_id
    , p_in_duplo => false
    , p_commit   => false
    , x_fout     => x_fout
    , x_melding  => x_melding
    );
  end;

PROCEDURE  stoftest_aanmelden_m_protocol
 (  p_stof_id  IN  NUMBER
 ,  p_frac_id  IN  NUMBER
 ,  p_onde_id  IN  NUMBER
 ,  p_stgr_id  IN  NUMBER
 ,  p_mede_id  IN  NUMBER
 ,  p_meti_id  IN  NUMBER
 ,  x_fout     OUT  BOOLEAN
 ,  x_melding  OUT  VARCHAR2
 )
 IS
  v_stof_id NUMBER := p_stof_id;
  v_frac_id NUMBER := p_frac_id;
  v_onde_id NUMBER := p_onde_id;
  v_stgr_id NUMBER := p_stgr_id;
  v_meti_id NUMBER := p_meti_id;
  v_stan_id NUMBER := null;
  v_onmo_id NUMBER := null;
  v_meet_id NUMBER := null;

  CURSOR stof_cur
  IS
    SELECT stof_id
    ,      eenheid
    ,      meet_reken
    ,      mwst_id
    ,      stof_id_voor
    ,      code
    ,      omschrijving
    ,      vervallen
    FROM   kgc_stoftesten
    WHERE  stof_id = v_stof_id
    ;

  stof_rec stof_cur%rowtype;

  CURSOR mwss_cur
  ( b_mwst_id IN NUMBER
  , b_aanmelden IN NUMBER
  )
  IS
    SELECT mwss.volgorde
    ,      mwss.prompt
    FROM   kgc_mwst_samenstelling mwss
    WHERE  mwss.mwst_id = b_mwst_id
    AND    mwss.aanmelden > 0
    AND    mwss.aanmelden <= b_aanmelden
    ORDER BY mwss.volgorde
    ;

  CURSOR mest_cur
  IS
    SELECT mest_id
    FROM   bas_meetwaarde_statussen
    WHERE  default_goed_fout = 'F'
    AND    vervallen = 'N'
    ;
  v_mest_id_fout NUMBER;

  cursor cuty_cur
  ( b_meet_id in number
  )
  is
    select rese.cuty_id
    ,      meti.onmo_id
    ,      meti.frac_id
    ,      meet.stof_id
    from   bas_reserveringen rese
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    where  meet.meet_id = rese.meet_id
    and    meet.meti_id = meti.meti_id
    and    meet.meet_id = b_meet_id
    ;

  CURSOR frac_cur
  IS
    SELECT frac.frac_id
    ,      frac.controle
    ,      mons.tijd_afname
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    WHERE  mons.mons_id = frac.mons_id
    AND    frac_id = v_frac_id
    ;
  frac_rec frac_cur%rowtype;

  CURSOR onmo_cur
  IS
    SELECT onmo.onmo_id
    FROM   bas_fracties frac
    ,      kgc_onderzoek_monsters onmo
    WHERE  onmo.mons_id = frac.mons_id
    AND    onmo.onde_id = p_onde_id
    AND    frac.frac_id = p_frac_id
    ;

  CURSOR meti_leeg_cur
  IS
    -- leeg bij fractie
    SELECT meti.meti_id
    FROM   bas_metingen meti
    WHERE  meti.onde_id = p_onde_id
    AND    meti.frac_id = p_frac_id
    AND    meti.stgr_id = p_stgr_id
    AND    meti.afgerond = 'N'
    AND    NOT EXISTS
           ( SELECT NULL
             FROM   bas_meetwaarden meet
             WHERE  meet.meti_id = meti.meti_id
           )
    ;

  CURSOR meti_cur
  IS
    SELECT frac_id
    ,      stan_id
    ,      onmo_id
    ,      onde_id
    ,      stgr_id
    ,      prioriteit
    ,      snelheid
    ,      afgerond
    FROM   bas_metingen
    WHERE  meti_id = v_meti_id
    ;
  meti_rec meti_cur%rowtype;

  CURSOR prst_cur
  ( b_stgr_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  IS
    SELECT prst.volgorde
    ,      prst.eenheid
    ,      prst.afnametijd
    FROM   kgc_protocol_stoftesten prst
    WHERE  prst.stgr_id = b_stgr_id
    AND    prst.stof_id = b_stof_id
    ;
  prst_rec prst_cur%rowtype;

  v_normaalwaarde_ondergrens NUMBER;
  v_normaalwaarde_bovengrens NUMBER;
  v_normaalwaarde_gemiddelde NUMBER;
  v_dummy_fout BOOLEAN;
  v_dummy_melding VARCHAR2(4000);
  v_dummy_meet_id NUMBER;

  -- is er een niet-afgeronde meetwaarde voor deze stoftest bij deze fractie?
  FUNCTION niet_afgerond
  ( b_meti_id IN NUMBER
  , b_stof_id IN NUMBER
  )
  RETURN BOOLEAN
  IS
    v_return BOOLEAN := FALSE;
    CURSOR c
    IS
      SELECT NULL
      FROM   bas_metingen meti
      ,      bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.meti_id = b_meti_id
      AND    meet.stof_id = b_stof_id
      AND    meet.afgerond = 'N'
      AND    meti.afgerond = 'N'
      ;
    l_dummy VARCHAR2(1);
   BEGIN
    IF ( b_meti_id IS NOT NULL
     AND b_stof_id IS NOT NULL
       )
    THEN
      OPEN  c;
      FETCH c
      INTO  l_dummy;
      v_return := c%found;
      CLOSE c;
    END IF;
    RETURN( v_return );
  END niet_afgerond;

  -- bouw (fout)melding op
  PROCEDURE  meld
   (  b_tekst  IN  VARCHAR2
   ,  b_fout   IN  BOOLEAN
   )
   IS
   BEGIN
    IF b_fout
    THEN
      x_fout := TRUE;
    END IF;

    IF ( x_melding IS NULL )
    THEN
      x_melding := b_tekst;
    ELSE
      x_melding := x_melding||CHR(10)||b_tekst;
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END meld;
BEGIN

  OPEN  stof_cur;
  FETCH stof_cur
  INTO  stof_rec;
  CLOSE stof_cur;
  IF ( stof_rec.stof_id IS NULL )
  THEN
    meld ( 'Foute aanroep: aan te melden stoftest is niet bekend!', TRUE );
    RETURN;
  ELSIF ( stof_rec.vervallen = 'J' )
  THEN
    meld ( stof_rec.omschrijving||' is vervallen!', TRUE );
    RETURN;
  END IF;

  IF ( v_meti_id IS NOT NULL )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  meti_rec;
    CLOSE meti_cur;
    -- is de meting al afgerond? zo ja dan is opnieuw aanmelden onmogelijk
    IF ( meti_rec.afgerond = 'J' )
    THEN
      meld( 'Meting is afgerond. Opnieuw aanmelding is niet meer toegestaan', TRUE );
      RETURN;
    END IF;
    v_stan_id := meti_rec.stan_id;
    v_onmo_id := meti_rec.onmo_id;
  ELSIF ( v_onmo_id IS NULL
      AND p_onde_id IS NOT NULL
      AND p_frac_id IS NOT NULL
        )
  THEN
    OPEN  onmo_cur;
    FETCH onmo_cur
    INTO  v_onmo_id;
    CLOSE onmo_cur;
  END IF;

  IF  niet_afgerond
       ( b_meti_id => v_meti_id
       , b_stof_id => p_stof_id
       )
     AND
       ( NOT kgc_onde_00.afgerond( p_meti_id => v_meti_id ) )
  THEN

    update bas_meetwaarde_details mdet
    set waarde = 'N'
    where upper(prompt) = 'TEST OK?'
    and mdet.meet_id in (select meet.meet_id
                         from bas_meetwaarden meet where meet.meti_id = p_meti_id and meet.stof_id = p_stof_id)
   -- and bas_meet_00.wijzigbaar( mdet.meet_id ) = 'J'
    ;

    update bas_meetwaarden
    set    afgerond = 'J'
    where  meti_id = p_meti_id
    and    stof_id = p_stof_id
    and    afgerond = 'N'
  --  and    bas_meet_00.wijzigbaar( meet_id ) = 'J'
    ;

  END IF;

  -- fractiegegevens (status)
  IF ( v_frac_id IS NOT NULL )
  THEN
    OPEN  frac_cur;
    FETCH frac_cur
    INTO  frac_rec;
    CLOSE frac_cur;
  END IF;

  -- aanmelden
  IF ( v_meti_id IS NULL )
  THEN
    BEGIN
      SELECT bas_meti_seq.nextval
      INTO   v_meti_id
      FROM   dual;
      INSERT INTO bas_metingen
      ( meti_id
      , stan_id
      , frac_id
      , onmo_id
      , onde_id
      , stgr_id
      )
      VALUES
      ( v_meti_id
      , v_stan_id
      , p_frac_id
      , v_onmo_id
      , p_onde_id
      , p_stgr_id
      );
    EXCEPTION
      WHEN OTHERS
      THEN
        meld ( 'Meting niet gemaakt:'||CHR(10)||SQLERRM, TRUE);
    END;
  END IF;

  IF ( v_meti_id IS NOT NULL )
  THEN
    IF ( stof_rec.mwst_id IS NULL )
    THEN
      stof_rec.mwst_id := kgc_mwst_00.meetwaarde_structuur
                          ( p_meti_id => v_meti_id
                          , p_stof_id => p_stof_id
                          );
    END IF;
    v_normaalwaarde_ondergrens := bas_meet_00.normaalwaarde_onder( v_stof_id, v_meti_id, NULL, NULL );
    v_normaalwaarde_bovengrens := bas_meet_00.normaalwaarde_boven( v_stof_id, v_meti_id, NULL, NULL );
    v_normaalwaarde_gemiddelde := bas_meet_00.normaalwaarde_gemiddeld( v_stof_id, v_meti_id, NULL, NULL );
    IF ( p_stof_id IS NOT NULL
     AND meti_rec.stgr_id IS NOT NULL
       )
    THEN
      OPEN  prst_cur( b_stgr_id => meti_rec.stgr_id
                    , b_stof_id => v_stof_id
                    );
      FETCH prst_cur
      INTO  prst_rec;
      stof_rec.eenheid := NVL( prst_rec.eenheid, stof_rec.eenheid );
      CLOSE prst_cur;
    END IF;

    BEGIN
      SELECT bas_meet_seq.nextval
      INTO   v_meet_id
      FROM   dual;
      INSERT INTO bas_meetwaarden
        ( meet_id
        , meti_id
        , stof_id
        , mwst_id
        , meet_reken
        , meeteenheid
        , normaalwaarde_ondergrens
        , normaalwaarde_bovengrens
        , normaalwaarde_gemiddelde
        , mede_id
        , volgorde
        )
        VALUES (
          v_meet_id
        , v_meti_id
        , p_stof_id
        , stof_rec.mwst_id
        , stof_rec.meet_reken
        , stof_rec.eenheid
        , v_normaalwaarde_ondergrens
        , v_normaalwaarde_bovengrens
        , v_normaalwaarde_gemiddelde
        , p_mede_id
        , prst_rec.volgorde
        )
        ;
        meld ( 'Stoftest '||stof_rec.omschrijving||' aangemeld', FALSE );

      EXCEPTION
        WHEN OTHERS
        THEN
          meld ( 'Meetwaarde niet gemaakt:'||CHR(10)||SQLERRM, TRUE );
      END;
    END IF;

END;


BEGIN

   --1. Haal de afdelings id op
   open  kafd_cur (b_afdeling => p_afdeling);
   fetch kafd_cur into v_kafd_id;
   close kafd_cur;

   --2. Ophalen van systeemparameter dat bepaald hoevaak een stoftest opnieuw aangemald mag worden
   v_stof_aantal_sypa := kgc_sypa_00.standaard_waarde
                         ( p_parameter_code => 'BEH_ROBOT_OPNIEUW_AANMELDEN'
                         , p_kafd_id        => v_kafd_id
                         );
   dbms_output.put_line ('Stoftest mag maximaal '||v_stof_aantal_sypa||' zijn aangemeld');

   --3. Bepaal welke metingen niet goed zijn in de postrobot tabel: testok = Nee en helix_verwerkt = Nee
   --   Haal ook alleen die meet_id's op voor de opgegeven afdeling
   for prbt_rec_nok in prbt_cur_nok ( b_kafd_id => v_kafd_id ) loop

     v_stof_id := '';
     v_onde_id := '';
     v_frac_id := '';
     v_meti_id := '';
     v_onderzoeknr := '';
     v_stof_omschrijving := '';
     v_stgr_code := '';
     v_stof_aantal := '';
     v_mede_id := '';
     v_stgr_id_m := '';
     v_stgr_code_m := '';
     v_meti_id_m := '';

      -- Controleer of de pcr_meetid wel gevuld is
     if prbt_rec_nok.meetid is null
     then

       dbms_output.put_line ('Meetid is leeg, pcrplatebarcode: '||prbt_rec_nok.pcrplatebarcode);
       gebruikers_info(p_tekst => 'Meetid is leeg, pcrplatebarcode: '||prbt_rec_nok.pcrplatebarcode);

     else

        -- Controleer of de stoftest wel opnieuw aangemeld mag worden, dit mag maar X keer gebeuren op basis van v_stof_aantal

        --4. haal de stoftest en onderzoek op van de betreffende meet_id
        open  meet_stof_cur(b_meet_id => prbt_rec_nok.meetid);
        fetch meet_stof_cur into v_stof_id, v_onde_id, v_frac_id, v_meti_id, v_onderzoeknr, v_stof_omschrijving, v_stgr_code;
        close meet_stof_cur;

        --5. bepaal hoevaak de stoftest voor het onderzoek is aangemeld
        open  stof_cur(b_stof_id => v_stof_id
                      ,b_onde_id => v_onde_id);
        fetch stof_cur into v_stof_aantal;
        close stof_cur;

        --6. Als de stoftest vaker of net zo vaak is aangemeld als de systeemparameter aangeeft, dan mag het niet opnieuw
        if v_stof_aantal >= v_stof_aantal_sypa
        then
          -- output voor de gebruiker
          gebruikers_info(p_tekst => 'Stoftest '||v_stof_omschrijving||', fractienummer '||prbt_rec_nok.dna_id||', voor onderzoek '||v_onderzoeknr||', wordt aangemeld bij manueel protocol. Deze is al '||v_stof_aantal||' keer aangemeld');
          dbms_output.put_line ('Stoftest '||v_stof_omschrijving||', fractienummer '||prbt_rec_nok.dna_id||', voor onderzoek '||v_onderzoeknr||', wordt aangemeld bij manueel protocol. Deze is al '||v_stof_aantal||' keer aangemeld');

          --6.1. Opnieuw aanmelden stoftest bij manueel protocol

          -- Haal de mede_id op van de ROBOT medewerker
          open  mede_cur (b_code => 'ROBOT');
          fetch mede_cur into v_mede_id;
          close mede_cur;

          -- Als de medewerker ROBOT niet bestaat dan geef hiervan een melding, ga verder met mede_id = 1
          if v_mede_id is null
          then
            gebruikers_info(p_tekst => 'ROBOT medewerker bestaat niet. Ingelogde medewerker ID gebruiken.');
            dbms_output.put_line ('ROBOT medewerker bestaat niet. Ingelogde medewerker ID gebruiken.');
            v_mede_id := kgc_mede_00.medewerker_id;
          end if;

          -- uitzoeken hoe je de _M protocol meegeeft aan procedure
          open  stgr_manueel_cur(b_stgr_code => v_stgr_code );
          fetch stgr_manueel_cur into v_stgr_id_m, v_stgr_code_m;
          close stgr_manueel_cur;

          if v_stgr_id_m is null then

              gebruikers_info(p_tekst => 'Geen manueel protocol gevonden voor Stoftest '||v_stof_omschrijving||', onderzoek '||v_onderzoeknr||', kon niet worden aangemeld.');
              dbms_output.put_line ('Geen manueel protocol gevonden voor Stoftest '||v_stof_omschrijving||', onderzoek '||v_onderzoeknr||', kon niet worden aangemeld.' );

          else

            -- output voor de gebruiker
            gebruikers_info(p_tekst => 'Stoftest '||v_stof_omschrijving||', fractienummer '||prbt_rec_nok.dna_id||', onderzoek '||v_onderzoeknr||', wordt aangemeld bij manueel protocol '||v_stgr_code_m);
            dbms_output.put_line ('Stoftest '||v_stof_omschrijving||', fractienummer '||prbt_rec_nok.dna_id||', onderzoek '||v_onderzoeknr||', wordt aangemeld bij manueel protocol '||v_stgr_code_m);

            open  meti_cur (b_onde_id => v_onde_id
                           ,b_stgr_id => v_stgr_id_m);
            fetch meti_cur into v_meti_id_m;
            close meti_cur;

            -- Stoftest aanmelden bij manueel protocol
            stoftest_aanmelden_m_protocol
             ( p_stof_id => v_stof_id
             , p_frac_id => v_frac_id
             , p_onde_id => v_onde_id
             , p_stgr_id => v_stgr_id_m
             , p_mede_id => v_mede_id
             , p_meti_id => v_meti_id_m
             , x_melding => v_melding
             , x_fout    => v_fout
             );

            --6.2. Als de stoftest niet opnieuw kon worden aangemeld dat geef daarvan een melding
            if v_fout
            then

              gebruikers_info(p_tekst => v_melding||': Stoftest '||v_stof_omschrijving||', onderzoek '||v_onderzoeknr||', kon niet worden aangemeld bij manueel protocol');
              dbms_output.put_line (v_melding||': Stoftest '||v_stof_omschrijving||', onderzoek '||v_onderzoeknr||', kon niet worden aangemeld bij manueel protocol');

            else

              gebruikers_info(p_tekst => 'Postrobot tabel aangepast, verwerkt door Helix ');
              dbms_output.put_line ('Postrobot tabel aangepast, verwerkt door Helix ');

              --6.3. Postrobot tabel updaten: helix_verwerkt = Ja
              update postrobot@storage prbt
              set    helix_verwerkt = 'J'
              where  testok         = 'N'
              and    helix_verwerkt = 'N'
              and    meetid         = prbt_rec_nok.meetid
              ;

            end if;

          end if;

        else
          -- output voor de gebruiker
          gebruikers_info(p_tekst => 'Stoftest '||v_stof_omschrijving||', voor onderzoek '||v_onderzoeknr||', wordt opnieuw aangemeld');
          dbms_output.put_line ('Stoftest '||v_stof_omschrijving||', voor onderzoek '||v_onderzoeknr||', wordt opnieuw aangemeld');

          --7. Opnieuw aanmelden stoftest
          stoftest_opnieuw_aanmelden
           ( p_stof_id => v_stof_id
           , p_meti_id => v_meti_id
           , p_frac_id => v_frac_id
           , p_onde_id => v_onde_id
           , x_fout    => v_fout
           , x_melding => v_melding
           );

          --8. Als de stoftest niet opnieuw kon worden aangemeld dat geef daarvan een melding
          if v_fout
          then

            gebruikers_info(p_tekst => v_melding||': Stoftest '||v_stof_omschrijving||', onderzoek '||v_onderzoeknr||', kon niet worden aangemeld');
            dbms_output.put_line (v_melding||': Stoftest '||v_stof_omschrijving||', onderzoek '||v_onderzoeknr||', kon niet worden aangemeld');

          else

            gebruikers_info(p_tekst => 'Postrobot tabel aangepast, verwerkt door Helix ');
            dbms_output.put_line ('Postrobot tabel aangepast, verwerkt door Helix ');

            --9. Postrobot tabel updaten: helix_verwerkt = Ja
            update postrobot@storage prbt
            set    helix_verwerkt = 'J'
            where  testok         = 'N'
            and    helix_verwerkt = 'N'
            and    meetid         = prbt_rec_nok.meetid
            ;

          end if;

        end if;

     end if;

   end loop;


   --10. Bepaal welke metingen GOED zijn in de postrobot tabel: testok = Ja en helix_verwerkt = Nee
   for prbt_rec_ok in prbt_cur_ok ( b_kafd_id => v_kafd_id ) loop

      v_stof_id := '';
      v_onde_id := '';
      v_frac_id := '';
      v_meti_id := '';
      v_onderzoeknr := '';
      v_stof_omschrijving := '';
      v_stgr_code := '';

      --11. haal de stoftest en onderzoek op van de betreffende meet_id
      open  meet_stof_cur(b_meet_id => prbt_rec_ok.meetid);
      fetch meet_stof_cur into v_stof_id, v_onde_id, v_frac_id, v_meti_id, v_onderzoeknr, v_stof_omschrijving, v_stgr_code;
      close meet_stof_cur;

      --12 afronden alleen als het nog niet afgerond is
      if ( niet_afgerond (p_meti_id => v_meti_id
                         ,p_stof_id => v_stof_id ) )
      then
        update bas_meetwaarden
        set    afgerond = 'J'
        where  meti_id = v_meti_id
        and    stof_id = v_stof_id
        and    afgerond = 'N'
        ;
      end if;

      --13. Postrobot tabel updaten: helix_verwerkt = Ja
      update postrobot@storage prbt
      set    helix_verwerkt = 'J'
      where  testok         = 'J'
      and    helix_verwerkt = 'N'
      and    meetid         = prbt_rec_ok.meetid
      ;

   end loop;


   if v_inhoud is not null
   then

     utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                     recipients => p_email,
                     subject    => v_onderwerp,
                     message    => v_inhoud
                  );
   end if;

  dbms_output.put_line ('Gereed');

  commit;

EXCEPTION
   WHEN OTHERS THEN
     ROLLBACK;

     v_inhoud := substr(v_inhoud, 1, 30000) || c_nieuwe_regel
              || 'Foutmelding: ' || sqlerrm || c_nieuwe_regel
              || dbms_utility.format_error_backtrace
              || dbms_utility.format_call_stack;

     utl_mail.send(  sender     => 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                  ,  recipients => p_email
                  ,  cc         => 'HelpdeskHelixCUKZ@cukz.umcn.nl'
                  ,  subject    => 'EXCEPTION in procedure start_robot_stoftest_aanmelden: ' || v_onderwerp
                  ,  message    => v_inhoud
                  );

     qms$errors.unhandled_exception('hlx_werklijst_tray_etiket.start_robot_stoftest_aanmelden');

END start_robot_stoftest_aanmelden;

END BEH_DNA_VOORTEST_OPN_AANMELDEN;
/

/
QUIT
