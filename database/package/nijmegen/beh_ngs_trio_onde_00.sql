CREATE OR REPLACE PACKAGE "HELIX"."BEH_NGS_TRIO_ONDE_00" AS
/******************************************************************************
   NAME:       BEH_NGS_TRIO_ONDE_00
   PURPOSE: Deze package wordt gebruikt om notitificatie mails te versturen. Dit is nu gemaakt voor RFD onderzoeksgroep. Indien er een onderzoek voor RFD onderzoeksgroep in Helix aangemaakt wordt, dan moet er notificatie mail gestuurd worden naar deze groep.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2016      M.Golebiowska       1. Created this package.
******************************************************************************/

procedure ngs_trio_onderzoeken;

end BEH_NGS_TRIO_ONDE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_NGS_TRIO_ONDE_00"
AS
   /******************************************************************************
      NAME:       BEH_NGS_TRIO_ONDE_00
      PURPOSE: Deze package wordt gebruikt om NGS TRIO onderzoeken in Helix netjes te maken

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        04-02-2016      M.Golebiowska       1. Created this package.
   ******************************************************************************/

   PROCEDURE ngs_trio_onderzoeken
   IS
      v_onin_id     NUMBER;
      v_meti_id     NUMBER;

      CURSOR onde_cur
      -- in deze cursor worden NGS TRIO onderzoeken met 'pakket' indicatie geselecteerd, personen en hun familierelaties opgehaald
      IS
      SELECT   DISTINCT
                    ONDE.ONDE_ID,
                    ONDE.ONDERZOEKNR,
                    ONDE.PERS_ID,
                    ONDE.ONGR_ID,
                    ONRE.CODE AS REDE_CODE,
                    ONIN.INDI_ID,
                    INDI.CODE,
                    FAON.FAMILIEONDERZOEKNR,
                    FAMI.FAMILIENUMMER,
                    CASE
                       WHEN FARE.CODE IN ('ZOO', 'DOC') AND REPE.RICHTING = '>'
                       THEN 'KIND' ELSE CASE WHEN FARE.CODE IN ('ZOO', 'DOC') AND REPE.RICHTING = '<' THEN 'OUDER' ELSE 'GEEN' END
                    END AS RELATIE,
                    CASE
                       WHEN FARE.CODE IN ('ZOO', 'DOC') AND REPE.RICHTING = '<'
                       THEN PERSK.PERS_ID ELSE NULL
                    END AS ALT_DECL_PERS
        FROM  kgc_onderzoeken onde,
                  kgc_onderzoek_indicaties onin,
                  kgc_indicatie_teksten indi,
                  kgc_onderzoeksredenen  onre,
                  kgc_families fami,
                  kgc_familie_leden fale,
                  kgc_familie_relaties fare,
                  KGC_KGCREPE01_VW repe,
                  kgc_familie_onderzoeken faon,
                  KGC_FAON_BETROKKENEN fobe,
                  KGC_FAON_BETROKKENEN fobe2,
                  kgc_personen pers, -- onderzoekspersoon
                  kgc_personen persk -- kind
        WHERE FAMI.FAMI_ID = FALE.FAMI_ID
            AND FALE.PERS_ID = PERS.PERS_ID
            AND PERS.PERS_ID = ONDE.PERS_ID

            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INDI.INDI_ID
            AND ONIN.INDI_ID = FAON.INDI_ID

            AND FAON.FAMI_ID = FALE.FAMI_ID
            AND FAON.FAON_ID = FOBE.FAON_ID
            AND FOBE.ONDE_ID = ONDE.ONDE_ID
            AND FOBE.PERS_ID = ONDE.PERS_ID

            AND FAON.FAON_ID = FOBE2.FAON_ID
            AND FOBE2.PERS_ID = PERSK.PERS_ID

            AND FARE.FARE_ID = REPE.FARE_ID
            AND REPE.PERS_ID = PERS.PERS_ID
            AND PERSK.PERS_ID = REPE.PERS_ID_RELATIE

            AND ONDE.OMSCHRIJVING = ONRE.OMSCHRIJVING (+)
            AND ONDE.ONGR_ID = ONRE.ONGR_ID (+)

            AND INDI.CODE LIKE '%TRIO'
            AND FARE.CODE IN ('ZOO', 'DOC')

            ;

      onde_rec      onde_cur%ROWTYPE;

   BEGIN
      OPEN onde_cur;
      FETCH onde_cur INTO onde_rec;
      CLOSE onde_cur;

      FOR onde_rec IN onde_cur
      LOOP
          -- verwijder eerst eventuele lege metingen
          DELETE FROM bas_metingen
          WHERE METI_ID IN (SELECT METI.METI_ID
                                     FROM bas_metingen meti, kgc_onderzoeken onde
                                     WHERE ONDE.ONDE_ID = METI.ONDE_ID
                                         AND METI.STGR_ID IS NULL -- lege meting!
                                         AND METI.METI_ID NOT IN (SELECT MEET.METI_ID FROM bas_meetwaarden meet)
                                         AND ONDE.ONDE_ID = ONDE_REC.ONDE_ID);
         COMMIT;

        -- verwijder TRIO indicaties bij het onderzoek
         DELETE FROM kgc_onderzoek_indicaties onin
               WHERE ONIN.ONDE_ID = ONDE_REC.ONDE_ID
                     AND ONIN.INDI_ID = ONDE_REC.INDI_ID;
         COMMIT;

        -- voeg bij OUDER onderzoeken alternatieve declaratie persoon -> KIND
         UPDATE kgc_onderzoeken onde
         SET pers_id_alt_decl = onde_rec.alt_decl_pers
         WHERE ONDE.ONDE_ID = ONDE_REC.ONDE_ID
         ;

         -- voeg bij KIND onderzoek onderzoeksreden 'bevestigen diagnose'
         UPDATE kgc_onderzoeken onde
         SET ONRE_ID = (select ONRE.ONRE_ID from kgc_onderzoeksredenen onre where ONRE.CODE= CASE WHEN onde_rec.rede_code like '%E' THEN '001-E' ELSE '001' END and onre.ONGR_ID = onde_rec.ongr_id ),
                omschrijving= (select OMSCHRIJVING from kgc_onderzoeksredenen onre where ONRE.CODE= CASE WHEN onde_rec.rede_code like '%E' THEN '001-E' ELSE '001' END and onre.ONGR_ID = onde_rec.ongr_id)
         WHERE ONDE.ONDE_ID = onde_rec.onde_id
            AND onde_rec.relatie = 'KIND';
         COMMIT;

         -- selecteer sequentie id voor onin_id tbv de insert van de juiste indicaties
         SELECT kgc_onin_seq.NEXTVAL INTO v_onin_id FROM DUAL;

        -- voeg bij OUDER onderzoeken indicaties zoals %_C
         INSERT INTO kgc_onderzoek_indicaties (onin_id, onde_id, indi_id)
            (SELECT v_onin_id, onde_rec.ONDE_ID, (select INDI.INDI_ID
                                                                        from kgc_indicatie_teksten indi
                                                                        where INDI.CODE LIKE '%!_C' ESCAPE '!'
                                                                        and INDI.ONGR_ID = ONDE_REC.ONGR_ID
                                                                        and INDI.CODE in (SELECT *
                                                                                                   FROM TABLE (CAST (beh_alfu_00.explode (',',UPPER ((SELECT  ATWA3.WAARDE
                                                                                                                                                                                 FROM kgc_indicatie_teksten inte3, KGC_ATTRIBUUT_WAARDEN atwa3, KGC_ATTRIBUTEN attr3
                                                                                                                                                                                 WHERE INTE3.INDI_ID = ATWA3.ID
                                                                                                                                                                                    AND ATTR3.ATTR_ID = ATWA3.ATTR_ID
                                                                                                                                                                                    AND ATTR3.CODE = 'NGS_TRIO_PAKKET'
                                                                                                                                                                                    AND ATWA3.ID = onde_rec.indi_id))
                                                                                                                                                                ) AS BEH_LISTTABLE
                                                                                                                                )
                                                                                                                       )
                                                                                                )
                                                                    )
               FROM DUAL
               WHERE onde_rec.relatie = 'OUDER'
               )
         ;
         COMMIT;

        -- voeg bij KIND onderzoeken indicaties NIET zoals %_C
         INSERT INTO kgc_onderzoek_indicaties (onin_id, onde_id, indi_id)
            (SELECT v_onin_id, onde_rec.ONDE_ID, (select INDI.INDI_ID
                                                                        from kgc_indicatie_teksten indi
                                                                        where INDI.CODE NOT LIKE '%!_C' ESCAPE '!'
                                                                        and INDI.ONGR_ID = ONDE_REC.ONGR_ID
                                                                        and INDI.CODE in (SELECT *
                                                                                                   FROM TABLE (CAST (beh_alfu_00.explode (',',UPPER ((SELECT  ATWA3.WAARDE
                                                                                                                                                                                 FROM kgc_indicatie_teksten inte3, KGC_ATTRIBUUT_WAARDEN atwa3, KGC_ATTRIBUTEN attr3
                                                                                                                                                                                 WHERE INTE3.INDI_ID = ATWA3.ID
                                                                                                                                                                                    AND ATTR3.ATTR_ID = ATWA3.ATTR_ID
                                                                                                                                                                                    AND ATTR3.CODE = 'NGS_TRIO_PAKKET'
                                                                                                                                                                                    AND ATWA3.ID = onde_rec.indi_id
                                                                                                                                                                                    ))
                                                                                                                                                                ) AS BEH_LISTTABLE
                                                                                                                                )
                                                                                                                       )
                                                                                                )
                                                                    )
               FROM DUAL
               WHERE onde_rec.relatie = 'KIND'
               )
         ;
         COMMIT;

      END LOOP;

      COMMIT;
   END ngs_trio_onderzoeken;
END BEH_NGS_TRIO_ONDE_00;
/

/
QUIT
