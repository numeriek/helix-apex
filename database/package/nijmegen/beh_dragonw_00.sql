CREATE OR REPLACE PACKAGE "HELIX"."BEH_DRAGONW_00" as
function haal_ver_code_op (p_indi_code in varchar2)
return varchar2;
end beh_dragonw_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BEH_DRAGONW_00" as
function haal_ver_code_op(p_indi_code in varchar2)
return varchar2 is

v_ver_code_spec kgc_decl_entiteiten.verrichtingcode%type;
v_gevonden boolean := false;

cursor c_ver_code_alg is
select verrichtingcode
from kgc_decl_entiteiten
where deen_id = 592 -- verrichtingcode 920003
and  upper(vervallen) = 'N'
;

r_ver_code_alg c_ver_code_alg%rowtype;
--alle declaratie criteria's waarin indicatiecodes zijn opgenomen
-- 1 - hier gaat het alleen om BU declaraties
-- voor onze doeleinden is genoeg om alleen ONBE declaratie entiteiten te checken
cursor c_ver_code_spec is
select  deen.verrichtingcode verrichtingcode_spec
,       replace(replace(replace(decr.waarde, 'in('),')'), '''') indi_codes_bu
from    kgc_decl_entiteiten deen
,       kgc_decl_criteria decr
,       kgc_declaratiewijzen dewy
,       kgc_kgc_afdelingen kafd
where deen.deen_id = decr.deen_id
and   deen.dewy_id = dewy.dewy_id
and   deen.kafd_id = kafd.kafd_id
and   dewy.kafd_id = kafd.kafd_id
and   upper(decr.tabelnaam) = 'KGC_INDICATIE_TEKSTEN'
and   upper(decr.kolomnaam) = 'CODE'
--and   deen.volgorde in (15, 16, 17, 18, 20)
and  upper(deen.entiteit) = 'ONBE'
and   upper(kafd.code) = 'GENOOM'
and   upper(dewy.code) = 'BU'
AND  upper(deen.vervallen) = 'N'
order by deen.volgorde
;
r_ver_code_spec c_ver_code_spec%rowtype;

cursor c_indi(p_indi_codes_bu in varchar2) is
select *
from table(cast(beh_alfu_00.explode(',', p_indi_codes_bu) as beh_listtable))
;
begin

open c_ver_code_alg;
fetch c_ver_code_alg into r_ver_code_alg;
close c_ver_code_alg;
-- checken of de indicatiecode een specifieke verrichtingscode heeft..
for r_ver_code_spec in c_ver_code_spec loop
    for r_indi in c_indi(r_ver_code_spec.indi_codes_bu) loop
        if upper(trim(p_indi_code)) = upper(trim(r_indi.column_value)) then
            v_gevonden := true;
            v_ver_code_spec := r_ver_code_spec.verrichtingcode_spec;
            exit;
        end if;
    end loop;
end loop;
-- als er geen specifieke verrichtingcode is gevonden
-- wordt algemen code (920003) teruggestuurd.
if (v_gevonden) then
    return v_ver_code_spec;
else
    return r_ver_code_alg.verrichtingcode;
end if;

end haal_ver_code_op;

end beh_dragonw_00;
/

/
QUIT
