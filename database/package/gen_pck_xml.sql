CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_XML"
AS
   /*
   ================================================================================
   Package Name :  gen_pck_xml
   Usage        :  Package with procedure for xml.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */

   -- get default XML stylesheet (based on IE stylesheet)
   FUNCTION f_get_default_xml_stylesheetie
      RETURN VARCHAR2;
   -- transform XML via stylesheet
   FUNCTION f_transform(i_xml         IN XMLTYPE
                     ,i_stylesheet  IN XMLTYPE := NULL)
      RETURN XMLTYPE;
   PROCEDURE p_htp_print_clob(i_clob         IN CLOB
                           ,i_add_newline  IN BOOLEAN := TRUE);
END GEN_pck_xml;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_XML"
AS
     /*
   ================================================================================
   Package Name :  gen_pck_xml
   Usage        :  Package with procedure for xml.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
    FUNCTION f_get_default_xml_stylesheetie
      RETURN VARCHAR2
   AS
      l_returnvalue  VARCHAR2(32000);
   BEGIN
      /*
      Purpose:      get default XML stylesheet (based on IE stylesheet)
      Remarks:      formats XML the way it is displayed by default in IE
                    see http://www.biglist.com/lists/xsl-list/archives/200303/msg00794.html
      Who     Date        Description
      ------  ----------  --------------------------------
      MBR     10.05.2010  Created
      */
      l_returnvalue      :=
         '<?xml version="1.0"?>
<!--
  IE5 default style sheet, provides a view of any XML document
  and provides the following features:
  - auto-indenting of the display, expanding of entity references
  - click or tab/return to expand/collapse
  - color coding of markup
  - color coding of recognized namespaces - xml, xmlns, xsl, dt
  This style sheet is available in IE5 in a compact form at the URL
  "res://msxml.dll/DEFAULTSS.xsl".  This version differs only in the
  addition of comments and whitespace for readability.
  Author:  Jonathan Marsh (jmarsh@xxxxxxxxxxxxx)
  Modified:   05/21/2001 by Nate Austin (naustin@xxxxxxxxxx), Converted to use XSLT rather than WD-xsl
              05/10/2010 by Morten Braten (http://ora-00001.blogspot.com), Cleaned up formatting, removed some comments to reduce size; see original post for original version
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dt="urn:schemas-microsoft-com:datatypes" xmlns:d2="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
<xsl:template match="/">
        <STYLE>
        .st div, .st span, .st a { font-family:lucida console,courier; }
        .c  {cursor:hand;}
        .b  {font-family:Verdana,Tahoma, arial;text-decoration:none}
        .e  {margin-left:3em; text-indent:-1.5em; margin-top:1px; margin-right:1em}
        .k  {margin-left:1em; text-indent:-1em; margin-right:1em}
        .t  {color:black}
        .xt {color:dark;}
        .ns {color:#E8E8CD;}
        .dt {color:green}
        .m  {color:#2D599E}
        .tx {font-weight:bold;}
        .db {text-indent:0px; margin-left:1em;  margin-top:0px; margin-bottom:0px; padding-left:.3em; border-left:1px solid #CCCCCC; font:small Courier}
        .di {font:small Courier}
        .d  {color:#2D599E}
        .pi {color:#2D599E}
        .cb {text-indent:0px; margin-left:1em; margin-top:0px;margin-bottom:0px; padding-left:.3em; font:small Courier; color:#888888}
        .ci {font:small Courier; color:#888888}
        PRE {margin:0px; display:inline}
      </STYLE>


            <div class="st">

                <xsl:apply-templates/>
     </div>
</xsl:template>
<!-- Templates for each node type follows.  The output of each template has a similar structure to enable script to walk the result tree easily for handling user interaction. -->
<!-- Template for the DOCTYPE declaration.  No way to get
      the DOCTYPE, so we just put in a placeholder -->
<!--  no support for doctypes
<xsl:template match="node()[nodeType()=10]">
  <DIV class="e"><SPAN>
  <SPAN class="b">&#160;</SPAN>
  <SPAN class="d">&lt;!DOCTYPE <xsl:value-of select="name()"/><I> (View Source for full doctype...)</I>&gt;</SPAN>
  </SPAN></DIV>
</xsl:template>
-->
<!-- Template for pis not handled elsewhere -->
    <xsl:template match="processing-instruction()">
        <DIV class="e">
            <SPAN class="b">&#160;</SPAN>
            <SPAN class="m">&lt;?</SPAN>
            <SPAN class="pi">
            <xsl:value-of select="name()" />&#160;<xsl:value-of select="." />
            </SPAN>
            <SPAN class="m">?&gt;</SPAN>
        </DIV>
    </xsl:template>
<!-- Template for the XML declaration.  Need a separate template because the
pseudo-attributes are actually exposed as attributes instead of just element content,
as in other pis -->
<!--  No support for the xml declaration
<xsl:template match="pi(''xml'')">
  <DIV class="e">
  <SPAN class="b">&#160;</SPAN>
  <SPAN class="m">&lt;?</SPAN><SPAN class="pi">xml
      <xsl:for-each select="@*"><xsl:value-of select="name()"/>="<xsl:value-of select="."/>"
</xsl:for-each></SPAN><SPAN class="m">?&gt;</SPAN>
  </DIV>
</xsl:template>
-->
<!-- Template for attributes not handled elsewhere -->
<xsl:template match="@*" xml:space="preserve">
<SPAN>
<xsl:attribute name="class">
  <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>t</xsl:attribute>
<xsl:value-of select="name()" />
</SPAN>
<SPAN class="m">=&quot;</SPAN>
<B>
  <xsl:value-of select="." />
</B>
<SPAN class="m">&quot;</SPAN>
</xsl:template>
<!-- Template for attributes in the xmlns or xml namespace -->
<!--  UNKNOWN
<xsl:template match="@xmlns:*|@xmlns|@xml:*"><SPAN class="ns"> <xsl:value-of select="name()"/></SPAN><SPAN class="m">="</SPAN>
  <B class="ns"><xsl:value-of select="."/></B><SPAN class="m">"</SPAN></xsl:template>
-->
<!-- Template for attributes in the dt namespace -->
<!-- UNKNOWN
<xsl:template match="@dt:*|@d2:*"><SPAN class="dt"> <xsl:value-of select="name()"/></SPAN><SPAN class="m">="</SPAN><B class="dt">
<xsl:value-of select="."/></B><SPAN class="m">"</SPAN>
</xsl:template>
-->
<!-- Template for text nodes -->
    <xsl:template match="text()">
        <DIV class="e">
            <SPAN class="b">&#160;</SPAN>
            <SPAN class="tx">
                <xsl:value-of select="." />
            </SPAN>
        </DIV>
    </xsl:template>
<!-- Template for comment nodes -->
    <xsl:template match="comment()">
        <DIV class="k">
            <SPAN>
                <A class="b" onclick="return false" onfocus="h()" STYLE="visibility:hidden">-</A>
                <SPAN class="m">&lt;!--</SPAN>
            </SPAN>
            <SPAN id="clean" class="ci">
      <PRE>
<xsl:value-of select="." />
</PRE>
            </SPAN>
            <SPAN class="b">&#160;</SPAN>
            <SPAN class="m">--&gt;</SPAN>
<SCRIPT>
f(clean);
</SCRIPT>
        </DIV>
    </xsl:template>
<!-- Template for cdata nodes -->
<!-- UNSUPPORTED
<xsl:template match="cdata()">
  <DIV class="k">
  <SPAN><A class="b" onclick="return false" onfocus="h()"
STYLE="visibility:hidden">-</A> <SPAN class="m">
      &lt;![CDATA[</SPAN></SPAN>
  <SPAN id="clean" class="di"><PRE><xsl:value-of
      select="."/></PRE></SPAN>
  <SPAN class="b">&#160;</SPAN> <SPAN
      class="m">]]&gt;</SPAN>
  <SCRIPT>f(clean);</SCRIPT></DIV>
</xsl:template>
-->
<!-- Template for elements not handled elsewhere (leaf nodes) -->
    <xsl:template match="*">
        <DIV class="e">
            <DIV STYLE="margin-left:1em;text-indent:-2em">
                <SPAN class="b">&#160;</SPAN>
                <SPAN class="m">&lt;</SPAN>
                <SPAN>
                    <xsl:attribute name="class">
                    <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>
                    t</xsl:attribute>
                    <xsl:value-of select="name()" />
                </SPAN>
                <xsl:apply-templates select="@*" />
                <SPAN class="m">/&gt;</SPAN>
            </DIV>
        </DIV>
    </xsl:template>
<!-- Template for elements with comment, pi and/or cdata children -->
    <xsl:template match="*[comment() | processing-instruction()]">
        <DIV class="e">
            <DIV class="c">
                <A href="#" onclick="return false" onfocus="h()" class="b">-</A>
                <SPAN class="m">&lt;</SPAN>
                <SPAN>
                    <xsl:attribute name="class">
                    <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>t</xsl:attribute>
                    <xsl:value-of select="name()" />
                </SPAN>
                <xsl:apply-templates select="@*" />
                <SPAN class="m">&gt;</SPAN>
            </DIV>
            <DIV>
                <xsl:apply-templates />
                <DIV>
                    <SPAN class="b">&#160;</SPAN>
                    <SPAN class="m">&lt;/</SPAN>
                    <SPAN>
                        <xsl:attribute name="class">
                        <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>t</xsl:attribute>
                        <xsl:value-of select="name()" />
                    </SPAN>
                    <SPAN class="m">&gt;</SPAN>
                </DIV>
            </DIV>
        </DIV>
    </xsl:template>
<!-- Template for elements with only text children -->
    <xsl:template match="*[text() and not(comment() | processing-instruction())]">
        <DIV class="e">
            <DIV STYLE="margin-left:1em;text-indent:-2em">
                <SPAN class="b">&#160;</SPAN>
                <SPAN class="m">&lt;</SPAN>
                <SPAN>
                    <xsl:attribute name="class">
                    <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>t</xsl:attribute>
                    <xsl:value-of select="name()" />
                </SPAN>
                <xsl:apply-templates select="@*" />
                <SPAN class="m">&gt;</SPAN>
                <SPAN class="tx">
                    <xsl:value-of select="." />
                </SPAN>
                <SPAN class="m">&lt;/</SPAN>
                <SPAN>
                    <xsl:attribute name="class">
                    <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>t</xsl:attribute>
                    <xsl:value-of select="name()" />
                </SPAN>
                <SPAN class="m">&gt;</SPAN>
            </DIV>
        </DIV>
    </xsl:template>
<!-- Template for elements with element children -->
    <xsl:template match="*[*]">
        <DIV class="e">
            <DIV class="c" STYLE="margin-left:1em;text-indent:-2em;">
                <A href="#" onclick="return false" onfocus="h()" class="b">+</A>
                <SPAN class="m">&lt;</SPAN>
                <SPAN>
                    <xsl:attribute name="class">
                    <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>t</xsl:attribute>
                    <xsl:value-of select="name()" />
                </SPAN>
                <xsl:apply-templates select="@*" />
                <SPAN class="m">&gt;</SPAN>
            </DIV>
            <DIV style="display:none;">
                <xsl:apply-templates />
                <DIV>
                    <SPAN class="b">&#160;</SPAN>
                    <SPAN class="m">&lt;/</SPAN>
                    <SPAN>
                        <xsl:attribute name="class">
                        <xsl:if test="starts-with(name(),&#39;xsl:&#39;)">x</xsl:if>t</xsl:attribute>
                        <xsl:value-of select="name()" />
                    </SPAN>
                    <SPAN class="m">&gt;</SPAN>
                </DIV>
            </DIV>
        </DIV>
    </xsl:template>
</xsl:stylesheet>';
      RETURN l_returnvalue;
   END f_get_default_xml_stylesheetie;
   -- transform XML via stylesheet
   FUNCTION f_transform(i_xml         IN XMLTYPE
                     ,i_stylesheet  IN XMLTYPE := NULL)
      RETURN XMLTYPE
   AS
      l_returnvalue  XMLTYPE;
   BEGIN
      /*
      Purpose:      transform XML via stylesheet
      Remarks:
      Who     Date        Description
      ------  ----------  --------------------------------
      MBR     22.01.2011  Created
      */
      l_returnvalue      :=
         i_xml.transform(COALESCE(i_stylesheet
                                 ,xmltype(f_get_default_xml_stylesheetie)));
      RETURN l_returnvalue;
   END f_transform;
   PROCEDURE p_htp_print_clob(i_clob         IN CLOB
                           ,i_add_newline  IN BOOLEAN := TRUE)
   AS
      l_buffer             VARCHAR2(32767);
      l_max_size  CONSTANT INTEGER := 8000;
      l_start              INTEGER := 1;
      l_cloblen            INTEGER;
   BEGIN
      /*
      Purpose:    print clob to HTTP buffer
      Remarks:    from http://francis.blog-city.com/ora20103_null_input_is_not_allowed.htm
      Who     Date        Description
      ------  ----------  -------------------------------------
      MBR     19.01.2009  Created
      */
      IF i_clob IS NOT NULL
      THEN
         l_cloblen   := dbms_lob.getlength(i_clob);
         LOOP
            l_buffer      :=
               dbms_lob.SUBSTR(i_clob
                              ,l_max_size
                              ,l_start);
            htp.prn(l_buffer);
            l_start   := l_start + l_max_size;
            EXIT WHEN l_start > l_cloblen;
         END LOOP;
         IF i_add_newline
         THEN
            htp.p;
         END IF;
      END IF;
   END p_htp_print_clob;
END GEN_pck_xml;
/

/
QUIT
