CREATE OR REPLACE PACKAGE "HELIX"."KGC_INIT_00" IS
-- Initialisaties
-- - het vullen van systeem-tabellen.
-- Zorg ervoor dat procedures keer op keer kunnen
-- worden gedraaid, zonder foutmeldingen.

-- Vul KGC_ENTITEITEN met rijen
PROCEDURE init_enti;
-- Vul KGC_SYSTEEM_PARAMETERS met rijen
PROCEDURE init_sypa;
-- Voeg rapport modules toe als systeemparameters (tbv definitie afwijkende rapporten)
PROCEDURE init_mods;
-- Vul KGC_PRIORITEITEN met rijen
PROCEDURE init_prio;
-- Vul KGC_NUMMERSTRUCTUUR met rijen
-- NB.: de inhoud van nummerstructuur staat in package kgc_nust_00
--      (genereer_nr, controleer_nr)
PROCEDURE init_nust;
-- Vul KGC_OPSLAG_ENTITEITEN met rijen
PROCEDURE init_open;
-- Vul KGC_ATTRIBUTEN met rijen
PROCEDURE init_attr;
-- Vul KGC_EXPORT_PARAMETERS met rijen
PROCEDURE init_expa;
-- Vul KGC_EMPOWER_FUNCTIONS met rijen
PROCEDURE init_emfs;
-- Vul KGC_EMPOWER_PROCESSING met rijen
PROCEDURE init_empg;
-- Doe alle initialisaties
PROCEDURE init;
-- toon afwijkende inhoud in tabel (niet gevuld door deze package)
procedure afwijkende_waarden
( p_tabel in varchar2 := null
);
END KGC_INIT_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_INIT_00" IS
-- pl/sql-tables om systeem-rijen in op te slaan ter vergelijking met werkelijke inhoud
g_toon_afwijkingen boolean := false;

type prio_tabtype is table of kgc_prioriteiten%rowtype
index by kgc_prioriteiten.code%type;
prio_tab prio_tabtype;
prio_rec kgc_prioriteiten%rowtype;

type enti_tabtype is table of kgc_entiteiten%rowtype
index by kgc_entiteiten.code%type;
enti_tab enti_tabtype;
enti_rec kgc_entiteiten%rowtype;

type sypa_tabtype is table of kgc_systeem_parameters%rowtype
index by kgc_systeem_parameters.code%type;
sypa_tab sypa_tabtype;
sypa_rec kgc_systeem_parameters%rowtype;

type nust_tabtype is table of kgc_nummerstructuur%rowtype
index by kgc_nummerstructuur.code%type;
nust_tab nust_tabtype;
nust_rec kgc_nummerstructuur%rowtype;

type open_tabtype is table of kgc_opslag_entiteiten%rowtype
index by kgc_opslag_entiteiten.code%type;
open_tab open_tabtype;
open_rec kgc_opslag_entiteiten%rowtype;

type attr_tabtype is table of kgc_attributen%rowtype
index by kgc_attributen.code%type;
attr_tab attr_tabtype;
attr_rec kgc_attributen%rowtype;

type expa_tabtype is table of kgc_export_parameters%rowtype
index by varchar2(100); -- TABELNAAM.KOLOMNAAM
expa_tab expa_tabtype;
expa_rec kgc_export_parameters%rowtype;

type emfs_tabtype is table of kgc_empower_functions%rowtype
index by binary_integer; -- kgc_empower_functions.emfs_id%type; number mag niet!
emfs_tab emfs_tabtype;
emfs_rec kgc_empower_functions%rowtype;

type empg_tabtype is table of kgc_empower_processing%rowtype
index by binary_integer; -- kgc_empower_processing.empg_id%type; number mag niet!
empg_tab empg_tabtype;
empg_rec kgc_empower_processing%rowtype;

PROCEDURE init_enti
IS
  PROCEDURE ins
  ( p_code IN VARCHAR2
  , p_omschrijving IN VARCHAR2
  , p_vertaal IN VARCHAR2
  , p_notitie IN VARCHAR2
  , p_tabel IN VARCHAR2
  , p_pk_kolom IN VARCHAR2
  , p_omschrijving_kolommen IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   enti_rec
        from   kgc_entiteiten
        where  code = UPPER( p_code );
        enti_tab( enti_rec.code ) := enti_rec;
      exception
        when no_data_found
        then
          p.l( p_code || ' staat niet in kgc_entiteiten!' );
      end;
      return;
    end if;
    UPDATE kgc_entiteiten
    SET omschrijving = p_omschrijving
    , vertaal = p_vertaal
    , notitie = p_notitie
    , tabel = p_tabel
    , pk_kolom = p_pk_kolom
    , omschrijving_kolommen = p_omschrijving_kolommen
    WHERE code = UPPER( p_code )
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_entiteiten
      ( code
      , omschrijving
      , vertaal
      , notitie
      , tabel
      , pk_kolom
      , omschrijving_kolommen
      )
      VALUES
      ( UPPER( p_code )
      , p_omschrijving
      , p_vertaal
      , p_notitie
      , p_tabel
      , p_pk_kolom
      , p_omschrijving_kolommen
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  -- notities
  ins( 'RELA', 'Relaties', 'N', 'J', 'KGC_RELATIES', 'RELA_ID', 'AANSPREKEN' );
  ins( 'PERS', 'Personen', 'N', 'J', 'KGC_PERSONEN', 'PERS_ID', 'AANSPREKEN' );
  ins( 'FAMI', 'Families', 'N', 'J', 'KGC_FAMILIES', 'FAMI_ID', 'FAMILIENUMMER||'', ''||NAAM' );
  ins( 'ONDE', 'Onderzoeken', 'N', 'J', 'KGC_ONDERZOEKEN', 'ONDE_ID', 'ONDERZOEKNR' );
  ins( 'MONS', 'Monsters', 'N', 'J', 'KGC_MONSTERS', 'MONS_ID', 'MONSTERNUMMER' );
  ins( 'FRAC', 'Fracties', 'N', 'J', 'BAS_FRACTIES', 'FRAC_ID', 'FRACTIENUMMER' );
  -- vertalingen
  ins( 'INDI', 'Indicatie teksten', 'J', 'N', 'KGC_INDICATIE_TEKSTEN', 'INDI_ID', 'OMSCHRIJVING' );
  ins( 'ONRE', 'Onderzoeksredenenen', 'J', 'N', 'KGC_ONDERZOEKSREDENEN', 'ONRE_ID', 'OMSCHRIJVING' );
  ins( 'CONC', 'Conclusie teksten', 'J', 'N', 'KGC_CONCLUSIE_TEKSTEN', 'CONC_ID', 'OMSCHRIJVING' );
  ins( 'TOTE', 'Toelichting teksten', 'J', 'N', 'KGC_TOELICHTING_TEKSTEN', 'TOTE_ID', 'OMSCHRIJVING' );
  ins( 'RETE', 'Resultaat teksten', 'J', 'N', 'KGC_RESULTAAT_TEKSTEN', 'RETE_ID', 'OMSCHRIJVING' );
  ins( 'MATE', 'Materialen', 'J', 'N', 'KGC_MATERIALEN', 'MATE_ID', 'OMSCHRIJVING' );
  ins( 'MEDE', 'Medewerker', 'J', 'N', 'KGC_MEDEWERKERS', 'MEDE_ID', 'FORMELE_NAAM' );
  ins( 'CATE', 'Categorieen', 'J', 'N', 'KGC_CATEGORIEEN', 'CATE_ID', 'OMSCHRIJVING' );
  ins( 'CAWA', 'Categorie waarden', 'J', 'N', 'KGC_CATEGORIE_WAARDEN', 'CAWA_ID', 'BESCHRIJVING' );
  -- "losse" vertalingen
  ins( 'PERS_GESL', 'Geslacht', 'J', 'N', 'KGC_PERSONEN', NULL, 'GESLACHT' );
  -- gesprekken
  ins( 'GESP', 'Gesprekken', 'J', 'N', 'KGC_GESPREKKEN', 'GESP_ID', NULL );
  ins( 'GEBE', 'Gesprek betrokkenen', 'J', 'N', 'KGC_GESPREK_BETROKKENEN', 'GEBE_ID', NULL );
  -- bestanden
  ins( 'TRGE', 'Tray gebruik', 'N', 'N', 'KGC_TRAY_GEBRUIK', 'TRGE_ID', 'WERKLIJST_NUMMER' );

END init_enti;

PROCEDURE init_prio
IS
  PROCEDURE ins
  ( p_code IN VARCHAR2
  , p_omschrijving IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   prio_rec
        from   kgc_prioriteiten
        where  code = UPPER( p_code );
        prio_tab( prio_rec.code ) := prio_rec;
      exception
        when no_data_found
        then
          p.l( p_code || ' staat niet in kgc_prioriteiten!' );
      end;
      return;
    end if;
    UPDATE kgc_prioriteiten
    SET    omschrijving = p_omschrijving
    WHERE  code = UPPER( p_code )
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_prioriteiten
      ( code
      , omschrijving
      )
      VALUES
      ( UPPER( p_code )
      , p_omschrijving
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( '1', 'Fatale fout: blokkeert verder werken' );
  ins( '2', 'Fatale fout: verder werken mogelijk' );
  ins( '4', 'Storend: verder werken gaat omslachtig' );
  ins( '5', 'Hinderlijk: verder werken kan' );
  ins( '7', 'Ongemakkelijk: het zou beter kunnen' );
  ins( '8', 'Wens / uitbreiding / aanvulling' );
  ins( '9', 'Bespreekgeval' );
  ins( '?', 'Vraag' );
END init_prio;

PROCEDURE init_sypa
IS
  PROCEDURE ins
  ( p_code IN VARCHAR2
  , p_omschrijving IN VARCHAR2
  , p_standaard_waarde IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   sypa_rec
        from   kgc_systeem_parameters
        where  code = UPPER( p_code );
        sypa_tab( sypa_rec.code ) := sypa_rec;
      exception
        when no_data_found
        then
          p.l( p_code || ' staat niet in kgc_systeem_parameters!' );
      end;
      return;
    end if;
    UPDATE kgc_systeem_parameters  sypa
    SET    sypa.omschrijving = p_omschrijving
    WHERE  sypa.code = UPPER( p_code )
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_systeem_parameters
      ( code
      , omschrijving
      , standaard_waarde
      )
      VALUES
      ( UPPER( p_code )
      , p_omschrijving
      , p_standaard_waarde
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( 'APPLICATIE_EIGENAAR', 'Eigenaar van de database-objecten van Helix', 'HELIX' );
  ins( 'ZIS_LOCATIE', 'Verwijzing naar het ZIS: NIJMEGEN, UTRECHT, MAASTRICHT', NULL );
  ins( 'DATABASE', 'Verwijzing naar de gebruikte database (KGCN/ONTW/TEST/PROD)', NULL );
  ins( 'DATABASE_DOMEIN', 'Naam van het databasedomein dat gebruikt wordt in de connectstring', NULL );
  ins( 'HELP_BESTAND', 'Naamconventie voor helpbestanden (bv. <MODULE>.HTM)', '<MODULE>.HTM' );
  ins( 'HELP_SYSTEEM_URL', 'URL waar de basis van het help systeem is geplaatst', NULL );
  ins( 'NAAM_INSTELLING_RAPPORT', 'Naam van de instelling onder aan rapporten', 'Klinisch Genetisch Centrum' );
  ins( 'SQLPLUS_EXECUTABLE', 'Programma om sql-bestanden mee uit te voeren (incl. volledige pad)', 'C:\<oracle>\BIN\SQLPLUS.EXE' );
  ins( 'ASTRAIA_FTP_UN_PW', 'ftpUserName/ftpPassword tbv. de astraia-koppeling', 'ftpUserName/ftpPassword' );
  ins( 'APPLICATIE_SERVER', 'URL van de applicatie server', NULL );
  ins( 'APP_SERVER_TEMP_DIR', 'Directory voor tijdelijke bestanden op de applicatie server', NULL);
  ins( 'REPORTS_SERVER', 'Naam van de reports server (op applicatie server)', 'rephelix' );
  ins( 'APPLICATIE_SERVER_POORT', 'Nummer van de poort waarop de applicatie server draait', '80' );
  ins( 'CLOSE_HTML', 'Het volledige pad en naam van de html om de applicatie af te sluiten.', 'http://<hostname>:<port> (meestal 7778)/forms-start/close.html' );
  ins( 'REPORTS_ENVIRONMENT', 'Geeft aan welke omgeving gebruikt moet worden voor het opstarten van reports. De mogelijke waarden zijn: ONTW , TEST en PROD ', NULL );

  ins( 'MEDEWERKER_AFDELING', 'Standaard afdeling van de medewerker', NULL );
  ins( 'MEDEWERKER_ONDERZOEKSGROEP', 'Standaard onderzoeksgroep voor een medewerker', NULL );
  ins( 'VERSIE_BASMEET01', 'Welke versie van BASMEET01 moet standaard worden aangeroepen', 'A' );
  ins( 'VERSIE_BASMEET03', 'Welke versie van BASMEET03 moet standaard worden aangeroepen', 'A' );
  ins( 'VERSIE_BASMEET25', 'Welke versie van BASMEET25 moet standaard worden aangeroepen', 'A' );
  ins( 'VERSIE_KGCONBE01', 'Welke versie van KGCONBE01 moet standaard worden aangeroepen', 'A' );
  ins( 'VERSIE_KGCWLST05', 'Welke versie van KGCWLST05 moet standaard worden aangeroepen', 'A' );
  ins( 'VERSIE_KGCTRVU51', 'Welke versie van KGCTRVU51 moet standaard worden aangeroepen', 'A' );
  ins( 'WERKLIJST_AFDRUK_PROG', 'Welke afwijkende rapport wordt gestart voor overzicht werklijst', NULL );

  ins( 'NUMMERSTRUCTUUR_FAMI', 'Gebruikte nummerstructuren voor familienummers', 'FAMI' );
  ins( 'NUMMERSTRUCTUUR_FAON', 'Gebruikte nummerstructuren voor familieonderzoeknummers', 'FAON' );
  ins( 'NUMMERSTRUCTUUR_FRAC', 'Gebruikte nummerstructuren voor fractienummers', 'FRAC' );
  ins( 'NUMMERSTRUCTUUR_ISOL', 'Gebruikte nummerstructuren voor isolatielijsten', 'ISOL' );
  ins( 'NUMMERSTRUCTUUR_ONDE', 'Gebruikte nummerstructuren voor onderzoeksnummers', 'ONDE' );
  ins( 'NUMMERSTRUCTUUR_MEGR', 'Gebruikte nummerstructuren voor metinggroepen', 'MEGR' );
  ins( 'NUMMERSTRUCTUUR_MONS', 'Gebruikte nummerstructuren voor monsternummers', 'MONS' );
  ins( 'NUMMERSTRUCTUUR_RUNS', 'Gebruikte nummerstructuren voor runnummers', 'RUNS' );
  ins( 'NUMMERSTRUCTUUR_SAMP', 'Gebruikte nummerstructuren voor samples', 'SAMP' );
  ins( 'NUMMERSTRUCTUUR_STAN', 'Gebruikte nummerstructuren voor standaardfractienummers', 'STAN' );
  ins( 'NUMMERSTRUCTUUR_WERK', 'Gebruikte nummerstructuren voor werklijstnummers', 'WERK' );
  ins( 'LETTER_FAMI', 'Vaste tekens gebruikt in het familienummer', NULL );
  ins( 'LETTER_FAON', 'Vaste tekens gebruikt in het familieonderzoeknummer', NULL );
  ins( 'LETTER_FRAC', 'Vaste tekens gebruikt in het fractienummer', NULL );
  ins( 'LETTER_ISOL', 'Vaste tekens gebruikt in het isolatielijstnummer', NULL );
  ins( 'LETTER_ONDE', 'Vaste teken(s) gebruikt in het onderzoeknummer', NULL );
  ins( 'LETTER_MEGR', 'Vaste tekens gebruikt in het metinggroepnummer', NULL );
  ins( 'LETTER_MONS', 'Vaste tekens gebruikt in het monsternummer', 'M' );
  ins( 'LETTER_RUNS', 'Vaste tekens gebruikt in het runnummer', 'R' );
  ins( 'LETTER_SAMP', 'Vaste tekens gebruikt in de samplenaam', NULL );
  ins( 'LETTER_STAN', 'Vaste tekens gebruikt in het standaardfractienummer', NULL );
  ins( 'LETTER_WERK', 'Vaste tekens gebruikt in het werklijstnummer', NULL );
  ins( 'FRACTIENUMMER_ZUIVERING', 'Toevoeging aan het fractienummer bij zuivering', 'Z' );
  ins( 'FRACTIENR_GENERATOR_EXCL_FRTY', 'Moet het volgnummer (sequence) in het fractienummer bepaald worden onafhankelijk van de fractietype-letters?', 'Nee' );

  ins( 'AUTOM_VERWYDEREN_KWEEKFRACTIES', 'Mogen de Kweekfracties Automatisch verwijderd worden  Default J', 'J' );
  ins( 'AUTORISEREN_BIJ_BEOORDELEN', 'Mag een onderzoek automatisch worden geautoriseerd bij de eerste beoordeling (standaard aanvinken na uitslagcode in KGCONDE25:  "beoordelen onderzoek").', 'J' );
  ins( 'BASMEET03C_WERKLIJST_INFO', 'Toon werklijst informatie op scherm BASMEET03C: Ja, Nee', 'N' );
  ins( 'BEREKEN_OOK_AFGEROND', 'Voer berekeningen uit op al afgeronde meetwaarden (aangemelde stoftesten).'
                   ||chr(10)|| 'REF(_SIGNAAL): alleen als meetwaarde van referentiestoftest wijzigt: _SIGNAAL betekent dat er een tussenscherm verschijnt, waarop de geraakte testen worden getoond, anders wordt de berekening direkt gedaan.'
                   ||chr(10)|| 'ALLE(_SIGNAAL): idem, maar voor alle stoftesten'
                   ||chr(10)|| 'NEE: voer deze berekeningen niet opnieuw uit'
                             , 'NEE' );
  ins( 'BEVOEGD_VERWIJDER_WERKLIJST', 'Lijst met medewerkers die bevoegd zijn om werklijsten te verwijderen (leeg=iedereen,geen beperking).', 'n.v.t.' );
  ins( 'BEVOEGD_WIJZIG_PERSOON', 'Lijst met medewerkers die bevoegd zijn om identificerende persoonsgegevens te wijzigen (leeg=allen).', 'n.v.t.' );
  ins( 'BEVOEGD_WIJZIG_ONDERZOEK', 'Lijst met medewerkers die bevoegd zijn om afgeronde onderzoeksgegevens te wijzigen (leeg=allen).', 'n.v.t.' );
  ins( 'BEVRIES_GOEDGEKEURD_ONDERZOEK', 'Geeft aan welke gegevens bij een goedgekeurd onderzoek niet gewijzigd mogen worden. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. Waarden: "N" (geen restrictie); "INDICATIE" (indicatie-gegevens); "MONSTER" (monster-gegevens; "INDICATIE,MONSTER" (indicatie- en monster-gegevens)', 'N' );
  ins( 'BUFFER_EENHEID', 'Eenheid waarin hoeveel buffer (bij monster) wordt uitgedrukt', 'ml' );
  ins( 'CATEGORIE_EINDBRIEF', '(Hoofd)categorie voor de bijlage van een eindbrief', NULL );
  ins( 'CATEGORIE_SONIFICATIE', '(Hoofd)categorie voor de sonificatielijst', NULL );
  ins( 'CDM_PROTOCOL_MEE', 'Protocolcodes die gebruikt worden in de Helix-CDM interface, omgeven door #-tekens', NULL );
  ins( 'CDM_PROTOCOL_NIET_MEE', 'Protocolcodes die uitgesloten worden in de Helix-CDM interface op basis van selectie CDM_PROTOCOL_MEE, omgeven door #-tekens', NULL );
  ins( 'CDM_XML_DIR_IN', 'Locatie waar de tijdelijke XML-bestanden (tbv van de CDM-koppeling) naar toe worden geschreven.', NULL );
  ins( 'CDM_XML_DIR_UIT', 'Locatie waar de tijdelijke XML-bestanden (tbv van de CDM-koppeling) uit worden gelezen.', NULL );
  ins( 'CONCEPT_TEKST', 'Tekst die op het rapport aanduidt dat het een concept-afdruk betreft.', NULL );
  ins( 'CONTROLE_BESTAANDE_FAMILIE', 'Moet er een waarschuwing komen als de familienaam al bestaat bij invoer van een nieuwe familie (J/N)?', 'J' );
  ins( 'CURSOR_POSITIE_IN_VELD', 'Deze parameter bepaald de positie van de cursor in SINGLE-LINE velden. De mogelijke waarden zijn: BEGIN , EIND , STANDAARD, POSITIE_HOUDEN', 'EIND' );
  ins( 'CURSOR_POSITIE_MULTI_LINE_VELD', 'Deze parameter bepaald de positie van de cursor in MULTI-LINE velden. De mogelijke waarden zijn: BEGIN , EIND ', 'EIND' );
  ins( 'DAGEN_ZWANGERSCHAP', 'Standaardduur van een zwangerschap in dagen', '280' );
  ins( 'DATUM_TIJD_VERPLICHT', 'De parameter geeft aan of de velden Datum en Tijd in de module Gesprekken (KGCGESP01) verplicht zijn. Mogelijke waarden en betekenis: J  =  Beide velden Datum en Tijd zijn verplicht. N =  Beide velden Datum en Tijd zijn niet verplicht.', 'N' );
  ins( 'DECLARABEL_GESPREKTYPE', 'Declarabel type gesprek bij counseling', NULL );
  ins( 'DECLARATIE_BEPALING', 'Welke bepaling van declarabele items wordt gebruikt (OUD, NIEUW)', 'NIEUW' );
  ins( 'EXTRA_INFO_BIJ_ONDERZOEK', 'Welke info wordt bij onderzoek getoond (MONSTER, FRACTIE, VREUGDEBRIEF)', 'VREUGDEBRIEF' );
  ins( 'FAMILIEONDERZOEK', 'Is registratie van familieonderzoek ALGEMEEN (dossier van indicatie bij een familie) of SPECIFIEK (aanvraag op datum, door aanvrager, naar indicatie)?'
               ||chr(10)|| 'De waarde beinvloedt automatismen en de layout van het familieonderzoekscherm.' , 'SPECIFIEK' );
  ins( 'FRACTIE_ONDERZOEK_BETROKKEN', 'Registreer betrokkenheid van fractie bij een onderzoek (op basis van meting)', 'N' );
  ins( 'GEBRUIK_FAMILIE', 'Wordt familie(nummer) gebruikt bij de identificatie van onderzoeken (J/N)?', 'N' );
  ins( 'GEBRUIK_GEPL_DATUM_PROTOCOL', 'Geeft aan of bij een protocol de rubriek "geplande_einddatum" gebruikt wordt. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. Waarden: "J" (wordt gebruikt); "N" (wordt niet gebruikt).', 'N' );
  ins( 'GEBRUIK_TRAY', 'Worden trays gebruikt? (J/N, extra tray-info op werklijsten)', 'N' );
  ins( 'GENIOS_BESTANDTYPE', 'Bestand type van het uitgaande bestand voor de robotstraat', NULL );
  ins( 'GENIOS_PROTOCOL', 'Stoftestgroep die gebruikt wordt voor de trays tbv. genios-concentratiemeting', NULL );
  ins( 'INDICATIE_ENTITEIT', 'Voor welke entiteiten (ONDE,MONS) worden indicaties vastgelegd', 'MONS,ONDE' );
  ins( 'INSTELLINGEN_CODE_GENERATIE', 'Door deze parameter op J te zetten wordt het veld CODE in de tabel instellingen automatisch gevuld met het INST_ID. Bij het invoeren op het scherm wordt het veld CODE optioneel en kan deze leeg gelaten worden.', 'N' );
  ins( 'KEUZE_RAPPORTEN', 'Rapportmodules die gestart kunnen worden vanuit KGCRAPP10', 'BLOEDBRIEF,ANDER_LAB' );
  ins( 'KGCFAMI05_VUL_ZOEKNAAM', 'Vul in KGCFAMI05 de zoeknaam in het met de familienaam uit KGCFAMI01. Mogelijke waarden: J,N', 'J' );
  ins( 'KGCPAHI51_AFGEROND_TEKST', 'Alternatieve uitslagtekst en resultaattekst voor het rapport KGCPAHI51 voor afgeronde onderzoeken.', 'Onderzoek is afgerond. U heeft geen bevoegdheid tot inzage van resultaat.' );
  ins( 'KGCPAHI51_NIET_AFGEROND_TEKST', 'Alternatieve uitslagtekst en resultaattekst voor het rapport KGCPAHI51 voor niet afgeronde onderzoeken.', 'Onderzoek is niet afgerond. U heeft geen bevoegdheid tot inzage van resultaat van afgeronde en niet afgeronde onderzoeken.' );
  ins( 'KGCOPTE01_LEGENDA', 'Legenda op scherm KGCOPTE01 (aangemelde metingen)'
                          , 'J = groen = afgerond'||CHR(10)||'N = geel = niet afgerond'||CHR(10)||'leeg = grijs = niet aangemeld'||CHR(10)||'* = al eerder getest' );
  ins( 'KGCUITS51_MATERIAAL_VOLGORDE', 'Volgorde waarin de protocoluitslagen moeten worden afgedrukt in de eindbrief (Maastricht Basisbrief)', NULL );
  ins( 'KLEUR_KGCKARY01', 'Gebruik kleur om afwijkingen in karyogram te tonen', 'GEEL' );
  ins( 'KWEEKFRACTIE_CONTROLE', 'Moet een kweekfractie eerst gecontroleerd zijn, voordat hij mag worden ingezet?', 'N' );
  ins( 'LOG_ISOLATIE_SCANS', 'Registreer foutieve scans bij isolatieproces (BASISOL03)', 'N' );
  ins( 'MACHTIGINGSBRIEF_NODIG', 'Machtigingsbrieven worden gemaakt voor de opgegeven "entiteiten" (ONDE=onderzoek; ONBE=betrokkene; METI=meting/deelonderzoek; GESP=gesprek/counseling; MONS=monsterafname', 'ONDE-ONBE-METI-GESP-MONS' );
  ins( 'MAIL_SERVER', 'SMTP-mail-server die via de helix-database gebruikt kan worden voor het verspreiden van email; Naam (b.v. smtp.umcn.nl) of ip-adres.', '');
  ins( 'MAX_AANTAL_METINGEN_IN_GROEP', 'Maximum aantal metingen in een groep', '20' );
  ins( 'MEETWAARDE_STRUCTUUR', 'Standaard meetwaarde invoervelden', NULL );
  ins( 'MIN_AANTAL_NORMALE_CELLEN', 'Minimum aantal normale cellen voor vermelding in karyogram', '2' );
  ins( 'MODULE_MONITOR', 'Schakelaar om het monitoren van modulegebruik AAN of UIT te zetten', 'UIT' );
  ins( 'MONSTERMATERIAAL_WIJZIGBAAR', 'is het materiaal bij een monster wijzigbaar', 'J' );

  -- Mosos-koppeling:
  ins( 'MOSOS_IMPORT_HL7_NIET_OKE', 'Parameter tbv mosos-verzendlijst niet correct verlopen berichtverwerking', NULL );
  ins( 'MOSOS_IMPORT_HL7_OKE', 'Parameter tbv mosos-verzendlijst goed verwerkte berichten', NULL );
  ins( 'MOSOS_MAIL_AFZENDER', 'Afzender Mosos E-mail-berichten', 'donotreply@mosos.nl' );
  ins( 'MOSOS_SERVERNAAM', 'Connectie tbv de Mosos interface (bijv. tdm1001.ds.umcutrecht.nl)', 'server' );
  ins( 'MOSOS_SERVERPOORT', 'Poort tbv de Mosos interface (bijv. 6595)', '0' );
  ins( 'MOSOS_ZIS_NIETGEVONDEN', 'Parameter tbv mosos-verzendlijst problematische ZIS-gegevens', NULL );

  ins( 'ONDERZOEKEN_TOEKENNEN', 'Onderzoeken worden  automatisch toegekend aan de autorisatoren (J/N)', 'N' );
  ins( 'ONDERZOEK_AUTOMATISCH_AFRONDEN', 'Mag een onderzoek zonder twijfelachtige meetwaarden automatisch worden afgerond? (J/N)', 'J' );
  ins( 'OPSLAG_TYPE_VOOR_AUTO_VULLING', 'Code van een opslag element dat gebruikt wordt om automatisch fracties in te plaatsen.', NULL );
  ins( 'OPSLAG_VERWIJDEREN', 'aangeven of opslag verwijderd mag worden. Mogelijke waarden zijn J en N', 'J' );
  ins( 'PATIENT_RANGE LEEF_CODE', 'Code leeftijdscategorie die gebruikt wordt om de patient-range te registreren', 'PAT_RANGE' );
  ins( 'PERSOONSGEGEVENS_LANG', 'Schrijfwijze lang voor persoons info', '<AANSPREKEN> (<GESLACHT>) <GEBOORTEDATUM> <OVERLEDEN> <FOETUS> <ZWANGERSCHAP>' );
  ins( 'PERSOONSGEGEVENS_MIDDEL', 'Schrijfwijze middel voor persoons info', '<ACHTERNAAM> (<GESLACHT>) <GEBOORTEDATUM> <OVERLEDEN> <FOETUS> <ZWANGERSCHAP>' );
  ins( 'PERSOONSGEGEVENS_KORT', 'Schrijfwijze kort voor persoons info', '<INITIALEN> (<GESLACHT>) <GEBOORTEDATUM>' );
  ins( 'PERSOONSGEGEVENS_BRIEF', 'Schrijfwijze voor persoons info in een brief', '<AANSPREKEN> (geb. <GEBOORTEDATUM>; <GESLACHT>)' );
  ins( 'PERSOONSGEGEVENS_VERVOLGVEL', 'Bij uitslagbrieven dit op vervolgvel tonen om kans op verwisseling te verkleinen.', 'Vervolgvel voor: <AANSPREKEN> (Gesl. <GESLACHT>, geb.: <GEBOORTEDATUM>) <OVERLEDEN> <FOETUS> <ZWANGERSCHAP>');
  ins( 'PERSOONSGEGEVENS_NIET_UIT_ZIS', 'Geef hier de kolomnamen op die niet uit ZIS komen en dus altijd in Helix beheerd mogen worden, ongeacht of de persoonsgegevens in ZIS beheerd worden.', 'PERS.HOEVEELLING, PERS.TELEFAX, PERS.EMAIL');
  ins( 'PLATE_URL', 'URL voor het direct benaderen van een plate (LIMS)', 'http://seqfac.azn.nl/servlet/PlateHandling?Process=Login2$ampUsername=<USERNAME>$ampPlate_ID=<PLATE_ID>' );
  ins( 'POSTCODE_CHECK', 'Deze parameter bepaalt hoe de postcodes gecontroleerd worden.'||chr(10)||'Mogelijke waarden: GEEN, BEPERKT, STRENG', 'BEPERKT' );
  ins( 'PROBE_MEET_MWST', 'Meetwaardestructuur voor aangemelde probes op stoftestniveau (mn. "Kleur"', null );
  ins( 'PROBE_METI_MWST', 'Meetwaardestructuur voor aangemelde probes op protocolniveau (mn. "Kwaliteit"', null );
  ins( 'RELATIE_CODE_GENERATIE', 'Door deze parameter op J te zetten wordt het veld CODE in de tabel relatie automatisch gevuld met het RELA_ID. Bij het invoeren op het scherm wordt het veld CODE optioneel en kan deze leeg gelaten worden.', 'N' );
  ins( 'ROBOTSTRAAT_INTERFACE', 'Waaraan moet de code van de interface-definities voldoen, om gebruikt te kunnen worden voor de robotstraat', '%ROBOT%' );
  ins( 'ROBOT_INTERFACE_DEFINITIE_UIT', 'Standaard interface definitie van helix naar robot', NULL );
  ins( 'ROBOT_INTERFACE_DEFINITIE_IN', 'Standaard interface definitie van robot naar helix', NULL );
  ins( 'REFERENTIE_STOFTEST', 'Code van de stoftest die als referentie moet worden opgenomen in rapporten en schermen', NULL );
  ins( 'RESTRICT_AFRONDEN_PROTOCOLLEN', 'Geeft aan of er een restrictie is op het afronden van metingen door een medewerker op scherm BASMEET03C en BASMEET01C, indien deze over de rol "ANALIST" beschikt. ' || CHR(10) || 'Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep.' || CHR(10) || 'Waarden: "J" (er is een restrictie); "N" (er is geen restrictie).', 'N' );
  ins( 'SCREENING_PROTOCOL', 'Standaard protocol voor screening onderzoeken', NULL );
  ins( 'SPOED_BOVENAAN_OP_WERKLIJST', 'In het scherm KGCWLST01 (Aanmaken werklijst) wordt de checkbox "Spoed bovenaan"  gevuld met deze waarde.'||CHR(10)||'NB: De waarde van de checkbox is leidend bij de bepaling of "spoedjes" bovenaan komen te staan in de volgorde van de werklijst.'||CHR(10)||'Waarden J of N', 'J');
  ins( 'STANDAARD_AFNAME_INDICATIE', 'Standaard machtigingsindicatie voor monsterafname', NULL );
  ins( 'STANDAARD_ARCHIEF', 'Standaard waarde voor archief bij families', NULL );
  ins( 'STANDAARD_DECLARATIEWIJZE', 'Standaard declaratiewijze voor een onderzoek', NULL );
  ins( 'STANDAARD_GESPREKTYPE', 'Standaard type gesprek bij counseling', NULL );
  ins( 'STANDAARD_FISH_INDICATIE', 'Standaard machtigingsindicatie voor FISH-onderzoek', NULL );
  ins( 'STANDAARD_FRACTIESTATUS', 'Standaard fractiestatus (Aangemeld, Opgewerkt)', NULL );
  ins( 'STANDAARD_IMPORT_DIRECTORY', 'Standaard directory voor het inlezen van (obstetrie-)bestanden', NULL );
  ins( 'STANDAARD_ONDERZOEKSBETROKKENE', 'Met welke rol moet een onderzoeksbetrokkene worden gemaakt voor de adviesvrager (onderzoek)', NULL );
  ins( 'STANDAARD_OPENEN_KGCONIN01', 'Afhankelijk van deze parameter, wordt het scherm KGCONIN01 op een bepaalde manier geopend.De mogelijkheden zijn BEKNOPT en UITGEBREID.', 'BEKNOPT' );
  ins( 'STANDAARD_OPENEN_KGCWLST16', 'Afhankelijk van deze parameter, wordt in het scherm KGCWLST16 een bepaald tabblad als eerste getoond. De mogelijkheden zijn PROTOCOL en STOFTEST.', 'PROTOCOL' );
  ins( 'STANDAARD_PRINTER', 'Een standaard printer kan ingesteld worden, deze wordt dan automatisch ingevuld in het rapportscherm.', '\\apdii\HP LaserJet 2200' );
  ins( 'STANDAARD_REDEN_NIET_DECL', 'Standaard reden niet-declarabel voor controle-fracties', NULL );
  ins( 'STANDAARD_UITSLAGBRIEFTYPE', 'Standaard brieftype voor eindbrieven (uitslagbrieven)', NULL );
  ins( 'STANDAARD_UITSLAGCODE', 'Standaard uitslagcode voor af te ronden onderzoeken', NULL );
  ins( 'TABBLAD_AANTAL_MONSTERBUIZEN', 'Op het monsterscherm staat het veld "aantal buizen" op dit tabblad (AANMELDING of OVERIG)', 'OVERIG' );
  ins( 'TOELICHTING_TEKSTEN', 'Waaruit bestaan de toelichtingteksten: Conclusie-, Resultaat-, specifieke Toelichtingteksten', 'CRT' );
  ins( 'TOON_ALLE_DETAILS_OP_WERKLIJST', 'Toon in werklijstrapportage alleen de gebruikte details (N) of alle aangemelde details (J).', 'N' );
  ins( 'TOON_FRACTIES_ONDERZOEKSBETR', 'Geeft aan of er uitsluitend fracties worden getoond van familieleden die reeds onderzoeksbetrokken zijn gemaakt.' || CHR(10) || 'Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep.' || CHR(10) || 'Waarden: "J" (er worden uitsluitend fracties getoond van familieleden die reeds onderzoeksbetrokken zijn gemaakt); "N" (fracties worden getoond van alle familieleden).', 'N' );
  ins( 'TOON_INDICATIES_SCHERMEN', 'Wijze waarop onderzoeksindicaties moeten worden getoond in de schermen.'
                       ||chr(10)|| 'Maak gebruik van <INDI_CODE>, <INDI_OMSCHRIJVING>, <ONRE_CODE>, <ONRE_OMSCHRIJVING>, <OPMERKING> om de gewenste elementen te kiezen.'
                       ||chr(10)|| 'Omsluit de tekenreeks met [] om alle indicaties te tonen, anders alleen de belangrijkste.', NULL );
  ins( 'TOON_GESPREK_ONDERZOEKSREDEN', 'Deze parameter regelt of het veld Reden (op onderzoeksniveau) wel of niet getoond wordt in het scherm KGCGESP01.', 'J' );
  ins( 'TOON_ONDERZOEKSREDEN', 'Deze parameter regelt of het veld Reden (op onderzoeksniveau) wel of niet getoond wordt.', 'J' );
  ins( 'TPMT_PROTOCOLLEN', 'Stoftestgroepcodes 9omgeven door # die de TPMT-protocollen omvatten.', NULL );
  ins( 'TRAY_INTERFACE', 'Doel van de tray informatie bij interface', NULL );
  ins( 'UITSLAGBRIEF_REFERENTIE', 'Opbouw van "onze referentie" in de uitslagbrief', '<ONDERZOEK>' );
  ins( 'VERZEKERAARS_CODE_GENERATIE', 'Door deze parameter op J te zetten wordt het veld CODE in de tabel verzekeraars automatisch gevuld met het VERZ_ID. Bij het invoeren op het scherm wordt het veld CODE optioneel en kan deze leeg gelaten worden.', 'N' );
  ins( 'VOORKEUR_RAPPORTGROEP', 'Eerstgetoonde rapportgroep', NULL );
  ins( 'VRAAG_DUPLO_TOEVOEGEN', 'Geeft aan of de gebruiker de vraag gesteld krijgt of een aanwezig protocol nogmaals aangemeld moet worden.' || CHR(10) || 'Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep.' || CHR(10) || 'Waarden: "J" (gebruiker krijgt de vraag gesteld); "N" (protocol wordt ongevraagd nogmaals aangemeld).', 'N' );
  ins( 'WEEKEND_DAGEN', 'Aantal dagen dat geteld moet worden voor een weekend (ZA/ZO) bij kweekduurberekening', '2' );
  ins( 'WORD_BEWAAR_XML', 'Moet een tijdelijk XML-bestand bewaard worden? Bewaren is alleen nodig indien de XML gecontroleerd dient te worden. J/N','N');
  ins( 'WORD_URL_ASP_SCRIPT', 'De URL voor het uitvoeren van het ASP script van de Word-koppeling', NULL );
  ins( 'WORD_XML_DIR', 'De naam van de Oracle Directory waar de tijdelijke XML-bestanden (tbv van de Word-koppeling) naar toe worden geschreven', 'XML_PATH');
  ins( 'ZISNUMMER_FORMAAT', 'formaatmasker van het zisnummer.Bijvoorbeeld 9999999 indien het zisnummer uit 7 posities bestaat. 99.999 indien op de 3e positie een punt moet komen te staan. Neem contact op met Assai voor andere mogelijkheden.', '9999999');
  ins( 'ZOEKVRAAG_VIEWS', 'Naamconventie voor specifieke views waarop zoekvragen kunnen worden gedaan', 'KGC_QUERY%' );

  -- ZIS-koppelingen:
  ins( 'HL7_VERSIE', 'Versie van HL7 die gebruikt wordt. 2.2 of 2.3', '');
  ins( 'ZIS_ADTA08_BLOK', 'Wanneer moet een wijziging geblokkeerd worden? A=als van de gegevens {geslacht, geboortedatum, achternaam} minimaal 2 van de 3 gewijzigd worden; mail naar ZIS_MAIL_ADTA08_BLOK', '');
  ins( 'ZIS_DFT_CONTROLES', 'Uit te voeren controles voor DFT-koppeling:'
                ||chr(10)|| 'A1: Van welke entiteiten mogen er declaraties verzonden worden?'
                ||chr(10)|| 'A2: Aantal dagen dat de verrichtingdatum (Datum/tijd van uitvoering van de verrichting, zie verderop voor de beschrijving van de bepaling) in het verleden mag liggen (-1 betekent geen controle)'
                ||chr(10)|| 'A3: Moet de verzekeraar van de declaratie (indien aanwezig) niet-vervallen zijn? (A3:J = Ja; N = Nee)'
                ||chr(10)|| 'A4: Moet er een indicatie en reden zijn bij het onderzoek? (A4: N = Nee; B=Beide, I=Alleen indicatie, R=alleen reden)'
                ||chr(10)|| 'A5: Mag de declaratiepersoon overleden zijn? (A5:J = Ja; N = Nee)'
                ||chr(10)|| 'A6: Moet de declaratiepersoon een ZISnummer hebben? (A6:J = Ja; N = Nee)'
                ||chr(10)|| 'A7: Moet de declaratiepersoon beheerd-in-ZIS zijn? (A7:J = Ja; N = Nee)'
                ||chr(10)|| 'A8: Moet de verrichtingcode bestaan als categorie van type VER_CODES? (A8:J = Ja; N = Nee)'
                ||chr(10)|| '(D-controles zijn voor "dummy-patienten", ofwel patienten zonder ZISnr)'
                ||chr(10)|| 'D1: Moet de declaratiepersoon zonder ZISnummer een geboortedatum hebben? (D1:J = Ja; N = Nee)'
                ||chr(10)|| 'D2: Moet de declaratiepersoon zonder ZISnummer een aanspreeknaam hebben? (D1:J = Ja; N = Nee)'
                ||chr(10)|| 'D3: Moet de declaratiepersoon zonder ZISnummer een Adres hebben? (D1:J = Ja; N = Nee)'
                ||chr(10)|| 'D4: Wat is de maximale lengte van het adres van een declaratiepersoon zonder ZISnummer? (-1 betekent geen controle)'
                ||chr(10)|| 'D5: Moet de declaratiepersoon zonder ZISnummer een postcode hebben? (D1:J = Ja; N = Nee)'
                ||chr(10)|| 'D6: Moet de declaratiepersoon zonder ZISnummer een woonplaats hebben? (D1:J = Ja; N = Nee)'
                ||chr(10)|| 'D7: Moet de declaratiepersoon zonder ZISnummer een land hebben? (D1:J = Ja; N = Nee)'
                          , 'A1,A2:-1,A3:N,A4:N,A5:J,A6:N,A7:N,A8:N,D1:N,D2:N,D3:N,D4:-1,D5:N,D6:N,D7:N');
  ins( 'ZIS_HL7_LISTENER', 'Moet de HL7-listener aan of uit staan? AAN of UIT', 'UIT');
  ins( 'ZIS_HL7_LOG_LEVEL', 'Code voor logging HL7-functies. Pos1: 0=alles uit, 1=aan; Pos2: 0=uit, 1=dbmsoutput aan; Pos3: 0=uit, 1=logtabel vullen; Pos4: 0=uit, 1=Mail naar email in Pos11+; Pos4-10 niet gebruik; Pos11+: email logger', '0000XXXXXXemailadreshier');
  ins( 'ZIS_HL7_SVR_LISTEN_TO_MSG', 'Ontvangen berichten die Helix kan verwerken, gescheiden door een komma, en voorafgegaan door MSA-1: de code die Helix moet terugsturen indien het bericht niet in de lijst staat! Dus b.v. AR,ADT^A08,ADT^A24', 'AR,ADT^A08');
  ins( 'ZIS_HL7_SVR_NAAM_DFT', 'Server die beschikbaar is om HL7-DFT-berichten te verwerken', '');
  ins( 'ZIS_HL7_SVR_NAAM_QRY', 'Server die beschikbaar is om HL7-QRY-berichten te beantwoorden', '');
  ins( 'ZIS_HL7_SVR_POORT_DFT', 'Poort van ZIS_HL7_SVR_NAAM_DFT die beschikbaar is om HL7-DFT-berichten te verwerken, gevolgd door de databasenaam. B.v. 6423,testias.umcn.nl', '');
  ins( 'ZIS_HL7_SVR_POORT_LISTENER', 'Poort van Helix-database-server die beschikbaar is om HL7-berichten te ontvangen, gevolgd door de databasenaam. B.v. 6424,testias.umcn.nl', '');
  ins( 'ZIS_HL7_SVR_POORT_QRY', 'Poort van ZIS_HL7_SVR_NAAM_QRY die beschikbaar is om HL7-QRY-berichten te beantwoorden, gevolgd door de databasenaam. B.v. 6425,iastest.ds.umcutrecht.nl', '');
  ins( 'ZIS_MAIL_ADTA08_BLOK', 'Email voor het versturen van een bericht uit de ZIS-programmatuur wanneer het niet lukt om een persoon bij te werken vanwege b.v. een lock.', '');
  ins( 'ZIS_MAIL_ADTA08_INFO', 'Email voor het versturen van een informatie uit de ZIS-programmatuur wanneer het gelukt is om een persoon bij te werken. Per gebruiker kan aangegeven wanneer het bericht verzonden moet worden. A=Altijd; O=er is een Onderzoek van de afdeling van de gebruiker aanwezig; L=er is een Lopend, niet afgerond onderzoek van de afdeling van de gebruiker aanwezig. Bij de code kan ook een conditie opgenomen worden: het veld dat gewijzigd is - b.v. #geboortedatum# - dan krijgt deze gebruiker alleen een bericht wanneer de geboortedatum gewijzigd is', '');
  ins( 'ZIS_MAIL_ADTA24', 'Email voor het versturen van boodschappen uit de ZIS-programmatuur wanneer een ADT-A24-bericht (=link, koppelen) is ontvangen.', '');
  ins( 'ZIS_MAIL_ADTA37', 'Email voor het versturen van boodschappen uit de ZIS-programmatuur wanneer een ADT-A37-bericht (=unlink, ontkoppelen; zeldzaam!) is ontvangen.', '');
  ins( 'ZIS_MAIL_AFZENDER', 'Standaardwaarde voor de afzender van emailberichten uit de ZIS-programmatuur.', 'helix-zis');
  ins( 'ZIS_MAIL_ERR', 'Email voor het versturen van foutboodschappen uit de ZIS-programmatuur.', '');

  -- Mantis 0283, BSN:
  ins( 'BSN_HANDM_AANPASSEN_TOEGESTAAN', 'Geeft aan of het BSN door een medewerker aangepast mag worden. ' || CHR(10) || 'Kan alleen ingesteld worden op het niveau van centrum.' || CHR(10) || 'Waarden: "J" (aanpassen toegestaan); "N" (aanpassen niet toegestaan).', 'N');

  -- Mantis 1710, Astraia:
  ins( 'ASTRAIA_MAIL_ERR', 'De foutboodschappen uit de Astraia-programmatuur worden per mail verzonden aan alle medewerkers die opgenomen zijn bij deze systeemparameter.', '');

  -- Mantis 1650, bijwerken foetus-geslacht:
  ins( 'UITSLAGTEKSTEN_FOETUS_M', 'Geeft aan bij welke uitslagteksten het foetus-geslacht in "M" gewijzigd wordt. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. De uitslagteksten moeten in hoofdletters, zonder spaties en door een komma van elkaar gescheiden worden.', 'NORMAALMANNELIJKKARYOTYPE,NORMAALMANNELIJKPROFIEL');
  ins( 'UITSLAGTEKSTEN_FOETUS_V', 'Geeft aan bij welke uitslagteksten het foetus-geslacht in "V" gewijzigd wordt. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. De uitslagteksten moeten in hoofdletters, zonder spaties en door een komma van elkaar gescheiden worden.', 'NORMAALVROUWELIJKKARYOTYPE,NORMAALVROUWELIJKPROFIEL');

  -- Mantis 236, kgcopte01:
  ins( 'KGCOPTE01_AFWIJKING_WEERGAVE', 'Wijze waarop de indicaties Mutatie, Polymorfisme en UV bepaald en weergegeven moeten worden (via prompt of waarde).', 'PROMPT');

  -- Pedigree-koppeling:
  ins( 'AUTOMATISCH_STARTEN_KGCPEDI01', 'Geeft aan of bij het opstarten van Helix, KGCPEDI01 automatisch moet worden opgestart. Kan ingesteld worden op het niveau van medewerker. Waarden: "J" (wel opstarten); "N" (niet opstarten).', 'N');
  ins( 'DATABASE_SIDNAAM', 'De SID-naam van de database. Kan alleen ingesteld worden op het niveau van locatie.', NULL);
  ins( 'DATABASE_SERVER_IPADRES', 'Het IP-adres van de database-server. Kan alleen ingesteld worden op het niveau van locatie.', NULL);
  ins( 'DATABASE_SERVER_POORT', 'Het poortnummer van de database-listener. Kan alleen ingesteld worden op het niveau van locatie.', NULL);
  -- Fusion-upgrade:
  ins( 'JAVA_JRE_LIB_SECURITY_PATH', 'Paden door commas gescheiden, naar de lib\security folder van de JAVA-runtime environment tbv. de Pedigree-koppeling. Kan alleen ingesteld worden op het niveau van Locatie. Voorbeeld: C:\Program Files\Java\jre6\lib\security,C:\Program Files (x86)\Java\jre6\lib\security', 'C:\Program Files\Java\jre6\lib\security,C:\Program Files (x86)\Java\jre6\lib\security');

  -- Mantis 1585, CDM:
  ins( 'CDM_MAIL_AFZENDER', 'Afzender van de E-mail-berichten vanuit het batch-proces "verwerk XML-bestanden van CDM". Kan alleen ingesteld worden op het niveau van Locatie. Voorbeeld: dontreply@cdm.nl', NULL);
  ins( 'CDM_MAIL_ERR', 'Foutboodschappen uit het batch-proces "verwerk XML-bestanden van CDM" worden verzonden naar het E-mail adres van alle medewerkers die opgenomen zijn bij deze systeemparameter.', NULL);
  ins( 'CDM_FTP_UN_PW', 'ftpUserName/ftpPassword tbv. het batch-proces "verwerk XML-bestanden van CDM". Kan alleen ingesteld worden op het niveau van Locatie. Voorbeeld: helixftp/helix', NULL);
  ins( 'CDM_XML_DIR_UIT_FTP', 'FTP-servernaam/FTP-padaanduiding tbv. het batch-proces "verwerk XML-bestanden van CDM". Kan alleen ingesteld worden op het niveau van Locatie. Voorbeeld: ftp://ftpserverhelix/CDM_uit/Produktie/', NULL);

  -- Mantis 1255, kgcpahi05:
  ins( 'TOON_ALLEEN_ONDERZOEKSPERSOON', 'Geeft aan welke onderzoeken waarbij de persoon geregistreerd is, in KGCPAHI05 standaard getoond worden. Kan alleen ingesteld worden op het niveau van medewerker. Waarden: "J" (alleen onderzoeken tonen waarbij de persoon de hoedanigheid heeft van onderzoekspersoon); "N" (tevens onderzoeken tonen waarbij de persoon de hoedanigheid heeft van onderzoeksbetrokkene).', 'N');

  -- Mantis 1945 electronisch ondertekenen:
  ins( 'TUSSENTIJDSE_UITSLAGEN_TOEGEST', 'Geeft aan of het toegestaan is dat uitslagen tussentijds geautoriseerd worden. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. Waarden: "J" (toegestaan); "N" (niet toegestaan).', 'N');
  ins( 'REDEN_DEAUTORISEREN_OPGEVEN', 'Geeft aan of bij het de-autoriseren van een onderzoek een de-autorisatiereden moet worden opgegeven. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. Waarden: "J" (reden verplicht); "N" (reden niet verplicht).', 'N');
  ins( 'APPLICATIE_SERVER_PDF_COMMAND', 'Het commando dat wordt gebruikt bij het printen van een pdf bestand vanaf de server. Kan ingesteld worden op het niveau van Locatie.'
                             ||chr(10)||'Het eerste gedeelte dient de verwijzing te bevatten naar de executable van acrobat (zie eventueel het bijbehorende printto commando in de registry). Soms moet dit commando voorafgegaan worden door " en na de .exe worden afgesloten met een ".'
                             ||chr(10)||'Het tweede gedeelte (vanaf /t) betreft parameters. Dit gedeelte dient ongewijzigd te blijven.'
                                       , NULL);
  ins( 'CONTROLE_NIETAFGEROND_PROTOCOL', 'Geeft aan of bij het autoriseren van een onderzoek gecontroleerd moet worden of er niet-afgeronde protocollen aanwezig zijn. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. Waarden: "J" (controle uitvoeren); "N" (controle niet uitvoeren).', 'J');
  ins( 'CONTROLE_UITSLAGCODE', 'Geeft aan of bij het autoriseren van een onderzoek gecontroleerd en gesignaleerd moet worden indien de uitslagcode bij het onderzoek ontbreekt. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. Waarden: "V" = Verplichte registratie van uitslagcode bij autorisatie onderzoek (foutmelding), "O" = Optionele registratie van uitslagcode (niet verplicht maar wel gewenst, waarschuwing), "G" = Geen controle op registratie uitslagcode (uitslagcode mag bij autorisatie ontbreken).', 'G');
  ins( 'AUTORISATIEDATUM_WIJZIGBAAR', 'Geeft aan of de autorisatiedatum mag worden gewijzigd. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep. Waarden: "J" (wijzigbaar); "N" (niet wijzigbaar).', 'J');
  ins( 'STANDAARD_OPENEN_KGCBEST01', 'Geeft aan voor welke bestands-extensies in scherm KGCUITS01 bij "print kopie", scherm KGCBEST01 moet worden opgestart. Kan ingesteld worden op het niveau van afdeling en onderzoeksgroep.'
                         ||chr(10)|| 'Een extensie moet in kleine letters en zonder punt opgegeven worden. Meerdere extensies moeten zonder spaties en enkel door een komma van elkaar gescheiden worden opgegeven. Voorbeeld: "doc,docx".'
                                   , NULL);

  -- Mantis 2303 terugzetten tray:
  ins( 'BEVESTIG_ZET_TERUG_FRAC_TRAY', 'Geeft aan dat bij het opschonen van een tray om een bevestiging moet worden gevraagd. Kan ingesteld worden op het niveau van afdeling en medewerker. Waarden: "J" (vragen om bevestiging); "N" (niet vragen om bevestiging).', 'J');

END init_sypa;

--  02022004 : GBR  Voeg rapport modules toe als systeemparameters (tbv definitie afwijkende rapporten)
PROCEDURE init_mods
IS
  CURSOR rapp_cur IS
    SELECT SUBSTR( mods.short_name, 1, 9 )  code
    ,      mods.title
    FROM   qms_modules  mods
    WHERE  SUBSTR( mods.short_name, 1, 3 ) IN ( 'BAS', 'KGC' )
    AND    mods.mtype = 'REPORT'
    ;
  v_waarde  kgc_systeem_parameters.code%TYPE;
BEGIN
return; -- hoeft niet, kan systeembeheer wel zelf!!!!
  FOR r IN rapp_cur
  LOOP
    IF r.code = 'KGCTRVU51'
    THEN
      v_waarde := r.code||'A';
    ELSE
      v_waarde := r.code;
    END IF;

    UPDATE kgc_systeem_parameters  sypa
    SET    sypa.omschrijving       = r.title
    ,      sypa.standaard_waarde   = NVL(sypa.standaard_waarde,v_waarde)
    WHERE  sypa.code               = r.code
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_systeem_parameters (
               code
      ,        omschrijving
      ,        standaard_waarde
      )
      VALUES ( r.code
      ,        r.title
      ,        v_waarde
      );
    END IF;
  END LOOP;
END init_mods;

PROCEDURE init_nust
IS
  PROCEDURE ins
  ( p_code IN VARCHAR2
  , p_omschrijving IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   nust_rec
        from   kgc_nummerstructuur
        where  code = UPPER( p_code );
        nust_tab( nust_rec.code ) := nust_rec;
      exception
        when no_data_found
        then
          p.l( p_code || ' staat niet in kgc_nummerstructuur!' );
      end;
      return;
    end if;
    UPDATE kgc_nummerstructuur
    SET    omschrijving = p_omschrijving
    WHERE  code = UPPER( p_code )
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_nummerstructuur
      ( code
      , omschrijving
      )
      VALUES
      ( UPPER( p_code )
      , p_omschrijving
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( 'ONDE', 'Standaard onderzoeknummer' );
  ins( 'FAON', 'Standaard familieonderzoeknummer' );
  ins( 'FAMI', 'Standaard familienummer' );
  ins( 'MONS', 'Standaard monsternummer' );
  ins( 'FRAC', 'Standaard fractienummer' );
  ins( 'STAN', 'Standaard standaardfractienummer' );
  ins( 'WERK', 'Standaard werklijstnummer' );
  ins( 'RUNS', 'Standaard runnummer' );
  ins( 'ISOL', 'Standaard isolatielijstnummer' );
  ins( 'MEGR', 'Standaard metinggroep' );
  ins( 'SAMP', 'Standaard samplenaam' );
END init_nust;

-- LST 08-09-2004 initialiseer kgc_opslag_entiteiten met de entiteiten die opgeslagen kunnen worden
PROCEDURE init_open
IS
  PROCEDURE ins
  ( p_code IN VARCHAR2
  , p_omschrijving IN VARCHAR2
  , p_tabelnaam IN VARCHAR2
  , p_vervallen IN VARCHAR2
  , p_code_kolom IN VARCHAR2
  , p_where_clause IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   open_rec
        from   kgc_opslag_entiteiten
        where  code = UPPER( p_code );
        open_tab( open_rec.code ) := open_rec;
      exception
        when no_data_found
        then
          p.l( p_code || ' staat niet in kgc_opslag_entiteiten!' );
      end;
      return;
    end if;
    UPDATE kgc_opslag_entiteiten
    SET    omschrijving = p_omschrijving
    ,      tabelnaam = p_tabelnaam
    ,      vervallen = p_vervallen
    ,      code_kolom = p_code_kolom
    ,      where_clause = p_where_clause
    WHERE  code = UPPER( p_code )
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_opslag_entiteiten
      ( code
      , omschrijving
      , tabelnaam
      , vervallen
      , code_kolom
      , where_clause
      )
      VALUES
      ( UPPER( p_code )
      , p_omschrijving
      , p_tabelnaam
      , p_vervallen
      , p_code_kolom
      , p_where_clause
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( 'BRIE'
     , 'Brieven'
     , 'KGC_BRIEVEN'
     , 'N'
     , 'brty.code||'' ''||brie.datum_print'
     , ', kgc_brieftypes brty where brty.brty_id = brie.brty_id'
     );
  ins( 'MONS'
     , 'Monsters'
     , 'KGC_MONSTERS'
     , 'N'
     , 'monsternummer'
     , NULL
     );
  ins( 'FRAC'
     , 'Fracties'
     , 'BAS_FRACTIES'
     , 'N'
     , 'fractienummer'
     , NULL
     );
  ins( 'KWFR'
     , 'Kweekfracties'
     , 'KGC_KWEEKFRACTIES'
     , 'N'
     , 'frac.fractienummer||'' ''||kwfr.code'
     , ', bas_fracties frac where frac.frac_id = kwfr.frac_id'
     );
END init_open;

-- Initialiseer kgc_attributen met de systeem-attributen (in helix-programmatuur gebruikte elementen, eigenlijk kolommen)
PROCEDURE init_attr
IS
  PROCEDURE ins
  ( p_tabel_naam IN VARCHAR2
  , p_code IN VARCHAR2
  , p_omschrijving IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   attr_rec
        from   kgc_attributen
        where  code = UPPER( p_code );
        attr_tab( attr_rec.code ) := attr_rec;
      exception
        when no_data_found
        then
          p.l( p_code || ' staat niet in kgc_attributen!' );
      end;
      return;
    end if;
    UPDATE kgc_attributen
    SET    tabel_naam = p_tabel_naam
    ,      omschrijving = p_omschrijving
    WHERE  code = UPPER( p_code )
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_attributen
      ( tabel_naam
      , code
      , omschrijving
      , systeem
      , vervallen
      )
      VALUES
      ( p_tabel_naam
      , UPPER( p_code )
      , p_omschrijving
      , 'J'
      , 'N'
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( 'KGC_MATERIALEN'
     , 'CATEGORIE_EINDBRIEF'
     , 'Hoofdcategorie voor de bijlage van de eindbrief (code in referentietabel)'
     );
  ins( 'KGC_MATERIALEN'
     , 'TOON_ZIEKTEBEELD'
     , 'Toon ziektebeeld-attribuut in bijlage van eindbrief (J/N)'
     );
  ins( 'KGC_STOFTESTEN'
     , 'ZIEKTEBEELD'
     , 'Ziektebeeld bij deze stoftest (vrije tekst)'
     );
  ins( 'KGC_STOFTESTEN'
     , 'MEAN_SD'
     , 'Mean en standaard deviatie voor deze stoftest (TPMT-brief)'
     );
  ins( 'KGC_STOFTESTEN'
     , 'RANGE'
     , 'Range van de meetwaarden voor deze stoftest (TPMT-brief)'
     );
  ins( 'KGC_STOFTESTEN'
     , 'SAMPLE'
     , 'Tekst gebruikt bij het genereren van samplenamen ipv. stotest-omschrijving'
     );
  ins( 'KGC_MEDEWERKERS'
     , 'EMPOWER_USER'
     , 'Usernaam waarmee in Empower wordt ingelogd'
     );
  ins( 'KGC_MEDEWERKERS'
     , 'EMPOWER_PASSWORD'
     , 'Password waarmee in Empower wordt ingelogd'
     );
  ins( 'KGC_STOFTESTGROEPEN'
     , 'VERRICHTINGCODE'
     , 'Verrichtingcode bij declaraties op protocol-nivo'
     );
  ins( 'KGC_ONDERZOEKSWIJZEN'
     , 'VERRICHTINGCODE'
     , 'Verrichtingcode bij declaraties per onderzoekswijze'
     );
  ins( 'KGC_ONDERZOEKSGROEPEN'
     , 'VERRICHTINGCODE'
     , 'Verrichtingcode bij declaraties per onderzoeksgroep'
     );
  ins( 'KGC_ONDERZOEKSGROEPEN'
     , 'AANVRAGENDE_AFDELING'
     , 'Aanvragende afdeling in DFT-koppeling'
     );
  ins( 'KGC_ONDERZOEKSGROEPEN'
     , 'PRODUCERENDE_AFDELING'
     , 'Producerende afdeling in DFT-koppeling'
     );
  ins( 'KGC_ONDERZOEKSGROEPEN'
     , 'AANVRAGENDE_AFDELING'
     , 'Aanvragende afdeling in DFT-koppeling'
     );
END init_attr;

-- Initialiseer kgc_export_parameters met de tot nu toe gedefinieerde waarden
PROCEDURE init_expa
IS
  PROCEDURE ins
  ( p_tabel_naam IN VARCHAR2
  , p_kolom_naam IN VARCHAR2
  , p_export_tabel IN VARCHAR2
  , p_export_kolom IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   expa_rec
        from   kgc_export_parameters
        where  tabel_naam = UPPER( p_tabel_naam )
        and    kolom_naam = UPPER(p_kolom_naam);
        expa_tab( expa_rec.tabel_naam||'.'||expa_rec.kolom_naam ) := expa_rec;
      exception
        when no_data_found
        then
          p.l( p_tabel_naam||'.'||p_kolom_naam || ' staat niet in kgc_export_parameters!' );
      end;
      return;
    end if;
    UPDATE kgc_export_parameters
    SET    export_tabel = p_export_tabel
   ,       export_kolom = p_export_kolom
    WHERE  tabel_naam = UPPER( p_tabel_naam )
    AND kolom_naam = UPPER(p_kolom_naam)
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_export_parameters
      ( tabel_naam
      , kolom_naam
      , export_tabel
      , export_kolom
      )
      VALUES
      ( p_tabel_naam
      , p_kolom_naam
      , p_export_tabel
      , p_export_kolom
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( 'BAS_ROBOT_INVOER'
     , 'PATIENTNAAM'
     , 'VIAL'
     , 'NAAM'
     );
  ins( 'BAS_ROBOT_INVOER'
     , 'ONDERZOEKNR'
     , 'VIAL'
     , 'ONDERZOEKSNUMMER'
     );
  ins( 'BAS_ROBOT_INVOER'
     , 'MEETWAARDE'
     , 'VIAL'
     , 'MEETWAARDE'
     );
  ins( 'BAS_ROBOT_INVOER'
     , 'PROTOCOL_CODE'
     , 'VIAL'
     , 'PROTOCOL'
     );
  ins( 'BAS_ROBOT_INVOER'
     , 'MONSTERNUMMER'
     , 'VIAL'
     , 'LABNUMMER'
     );
END init_expa;

-- Initialiseer kgc_empower_functions met de tot nu toe gedefinieerde waarden
PROCEDURE init_emfs
IS
  PROCEDURE ins
  ( p_emfs_id IN NUMBER
  , p_omschrijving IN VARCHAR2
  , p_constante IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   emfs_rec
        from   kgc_empower_functions
        where  emfs_id = p_emfs_id;
        emfs_tab( emfs_rec.emfs_id ) := emfs_rec;
      exception
        when no_data_found
        then
          p.l( p_emfs_id || ' staat niet in kgc_empower_functions!' );
      end;
      return;
    end if;
    UPDATE kgc_empower_functions
    SET    omschrijving = p_omschrijving
   ,       empo_constante = p_constante
    WHERE  emfs_id = p_emfs_id
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_empower_functions
      ( emfs_id
      , omschrijving
      , empo_constante
      )
      VALUES
      ( p_emfs_id
      , p_omschrijving
      , p_constante
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( 1
     , 'Inject samples'
     , 'mtkInjectSamples'
     );
  ins( 2
     , 'Inject narrow sample'
     , 'mtkInjectNarrowSample'
     );
  ins( 3
     , 'Inject broad sample'
     , 'mtkInjectBroadSample'
     );
  ins( 4
     , 'Inject standards'
     , 'mtkInjectStandards'
     );
  ins( 6
     , 'Inject narrow standard'
     , 'mtkInjectNarrowStandards'
     );
  ins( 7
     , 'Inject broad standard'
     , 'mtkInjectBroadStandards'
     );
  ins( 8
     , 'Clear Calibration'
     , 'mtkClearCalibration'
     );
  ins( 9
     , 'Equilibrate'
     , 'mtkEquilibrate'
     );
  ins( 12
     , 'Report'
     , 'mtkReport'
     );
  ins( 14
     , 'Quantitate'
     , 'mtkQuantitate'
     );
  ins( 15
     , 'Calibrate'
     , 'mtkCalibrate'
     );
  ins( 16
     , 'mtkCondition'
     , 'mtkCondition'
     );
  ins( 23
     , 'PurgeInj'
     , 'mtkPurgeInj'
     );
  ins( 24
     , 'PurgeDetector'
     , 'mtkPurgeDetector'
     );
  ins( 27
     , 'EasyTune'
     , 'mtkEasyTune'
     );
END init_emfs;

-- Initialiseer kgc_empower_processing met de tot nu toe gedefinieerde waarden
PROCEDURE init_empg
IS
  PROCEDURE ins
  ( p_empg_id IN NUMBER
  , p_omschrijving IN VARCHAR2
  , p_constante IN VARCHAR2
  )
  IS
  BEGIN
    if ( g_toon_afwijkingen )
    then
      begin
        select *
        into   empg_rec
        from   kgc_empower_processing
        where  empg_id = p_empg_id;
        empg_tab( empg_rec.empg_id ) := empg_rec;
      exception
        when no_data_found
        then
          p.l( p_empg_id || ' staat niet in kgc_empower_processing!' );
      end;
      return;
    end if;
    UPDATE kgc_empower_processing
    SET    omschrijving = p_omschrijving
   ,       empo_constante = p_constante
    WHERE  empg_id = p_empg_id
    ;
    IF ( sql%rowcount = 0 )
    THEN
      INSERT INTO kgc_empower_processing
      ( empg_id
      , omschrijving
      , empo_constante
      )
      VALUES
      ( p_empg_id
      , p_omschrijving
      , p_constante
      );
    END IF;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END ins;
BEGIN
  ins( 1
     , 'Dont process or report'
     , 'mtkDontProcessOrReport'
     );
  ins( 2
     , 'Dont report'
     , 'mtkDontReport'
     );
  ins( 3
     , 'Normal'
     , 'mtkNormal'
     );
  ins( 4
     , 'Ignore faults'
     , 'mtkIgnoreFaults'
     );
  ins( 5
     , 'No Sys Suit'
     , 'mtkNoSySuit'
     );
END init_empg;

PROCEDURE init
IS
BEGIN
  init_enti;
  init_nust;
  init_prio;
  init_sypa;
--  init_mods; nergens goed voor
  init_open;
  init_attr;
  init_expa;
  init_emfs;
END init;

procedure afwijkende_waarden
( p_tabel in varchar2 := null
)
is
  cursor enti_cur
  is
    select code
    ,      omschrijving
    from   kgc_entiteiten
    order by code asc
    ;
  cursor prio_cur
  is
    select code
    ,      omschrijving
    from   kgc_prioriteiten
    order by code asc
    ;
  cursor sypa_cur
  is
    select code
    ,      omschrijving
    from   kgc_systeem_parameters
    order by code asc
    ;
  cursor nust_cur
  is
    select code
    ,      omschrijving
    from   kgc_nummerstructuur
    order by code asc
    ;
  cursor open_cur
  is
    select code
    ,      omschrijving
    from   kgc_opslag_entiteiten
    order by code asc
    ;
  cursor attr_cur
  is
    select code
    ,      omschrijving
    from   kgc_attributen
    order by code asc
    ;
  cursor expa_cur
  is
    select tabel_naam
    ,      kolom_naam
    from   kgc_export_parameters
    order by tabel_naam asc
    ,        kolom_naam asc
    ;
  cursor emfs_cur
  is
    select emfs_id
    ,      omschrijving
    from   kgc_empower_functions
    order by emfs_id asc
    ;
  cursor empg_cur
  is
    select empg_id
    ,      omschrijving
    from   kgc_empower_processing
    order by empg_id asc
    ;
begin
  if ( nvl( p_tabel, 'ENTI' ) = 'ENTI' )
  then
    p.l( '' );
    p.l( 'Niet-systeem entiteiten' );
    g_toon_afwijkingen := true;
    init_enti;
    g_toon_afwijkingen := false;
    for r in enti_cur
    loop
      if ( enti_tab.exists( r.code ) )
      then
        null;
      else
        p.l( r.code||' : '||r.omschrijving );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'PRIO' ) = 'PRIO' )
  then
    p.l( '' );
    p.l( 'Niet-systeem prioriteiten' );
    g_toon_afwijkingen := true;
    init_prio;
    g_toon_afwijkingen := false;
    for r in prio_cur
    loop
      if ( prio_tab.exists( r.code ) )
      then
        null;
      else
        p.l( r.code||' : '||r.omschrijving );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'SYPA' ) = 'SYPA' )
  then
    p.l( '' );
    p.l( 'Niet-systeem systeemparameters' );
    g_toon_afwijkingen := true;
    init_sypa;
    g_toon_afwijkingen := false;
    for r in sypa_cur
    loop
      if ( sypa_tab.exists( r.code ) )
      then
        null;
      else
        p.l( r.code||' : '||r.omschrijving );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'NUST' ) = 'NUST' )
  then
    p.l( '' );
    p.l( 'Niet-systeem nummerstructuur' );
    g_toon_afwijkingen := true;
    init_nust;
    g_toon_afwijkingen := false;
    for r in nust_cur
    loop
      if ( nust_tab.exists( r.code ) )
      then
        null;
      else
        p.l( r.code||' : '||r.omschrijving );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'OPEN' ) = 'OPEN' )
  then
    p.l( '' );
    p.l( 'Niet-systeem opslag-entiteiten' );
    g_toon_afwijkingen := true;
    init_open;
    g_toon_afwijkingen := false;
    for r in open_cur
    loop
      if ( open_tab.exists( r.code ) )
      then
        null;
      else
        p.l( r.code||' : '||r.omschrijving );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'ATTR' ) = 'ATTR' )
  then
    p.l( '' );
    p.l( 'Niet-systeem attributen' );
    g_toon_afwijkingen := true;
    init_attr;
    g_toon_afwijkingen := false;
    for r in attr_cur
    loop
      if ( attr_tab.exists( r.code ) )
      then
        null;
      else
        p.l( r.code||' : '||r.omschrijving );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'EXPA' ) = 'EXPA' )
  then
    p.l( '' );
    p.l( 'Niet-systeem export-parameters' );
    g_toon_afwijkingen := true;
    init_expa;
    g_toon_afwijkingen := false;
    for r in expa_cur
    loop
      if ( expa_tab.exists( r.tabel_naam||'.'||r.kolom_naam ) )
      then
        null;
      else
        p.l( r.tabel_naam||'.'||r.kolom_naam );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'EMFS' ) = 'EMFS' )
  then
    p.l( '' );
    p.l( 'Niet-systeem empower-functions' );
    g_toon_afwijkingen := true;
    init_emfs;
    g_toon_afwijkingen := false;
    for r in emfs_cur
    loop
      if ( emfs_tab.exists( r.emfs_id ) )
      then
        null;
      else
        p.l( r.emfs_id||': '||r.omschrijving );
      end if;
    end loop;
  end if;
  if ( nvl( p_tabel, 'EMPG' ) = 'EMPG' )
  then
    p.l( '' );
    p.l( 'Niet-systeem empower-processing' );
    g_toon_afwijkingen := true;
    init_empg;
    g_toon_afwijkingen := false;
    for r in empg_cur
    loop
      if ( empg_tab.exists( r.empg_id ) )
      then
        null;
      else
        p.l( r.empg_id||': '||r.omschrijving );
      end if;
    end loop;
  end if;
end afwijkende_waarden;
END kgc_init_00;
/

/
QUIT
