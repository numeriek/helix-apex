CREATE OR REPLACE PACKAGE "HELIX"."KGC_VREUGDEBRIEF_00" IS
-- moet er een vreugdebrief worden gemaakt?
FUNCTION vreugdebrief_onderzoek
( p_onde_id NUMBER
, p_eerder_geprint VARCHAR2 := 'N'
)
RETURN VARCHAR2;
--PRAGMA RESTRICT_REFERENCES( vreugde_onderzoek, WNDS, WNPS );
--
-- is er reden tot vreugde (J/N)
-- alle onderzoeken hebben uitslagcode ... ('1' voor cyto.prenataal)
-- NB.: Bedenk iets slims (dynamischer, minder hard-coded)
-- geen van de onderzoeken heeft indicatie ... (nog te doen)
FUNCTION vreugde
( p_zwan_id IN NUMBER
, p_onui_code IN VARCHAR2 := NULL  -- overruled de db-waarde
)
RETURN VARCHAR2;
--PRAGMA RESTRICT_REFERENCES( vreugde, WNDS, WNPS, RNPS );

-- retourneer de onderzoeksnummer(s)
FUNCTION referentie
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( referentie, WNDS, WNPS, RNPS );

-- retourneer de verrichting op basis van het monstermateriaal van foetus,
-- gebruikt in een onderzoek tijdens de zwangerschap
FUNCTION verrichting
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( verrichting, WNDS, WNPS, RNPS );

-- retourneer het geslacht van de foetus, indien de ouder het wil weten
FUNCTION info_geslacht
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( info_geslacht, WNDS, WNPS, RNPS );

-- retourneer de uitgevoerde deelonderzoeken (metingen,protocollen)
FUNCTION metingen
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( metingen, WNDS, WNPS, RNPS );

-- retourneer J als patiente het geslacht wil weten, N als niet, leeg als niet van toepassing
FUNCTION geslacht_in_uitslag
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( geslacht_in_uitslag, WNDS, WNPS, RNPS );

-- retourneer de ondertekenaar
FUNCTION ondertekenaar
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( ondertekenaar, WNDS, WNPS, RNPS );

-- bepaalt systeem (zie criteria bij "vreugde") of een vreugdebrief mag worden gemaakt
-- of bepaalt gebruiker dat: Ja of Nee
-- Per onderzoek!
FUNCTION brief_indicator
( p_onde_id IN NUMBER
, p_onui_code IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
PROCEDURE zet_brief_indicator
( p_onde_id IN NUMBER
, p_indicator IN VARCHAR2 -- S, J, N
);
END KGC_VREUGDEBRIEF_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_VREUGDEBRIEF_00" IS
FUNCTION vreugdebrief_onderzoek
( p_onde_id NUMBER
, p_eerder_geprint VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR zwon_cur
  IS
    SELECT zwon.zwan_id
    ,      zwon.vreugdebrief
    FROM   kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = p_onde_id
    AND    zwon.datum_binnen >= add_months( sysdate, -3 )
    AND    kgc_brie_00.vreugdebrief_reeds_geprint( zwon.zwan_id )= p_eerder_geprint
    AND    nvl( zwon.vreugdebrief, 'S' ) <> 'N'
    ;
  zwon_rec zwon_cur%rowtype;
BEGIN
  OPEN  zwon_cur;
  FETCH zwon_cur INTO zwon_rec;
  CLOSE zwon_cur;
  IF ( zwon_rec.zwan_id is not null )
  THEN
    if ( zwon_rec.vreugdebrief = 'J' )
    then  -- systeem wordt overruled
      v_return := 'J';
    else
      v_return := vreugde( p_zwan_id => zwon_rec.zwan_id );
    end if;
  ELSE
    v_return := 'N';
  END IF;
  RETURN(v_return);
END vreugdebrief_onderzoek;
---
FUNCTION vreugde
( p_zwan_id IN NUMBER
, p_onui_code IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'J';
  -- er is een onderzoek/monster bij een van de foetussen?
  CURSOR onmo_cur
  IS
    SELECT NULL
    FROM   kgc_onderzoek_monsters onmo
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onmo.onde_id
    AND    zwon.foet_id = zwon.foet_id -- is not null
    AND    zwon.zwan_id = p_zwan_id
    ;

  -- er is geen onderzoek dat een uitslagcode heeft, niet uit de (normale) lijst?
  -- NB. onderzoeken van voor 2001 hebben veelal geen uitslagcode (!) en worden hierdoor uitgesloten van een vreugdebrief
  CURSOR onui_cur
  IS
    SELECT NULL
    FROM   kgc_onderzoek_uitslagcodes onui
    ,      kgc_onderzoeken onde
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onde.onde_id
    AND    zwon.foet_id = zwon.foet_id -- is not null
    AND    onde.onui_id = onui.onui_id (+)
    AND    NVL( onui.code, 'kleine letters' ) NOT IN ( 'A01', 'A04', 'A09' ) --aanpassing voor NIPT mantis 9503 en ook voor mantis13314
    AND    zwon.zwan_id = p_zwan_id
    ;
  -- er is geen onderzoek dat indicatie "echoscopische afwijking" (oid.) heeft?
  -- 'echo' kan eigenlijk verwijderd worden nu we 'afwijking' hebben opgenomen als criteria.
  -- Als 'Afwijking' gelijk is aan 'J' dan mag geen vreugdebrief aangemaakt worden
  -- tenzij expliciet opgegeven bij de zwangerschap
  CURSOR onin_cur
  IS
    SELECT NULL
    FROM   kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onin.onde_id
    AND    zwon.foet_id = zwon.foet_id -- is not null
    AND    onin.indi_id = indi.indi_id
    AND    (LOWER( indi.omschrijving ) LIKE '%echo%'
            OR
            indi.afwijking = 'J')
    AND    zwon.zwan_id = p_zwan_id
    ;
  v_dummy VARCHAR2(1);
-- PL/SQL Block
BEGIN
  OPEN  onmo_cur;
  FETCH onmo_cur
  INTO  v_dummy;
  IF ( onmo_cur%found )
  THEN -- onderzoek-monster bij een foetus
    v_return := 'J';
  ELSE
    v_return := 'N';
  END IF;
  CLOSE  onmo_cur;

  IF ( v_return = 'J' )
  THEN
    -- test uitslagcode voordat onderzoek in database is gewijzigd...
    IF ( p_onui_code IS NOT NULL )
    THEN
      IF ( UPPER( p_onui_code ) NOT IN ( 'A01', 'A04', 'A09' ) )  --aanpassing voor NIPT mantis 9503 en ook voor mantis13314
      THEN
        v_return := 'N';
      END IF;
    ELSE
      OPEN  onui_cur;
      FETCH onui_cur
      INTO  v_dummy;
      IF ( onui_cur%found )
      THEN -- onderzoek niet-normale uitslagcode
        v_return := 'N';
      END IF;
      CLOSE  onui_cur;
    END IF;
  END IF;

  IF ( v_return = 'J' )
  THEN
    OPEN  onin_cur;
    FETCH onin_cur
    INTO  v_dummy;
    IF ( onin_cur%found )
    THEN -- onderzoek met indicatie ...echo... (echoscopische afwijking)
      v_return := 'N';
    END IF;
    CLOSE  onin_cur;
  END IF;
  RETURN( v_return );
END vreugde;

FUNCTION referentie
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR onde_cur
  IS
    SELECT onde.onderzoeknr
    FROM   kgc_onderzoeken onde
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onde.onde_id
    AND    zwon.zwan_id = p_zwan_id
    ORDER BY onde.onde_id
    ;
  onde_rec onde_cur%rowtype;
BEGIN
  FOR r IN onde_cur
  LOOP
    BEGIN
      IF ( v_return IS NULL )
      THEN
        v_return := r.onderzoeknr;
      ELSE
        v_return := v_return||','||r.onderzoeknr;
      END IF;
    EXCEPTION
      WHEN VALUE_ERROR
      THEN
        NULL;
    END;
  END LOOP;
  RETURN( v_return );
END referentie;

FUNCTION verrichting
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  CURSOR mons_cur
  IS
    SELECT mate.code
    ,      mate.omschrijving
    FROM   kgc_materialen mate
    ,      kgc_monsters mons
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onmo.onde_id
    AND    zwon.foet_id = mons.foet_id
    AND    onmo.mons_id = mons.mons_id
    AND    mons.mate_id = mate.mate_id
    AND    zwon.zwan_id = p_zwan_id
    ORDER BY mons.mons_id DESC -- laatste eerst
    ;
  mons_rec mons_cur%rowtype;
BEGIN
  OPEN  mons_cur;
  FETCH mons_cur
  INTO  mons_rec;
  CLOSE mons_cur;
  IF ( mons_rec.code IN ( 'A' ) )
  THEN
    v_return := 'een vruchtwaterpunctie';
  ELSIF ( mons_rec.code IN ( 'C' ) )
  THEN
    v_return := 'een vlokkentest';
  ELSIF ( mons_rec.code IS NOT NULL )
  THEN
    v_return := 'een test op '||LOWER( mons_rec.omschrijving );
  ELSE
    v_return := 'een onderzoek';
  END IF;
  RETURN( v_return );
END verrichting;

FUNCTION info_geslacht
( p_zwan_id IN NUMBER
)
RETURN  VARCHAR2
IS
  v_return VARCHAR2(4000);
  CURSOR foet_cur
  IS
    SELECT zwan.info_geslacht
    ,      foet.geslacht -- M/V/O
    ,      COUNT(*) aantal
    FROM   kgc_foetussen foet
    ,      kgc_zwangerschappen zwan
    WHERE  zwan.zwan_id = foet.zwan_id
    AND    zwan.zwan_id = p_zwan_id
    GROUP BY zwan.info_geslacht
    ,        foet.geslacht
    ;
  v_info VARCHAR2(1) := 'N';
  v_aantal_jongens NUMBER := 0;
  v_aantal_meisjes NUMBER := 0;
  v_aantal_onbekend NUMBER := 0;
BEGIN
  FOR r IN foet_cur
  LOOP
    v_info := r.info_geslacht;
    IF ( r.geslacht = 'M' )
    THEN
      v_aantal_jongens := r.aantal;
    ELSIF ( r.geslacht = 'V' )
    THEN
      v_aantal_meisjes := r.aantal;
    ELSE
      v_aantal_onbekend := r.aantal;
    END IF;
  END LOOP;

  IF ( v_info = 'J' )
  THEN
    IF ( v_aantal_onbekend = 0 )
    THEN
      IF ( v_aantal_jongens = 1 )
      THEN
        v_return := 'een zoon';
      ELSIF ( v_aantal_jongens > 1 )
      THEN
        v_return := TO_CHAR(v_aantal_jongens)||' zonen';
      END IF;
      IF ( v_aantal_meisjes = 1 )
      THEN
        IF ( v_return IS NULL )
        THEN
          v_return := 'een dochter';
        ELSE
          v_return := v_return || ' en een dochter';
        END IF;
      ELSIF ( v_aantal_meisjes > 1 )
      THEN
        IF ( v_return IS NULL )
        THEN
          v_return := TO_CHAR(v_aantal_meisjes)||' dochters';
        ELSE
          v_return := v_return ||' en '|| TO_CHAR(v_aantal_meisjes)||' dochters';
        END IF;
      END IF;
    END IF;
  END IF;
  RETURN( v_return );
END info_geslacht;

FUNCTION metingen
( p_zwan_id IN NUMBER
)
RETURN  VARCHAR2
IS
  v_return VARCHAR2(4000);
  CURSOR zwon_cur
  IS
    SELECT onde_id
    FROM   kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.zwan_id = p_zwan_id
    ;
  v_onde_id NUMBER;
  CURSOR meti_cur
  ( b_bepaling IN VARCHAR2
  )
  IS
    SELECT NULL
    FROM   kgc_stoftestgroepen stgr
    ,      bas_metingen meti
    WHERE  meti.onde_id = v_onde_id
    AND    meti.stgr_id = stgr.stgr_id
    AND    INSTR( LOWER(stgr.omschrijving), b_bepaling ) > 0
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  zwon_cur;
  FETCH zwon_cur
  INTO  v_onde_id;
  CLOSE zwon_cur;

  OPEN  meti_cur( 'karyotype' );
  FETCH meti_cur
  INTO  v_dummy;
  IF ( meti_cur%found )
  THEN
    v_return := 'het chromosomenonderzoek';
  END IF;
  CLOSE meti_cur;

  OPEN  meti_cur( 'afp' );
  FETCH meti_cur
  INTO  v_dummy;
  IF ( meti_cur%found )
  THEN
    IF ( v_return IS NULL )
    THEN
      v_return := 'het onderzoek naar neuraalbuisdefecten';
    ELSE
      v_return := v_return|| ' en het onderzoek naar neuraalbuisdefecten';
    END IF;
  END IF;
  CLOSE meti_cur;

  IF ( v_return IS NULL )
  THEN
    v_return := 'het onderzoek';
  END IF;
  RETURN( v_return );
END metingen;

FUNCTION ondertekenaar
( p_zwan_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return kgc_medewerkers.formele_naam%TYPE;
  CURSOR onde_cur
  IS
    SELECT NVL( mede.formele_naam, mede.naam ) naam
    FROM   kgc_medewerkers mede
    ,      kgc_brieftypes brty
    ,      kgc_uitslagen uits
    ,      kgc_onderzoeken onde
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onde.onde_id
    AND    zwon.onde_id = uits.onde_id (+)
    AND    uits.brty_id = brty.brty_id (+)
    AND  ( brty.brty_id IS NULL
        OR brty.code = 'EINDBRIEF'
         )
    AND    mede.mede_id = NVL( uits.mede_id, onde.mede_id_autorisator )
    AND    zwon.zwan_id = p_zwan_id
    ORDER BY DECODE( uits.mede_id
                   , NULL, 9
                   , 1
                   )
    ,        DECODE( onde.mede_id_autorisator
                   , NULL, 9
                   , 1
                   )
    ,        onde.onde_id
    ;
  onde_rec onde_cur%rowtype;
BEGIN
  OPEN  onde_cur;
  FETCH onde_cur
  INTO  v_return;
  CLOSE onde_cur;
  RETURN( v_return );
END ondertekenaar;

FUNCTION geslacht_in_uitslag
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := NULL;
  CURSOR zwan_cur
  IS
    SELECT zwan.info_geslacht
    FROM   kgc_zwangerschappen zwan
    ,      kgc_foetussen foet
    ,      kgc_onderzoeken onde
    ,      kgc_brieven brie
    ,      kgc_brieftypes brty
    WHERE  onde.foet_id = foet.foet_id
    AND    foet.zwan_id = zwan.zwan_id
    AND    zwan.zwan_id = brie.zwan_id
    AND    brie.brty_id = brty.brty_id
    AND    brty.code = 'VREUGDE'
    AND    onde.onde_id = p_onde_id
    AND    NVL(zwan.vreugdebrief,CHR(2)) <> 'N'
    ;
BEGIN
  OPEN  zwan_cur;
  FETCH zwan_cur
  INTO  v_return;
  CLOSE zwan_cur;
  RETURN( v_return );
END geslacht_in_uitslag;

FUNCTION brief_indicator
( p_onde_id IN NUMBER
, p_onui_code IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2);
  CURSOR c
  IS
    SELECT vreugdebrief
    ,      zwan_id
    FROM   kgc_prenataal_zwan_onde_vw
    WHERE  onde_id = p_onde_id
    ;
  r c%rowtype;
BEGIN
  OPEN  c;
  FETCH c
  INTO  r;
  CLOSE c;
  IF ( r.vreugdebrief IS NULL )
  THEN
    v_return := 'S'||vreugde( p_zwan_id => r.zwan_id
                            , p_onui_code => p_onui_code
                            );
  ELSE
    v_return := r.vreugdebrief;
  END IF;
  RETURN( v_return );
END brief_indicator;

PROCEDURE zet_brief_indicator
( p_onde_id IN NUMBER
, p_indicator IN VARCHAR2 -- SJ, SN, J, N
)
IS
  CURSOR c
  IS
    SELECT zwan_id
    ,      vreugdebrief
    FROM   kgc_prenataal_zwan_onde_vw
    WHERE  onde_id = p_onde_id
    ;
  r c%rowtype;
BEGIN
  OPEN  c;
  FETCH c
  INTO  r;
  CLOSE c;
  IF ( r.zwan_id IS NOT NULL )
  THEN
    UPDATE kgc_zwangerschappen
    SET    vreugdebrief = DECODE( UPPER( p_indicator )
                                , 'J', 'J'
                                , 'N', 'N'
                                , NULL
                                )
    WHERE zwan_id = r.zwan_id
    ;
  END IF;
  COMMIT;
END zet_brief_indicator;
END  kgc_vreugdebrief_00;
/

/
QUIT
