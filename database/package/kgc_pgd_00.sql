CREATE OR REPLACE PACKAGE "HELIX"."KGC_PGD_00" IS
/******************************************************************************
    NAME:      KGC_PGD_00
    PURPOSE:   PGD in Helix

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.2        29-03-2016  APL             Mantis-9355
    1.1        01-02-2016  ATS              Mantis-12037
    1.0        19-06-2009  ATS              Mantis-0671
 *****************************************************************************/

--CREATED BY ATHAR SHAIKH ON 18/07/2014 FOR MANTIS 0671
   PROCEDURE chk_kgc_waarde_bepaling (
      p_string_x    VARCHAR2,
      p_string_y    VARCHAR2,
      p_proc_name   VARCHAR2
   );

--CREATED BY RAJENDRA ON 25/07/2014 FOR MANTIS 0671
   PROCEDURE autoriseren (p_onde_id IN NUMBER, p_afgerond IN VARCHAR2);

--CREATED BY ANUJA ON 28/07/2014 FOR MANTIS 0671
   PROCEDURE bepaal_fenotype_en_et_vrij_gen (
      p_embr_id   IN       NUMBER,
      p_result1   OUT      VARCHAR2,
      p_result2   OUT      VARCHAR2
   );

--CREATED BY Dharmik Vyas ON 24/07/2014 FOR MANTIS 0671
   PROCEDURE check_embryo_onderzoek (
      p_onde_id   IN   kgc_onderzoek_monsters.onde_id%TYPE DEFAULT NULL,
      p_embr_id   IN   NUMBER DEFAULT NULL,
      p_mons_id   IN   kgc_onderzoek_monsters.mons_id%TYPE DEFAULT NULL
   );


  PROCEDURE check_embryo_onderzoek1 (
      p_onde_id   IN   kgc_onderzoek_monsters.onde_id%TYPE ,
      p_embr_id   IN   NUMBER DEFAULT NULL,
      p_mons_id   IN   kgc_onderzoek_monsters.mons_id%TYPE
   );
END KGC_PGD_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_PGD_00" IS
--CREATED BY ATHAR SHAIKH ON 18/07/2014 FOR MANTIS 0671
  PROCEDURE chk_kgc_waarde_bepaling(p_string_x  VARCHAR2,
                                    p_string_y  VARCHAR2,
                                    p_proc_name VARCHAR2) IS
    CURSOR c1 IS
      SELECT 'X'
        FROM kgc_waarde_bepaling wabe
       WHERE wabe.string_x = p_string_y
         AND wabe.string_y = p_string_x
         AND wabe.proc_name = p_proc_name;

    v_char CHAR(1);
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    IF c1%ISOPEN THEN
      CLOSE c1;
    END IF;

    OPEN c1;

    FETCH c1
      INTO v_char;

    IF c1%FOUND THEN
      raise_application_error(-20000, 'Combinatie bestaat al!');
    END IF;

    CLOSE c1;
  END chk_kgc_waarde_bepaling;

  PROCEDURE autoriseren(p_onde_id IN NUMBER, p_afgerond IN VARCHAR2) IS
    CURSOR c_embr(p_onde_onde_id NUMBER) IS
      SELECT embr.embr_id, embr.uits_handm
        FROM kgc_onderzoeken        onde,
             kgc_onderzoek_monsters onmo,
             kgc_monsters           mons,
             kgc_embryos            embr
       WHERE onde.onde_id = onmo.onde_id
         AND onmo.mons_id = mons.mons_id
         AND mons.embr_id = embr.embr_id
         AND onde.onde_id = p_onde_onde_id;

    v_embr_id     kgc_embryos.embr_id%TYPE;
    v_fenotype    kgc_embryos.fenotype%TYPE := NULL;
    v_et_vrij_gen kgc_embryos.et_vrij_gen%TYPE := NULL;
    v_uits_handm  kgc_embryos.uits_handm%TYPE;
    v_result_1    VARCHAR2(4000);
    v_result_2    VARCHAR2(4000);
  BEGIN
    OPEN c_embr(p_onde_id);

    LOOP
      FETCH c_embr
        INTO v_embr_id, v_uits_handm;

      EXIT WHEN c_embr%NOTFOUND;

      IF v_uits_handm <> 'J' THEN
        IF p_afgerond = 'J' THEN
          bepaal_fenotype_en_et_vrij_gen(p_embr_id => v_embr_id,
                                         p_result1 => v_result_1,
                                         p_result2 => v_result_2);

          IF UPPER(v_result_2) = 'NEE' THEN
            v_result_2 := 'N';
          ELSIF UPPER(v_result_2) = 'JA' THEN
            v_result_2 := 'J';
          END IF;

          v_fenotype    := v_result_1;
          v_et_vrij_gen := v_result_2;
        ELSE
          v_fenotype    := NULL;
          v_et_vrij_gen := NULL;
        END IF;

        UPDATE kgc_embryos embr
           SET embr.fenotype = v_fenotype, embr.et_vrij_gen = v_et_vrij_gen
         WHERE embr.embr_id = v_embr_id;

        COMMIT;
      END IF;
    END LOOP;

    CLOSE c_embr;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END autoriseren;

  --CREATED BY ANUJA ON 28/07/2014 FOR MANTIS 0671----
  PROCEDURE bepaal_fenotype_en_et_vrij_gen(p_embr_id IN NUMBER,
                                           p_result1 OUT VARCHAR2,
                                           p_result2 OUT VARCHAR2) IS

    CURSOR conclusie_cur IS
      SELECT to_char(meti.conclusie), ROWNUM   --TO_CHAR added by Anuja for MANTIS 9355 on 29-03-2016 for Release 8.11.0.2
        FROM kgc_monsters mons, bas_fracties frac, bas_metingen meti
       WHERE mons.mons_id = frac.mons_id
         AND frac.frac_id = meti.frac_id
         AND meti.afgerond = 'J'
         AND NVL(mons.embr_id, 0) = p_embr_id
         AND ROWNUM <= 2;

    CURSOR waarde_cur(p_proc_name IN VARCHAR2) IS
      SELECT wabe.string_x, wabe.string_y, wabe.result_1, wabe.result_2
        FROM kgc_waarde_bepaling wabe
       WHERE wabe.proc_name = p_proc_name
         and wabe.string_y != '----';

    CURSOR waarde_cur_1(p_proc_name IN VARCHAR2) IS
      SELECT wabe.string_x, wabe.result_1, wabe.result_2
        FROM kgc_waarde_bepaling wabe
       WHERE wabe.proc_name = p_proc_name
         and (wabe.string_y = '----' OR wabe.string_y is null);

    v_conclusie      bas_metingen.conclusie%TYPE;
    v_rownum         NUMBER;
    conclusie_x      bas_metingen.conclusie%TYPE;
    conclusie_y      bas_metingen.conclusie%TYPE;
    v_string_x       kgc_waarde_bepaling.string_x%TYPE;
    v_string_y       kgc_waarde_bepaling.string_y%TYPE;
    v_result1        kgc_waarde_bepaling.result_1%TYPE;
    v_result2        kgc_waarde_bepaling.result_2%TYPE;
    v_result1_pgd    kgc_waarde_bepaling.result_1%TYPE;
    v_result1_drag   kgc_waarde_bepaling.result_1%TYPE;
    flag             NUMBER;
    v_onge_x         NUMBER;
    v_onge_y         NUMBER;
    v_geen_x         number;
    v_geen_y         number;
    v_niet_x         NUMBER;
    v_niet_Y         NUMBER;
    v_check_regexp_x varchar2(1000);
    v_check_regexp_y varchar2(1000);
  BEGIN
    OPEN conclusie_cur;
    LOOP
      FETCH conclusie_cur
        INTO v_conclusie, v_rownum;
      EXIT WHEN conclusie_cur%NOTFOUND;

      IF v_rownum = 1 THEN
        conclusie_x := v_conclusie;
      END IF;

      IF v_rownum = 2 THEN
        conclusie_y := v_conclusie;
        IF (NVL(conclusie_x, 'M') = 'M' or NVL(conclusie_y, 'N') = 'N') THEN
          flag := 1;
          IF NVL(conclusie_x, 'N') = 'N' THEN
            conclusie_x := conclusie_y;
            conclusie_y := NULL;
          END IF;
        ELSE
          flag := 2;
        END IF;
      ELSE
        flag := 1;
        conclusie_y := NULL;
      END IF;
    END LOOP;
    CLOSE conclusie_cur;

    ----Cursor for procedure PGD---------
    v_niet_x := INSTR(UPPER(conclusie_x), 'NIET AANGEDAAN');
    v_niet_y := INSTR(UPPER(conclusie_y), 'NIET AANGEDAAN');
    v_onge_x := INSTR(UPPER(conclusie_x), 'ONGEBALANCEERD');
    v_onge_y := INSTR(UPPER(conclusie_y), 'ONGEBALANCEERD');

    IF flag = 1 THEN
      OPEN waarde_cur_1('Fenotype bepaling');
      LOOP
        FETCH waarde_cur_1
          INTO v_string_x, v_result1, v_result2;

        EXIT WHEN waarde_cur_1%NOTFOUND;

        IF (INSTR(UPPER(conclusie_x), UPPER(v_string_x)) <> 0) THEN
          IF v_niet_x <> 0 THEN
            -- NIET found
            IF (upper(v_string_x) = 'NIET AANGEDAAN') THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
              EXIT;
            END IF;
          ELSE
            p_result1 := v_result1;
            p_result2 := v_result2;
            EXIT;
          END IF;
        END IF;
      END LOOP;
      CLOSE waarde_cur_1;
    ELSE
      OPEN waarde_cur('Fenotype bepaling');
      LOOP
        FETCH waarde_cur
          INTO v_string_x, v_string_y, v_result1, v_result2;

        EXIT WHEN waarde_cur%NOTFOUND;

        IF ((INSTR(UPPER(conclusie_x), UPPER(v_string_x)) <> 0 AND
           INSTR(UPPER(conclusie_y), UPPER(v_string_y)) <> 0) OR
           (INSTR(UPPER(conclusie_x), UPPER(v_string_y)) <> 0 AND
           INSTR(UPPER(conclusie_y), UPPER(v_string_x)) <> 0)) THEN

          IF
           (
             v_niet_x <> 0
           )
          THEN
            -- NIET found
            IF
             (
             upper(v_string_x) = 'NIET AANGEDAAN'
             )
            THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
            -- EXIT; -- Commented by Athar Shaikh for Mantis-12037 on 01-02-2015
            -- Start added by Athar Shaikh for Mantis-12037 on 01-02-2015
            ELSIF
             (
             upper(v_string_y) = 'NIET AANGEDAAN'
             )
            THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
            -- End added by Athar Shaikh for Mantis-12037 on 01-02-2015
            END IF;

          ELSIF
           (
             v_niet_Y <> 0
           )
          THEN
            -- NIET found
            IF
             (
             upper(v_string_y) = 'NIET AANGEDAAN'
             )
            THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
            -- EXIT; -- Commented by Athar Shaikh for Mantis-12037 on 01-02-2015
            -- Start added by Athar Shaikh for Mantis-12037 on 01-02-2015
            ELSIF
             (
             upper(v_string_x) = 'NIET AANGEDAAN'
             )
            THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
            -- End added by Athar Shaikh for Mantis-12037 on 01-02-2015
            END IF;

          ELSIF
           (
             v_onge_x <> 0
           )
          THEN
             IF
              (
              upper(v_string_x) = 'ONGEBALANCEERD'
              )
             THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
             -- EXIT; -- Commented by Athar Shaikh for Mantis-12037 on 01-02-2015
             -- Start added by Athar Shaikh for Mantis-12037 on 01-02-2015
             ELSIF
              (
              upper(v_string_y) = 'ONGEBALANCEERD'
              )
             THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
             -- End added by Athar Shaikh for Mantis-12037 on 01-02-2015
             END IF;

          ELSIF
           (
             v_onge_y <> 0
           )
          THEN
             IF
              (
              upper(v_string_Y) = 'ONGEBALANCEERD'
              )
             THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
             -- EXIT; -- Commented by Athar Shaikh for Mantis-12037 on 01-02-2015
             -- Start added by Athar Shaikh for Mantis-12037 on 01-02-2015
             ELSIF
              (
              upper(v_string_x) = 'ONGEBALANCEERD'
              )
             THEN
              p_result1 := v_result1;
              p_result2 := v_result2;
             -- End added by Athar Shaikh for Mantis-12037 on 01-02-2015
             END IF;

          ELSE
            p_result1 := v_result1;
            p_result2 := v_result2;
            EXIT;
          END IF;
        END IF;
      END LOOP;
      CLOSE waarde_cur;
    END IF;

    ----Cursor for procedure Dragerschap bepaling -------------------

    IF (INSTR(UPPER(conclusie_x), 'DRAGER') <> 0 OR
       INSTR(UPPER(conclusie_y), 'DRAGER') <> 0) THEN
      v_geen_x := INSTR(UPPER(conclusie_x), 'GEEN DRAGER');
      v_geen_y := INSTR(UPPER(conclusie_y), 'GEEN DRAGER');

      IF flag = 1 THEN

        OPEN waarde_cur_1('Dragerschap bepaling');
        LOOP
          FETCH waarde_cur_1
            INTO v_string_x, v_result1, v_result2;
          EXIT WHEN waarde_cur_1%NOTFOUND;
          IF (INSTR(UPPER(conclusie_x), upper(v_string_x)) <> 0) THEN
            IF v_geen_x <> 0 THEN
              -- geen found
              IF (upper(v_string_x) = 'GEEN DRAGER') THEN
                v_result1_drag := v_result1;
                EXIT;
              END IF;
            ELSE
              v_result1_drag := v_result1;
              EXIT;
            END IF;
          END IF;
        END LOOP;
        CLOSE waarde_cur_1;
      ELSE
        OPEN waarde_cur('Dragerschap bepaling');

        LOOP
          FETCH waarde_cur
            INTO v_string_x, v_string_y, v_result1, v_result2;
          EXIT WHEN waarde_cur%NOTFOUND;
          IF (INSTR(UPPER(conclusie_x), UPPER(v_string_x)) <> 0 AND
             INSTR(UPPER(conclusie_y), UPPER(v_string_y)) <> 0) OR
             (INSTR(UPPER(conclusie_x), UPPER(v_string_y)) <> 0 AND
             INSTR(UPPER(conclusie_y), UPPER(v_string_x)) <> 0) THEN
            IF v_geen_x <> 0 THEN
              -- geen found in conclusie 1
              IF (upper(v_string_x) = 'GEEN DRAGER') THEN
                v_result1_drag := v_result1;
                EXIT;
              END IF;
            ELSIF v_geen_y <> 0 Then
              -- geen found in conclusie 2
              IF (upper(v_string_y) = 'GEEN DRAGER') THEN
                v_result1_drag := v_result1;
                EXIT;
              END IF;
            ELSE
              v_result1_drag := v_result1;
              EXIT;
            END IF;
          END IF;
        END LOOP;

        CLOSE waarde_cur;
      END IF;
    END IF;

    IF p_result1 IS NOT NULL
    THEN
       p_result1 := p_result1 || ' ' || v_result1_drag;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      p_result1 := NULL;
      p_result2 := NULL;
  END bepaal_fenotype_en_et_vrij_gen;

  --CREATED BY Dharmik Vyas ON 24/07/2014 FOR MANTIS 0671
  PROCEDURE check_embryo_onderzoek(p_onde_id IN kgc_onderzoek_monsters.onde_id%TYPE DEFAULT NULL,
                                   p_embr_id IN NUMBER DEFAULT NULL,
                                   p_mons_id IN kgc_onderzoek_monsters.mons_id%TYPE DEFAULT NULL) IS
    CURSOR mons_cur IS
      SELECT mons_id FROM kgc_monsters WHERE embr_id = p_embr_id;

    v_mons_id kgc_onderzoek_monsters.mons_id%TYPE := NULL;

    CURSOR onmo_cur(b_mons_id IN kgc_onderzoek_monsters.mons_id%TYPE) IS
      SELECT onde.onde_id
      FROM kgc_onderzoek_monsters onmo, kgc_onderzoeken onde
      WHERE onmo.onde_id = onde.onde_id
      and onmo.mons_id = b_mons_id
      and NVL(onde.afgerond, 'N') = 'N';   -- Added by Athar Shaikh for the PGD new Development on 11-03-2015

    v_onde_id     kgc_onderzoek_monsters.onde_id%TYPE := NULL;
    v_onde_id_new kgc_onderzoek_monsters.onde_id%TYPE := NULL;
    v_ondenr      kgc_onderzoeken.onderzoeknr%TYPE := NULL;

    CURSOR onmo_new_cur IS
      SELECT onde_id FROM kgc_onderzoek_monsters WHERE mons_id = p_mons_id;

    CURSOR ondenr_cur(p_onde_id IN kgc_onderzoeken.onde_id%TYPE) IS
      SELECT onderzoeknr FROM kgc_onderzoeken WHERE onde_id = p_onde_id;
  BEGIN
    IF p_onde_id IS NULL THEN
      OPEN mons_cur;
      FETCH mons_cur
        INTO v_mons_id;
      CLOSE mons_cur;

      OPEN onmo_cur(v_mons_id);
      FETCH onmo_cur
        INTO v_onde_id;
      CLOSE onmo_cur;

      OPEN onmo_new_cur;
      FETCH onmo_new_cur
        INTO v_onde_id_new;
      CLOSE onmo_new_cur;

      IF v_onde_id_new IS NOT NULL THEN
        OPEN ondenr_cur(v_onde_id_new);
        FETCH ondenr_cur
          INTO v_ondenr;
        CLOSE ondenr_cur;
      END IF;

      IF v_onde_id IS NOT NULL THEN
        IF v_onde_id != nvl(v_onde_id_new, v_onde_id) THEN
          qms$errors.show_message(p_mesg   => 'KGC-11420',
                                  p_param0 => v_ondenr);
        END IF;
      END IF;
    ELSE
      OPEN onmo_new_cur;

      FETCH onmo_new_cur
        INTO v_onde_id_new;

      CLOSE onmo_new_cur;

      IF v_onde_id_new IS NOT NULL THEN
        OPEN ondenr_cur(v_onde_id_new);

        FETCH ondenr_cur
          INTO v_ondenr;

        CLOSE ondenr_cur;
      END IF;

      IF p_onde_id != nvl(v_onde_id_new, p_onde_id) THEN
        qms$errors.show_message(p_mesg   => 'KGC-11420',
                                p_param0 => v_ondenr);
      END IF;
    END IF;
  END;

  PROCEDURE check_embryo_onderzoek1(p_onde_id IN kgc_onderzoek_monsters.onde_id%TYPE,
                                    p_embr_id IN NUMBER DEFAULT NULL,
                                    p_mons_id IN kgc_onderzoek_monsters.mons_id%TYPE) IS

    CURSOR embr_onde_cur(b_embr_id kgc_embryos.embr_id%TYPE) IS
      SELECT distinct onmo.onde_id
        FROM kgc_onderzoek_monsters onmo, kgc_onderzoeken onde
        where onde.onde_id = onmo.onde_id
        and   nvl(onde.afgerond, 'N') = 'N'   -- Added by Athar Shaikh for the PGD new Development on 11-03-2015
        and   onmo.mons_id in
             (select mons.mons_id
                from kgc_monsters mons
               where mons.embr_id = b_embr_id);

    CURSOR nummer_cur(b_onde_id IN kgc_onderzoeken.onde_id%TYPE) IS
      SELECT onde.onderzoeknr
        FROM kgc_onderzoeken onde
       WHERE onde.onde_id = b_onde_id;

    CURSOR embr_cur IS
      SELECT embr_id FROM kgc_monsters WHERE mons_id = p_mons_id;

   /* CURSOR onmo_onde_cur is
    select onde_id
    from kgc_onderzoeken
    where onde_id = p_onde_id
    and nvl(afgerond, 'N') = 'N'; */   -- Commented by athar shaikh for the release rework 8.8.1.2 on 08062015



    v_embr_id     kgc_embryos.embr_id%TYPE := NULL;
    v_onde_id     kgc_onderzoek_monsters.onde_id%TYPE := NULL;
    v_onderzoeknr kgc_onderzoeken.onderzoeknr%TYPE := NULL;
    v_onmo_onde_id kgc_onderzoeken.onde_id%TYPE := NULL;

  Begin

    If p_embr_id is Null Then
      --- find if monster has embryo
      Open embr_cur;
      FETCH embr_cur
        INTO v_embr_id;
      CLOSE embr_cur;
    Else
      v_embr_id := p_embr_id;
    End if;

    If v_embr_id is not Null Then
      -- Find if an embryo is already linked to an onderzoek via its monsters.
      OPEN embr_onde_cur(v_embr_id);
      FETCH embr_onde_cur
        INTO v_onde_id;
      CLOSE embr_onde_cur;

     /* open onmo_onde_cur;
      fetch onmo_onde_cur into v_onmo_onde_id;
      close onmo_onde_cur;  */   -- Commentd by athar shaikh for the release rework 8.8.1.2 on 09062015


        --IF v_onde_id is not null and v_onmo_onde_id is not null then -- Commentd by athar shaikh for the release rework 8.8.1.2 on 09062015
        IF NVL(v_onde_id, p_onde_id) != p_onde_id then               -- Added by athar shaikh for the release rework 8.8.1.2 on 09062015

        OPEN nummer_cur(v_onde_id);
        FETCH nummer_cur
          INTO v_onderzoeknr;
        CLOSE nummer_cur;

         qms$errors.show_message(p_mesg   => 'KGC-11420',
                                p_param0 => v_onderzoeknr);
      End if;
    End if;
  End;
END kgc_pgd_00;
/

/
QUIT
