CREATE OR REPLACE PACKAGE "HELIX"."KGC_ZIS_NIJMEGEN" IS
  /******************************************************************************
    NAME:      KGC_ZIS_NIJMEGEN
    PURPOSE:   Nijmegen specifieke onderdelen voor de ZIS-koppelingen

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    9.0        03-10-2014  RDE              Aanpassingen tbv EPIC, zie doc ADT Nijmegen
                                            0009526: EPIC personen met 000000000 als bsn
                                            0009798: Niet importeren van huisarts gegevens van ADT berichten
    8.0        23-02-2011  JKO              Mantis 3952: meerling overnemen
    7.0        20-01-2010  RDE              Mantis 3850: lengte velden verhoogd in persoon_in_zis
    6.0        23-07-2009  JKO              package-body overgenomen van Cootje Vermaat:
                                            bij verwerking ADT_A31 de gebruiker die het doet weergeven
    5.0        13-05-2009  JKO              hl7msg2rec: nieuwe code-deel overgenomen van RDE
    4.0        08-05-2009  RDE              Mantis 283 ingebouwd
    3.0        27-02-2007  RDE              Aanpassingen tbv patcomp2 - 1 extra gegeven! + aanpassing datumformaat
                                            tevens opgehaalde verz_id in verz-rec zetten
    2.0        23-02-2007  RDE              Mantis 452: ADT-aanspreken bevat OF de mansnaam, OF de eigennaam
    1.0       <23-02-2007  RDE              creatie
 **************************************************************************** */
  FUNCTION persoon_in_zis
  ( p_zisnr IN VARCHAR2
  , pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
  , rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
  , verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
  , x_msg        OUT VARCHAR2
  ) RETURN BOOLEAN;

  PROCEDURE hl7msg2rec -- vertaal HL7-message naar rec's
  ( p_hl7_msg IN     kgc_zis.hl7_msg
  , pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
  , rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
  , verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
  );

  PROCEDURE rec2hl7msg
  ( pers_trow IN     cg$KGC_PERSONEN.cg$row_type
  , rela_trow IN     cg$KGC_RELATIES.cg$row_type
  , verz_trow IN     cg$KGC_VERZEKERAARS.cg$row_type
  , p_extra   IN     VARCHAR2
  , x_hl7_msg IN OUT kgc_zis.hl7_msg
  );

  FUNCTION verwerk_ander_bericht -- specifiek
  ( p_hlbe_id   NUMBER
  , p_hl7_msg   kgc_zis.hl7_msg
  , x_entiteit  IN OUT VARCHAR2
  , x_id        IN OUT NUMBER
  ) RETURN VARCHAR2;

  FUNCTION decl2dft_p03
  ( p_decl_id        IN     NUMBER
  , p_aantal         IN     NUMBER
  , p_check_only     IN     BOOLEAN
  , p_msg_control_id IN     VARCHAR2
  , x_msg            IN OUT VARCHAR2
  ) RETURN BOOLEAN;

/* niet meer nodig FUNCTION persoon_in_zis
  ( p_zisnr          IN     VARCHAR2
  , pers_trow        IN OUT cg$KGC_PERSONEN.cg$row_type
  , rela_trow        IN OUT cg$KGC_RELATIES.cg$row_type
  , verz_trow        IN OUT cg$KGC_VERZEKERAARS.cg$row_type
  , x_pat_aangemeld     OUT BOOLEAN
  , x_msg               OUT VARCHAR2
  ) RETURN BOOLEAN;
*/
END KGC_ZIS_NIJMEGEN;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ZIS_NIJMEGEN" IS
function datum
( p_dat in varchar2
)
return date
is
  v_return date;
begin
  begin
    v_return := to_date( p_dat, 'yyyy-mm-dd' );
  exception
    when others then
      v_return := null;
  end;
  return( v_return );
end datum;
-- lokale functie... indien ook elders nodig: verhuizen naar kgc_zis!
FUNCTION getAttrSub(p_str VARCHAR2, p_sub NUMBER) RETURN VARCHAR2 IS
  -- zoek 'p_sub'-ste stuk van p_str; scheidingsteken is ampersand
  v_return VARCHAR2(4000) := p_str;
  v_pos_b  NUMBER;
  v_pos_e  NUMBER;
BEGIN
  -- begin
  IF p_sub = 1 THEN
    v_pos_b := 1; -- vooraan
  ELSE
    v_pos_b := INSTR(p_str, '&', 1, p_sub - 1);
    IF v_pos_b = 0 THEN
      RETURN '';
    ELSE
      v_pos_b := v_pos_b + 1; -- voorbij de ampersand
    END IF;
  END IF;
  -- eind
  v_pos_e := INSTR(p_str, '&', 1, p_sub);
  IF v_pos_e = 0 THEN
    v_pos_e := LENGTH(p_str) + 1;
  END IF;

  RETURN SUBSTR(p_str, v_pos_b, v_pos_e - v_pos_b);
END getAttrSub;

FUNCTION persoon_in_zis
( p_zisnr          IN     VARCHAR2
, pers_trow        IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow        IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow        IN OUT cg$KGC_VERZEKERAARS.cg$row_type
, x_msg               OUT VARCHAR2
) RETURN BOOLEAN IS
  v_sypa_code_host  kgc_systeem_parameters.standaard_waarde%type;
  v_hl7_svr_poort_qry NUMBER := kgc_zis.getHL7Poort('ZIS_HL7_SVR_POORT_QRY');
  v_strHL7in        VARCHAR2(32767);
  v_strAntw         VARCHAR2(32767);
  v_ok              BOOLEAN;
  v_MSA             VARCHAR2(10);
  v_MSH             VARCHAR2(10);
  v_hl7_msg_bron    kgc_zis.hl7_msg;
  v_hl7_msg_res     kgc_zis.hl7_msg;
  v_timeout         NUMBER := 100; -- 10 seconden
  v_msg_control_id  VARCHAR2(10);
  v_sysdate         varchar2(15) := to_char(sysdate,'yyyymmddhh24mi');
  v_qry_zis         varchar2(10) := replace(p_zisnr, '.'); -- geen puntjes!
  v_thisproc        VARCHAR2(100) := 'kgc_zis_nijmegen.persoon_in_zis';
  v_hl7_versie      VARCHAR2(10) := '2.4';
BEGIN
  kgc_zis.log(v_thisproc || ': Start!');
  v_sypa_code_host := kgc_zis.get_sypa_sywa('ZIS_HL7_SVR_NAAM_QRY', TRUE, TRUE, v_sypa_code_host);
  dbms_output.put_line('ZIS_HL7_SVR_NAAM_QRY = ' || v_sypa_code_host);
  dbms_output.put_line('ZIS_HL7_SVR_POORT_QRY = ' || to_char(v_hl7_svr_poort_qry));
  v_msg_control_id := SUBSTR(TO_CHAR(abs(dbms_random.random)), 1, 10);

  -- **** MSH
  v_hl7_msg_bron('MSH') := kgc_zis.init_msg('MSH', v_hl7_versie);
  v_hl7_msg_bron('MSH')(1)(1)(1) := '|';
  v_hl7_msg_bron('MSH')(2)(1)(1) := '^~\&';
  v_hl7_msg_bron('MSH')(3)(1)(1) := 'HELIX';
  v_hl7_msg_bron('MSH')(5)(1)(1) := 'EPIC';
  v_hl7_msg_bron('MSH')(7)(1)(1) := v_sysdate;
  v_hl7_msg_bron('MSH')(9)(1)(1) := 'QRY^Q01';
  v_hl7_msg_bron('MSH')(10)(1)(1) := v_msg_control_id;
  -- P voor productie, T voor Training; "Processing-id"; dit staat in SYPA BEH_ZIS_HL7_MSH11_PROC_ID
  v_hl7_msg_bron('MSH')(11)(1)(1) := kgc_sypa_00.systeem_waarde('BEH_ZIS_HL7_MSH11_PROC_ID');
  v_hl7_msg_bron('MSH')(12)(1)(1) := v_hl7_versie;
  v_hl7_msg_bron('MSH')(15)(1)(1) := ''; --hbo naar aanleiding van mail van epic mensen.
  v_hl7_msg_bron('MSH')(16)(1)(1) := '';--hbo naar aanleiding van mail van epic mensen.
--  v_hl7_msg_bron('MSH')(15)(1)(1) := 'NE';
--  v_hl7_msg_bron('MSH')(16)(1)(1) := 'AL';

  -- **** QRD
  v_hl7_msg_bron('QRD') := kgc_zis.init_msg('QRD', v_hl7_versie);
  v_hl7_msg_bron('QRD')(1)(1)(1) := v_sysdate;
  v_hl7_msg_bron('QRD')(2)(1)(1) := 'R';
  v_hl7_msg_bron('QRD')(3)(1)(1) := 'I';
  v_hl7_msg_bron('QRD')(4)(1)(1) := '0000000001';
  v_hl7_msg_bron('QRD')(8)(1)(1) := v_qry_zis; -- PI is default, dus niet nodig
  -- mantis 283; in 2.4 is voor QRD-9 NAW geen goede waarde; dan DEM
  v_hl7_msg_bron('QRD')(9)(1)(1) := 'NAW';

  v_strHL7in := kgc_zis.g_sob_char
    || kgc_zis.hl7seg2str('MSH', v_hl7_msg_bron('MSH')) || kgc_zis.g_cr
    || kgc_zis.hl7seg2str('QRD', v_hl7_msg_bron('QRD')) || kgc_zis.g_cr
    || kgc_zis.g_eob_char || kgc_zis.g_term_char
    ;
  v_ok := kgc_zis.send_msg
  ( p_host => v_sypa_code_host
  , p_port => to_char(v_hl7_svr_poort_qry)
  , p_strHL7in  => v_strHL7in
  , p_timeout => v_timeout
  , x_strHL7out => v_strAntw
  , x_msg => x_msg
  );
  kgc_zis.log(v_strAntw); --, '1100XXXXXX');
  IF NOT v_ok THEN
    RETURN FALSE; -- msg al gevuld
  END IF;
  v_hl7_msg_res := kgc_zis.str2hl7msg(0, v_strAntw);
  v_MSA := kgc_zis.getSegmentCode('MSA', v_hl7_msg_res);
  v_MSH := kgc_zis.getSegmentCode('MSH', v_hl7_msg_res);
  IF NVL(LENGTH(v_MSA), 0) = 0 OR NVL(LENGTH(v_MSH), 0) = 0 THEN
    x_msg := 'Ophalen gegevens NIET gelukt! (MSA- of MSH-segment afwezig)';
--    RETURN FALSE;
  END IF;
  IF NVL(v_hl7_msg_res(v_MSA)(1)(1)(1), 'XX') <> 'AA' THEN
    x_msg := 'Ophalen gegevens NIET gelukt! (Code MSA-1: '
      || v_hl7_msg_res(v_MSA)(1)(1)(1) || '; Tekst: '
      || v_hl7_msg_res(v_MSA)(3)(1)(1) || ')';
    RETURN FALSE;
  ELSIF v_hl7_msg_res(v_MSH)(10)(1)(1) <> v_msg_control_id THEN -- komt 'nooit' voor
    x_msg := 'Fout bij ophalen gegevens: mismatch control-id! Verzonden: '
      || v_msg_control_id || '; Ontvangen: '
      || v_hl7_msg_res(v_MSH)(10)(1)(1) || '!';
    RETURN FALSE;
  END IF;

  -- omzetten naar input-parms (ook nodig voor A08!)
  hl7msg2rec
  ( p_hl7_msg => v_hl7_msg_res
  , pers_trow => pers_trow
  , rela_trow => rela_trow
  , verz_trow => verz_trow
  );

  x_msg := 'OK';
  RETURN TRUE;
EXCEPTION
  WHEN OTHERS THEN
    x_msg := v_thisproc || ' Error: ' || SQLERRM;
    RETURN FALSE;
END persoon_in_zis;

PROCEDURE hl7msg2rec -- vertaal HL7-message naar rec's
( p_hl7_msg IN     kgc_zis.hl7_msg
, pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
) IS
  CURSOR c_land_via_isocode(p_isocode VARCHAR2) IS
    SELECT land.naam
    FROM   kgc_landen land
    ,      kgc_attributen attr
    ,      kgc_attribuut_waarden atwa
    WHERE  attr.code = 'ISO_CODE'
    AND    attr.tabel_naam = 'KGC_LANDEN'
    AND    attr.attr_id = atwa.attr_id
    AND    atwa.waarde = p_isocode
    AND    atwa.id = land.land_id
    ;
  CURSOR c_verz
  ( p_code VARCHAR2
  ) IS
    SELECT verz_id
    FROM   kgc_verzekeraars verz
    WHERE  verz.code = p_code
    ;
  TYPE t_tab IS TABLE OF VARCHAR2(200) INDEX BY BINARY_INTEGER;
  v_telf_tab         t_tab;
  v_rela_id          NUMBER;
  v_rela_code        VARCHAR2(20);
  v_verz_id          NUMBER;
  v_pat_gehuwd       KGC_PERSONEN.GEHUWD%TYPE;
  v_MSH              VARCHAR2(10);
  v_EVN              VARCHAR2(10);
  v_PID              VARCHAR2(10);
  v_roepnaam         VARCHAR2(200);
  v_voornaam1        VARCHAR2(200);
  v_pos              NUMBER;
  v_dummy            NUMBER;
  v_tmp              VARCHAR2(4000);
  v_tmp2             VARCHAR2(4000);
  v_column_length    NUMBER;
  v_pid11_type       NUMBER;
  v_pid11_typeM      NUMBER;
  v_pid11_typeH      NUMBER;
  v_pid5_typeL       NUMBER;
  v_pid5_typeN       NUMBER;
  v_zis_achternaam   VARCHAR2(200);
  v_zis_achternaam_p VARCHAR2(200);
  v_zis_aanspreken   VARCHAR2(200);
  v_helix_aanspreken VARCHAR2(200);
  v_rela_trow_null   cg$KGC_RELATIES.cg$row_type;
  v_msg              VARCHAR2(2000);
  v_debug            VARCHAR2(4000);
  v_thisproc         VARCHAR2(100) := 'kgc_zis_nijmegen.hl7msg2rec';
BEGIN
  kgc_zis.log(v_thisproc || ': Start');
  -- A08 Nijmegen: MSH EVN PID [{ROL}] [PV1] [PV2]
  -- QRY Q01 Nijmegen?:  MSH EVN PID [{ROL}] [PV1] [PV2]

  v_debug := '1';
--****** testing
--  kgc_zis.log(kgc_zis.hl7msg2userstr(p_hl7_msg), '1100');
--****** testing

  v_debug := v_debug || 'a';
  v_MSH := kgc_zis.getSegmentCode('MSH', p_hl7_msg);
  v_EVN := kgc_zis.getSegmentCode('EVN', p_hl7_msg);
  v_PID := kgc_zis.getSegmentCode('PID', p_hl7_msg);

  -- hl7 nl profile!
  v_debug := v_debug || 'b';
  IF NVL(LENGTH(v_PID), 0) = 0 THEN
    raise_application_error(-20000, 'Er kwamen geen gegevens van ZIS! (PID-segment afwezig)');
  ELSE
    -- *** ZISnr
    -- *** BSN - het bestaande ZISnr zeker niet leegmaken; hooguit overschrijven met een geverifieerd bsn
    pers_trow.bsn_geverifieerd := NVL(pers_trow.bsn_geverifieerd, 'N');
    FOR i IN 1 .. p_hl7_msg(v_PID) (3).COUNT LOOP
      v_debug := v_debug || '#=' || p_hl7_msg(v_PID) (3) (i).COUNT;
      IF p_hl7_msg(v_PID) (3) (i).COUNT < 5 THEN -- rde 03-09-2013 geen ZISnr! foute testdata?
        NULL;
      ELSIF p_hl7_msg(v_PID) (3) (i) (5) = 'PI' THEN -- zisnr
        kgc_zis.getHL7waarde(p_hl7_msg(v_PID) (3) (i) (1), pers_trow.zisnr);
      ELSIF p_hl7_msg(v_PID) (3) (i) (5) = 'NNNLD' THEN -- BSN
        kgc_zis.getHL7waarde(p_hl7_msg(v_PID) (3) (i) (1), v_tmp);
        IF LENGTH(v_tmp) > 0 THEN -- overnemen!
          pers_trow.bsn := TO_NUMBER(v_tmp);
--          IF pers_trow.bsn = 0 THEN -- Mantis 0009526: EPIC personen met 000000000 als bsn
          IF pers_trow.bsn = 0 OR pers_trow.bsn = 999999999 THEN
            -- Mantis 0009526: EPIC personen met 000000000 als bsn
            -- of
            -- Mantis 0011979: BSN 9x9 in EPIC zorgt ervoor dat een ADT-A08 bericht niet wordt verwerkt (RDE 13-10-2015)
            pers_trow.bsn := NULL;
            pers_trow.bsn_geverifieerd := 'N';
          ELSE -- is het bsn geverifieerd? Default ja
            pers_trow.bsn_geverifieerd := 'J';
            IF p_hl7_msg(v_PID).COUNT >= 32 THEN -- rde 2014-10-21: extra gegevens aanwezig; BSN0geverifieerd wordt alleen op niet-geverifieerd gezet indien
               -- PID-31 <> "N" EN PID-32 <> "ELDERS"
               IF NVL(p_hl7_msg(v_PID) (31) (1) (1), 'X') <> 'N' AND NVL(p_hl7_msg(v_PID) (32) (1) (1), 'X') <> 'ELDERS' THEN
                  pers_trow.bsn_geverifieerd := 'N';
               END IF;
            END IF;
          END IF;
        END IF;
      END IF;
    END LOOP;

    -- *** geboortedatum; part_geboortedatum is niet beschikbaar
    v_debug := v_debug || 'c';
    pers_trow.part_geboortedatum := NULL;
    IF nvl(p_hl7_msg(v_PID) (7) (1) (1)
          ,'00000000') = '00000000'
    THEN
      pers_trow.geboortedatum := NULL;
    ELSE
      pers_trow.geboortedatum      := to_date(SUBSTR(p_hl7_msg(v_PID) (7) (1) (1), 1, 8) -- rde 03-09-2013: er kan ook nog een tijd bij zitten
                                             ,'YYYYMMDD');
    END IF;
    -- *** geslacht
    v_debug := v_debug || 'd';
    IF p_hl7_msg(v_PID) (8) (1) (1) = 'M'
    THEN
       pers_trow.geslacht := 'M';
    ELSIF p_hl7_msg(v_PID) (8) (1) (1) IN ('V'
                   ,'F')
    THEN
       pers_trow.geslacht := 'V';
    ELSE
       pers_trow.geslacht := 'O';
    END IF;
    -- *** meerling
    v_debug := v_debug || 'e';
    IF p_hl7_msg(v_PID) (24) (1) (1) IN ('Y'
                ,'J')
    THEN
       pers_trow.meerling := 'J';
    ELSE
       pers_trow.meerling := 'N';
    END IF;
    -- *** adres
    -- binnen pid-11 opzoek naar type M (mailing); dit het enige beschikbare adres (= Epic adrestype "permanent")
    v_pid11_type := 0;
    v_pid11_typeM := 0;
    v_pid11_typeH := 0;
    v_debug := v_debug || 'f#=' || p_hl7_msg(v_PID) (11).COUNT;
    FOR i IN 1 .. p_hl7_msg(v_PID) (11).COUNT LOOP
      IF p_hl7_msg(v_PID) (11)(i).COUNT >= 7 THEN
        IF p_hl7_msg(v_PID) (11)(i)(7) = 'M' THEN
          v_pid11_typeM := i;
        ELSIF p_hl7_msg(v_PID) (11)(i)(7) = 'H' THEN
          v_pid11_typeH := i;
        ELSE
          v_pid11_type := i;
        END IF;
      END IF;
    END LOOP;

    v_debug := v_debug || 'g';
    IF v_pid11_typeM = 0 THEN
      pers_trow.adres := NULL;
      pers_trow.woonplaats := NULL;
      pers_trow.postcode := NULL;
      pers_trow.provincie := NULL;
      pers_trow.land := NULL;
    ELSE
      v_pid11_type := v_pid11_typeM;
      pers_trow.adres := SUBSTR(getAttrSub(p_hl7_msg(v_PID)(11)(v_pid11_type)(1), 2), 1, 250);
      pers_trow.adres := INITCAP(pers_trow.adres);
      v_tmp := NULL; -- huisnummer
      kgc_zis.getHL7waarde(getAttrSub(p_hl7_msg(v_PID)(11)(v_pid11_type)(1), 3), v_tmp);
      IF LENGTH(v_tmp) > 0 THEN
        pers_trow.adres := SUBSTR(pers_trow.adres || ' ' || v_tmp, 1, 250);
      END IF;
      IF nvl(length(p_hl7_msg(v_pid) (11) (v_pid11_type) (2)),0) > 0
      THEN -- huisnr-toevoeging aanwezig
        pers_trow.adres := SUBSTR(pers_trow.adres || ' ' || ltrim(p_hl7_msg(v_PID) (11) (v_pid11_type) (2)), 1, 250);
      END IF;
      pers_trow.woonplaats := rtrim(substr(p_hl7_msg(v_PID) (11) (v_pid11_type) (3),1,50));
      kgc_formaat_00.formatteer(p_naamdeel => 'WOONPLAATS'
                               ,x_naam     => pers_trow.woonplaats);
      pers_trow.postcode := rtrim(substr(p_hl7_msg(v_PID) (11) (v_pid11_type) (5),1,7)); -- formattering: specifiek!!!
      -- land
      IF p_hl7_msg(v_PID) (11) (v_pid11_type) (6) IS NULL THEN
        pers_trow.land := NULL;
      ELSE
        -- zoeken via extra attribuut ISO_CODE bij landen
        OPEN c_land_via_isocode(p_isocode => p_hl7_msg(v_PID) (11) (v_pid11_type) (6));
        FETCH c_land_via_isocode INTO pers_trow.land;
        IF c_land_via_isocode%NOTFOUND THEN
           pers_trow.land := NULL;
        END IF;
        CLOSE c_land_via_isocode;
      END IF;
    END IF;
    -- *** naam: gebruik altijd de Legal name - instance 1 van pid-5
    -- v5: zoeken!
    v_debug := v_debug || 'h';
    v_pid5_typeL := 0;
    v_pid5_typeN := 0;
    FOR i IN 1 .. p_hl7_msg(v_PID) (5).COUNT LOOP
      IF p_hl7_msg(v_PID) (5)(i).COUNT >= 7 THEN
        IF p_hl7_msg(v_PID) (5)(i)(7) = 'L' THEN
          v_pid5_typeL := i;
        ELSIF p_hl7_msg(v_PID) (5)(i)(7) = 'N' THEN
          v_pid5_typeN := i;
        END IF;
      END IF;
    END LOOP;

    -- *** roepnaam
    -- te vinden in PID-5-x-2 waarbij PID-5-x-7 = "N" (nickname)
    v_debug := v_debug || 'i';
    v_roepnaam := NULL;
    IF v_pid5_typeN > 0 THEN
      kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(5)(v_pid5_typeN)(2), v_roepnaam);
    END IF;
    v_debug := v_debug || '2';
    IF v_pid5_typeL > 0 THEN
      -- v5 zis-aanspreeknaam
      kgc_zis.getHL7waarde(getAttrSub(p_hl7_msg(v_PID)(5)(v_pid5_typeL)(1), 1), v_zis_aanspreken);
      -- achternaam+
      kgc_zis.getHL7waarde(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(v_pid5_typeL)(1), 3), 1, 30), pers_trow.achternaam);
      pers_trow.achternaam := TRIM(pers_trow.achternaam);
      kgc_formaat_00.formatteer( p_naamdeel => 'ACHTERNAAM', x_naam => pers_trow.achternaam);
      pers_trow.voorvoegsel := LOWER(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(v_pid5_typeL)(1), 2), 1, 10));
      kgc_formaat_00.formatteer( p_naamdeel => 'VOORVOEGSEL', x_naam => pers_trow.voorvoegsel);
      -- *** partner naam
      pers_trow.voorvoegsel_partner := LOWER(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(v_pid5_typeL)(1), 4), 1, 10));
      kgc_formaat_00.formatteer( p_naamdeel => 'VOORVOEGSEL', x_naam => pers_trow.voorvoegsel_partner);
      pers_trow.achternaam_partner := SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(v_pid5_typeL)(1), 5), 1, 30);
      kgc_formaat_00.formatteer( p_naamdeel => 'ACHTERNAAM', x_naam => pers_trow.achternaam_partner);
      -- *** voorletters / eerste VOORnaam
      SELECT NVL(MAX(atc.data_length), 30)
      INTO   v_column_length
      FROM   all_tab_columns atc
      WHERE  atc.TABLE_NAME = 'KGC_PERSONEN'
      AND    atc.OWNER = 'HELIX'
      AND    atc.column_name = 'VOORLETTERS';
      v_debug := v_debug || '3';
      v_voornaam1 := p_hl7_msg(v_PID)(5)(v_pid5_typeL)(2);
      pers_trow.voorletters := SUBSTR(REPLACE(p_hl7_msg(v_PID)(5)(v_pid5_typeL)(3), ' '), 1, v_column_length);
      v_debug := v_debug || 'a';
      IF NVL(LENGTH(v_voornaam1), 0) > 0 THEN
        pers_trow.voorletters := SUBSTR(SUBSTR(v_voornaam1, 1, 1) || '.' || pers_trow.voorletters, 1, v_column_length);
        -- v5: met puntjes
        pers_trow.voorletters := SUBSTR(kgc_zis.met_puntjes(REPLACE(REPLACE(pers_trow.voorletters, ' '), '.')), 1, v_column_length);
        v_debug := v_debug || 'b';
/* dit onderdeel is niet nodig, geen voornamen voor Nijmegen       -- indien leeftijd < 18: voornaam!
        IF pers_trow.geboortedatum IS NOT NULL THEN
          IF months_between(sysdate, pers_trow.geboortedatum) < 216 THEN

            IF v_roepnaam IS NOT NULL THEN
              v_debug := v_debug || 'c';
              -- pers_trow.voorletters := TRIM(SUBSTR(v_roepnaam, 1, 10)); -- commented by saroj, mantis 0182
              pers_trow.voorletters := TRIM(SUBSTR(v_roepnaam, 1, v_column_length)); -- added against previous commented row
            ELSIF v_voornaam1 IS NOT NULL THEN
              v_debug := v_debug || 'd';
              -- pers_trow.voorletters := TRIM(SUBSTR(v_voornaam1, 1, 10)); -- commented by saroj, mantis 0182
              pers_trow.voorletters := TRIM(SUBSTR(v_voornaam1, 1, v_column_length)); -- added against previous commented row
            END IF;
            v_debug := v_debug || 'e';
            kgc_formaat_00.formatteer( p_naamdeel => 'NAAM', x_naam => pers_trow.voorletters);
            -- soms voorletters in voornaam...
            IF TRIM(pers_trow.voorletters) LIKE '_' OR TRIM(pers_trow.voorletters) LIKE '_ _'  THEN
              v_debug := v_debug || 'f';
              pers_trow.voorletters := kgc_zis.met_puntjes(REPLACE(REPLACE(UPPER(pers_trow.voorletters), ' '), '.'));
            END IF;
          END IF;
        END IF;*/
      END IF;
    END IF;

    -- *** gehuwd
    -- met deze vertaling krijgen alle weduwen en gescheiden mensen ook de status 'N' (niet gehuwd)
    v_debug := v_debug || '4.1';
-- rde: nog kijken of dit relevant is
    IF p_hl7_msg(v_PID)(16)(1)(1) IN ('M', 'W') THEN
      pers_trow.gehuwd := 'J';
    ELSE
      pers_trow.gehuwd := 'N';
    END IF;

    -- *** aanspreken / default_aanspreken
    -- de aanspreeknaam moet altijd overgenomen worden, omdat in EPIC alle mogelijkheden gekozen kunnen worden
    -- in v_zis_aanspreken staat de volgorde wel goed maar de VOORVOEGSELS niet!
    -- en in Helix kun je alleen een combinatie krijgen dmv geslacht V en gehuwd.
    -- dus: aanspreken bepalen conform v_zis_aanspreken en overnemen; vervolgens 'default_aanspreken' bepalen
    v_zis_achternaam := pers_trow.achternaam;
    IF v_zis_achternaam IS NOT NULL AND pers_trow.voorvoegsel IS NOT NULL THEN
      v_zis_achternaam := pers_trow.voorvoegsel || ' ' || v_zis_achternaam;
    END IF;
    v_zis_achternaam_p := pers_trow.achternaam_partner;
    IF v_zis_achternaam_p IS NOT NULL AND pers_trow.voorvoegsel_partner IS NOT NULL THEN
      v_zis_achternaam_p := pers_trow.voorvoegsel_partner || ' ' || v_zis_achternaam_p;
    END IF;
    -- stel aanspreken samen
    pers_trow.aanspreken := pers_trow.voorletters;
    IF UPPER(v_zis_aanspreken) = UPPER(v_zis_achternaam) THEN
      IF pers_trow.voorvoegsel IS NOT NULL THEN
        kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.voorvoegsel, p_separator => ' ');
      END IF;
      kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.achternaam, p_separator => ' ');
    ELSIF UPPER(v_zis_aanspreken) = UPPER(v_zis_achternaam_p) THEN
      IF pers_trow.voorvoegsel_partner IS NOT NULL THEN
        kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.voorvoegsel_partner, p_separator => ' ');
      END IF;
      kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.achternaam_partner, p_separator => ' ');
    ELSIF UPPER(v_zis_aanspreken) = UPPER(v_zis_achternaam) || '-' || UPPER(v_zis_achternaam_p) THEN
      IF pers_trow.voorvoegsel IS NOT NULL THEN
        kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.voorvoegsel, p_separator => ' ');
      END IF;
      kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.achternaam, p_separator => ' ');
      -- partnernaam toevoegen?
      v_zis_achternaam_p := pers_trow.voorvoegsel_partner;
      kgc_import.addtext(x_text => v_zis_achternaam_p, p_max => 200, p_extra_text => pers_trow.achternaam_partner, p_separator => ' ');
      kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => v_zis_achternaam_p, p_separator => ' - ');
    ELSIF UPPER(v_zis_aanspreken) = UPPER(v_zis_achternaam_p) || '-' || UPPER(v_zis_achternaam) THEN
      IF pers_trow.voorvoegsel_partner IS NOT NULL THEN
        kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.voorvoegsel_partner, p_separator => ' ');
      END IF;
      kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.achternaam_partner, p_separator => ' ');
      -- eigennaam toevoegen?
      v_zis_achternaam := pers_trow.voorvoegsel;
      kgc_import.addtext(x_text => v_zis_achternaam, p_max => 200, p_extra_text => pers_trow.achternaam, p_separator => ' ');
      kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => v_zis_achternaam, p_separator => ' - ');
    ELSE -- formaat aanpassing?
      IF pers_trow.voorvoegsel IS NOT NULL THEN
        kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.voorvoegsel, p_separator => ' ');
      END IF;
      kgc_import.addtext(x_text => pers_trow.aanspreken, p_max => 80, p_extra_text => pers_trow.achternaam, p_separator => ' ');
    END IF;

    v_debug := v_debug || '5';

    kgc_formaat_00.standaard_aanspreken
    ( p_type => 'PERS'
    , p_voorletters => pers_trow.voorletters
    , p_voorvoegsel => pers_trow.voorvoegsel
    , p_achternaam => pers_trow.achternaam
    , p_achternaam_partner => pers_trow.achternaam_partner
    , p_voorvoegsel_partner => pers_trow.voorvoegsel_partner
    , p_gehuwd => pers_trow.gehuwd
    , p_geslacht => pers_trow.geslacht
    , x_aanspreken => v_helix_aanspreken
    );

    IF pers_trow.aanspreken = v_helix_aanspreken THEN
      pers_trow.default_aanspreken := 'J';
    ELSE
      pers_trow.default_aanspreken := 'N';
    END IF;

    -- formateren...
    kgc_zis.format_geo
    ( x_postcode => pers_trow.postcode
    , x_woonplaats => pers_trow.woonplaats
    , x_provincie => pers_trow.provincie
    , x_land => pers_trow.land
    , x_msg => v_msg
    );

    -- *** overleden
    v_debug := v_debug || '7';
    IF p_hl7_msg(v_PID)(30)(1)(1) = 'Y' THEN
      pers_trow.overleden := 'J';
      BEGIN
        pers_trow.overlijdensdatum := TO_DATE(SUBSTR(p_hl7_msg(v_PID)(29)(1)(1),1,8), 'YYYYMMDD');
      EXCEPTION
        WHEN OTHERS THEN NULL;
      END;
    ELSE
      pers_trow.overleden := 'N';
      pers_trow.overlijdensdatum := NULL;
    END IF;
    -- *** telefoon; 2.4: zowel pid-13 als 14 zijn herhalende groepen!
    v_debug := v_debug || '8=' || p_hl7_msg(v_PID)(13).COUNT;
    -- verzamel alle telefoonnummers, te beginnen bij pid-13, de prive-nummers
    v_telf_tab.delete;
    FOR i IN 1.. p_hl7_msg(v_PID)(13).COUNT LOOP
      IF p_hl7_msg(v_PID)(13)(i).COUNT >= 3 THEN
        IF p_hl7_msg(v_PID)(13)(i)(2) IN ('PRN') THEN
--        IF p_hl7_msg(v_PID)(13)(i)(2) IN ('PRN', 'M') THEN -- LET OP: M is geen HL7, maar wordt alleen voor de ADT-A08 gebruikt. Kan weg zodra dit in EPIC is aangepast!
          v_tmp := NULL;
    v_debug := v_debug || 'prn=' || p_hl7_msg(v_PID)(13)(i)(1);
          kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(13)(i)(1), v_tmp);
          IF LENGTH(v_tmp) > 0 THEN
            v_telf_tab(v_telf_tab.count + 1) := SUBSTR('PRN_' || p_hl7_msg(v_PID)(13)(i)(3) || '_' || v_tmp, 1, 200);
          END IF;
--        ELSIF p_hl7_msg(v_PID)(13)(i)(2) IN ('NET') THEN
        ELSIF p_hl7_msg(v_PID)(13)(i)(2) IN ('NET') AND p_hl7_msg(v_PID)(13)(i).COUNT >= 4 THEN -- aanpassing rde 13-12-2013
          v_tmp := NULL;
          kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(13)(i)(4), v_tmp);
          IF LENGTH(v_tmp) = 0 THEN
            pers_trow.email := NULL;
          ELSE
            -- email-adres gevonden! nu nog lengte 30, binnenkort langer...
            SELECT NVL(MAX(atc.data_length), 30)
            INTO   v_column_length
            FROM   all_tab_columns atc
            WHERE  atc.TABLE_NAME = 'KGC_PERSONEN'
            AND    atc.OWNER = 'HELIX'
            AND    atc.column_name = 'EMAIL';
            IF LENGTH(v_tmp) <= v_column_length THEN
               pers_trow.email := v_tmp;
            END IF;
          END IF;
        END IF;
      END IF;
    END LOOP;
    FOR i IN 1.. p_hl7_msg(v_PID)(14).COUNT LOOP
      IF p_hl7_msg(v_PID)(14)(i).COUNT >= 3 THEN
        IF p_hl7_msg(v_PID)(14)(i)(2) IN ('WPN') THEN
          v_tmp := NULL;
          kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(14)(i)(1), v_tmp);
          IF LENGTH(v_tmp) > 0 THEN
--            v_telf_tab(v_telf_tab.count + 1) := SUBSTR('WPN_' || p_hl7_msg(v_PID)(13)(i)(3) || '_' || v_tmp, 1, 200);
            v_telf_tab(v_telf_tab.count + 1) := SUBSTR('WPN_' || p_hl7_msg(v_PID)(14)(i)(3) || '_' || v_tmp, 1, 200);  -- aanpassing rde 13-12-2013
          END IF;
        END IF;
      END IF;
    END LOOP;
    v_debug := v_debug || 'k' || v_telf_tab.COUNT;
    -- zoek telefoon 1 + 2;
    pers_trow.telefoon := NULL;
    pers_trow.telefoon2 := NULL;
    -- pers.telefoon1 en 2 worden in de volgende volgorde bepaald:
    -- 1. PRN + CP (�prive 06� eerst);
    -- 2. PRN + PH;
    -- 3. WPN + PH, en
    -- als laatste WPN + CP; deze wordt echter nooit als 1e telefoon gebruikt, want hier worden �vreugde smsjes� naar verzonden, en dit moet geen werk-06 nummer zijn
    IF v_telf_tab.COUNT > 0 THEN
      FOR j IN 1 .. 4 LOOP -- 4 combinaties
        FOR i IN 1 .. v_telf_tab.COUNT LOOP
          IF j = 1 AND v_telf_tab(i) LIKE 'PRN_CP_%' THEN
            IF pers_trow.telefoon IS NULL THEN
              pers_trow.telefoon := SUBSTR(v_telf_tab(i), 8);
            ELSIF pers_trow.telefoon2 IS NULL THEN
              pers_trow.telefoon2 := SUBSTR(v_telf_tab(i), 8);
            END IF;
          ELSIF j = 2 AND v_telf_tab(i) LIKE 'PRN_PH_%' THEN
            IF pers_trow.telefoon IS NULL THEN
    v_debug := v_debug || '/' || v_telf_tab(i);
              pers_trow.telefoon := SUBSTR(v_telf_tab(i), 8);
            ELSIF pers_trow.telefoon2 IS NULL THEN
              pers_trow.telefoon2 := SUBSTR(v_telf_tab(i), 8);
            END IF;
          ELSIF j = 3 AND v_telf_tab(i) LIKE 'WPN_PH_%' THEN
            IF pers_trow.telefoon IS NULL THEN
              pers_trow.telefoon := SUBSTR(v_telf_tab(i), 8);
            ELSIF pers_trow.telefoon2 IS NULL THEN
              pers_trow.telefoon2 := SUBSTR(v_telf_tab(i), 8);
            END IF;
          ELSIF j = 4 AND v_telf_tab(i) LIKE 'WPN_CP_%' THEN -- nooit als eerste telefoon!
            IF pers_trow.telefoon2 IS NULL THEN
              pers_trow.telefoon2 := SUBSTR(v_telf_tab(i), 8);
            END IF;
          END IF;
        END LOOP;
      END LOOP;
    END IF;
  END IF;

  -- huisarts: deze wordt niet meer overgenomen! Mantis 0009798: Niet importeren van huisarts gegevens van ADT berichten
  v_debug := v_debug || '9';
  rela_trow := v_rela_trow_null;
  rela_trow.rela_id := kgc_uk2pk.rela_id('ONBHUISART');
  IF rela_trow.rela_id IS NOT NULL THEN
     cg$kgc_relaties.slct(rela_trow);
  END IF;
  pers_trow.rela_id := rela_trow.rela_id; -- opnemen bij persoon!

  -- verzekeraar / verzekering: deze worden niet overgenomen!
  pers_trow.verzekeringsnr := NULL;
  pers_trow.verzekeringswijze := 'O';
  open c_verz('ONB');
  fetch c_verz into pers_trow.verz_id;
  close c_verz;

  -- *** wie heeft de wijziging uitgevoerd?
  pers_trow.last_updated_by := SUBSTRB(p_hl7_msg(v_EVN) (5) (1) (1)
    || ';' || p_hl7_msg(v_EVN) (5) (1) (2)
    || ' ' || p_hl7_msg(v_EVN) (5) (1) (3), 1, 30)
    ;

  v_debug := v_debug || '99';
  kgc_zis.log(v_thisproc || ': Klaar; debug=' || v_debug);

EXCEPTION
  WHEN OTHERS THEN
    kgc_zis.log(v_thisproc || ': ' || v_debug || '/' || SQLERRM);
    RAISE; -- door naar aanroeper
END;

-- public
PROCEDURE persoon_in_zis
( p_zisnr IN VARCHAR2
, pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
) IS
  v_ok BOOLEAN;
  x_msg VARCHAR2(32767);
BEGIN
  -- hier de aanroep naar patcomp opnemen?
  null;
END;


-- deze wordt gebruikt om in aanleverende berichten een persoon te identificeren
-- indien een segment niet aanwezig is in x_hl7_msg, dan wordt deze niet gevuld!
PROCEDURE rec2hl7msg
( pers_trow IN     cg$KGC_PERSONEN.cg$row_type
, rela_trow IN     cg$KGC_RELATIES.cg$row_type
, verz_trow IN     cg$KGC_VERZEKERAARS.cg$row_type
, p_extra   IN     VARCHAR2
, x_hl7_msg IN OUT kgc_zis.hl7_msg
) IS
  v_PID              VARCHAR2(10);
BEGIN
  v_PID := kgc_zis.getSegmentCode('PID', x_hl7_msg);
  IF NVL(LENGTH(v_PID), 0) > 0
    THEN
    -- specifiek voor Nijmegen:
    -- *** geslacht
    IF pers_trow.geslacht in ('M', 'V', 'O')
        THEN
      x_hl7_msg('PID')(8)(1)(1) := pers_trow.geslacht;
    END IF; -- en anders: leeg!
    -- *** meerling
    x_hl7_msg(v_PID)(24)(1)(1) := pers_trow.meerling;
    -- *** aanvullend op generieke velden:
    -- *** ZISnr
    x_hl7_msg(v_PID)(3)(1)(1) := pers_trow.zisnr;

    -- *** adres
    -- adres - ai, in Helix is straat en huisnummer niet gescheiden; -in US-profile HL7 ook niet!
    --  Nijm: XAD-componenten: straat^huis-nr^woonplaats^^postcode^land
    x_hl7_msg(v_PID)(11)(1)(1) := pers_trow.adres;
    x_hl7_msg(v_PID)(11)(1)(3) := pers_trow.woonplaats;
    x_hl7_msg(v_PID)(11)(1)(4) := pers_trow.land;
    x_hl7_msg(v_PID)(11)(1)(5) := pers_trow.postcode;
  END IF;

END;

FUNCTION verwerk_ander_bericht -- specifiek
( p_hlbe_id   NUMBER
, p_hl7_msg   kgc_zis.hl7_msg
, x_entiteit  IN OUT VARCHAR2
, x_id        IN OUT NUMBER
) RETURN VARCHAR2 IS
  v_ret_status       kgc_hl7_berichten.status%TYPE := 'V';
  v_debug            VARCHAR2(100);
  v_thisproc         VARCHAR2(100) := 'kgc_zis_nijmegen.verwerk_ander_bericht';
BEGIN
  -- nog geen specifieke berichten aanwezig voor nijmegen
  RETURN v_ret_status;
END;

FUNCTION decl2dft_p03
( p_decl_id        IN     NUMBER
, p_aantal         IN     NUMBER
, p_check_only     IN     BOOLEAN
, p_msg_control_id IN     VARCHAR2
, x_msg            IN OUT VARCHAR2
) RETURN BOOLEAN IS
BEGIN
  RETURN FALSE; -- deze logica was al verplaatst naar kgc_zis_nijmegen; hier weggehaald tegelijk met mantis 283 ter voorkoming verwarring
END;

END KGC_ZIS_NIJMEGEN;
/

/
QUIT
