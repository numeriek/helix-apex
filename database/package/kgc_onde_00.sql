CREATE OR REPLACE PACKAGE "HELIX"."KGC_ONDE_00" IS
-- Controleer of onderzoek mag worden afgerond
PROCEDURE afronden_ok;

-- Controleer of onderzoek is afgerond; ingang kan van alles zijn (zie parameterlijst)
FUNCTION afgerond
( p_onde_id IN NUMBER := NULL
, p_uits_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_frac_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
, p_rftf IN BOOLEAN := TRUE
)
RETURN BOOLEAN;

-- Controleer of onderzoek is goedgekeurd; ingang kan van alles zijn (zie parameterlijst)
FUNCTION goedgekeurd
( p_onde_id IN NUMBER := NULL
, p_uits_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_frac_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_meet_id IN NUMBER := NULL
)
RETURN BOOLEAN;

-- Bepaal medewerker (beoordelaar) waaraan onderzoek wordt toegekend
-- 1. zelfde beoordelaar als bij het vorige onderzoek op die patient
-- 2. de autorisator met het minst aantal toegewezen openstaande onderzoek
FUNCTION beoordelaar
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER
, p_pers_id IN NUMBER
)
RETURN NUMBER
;
-- Bepaal of alle meetwaarden bij een onderzoek voldoen aan de standaard succes-voorwaarden
-- alle meetwaarden zijn afgerond
-- alle meetwaarden vallen binnen de normaalwaarden
-- alle meetwaarde-details voldoen aan de succes-voorwaarde
PROCEDURE meetwaarden_succes
( p_onde_id IN NUMBER
, x_result OUT BOOLEAN
, x_opmerking OUT VARCHAR2
);

-- als alle onderzoeken bij een familieonderzoek zijn afgerond
-- wordt het familieonderzoek ook afgerond
PROCEDURE familieonderzoek_afronden
( p_onde_id IN NUMBER
);

-- Retourneer onderzoeknr (en eventueel andere info)
-- Gebruikt bij controle afgerond in diverse db-triggers
FUNCTION onderzoek_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( onderzoek_info, WNDS, WNPS );

-- Is medewerker bevoegd om afgerond onderzoek te wijzigen?
FUNCTION bevoegd
( p_mede_id IN NUMBER := NULL
)
RETURN BOOLEAN;

-- zet de resultaattekst (meting) van een onderzoek in een (1) veld
-- gebruik alleen de teksten van de onderzochte persoon
FUNCTION resultaat_tekst
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( resultaat_tekst, WNDS, WNPS, RNPS );

-- zet de gerelateerde onderzoeken van een onderzoek in een (1) veld
FUNCTION gerelateerde_onderzoeken
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( gerelateerde_onderzoeken, WNDS, WNPS, RNPS );

-- de taal bij het onderzoek (leeg is Nederlands)
FUNCTION taal_id
( p_onde_id IN NUMBER
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( taal_id, WNDS, WNPS, RNPS );

-- toon stand van zaken van testen voor een onderzoek
FUNCTION meetwaarden_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;

-- toon stand van zaken van declaraties voor een onderzoek
FUNCTION declaraties_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;

-- omzeil mutating table probleem: zet af te ronden onderzoeken in een pl/sql-table
PROCEDURE controleer_afronden
( p_onde_id IN NUMBER
);
--
-- kopieer onderzoek in andere onderzoeksgroep
FUNCTION kopieer_onderzoek
( p_onde_id_orig IN NUMBER
, p_onde_id_nieuw IN NUMBER := NULL
, p_ongr_id_nieuw IN NUMBER := NULL
, p_mons_id_orig IN NUMBER := NULL
)
RETURN NUMBER;
--
PROCEDURE kopieer_onderzoek
( p_onde_id_orig IN NUMBER
, p_onde_id_nieuw IN NUMBER := NULL
, p_ongr_id_nieuw IN NUMBER := NULL
, p_mons_id_orig IN NUMBER := NULL
);
--
-- Monsternummers in volgorde van aanmelden, laatst aangemelde monsternummer eerst
FUNCTION monsters_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
--
-- Fractienummers in volgorde van aanmelden, laatst aangemelde fractienummer eerst
FUNCTION fracties_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
--
FUNCTION fracties_info_alfabetisch
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
--
-- Materiaal dat bij een onderzoeksmonster hoort
FUNCTION materiaal_info
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
-- Idem mate_id bij onderzoek(monster)...
FUNCTION monster_mate_id
( p_onde_id IN NUMBER
)
RETURN NUMBER;

FUNCTION counselor
( P_ONDE_ID IN NUMBER)
RETURN VARCHAR2
;
--
-- Sorteerkenmerk van een onderzoek bepalen
FUNCTION sorteerkenmerk
( P_ONDE_ID IN NUMBER )
RETURN VARCHAR2
;
-- used in KGCONDE22 module, created by saroj for mantis 4499
 PROCEDURE search_conc_tote_teksten (p_onde_list in OUT varchar2, p_entity varchar2 :=  'CONC', p_flag out varchar2, p_indi_list out varchar2 );
 -- used in KGCONDE22 module, created by Abhijit  for mantis 4499
 PROCEDURE search_rete_teksten (p_onde_list in OUT VARCHAR2, p_flag out VARCHAR2,p_stgr_list out varchar2 );
END KGC_ONDE_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ONDE_00" IS
  -- gevoed via BUR-trigger (af te ronden onderzoeken), geleegd via AUS-trigger
  TYPE onde_id_tab_type IS TABLE OF NUMBER INDEX BY BINARY_INTEGER; -- onde_id
  onde_id_tab onde_id_tab_type;

  -- Omschrijf de afrondvoorwaarden
  FUNCTION afrondvoorwaarden(p_kafd_id     IN NUMBER
                            ,p_entiteit_pk IN VARCHAR2) RETURN VARCHAR2 IS
    v_return VARCHAR2(4000);
    CURSOR afcr_cur IS
      SELECT omschrijving
      FROM   kgc_afrond_criteria
      WHERE  kafd_id = p_kafd_id
      AND    entiteit_pk = UPPER(p_entiteit_pk)
      AND    omschrijving IS NOT NULL
      ORDER  BY table_view
               ,column_name;
  BEGIN
    FOR r IN afcr_cur
    LOOP
      IF (v_return IS NULL)
      THEN
        v_return := r.omschrijving;
      ELSE
        v_return := SUBSTR(v_return || CHR(10) || r.omschrijving
                          ,1
                          ,4000);
      END IF;
    END LOOP;
    RETURN(v_return);
  END afrondvoorwaarden;

  PROCEDURE controleer_afronden(p_onde_id IN NUMBER) IS
  BEGIN
    onde_id_tab(p_onde_id) := p_onde_id;
  END controleer_afronden;

  PROCEDURE afronden_ok IS
    v_entiteit_pk CONSTANT VARCHAR2(10) := 'ONDE_ID';
    v_onde_id BINARY_INTEGER := onde_id_tab.first;
    CURSOR onde_cur(b_onde_id IN NUMBER) IS
      SELECT kafd_id FROM kgc_onderzoeken WHERE onde_id = b_onde_id;
    onde_rec onde_cur%ROWTYPE;

    CURSOR afcr_cur(b_kafd_id IN NUMBER) IS
      SELECT entiteit_pk
            ,table_view
            ,column_name
            ,waarde
            ,gevuld
            ,alle_rijen
            ,omschrijving
      FROM   kgc_afrond_criteria
      WHERE  kafd_id = b_kafd_id
      AND    entiteit_pk = v_entiteit_pk
      AND    (gevuld = 'J' OR waarde IS NOT NULL)
      ORDER  BY table_view
               ,column_name;
    v_voorwaarde  VARCHAR2(1000);
    v_statement   VARCHAR2(32767);
    v_ok          VARCHAR2(2);
    v_afronden_ok BOOLEAN := TRUE;
    v_melding     VARCHAR2(4000) := NULL;
  BEGIN
    WHILE (v_onde_id IS NOT NULL)
    LOOP
      OPEN onde_cur(v_onde_id);
      FETCH onde_cur
        INTO onde_rec;
      CLOSE onde_cur;

      FOR r IN afcr_cur(onde_rec.kafd_id)
      LOOP
        v_voorwaarde := r.column_name ||
                        kgc_util_00.ite(r.gevuld = 'J'
                                       ,' is not null'
                                       ,' = ''' || r.waarde || '''');
        v_statement  := 'begin';
        IF (r.alle_rijen = 'N')
        THEN
          -- er is een rij met de voorwaarde
          v_statement := v_statement || CHR(10) ||
                         'select min(''J'') into :result from ' ||
                         r.table_view || CHR(10) || 'where (' ||
                         v_voorwaarde || ')';
        ELSE
          -- er is geen rij met niet de voorwaarde
          v_statement := v_statement || CHR(10) ||
                         'select min(''N'') into :result from ' ||
                         r.table_view || CHR(10) || 'where not (' ||
                         v_voorwaarde || ')';
        END IF;
        v_statement := v_statement || CHR(10) || 'and onde_id = ' ||
                       TO_CHAR(v_onde_id) || ';' || CHR(10) || 'end;';
        v_ok        := kgc_util_00.dyn_exec(v_statement);
        IF (v_ok = 'N')
        THEN
          v_afronden_ok := FALSE;
          IF (v_melding IS NULL)
          THEN
            v_melding := r.omschrijving;
          ELSE
            v_melding := SUBSTR(v_melding || CHR(10) || r.omschrijving
                               ,1
                               ,4000);
          END IF;
        END IF;
      END LOOP;
      onde_id_tab.delete(v_onde_id);
      IF (NOT v_afronden_ok)
      THEN
        qms$errors.show_message(p_mesg   => 'KGC-00017'
                               ,p_param0 => v_melding
                               ,p_param1 => afrondvoorwaarden(p_kafd_id     => onde_rec.kafd_id
                                                             ,p_entiteit_pk => v_entiteit_pk)
                               ,p_errtp  => 'E'
                               ,p_rftf   => TRUE);
      END IF;
      v_onde_id := onde_id_tab.next(v_onde_id);
    END LOOP;
  END afronden_ok;

  FUNCTION afgerond(p_onde_id IN NUMBER := NULL
                   ,p_uits_id IN NUMBER := NULL
                   ,p_mons_id IN NUMBER := NULL
                   ,p_frac_id IN NUMBER := NULL
                   ,p_meti_id IN NUMBER := NULL
                   ,p_meet_id IN NUMBER := NULL
                   ,p_rftf    IN BOOLEAN := TRUE) RETURN BOOLEAN IS
    v_return BOOLEAN := FALSE;
    CURSOR onde_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
      WHERE  onde.onde_id = p_onde_id
      AND    onde.afgerond = 'J';
    CURSOR uits_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,kgc_uitslagen   uits
      WHERE  uits.onde_id = onde.onde_id
      AND    onde.afgerond = 'J'
      AND    uits.uits_id = p_uits_id;
    CURSOR onmo_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken        onde
            ,kgc_onderzoek_monsters onmo
      WHERE  onmo.onde_id = onde.onde_id
      AND    onde.afgerond = 'J'
      AND    onmo.mons_id = p_mons_id;
    CURSOR frac_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,bas_metingen    meti
            ,bas_fracties    frac
      WHERE  meti.onde_id = onde.onde_id
      AND    onde.afgerond = 'J'
      AND    meti.frac_id = frac.frac_id
      AND    frac.frac_id = p_frac_id;
    CURSOR meti_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,bas_metingen    meti
      WHERE  meti.onde_id = onde.onde_id
      AND    onde.afgerond = 'J'
      AND    meti.meti_id = p_meti_id;
    CURSOR meet_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,bas_metingen    meti
            ,bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = onde.onde_id
      AND    onde.afgerond = 'J'
      AND    meet.meet_id = p_meet_id;
    v_onderzoeknr VARCHAR2(20);
  BEGIN
    IF (p_onde_id IS NOT NULL)
    THEN
      OPEN onde_cur;
      FETCH onde_cur
        INTO v_onderzoeknr;
      v_return := onde_cur%FOUND;
      CLOSE onde_cur;
    ELSIF (p_uits_id IS NOT NULL)
    THEN
      OPEN uits_cur;
      FETCH uits_cur
        INTO v_onderzoeknr;
      v_return := uits_cur%FOUND;
      CLOSE uits_cur;
    ELSIF (p_mons_id IS NOT NULL)
    THEN
      OPEN onmo_cur;
      FETCH onmo_cur
        INTO v_onderzoeknr;
      v_return := onmo_cur%FOUND;
      CLOSE onmo_cur;
    ELSIF (p_frac_id IS NOT NULL)
    THEN
      OPEN frac_cur;
      FETCH frac_cur
        INTO v_onderzoeknr;
      v_return := frac_cur%FOUND;
      CLOSE frac_cur;
    ELSIF (p_meti_id IS NOT NULL)
    THEN
      OPEN meti_cur;
      FETCH meti_cur
        INTO v_onderzoeknr;
      v_return := meti_cur%FOUND;
      CLOSE meti_cur;
    ELSIF (p_meet_id IS NOT NULL)
    THEN
      OPEN meet_cur;
      FETCH meet_cur
        INTO v_onderzoeknr;
      v_return := meet_cur%FOUND;
      CLOSE meet_cur;
    END IF;
    IF (p_rftf)
    THEN
      IF (v_return)
      THEN
        qms$errors.show_message(p_mesg   => 'KGC-00045'
                               ,p_param0 => v_onderzoeknr
                               ,p_errtp  => 'E'
                               ,p_rftf   => TRUE);
      END IF;
    END IF;
    RETURN(v_return);
  END afgerond;

  FUNCTION goedgekeurd(p_onde_id IN NUMBER := NULL
                      ,p_uits_id IN NUMBER := NULL
                      ,p_mons_id IN NUMBER := NULL
                      ,p_frac_id IN NUMBER := NULL
                      ,p_meti_id IN NUMBER := NULL
                      ,p_meet_id IN NUMBER := NULL) RETURN BOOLEAN IS
    v_return BOOLEAN := FALSE;
    CURSOR onde_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
      WHERE  onde.onde_id = p_onde_id
      AND    onde.status = 'G';
    CURSOR uits_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,kgc_uitslagen   uits
      WHERE  uits.onde_id = onde.onde_id
      AND    onde.status = 'G'
      AND    uits.uits_id = p_uits_id;
    CURSOR onmo_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken        onde
            ,kgc_onderzoek_monsters onmo
      WHERE  onmo.onde_id = onde.onde_id
      AND    onde.status = 'G'
      AND    onmo.mons_id = p_mons_id;
    CURSOR frac_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,bas_metingen    meti
            ,bas_fracties    frac
      WHERE  meti.onde_id = onde.onde_id
      AND    onde.status = 'G'
      AND    meti.frac_id = frac.frac_id
      AND    frac.frac_id = p_frac_id;
    CURSOR meti_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,bas_metingen    meti
      WHERE  meti.onde_id = onde.onde_id
      AND    onde.status = 'G'
      AND    meti.meti_id = p_meti_id;
    CURSOR meet_cur IS
      SELECT onde.onderzoeknr
      FROM   kgc_onderzoeken onde
            ,bas_metingen    meti
            ,bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = onde.onde_id
      AND    onde.status = 'G'
      AND    meet.meet_id = p_meet_id;
    v_onderzoeknr VARCHAR2(20);
  BEGIN
    IF (p_onde_id IS NOT NULL)
    THEN
      OPEN onde_cur;
      FETCH onde_cur
        INTO v_onderzoeknr;
      v_return := onde_cur%FOUND;
      CLOSE onde_cur;
    ELSIF (p_uits_id IS NOT NULL)
    THEN
      OPEN uits_cur;
      FETCH uits_cur
        INTO v_onderzoeknr;
      v_return := uits_cur%FOUND;
      CLOSE uits_cur;
    ELSIF (p_mons_id IS NOT NULL)
    THEN
      OPEN onmo_cur;
      FETCH onmo_cur
        INTO v_onderzoeknr;
      v_return := onmo_cur%FOUND;
      CLOSE onmo_cur;
    ELSIF (p_frac_id IS NOT NULL)
    THEN
      OPEN frac_cur;
      FETCH frac_cur
        INTO v_onderzoeknr;
      v_return := frac_cur%FOUND;
      CLOSE frac_cur;
    ELSIF (p_meti_id IS NOT NULL)
    THEN
      OPEN meti_cur;
      FETCH meti_cur
        INTO v_onderzoeknr;
      v_return := meti_cur%FOUND;
      CLOSE meti_cur;
    ELSIF (p_meet_id IS NOT NULL)
    THEN
      OPEN meet_cur;
      FETCH meet_cur
        INTO v_onderzoeknr;
      v_return := meet_cur%FOUND;
      CLOSE meet_cur;
    END IF;
    RETURN(v_return);
  END goedgekeurd;

  FUNCTION beoordelaar(p_kafd_id IN NUMBER
                      ,p_ongr_id IN NUMBER
                      ,p_pers_id IN NUMBER) RETURN NUMBER IS
    v_mede_id NUMBER;
    -- beoordelaar van een eerder onderzoek?
    CURSOR onde_cur IS
      SELECT mede_id_beoordelaar
      FROM   kgc_onderzoeken
      WHERE  kafd_id = p_kafd_id
      AND    ongr_id = p_ongr_id
      AND    pers_id = p_pers_id
      AND    mede_id_beoordelaar IS NOT NULL
      ORDER  BY onde_id DESC -- laatste eerst
      ;
    -- reeds toegewezen onderzoeken per autorisator (niet per patient!)
    CURSOR auto_cur IS
      SELECT meka.mede_id
            ,COUNT(onde.onde_id) aantal
      FROM   kgc_onderzoeken onde
            ,kgc_mede_kafd   meka
      WHERE  onde.mede_id_beoordelaar(+) = meka.mede_id
      AND    NVL(onde.afgerond
                ,'N') = 'N'
      AND    meka.autorisator = 'J'
      AND    meka.rol = 'STAF'
      AND    meka.kafd_id = p_kafd_id
      AND    NVL(onde.kafd_id
                ,p_kafd_id) = p_kafd_id
      AND    NVL(onde.ongr_id
                ,p_ongr_id) = p_ongr_id
      GROUP  BY meka.mede_id
      ORDER  BY 2 -- minste aantal eerst
               ,1;
    v_aantal NUMBER;
  BEGIN
    IF (kgc_sypa_00.standaard_waarde(p_parameter_code => 'ONDERZOEKEN_TOEKENNEN'
                                    ,p_kafd_id        => p_kafd_id
                                    ,p_ongr_id        => p_ongr_id) = 'J')
    THEN
      OPEN onde_cur;
      FETCH onde_cur
        INTO v_mede_id;
      CLOSE onde_cur;
      -- verspreidt de onderzoeken over de autorisatoren
      IF (v_mede_id IS NULL)
      THEN
        OPEN auto_cur;
        FETCH auto_cur
          INTO v_mede_id
              ,v_aantal;
        CLOSE auto_cur;
      END IF;
    END IF;
    RETURN(v_mede_id);
  END beoordelaar;

  PROCEDURE meetwaarden_succes(p_onde_id   IN NUMBER
                              ,x_result    OUT BOOLEAN
                              ,x_opmerking OUT VARCHAR2) IS
    v_aantal NUMBER;
    -- een meetwaarde niet afgerond?
    CURSOR meet_afg_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen    meti
            ,bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = p_onde_id
      AND    meti.afgerond = 'N'
      AND    meet.afgerond = 'N';
    -- een (nummerieke) meetwaarde buiten referentiewaarden?
    CURSOR meet_ref_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen    meti
            ,bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = p_onde_id
      AND    (meet.meetwaarde < meet.normaalwaarde_ondergrens OR
            meet.meetwaarde > meet.normaalwaarde_bovengrens)
      AND    LTRIM(TRANSLATE(meet.meetwaarde
                            ,'0123456789-+.'
                            ,'             ')) IS NULL;
    CURSOR mwss_cur IS
      SELECT mwss.succes_voorwaarde
            ,mdet.waarde
      FROM   kgc_mwst_samenstelling mwss
            ,bas_meetwaarde_details mdet
            ,bas_metingen           meti
            ,bas_meetwaarden        meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = p_onde_id
      AND    meet.mwst_id = mwss.mwst_id
      AND    mwss.succes_voorwaarde IS NOT NULL
      AND    meet.meet_id = mdet.meet_id
      AND    mdet.volgorde = mwss.volgorde
      AND    mdet.waarde IS NOT NULL;
    v_statement VARCHAR2(10000);
    v_dummy     VARCHAR2(1);
  BEGIN
    x_result := TRUE;
    v_aantal := 0;
    IF (x_result)
    THEN
      OPEN meet_afg_cur;
      FETCH meet_afg_cur
        INTO v_aantal;
      CLOSE meet_afg_cur;
      IF (v_aantal > 0)
      THEN
        x_result    := FALSE;
        x_opmerking := TO_CHAR(v_aantal) ||
                       ' meetwaarden zijn nog niet afgerond';
      END IF;
    END IF;
    v_aantal := 0;
    IF (x_result)
    THEN
      OPEN meet_ref_cur;
      FETCH meet_ref_cur
        INTO v_aantal;
      CLOSE meet_ref_cur;
      IF (v_aantal > 0)
      THEN
        x_result    := FALSE;
        x_opmerking := TO_CHAR(v_aantal) ||
                       ' meetwaarden vallen buiten normaalwaarden';
      END IF;
    END IF;
    v_aantal := 0;
    IF (x_result)
    THEN
      FOR r IN mwss_cur
      LOOP
        r.succes_voorwaarde := REPLACE(r.succes_voorwaarde
                                      ,'<waarde>'
                                      ,'<WAARDE>');
        r.succes_voorwaarde := REPLACE(r.succes_voorwaarde
                                      ,'<WAARDE>'
                                      ,r.waarde);
        v_statement         := 'select ''X'' from dual where ' ||
                               r.succes_voorwaarde;
        v_dummy             := kgc_util_00.dyn_exec(v_statement);
        IF (v_dummy = 'X')
        THEN
          v_aantal := v_aantal + 1;
        END IF;
      END LOOP;
      IF (v_aantal > 0)
      THEN
        x_result    := FALSE;
        x_opmerking := TO_CHAR(v_aantal) ||
                       ' meetwaarde details voldoen niet aan de succes-voorwaarde';
      END IF;
    END IF;
  END meetwaarden_succes;

  PROCEDURE familieonderzoek_afronden(p_onde_id IN NUMBER) IS
    CURSOR faon_cur IS
      SELECT faon.faon_id
            ,faon.afgerond
      FROM   kgc_familie_onderzoeken faon
      WHERE  faon.afgerond = 'N'
      AND    EXISTS (SELECT NULL
              FROM   kgc_faon_betrokkenen fobe
              WHERE  fobe.faon_id = faon.faon_id
              AND    fobe.onde_id = p_onde_id)
      AND    NOT EXISTS (SELECT NULL
              FROM   kgc_faon_betrokkenen fobe
                    ,kgc_onderzoeken      onde
              WHERE  fobe.faon_id = faon.faon_id
              AND    fobe.onde_id = onde.onde_id
              AND    onde.afgerond = 'N')
      FOR    UPDATE OF faon.afgerond NOWAIT;
    faon_rec faon_cur%ROWTYPE;
  BEGIN
    IF (p_onde_id IS NOT NULL)
    THEN
      FOR r IN faon_cur
      LOOP
        BEGIN
          UPDATE kgc_familie_onderzoeken
          SET    afgerond = 'J'
          WHERE  CURRENT OF faon_cur;
        EXCEPTION
          WHEN OTHERS THEN
            qms$errors.show_message(p_mesg   => 'KGC-00000'
                                   ,p_param0 => 'Familieonderzoek is niet afgerond!'
                                   ,p_param1 => SQLERRM);
        END;
      END LOOP;
    END IF;
  END familieonderzoek_afronden;

  FUNCTION onderzoek_info(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_return VARCHAR2(1000);
    CURSOR onde_cur IS
      SELECT onderzoeknr FROM kgc_onderzoeken WHERE onde_id = p_onde_id;
  BEGIN
    OPEN onde_cur;
    FETCH onde_cur
      INTO v_return;
    CLOSE onde_cur;
    RETURN(v_return);
  END onderzoek_info;

  FUNCTION bevoegd(p_mede_id IN NUMBER := NULL) RETURN BOOLEAN IS
    v_return BOOLEAN := TRUE;
    CURSOR spwa_cur(b_mede_id IN NUMBER) IS
      SELECT NULL
      FROM   kgc_systeem_par_waarden spwa
            ,kgc_systeem_parameters  sypa
      WHERE  sypa.sypa_id = spwa.sypa_id
      AND    sypa.code = 'BEVOEGD_WIJZIG_ONDERZOEK'
      AND    (spwa.mede_id = b_mede_id OR b_mede_id IS NULL);
    v_dummy VARCHAR2(1);
  BEGIN
    OPEN spwa_cur(NULL);
    FETCH spwa_cur
      INTO v_dummy;
    IF (spwa_cur%FOUND)
    THEN
      CLOSE spwa_cur;
      OPEN spwa_cur(NVL(p_mede_id
                       ,kgc_mede_00.medewerker_id));
      FETCH spwa_cur
        INTO v_dummy;
      IF (spwa_cur%NOTFOUND)
      THEN
        CLOSE spwa_cur;
        v_return := FALSE;
      ELSE
        CLOSE spwa_cur;
        v_return := TRUE;
      END IF;
    ELSE
      CLOSE spwa_cur;
      v_return := TRUE;
    END IF;
    RETURN(v_return);
  END bevoegd;

  FUNCTION resultaat_tekst(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_tekst VARCHAR2(32767);
    CURSOR ores_cur IS
      SELECT resultaat
      FROM   kgc_onderzoek_resultaten_vw
      WHERE  onde_id = p_onde_id
      ORDER  BY last_update_date;
  BEGIN
    FOR r IN ores_cur
    LOOP
      IF (v_tekst IS NULL)
      THEN
        v_tekst := r.resultaat;
      ELSE
        IF (INSTR(v_tekst
                 ,r.resultaat) = 0)
        THEN
          v_tekst := v_tekst || CHR(10) || r.resultaat;
        END IF;
      END IF;
    END LOOP;
    RETURN(v_tekst);
  END resultaat_tekst;

  FUNCTION gerelateerde_onderzoeken(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_tekst VARCHAR2(32767);
    CURSOR reon_cur IS
    --    SELECT onty.omschrijving||' '||nvl( onde.onderzoeknr, externe_onderzoeknr ) onderzoek --
      SELECT NVL(onde.onderzoeknr
                ,reon.externe_onderzoeknr) onderzoek
      FROM   kgc_onderzoekrelatie_types   onty
            ,kgc_onderzoeken              onde
            ,kgc_gerelateerde_onderzoeken reon
      WHERE  reon.onty_id = onty.onty_id(+)
      AND    reon.onde_id_gerelateerde = onde.onde_id(+)
      AND    reon.onde_id = p_onde_id
      UNION
      --    SELECT nvl( onde.onderzoeknr, externe_onderzoeknr )||' ('||onty.omschrijving||')' --
      SELECT NVL(onde.onderzoeknr
                ,reon.externe_onderzoeknr)
      FROM   kgc_onderzoekrelatie_types   onty
            ,kgc_onderzoeken              onde
            ,kgc_gerelateerde_onderzoeken reon
      WHERE  reon.onty_id = onty.onty_id(+)
      AND    reon.onde_id = onde.onde_id(+)
      AND    reon.onde_id_gerelateerde = p_onde_id;
  BEGIN
    FOR r IN reon_cur
    LOOP
      IF (v_tekst IS NULL)
      THEN
        v_tekst := r.onderzoek;
      ELSE
        IF (INSTR(v_tekst
                 ,r.onderzoek) = 0)
        THEN
          v_tekst := v_tekst || CHR(10) || r.onderzoek;
        END IF;
      END IF;
    END LOOP;
    RETURN(v_tekst);
  END gerelateerde_onderzoeken;

  FUNCTION taal_id(p_onde_id IN NUMBER) RETURN NUMBER IS
    v_return NUMBER;
    CURSOR c IS
      SELECT taal_id FROM kgc_onderzoeken WHERE onde_id = p_onde_id;
  BEGIN
    IF (p_onde_id IS NOT NULL)
    THEN
      OPEN c;
      FETCH c
        INTO v_return;
      CLOSE c;
    END IF;
    RETURN(v_return);
  END taal_id;

  FUNCTION meetwaarden_info(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_return VARCHAR2(2000) := NULL;

    CURSOR stoftest_aangemeld_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen    meti
            ,bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = p_onde_id;
    CURSOR stoftest_waarde_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen    meti
            ,bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = p_onde_id
      AND    meet.meetwaarde IS NOT NULL;
    CURSOR stoftest_afgerond_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen    meti
            ,bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meti.onde_id = p_onde_id
      AND    (meet.afgerond = 'J' OR meti.afgerond = 'J');
    CURSOR protocol_aangemeld_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen meti
      WHERE  meti.onde_id = p_onde_id;
    CURSOR protocol_spoed_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen meti
      WHERE  meti.onde_id = p_onde_id
      AND    meti.prioriteit = 'J';
    CURSOR protocol_afgerond_cur IS
      SELECT COUNT(*)
      FROM   bas_metingen meti
      WHERE  meti.onde_id = p_onde_id
      AND    meti.afgerond = 'J';
    v_stoftest_aangemeld NUMBER;
    v_stoftest_waarde    NUMBER;
    v_stoftest_afgerond  NUMBER;
    v_protocol_aangemeld NUMBER;
    v_protocol_spoed     NUMBER;
    v_protocol_afgerond  NUMBER;
  BEGIN
    IF (p_onde_id IS NOT NULL)
    THEN
      OPEN stoftest_aangemeld_cur;
      FETCH stoftest_aangemeld_cur
        INTO v_stoftest_aangemeld;
      CLOSE stoftest_aangemeld_cur;

      OPEN stoftest_waarde_cur;
      FETCH stoftest_waarde_cur
        INTO v_stoftest_waarde;
      CLOSE stoftest_waarde_cur;

      OPEN stoftest_afgerond_cur;
      FETCH stoftest_afgerond_cur
        INTO v_stoftest_afgerond;
      CLOSE stoftest_afgerond_cur;

      OPEN protocol_aangemeld_cur;
      FETCH protocol_aangemeld_cur
        INTO v_protocol_aangemeld;
      CLOSE protocol_aangemeld_cur;

      OPEN protocol_spoed_cur;
      FETCH protocol_spoed_cur
        INTO v_protocol_spoed;
      CLOSE protocol_spoed_cur;

      OPEN protocol_afgerond_cur;
      FETCH protocol_afgerond_cur
        INTO v_protocol_afgerond;
      CLOSE protocol_afgerond_cur;

      IF (v_stoftest_aangemeld > 0)
      THEN
        v_return := 'Aangemelde stoftesten: ' ||
                    TO_CHAR(v_stoftest_aangemeld) || CHR(10) ||
                    '  Met meetwaarde: ' || TO_CHAR(v_stoftest_waarde) ||
                    CHR(10) || '  Afgerond: ' ||
                    TO_CHAR(v_stoftest_afgerond) || CHR(10) ||
                    'Protocollen : ' || TO_CHAR(v_protocol_aangemeld) ||
                    CHR(10) || '  Spoed : ' || TO_CHAR(v_protocol_spoed) ||
                    CHR(10) || '  Afgerond : ' ||
                    TO_CHAR(v_protocol_afgerond);
      ELSE
        v_return := 'Geen aangemelde stoftesten';
      END IF;
    END IF;
    RETURN(v_return);
  END meetwaarden_info;

  -- toon stand van zaken van declaraties voor een onderzoek
  FUNCTION declaraties_info(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_return VARCHAR2(2000);
    CURSOR decl_onde_cur IS
      SELECT COUNT(*)
      FROM   kgc_declaraties decl
      WHERE  decl.entiteit = 'ONDE'
      AND    decl.id = p_onde_id
      AND    decl.declareren = 'J';
    CURSOR decl_onbe_cur IS
      SELECT COUNT(*)
      FROM   kgc_declaraties           decl
            ,kgc_onderzoek_betrokkenen onbe
      WHERE  decl.entiteit = 'ONBE'
      AND    decl.id = onbe.onbe_id
      AND    onbe.onde_id = p_onde_id
      AND    decl.declareren = 'J';
    CURSOR decl_mons_cur IS
      SELECT COUNT(*)
      FROM   kgc_declaraties        decl
            ,kgc_onderzoek_monsters onmo
      WHERE  decl.entiteit = 'MONS'
      AND    decl.id = onmo.mons_id
      AND    onmo.onde_id = p_onde_id
      AND    decl.declareren = 'J';
    CURSOR decl_meti_cur IS
      SELECT COUNT(*)
      FROM   kgc_declaraties decl
            ,bas_metingen    meti
      WHERE  decl.entiteit = 'METI'
      AND    decl.id = meti.meti_id
      AND    meti.onde_id = p_onde_id
      AND    decl.declareren = 'J';
    CURSOR decl_gesp_cur IS
      SELECT COUNT(*)
      FROM   kgc_declaraties decl
            ,kgc_gesprekken  gesp
      WHERE  decl.entiteit = 'GESP'
      AND    decl.id = gesp.gesp_id
      AND    gesp.onde_id = p_onde_id
      AND    decl.declareren = 'J';
    v_aantal_onde NUMBER;
    v_aantal_onbe NUMBER;
    v_aantal_mons NUMBER;
    v_aantal_meti NUMBER;
    v_aantal_gesp NUMBER;
  BEGIN
    IF (p_onde_id IS NOT NULL)
    THEN
      OPEN decl_onde_cur;
      FETCH decl_onde_cur
        INTO v_aantal_onde;
      CLOSE decl_onde_cur;

      OPEN decl_onbe_cur;
      FETCH decl_onbe_cur
        INTO v_aantal_onbe;
      CLOSE decl_onbe_cur;

      OPEN decl_mons_cur;
      FETCH decl_mons_cur
        INTO v_aantal_mons;
      CLOSE decl_mons_cur;

      OPEN decl_meti_cur;
      FETCH decl_meti_cur
        INTO v_aantal_meti;
      CLOSE decl_meti_cur;

      OPEN decl_gesp_cur;
      FETCH decl_gesp_cur
        INTO v_aantal_gesp;
      CLOSE decl_gesp_cur;

      IF (v_aantal_onde > 0)
      THEN
        v_return := v_return || CHR(10) || 'Onderzoek: ' ||
                    TO_CHAR(v_aantal_onde);
      END IF;
      IF (v_aantal_onbe > 0)
      THEN
        v_return := v_return || CHR(10) || 'Betrokkene: ' ||
                    TO_CHAR(v_aantal_onbe);
      END IF;
      IF (v_aantal_mons > 0)
      THEN
        v_return := v_return || CHR(10) || 'Monsterafname: ' ||
                    TO_CHAR(v_aantal_mons);
      END IF;
      IF (v_aantal_meti > 0)
      THEN
        v_return := v_return || CHR(10) || 'Deelonderzoek (meting): ' ||
                    TO_CHAR(v_aantal_meti);
      END IF;
      IF (v_aantal_gesp > 0)
      THEN
        v_return := v_return || CHR(10) || 'Counseling: ' ||
                    TO_CHAR(v_aantal_gesp);
      END IF;
      IF (v_return IS NULL)
      THEN
        v_return := 'Geen declaraties';
      END IF;
    END IF;
    -- verwijder eerste <CR>...
    v_return := LTRIM(v_return
                     ,CHR(10));
    RETURN(v_return);
  END declaraties_info;

  FUNCTION kopieer_onderzoek(p_onde_id_orig  IN NUMBER
                            ,p_onde_id_nieuw IN NUMBER := NULL
                            ,p_ongr_id_nieuw IN NUMBER := NULL
                            ,p_mons_id_orig  IN NUMBER := NULL) RETURN NUMBER IS
    v_onde_id NUMBER; -- nieuw
    v_ongr_id NUMBER; -- nieuw
    CURSOR onde_cur(b_onde_id IN NUMBER) IS
      SELECT onde.onde_id
            ,onde.ongr_id
            ,onde.kafd_id
            ,onde.pers_id
            ,onde.onderzoekstype
            ,onde.onderzoeknr
            ,onde.rela_id
            ,onde.spoed
            ,onde.foet_id
            ,onde.datum_binnen
            ,onde.geplande_einddatum
            ,onde.herk_id
            ,onde.inst_id
            ,onde.onderzoekswijze
            ,onde.declareren
            ,onde.dewy_id
            ,onde.referentie
            ,onde.nota_adres
            ,onde.omschrijving
            ,onde.onre_id
            ,onde.rela_id_oorsprong
            ,herk.code herk_code
      FROM   kgc_herkomsten  herk
            ,kgc_onderzoeken onde
      WHERE  onde.herk_id = herk.herk_id(+)
      AND    onde.onde_id = b_onde_id;
    onde_rec onde_cur%ROWTYPE;
    CURSOR ongr_cur IS
      SELECT ongr.ongr_id
            ,ongr.kafd_id
            ,kafd.code    kafd_code
            ,ongr.code    ongr_code
      FROM   kgc_kgc_afdelingen    kafd
            ,kgc_onderzoeksgroepen ongr
      WHERE  ongr.kafd_id = kafd.kafd_id
      AND    ongr.ongr_id = p_ongr_id_nieuw;
    ongr_rec ongr_cur%ROWTYPE;
  BEGIN
    IF (p_onde_id_nieuw IS NULL AND p_ongr_id_nieuw IS NULL)
    THEN
      raise_application_error(-20009
                             ,'Fout in aanroep KGC_ONDE_00.KOPIEER_ONDERZOEK');
    END IF;
    IF (p_onde_id_nieuw IS NOT NULL AND p_ongr_id_nieuw IS NULL)
    THEN
      OPEN onde_cur(p_onde_id_nieuw);
      FETCH onde_cur
        INTO onde_rec;
      CLOSE onde_cur;
      v_ongr_id := onde_rec.ongr_id;
      v_onde_id := p_onde_id_nieuw;
    END IF;

    IF (p_onde_id_nieuw IS NULL)
    THEN
      OPEN onde_cur(p_onde_id_orig);
      FETCH onde_cur
        INTO onde_rec;
      CLOSE onde_cur;

      -- nieuwe onderzoeksgroep
      OPEN ongr_cur;
      FETCH ongr_cur
        INTO ongr_rec;
      CLOSE ongr_cur;
      onde_rec.onderzoeknr := kgc_nust_00.genereer_nr(p_nummertype => 'ONDE'
                                                     ,p_ongr_id    => p_ongr_id_nieuw);
      onde_rec.declareren  := kgc_decl_00.declarabel(p_criteria => kgc_util_00.tag('ONDE'
                                                                                  ,'entiteit') ||
                                                                   kgc_util_00.tag(ongr_rec.kafd_code
                                                                                  ,'kafd_code') ||
                                                                   kgc_util_00.tag(ongr_rec.ongr_code
                                                                                  ,'ongr_code') ||
                                                                   kgc_util_00.tag(onde_rec.onderzoekstype
                                                                                  ,'onderzoekstype') ||
                                                                   kgc_util_00.tag(onde_rec.onderzoekswijze
                                                                                  ,'onderzoekswijze') ||
                                                                   kgc_util_00.tag(onde_rec.omschrijving
                                                                                  ,'onde_omschrijving') ||
                                                                   kgc_util_00.tag(onde_rec.datum_binnen
                                                                                  ,'datum') ||
                                                                   kgc_util_00.tag(onde_rec.herk_code
                                                                                  ,'herk_code') ||
                                                                   kgc_util_00.tag(onde_rec.pers_id
                                                                                  ,'pers_id') ||
                                                                   kgc_util_00.tag(onde_rec.rela_id
                                                                                  ,'rela_id'));
      SELECT kgc_onde_seq.nextval INTO onde_rec.onde_id FROM dual;
      INSERT INTO kgc_onderzoeken
        (onde_id
        ,ongr_id
        ,pers_id
        ,onderzoekstype
        ,onderzoeknr
        ,rela_id
        ,spoed
        ,foet_id
        ,datum_binnen
        ,geplande_einddatum
        ,herk_id
        ,inst_id
        ,onderzoekswijze
        ,dewy_id
        ,referentie
        ,nota_adres
        ,omschrijving
        ,onre_id
        ,rela_id_oorsprong)
      VALUES
        (onde_rec.onde_id
        ,onde_rec.ongr_id
        ,onde_rec.pers_id
        ,onde_rec.onderzoekstype
        ,onde_rec.onderzoeknr
        ,onde_rec.rela_id
        ,onde_rec.spoed
        ,onde_rec.foet_id
        ,onde_rec.datum_binnen
        ,onde_rec.geplande_einddatum
        ,onde_rec.herk_id
        ,onde_rec.inst_id
        ,onde_rec.onderzoekswijze
        ,onde_rec.dewy_id
        ,onde_rec.referentie
        ,onde_rec.nota_adres
        ,onde_rec.omschrijving
        ,onde_rec.onre_id
        ,onde_rec.rela_id_oorsprong);
      v_onde_id := onde_rec.onde_id;
      v_ongr_id := p_ongr_id_nieuw;
    END IF;
    IF (v_onde_id IS NOT NULL)
    THEN
      INSERT INTO kgc_onderzoek_monsters
        (onde_id
        ,mons_id)
        SELECT v_onde_id
              ,onmo.mons_id
        FROM   kgc_onderzoek_monsters onmo
        WHERE  onmo.onde_id = p_onde_id_orig
        AND    onmo.mons_id = nvl(p_mons_id_orig
                                 ,onmo.mons_id)
        AND    NOT EXISTS (SELECT NULL
                FROM   kgc_onderzoek_monsters onmo2
                WHERE  onmo2.onde_id = v_onde_id
                AND    onmo2.mons_id = onmo.mons_id);
      INSERT INTO kgc_onderzoek_indicaties
        (onde_id
        ,volgorde
        ,indi_id
        ,opmerking)
        SELECT v_onde_id
              ,onin.volgorde
              ,indi_n.indi_id
              ,onin.opmerking
        FROM   kgc_indicatie_teksten    indi_n
              ,kgc_indicatie_teksten    indi_o
              ,kgc_onderzoek_indicaties onin
        WHERE  onin.onde_id = p_onde_id_orig
        AND    onin.indi_id = indi_o.indi_id
        AND    indi_o.code = indi_n.code
        AND    indi_n.ongr_id = v_ongr_id
        AND    NOT EXISTS
         (SELECT NULL
                FROM   kgc_onderzoek_indicaties onin2
                WHERE  onin2.onde_id = v_onde_id
                AND    onin2.indi_id = indi_n.indi_id);
      INSERT INTO kgc_onderzoek_indicaties
        (onde_id
        ,volgorde
        ,ingr_id
        ,opmerking)
        SELECT v_onde_id
              ,onin.volgorde
              ,ingr_n.ingr_id
              ,onin.opmerking
        FROM   kgc_indicatie_groepen    ingr_n
              ,kgc_indicatie_groepen    ingr_o
              ,kgc_onderzoek_indicaties onin
        WHERE  onin.onde_id = p_onde_id_orig
        AND    onin.ingr_id = ingr_o.ingr_id
        AND    ingr_o.code = ingr_n.code
        AND    ingr_n.ongr_id = v_ongr_id
        AND    NOT EXISTS
         (SELECT NULL
                FROM   kgc_onderzoek_indicaties onin2
                WHERE  onin2.onde_id = v_onde_id
                AND    onin2.ingr_id = ingr_n.ingr_id);
      INSERT INTO kgc_onderzoek_indicaties
        (onde_id
        ,volgorde
        ,opmerking)
        SELECT v_onde_id
              ,onin.volgorde
              ,onin.opmerking
        FROM   kgc_onderzoek_indicaties onin
        WHERE  onin.onde_id = p_onde_id_orig
        AND    onin.indi_id IS NULL
        AND    onin.ingr_id IS NULL
        AND    NOT EXISTS
         (SELECT NULL
                FROM   kgc_onderzoek_indicaties onin2
                WHERE  onin2.onde_id = v_onde_id
                AND    onin2.opmerking = onin.opmerking);
      INSERT INTO kgc_onderzoek_kopiehouders
        (onde_id
        ,rela_id)
        SELECT v_onde_id
              ,onkh.rela_id
        FROM   kgc_onderzoek_kopiehouders onkh
        WHERE  onkh.onde_id = p_onde_id_orig
        AND    NOT EXISTS (SELECT NULL
                FROM   kgc_onderzoek_kopiehouders onkh2
                WHERE  onkh2.onde_id = v_onde_id
                AND    onkh2.rela_id = onkh.rela_id);
    END IF;
    COMMIT;
    RETURN(v_onde_id);
  END kopieer_onderzoek;

  PROCEDURE kopieer_onderzoek(p_onde_id_orig  IN NUMBER
                             ,p_onde_id_nieuw IN NUMBER := NULL
                             ,p_ongr_id_nieuw IN NUMBER := NULL
                             ,p_mons_id_orig  IN NUMBER := NULL) IS
    v_onde_id NUMBER;
  BEGIN
    v_onde_id := kopieer_onderzoek(p_onde_id_orig  => p_onde_id_orig
                                  ,p_onde_id_nieuw => p_onde_id_nieuw
                                  ,p_ongr_id_nieuw => p_ongr_id_nieuw
                                  ,p_mons_id_orig  => p_mons_id_orig);
  END kopieer_onderzoek;

  FUNCTION monsters_info(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_return VARCHAR2(2000);

    CURSOR mons_cur IS
      SELECT mons.monsternummer
      FROM   kgc_onderzoek_monsters onmo
            ,kgc_monsters           mons
      WHERE  onmo.mons_id = mons.mons_id
      AND    onmo.onde_id = p_onde_id
      ORDER  BY mons.datum_aanmelding DESC;
  BEGIN
    FOR r IN mons_cur
    LOOP
      IF (v_return IS NULL)
      THEN
        v_return := r.monsternummer;
      ELSE
        v_return := v_return || ', ' || r.monsternummer;
      END IF;
    END LOOP;
    --
    RETURN(v_return);
  END monsters_info;

  FUNCTION fracties_info(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_return VARCHAR2(2000);

    CURSOR frac_cur IS
      SELECT frac.fractienummer
      FROM   kgc_onderzoek_monsters onmo
            ,kgc_monsters           mons
            ,bas_fracties           frac
      WHERE  onmo.onde_id = p_onde_id
      AND    mons.mons_id = onmo.mons_id
      AND    frac.mons_id = mons.mons_id
      ORDER  BY frac.creation_date DESC;
  BEGIN
    FOR r IN frac_cur
    LOOP
      IF (v_return IS NULL)
      THEN
        v_return := r.fractienummer;
      ELSE
        v_return := v_return || ', ' || r.fractienummer;
      END IF;
    END LOOP;
    RETURN(v_return);
  END fracties_info;

  FUNCTION fracties_info_alfabetisch(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_return VARCHAR2(2000);

    CURSOR frac_cur IS
      SELECT DISTINCT frac.fractienummer
      FROM   kgc_monsters    mons
            ,bas_fracties    frac
            ,bas_metingen    meti
            ,kgc_onderzoeken onde
      WHERE  meti.onde_id = onde.onde_id
      AND    meti.frac_id = frac.frac_id
      AND    frac.mons_id = mons.mons_id
      AND    mons.pers_id = onde.pers_id -- alleen van onderzoekspersoon, niet van overige betrokkenen!
      AND    ((mons.foet_id IS NULL AND onde.foet_id IS NULL) -- rde 15-02-2012 ook koppelen op foetus
            --AND condition Added for Mantis 7565 by Abhijit as given by customer.
            OR (mons.foet_id = onde.foet_id))
      AND    onde.onde_id = p_onde_id
      ORDER  BY frac.fractienummer;

  BEGIN
    FOR r IN frac_cur
    LOOP
      IF (v_return IS NULL)
      THEN
        v_return := r.fractienummer;
      ELSE
        v_return := v_return || CHR(10) || r.fractienummer;
      END IF;
    END LOOP;
    RETURN(v_return);
  END fracties_info_alfabetisch;

  FUNCTION materiaal_info(p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    l_v_materiaal VARCHAR2(2000);

    CURSOR mate_cur IS
      SELECT mate.omschrijving
      FROM   kgc_onderzoeken        onde
            ,kgc_onderzoek_monsters onmo
            ,kgc_monsters           mons
            ,kgc_materialen         mate
      WHERE  onde.onde_id = onmo.onde_id
      AND    onmo.mons_id = mons.mons_id
      AND    mons.mate_id = mate.mate_id
      AND    onde.onde_id = p_onde_id;
  BEGIN
    OPEN mate_cur;
    FETCH mate_cur
      INTO l_v_materiaal;
    CLOSE mate_cur;

    IF l_v_materiaal IS NOT NULL
    THEN
      RETURN(', ' || l_v_materiaal);
    ELSE
      RETURN NULL;
    END IF;
  END materiaal_info;

  FUNCTION monster_mate_id(p_onde_id IN NUMBER) RETURN NUMBER IS
    v_mate_id KGC_MATERIALEN.MATE_ID%TYPE;
    CURSOR mate_cur IS
      SELECT mate.mate_id
      FROM   kgc_onderzoeken        onde
            ,kgc_onderzoek_monsters onmo
            ,kgc_monsters           mons
            ,kgc_materialen         mate
      WHERE  onde.onde_id = onmo.onde_id
      AND    onmo.mons_id = mons.mons_id
      AND    mons.mate_id = mate.mate_id
      AND    onde.onde_id = p_onde_id;
  BEGIN
    OPEN mate_cur;
    FETCH mate_cur
      INTO v_mate_id;
    CLOSE mate_cur;
    --
    IF v_mate_id IS NOT NULL
    THEN
      RETURN(v_mate_id);
    ELSE
      RETURN NULL;
    END IF;
  END monster_mate_id;

  FUNCTION counselor(P_ONDE_ID IN NUMBER) RETURN VARCHAR2 IS
    /*****************************************************************************
       NAME:       counselor
       PURPOSE:    geef de counselor van het onderzoek.

       REVISIONS:
       Ver        Date        Author       Description
       ---------  ----------  ------------ --------------------------------------
       1.0        20/04/2006  ESO          Melding: HLX-KGEN-11126 .
    *****************************************************************************/

    v_return VARCHAR2(200);

    CURSOR csr_onde(b_onde_id NUMBER) IS
      SELECT mede.code
      FROM   kgc_onderzoeken onde
            ,kgc_medewerkers mede
      WHERE  onde.mede_id_beoordelaar = mede.mede_id
      AND    onde.onde_id = b_onde_id;

  BEGIN

    OPEN csr_onde(p_onde_id);
    FETCH csr_onde
      INTO v_return;
    IF csr_onde%FOUND
    THEN
      v_return := ' (' || v_return || ')';
    END IF;
    CLOSE csr_onde;

    RETURN(v_return);

  END counselor;

  FUNCTION sorteerkenmerk(P_ONDE_ID IN NUMBER) RETURN VARCHAR2 IS
    /*****************************************************************************
       NAME:       Sorteerkenmerk
       PURPOSE:    Levert tbv. KGCONDE25 het sorteerkenmerk van een onderzoek.

       REVISIONS:
       Ver        Date        Author       Description
       ---------  ----------  ------------ --------------------------------------
       1.0        04/09/2007  JKO          Mantis 287
    *****************************************************************************/
    v_sorteerkenmerk VARCHAR2(40) := NULL;

    CURSOR onde_cur(b_onde_id NUMBER) IS
      SELECT onde.spoed
            ,onde.geplande_einddatum
            ,onde.onderzoeknr
      FROM   kgc_onderzoeken onde
      WHERE  onde.onde_id = b_onde_id;
    v_onde_spoed              kgc_onderzoeken.spoed%TYPE := NULL;
    v_onde_geplande_einddatum kgc_onderzoeken.geplande_einddatum%TYPE := NULL;
    v_onde_onderzoeknr        kgc_onderzoeken.onderzoeknr%TYPE := NULL;

    CURSOR meti_cur(b_onde_id NUMBER) IS
    -- Zoek de rij met de laagste geplande_einddatum (meest spoed-eisend)
    -- (rijen met lege einddatum liggen helemaal achteraan in de sortering)
      SELECT meti.prioriteit
            ,meti.geplande_einddatum
      FROM   bas_metingen meti
      WHERE  meti.onde_id = b_onde_id
      AND    meti.prioriteit = 'J'
      ORDER  BY meti.geplande_einddatum;
    v_meti_prioriteit         bas_metingen.prioriteit%TYPE := NULL;
    v_meti_geplande_einddatum bas_metingen.geplande_einddatum%TYPE := NULL;

  BEGIN
    OPEN onde_cur(p_onde_id);
    FETCH onde_cur
      INTO v_onde_spoed
          ,v_onde_geplande_einddatum
          ,v_onde_onderzoeknr;
    CLOSE onde_cur;

    IF v_onde_spoed = 'J'
    THEN
      -- Onderzoek is spoed: rij hoort bij 1e sorteergroep met code O (Onderzoek)
      v_sorteerkenmerk := 'O' || NVL(TO_CHAR(v_onde_geplande_einddatum
                                            ,'yyyymmdd')
                                    ,'99991231') ||
                          TO_CHAR(v_onde_onderzoeknr);
    ELSE
      OPEN meti_cur(p_onde_id);
      FETCH meti_cur
        INTO v_meti_prioriteit
            ,v_meti_geplande_einddatum;
      IF meti_cur%FOUND
      THEN
        -- Er is een meting met spoed: rij hoort bij 2e sorteergroep met code P (Protocol)
        v_sorteerkenmerk := 'P' || NVL(TO_CHAR(v_meti_geplande_einddatum
                                              ,'yyyymmdd')
                                      ,'99991231') ||
                            TO_CHAR(v_onde_onderzoeknr);
      ELSE
        -- Er is nergens sprake van spoed: rij hoort bij 3e sorteergroep met code Z (Zonder spoed)
        v_sorteerkenmerk := 'Z' || NVL(TO_CHAR(v_onde_geplande_einddatum
                                              ,'yyyymmdd')
                                      ,'99991231') ||
                            TO_CHAR(v_onde_onderzoeknr);
      END IF;
      CLOSE meti_cur;
    END IF;
    RETURN(v_sorteerkenmerk);
  END sorteerkenmerk;
---------------------

PROCEDURE search_conc_tote_teksten (p_onde_list in OUT varchar2, p_entity varchar2 :=  'CONC', p_flag out varchar2, p_indi_list out varchar2 )
IS
    /*****************************************************************************
       NAME:
       PURPOSE:   called from KGCONDE22 module

       REVISIONS:
       Ver        Date        Author       Description
       ---------  ----------  ------------ --------------------------------------
       1.0        17/09/2013  SOJ          Mantis 4499
       1.1        06/11/2013  ASI          Rework Mantis 4499
    *****************************************************************************/

  c1 sys_refcursor;
  cursor c2 (p_indi_id kgc_teksten_context.indi_id%type)
  is
  select '1'
  from kgc_teksten_context teco
  WHERE teco.indi_id = p_indi_id
  and teco.tote_id is not null;

  cursor c3 (p_indi_id kgc_teksten_context.indi_id%type)
  is
  select '1'
  from kgc_teksten_context teco
  WHERE teco.indi_id = p_indi_id
  and teco.conc_id is not null;

    l_dummy varchar2(10);
    l_flag varchar2(1) := 'N';
    l_indi_id  kgc_teksten_context.indi_id%type;
    l_indi_list varchar2(32767);
    l_cur_text varchar2(32767);
BEGIN

   p_onde_list := REPLACE(P_onde_list
                                ,',,'
                                ,',');

   p_onde_list := TRIM(both ',' FROM p_onde_list);
/*   l_cur_text :=  ' SELECT distinct indi.indi_id '
                 ||' FROM kgc_indicatie_groepen ingr , kgc_onderzoeken onde, kgc_indicatie_teksten indi '
                 ||' WHERE ingr.ongr_id = onde.ongr_id '
                 ||' and  to_char(onde.onde_id) in (' || p_onde_list ||')'
                 ||' AND   ingr.ingr_id = indi.ingr_id '
                 ||' and  indi.vervallen = ' || '''' || 'N' || ''''
                 ||' and ingr.vervallen = ' || '''' || 'N' || '''';*/

   l_cur_text :=  ' SELECT distinct indi.indi_id '
                 ||' FROM kgc_onderzoek_indicaties onin , kgc_onderzoeken onde, kgc_indicatie_teksten indi '
                 ||' WHERE onin.onde_id = onde.onde_id  '
                 ||' and  to_char(onde.onde_id) in (' || p_onde_list ||')'
                 ||' AND    onin.indi_id = indi.indi_id '
                 ||' and  indi.vervallen = ' || '''' || 'N' || '''';

  open c1 for l_cur_text;
  loop
    fetch c1 into l_indi_id;
    if c1%found then
      if p_entity='TOTE' then
            open c2 (l_indi_id);
            fetch c2 into l_dummy;
            if c2%notfound then
              l_flag := 'J' ;
              exit ;
            else
              l_indi_list := l_indi_list || ',' || l_indi_id;
            end if;
            close c2;
        else
            open c3 (l_indi_id);
            fetch c3 into l_dummy;
            if c3%notfound then
              l_flag := 'J' ;
              exit ;
            else
              l_indi_list := l_indi_list || ',' || l_indi_id;
            end if;
            close c3;
        end if;
    else
          if c1%rowcount = 0 then
            l_flag := 'J' ;
          end if;
          exit;
    end if;
  end loop;
  close c1  ;
  p_indi_list := TRIM(both ',' FROM l_indi_list);
  p_flag := l_flag;

end search_conc_tote_teksten;
PROCEDURE search_rete_teksten (p_onde_list in OUT VARCHAR2, p_flag out VARCHAR2 ,p_stgr_list out varchar2)
IS
    /*****************************************************************************
       NAME:
       PURPOSE:   called from KGCONDE22 module

       REVISIONS:
       Ver        Date        Author       Description
       ---------  ----------  ------------ --------------------------------------
       1.0        31/10/2013  ASI          Mantis 4499
    *****************************************************************************/

  c1 sys_refcursor;
  cursor c2 (p_stgr_id kgc_teksten_context.stgr_id%type)
  is
  select '1'
  from kgc_teksten_context teco
  WHERE teco.stgr_id = p_stgr_id
  and teco.rete_id  is not null;

    l_dummy varchar2(10);
    l_flag varchar2(1) := 'N';
    l_stgr_id  kgc_teksten_context.indi_id%type;
    l_stgr_list varchar2(32767);
    l_cur_text varchar2(32767);
BEGIN

   p_onde_list := REPLACE(P_onde_list
                                ,',,'
                                ,',');

   p_onde_list := TRIM(both ',' FROM p_onde_list);
   l_cur_text :='  SELECT  DISTINCT stgr_id  FROM kgc_onderzoeken onde ,bas_metingen meti  '
                 ||'  WHERE meti.onde_id=onde.onde_id  '
                 ||' and  to_char(meti.onde_id) in (' || p_onde_list ||')'
     ;

  open c1 for l_cur_text;
  loop
    fetch c1 into l_stgr_id;
    if c1%found then

            open c2 (l_stgr_id);
            fetch c2 into l_dummy;
            if c2%notfound then
              l_flag := 'J' ;
              exit ;
            else
              l_stgr_list := l_stgr_list || ',' || l_stgr_id;
            end if;
            close c2;

    else
          if c1%rowcount = 0 then
            l_flag := 'J' ;
          end if;
          exit;
    end if;
  end loop;
  close c1  ;
  p_stgr_list := TRIM(both ',' FROM l_stgr_list);
  p_flag := l_flag;

end search_rete_teksten;
END kgc_onde_00;
/

/
QUIT
