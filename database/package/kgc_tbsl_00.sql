CREATE OR REPLACE PACKAGE "HELIX"."KGC_TBSL_00" IS
  PROCEDURE vullen(
      p_view_name     IN  VARCHAR2
    , p_enti_code     IN  VARCHAR2
    , p_gebruiker     IN  VARCHAR2
    , p_sessie        IN  VARCHAR2
    , p_where_clause  IN  VARCHAR2
    , p_afmeld_proc   IN  VARCHAR2
    , p_btyp_id       IN  NUMBER
                  );
END KGC_TBSL_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_TBSL_00" IS
  FUNCTION col_in_view(
      p_view_name   IN VARCHAR2
    , p_column_name IN VARCHAR2
                      ) RETURN BOOLEAN IS
    CURSOR vcol_cur(
        b_view_name   IN  VARCHAR2
      , b_column_name IN  VARCHAR2
                   ) IS
      SELECT '1'
      FROM   kgc_view_columns  vcol
      WHERE  vcol.view_name   = b_view_name
      AND    vcol.column_name = b_column_name
      ;
    vcol_rec  vcol_cur%ROWTYPE;
    v_retval  BOOLEAN;
  BEGIN
    OPEN  vcol_cur( b_view_name   => p_view_name
                  , b_column_name => p_column_name
                  );
    FETCH vcol_cur
    INTO  vcol_rec;
    IF vcol_cur%NOTFOUND
    THEN
      v_retval := FALSE;
    ELSE
      v_retval := TRUE;
    END IF;
    IF vcol_cur%ISOPEN
    THEN
      CLOSE vcol_cur;
    END IF;
    RETURN v_retval;
  END col_in_view;

  PROCEDURE vullen(
      p_view_name     IN  VARCHAR2
    , p_enti_code     IN  VARCHAR2
    , p_gebruiker     IN  VARCHAR2
    , p_sessie        IN  VARCHAR2
    , p_where_clause  IN  VARCHAR2
    , p_afmeld_proc   IN  VARCHAR2
    , p_btyp_id       IN  NUMBER
                  ) IS
  /******************************************************************************
      NAME:       vullen
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        15-08-2006  YAR              aanpassingen nav Word koppeling
   *****************************************************************************/
    v_dyn_sql            VARCHAR2(2000);
    v_kafd_code_in_view  BOOLEAN;
    v_ongr_code_in_view  BOOLEAN;
    enti_prow  cg$kgc_entiteiten.cg$row_type;
    btyp_prow  cg$kgc_bestand_types.cg$row_type;
  BEGIN
    dbms_output.put_line ('vullen begin');

    DELETE
    FROM   kgc_tmp_bestand_selectie  tbsl
    WHERE  tbsl.gebruiker = p_gebruiker
    ;

    v_kafd_code_in_view := col_in_view( p_view_name   => upper(p_view_name)
                                      , p_column_name => 'KAFD_CODE'
                                      );
    v_ongr_code_in_view := col_in_view( p_view_name   => upper(p_view_name)
                                      , p_column_name => 'ONGR_CODE'
                                      );
    enti_prow.code := p_enti_code;
    cg$kgc_entiteiten.slct( enti_prow );

    btyp_prow.btyp_id := p_btyp_id;
    cg$kgc_bestand_types.slct( btyp_prow );

    v_dyn_sql := 'INSERT INTO KGC_TMP_BESTAND_SELECTIE ('
      ||CHR(10)||'BTYP_ID'
      ||CHR(10)||', GEBRUIKER'
      ||CHR(10)||', SESSIE'
      ||CHR(10)||', STATUS'
      ||CHR(10)||', ID'
      ||CHR(10)||', OMSCHRIJVING'
      ||CHR(10)||', BESTAND'
      ||CHR(10)||', AFMELD_PROCEDURE'
                 ;
    IF v_kafd_code_in_view
    THEN
      v_dyn_sql := v_dyn_sql||CHR(10)||', KAFD_CODE';
    END IF;
    IF v_ongr_code_in_view
    THEN
      v_dyn_sql := v_dyn_sql||CHR(10)||', ONGR_CODE';
    END IF;

    v_dyn_sql := v_dyn_sql
      ||CHR(10)||') '
      ||CHR(10)||'SELECT '||p_btyp_id
      ||CHR(10)||', '''   ||p_gebruiker                    ||''''
      ||CHR(10)||', '     ||p_sessie
      ||CHR(10)||', DECODE((select max(best.best_id) '
               ||        'from   kgc_bestanden best '
               ||        'where  best.entiteit_code = ''' || enti_prow.code || ''' '
               ||        'and    best.entiteit_pk = ' || enti_prow.pk_kolom || ')'
               ||  ', NULL, ''S'''
               ||  ', ''B'')'
      ||CHR(10)||', '     ||enti_prow.pk_kolom
      ||CHR(10)||', '     ||NVL(enti_prow.omschrijving_kolommen,'NULL')
                 ;
    v_dyn_sql := v_dyn_sql
      ||CHR(10)||',  nvl((select bestand_specificatie '
               ||'from   kgc_bestanden best '
               ||'where  best.entiteit_code = ''' || enti_prow.code || ''' '
               ||'and    best.entiteit_pk = ' || enti_prow.pk_kolom || ' '
               ||'and    best.best_id = (select max(best2.best_id) from kgc_bestanden best2 '
               ||      ' where  best2.entiteit_code = best2.entiteit_code '
               ||      ' and    best.entiteit_pk = best2.entiteit_pk))'
               ||', NULL)'
               ;

   IF  p_afmeld_proc IS NULL
    THEN
      v_dyn_sql := v_dyn_sql||CHR(10)||', NULL';
    ELSE
      v_dyn_sql := v_dyn_sql||CHR(10)||', '''
                                     ||REPLACE(UPPER(p_afmeld_proc),CHR(10),NULL)
                                     ||'''';
    END IF;

    IF v_kafd_code_in_view
    THEN
      v_dyn_sql := v_dyn_sql||CHR(10)||', KAFD_CODE';
    END IF;
    IF v_ongr_code_in_view
    THEN
      v_dyn_sql := v_dyn_sql||CHR(10)||', ONGR_CODE';
    END IF;
    v_dyn_sql := v_dyn_sql||CHR(10)||'FROM '  ||p_view_name;
    IF  p_where_clause IS NOT NULL
    THEN
      v_dyn_sql := v_dyn_sql||CHR(10)||' '||p_where_clause;
    END IF;
    v_dyn_sql := v_dyn_sql||';';

    kgc_util_00.dyn_exec( v_dyn_sql );
    COMMIT;

  END vullen;
END KGC_TBSL_00;
/

/
QUIT
