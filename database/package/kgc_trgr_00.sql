CREATE OR REPLACE PACKAGE "HELIX"."KGC_TRGR_00" IS
-- maak velden, elementen op basis van traytype
PROCEDURE standaard_elementen
( p_trgr_id IN NUMBER
, p_richting IN VARCHAR2 := NULL -- HOR, VER
);
-- vul een tray aan de hand van een grid
PROCEDURE vul_tray
( p_trge_id IN NUMBER
, p_trgr_id IN NUMBER
, p_meet_id IN NUMBER
, x_fout OUT BOOLEAN
, x_melding OUT VARCHAR2
);
END KGC_TRGR_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_TRGR_00" IS
PROCEDURE standaard_elementen
( p_trgr_id IN NUMBER
, p_richting IN VARCHAR2
)
IS
  CURSOR trty_cur
  IS
    SELECT trty.indicatie_hor
    ,      trty.indicatie_ver
    ,      trty.horizontaal
    ,      trty.verticaal
    FROM   kgc_tray_types trty
    ,      kgc_tray_grids trgr
    WHERE  trgr.trty_id = trty.trty_id
    AND    trgr.trgr_id = p_trgr_id
    ;
  trty_rec trty_cur%rowtype;

  v_y VARCHAR2(3);
  v_x VARCHAR2(3);
  num NUMBER;
  y_num BOOLEAN := TRUE;
  x_num BOOLEAN := TRUE;
  v_volgorde NUMBER := 0;
BEGIN
  IF ( p_trgr_id IS NULL )
  THEN
    RETURN;
  ELSE
    OPEN  trty_cur;
    FETCH trty_cur
    INTO  trty_rec;
    CLOSE trty_cur;
  END IF;
  BEGIN
    num := trty_rec.indicatie_ver;
  EXCEPTION
    WHEN OTHERS
    THEN
      y_num := FALSE;
  END;
  BEGIN
    num := trty_rec.indicatie_hor;
  EXCEPTION
    WHEN OTHERS
    THEN
      x_num := FALSE;
  END;
  IF ( p_richting = 'VER' )
  THEN
    v_x := trty_rec.indicatie_hor;
    FOR i IN 1..trty_rec.horizontaal
    LOOP
      v_y := trty_rec.indicatie_ver;
      FOR j IN 1..trty_rec.verticaal
      LOOP
        v_volgorde := v_volgorde + 1;
        BEGIN
          INSERT INTO kgc_tray_grid_vulling
          ( trgr_id
          , x_positie
          , y_positie
          , volgorde
          , inhoud
          )
          VALUES
          ( p_trgr_id
          , v_x
          , v_y
          , v_volgorde
          , 'FS'
          );
        EXCEPTION
          WHEN dup_val_on_index
          THEN
            NULL;
        END;
        IF ( y_num )
        THEN
          v_y := TO_NUMBER( v_y ) + 1;
        ELSE
          v_y := CHR( ASCII( v_y ) + 1 );
        END IF;
      END LOOP;
      IF ( x_num )
      THEN
        v_x := TO_NUMBER( v_x ) + 1;
      ELSE
        v_x := CHR( ASCII( v_x ) + 1 );
      END IF;
    END LOOP;
  ELSE -- horizontaal
    v_y := trty_rec.indicatie_ver;
    FOR i IN 1..trty_rec.verticaal
    LOOP
      v_x := trty_rec.indicatie_hor;
      FOR j IN 1..trty_rec.horizontaal
      LOOP
        v_volgorde := v_volgorde + 1;
        BEGIN
          INSERT INTO kgc_tray_grid_vulling
          ( trgr_id
          , y_positie
          , x_positie
          , volgorde
          , inhoud
          )
          VALUES
          ( p_trgr_id
          , v_y
          , v_x
          , v_volgorde
          , 'FS'
          );
        EXCEPTION
          WHEN dup_val_on_index
          THEN
            NULL;
        END;
        IF ( x_num )
        THEN
          v_x := TO_NUMBER( v_x ) + 1;
        ELSE
          v_x := CHR( ASCII( v_x ) + 1 );
        END IF;
      END LOOP;
      IF ( y_num )
      THEN
        v_y := TO_NUMBER( v_y ) + 1;
      ELSE
        v_y := CHR( ASCII( v_y ) + 1 );
      END IF;
    END LOOP;
  END IF;
  COMMIT;
END standaard_elementen;

PROCEDURE vul_tray
( p_trge_id IN NUMBER
, p_trgr_id IN NUMBER
, p_meet_id IN NUMBER
, x_fout OUT BOOLEAN
, x_melding OUT VARCHAR2
)
IS
  CURSOR meet_cur
  IS
    SELECT UPPER( stof.omschrijving ) stof_omschrijving
    ,      UPPER( NVL( frac.fractienummer, stan.standaardfractienummer )) fractienummer
    ,      DECODE( meti.stan_id
                 , NULL, 'F'
                 , 'S'
                 ) soort_fractie
    ,      meet.stof_id
    FROM   kgc_stoftesten stof
    ,      bas_standaardfracties stan
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.stof_id = stof.stof_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id (+)
    AND    meti.stan_id = stan.stan_id (+)
    AND    meet.meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;

/* ondersteunde coderingen in het INHOUD-veld:
   F=aangemelde stoftest voor een fractie
   S=aangemelde stoftest voor een standaardfractie
   [%X]=aangemelde stoftesten, waarvan de omschrijving eindigt op X (bijvoorbeeld)
   <Y%>=(standaard)fractienummer, waarvan de omschrijving begint met Y (bijvoorbeeld)
   Combinaties zijn mogelijk
*/
  CURSOR tgvu_cur
  ( b_soort_fractie IN VARCHAR2
  , b_stof_id IN NUMBER
  , b_fractienummer IN VARCHAR2
  )
  IS
    SELECT tgvu.y_positie
    ,      tgvu.x_positie
    ,      tgvu.inhoud
    FROM   kgc_stoftesten stof
    ,      kgc_tray_grids trgr
    ,      kgc_tray_grid_vulling tgvu
    WHERE  tgvu.trgr_id = trgr.trgr_id
    AND    trgr.trgr_id = p_trgr_id
    AND    stof.stof_id = b_stof_id
    AND    tgvu.inhoud IS NOT NULL
    AND    INSTR( kgc_util_00.strip( tgvu.inhoud, '[', ']', 'J' ), b_soort_fractie ) > 0
    AND    stof.omschrijving LIKE NVL( kgc_util_00.strip( tgvu.inhoud, '[', ']', 'N' ), '%' )
    AND    b_fractienummer LIKE NVL( kgc_util_00.strip( tgvu.inhoud, '<', '>', 'N' ), '%' )
    AND ( trgr.stoftest_rij = 'N'
       OR ( trgr.stoftest_rij = 'J'
        AND trgr.richting = 'HOR'
        AND NOT EXISTS
            ( SELECT NULL
              FROM   bas_meetwaarden meet
              ,      kgc_tray_vulling trvu
              WHERE  trvu.meet_id = meet.meet_id
              AND    trvu.trge_id = p_trge_id
              AND    trvu.y_positie = tgvu.y_positie
              AND    meet.stof_id <> b_stof_id
            )
          )
       OR ( trgr.stoftest_rij = 'J'
        AND trgr.richting = 'VER'
        AND NOT EXISTS
            ( SELECT NULL
              FROM   bas_meetwaarden meet
              ,      kgc_tray_vulling trvu
              WHERE  trvu.meet_id = meet.meet_id
              AND    trvu.trge_id = p_trge_id
              AND    trvu.x_positie = tgvu.x_positie
              AND    meet.stof_id <> b_stof_id
            )
          )
        )
    AND    NOT EXISTS
           ( SELECT NULL
             FROM   kgc_tray_vulling trvu
             WHERE  trvu.y_positie = tgvu.y_positie
             AND    trvu.x_positie = tgvu.x_positie
             AND    trvu.trge_id = p_trge_id
           )
    ORDER BY tgvu.volgorde
    ,        LPAD(tgvu.y_positie, 5, '!')
    ,        LPAD(tgvu.x_positie, 5, '!')
    ;
  tgvu_rec tgvu_cur%rowtype;
BEGIN
  x_fout := FALSE;
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  meet_rec;
  IF ( meet_cur%notfound )
  THEN
    x_melding := 'Onbekende meet_id (aangemelde stoftest)';
    CLOSE  meet_cur;
    RETURN;
  ELSE
    CLOSE  meet_cur;
  END IF;
  -- zoek eerstvolgende open plek
  OPEN  tgvu_cur( b_soort_fractie => meet_rec.soort_fractie
                , b_stof_id => meet_rec.stof_id
                , b_fractienummer => meet_rec.fractienummer
                );
  FETCH tgvu_cur
  INTO  tgvu_rec;
  CLOSE tgvu_cur;
  IF ( tgvu_rec.y_positie IS NOT NULL )
  THEN
    BEGIN
      INSERT INTO kgc_tray_vulling
      ( trge_id
      , y_positie
      , x_positie
      , z_positie
      , meet_id
      )
      VALUES
      ( p_trge_id
      , tgvu_rec.y_positie
      , tgvu_rec.x_positie
      , 0
      , p_meet_id
      );
    END;
    COMMIT;
  ELSE
    x_melding := 'Niet geplaatst.';
  END IF;
EXCEPTION
  WHEN OTHERS
  THEN
    x_fout := TRUE;
    x_melding := SQLERRM;
END vul_tray;
END  kgc_trgr_00;
/

/
QUIT
