CREATE OR REPLACE PACKAGE "HELIX"."KGC_ONMO_00" IS
-- true als monster en onderzoek bij elkaar horen
FUNCTION bestaat_onmo
( p_onde_id IN NUMBER
, p_mons_id IN NUMBER
)
RETURN BOOLEAN;

-- controleer of er een monster van een gegeven persoon bestaat bij een gegeven onderzoek (vv)
FUNCTION bestaat_onmo_pers
( p_pers_id IN NUMBER
, p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
)
RETURN BOOLEAN;

-- dwing af dat het is toegestaan monstergegevens te wijzigen
PROCEDURE br_onmo0001_ier
( p_onde_id IN NUMBER
)
;
END KGC_ONMO_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ONMO_00" IS
FUNCTION bestaat_onmo
( p_onde_id IN NUMBER
, p_mons_id IN NUMBER
)
RETURN BOOLEAN
IS
  v_return_value BOOLEAN := TRUE;
  CURSOR  onmo_cur
  IS
    SELECT NULL
    FROM   kgc_onderzoek_monsters
    WHERE  onde_id = p_onde_id
    AND    mons_id = p_mons_id
    ;
  v_dummy VARCHAR2(1);
BEGIN
  IF ( p_onde_id IS NOT NULL
   AND p_mons_id IS NOT NULL
     )
  THEN
    OPEN  onmo_cur;
    FETCH onmo_cur
    INTO  v_dummy;
    v_return_value := onmo_cur%found;
    CLOSE onmo_cur;
  END IF;
  RETURN( v_return_value );
END bestaat_onmo;

FUNCTION bestaat_onmo_pers
( p_pers_id IN NUMBER
, p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
)
RETURN BOOLEAN
IS
  v_return_value BOOLEAN := TRUE;
  CURSOR  mons_cur
  IS
    SELECT NULL
    FROM   kgc_onderzoek_monsters onmo
    ,      kgc_monsters mons
    WHERE  mons.mons_id = onmo.mons_id
    AND    mons.pers_id = p_pers_id
    AND    onmo.onde_id = p_onde_id
    ;
  CURSOR  onde_cur
  IS
    SELECT NULL
    FROM   kgc_onderzoek_monsters onmo
    ,      kgc_onderzoeken onde
    WHERE  onde.onde_id = onmo.onde_id
    AND    onde.pers_id = p_pers_id
    AND    onmo.mons_id = p_mons_id
    ;
  v_dummy VARCHAR2(1);
BEGIN
  IF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  v_dummy;
    v_return_value := mons_cur%found;
    CLOSE mons_cur;
  ELSIF ( p_mons_id IS NOT NULL )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_dummy;
    v_return_value := onde_cur%found;
    CLOSE onde_cur;
  END IF;
  RETURN( v_return_value );
END bestaat_onmo_pers;

PROCEDURE br_onmo0001_ier
( p_onde_id IN NUMBER
)
IS
BEGIN
  IF kgc_onde_00.goedgekeurd(p_onde_id => p_onde_id)
  THEN
    DECLARE
      CURSOR onde_cur(b_onde_id IN NUMBER) IS
        SELECT onde.kafd_id
              ,onde.ongr_id
          FROM kgc_onderzoeken onde
         WHERE onde.onde_id = b_onde_id;
      v_kafd_id kgc_onderzoeken.kafd_id%TYPE;
      v_ongr_id kgc_onderzoeken.ongr_id%TYPE;
    BEGIN
      OPEN onde_cur(p_onde_id);
      FETCH onde_cur
        INTO v_kafd_id, v_ongr_id;
      CLOSE onde_cur;
      IF INSTR(UPPER(kgc_sypa_00.standaard_waarde(p_parameter_code => 'BEVRIES_GOEDGEKEURD_ONDERZOEK'
                                                 ,p_kafd_id        => v_kafd_id
                                                 ,p_ongr_id        => v_ongr_id))
              ,'MONSTER') > 0
      THEN
        qms$errors.show_message(p_mesg  => 'KGC-11291'
                               ,p_errtp => 'E'
                               ,p_rftf  => TRUE);
      END IF;
    END;
  END IF;
END br_onmo0001_ier;
END kgc_onmo_00;
/

/
QUIT
