CREATE OR REPLACE PACKAGE HELIX.KGC_FILE IS

   FUNCTION FileExists ( p_file   IN     VARCHAR2
                      ) RETURN VARCHAR2;

   FUNCTION bestand_naar_blob  (p_file   IN     VARCHAR2
                             , p_blob   IN OUT BLOB
                             ) RETURN VARCHAR2;

   FUNCTION blob_naar_bestand ( p_file   IN   VARCHAR2
                              , p_blob   IN   BLOB
                              ) RETURN VARCHAR2;

   FUNCTION copy_bestand ( p_file_van   IN   VARCHAR2
                         , p_file_naar  IN   VARCHAR2
                         ) RETURN VARCHAR2;

   FUNCTION delete_bestand ( p_file   IN   VARCHAR2
                           ) RETURN VARCHAR2;

END KGC_FILE;
/

CREATE OR REPLACE PACKAGE BODY HELIX.KGC_FILE IS

   FUNCTION FileExists ( p_file   IN     VARCHAR2
                      ) RETURN VARCHAR2 IS
$IF (kgc_coco_00.v_is_locatie_nijmegen) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden2.FileExists(java.lang.String) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_utrecht) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden2.FileExists(java.lang.String) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_maastricht) 
$THEN
      -- nog onveranderd
      LANGUAGE JAVA
      NAME 'Bestanden2.FileExists(java.lang.String) return java.lang.String';
$ELSE
      LANGUAGE JAVA
      NAME 'Bestanden2.FileExists(java.lang.String) return java.lang.String';
$END

   FUNCTION bestand_naar_blob  (p_file   IN     VARCHAR2
                             , p_blob   IN OUT BLOB
                             ) RETURN VARCHAR2 IS
$IF (kgc_coco_00.v_is_locatie_nijmegen) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden2.File2Blob(java.lang.String, oracle.sql.BLOB[]) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_utrecht) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden2.File2Blob(java.lang.String, oracle.sql.BLOB[]) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_maastricht) 
$THEN
      -- nog onveranderd
      LANGUAGE JAVA
      NAME 'Bestanden2.File2Blob(java.lang.String, oracle.sql.BLOB[]) return java.lang.String';
$ELSE
      LANGUAGE JAVA
      NAME 'Bestanden2.File2Blob(java.lang.String, oracle.sql.BLOB[]) return java.lang.String';
$END

   -- wrapper voor blob_naar_bestand_java; deze geeft in forms een internal error bij compileren!
   FUNCTION blob_naar_bestand ( p_file   IN   VARCHAR2
                              , p_blob   IN   BLOB
                              ) RETURN VARCHAR2 IS
   BEGIN
$IF (kgc_coco_00.v_is_locatie_nijmegen) 
$THEN
      RETURN kgc_interface_generiek.blob_naar_bestand_java(p_file, p_blob);
$ELSIF (kgc_coco_00.v_is_locatie_utrecht) 
$THEN
      RETURN kgc_interface_generiek.blob_naar_bestand_java(p_file, p_blob);
$ELSIF (kgc_coco_00.v_is_locatie_maastricht) 
$THEN
      -- CHANGE MAASTRICHT CALL WEBSERICE INSTEAD OF JAVA CLASS, RKON 11-10-2016
      RETURN helix_import_api.processblobfile(p_file,p_blob); 
$ELSE
      RETURN kgc_interface_generiek.blob_naar_bestand_java(p_file, p_blob);
$END
   END blob_naar_bestand;

   FUNCTION copy_bestand ( p_file_van   IN   VARCHAR2
                         , p_file_naar  IN   VARCHAR2
                         ) RETURN VARCHAR2 IS
$IF (kgc_coco_00.v_is_locatie_nijmegen) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden.copy(java.lang.String, java.lang.String) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_utrecht) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden.copy(java.lang.String, java.lang.String) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_maastricht) 
$THEN
   -- 11-10-2016 RKON: Changed file routines from Java CLASS to webservice call
   BEGIN                      
      RETURN Helix_Import_Api.copyfiles(p_file_van,p_file_naar);
   END copy_bestand; 
$ELSE
      LANGUAGE JAVA
      NAME 'Bestanden.copy(java.lang.String, java.lang.String) return java.lang.String';
$END

   FUNCTION delete_bestand ( p_file   IN   VARCHAR2
                           ) RETURN VARCHAR2 IS
$IF (kgc_coco_00.v_is_locatie_nijmegen) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden.delete(java.lang.String) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_utrecht) 
$THEN
      LANGUAGE JAVA
      NAME 'Bestanden.delete(java.lang.String) return java.lang.String';
$ELSIF (kgc_coco_00.v_is_locatie_maastricht) 
$THEN
   BEGIN                        
      RETURN HELIX_IMPORT_API.deletefiles(p_file);
   END; 
$ELSE
      LANGUAGE JAVA
      NAME 'Bestanden.delete(java.lang.String) return java.lang.String';
$END


END KGC_FILE;
/

