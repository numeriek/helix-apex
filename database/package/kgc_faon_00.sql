CREATE OR REPLACE PACKAGE "HELIX"."KGC_FAON_00" IS
  FUNCTION uitslagen
  ( p_faon_id IN NUMBER
  )
  RETURN VARCHAR2;
END KGC_FAON_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FAON_00" IS
      FUNCTION uitslagen
    -- changed by saroj on 27/09/2011, mantis 5918
    -- restrcited the function  output to maximum 200 characters

      ( p_faon_id IN NUMBER
      )
      RETURN VARCHAR2
      IS
        CURSOR onde_cur IS
          SELECT onui.code
          FROM   kgc_onderzoek_uitslagcodes  onui
          ,      kgc_onderzoeken             onde
          ,      kgc_faon_betrokkenen        fobe
          WHERE  onde.onde_id = fobe.onde_id
          AND    onde.onui_id = onui.onui_id
          AND    fobe.faon_id = p_faon_id
          ;
        v_uitslag  VARCHAR2(32767) := NULL;
    BEGIN
        IF p_faon_id IS NOT NULL
        THEN
          FOR onde_rec IN onde_cur
          LOOP
            v_uitslag := v_uitslag||onde_rec.code||',';
            EXIT WHEN length(v_uitslag) >= 200;  -- added for mantis 5918
          END LOOP;
        END IF;
        v_uitslag := substr(v_uitslag,1,200);  -- added for mantis 5918
        RETURN RTRIM(v_uitslag,',');
      END uitslagen;
END kgc_faon_00;
/

/
QUIT
