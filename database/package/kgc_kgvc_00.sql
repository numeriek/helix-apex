CREATE OR REPLACE PACKAGE "HELIX"."KGC_KGVC_00" IS
  FUNCTION column_value(
       p_view_name    IN  VARCHAR2
     , p_column_name  IN  VARCHAR2
     , p_pk_seq_nr    IN  NUMBER
     , p_pk_col_name  IN  VARCHAR2 DEFAULT 'PK_SEQ_NR'
                       ) RETURN VARCHAR2;
END KGC_kgvc_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_KGVC_00" IS
  FUNCTION column_value(
       p_view_name    IN  VARCHAR2
     , p_column_name  IN  VARCHAR2
     , p_pk_seq_nr    IN  NUMBER
     , p_pk_col_name  IN  VARCHAR2 DEFAULT 'PK_SEQ_NR'
                       ) RETURN VARCHAR2 IS
    v_stmt  VARCHAR2(4000);
  BEGIN
    v_stmt := ' SELECT '||p_column_name||
              ' FROM   '||p_view_name  ||
              ' WHERE  '||p_pk_col_name||'='||p_pk_seq_nr
              ;
    RETURN kgc_util_00.dyn_exec( v_stmt );
  END column_value
  ;
END kgc_kgvc_00;
/

/
QUIT
