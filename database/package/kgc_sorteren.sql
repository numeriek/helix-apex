CREATE OR REPLACE PACKAGE "HELIX"."KGC_SORTEREN" IS
-- PL/SQL Specification
-- Package Specification
--
-- retourneer een waarde waarmee gesorteerd kan worden
-- waarde is  een varchar2!!!
-- dus datum als YYYYMMDD en nummers als 0000000999
-- Let op: sommige sorteersystematieken komen in meerdere functies voor!
--
FUNCTION sorteer_teksten
( p_id IN NUMBER
, p_type IN VARCHAR2 )
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( sorteer_teksten, WNDS, WNPS );
--
-- retourneer een waarde waarmee gesorteerd kan worden
-- waarde is  een varchar2!!!
-- dus datum als YYYYMMDD en nummers als 0000000999
-- Let op: sommige sorteersystematieken komen in meerdere functies voor!
FUNCTION sorteerwaarde
( p_meet_id IN NUMBER
, p_systematiek IN VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( sorteerwaarde, WNDS, WNPS );

FUNCTION sorteerwaarde_mons
( p_mons_id IN NUMBER
, p_systematiek IN VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( sorteerwaarde_mons, WNDS, WNPS );

FUNCTION sorteerwaarde_stof
( p_stof_id IN NUMBER
, p_systematiek IN VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( sorteerwaarde_stof, WNDS, WNPS );
--
FUNCTION stoftest_volgorde
( p_stgr_id number
, p_stof_id number
)
RETURN number;
PRAGMA RESTRICT_REFERENCES( stoftest_volgorde, WNDS, WNPS );
--

END KGC_SORTEREN;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_SORTEREN" IS
-- PL/SQL Block
----------------------------------------------------------
FUNCTION sorteer_teksten
( p_id IN NUMBER
, p_type IN VARCHAR2 )
RETURN VARCHAR2
----------------------------------------------------------
-- Auteur : ASSAI, ESO
-- Datum  : 23-04-2004
-- Omschr : Haal code op uit tekstvelden tbv. sortering
--          in div modules o.a. kgcstgr01, kgcindi01 etc.
----------------------------------------------------------
IS
 v_return VARCHAR2(100);
 CURSOR rete_cur( p_rete_id NUMBER )
 IS
   SELECT rete.code
   FROM   kgc_resultaat_teksten rete
   WHERE  rete.rete_id = p_rete_id;
 CURSOR tote_cur( p_tote_id NUMBER )
 IS
   SELECT tote.code
   FROM   kgc_toelichting_teksten tote
   WHERE  tote.tote_id =  p_tote_id;
 CURSOR conc_cur( p_conc_id NUMBER )
 IS
   SELECT conc.code
   FROM   kgc_conclusie_teksten conc
   WHERE  conc.conc_id = p_conc_id;
-- PL/SQL Block
BEGIN
 IF p_type = 'CONC'
 THEN
  OPEN conc_cur( p_id );
  FETCH conc_cur INTO v_return;
  CLOSE conc_cur;
 ELSIF p_type = 'RETE'
 THEN
  OPEN rete_cur( p_id );
  FETCH rete_cur INTO v_return;
  CLOSE rete_cur;
 ELSIF p_type = 'TOTE'
 THEN
  OPEN  tote_cur( p_id );
  FETCH tote_cur INTO v_return;
  CLOSE tote_cur;
 END IF;
 RETURN(v_return);
END;
--
FUNCTION sorteerwaarde
( p_meet_id IN NUMBER
, p_systematiek IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  -- fractienummer
  CURSOR frac_cur
  IS
    SELECT  NVL( frac.fractienummer, stan.standaardfractienummer ) fractienummer
    FROM    bas_standaardfracties stan
    ,       bas_fracties frac
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.frac_id = frac.frac_id (+)
    AND     meti.stan_id = stan.stan_id (+)
    AND     meet.meet_id = p_meet_id
    ;
  -- monstermateriaalcode
  CURSOR mate_cur
  IS
    SELECT  mate.code
    FROM    kgc_materialen mate
    ,       kgc_monsters mons
    ,       bas_fracties frac
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.frac_id = frac.frac_id
    AND     frac.mons_id = mons.mons_id
    AND     mons.mate_id = mate.mate_id
    AND     meet.meet_id = p_meet_id
    ;
  -- monsternummer (en vervolgens op onderzoek)
  CURSOR mons_cur
  IS
    SELECT  RPAD(mons.monsternummer,20,' ')
         || RPAD(onde.onderzoeknr,20,' ')
    FROM    kgc_monsters mons
    ,       bas_fracties frac
    ,       kgc_onderzoeken onde
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.onde_id = onde.onde_id (+)
    AND     meti.frac_id = frac.frac_id
    AND     frac.mons_id = mons.mons_id
    AND     meet.meet_id = p_meet_id
    ;
  -- onderzoek (en vervolgens op monster)
  CURSOR onde_cur
  IS
    SELECT  RPAD(onde.onderzoeknr,20,' ')
         || RPAD(mons.monsternummer,20,' ')
    FROM    kgc_onderzoeken onde
    ,       kgc_monsters mons
    ,       bas_fracties frac
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.onde_id = onde.onde_id
    AND     meti.frac_id = frac.frac_id
    AND     frac.mons_id = mons.mons_id
    AND     meet.meet_id = p_meet_id
    ;
  -- persoon: achternaam, geboortedatum (en vervolgens op onderzoek en monster)
  CURSOR pers_cur
  IS
    SELECT  RPAD(pers.achternaam,30,' ')
         || TO_CHAR(pers.geboortedatum,'YYYYMMDD')
         || RPAD(onde.onderzoeknr,20,' ')
         || RPAD(mons.monsternummer,20,' ') persoon
    FROM    kgc_personen pers
    ,       kgc_onderzoeken onde
    ,       kgc_monsters mons
    ,       bas_fracties frac
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.onde_id = onde.onde_id
    AND     meti.frac_id = frac.frac_id
    AND     frac.mons_id = mons.mons_id
    AND     mons.pers_id = pers.pers_id
    AND     meet.meet_id = p_meet_id
    ;
  -- prioriteit meting / spoed onderzoek
  CURSOR prio_cur
  IS
    SELECT  DECODE( onde.spoed
                   , 'J', 'A'
                   , DECODE( meti.prioriteit
                           , 'J', 'A'
                           , 'Z'
                           )
                   ) prioriteit
    FROM    kgc_onderzoeken onde
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.onde_id = onde.onde_id
    AND     meet.meet_id = p_meet_id
    ;
  -- protocolcode
  CURSOR stgr_cur
  IS
    SELECT  stgr.code
    FROM    kgc_stoftestgroepen stgr
    ,       bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meti.stgr_id = stgr.stgr_id (+)
    AND     meet.meet_id = p_meet_id
    ;
  -- stoftestcode
  CURSOR stof_cur
  IS
    SELECT  stof.code
    FROM    kgc_stoftesten stof
    ,       bas_meetwaarden meet
    WHERE   meet.stof_id = stof.stof_id
    AND     meet.meet_id = p_meet_id
    ;
  -- datum aanmelding meting
  CURSOR meti_cur
  IS
    SELECT  TO_CHAR(meti.datum_aanmelding,'yyyymmdd') datum
    FROM    bas_metingen meti
    ,       bas_meetwaarden meet
    WHERE   meet.meti_id = meti.meti_id
    AND     meet.meet_id = p_meet_id
    ;
  -- traypositie (vlnr)
  CURSOR tray_lr_cur
  IS
    SELECT  LPAD( trvu.y_positie, 5, '!' )
         || LPAD( trvu.x_positie, 5, '!' )
         || LPAD( trvu.z_positie, 5, '!' ) positie
    FROM    kgc_tray_vulling trvu
    WHERE   trvu.meet_id = p_meet_id
    ;
  -- traypositie (vbnb)
  CURSOR tray_bb_cur
  IS
    SELECT  LPAD( trvu.x_positie, 5, '!' )
         || LPAD( trvu.y_positie, 5, '!' )
         || LPAD( trvu.z_positie, 5, '!' ) positie
    FROM    kgc_tray_vulling trvu
    WHERE   trvu.meet_id = p_meet_id
    ;
  -- samenstellingen eerst, dan voortest, verder alfabetisch stoftestcode
  CURSOR stof_type_cur
  IS
    SELECT  DECODE( stof.meet_reken
                  , 'S', 'a'
                  , 'V', 'b'
                  , 'M', 'c'
                  , 'R', 'y' -- normaliter niet op werklijst
                  , 'z'      -- komt niet voor
                  )
            || stof.code
    FROM    kgc_stoftesten stof
    ,       bas_meetwaarden meet
    WHERE   meet.stof_id = stof.stof_id
    AND     meet.meet_id = p_meet_id
    ;
  -- per samenstelling de stoftesten
  CURSOR samenstelling_cur
  IS
    -- samenstellingen (super)
    SELECT  'a'
            || RPAD(stof1.code,10,'!')
            || RPAD('{',10,'{')
    FROM    kgc_stoftesten stof1
    ,       kgc_sub_stoftesten subs
    ,       bas_meetwaarden meet
    WHERE   stof1.stof_id = subs.stof_id_super
    AND     meet.stof_id = stof1.stof_id
    AND     meet.meet_id = p_meet_id
    UNION
    -- samenstellingen (subs)
    SELECT  'a'
            || RPAD(stof1.code,10,'{')
            || RPAD(stof2.code,10,'{')
    FROM    kgc_stoftesten stof1
    ,       kgc_stoftesten stof2
    ,       kgc_sub_stoftesten subs
    ,       bas_meetwaarden meet
    WHERE   stof1.stof_id = subs.stof_id_super
    AND     stof2.stof_id = subs.stof_id_sub
    AND     meet.stof_id = stof2.stof_id
    AND     meet.meet_id = p_meet_id
    UNION
    -- overige, niet-samenstellingen,
    SELECT  DECODE( stof.meet_reken
                  , 'S', 'a'
                  , 'V', 'b'
                  , 'M', 'c'
                  , 'R', 'y'
                  , 'z'      -- komt niet voor
                  )
            || RPAD(stof.code,20,'{')
    FROM    kgc_stoftesten stof
    ,       bas_meetwaarden meet
    WHERE   meet.stof_id = stof.stof_id
    AND     meet.meet_id = p_meet_id
    AND     NOT EXISTS
            ( SELECT NULL
              FROM    kgc_sub_stoftesten subs
              WHERE   subs.stof_id_super = stof.stof_id
            )
    ;
    --  Volgorde stoftesten in protocol
    CURSOR prst_cur
    IS
      SELECT LPAD( meet.meti_id, 10, '0')
          || LPAD( NVL( meet.volgorde, prst.volgorde ), 10, '0')
          || stof.omschrijving
      FROM   kgc_stoftesten stof
      ,      kgc_protocol_stoftesten prst
      ,      bas_metingen meti
      ,      bas_meetwaarden meet
      WHERE  meet.meti_id = meti.meti_id
      AND    meet.stof_id = stof.stof_id
      and    meet.stof_id = prst.stof_id (+)
      and  ( meti.stgr_id = prst.stgr_id or meti.stgr_id is null )
      AND    meet.meet_id = p_meet_id
      ;
    --  Volgorde op werklijst (meet_id kan op meerdere werklijsten voorkomen!)
    CURSOR wlit_cur
    IS
      SELECT LPAD( wlit.volgnummer, 10, '0')
      FROM   kgc_werklijst_items wlit
      WHERE  wlit.meet_id = p_meet_id
      ORDER BY wlit.meet_id DESC -- laatste eerst
      ;
-- PL/SQL Block
BEGIN
  IF ( p_systematiek = 'FRACTIE' )
  THEN
    OPEN  frac_cur;
    FETCH frac_cur
    INTO  v_return;
    CLOSE frac_cur;
  ELSIF ( p_systematiek = 'MATERIAAL' )
  THEN
    OPEN  mate_cur;
    FETCH mate_cur
    INTO  v_return;
    CLOSE mate_cur;
  ELSIF ( p_systematiek = 'METING' )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  v_return;
    CLOSE meti_cur;
  ELSIF ( p_systematiek = 'MONSTER' )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  v_return;
    CLOSE mons_cur;
  ELSIF ( p_systematiek = 'ONDERZOEK' )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_return;
    CLOSE onde_cur;
  ELSIF ( p_systematiek = 'PERSOON' )
  THEN
    OPEN  pers_cur;
    FETCH pers_cur
    INTO  v_return;
    CLOSE pers_cur;
  ELSIF ( p_systematiek = 'PRIORITEIT' )
  THEN
    OPEN  prio_cur;
    FETCH prio_cur
    INTO  v_return;
    CLOSE prio_cur;
  ELSIF ( p_systematiek = 'PROTOCOL' )
  THEN
    OPEN  stgr_cur;
    FETCH stgr_cur
    INTO  v_return;
    CLOSE stgr_cur;
  ELSIF ( p_systematiek = 'STOFTEST' )
  THEN
    OPEN  stof_cur;
    FETCH stof_cur
    INTO  v_return;
    CLOSE stof_cur;
  ELSIF ( p_systematiek = 'TRAY_LR' )
  THEN
    OPEN  tray_lr_cur;
    FETCH tray_lr_cur
    INTO  v_return;
    CLOSE tray_lr_cur;
  ELSIF ( p_systematiek = 'TRAY_BB' )
  THEN
    OPEN  tray_bb_cur;
    FETCH tray_bb_cur
    INTO  v_return;
    CLOSE tray_bb_cur;
  ELSIF ( p_systematiek = 'STOF_TYPE' )
  THEN
    OPEN  stof_type_cur;
    FETCH stof_type_cur
    INTO  v_return;
    CLOSE stof_type_cur;
    -- en daarbinnen op traypositie
    v_return := v_return
             || sorteerwaarde
                ( p_meet_id => p_meet_id
                , p_systematiek => 'TRAY_LR'
                );
  ELSIF ( p_systematiek = 'SAMENSTEL' )
  THEN
    OPEN  samenstelling_cur;
    FETCH samenstelling_cur
    INTO  v_return;
    CLOSE samenstelling_cur;
    -- en daarbinnen op traypositie
    v_return := v_return
             || sorteerwaarde
                ( p_meet_id => p_meet_id
                , p_systematiek => 'TRAY_LR'
                );
  ELSIF ( p_systematiek = 'STOFTEST_IN_PROTOCOL' )           -- 'STOFINPROT'
  THEN
    OPEN  prst_cur;
    FETCH prst_cur
    INTO  v_return;
    CLOSE prst_cur;
  ELSIF ( p_systematiek = 'WERKLIJST' )
  THEN
    OPEN  wlit_cur;
    FETCH wlit_cur
    INTO  v_return;
    CLOSE wlit_cur;
    -- returnwaarde MOET numeriek zijn!!!
    IF ( v_return IS NULL )
    THEN
      v_return := '99999';
    END IF;
  ELSE
    v_return := NULL;
  END IF;
  IF ( v_return IS NULL )
  THEN
    v_return := LPAD( '}', 100, '}' ); -- } komt alfabetisch na de z
  END IF;
  RETURN( v_return );
END sorteerwaarde;
--
FUNCTION sorteerwaarde_mons
( p_mons_id IN NUMBER
, p_systematiek IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  -- monstermateriaalcode
  CURSOR mate_cur
  IS
    SELECT  mate.code
    FROM    kgc_materialen mate
    ,       kgc_monsters mons
    WHERE   mons.mate_id = mate.mate_id
    AND     mons.mons_id = p_mons_id
    ;
  -- monsternummer
  CURSOR mons_cur
  IS
    SELECT  mons.monsternummer
    FROM    kgc_monsters mons
    WHERE   mons.mons_id = p_mons_id
    ;
  -- persoon: achternaam, geboortedatum
  CURSOR pers_cur
  IS
    SELECT  RPAD(pers.achternaam,30,' ')
         || TO_CHAR(pers.geboortedatum,'YYYYMMDD')
         || RPAD(mons.monsternummer,20,' ') persoon
    FROM    kgc_personen pers
    ,       kgc_monsters mons
    WHERE   mons.pers_id = pers.pers_id
    AND     mons.mons_id = p_mons_id
    ;
BEGIN
  IF ( p_systematiek = 'MATERIAAL' )
  THEN
    OPEN  mate_cur;
    FETCH mate_cur
    INTO  v_return;
    CLOSE mate_cur;
  ELSIF ( p_systematiek = 'MONSTER' )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  v_return;
    CLOSE mons_cur;
  ELSIF ( p_systematiek = 'PERSOON' )
  THEN
    OPEN  pers_cur;
    FETCH pers_cur
    INTO  v_return;
    CLOSE pers_cur;
  ELSE
    v_return := NULL;
  END IF;
  IF ( v_return IS NULL )
  THEN
    v_return := LPAD( '}', 100, '}' ); -- } komt alfabetisch na de z
  END IF;
  RETURN( v_return );
END sorteerwaarde_mons;

FUNCTION sorteerwaarde_stof
( p_stof_id IN NUMBER
, p_systematiek IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  -- stoftestcode
  CURSOR stof_cur
  IS
    SELECT  stof.code
    FROM    kgc_stoftesten stof
    WHERE   stof.stof_id = p_stof_id
    ;
  -- samenstellingen eerst, dan voortest, verder alfabetisch stoftestcode
  CURSOR stof_type_cur
  IS
    SELECT  DECODE( stof.meet_reken
                  , 'S', 'a'
                  , 'V', 'b'
                  , 'M', 'c'
                  , 'R', 'y' -- normaliter niet op werklijst
                  , 'z'      -- komt niet voor
                  )
            || stof.code
    FROM    kgc_stoftesten stof
    WHERE   stof.stof_id = p_stof_id
    ;
  -- per samenstelling de stoftesten
  CURSOR samenstelling_cur
  IS
    -- samenstellingen (super)
    SELECT  'a'
            || RPAD(stof1.code,10,'!')
            || RPAD('{',10,'{')
    FROM    kgc_stoftesten stof1
    ,       kgc_sub_stoftesten subs
    WHERE   stof1.stof_id = subs.stof_id_super
    AND     stof1.stof_id = p_stof_id
    UNION
    -- samenstellingen (subs)
    SELECT  'a'
            || RPAD(stof1.code,10,'{')
            || RPAD(stof2.code,10,'{')
    FROM    kgc_stoftesten stof1
    ,       kgc_stoftesten stof2
    ,       kgc_sub_stoftesten subs
    WHERE   stof1.stof_id = subs.stof_id_super
    AND     stof2.stof_id = subs.stof_id_sub
    AND     stof2.stof_id = p_stof_id
    UNION
    -- overige, niet-samenstellingen,
    SELECT  DECODE( stof.meet_reken
                  , 'S', 'a'
                  , 'V', 'b'
                  , 'M', 'c'
                  , 'R', 'y'
                  , 'z'      -- komt niet voor
                  )
            || RPAD(stof.code,20,'{')
    FROM    kgc_stoftesten stof
    WHERE   stof.stof_id = p_stof_id
    AND     NOT EXISTS
            ( SELECT NULL
              FROM    kgc_sub_stoftesten subs
              WHERE   subs.stof_id_super = stof.stof_id
            )
    ;
BEGIN
  IF ( p_systematiek = 'STOFTEST' )
  THEN
    OPEN  stof_cur;
    FETCH stof_cur
    INTO  v_return;
    CLOSE stof_cur;
  ELSIF ( p_systematiek = 'STOF_TYPE' )
  THEN
    OPEN  stof_type_cur;
    FETCH stof_type_cur
    INTO  v_return;
    CLOSE stof_type_cur;
  ELSIF ( p_systematiek = 'SAMENSTEL' )
  THEN
    OPEN  samenstelling_cur;
    FETCH samenstelling_cur
    INTO  v_return;
    CLOSE samenstelling_cur;
  ELSE
    v_return := NULL;
  END IF;
  IF ( v_return IS NULL )
  THEN
    v_return := LPAD( '}', 100, '}' ); -- } komt alfabetisch na de z
  END IF;
  RETURN( v_return );
END sorteerwaarde_stof;
--
FUNCTION stoftest_volgorde
( p_stgr_id NUMBER
, p_stof_id NUMBER
)
RETURN NUMBER
IS
 CURSOR prst_csr
 ( p_stgr_id2 NUMBER
 , p_stof_id2 NUMBER)
 IS
  SELECT prst.volgorde
  FROM   kgc_protocol_stoftesten prst
  WHERE  prst.stgr_id = p_stgr_id2
  AND    prst.stof_id = p_stof_id2
  ;
  v_volgorde kgc_protocol_stoftesten.volgorde%type := NULL;
BEGIN
 IF ( p_stgr_id IS NOT NULL
  AND p_stof_id IS NOT NULL )
 THEN
  OPEN  prst_csr(p_stgr_id,p_stof_id);
  FETCH prst_csr INTO v_volgorde;
  CLOSE prst_csr;
 END IF;
 RETURN(v_volgorde);
END stoftest_volgorde;
--
END kgc_sorteren;
/

/
QUIT
