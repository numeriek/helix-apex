CREATE OR REPLACE PACKAGE "HELIX"."KGC_EMPO_00" IS
/* LST 22-02-2005                                             */
/* Package voor interface met EMPOWER systeem  */
procedure get_empower_user
(p_user out varchar2
,p_password out varchar2
)
;
function get_projects
return varchar2
;
procedure project_login
(p_project in varchar2
);
function get_ssmethod
return varchar2
;
procedure insert_sampleset
(p_sessie  in varchar2
,p_project in varchar2
,p_ss_naam in varchar2
)
;
procedure fetch_ss
(p_ss_method in varchar2
);

function bestaat_ss
(p_ss_method in varchar2
) return boolean
;

procedure create_ss
(p_ss_newmethod in varchar2
);

procedure fetch_methodset
(p_methodset in varchar2
);

procedure destroy_project
;

procedure destroy_ssmethod
;
procedure destroy_newssmethod
;
procedure destroy_methodset
;
function projectnaam_ok
(p_project in varchar2
) return boolean
;
function methodnaam_ok
(p_method in varchar2
) return boolean
;
END KGC_EMPO_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_EMPO_00" IS
/* LST 22-02-2005                                             */
/* Package voor interface met EMPOWER systeem  */

type stui_cur_type is ref cursor return kgc_stoftestgroep_uitvoer%rowtype;
type roin_cur_type is ref cursor return bas_robot_invoer%rowtype;
type stui_tab_type is table of kgc_stoftestgroep_uitvoer%rowtype index by binary_integer;
type roin_tab_type is table of bas_robot_invoer%rowtype index by binary_integer;

-- pointers voor de diverse objecten
v_ssmethod_object     binary_integer;
v_ssmethod_newobject  binary_integer;
v_methodset_object    binary_integer;
v_sslines_obj         binary_integer;
v_project_obj         binary_integer;
/* lokaal */

procedure raise_error
(p_result in binary_integer
) is
  error_src         varchar2(255);
  error_description varchar2(255);
  error_helpfile    varchar2(255);
  error_helpID      binary_integer;
  e_fout            exception;
/* de empower inlognaam is een extra attribuut bij de table kgc_medewerkers  */












/* global */

/*  bij aanroep van project.login zonder orojectnaam worden alle projecten */
/* in project.names gezet.                                                 */
/* als wel een user wordt opgegeven worden alleen die projecten in         */
/* project.names gezet waar de gebruiker toegang toe heeft                 */


/* het eigenlijke inloggen op een project. Dit moet altijd als eerste */
/* actie plaatsvinden.                                                */


/* haal sampleset methods voor lov*/
begin
  IF (p_result!=0) THEN
    ORDCOM.GetLastError(error_src, error_description, error_helpfile, error_helpID);
    dbms_output.put_line('hresult '||to_char(p_result));
    dbms_output.put_line('fout '||error_src||error_description||to_char(error_helpID));
    raise e_fout;
  end if;
EXCEPTION
  WHEN e_fout
  THEN
    ROLLBACK;
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_errtp => 'E'
    , p_rftf => TRUE
    , p_param0 => to_char(p_result)||' '||error_src||' '||error_description
    , p_param1 => NULL
    );
end;

procedure store_ssmethod
is
  hresult  binary_integer := -1;
  v_result binary_integer := -1;
begin
  hresult := ordcom.invoke(v_ssmethod_newobject, 'Store',0,v_result);
  raise_error(hresult);
exception
  when others then
    raise;
end;

procedure store_sslines
(p_line_obj in binary_integer
) is
  hresult       binary_integer := -1;
  v_result      binary_integer := -1;
begin
  ordcom.initarg();
  ordcom.setarg(p_line_obj,'DISPATCH');
  hresult := ordcom.invoke(v_sslines_obj, 'Add', 1, v_result);
  raise_error(hresult);
exception
  when others then
    raise;
end;

/* de empower inlognaam is een extra attribuut bij de table kgc_medewerkers  */
procedure get_empower_user
(p_user out varchar2
,p_password out varchar2
)is
  cursor c_mede
  is
  select attr.code
  ,      atwa.waarde
  from   kgc_attributen attr
  ,      kgc_attribuut_waarden atwa
  ,      kgc_medewerkers mede
  where  atwa.attr_id = attr.attr_id
  and    attr.tabel_naam = 'KGC_MEDEWERKERS'
  and    atwa.id = mede.mede_id
  and    mede.oracle_uid = user
  ;
  l_empower_naam kgc_attribuut_waarden.waarde%type := null;
  l_empower_pass kgc_attribuut_waarden.waarde%type := null;
begin
  for r_mede in c_mede loop
    if r_mede.code = 'EMPOWER_USER' then
      l_empower_naam := r_mede.waarde;
    elsif r_mede.code = 'EMPOWER_PASSWORD' then
      l_empower_pass := r_mede.waarde;
    end if;
  end loop;
  p_user := l_empower_naam;
  p_password := l_empower_pass;
end get_empower_user;


procedure set_field
(p_veld        in varchar2
,p_waarde      in varchar2
,p_waarde_type in varchar2
,p_objecttoken in binary_integer
) is
  v_result      binary_integer;
  hresult       binary_integer := 0;
  v_waarde_int  binary_integer;
  v_waarde_real double precision;
begin
  ordcom.initarg();
  ordcom.setarg(p_veld,'BSTR');
  if p_waarde_type in ('DISPATCH','pDISPATCH')
  then
    v_waarde_int := p_waarde;
    ordcom.setarg(v_waarde_int, p_waarde_type);
  elsif p_waarde_type in ('BSTR','pBSTR','BOOL','pBOOL','DATE','pDATE')
  then
    ordcom.setarg(p_waarde, p_waarde_type);
  else
    v_waarde_real := p_waarde;
    ordcom.setarg(v_waarde_real, p_waarde_type);
  end if;
  hresult := ordcom.invoke(p_objecttoken, 'Set', 2, v_result);
  raise_error(hresult);
end set_field
;

procedure voeg_atwa_toe
(p_objecttoken in binary_integer
,p_stdf_id in number
)
is
  cursor c_atwa
  is
  select expa.kolom_naam
  ,      expa.export_tabel
  ,      expa.export_kolom
  ,      atwa.waarde
  from   kgc_attribuut_waarden atwa
  ,      kgc_attributen attr
  ,      kgc_export_parameters expa
  where  atwa.attr_id = attr.attr_id
  and    attr.tabel_naam = 'KGC_STOFTESTGROEP_UITVOER'
  and    atwa.ID = p_stdf_id
  and    expa.tabel_naam = 'KGC_STOFTESTGROEP_UITVOER'
  AND    expa.kolom_naam = attr.code
  ;
begin
  for r_atwa in c_atwa
  loop
    set_field( p_veld => r_atwa.export_kolom
              , p_waarde => r_atwa.waarde
              , p_waarde_type => 'BSTR'
              , p_objecttoken => p_objecttoken);
  end loop;
end voeg_atwa_toe
;

procedure set_empo_defaults
(p_stui_rec    in kgc_stoftestgroep_uitvoer%rowtype
,p_ss_naam     in varchar2
,p_objecttoken in binary_integer
) is
begin
    set_field( p_veld => 'Vial'
              , p_waarde => p_stui_rec.vialnr_controlerec
              , p_waarde_type => 'BSTR'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Injvol'
              , p_waarde => p_stui_rec.injection_vol
              , p_waarde_type => 'R4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Processing'
              , p_waarde => p_stui_rec.empg_id
              , p_waarde_type => 'I4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Label'
              , p_waarde => p_stui_rec.label
              , p_waarde_type => 'BSTR'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Runtime'
              , p_waarde => p_stui_rec.runtime
              , p_waarde_type => 'R4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Datastart'
              , p_waarde => p_stui_rec.datastart
              , p_waarde_type => 'R4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'NextInjDelay'
              , p_waarde => p_stui_rec.inj_delay
              , p_waarde_type => 'R4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Sampleweight'
              , p_waarde => p_stui_rec.sampleweight
              , p_waarde_type => 'R4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Dilution'
              , p_waarde => p_stui_rec.dilution
              , p_waarde_type => 'R4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Level'
              , p_waarde => p_stui_rec.cal_level
              , p_waarde_type => 'BSTR'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'Function'
              , p_waarde => p_stui_rec.emfs_id
              , p_waarde_type => 'I4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'NumOfInjs'
              , p_waarde => p_stui_rec.aantal_injections
              , p_waarde_type => 'I4'
              , p_objecttoken => p_objecttoken);
    set_field( p_veld => 'MethodSetOrReportMethod'
              , p_waarde => p_stui_rec.methodset
              , p_waarde_type => 'BSTR'
              , p_objecttoken => p_objecttoken);
/*
    set_field( p_veld => 'Samplename'
              , p_waarde => p_ss_naam
              , p_waarde_type => 'BSTR'
              , p_objecttoken => p_objecttoken);
*/
exception
  when others then
    raise;
end set_empo_defaults;

procedure insert_controle
(p_objecttoken in binary_integer
,p_recordcode  in varchar2
)is
  cursor c_expa
  is
  select expa.kolom_naam
  ,      expa.export_tabel
  ,      expa.export_kolom
  from   kgc_export_parameters expa
  where  expa.tabel_naam in ('BAS_ROBOT_INVOER','KGC_ATTRIBUUT_WAARDEN')
  ;
  r_expa c_expa%rowtype;
  hresult       binary_integer := 0;
  v_objecttoken binary_integer := -1;
  v_result      binary_integer;
BEGIN
  for r_expa in c_expa
  loop
    if r_expa.kolom_naam = 'PATIENTNAAM' then
      set_field( p_veld => r_expa.export_kolom
               , p_waarde => p_recordcode
               , p_waarde_type => 'BSTR'
               , p_objecttoken => p_objecttoken);
    else
      set_field( p_veld => r_expa.export_kolom
               , p_waarde => '0000'
               , p_waarde_type => 'BSTR'
               , p_objecttoken => p_objecttoken);
    end if;
  end loop;
end insert_controle;


/* global */

/*  bij aanroep van project.login zonder orojectnaam worden alle projecten */
/* in project.names gezet.                                                 */
/* als wel een user wordt opgegeven worden alleen die projecten in         */
/* project.names gezet waar de gebruiker toegang toe heeft                 */
function get_projects
return varchar2
IS
  hresult       binary_integer := 0;
  v_objecttoken binary_integer := -1;
  v_database    varchar2(50) := null;
  v_project     varchar2(50) := null;
  v_user        varchar2(50) := null;
  v_password    varchar2(50) := null;
  v_result      binary_integer;
  v_names       varchar2(2000);
BEGIN
  get_empower_user( p_user => v_user
                  , p_password => v_password);
  hresult := ordcom.createobject('MillenniumToolkit.Project',0,'',v_objecttoken);
  ordcom.initarg();
  ordcom.setarg(v_database,'BSTR');
  ordcom.setarg(v_project, 'BSTR');
  ordcom.setarg(v_user, 'BSTR');
  ordcom.setarg(v_password, 'BSTR');
  hresult := ordcom.invoke(v_objecttoken, 'Login', 4, v_result);
  raise_error(hresult);
  hresult := ordcom.getproperty(v_objecttoken, 'Names', 0, v_names);
  raise_error(hresult);
  return to_char(v_names);
EXCEPTION
  WHEN OTHERS THEN
    RAISE;
END get_projects;

/* het eigenlijke inloggen op een project. Dit moet altijd als eerste */
/* actie plaatsvinden.                                                */
procedure project_login
(p_project in varchar2
)
IS
  hresult       binary_integer := 0;
  v_database    varchar2(50) := null;
  v_project     varchar2(50) := null;
  v_user        varchar2(50) := null;
  v_password    varchar2(50) := null;
  v_result      binary_integer;
  v_names       varchar2(2000);
BEGIN
  get_empower_user( p_user => v_user
                  , p_password => v_password);
  hresult := ordcom.createobject('MillenniumToolkit.Project',0,'',v_project_obj);
  raise_error(hresult);
  ordcom.initarg();
  ordcom.setarg(v_database,'BSTR');
  ordcom.setarg(p_project, 'BSTR');
  ordcom.setarg(v_user, 'BSTR');
  ordcom.setarg(v_password, 'BSTR');
  hresult := ordcom.invoke(v_project_obj, 'Login', 4, v_result);
  raise_error(hresult);
EXCEPTION
  WHEN OTHERS THEN
    RAISE;
END project_login;

/* haal sampleset methods voor lov*/
function get_ssmethod
return varchar2
IS
  hresult       binary_integer := 0;
  v_objecttoken binary_integer := -1;
  v_names       varchar2(2000);
BEGIN
  hresult := ordcom.createobject('MillenniumToolkit.SampleSetMethod',0,'',v_objecttoken);
  raise_error(hresult);
  hresult := ordcom.getproperty(v_objecttoken, 'MethodSetNames', 0, v_names);
  raise_error(hresult);
  return v_names;
EXCEPTION
  WHEN OTHERS THEN
    RAISE;
END get_ssmethod;

procedure insert_sampleset
(p_sessie  in varchar2
,p_project in varchar2
,p_ss_naam in varchar2
)
is
  cursor c_roin
  is
  select roin.*
  from   bas_robot_invoer roin
  where  roin.sessie = p_sessie
  order by roin.volgorde
  ;
  cursor c_expa
  is
  select expa.kolom_naam
  ,      expa.export_tabel
  ,      expa.export_kolom
  from   kgc_export_parameters expa
  where  expa.tabel_naam  = 'BAS_ROBOT_INVOER'
  ;
  r_roin          c_roin%rowtype;
  r_expa          c_expa%rowtype;
  c_stui          stui_cur_type;
  r_stui          c_stui%rowtype;
  tab_con         stui_tab_type;
  r_stui_eind     c_stui%rowtype;
  l_eind_controle boolean := false;
  l_sluitrec      boolean := false;
  l_teller        number(10);
  hresult         binary_integer := 0;
  v_objecttoken   binary_integer := -1;
  v_result        binary_integer;
begin
  /* structuur voor de insert van de records is als volgt: */
  /* insert start controle rec (optioneel)                 */
  /* insert patient records                                */
  /* insert controlerec om de X patientrecords             */
  /* er kunnen verschillende typen controlerecords geinsert*/
  /* worden.                                               */
  /* insert eind controlerec (optioneel)                   */
  /* insert afsluitend record                              */
  project_login(p_project => p_project);
  open c_stui for
    select stui.*
    from   kgc_stoftestgroep_uitvoer stui
    where  stui.sessie = p_sessie
    order by stui.type_record;
  fetch c_stui into r_stui;
  fetch_ss( p_ss_method => r_stui.samplesetmethod
          );
  create_ss(p_ss_newmethod => p_ss_naam
           );
  while c_stui%found
  loop
    if r_stui.type_record = 'C' /* controle record*/
    then
      tab_con(nvl(tab_con.last,0)+1) := r_stui;
      if r_stui.start_controlerec = 'J'
      then
        /* insert als eerste controle record */
        hresult := ordcom.createobject('MillenniumToolkit.SampleSetLine',0,'',v_objecttoken);
        raise_error(hresult);
        set_empo_defaults(p_stui_rec => tab_con(tab_con.last)
                         ,p_ss_naam => p_ss_naam
                         ,p_objecttoken => v_objecttoken
                         );
        insert_controle(p_objecttoken => v_objecttoken
                       ,p_recordcode => r_stui.code
                       );
        store_sslines(p_line_obj => v_objecttoken);
        store_ssmethod;
      end if;
      if r_stui.eind_controlerec = 'J'
      then
        l_eind_controle := true;
      end if;
    elsif r_stui.type_record = 'P' /* patient */
    then
      l_teller := r_stui.vial_startwaarde - 1;
      for r_roin in c_roin
      loop
        /* insert patientrec * aantal vials */
        for j in 1 .. r_roin.aantal loop
          hresult := ordcom.createobject('MillenniumToolkit.SampleSetLine',0,'',v_objecttoken);
          raise_error(hresult);
          set_empo_defaults(p_stui_rec => r_stui
                           ,p_ss_naam => p_ss_naam
                           ,p_objecttoken => v_objecttoken
                           );
          -- vialnr is de loopteller + startwaarde
          set_field('Vial',to_char(j + l_teller), 'BSTR', v_objecttoken);
          for r_expa in c_expa loop
            if r_expa.kolom_naam = 'ONDERZOEKNR' then
              set_field( p_veld => r_expa.export_kolom
                       , p_waarde => r_roin.onderzoeknr
                       , p_waarde_type => 'BSTR'
                       , p_objecttoken => v_objecttoken);
            elsif r_expa.kolom_naam = 'MONSTERNUMMER' then
              set_field( p_veld => r_expa.export_kolom
                       , p_waarde => r_roin.monsternummer
                       , p_waarde_type => 'BSTR'
                       , p_objecttoken => v_objecttoken);
            elsif r_expa.kolom_naam = 'PROTOCOL_CODE' then
              set_field( p_veld => r_expa.export_kolom
                       , p_waarde => r_roin.protocol_code
                       , p_waarde_type => 'BSTR'
                       , p_objecttoken => v_objecttoken);
            elsif r_expa.kolom_naam = 'PATIENTNAAM' then
              set_field( p_veld => r_expa.export_kolom
                       , p_waarde => r_roin.patientnaam
                       , p_waarde_type => 'BSTR'
                       , p_objecttoken => v_objecttoken);
            elsif r_expa.kolom_naam = 'MEETWAARDE' then
              set_field( p_veld => r_expa.export_kolom
                       , p_waarde => r_roin.meetwaarde
                       , p_waarde_type => 'BSTR'
                       , p_objecttoken => v_objecttoken);
            end if;
          end loop;
          voeg_atwa_toe(v_objecttoken, r_stui.stdf_id);
          store_sslines(p_line_obj => v_objecttoken);
          store_ssmethod;
          l_teller := l_teller + 1;
          for i in tab_con.first .. tab_con.last loop
            if mod(l_teller - (r_stui.vial_startwaarde-1), tab_con(i).aantal_controlerec) = 0 then
              /* insert controlerec = tab_con(i)*/
              hresult := ordcom.createobject('MillenniumToolkit.SampleSetLine',0,'',v_objecttoken);
              raise_error(hresult);
              set_empo_defaults(p_stui_rec => tab_con(i)
                               ,p_ss_naam => p_ss_naam
                               ,p_objecttoken => v_objecttoken
                               );
              insert_controle(p_objecttoken => v_objecttoken
                             ,p_recordcode => tab_con(i).code
                             );
              store_sslines(p_line_obj => v_objecttoken);
              store_ssmethod;
            end if;
          end loop;  /* controlerec */
        end loop; /* aantal vials */
      end loop; /* roin */
    elsif r_stui.type_record = 'S'  /* sluitrecord */
    then
      l_sluitrec := true;
      if l_eind_controle
      then
        for i in tab_con.first .. tab_con.last loop
          if tab_con(i).eind_controlerec = 'J' then
            /* insert eindcontrole records in tab_con(i) */
            hresult := ordcom.createobject('MillenniumToolkit.SampleSetLine',0,'',v_objecttoken);
            raise_error(hresult);
            set_empo_defaults(p_stui_rec => tab_con(i)
                             ,p_ss_naam => p_ss_naam
                             ,p_objecttoken => v_objecttoken
                             );
            insert_controle(p_objecttoken => v_objecttoken
                           ,p_recordcode => tab_con(i).code
                           );
            store_sslines(p_line_obj => v_objecttoken);
            store_ssmethod;
          end if;
        end loop;
      end if;
      /* insert afsluitend record */
      hresult := ordcom.createobject('MillenniumToolkit.SampleSetLine',0,'',v_objecttoken);
      raise_error(hresult);
      set_empo_defaults(p_stui_rec => r_stui
                       ,p_ss_naam => p_ss_naam
                       ,p_objecttoken => v_objecttoken
                       );
      insert_controle(p_objecttoken => v_objecttoken
                     ,p_recordcode => r_stui.code
                     );
      store_sslines(p_line_obj => v_objecttoken);
      store_ssmethod;
    end if;
    fetch c_stui into r_stui;
  end loop;
  close c_stui;
  /* als er geen sluitrecord is moeten er misschien nog wel */
  /* controlerecords geinsert worden.                       */
  if not l_sluitrec
  and l_eind_controle
  then
    for i in tab_con.first .. tab_con.last loop
      if tab_con(i).eind_controlerec = 'J' then
        /* insert eindcontrole records in tab_con(i) */
        hresult := ordcom.createobject('MillenniumToolkit.SampleSetLine',0,'',v_objecttoken);
        raise_error(hresult);
        set_empo_defaults(p_stui_rec => tab_con(i)
                         ,p_ss_naam => p_ss_naam
                         ,p_objecttoken => v_objecttoken
                         );
        insert_controle(p_objecttoken => v_objecttoken
                       ,p_recordcode => tab_con(i).code
                       );
        store_sslines(p_line_obj => v_objecttoken);
        store_ssmethod;
      end if;
    end loop;
  end if;
  store_ssmethod;
  hresult := ordcom.destroyobject(v_sslines_obj);
  raise_error(hresult);
  destroy_ssmethod;
  destroy_newssmethod;
  destroy_project;
exception
  when others then
    raise;
end insert_sampleset
;

procedure fetch_ss
(p_ss_method in varchar2
) is
  hresult       binary_integer := 0;
  v_result      binary_integer;
  v_dummy       varchar2(2000);
begin
  hresult := ordcom.createobject('MillenniumToolkit.SampleSetMethod',0,'',v_ssmethod_object);
  raise_error(hresult);
  hresult := ordcom.setproperty(v_ssmethod_object, 'Name',p_ss_method,'BSTR');
  raise_error(hresult);
  hresult := ordcom.invoke(v_ssmethod_object,'Fetch',0, v_result);
  raise_error(hresult);
  hresult := ordcom.getproperty(v_ssmethod_object, 'SampleSetLines', 0, v_sslines_obj);
  raise_error(hresult);
end fetch_ss;

function bestaat_ss
(p_ss_method in varchar2
) return boolean
is
  hresult       binary_integer := 0;
  v_result      binary_integer;
  v_dummy       varchar2(2000);
  v_return      boolean := false;
begin
  hresult := ordcom.createobject('MillenniumToolkit.SampleSetMethod',0,'',v_ssmethod_object);
  raise_error(hresult);
  hresult := ordcom.setproperty(v_ssmethod_object, 'Name',p_ss_method,'BSTR');
  raise_error(hresult);
  hresult := ordcom.invoke(v_ssmethod_object,'Fetch',0, v_result);
  if hresult != 0 then
    v_return := false;
  else
    v_return := true;
  end if;
  return v_return;
exception
  when others then
    raise;
end bestaat_ss;

procedure create_ss
(p_ss_newmethod in varchar2
) is
  hresult       binary_integer := 0;
  v_result      binary_integer;
  v_dummy       varchar2(2000);
begin
  hresult := ordcom.createobject('MillenniumToolkit.SampleSetMethod',0,'',v_ssmethod_newobject);
  raise_error(hresult);
  hresult := ordcom.setproperty(v_ssmethod_newobject, 'Name',p_ss_newmethod,'BSTR');
  raise_error(hresult);
  hresult := ordcom.setproperty(v_ssmethod_newobject, 'SampleSetLines', v_sslines_obj, 'DISPATCH' );
  raise_error(hresult);
end create_ss;

procedure fetch_methodset
(p_methodset in varchar2
) is
  hresult       binary_integer := 0;
  v_result      binary_integer;
  v_dummy       varchar2(2000);
begin
  hresult := ordcom.createobject('MillenniumToolkitExtensions.MethodSet',0,'',v_methodset_object);
  raise_error(hresult);
  hresult := ordcom.setproperty(v_methodset_object, 'Name',p_methodset,'BSTR');
  raise_error(hresult);
  hresult := ordcom.invoke(v_methodset_object,'Fetch',0, v_result);
  raise_error(hresult);
end fetch_methodset;

procedure destroy_project
is
  hresult binary_integer;
begin
  hresult := ordcom.destroyobject(v_project_obj);
  raise_error(hresult);
end;

procedure destroy_ssmethod
is
  hresult binary_integer;
begin
  hresult := ordcom.destroyobject(v_ssmethod_object);
  raise_error(hresult);
end;

procedure destroy_newssmethod
is
  hresult binary_integer;
begin
  hresult := ordcom.destroyobject(v_ssmethod_newobject);
  raise_error(hresult);
end;

procedure destroy_methodset
is
  hresult binary_integer;
begin
  hresult := ordcom.destroyobject(v_methodset_object);
  raise_error(hresult);
end;

function projectnaam_ok
(p_project in varchar2
) return boolean
is
  v_return boolean := true;
begin
  /* juiste syntax projectnaam is                          */
  /* maximaal 30 karakters                                 */
  /* begint met letter                                     */
  /* bevat letters, cijfers en underscores                 */
  /* dus geen spaties, streepjes, enz                      */
  if length(p_project) > 30
  then
    v_return := false;
  end if;
  if not ascii(substr(p_project,1,1)) between 65 and 90
  and not ascii(substr(p_project,1,1)) between 97 and 122
  then
    v_return := false;
  end if;
  if ltrim(translate(p_project
          ,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_'
          ,'                                                               '
                    )
          ) is not null
  then
    v_return := false;
  end if;
  if instr(p_project,' ') > 0
  then
    v_return := false;
  end if;
  return v_return;
end projectnaam_ok
;
function methodnaam_ok
(p_method in varchar2
) return boolean
is
  v_return boolean := true;
begin
  /* juiste syntax methodnaam is                           */
  /* maximaal 30 karakters                                 */
  /* begint niet met spatie                                */
  /* bevat letters, cijfers, spaties en underscores        */
  /* dus geen streepjes, enz                               */
  if length(p_method) > 30
  then
    v_return := false;
  end if;
  if substr(p_method,1,1) = ' '
  then
    v_return := false;
  end if;
  if ltrim(translate(p_method
          ,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_'
          ,'                                                               '
                    )
          ) is not null
  then
    v_return := false;
  end if;
  return v_return;
end methodnaam_ok
;
END KGC_EMPO_00;
/

/
QUIT
