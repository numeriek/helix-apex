CREATE OR REPLACE PACKAGE "HELIX"."KGC_DFT_NIJMEGEN" IS
 /******************************************************************************
   NAME:       kgc_dft_nijmegen
   PURPOSE:    controleren van declaratiegegevens en overzetten naar financieel systeem
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
              12-02-2013 HBO  Mantis 8116: indien bekend, wordt AGB-code van de aanvrager meegestuurd
              anders "rela.aanspreken"
              12-02-2013 RDE (HBO): cursor c_decl_dubbel uitbereid met decl.pers_id_alt_decl
              06-08-2012  RDE (HBO)        Mantis 3866: dubbele declaraties worden tegengehouden.
              04-05-2011  RDE              Mantis 4668: Toevoegen Icode, en aanvragers gegevens aan DFT bericht
              30-08-2010  GWE              Mantis 1268: Alternatief persoon voor declaratie
   1.0        04-07-2006  op locatie
***************************************************************************** */
PROCEDURE open_verbinding;
PROCEDURE sluit_verbinding;
PROCEDURE controleer
( p_decl_id IN NUMBER
, x_ok out boolean
);
PROCEDURE verwerk
( p_decl_id IN NUMBER
, x_ok out boolean
);
PROCEDURE draai_terug
( p_decl_id IN NUMBER
, x_ok out boolean
);
PROCEDURE test_bepaal_controle;
END KGC_DFT_NIJMEGEN;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DFT_NIJMEGEN" IS
 /******************************************************************************
 NAME: HELIX.kgc_dft_nijmegen
 PURPOSE: controleren van declaratiegegevens en overzetten naar financieel systeem

 REVISIONS:
 Ver Date Author Description
 ------- ---------- --------------- ------------------------------------
 23-01-2014 ophalen service provider igv van handmatige declaraties (zonder deen_id) zie mantisnr 9267
 23-01-2014 modifier SAVEW toegevoegd ivm verwijztyering zie mantisnr 9259
 20-12-2013 HBO Mantisnr 8308
 27-10-2013 HBO diverse aanpasssingen nav ipn EPIC. Zie subproject EPIC->DFT in Mantis
 09-04-2013 CV controle op specialisme toegevoegd zie mantis 8288
 05-03-2013 CV  controle op AGB code aanwezig toegevoegd E:1  zie mantis 8194
 12-02-2013 HBO Mantis 8116: indien bekend, wordt AGB-code van de aanvrager of
                meegestuurd aanspreken of instelling doorgestuurd. Anders "rela.aanspreken"
 12-02-2013 RDE (HBO): cursor c_decl_dubbel uitbereid met decl.pers_id_alt_decl
 06-08-2012 RDE (HBO) Mantis 3866: dubbele declaraties worden tegengehouden.
 04-05-2011 RDE Mantis 4668: Toevoegen Icode, en aanvragers gegevens aan DFT bericht
 30-08-2010 GWE Mantis 1268: Alternatief persoon voor declaratie
 2 21-02-2007 RDE Blokkade op aangemeld in ROCS toegevoegd (B4)
 1 04-07-2006 op locatie
 ***************************************************************************** */

 TYPE t_tab_controles IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
 -- lokaal
 FUNCTION bepaal_controle -- zie testfunctie test_bepaal_controle
 (p_controles IN t_tab_controles
 ,p_ccode IN VARCHAR2
 ,x_ccode_parm_s OUT VARCHAR2
 ,x_ccode_parm_n OUT NUMBER) RETURN BOOLEAN IS
 v_pos NUMBER;
 v_pos_parm NUMBER;
 v_code_aanwezig BOOLEAN := FALSE;
 BEGIN
 --dbms_output.put_line('KGC_DFT_NIJMEGEN.bepaal_controle: 2: '||p_ccode || ';#c=' || to_char(p_controles.COUNT));
 x_ccode_parm_s := NULL;
 x_ccode_parm_n := NULL;
 IF p_controles.COUNT = 0
 THEN
 dbms_output.put_line('Geen controles aanwezig!');
 RETURN FALSE;
 END IF;
 FOR i IN REVERSE p_controles.FIRST .. p_controles.LAST
 LOOP
 -- begin bij laagste nivo
 v_code_aanwezig := TRUE;
 -- aantal toegestane plekken
 v_pos := INSTR(p_controles(i)
 ,',' || p_ccode || ':');
 IF v_pos > 0
 THEN
 -- code gevonden met parm, bv als 2e code of verder
 v_pos_parm := v_pos + 2 + LENGTH(p_ccode);
 ELSE
 -- verder zoeken
 v_pos := INSTR(p_controles(i)
 ,',' || p_ccode || ',');
 IF v_pos > 0
 THEN
 v_pos_parm := 0; -- wel code, geen parm, bv als 2e code of verder
 ELSE
 -- verder zoeken
 IF p_controles(i) LIKE p_ccode || ':%'
 THEN
 -- OK, aan begin
 v_pos_parm := 2 + LENGTH(p_ccode);
 ELSE
 -- verder zoeken
 IF p_controles(i) LIKE p_ccode || ',%'
 THEN
 -- OK, aan begin
 v_pos_parm := 0; -- geen parm
 ELSE
 -- verder zoeken
 IF p_controles(i) LIKE '%,' || p_ccode
 THEN
 -- OK, aan eind
 v_pos_parm := 0; -- geen parm
 ELSE
 v_code_aanwezig := FALSE;
 END IF;
 END IF;
 END IF;
 END IF;
 END IF;
 IF v_code_aanwezig
 THEN
 -- code gevonden? evt parm ophalen en klaar
 IF v_pos_parm > 0
 THEN
 v_pos := INSTR(p_controles(i)
 ,','
 ,v_pos_parm);
 IF v_pos > 0
 THEN
 -- er is een volgende controlecode
 x_ccode_parm_s := SUBSTR(p_controles(i)
 ,v_pos_parm
 ,v_pos - v_pos_parm);
 ELSE
 -- tot einde
 x_ccode_parm_s := SUBSTR(p_controles(i)
 ,v_pos_parm);
 END IF;
 IF NVL(LENGTH(x_ccode_parm_s)
 ,0) > 0
 THEN
 -- probeer numeriek
 BEGIN
 x_ccode_parm_n := TO_NUMBER(x_ccode_parm_s);
 EXCEPTION
 WHEN OTHERS THEN
 NULL;
 END;
 END IF;
 END IF;
 RETURN TRUE;
 END IF;
 END LOOP;
  RETURN FALSE;
 END;

function check_datum
  ( p_datum in varchar2,
    p_formaat in varchar2 default 'dd-mm-yyyy')
  return boolean
  is
  v_datum date;
  begin
    select to_date(p_datum, p_formaat)
    into v_datum
    from dual
    ;
    return true;
    exception when others then
    return false;
END check_datum;

  FUNCTION check_verstuur_dft_p03
  ( p_decl_id IN NUMBER
  , p_aantal IN NUMBER
  , p_check_only IN BOOLEAN
  ) RETURN BOOLEAN IS
    CURSOR c_decl(p_decl_id NUMBER) IS
      SELECT decl.*
      ,      nvl(upper(dewy.code), 'BI') dewy_code
      ,      ongr.code ongr_code
      ,      upper(kgc_attr_00.waarde('KGC_DECL_ENTITEITEN', 'DFT_SERVICE_PROVIDER', decl.deen_id)) service_provider
      FROM   kgc_declaraties decl
      ,      kgc_declaratiewijzen dewy
      ,      kgc_onderzoeksgroepen ongr
      WHERE  decl.dewy_id = dewy.dewy_id(+)
      and   decl.ongr_id = ongr.ongr_id(+)
      and decl.decl_id = p_decl_id
      FOR UPDATE OF decl.status NOWAIT -- rde 20-08-2013: alleen decl locken, niet dewy en ongr
      ;
    r_decl c_decl%ROWTYPE;

    -- mantisnr 8116
    -- kgc_specialismen toegevoegd
    -- mantis 8288 relatie code toegevoegd (in melding) en bevoegd_declareren,dit laatste niet voor buitenlandse declaraties
    CURSOR c_rela(p_rela_id NUMBER) IS
      SELECT rela.rela_id
      ,rela.code rela_code
      ,rela.aanspreken
      ,rela.inst_id
      ,NVL(rela.land, inst.land) land
      ,inst.code inst_code
      ,spec.code spec_code
      , kgc_attr_00.waarde('KGC_SPECIALISMEN', 'BEVOEGD_DECLAREREN', spec.spec_id) bevoegd_declareren
      , upper(rela.liszcode) liszcode
      , rela.achternaam
      FROM kgc_relaties rela
      ,kgc_instellingen inst
      ,kgc_specialismen spec
      WHERE rela.rela_id = p_rela_id
      AND rela.inst_id = inst.inst_id(+)
      AND rela.spec_id = spec.spec_id(+)
      ;
      r_rela c_rela%ROWTYPE;
      r_rela_wds c_rela%ROWTYPE;
    cursor c_rela2 (p_znr varchar2) is
      select *
      from   kgc_relaties
      where  upper(liszcode) = upper(p_znr)
      ;
    r_rela2 c_rela2%ROWTYPE;
    CURSOR c_onde(p_onde_id NUMBER) IS
      SELECT *
      FROM   kgc_onderzoeken onde
      WHERE  onde.onde_id = p_onde_id
      ;
    r_onde c_onde%ROWTYPE;
    CURSOR c_onbe(p_onbe_id NUMBER) IS
      SELECT *
      FROM kgc_onderzoek_betrokkenen onbe
      WHERE onbe.onbe_id = p_onbe_id
      ;
    r_onbe c_onbe%ROWTYPE;
    CURSOR c_meti(p_meti_id NUMBER) IS
      SELECT *
      FROM   bas_metingen meti
      WHERE meti.meti_id = p_meti_id
      ;
    r_meti c_meti%ROWTYPE;
    CURSOR c_gesp(p_gesp_id NUMBER) IS
      SELECT gesp.*
      ,      ongr.code ongr_code
      FROM   kgc_gesprekken        gesp
             ,kgc_onderzoeksgroepen ongr
      WHERE  gesp.gesp_id = p_gesp_id
      AND    gesp.ongr_id = ongr.ongr_id
      ;
    r_gesp c_gesp%ROWTYPE;
    CURSOR c_cate(p_kafd_id NUMBER, p_verrichtingcode VARCHAR2) IS
      SELECT NULL
      FROM   kgc_categorie_types   caty
            ,kgc_categorieen       cate
            ,kgc_categorie_waarden cawa
      WHERE  caty.code = 'VIC'
      AND    cate.kafd_id = p_kafd_id
      AND    cawa.waarde = p_verrichtingcode
      AND    caty.caty_id = cate.caty_id
      AND    cate.cate_id = cawa.cate_id;
    r_cate c_cate%ROWTYPE;

    CURSOR c_cate2(p_code VARCHAR2, p_waarde VARCHAR2) IS
      SELECT null
      FROM   kgc_categorie_types   caty
            ,kgc_categorieen       cate
            ,kgc_categorie_waarden cawa
      WHERE  caty.caty_id = cate.caty_id
      AND    cate.cate_id = cawa.cate_id
      AND    caty.code = upper(p_code)
      AND    cawa.waarde = p_waarde;
    r_cate2 c_cate2%ROWTYPE;

    CURSOR c_onin(p_onde_id NUMBER) IS
      SELECT ingr_id
            ,indi_id
            ,onre_id
      FROM   kgc_onderzoek_indicaties onin
      WHERE  onin.onde_id = p_onde_id;
    r_onin c_onin%ROWTYPE;
    CURSOR c_kafd(p_kafd_id NUMBER) IS
      SELECT code
      FROM   kgc_kgc_afdelingen kafd
      WHERE  kafd.kafd_id = p_kafd_id;
    CURSOR c_sypa(p_code VARCHAR2) IS
      SELECT sypa_id
            ,standaard_waarde
      FROM   kgc_systeem_parameters
      WHERE  code = UPPER(p_code);
    r_sypa c_sypa%ROWTYPE;
    CURSOR c_spwa(p_sypa_id IN NUMBER, p_kafd_id IN NUMBER, p_ongr_id IN NUMBER) IS
      SELECT waarde
      FROM   kgc_systeem_par_waarden spwa
      WHERE  spwa.sypa_id = p_sypa_id
      AND    spwa.kafd_id = p_kafd_id
      AND    (spwa.ongr_id = p_ongr_id OR
            (p_ongr_id IS NULL AND spwa.ongr_id IS NULL));
    r_spwa c_spwa%ROWTYPE;
    CURSOR c_macht(p_decl_id NUMBER) IS
      SELECT 1
      FROM   kgc_brieven    brie
            ,kgc_brieftypes brty
      WHERE  brie.decl_id = p_decl_id
      AND    brie.brty_id = brty.brty_id
      AND    brty.code = 'MACHTIGING';
    r_macht c_macht%ROWTYPE;
    -- cursor c_decl_dubbel toegevoegd naar aanleiding van mantis 3866
    CURSOR c_decl_dubbel(p_decl_id NUMBER, p_pers_id NUMBER, p_datum DATE, p_verrichtingcode VARCHAR2) IS
      SELECT 1
      FROM   kgc_declaraties decl, kgc_declaratiewijzen dewy
      WHERE  decl.dewy_id = dewy.dewy_id(+)
      AND    nvl(decl.pers_id_alt_decl, decl.pers_id) = p_pers_id
      AND    TRUNC(decl.datum) = TRUNC(p_datum)
      AND    decl.status = 'V'
      -- de bijvoegsels van de verrichtingcodes wordt niet meegenomen in de vergelijking (mantisnr:8308)
      AND    regexp_replace(decl.verrichtingcode, '[^[:digit:]]') = regexp_replace(p_verrichtingcode, '[^[:digit:]]')
      AND    nvl(upper(dewy.code), 'BI') <> 'BU' -- niet vergelijken met bu declaraties (mantisnr:8308)
      AND    decl.decl_id <> p_decl_id;
    r_decl_dubbel     c_decl_dubbel%ROWTYPE;
    CURSOR c_attr (p_tabel VARCHAR2, p_code VARCHAR2) is
      SELECT *
      FROM   kgc_attributen attr
      WHERE  attr.tabel_naam = p_tabel
      AND    attr.code = p_code
      ;
    r_attr_inst c_attr%ROWTYPE;
    r_attr_onde c_attr%ROWTYPE;
    CURSOR c_inst_via_attr(p_attr_id NUMBER, p_waarde VARCHAR2) IS
      SELECT inst.inst_id
      ,      inst.code
      FROM   kgc_instellingen inst
      ,      kgc_attribuut_waarden atwa
      WHERE  atwa.waarde = p_waarde
      AND    atwa.attr_id = p_attr_id
      AND    atwa.id = inst.inst_id
      ;
    r_inst_via_attr c_inst_via_attr%ROWTYPE;
    CURSOR c_imlo_inst(p_onde_id NUMBER) IS
      SELECT impw.helix_waarde
      FROM   kgc_im_onderzoeken imon
      ,      kgc_import_files imfi
      ,      kgc_import_parameters impa
      ,      kgc_import_param_waarden impw
      WHERE  imon.kgc_onde_id = p_onde_id
      AND    imon.imfi_id = imfi.imfi_id
      AND    impa.helix_table_domain = 'KGC_INSTELLINGEN'
      AND    impw.imlo_id = imfi.imlo_id
      AND    impw.impa_id = impa.impa_id
      ;
    r_imlo_inst c_imlo_inst%ROWTYPE;

    -- alternatieve service_provider igv handmatige declaraties buiten declaratie criteria
    cursor c_deen (p_kafd_id number, p_verrichtingcode varchar2) is
    select upper(kgc_attr_00.waarde('KGC_DECL_ENTITEITEN', 'DFT_SERVICE_PROVIDER', deen_id)) service_provider
    from kgc_decl_entiteiten
    where kafd_id = p_kafd_id
    and upper(verrichtingcode) = upper(p_verrichtingcode)
    ;
    r_deen c_deen%rowtype;

    v_hl7_msg_bron kgc_zis.hl7_msg;
    v_hl7_msg_res  kgc_zis.hl7_msg;

    v_pers_trow      cg$KGC_PERSONEN.cg$row_type;
    v_pers_trow_null cg$KGC_PERSONEN.cg$row_type;
    v_verz_trow      cg$KGC_VERZEKERAARS.cg$row_type;
    v_onde_id        NUMBER;
    v_attr_onde_tabel varchar2(30) := 'KGC_ONDERZOEKEN';
    v_attr_onde_code varchar2(30) := 'FEZ_ALT_ICODE';
    v_attr_inst_tabel varchar2(30) := 'KGC_INSTELLINGEN';
    v_attr_inst_code varchar2(30) := 'FEZ_ICODE';
    -- speciale gegevens gebruikt in het bericht:
    v_spec_verdatum       DATE; -- KGCN specifiek 1
    v_spec_vercode        VARCHAR2(20); -- KGCN specifiek 2
    v_spec_aanvrager      VARCHAR2(2000);
    v_bron_dt_uitv        VARCHAR2(50);
    v_spec_fez_icode      VARCHAR2(100);

    v_pers_is_dummy  BOOLEAN;
    v_verdat_dagen   NUMBER;
    v_decl_kafd_code KGC_KGC_AFDELINGEN.CODE%TYPE;
    v_sysdate        VARCHAR2(20) := to_char(SYSDATE
                                            ,'yyyymmddhh24miss');
    v_temp           VARCHAR2(2000);
    v_tab_controles  t_tab_controles;
    v_controle_sypa  VARCHAR2(100) := 'ZIS_DFT_CONTROLES';
    v_ccode_aanwezig BOOLEAN;
    v_ccode_parm_s   VARCHAR2(4000);
    v_ccode_parm_n   NUMBER;
    v_strAntw        VARCHAR2(32767);
    v_strDFT         VARCHAR2(32767);
    v_MSA            VARCHAR2(10);
    v_MSH            VARCHAR2(10);
    v_ok             BOOLEAN;
    v_msg_control_id VARCHAR2(10);
    v_msg            VARCHAR2(32767);
    v_sep            VARCHAR2(2);
    v_nl             VARCHAR2(2) := CHR(10);
    v_debug          VARCHAR2(100);
    v_thisproc       VARCHAR2(100) := 'kgc_dft_nijmegen.check_verstuur_dft_p03';
    v_trans_type     VARCHAR2(2);
    v_uitv_afd       varchar2(50);
    v_aanvrager_id   varchar2(50);
    v_assigned_auth  varchar2(10);
    v_name_type      varchar2(20);
    v_korting_perc   varchar2(10);
    v_korting_datum_vanaf  varchar2(10);
    v_korting_datum_tm varchar2(10);
    v_korting_atwa  varchar2(100);
    v_modifiers      varchar2(100);
    v_modif_betaler  varchar (20);
    v_modif_korting   varchar2(10);
    v_hlbe_id         kgc_hl7_berichten.hlbe_id%type;

    PROCEDURE add2msg(p_tekst VARCHAR2) IS
    BEGIN
      v_msg := v_msg || v_sep || p_tekst;
      v_sep := v_nl;
    END;
    PROCEDURE upd_decl(p_decl_id          NUMBER
                      ,p_datum               DATE --Mantis 3866
                      ,p_status           VARCHAR2
                      ,p_datum_verwerking DATE
                      ,p_verwerking       VARCHAR2) IS
    BEGIN
      UPDATE kgc_declaraties
      SET    verwerking       = p_verwerking
            ,datum            = p_datum       --Mantis 3866
            ,status           = p_status
            ,datum_verwerking = p_datum_verwerking
      WHERE  decl_id = p_decl_id;
      COMMIT;
    END;
  BEGIN
    kgc_zis.g_sypa_hl7_versie := kgc_sypa_00.systeem_waarde('HL7_VERSIE');
    v_msg_control_id          := SUBSTR(TO_CHAR(abs(dbms_random.random))
                                       ,1
                                       ,10);

    -- ** generieke controles **
    BEGIN
      OPEN c_decl(p_decl_id);
      FETCH c_decl
        INTO r_decl;
      IF c_decl%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': p_decl_id niet gevonden: ' ||
                                to_char(p_decl_id));
      END IF;
      CLOSE c_decl;
    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE = -54
        THEN
          -- resource busy and acquire with NOWAIT specified
          RETURN FALSE;
        END IF;
        RAISE; -- iets anders? Niet OK
    END;
    -- indien decl-status = V + verwerken: niet OK
    IF r_decl.status = 'V'
    THEN
      IF p_check_only
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al verwerkt! GEEN melding!
      ELSIF p_aantal = 1
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al verwerkt! GEEN melding!
      END IF;
    ELSIF r_decl.status = 'R'
    THEN
      IF p_check_only
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al teruggedraaid! GEEN melding!
      ELSIF p_aantal = -1
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al teruggedraaid! GEEN melding!
      END IF;
    END IF;

    OPEN c_kafd(r_decl.kafd_id);
    FETCH c_kafd
      INTO v_decl_kafd_code;
    CLOSE c_kafd;
    -- verrichting - nu alleen nog maar ONDE/ONBE/METI/GESP
    IF r_decl.entiteit = 'ONDE'
    THEN
      v_debug   := v_debug || 'a';
      v_onde_id := r_decl.id;
    ELSIF r_decl.entiteit = 'ONBE'
    THEN
      v_debug := v_debug || 'b';
      OPEN c_onbe(r_decl.id);
      FETCH c_onbe
        INTO r_onbe;
      IF c_onbe%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': decl.ONBE_ID niet gevonden: ' ||
                                to_char(r_decl.id));
      END IF;
      CLOSE c_onbe;
      v_onde_id := r_onbe.onde_id;
    ELSIF r_decl.entiteit = 'METI'
    THEN
      v_debug := v_debug || 'c';
      OPEN c_meti(r_decl.id);
      FETCH c_meti
        INTO r_meti;
      IF c_meti%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': decl.METI_ID niet gevonden: ' ||
                                to_char(r_decl.id));
      END IF;
      CLOSE c_meti;
      v_onde_id := r_meti.onde_id;
    ELSIF r_decl.entiteit = 'GESP'
    THEN
      v_debug := v_debug || 'd';
      OPEN c_gesp(r_decl.id);
      FETCH c_gesp
        INTO r_gesp;
      IF c_gesp%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': decl.GESP_ID niet gevonden: ' ||
                                to_char(r_decl.id));
      END IF;
      CLOSE c_gesp;
      v_onde_id := r_gesp.onde_id;
    ELSE
      v_debug := v_debug || 'e';
      raise_application_error(-20000
                             ,v_thisproc ||
                              ': decl.entiteit wordt niet ondersteund: ' ||
                              r_decl.entiteit);
    END IF;
    -- onderzoek ophalen
    OPEN c_onde(v_onde_id);
    FETCH c_onde
      INTO r_onde;
    IF c_onde%NOTFOUND
    THEN
      raise_application_error(-20000
                             ,v_thisproc || ': onde_id niet gevonden: ' ||
                              to_char(v_onde_id));
    END IF;
    CLOSE c_onde;
    -- (oorspronkelijke)aanvrager ophalen
    v_debug := '2';
    OPEN c_rela(nvl(r_onde.rela_id_oorsprong
                   ,r_onde.rela_id));
    FETCH c_rela
      INTO r_rela;
    CLOSE c_rela;
    v_debug := '3';

    -- Mantis 4886: bepaal extra attributen
    OPEN c_attr (v_attr_inst_tabel, v_attr_inst_code);
    FETCH c_attr INTO r_attr_inst;
    IF c_attr%NOTFOUND THEN
      raise_application_error(-20000
                             ,v_thisproc || ': extra attribuutwaarde ' || v_attr_inst_code || ' bestaat niet voor ' || v_attr_inst_tabel || '!');
    END IF;
    CLOSE c_attr;
    OPEN c_attr (v_attr_onde_tabel, v_attr_onde_code);
    FETCH c_attr INTO r_attr_onde;
    IF c_attr%NOTFOUND THEN
      raise_application_error(-20000
                             ,v_thisproc || ': extra attribuutwaarde ' || v_attr_onde_code || ' bestaat niet voor ' || v_attr_onde_tabel || '!');
    END IF;
    CLOSE c_attr;

    -- Mantis 1268: Bij alternatief persoon deze gebruiken i.p.v. de persoon waarop de declaratie betekking heeft
    IF r_decl.pers_id_alt_decl IS NULL
    THEN
      v_pers_trow.pers_id := r_decl.pers_id;
    ELSE
      v_pers_trow.pers_id := r_decl.pers_id_alt_decl;
    END IF;

    cg$kgc_personen.slct(v_pers_trow); -- notfound is al afgevangen

    v_pers_is_dummy := (v_pers_trow.zisnr IS NULL);
    IF v_pers_trow.verz_id IS NOT NULL
    THEN
      v_verz_trow.verz_id := v_pers_trow.verz_id;
      cg$kgc_verzekeraars.slct(v_verz_trow);
    END IF;
    -- ** generieke controles **
    -- decl declareren?
    IF r_decl.declareren <> 'J'
    THEN
      add2msg('Declaratie.declareren staat op NEE');
    END IF;
    -- pers aanwezig?
    IF v_pers_trow.pers_id IS NULL
    THEN
      add2msg('Declaratie.persoon is afwezig!');
    ELSE
      -- check persoon
      NULL; -- afhankelijk van de instelling
    END IF;
    -- kafd aanwezig?
    IF r_decl.kafd_id IS NULL
    THEN
      add2msg('Declaratie.afdeling is afwezig!');
    END IF;
    -- check aantal
    IF p_aantal IN (1, -1)
    THEN
      NULL; -- OK
    ELSE
      add2msg('Aantal declaraties moet 1 of -1 zijn (aangeboden: ' ||
              to_char(p_aantal) || ')!');
    END IF;

   -- BU en WDS declaraties die door een derde betaald worden.
   if r_decl.dewy_code in('WDS', 'BU') then
      v_modif_betaler := 'WDS';

      v_spec_fez_icode := upper(kgc_attr_00.waarde(v_attr_onde_tabel, v_attr_onde_code, r_onde.onde_id));
      IF NVL(LENGTH(v_spec_fez_icode), 0) > 0
      THEN
         -- controleer of deze code bij een instelling voorkomt
          OPEN c_inst_via_attr(p_attr_id => r_attr_inst.attr_id, p_waarde => v_spec_fez_icode);
          FETCH c_inst_via_attr INTO r_inst_via_attr;
          IF c_inst_via_attr%NOTFOUND THEN
            add2msg('Onderzoek.' || v_attr_onde_code || ' bevat waarde ''' || v_spec_fez_icode || ''',  maar deze komt niet voor bij een instelling!');
          END IF;
          CLOSE c_inst_via_attr;
      ELSE
        -- geen code op nivo onderzoek? dan kijken naar instelling
        IF r_decl.dewy_code =  'BU' THEN
          v_spec_fez_icode := upper(kgc_attr_00.waarde(v_attr_inst_tabel, v_attr_inst_code, r_rela.inst_id));
          IF NVL(LENGTH(v_spec_fez_icode), 0) = 0 THEN -- verplicht!
            add2msg('Onderzoek.relatie.instelling.' || v_attr_inst_code || ' bevat geen waarde! (relatie ' || r_rela.aanspreken || ')');
          END IF;
        ELSIF r_decl.dewy_code =  'WDS' THEN -- niet kijken naar de eventuele oorspronkelijke aanvrager; rde 20-08-2013
          OPEN c_rela(r_onde.rela_id);
          FETCH c_rela INTO r_rela_wds;
          CLOSE c_rela;
          v_spec_fez_icode := upper(kgc_attr_00.waarde(v_attr_inst_tabel, v_attr_inst_code, r_rela_wds.inst_id));
          IF NVL(LENGTH(v_spec_fez_icode), 0) = 0 THEN -- verplicht!
            add2msg('Onderzoek.relatie.instelling.' || v_attr_inst_code || ' bevat geen waarde! (relatie ' || r_rela_wds.aanspreken || ')');
          END IF;
        END IF;
      END IF;
   end if;

    IF NVL(LENGTH(v_msg)
          ,0) > 0
    THEN
      -- nu al fout? niet verder gaan!
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Controle NIET OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.datum  -- geen wijziging; Mantis 3866
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    END IF;

    -- ** specifieke controles en samenstellen msg **
    -- algemene controles
    OPEN c_sypa(v_controle_sypa);
    FETCH c_sypa
      INTO r_sypa;
    IF c_sypa%NOTFOUND
    THEN
      raise_application_error(-20000
                             ,v_thisproc || ': systeemparameter ' ||
                              v_controle_sypa || ' werd niet gevonden!');
    END IF;
    CLOSE c_sypa;
    dbms_output.put_line('1; ' || r_sypa.standaard_waarde);
    v_tab_controles(1) := r_sypa.standaard_waarde;
    -- afdelingspecifieke controles
    OPEN c_spwa(r_sypa.sypa_id
               ,r_decl.kafd_id
               ,NULL);
    FETCH c_spwa
      INTO r_spwa;
    IF c_spwa%NOTFOUND
    THEN
      v_tab_controles(2) := '';
    ELSE
      v_tab_controles(2) := r_spwa.waarde;
    END IF;
    CLOSE c_spwa;
    dbms_output.put_line('2; ' || r_spwa.waarde);
    -- onderzoeksgroepspecifieke controles
    OPEN c_spwa(r_sypa.sypa_id
               ,r_decl.kafd_id
               ,r_decl.ongr_id);
    FETCH c_spwa
      INTO r_spwa;
    IF c_spwa%NOTFOUND
    THEN
      v_tab_controles(3) := '';
    ELSE
      v_tab_controles(3) := r_spwa.waarde;
    END IF;
    CLOSE c_spwa;
    dbms_output.put_line('3; ' || r_spwa.waarde);

    --*** A1 Van welke entiteiten mogen er declaraties verzonden worden?
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A1 is afwezig!');
    ELSIF INSTR('-' || v_ccode_parm_s || '-'
               ,'-' || r_decl.entiteit || '-') > 0
    THEN
      NULL; -- OK!
    ELSE
      add2msg('A1-entiteit ' || r_decl.entiteit ||
              ' mag NIET gedeclareerd worden!');
    END IF;

    -- **** KGCN specifiek 1
    v_spec_verdatum := r_decl.datum; -- mantis 3866
    v_bron_dt_uitv  := 'decl.datum'; -- Mantis 3866

    -- *** onderstaande is tijdelijk (Mantis 3866): totdat de verzekeraars meerdere declaraties per dag accpeteren (/pers/vc/)
  -- deze bepaling moet NA het evt. ophalen van de alternatieve declaratiepersoon, en de evt gewijzigde datum
  -- moet alleen opgeslagen worden indien het bericht verzonden is.
  -- er moet gekeken worden of er al een verzonden declaratie bestaat voor de persoon/verrichtingcode/datum (los van de entiteiten)
     --**** KGCN specifiek 1
  IF r_decl.dewy_code <> 'BU' THEN  -- voor BU declaratie mogen dubbele declaraties verzonden worden.(mantisnr:8308)
    WHILE TRUE --
    LOOP
      -- check of deze datum ok is
      OPEN c_decl_dubbel
        ( p_decl_id         => r_decl.decl_id
        , p_pers_id         => v_pers_trow.pers_id
        , p_datum           => v_spec_verdatum
        , p_verrichtingcode => r_decl.verrichtingcode);
      FETCH c_decl_dubbel INTO r_decl_dubbel;
      IF c_decl_dubbel%NOTFOUND
      THEN -- OK!
        CLOSE c_decl_dubbel;
        EXIT;
      END IF;
      CLOSE c_decl_dubbel;
      -- nieuwe datum: Deze mag niet groter zijn dan overlijdensdatum patient (mantisnr:8308)
      IF (
        (v_pers_trow.overlijdensdatum is not null) and
        (trunc(v_spec_verdatum + 1) > trunc(v_pers_trow.overlijdensdatum) )
        ) THEN
        EXIT;
      END IF;
      v_spec_verdatum := v_spec_verdatum + 1;

      IF TRUNC(v_spec_verdatum) >= TRUNC(sysdate)
      THEN
        add2msg('Er zijn reeds vergelijkbare declaraties verzonden met verrichtingsdatum ' || to_char(r_decl.datum, 'dd-mm-yyyy') || '; er kon geen nieuwe verrichtingsdatum in het verleden bepaald worden');
        EXIT;
      END IF;
    END LOOP;
 END IF;
  -- *** EINDE "onderstaande is tijdelijk"

    IF r_decl.entiteit = 'GESP'
    THEN
      v_spec_verdatum := r_gesp.datum;
      v_bron_dt_uitv  := 'gesp.datum';
    END IF;
    IF v_spec_verdatum IS NULL
    THEN
      add2msg('Declaratie.verrichtingdatum ontbreekt (bron: ' ||
              v_bron_dt_uitv || ')!');
    ELSE
      --*** A2 Aantal dagen dat de verrichtingdatum in het verleden mag liggen (-1 betekent geen controle)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'A2'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - A2 is afwezig!');
      ELSIF v_ccode_parm_n = -1
      THEN
        NULL; -- geen controle
      ELSE
        IF SYSDATE - v_spec_verdatum <= v_ccode_parm_n
        THEN
          NULL; -- OK
        ELSE
          add2msg('A2-Declaratie.verrichtingdatum (' ||
                  to_char(v_spec_verdatum
                         ,'dd-mm-yyyy') || ') ligt meer dan ' ||
                  NVL(to_char(v_ccode_parm_n)
                     ,'<LEEG!>') || ' dagen in het verleden!');
        END IF;
      END IF;
    END IF;

    -- **** KGCN specifiek 2
    v_spec_vercode := r_decl.verrichtingcode;
    IF NVL(LENGTH(v_spec_vercode)
          ,0) = 0
    THEN
      add2msg('Declaratie.Verrichtingcode ontbreekt!');
    END IF;

    -- **** KGCN specifiek 5
    -- aanvrager + data bepalen: instelling (evt) + aanvrager
    v_debug               := '3';
    v_debug               := v_debug || 'd';

    -- algemene controles
    --*** A3 Moet de verzekeraar van de declaratie (indien aanwezig) niet-vervallen zijn? (A3:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A3 is afwezig!');
    ELSIF v_ccode_parm_s = 'J'
    THEN
      IF v_verz_trow.verz_id IS NOT NULL
         AND v_verz_trow.vervallen = 'J'
      THEN
        add2msg('A3-Declaratie.persoon.verzekeraar (zisnr=' ||
                v_pers_trow.zisnr || ', code=' || v_verz_trow.code ||
                ') is vervallen!');
      END IF;
    END IF;
    --*** A4 Moet er een indicatie en reden zijn bij het onderzoek (A4: N = Nee; B=Beide, I=Alleen indicatie, R=alleen reden)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A4 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      OPEN c_onin(v_onde_id);
      FETCH c_onin
        INTO r_onin;
      IF c_onin%NOTFOUND
      THEN
        add2msg('A4-Een indicatie/reden bij onderzoek ' ||
                r_onde.onderzoeknr ||
                ' is verplicht maar werd niet gevonden!');
      ELSIF r_onin.indi_id IS NULL
            AND UPPER(v_ccode_parm_s) IN ('B', 'I')
      THEN
        add2msg('A4-Een indicatie bij onderzoek ' || r_onde.onderzoeknr ||
                ' is verplicht maar deze is niet gevuld!');
      ELSIF r_onin.onre_id IS NULL
            AND UPPER(v_ccode_parm_s) IN ('B', 'R')
      THEN
        add2msg('A4-Een reden bij onderzoek ' || r_onde.onderzoeknr ||
                ' is verplicht maar is niet gevuld!');
      ELSIF (r_onin.indi_id IS NULL OR r_onin.onre_id IS NULL)
            AND UPPER(v_ccode_parm_s) IN ('B')
      THEN
        add2msg('A4-Een indicatie en reden bij onderzoek ' ||
                r_onde.onderzoeknr ||
                ' zijn beide verplicht maar zijn niet allebei gevuld!');
      END IF;
      CLOSE c_onin;
    END IF;
    --*** A5 Mag de declaratiepersoon overleden zijn? (A5:J = Ja; N = Nee)
    v_debug          := 'A5';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A5'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A5 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'J'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_trow.overleden = 'J'
      THEN
        add2msg('A5-declaratiepersoon (zisnr=' || v_pers_trow.zisnr ||
                ') is overleden!');
      END IF;
    END IF;

    v_debug := 'A6';
    --*** A6 Moet de declaratie-persoon een ZISnummer hebben? (D1:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A6'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A6 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_is_dummy
      THEN
        add2msg('A6-declaratiepersoon moet een ZISnummer hebben!');
      END IF;
    END IF;

    --*** A7 Moet de declaratiepersoon beheerd-in-ZIS zijn? (A7:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A7'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A7 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF NVL(v_pers_trow.beheer_in_zis
            ,'N') <> 'J'
      THEN
        add2msg('A7-declaratiepersoon (zisnr=' || v_pers_trow.zisnr ||
                ') is niet beheerd in ZIS!');
      END IF;
    END IF;

    --*** A8 Moet de verrichtingcode bestaan als categorie van type VIC? (A8:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A8'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A8 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF NVL(LENGTH(v_spec_vercode)
            ,0) > 0
      THEN
        -- indien leeg, dan is er al een foutmelding gegeven!
        -- controleer of deze code genoemd wordt in categorie
        OPEN c_cate(r_decl.kafd_id
                   ,v_spec_vercode);
        FETCH c_cate
          INTO r_cate;
        IF c_cate%NOTFOUND
        THEN
          add2msg('A8-Declaratie.Verrichtingcode ''' || v_spec_vercode ||
                  ''' bestaat niet als Helix-categorie VIC bij de afdeling ''' ||
                  v_decl_kafd_code || '''!');
        END IF;
        CLOSE c_cate;
      END IF;
    END IF;

    --*** A9 Indien de overlijdensdatum gevuld is, moet dan de verrichtingdatum voor of op deze overlijdensdatum liggen? (A9:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A9'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A9 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_trow.overlijdensdatum IS NOT NULL
      THEN
        IF TRUNC(v_spec_verdatum) > TRUNC(v_pers_trow.overlijdensdatum)
        THEN
          add2msg('A9-Decl-persoon zisnr=' || v_pers_trow.zisnr ||
                  '); Verrichtingdatum (' ||
                  to_char(v_spec_verdatum
                         ,'dd-mm-yyyy') ||
                  ' ligt na de overlijdensdatum (' ||
                  to_char(v_pers_trow.overlijdensdatum
                         ,'dd-mm-yyyy') || ')!');
        END IF;
      END IF;
    END IF;

    --*** B1 Is de functie 'Correctie' toegestaan? (B1:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'B1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - B1 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'J'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF p_aantal < 0
      THEN
        add2msg('B1-De functie ''Correctie'' is NIET toegestaan!');
      END IF;
    END IF;

    --*** B2 Mag er een machtiging geprint zijn voor de declaratie? (B2:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'B2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - B2 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'J'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      OPEN c_macht(p_decl_id);
      FETCH c_macht
        INTO r_macht;
      IF c_macht%FOUND
      THEN
        add2msg('B2-Er is reeds een machtiging voor deze declaratie geprint; dit is niet toegestaan!');
      END IF;
      CLOSE c_macht;
    END IF;

    --*** B3 Moet de meting afgerond zijn igv een meting-declaratie? (B3:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'B3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - B3 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF r_decl.entiteit = 'METI'
      THEN
        IF r_meti.afgerond = 'N'
        THEN
          add2msg('B3-De bijbehorende meting is nog niet afgerond; dit is verplicht!');
        END IF;
      END IF;
    END IF;
    -- *** B4 Moet de declaratiepersoon geactiveerd zijn in ROCS? (B4: J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'B4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - B4 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      DECLARE
        v_tmp_pers_trow cg$KGC_PERSONEN.cg$row_type;
        v_tmp_rela_trow cg$KGC_RELATIES.cg$row_type;
        v_tmp_verz_trow cg$KGC_VERZEKERAARS.cg$row_type;
        v_pat_aangemeld BOOLEAN;
        v_ok            BOOLEAN;
        v_tmp_msg       VARCHAR2(2000);
      BEGIN
        v_ok := kgc_zis_nijmegen.persoon_in_zis(p_zisnr         => v_pers_trow.zisnr
                                               ,pers_trow       => v_tmp_pers_trow
                                               ,rela_trow       => v_tmp_rela_trow
                                               ,verz_trow       => v_tmp_verz_trow
--                                               ,x_pat_aangemeld => v_pat_aangemeld
                                               ,x_msg           => v_tmp_msg);

        IF NOT v_ok
        THEN
          -- zou nooit mogen voorkomen!
          add2msg('B4-De declaratiepersoon kon niet opgehaald worden via Patcomp!');
        ELSIF NOT v_pat_aangemeld
        THEN
          add2msg('B4-De declaratiepersoon moet eerst geactiveerd worden in ROCS!');
        END IF;
      END;
    END IF;

    IF v_pers_is_dummy
    THEN
      --*** D1 Moet de declaratiepersoon zonder ZISnummer een geboortedatum hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D1';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D1'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D1 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.geboortedatum IS NULL
        THEN
          add2msg('D1-Persoon(dummie).Geboortedatum ontbreekt!');
        END IF;
      END IF;
      --*** D2 Moet de declaratiepersoon zonder ZISnummer een geboortedatum hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D2';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D2'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D2 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.aanspreken IS NULL
        THEN
          add2msg('D2-Persoon(dummie).Aanspreeknaam ontbreekt!');
        END IF;
      END IF;
      --*** D3 Moet de declaratiepersoon zonder ZISnummer een adres hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D3';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D3'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D3 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.adres IS NULL
        THEN
          add2msg('D3-Persoon(dummie).Adres ontbreekt!');
        END IF;
      END IF;
      --*** D4 Wat is de maximale lengte van het adres van een declaratiepersoon zonder ZISnummer? (-1 betekent geen controle)
      v_debug          := 'D4';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D4'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D4 is afwezig!');
      ELSIF v_ccode_parm_n = -1
      THEN
        NULL; -- geen controle
      ELSE
        IF LENGTH(v_pers_trow.adres) > v_ccode_parm_n
        THEN
          add2msg('D4-Persoon(dummie).Adres mag max ' ||
                  to_char(v_ccode_parm_n) || ' posities lang zijn!');
        END IF;
      END IF;
      --*** D5 Moet de declaratiepersoon zonder ZISnummer een postcode hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D5';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D5'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D5 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.postcode IS NULL
        THEN
          add2msg('D5-Persoon(dummie).Postcode ontbreekt!');
        END IF;
      END IF;
      --*** D6 Moet de declaratiepersoon zonder ZISnummer een woonplaats hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D6';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D6'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D6 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.woonplaats IS NULL
        THEN
          add2msg('D6-Persoon(dummie).Woonplaats ontbreekt!');
        END IF;
      END IF;
      --*** D7 Moet de declaratiepersoon zonder ZISnummer een land hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D7';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D7'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D7 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.land IS NULL
        THEN
          add2msg('D7-Persoon(dummie).Land ontbreekt!');
        END IF;
      END IF;
    END IF;
      --mantis 8288 E2: check op specialisme aanwezig en bevoegdheid om te declareren, niet bij buitenlandse declaraties
      v_debug          := 'E2';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'E2'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - E2 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        if r_decl.dewy_code <> 'BU'
        then
          IF r_rela.spec_code is null
          THEN
            add2msg('E2- Specialisme bij aanvrager ' ||r_rela.rela_code||' ontbreekt!');
          ELSIF r_rela.bevoegd_declareren = 'N'
          THEN
            add2msg('E2- Specialisme bij aanvrager ' ||r_rela.rela_code||' is niet bevoegd om te declareren!');
          END IF;
        end if;
      end if;

    -- ophalen uitvoerende afdeling
    v_uitv_afd := nvl(
      upper(kgc_attr_00.waarde('KGC_ONDERZOEKSGROEPEN', 'DFT_UITVOERENDE_AFDELING', r_decl.ongr_id)),
      upper(kgc_attr_00.waarde('KGC_INSTELLINGEN', 'DFT_UITVOERENDE_AFDELING', r_onde.inst_id))
      );
      -- onderstaande oplossen werkt af en toe niet. zie onderznr JB/14/0125 als voorbeeld.
     /*
    -- rde 11-09-2013: in onderzoeksgroep OBST_TILB zijn 2 uitvoerende afdelingen opgenomen. Adhv de instellings-parameter ALL in de bijbehorende IMLO moet deze afgeleid worden
    -- voor OBST_TILB is de waarde KGPNDTWE of KGPNDSTE. Gecodeerd is dit: TILBTWEEST=KGPNDTWE; TILBELISAB=KGPNDSTE;
    IF INSTR(v_uitv_afd, '=') > 0 THEN -- bepaal bijbehordende instelling via onde_id
      OPEN c_imlo_inst(p_onde_id => r_onde.onde_id);
      FETCH c_imlo_inst INTO r_imlo_inst;
      IF c_imlo_inst%NOTFOUND THEN
        add2msg('Bij onderzoekgroep: ' || r_decl.ongr_code || ' is in de uitvoerende afdeling een verwijzing naar de importlocatie-instelling ALL. Deze werd niet gevonden!');
      ELSE
        DECLARE
          v_inst_id NUMBER := TO_NUMBER(r_imlo_inst.helix_waarde);
          v_inst_code kgc_instellingen.code%TYPE;
          v_pos NUMBER;
        BEGIN
          v_inst_code := kgc_util_00.pk2uk('INST', v_inst_id, 'CODE');
          v_pos := INSTR(v_uitv_afd, v_inst_code || '=');
          IF v_pos = 0 THEN
            add2msg('Bij onderzoekgroep: ' || r_decl.ongr_code || ' werd in de uitvoerende afdeling de instelling ''' || v_inst_code || ''' niet gevonden!');
          ELSE
            v_uitv_afd := SUBSTR(v_uitv_afd, v_pos + LENGTH(v_inst_code || '='));
            v_pos := INSTR(v_uitv_afd, ';');
            IF v_pos = 0 THEN
              add2msg('Bij onderzoekgroep: ' || r_decl.ongr_code || ' werd in de uitvoerende afdeling de instelling ''' || v_inst_code || ''' wel gevonden, maar geen opvolgende '';''!');
            ELSE
              v_uitv_afd := SUBSTR(v_uitv_afd, 1, v_pos - 1);
            END IF;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            add2msg('Bij onderzoekgroep: ' || r_decl.ongr_code || ' is in de uitvoerende afdeling een verwijzing naar de importlocatie-instelling ALL gevonden: ' || r_imlo_inst.helix_waarde || '; deze is niet numeriek!');
        END;
      END IF;
      CLOSE c_imlo_inst;
    END IF;
    */
    IF NVL(LENGTH(v_uitv_afd), 0) = 0
      THEN
          -- verplicht!
       add2msg('Bij onderzoekgroep: ' || r_decl.ongr_code || ' is geen uitvoerende afdeling (attribuut waarde) ingevoerd.');
    else
        -- controleer of deze code genoemd wordt in categorie
        OPEN c_cate2('UITV_AFDL', v_uitv_afd);
        FETCH c_cate2
          INTO r_cate2;
        IF c_cate2%NOTFOUND
        THEN
          add2msg('uitvoerende afdeling:  ''' || v_uitv_afd ||
                  ''' bestaat niet in Helix-categorie UITV_AFDL!');
        END IF;
        CLOSE c_cate2;

    END IF;

    -- Aanvrager gegevens (eerst znr en dan ser_id)
    if r_rela.liszcode is not null then
      v_aanvrager_id := r_rela.liszcode;
      v_assigned_auth := 'UMCN';
      v_name_type := 'PRN';
    else
      v_aanvrager_id :=
        kgc_attr_00.waarde('KGC_RELATIES', 'SER_ID', r_rela.rela_id);
      if v_aanvrager_id is not null then
        v_assigned_auth := 'UMCN';
        v_name_type := 'INTERNALPRN';
      end if;
    end if;

    IF NVL(LENGTH(v_aanvrager_id), 0) = 0
      THEN
    -- verplicht!
          add2msg('Relatie: ' || r_rela.aanspreken || ' is niet gekoppeld met een EPIC relatie.');
    END IF;

    -- bepalen van declaratie korting
    -- ophalen korting percentage in onderzoek nivo
    v_korting_atwa := kgc_attr_00.waarde('KGC_ONDERZOEKEN', 'DFT_KORTING', r_onde.onde_id);

    -- geen korting op onderzoek nivo? checken op instelling nivo
    -- zo ziet de waard eruit:
    -- korting percentage;gedigheidsdatum vanaf;gedigheidsdatum t/m
    -- bijv: 40;01-01-2013;01-01-2014
    IF NVL(LENGTH(v_korting_atwa), 0) = 0 then
      v_korting_atwa := kgc_attr_00.waarde('KGC_INSTELLINGEN', 'DFT_KORTING', r_rela.inst_id);

      IF NVL(LENGTH(v_korting_atwa), 0) > 0 then
        v_korting_perc := substr(v_korting_atwa, 0, instr(v_korting_atwa, ';', 1, 1)-1);
        v_korting_datum_vanaf := substr(v_korting_atwa, instr(v_korting_atwa, ';', 1, 1)+1, instr(v_korting_atwa, ';', 1, 2) - instr(v_korting_atwa, ';', 1, 1)-1 );
        v_korting_datum_tm := substr(v_korting_atwa, instr(v_korting_atwa, ';', 1, 2)+1);

        -- zijn de ingevoerde datums goed?
        if( check_datum(v_korting_datum_vanaf) and check_datum(v_korting_datum_tm) ) then
          -- is korting percentage nog geldig?
          if trunc(v_spec_verdatum) >= v_korting_datum_vanaf and trunc(v_spec_verdatum) <= v_korting_datum_tm then
            v_modif_korting := 'KGC' || v_korting_perc;
          else
            v_modif_korting := 'KGC00';
          end if;
        else
          add2msg('Ongeldige geldigheidsdatum bij korting code van instelling:  ''' || r_rela.inst_code || '''.');
        end if;
      else
        v_modif_korting := 'KGC00';
      end if;
    else
      v_modif_korting := 'KGC' || v_korting_atwa;
    end if;

      -- controleer op korting, in categorien voorkomt.
    IF NVL(LENGTH(v_modif_korting)
            ,0) > 0
      THEN
        -- controleer of deze code genoemd wordt in categorie
        OPEN c_cate2('KORTINGEN', v_modif_korting);
        FETCH c_cate2
          INTO r_cate2;
        IF c_cate2%NOTFOUND
        THEN
          add2msg('Korting code:  ''' || v_modif_korting ||
                  ''' bestaat niet in Helix-categorie KORTINGEN!');
        END IF;
        CLOSE c_cate2;
    END IF;

    -- bepalen van de modifiers (voorlopig alleen betaler en korting perc.)
    if (length(v_spec_fez_icode) > 0) then
      v_modif_betaler := 'WDS~' || v_spec_fez_icode;
    else
      v_modif_betaler := 'KGCZV';
    end if;
    -- toevoegen nog een extra modifier item ivm verwijstypering (alleen bij interne declaraties)
    -- zie ook mantisnr 9259
    if r_rela.liszcode is not null then
      if r_rela.spec_code = 'KG' or r_decl.dewy_code = 'WDS' then --klinische geneticus of wds declaratie
        v_modifiers := v_modif_betaler ||'~' || v_modif_korting; -- voor kgcn (klinische geneticus) aanvragers
      else
        v_modifiers := v_modif_betaler ||'~' || v_modif_korting ||'~' || 'SAVW'; -- voor radboud aanvragers
      end if;
    else
      v_modifiers := v_modif_betaler ||'~' || v_modif_korting; -- voor externe aanvragers
    end if;

    --ophalen service provider (uitvoerder)
    -- service provider znr
    IF NVL(LENGTH(r_decl.service_provider), 0) = 0 then
      if r_decl.deen_id is null then -- declaraties buiten declaratie criteria. zie mantisnr: 9267
        open c_deen (r_decl.kafd_id, r_decl.verrichtingcode);
        fetch c_deen into r_deen;
        close c_deen;
        r_decl.service_provider := r_deen.service_provider;
      end if;
    end if;

    IF NVL(LENGTH(r_decl.service_provider), 0) > 0 then
      open c_rela2(r_decl.service_provider);
      fetch c_rela2 into r_rela2;
      if c_rela2%NOTFOUND then
        add2msg('attribuut: Geen relatie gevonden met znr: ' || r_decl.service_provider || ' als DFT_SERVICE_PROVIDER.');
      end if;
      close c_rela2;
    else
      add2msg('attribuut: kgc_decl_entiteiten.DFT_SERVICE_PROVIDER bevat geen waarde.');
    end if;
    -- eind ophalen service provider

    -- *** KLAAR met controleren
    IF NVL(LENGTH(v_msg)
          ,0) > 0
    THEN
      -- foutmelding?
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Controle NIET OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
                ,r_decl.datum  -- geen wijziging; Mantis 3866
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    END IF;

    IF p_check_only
    THEN
      -- klaar
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Controle OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || '!');
      upd_decl(p_decl_id
                ,r_decl.datum  -- geen wijziging; Mantis 3866
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN TRUE;
    END IF;
    -- toegevoegd naar aanleiding van EPIC (mantisnr 8491)
    IF p_aantal = 1 then
      v_trans_type := 'CG'; --verwerken
    elsif p_aantal = -1 then
      v_trans_type := 'VD'; -- terugdraaien
    end if;

    -- bericht samenstellen
    v_debug := '20';
    -- **** MSH
    v_hl7_msg_bron('MSH') := kgc_zis.init_msg('MSH' ,kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('MSH')(1)(1)(1) := '|';
    v_hl7_msg_bron('MSH')(2)(1)(1) := '^~\&';
    v_hl7_msg_bron('MSH')(3)(1)(1) := 'HELIX';
    v_hl7_msg_bron('MSH')(5)(1)(1) := 'EPIC'; -- toegevoegd: mantisnr 8491
    v_hl7_msg_bron('MSH')(6)(1)(1) := 'NCA'; -- toegevoegd: mantisnr 8491
    v_hl7_msg_bron('MSH')(7)(1)(1) := v_sysdate;
    v_hl7_msg_bron('MSH')(9)(1)(1) := 'DFT^P03';
    v_hl7_msg_bron('MSH')(10)(1)(1) := v_msg_control_id;
    v_hl7_msg_bron('MSH')(11)(1)(1) := upper(kgc_sypa_00.systeem_waarde('BEH_ZIS_HL7_MSH11_PROC_ID')); --mantisnr 8491
    v_hl7_msg_bron('MSH')(12)(1)(1) := kgc_zis.g_sypa_hl7_versie;
    v_hl7_msg_bron('MSH')(15)(1)(1) := ''; -- mantisnr 8491
    v_hl7_msg_bron('MSH')(16)(1)(1) := ''; -- mantisnr 8491
    v_hl7_msg_bron('MSH')(17)(1)(1) := 'NL';

    -- **** EVN
    v_debug := v_debug || 'b';
    v_hl7_msg_bron('EVN') := kgc_zis.init_msg('EVN', kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('EVN')(1)(1)(1) := 'P03';
    v_hl7_msg_bron('EVN')(2)(1)(1) := v_sysdate;

    -- **** PID
    v_debug := v_debug || 'c';
    v_hl7_msg_bron('PID') := kgc_zis.init_msg('PID', kgc_zis.g_sypa_hl7_versie);
    v_debug := v_debug || 'e';
    v_hl7_msg_bron('PID')(3)(1)(1) := v_pers_trow.zisnr;
    v_hl7_msg_bron('PID')(5)(1)(1) := v_pers_trow.achternaam;
    v_hl7_msg_bron('PID')(7)(1)(1) := to_char(v_pers_trow.geboortedatum, 'yyyymmdd');
    IF v_pers_trow.geslacht = 'M' THEN -- vertaal geslacht naar engels
      v_hl7_msg_bron('PID')(8)(1)(1) := 'M';
    ELSIF v_pers_trow.geslacht = 'V' THEN
      v_hl7_msg_bron('PID')(8)(1)(1) := 'F';
    ELSE
      v_hl7_msg_bron('PID')(8)(1)(1) := 'U';
    END IF;

    -- **** FT1
    v_debug := v_debug || 'f';
    v_hl7_msg_bron('FT1') := kgc_zis.init_msg('FT1', kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('FT1')(1)(1)(1) := '1'; -- volgorde van declaratie
    v_hl7_msg_bron('FT1')(2)(1)(1) := to_char(r_decl.decl_id); -- declaratie-id
    v_hl7_msg_bron('FT1')(4)(1)(1) := to_char(v_spec_verdatum ,'yyyymmdd');
    v_hl7_msg_bron('FT1')(6)(1)(1) := v_trans_type;
    v_hl7_msg_bron('FT1')(7)(1)(1) := v_spec_vercode; -- vic, de verrichtingcode
    v_hl7_msg_bron('FT1')(10)(1)(1) := LTRIM(to_char(p_aantal)); -- 1 of -1; is al gecontroleerd!
    v_hl7_msg_bron('FT1')(13)(1)(1) := v_uitv_afd;

    -- de service provider (aanvrager=FT1-21)
    WHILE v_hl7_msg_bron('FT1') (20) (1).COUNT < 13
    LOOP
      -- aanvullen tot 13
      v_hl7_msg_bron('FT1')(20)(1) .EXTEND;
    END LOOP;
   v_hl7_msg_bron('FT1')(20)(1)(1) := r_decl.service_provider;
   v_hl7_msg_bron('FT1')(20)(1)(2) := r_rela2.achternaam;
   v_hl7_msg_bron('FT1')(20)(1)(9) := 'UMCN';
   v_hl7_msg_bron('FT1')(20)(1)(13) := 'PRN';

    WHILE v_hl7_msg_bron('FT1') (21) (1).COUNT < 13
    LOOP
      -- aanvullen tot 13
      v_hl7_msg_bron('FT1')(21)(1) .EXTEND;
    END LOOP;

   v_hl7_msg_bron('FT1')(21)(1)(1) := v_aanvrager_id;
   v_hl7_msg_bron('FT1')(21)(1)(2) := r_rela.achternaam;
   v_hl7_msg_bron('FT1')(21)(1)(9) := v_assigned_auth;
   v_hl7_msg_bron('FT1')(21)(1)(13) := v_name_type;

   v_hl7_msg_bron('FT1')(26)(1)(1) := v_modifiers;
    -- **** ZFT
    v_debug := v_debug || 'g';
    v_hl7_msg_bron('ZFT') := kgc_zis.init_msg('ZFT', kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('ZFT')(13)(1)(1) := r_onde.referentie; -- externe ref. nr.

    v_debug  := '30';
    v_strDFT := kgc_zis.g_sob_char ||
                kgc_zis.hl7seg2str('MSH'
                                  ,v_hl7_msg_bron('MSH')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('EVN'
                                  ,v_hl7_msg_bron('EVN')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('PID'
                                  ,v_hl7_msg_bron('PID')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('FT1'
                                  ,v_hl7_msg_bron('FT1')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('ZFT'
                                  ,v_hl7_msg_bron('ZFT')) || kgc_zis.g_cr ||
                kgc_zis.g_eob_char || kgc_zis.g_term_char;

    v_ok := kgc_dft_interface.send_msg(p_strHL7in  => v_strDFT
                                      ,p_timeout   => 150 -- 15 seconden
                                      ,x_strHL7out => v_strAntw
                                      ,x_msg       => v_msg);
    IF NOT v_ok
    THEN
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Verwerking NIET OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.datum  -- geen wijziging; Mantis 3866
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
      -- mantisnr 8491
    ELSE
      -- mantisnr 9796
      insert into kgc_hl7_berichten
      (bericht, bericht_type, entiteit, id)
      values
      (v_strDFT, 'DFT_P03', 'DECL', r_decl.decl_id)
      returning hlbe_id into v_hlbe_id
      ;

      update kgc_hl7_berichten
      set opmerkingen = ltrim(to_char(p_aantal))
      where hlbe_id = v_hlbe_id
      ;

      END IF;

    IF kgc_dft_interface.test_hl7
    THEN
      -- geen retourbericht
      -- verwerking OK - declaratie bijwerken!
      IF p_aantal > 0
      THEN
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Verwerkt op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ' door ' ||
                              kgc_mede_00.medewerker_code || '; Status:' ||
                              r_decl.status || '->V.');
        upd_decl(p_decl_id
                ,v_spec_verdatum  -- WEL eventueel een wijziging; Mantis 3866
                ,'V'
                ,trunc(SYSDATE)
                ,r_decl.verwerking);
      ELSE
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Teruggedraaid op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ' door ' ||
                              kgc_mede_00.medewerker_code || '; Status:' ||
                              r_decl.status || '->R.');
        upd_decl(p_decl_id
                ,r_decl.datum  -- geen wijziging; Mantis 3866
                ,'R'
                ,trunc(SYSDATE)
                ,r_decl.verwerking);
      END IF;
    ELSE
      kgc_zis.log(v_strAntw
                 ,'1100XXXXXX');
      v_hl7_msg_res := kgc_zis.str2hl7msg(0
                                         ,v_strAntw);
      v_MSA         := kgc_zis.getSegmentCode('MSA'
                                             ,v_hl7_msg_res);
      v_MSH         := kgc_zis.getSegmentCode('MSH'
                                             ,v_hl7_msg_res);

      IF NVL(LENGTH(v_MSA)
            ,0) = 0
         OR NVL(LENGTH(v_MSH)
               ,0) = 0
      THEN
        v_msg := 'MSA- of MSH-segment afwezig!';
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Verwerking NIET OK op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                              v_msg);
        upd_decl(p_decl_id
                ,r_decl.datum  -- geen wijziging; Mantis 3866
                ,r_decl.status
                ,r_decl.datum_verwerking
                ,r_decl.verwerking);
        RETURN FALSE;
      END IF;
      -- check op control-id; soms wordt er iets aan vastgeplakt (=niet erg)
      IF SUBSTR(v_hl7_msg_res(v_MSH) (10) (1) (1)
               ,1
               ,LENGTH(v_msg_control_id)) <> v_msg_control_id
      THEN
        -- komt 'nooit' voor
        v_msg := 'Fout bij ophalen gegevens: mismatch control-id! Verzonden: ' ||
                 v_msg_control_id || '; Ontvangen: ' ||
                 v_hl7_msg_res(v_MSH) (10) (1) (1) || '!';
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Verwerking NIET OK op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                              v_msg);
        upd_decl(p_decl_id
                ,r_decl.datum  -- geen wijziging; Mantis 3866
                ,r_decl.status
                ,r_decl.datum_verwerking
                ,r_decl.verwerking);
        RETURN FALSE;
      END IF;
      IF NVL(v_hl7_msg_res(v_MSA) (1) (1) (1)
            ,'XX') NOT IN ('AA'
                          ,'CA')
      THEN
        v_msg := 'MSA-1 = ' || v_hl7_msg_res(v_MSA) (1) (1)
                 (1) || '; Text: ' || v_hl7_msg_res(v_MSA) (3) (1) (1);
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Verwerking NIET OK op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                              v_msg);
        upd_decl(p_decl_id
                ,r_decl.datum  -- geen wijziging; Mantis 3866
                ,r_decl.status
                ,r_decl.datum_verwerking
                ,r_decl.verwerking);
        RETURN FALSE;
      ELSE
        -- verwerking OK - declaratie bijwerken!
        IF p_aantal > 0
        THEN
          kgc_dft_interface.log(r_decl.verwerking
                               ,'Verwerkt op ' ||
                                to_char(SYSDATE
                                       ,'dd-mm-yyyy hh24:mi:ss') ||
                                ' door ' || kgc_mede_00.medewerker_code ||
                                '; Status:' || r_decl.status || '->V.');
          upd_decl(p_decl_id
                  ,v_spec_verdatum  -- WEL eventueel een wijziging; Mantis 3866
                  ,'V'
                  ,trunc(SYSDATE)
                  ,r_decl.verwerking);
        ELSE
          kgc_dft_interface.log(r_decl.verwerking
                               ,'Teruggedraaid op ' ||
                                to_char(SYSDATE
                                       ,'dd-mm-yyyy hh24:mi:ss') ||
                                ' door ' || kgc_mede_00.medewerker_code ||
                                '; Status:' || r_decl.status || '->R.');
          upd_decl(p_decl_id
                  ,r_decl.datum  -- geen wijziging; Mantis 3866
                  ,'R'
                  ,trunc(SYSDATE)
                  ,r_decl.verwerking);
        END IF;
      END IF;
    END IF;
    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      v_msg := v_thisproc || ': Error: ' || SQLERRM;
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Error op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.datum  -- geen wijziging; Mantis 3866
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
  END check_verstuur_dft_p03;

  /* globaal ******************/
  PROCEDURE open_verbinding IS
  BEGIN
    NULL;
  END open_verbinding;

  PROCEDURE sluit_verbinding IS
  BEGIN
    NULL;
  END sluit_verbinding;

  PROCEDURE controleer(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => 1
                                  ,p_check_only => TRUE);
  END controleer;

  PROCEDURE verwerk(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => 1
                                  ,p_check_only => FALSE);
  END verwerk;

  PROCEDURE draai_terug(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => -1
                                  ,p_check_only => FALSE);
  END draai_terug;

  PROCEDURE test_bepaal_controle IS
    v_tab_controles  t_tab_controles;
    v_ccode_aanwezig BOOLEAN;
    v_ccode_parm_s   VARCHAR2(4000);
    v_ccode_parm_n   NUMBER;
  BEGIN
    dbms_output.put_line('Start test_bepaal_controle...');
    dbms_output.put_line('Test aanwezig zonder parameter - begin, midden, eind, afwezig');
    v_tab_controles(1) := 'A1,A2,A3';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test begin OK');
    ELSE
      dbms_output.put_line('Test begin FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test midden OK');
    ELSE
      dbms_output.put_line('Test midden FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test eind OK');
    ELSE
      dbms_output.put_line('Test eind FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test afwezig OK');
    ELSE
      dbms_output.put_line('Test afwezig FOUT');
    END IF;
    --
    dbms_output.put_line('Test aanwezig met parameter - begin, midden1, midden2, eind, afwezig');
    v_tab_controles(1) := 'A1:-1,A2:TEST,A3:-2,A4:9999999';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test begin OK');
      IF v_ccode_parm_n = -1
         AND v_ccode_parm_s = '-1'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test begin FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test midden1 OK');
      IF v_ccode_parm_n IS NULL
         AND v_ccode_parm_s = 'TEST'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test midden1 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test midden2 OK');
      IF v_ccode_parm_n = -2
         AND v_ccode_parm_s = '-2'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test midden2 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test eind OK');
      IF v_ccode_parm_n = 9999999
         AND v_ccode_parm_s = '9999999'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test eind FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A5'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test afwezig OK');
    ELSE
      dbms_output.put_line('Test afwezig FOUT');
    END IF;
    --
    dbms_output.put_line('Test nesting - level3, level2, level1, afwezig');
    v_tab_controles(1) := 'A1:-1,A2:TEST1,A3:-2';
    v_tab_controles(2) := 'A1:-2,A2:TEST2';
    v_tab_controles(3) := 'A1:-3';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test level3 OK');
      IF v_ccode_parm_n = -3
         AND v_ccode_parm_s = '-3'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test level3 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test level2 OK');
      IF v_ccode_parm_n IS NULL
         AND v_ccode_parm_s = 'TEST2'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test level2 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test level1 OK');
      IF v_ccode_parm_n = -2
         AND v_ccode_parm_s = '-2'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test level1 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test afwezig OK');
    ELSE
      dbms_output.put_line('Test afwezig FOUT');
    END IF;
    dbms_output.put_line('Eind test_bepaal_controle...');
  END;
END kgc_dft_nijmegen;
/

/
QUIT
