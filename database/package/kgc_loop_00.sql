CREATE OR REPLACE PACKAGE "HELIX"."KGC_LOOP_00" IS
-- PL/SQL Specification
-- wordt mogelijk bepaald via indicatie
FUNCTION geplande_einddatum
( p_datum IN DATE
, p_indi_id IN NUMBER := NULL
, p_onderzoekswijze IN VARCHAR2
)
RETURN DATE;
PRAGMA RESTRICT_REFERENCES( geplande_einddatum, WNDS, WNPS, RNPS );

FUNCTION signaleringsdatum
( p_datum IN DATE
, p_indi_id IN NUMBER := NULL
, p_onderzoekswijze IN VARCHAR2
)
RETURN DATE;
PRAGMA RESTRICT_REFERENCES( signaleringsdatum, WNDS, WNPS, RNPS );


END KGC_LOOP_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_LOOP_00" IS
FUNCTION geplande_einddatum
( p_datum IN DATE
, p_indi_id IN NUMBER := NULL
, p_onderzoekswijze IN VARCHAR2
)
RETURN DATE
IS
  v_return DATE;
  CURSOR loop_cur
  IS
    SELECT doorlooptijd
    FROM   kgc_looptijden
    WHERE  indi_id = p_indi_id
    AND  ( onderzoekswijze = p_onderzoekswijze
        OR onderzoekswijze IS NULL
         )
    ORDER BY onderzoekswijze ASC -- lege laatst
    ;
  v_dagen NUMBER;
-- PL/SQL Block
BEGIN
  IF ( p_datum IS NOT NULL )
  THEN
    IF ( p_indi_id IS NOT NULL )
    THEN
      OPEN  loop_cur;
      FETCH loop_cur
      INTO  v_dagen;
      CLOSE loop_cur;
      v_return := p_datum + v_dagen; -- kan en mag null zijn
    END IF;
  END IF;
  RETURN( v_return );
END geplande_einddatum;

FUNCTION signaleringsdatum
( p_datum IN DATE
, p_indi_id IN NUMBER := NULL
, p_onderzoekswijze IN VARCHAR2
)
RETURN DATE
IS
  v_return DATE;
  CURSOR loop_cur
  IS
    SELECT signaleren_na
    FROM   kgc_looptijden
    WHERE  indi_id = p_indi_id
    AND  ( onderzoekswijze = p_onderzoekswijze
        OR onderzoekswijze IS NULL
         )
    ORDER BY onderzoekswijze ASC -- lege laatst
    ;
  v_dagen NUMBER;
BEGIN
  IF ( p_datum IS NOT NULL )
  THEN
    IF ( p_indi_id IS NOT NULL )
    THEN
      OPEN  loop_cur;
      FETCH loop_cur
      INTO  v_dagen;
      CLOSE loop_cur;
      v_return := p_datum + v_dagen; -- kan en mag null zijn
    END IF;
  END IF;
  RETURN( v_return );
END signaleringsdatum;

END kgc_loop_00;
/

/
QUIT
