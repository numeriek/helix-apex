CREATE OR REPLACE PACKAGE "HELIX"."BAS_MDOK_00" IS
 -- PL/SQL Specification
-- voldoet de waarde van een meetwaarde_detail aan de succes-voorwaarde?
FUNCTION  mdet_ok
 (  p_mdet_id  IN  NUMBER
 )
 RETURN  VARCHAR2;  -- J/N
-- overloaded
FUNCTION  mdet_ok
 (  p_meet_id  IN  NUMBER
 ,  p_volgorde  IN  NUMBER
 )
 RETURN  VARCHAR2;  -- J/N
PRAGMA  RESTRICT_REFERENCES(    mdet_ok,    WNDS,    WNPS    );
 -- voldoen alle detail-waarden van een meetwaarde aan de succes-voorwaarde?
FUNCTION  meet_ok
 (  p_meet_id  IN  NUMBER
 )
 RETURN  VARCHAR2;  -- J/N
PRAGMA  RESTRICT_REFERENCES(    meet_ok,    WNDS,    WNPS    );
 -- bepaal of waarde al dan niet ok is...
FUNCTION   meetwaarde_detail_ok
 (  p_meet_id  IN  NUMBER
 ,  p_volgorde  IN  NUMBER
 ,  p_mwst_id  IN  OUT  NUMBER
 ,  p_waarde  IN  VARCHAR2
 )
 RETURN  VARCHAR2;
 -- onderhoud de tabel bas_mdet_ok.
FUNCTION  zet_mdet_ok
 (  p_aktie  IN  VARCHAR2  -- I, U, D
,  p_waarde  IN  VARCHAR2
,  p_mdet_id  IN  NUMBER  :=  NULL
 ,  p_meet_id  IN  NUMBER  :=  NULL
 ,  p_volgorde  IN  NUMBER  :=  NULL
 ,  p_mwst_id  IN  NUMBER  :=  NULL
 )
 RETURN  VARCHAR2;  -- J/N
-- overloaded
PROCEDURE  zet_mdet_ok
 (  p_aktie  IN  VARCHAR2  -- I, U, D
,  p_waarde  IN  VARCHAR2
,  p_mdet_id  IN  NUMBER  :=  NULL
 ,  p_meet_id  IN  NUMBER  :=  NULL
 ,  p_volgorde  IN  NUMBER  :=  NULL
 ,  p_mwst_id  IN  NUMBER  :=  NULL
 );


END  BAS_MDOK_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_MDOK_00" IS
FUNCTION mdet_ok
( p_mdet_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'J';
  CURSOR mdok_cur
  IS
    SELECT ok
    FROM   bas_mdet_ok
    WHERE  mdet_id = p_mdet_id
    ;
BEGIN
  OPEN  mdok_cur;
  FETCH mdok_cur
  INTO  v_return;
  CLOSE  mdok_cur;
  RETURN( NVL( v_return, 'J' ) );
END mdet_ok;
-- overloaded
FUNCTION  mdet_ok
 (  p_meet_id  IN  NUMBER
 ,  p_volgorde  IN  NUMBER
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(1) := 'J';
  CURSOR mdok_cur
  IS
    SELECT ok
    FROM   bas_mdet_ok
    WHERE  meet_id = p_meet_id
    AND    volgorde = p_volgorde
    ;
 BEGIN
  OPEN  mdok_cur;
  FETCH mdok_cur
  INTO  v_return;
  CLOSE  mdok_cur;
  RETURN( NVL( v_return, 'J' ) );
END mdet_ok;

-- voldoen alle detail-waarden van een meetwaarde aan de succes-voorwaarde?
FUNCTION  meet_ok
 (  p_meet_id  IN  NUMBER
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(1) := 'J';
  CURSOR mdok_cur
  IS
    SELECT NULL
    FROM   bas_mdet_ok mdok
    ,      bas_meetwaarden meet
    WHERE  meet.meet_id = p_meet_id
    AND    meet.afgerond = 'J'
    AND    mdok.meet_id = p_meet_id
    AND    mdok.ok = 'N'
    ;
  v_dummy VARCHAR2(1);
 BEGIN
  OPEN  mdok_cur;
  FETCH mdok_cur
  INTO  v_return;
  IF ( mdok_cur%found )
  THEN
    v_return := 'N'; -- meetwaarde afgerond en een detail niet-ok
  ELSE
    v_return := 'J';
  END IF;
  CLOSE  mdok_cur;
  RETURN( NVL( v_return, 'J' ) );
END meet_ok;

FUNCTION   meetwaarde_detail_ok
 (  p_meet_id  IN  NUMBER
 ,  p_volgorde  IN  NUMBER
 ,  p_mwst_id  IN  OUT  NUMBER
 ,  p_waarde  IN  VARCHAR2
 )
 RETURN  VARCHAR2
 IS
  v_return VARCHAR2(1);
  CURSOR meet_cur
  IS
    SELECT mons.kafd_id
    ,      mons.ongr_id
    ,      meet.mwst_id
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    meet.meet_id = p_meet_id
    UNION
    SELECT ongr.kafd_id
    ,      stan.ongr_id
    ,      meet.mwst_id
    FROM   kgc_onderzoeksgroepen ongr
    ,      bas_standaardfracties stan
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.stan_id = stan.stan_id
    AND    stan.ongr_id = ongr.ongr_id
    AND    meet.meet_id = p_meet_id
    ;
  meet_rec meet_cur%rowtype;

  CURSOR mwss_cur
  ( b_mwst_id IN NUMBER
  )
  IS
    SELECT succes_voorwaarde
    ,      vervolgactie
    FROM   kgc_mwst_samenstelling
    WHERE  mwst_id = b_mwst_id
    AND    volgorde = p_volgorde
    ;
  mwss_rec mwss_cur%rowtype;
  v_statement VARCHAR2(1000);
  v_dummy VARCHAR2(1) := NULL;
 BEGIN
  IF ( p_mwst_id IS NULL )
  THEN
    OPEN  meet_cur;
    FETCH meet_cur
    INTO  meet_rec;
    CLOSE  meet_cur;
    IF ( meet_rec.mwst_id IS NULL )
    THEN
      -- bepaal alsnog mwst:
      meet_rec.mwst_id := kgc_mwst_00.meetwaarde_structuur
                          ( p_kafd_id => meet_rec.kafd_id
                          , p_ongr_id => meet_rec.ongr_id
                          , p_meet_id => p_meet_id
                       );
    END IF;
    -- geef gevonden waarde terug
    p_mwst_id := meet_rec.mwst_id;
  ELSE
    meet_rec.mwst_id := p_mwst_id;
  END IF;

  OPEN  mwss_cur( meet_rec.mwst_id );
  FETCH mwss_cur
  INTO  mwss_rec;
  CLOSE  mwss_cur;
  IF ( mwss_rec.succes_voorwaarde IS NULL )
  THEN
    v_return := NULL;
  ELSE
    -- test voorwaarde (gebruik het resultaat,mdet.ok, niet!)
    mwss_rec.succes_voorwaarde := REPLACE( mwss_rec.succes_voorwaarde, '<waarde>','<WAARDE>' );
    mwss_rec.succes_voorwaarde := REPLACE( mwss_rec.succes_voorwaarde, '<WAARDE>', p_waarde );
    v_statement := 'select ''X'' from dual where '||mwss_rec.succes_voorwaarde;
    BEGIN
      v_dummy := kgc_util_00.dyn_exec( v_statement );
    EXCEPTION
      WHEN OTHERS
      THEN
        v_dummy := NULL; -- mis = fout
    END;
    IF ( v_dummy = 'X' )
    THEN
      v_return := 'J';
    ELSE
      v_return := 'N';
    END IF;
  END IF;
  RETURN( v_return );
END meetwaarde_detail_ok;

FUNCTION  zet_mdet_ok
(  p_aktie  IN  VARCHAR2  -- I, U, D
,  p_waarde  IN  VARCHAR2
,  p_mdet_id  IN  NUMBER  :=  NULL
,  p_meet_id  IN  NUMBER  :=  NULL
,  p_volgorde  IN  NUMBER  :=  NULL
,  p_mwst_id  IN  NUMBER  :=  NULL
)
RETURN  VARCHAR2
IS
  v_return VARCHAR2(1) := 'J';
  mdok_rec bas_mdet_ok%rowtype;
BEGIN
  IF ( p_mdet_id IS NULL
   AND p_meet_id IS NULL
   AND p_volgorde IS NULL
     )
  THEN
    RETURN( NULL ); -- geen foutmelding
    raise_application_error( -20000, 'Parameters zijn niet voldoende doorgegeven aan BAS_MDOK_00.ZET_MDET_OK.'
                          ||CHR(10)||'Neem contact op met de applicatiebeheerder'
                           );
  END IF;

  mdok_rec.mdet_id := p_mdet_id;
  mdok_rec.mwst_id := p_mwst_id;
  mdok_rec.meet_id := p_meet_id;
  mdok_rec.volgorde := p_volgorde;

  -- voorbereidingen voor invoer en update van meetwaarde-detail
  IF ( p_aktie IN ( 'I', 'U' ) )
  THEN
    -- bepaal ok/niet-ok
    mdok_rec.ok := meetwaarde_detail_ok
                   ( p_meet_id => mdok_rec.meet_id
                   , p_volgorde => mdok_rec.volgorde
                   , p_mwst_id => mdok_rec.mwst_id
                   , p_waarde => p_waarde
                   );
  ELSIF ( p_aktie = 'D' )
  THEN
    NULL;
  ELSE
    raise_application_error( -20000, 'Aktie "'||p_aktie||'" is onbekend voor BAS_MDET_00.ZET_MDET_OK.'
                          ||CHR(10)||'Neem contact op met de applicatiebeheerder'
                           );
  END IF;

  IF ( p_aktie = 'I' )
  THEN
    if ( mdok_rec.mwst_id is not null ) -- mwst kan ook via systeemparameter toegekend zijn
    then
      BEGIN
        INSERT INTO bas_mdet_ok
        ( mdet_id
        , mwst_id
        , meet_id
        , volgorde
        , ok
        )
        VALUES
        ( mdok_rec.mdet_id
        , mdok_rec.mwst_id
        , mdok_rec.meet_id
        , mdok_rec.volgorde
        , NVL(mdok_rec.ok,'J')
        );
      EXCEPTION
        WHEN dup_val_on_index
        THEN
          BEGIN
            UPDATE bas_mdet_ok
            SET    ok = NVL(mdok_rec.ok,'J')
            WHERE  mdet_id = mdok_rec.mdet_id
            ;
          END;
      END;
      v_return := mdok_rec.ok;
    end if;
  ELSIF ( p_aktie = 'U' )
  THEN
    BEGIN
      UPDATE bas_mdet_ok
      SET    ok = NVL(mdok_rec.ok, ok) -- als leeg, dan niet wijzigen
      WHERE  mdet_id = mdok_rec.mdet_id
      ;
      IF ( sql%rowcount = 0 )
      THEN -- insert dan maar...
        if ( mdok_rec.mwst_id is not null ) -- mwst kan ook via systeemparameter toegekend zijn
        then
          BEGIN
            INSERT INTO bas_mdet_ok
            ( mdet_id
            , mwst_id
            , meet_id
            , volgorde
            , ok
            )
            VALUES
            ( mdok_rec.mdet_id
            , mdok_rec.mwst_id
            , mdok_rec.meet_id
            , mdok_rec.volgorde
            , NVL(mdok_rec.ok,'J')
            );
          EXCEPTION
            WHEN dup_val_on_index
            THEN
              -- tikkie overdreven, maar ja...
              BEGIN
                UPDATE bas_mdet_ok
                SET    ok = NVL(mdok_rec.ok,ok)
                WHERE  meet_id = mdok_rec.meet_id
                AND    volgorde = mdok_rec.volgorde
                ;
              END;
          END;
        end if;
      END IF;
    END;
    v_return := mdok_rec.ok;
  ELSIF ( p_aktie = 'D' )
  THEN
    BEGIN
      DELETE FROM bas_mdet_ok
      WHERE  mdet_id = mdok_rec.mdet_id
      ;
    END;
    v_return := NULL;
  END IF;
  RETURN( NVL( v_return, 'J' ) );
END zet_mdet_ok;

-- overloaded
PROCEDURE  zet_mdet_ok
 (  p_aktie  IN  VARCHAR2  -- I, U, D
,  p_waarde  IN  VARCHAR2
 ,  p_mdet_id  IN  NUMBER  :=  NULL
 ,  p_meet_id  IN  NUMBER  :=  NULL
 ,  p_volgorde  IN  NUMBER  :=  NULL
 ,  p_mwst_id  IN  NUMBER  :=  NULL
 )
 IS
  v_dummy VARCHAR2(1);
 BEGIN
  v_dummy := zet_mdet_ok
             ( p_aktie => p_aktie
             , p_waarde => p_waarde
             , p_mdet_id => p_mdet_id
             , p_meet_id => p_meet_id
             , p_volgorde => p_volgorde
             , p_mwst_id => p_mwst_id
             );
END zet_mdet_ok;
END  bas_mdok_00;
/

/
QUIT
