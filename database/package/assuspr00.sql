CREATE OR REPLACE PACKAGE "HELIX"."ASSUSPR00" IS
-- Package Specification
--
  PROCEDURE init_uspr;
  PROCEDURE dflt_row
  ( p_user  IN  VARCHAR2
  );
  FUNCTION required_item_prompt RETURN VARCHAR2;
  FUNCTION default_item_prompt  RETURN VARCHAR2;
  PROCEDURE ins_uvap
  ( p_user_name VARCHAR2
  , p_va_name VARCHAR2
  , p_va_foreground VARCHAR2
  , p_va_background VARCHAR2
  );
END ASSUSPR00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."ASSUSPR00" IS

  g_uspr_rec  assai_user_preferences%ROWTYPE;

  --  ===============================================================================
  --  L O C A L   P R O C E D U R E S
  --
  PROCEDURE own_settings
  IS
    CURSOR uspr_cur IS
          SELECT uspr.*
          FROM   assai_user_preferences  uspr
          WHERE  user_name = USER
          ;
  BEGIN
    OPEN  uspr_cur;
        FETCH uspr_cur
        INTO  g_uspr_rec;
        IF uspr_cur%NOTFOUND
        THEN
      dflt_row
      ( p_user => USER
      );
          own_settings;
        END IF;
        IF uspr_cur%NOTFOUND
        THEN
      CLOSE uspr_cur;
        END IF;
  END;

  --  ===============================================================================
  --  P U B L I C   P R O C E D U R E S
  --
  PROCEDURE init_uspr IS
    --
  BEGIN
    BEGIN
      INSERT INTO assai_user_preferences (
        user_name
      )
      VALUES (
        'DEFAULT'
      );
      COMMIT;
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN NULL;
      WHEN OTHERS           THEN RAISE;
    END;
    assuspr00.ins_uvap( 'DEFAULT', 'QMS$BLACK_ON_RED', 'r0g51b51', 'r250g0b0');
    assuspr00.ins_uvap( 'DEFAULT', 'QMS$BLACK_ON_GREEN', 'r0g51b51', 'r0g250b0');
    assuspr00.ins_uvap( 'DEFAULT', 'QMS$WHITE_ON_BLACK', 'r251g251b251', 'r0g51b51');
    assuspr00.ins_uvap( 'DEFAULT', 'QMS$BLACK_ON_YELLOW', 'r0g51b51', 'r250g250b0');
    assuspr00.ins_uvap( 'DEFAULT', 'QMS$BLACK_ON_CYAN', 'r0g51b51', 'r0g255b255');
    assuspr00.ins_uvap( 'DEFAULT', 'QMS$BLACK_ON_GRAY', 'r0g51b51', 'r200g200b200');
  END init_uspr;

  PROCEDURE dflt_row
  ( p_user  IN  VARCHAR2
  )
  IS
    CURSOR uspr_user_cur IS
      SELECT uspr.*
      FROM   assai_user_preferences  uspr
      WHERE  uspr.user_name = p_user;
    uspr_user_rec  uspr_user_cur%ROWTYPE;
    --
    CURSOR uspr_dflt_cur IS
      SELECT uspr.*
      FROM   assai_user_preferences  uspr
      WHERE  uspr.user_name = 'DEFAULT';
    uspr_dflt_rec  uspr_dflt_cur%ROWTYPE;
    --
    CURSOR uvap_dflt_cur IS
      SELECT uvap.*
      FROM   assai_user_va_preferences  uvap
      WHERE  uvap.user_name = 'DEFAULT';
    --
  BEGIN
    OPEN  uspr_user_cur;
    FETCH uspr_user_cur
    INTO  uspr_user_rec;
    IF uspr_user_cur%NOTFOUND
    THEN
      OPEN  uspr_dflt_cur;
      FETCH uspr_dflt_cur
      INTO  uspr_dflt_rec;
      CLOSE uspr_dflt_cur;
          BEGIN
      INSERT INTO assai_user_preferences (
        user_name
      , menu_call_method
      , enable_cs_ind
      , raise_find_window
      , highlight_qi_ind
      , highlight_ri_ind
      , default_item_background
      , default_item_foreground
      , default_item_prompt
      , required_item_background
      , required_item_foreground
      , required_item_prompt
      , readonly_item_background
      , readonly_item_foreground
      , queryable_item_background
      , queryable_item_foreground
      , selected_record_background
      , selected_record_foreground
      , current_record_background
      , current_record_foreground
      , button_background
      , button_foreground
      , canvas
      , tab_canvas
      , prompts
      , language
      , date_format
      )
      VALUES (
        p_user
      , uspr_dflt_rec.menu_call_method
      , uspr_dflt_rec.enable_cs_ind
      , uspr_dflt_rec.raise_find_window
      , uspr_dflt_rec.highlight_qi_ind
      , uspr_dflt_rec.highlight_ri_ind
      , uspr_dflt_rec.default_item_background
      , uspr_dflt_rec.default_item_foreground
      , uspr_dflt_rec.default_item_prompt
      , uspr_dflt_rec.required_item_background
      , uspr_dflt_rec.required_item_foreground
      , uspr_dflt_rec.required_item_prompt
      , uspr_dflt_rec.readonly_item_background
      , uspr_dflt_rec.readonly_item_foreground
      , uspr_dflt_rec.queryable_item_background
      , uspr_dflt_rec.queryable_item_foreground
      , uspr_dflt_rec.selected_record_background
      , uspr_dflt_rec.selected_record_foreground
      , uspr_dflt_rec.current_record_background
      , uspr_dflt_rec.current_record_foreground
      , uspr_dflt_rec.button_background
      , uspr_dflt_rec.button_foreground
      , uspr_dflt_rec.canvas
      , uspr_dflt_rec.tab_canvas
      , uspr_dflt_rec.prompts
      , uspr_dflt_rec.language
      , uspr_dflt_rec.date_format
      );
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN NULL;
            WHEN OTHERS           THEN RAISE;
          END;
          -- voeg de accentkleuren toe:
          FOR uvap_dflt_rec in uvap_dflt_cur
          LOOP
            assuspr00.ins_uvap
                ( p_user_name => p_user
                , p_va_name   => uvap_dflt_rec.va_name
        , p_va_foreground => uvap_dflt_rec.va_foreground
        , p_va_background => uvap_dflt_rec.va_background );
          END LOOP;
      COMMIT;
    END IF;
    IF uspr_user_cur%ISOPEN
    THEN
      CLOSE uspr_user_cur;
    END IF;
  END;

  FUNCTION required_item_prompt
  RETURN VARCHAR2
  IS
  BEGIN
    own_settings;
        RETURN g_uspr_rec.required_item_prompt;
  END;

  FUNCTION default_item_prompt
  RETURN VARCHAR2
  IS
  BEGIN
    own_settings;
        RETURN g_uspr_rec.default_item_prompt;
  END;
 --
  PROCEDURE ins_uvap
  ( p_user_name VARCHAR2
  , p_va_name VARCHAR2
  , p_va_foreground VARCHAR2
  , p_va_background VARCHAR2
  )
  IS
  BEGIN
    INSERT INTO ASSAI_USER_VA_PREFERENCES
          ( USER_NAME
      , VA_NAME
      , VA_FOREGROUND
      , VA_BACKGROUND )
    VALUES
          ( p_user_name
      , p_va_name
      , p_va_foreground
      , p_va_background );
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN NULL;
    WHEN OTHERS           THEN RAISE;
  END;
  --
END  ASSUSPR00;
/

/
QUIT
