CREATE OR REPLACE PACKAGE "HELIX"."KGC_DIDO_00"
IS
  FUNCTION file_merger(
      p_best_ids    IN VARCHAR2,
      p_file_type    IN VARCHAR2 DEFAULT NULL,
      p_footer_tekst IN VARCHAR2 DEFAULT NULL )
    RETURN VARCHAR2;
END kgc_dido_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DIDO_00"
IS
  FUNCTION file_merger(
      p_best_ids     IN VARCHAR2,
      p_file_type    IN VARCHAR2 DEFAULT NULL,
      p_footer_tekst IN VARCHAR2 DEFAULT NULL )
    RETURN VARCHAR2
  AS

    cursor c_best (b_best_id in number)
    is
    select bestand_specificatie from kgc_bestanden
    where best_id = b_best_id
    ;
    e_te_veel_bestanden EXCEPTION;
    v_post_params     VARCHAR2(32000);
    v_file_merger_url CONSTANT VARCHAR2(500)  := kgc_sypa_00.systeem_waarde ('HELIX_WEBSERVICE_URL') || 'filemerger/index.php';
    v_request utl_http.req;
    v_post_text VARCHAR2(32000);
    v_response utl_http.resp;
    v_response_text VARCHAR2(4000);
    v_return        VARCHAR2(4000);

    v_element varchar2(4000);
    v_bestand_specificatie varchar2(4000);
    v_bestanden varchar2(4000);
    v_dir_files varchar2(32000);

BEGIN

  v_bestanden := p_best_ids;

  while v_bestanden is not null loop
    v_element := kgc_string.nxt_elem_in_list(p_list => v_bestanden
	                                         , x_remainder  => v_bestanden);
    open  c_best(b_best_id => v_element);
    fetch c_best into v_bestand_specificatie;
    close c_best;

    if v_dir_files is null then
      v_dir_files := v_bestand_specificatie;
    else
      if length(v_dir_files ||'|'||v_bestand_specificatie) > 31500 then
        RAISE e_te_veel_bestanden;
      else
        v_dir_files := v_dir_files ||'|'||v_bestand_specificatie;
      end if;
    end if;
  end loop;

  v_post_params := 'dirFiles=' || v_dir_files || '&fileType=' || p_file_type || '&footerText=' ||p_footer_tekst;

  --send_mail_html( p_from => 'hamid.bourchi@radboudumc.nl', p_to => 'hamid.bourchi@radboudumc.nl', p_subject => 'debug', p_html =>  p_dir_files);
    ----------------------------------------------------------------------------
    -- Opbouw van http opdracht
    ----------------------------------------------------------------------------
    v_post_text := utl_url.escape(v_post_params, true);
    ----------------------------------------------------------------------------
    -- Verzenden van http opdracht.
    ----------------------------------------------------------------------------
    utl_http.set_transfer_timeout(120);
    v_request := utl_http.begin_request ( url => v_file_merger_url , method => 'POST' );
    utl_http.set_header ( r => v_request , name => 'Content-Type' , value => 'application/x-www-form-urlencoded' );
    utl_http.set_header ( r => v_request , name => 'Content-Length' , value => LENGTH(v_post_text) );
    utl_http.write_text ( r => v_request , data => v_post_text );
    v_response := utl_http.get_response(v_request);
    -- code 200 betekent http opdracht is goed uitgevoerd
    IF v_response.status_code = '200' THEN
      utl_http.read_text(v_response, v_response_text);
      v_return := v_response_text;
    ELSE
      v_return := 'ERROR: HTTP status: '||v_response.status_code||'-'||v_response.reason_phrase;
    END IF;
    utl_http.end_response(v_response);
    RETURN v_return;
  EXCEPTION
  WHEN e_te_veel_bestanden THEN
    v_return := 'ERROR: te veel bestanden geselecteerd.';
    RETURN v_return;
  WHEN OTHERS THEN
    v_return := 'ERROR: ' || SUBSTR(sqlerrm, 1, 4000);
    RETURN v_return;
  END file_merger;
END kgc_dido_00;
/

/
QUIT
