CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPVZ_00" IS
-- Geeft de identificatie uit de medewerkerstabel van Helix
PROCEDURE geef_helix_medewerker
( p_vms_username  IN  VARCHAR2
, x_mede_id       OUT VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=medewerker gevonden
  -- Verwerking afgebroken:
  --    OM=Onbekende Medewerker
  --    NU=Niet Unieke medewerker
  --    OF=Oracle Foutsituatie
);

-- Controleer de autorisatie van de medewerker via de database-rollen
PROCEDURE controleer_autorisatie
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Controle uitgevoerd:
  --    OK=autorisatie
  --    GA=Geen Autorisatie
  --    OF=Oracle Foutsituatie
);

-- Stel de medewerker in het ontvangende systeem, open voor opstartverzoeken vanuit het verzendende systeem
PROCEDURE open_koppeling
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=koppeling geopend
  --    KH=Koppeling Heropend
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GA=Geen Autorisatie
  --    OF=Oracle Foutsituatie
);

-- Kijk of er voor de medewerker in het ontvangende systeem, een opstartverzoek door het verzendende systeem is klaargezet
PROCEDURE opstart_verzoek_aanwezig
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=verzoek aanwezig
  --    GV=Geen Verzoeken
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GK=Geen Koppeling
  --    OF=Oracle Foutsituatie
);

-- Lees voor de medewerker in het ontvangende systeem, 1 opstartverzoek dat is klaargezet door het verzendende systeem
PROCEDURE ontvang_opstart_verzoek
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_schermnaam    OUT VARCHAR2
, x_entiteit_code OUT VARCHAR2
, x_id            OUT VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=verzoek ontvangen
  --    GV=Geen Verzoeken
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GK=Geen Koppeling
  --    OF=Oracle Foutsituatie
);

-- Sluit de medewerker in het ontvangende systeem, af voor opstartverzoeken vanuit het verzendende systeem
PROCEDURE sluit_koppeling
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=koppeling gesloten
  --    KH=Koppeling Hersloten
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    OF=Oracle Foutsituatie
);

-- Zet vanuit het verzendende systeem, 1 opstartverzoek voor de medewerker in het ontvangende systeem klaar
PROCEDURE verzend_opstart_verzoek
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, p_schermnaam    IN  VARCHAR2
, p_entiteit_code IN  VARCHAR2
, p_id            IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=verzoek verzonden
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GK=Geen Koppeling
  --    OF=Oracle Foutsituatie
);
END KGC_OPVZ_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPVZ_00" IS
-- Geeft de identificatie uit de medewerkerstabel van Helix
PROCEDURE geef_helix_medewerker
( p_vms_username  IN  VARCHAR2
, x_mede_id       OUT VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=medewerker gevonden
  -- Verwerking afgebroken:
  --    OM=Onbekende Medewerker
  --    NU=Niet Unieke medewerker
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR mede_cur
  ( b_vms_username IN VARCHAR2 )
  IS
    SELECT mede.mede_id
      FROM kgc_medewerkers mede
     WHERE mede.vms_username = upper( b_vms_username )
    ;
  mede_rec     mede_cur%ROWTYPE := NULL;
  mede_rec_dum mede_cur%ROWTYPE := NULL;

BEGIN
  x_mede_id := NULL;
  x_status  := '??';
  IF p_vms_username IS NULL
  THEN -- OM=Onbekende Medewerker
    x_status := 'OM';
  ELSE
    OPEN mede_cur(b_vms_username => p_vms_username);
    FETCH mede_cur INTO mede_rec;
    IF mede_cur%FOUND
    THEN
      FETCH mede_cur INTO mede_rec_dum;
      IF mede_cur%FOUND
      THEN -- NU=Niet Unieke medewerker
        x_status := 'NU';
      ELSE -- OK=medewerker gevonden
        x_mede_id := to_char(mede_rec.mede_id);
        x_status  := 'OK';
      END IF;
    ELSE -- OM=Onbekende Medewerker
      x_status := 'OM';
    END IF;
    CLOSE mede_cur;
  END IF;
EXCEPTION
  WHEN OTHERS
  THEN -- OF=Oracle Foutsituatie
    x_status := 'OF';
END geef_helix_medewerker;

-- Controleer de context parameters p_ontvanger, p_verzender en p_mede_id
PROCEDURE controleer_context_parameters
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Controle uitgevoerd:
  --    OK=parameters in orde
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR mede_cur
  ( b_mede_id IN VARCHAR2 )
  IS
    SELECT NULL
      FROM kgc_medewerkers mede
     WHERE TO_CHAR( mede.mede_id ) = NVL( b_mede_id, '-9999999999')
    ;
  mede_rec mede_cur%ROWTYPE := NULL;
BEGIN
  x_status  := '??';
  IF    NVL( p_ontvanger, '?' ) NOT IN ( 'HELIX', 'PEDIGREE' )
  THEN -- OO=Ongeldige Ontvanger
    x_status := 'OO';
  ELSIF NVL( p_verzender, '?' ) NOT IN ( 'HELIX', 'PEDIGREE' )
  THEN -- OV=Ongeldige Verzender
    x_status := 'OV';
  ELSIF NOT ( ( p_ontvanger = 'HELIX'    AND p_verzender = 'PEDIGREE' ) OR
              ( p_ontvanger = 'PEDIGREE' AND p_verzender = 'HELIX'    ) )
  THEN -- OC=Ongeldige Combinatie
    x_status := 'OC';
  ELSE
    OPEN mede_cur(b_mede_id => p_mede_id);
    FETCH mede_cur INTO mede_rec;
    IF mede_cur%FOUND
    THEN -- OK=parameters in orde
      x_status := 'OK';
    ELSE -- OM=Ongeldige Medewerker
      x_status := 'OM';
    END IF;
    CLOSE mede_cur;
  END IF;
EXCEPTION
  WHEN OTHERS
  THEN
    x_status := 'OF';
END controleer_context_parameters;

-- Controleer de autorisatie van de medewerker via de database-rollen
PROCEDURE controleer_autorisatie
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Controle uitgevoerd:
  --    OK=autorisatie
  --    GA=Geen Autorisatie
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR rolp_cur
  ( b_mede_id IN VARCHAR2
  , b_rol     IN VARCHAR2
  )
  IS
    SELECT NULL
      FROM kgc_role_privs_vw rolp
     WHERE rolp.grantee =
           ( SELECT NVL( mede.oracle_uid, '?' )
               FROM kgc_medewerkers mede
              WHERE ( TO_CHAR( mede.mede_id ) = b_mede_id )
       AND rolp.granted_role = b_rol )
    ;
  rolp_rec rolp_cur%ROWTYPE := NULL;

  v_rol VARCHAR2(20);
BEGIN
  x_status  := '??';
  v_rol := 'STAMBOOM_GEBRUIKER'; -- De koppeling van HELIX naar PEDIGREE en omgekeerd, valt onder zelfde database-rol
  OPEN rolp_cur
         ( b_mede_id => p_mede_id
         , b_rol     => v_rol );
  FETCH rolp_cur INTO rolp_rec;
  IF rolp_cur%FOUND
  THEN -- OK=autorisatie
    x_status := 'OK';
  ELSE -- GA=Geen Autorisatie
    x_status := 'GA';
  END IF;
  CLOSE rolp_cur;
EXCEPTION
  WHEN OTHERS
  THEN
    x_status := 'OF';
END controleer_autorisatie;

-- Controleer of de medewerker in het ontvangende systeem, openstaat voor opstartverzoeken vanuit het verzendende systeem
PROCEDURE controleer_koppeling
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Controle uitgevoerd:
  --    OK=wel koppeling
  --    GK=Geen Koppeling
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR opvz_cur
  ( b_ontvanger IN VARCHAR2
  , b_verzender IN VARCHAR2
  , b_mede_id   IN VARCHAR2
  )
  IS
    SELECT NULL
      FROM kgc_opstart_verzoeken opvz
     WHERE opvz.type_rij = 'K'
       AND opvz.ontvanger = b_ontvanger
       AND opvz.verzender = b_verzender
       AND TO_CHAR( opvz.mede_id ) = b_mede_id
       AND TRUNC( opvz.datum_aanmaak ) = TRUNC( SYSDATE )
       -- er staat geen openstaand verzoek van ouder dan 10 minuten (i.v.m. afbreken browser/sessie)
       AND NOT EXISTS (SELECT 1 FROM kgc_opstart_verzoeken opvz2
                        WHERE opvz2.ontvanger = opvz.ontvanger
                          AND opvz2.verzender = opvz.verzender
                          AND opvz2.type_rij = 'O'
                          AND opvz2.mede_id = opvz.mede_id
                          AND opvz2.datum_aanmaak < sysdate - (10 / (24 * 60))
                      )
    ;
  opvz_rec opvz_cur%ROWTYPE := NULL;
BEGIN
  x_status  := '??';
  OPEN opvz_cur
         ( b_ontvanger => p_ontvanger
         , b_verzender => p_verzender
         , b_mede_id   => p_mede_id );
  FETCH opvz_cur INTO opvz_rec;
  IF opvz_cur%FOUND
  THEN -- OK=wel koppeling
    x_status := 'OK';
  ELSE -- GK=Geen Koppeling
    x_status := 'GK';
  END IF;
  CLOSE opvz_cur;
EXCEPTION
  WHEN OTHERS
  THEN
    x_status := 'OF';
END controleer_koppeling;

-- Schoon alle rijen van voor de systeemdatum
PROCEDURE schoon_oude_verzoeken
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=schoning uitgevoerd
  --    GS=Geen Schoning nodig
  -- Verwerking afgebroken:
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR opvz_vandaag_cur
  ( b_ontvanger IN VARCHAR2
  , b_verzender IN VARCHAR2
  )
  IS
    SELECT NULL
      FROM kgc_opstart_verzoeken opvz
     WHERE opvz.type_rij = 'D'
       AND opvz.ontvanger = b_ontvanger
       AND opvz.verzender = b_verzender
       AND TRUNC( opvz.datum_aanmaak ) = TRUNC( SYSDATE )
    ;
  opvz_vandaag_rec opvz_vandaag_cur%ROWTYPE := NULL;

  CURSOR opvz_schoon_cur
  ( b_ontvanger IN VARCHAR2
  , b_verzender IN VARCHAR2
  )
  IS
    SELECT NULL
      FROM kgc_opstart_verzoeken opvz
     WHERE opvz.type_rij IN ( 'D', 'K', 'O' )
       AND opvz.ontvanger = b_ontvanger
       AND opvz.verzender = b_verzender
       AND TRUNC( opvz.datum_aanmaak ) < TRUNC( SYSDATE )
       FOR UPDATE
    ;

  v_status_koppeling VARCHAR2(2) := NULL;
BEGIN
  x_status := '??';
  OPEN opvz_vandaag_cur
         ( b_ontvanger => p_ontvanger
         , b_verzender => p_verzender );
  FETCH opvz_vandaag_cur INTO opvz_vandaag_rec;
  IF opvz_vandaag_cur%FOUND
  THEN -- GS=Geen Schoning nodig
    CLOSE opvz_vandaag_cur;
    x_status := 'GS';
  ELSE
    CLOSE opvz_vandaag_cur;
    x_status := 'OK'; -- OK=schoning uitgevoerd

    -- Verwijder alle rijen mbt deze koppeling:
    FOR opvz_schoon_rec IN opvz_schoon_cur
      ( b_ontvanger => p_ontvanger
      , b_verzender => p_verzender )
    LOOP
      DELETE FROM kgc_opstart_verzoeken
      WHERE CURRENT OF opvz_schoon_cur;
    END LOOP;

    -- Voeg voor deze koppeling de Delete-rij toe:
    INSERT INTO kgc_opstart_verzoeken
      ( type_rij
      , ontvanger
      , verzender
      , datum_aanmaak
      )
    VALUES
      ( 'D'
      , p_ontvanger
      , p_verzender
      , SYSDATE
      );

    COMMIT;

  END IF;
EXCEPTION
  WHEN OTHERS
  THEN -- OF=Oracle Foutsituatie
    x_status := 'OF';
END schoon_oude_verzoeken;

-- Stel de medewerker in het ontvangende systeem, open voor opstartverzoeken vanuit het verzendende systeem
PROCEDURE open_koppeling
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=koppeling geopend
  --    KH=Koppeling Heropend
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GA=Geen Autorisatie
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR opvz_cur
  ( b_ontvanger IN VARCHAR2
  , b_verzender IN VARCHAR2
  , b_mede_id   IN VARCHAR2
  )
  IS
    SELECT NULL
      FROM kgc_opstart_verzoeken opvz
     WHERE opvz.type_rij IN ( 'K', 'O' )
       AND opvz.ontvanger = b_ontvanger
       AND opvz.verzender = b_verzender
       AND TO_CHAR( opvz.mede_id ) = b_mede_id
       FOR UPDATE
    ;

  v_status_koppeling VARCHAR2(2) := NULL;
BEGIN
  x_status := '??';

  controleer_context_parameters
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Ongeldige context-parameters, stoppen met de verwerking
    RETURN;
  END IF;

  controleer_autorisatie
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Medewerker heeft geen autorisatie, stoppen met de verwerking
    RETURN;
  END IF;

  -- De eerste medewerker die deze koppeling opent voert de schoning voor deze koppeling uit:
  schoon_oude_verzoeken
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , x_status    => x_status -- OK, GS
    );
  IF x_status NOT IN ( 'OK', 'GS' )
  THEN -- Fout opgetreden tijdens de schoning stoppen met de verwerking
    RETURN;
  END IF;

  -- Kijk of voor deze medewerker de koppeling al dan niet openstaat:
  controleer_koppeling
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => v_status_koppeling -- OK, GK
    );

  -- GWE 05-03-2010 niet meer i.v.m. eventueel afgebroken programma(browser)
  -- Verwijder alle rijen van de medewerker mbt deze koppeling:
  --FOR opvz_rec IN opvz_cur
  --  ( b_ontvanger => p_ontvanger
  --  , b_verzender => p_verzender
  --  , b_mede_id   => p_mede_id )
  --LOOP
  --  DELETE FROM kgc_opstart_verzoeken
  --  WHERE CURRENT OF opvz_cur;
  --END LOOP;

  -- Voeg voor de medewerker een Koppel-rij toe:
  INSERT INTO kgc_opstart_verzoeken
    ( type_rij
    , ontvanger
    , verzender
    , mede_id
    , datum_aanmaak
    )
  VALUES
    ( 'K'
    , p_ontvanger
    , p_verzender
    , TO_NUMBER( p_mede_id )
    , SYSDATE
    );

  -- Geef status terug:
  IF    v_status_koppeling = 'OK'
  THEN -- KH=Koppeling Heropend
    x_status := 'KH';
  ELSIF v_status_koppeling = 'GK'
  THEN -- OK=koppeling geopend
    x_status := 'OK';
  END IF;

  COMMIT;

EXCEPTION
  WHEN OTHERS
  THEN -- OF=Oracle Foutsituatie
    x_status := 'OF';
END open_koppeling;

-- Kijk of er voor de medewerker in het ontvangende systeem, een opstartverzoek door het verzendende systeem is klaargezet
PROCEDURE opstart_verzoek_aanwezig
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=verzoek aanwezig
  --    GV=Geen Verzoeken
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GK=Geen Koppeling
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR opvz_cur -- Levert 1 of meerdere Opstartverzoek-rijen
  ( b_ontvanger IN VARCHAR2
  , b_verzender IN VARCHAR2
  , b_mede_id   IN VARCHAR2
  )
  IS
    SELECT opvz.schermnaam
         , opvz.entiteit_code
         , opvz.id
      FROM kgc_opstart_verzoeken opvz
     WHERE opvz.type_rij = 'O'
       AND opvz.ontvanger = b_ontvanger
       AND opvz.verzender = b_verzender
       AND TO_CHAR( opvz.mede_id ) = b_mede_id
       AND TRUNC( opvz.datum_aanmaak ) = TRUNC( SYSDATE )
    ;
  opvz_rec opvz_cur%ROWTYPE := NULL;
BEGIN
  x_status := '??';

  controleer_context_parameters
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Ongeldige context-parameters, stoppen met de verwerking
    RETURN;
  END IF;

  controleer_koppeling
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Geen koppeling voor de medewerker geopend, stoppen met de verwerking
    RETURN;
  END IF;

  -- Kijk of er een Opstartverzoek-rij van de medewerker mbt deze koppeling bestaat:
  OPEN opvz_cur
         ( b_ontvanger => p_ontvanger
         , b_verzender => p_verzender
         , b_mede_id   => p_mede_id );
  FETCH opvz_cur INTO opvz_rec;
  IF opvz_cur%FOUND
  THEN -- OK=verzoek aanwezig
    x_status := 'OK';
  ELSE -- GV=Geen Verzoeken
    x_status := 'GV';
  END IF;
  CLOSE opvz_cur;

EXCEPTION
  WHEN OTHERS
  THEN -- OF=Oracle Foutsituatie
    x_status := 'OF';
END opstart_verzoek_aanwezig;

-- Lees voor de medewerker in het ontvangende systeem, 1 opstartverzoek dat is klaargezet door het verzendende systeem
PROCEDURE ontvang_opstart_verzoek
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_schermnaam    OUT VARCHAR2
, x_entiteit_code OUT VARCHAR2
, x_id            OUT VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=verzoek ontvangen
  --    GV=Geen Verzoeken
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GK=Geen Koppeling
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR opvz_cur -- Levert maximaal 1 Opstartverzoek-rij
  ( b_ontvanger IN VARCHAR2
  , b_verzender IN VARCHAR2
  , b_mede_id   IN VARCHAR2
  )
  IS
    SELECT opvz.schermnaam
         , opvz.entiteit_code
         , opvz.id
      FROM kgc_opstart_verzoeken opvz
     WHERE opvz.type_rij = 'O'
       AND opvz.ontvanger = b_ontvanger
       AND opvz.verzender = b_verzender
       AND TO_CHAR( opvz.mede_id ) = b_mede_id
       AND TRUNC( opvz.datum_aanmaak ) = TRUNC( SYSDATE )
       AND opvz_id = (SELECT MIN( opvz2.opvz_id )
                       FROM kgc_opstart_verzoeken opvz2
                      WHERE opvz2.type_rij = 'O'
                        AND opvz2.ontvanger = b_ontvanger
                        AND opvz2.verzender = b_verzender
                        AND TO_CHAR( opvz2.mede_id ) = b_mede_id
                        AND TRUNC( opvz2.datum_aanmaak ) = TRUNC( SYSDATE )
                     )
       FOR UPDATE
    ;
  opvz_rec opvz_cur%ROWTYPE := NULL;
BEGIN
  x_status := '??';

  controleer_context_parameters
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Ongeldige context-parameters, stoppen met de verwerking
    RETURN;
  END IF;

  controleer_koppeling
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Geen koppeling voor de medewerker geopend, stoppen met de verwerking
    RETURN;
  END IF;

  -- Lees en verwijder de Opstartverzoek-rij van de medewerker mbt deze koppeling:
  OPEN opvz_cur
         ( b_ontvanger => p_ontvanger
         , b_verzender => p_verzender
         , b_mede_id   => p_mede_id );
  FETCH opvz_cur INTO opvz_rec;
  IF opvz_cur%FOUND
  THEN -- OK=verzoek ontvangen
    x_schermnaam    := opvz_rec.schermnaam;
    x_entiteit_code := opvz_rec.entiteit_code;
    x_id            := opvz_rec.id;
    x_status        := 'OK';
    DELETE FROM kgc_opstart_verzoeken
    WHERE CURRENT OF opvz_cur;

    COMMIT;

  ELSE -- GV=Geen Verzoeken
    x_status := 'GV';
  END IF;
  CLOSE opvz_cur;

EXCEPTION
  WHEN OTHERS
  THEN -- OF=Oracle Foutsituatie
    x_status := 'OF';
END ontvang_opstart_verzoek;

-- Sluit de medewerker in het ontvangende systeem, af voor opstartverzoeken vanuit het verzendende systeem
PROCEDURE sluit_koppeling
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=koppeling gesloten
  --    KH=Koppeling Hersloten
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    OF=Oracle Foutsituatie
)
IS
  CURSOR opvz_cur
  ( b_ontvanger IN VARCHAR2
  , b_verzender IN VARCHAR2
  , b_mede_id   IN VARCHAR2
  )
  IS
    SELECT NULL
      FROM kgc_opstart_verzoeken opvz
     WHERE opvz.type_rij IN ( 'K', 'O' )
       AND opvz.ontvanger = b_ontvanger
       AND opvz.verzender = b_verzender
       AND TO_CHAR( opvz.mede_id ) = b_mede_id
       FOR UPDATE
    ;

  v_status_koppeling VARCHAR2(2) := NULL;
BEGIN
  x_status := '??';

  controleer_context_parameters
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Ongeldige context-parameters, stoppen met de verwerking
    RETURN;
  END IF;

  controleer_koppeling
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => v_status_koppeling -- OK, GK
    );

  -- Verwijder alle rijen van de medewerker mbt deze koppeling:
  FOR opvz_rec IN opvz_cur
    ( b_ontvanger => p_ontvanger
    , b_verzender => p_verzender
    , b_mede_id   => p_mede_id )
  LOOP
    DELETE FROM kgc_opstart_verzoeken
    WHERE CURRENT OF opvz_cur;
  END LOOP;

  -- Geef status terug:
  IF    v_status_koppeling = 'OK'
  THEN -- OK=koppeling gesloten
    x_status := 'OK';
  ELSIF v_status_koppeling = 'GK'
  THEN -- KH=Koppeling Hersloten
    x_status := 'KH';
  END IF;

  COMMIT;

EXCEPTION
  WHEN OTHERS
  THEN -- OF=Oracle Foutsituatie
    x_status := 'OF';
END sluit_koppeling;

-- Zet vanuit het verzendende systeem, 1 opstartverzoek voor de medewerker in het ontvangende systeem klaar
PROCEDURE verzend_opstart_verzoek
( p_ontvanger     IN  VARCHAR2
, p_verzender     IN  VARCHAR2
, p_mede_id       IN  VARCHAR2
, p_schermnaam    IN  VARCHAR2
, p_entiteit_code IN  VARCHAR2
, p_id            IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Verwerking uitgevoerd:
  --    OK=verzoek verzonden
  -- Verwerking afgebroken:
  --    OO=Ongeldige Ontvanger
  --    OV=Ongeldige Verzender
  --    OC=Ongeldige Combinatie
  --    OM=Ongeldige Medewerker
  --    GK=Geen Koppeling
  --    OF=Oracle Foutsituatie
)
IS
BEGIN
  x_status := '??';

  controleer_context_parameters
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Ongeldige context-parameters, stoppen met de verwerking
    RETURN;
  END IF;

  controleer_koppeling
    ( p_ontvanger => p_ontvanger
    , p_verzender => p_verzender
    , p_mede_id   => p_mede_id
    , x_status    => x_status
    );
  IF x_status <> 'OK'
  THEN -- Geen koppeling voor de medewerker geopend, stoppen met de verwerking
    RETURN;
  END IF;

  -- Voeg voor de medewerker een Opstartverzoek-rij toe:
  INSERT INTO kgc_opstart_verzoeken
    ( type_rij
    , ontvanger
    , verzender
    , mede_id
    , datum_aanmaak
    , schermnaam
    , entiteit_code
    , id
    )
  VALUES
    ( 'O'
    , p_ontvanger
    , p_verzender
    , TO_NUMBER( p_mede_id )
    , SYSDATE
    , p_schermnaam
    , p_entiteit_code
    , p_id
    );

  -- Geef status terug: OK=verzoek verzonden
  x_status := 'OK';

  COMMIT;

EXCEPTION
  WHEN OTHERS
  THEN -- OF=Oracle Foutsituatie
    x_status := 'OF';
END verzend_opstart_verzoek;
END kgc_opvz_00;
/

/
QUIT
