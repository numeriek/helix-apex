CREATE OR REPLACE PACKAGE "HELIX"."BAS_ISOL_00" IS
-- vul bas_isolatielijsten op basis van een where-clausule (zie oa. form basisol01)
PROCEDURE maak_isolatielijst
( p_isolatielijstnummer IN VARCHAR2
, p_kafd_id IN NUMBER
, p_proc_id IN NUMBER
, p_where IN VARCHAR2
, p_zusterfractie in varchar2
);
-- bepaal familienaam van een persoon (in het kader van een familieonderzoek
FUNCTION familie
( p_pers_id IN NUMBER
, p_mons_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( familie, WNDS, WNPS, RNPS );
-- mag monster op isolatielijst? (onderzoek is goedgekeurd of geen onderzoek)
FUNCTION goedgekeurd
( p_frac_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( goedgekeurd, WNDS, WNPS, RNPS );
-- aantal monsterbuizen per fractie dat nog geisoleerd kan worden
-- (verwijderde buizen kunnen later worden gebruikt, op andere isolatielijst
FUNCTION aantal_buizen
( p_mons_id IN NUMBER
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( aantal_buizen, WNDS, WNPS, RNPS );

-- bepaal prioriteit van monster op basis van onderzoek
FUNCTION spoed
( p_mons_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( spoed, WNDS, WNPS, RNPS );

-- bepaal familie van persoon van monster
FUNCTION familie
( p_mons_id IN NUMBER
, p_incl_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( familie, WNDS, WNPS, RNPS );
END BAS_ISOL_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_ISOL_00" IS
PROCEDURE maak_isolatielijst
( p_isolatielijstnummer IN VARCHAR2
, p_kafd_id IN NUMBER
, p_proc_id IN NUMBER
, p_where IN VARCHAR2
, p_zusterfractie in varchar2
)
IS
  v_statement VARCHAR2(32767);
  v_where VARCHAR2(4000) := '(1=1)';
BEGIN
  -- vul where-clausule aan
  IF ( p_where IS NOT NULL )
  THEN
    v_where := v_where||CHR(10)||' and '||REPLACE(p_where, 'BAS_ISOLATIELIJST_VW', 'isov' );
  END IF;

  v_statement := 'declare'
     ||CHR(10)|| ' v_datum date := trunc(SYSDATE);'
     ||CHR(10)|| ' v_volgnr number;'
     ||CHR(10)|| ' v_user varchar2(30) := user;'
     ||CHR(10)|| 'cursor c is'
     ||CHR(10)|| 'select isov.frac_id'
     ||CHR(10)|| ', isov.mons_id'
     ||CHR(10)|| ', isov.ongr_id'
     ||CHR(10)|| ', isov.kafd_id';
  if p_zusterfractie = 'J'
  then
    v_statement :=v_statement
     ||CHR(10)|| ', 1  mons_aantal_buizen';
  else
    v_statement :=v_statement
     ||CHR(10)|| ', decode(isov.frac_id_parent,null,isov.mons_aantal_buizen,1) mons_aantal_buizen';
  end if;
  v_statement :=v_statement
     ||CHR(10)|| 'from bas_isolatielijst_vw isov'
     ||CHR(10)|| 'where isov.kafd_id = '||TO_CHAR( p_kafd_id )
     ||CHR(10)|| 'and '||v_where
     ||CHR(10)|| ';'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'select nvl(max(volgnummer),0)'
     ||CHR(10)|| 'into v_volgnr'
     ||CHR(10)|| 'from bas_isolatielijsten'
     ||CHR(10)|| 'where isolatielijst_nummer = '''||p_isolatielijstnummer||''';'
     ||CHR(10)|| 'for r in c loop'
     ||CHR(10)|| 'for i in 1..nvl(r.mons_aantal_buizen,1) loop'
     ||CHR(10)|| 'v_volgnr := v_volgnr + 1;'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'insert into bas_isolatielijsten'
     ||CHR(10)|| '( isolatielijst_nummer'
     ||CHR(10)|| ', oracle_uid'
     ||CHR(10)|| ', datum'
     ||CHR(10)|| ', stap'
     ||CHR(10)|| ', volgnummer'
     ||CHR(10)|| ', frac_id'
     ||CHR(10)|| ', mons_id'
     ||CHR(10)|| ', ongr_id'
     ||CHR(10)|| ', kafd_id'
     ||CHR(10)|| ', proc_id'
     ||CHR(10)|| ', buisnr'
     ||CHR(10)|| ')'
     ||CHR(10)|| 'values'
     ||CHR(10)|| '( '''||p_isolatielijstnummer||''''
     ||CHR(10)|| ', v_user'
     ||CHR(10)|| ', v_datum'
     ||CHR(10)|| ', 0'
     ||CHR(10)|| ', v_volgnr'
     ||CHR(10)|| ', r.frac_id'
     ||CHR(10)|| ', r.mons_id'
     ||CHR(10)|| ', r.ongr_id'
     ||CHR(10)|| ', r.kafd_id'
     ||CHR(10)|| ', '||NVL(TO_CHAR( p_proc_id ),'null')
     ||CHR(10)|| ', i'
     ||CHR(10)|| ');'
     ||CHR(10)|| 'exception when dup_val_on_index then null;'
     ||CHR(10)|| 'v_volgnr := v_volgnr - 1;'
     ||CHR(10)|| 'when others then raise;'
     ||CHR(10)|| 'end;'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'commit;'
     ||CHR(10)|| 'exception when others then'
     ||CHR(10)|| 'qms$errors.show_message'
     ||CHR(10)|| '(p_mesg => ''KGC-00000'''
     ||CHR(10)|| ',p_param0 => ''Isolatielijst niet aangemaakt!'''
     ||CHR(10)|| ',p_param1 => sqlerrm'
     ||CHR(10)|| ',p_errtp => ''E'''
     ||CHR(10)|| ',p_rftf => true'
     ||CHR(10)|| ');'
     ||CHR(10)|| 'end;'
               ;
  BEGIN
    kgc_util_00.dyn_exec( v_statement );
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message
      (p_mesg => 'KGC-00000'
      ,p_param0 => 'Isolatielijst niet aangemaakt!'
      ,p_param1 => SQLERRM
      ,p_errtp => 'E'
      ,p_rftf => TRUE
      );
  END;
END maak_isolatielijst;

FUNCTION familie
( p_pers_id IN NUMBER
, p_mons_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR c
  IS
    SELECT RPAD(TO_CHAR( fami.fami_id ),11,' ')||fami.naam
    FROM   kgc_families fami
    ,      kgc_familie_onderzoeken faon
    ,      kgc_faon_betrokkenen fobe
    ,      kgc_onderzoek_monsters onmo
    WHERE  fami.fami_id = faon.fami_id
    AND    faon.faon_id = fobe.faon_id
    AND    fobe.onde_id = onmo.onde_id
    AND    onmo.mons_id = p_mons_id
    AND    fobe.pers_id = p_pers_id
    ;
BEGIN
  IF ( p_pers_id IS NOT NULL
   AND p_mons_id IS NOT NULL
     )
  THEN
    OPEN  c;
    FETCH c
    INTO  v_return;
    CLOSE c;
  END IF;
  RETURN( v_return );
END familie;

FUNCTION goedgekeurd
( p_frac_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR c
  IS
    SELECT onde.status
    FROM   kgc_onderzoeken onde
    ,      bas_metingen meti
    WHERE  meti.onde_id = onde.onde_id
    AND    meti.frac_id = p_frac_id
    ;
  r c%rowtype;
BEGIN
  IF ( p_frac_id IS NOT NULL )
  THEN
    OPEN  c;
    FETCH c
    INTO  r;
/*    IF ( c%notfound )---Commented for Mantis  7231
    THEN
      v_return := 'J';
    ELSE
      v_return := 'N'; -- init
    END IF;*/---Commented for Mantis  7231
    WHILE ( c%found )
    LOOP
      IF ( r.status = 'G' )
      THEN
        v_return := 'J';
        EXIT;
      END IF;
      FETCH c
      INTO  r;
    END LOOP;
    IF ( c%isopen )
    THEN
      CLOSE c;
    END IF;
  END IF;
  RETURN( v_return );
END goedgekeurd;

FUNCTION aantal_buizen
( p_mons_id IN NUMBER
)
RETURN NUMBER
IS
  v_aantal NUMBER;
  CURSOR mons_cur
  IS
    SELECT mons.aantal_buizen
    FROM   kgc_monsters mons
    WHERE  mons.mons_id = p_mons_id
    ;
  mons_rec mons_cur%rowtype;
  CURSOR isol_cur
  IS
    SELECT COUNT(*) aantal_buizen
    FROM   bas_isolatielijsten isol
    WHERE  isol.mons_id = p_mons_id
    ;
  isol_rec isol_cur%rowtype;
BEGIN
  IF ( p_mons_id IS NOT NULL )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  mons_rec;
    CLOSE mons_cur;

    OPEN  isol_cur;
    FETCH isol_cur
    INTO  isol_rec;
    CLOSE isol_cur;
    v_aantal := NVL( mons_rec.aantal_buizen, 1 )
              - NVL( isol_rec.aantal_buizen, 0 )
              ;
    v_aantal := NVL( GREATEST( v_aantal, 0 ), 0 );
  END IF;
  RETURN( v_aantal );
END aantal_buizen;

FUNCTION spoed
( p_mons_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  -- 1x spoed betekent met prioriteit
  CURSOR onde_cur
  IS
    SELECT 'J'
    FROM   kgc_onderzoeken onde
    ,      kgc_onderzoek_monsters onmo
    WHERE  onmo.onde_id = onde.onde_id
    AND    onde.spoed = 'J'
    AND    onde.afgerond = 'N'
    AND    onmo.mons_id = p_mons_id
    ;
BEGIN
  OPEN  onde_cur;
  FETCH onde_cur
  INTO  v_return;
  CLOSE onde_cur;
  RETURN( v_return );
END spoed;

FUNCTION familie
( p_mons_id IN NUMBER
, p_incl_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(200);
  -- lid van familie (laatste)
  CURSOR fale_cur
  IS
    SELECT fami.familienummer
    ,      fami.naam
    FROM   kgc_families fami
    ,      kgc_familie_leden fale
    ,      kgc_monsters mons
    WHERE  fale.fami_id = fami.fami_id
    AND    fale.pers_id = mons.pers_id
    AND    mons.mons_id = p_mons_id
    ORDER BY fami.fami_id DESC
    ;
  fami_rec fale_cur%rowtype;
BEGIN
  OPEN  fale_cur;
  FETCH fale_cur
  INTO  fami_rec;
  CLOSE fale_cur;
  IF ( p_incl_naam = 'J' )
  THEN
    v_return := fami_rec.familienummer||' '||fami_rec.naam;
  ELSE
    v_return := fami_rec.familienummer;
  END IF;
  RETURN( v_return );
END familie;
END  bas_isol_00;
/

/
QUIT
