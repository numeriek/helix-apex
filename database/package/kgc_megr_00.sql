CREATE OR REPLACE PACKAGE "HELIX"."KGC_MEGR_00" IS
-- PL/SQL Specification
-- plaats meting in een (nieuwe) metinggroep
PROCEDURE plaats_in_groep
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER
, p_meti_id IN NUMBER
, p_mede_id IN NUMBER
, p_forceer_nieuw IN BOOLEAN := FALSE
, x_megr_id IN OUT NUMBER
);


END KGC_MEGR_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MEGR_00" IS
PROCEDURE plaats_in_groep
( p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER
, p_meti_id IN NUMBER
, p_mede_id IN NUMBER
, p_forceer_nieuw IN BOOLEAN := FALSE
, x_megr_id IN OUT NUMBER
)
IS
  v_max NUMBER;
  CURSOR megr_cur
  IS
    SELECT megr.megr_id
    ,      COUNT(*) aantal
    FROM   kgc_megr_samenstelling mgsa
    ,      kgc_meting_groepen megr
    WHERE  megr.megr_id = mgsa.megr_id (+)
    AND    megr.megr_id = NVL( x_megr_id, megr.megr_id )
    AND    megr.gefixeerd = 'N'
    AND    megr.kafd_id = p_kafd_id
    AND  ( megr.mede_id = p_mede_id
        OR p_mede_id IS NULL
         )
    GROUP BY megr.megr_id
    HAVING COUNT(*) < v_max
    ;
  megr_rec megr_cur%rowtype;
  megr_trow kgc_meting_groepen%rowtype;
  megr_null kgc_meting_groepen%rowtype;
-- PL/SQL Block
BEGIN
  BEGIN
    v_max := kgc_sypa_00.standaard_waarde
             ( p_parameter_code => 'MAX_AANTAL_METINGEN_IN_GROEP'
             , p_kafd_id => p_kafd_id
             );
  EXCEPTION
    WHEN OTHERS
    THEN
      v_max := 10;
  END;
  -- Let op: aanroep van deze procedure in een loop
  -- de eerste keer wordt een groep gemaakt, de volgende keren wordt die groep aangeroepen
  IF ( p_forceer_nieuw
   AND x_megr_id IS NULL
     )
  THEN
    megr_rec.aantal := v_max + 1;
  ELSE
    OPEN  megr_cur;
    FETCH megr_cur
    INTO  megr_rec;
    CLOSE megr_cur;
  END IF;
  IF ( megr_rec.aantal < v_max )
  THEN
    INSERT INTO kgc_megr_samenstelling
    ( megr_id
    , meti_id
    )
    VALUES
    ( megr_rec.megr_id
    , p_meti_id
    );
  ELSE
    -- maak nieuwe groep
    megr_trow := megr_null;
    megr_trow.kafd_id := p_kafd_id;
    megr_trow.nummer := kgc_nust_00.genereer_nr
                        ( p_nummertype => 'MEGR'
                        , p_kafd_id => p_kafd_id
                        , p_ongr_id => p_ongr_id
                        );
    megr_trow.omschrijving := NULL;
    megr_trow.mede_id := p_mede_id;
    SELECT kgc_megr_seq.nextval
    INTO megr_trow.megr_id
    FROM dual;
    INSERT INTO kgc_meting_groepen
    ( megr_id
    , kafd_id
    , nummer
    , omschrijving
    , mede_id
    )
    VALUES
    ( megr_trow.megr_id
    , megr_trow.kafd_id
    , megr_trow.nummer
    , megr_trow.omschrijving
    , megr_trow.mede_id
    );
    x_megr_id := megr_trow.megr_id;
    INSERT INTO kgc_megr_samenstelling
    ( megr_id
    , meti_id
    )
    VALUES
    ( megr_trow.megr_id
    , p_meti_id
    );
  END IF;
END plaats_in_groep;

END kgc_megr_00;
/

/
QUIT
