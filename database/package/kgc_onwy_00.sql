CREATE OR REPLACE PACKAGE "HELIX"."KGC_ONWY_00" IS
-- Retourneer onwy_omschrijving
FUNCTION onwy_omschrijving
( p_onde_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( onwy_omschrijving, WNDS, WNPS, RNPS );
END KGC_ONWY_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ONWY_00" IS
FUNCTION onwy_omschrijving
( p_onde_id IN NUMBER
)
RETURN VARCHAR2
IS
  CURSOR onwy_cur
  ( b_onde_id IN NUMBER
  )
  IS
   SELECT onwy.omschrijving
     FROM kgc_onderzoekswijzen onwy
        , kgc_onderzoeken      onde
    WHERE onwy.kafd_id = onde.kafd_id
      AND onwy.code(+) = onde.onderzoekswijze
      AND onde.onde_id = b_onde_id
  ;
  v_return kgc_onderzoekswijzen.omschrijving%TYPE := NULL;

BEGIN
  IF ( p_onde_id IS NOT NULL )
  THEN
     OPEN  onwy_cur ( p_onde_id );
     FETCH onwy_cur
     INTO  v_return;
     CLOSE onwy_cur;
  END IF;
  RETURN(v_return);
END onwy_omschrijving;
END kgc_onwy_00;
/

/
QUIT
