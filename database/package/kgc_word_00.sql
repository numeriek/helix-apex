CREATE OR REPLACE PACKAGE "HELIX"."KGC_WORD_00" IS

  TYPE t_document_gegevens IS RECORD (
    procedure_naam              kgc_xml_documenten.procedure_naam%TYPE
   ,standaard_pad               kgc_bestand_types.standaard_pad%TYPE
   ,standaard_extentie          kgc_bestand_types.standaard_extentie%TYPE
   ,bestand_specificatie        kgc_model_bestanden.bestand_specificatie%TYPE
   ,nummerstructuur             kgc_nummerstructuur.code%TYPE
   ,view_name                   kgc_bestand_types.view_name%TYPE
   ,ind_registreren_als_brief   kgc_bestand_types.ind_registreren_als_brief%TYPE
   ,entiteit_code               kgc_entiteiten.code%TYPE
   ,entiteit_pk_kolom           kgc_entiteiten.pk_kolom%TYPE
  );

  FUNCTION get_document_gegevens (
    p_btyp_id   IN   kgc_bestand_types.btyp_id%TYPE
   ,p_kafd_id   IN   kgc_model_bestanden.kafd_id%TYPE
   ,p_ongr_id   IN   kgc_model_bestanden.ongr_id%TYPE
  )
    RETURN t_document_gegevens;

  PROCEDURE roep_aan_specifiek (
    p_procedure_naam          IN   kgc_xml_documenten.procedure_naam%TYPE
   ,p_view_name               IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause            IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie               IN   VARCHAR2 DEFAULT 'N'
  , p_ind_brief              IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_bijlage             IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_kopiehouders   IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst          IN   VARCHAR2 DEFAULT NULL
   ,p_bestandsnaam            IN   kgc_bestanden.bestandsnaam%TYPE
   ,p_bestand_specificatie    IN   kgc_model_bestanden.bestand_specificatie%TYPE
   ,p_type                    IN   VARCHAR2 DEFAULT 'DOC'
   ,p_app_server_temp_dir     IN   kgc_systeem_parameters.standaard_waarde%TYPE
	 ,p_destype                 in   varchar2 default null
	 ,p_best_id                 in   kgc_bestanden.best_id%type default null
   ,p_blob_bestand            OUT BLOB
  );

  PROCEDURE uitvoer_naar_bestand (
    p_rapport        IN       VARCHAR2
   ,p_kafd_id        IN       NUMBER := NULL
   ,p_ongr_id        IN       NUMBER := NULL
   ,x_btyp_id        OUT      NUMBER
   ,x_btyp_pcap_id   OUT      NUMBER
   ,x_btyp_enti_code OUT      VARCHAR2
   ,x_generator      OUT      VARCHAR2
  );

  FUNCTION maak_word_doc (
    p_xml_clob               IN   CLOB
   ,p_word_bestandsnaam      IN   VARCHAR2
   ,p_bestand_specificatie   IN   kgc_model_bestanden.bestand_specificatie%TYPE
   ,p_type                   IN   VARCHAR2 DEFAULT 'DOC'
   ,p_resultdata             OUT  BLOB
  )
    RETURN VARCHAR2;

  PROCEDURE kafd_id_ongr_id_van_entiteit (
    p_entiteit_code   IN       kgc_bestanden.entiteit_code%TYPE
   ,p_entiteit_pk     IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id         OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id         OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr           OUT      kgc_personen.zisnr%TYPE
  );
END KGC_WORD_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_WORD_00" IS

  /******************************************************************************
      NAME:       kgc_word_00
      PURPOSE:    aanmaken van Word documenten dmv aanroep ASP op IIS

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        31-03-2006   HZO              Creatie
      1.1        18-05-2006   HZO              Aanpassingen nav test
      1.2        10-01-2010   GWE              Mantis 1945: generator toegevoegd
                                               aan model bestanden
      1.3        20-01-2011   JKO              Mantis 4188: maak_word_doc aangepast
      1.4        13-06-2012   JKO              Fusion-upgrade: maak_word_doc aangepast
   *****************************************************************************/

  /*****************************************************************************
     NAME:       ExportBlob
     PURPOSE:    Schrijf blob naar bestand. Wrapper voor stored Java programma

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        06-4-2006    HZO             Creatie
     1.1        18-5-2006    HZO             Java method is function geworden,
                                             derhalve ook function gemaakt van wrapper

     NOTES:

  *****************************************************************************/
  FUNCTION blob_naar_bestand (
    p_file   IN   VARCHAR2
   ,p_blob   IN   BLOB
  )
    RETURN VARCHAR2
  AS
    LANGUAGE JAVA
    NAME 'BlobHandler.ExportBlob(java.lang.String, oracle.sql.BLOB) return java.lang.String';


  /******************************************************************************
     NAME:       perform_dummy_http_transfer
     PURPOSE:    Workaround voor Oracle Database 11g bug:
                 De eerste keer nadat een gebruiker is ingelogd, werkt de WORD-koppeling niet;
                 De WORD-brief wordt correct aangemaakt op de webserver, maar bij het binnenhalen
                 van de binaire stream met utl_http.read_raw, blijkt deze afgekapt op +/- 44K.
                 Gevolg is, dat WORD het document niet kan openen wegens een verminking.
                 Oorzaak ligt in utl_http die in Oracle 11g de eerste keer na het inloggen,
                 ondanks de setting 'utl_http.set_persistent_conn_support(TRUE)',
                 de parameter close-connection=TRUE meegeeft. Hierdoor wordt de verbinding verbroken.
                 Deze workaround voert voorafgaand aan de eerste transfer, een dummy transfer uit.

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        19-12-2011  JKO              Creatie

     NOTES:

  *****************************************************************************/
  PROCEDURE perform_dummy_http_transfer
  IS
      v_dummy_file   kgc_systeem_parameters.standaard_waarde%TYPE;
      v_dummy_piece  UTL_HTTP.HTML_PIECES;
  BEGIN
      v_dummy_file := kgc_sypa_00.systeem_waarde(p_parameter_code => 'HELP_SYSTEEM_URL');
      utl_http.set_transfer_timeout(540);
      utl_http.set_persistent_conn_support (TRUE);
      v_dummy_piece := UTL_HTTP.REQUEST_PIECES(v_dummy_file, 1);
  EXCEPTION
      WHEN OTHERS
      THEN
        qms$errors.show_message (p_mesg       => 'KGC-00213'
                                ,p_param0     => 'FOUT'
                                ,p_param1     => 'KGC_WORD_00.perform_dummy_http_transfer'
                                ,p_param2     => sqlerrm
                                );
  END perform_dummy_http_transfer;


  /******************************************************************************
     NAME:       maak_word_doc
     PURPOSE:    Aanmaken van Word document op basis van XML-document

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        06-04-2006  HZO              Creatie
     1.1        18-05-2006  HZO              Diverse aanpassingen n.a.v test
     1.2        24-07-2006  YAR              Aanpassingen Rob Debets nav meldingen Maastricht
     1.3        20-01-2011  JKO              Mantis 4188: time-out parm toegevoegd
     1.4        13-06-2012  JKO              Fusion-upgrade: CLOB in blokken van 32K verzenden
                                             HTTP/1.0 --> HTTP/1.1

     NOTES:

  *****************************************************************************/
  FUNCTION maak_word_doc (
    p_xml_clob               IN   CLOB
   ,p_word_bestandsnaam      IN   VARCHAR2
   ,p_bestand_specificatie   IN   kgc_model_bestanden.bestand_specificatie%TYPE
   ,p_type                   IN   VARCHAR2 DEFAULT 'DOC'
   ,p_resultdata             OUT  BLOB
  )
    RETURN VARCHAR2
  IS
    v_http_req             utl_http.req;
    v_http_resp            utl_http.resp;
    -- URL voor het benaderen van ASP script
    v_url                  kgc_systeem_parameters.standaard_waarde%TYPE
      := kgc_sypa_00.standaard_waarde
                                  (p_parameter_code     => 'WORD_URL_ASP_SCRIPT'
                                  ,p_kafd_id            => NULL);
    -- tijdelijk word doc IIS, wordt puur voor de syntax meegegeven
    v_temp_word_document   VARCHAR2 (255)              := p_word_bestandsnaam;
    -- word doc op DB server (incl pad) , hier wordt de binary stream naar toe geschreven
    v_aant_bytes           NUMBER                                        := 0;
    v_rawdata              RAW (32767);
    v_multi_sep            VARCHAR2 (100)
                               := '---------------------------7d620d201b0b6c';
    crlf                   VARCHAR2 (2)                := chr (13)
                                                          || chr (10);
    v_msg_multipart1       VARCHAR2 (32767);
    v_msg_multipart2       VARCHAR2 (32767);
    v_msg_multipart3       VARCHAR2 (32767);
    v_linebuf              VARCHAR2 (4000);
    v_stap                 VARCHAR2 (100);
    v_resultaat            VARCHAR2 (1000);
    v_blob_bestand         BLOB;

    v_xml_size             BINARY_INTEGER;
    v_xml_offset           PLS_INTEGER;
    v_xml_amount           PLS_INTEGER := 32767;
    v_xml_buffer           VARCHAR2(32767);

  BEGIN
    --> 20111219/JKO: nodig ivm bug in Oracle database 11g
    IF UTL_HTTP.GET_COOKIE_COUNT() < 1
    THEN
      perform_dummy_http_transfer;
    END IF;
    --< 20111219/JKO: nodig ivm bug in Oracle database 11g

    utl_http.set_transfer_timeout(540);
    v_stap := 'begin http request ';
    v_http_req := utl_http.begin_request (v_url
                                         ,'POST'
                                         ,'HTTP/1.1'
                                         );
    utl_http.set_persistent_conn_support (TRUE);
    -- parameters meegeven
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'User-Agent'
                        ,VALUE     => 'Mozilla/4.0'
                        );
    -- file data
    v_stap := 'data samenstellen ';
    v_msg_multipart3 :=
         '--'
      || v_multi_sep
      || crlf
      || 'Content-Disposition: form-data; name="xmldata"; filename="'
      || v_temp_word_document
      || '"'
      || crlf
      || 'Content-Type: text/xml'
      || crlf
      || v_msg_multipart3
      || crlf
      || crlf;
    -- type
    v_msg_multipart1 := v_msg_multipart1 || '--' || v_multi_sep || crlf;
    v_msg_multipart1 :=    v_msg_multipart1
                        || 'Content-Disposition: form-data; name="doctype"'
                        || crlf;
    v_msg_multipart1 := v_msg_multipart1 || crlf;
    v_msg_multipart1 := v_msg_multipart1 || p_type || crlf;
    -- DOC , PDF  etc
    v_msg_multipart1 := v_msg_multipart1 || '--' || v_multi_sep || crlf;
    v_msg_multipart1 :=
         v_msg_multipart1
      || 'Content-Disposition: form-data; name="xsltemplate"'
      || crlf;
    v_msg_multipart1 := v_msg_multipart1 || crlf;
    v_msg_multipart1 := v_msg_multipart1 || p_bestand_specificatie || crlf;
                                                                 -- template
    -- afsluiten
    v_msg_multipart2 := v_msg_multipart2 || '--' || v_multi_sep || '--'
                        || crlf;
    -- init xml-stream
    v_msg_multipart3 :=
         '--'
      || v_multi_sep
      || crlf
      || 'Content-Disposition: form-data; name="xmldata"; filename="'
      || v_temp_word_document
      || '"'
      || crlf
      || 'Content-Type: text/xml'
      || crlf
      || crlf;

    v_aant_bytes :=   nvl (LENGTH (v_msg_multipart1), 0)
                    + nvl (LENGTH (v_msg_multipart2), 0)
                    + nvl (LENGTH (v_msg_multipart3), 0)
                    + dbms_lob.getlength(p_xml_clob)
                    + nvl (LENGTH (crlf || crlf), 0);
    utl_http.set_header (v_http_req
                        ,'Content-Length'
                        ,v_aant_bytes
                        );

-- definitie van html bericht
    v_stap := 'defnitie html bericht ';
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'Cache-Control'
                        ,VALUE     => 'no-cache'
                        );
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'Accept-Language'
                        ,VALUE     => 'en-us'
                        );
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'Referer'
                        ,VALUE     => 'http://umc6wypq0j:8083/index.htm'
                        );
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'Connection'
                        ,VALUE     => 'Keep-Alive'
                        );
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'Accept'
                        ,VALUE     => '*' || '/' || '*'
                        -- concatenering ivm designer bug !
                        );
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'Accept-Encoding'
                        ,VALUE     => 'gzip, deflate'
                        );
    utl_http.set_header (r         => v_http_req
                        ,NAME      => 'Content-Type'
                        ,VALUE     =>    'multipart/form-data; boundary='
                                      || v_multi_sep
                        );
-- Verzenden
    v_stap := ' verzenden ';
    utl_http.write_text (v_http_req, v_msg_multipart3);

    -- Inhoud (p_xml_clob) verzenden
    v_xml_size := dbms_lob.getlength( p_xml_clob );
    v_xml_offset := 1;
    WHILE ( v_xml_offset < v_xml_size )
    LOOP
       dbms_lob.read( p_xml_clob
                    , v_xml_amount
                    , v_xml_offset
                    , v_xml_buffer
                    );
        utl_http.write_text( v_http_req, v_xml_buffer );
        v_xml_offset := v_xml_offset + v_xml_amount ;
    END LOOP;

    -- afsl xml-stream
    utl_http.write_text (v_http_req, crlf || crlf);

    -- rest:
    utl_http.write_text (v_http_req, v_msg_multipart1);
    utl_http.write_text (v_http_req, v_msg_multipart2);

-- ontvangen response
    v_stap := ' ontvangen response ';
    v_http_resp := utl_http.get_response (v_http_req);
    -- lees de binaire stream
    v_stap := ' lees de binaire stream ';

    dbms_lob.createtemporary (p_resultdata, FALSE);

    BEGIN
      LOOP
        utl_http.read_raw (v_http_resp
                          ,v_rawdata
                          ,32767
                          );
        dbms_lob.writeappend (p_resultdata
                             ,utl_raw.LENGTH (v_rawdata)
                             ,v_rawdata
                             );
      END LOOP;

      utl_http.end_response (v_http_resp);
    EXCEPTION
      WHEN utl_http.end_of_body
      THEN
        utl_http.end_response (v_http_resp);
      WHEN OTHERS
      THEN
        RAISE;
    END;

    RETURN v_resultaat;
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_WORD_00.maak_word_doc'
                              ,p_param2     => sqlerrm
                              );
  END maak_word_doc;

  /******************************************************************************
      NAME:       roep_aan_specifiek
      PURPOSE:    roept een specifieke procedure aan voor het aanmaken van een brief
                  en maakt hiervan een word doc.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        6-4-2006    HZO             Creatie
      1.1       18-05-2006   HZO             Diverse aanpassingen n.a.v test
      1.2       24-07-2006   YAR             Diverse aanpassingen n.a.v. commentaar Rob Debets
      NOTES:

   *****************************************************************************/
  PROCEDURE roep_aan_specifiek (
    p_procedure_naam         IN   kgc_xml_documenten.procedure_naam%TYPE
   ,p_view_name              IN   kgc_bestand_types.view_name%TYPE
   ,p_where_clause           IN   VARCHAR2 DEFAULT NULL
   ,p_ind_kopie              IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_brief              IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_bijlage            IN   VARCHAR2 DEFAULT 'N'
   ,p_ind_kopiehouders       IN   VARCHAR2 DEFAULT 'N'
   ,p_speciale_tekst         IN   VARCHAR2 DEFAULT NULL
   ,p_bestandsnaam           IN   kgc_bestanden.bestandsnaam%TYPE
   ,p_bestand_specificatie   IN   kgc_model_bestanden.bestand_specificatie%TYPE
   ,p_type                   IN   VARCHAR2 DEFAULT 'DOC'
   ,p_app_server_temp_dir    IN   kgc_systeem_parameters.standaard_waarde%TYPE
   ,p_destype                IN   VARCHAR2 DEFAULT NULL
   ,p_best_id                IN   kgc_bestanden.best_id%TYPE DEFAULT NULL
   ,p_blob_bestand           OUT  BLOB
  )
  IS
    v_stmt                   VARCHAR2 (4000);
    v_xml_doc                kgc_xml_00.t_xml_doc_collection;

    v_stap                   VARCHAR2 (100);
    v_ontbrekend_gegeven     VARCHAR2 (100);
    e_geen_rijen             EXCEPTION;
    e_gegeven_ontbreekt      EXCEPTION;
    e_file_error             EXCEPTION;
    e_permission_error       EXCEPTION;
    e_onbekende_java_error   EXCEPTION;
    v_resultaat              VARCHAR2 (4000);
  BEGIN
    -- roep de XML generatie procedure dynamisch aan
    v_stap := 'Roep XML generatie procedure aan';

    IF p_procedure_naam IS NULL
    THEN
      v_ontbrekend_gegeven :=
                           'procedurenaam (te definieren bij XML-documenten)';
      RAISE e_gegeven_ontbreekt;
    ELSIF p_view_name IS NULL
    THEN
      v_ontbrekend_gegeven := 'view naam (te definieren bij bestandstype)';
      RAISE e_gegeven_ontbreekt;
    ELSIF p_bestand_specificatie IS NULL
    THEN
      v_ontbrekend_gegeven :=
                      'Bestand specificatie (te definieren bij modelbestand)';
      RAISE e_gegeven_ontbreekt;
    ELSIF p_type IS NULL
    THEN
      v_ontbrekend_gegeven :=
         'Standaard extentie (te definieren bij bestandtype of modelbestand)';
      RAISE e_gegeven_ontbreekt;
    ELSIF p_bestandsnaam IS NULL
    THEN
      v_ontbrekend_gegeven :=
                           'Nummerstructuur (te definieren bij modelbestand)';
      RAISE e_gegeven_ontbreekt;
    END IF;

    v_stmt :=    ' BEGIN '
              || p_procedure_naam
              || ' ( '
              || '   p_view_name       =>  :1 '
              || '  , p_where_clause   =>  :2 '
              || '  , p_ind_kopie      =>  :3 '
              || '  , p_ind_brief      =>  :4 '
              || '  , p_destype        =>  :5 '
              || '  , p_best_id        =>  :6 '
              || '  , p_speciale_tekst =>  :7 ); '
              || ' END; ';
    EXECUTE IMMEDIATE v_stmt
                USING p_view_name
                     ,p_where_clause
                     ,p_ind_kopie
                     ,p_ind_brief
                     ,p_destype
                     ,p_best_id
                     ,p_speciale_tekst;

    -- haal collection van xml documenten op
    v_stap := 'haal collection van xml documenten op';
    v_xml_doc := kgc_xml_00.get_xml_doc_collection;

    -- controleer of er records zijn opgehaald, breek anders af
    IF v_xml_doc.COUNT = 0
    THEN
      RAISE e_geen_rijen;
    END IF;

    v_stap := 'Maak Word doc';
    -- maak Word document
    v_resultaat :=
      kgc_word_00.maak_word_doc
                          (p_xml_clob                 => v_xml_doc (1)
                          ,p_word_bestandsnaam        =>    p_bestandsnaam
                                                        || '.'
                                                        || p_type
                          ,p_bestand_specificatie     => p_bestand_specificatie
                          ,p_type                     => p_type
                          ,p_resultdata               => p_blob_bestand
                          );

    -- vang Java fouten af
    IF instr (v_resultaat, 'java.io.FileNotFoundException') != 0
    THEN
      RAISE e_file_error;
    ELSIF instr (v_resultaat, 'java.security.AccessControlException') != 0
    THEN
      RAISE e_permission_error;
    ELSIF v_resultaat != 'OK'
    THEN
      RAISE e_onbekende_java_error;
    END IF;

  EXCEPTION
    WHEN e_gegeven_ontbreekt
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00210'
                              ,p_param0     => v_ontbrekend_gegeven);
    WHEN e_file_error
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00216'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_WORD_00.roep_aan_specifiek'
                              ,p_param2     => v_resultaat
                              );
    WHEN e_permission_error
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00217'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_WORD_00.roep_aan_specifiek'
                              ,p_param2     => v_resultaat
                              );
    WHEN e_onbekende_java_error
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00218'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_WORD_00.roep_aan_specifiek'
                              ,p_param2     => v_resultaat
                              );
    WHEN e_geen_rijen
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00214'
                              ,p_param0     => p_view_name
                              ,p_param1     => p_where_clause
                              ,p_rftf       => TRUE
                              );
    WHEN OTHERS
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_WORD_00.roep_aan_specifiek'
                              ,p_param2     => sqlerrm
                              );
  END roep_aan_specifiek;

  PROCEDURE uitvoer_naar_bestand (
    p_rapport        IN       VARCHAR2
   ,p_kafd_id        IN       NUMBER := NULL
   ,p_ongr_id        IN       NUMBER := NULL
   ,x_btyp_id        OUT      NUMBER
   ,x_btyp_pcap_id   OUT      NUMBER
   ,x_btyp_enti_code OUT      VARCHAR2
   ,x_generator      OUT      VARCHAR2
  )
  IS
/*****************************************************************************
   NAME:       uitvoer_naar_bestand
   PURPOSE:    Bepaal of een WORD document of een Oracle Report aangemaakt
               dient te worden.

   REVISIONS:

   Ver        Date        Author       Description
   ---------  ----------  ------------ ---------------------------------------
   1.0        02-05-2006  YAR          Melding: Word koppeling
   1.1        10-01-2010  GWE          Mantis 1945: extra parameter generator
   1.2        30-06-2010  JKO          Mantis 1945: extra parameter enti_code
*****************************************************************************/
    CURSOR btyp_cur
    (
      b_btyp_code   IN   VARCHAR2
     ,b_kafd_id     IN   NUMBER
     ,b_ongr_id     IN   NUMBER
    )
    IS
      SELECT btyp.btyp_id
           , btyp.pcap_id
           , btyp.enti_code
           , mobe.generator
        FROM kgc_bestand_types btyp, kgc_model_bestanden mobe
       WHERE btyp.code = b_btyp_code
         AND btyp.vervallen = 'N'
         AND btyp.btyp_id = mobe.btyp_id
         AND mobe.kafd_id = b_kafd_id
         AND (mobe.ongr_id = b_ongr_id OR
              (b_ongr_id IS NULL AND mobe.ongr_id IS NULL) OR
              mobe.ongr_id IS NULL);

    btyp_rec   btyp_cur%ROWTYPE;
  BEGIN
    -- Controleer of een Oracle Rapport gestart moet worden, of een Word document mbv XML
    OPEN btyp_cur (b_btyp_code     => p_rapport
                  ,b_kafd_id       => p_kafd_id
                  ,b_ongr_id       => p_ongr_id
                  );

    FETCH btyp_cur
     INTO btyp_rec;

    x_btyp_id        := btyp_rec.btyp_id;
    x_btyp_pcap_id   := btyp_rec.pcap_id;
    x_btyp_enti_code := btyp_rec.enti_code;
    x_generator      := btyp_rec.generator;

    CLOSE btyp_cur;
  END uitvoer_naar_bestand;

  /******************************************************************************
       NAME:       kafd_id_ongr_OBMG
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        19-05-2006    HZO             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_obmg (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_obmg (
      b_obmg_id   kgc_onbe_med_gegevens.obmg_id%TYPE
    )
    IS
      SELECT onde.kafd_id
            ,onde.ongr_id
		        ,pers.zisnr
        FROM kgc_onbe_med_gegevens     obmg
            ,kgc_onderzoek_betrokkenen onbe
            ,kgc_onderzoeken           onde
		        ,kgc_personen              pers
       WHERE obmg.onbe_id = onbe.onbe_id
         AND onbe.onde_id = onde.onde_id
		 AND onbe.pers_id = pers.pers_id
         AND obmg.obmg_id = b_obmg_id;
  BEGIN
    OPEN c_obmg (p_entiteit_pk);

    FETCH c_obmg
     INTO p_kafd_id
         ,p_ongr_id
		 ,p_zisnr;

    CLOSE c_obmg;
  END kafd_id_ongr_id_obmg;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_UITS
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        19-05-2006    HZO             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_uits (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_uits (
      b_uits_id   kgc_uitslagen.uits_id%TYPE
    )
    IS
      SELECT onde.kafd_id
            ,onde.ongr_id
			,pers.zisnr
        FROM kgc_uitslagen   uits
  		   , kgc_onderzoeken onde
			,kgc_personen    pers
       WHERE uits.onde_id = onde.onde_id
	     AND onde.pers_id = pers.pers_id
         AND uits.uits_id = b_uits_id;
  BEGIN
    OPEN c_uits (p_entiteit_pk);

    FETCH c_uits
     INTO p_kafd_id
         ,p_ongr_id
		 ,p_zisnr;

    CLOSE c_uits;
  END kafd_id_ongr_id_uits;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_ONDE
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_onde (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_onde (
      b_onde_id   kgc_onderzoeken.onde_id%TYPE
    )
    IS
      SELECT onde.kafd_id
            ,onde.ongr_id
			,pers.zisnr
        FROM kgc_onderzoeken onde
			,kgc_personen    pers
       WHERE onde.onde_id = b_onde_id
	   AND   onde.pers_id= pers.pers_id
	   ;
  BEGIN
    OPEN c_onde (p_entiteit_pk);

    FETCH c_onde
     INTO p_kafd_id
         ,p_ongr_id
		 ,p_zisnr;

    CLOSE c_onde;
  END kafd_id_ongr_id_onde;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_MONS
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_mons (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_mons (
      b_mons_id   kgc_monsters.mons_id%TYPE
    )
    IS
      SELECT mons.kafd_id
            ,mons.ongr_id
			,pers.zisnr
        FROM kgc_monsters mons
		,    kgc_personen pers
       WHERE mons.mons_id = b_mons_id
	   AND   mons.pers_id = pers.pers_id
	   ;
  BEGIN
    OPEN c_mons (p_entiteit_pk);

    FETCH c_mons
     INTO p_kafd_id
         ,p_ongr_id
		 ,p_zisnr;

    CLOSE c_mons;
  END kafd_id_ongr_id_mons;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_FRAC
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_frac (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_frac (
      b_frac_id   bas_fracties.frac_id%TYPE
    )
    IS
      SELECT mons.kafd_id
            ,mons.ongr_id
			,pers.zisnr
        FROM bas_fracties frac
		,    kgc_monsters mons
		,    kgc_personen pers
       WHERE mons.mons_id = frac.mons_id
	   and   frac.frac_id = b_frac_id
	   and   mons.pers_id = pers.pers_id
	   ;
  BEGIN
    OPEN c_frac (p_entiteit_pk);

    FETCH c_frac
     INTO p_kafd_id
         ,p_ongr_id
		 ,p_zisnr;

    CLOSE c_frac;
  END kafd_id_ongr_id_frac;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_GESP
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_gesp (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_gesp (
      b_gesp_id   kgc_gesprekken.gesp_id%TYPE
    )
    IS
      SELECT onde.kafd_id
            ,gesp.ongr_id
            ,pers.zisnr
        FROM kgc_gesprekken  gesp
        ,    kgc_onderzoeken onde
        ,    kgc_personen    pers
       WHERE gesp.onde_id = onde.onde_id
	   and   gesp.gesp_id = b_gesp_id
	   and   onde.pers_id = pers.pers_id(+)
	   ;
  BEGIN
    OPEN c_gesp (p_entiteit_pk);

    FETCH c_gesp
     INTO p_kafd_id
         ,p_ongr_id
         ,p_zisnr;

    CLOSE c_gesp;
  END kafd_id_ongr_id_gesp;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_GEBE
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_gebe (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_gebe (
      b_gebe_id   kgc_gesprek_betrokkenen.gebe_id%TYPE
    )
    IS
      SELECT onde.kafd_id
            ,gesp.ongr_id
            ,pers.zisnr
        FROM kgc_gesprek_betrokkenen gebe
        ,    kgc_gesprekken  gesp
        ,    kgc_onderzoeken onde
        ,    kgc_personen    pers
       WHERE gesp.onde_id = onde.onde_id
	   and   gesp.gesp_id = gebe.gesp_id
	   and   gebe.gebe_id = b_gebe_id
	   and   gebe.pers_id = pers.pers_id(+)
	   ;
  BEGIN
    OPEN c_gebe (p_entiteit_pk);

    FETCH c_gebe
     INTO p_kafd_id
         ,p_ongr_id
         ,p_zisnr;

    CLOSE c_gebe;
  END kafd_id_ongr_id_gebe;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_DIAG
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_diag (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_diag (
      b_diag_id   kgc_diagnoses.diag_id%TYPE
    )
    IS
      SELECT onde.kafd_id
            ,onde.ongr_id
			,pers.zisnr
        FROM kgc_onderzoek_betrokkenen onbe
		,    kgc_onderzoeken onde
		,    kgc_diagnoses diag
		,    kgc_personen  pers
       WHERE diag.onbe_id = onbe.onbe_id
	   and   onbe.onde_id = onde.onde_id
	   and   diag.diag_id = b_diag_id
	   and   diag.pers_id = pers.pers_id
	   ;
  BEGIN
    OPEN c_diag (p_entiteit_pk);

    FETCH c_diag
     INTO p_kafd_id
         ,p_ongr_id
		 ,p_zisnr;

    CLOSE c_diag;
  END kafd_id_ongr_id_diag;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_ONBE
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie
	   1.1        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_onbe (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id       OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr         OUT      kgc_personen.zisnr%TYPE
  )
  IS
    CURSOR c_onbe (
      b_onbe_id   kgc_onderzoek_betrokkenen.onbe_id%TYPE
    )
    IS
      SELECT onde.kafd_id
            ,onde.ongr_id
			,pers.zisnr
        FROM kgc_onderzoek_betrokkenen onbe
		,    kgc_onderzoeken onde
		,    kgc_personen    pers
       WHERE onbe.onde_id = onde.onde_id
	   and   onbe.onbe_id = b_onbe_id
	   and   onbe.pers_id = pers.pers_id
	   ;
  BEGIN
    OPEN c_onbe (p_entiteit_pk);

    FETCH c_onbe
     INTO p_kafd_id
         ,p_ongr_id
		 ,p_zisnr;

    CLOSE c_onbe;
  END kafd_id_ongr_id_onbe;

  /******************************************************************************
       NAME:       kafd_id_ongr_id_WLST
       PURPOSE:    bepaal kafd_id/ongr_id voor entiteit uits opbv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        08-06-2006    YAR             Creatie

       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_wlst (
    p_entiteit_pk   IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id       OUT      kgc_kgc_afdelingen.kafd_id%TYPE
  )
  IS
    CURSOR c_wlst (
      b_wlst_id   kgc_werklijsten.wlst_id%TYPE
    )
    IS
      SELECT wlst.kafd_id
        FROM kgc_werklijsten wlst
       WHERE wlst.wlst_id = b_wlst_id
	   ;
  BEGIN
    OPEN c_wlst (p_entiteit_pk);

    FETCH c_wlst
     INTO p_kafd_id;

    CLOSE c_wlst;
  END kafd_id_ongr_id_wlst;


  /******************************************************************************
       NAME:       kafd_id_ongr_id_van_entiteit
       PURPOSE:    bepaal kafd_id voor entiteit voor een enteit
                    obv meegegeven pk waarde

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        19-05-2006    HZO             Creatie
       1.1        05-06-2006    YAR             Melding: Word, aanroepen naar nieuwe functies
	   1.2        06-07-2006    YAR             Melding: HLX-KGC_NUST_00-11417
       NOTES:

    *****************************************************************************/
  PROCEDURE kafd_id_ongr_id_van_entiteit (
    p_entiteit_code   IN       kgc_bestanden.entiteit_code%TYPE
   ,p_entiteit_pk     IN       kgc_bestanden.entiteit_pk%TYPE
   ,p_kafd_id         OUT      kgc_kgc_afdelingen.kafd_id%TYPE
   ,p_ongr_id         OUT      kgc_onderzoeksgroepen.ongr_id%TYPE
   ,p_zisnr           OUT      kgc_personen.zisnr%TYPE
  )
  IS
  BEGIN
    CASE p_entiteit_code
      WHEN 'OBMG'
      THEN
        kafd_id_ongr_id_obmg (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'UITS'
      THEN
        kafd_id_ongr_id_uits (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'ONDE'
      THEN
        kafd_id_ongr_id_onde (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'MONS'
      THEN
        kafd_id_ongr_id_mons (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'FRAC'
      THEN
        kafd_id_ongr_id_frac (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'GESP'
      THEN
        kafd_id_ongr_id_gesp (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'GEBE'
      THEN
        kafd_id_ongr_id_gebe (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'DIAG'
      THEN
        kafd_id_ongr_id_diag (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'ONBE'
      THEN
        kafd_id_ongr_id_onbe (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             ,p_ongr_id         => p_ongr_id
                             ,p_zisnr           => p_zisnr
                             );
      WHEN 'WLST'
      THEN
        kafd_id_ongr_id_wlst (p_entiteit_pk     => p_entiteit_pk
                             ,p_kafd_id         => p_kafd_id
                             );
    END CASE;
  END kafd_id_ongr_id_van_entiteit;

  /******************************************************************************
      NAME:       get_document_gegevens
      PURPOSE:    haal alle relevante gegevens op voor het aanmaken van een document

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        6-4-2006    HZO              Creatie
      1.1        18-05-2006  HZO              Diverse aanpassingen n.a.v test
      1.2        23-06-2006  YAR              als p_ongr_id leeg is, gaat het fout
      1.3        01-07-2010  JKO              Naamgeving parameter verbeterd
      NOTES:

   *****************************************************************************/
  FUNCTION get_document_gegevens (
    p_btyp_id   IN   kgc_bestand_types.btyp_id%TYPE
   ,p_kafd_id   IN   kgc_model_bestanden.kafd_id%TYPE
   ,p_ongr_id   IN   kgc_model_bestanden.ongr_id%TYPE
  )
    RETURN t_document_gegevens
  IS
    v_document_gegevens   t_document_gegevens;
    e_geen_modelbestand   EXCEPTION;

    CURSOR c_btyp (
      b_btyp_id   kgc_bestand_types.btyp_id%TYPE
     ,b_kafd_id   kgc_model_bestanden.kafd_id%TYPE
     ,b_ongr_id   kgc_model_bestanden.ongr_id%TYPE
    )
    IS
      SELECT   btyp.standaard_extentie btyp_standaard_extentie
              ,btyp.standaard_pad btyp_standaard_pad
              ,btyp.view_name
              ,btyp.ind_registreren_als_brief
              ,enti.code entiteit_code
              ,enti.pk_kolom entiteit_pk_kolom
              ,mobe.standaard_extentie mobe_standaard_extentie
              ,mobe.standaard_pad mobe_standaard_pad
              ,mobe.bestand_specificatie
              ,xdoc.procedure_naam
              ,nust.code nust_code
          FROM kgc_bestand_types btyp
              ,kgc_entiteiten enti
              ,kgc_model_bestanden mobe
              ,kgc_xml_documenten xdoc
              ,kgc_nummerstructuur nust
         WHERE enti.code = btyp.enti_code
           AND btyp.btyp_id = mobe.btyp_id
           AND nust.nust_id = mobe.nust_id
           AND xdoc.xdoc_id = mobe.xdoc_id
           AND btyp.btyp_id = b_btyp_id
           AND mobe.kafd_id = b_kafd_id
           AND (mobe.ongr_id = b_ongr_id OR
                (b_ongr_id IS NULL AND mobe.ongr_id IS NULL) OR
                mobe.ongr_id IS NULL)
      ORDER BY mobe.ongr_id;

    v_stap                VARCHAR2 (100);
    -- bij meerdere records is alleen record met gevulde ongr_id relevant
    r_btyp                c_btyp%ROWTYPE;
  BEGIN
    v_stap := 'openen cursor';

    OPEN c_btyp (p_btyp_id
                ,p_kafd_id
                ,p_ongr_id
                );

    FETCH c_btyp
     INTO r_btyp;

    IF c_btyp%NOTFOUND
    THEN
      CLOSE c_btyp;

      RAISE e_geen_modelbestand;
    END IF;

    CLOSE c_btyp;

    v_stap := 'document gegevens bepalen';
    v_document_gegevens.procedure_naam := r_btyp.procedure_naam;

    IF r_btyp.mobe_standaard_pad IS NOT NULL
    THEN
      -- gedefnieerd op mobe niveau
      v_document_gegevens.standaard_pad := r_btyp.mobe_standaard_pad;
    ELSE
      v_document_gegevens.standaard_pad := r_btyp.btyp_standaard_pad;
    END IF;

    IF r_btyp.mobe_standaard_extentie IS NOT NULL
    THEN
      v_document_gegevens.standaard_extentie :=
                                               r_btyp.mobe_standaard_extentie;
    ELSE
      v_document_gegevens.standaard_extentie :=
                                               r_btyp.btyp_standaard_extentie;
    END IF;

    -- zorg dat er altijd een backslash aan het eind staat
    v_document_gegevens.standaard_pad :=
                          RTRIM (v_document_gegevens.standaard_pad, '\')
                          || '\';
    v_document_gegevens.bestand_specificatie := r_btyp.bestand_specificatie;
    v_document_gegevens.nummerstructuur := r_btyp.nust_code;
    v_document_gegevens.procedure_naam := r_btyp.procedure_naam;
    v_document_gegevens.view_name := r_btyp.view_name;
    v_document_gegevens.ind_registreren_als_brief :=
                                              nvl(r_btyp.ind_registreren_als_brief,'N');
    v_document_gegevens.entiteit_code := r_btyp.entiteit_code;
    v_document_gegevens.entiteit_pk_kolom := r_btyp.entiteit_pk_kolom;
    RETURN v_document_gegevens;
  EXCEPTION
    WHEN e_geen_modelbestand
    THEN
      qms$errors.show_message (p_mesg     => 'KGC-00215');
    WHEN OTHERS
    THEN
      qms$errors.show_message
                            (p_mesg       => 'KGC-00213'
                            ,p_param0     => v_stap
                            ,p_param1     => 'KGC_WORD_00.get_document_gegevens'
                            ,p_param2     => sqlerrm
                            );
  END get_document_gegevens;
END kgc_word_00;
/

/
QUIT
