CREATE OR REPLACE PACKAGE "HELIX"."KGC_MONI_00" IS
-- zet monitor aan of uit: vastleggen in systeemparameter
PROCEDURE schakelaar
( p_monitor IN VARCHAR2 := 'MODULE'
, p_aan_uit IN VARCHAR2 -- AAN, UIT
);

-- wordt er gemonitord?
FUNCTION monitoring_aan
( p_monitor IN VARCHAR2 := 'MODULE'
)
RETURN BOOLEAN;

-- als aan dan uit; als uit dan aan
PROCEDURE schakel
( p_monitor IN VARCHAR2 := 'MODULE'
);

-- tabel leegmaken
PROCEDURE schoon
( p_monitor IN VARCHAR2 := 'MODULE'
);

-- registreer het open en sluiten van modules
PROCEDURE registreer
( p_user IN VARCHAR2 := NULL
, p_module IN VARCHAR2
, p_actie IN VARCHAR2  -- OPEN, SLUIT
);
END KGC_MONI_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MONI_00" IS
PROCEDURE schakelaar
( p_monitor IN VARCHAR2 := 'MODULE'
, p_aan_uit IN VARCHAR2 -- AAN, UIT
)
IS
BEGIN
  UPDATE kgc_systeem_parameters
  SET    standaard_waarde = UPPER( p_aan_uit )
  WHERE  code = 'MODULE_MONITOR'
  ;
  IF ( sql%rowcount = 0 )
  THEN
    INSERT INTO kgc_systeem_parameters
    ( code
    , omschrijving
    , standaard_waarde
    )
    VALUES
    ( 'MODULE_MONITOR'
    , 'Schakelaar om het monitoren van modulegebruik aan of uit te zetten (AAN/UIT)'
    , UPPER( p_aan_uit )
    );
  END IF;
  COMMIT;
END schakelaar;

-- wordt er gemonitord?
FUNCTION monitoring_aan
( p_monitor IN VARCHAR2 := 'MODULE'
)
RETURN BOOLEAN
IS
  CURSOR sypa_cur
  IS
    SELECT standaard_waarde
    FROM   kgc_systeem_parameters
    WHERE  code = 'MODULE_MONITOR'
    ;
  v_waarde VARCHAR2(100);
BEGIN
  OPEN  sypa_cur;
  FETCH sypa_cur
  INTO  v_waarde;
  CLOSE sypa_cur;
  IF ( UPPER( v_waarde ) = 'AAN' )
  THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END monitoring_aan;

PROCEDURE schakel
( p_monitor IN VARCHAR2 := 'MODULE'
)
IS
BEGIN
  -- in null-situatie UIT
  IF ( NOT monitoring_aan( p_monitor => p_monitor ) )
  THEN
    schakelaar
    ( p_monitor => p_monitor
    , p_aan_uit => 'AAN'
    );
  ELSE
    schakelaar
    ( p_monitor => p_monitor
    , p_aan_uit => 'UIT'
    );
  END IF;
END schakel;

PROCEDURE schoon
( p_monitor IN VARCHAR2 := 'MODULE'
)
IS
BEGIN
  DELETE FROM kgc_monitoring
  WHERE  monitor = UPPER( p_monitor )
  ;
  COMMIT;
END schoon;

PROCEDURE registreer
( p_user IN VARCHAR2 := NULL
, p_module IN VARCHAR2
, p_actie IN VARCHAR2  -- OPEN, SLUIT
)
IS
  moni_trow kgc_monitoring%rowtype;
  v_user     VARCHAR2(40) := USER;
  v_count    NUMBER:=0;      --Added by Rajendra for Mantis 7918
BEGIN
  IF upper(p_module) not in ('QMS0002F'
                            ,'QMS0008F'
                            ,'QMS0010F'
                            ,'QMS0020F')
  THEN
    IF ( monitoring_aan( p_monitor => 'MODULE' ) )
    THEN
      -- registreer het open en sluiten van modules
      moni_trow.monitor := 'MODULE';
      moni_trow.tijdstip := SYSDATE;
      moni_trow.oracle_uid := NVL( p_user, v_user );
      moni_trow.mede_id := kgc_mede_00.medewerker_id ( p_user => moni_trow.oracle_uid );
      moni_trow.module := UPPER( p_module );
      moni_trow.actie := p_actie;

      --Added by Rajendra for Mantis 7918
      SELECT COUNT(*)
      INTO v_count
      FROM kgc_monitoring
      WHERE monitor=moni_trow.monitor
      AND oracle_uid=moni_trow.oracle_uid
      AND tijdstip=moni_trow.tijdstip
      AND mede_id=moni_trow.mede_id
      AND module=moni_trow.module
      AND actie=moni_trow.actie;

    IF v_count=0
      THEN
      INSERT INTO kgc_monitoring
      ( monitor
      , oracle_uid
      , tijdstip
      , mede_id
      , module
      , actie
      )
      VALUES
      ( moni_trow.monitor
      , moni_trow.oracle_uid
      , moni_trow.tijdstip
      , moni_trow.mede_id
      , moni_trow.module
      , moni_trow.actie
      );
      COMMIT;
    END IF;
    ELSE
      NULL; -- nog niks anders
    END IF;
  END IF;
END registreer;
END kgc_moni_00;
/

/
QUIT
