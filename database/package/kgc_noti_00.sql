CREATE OR REPLACE PACKAGE "HELIX"."KGC_NOTI_00" IS
-- Haal een omschrijving op van een entiteit-rij
-- dat als basis dient voor vertalingen
FUNCTION basis_omschrijving
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
)
RETURN VARCHAR2;

-- Verwijder notities zonder relatie:
-- via kolom id. en entiteit zijn rijen logisch gerelateerd aan masters
-- maar niet fysiek.
PROCEDURE schonen
( p_entiteit IN VARCHAR2 := NULL
);
END KGC_NOTI_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_NOTI_00" IS
FUNCTION basis_omschrijving
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
)
RETURN VARCHAR2
IS
  --v_return VARCHAR2(100) := NULL; Commented by Rajendra for Mantis 0182 Rework (12-08-2013)
  v_return VARCHAR2(32767) := NULL; --Added by Rajendra for Mantis 0182 Rework (12-08-2013)
  CURSOR enti_cur
  IS
    SELECT tabel
    ,      pk_kolom
    ,      omschrijving_kolommen
    FROM   kgc_entiteiten
    WHERE  code = p_entiteit
    ;
  enti_rec enti_cur%rowtype;
  v_statement VARCHAR2(1000);
BEGIN
  OPEN  enti_cur;
  FETCH enti_cur
  INTO  enti_rec;
  CLOSE enti_cur;
  v_statement := 'SELECT '||enti_rec.omschrijving_kolommen||CHR(10)
              || 'INTO :result'||CHR(10)
              || 'FROM '||enti_rec.tabel||CHR(10)
              || 'WHERE '||NVL(enti_rec.pk_kolom,p_entiteit||'_ID')||' = '||TO_CHAR(p_id)||CHR(10)
               ;
  v_return := kgc_util_00.dyn_exec( v_statement );
  RETURN( v_return );
END basis_omschrijving;

PROCEDURE schonen
( p_entiteit IN VARCHAR2 := NULL
)
IS
  CURSOR enti_cur
  IS
    SELECT code
    ,      tabel
    ,      pk_kolom
    FROM   kgc_entiteiten
    WHERE  code = NVL( p_entiteit, code )
    ;
  v_statement VARCHAR2(1000);
BEGIN
  FOR enti_rec IN enti_cur
  LOOP
    v_statement := 'DELETE FROM KGC_NOTITIES noti'||CHR(10)
                || 'WHERE noti.entiteit = '''||enti_rec.code||''''||CHR(10)
                || 'AND NOT EXISTS'||CHR(10)
                || '( SELECT NULL FROM '||enti_rec.tabel||' mtab'||CHR(10)
                || '  WHERE mtab.'||NVL(enti_rec.pk_kolom,enti_rec.code||'_ID')|| ' = noti.id'||CHR(10)
                || ')'
                 ;
    kgc_util_00.dyn_exec( v_statement );
  END LOOP;

END schonen;
END kgc_noti_00;
/

/
QUIT
