CREATE OR REPLACE PACKAGE "HELIX"."KGC_INTERFACE" IS
-- Package Specification
/*
   Wijzigingshistorie:
   ===================

   Bevindingnr	Wie	Wanneer
   Wat
   ---------------------------------------------------------------------------------------------------------
   Mantis 3146	GWE	2010-01-13
   - zoeken naar standaard waarden aangepast
*/

-- procedures en functions uit deze package kunnen aangeroepen worden aangeroepen
-- vanuit de helix-applicatie.
-- Van hieruit worden procedures en functions aangeroepen uit de betreffende API-package
--
-- Zet data over van Helix naar ... (SQLLIMS)
PROCEDURE tray_uit
( p_trge_id IN NUMBER
, p_interface IN VARCHAR2
);
-- is meetwaarde afgerond aan gene zijde?
-- p_meet_id leeg: alle openstaande metingen
PROCEDURE meetwaarde_afgerond
( p_kafd_id IN NUMBER
, p_meet_id IN NUMBER := NULL
);
-- construeer standaardwaarde (projectnaam,user_sample_id)
FUNCTION standaard_waarde
( p_meta_type IN VARCHAR2
, p_trge_id IN NUMBER := NULL
, p_trvu_id IN NUMBER := NULL
)
RETURN VARCHAR2;

-- haal gegevens op uit het ZIS
-- in de body kan de gewenste koppeling (ppip, patcomp, hiscom, ...)
PROCEDURE persoon_in_zis
( p_zisnr IN VARCHAR2
, pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
);

-- bepaal aanspreken obv basis-gegevens EN zis_aanspreken
PROCEDURE standaard_aanspreken_zis
( p_voorletters  IN  VARCHAR2  :=  NULL
, p_voorvoegsel  IN  VARCHAR2  :=  NULL
, p_achternaam   IN  VARCHAR2  :=  NULL
, p_achternaam_partner IN VARCHAR2 := NULL
, p_voorvoegsel_partner IN VARCHAR2 := NULL
, p_zis_aanspreeknaam IN VARCHAR2 := NULL
, p_geslacht     IN VARCHAR2 := 'O'
, x_default_aanspreken IN OUT VARCHAR2
, x_gehuwd       IN  OUT VARCHAR2
, x_aanspreken   IN  OUT  VARCHAR2
);

FUNCTION STOFTEST_SUBSTRING
( p_stoftest_omschrijving varchar2
, p_deel varchar2)
RETURN VARCHAR2
;
END KGC_INTERFACE;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_INTERFACE" IS
-- lokaal
function datum
( p_dat in varchar2
)
return date
is
  v_return date;
begin
  begin
    v_return := to_date( p_dat, 'dd-mm-yyyy' );
  exception
    when others then
      v_return := null;
  end;
  return( v_return );
end datum;

FUNCTION tray_meta_waarde
( b_meta_type IN VARCHAR2
, b_trge_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_waarde VARCHAR2(100) := NULL;
  CURSOR tifd_cur
  IS
    SELECT waarde
    FROM   kgc_tray_interface_data
    WHERE  trge_id = b_trge_id
    AND    meta_type = b_meta_type
    ;
BEGIN
  OPEN  tifd_cur;
  FETCH tifd_cur
  INTO  v_waarde;
  CLOSE tifd_cur;
  RETURN( v_waarde );
END tray_meta_waarde;

FUNCTION formatteer_bestandsnaam
( p_naam IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_set VARCHAR2(100) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  v_replacement VARCHAR2(1) := '_';
  v_teken VARCHAR2(1);
  result VARCHAR2(32767) := '';
BEGIN
  FOR i IN 1..LENGTH(p_naam)
  LOOP
    v_teken := SUBSTR( p_naam,i,1);
    IF ( INSTR( v_set, v_teken ) <> 0 )
    THEN
      IF ( result IS NULL )
      THEN
        result := v_teken;
      ELSE
        result := result || v_teken;
      END IF;
    ELSE
      IF ( result IS NULL )
      THEN
        result := v_replacement;
      ELSE
        result := result || v_replacement;
      END IF;
    END IF ;
  END LOOP ;
  RETURN( result );
END formatteer_bestandsnaam;

-- publiek
PROCEDURE tray_uit
( p_trge_id IN NUMBER
, p_interface IN VARCHAR2
)
IS
  CURSOR trge_cur
  IS
    SELECT trty.horizontaal
    ,      trty.verticaal
    ,      mede.vms_username ext_mede_code
    ,      trge.gefixeerd
    ,      trge.externe_id
    FROM   kgc_tray_types trty
    ,      kgc_medewerkers mede
    ,      kgc_tray_gebruik trge
    WHERE  trge.trty_id = trty.trty_id
    AND    trge.mede_id = mede.mede_id
    AND NVL (trty.vervallen, 'N') = 'N' --Added for Mantis 2831 , ASI
    AND    trge.trge_id = p_trge_id
    ;
  trge_rec trge_cur%rowtype;
  v_date_logged DATE := NULL;
  -- overige parameterwaarden
  CURSOR tifd_cur
  IS
    SELECT tifd.meta_type
    ,      tifd.waarde
    FROM   kgc_tray_interface_data tifd
    WHERE  tifd.trge_id = p_trge_id
    AND    tifd.meta_type NOT IN ( 'PROJECT'
                                 , 'COLOR_INFO1'
                                 , 'COLOR_COMMENT1'
                                 , 'COLOR_INFO2'
                                 , 'COLOR_COMMENT2'
                                 , 'COLOR_INFO3'
                                 , 'COLOR_COMMENT3'
                                 , 'COLOR_INFO4'
                                 , 'COLOR_COMMENT4'
                                 , 'COLOR_INFO5'
                                 , 'COLOR_COMMENT5'
                                 )
    ;
  CURSOR trvu_cur
  IS
    SELECT trvu.trvu_id
    ,      trvu.meet_id
    ,      trvu.x_positie
    ,      trvu.y_positie
    FROM   kgc_tray_vulling trvu
    WHERE  trvu.trge_id = p_trge_id
    AND    trvu.z_positie = 0
    ORDER BY LPAD (trvu.y_positie, 5, '0'),
                   LPAD (trvu.x_positie, 5, '0') /*Changes done for Mantis 2658*/
                                                        ;

  v_sample_name VARCHAR2(100);

  v_plate_id NUMBER;
  v_plate_name varchar2(100); -- kgc_tray_gebruik.externe_id
  v_fout BOOLEAN := FALSE;
  v_melding VARCHAR2(4000) := NULL;
  e_fout EXCEPTION;
  v_color_info1 kgc_onderzoeken.onderzoeknr%type;    --aanpassing Yuri Mantis 4923
  v_color_info2 bas_meetwaarden.meet_id%type;    --aanpassing Yuri Mantis 4970  01-12-2010

  v_toegewezen_interface varchar2(20); -- mantisnr 8272

BEGIN
-- begin aanpassing HBO 03-04-2013  (mantisnr 8272)
  -- toegevoegd naar aanleiding van labv implementatie
  -- alle technieken met attribuut waarden LABV worden
  -- naar labvantage applicatie verstuurd.
  if sysdate > to_date('19-06-2015 15:00:00', 'dd-mm-yyyy HH24:MI:SS') then  -- ipn datum Labvantage

    select kgc_attr_00.waarde('KGC_TECHNIEKEN', 'TOEGEWEZEN_INTERFACE', (select tech_id from kgc_tray_gebruik where trge_id = p_trge_id))
    into v_toegewezen_interface
    from dual
    ;

    if v_toegewezen_interface = 'LABV' then
        beh_labv_00.call_processaction( p_trge_id => p_trge_id);
        return;
    else
      return;
    end if;
  end if;
  -- eind aanpassing HBO 03-04-2013

  IF ( p_interface = 'SQLLIMS' )
  THEN
    -- gebruik gegevens in kgc_tray_interface_data
    -- maak een welplate aan: aaroep sqllims-API
    -- toon eventuele fout
    -- registreer teruggekomen externe_id ( plate_id )
    OPEN  trge_cur;
    FETCH trge_cur
    INTO  trge_rec;
    CLOSE trge_cur;
    -- controles
    IF ( trge_rec.externe_id IS NOT NULL )
    THEN
      v_melding := 'Tray is al eerder overgezet.'
                || CHR(10)
                || 'De externe id is bij dit tray-gebruik ingevuld.'
                 ;
      RAISE e_fout;
    END IF;
    -- einde controles
    IF ( trge_rec.gefixeerd = 'J' )
    THEN
      v_date_logged := SYSDATE;
    ELSE
      v_date_logged := NULL;
    END IF;
    kgc_sqllims.customer_well_plate
    ( p_max_columns => trge_rec.horizontaal
    , p_max_rows => trge_rec.verticaal
    , p_project => tray_meta_waarde( 'PROJECT', p_trge_id )
    , p_color_info1 => tray_meta_waarde( 'COLOR_INFO1', p_trge_id )
    , p_color_comment1 => tray_meta_waarde( 'COLOR_COMMENT1', p_trge_id )
    , p_color_info2 => tray_meta_waarde( 'COLOR_INFO2', p_trge_id )
    , p_color_comment2 => tray_meta_waarde( 'COLOR_COMMENT2', p_trge_id )
    , p_color_info3 => tray_meta_waarde( 'COLOR_INFO3', p_trge_id )
    , p_color_comment3 => tray_meta_waarde( 'COLOR_COMMENT3', p_trge_id )
    , p_color_info4 => tray_meta_waarde( 'COLOR_INFO4', p_trge_id )
    , p_color_comment4 => tray_meta_waarde( 'COLOR_COMMENT4', p_trge_id )
    , p_color_info5 => tray_meta_waarde( 'COLOR_INFO5', p_trge_id )
    , p_color_comment5 => tray_meta_waarde( 'COLOR_COMMENT5', p_trge_id )
    , p_userstamp => trge_rec.ext_mede_code
    , p_date_logged => v_date_logged
    , x_plate_id => v_plate_id
    , x_plate_name => v_plate_name
    , x_error => v_fout
    , x_error_text => v_melding
    );
    IF ( v_fout )
    THEN
      RAISE e_fout;
    END IF;
    v_plate_id := NVL( v_plate_id, p_trge_id );
    BEGIN
      UPDATE kgc_tray_gebruik
      SET    externe_id = nvl( v_plate_name, 'ID='||TO_CHAR(v_plate_id) )
      WHERE  trge_id = p_trge_id
      ;
    END;
    -- overige parameters
    FOR r IN tifd_cur
    LOOP
      kgc_sqllims.plate_attribute
      ( p_plate_id => v_plate_id
      , p_name => r.meta_type
      , p_value => r.waarde
      , x_error => v_fout
      , x_error_text => v_melding
      );
      IF ( v_fout )
      THEN
        v_melding := 'Fout bij toevoegen parameter '||r.meta_type||CHR(10)||SQLERRM;
        RAISE e_fout;
      END IF;
    END LOOP;

    FOR r IN trvu_cur
    LOOP
      -- genereer een sample_id
      -- maak een sample aan per trvu-rij: aanroep sqllims-API
      v_sample_name := standaard_waarde
                       ( p_meta_type => 'SAMPLE#'
                       , p_trge_id => p_trge_id
                       , p_trvu_id => r.trvu_id
                       );
      --aanpassing Yuri Mantis 4923 begin
      v_color_info1 := standaard_waarde
                           ( p_meta_type => 'COLOR_INFO1'
                           , p_trge_id => p_trge_id
                           , p_trvu_id => r.trvu_id
                           );
      --aanpassing Yuri Mantis 4923 eind

      --aanpassing Yuri Mantis 4970 begin
      v_color_info2 := standaard_waarde
                           ( p_meta_type => 'COLOR_INFO2'
                           , p_trge_id => p_trge_id
                           , p_trvu_id => r.trvu_id
                           );
      --aanpassing Yuri Mantis 4970 eind

      kgc_sqllims.customer_sample
      ( p_sample_name => v_sample_name
      , p_sample_comment => NULL
      , p_plate_id => v_plate_id
      , p_column => r.x_positie
      , p_row => r.y_positie
      --aanpassing Yuri Mantis 4923, deze regel weg => , p_color_info1 => tray_meta_waarde( 'COLOR_INFO1', p_trge_id )
      , p_color_info1 => v_color_info1 --aanpassing Yuri Mantis 4923
      , p_color_comment1 => tray_meta_waarde( 'COLOR_COMMENT1', p_trge_id )
      --aanpassing Yuri Mantis 4970, deze regel weg => , p_color_info2 => tray_meta_waarde( 'COLOR_INFO2', p_trge_id )
      , p_color_info2 => v_color_info2 --aanpassing Yuri Mantis 4970
      , p_color_comment2 => tray_meta_waarde( 'COLOR_COMMENT2', p_trge_id )
      , p_color_info3 => tray_meta_waarde( 'COLOR_INFO3', p_trge_id )
      , p_color_comment3 => tray_meta_waarde( 'COLOR_COMMENT3', p_trge_id )
      , p_color_info4 => tray_meta_waarde( 'COLOR_INFO4', p_trge_id )
      , p_color_comment4 => tray_meta_waarde( 'COLOR_COMMENT4', p_trge_id )
      , p_color_info5 => tray_meta_waarde( 'COLOR_INFO5', p_trge_id )
      , p_color_comment5 => tray_meta_waarde( 'COLOR_COMMENT5', p_trge_id )
      , p_userstamp => trge_rec.ext_mede_code
      , x_error => v_fout
      , x_error_text => v_melding
      );
      IF ( v_fout )
      THEN
        RAISE e_fout;
      END IF;
      BEGIN
        INSERT INTO kgc_samples
        ( meet_id
        , prompt
        , naam
        )
        VALUES
        ( r.meet_id
        , 'Sample'
        , v_sample_name
        );
      EXCEPTION
        WHEN OTHERS
        THEN
          v_melding := SUBSTR('Sample niet geregistreerd als bestand.'||CHR(10)||SQLERRM,1,4000);
          RAISE e_fout;
      END;
    END LOOP;
    COMMIT;
  END IF;
EXCEPTION
  WHEN e_fout
  THEN
    ROLLBACK;
    qms$errors.show_message
    ( p_mesg => 'KGC-00081'
    , p_errtp => 'E'
    , p_rftf => TRUE
    , p_param0 => v_melding
    , p_param1 => NULL
    );
  WHEN OTHERS
  THEN
    ROLLBACK;
    qms$errors.show_message
    ( p_mesg => 'KGC-00081'
    , p_errtp => 'E'
    , p_rftf => TRUE
    , p_param0 => 'Onbekende fout'
    , p_param1 => SQLERRM
    );
END tray_uit;

PROCEDURE meetwaarde_afgerond
( p_kafd_id IN NUMBER
, p_meet_id IN NUMBER := NULL
)
IS
  CURSOR meet_cur
  IS
    SELECT meet.meet_id
    FROM   kgc_stoftesten stof -- enkel en alleen voor bepalen kafd
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meet.afgerond = 'N'
    AND    meti.afgerond = 'N'
    AND    meet.stof_id = stof.stof_id
    AND    stof.kafd_id = p_kafd_id
    AND    meet.meet_id = NVL( p_meet_id, meet.meet_id )
    ;
  CURSOR samp_cur
  ( b_meet_id IN NUMBER
  )
  IS
    SELECT naam
    FROM   kgc_samples
    WHERE  meet_id = b_meet_id
    ;
  v_afgerond VARCHAR2(1);
  v_meetwaarde_afgerond VARCHAR2(1);
  v_waarde VARCHAR2(100);
BEGIN
  FOR meet_rec IN meet_cur
  LOOP
    v_meetwaarde_afgerond := 'J';
    v_afgerond := NULL;
    -- 1 niet afgeronde sample = meetwaarde niet afgerond
    FOR samp_rec IN samp_cur( meet_rec.meet_id )
    LOOP
      v_afgerond := 'N';
      kgc_sqllims.sample_complete
      ( p_sample_id => samp_rec.naam
      , x_complete => v_afgerond
      , x_value => v_waarde
      );
      IF ( v_afgerond = 'N' )
      THEN
        v_meetwaarde_afgerond := 'N';
      END IF;
    END LOOP;
    IF ( v_afgerond IS NOT NULL ) -- in tweede loop geweest
    THEN
      IF ( v_meetwaarde_afgerond = 'J' )
      THEN
        BEGIN
          -- meetwaarde wordt vooralsnog niet aan helix teruggegeven
          -- als dat wel gebeurt, moet bekeken worden hoe (overschrijven?, details vullen?)
          UPDATE bas_meetwaarden
          SET    afgerond = 'J'
          ,      meetwaarde = NVL( meetwaarde, v_waarde )
          WHERE  meet_id = meet_rec.meet_id
          ;
        END;
      END IF;
    END IF;
  END LOOP;
END meetwaarde_afgerond;

FUNCTION standaard_waarde
( p_meta_type IN VARCHAR2
, p_trge_id IN NUMBER := NULL
, p_trvu_id IN NUMBER := NULL
)
RETURN VARCHAR2
IS
  v_waarde VARCHAR2(100) := NULL;
  cursor trvu_cur
  is
    select trty.kafd_id
    from   kgc_tray_types trty
    ,      kgc_tray_gebruik trge
    ,      kgc_tray_vulling trvu
    where  trge.trty_id = trty.trty_id
    and    trge.trge_id = trvu.trge_id
    AND NVL (trty.vervallen, 'N') = 'N' --Added for Mantis 2831 , ASI
    and    trvu.trvu_id = p_trvu_id
    ;
  v_kafd_id number;
  v_sypa_waarde varchar2(1000);
  v_stoftest_samplenaam varchar2(100);
  -- 14-10-2009 YA Onderzoeknr toegevoegd --aanpassing Yuri Mantis 4923
  cursor meet_cur
  is
    SELECT frac.fractienummer
    ,      stof.stof_id
    ,      stof.code stof_code
    ,      stof.omschrijving stof_omschrijving
    ,      onde.onderzoeknr onderzoeknr --aanpassing Yuri Mantis 4923
    ,      meet.meet_id meet_id --aanpassing Yuri Mantis 4970
    FROM   kgc_stoftesten stof
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_tray_vulling trvu
    ,      kgc_onderzoeken onde --aanpassing Yuri Mantis 4923
    WHERE  trvu.meet_id = meet.meet_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    meet.stof_id = stof.stof_id
    AND    trvu.trvu_id = p_trvu_id
    AND    meti.onde_id = onde.onde_id(+) --aanpassing Yuri Mantis 4923
    UNION
    SELECT stan.standaardfractienummer
    ,      stof.stof_id
    ,      stof.code stof_code
    ,      stof.omschrijving stof_omschrijving
    ,      onde.onderzoeknr onderzoeknr --aanpassing Yuri Mantis 4923
    ,      meet.meet_id meet_id --aanpassing Yuri Mantis 4970
    FROM   kgc_stoftesten stof
    ,      bas_standaardfracties stan
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_tray_vulling trvu
    ,      kgc_onderzoeken onde --aanpassing Yuri Mantis 4923
    WHERE  trvu.meet_id = meet.meet_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.stan_id = stan.stan_id
    AND    meet.stof_id = stof.stof_id
    AND    trvu.trvu_id = p_trvu_id
    AND    meti.onde_id = onde.onde_id(+) --aanpassing Yuri Mantis 4923
    ;
  meet_rec meet_cur%rowtype;

  -- 08-05-2006 nieuwe bepaling project default waarde:.
  -- projectnaam: techniek_SQLLIMSafdeling_gen
  -- op de tray behoren volgens de DNA standaardwerkwijze stoftesten
  -- met ? techniek en ? gen te staan. Met deze waardes wordt
  -- de waarde van ? Project uit KGC_SQLLIMS_PROJECTS opgezocht.
  -- De afdelingen in SQLLIMS zijn komen niet (alle?) overeen met HELIX-afdelingen
  -- Als meerdere regels gevonden worden, wordt NULL geretourneerd.
  CURSOR tray_stof_cur( b_trge_id NUMBER)
  IS
    SELECT distinct
           UPPER( kgc_interface.stoftest_substring( stof.omschrijving , 'TECHNIEK') ) techniek
    ,      UPPER( kgc_interface.stoftest_substring( stof.omschrijving , 'GEN') )      gen
    FROM   kgc_stoftesten stof
    ,      bas_meetwaarden meet
    ,      kgc_tray_gebruik trge
    ,      kgc_tray_vulling trvu
    WHERE  trvu.meet_id = meet.meet_id
    AND    trvu.trge_id = trge.trge_id
    AND    meet.stof_id = stof.stof_id
    AND    trvu.trge_id = b_trge_id
    ;
  tray_stof_rec tray_stof_cur%rowtype;

  -- mantis 3146 gwe maak gebruik van escape character i.p.v. chr(95)

  -- zoek project, maak gebruik van vertaalde underscores chr(95) ipv letterlijke underscores '_',
  -- letterlijke underscores worden nl als wildcard gezien!!!
  CURSOR project_cur( p_techniek varchar2, p_gen varchar2)
  IS
    SELECT waarde
    FROM   kgc_interface_meta_data_vw
    WHERE  meta_type = 'PROJECT'
--    AND    UPPER(waarde) like  p_techniek||chr(95)||'%'||chr(95)|| p_gen
    AND    UPPER(waarde) like  p_techniek||'\_%\_'||p_gen escape '\'
    ;
  project_rec project_cur%rowtype;

  -- user_sample_id: fractienr_stoftest_omschrijving_volgnr
  CURSOR sample_cur
  IS
    SELECT frac.fractienummer
        || '_'
        || stof.omschrijving
        || '_' sample_id
    FROM   kgc_stoftesten stof
    ,      bas_fracties frac
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_tray_vulling trvu
    WHERE  trvu.meet_id = meet.meet_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.frac_id = frac.frac_id
    AND    meet.stof_id = stof.stof_id
    AND    trvu.trvu_id = p_trvu_id
    UNION
    SELECT stan.standaardfractienummer
        || '_'
        || stof.omschrijving
        || '_' sample_id
    FROM   kgc_stoftesten stof
    ,      bas_standaardfracties stan
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    ,      kgc_tray_vulling trvu
    WHERE  trvu.meet_id = meet.meet_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.stan_id = stan.stan_id
    AND    meet.stof_id = stof.stof_id
    AND    trvu.trvu_id = p_trvu_id
    ;

  sample_rec sample_cur%rowtype;
  -- bepaal volgnr
  CURSOR sample_vnr_cur
  ( p_sample_id IN VARCHAR2
  )
  IS
    SELECT MAX( TO_NUMBER( SUBSTR( naam
                                 , LENGTH( p_sample_id ) + 1
                                 )
                          )
              ) vnr
    FROM   kgc_samples
    WHERE  INSTR( naam, p_sample_id ) = 1
    AND    LTRIM(TRANSLATE( SUBSTR( naam
                                  , LENGTH( p_sample_id ) + 1
                                  )
                          , '0123456789'
                          , '          '
                          )
                ) IS NULL
    ;
  v_sample_id_vnr NUMBER;

BEGIN
  --aanpassing Yuri Mantis 4923 begin
  -- 14-10-2009 YA COLOR_INFO1 wordt gebruikt om het onderzoeknr mee te geven aan SQLLIMS
  IF ( p_meta_type = 'COLOR_INFO1' )
  THEN
     open  meet_cur;
     fetch meet_cur
     into  meet_rec;
     close meet_cur;

     v_waarde := meet_rec.onderzoeknr;

  --aanpassing Yuri Mantis 4970 begin
  -- 01-11-2010 YA COLOR_INFO2 wordt gebruikt om de meet_id mee te geven aan SQLLIMS
  ELSIF ( p_meta_type = 'COLOR_INFO2' )
  THEN
     open  meet_cur;
     fetch meet_cur
     into  meet_rec;
     close meet_cur;

     v_waarde := meet_rec.meet_id;
  --aanpassing Yuri Mantis 4970 eind

  ELSIF ( p_meta_type = 'PROJECT' )
      --aanpassing Yuri Mantis 4923 eind
      --aanpassing Yuri Mantis 4923, deze regel weg => IF ( p_meta_type = 'PROJECT' )
  THEN
    OPEN  tray_stof_cur(p_trge_id);
    FETCH tray_stof_cur
    INTO  tray_stof_rec;
    IF (   tray_stof_rec.techniek is not null
       and tray_stof_rec.gen is not null )
    THEN

      OPEN project_cur(tray_stof_rec.techniek,tray_stof_rec.gen);
      FETCH project_cur
      INTO  v_waarde;
      IF   project_cur%found
      THEN
        --OPEN  project_cur(tray_stof_rec.techniek,tray_stof_rec.gen);
        FETCH project_cur
        INTO  project_rec;
        IF    project_cur%found
        THEN
          v_waarde := NULL;
        END IF;
      END IF;

      FETCH tray_stof_cur
      INTO  tray_stof_rec;
      -- meer dan 1 gevonden (waarschijnlijk meerdere technieken)
      IF ( tray_stof_cur%found )
      THEN
        v_waarde := NULL;
      END IF;
    END IF;

    -- Afsluiten
    IF ( tray_stof_cur%isopen )
    THEN
      CLOSE tray_stof_cur;
    END IF;
    IF ( project_cur%isopen )
    THEN
      CLOSE project_cur;
    END IF;

    --v_waarde := NULL;
  ELSIF ( p_meta_type = 'SAMPLE#' )
  THEN
    open  trvu_cur;
    fetch trvu_cur
    into  v_kafd_id;
    close trvu_cur;
    v_sypa_waarde := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'NUMMERSTRUCTUUR_SAMP'
                     , p_kafd_id => v_kafd_id
                     );
    if ( v_sypa_waarde is null )
    then
      OPEN  sample_cur;
      FETCH sample_cur
      INTO  sample_rec;
      CLOSE sample_cur;
      sample_rec.sample_id := formatteer_bestandsnaam( sample_rec.sample_id );
      -- volgnummer...
      OPEN  sample_vnr_cur( sample_rec.sample_id );
      FETCH sample_vnr_cur
      INTO  v_sample_id_vnr;
      CLOSE sample_vnr_cur;
      v_sample_id_vnr := NVL( v_sample_id_vnr, 0 ) + 1;
      v_waarde := sample_rec.sample_id || TO_CHAR(v_sample_id_vnr);
    else  -- genereer op basis van nummerstructuur
      open  meet_cur;
      fetch meet_cur
      into  meet_rec;
      close meet_cur;
      v_stoftest_samplenaam := kgc_attr_00.waarde
                               ( p_tabel_naam => 'KGC_STOFTESTEN'
                               , p_id => meet_rec.stof_id
                               , p_code => 'SAMPLE'
                               );
      v_waarde := kgc_nust_00.genereer_nr
                  ( p_nummertype => 'SAMP'
                  , p_kafd_id => v_kafd_id
                  , p_ongr_id => to_number(null) -- ????
                  , p_waarde1 => meet_rec.fractienummer
                  , p_waarde2 => meet_rec.stof_code
                  , p_waarde3 => meet_rec.stof_omschrijving
                  , p_waarde4 => nvl( v_stoftest_samplenaam,meet_rec.stof_omschrijving)
                  );
    end if;
  END IF;
  RETURN( v_waarde );
END standaard_waarde;

PROCEDURE persoon_in_zis
( p_zisnr IN VARCHAR2
, pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
)
IS
  v_sypa_code varchar2(30);
  pers_leeg cg$KGC_PERSONEN.cg$row_type;
  rela_leeg cg$KGC_RELATIES.cg$row_type;
  verz_leeg cg$KGC_VERZEKERAARS.cg$row_type;
  v_msg varchar2(2000);
  v_ok boolean;
BEGIN
  v_ok := kgc_zis.persoon_in_zis
          ( p_zisnr => p_zisnr
          , pers_trow => pers_trow
          , rela_trow => rela_trow
          , verz_trow => verz_trow
          , x_msg => v_msg
          );
  if not v_ok
  then
    raise_application_error(-20000, v_msg);
  end if;

END persoon_in_zis;

PROCEDURE standaard_aanspreken_zis
( p_voorletters  IN  VARCHAR2  :=  NULL
, p_voorvoegsel  IN  VARCHAR2  :=  NULL
, p_achternaam   IN  VARCHAR2  :=  NULL
, p_achternaam_partner IN VARCHAR2 := NULL
, p_voorvoegsel_partner IN VARCHAR2 := NULL
, p_zis_aanspreeknaam IN VARCHAR2 := NULL
, p_geslacht     IN VARCHAR2 := 'O'
, x_default_aanspreken IN OUT VARCHAR2
, x_gehuwd       IN  OUT VARCHAR2
, x_aanspreken   IN  OUT  VARCHAR2
)
IS
  v_anm_in_aanspreken BOOLEAN := FALSE;
  v_anm_part_in_aanspreken BOOLEAN := FALSE;
BEGIN
  IF NVL(p_geslacht, 'O') = 'O' THEN -- M en V zijn tegenwoordig gelijk qua naamkeuze!!!
    x_default_aanspreken := 'J';
  ELSIF NVL(LENGTH(p_achternaam_partner), 0) = 0
  THEN
    x_default_aanspreken := 'J';
  ELSE
    v_anm_in_aanspreken := INSTR(UPPER(p_zis_aanspreeknaam), UPPER(p_achternaam)) > 0;
    v_anm_part_in_aanspreken := INSTR(UPPER(p_zis_aanspreeknaam), UPPER(p_achternaam_partner)) > 0;

    IF v_anm_in_aanspreken AND v_anm_part_in_aanspreken
    THEN
      x_gehuwd := 'J';
      x_default_aanspreken := 'J';
    ELSIF v_anm_in_aanspreken AND (NOT v_anm_part_in_aanspreken)
    THEN
      -- nu valt er niets te zeggen over x_gehuwd
      x_default_aanspreken := 'N';
      -- alleen eigennaam:
      kgc_formaat_00.standaard_aanspreken
      ( p_type => 'PERS'
      , p_voorletters => p_voorletters
      , p_voorvoegsel => p_voorvoegsel
      , p_achternaam => p_achternaam
      , x_aanspreken => x_aanspreken
      );
    ELSIF (NOT v_anm_in_aanspreken) AND v_anm_part_in_aanspreken
    THEN
      x_gehuwd := 'J';
      x_default_aanspreken := 'N';
      -- achternaam is partner-achternaam:
      kgc_formaat_00.standaard_aanspreken
      ( p_type => 'PERS'
      , p_voorletters => p_voorletters
      , p_voorvoegsel => p_voorvoegsel_partner
      , p_achternaam => p_achternaam_partner
      , x_aanspreken => x_aanspreken
      );
    ELSE -- beide niet in aanspreken? onwaarschijnlijk. opnieuw samenstellen:
      x_default_aanspreken := 'J';
    END IF;
  END IF;
  IF x_default_aanspreken = 'J'
  THEN
    kgc_formaat_00.standaard_aanspreken
    ( p_type => 'PERS'
    , p_voorletters => p_voorletters
    , p_voorvoegsel => p_voorvoegsel
    , p_achternaam => p_achternaam
    , p_achternaam_partner => p_achternaam_partner
    , p_voorvoegsel_partner => p_voorvoegsel_partner
    , p_gehuwd => x_gehuwd
    , p_geslacht => p_geslacht
    , x_aanspreken => x_aanspreken
    );
  END IF;
  IF  ( x_default_aanspreken IS NULL )
  THEN
    x_default_aanspreken := 'N';
  END IF;
  IF  ( x_gehuwd IS NULL )
  THEN
    x_gehuwd := 'N';
  END IF;
END standaard_aanspreken_zis;

FUNCTION stoftest_substring
( p_stoftest_omschrijving varchar2
, p_deel varchar2)
RETURN VARCHAR2
/*****************************************************************************
   NAME:       stoftest_substring
   PURPOSE:    Gebruikt bij bepaling standaard_waarde PROJECT metatype:
   Bepaling van de techniek en gen uit de omschrijving van de stoftest.
   Volgens de DNA werkwijze is het eerste deel de gebruikte Techniek,
   het tweede deel is het Gen.

   REVISIONS:
   Ver        Date        Author       Description
   ---------  ----------  ------------ ---------------------------------------
   1.0        09/05/2006  ESO          Melding: HLX-KGCTIFD01-00048
*****************************************************************************/
IS
  v_eerste_underscore number(2);
  v_tweede_underscore number(2);
  v_lengte_genstring  number(2);
  v_return varchar2(200);
BEGIN
  BEGIN
    v_eerste_underscore := instr( p_stoftest_omschrijving , '_');
    v_tweede_underscore := instr( p_stoftest_omschrijving , '_' , v_eerste_underscore + 1);
    v_lengte_genstring := v_tweede_underscore - v_eerste_underscore;
    IF p_deel = 'TECHNIEK'
    THEN -- Dit is het eerste deel uit stofstest omschrijving tot de eerste underscore
      v_return := substr(p_stoftest_omschrijving, 1,v_eerste_underscore-1) ;
    ELSIf p_deel = 'GEN'
    THEN -- Dit is het tweede deel uit stofstest omschrijving,
         -- vanaf na de eerste underscore tot de tweede underscore
      v_return := substr(p_stoftest_omschrijving, v_eerste_underscore +1,v_lengte_genstring -1) ;
    END IF;
  EXCEPTION
    WHEN others
    THEN
      v_return := '';
  END;

  RETURN( v_return );

END stoftest_substring;
END kgc_interface;
/

/
QUIT
