CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_NPE"
AS
   /*
   ================================================================================
   Package Name : gen_pck_npe
   Usage        : This package contains functions and procedure referencing
                  table gen_named_preferences
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   PROCEDURE p_ins_named_preferences(i_npe_rec IN gen_named_preferences%ROWTYPE);
   PROCEDURE p_upd_named_preferences(i_npe_rec IN gen_named_preferences%ROWTYPE);
   PROCEDURE p_delete_named_preference(i_npe_id IN gen_named_preferences.id%TYPE);
END gen_pck_npe;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_NPE"
AS
   /*
   ================================================================================
   Package Name : gen_pck_npe
   Usage        : This package contains functions and procedure referencing
                  table gen_named_preferences
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */

   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   --
   PROCEDURE p_ins_named_preferences(i_npe_rec IN gen_named_preferences%ROWTYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'p_ins_named_preferences';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      INSERT INTO gen_named_preferences(preference_name
                                       ,description
                                       ,DEFAULT_VALUE
                                       ,lookup_type
                                       )
           VALUES (i_npe_rec.preference_name
                  ,i_npe_rec.description
                  ,i_npe_rec.DEFAULT_VALUE
                  ,i_npe_rec.lookup_type
                  );
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   END p_ins_named_preferences;
   PROCEDURE p_upd_named_preferences(i_npe_rec IN gen_named_preferences%ROWTYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'p_upd_named_preferences';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      UPDATE gen_named_preferences
         SET preference_name   = i_npe_rec.preference_name
            ,description       = i_npe_rec.description
            ,DEFAULT_VALUE     = i_npe_rec.DEFAULT_VALUE
            ,lookup_type       = i_npe_rec.lookup_type
       WHERE id = i_npe_rec.id;
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   END p_upd_named_preferences;
   PROCEDURE p_delete_named_preference(i_npe_id IN gen_named_preferences.id%TYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'i_delete_named_preference';
      l_pref_name       gen_named_preferences.preference_name%TYPE;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      SELECT npe.preference_name
        INTO l_pref_name
        FROM gen_named_preferences npe
       WHERE npe.id = i_npe_id;
      logger.log_warning(
         'Named Preference ' || l_pref_name || ' deleted by ' || v('APP_USER')
        ,l_scope);
      DELETE FROM gen_named_preferences
            WHERE id = i_npe_id;
      gen_pck_exception.p_json_error('');
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         gen_pck_exception.p_json_error(gen_pck_exception.f_get_err_message(
                                         SQLERRM
                                        ,v('LANGCODE')
                                        ,'N'
                                        ,'N'));
   END p_delete_named_preference;
END gen_pck_npe;
/

/
QUIT
