CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPAT_00" IS
-- PL/SQL Specification
/* LST 22-07-2004                                                                                                                                        */
/* Package voor procedures op de tabel KGC_OPSLAG_ATTRIBUTEN                                                   */

PROCEDURE sel_opin
(p_opat_id       IN NUMBER
,p_opel_id      OUT NUMBER
,p_oppo_id      OUT NUMBER
,p_opin_id      OUT NUMBER
)
;

FUNCTION exists_opat
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
) RETURN BOOLEAN
;

PROCEDURE insert_opat
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
,p_aantal IN NUMBER := 1
)
;

PROCEDURE haal_helix_geg
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
,p_helix_code OUT VARCHAR2
)
;

FUNCTION haal_helix_code
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
) RETURN VARCHAR2
;



END KGC_OPAT_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPAT_00" IS
/* LST 22-07-2004                                                                                                                                        */
/* Package voor procedures op de tabel KGC_OPSLAG_ATTRIBUTEN                                                   */

PROCEDURE sel_opin
(p_opat_id       IN NUMBER
,p_opel_id      OUT NUMBER
,p_oppo_id      OUT NUMBER
,p_opin_id      OUT NUMBER
)
IS
  CURSOR c_opin
  (cp_opat_id NUMBER
  ) IS
  SELECT opin.opin_id
  ,      oppo.oppo_id
  ,      oppo.opel_id
  FROM   kgc_opslag_posities oppo
  ,      kgc_opslag_inhoud opin
  WHERE  oppo.oppo_id = opin.oppo_id
  AND    opin.opat_id = cp_opat_id
  ;
  r_opin c_opin%rowtype;
-- PL/SQL Block
BEGIN
  IF p_opat_id IS NOT NULL
  THEN
    OPEN c_opin(p_opat_id);
    FETCH c_opin INTO r_opin;
    CLOSE c_opin;
    p_opel_id := r_opin.opel_id;
    p_oppo_id := r_opin.oppo_id;
    p_opin_id := r_opin.opin_id;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opin%isopen THEN
      CLOSE c_opin;
    END IF;
    RAISE;
END sel_opin;

FUNCTION exists_opat
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
) RETURN BOOLEAN
IS
  CURSOR c_opat
  IS
  SELECT '1'
  FROM   kgc_opslag_attributen opat
  ,      kgc_opslag_entiteiten OPEN
  WHERE  open.open_id    = opat.open_id
  AND    opat.helix_id   = p_helix_id
  AND    open.code       = p_entiteit_code
  ;
  v_dummy VARCHAR2(1);
  v_oke   BOOLEAN := FALSE;
BEGIN
  OPEN c_opat;
  FETCH c_opat INTO v_dummy;
  IF c_opat%found
  THEN
    v_oke := TRUE;
  ELSE
    v_oke := FALSE;
  END IF;
  CLOSE c_opat;
  RETURN v_oke;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opat%isopen THEN
      CLOSE c_opat;
    END IF;
    RAISE;
END exists_opat
;

PROCEDURE insert_opat
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
,p_aantal IN NUMBER := 1
)
IS
  CURSOR c_open
  (cp_entiteit_code VARCHAR2
  )
  IS
  SELECT open.open_id
  ,      open.tabelnaam
  FROM   kgc_opslag_entiteiten OPEN
  WHERE  open.code = cp_entiteit_code
  ;
  r_open       c_open%rowtype;
  i            NUMBER;
BEGIN
  /* p_entiteit_code bevat de tabel alias van de aanleverende tabel  */
  /* omschrijving in kgc_opslag_entiteiten is de volledige tabelnaam */
  IF p_helix_id IS NULL
  OR p_entiteit_code IS NULL
  THEN
    qms$errors.show_message(p_mesg =>'KGC-11155'
	                         ,p_errtp => 'E'
						               ,p_rftf => TRUE
						               );
  END IF;
  OPEN c_open(p_entiteit_code);
  FETCH c_open INTO r_open;
  CLOSE c_open;
  IF r_open.open_id IS NOT NULL
  THEN
    FOR i IN 1 .. p_aantal
    LOOP
      INSERT INTO kgc_opslag_attributen
      (opat_id
      ,open_id
      ,helix_id
      ,volgnr
      ,created_by
      ,creation_date
      ,last_updated_by
      ,last_update_date
      ) VALUES
      (kgc_opat_seq.nextval
      ,r_open.open_id
      ,p_helix_id
      ,i
      ,USER
      ,SYSDATE
      ,USER
      ,SYSDATE
      );
    END LOOP;
  ELSE
    qms$errors.show_message(p_mesg =>'KGC-11155'
	                         ,p_errtp => 'E'
						               ,p_rftf => TRUE
						               );
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    IF c_open%isopen THEN
      CLOSE c_open;
    END IF;
    RAISE;
END insert_opat;

/* lst 29-07-2004 bij sommige tabellen is een unique key aanwezig     */
/* de kolom hiervoor staat vermeld in code_kolom in opslag_entiteiten */
PROCEDURE haal_helix_geg
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
,p_helix_code OUT VARCHAR2
)
IS
  CURSOR c_open
  (cp_entiteit_code VARCHAR2
  ) IS
  SELECT open.tabelnaam
  ,      open.code_kolom
  ,      open.where_clause
  FROM   kgc_opslag_entiteiten OPEN
  WHERE  open.code = p_entiteit_code
  ;
  r_open        c_open%rowtype;
  v_select      VARCHAR2(2000);
  v_cur         INTEGER;
  v_rows        INTEGER;
  v_helix_code  VARCHAR2(100);
  v_aantal_kols NUMBER(1);
BEGIN
  IF p_helix_id IS NOT NULL
  AND p_entiteit_code IS NOT NULL
  THEN
    OPEN c_open(p_entiteit_code);
    FETCH c_open INTO r_open;
    CLOSE  c_open;
    IF r_open.code_kolom IS NOT NULL
    THEN
      v_select := 'select '||r_open.code_kolom;
    END IF;
    IF v_select IS NOT NULL
    THEN
      v_select := v_select||
                  ' from '||r_open.tabelnaam||' '||p_entiteit_code
                  ;
      IF r_open.where_clause IS NOT NULL
      THEN
        v_select := v_select||
                    r_open.where_clause||
                    ' and '||p_entiteit_code||'.'||p_entiteit_code||'_id = '||
                    TO_CHAR(p_helix_id)
                    ;
      ELSE
        v_select := v_select||
                    ' where '||p_entiteit_code||'.'||p_entiteit_code||'_id = '||
                    TO_CHAR(p_helix_id)
                    ;
      END IF;
      v_cur :=dbms_sql.open_cursor;
      dbms_sql.parse(v_cur, v_select, dbms_sql.native);
      v_aantal_kols := 0;
      IF r_open.code_kolom IS NOT NULL
      THEN
        v_aantal_kols := v_aantal_kols + 1;
        dbms_sql.define_column(v_cur, v_aantal_kols, v_helix_code, 50);
      END IF;
      v_rows := dbms_sql.execute(v_cur);
      LOOP
        IF dbms_sql.fetch_rows(v_cur) = 0
        THEN
          EXIT;
        END IF;
        IF r_open.code_kolom IS NOT NULL
        THEN
          dbms_sql.column_value(v_cur, 1, v_helix_code);
        END IF;
      END LOOP;
      dbms_sql.close_cursor(v_cur);
    END IF;
  END IF;
  p_helix_code := NVL(v_helix_code, p_helix_id);
EXCEPTION
  WHEN OTHERS THEN
    IF dbms_sql.is_open(v_cur) THEN
      dbms_sql.close_cursor(v_cur);
    END IF;
    IF c_open%isopen THEN
      CLOSE c_open;
    END IF;
    RAISE;
END haal_helix_geg
;

FUNCTION haal_helix_code
(p_entiteit_code IN VARCHAR2
,p_helix_id IN NUMBER
) RETURN VARCHAR2
IS
  v_helix_code VARCHAR2(100) := NULL;
BEGIN
  IF p_entiteit_code IS NOT NULL
  AND p_helix_id IS NOT NULL
  THEN
    haal_helix_geg( p_entiteit_code => p_entiteit_code
                  , p_helix_id      => p_helix_id
                  , p_helix_code    => v_helix_code
                  );
  END IF;
  RETURN v_helix_code;
END
;
END KGC_OPAT_00;
/

/
QUIT
