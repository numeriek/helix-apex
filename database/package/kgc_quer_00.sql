CREATE OR REPLACE PACKAGE "HELIX"."KGC_QUER_00" IS
-- PL/SQL Specification
-- Vul hulptabel met gegevens uit samengesteld query
-- Hangt nauw samen met functionaliteit in form kgcquer01
FUNCTION vul_data
( p_session_id IN NUMBER
)
RETURN VARCHAR2;
PROCEDURE vul_data
( p_session_id IN NUMBER
);

END KGC_QUER_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_QUER_00" IS
FUNCTION vul_data
( p_session_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_statement VARCHAR2(32767);
  v_kolomlijst VARCHAR2(2000) := 'session_id';
  v_select VARCHAR2(2000) := 'select '||TO_CHAR(p_session_id);
  v_from VARCHAR2(2000);
  v_where VARCHAR2(4000) := 'where 1=1';

  v_master_kolomlijst VARCHAR2(2000) := 'session_id';
  v_master_select VARCHAR2(2000) := 'select distinct '||TO_CHAR(p_session_id);
  v_master_detail BOOLEAN := FALSE;

  CURSOR view_cur
  IS
    SELECT DISTINCT view_naam
        FROM   kgc_query
        WHERE  session_id = p_session_id
        ;
  CURSOR vcol_cur
  ( p_view_naam IN VARCHAR2
  )
  IS
    SELECT *
    FROM   kgc_query
    WHERE  session_id = p_session_id
    AND    view_naam = p_view_naam
    ORDER  BY volgorde, kolom
    ;
  -- order by moet in overeenstemming zijn met
  -- order by in cursor c in PU toon_data in form kgcquer01!!!

  FUNCTION add_select
  ( p_kolom IN VARCHAR2
  , p_datatype IN VARCHAR2 := 'VARCHAR2'
  )
  RETURN VARCHAR2
  IS
    v_return VARCHAR2(1000);
-- PL/SQL Block
BEGIN
    IF ( p_datatype = 'NUMBER' )
    THEN
      v_return := ', to_char('||p_kolom||')';
    ELSIF ( p_datatype = 'DATE' )
    THEN
      v_return := ', to_char('||p_kolom||',''dd-mm-yyyy'')';
    ELSE
      v_return := ', replace('||p_kolom||','''''''','''''''''''')';
    END IF;
    RETURN( v_return );
  END add_select;

  FUNCTION add_where
  ( p_kolom IN VARCHAR2
  , p_voorwaarde IN VARCHAR2
  , p_operator IN VARCHAR2 := NULL
  , p_datatype IN VARCHAR2 := 'VARCHAR2'
  )
  RETURN VARCHAR2
  IS
    v_return VARCHAR2(1000);
  BEGIN
    -- where
    v_return := 'and '||p_kolom;
    IF ( INSTR( p_voorwaarde, '=' ) > 0
      OR INSTR( p_voorwaarde, '<' ) > 0
      OR INSTR( p_voorwaarde, '>' ) > 0
      OR INSTR( LOWER(p_voorwaarde), 'like' ) > 0
      OR INSTR( LOWER(p_voorwaarde), 'is' ) > 0
       )
    THEN
      v_return := v_return||' '||p_voorwaarde;
      RETURN( v_return );
    END IF;
    -- operator
    IF ( p_operator IS NOT NULL )
    THEN
      v_return := v_return||' '||p_operator;
    ELSIF ( INSTR( p_voorwaarde, '%' ) > 0 )
    THEN
      v_return := v_return||' like ';
    ELSE
      v_return := v_return||' = ';
    END IF;
    -- voorwaarde
    IF ( p_datatype = 'NUMBER' )
    THEN
      v_return := v_return||' '||p_voorwaarde;
    ELSIF ( p_datatype = 'DATE' )
    THEN
      v_return := v_return||' to_date('''||p_voorwaarde||''',''dd-mm-yyyy'')';
    ELSE
      v_return := v_return||' '''||p_voorwaarde||'''';
    END IF;
    RETURN( v_return );
  END add_where;

BEGIN
  FOR view_rec IN view_cur
  LOOP
    -- FROM
    IF ( v_from IS NULL )
    THEN
      v_from := 'from '||view_rec.view_naam;
    ELSE
      v_from := v_from||', '||view_rec.view_naam;
        END IF;
    FOR vcol_rec IN vcol_cur( view_rec.view_naam )
    LOOP
      -- kolomlijst is kolom1..kolom20
      v_kolomlijst := v_kolomlijst||CHR(10)||', kolom'||vcol_cur%rowcount;
      -- SELECT
      v_select := v_select ||CHR(10)||
                  add_select
                  ( p_kolom => vcol_rec.kolom
                  , p_datatype => vcol_rec.datatype
                  );
      -- masters
      IF ( vcol_rec.master_detail = 'M' )
      THEN
        v_master_detail := TRUE;
        v_master_kolomlijst := v_master_kolomlijst||CHR(10)||', kolom'||vcol_cur%rowcount;
        v_master_select := v_master_select ||CHR(10)||', kolom'||vcol_cur%rowcount;
      END IF;
      -- WHERE
      IF ( vcol_rec.voorwaarde IS NOT NULL )
      THEN
        v_where := v_where ||CHR(10)||
                   add_where
                   ( p_kolom => vcol_rec.kolom
                   , p_voorwaarde => vcol_rec.voorwaarde
                   , p_datatype => vcol_rec.datatype
                   );
      END IF;
      IF ( vcol_rec.ondergrens IS NOT NULL )
      THEN
        v_where := v_where ||CHR(10)||
                   add_where
                   ( p_kolom => vcol_rec.kolom
                   , p_voorwaarde => vcol_rec.ondergrens
                   , p_operator => '>='
                   , p_datatype => vcol_rec.datatype
                   );
      END IF;
      IF ( vcol_rec.bovengrens IS NOT NULL )
      THEN
        v_where := v_where ||CHR(10)||
                   add_where
                   ( p_kolom => vcol_rec.kolom
                   , p_voorwaarde => vcol_rec.bovengrens
                   , p_operator => '<='
                   , p_datatype => vcol_rec.datatype
                   );
      END IF;
    END LOOP;
  END LOOP;
  v_statement := 'insert into kgc_query_details'
     ||CHR(10)|| '('||v_kolomlijst||')'
     ||CHR(10)|| v_select
     ||CHR(10)|| v_from
     ||CHR(10)|| v_where
               ;
  BEGIN
    kgc_util_00.dyn_exec( v_statement );
    COMMIT;
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END;

  IF ( v_master_detail )
  THEN
    v_statement := 'insert into kgc_query_masters'
       ||CHR(10)|| '('||v_master_kolomlijst||')'
       ||CHR(10)|| v_master_select
       ||CHR(10)|| 'from kgc_query_details'
       ||CHR(10)|| 'where session_id = '||TO_CHAR(p_session_id)
                 ;
    BEGIN
      kgc_util_00.dyn_exec( v_statement );
      COMMIT;
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END IF;
  RETURN( v_select||CHR(10)||v_from||CHR(10)||v_where );
END vul_data;

PROCEDURE vul_data
( p_session_id IN NUMBER
)
IS
  v_query VARCHAR2(32767);
BEGIN
  v_query := vul_data( p_session_id => p_session_id );
END vul_data;

END kgc_quer_00;
/

/
QUIT
