CREATE OR REPLACE PACKAGE "HELIX"."KGC_REPE_00" IS
-- mag de relatie bestaan tussen 2 personen?
function relatie_ok
( p_pers_id in number
, p_pers_id_relatie in number
, p_fare_id in number
)
return varchar2;

-- omschrijf de familierelatie tussen twee personen
function omschrijving_relatie
( p_richting in varchar2 -- >=heen, <=terug
, p_pers_id in number
, p_pers_id_relatie in number
, p_fare_id in number
)
return varchar2;
-- idem, overloaded
function omschrijving_relatie
( p_richting in varchar2 -- >=heen, <=terug
, p_repe_id in number
)
return varchar2;
-- idem, overloaded
function omschrijving_relatie
( p_pers_id1 in number -- persoon van waaruit de omschrijving wordt gegeven
, p_pers_id2 in number -- gerelateerde persoon
)
return varchar2;
END KGC_REPE_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_REPE_00" IS
/******************************************************************************
 NAME:      relatie_ok
 PURPOSE:   controleren of de relatie aangemaakt mag worden.
            Ouders moeten een hogere leeftijd hebben dan hun kinderen

 REVISIONS:

  Ver        Date        Author       Description
  ---------  ----------  ------------ ---------------------------------------
  1.0        19/04/2006  YAR          Melding: HLX-KGEN-11116
  1.1        24/04/2006  MKL          Controle op leeftijd eruit gehaald: geen toestemming
                                      Controle op geslacht toegevoegd
                                      Functie omschrijving_relatie toegevoegd

*****************************************************************************/
function relatie_ok
( p_pers_id in number
, p_pers_id_relatie in number
, p_fare_id in number
)
return varchar2
is
  v_return varchar2(1) := 'J'; -- J/N

  cursor fare_cur
  ( b_fare_id in number
  )
  is
    select code
    ,      omschrijving -- heen
    ,      geslacht
    from   kgc_familie_relaties
    where  fare_id = b_fare_id
    ;
  fare_rec fare_cur%rowtype;

  cursor pers_cur
  ( b_pers_id in number
  )
  is
    select geboortedatum
    ,      geslacht
    from   kgc_personen
    where  pers_id = b_pers_id
    ;
  pers_rec1 pers_cur%rowtype;
  pers_rec2 pers_cur%rowtype;
begin
  open  fare_cur( b_fare_id => p_fare_id );
  fetch fare_cur
  into  fare_rec;
  close fare_cur;

  open  pers_cur( b_pers_id => p_pers_id );
  fetch pers_cur
  into  pers_rec1;
  close pers_cur;

  open  pers_cur( b_pers_id => p_pers_id_relatie );
  fetch pers_cur
  into  pers_rec2;
  close pers_cur;

  if ( v_return = 'J' )
  then
    if ( fare_rec.geslacht is not null
     and p_pers_id is not null
       )
    then
      if ( nvl( pers_rec1.geslacht, 'O' ) <> fare_rec.geslacht  )
      then
        v_return := 'N';
      end if;
    end if;
  end if;

  if ( v_return = 'J' )
  then
    if ( ( lower(fare_rec.omschrijving) like '%moeder%'
        or lower(fare_rec.omschrijving) like '%vader%'
         )
     and p_pers_id is not null
     and p_pers_id_relatie is not null
       )
    then
      if ( pers_rec1.geboortedatum >= pers_rec2.geboortedatum )
      then
        v_return := 'N';
      end if;
    end if;
  end if;

  return( v_return );

end relatie_ok;

function omschrijving_relatie
( p_richting in varchar2 -- >=heen, <=terug
, p_pers_id in number
, p_pers_id_relatie in number
, p_fare_id in number
)
return varchar2
is
  v_return varchar2(1000);

  cursor fare_cur
  ( b_fare_id in number
  )
  is
    select code
    ,      omschrijving -- heen
    ,      omschrijving_terug
    from   kgc_familie_relaties
    where  fare_id = b_fare_id
    ;
  fare_rec fare_cur%rowtype;

  cursor pers_cur
  ( b_pers_id in number
  )
  is
    select aanspreken
    ,      geslacht
    ,      geboortedatum
    from   kgc_personen
    where  pers_id = b_pers_id
    ;
  pers_rec1 pers_cur%rowtype;
  pers_rec2 pers_cur%rowtype;
begin
  if ( p_pers_id is null
    or p_pers_id_relatie is null
    or p_fare_id is null
     )
  then
    return( null );
  end if;
  open  fare_cur( b_fare_id => p_fare_id );
  fetch fare_cur
  into  fare_rec;
  close fare_cur;

  open  pers_cur( b_pers_id => p_pers_id );
  fetch pers_cur
  into  pers_rec1;
  close pers_cur;

  open  pers_cur( b_pers_id => p_pers_id_relatie );
  fetch pers_cur
  into  pers_rec2;
  close pers_cur;

  if ( p_richting = '>' )
  then
    v_return := pers_rec1.aanspreken
             || ' '
             || fare_rec.omschrijving
             || ' '
             || pers_rec2.aanspreken
             || ' ('
             || pers_rec2.geslacht
             || ') '
             || to_char( pers_rec2.geboortedatum, 'dd-mm-yyyy' )
              ;
  elsif ( p_richting = '<' )
  then
    v_return := pers_rec2.aanspreken
             || ' '
             || fare_rec.omschrijving_terug
             || ' '
             || pers_rec1.aanspreken
             || ' ('
             || pers_rec1.geslacht
             || ') '
             || to_char( pers_rec1.geboortedatum, 'dd-mm-yyyy' )
              ;
  else
    v_return := pers_rec2.aanspreken
             || ' ('
             || pers_rec2.geslacht
             || ') '
             || to_char( pers_rec2.geboortedatum, 'dd-mm-yyyy' )
              ;
  end if;

  return( v_return );

end omschrijving_relatie;

function omschrijving_relatie
( p_richting in varchar2
, p_repe_id in number
)
return varchar2
is
  v_return varchar2(1000);
  cursor repe_cur
  ( b_repe_id in number
  )
  is
    select repe_id
    ,      pers_id
    ,      pers_id_relatie
    ,      fare_id
    ,      relatie
    from   kgc_relaties_van_persoon
    where  repe_id = b_repe_id
    ;
  repe_rec repe_cur%rowtype;
begin
  open  repe_cur( p_repe_id );
  fetch repe_cur
  into  repe_rec;
  close repe_cur;
  if ( repe_rec.relatie is not null )
  then
    v_return := repe_rec.relatie;
  else
    v_return := omschrijving_relatie
                ( p_richting => p_richting
                , p_pers_id => repe_rec.pers_id
                , p_pers_id_relatie => repe_rec.pers_id_relatie
                , p_fare_id => repe_rec.fare_id
                );
  end if;

  return( v_return );

end omschrijving_relatie;

function omschrijving_relatie
( p_pers_id1 in number -- persoon van waaruit de omschrijving wordt gegeven
, p_pers_id2 in number -- gerelateerde persoon
)
return varchar2
is
  v_return varchar2(1000);
  cursor repe_cur
  ( b_pers_id in number
  , b_pers_id_relatie in number
  )
  is
    select repe_id
    ,      pers_id
    ,      pers_id_relatie
    ,      fare_id
    ,      relatie
    from   kgc_relaties_van_persoon
    where  pers_id = b_pers_id
    and    pers_id_relatie = b_pers_id_relatie
    ;
  repe_rec repe_cur%rowtype;
begin
  open  repe_cur( b_pers_id => p_pers_id1
                , b_pers_id_relatie => p_pers_id2
                );
  fetch repe_cur
  into  repe_rec;
  close repe_cur;

  if ( repe_rec.repe_id is null )
  then -- andersom
    open  repe_cur( b_pers_id => p_pers_id2
                  , b_pers_id_relatie => p_pers_id1
                  );
    fetch repe_cur
    into  repe_rec;
    close repe_cur;
  end if;

  if ( repe_rec.repe_id is not null )
  then
    if ( repe_rec.relatie is not null )
    then
      v_return := repe_rec.relatie;
    elsif ( p_pers_id1 = repe_rec.pers_id )
    then
      v_return := omschrijving_relatie
                  ( p_richting => '>' -- heen
                  , p_pers_id => repe_rec.pers_id
                  , p_pers_id_relatie => repe_rec.pers_id_relatie
                  , p_fare_id => repe_rec.fare_id
                  );
    elsif ( p_pers_id1 = repe_rec.pers_id_relatie )
    then
      v_return := omschrijving_relatie
                  ( p_richting => '<' -- terug
                  , p_pers_id => repe_rec.pers_id
                  , p_pers_id_relatie => repe_rec.pers_id_relatie
                  , p_fare_id => repe_rec.fare_id
                  );
    end if;
  end if;

  return( v_return );

end omschrijving_relatie;
end   kgc_repe_00;
/

/
QUIT
