CREATE OR REPLACE PACKAGE "HELIX"."KGC_POCD_00" IS
/******************************************************************************
   NAME:       kgc_pocd_00
   PURPOSE:    controleer postcode en haal afgeleide gegevens op
               NB. foutafhandeling vindt plaats bij aanroep.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        29-05-2006  MKL              HLX-HELIX-11326.
***************************************************************************** */
-- controleer (zo nodig) de postcode
function postcode_ok
( p_postcode in out varchar2
, p_land in varchar2 default 'NEDERLAND'
, p_postcode_check in varchar2 default null
)
return boolean;

-- haal gerelateerde gegevens op bij een postcode
procedure gerelateerde_gegevens
( p_postcode in varchar2
, x_gevonden out boolean
, x_plaats out varchar2
, x_provincie out varchar2
, x_land out varchar2
);
END KGC_POCD_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_POCD_00" IS
function postcode_ok
( p_postcode in out varchar2
, p_land in varchar2 default 'NEDERLAND'
, p_postcode_check in varchar2 default null
)
return boolean
is
  v_postcode_check varchar2(100);
  v_num number(4);
  v_chr varchar2(2);
  v_foute_postcode exception;

  -- let op: in onderstaande procedure worden externe variabelen gelezen en gewijzigd
  procedure controles
  is
  begin
    if ( length ( p_postcode ) < 6 )
    then
      raise v_foute_postcode;
    end if;

    begin
      v_num := to_number( substr( p_postcode, 1, 4 ) );
    exception
      when value_error
      then
        raise v_foute_postcode;
    end;

    if ( length( v_num ) <> 4 )
    then
      raise v_foute_postcode;
    end if;

    if ( length( p_postcode ) = 7 )
    then
      if ( substr( p_postcode, 5, 1 ) <> ' ' )
      then
        raise v_foute_postcode;
      end if;
    end if;

    v_chr := substr( upper( p_postcode ), -2 );
    if ( v_chr not between 'AA' and 'ZZ' )
    then
      raise v_foute_postcode;
    end if;

    if ( length( ltrim( rtrim( v_chr) ) ) <> 2 )
    then
      raise v_foute_postcode;
    end if;

  end controles;

begin
  if ( p_postcode is null )
  then
    return (true);
  end if;

  if ( p_postcode_check is not null )
  then
    v_postcode_check := upper( p_postcode_check );
  else
    begin
      v_postcode_check := upper( kgc_sypa_00.standaard_waarde
                                 ( p_parameter_code => 'POSTCODE_CHECK'
                                 , p_kafd_id => null -- niet afdelingsafhankelijk!
                                 )
                               );
    exception
      when others
      then
        v_postcode_check := 'BEPERKT';
    end;
  end if;

  if ( v_postcode_check = 'BEPERKT' )
  then
    if ( nvl( upper( p_land ), 'NEDERLAND' ) = 'NEDERLAND' )
    then
      controles;
      p_postcode := to_char( v_num )||' '||v_chr;
    end if;
  elsif ( v_postcode_check = 'STRENG' )
  then
    controles;
    p_postcode := to_char( v_num )||' '||v_chr;
  else
    null; -- geen controles
  end if;

  return( true );
exception
  when v_foute_postcode
  then
    return( false );
  when others
  then
    raise;
end postcode_ok;

procedure gerelateerde_gegevens
( p_postcode in varchar2
, x_gevonden out boolean
, x_plaats out varchar2
, x_provincie out varchar2
, x_land out varchar2
)
is
  v_postcode_check varchar2(100);
  cursor pocd_cur
  ( b_postcode in varchar2 )
  is
    select pocd.plaats
    ,      geme.provincie
    from   kgc_gemeenten geme
    ,      kgc_postcodes pocd
    where  pocd.geme_id = geme.geme_id (+)
    and    pocd.postcode = b_postcode
    ;
  pocd_rec pocd_cur%rowtype;
begin
  if ( p_postcode is null )
  then
    return;
  end if;

  open  pocd_cur( substr( p_postcode, 1, 4 ) );
  fetch pocd_cur
  into  pocd_rec;
  x_gevonden := pocd_cur%found;
  close pocd_cur;

  x_plaats := pocd_rec.plaats;
  x_provincie := pocd_rec.provincie;
  if ( nvl( x_provincie, '99' ) not in ( '00', '99' ) )
  then
    x_land := kgc_land_00.controleer( 'Nederland' );
  end if;

end gerelateerde_gegevens;
END KGC_POCD_00;
/

/
QUIT
