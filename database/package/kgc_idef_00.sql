CREATE OR REPLACE PACKAGE "HELIX"."KGC_IDEF_00" IS
-- duplo-waarden worden in aparte regel ingelezen, maar moeten naar de bijbehorende rij
PROCEDURE duplo
( p_session_id IN NUMBER := NULL
);
END KGC_IDEF_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_IDEF_00" IS
PROCEDURE duplo
( p_session_id IN NUMBER := NULL
)
IS
  CURSOR roui_cur
  IS
    SELECT ROWID
    ,      onderzoeknr
    ,      monsternummer
    ,      meti_id
    ,      protocol_code
    ,      stoftest_omschrijving
    ,      volgnummer
    ,      meetwaarde_1
    FROM   bas_robot_uitslagen
    WHERE  sessie = NVL( p_session_id, sessie )
    ORDER BY onderzoeknr
    ,        monsternummer
    ,        meti_id
    ,        protocol_code
    ,        stoftest_omschrijving
    ,        volgnummer ASC
    FOR UPDATE OF volgnummer nowait
    ;
  roui_rec roui_cur%rowtype; -- 1e rij
BEGIN
  FOR r IN roui_cur
  LOOP
    IF ( NVL(r.onderzoeknr,'~') = NVL(roui_rec.onderzoeknr,'~')
     AND r.monsternummer = roui_rec.monsternummer
     AND NVL(r.meti_id,-999) = NVL(roui_rec.meti_id,-999)
     AND NVL(r.protocol_code,'~') = NVL(roui_rec.protocol_code,'~')
     AND r.stoftest_omschrijving = roui_rec.stoftest_omschrijving
       )
    THEN -- duplo gevonden
      BEGIN
        UPDATE bas_robot_uitslagen
        SET    meetwaarde_2 = r.meetwaarde_1
        WHERE  ROWID = roui_rec.rowid;
        -- verwijder duplo rij
        DELETE FROM bas_robot_uitslagen
        WHERE CURRENT OF roui_cur;
      END;
    ELSE -- volgende 1e rij
      roui_rec := r;
    END IF;
  END LOOP;
END duplo;
END kgc_idef_00;
/

/
QUIT
