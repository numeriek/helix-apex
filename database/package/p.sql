CREATE OR REPLACE PACKAGE "HELIX"."P"
is
  -- global variables
  prefix varchar2(3) := ' . ';
  confix varchar2(3) := ' - ';
  -- versions of l (overloaded)
  procedure l
  ( p1 in varchar2
  )
  ;
  procedure l
  ( p1 in varchar2
  , p2 in varchar2
  )
  ;
  procedure l
  ( p1 in varchar2
  , p2 in varchar2
  , p3 in varchar2
  )
  ;
  procedure l
  ( p1 in number
  )
  ;
  procedure l
  ( p1 in number
  , p2 in number
  )
  ;
  procedure l
  ( p1 in number
  , p2 in number
  , p3 in number
  )
  ;
  procedure l
  ( p1 in date
  )
  ;
  procedure l
  ( p1 in date
  , p2 in date
  )
  ;
  procedure l
  ( p1 in date
  , p2 in date
  , p3 in date
  )
  ;
  procedure l
  ( p1 in boolean
  )
  ;
  procedure l
  ( p1 in boolean
  , p2 in boolean
  )
  ;
  procedure l
  ( p1 in boolean
  , p2 in boolean
  , p3 in boolean
  )
  ;
  procedure l
  ( p1 in varchar2
  , p2 in number
  )
  ;
  procedure l
  ( p1 in number
  , p2 in varchar2
  )
  ;
  procedure l
  ( p1 in varchar2
  , p2 in varchar2
  , p3 in number
  )
  ;
  procedure l
  ( p1 in varchar2
  , p2 in boolean
  )
  ;
end p;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."P"
is
  --
  procedure display
  ( p_string in varchar2
  )
  is
    v_string varchar2(32767) := p_string;
    v_line varchar2(255);
  begin
--    dbms_output.disable;
    dbms_output.enable( buffer_size=>1000000 );
    while ( v_string is not null )
    loop
      if ( length( v_string ) > 255
       and ( instr( substr( v_string,1,255), ' ', -1) > 0
          or instr( substr( v_string,1,255), chr(10), -1) > 0
           )
         )
      then
        v_line := substr( v_string, 1, greatest( instr( substr( v_string,1,255), ' ', -1)
                                               , instr( substr( v_string,1,255), chr(10), -1)
                                               )
                        );
      else
        v_line := substr( v_string, 1, 255 );
      end if;
      v_string := ltrim( v_string, v_line );
      dbms_output.put_line( v_line );
    end loop;
  end display;
  --
  procedure l
  ( p1 in varchar2
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||p1;
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in varchar2
  , p2 in varchar2
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||p1||p.confix||p2;
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in varchar2
  , p2 in varchar2
  , p3 in varchar2
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||p1||p.confix||p2||p.confix||p3;
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in number
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||to_char(p1);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in number
  , p2 in number
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||to_char(p1)||p.confix||to_char(p2);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in number
  , p2 in number
  , p3 in number
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||to_char(p1)||p.confix||to_char(p2)||p.confix||to_char(p3);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in date
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||to_char(p1);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in date
  , p2 in date
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||to_char(p1)||p.confix||to_char(p2);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in date
  , p2 in date
  , p3 in date
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||to_char(p1)||p.confix||to_char(p2)||p.confix||to_char(p3);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in boolean
  )
  is
    v_string varchar2(32767);
	v1 varchar2(10);
  begin
    if ( p1 )
	then
	  v1 := 'TRUE';
	elsif ( not p1 )
	then
	  v1 := 'FALSE';
	else
	  v1 := '';
	end if;
    v_string := p.prefix||v1;
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in boolean
  , p2 in boolean
  )
  is
    v_string varchar2(32767);
	v1 varchar2(10);
	v2 varchar2(10);
  begin
    if ( p1 )
	then
	  v1 := 'TRUE';
	elsif ( not p1 )
	then
	  v1 := 'FALSE';
	else
	  v1 := '';
	end if;
    if ( p2 )
	then
	  v2 := 'TRUE';
	elsif ( not p2 )
	then
	  v2 := 'FALSE';
	else
	  v2 := '';
	end if;
    v_string := p.prefix||v1||p.confix||v2;
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in boolean
  , p2 in boolean
  , p3 in boolean
  )
  is
    v_string varchar2(32767);
	v1 varchar2(10);
	v2 varchar2(10);
	v3 varchar2(10);
  begin
    if ( p1 )
	then
	  v1 := 'TRUE';
	elsif ( not p1 )
	then
	  v1 := 'FALSE';
	else
	  v1 := '';
	end if;
    if ( p2 )
	then
	  v2 := 'TRUE';
	elsif ( not p2 )
	then
	  v2 := 'FALSE';
	else
	  v2 := '';
	end if;
    if ( p3 )
	then
	  v3 := 'TRUE';
	elsif ( not p3 )
	then
	  v3 := 'FALSE';
	else
	  v3 := '';
	end if;
    v_string := p.prefix||v1||p.confix||v2||p.confix||v3;
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in varchar2
  , p2 in number
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||p1||p.confix||to_char(p2);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in number
  , p2 in varchar2
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||to_char(p1)||p.confix||p2;
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in varchar2
  , p2 in varchar2
  , p3 in number
  )
  is
    v_string varchar2(32767);
  begin
    v_string := p.prefix||p1||p.confix||p2||p.confix||to_char(p3);
    display( v_string );
  end l;
  --
  procedure l
  ( p1 in varchar2
  , p2 in boolean
  )
  is
    v_string varchar2(32767);
	v2 varchar2(10);
  begin
    if ( p2 )
	then
	  v2 := 'TRUE';
	elsif ( not p2 )
	then
	  v2 := 'FALSE';
	else
	  v2 := '';
	end if;
    v_string := p.prefix||p1||p.confix||v2;
    display( v_string );
  end l;
  --
end p;
/

/
QUIT
