CREATE OR REPLACE PACKAGE "HELIX"."KGC_SYPA_00" IS
-- Standaard waarden kunnen worden vastgelegd op een aantal nivo-s:
--  1 . voor een medewerker in een afdeling voor een onderzoeksgroep
--  2 . voor een afdeling voor een onderzoeksgroep (voor elke medewerker)
--  3 . voor een medewerker in een afdeling
-- (4). voor een medewerker voor een onderzoeksgroep (=1: dan is in de praktijk ook afdeling gevuld!)
--  5 . voor een afdeling (elke medewerker)
-- (6). voor een onderzoeksgroep (elke medewerker)(=2: dan is in de praktijk ook afdeling gevuld!)
--  7 . voor een medewerker (in elke afdeling en onderzoeksgroep)
--  8 . voor een ieder (elke medewerker, elke afdeling, elke onderzoeksgroep)
--  9 . niet nader gespecificeerd (systeembreed)
-- waarbij 9 wordt overruled door 8, 8 wordt overruled door 7, enz...
--
-- De medewerker is standaard de medewerker met de corresponderende Oracle userid
--
-- Sommige nivo-s hebben bij bepaalde systeemparameters geen betekenis
--
FUNCTION standaard_waarde
( p_parameter_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( standaard_waarde, WNDS, WNPS, RNPS );
-- overloaded
FUNCTION standaard_waarde
( p_parameter_code IN VARCHAR2
, p_afdeling IN VARCHAR2 := NULL
, p_onderzoeksgroep IN VARCHAR2 := NULL
, p_medewerker IN VARCHAR2 := NULL
) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( standaard_waarde, WNDS, WNPS, RNPS );

-- medewerker waarde
FUNCTION medewerker_waarde
( p_sypa_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( medewerker_waarde, WNDS, WNPS, RNPS );

-- zelfde als standaardwaarde, maar niet overloaded
FUNCTION systeem_waarde
( p_parameter_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
)
RETURN VARCHAR2;

-- retourneer de cate_id van een (start)categorie voor een bepaalde situatie (=systeemparameter)
FUNCTION categorie_id
( p_parameter_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( categorie_id, WNDS, WNPS, RNPS );
END KGC_SYPA_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_SYPA_00" IS
FUNCTION standaard_waarde
( p_parameter_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
) RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  v_kafd_id NUMBER := p_kafd_id;
  v_ongr_id NUMBER := p_ongr_id;
  v_mede_id NUMBER := NVL( p_mede_id, kgc_mede_00.medewerker_id );
  CURSOR sypa_id_cur
  IS
    SELECT sypa_id
    FROM   kgc_systeem_parameters
    WHERE  code = UPPER( p_parameter_code )
    ;
  v_sypa_id NUMBER;
  -- haal kafd_id op als ongr_id is doorgegeven:
  CURSOR ongr_cur
  IS
    SELECT kafd_id
    FROM   kgc_onderzoeksgroepen
    WHERE  ongr_id = p_ongr_id
    ;
  -- waarde van systeemparameter als geen onderliggende waarde is gevonden
  CURSOR sypa_cur
  IS
    SELECT standaard_waarde
    FROM   kgc_systeem_parameters
    WHERE  sypa_id = v_sypa_id
    ;
  -- waarde van systeem parameter voor bekende afdeling en medewerker
  CURSOR spwa_cur
  ( l_kafd_id IN NUMBER
  , l_ongr_id IN NUMBER
  , l_mede_id IN NUMBER
  )
  IS
    SELECT waarde
    FROM   kgc_systeem_par_waarden
    WHERE  sypa_id = v_sypa_id
    AND  ( kafd_id = l_kafd_id
        OR ( l_kafd_id IS NULL AND kafd_id IS NULL )
         )
    AND  ( ongr_id = l_ongr_id
        OR ( l_ongr_id IS NULL AND ongr_id IS NULL )
         )
    AND  ( mede_id = l_mede_id
        OR ( l_mede_id IS NULL AND mede_id IS NULL )
         )
    ORDER BY DECODE( kafd_id, NULL, 1, 2 )
    ,        DECODE( ongr_id, NULL, 1, 2 )
    ,        DECODE( mede_id, NULL, 1, 2 )
    ;
-- PL/SQL Block
BEGIN
  OPEN  sypa_id_cur;
  FETCH sypa_id_cur
  INTO  v_sypa_id;
  CLOSE sypa_id_cur;
  IF ( v_sypa_id IS NULL )
  THEN
    raise_application_error
    ( -20000
    , 'Interne fout: '||p_parameter_code||' is onbekend als systeemparameter.'
    );
  END IF;
  IF ( v_ongr_id IS NOT NULL
   AND v_kafd_id IS NULL
     )
  THEN
    OPEN  ongr_cur;
    FETCH ongr_cur
    INTO  v_kafd_id;
    CLOSE ongr_cur;
  END IF;
  IF ( v_kafd_id IS NULL )
  THEN
    v_kafd_id := kgc_mede_00.afdeling_id;
  END IF;
  IF ( v_ongr_id IS NULL )
  THEN
    v_ongr_id := kgc_mede_00.onderzoeksgroep_id( p_kafd_id => v_kafd_id );
  END IF;
  -- 1e poging
  OPEN  spwa_cur( v_kafd_id, v_ongr_id, v_mede_id );
  FETCH spwa_cur
  INTO  v_return;
  IF ( spwa_cur%notfound )
  THEN
    -- 2e poging
    CLOSE spwa_cur;
    OPEN  spwa_cur( v_kafd_id, v_ongr_id, NULL );
    FETCH spwa_cur
    INTO  v_return;
    IF ( spwa_cur%notfound )
    THEN
      -- 3e poging
      CLOSE spwa_cur;
      OPEN  spwa_cur( v_kafd_id, NULL, v_mede_id );
      FETCH spwa_cur
      INTO  v_return;
      IF ( spwa_cur%notfound )
      THEN
        -- 4e poging
        CLOSE spwa_cur;
        OPEN  spwa_cur( v_kafd_id, NULL, NULL );
        FETCH spwa_cur
        INTO  v_return;
        IF ( spwa_cur%notfound )
        THEN
          -- 5e poging
          CLOSE spwa_cur;
          OPEN  spwa_cur( NULL, NULL, v_mede_id );
          FETCH spwa_cur
          INTO  v_return;
          IF ( spwa_cur%notfound )
          THEN
            -- 6e poging
            CLOSE spwa_cur;
            OPEN  spwa_cur( NULL, NULL, NULL );
            FETCH spwa_cur
            INTO  v_return;
            IF ( spwa_cur%notfound )
            THEN
              -- 7e en laatste poging (systeem-waarde)
              CLOSE spwa_cur;
              OPEN  sypa_cur;
              FETCH sypa_cur
              INTO  v_return;
              CLOSE sypa_cur;
            END IF;
          END IF;
        END IF;
      END IF;
    END IF;
  END IF;
  IF ( spwa_cur%isopen )
  THEN
    CLOSE spwa_cur;
  END IF;
  IF ( sypa_cur%isopen )
  THEN
    CLOSE sypa_cur;
  END IF;
  RETURN( v_return );
END standaard_waarde;
-- overloaded
FUNCTION standaard_waarde
( p_parameter_code IN VARCHAR2
, p_afdeling IN VARCHAR2 := NULL
, p_onderzoeksgroep IN VARCHAR2 := NULL
, p_medewerker IN VARCHAR2 := NULL
) RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  CURSOR kafd_cur
  IS
    SELECT kafd_id
    FROM   kgc_kgc_afdelingen
    WHERE  code = UPPER( p_afdeling )
    ;
  kafd_rec kafd_cur%rowtype;
  CURSOR ongr_cur
  IS
    SELECT ongr_id
    FROM   kgc_onderzoeksgroepen
    WHERE  code = UPPER( p_onderzoeksgroep )
    ;
  ongr_rec ongr_cur%rowtype;
  CURSOR mede_cur
  IS
    SELECT mede_id
    FROM   kgc_medewerkers
    WHERE  code = UPPER( p_medewerker )
    ;
  mede_rec mede_cur%rowtype;
BEGIN
  IF ( p_afdeling IS NOT NULL )
  THEN
    OPEN  kafd_cur;
    FETCH kafd_cur
    INTO  kafd_rec;
    CLOSE kafd_cur;
  END IF;
  IF ( p_onderzoeksgroep IS NOT NULL )
  THEN
    OPEN  ongr_cur;
    FETCH ongr_cur
    INTO  ongr_rec;
    CLOSE ongr_cur;
  END IF;
  IF ( p_medewerker IS NOT NULL )
  THEN
    OPEN  mede_cur;
    FETCH mede_cur
    INTO  mede_rec;
    CLOSE mede_cur;
  END IF;
  v_return := standaard_waarde
              ( p_parameter_code => p_parameter_code
              , p_kafd_id => kafd_rec.kafd_id
              , p_ongr_id => ongr_rec.ongr_id
              , p_mede_id => NVL( mede_rec.mede_id, kgc_mede_00.medewerker_id )
              );
  RETURN( v_return );
END standaard_waarde;

FUNCTION medewerker_waarde
( p_sypa_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
  v_kafd_id NUMBER := kgc_mede_00.afdeling_id;
  v_ongr_id NUMBER := kgc_mede_00.onderzoeksgroep_id( p_kafd_id => v_kafd_id );
  v_mede_id NUMBER := kgc_mede_00.medewerker_id;
  CURSOR spwa_cur
  IS
    SELECT spwa.waarde
    FROM   kgc_systeem_par_waarden spwa
    WHERE  spwa.sypa_id = p_sypa_id
    AND  ( spwa.kafd_id = v_kafd_id OR spwa.kafd_id IS NULL )
    AND  ( spwa.mede_id = v_mede_id OR spwa.mede_id IS NULL )
    ORDER BY DECODE( spwa.mede_id
                   , NULL, DECODE( spwa.kafd_id
                                 , NULL, 9
                                 , 4
                                 )
                   , DECODE( spwa.kafd_id
                           , NULL, 7
                           , 1
                           )
                   )
    ;
BEGIN
  OPEN  spwa_cur;
  FETCH spwa_cur
  INTO  v_return;
  CLOSE spwa_cur;
  RETURN( v_return );
END medewerker_waarde;

FUNCTION systeem_waarde
( p_parameter_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
)
RETURN VARCHAR2
IS
BEGIN
  RETURN( standaard_waarde( p_parameter_code => p_parameter_code
                          , p_kafd_id        => p_kafd_id
                          , p_ongr_id        => p_ongr_id
                          , p_mede_id        => p_mede_id
                          )
        );
END systeem_waarde;

FUNCTION categorie_id
( p_parameter_code IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
, p_mede_id IN NUMBER := NULL
)
RETURN NUMBER
IS
  v_return NUMBER;
  v_cate_code VARCHAR2(10);
  CURSOR cate_cur
  IS
    SELECT cate_id
    FROM   kgc_categorieen
    WHERE  code = v_cate_code
    AND  ( kafd_id = p_kafd_id OR kafd_id IS NULL )
    AND  ( ongr_id = p_ongr_id OR ongr_id IS NULL )
    ;
BEGIN
  v_cate_code := standaard_waarde
                 ( p_parameter_code => p_parameter_code
                 , p_kafd_id        => p_kafd_id
                 , p_ongr_id        => p_ongr_id
                 , p_mede_id        => p_mede_id
                 );
  IF ( v_cate_code IS NOT NULL )
  THEN
    OPEN  cate_cur;
    FETCH cate_cur
    INTO  v_return;
    CLOSE cate_cur;
  END IF;
  RETURN( v_return );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( NULL );
END categorie_id;

END kgc_sypa_00;
/

/
QUIT
