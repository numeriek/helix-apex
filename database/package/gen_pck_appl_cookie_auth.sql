CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_APPL_COOKIE_AUTH"
IS
   /*
   =============================================================================
   Package Name : gen_pck_appl_cookie_auth
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_cookie_users
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   FUNCTION f_is_cookie_valid
      RETURN BOOLEAN;
   FUNCTION f_get_user_from_cookie
      RETURN VARCHAR2;
   FUNCTION f_remember_me(i_user VARCHAR2)
      RETURN VARCHAR2;
   FUNCTION f_insert_cookie(i_username  IN VARCHAR2
                           ,i_password  IN VARCHAR2)
      RETURN BOOLEAN;
END gen_pck_appl_cookie_auth;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_APPL_COOKIE_AUTH"
IS
   /*
   =============================================================================
   Package Name : gen_pck_appl_cookie_auth
   Usage        : This package contains functions and procedure referencing
                  table gen_appl_cookie_users
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   03-03-2017  The DOC      Installed
   =============================================================================
   */

   FUNCTION f_get_token(i_user VARCHAR2)
      RETURN VARCHAR2
   IS
      l_token  VARCHAR2(100);
   BEGIN

      SELECT token
        INTO l_token
        FROM gen_appl_users
       WHERE UPPER(user_id) = UPPER(i_user);

      RETURN l_token;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;
   FUNCTION f_remember_me(i_user VARCHAR2)
      RETURN VARCHAR2
   IS
      l_token  gen_appl_users.token%TYPE := f_get_token(i_user);
   BEGIN
      IF l_token IS NULL
      THEN
         l_token      :=
            dbms_random.string('A'
                              ,15);
         UPDATE gen_appl_users
            SET token   = l_token
          WHERE UPPER(user_id) = UPPER(i_user);
      END IF;
      RETURN l_token;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;
   FUNCTION f_is_cookie_valid
      RETURN BOOLEAN
   IS
   BEGIN
      IF f_get_user_from_cookie IS NOT NULL
      THEN
         RETURN TRUE;
      END IF;
      RETURN FALSE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END;
   FUNCTION f_get_user_from_cookie
      RETURN VARCHAR2
   IS
      l_user  gen_appl_users.user_id%TYPE;
      l_c     owa_cookie.cookie;
   BEGIN
      l_c   := owa_cookie.get('REMEMBER_ME');
      SELECT user_id
        INTO l_user
        FROM gen_appl_users
       WHERE token = l_c.vals(1);
      RETURN l_user;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;
   FUNCTION f_insert_cookie(i_username  IN VARCHAR2
                           ,i_password  IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_password         VARCHAR2(4000);
      l_stored_password  VARCHAR2(4000);
      l_token_stored     VARCHAR2(100);
      l_count            NUMBER;
      l_token            gen_appl_users.token%TYPE;
   BEGIN
      SELECT COUNT(*)
        INTO l_count
        FROM gen_appl_users
       WHERE UPPER(user_id) = UPPER(TRIM(i_username))
         AND i_password IS NOT NULL;
      CASE
         WHEN l_count >= 1
         THEN
            l_token      :=
               dbms_random.string('A'
                                 ,15);
            UPDATE gen_appl_users
               SET token   = l_token
             WHERE UPPER(user_id) = UPPER(i_username);
            RETURN TRUE;
         ELSE
            RETURN FALSE;
      END CASE;
   END;
END gen_pck_appl_cookie_auth;
/

/
QUIT
