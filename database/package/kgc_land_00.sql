CREATE OR REPLACE PACKAGE "HELIX"."KGC_LAND_00" IS
-- PL/SQL Specification
FUNCTION controleer
( p_naam IN VARCHAR2
)
RETURN VARCHAR2;

PROCEDURE controleer
( x_naam IN OUT VARCHAR2
)
;
END KGC_LAND_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_LAND_00" IS
FUNCTION controleer
( p_naam IN VARCHAR2
)
RETURN VARCHAR2
IS
  CURSOR land_cur
  ( b_naam IN VARCHAR2 )
  IS
    SELECT naam
    FROM   kgc_landen
    WHERE  UPPER( naam ) LIKE UPPER( b_naam )
    ORDER BY DECODE( naam
                   , p_naam, 1
                   , 2
                   )
    ,        DECODE( UPPER( naam )
                   , UPPER( p_naam ), 1
                   , 2
                   )
    ;
  v_naam VARCHAR2(100) := p_naam;
-- PL/SQL Block
BEGIN
  IF ( p_naam IS NOT NULL )
  THEN
    v_naam := UPPER( v_naam );
    v_naam := REPLACE( v_naam, '_', '%' );
    v_naam := REPLACE( v_naam, '-', '%' );
    v_naam := REPLACE( v_naam, ' ', '%' );
    OPEN  land_cur( v_naam );
    FETCH land_cur
    INTO  v_naam;
    IF ( land_cur%notfound )
    THEN
      v_naam := NULL;
    END IF;
    IF ( land_cur%isopen )
    THEN
      CLOSE land_cur;
    END IF;
  END IF;
  RETURN( v_naam );
END controleer;

PROCEDURE controleer
( x_naam IN OUT VARCHAR2
)
IS
  v_naam VARCHAR2(100) := x_naam;
BEGIN
  IF ( x_naam IS NOT NULL )
  THEN
    v_naam := controleer( x_naam );
    IF ( v_naam IS NULL )
    THEN
      qms$errors.show_message
      ( p_mesg => 'KGC-00007'
      );
    END IF;
    x_naam := v_naam;
  END IF;
END controleer;

END KGC_LAND_00;
/

/
QUIT
