CREATE OR REPLACE PACKAGE "HELIX"."KGC_EMBR_00" IS
  FUNCTION bevoegd(p_mede_id IN NUMBER)
  RETURN VARCHAR2;

  PROCEDURE controleer_autorisatie
( p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2);

 PROCEDURE controleer_autorisatie_pgd
( p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2);

  --CREATED BY Dharmik Vyas ON 06/08/2014 FOR MANTIS 0671
  PROCEDURE check_mons_onderzoek (p_mons_id IN kgc_onderzoek_monsters.mons_id%TYPE);
END KGC_EMBR_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_EMBR_00" IS
FUNCTION bevoegd(p_mede_id IN NUMBER)
RETURN VARCHAR2 IS

  v_return VARCHAR2(32767) := NULL;
  v_dummy  VARCHAR2(1000);

  CURSOR spwa_cur IS
    SELECT spwa.waarde
    FROM kgc_systeem_par_waarden spwa, kgc_systeem_parameters sypa
    WHERE sypa.sypa_id = spwa.sypa_id
    AND sypa.code = 'BEVOEGD_WIJZIG_EMBR'
    AND (spwa.mede_id = p_mede_id );

  BEGIN
    OPEN spwa_cur;
    FETCH spwa_cur
    INTO v_dummy;

    CLOSE spwa_cur;
    v_return := v_dummy;
    RETURN(v_return);

END bevoegd;

-- Controleer de autorisatie van de medewerker via de database-rollen
PROCEDURE controleer_autorisatie
( p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Controle uitgevoerd:
  --    OK=autorisatie
  --    GA=Geen Autorisatie
)
IS
  CURSOR rolp_cur
  ( b_mede_id IN VARCHAR2
  )
  IS
    SELECT NULL
    FROM kgc_role_privs_vw rolp
    WHERE rolp.grantee =
           ( SELECT NVL( mede.oracle_uid, '?' )
             FROM kgc_medewerkers mede
             WHERE ( TO_CHAR( mede.mede_id ) = b_mede_id ))
    AND rolp.granted_role = 'PGD_EMBRYO';

    rolp_rec rolp_cur%ROWTYPE := NULL;

BEGIN


  OPEN rolp_cur
         ( b_mede_id => p_mede_id    );
  FETCH rolp_cur INTO rolp_rec;
  IF rolp_cur%FOUND
  THEN -- OK=autorisatie
    x_status := 'OK';
  ELSE -- GA=Geen Autorisatie
    x_status := 'GA';
  END IF;
  CLOSE rolp_cur;
EXCEPTION
  WHEN OTHERS
  THEN
    x_status := 'OF';
END controleer_autorisatie;

--CREATED BY Dharmik Vyas ON 06/08/2014 FOR MANTIS 0671
--- For Business Rule BR_MONS0005_IER
PROCEDURE check_mons_onderzoek (p_mons_id IN kgc_onderzoek_monsters.mons_id%TYPE)
IS
  CURSOR onde_cur IS
  SELECT onde_id
  FROM kgc_onderzoek_monsters
  WHERE mons_id = p_mons_id;

  v_onde_id kgc_onderzoek_monsters.onde_id%TYPE;
BEGIN
  OPEN onde_cur;
  fetch onde_cur
  INTO v_onde_id;
  CLOSE onde_cur;

  IF v_onde_id IS NOT NULL THEN
    qms$errors.show_message (p_mesg => 'KGC-11421');
  END IF;
END;

-- Controleer de autorisatie van de medewerker via de database-rollen
PROCEDURE controleer_autorisatie_pgd
( p_mede_id       IN  VARCHAR2
, x_status        OUT VARCHAR2
  -- Controle uitgevoerd:
  --    OK=autorisatie
  --    GA=Geen Autorisatie
)
IS
  CURSOR rolp_cur
  ( b_mede_id IN VARCHAR2
  )
  IS
    SELECT NULL
    FROM kgc_role_privs_vw rolp
    WHERE rolp.grantee =
           ( SELECT NVL( mede.oracle_uid, '?' )
             FROM kgc_medewerkers mede
             WHERE ( TO_CHAR( mede.mede_id ) = b_mede_id ))
    AND rolp.granted_role in ('DNA_STAF','CYTO_STAF') ;

    rolp_rec rolp_cur%ROWTYPE := NULL;

BEGIN


  OPEN rolp_cur ( b_mede_id => p_mede_id    );
  FETCH rolp_cur INTO rolp_rec;
  IF rolp_cur%FOUND
  THEN -- OK=autorisatie
    x_status := 'OK';
  ELSE -- GA=Geen Autorisatie
    x_status := 'GA';
  END IF;
  CLOSE rolp_cur;
EXCEPTION
  WHEN OTHERS
  THEN
    x_status := 'OF';
END controleer_autorisatie_pgd;
END KGC_EMBR_00;
/

/
QUIT
