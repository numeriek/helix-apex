CREATE OR REPLACE PACKAGE "HELIX"."KGC_MEDE_00" IS
-- retourneer de (interne) mede_id van een ingelogde user
FUNCTION medewerker_id
( p_user IN VARCHAR2 := NULL
) RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( medewerker_id, WNDS, WNPS, RNPS);

-- retourneer de (interne) kafd_id van de kgc-afdeling van een medewerker
FUNCTION afdeling_id
( p_user IN VARCHAR2 :=NULL
) RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( afdeling_id, WNDS,  WNPS,  RNPS  );

-- retourneer een default where-clause regel voor query van eigen afdelingen
-- (zie forms-library kgclib60.kgc$context.startup)
-- Onderdeel van de beveiliging
FUNCTION kafd_default_where
( p_user IN VARCHAR2 := NULL
) RETURN VARCHAR2;

-- retourneer een where-clause regel voor query in context
-- (zie forms-library kgclib60.kgc$context.startup)
FUNCTION kafd_context_where
( p_user IN VARCHAR2 := NULL
) RETURN VARCHAR2;

-- retourneer de medewerker naam van een ingelogde user
FUNCTION medewerker_naam
( p_user IN VARCHAR2 := NULL
) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( medewerker_naam, WNDS, WNPS, RNPS);

FUNCTION onderzoeksgroep_id
( p_kafd_id IN NUMBER:= NULL
, p_user IN VARCHAR2 := NULL
) RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( onderzoeksgroep_id, WNDS,  WNPS,  RNPS  );

-- retourneer de medewerker code van een ingelogde user
FUNCTION  medewerker_code
(  p_user  IN  VARCHAR2  :=  NULL
)
RETURN  VARCHAR2;
PRAGMA  RESTRICT_REFERENCES(  medewerker_code,  WNDS,  WNPS,  RNPS);

-- retourneer een code die de ondertekenaar/autorisator-rechten van de ingelogde user bij de opgegeven afdeling aangeeft
FUNCTION  medewerker_ond_aut
( p_kafd_id IN NUMBER
, p_user    IN VARCHAR2 := NULL
)
RETURN  VARCHAR2; -- NEE=geen rechten of geen koppeling met afdeling, OND=ondertekenaar, AUT=autorisator/ondertekenaar
PRAGMA  RESTRICT_REFERENCES(  medewerker_ond_aut,  WNDS,  WNPS,  RNPS);
END KGC_MEDE_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MEDE_00" IS
FUNCTION medewerker_id
( p_user IN VARCHAR2 := NULL
)
RETURN NUMBER
IS
  v_return NUMBER;
  CURSOR mede_cur
  IS
    SELECT mede_id
    FROM   kgc_medewerkers
    WHERE  oracle_uid = UPPER(NVL(p_user,USER))
    ;
-- function (zoals sysdate,user) als default voor parameter levert fout op in Oracle 8.1.7 database
-- bij aanroep vanuit form/report!
BEGIN
  OPEN  mede_cur;
  FETCH mede_cur
  INTO  v_return;
  CLOSE mede_cur;
  RETURN( v_return );
END medewerker_id;

FUNCTION medewerker_naam
( p_user IN VARCHAR2 := NULL
)
RETURN   VARCHAR2
IS
  v_return VARCHAR2(100);
  v_mede_id NUMBER;
  CURSOR mede_cur
  IS
    SELECT naam
    FROM   kgc_medewerkers
    WHERE  mede_id = v_mede_id
    ;
BEGIN
  BEGIN
    v_mede_id := p_user;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      v_mede_id := NULL;
  END;
  IF ( v_mede_id IS NULL )
  THEN
    v_mede_id := medewerker_id(p_user);
  END IF;
  OPEN  mede_cur;
  FETCH mede_cur
  INTO  v_return;
  CLOSE mede_cur;
  RETURN( v_return );
END medewerker_naam;

FUNCTION medewerker_code
( p_user IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(10);
  v_mede_id NUMBER;
  CURSOR mede_cur
  IS
    SELECT code
    FROM   kgc_medewerkers
    WHERE  mede_id = v_mede_id
    ;
BEGIN
  BEGIN
    v_mede_id := p_user;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      v_mede_id := NULL;
  END;
  IF ( v_mede_id IS NULL )
  THEN
    v_mede_id := medewerker_id(p_user);
  END IF;
  OPEN  mede_cur;
  FETCH mede_cur
  INTO  v_return;
  CLOSE mede_cur;
  RETURN( v_return );
END medewerker_code;

FUNCTION afdeling_id
( p_user IN VARCHAR2 := NULL
)
RETURN    NUMBER
IS
  v_return NUMBER;
--  v_afdeling VARCHAR2(30);
  CURSOR kafd_cur
  IS
    SELECT kafd.kafd_id
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_systeem_par_waarden spwa
    ,      kgc_systeem_parameters sypa
    WHERE  kafd.code = UPPER(spwa.waarde)
    AND    sypa.code = 'MEDEWERKER_AFDELING'
    AND    sypa.sypa_id = spwa.sypa_id
    AND    spwa.mede_id = medewerker_id( p_user )
    ;
   BEGIN
/*
  -- vanwege overloaded gc_sypa_00.standaard_waarde is pragma restrict_references niet mogelijk
  v_afdeling := kgc_sypa_00.standaard_waarde
                ( p_parameter_code => 'MEDEWERKER_AFDELING'
                , p_kafd_id => NULL
                , p_ongr_id => NULL
                , p_mede_id => medewerker_id( p_user => p_user )
                );
  OPEN  kafd_cur( v_afdeling );
  FETCH kafd_cur
  INTO  v_return;
  CLOSE kafd_cur;
*/
/* */
  OPEN  kafd_cur;
  FETCH kafd_cur
  INTO  v_return;
  CLOSE kafd_cur;
  RETURN( v_return );
END afdeling_id;

FUNCTION kafd_default_where
( p_user IN VARCHAR2 := NULL
)
RETURN   VARCHAR2
IS
  v_where VARCHAR2(1000);
  v_mede_id NUMBER := medewerker_id( p_user => p_user );
BEGIN
  -- kafd_id kan optioneel zijn!!!
  v_where := ' AND ( kafd_id IS NULL OR kafd_id IN'
          || ' (SELECT kafd_id FROM kgc_mede_kafd'
          || ' WHERE mede_id = '||NVL(TO_CHAR(v_mede_id), 'mede_id')
          || '))'
           ;
  RETURN( v_where );
END kafd_default_where;

FUNCTION kafd_context_where
( p_user IN VARCHAR2 := NULL
)
RETURN   VARCHAR2
IS
  v_where VARCHAR2(1000);
  v_kafd_id NUMBER := afdeling_id( p_user => p_user );
  BEGIN
  IF ( v_kafd_id IS NOT NULL )
  THEN
    v_where := ' AND kafd_id = '||TO_CHAR( v_kafd_id );
  ELSE
    v_where := NULL;
  END IF;
  RETURN( v_where );
END kafd_context_where;

FUNCTION onderzoeksgroep_id
( p_kafd_id IN NUMBER := NULL
, p_user IN VARCHAR2 := NULL
)
RETURN NUMBER
IS
  v_return NUMBER;

  v_user VARCHAR2(30) := NVL( p_user, USER );
  v_kafd_id NUMBER := NVL( p_kafd_id, kgc_mede_00.afdeling_id( v_user ) );
  v_mede_id NUMBER := kgc_mede_00.medewerker_id( v_user );

  CURSOR ongr_cur
  IS
    SELECT ongr.ongr_id
    FROM   kgc_onderzoeksgroepen ongr
    ,      kgc_systeem_par_waarden spwa
    ,      kgc_systeem_parameters sypa
    WHERE  ongr.kafd_id = NVL( v_kafd_id, ongr.kafd_id )
    AND    ongr.code = UPPER(spwa.waarde)
    AND    sypa.code = 'MEDEWERKER_ONDERZOEKSGROEP'
    AND    sypa.sypa_id = spwa.sypa_id
    AND  ( spwa.kafd_id = v_kafd_id OR spwa.kafd_id IS NULL )
    AND  ( spwa.mede_id = v_mede_id OR spwa.mede_id IS NULL )
    ORDER BY DECODE( spwa.mede_id
                   , NULL, 9
                   , 1
                   ) ASC
    ;
BEGIN
  OPEN  ongr_cur;
  FETCH ongr_cur
  INTO  v_return;
  CLOSE ongr_cur;
  RETURN( v_return );
END onderzoeksgroep_id;

-- retourneer een code die de ondertekenaar/autorisator-rechten van de ingelogde user bij de opgegeven afdeling aangeeft
FUNCTION  medewerker_ond_aut
( p_kafd_id IN NUMBER
, p_user    IN VARCHAR2 := NULL
)
RETURN  VARCHAR2 -- NEE=geen rechten of geen koppeling met afdeling, OND=ondertekenaar, AUT=autorisator/ondertekenaar
IS
  v_mede_id  NUMBER;
  CURSOR   meka_cur
  IS
    SELECT meka.meka_id
         , meka.ondertekenaar
         , meka.autorisator
    FROM   kgc_mede_kafd meka
    WHERE  meka.mede_id = v_mede_id
    AND    meka.kafd_id = p_kafd_id
    ;
  meka_rec meka_cur%ROWTYPE;
BEGIN
  BEGIN
    v_mede_id := p_user;
  EXCEPTION
    WHEN VALUE_ERROR
    THEN
      v_mede_id := NULL;
  END;
  IF ( v_mede_id IS NULL )
  THEN
    v_mede_id := medewerker_id(p_user);
  END IF;
  OPEN  meka_cur;
  FETCH meka_cur
  INTO  meka_rec;
  CLOSE meka_cur;
  IF    ( meka_rec.meka_id IS NULL )
  THEN -- geen koppeling
    RETURN( 'NEE' );
  ELSIF ( meka_rec.autorisator = 'J' )
  THEN
    RETURN( 'AUT' );
  ELSIF ( meka_rec.ondertekenaar = 'J' )
  THEN
    RETURN( 'OND' );
  ELSE
    RETURN( 'NEE' );
  END IF;
END medewerker_ond_aut;
END kgc_mede_00;
/

/
QUIT
