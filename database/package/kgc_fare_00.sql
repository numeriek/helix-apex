CREATE OR REPLACE PACKAGE "HELIX"."KGC_FARE_00" IS
FUNCTION id
(  p_code     IN  VARCHAR2
) RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( id, WNDS, WNPS, RNPS );
END kgc_fare_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FARE_00" IS
FUNCTION id
(  p_code     IN  VARCHAR2
) RETURN NUMBER IS
  CURSOR fare_cur IS
    SELECT fare.fare_id
    FROM   kgc_familie_relaties fare
    WHERE  fare.code    = p_code
    ;
  fare_rec  fare_cur%ROWTYPE;
BEGIN
  IF  p_code IS NOT NULL
  THEN
    OPEN  fare_cur;
    FETCH fare_cur
    INTO  fare_rec;
    CLOSE fare_cur;
  END IF;
  RETURN fare_rec.fare_id;
END id;

END kgc_fare_00;
/

/
QUIT
