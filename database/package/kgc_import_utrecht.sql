CREATE OR REPLACE PACKAGE       KGC_IMPORT_UTRECHT IS

   PROCEDURE vertaal_naar_helix(p_imfi_id NUMBER, x_imfi_verslag IN OUT VARCHAR2);

   FUNCTION specifieke_verwerking(p_imfi cg$kgc_import_files.cg$row_type
                                 ,p_plekvanaanroep VARCHAR2
                                 ,x_imfi_verslag IN OUT VARCHAR2
                                 ) RETURN VARCHAR2;


END KGC_IMPORT_UTRECHT;
/


CREATE OR REPLACE PACKAGE BODY       KGC_IMPORT_UTRECHT IS


   PROCEDURE vertaal_naar_helix(p_imfi_id NUMBER, x_imfi_verslag IN OUT VARCHAR2) IS
$IF (kgc_coco_00.v_is_locatie_utrecht)
$THEN
      CURSOR c_imfi IS
         SELECT imlo.directorynaam
         ,      hlbe.hlbe_id
         ,      hlbe.bericht
         FROM   kgc_import_locaties imlo
         ,      kgc_import_files imfi
         ,      kgc_hl7_berichten hlbe
         WHERE  imfi.imfi_id = p_imfi_id
         AND    imfi.imlo_id = imlo.imlo_id
         AND    imfi.hlbe_id = hlbe.hlbe_id
         AND    imlo.interface_type NOT IN ('M') -- mosos
         ;
      r_imfi c_imfi%ROWTYPE;
      v_hl7_msg              kgc_zis.hl7_msg;
      v_msh_directorynaam    kgc_import_locaties.directorynaam%TYPE;
      v_dna_type_diagnostiek VARCHAR2(100);
      v_tab_mons_mat         kgc_interface_hl7.t_tab_waarden;
      v_tab_ondes            kgc_interface_hl7.t_tab_waarden;
      v_ondes_omschrijving   VARCHAR2(32767);
      v_tab_ind_mat_ond      kgc_interface_hl7.t_tab_waarden;
      v_tab_indis            kgc_interface_hl7.t_tab_waarden;
      v_indis_omschrijving   VARCHAR2(32767);
      v_klin_geg             VARCHAR2(32767);
      v_col_ar_obx           kgc_interface_hl7.t_col_ar;
      v_aantal_onde          NUMBER;
   BEGIN
      OPEN c_imfi;
      FETCH c_imfi INTO r_imfi;
      IF c_imfi%NOTFOUND THEN -- mosos!
         CLOSE c_imfi;
         RETURN;
      END IF;
      CLOSE c_imfi;
      v_hl7_msg := kgc_zis.str2hl7msg
                   ( r_imfi.hlbe_id
                   , r_imfi.bericht
                   );
      -- eventuele hpo-codes bij IMFI omzetten naar ONDE / MONS
      kgc_interface_hl7.bepaal_bericht_data
      ( p_hl7_msg               => v_hl7_msg
      , x_directorynaam         => v_msh_directorynaam
      , x_dna_type_diagnostiek  => v_dna_type_diagnostiek
      , x_tab_ind_mat_ond       => v_tab_ind_mat_ond
      , x_tab_mons_mat          => v_tab_mons_mat
      , x_tab_indis             => v_tab_indis
      , x_indis_omschrijving    => v_indis_omschrijving
      , x_tab_ondes             => v_tab_ondes
      , x_ondes_omschrijving    => v_ondes_omschrijving
      , x_klin_geg              => v_klin_geg
      , x_col_ar_obx            => v_col_ar_obx
      );

      IF v_dna_type_diagnostiek = 'NGS'
         OR
         r_imfi.directorynaam IN (kgc_interface_hl7.g_type_vw_array)
      THEN
         -- bepaal v_aantal_onde
         SELECT COUNT(*)
         INTO   v_aantal_onde
         FROM   kgc_im_onderzoeken imon
         WHERE  imon.imfi_id = p_imfi_id
         ;
         -- verwerk
         kgc_interface_hl7.verwerk_hpo
         ( p_imfi_id              => p_imfi_id
         , p_naar_notitie         => 'N'
         , p_klin_geg             => v_klin_geg
         , p_aantal_onde          => v_aantal_onde
         , p_dna_type_diagnostiek => v_dna_type_diagnostiek
         , p_directorynaam        => r_imfi.directorynaam
         , p_tab_indis            => v_tab_indis
         , p_col_ar_obx           => v_col_ar_obx
         , x_imfi_verslag         => x_imfi_verslag
         );
      END IF;
$ELSE
   BEGIN
      NULL;
$END

   END vertaal_naar_helix;

   FUNCTION specifieke_verwerking(p_imfi cg$kgc_import_files.cg$row_type
                                 ,p_plekvanaanroep VARCHAR2 -- VOOR, NA_CHECK, NA_VERW
                                 ,x_imfi_verslag IN OUT VARCHAR2
                                 ) RETURN VARCHAR2 IS
   BEGIN
      RETURN 'D'; -- doorgaan...
   END specifieke_verwerking;


END KGC_IMPORT_UTRECHT;
/
