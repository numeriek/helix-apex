CREATE OR REPLACE PACKAGE "HELIX"."KGC_FORMS_00" IS
-- PL/SQL Specification
-- als return-waarde van een dynamische LOV niet in global (max 255 tekens) past
-- dan proberen tekst op te halen met behulp van de andere waarden
FUNCTION lov_tekst
( p_lov_query IN varchar2
, p_lov_code in varchar2
, p_lov_description in varchar2
, p_lov_id in varchar2
, p_taal_id in number := null
, p_kafd_id in number := null
, p_ongr_id in number := null
)
RETURN varchar2;

END KGC_FORMS_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FORMS_00" IS
FUNCTION lov_tekst
( p_lov_query IN VARCHAR2
, p_lov_code IN VARCHAR2
, p_lov_description IN VARCHAR2
, p_lov_id IN VARCHAR2
, p_taal_id IN NUMBER := NULL
, p_kafd_id IN NUMBER := NULL
, p_ongr_id IN NUMBER := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(4000);
  v_from_deel VARCHAR2(1000);
  v_statement VARCHAR2(2000);
-- PL/SQL Block
BEGIN
  IF ( p_lov_description IS NULL )
  THEN
    v_return := NULL;
  ELSIF ( LENGTH( p_lov_description ) < 200 )
  THEN
    v_return := p_lov_description;
  ELSE
    v_from_deel := kgc_util_00.strip
                   ( p_string => UPPER( p_lov_query )
                   , p_tag_va => 'FROM'
                   , p_tag_tm => 'WHERE'
                   , p_rest => 'N'
                   );
    IF ( INSTR( v_from_deel, 'KGC_CONCLUSIE_TEKSTEN' ) > 0
      OR INSTR( v_from_deel, 'KGC_CONC_CONTEXT_VW' ) > 0
       )
    THEN
      v_statement := 'select nvl(kgc_vert_00.vertaling( ''CONC'',conc_id,null,'||NVL( TO_CHAR(p_taal_id),'null')||'),omschrijving)'
         ||CHR(10)|| 'from kgc_conclusie_teksten'
                   ;
      IF ( p_lov_id IS NOT NULL )
      THEN
        v_statement := v_statement
           ||CHR(10)|| 'where conc_id = '||p_lov_id
                     ;
      ELSIF ( p_lov_code IS NOT NULL )
      THEN
        v_statement := v_statement
           ||CHR(10)|| 'where kafd_id = '||TO_CHAR(p_kafd_id)
           ||CHR(10)|| 'and code = '''||p_lov_code||''''
                     ;
      END IF;
    ELSIF ( INSTR( v_from_deel, 'KGC_RESULTAAT_TEKSTEN' ) > 0
         OR INSTR( v_from_deel, 'KGC_RETE_CONTEXT_VW' ) > 0
          )
    THEN
      v_statement := 'select nvl(kgc_vert_00.vertaling( ''RETE'',rete_id,null,'||NVL( TO_CHAR(p_taal_id),'null')||'),omschrijving)'
         ||CHR(10)|| 'from kgc_resultaat_teksten'
                   ;
      IF ( p_lov_id IS NOT NULL )
      THEN
        v_statement := v_statement
           ||CHR(10)|| 'where rete_id = '||p_lov_id
                     ;
      ELSIF ( p_lov_code IS NOT NULL )
      THEN
        v_statement := v_statement
           ||CHR(10)|| 'where kafd_id = '||TO_CHAR(p_kafd_id)
           ||CHR(10)|| 'and code = '''||p_lov_code||''''
                     ;
      END IF;
    ELSIF ( INSTR( v_from_deel, 'KGC_TOELICHTING_TEKSTEN' ) > 0
         OR INSTR( v_from_deel, 'KGC_TOELICHTING_TEKSTEN_VW' ) > 0
         OR INSTR( v_from_deel, 'KGC_TOTE_CONTEXT_VW' ) > 0
          )
    THEN
--      v_statement := 'select nvl(kgc_vert_00.vertaling( ''TOTE'',tote_id,null,'||NVL( TO_CHAR(p_taal_id),'null')||'),omschrijving)'
--         ||CHR(10)|| 'from kgc_toelichting_teksten' -- !!!
--                   ;
      v_statement := 'select max(nvl(kgc_vert_00.vertaling( entiteit,id,null,'||NVL( TO_CHAR(p_taal_id),'null')||'),tekst))'
         ||chr(10)|| 'from kgc_toelichting_teksten_vw' -- !!!
                   ;
      IF ( p_lov_id IS NOT NULL )
      THEN
        v_statement := v_statement
           ||CHR(10)|| 'where id = '||p_lov_id
                     ;
      ELSIF ( p_lov_code IS NOT NULL )
      THEN
        v_statement := v_statement
           ||CHR(10)|| 'where kafd_id = '||TO_CHAR(p_kafd_id)
           ||CHR(10)|| 'and code = '''||p_lov_code||''''
                     ;
      END IF;
    END IF;
    v_return := kgc_util_00.dyn_exec( v_statement );
  END IF;
  RETURN( v_return );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( v_statement );
END lov_tekst;

END KGC_FORMS_00;
/

/
QUIT
