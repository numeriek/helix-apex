CREATE OR REPLACE PACKAGE "HELIX"."KGC_DFT_MAASTRICHT" IS
  /******************************************************************************
     NAME:       kgc_dft_maastricht
     PURPOSE:    controleren van declaratiegegevens en overzetten naar financieel systeem

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     2.4        08-09-2010  GWE              Mantis 1268: Alternatief persoon voor declaratie
     2          17-04-2007  RDE              Gelijktrekken met Nijmegen/Utrecht
     1          04-07-2006  RDE              op locatie
  ***************************************************************************** */

  PROCEDURE open_verbinding;

  PROCEDURE sluit_verbinding;

  PROCEDURE controleer(p_decl_id IN NUMBER, x_ok OUT BOOLEAN);

  PROCEDURE verwerk(p_decl_id IN NUMBER, x_ok OUT BOOLEAN);

  PROCEDURE draai_terug(p_decl_id IN NUMBER, x_ok OUT BOOLEAN);

  PROCEDURE test_bepaal_controle;

  /* Controleer of de Foetus overleden is Op basis van een ONDE_ID */
  Function FoetusDeceased_Onde(PID Integer) return Varchar2;

  /* Controleer of de Foetus overleden is Op basis van een METI_ID */
  Function FoetusDeceased_Meti(PID Integer) return varchar2;

  /* Controleer of de Persoon overleden is Op basis van een ONBE_ID  */
  Function FoetusDeceased_Onbe(PID Integer) return Varchar2;

  /* VOEG COMMENTAAR TOE AAN KGC_DCLARATIES */
  PROCEDURE UpdateCommentsDecl(P_RAP_CODE     IN VARCHAR2,
                               PDECL_ID       IN INTEGER,
                               PVERWERKINGTXT IN VARCHAR2);
END KGC_DFT_MAASTRICHT;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DFT_MAASTRICHT" IS
  /******************************************************************************
  NAME: kgc_dft_maastricht
  PURPOSE: controleren van declaratiegegevens en overzetten naar financieel systeem

  REVISIONS:
  Ver Date Author Description
  --------- ---------- --------------- ------------------------------------
  2.4  08-09-2010 Gerrit Wiskerke   Mantis 1268: Alternatief persoon voor declaratie
  2.3  02-07-2010 Marc Woutersen In check_verstuur_dft_p03 is het volgende aangepast:
       Toegevoegd voor alle afdelingen: Als aan de entiteit (ONDE/ONBE/METI/GESP) een vervallen onderzoek hangt,
       wordt de betreffende declaratie tegengehouden
       Aangapast voor DNA: Als bij het onderzoek declaratiewijze BU, BU_1, BU_2, BU_3 of BU_4 is vastgelegd,
       moet de aanvragerscode (zowel arts als afdeling) op DIV worden gezet
  2.2  29-06-2010 Marc Woutersen In check_verstuur_dft_p03 code r_rela.inst_code = 'AZM' vervangen door r_rela.inst_code like 'AZM%'
  2.1  16-12-2009 Roy Konings    Toevoegen laatste commentaar aan programma blok ten behoeve
                                 van de code NB.
  2.0  23-11-2009 Roy Konings Herzien van programmatuur betreffende code NB.
  1.10 13-11-2009 Roy Konings Aanpassingen Package op basis van wensen DNA.
  1.9  20-07-2009 Roy Konings Update aanpassing Code NB
  1.8  10-06-2009 Roy Konings Aanpassing Aanvr/prod AfdelingCode
  1.7  01-06-2009 Roy Konings Aanpassing ABW CODE op NB overledenen
  1.6  04-02-2009 RDE Aanpassing tbv CYTO
  1.5  21-11-2007 RDE Aanpassing tbv DNA - DIV gewijzigd
  1.4  02-10-2007 RDE Aanpassing tbv DNA
  1.3  30-05-2007 RDE Aanpassing tbv Metabool
  1.2  17-04-2007 RDE Aanpassing tbv Metabool
  1.0  16-10-2006 MKL Op basis van kgc_dft_utrecht (Rob Debets)

  ***************************************************************************** */

  -- sla systeemparameter-waarden ZIS_DFT_CONTOLES op in pl-sqltable
  -- 1= algemeen nivo
  -- 2= afdelingsnivo
  -- 3=onderzoeksgroepnivo
  TYPE t_tab_controles IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;

  -- lokaal
  FUNCTION bepaal_controle -- zie testfunctie test_bepaal_controle
  (p_controles    IN t_tab_controles
  ,p_ccode        IN VARCHAR2
  ,x_ccode_parm_s OUT VARCHAR2
  ,x_ccode_parm_n OUT NUMBER) RETURN BOOLEAN IS
    v_pos           NUMBER;
    v_pos_parm      NUMBER;
    v_code_aanwezig BOOLEAN := FALSE;
  BEGIN
    --dbms_output.put_line('HELIX.KGC_DFT_maastricht.bepaal_controle: 2: '||p_ccode || ';#c=' || to_char(p_controles.COUNT));
    x_ccode_parm_s := NULL;
    x_ccode_parm_n := NULL;
    IF p_controles.COUNT = 0
    THEN
      DBMS_OUTPUT.PUT_LINE('Geen controles aanwezig!');
      RETURN FALSE;
    END IF;
    FOR i IN REVERSE p_controles.FIRST .. p_controles.LAST
    LOOP
      -- begin bij laagste nivo
      v_code_aanwezig := TRUE;
      -- aantal toegestane plekken
      v_pos := INSTR(p_controles(i)
                    ,',' || p_ccode || ':');
      IF v_pos > 0
      THEN
        -- code gevonden met parm, bv als 2e code of verder
        v_pos_parm := v_pos + 2 + LENGTH(p_ccode);
      ELSE
        -- verder zoeken
        v_pos := INSTR(p_controles(i)
                      ,',' || p_ccode || ',');
        IF v_pos > 0
        THEN
          v_pos_parm := 0; -- wel code, geen parm, bv als 2e code of verder
        ELSE
          -- verder zoeken
          IF p_controles(i) LIKE p_ccode || ':%'
          THEN
            -- OK, aan begin
            v_pos_parm := 2 + LENGTH(p_ccode);
          ELSE
            -- verder zoeken
            IF p_controles(i) LIKE p_ccode || ',%'
            THEN
              -- OK, aan begin
              v_pos_parm := 0; -- geen parm
            ELSE
              -- verder zoeken
              IF p_controles(i) LIKE '%,' || p_ccode
              THEN
                -- OK, aan eind
                v_pos_parm := 0; -- geen parm
              ELSE
                v_code_aanwezig := FALSE;
              END IF;
            END IF;
          END IF;
        END IF;
      END IF;
      IF v_code_aanwezig
      THEN
        -- code gevonden? evt parm ophalen en klaar
        IF v_pos_parm > 0
        THEN
          v_pos := INSTR(p_controles(i)
                        ,','
                        ,v_pos_parm);
          IF v_pos > 0
          THEN
            -- er is een volgende controlecode
            x_ccode_parm_s := SUBSTR(p_controles(i)
                                    ,v_pos_parm
                                    ,v_pos - v_pos_parm);
          ELSE
            -- tot einde
            x_ccode_parm_s := SUBSTR(p_controles(i)
                                    ,v_pos_parm);
          END IF;
          IF NVL(LENGTH(x_ccode_parm_s)
                ,0) > 0
          THEN
            -- probeer numeriek
            BEGIN
              x_ccode_parm_n := TO_NUMBER(x_ccode_parm_s);
            EXCEPTION
              WHEN OTHERS THEN
                NULL;
            END;
          END IF;
        END IF;
        RETURN TRUE;
      END IF;
    END LOOP;
    --dbms_output.put_line('HELIX.KGC_DFT_maastricht.bepaal_controle: Found! parm: '||x_ccode_parm_s);
    RETURN FALSE;
  END bepaal_controle;

  /*-------------------------------------------------------------------------------------------
  ----------- CHECK_VERSTUUR_DFT_P03 ----------------------------------------------------------

  ---------------------------------------------------------------------------------------------
  November 20009 Roy Konings:
  Commentaar aan specificatie blok van onderstaande procedure toegevoegd,
  Ter verduidelijking van de Procedure Variabele specificaties.
   */
  FUNCTION check_verstuur_dft_p03(p_decl_id    IN NUMBER
                                 ,p_aantal     IN NUMBER
                                 ,p_check_only IN BOOLEAN) RETURN BOOLEAN IS
    ------------------------------------------------------
    ----- GEGEVENS DECLARATIE TABEL ----------------------
    ------------------------------------------------------
    CURSOR c_decl(p_decl_id NUMBER) IS
      SELECT decl.*
            ,kafd.code kafd_code
            ,dewy.code dewy_code
      FROM   KGC_DECLARATIES      decl
            ,KGC_KGC_AFDELINGEN   kafd
            ,KGC_DECLARATIEWIJZEN dewy
      WHERE  decl.decl_id = p_decl_id
      AND    decl.kafd_id = kafd.kafd_id
      AND    decl.dewy_id = dewy.dewy_id(+);

    r_decl c_decl%ROWTYPE; -- DECLARATIE RECORD.

    -------------------------------------------------------
    -------- GEGEVENS RELATIES/AFDELINGEN/INSTELLINGEN. ---
    -------------------------------------------------------
    CURSOR c_rela(p_rela_id NUMBER) IS
      SELECT rela.*
            ,afdg.omschrijving afdg_omschrijving
            ,inst.code inst_code
      FROM   KGC_RELATIES     rela
            ,KGC_AFDELINGEN   afdg
            ,KGC_INSTELLINGEN inst
      WHERE  rela.rela_id = p_rela_id
      AND    rela.afdg_id = afdg.afdg_id(+)
      AND    rela.inst_id = inst.inst_id(+);

    r_rela c_rela%ROWTYPE; -- RELATIE RECORD

    ------------------------------------------------------
    -------- GEGEVENS ONDERZOEKEN ------------------------
    ------------------------------------------------------
    CURSOR c_onde(p_onde_id NUMBER) IS
      SELECT onde.*
      FROM   KGC_ONDERZOEKEN onde
      WHERE  onde.onde_id = p_onde_id;

    r_onde c_onde%ROWTYPE; -- ONDERZOEK RECORD

    ------------------------------------------------------
    ---------- GEGEVENS ONDERZOEKS BETROKKENEN -----------
    ------------------------------------------------------
    CURSOR c_onbe(p_onbe_id NUMBER) IS
      SELECT *
      FROM   KGC_ONDERZOEK_BETROKKENEN onbe
      WHERE  onbe.onbe_id = p_onbe_id;

    r_onbe c_onbe%ROWTYPE; --- ONDERZOEKSBETROKKENE RECORD.

    --------------------------------------------------------
    --------- GEGEVENS METINGEN ----------------------------
    --------------------------------------------------------
    CURSOR c_meti(p_meti_id NUMBER) IS
      SELECT * FROM BAS_METINGEN meti WHERE meti.meti_id = p_meti_id;

    r_meti c_meti%ROWTYPE; -- METING RECORD.

    ---------------------------------------------------------
    ----------- GEGEVENS GESPREKKEN--------------------------
    ---------------------------------------------------------

    CURSOR c_gesp(p_gesp_id NUMBER) IS
      SELECT gesp.*
      FROM   KGC_GESPREKKEN gesp
      WHERE  gesp.gesp_id = p_gesp_id;

    r_gesp c_gesp%ROWTYPE; -- GESPREK RECORD

    ----------------------------------------------------------
    ----------- GEGEVENS CATEGORIEN --------------------------
    ----------------------------------------------------------

    CURSOR c_cate(p_kafd_id NUMBER, p_verrichtingcode VARCHAR2) IS
      SELECT NULL
      FROM   KGC_CATEGORIE_TYPES   caty
            ,KGC_CATEGORIEEN       cate
            ,KGC_CATEGORIE_WAARDEN cawa
      WHERE  caty.code = 'VER_CODES'
      AND    cate.kafd_id = p_kafd_id
      AND    cawa.waarde = p_verrichtingcode
      AND    caty.caty_id = cate.caty_id
      AND    cate.cate_id = cawa.cate_id;
    r_cate c_cate%ROWTYPE; -- CATEGORIE RECORD
    ----------------------------------------------------------
    ----------- GEGEVENS ONDERZOEKSINDICATIES ----------------
    ----------------------------------------------------------

    CURSOR c_onin(p_onde_id NUMBER) IS
      SELECT ingr_id
            ,indi_id
            ,onre_id
      FROM   KGC_ONDERZOEK_INDICATIES onin
      WHERE  onin.onde_id = p_onde_id;
    r_onin c_onin%ROWTYPE;
    CURSOR c_sypa(p_code VARCHAR2) IS
      SELECT sypa_id
            ,standaard_waarde
      FROM   KGC_SYSTEEM_PARAMETERS
      WHERE  code = UPPER(p_code);
    r_sypa c_sypa%ROWTYPE;

    ------------------------------------------------------------
    ------------SYSTEEM PAR WAARDEN RECORD ---------------------
    ------------------------------------------------------------

    CURSOR c_spwa(p_sypa_id IN NUMBER, p_kafd_id IN NUMBER, p_ongr_id IN NUMBER) IS
      SELECT waarde
      FROM   KGC_SYSTEEM_PAR_WAARDEN spwa
      WHERE  spwa.sypa_id = p_sypa_id
      AND    spwa.kafd_id = p_kafd_id
      AND    (spwa.ongr_id = p_ongr_id OR
            (p_ongr_id IS NULL AND spwa.ongr_id IS NULL));

    r_spwa c_spwa%ROWTYPE; -- Systeem Par waarden Record

    ---------------------------------------------------------------
    --------- FOETUS MATERIAAL DNA BIJ ONDE_ID XXXX (JA/NEE) ------
    --------- added by RKO on 13-11-2009 --------------------------
    ---------------------------------------------------------------
    CURSOR c_mate_x(P_onde_id INTEGER) IS
      SELECT DISTINCT 'J'
      FROM   kgc_materialen         MATE
            ,kgc_monsters           MONS
            ,Kgc_onderzoeken        ONDE
            ,Kgc_Onderzoek_Monsters ONMO
      WHERE  MATE.Mate_Id = Mons.Mate_Id
      AND    ONMO.onde_id = ONDE.onde_id
      AND    ONMO.mons_id = MONS.mons_id
      AND    MATE.code = 'FM'
      AND    MATE.Kafd_Id = 48
      AND    ONDE.onde_id = P_onde_id;

    r_mate_x C_mate_x%ROWTYPE; -- Record C_mate_x;

    v_hl7_msg_bron Kgc_Zis.hl7_msg;
    v_hl7_msg_res  Kgc_Zis.hl7_msg;

    v_pers_trow Cg$kgc_Personen.cg$row_type;
    v_rela_trow Cg$kgc_Relaties.cg$row_type;
    v_verz_trow Cg$kgc_Verzekeraars.cg$row_type;
    v_onde_id   NUMBER;

    -- speciale Maastrichtse gegevens gebruikt in het bericht:
    v_spec_systeemdeel    VARCHAR2(8); -- specifiek 1
    v_spec_prod_afdeling  VARCHAR2(4); -- specifiek 2
    v_spec_verdatum       DATE; -- specifiek 4
    v_spec_aanvr_afdeling VARCHAR2(4); -- specifiek 5
    v_bron_aanvr_afdeling VARCHAR2(100);
    v_spec_prod_spec      VARCHAR2(4); -- specifiek 6
    v_spec_aanvr_spec     VARCHAR2(10); -- specifiek 7
    v_bron_aanvr_spec     VARCHAR2(100);
    v_spec_abw            VARCHAR2(2); -- specifiek 8
    v_spec_abi_verzcd     VARCHAR2(10); -- specifiek 9
    v_spec_abi_verznr     VARCHAR2(10); -- specifiek 10
    v_spec_aanvrager      VARCHAR2(2000); -- specifiek 11
    v_spec_vercode        VARCHAR2(20); -- specifiek 12
    v_dewy_id_BU          NUMBER;
    v_dewy_id_BU_1        NUMBER;
    v_dewy_id_BU_2        NUMBER;
    v_dewy_id_BU_3        NUMBER;
    v_dewy_id_BU_4        NUMBER;
    v_pers_is_dummy       BOOLEAN;
    v_verdat_dagen        NUMBER;
    v_sysdate             VARCHAR2(20) := TO_CHAR(SYSDATE
                                                 ,'yyyymmddhh24miss');
    v_tab_controles       t_tab_controles;
    v_controle_sypa       VARCHAR2(100) := 'ZIS_DFT_CONTROLES';
    v_ccode_aanwezig      BOOLEAN;
    v_ccode_parm_s        VARCHAR2(4000);
    v_ccode_parm_n        NUMBER;
    v_strAntw             VARCHAR2(32767);
    v_strDFT              VARCHAR2(32767);
    v_MSA                 VARCHAR2(10);
    v_MSH                 VARCHAR2(10);
    v_ok                  BOOLEAN;
    v_msg_control_id      VARCHAR2(10);
    v_msg                 VARCHAR2(32767);
    v_sep                 VARCHAR2(2);
    v_nl                  VARCHAR2(2) := CHR(10);
    v_debug               VARCHAR2(100);
    v_thisproc            VARCHAR2(100) := 'kgc_dft_maastricht.check_verstuur_dft_p03';
    V_FOET_OVL            VARCHAR2(1); -- FOETUS NIET GEBOREN JA/NEE

    /* Onderstaande prameter komt erbij omdat het checken met verleden datum fout loopt. */
    V_VerDat_verl DATE; -- Check of verrrichtings datum te ver in het verleden ligt -- R Konings

    -- geeft id van onderzoekswijze, tbv.  zoeken naar verrichtingcode (attribuut)
    FUNCTION onwy_id(b_kafd_id IN NUMBER, b_code IN VARCHAR2) RETURN NUMBER IS
      v_return NUMBER;
      CURSOR onwy_cur IS
        SELECT onwy_id
        FROM   KGC_ONDERZOEKSWIJZEN
        WHERE  kafd_id = b_kafd_id
        AND    code = b_code;
    BEGIN
      IF (b_code IS NOT NULL)
      THEN
        OPEN onwy_cur;
        FETCH onwy_cur
          INTO v_return;
        CLOSE onwy_cur;
      END IF;
      RETURN(v_return);
    END onwy_id;

    PROCEDURE add2msg(p_tekst VARCHAR2) IS
    BEGIN
      v_msg := v_msg || v_sep || p_tekst;
      v_sep := v_nl;
    END;

    PROCEDURE upd_decl(p_decl_id          NUMBER
                      ,p_status           VARCHAR2
                      ,p_datum_verwerking DATE
                      ,p_verwerking       VARCHAR2) IS
    BEGIN
      UPDATE KGC_DECLARATIES
      SET    verwerking       = p_verwerking
            ,status           = p_status
            ,datum_verwerking = p_datum_verwerking
      WHERE  decl_id = p_decl_id;
      COMMIT;
    END upd_decl;
  BEGIN
    Kgc_Zis.g_sypa_hl7_versie := Kgc_Sypa_00.systeem_waarde('HL7_VERSIE');
    v_msg_control_id          := SUBSTR(TO_CHAR(ABS(dbms_random.RANDOM))
                                       ,1
                                       ,10);

    -- ** generieke controles **
    BEGIN
      OPEN c_decl(p_decl_id);
      FETCH c_decl
        INTO r_decl;
      IF c_decl%NOTFOUND
      THEN
        RAISE_APPLICATION_ERROR(-20000
                               ,v_thisproc ||
                                ': p_decl_id niet gevonden: ' ||
                                TO_CHAR(p_decl_id));
      END IF;
      CLOSE c_decl;
    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE = -54
        THEN
          -- resource busy and acquire with NOWAIT specified
          RETURN FALSE;
        END IF;
        RAISE; -- iets anders? Niet OK
    END;
    v_dewy_id_BU   := kgc_uk2pk.dewy_id(p_kafd_id => r_decl.kafd_id
                                       ,p_code    => 'BU');
    v_dewy_id_BU_1 := kgc_uk2pk.dewy_id(p_kafd_id => r_decl.kafd_id
                                       ,p_code    => 'BU_1');
    v_dewy_id_BU_2 := kgc_uk2pk.dewy_id(p_kafd_id => r_decl.kafd_id
                                       ,p_code    => 'BU_2');
    v_dewy_id_BU_3 := kgc_uk2pk.dewy_id(p_kafd_id => r_decl.kafd_id
                                       ,p_code    => 'BU_3');
    v_dewy_id_BU_4 := kgc_uk2pk.dewy_id(p_kafd_id => r_decl.kafd_id
                                       ,p_code    => 'BU_4');
    -- indien decl-status = V + verwerken: niet OK
    IF r_decl.status = 'V'
    THEN
      IF p_check_only
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al verwerkt! GEEN melding!
      ELSIF p_aantal = 1
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al verwerkt! GEEN melding!
      END IF;
    ELSIF r_decl.status = 'R'
    THEN
      IF p_check_only
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al teruggedraaid! GEEN melding!
      ELSIF p_aantal = -1
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al teruggedraaid! GEEN melding!
      END IF;
    END IF;

    -- verrichting - nu alleen nog maar ONDE/ONBE/METI/GESP
    IF r_decl.entiteit = 'ONDE'
    THEN
      v_onde_id := r_decl.ID;
    ELSIF r_decl.entiteit = 'ONBE'
    THEN
      OPEN c_onbe(r_decl.ID);
      FETCH c_onbe
        INTO r_onbe;
      IF c_onbe%NOTFOUND
      THEN
        CLOSE c_onbe;
        RAISE_APPLICATION_ERROR(-20000
                               ,v_thisproc ||
                                ': decl.ONBE_ID niet gevonden: ' ||
                                TO_CHAR(r_decl.ID));
      END IF;
      CLOSE c_onbe;
      v_onde_id := r_onbe.onde_id;
    ELSIF r_decl.entiteit = 'METI'
    THEN
      OPEN c_meti(r_decl.ID);
      FETCH c_meti
        INTO r_meti;
      IF c_meti%NOTFOUND
      THEN
        CLOSE c_meti;
        RAISE_APPLICATION_ERROR(-20000
                               ,v_thisproc ||
                                ': decl.METI_ID niet gevonden: ' ||
                                TO_CHAR(r_decl.ID));
      END IF;
      CLOSE c_meti;
      v_onde_id := r_meti.onde_id;
    ELSIF r_decl.entiteit = 'GESP'
    THEN
      OPEN c_gesp(r_decl.ID);
      FETCH c_gesp
        INTO r_gesp;
      IF c_gesp%NOTFOUND
      THEN
        CLOSE c_gesp;
        RAISE_APPLICATION_ERROR(-20000
                               ,v_thisproc ||
                                ': decl.GESP_ID niet gevonden: ' ||
                                TO_CHAR(r_decl.ID));
      END IF;
      CLOSE c_gesp;
      v_onde_id := r_gesp.onde_id;
    ELSE
      RAISE_APPLICATION_ERROR(-20000
                             ,v_thisproc ||
                              ': decl.entiteit wordt niet ondersteund: ' ||
                              r_decl.entiteit);
    END IF;
    -- onderzoek ophalen
    OPEN c_onde(v_onde_id);
    FETCH c_onde
      INTO r_onde;
    IF c_onde%NOTFOUND
    THEN
      CLOSE c_onde;
      RAISE_APPLICATION_ERROR(-20000
                             ,v_thisproc || ': onde_id niet gevonden: ' ||
                              TO_CHAR(v_onde_id));
    END IF;
    CLOSE c_onde;
    -- (oorspronkelijke)aanvrager ophalen
    OPEN c_rela(NVL(r_onde.rela_id_oorsprong
                   ,r_onde.rela_id));
    FETCH c_rela
      INTO r_rela;
    CLOSE c_rela;
    -- persoon ophalen
    -- Mantis 1268: Bij alternatief persoon deze gebruiken i.p.v. de persoon waarop de declaratie betekking heeft
    IF r_decl.pers_id_alt_decl IS NULL
    THEN
      v_pers_trow.pers_id := r_decl.pers_id;
    ELSE
      v_pers_trow.pers_id := r_decl.pers_id_alt_decl;
    END IF;

    Cg$kgc_Personen.slct(v_pers_trow); -- notfound is al afgevangen door kgc_zis
    v_pers_is_dummy := (v_pers_trow.zisnr IS NULL);
    IF v_pers_trow.rela_id IS NOT NULL
    THEN
      v_rela_trow.rela_id := v_pers_trow.rela_id;
      Cg$kgc_Relaties.slct(v_rela_trow);
    END IF;
    IF v_pers_trow.verz_id IS NOT NULL
    THEN
      v_verz_trow.verz_id := v_pers_trow.verz_id;
      Cg$kgc_Verzekeraars.slct(v_verz_trow);
    END IF;

    -- ** generieke controles **
    -- decl declareren?
    IF r_decl.declareren <> 'J'
    THEN
      NULL; --    add2msg('Declaratie.Declareren staat op NEE');
    END IF;
    -- pers aanwezig?
    IF r_decl.pers_id IS NULL
    THEN
      add2msg('Declaratie.Persoon is afwezig!');
    ELSE
      -- check persoon
      NULL; -- afhankelijk van de instelling
    END IF;
    -- kafd aanwezig?
    IF r_decl.kafd_id IS NULL
    THEN
      add2msg('Declaratie.Afdeling is afwezig!');
    END IF;
    -- check aantal
    IF p_aantal IN (1, -1)
    THEN
      NULL; -- OK
    ELSE
      add2msg('Aantal declaraties moet 1 of -1 zijn (aangeboden: ' ||
              TO_CHAR(p_aantal) || ')!');
    END IF;
    -- check op status onderzoek
    IF r_onde.status = 'V'
    THEN
      add2msg('Dit onderzoek is vervallen');
    ELSE
      NULL; -- OK
    END IF;

    IF (v_msg IS NOT NULL)
    THEN
      -- nu al fout? niet verder gaan!
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Controle NIET OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    END IF;

    -- ** specifieke controles en samenstellen msg **
    -- algemene controles
    OPEN c_sypa(v_controle_sypa);
    FETCH c_sypa
      INTO r_sypa;
    IF c_sypa%NOTFOUND
    THEN
      CLOSE c_sypa;
      RAISE_APPLICATION_ERROR(-20000
                             ,v_thisproc || ': systeemparameter ' ||
                              v_controle_sypa || ' werd niet gevonden!');
    END IF;
    CLOSE c_sypa;
    DBMS_OUTPUT.PUT_LINE('1; ' || r_sypa.standaard_waarde);
    v_tab_controles(1) := r_sypa.standaard_waarde;
    -- afdelingspecifieke controles
    OPEN c_spwa(r_sypa.sypa_id
               ,r_decl.kafd_id
               ,NULL);
    FETCH c_spwa
      INTO r_spwa;
    IF c_spwa%NOTFOUND
    THEN
      v_tab_controles(2) := '';
    ELSE
      v_tab_controles(2) := r_spwa.waarde;
    END IF;
    CLOSE c_spwa;
    DBMS_OUTPUT.PUT_LINE('2; ' || r_spwa.waarde);
    -- onderzoeksgroepspecifieke controles
    OPEN c_spwa(r_sypa.sypa_id
               ,r_decl.kafd_id
               ,r_decl.ongr_id);
    FETCH c_spwa
      INTO r_spwa;
    IF c_spwa%NOTFOUND
    THEN
      v_tab_controles(3) := '';
    ELSE
      v_tab_controles(3) := r_spwa.waarde;
    END IF;
    CLOSE c_spwa;
    DBMS_OUTPUT.PUT_LINE('3; ' || r_spwa.waarde);

    --*** A1 Van welke entiteiten mogen er declaraties verzonden worden?
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A1 is afwezig!');
    ELSIF INSTR('-' || v_ccode_parm_s || '-'
               ,'-' || r_decl.entiteit || '-') > 0
    THEN
      NULL; -- OK!
    ELSE
      add2msg('A1-entiteit ' || r_decl.entiteit ||
              ' mag NIET gedeclareerd worden!');
    END IF;

    v_spec_verdatum := r_decl.datum;
    V_VerDat_verl   := r_decl.datum;
    IF (V_VerDat_verl IS NULL)
    THEN
      add2msg('Declaratie.Verrichtingdatum ontbreekt!');
    END IF;

    /* VERWIJDERD DOOR R KONINGS NOVEMBER 2009. ONDERSTAANDE CODE IS VERWIJDERD IN HET KADER VAN DE GEWENSTE
       FUNCTIONALITEIT MET BETREKKING OP HET DECLARATIE PROCES. DEZE WENSEN ZIJN GESPECIFICEERD DOOR DE
       DE AFDELING KLINISCHE GENETICA EN GELDEN VOOR ALLE UNITS. DEZE STUK CODE IS VERWIJDERD, OMDAT DEZE
       CODE VOORKWAM DAT DECLARATIE RECORDS MET EEN BEPAALDE DATUM VERSTUURD WERDEN. DE NIEUWE WENS IS OM
       DEZE RECORDS TOCH TE VERSTUREN MAAR DAN VOORZIEN VAN DE CODE NB.

    ELSE
      --*** A2 Aantal dagen dat de verrichtingdatum in het verleden mag liggen (-1 betekent geen controle)
      v_ccode_aanwezig := bepaal_controle
                          ( p_controles => v_tab_controles
                          , p_ccode => 'A2'
                          , x_ccode_parm_s => v_ccode_parm_s
                          , x_ccode_parm_n => v_ccode_parm_n
                          );
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - A2 is afwezig!');
      ELSIF v_ccode_parm_n = -1
      THEN
        NULL; -- geen controle
      ELSE
        IF SYSDATE - V_VerDat_verl <= v_ccode_parm_n
        THEN
          NULL; -- OK
        ELSE
          add2msg('A2-Declaratie.verrichtingdatum ('|| TO_CHAR(V_VerDat_verl, 'dd-mm-yyyy') ||') ligt meer dan ' || NVL(TO_CHAR(v_ccode_parm_n),'<LEEG!>') || ' dagen in het verleden!');
        END IF;
      END IF;
    END IF;

    */

    -- **** AZM specifiek 1
    v_spec_systeemdeel := kgc_attr_00.waarde('KGC_KGC_AFDELINGEN'
                                            ,'SYSTEEMDEEL'
                                            ,r_decl.kafd_id);
    IF (v_spec_systeemdeel IS NULL)
    THEN
      add2msg('Declaratie.systeemdeel afdeling ontbreekt ! (extra attribuut SYSTEEMDEEL bij tabel KGC_KGC_AFDELINGEN)');
    END IF;

    -- **** AZM specifiek 2
    v_spec_prod_afdeling := kgc_attr_00.waarde('KGC_ONDERZOEKSGROEPEN'
                                              ,'PRODUCERENDE_AFDELING'
                                              ,r_decl.ongr_id);
    IF (v_spec_prod_afdeling IS NULL)
    THEN
      add2msg('Declaratie.Producerende afdeling ontbreekt!(extra attribuut PRODUCERENDE_AFDELING bij tabel KGC_ONDERZOEKSGROEPEN)');
    END IF;

    -- **** AZM specifiek 5 en 7!!
    v_spec_aanvr_afdeling := NULL;
    v_bron_aanvr_afdeling := NULL;
    v_spec_aanvr_spec     := NULL;
    v_bron_aanvr_spec     := NULL;
    IF r_decl.kafd_code = 'METAB'
    THEN
      IF r_decl.dewy_code = 'WDS'
      THEN
        IF r_rela.inst_code LIKE 'AZM%'
        THEN
          -- is de aanvrager intern?
          v_bron_aanvr_afdeling := 'Aanvrager.afdeling.TOREN_AANVR_AFD';
          v_spec_aanvr_afdeling := kgc_attr_00.waarde('KGC_AFDELINGEN'
                                                     ,'TOREN_AANVR_AFD'
                                                     ,r_rela.afdg_id);
          v_bron_aanvr_spec     := 'Aanvrager.TOREN_AANVR_SPEC';
          v_spec_aanvr_spec     := kgc_attr_00.waarde('KGC_RELATIES'
                                                     ,'TOREN_AANVR_SPEC'
                                                     ,r_rela.rela_id);
        ELSIF r_rela.relatie_type = 'HA'
        THEN
          -- huisarts
          v_bron_aanvr_afdeling := 'Aanvrager.TOREN_AANVR_AFD';
          v_spec_aanvr_afdeling := kgc_attr_00.waarde('KGC_RELATIES'
                                                     ,'TOREN_AANVR_AFD'
                                                     ,r_rela.rela_id);
          v_bron_aanvr_spec     := 'Aanvrager.TOREN_AANVR_SPEC';
          v_spec_aanvr_spec     := kgc_attr_00.waarde('KGC_RELATIES'
                                                     ,'TOREN_AANVR_SPEC'
                                                     ,r_rela.rela_id);
        ELSE
          -- extern? via instelling!
          -- geen bron nodig: wordt altijd gevuld
          v_spec_aanvr_afdeling := kgc_attr_00.waarde('KGC_INSTELLINGEN'
                                                     ,'TOREN_AANVR_AFD'
                                                     ,r_rela.inst_id);
          IF v_spec_aanvr_afdeling IS NULL
          THEN
            v_spec_aanvr_afdeling := 'DIV';
          END IF;
          v_spec_aanvr_spec := v_spec_aanvr_afdeling;
        END IF;
      ELSE
        -- niet-WDS:
        v_spec_aanvr_afdeling := 'KGMB';
        v_spec_aanvr_spec     := 'GEN'; -- Changed By R Konings (MUMC) NAAR CODE GEN OP LAST VAN AFD FINANCIEN
      END IF;
    ELSIF r_decl.kafd_code = 'DNA'
    THEN
      -- voor DNA: nog rekening houden met 4 belgische aanvragers? Ja, dit gaat echter via declaratiewijze buitenland!
      IF v_dewy_id_BU IS NULL
      THEN
        add2msg('De declaratiewijze BU kon niet gevonden worden - voeg deze toe in scherm KGCDEWY01!');
      ELSIF r_onde.dewy_id IN (v_dewy_id_BU, v_dewy_id_BU_1, v_dewy_id_BU_2,
             v_dewy_id_BU_3, v_dewy_id_BU_4)
      THEN
        v_spec_aanvr_afdeling := 'DIV';
        v_spec_aanvr_spec     := 'DIV'; -- identiek
      ELSE
        v_spec_aanvr_afdeling := 'KGDN';
        v_spec_aanvr_spec     := 'GEN';
      END IF;
    ELSIF r_decl.kafd_code = 'CYTO'
    THEN
      IF v_dewy_id_BU IS NULL
      THEN
        add2msg('De declaratiewijze BU kon niet gevonden worden - voeg deze toe in scherm KGCDEWY01!');
      ELSIF r_onde.dewy_id = v_dewy_id_BU
      THEN
        v_spec_aanvr_afdeling := 'DIV';
        v_spec_aanvr_spec     := 'DIV'; -- identiek
      ELSE
        v_spec_aanvr_afdeling := v_spec_prod_afdeling;
        v_spec_aanvr_spec     := 'GEN'; -- Changed By Rkonings (MUMC)
      END IF;
    ELSE
      NULL;
    END IF;

    IF v_spec_aanvr_afdeling IS NULL
    THEN
      add2msg('Declaratie.Aanvragende afdeling ontbreekt (bron: ' ||
              v_bron_aanvr_afdeling || ')!');
      --  ELSE -- testing
      --    add2msg('Declaratie.Aanvragende afdeling: ' || v_spec_aanvr_afdeling || ' (bron: ' || v_bron_aanvr_afdeling || ')!');
    END IF;
    IF v_spec_aanvr_spec IS NULL
    THEN
      add2msg('Declaratie.Aanvragend specialisme ontbreekt (bron: ' ||
              v_bron_aanvr_spec || ')!');
      --  ELSE -- testing
      --    add2msg('Declaratie.Aanvragende specialisme: ' || v_spec_aanvr_spec || ' (bron: ' || v_bron_aanvr_spec || ')!');
    END IF;

    -- **** AZM specifiek 6
    v_spec_prod_spec := 'GEN'; -- Changed By R Konings (MUMC)
    IF (v_spec_prod_spec IS NULL)
    THEN
      add2msg('Declaratie.Producerend specialisme ontbreekt!');
    END IF;

    -- **** AZM specifiek 8
    IF (r_decl.declareren = 'N')
    THEN
      v_spec_abw := 'NB';
    ELSE
      v_spec_abw := 'P';
    END IF;

    /*------------------------------------------------------------------------------------------------------
    -----------------------------     CODE NB         ------------------------------------------------------
    --------------------------------------------------------------------------------------------------------
         START CREATIE CODE TEN BEHOEVE VAN CODE NB OP 20 JULI 2009.
         INITIELE PROGRAMMATUUR DOOR R KONINGS, APPLICATIEN SPECIALIST ITS CENTRUM ACADEMISCH
         ZIEKENHUIS MAASTRICHT. CODE HERHAALDELIJK AANGEPAST DOOR R KONINGS, LAATSTE AANPASSING
         OP 13-11-2009.
         OP 23-11-2009 WORDT DE CODE VOLLEDIG HERZIEN DOOR R KONINGS, OP BASIS VAN DE AANGEGEVEN WENSEN
         DOOR DE FUNCTIONEEL BEHEERDER EN RICHTLIJNEN VAN DE AFDELINGEN FINANCIEN EN KG, VOLDOEN DE
         VORIGE VERSIES VAN DE PROGRAMMATUUR BETREFFENDE CODE NB NIET MEER AAN DE FUNCTIONELE EISEN.
         ZIE DOCUMENTATIE VOOR DE FUNCTIONELE BESCHRIJVING EN UITWERKING
    --------------------------------------------------------------------------------------------------------

    --------------------------------------------------------------------------------------------------------
        CASE STATEMENT VOOR CONTROLE FOETUS, INDIEN IN DE TABEL KGC_FOETUSSEN HET VELD
        NIET GEBOREN OP J STAAT OF IN GEVAL VAN DNA HET MATERIAAL FOETAAL MATERIAAL IS (CODE FM)
        INDIEN FM OF NIET GEBOREN DAN WORDT VARIABLE V_FOET_OVL OP JA GEZET, INDIEN V_FOET_OVL = J
        DAN NIET BEREKENEN
    ---------------------------------------------------------------------------------------------------------
    */

    CASE r_decl.entiteit
      WHEN 'ONDE' THEN
        V_FOET_OVL := FoetusDeceased_Onde(r_onde.onde_id);
      WHEN 'METI' THEN
        V_FOET_OVL := FoetusDeceased_Meti(r_meti.meti_id);
      WHEN 'ONBE' THEN
        BEGIN
          V_FOET_OVL := FoetusDeceased_Onbe(r_onbe.onbe_id);
          -- CHECK OF DE FOETUS OVERLEDEN IS INDIEN JA,
          -- DAN HOEFT VOLGENDE CHECK NIET UITGEVOERD TE WORDEN
          IF V_FOET_OVL = 'N'
          THEN
            -- CONTROLEER OP FOETAAL MATERIAAL, ALLEEN UNIT DNA.
            IF r_decl.kafd_id = 48
            THEN
              OPEN C_mate_x(r_onde.onde_id);
              FETCH c_mate_x
                INTO r_mate_x;
              IF C_mate_x%FOUND
              THEN
                -- IF GEVONDEN DAN IS DE FOETUS OVERLEDEN(NIET GEBOREN)
                V_FOET_OVL := 'J';
              END IF;
            END IF;
          END IF;
        END;
    END CASE;

    -- ALS DE OVERLIJDENSDATUM GELIJK IS AAN DE GEBOORTEDATUM DAN CODE NB MEEGEVEN
    IF (to_char(v_pers_trow.OVERLIJDENSDATUM
               ,'YYYYMMDD') = to_char(v_pers_trow.GEBOORTEDATUM
                                      ,'YYYYMMDD'))
    THEN

      V_SPEC_ABW := 'NB';
      UpdateCommentsDecl('C1'
                        ,r_decl.decl_id
                        ,R_DECL.VERWERKING); -- COMMENTAAR AAN DECLARATIE RECORD TOEVOEGEN

    ELSE
      -- ALS DE PATIENT MEER DAN 730 DAGEN IN HET VERLEDEN OVERLEDEN IS DAN CODE NB MEEGEVEN.
      IF ((trunc(v_pers_trow.overlijdensdatum) + 730) < trunc(SYSDATE))
      THEN

        V_SPEC_ABW := 'NB';
        UpdateCommentsDecl('C2'
                          ,r_decl.decl_id
                          ,R_DECL.VERWERKING);

      ELSE
        -- ALS DE VERRICHTING, HET ONDERZOEK MEER DAN 730 DAGEN GELEDEN UITGEVOERD IS DAN CODE NB.
        IF ((trunc(r_decl.datum) + 730) < trunc(SYSDATE))
        THEN

          V_SPEC_ABW := 'NB';
          UpdateCommentsDecl('C3'
                            ,r_decl.decl_id
                            ,R_DECL.VERWERKING);

        ELSE
          -- ALS DE FOETUS DOODGEBOREN/OVERLEDEN IS VOOR DE BEVALLING DAN CODE NB MEEGEVEN.
          IF V_FOET_OVL = 'J'
          THEN

            V_SPEC_ABW := 'NB';
            UpdateCommentsDecl('C4'
                              ,r_decl.decl_id
                              ,R_DECL.VERWERKING);

          ELSE
            -- INDIEN ER EEN FOUT OPDTREEDT TIJDENS HET BEPALEN VAN DE OVERLIJDENSSTATUS VAN DE
            -- FOETUS DAN MOET ER EEN FOUTMELDING GEGEVEN WORDEN
            IF V_FOET_OVL = 'E'
            THEN
              add2msg('Fout tijdens bepalen niet_geboren foetus, verwerking stopgezet');

            ELSE
              -- INDIEN AAN GEEN VAN BOVENSTAANDE CONDITIES VOLDAAN WORDT DAN DECLAREREN MET DE CODE PRODUCTIE
              V_SPEC_ABW := 'P';
            END IF;

          END IF;
        END IF;
      END IF;
    END IF;

    -- **** AZM specifiek 9 en 10
    v_spec_abi_verzcd := NULL;
    v_spec_abi_verznr := NULL;
    IF v_pers_is_dummy
    THEN
      v_spec_abi_verznr := SUBSTR(REPLACE(v_pers_trow.verzekeringsnr
                                         ,'.'
                                         ,'')
                                 ,1
                                 ,10);
    END IF;

    -- **** AZM specifiek 11
    v_spec_aanvrager := SUBSTR(r_onde.onderzoeknr || ' ' ||
                               r_rela.aanspreken || ' ' ||
                               r_rela.afdg_omschrijving
                              ,1
                              ,44);

    -- **** AZM specifiek 12
    v_spec_vercode := r_decl.verrichtingcode;
    IF (v_spec_vercode IS NULL)
    THEN
      add2msg('Declaratie.Verrichtingcode ontbreekt!');
    END IF;

    -- algemene controles
    --*** A3 Moet de verzekeraar van de declaratie (indien aanwezig) niet-vervallen zijn? (A3:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A3 is afwezig!');
    ELSIF v_ccode_parm_s = 'J'
    THEN
      IF v_verz_trow.verz_id IS NOT NULL
         AND v_verz_trow.vervallen = 'J'
      THEN
        add2msg('A3-Declaratie.persoon.verzekeraar (code=' ||
                v_verz_trow.code || ') is vervallen!');
      END IF;
    END IF;
    --*** A4 Moet er een indicatie en reden zijn bij het onderzoek (A4: N = Nee; B=Beide, I=Alleen indicatie, R=alleen reden)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A4 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      OPEN c_onin(v_onde_id);
      FETCH c_onin
        INTO r_onin;
      IF c_onin%NOTFOUND
      THEN
        add2msg('A4-Een indicatie/reden bij onderzoek ' ||
                r_onde.onderzoeknr ||
                ' is verplicht maar werd niet gevonden!');
      ELSIF r_onin.indi_id IS NULL
            AND UPPER(v_ccode_parm_s) IN ('B', 'I')
      THEN
        add2msg('A4-Een indicatie bij onderzoek ' || r_onde.onderzoeknr ||
                ' is verplicht maar deze is niet gevuld!');
      ELSIF r_onin.onre_id IS NULL
            AND UPPER(v_ccode_parm_s) IN ('B', 'R')
      THEN
        add2msg('A4-Een reden bij onderzoek ' || r_onde.onderzoeknr ||
                ' is verplicht maar is niet gevuld!');
      ELSIF (r_onin.indi_id IS NULL OR r_onin.onre_id IS NULL)
            AND UPPER(v_ccode_parm_s) IN ('B')
      THEN
        add2msg('A4-Een indicatie en reden bij onderzoek ' ||
                r_onde.onderzoeknr ||
                ' zijn beide verplicht maar zijn niet allebei gevuld!');
      END IF;
      CLOSE c_onin;
    END IF;

    --*** A5 Mag de declaratiepersoon overleden zijn? (A5:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A5'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A5 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'J'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_trow.overleden = 'J'
      THEN
        add2msg('A5-declaratiepersoon is overleden!');
      END IF;
    END IF;

    --*** A6 Moet de declaratie-persoon een ZISnummer hebben? (D1:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A6'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A6 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_is_dummy
      THEN
        add2msg('A6-declaratiepersoon moet een ZISnummer hebben!');
      END IF;
    END IF;

    --*** A7 Moet de declaratiepersoon beheerd-in-ZIS zijn? (A7:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A7'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A7 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF NOT v_pers_is_dummy
      THEN
        add2msg('A7-declaratiepersoon is niet beheerd in ZIS!');
      END IF;
    END IF;

    --*** A8 Moet de verrichtingcode bestaan als categorie van type VER_CODES? (A8:J = Ja; N = Nee)
    --    IF r_decl.kafd_code = 'MET' AND r_decl.entiteit = 'METI' THEN
    --      null; -- geen actie: deze verrichtingcodes komen uit stotestgroepen!
    -- dit moet generiek!
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A8'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A8 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF (v_spec_vercode IS NULL)
      THEN
        -- indien leeg, dan is er al een foutmelding gegeven!
        -- controleer of deze code genoemd wordt in categorie
        OPEN c_cate(r_decl.kafd_id
                   ,v_spec_vercode);
        FETCH c_cate
          INTO r_cate;
        IF c_cate%NOTFOUND
        THEN
          add2msg('Declaratie.Verrichtingcode ''' || v_spec_vercode ||
                  ''' bestaat niet als Helix-categorie VER_CODES bij de afdeling ''' ||
                  r_decl.kafd_code || '''!');
        END IF;
        CLOSE c_cate;
      END IF;
    END IF;

    --*** A9 Indien de overlijdensdatum gevuld is, moet dan de verrichtingdatum voor of op deze overlijdensdatum liggen? (A9:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A9'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A9 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_trow.overlijdensdatum IS NOT NULL
      THEN
        --*** Aanpassing indien de verrichtingsdatum na overlijdensdatum ligt dan moet de
        --*** verrichtingsdatum met de overlijdensdatum gevuld worden. Dit op last van Huub Heuts.

        IF TRUNC(v_spec_verdatum) > TRUNC(v_pers_trow.overlijdensdatum)
        THEN
          v_spec_verdatum := v_pers_trow.OVERLIJDENSDATUM;
        END IF;
      END IF;
    END IF;

    --*** B1 Is de functie 'Correctie' toegestaan? (B1:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'B1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - B1 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'J'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF p_aantal < 0
      THEN
        add2msg('B1-De functie ''Correctie'' is NIET toegestaan!');
      END IF;
    END IF;

    IF v_pers_is_dummy
    THEN
      --*** D1 Moet de declaratiepersoon zonder ZISnummer een geboortedatum hebben? (D1:J = Ja; N = Nee)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D1'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D1 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.geboortedatum IS NULL
        THEN
          add2msg('D1-Persoon(dummie).Geboortedatum ontbreekt!');
        END IF;
      END IF;

      --*** D2 Moet de declaratiepersoon zonder ZISnummer een geboortedatum hebben? (D1:J = Ja; N = Nee)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D2'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D2 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.aanspreken IS NULL
        THEN
          add2msg('D2-Persoon(dummie).Aanspreeknaam ontbreekt!');
        END IF;
      END IF;

      --*** D3 Moet de declaratiepersoon zonder ZISnummer een adres hebben? (D1:J = Ja; N = Nee)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D3'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D3 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.adres IS NULL
        THEN
          add2msg('D3-Persoon(dummie).Adres ontbreekt!');
        END IF;
      END IF;

      --*** D4 Wat is de maximale lengte van het adres van een declaratiepersoon zonder ZISnummer? (-1 betekent geen controle)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D4'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D4 is afwezig!');
      ELSIF v_ccode_parm_n = -1
      THEN
        NULL; -- geen controle
      ELSE
        IF LENGTH(v_pers_trow.adres) > v_ccode_parm_n
        THEN
          add2msg('D4-Persoon(dummie).Adres mag max ' ||
                  TO_CHAR(v_ccode_parm_n) || ' posities lang zijn!');
        END IF;
      END IF;

      --*** D5 Moet de declaratiepersoon zonder ZISnummer een postcode hebben? (D1:J = Ja; N = Nee)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D5'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D5 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.postcode IS NULL
        THEN
          add2msg('D5-Persoon(dummie).Postcode ontbreekt!');
        END IF;
      END IF;

      --*** D6 Moet de declaratiepersoon zonder ZISnummer een woonplaats hebben? (D1:J = Ja; N = Nee)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D6'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D6 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.woonplaats IS NULL
        THEN
          add2msg('D6-Persoon(dummie).Woonplaats ontbreekt!');
        END IF;
      END IF;

      --*** D7 Moet de declaratiepersoon zonder ZISnummer een land hebben? (D1:J = Ja; N = Nee)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D7'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D7 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.land IS NULL
        THEN
          add2msg('D7-Persoon(dummie).Land ontbreekt!');
        END IF;
      END IF;
    END IF; -- dummy persoon

    -- *** KLAAR met controleren
    IF (v_msg IS NOT NULL)
    THEN
      -- foutmelding?
      Kgc_Dft_Interface.LOG(r_decl.verwerking
                           ,'Controle NIET OK op ' ||
                            TO_CHAR(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    END IF;

    IF p_check_only
    THEN
      -- klaar
      Kgc_Dft_Interface.LOG(r_decl.verwerking
                           ,'Controle OK op ' ||
                            TO_CHAR(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || '!');
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN TRUE;
    END IF;

    -- bericht samenstellen
    -- **** MSH
    v_hl7_msg_bron('MSH') := Kgc_Zis.init_msg('MSH'
                                             ,Kgc_Zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('MSH')(1)(1)(1) := '|';
    v_hl7_msg_bron('MSH')(2)(1)(1) := '^~\&';
    v_hl7_msg_bron('MSH')(3)(1)(1) := v_spec_systeemdeel;
    v_hl7_msg_bron('MSH')(5)(1)(1) := 'HISCOM-TOREN';
    v_hl7_msg_bron('MSH')(7)(1)(1) := v_sysdate;
    v_hl7_msg_bron('MSH')(9)(1)(1) := 'DFT^P03';
    v_hl7_msg_bron('MSH')(10)(1)(1) := v_msg_control_id;
    v_hl7_msg_bron('MSH')(11)(1)(1) := 'P';
    v_hl7_msg_bron('MSH')(12)(1)(1) := Kgc_Zis.g_sypa_hl7_versie;
    v_hl7_msg_bron('MSH')(15)(1)(1) := 'AL';
    v_hl7_msg_bron('MSH')(16)(1)(1) := 'NE';

    -- **** EVN
    v_hl7_msg_bron('EVN') := Kgc_Zis.init_msg('EVN'
                                             ,Kgc_Zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('EVN')(1)(1)(1) := 'P03';
    v_hl7_msg_bron('EVN')(2)(1)(1) := v_sysdate;

    -- **** PID
    v_hl7_msg_bron('PID') := Kgc_Zis.init_msg('PID'
                                             ,Kgc_Zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('PID')(3)(1)(1) := v_pers_trow.zisnr; -- 'specifiek 3'
    --??  -- onderstaande regel: stond niet in oude specificatie, maar werd wel zo gedaan in actuele scripts!
    v_hl7_msg_bron('PID')(5)(1)(1) := SUBSTR(v_pers_trow.aanspreken
                                            ,1
                                            ,46); -- max 48 - 2 seps
    -- ruimte reserveren voor evt adres:
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 2
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 3
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 4
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 5
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF v_pers_is_dummy
    THEN
      v_hl7_msg_bron('PID')(7)(1)(1) := TO_CHAR(v_pers_trow.geboortedatum
                                               ,'yyyymmdd');
      -- *** adres
      v_hl7_msg_bron('PID')(11)(1)(1) := SUBSTR(v_pers_trow.adres
                                               ,1
                                               ,61);
      v_hl7_msg_bron('PID')(11)(1)(3) := SUBSTR(v_pers_trow.woonplaats
                                               ,1
                                               ,30);
      v_hl7_msg_bron('PID')(11)(1)(4) := SUBSTR(v_pers_trow.land
                                               ,1
                                               ,15);
      v_hl7_msg_bron('PID')(11)(1)(5) := SUBSTR(REPLACE(v_pers_trow.postcode
                                                       ,' '
                                                       ,'')
                                               ,1
                                               ,6);
    END IF;

    -- **** FT1
    v_hl7_msg_bron('FT1') := Kgc_Zis.init_msg('FT1'
                                             ,Kgc_Zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('FT1')(2)(1)(1) := TO_CHAR(r_decl.decl_id); -- declaratie-id
    v_hl7_msg_bron('FT1')(4)(1)(1) := TO_CHAR(v_spec_verdatum
                                             ,'yyyymmdd');
    v_hl7_msg_bron('FT1')(7)(1)(1) := v_spec_vercode;
    v_hl7_msg_bron('FT1')(13)(1)(1) := v_spec_prod_afdeling;
    v_hl7_msg_bron('FT1')(16)(1)(1) := v_spec_aanvr_afdeling; -- aanvragende afdeling
    v_hl7_msg_bron('FT1')(20)(1)(1) := v_spec_prod_spec; -- producerende specialisme

    -- **** ZFU
    v_hl7_msg_bron('ZFU') := Kgc_Zis.init_msg('ZFU'
                                             ,Kgc_Zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('ZFU')(11)(1)(1) := v_spec_aanvr_spec; -- aanvragende specialisme
    v_hl7_msg_bron('ZFU')(12)(1)(1) := v_spec_abw;
    v_hl7_msg_bron('ZFU')(14)(1)(1) := SUBSTR(v_spec_aanvrager
                                             ,1
                                             ,44);
    v_hl7_msg_bron('ZFU')(15)(1)(1) := LTRIM(TO_CHAR(p_aantal)); -- 1 of -1; is al gecontroleerd!

    -- **** IN1
    v_hl7_msg_bron('IN1') := Kgc_Zis.init_msg('IN1'
                                             ,Kgc_Zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('IN1')(3)(1)(1) := v_spec_abi_verzcd;
    v_hl7_msg_bron('IN1')(36)(1)(1) := v_spec_abi_verznr;

    v_strDFT := Kgc_Zis.g_sob_char ||
                Kgc_Zis.hl7seg2str('MSH'
                                  ,v_hl7_msg_bron('MSH')) || Kgc_Zis.g_cr ||
                Kgc_Zis.hl7seg2str('EVN'
                                  ,v_hl7_msg_bron('EVN')) || Kgc_Zis.g_cr ||
                Kgc_Zis.hl7seg2str('PID'
                                  ,v_hl7_msg_bron('PID')) || Kgc_Zis.g_cr ||
                Kgc_Zis.hl7seg2str('FT1'
                                  ,v_hl7_msg_bron('FT1')) || Kgc_Zis.g_cr ||
                Kgc_Zis.hl7seg2str('ZFU'
                                  ,v_hl7_msg_bron('ZFU')) || Kgc_Zis.g_cr ||
                Kgc_Zis.hl7seg2str('IN1'
                                  ,v_hl7_msg_bron('IN1')) || Kgc_Zis.g_cr ||
                Kgc_Zis.g_eob_char || Kgc_Zis.g_term_char;

    v_ok := Kgc_Dft_Interface.send_msg(p_strHL7in  => v_strDFT
                                      ,p_timeout   => 150 -- 15 seconden
                                      ,x_strHL7out => v_strAntw
                                      ,x_msg       => v_msg);
    IF NOT v_ok
    THEN
      Kgc_Dft_Interface.LOG(r_decl.verwerking
                           ,'Verwerking NIET OK op ' ||
                            TO_CHAR(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            CHR(10) || v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    END IF;
    -- T.b.v. van testsituatie een afwijkende afhandeling
    IF kgc_dft_interface.test_hl7
    THEN
      -- geen retourbericht
      -- verwerking OK - declaratie bijwerken!
      IF p_aantal > 0
      THEN
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Verwerkt op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ' door ' ||
                              kgc_mede_00.medewerker_code || '; Status:' ||
                              r_decl.status || '->V.');
        upd_decl(p_decl_id
                ,'V'
                ,trunc(SYSDATE)
                ,r_decl.verwerking);
      ELSE
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Teruggedraaid op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ' door ' ||
                              kgc_mede_00.medewerker_code || '; Status:' ||
                              r_decl.status || '->R.');
        upd_decl(p_decl_id
                ,'R'
                ,trunc(SYSDATE)
                ,r_decl.verwerking);
      END IF;
    ELSE
      Kgc_Zis.LOG(v_strAntw
                 ,'1100XXXXXX');
      v_hl7_msg_res := Kgc_Zis.str2hl7msg(0
                                         ,v_strAntw);
      v_MSA         := Kgc_Zis.getSegmentCode('MSA'
                                             ,v_hl7_msg_res);
      v_MSH         := Kgc_Zis.getSegmentCode('MSH'
                                             ,v_hl7_msg_res);

      IF (NVL(LENGTH(v_MSA)
             ,0) = 0 OR NVL(LENGTH(v_MSH)
                            ,0) = 0)
      THEN
        v_msg := 'MSA- of MSH-segment afwezig!';
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Verwerking NIET OK op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                              v_msg);
        upd_decl(p_decl_id
                ,r_decl.status
                ,r_decl.datum_verwerking
                ,r_decl.verwerking);
        RETURN FALSE;
      END IF;
      -- check op control-id; soms wordt er iets aan vastgeplakt (=niet erg)
      IF SUBSTR(v_hl7_msg_res(v_MSH) (10) (1) (1)
               ,1
               ,LENGTH(v_msg_control_id)) <> v_msg_control_id
      THEN
        -- komt 'nooit' voor
        v_msg := 'Fout bij ophalen gegevens: mismatch control-id! Verzonden: ' ||
                 v_msg_control_id || '; Ontvangen: ' ||
                 v_hl7_msg_res(v_MSH) (10) (1) (1) || '!';
        Kgc_Dft_Interface.LOG(r_decl.verwerking
                             ,'Verwerking NIET OK op ' ||
                              TO_CHAR(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                              CHR(10) || v_msg);
        upd_decl(p_decl_id
                ,r_decl.status
                ,r_decl.datum_verwerking
                ,r_decl.verwerking);
        RETURN FALSE;
      END IF;
      IF NVL(v_hl7_msg_res(v_MSA) (1) (1) (1)
            ,'XX') NOT IN ('AA'
                          ,'CA')
      THEN
        v_msg := 'MSA-1 = ' || v_hl7_msg_res(v_MSA) (1) (1)
                 (1) || '; Text: ' || v_hl7_msg_res(v_MSA) (3) (1) (1);
        Kgc_Dft_Interface.LOG(r_decl.verwerking
                             ,'Verwerking NIET OK op ' ||
                              TO_CHAR(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                              CHR(10) || v_msg);
        upd_decl(p_decl_id
                ,r_decl.status
                ,r_decl.datum_verwerking
                ,r_decl.verwerking);
        RETURN FALSE;
      ELSE
        -- verwerking OK - declaratie bijwerken!
        IF p_aantal > 0
        THEN
          kgc_dft_interface.log(r_decl.verwerking
                               ,'Verwerkt op ' ||
                                to_char(SYSDATE
                                       ,'dd-mm-yyyy hh24:mi:ss') ||
                                ' door ' || kgc_mede_00.medewerker_code ||
                                '; Status:' || r_decl.status || '->V.');
          upd_decl(p_decl_id
                  ,'V'
                  ,trunc(SYSDATE)
                  ,r_decl.verwerking);
        ELSE
          kgc_dft_interface.log(r_decl.verwerking
                               ,'Teruggedraaid op ' ||
                                to_char(SYSDATE
                                       ,'dd-mm-yyyy hh24:mi:ss') ||
                                ' door ' || kgc_mede_00.medewerker_code ||
                                '; Status:' || r_decl.status || '->R.');
          upd_decl(p_decl_id
                  ,'R'
                  ,trunc(SYSDATE)
                  ,r_decl.verwerking);
        END IF;
      END IF;
    END IF;

    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      v_msg := v_thisproc || ': Error: ' || SQLERRM;
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Error op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
  END check_verstuur_dft_p03;

  /* globaal ***************** */
  PROCEDURE open_verbinding IS
  BEGIN
    NULL;
  END open_verbinding;

  PROCEDURE sluit_verbinding IS
  BEGIN
    NULL;
  END sluit_verbinding;

  PROCEDURE controleer(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => 1
                                  ,p_check_only => TRUE);
  END controleer;

  PROCEDURE verwerk(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => 1
                                  ,p_check_only => FALSE);
  END verwerk;

  PROCEDURE draai_terug(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => -1
                                  ,p_check_only => FALSE);
  END draai_terug;

  PROCEDURE test_bepaal_controle IS
    v_tab_controles  t_tab_controles;
    v_ccode_aanwezig BOOLEAN;
    v_ccode_parm_s   VARCHAR2(4000);
    v_ccode_parm_n   NUMBER;
  BEGIN
    DBMS_OUTPUT.PUT_LINE('Start test_bepaal_controle...');
    DBMS_OUTPUT.PUT_LINE('Test aanwezig zonder parameter - begin, midden, eind, afwezig');
    v_tab_controles(1) := 'A1,A2,A3';

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test begin OK');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test begin FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test midden OK');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test midden FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test eind OK');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test eind FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test afwezig OK');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test afwezig FOUT');
    END IF;

    DBMS_OUTPUT.PUT_LINE('Test aanwezig met parameter - begin, midden1, midden2, eind, afwezig');
    v_tab_controles(1) := 'A1:-1,A2:TEST,A3:-2,A4:9999999';

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test begin OK');
      IF v_ccode_parm_n = -1
         AND v_ccode_parm_s = '-1'
      THEN
        DBMS_OUTPUT.PUT_LINE('... parameter OK');
      ELSE
        DBMS_OUTPUT.PUT_LINE('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || TO_CHAR(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test begin FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test midden1 OK');
      IF v_ccode_parm_n IS NULL
         AND v_ccode_parm_s = 'TEST'
      THEN
        DBMS_OUTPUT.PUT_LINE('... parameter OK');
      ELSE
        DBMS_OUTPUT.PUT_LINE('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || TO_CHAR(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test midden1 FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test midden2 OK');
      IF v_ccode_parm_n = -2
         AND v_ccode_parm_s = '-2'
      THEN
        DBMS_OUTPUT.PUT_LINE('... parameter OK');
      ELSE
        DBMS_OUTPUT.PUT_LINE('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || TO_CHAR(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test midden2 FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test eind OK');
      IF v_ccode_parm_n = 9999999
         AND v_ccode_parm_s = '9999999'
      THEN
        DBMS_OUTPUT.PUT_LINE('... parameter OK');
      ELSE
        DBMS_OUTPUT.PUT_LINE('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || TO_CHAR(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test eind FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A5'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test afwezig OK');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test afwezig FOUT');
    END IF;

    DBMS_OUTPUT.PUT_LINE('Test nesting - level3, level2, level1, afwezig');
    v_tab_controles(1) := 'A1:-1,A2:TEST1,A3:-2';
    v_tab_controles(2) := 'A1:-2,A2:TEST2';
    v_tab_controles(3) := 'A1:-3';

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test level3 OK');
      IF v_ccode_parm_n = -3
         AND v_ccode_parm_s = '-3'
      THEN
        DBMS_OUTPUT.PUT_LINE('... parameter OK');
      ELSE
        DBMS_OUTPUT.PUT_LINE('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || TO_CHAR(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test level3 FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test level2 OK');
      IF v_ccode_parm_n IS NULL
         AND v_ccode_parm_s = 'TEST2'
      THEN
        DBMS_OUTPUT.PUT_LINE('... parameter OK');
      ELSE
        DBMS_OUTPUT.PUT_LINE('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || TO_CHAR(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test level2 FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test level1 OK');
      IF v_ccode_parm_n = -2
         AND v_ccode_parm_s = '-2'
      THEN
        DBMS_OUTPUT.PUT_LINE('... parameter OK');
      ELSE
        DBMS_OUTPUT.PUT_LINE('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || TO_CHAR(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test level1 FOUT');
    END IF;

    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      DBMS_OUTPUT.PUT_LINE('Test afwezig OK');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Test afwezig FOUT');
    END IF;
    DBMS_OUTPUT.PUT_LINE('Eind test_bepaal_controle...');
  END test_bepaal_controle;

  /*---------------------------------------------------------------------------------------------------
    HAAL NIET_GEBOREN VELD INFORMATIE OP VANUIT TABEL KGC_FOETUSSEN, GEEF WAARDE TERUG
    AAN CHECK_VERSTUUR_DFT. R KONINGS JUNI 2009 / NOVEMBER 2009.
  ------------------------------------------------------------------------------------------------------*/

  -------------------------- METI -----------------------------------------------
  -------------------------------------------------------------------------------
  FUNCTION FoetusDeceased_Meti(PID INTEGER) RETURN VARCHAR2 IS
    /* HAAL NIET_GEBOREN GEGEVENS OP VANUIT TABEL KGC_FOETUSSEN OP BASIS VAN HET
      AANGELEVERDE METI_ID.
     JUNI 2009 RKONINGS.
    */

    CURSOR Foet_Onde(PID IN INTEGER) -- RETRIEVE VELD NIET_GEBOREN(J/N)
    IS
      SELECT Foet.NIET_GEBOREN
      FROM   Kgc_foetussen Foet
      WHERE  Foet.Foet_Id IN
             (SELECT Onde.Foet_ID
              FROM   kgc_onderzoeken Onde
              WHERE  Onde.Onde_id IN
                     (SELECT Meti.Onde_id
                      FROM   bas_metingen Meti
                      WHERE  Meti.Meti_id = PID));

    OVL    VARCHAR2(1);
    OVLCHK BOOLEAN; -- CONTROLE OF OVL_CHECK UITGEVOERDKAN WORDEN.

  BEGIN
    -- HAAL GEGEVENS OP VANUIT KGC_FOETUSSEN(NIET GEBOREN JA/NEE).
    OPEN Foet_Onde(PID);
    FETCH Foet_onde
      INTO OVL;
    OVLCHK := Foet_onde%FOUND;
    CLOSE Foet_Onde;

    -- CHECK OF VELD LEEG IS, ZO JA DAN GEEF EEN FOUTMELDING TERUG
    -- ANDERS GEEF DE INHOUD VAN HET VELD TERUG.
    IF OVLCHK
    THEN
      IF TRIM(OVL) IS NOT NULL
      THEN
        RETURN(OVL);
      ELSE
        RETURN('E');
      END IF;
    ELSE
      RETURN('N');

    END IF;
    -- INDIEN ER EEN ANDERE EXCEPTION OPTREEDT DAN GEEF CODE E(ERROR) TERUG.
  EXCEPTION
    WHEN OTHERS THEN
      RETURN('E');

  END;

  ------------------ ONDE ------------------------------------
  ------------------------------------------------------------

  FUNCTION FoetusDeceased_Onde(PID INTEGER) RETURN VARCHAR2 IS
    /* HAAL NIET_GEBOREN GEGEVENS OP VANUIT TABEL KGC_FOETUSSEN OP BASIS VAN HET
       AANGELEVERDE ONDE_ID.
       JUNI 2009 RKONINGS.
    */

    CURSOR Foet_Onde(PID IN INTEGER) -- RETRIEVE VELD NIET_GEBOREN(J/N)
    IS
      SELECT Foet.NIET_GEBOREN
      FROM   kgc_foetussen Foet
      WHERE  Foet.Foet_Id IN (SELECT Onde.Foet_ID
                              FROM   kgc_onderzoeken Onde
                              WHERE  Onde.Onde_id = PID);

    OVL    VARCHAR2(1);
    OVLCHK BOOLEAN;

  BEGIN
    -- HAAL GEGEVENS OP VANUIT KGC_FOETUSSEN(NIET GEBOREN JA/NEE).
    OPEN Foet_Onde(PID);
    FETCH Foet_onde
      INTO OVL;
    OVLCHK := Foet_onde%FOUND;
    CLOSE Foet_Onde;

    -- CHECK OF VELD LEEG IS, ZO JA DAN GEEF EEN FOUTMELDING TERUG
    -- ANDERS GEEF DE INHOUD VAN HET VELD TERUG.
    IF OVLCHK
    THEN
      IF TRIM(OVL) IS NOT NULL
      THEN
        RETURN(OVL);
      ELSE
        RETURN('E');
      END IF;
    ELSE
      RETURN('N');

    END IF;
    -- INDIEN ER EEN ANDERE EXCEPTION OPTREEDT DAN GEEF CODE E(ERROR) TERUG.
  EXCEPTION
    WHEN OTHERS THEN
      RETURN('E');

  END;

  ----------------- ONBE ------------------------------------------------
  -----------------------------------------------------------------------
  FUNCTION FoetusDeceased_Onbe(PID INTEGER) RETURN VARCHAR2 IS

    /* HAAL NIET_GEBOREN GEGEVENS OP VANUIT TABEL KGC_FOETUSSEN OP BASIS VAN HET
        AANGELEVERDE ONBE_ID.
        NOVEMBER 2009 RKONINGS.
    */

    CURSOR Foet_Onde(PID IN INTEGER) -- RETRIEVE VELD NIET_GEBOREN(J/N)
    IS
      SELECT Foet.NIET_GEBOREN
      FROM   Kgc_foetussen Foet
      WHERE  Foet.Foet_Id IN
             (SELECT Onde.Foet_ID
              FROM   Kgc_onderzoeken Onde
              WHERE  Onde.Onde_id IN
                     (SELECT Onbe.onde_id
                      FROM   Kgc_onderzoek_betrokkenen Onbe
                      WHERE  Onbe.Onbe_Id = PID));
    OVL    VARCHAR2(1);
    OVLCHK BOOLEAN;

  BEGIN
    -- HAAL GEGEVENS OP VANUIT KGC_FOETUSSEN(NIET GEBOREN JA/NEE).
    OPEN Foet_Onde(PID);
    FETCH Foet_onde
      INTO OVL; -- Haal gegevens foetus op
    OVLCHK := Foet_onde%FOUND;
    CLOSE Foet_Onde;

    -- CHECK OF VELD LEEG IS, ZO JA DAN GEEF EEN FOUTMELDING TERUG
    -- ANDERS GEEF DE INHOUD VAN HET VELD TERUG.
    IF OVLCHK
    THEN
      IF TRIM(OVL) IS NOT NULL
      THEN
        RETURN(OVL);
      ELSE
        RETURN('E');
      END IF;
    ELSE
      RETURN('N');

    END IF;
    -- INDIEN ER EEN ANDERE EXCEPTION OPTREEDT DAN GEEF CODE E(ERROR) TERUG.
  EXCEPTION
    WHEN OTHERS THEN
      RETURN('E');
      -- EINDE functie
  END;

  PROCEDURE UpdateCommentsDecl(P_RAP_CODE     IN VARCHAR2
                              ,PDECL_ID       IN INTEGER
                              ,PVERWERKINGTXT IN VARCHAR2) AS
    V_VERWERKINGS_TEXT VARCHAR2(4000);
    /* COMMENTAAR TOEVOEGEN AAN KGC_DECLARATIES RECORD, TEN BEHOEVE VAN CONTROLE DOOR
       DE KEY_USERS. BIJ IEDERE NIEUWE VERWERKING WORDT HET VERWERKINGSVELD LEEG GEMAAKT.
       ER KUNNEN BINNEN 1 RECORD MEERDERE OORZAKEN ZIJN VOOR DE CODE NB, ER WORDT ALLEEN UITGEGAAN
       VAN DE EERSTE OPTIE TIJDENS HET STUREN EN VOOR DE LOGGING.
       GEZET WORDEN. DOEL IS OM AAN DE HAND VAN DE FOUTCODE DE JUISTE INFORMATIE TE VERWERKEN IN
       HET VERWERKINGSVELD VAN HET RECORD
       C1 : OVERLEDEN BABY.
       C2 : OVERLEDEN DATUM > 730 DAGEN IN VERLEDEN.
       C3 : VERRICHTING DATUM > 730 DAGEN IN VERLEDEN.
       C4 : FOETUS NIET GEBOREN.

    */

  BEGIN
    V_VERWERKINGS_TEXT := '';
    -- VUL VERWERKINGSCOMMENTAAR
    CASE P_RAP_CODE
      WHEN 'C1' THEN
        V_VERWERKINGS_TEXT := V_VERWERKINGS_TEXT ||
                              'CODE NB, BABY DOODGEBOREN.';
      WHEN 'C2' THEN
        V_VERWERKINGS_TEXT := V_VERWERKINGS_TEXT ||
                              'CODE NB, MEER DAN 730 DAGEN GELEDEN OVERLEDEN.';
      WHEN 'C3' THEN
        V_VERWERKINGS_TEXT := V_VERWERKINGS_TEXT ||
                              'CODE NB, VERRICHTINGSDATUM OUDER DAN 730 DAGEN.';
      WHEN 'C4' THEN
        V_VERWERKINGS_TEXT := V_VERWERKINGS_TEXT ||
                              'CODE NB, NIET_GEBOREN FOETUS.';

    END CASE;

    -- update kgc_declaratie record met de bevindingen.
    UPDATE Kgc_Declaraties decl
    SET    decl.verwerking = V_VERWERKINGS_TEXT
    WHERE  decl.decl_id = pdecl_id;

    COMMIT;

  END;
END kgc_dft_maastricht;
/

/
QUIT
