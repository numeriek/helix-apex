CREATE OR REPLACE PACKAGE "HELIX"."KGC_DFT_UTRECHT" IS
 /******************************************************************************
   NAME:       kgc_dft_utrecht
   PURPOSE:    controleren van declaratiegegevens en overzetten naar financieel systeem

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-07-2006  op locatie
***************************************************************************** */

PROCEDURE controleer
( p_decl_id IN NUMBER
, x_ok out boolean
);

PROCEDURE verwerk
( p_decl_id IN NUMBER
, x_ok out boolean
);

PROCEDURE draai_terug
( p_decl_id IN NUMBER
, x_ok out boolean
);

PROCEDURE test_bepaal_controle;

PROCEDURE verstuur_dft_bericht
( p_spec_systeemdeel VARCHAR2
, p_spec_patnr VARCHAR2
, p_pers_trow    cg$KGC_PERSONEN.cg$row_type
, p_pers_is_dummy BOOLEAN
, p_decl_id NUMBER
, p_spec_verdatum DATE
, p_spec_vercode VARCHAR2
, p_spec_kafd_code VARCHAR2
, p_spec_aanvr_afdeling VARCHAR2
, p_spec_prod_spec VARCHAR2
, p_spec_aanvr_spec VARCHAR2
, p_spec_abw VARCHAR2
, p_spec_tekst VARCHAR2
, p_aantal NUMBER
, p_spec_abi_verzcd VARCHAR2
, p_spec_abi_verznr VARCHAR2
, x_debug IN OUT  VARCHAR2
, x_ok  IN OUT  BOOLEAN
, x_msg IN OUT  VARCHAR2
);
END KGC_DFT_UTRECHT;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DFT_UTRECHT" IS
  /******************************************************************************
     NAME:       kgc_dft_utrecht
     PURPOSE:    controleren van declaratiegegevens en overzetten naar financieel systeem

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
                30-08-2010  GWE              Mantis 1268: Alternatief persoon voor declaratie
     1.0        04-07-2006  op locatie
  ***************************************************************************** */

  TYPE t_tab_controles IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;

  -- lokaal
  FUNCTION bepaal_controle -- zie testfunctie test_bepaal_controle
  (p_controles    IN t_tab_controles
  ,p_ccode        IN VARCHAR2
  ,x_ccode_parm_s OUT VARCHAR2
  ,x_ccode_parm_n OUT NUMBER) RETURN BOOLEAN IS
    v_pos           NUMBER;
    v_pos_parm      NUMBER;
    v_code_aanwezig BOOLEAN := FALSE;
  BEGIN
    --dbms_output.put_line('KGC_DFT_utrecht.bepaal_controle: 2: '||p_ccode || ';#c=' || to_char(p_controles.COUNT));
    x_ccode_parm_s := NULL;
    x_ccode_parm_n := NULL;
    IF p_controles.COUNT = 0
    THEN
      dbms_output.put_line('Geen controles aanwezig!');
      RETURN FALSE;
    END IF;
    FOR i IN REVERSE p_controles.FIRST .. p_controles.LAST
    LOOP
      -- begin bij laagste nivo
      v_code_aanwezig := TRUE;
      -- aantal toegestane plekken
      v_pos := INSTR(p_controles(i)
                    ,',' || p_ccode || ':');
      IF v_pos > 0
      THEN
        -- code gevonden met parm, bv als 2e code of verder
        v_pos_parm := v_pos + 2 + LENGTH(p_ccode);
      ELSE
        -- verder zoeken
        v_pos := INSTR(p_controles(i)
                      ,',' || p_ccode || ',');
        IF v_pos > 0
        THEN
          v_pos_parm := 0; -- wel code, geen parm, bv als 2e code of verder
        ELSE
          -- verder zoeken
          IF p_controles(i) LIKE p_ccode || ':%'
          THEN
            -- OK, aan begin
            v_pos_parm := 2 + LENGTH(p_ccode);
          ELSE
            -- verder zoeken
            IF p_controles(i) LIKE p_ccode || ',%'
            THEN
              -- OK, aan begin
              v_pos_parm := 0; -- geen parm
            ELSE
              -- verder zoeken
              IF p_controles(i) LIKE '%,' || p_ccode
              THEN
                -- OK, aan eind
                v_pos_parm := 0; -- geen parm
              ELSE
                v_code_aanwezig := FALSE;
              END IF;
            END IF;
          END IF;
        END IF;
      END IF;
      IF v_code_aanwezig
      THEN
        -- code gevonden? evt parm ophalen en klaar
        IF v_pos_parm > 0
        THEN
          v_pos := INSTR(p_controles(i)
                        ,','
                        ,v_pos_parm);
          IF v_pos > 0
          THEN
            -- er is een volgende controlecode
            x_ccode_parm_s := SUBSTR(p_controles(i)
                                    ,v_pos_parm
                                    ,v_pos - v_pos_parm);
          ELSE
            -- tot einde
            x_ccode_parm_s := SUBSTR(p_controles(i)
                                    ,v_pos_parm);
          END IF;
          IF NVL(LENGTH(x_ccode_parm_s)
                ,0) > 0
          THEN
            -- probeer numeriek
            BEGIN
              x_ccode_parm_n := TO_NUMBER(x_ccode_parm_s);
            EXCEPTION
              WHEN OTHERS THEN
                NULL;
            END;
          END IF;
        END IF;
        RETURN TRUE;
      END IF;
    END LOOP;
    --dbms_output.put_line('KGC_DFT_utrecht.bepaal_controle: Found! parm: '||x_ccode_parm_s);
    RETURN FALSE;
  END;

  FUNCTION check_verstuur_dft_p03(p_decl_id    IN NUMBER
                                 ,p_aantal     IN NUMBER
                                 ,p_check_only IN BOOLEAN) RETURN BOOLEAN IS
    CURSOR c_decl(p_decl_id NUMBER) IS
      SELECT *
      FROM   kgc_declaraties decl
      WHERE  decl.decl_id = p_decl_id
      FOR    UPDATE NOWAIT;
    r_decl c_decl%ROWTYPE;
    CURSOR c_rela(p_rela_id NUMBER) IS
      SELECT rela.*
            ,afdg.omschrijving afdg_omschrijving
      FROM   kgc_relaties   rela
            ,kgc_afdelingen afdg
      WHERE  rela.rela_id = p_rela_id
      AND    rela.afdg_id = afdg.afdg_id(+);
    r_rela c_rela%ROWTYPE;
    CURSOR c_onde(p_onde_id NUMBER) IS
      SELECT onde.*
            ,ongr.code ongr_code
      FROM   kgc_onderzoeken       onde
            ,kgc_onderzoeksgroepen ongr
      WHERE  onde.onde_id = p_onde_id
      AND    onde.ongr_id = ongr.ongr_id;
    r_onde c_onde%ROWTYPE;
    CURSOR c_onbe(p_onbe_id NUMBER) IS
      SELECT *
      FROM   kgc_onderzoek_betrokkenen onbe
      WHERE  onbe.onbe_id = p_onbe_id;
    r_onbe c_onbe%ROWTYPE;
    CURSOR c_meti(p_meti_id NUMBER) IS
      SELECT meti.*
            ,stgr.code stgr_code
      FROM   bas_metingen        meti
            ,kgc_stoftestgroepen stgr
      WHERE  meti.meti_id = p_meti_id
      AND    meti.stgr_id = stgr.stgr_id(+);
    r_meti c_meti%ROWTYPE;
    CURSOR c_gesp(p_gesp_id NUMBER) IS
      SELECT gesp.*
            ,ongr.code ongr_code
      FROM   kgc_gesprekken        gesp
            ,kgc_onderzoeksgroepen ongr
      WHERE  gesp.gesp_id = p_gesp_id
      AND    gesp.ongr_id = ongr.ongr_id;
    r_gesp c_gesp%ROWTYPE;
    CURSOR c_cate(p_kafd_id NUMBER, p_verrichtingcode VARCHAR2) IS
      SELECT NULL
      FROM   kgc_categorie_types   caty
            ,kgc_categorieen       cate
            ,kgc_categorie_waarden cawa
      WHERE  caty.code = 'VER_CODES'
      AND    cate.kafd_id = p_kafd_id
      AND    cawa.waarde = p_verrichtingcode
      AND    caty.caty_id = cate.caty_id
      AND    cate.cate_id = cawa.cate_id;
    r_cate c_cate%ROWTYPE;
    CURSOR c_onin(p_onde_id NUMBER) IS
      SELECT ingr_id
            ,indi_id
            ,onre_id
      FROM   kgc_onderzoek_indicaties onin
      WHERE  onin.onde_id = p_onde_id;
    r_onin c_onin%ROWTYPE;
    CURSOR c_kafd(p_kafd_id NUMBER) IS
      SELECT code
      FROM   kgc_kgc_afdelingen kafd
      WHERE  kafd.kafd_id = p_kafd_id;
    CURSOR c_sypa(p_code VARCHAR2) IS
      SELECT sypa_id
            ,standaard_waarde
      FROM   kgc_systeem_parameters
      WHERE  code = UPPER(p_code);
    r_sypa c_sypa%ROWTYPE;
    CURSOR c_spwa(p_sypa_id IN NUMBER, p_kafd_id IN NUMBER, p_ongr_id IN NUMBER) IS
      SELECT waarde
      FROM   kgc_systeem_par_waarden spwa
      WHERE  spwa.sypa_id = p_sypa_id
      AND    spwa.kafd_id = p_kafd_id
      AND    (spwa.ongr_id = p_ongr_id OR
            (p_ongr_id IS NULL AND spwa.ongr_id IS NULL));
    r_spwa c_spwa%ROWTYPE;

    v_pers_trow cg$KGC_PERSONEN.cg$row_type;
    v_rela_trow cg$KGC_RELATIES.cg$row_type;
    v_verz_trow cg$KGC_VERZEKERAARS.cg$row_type;
    v_onde_id   NUMBER;
    -- speciale gegevens gebruikt in het bericht:
    v_spec_systeemdeel    VARCHAR2(8); -- UMCU specifiek 1
    v_spec_kafd_code      VARCHAR2(4); -- UMCU specifiek 2
    v_spec_patnr          VARCHAR2(20); -- UMCU specifiek 3
    v_spec_verdatum       DATE; -- UMCU specifiek 4
    v_spec_aanvr_afdeling VARCHAR2(4); -- UMCU specifiek 5
    v_spec_prod_spec      VARCHAR2(4); -- UMCU specifiek 6
    v_spec_aanvr_spec     VARCHAR2(4); -- UMCU specifiek 7
    v_spec_abw            VARCHAR2(2); -- UMCU specifiek 8
    v_spec_abi_verzcd     VARCHAR2(10); -- UMCU specifiek 9
    v_spec_abi_verznr     VARCHAR2(10); -- UMCU specifiek 10
    v_spec_tekst          VARCHAR2(44); -- UMCU specifiek 11
    v_spec_vercode        VARCHAR2(20); -- UMCU specifiek 12
    v_pers_is_dummy       BOOLEAN;
    v_verdat_dagen        NUMBER;
    v_deen_row            cg$KGC_DECL_ENTITEITEN.cg$row_type;
    v_atwa_waarde         KGC_ATTRIBUUT_WAARDEN.WAARDE%TYPE;
    v_decl_kafd_code      KGC_KGC_AFDELINGEN.CODE%TYPE;
    v_temp                VARCHAR2(2000);
    v_tab_controles       t_tab_controles;
    v_controle_sypa       VARCHAR2(100) := 'ZIS_DFT_CONTROLES';
    v_ccode_aanwezig      BOOLEAN;
    v_ccode_parm_s        VARCHAR2(4000);
    v_ccode_parm_n        NUMBER;
    v_ok                  BOOLEAN;
    v_msg                 VARCHAR2(32767);
    v_sep                 VARCHAR2(2);
    v_nl                  VARCHAR2(2) := CHR(10);
    v_debug               VARCHAR2(100);
    v_thisproc            VARCHAR2(100) := 'kgc_dft_utrecht.check_verstuur_dft_p03';
    PROCEDURE add2msg(p_tekst VARCHAR2) IS
    BEGIN
      v_msg := v_msg || v_sep || p_tekst;
      v_sep := v_nl;
    END;
    PROCEDURE upd_decl(p_decl_id          NUMBER
                      ,p_status           VARCHAR2
                      ,p_datum_verwerking DATE
                      ,p_verwerking       VARCHAR2) IS
    BEGIN
      UPDATE kgc_declaraties
      SET    verwerking       = p_verwerking
            ,status           = p_status
            ,datum_verwerking = p_datum_verwerking
      WHERE  decl_id = p_decl_id;
      COMMIT;
    END;
  BEGIN
    v_debug                   := '1';
    kgc_zis.g_sypa_hl7_versie := kgc_sypa_00.systeem_waarde('HL7_VERSIE');

    -- ** generieke controles **
    v_debug := '2';
    BEGIN
      OPEN c_decl(p_decl_id);
      FETCH c_decl
        INTO r_decl;
      IF c_decl%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': p_decl_id niet gevonden: ' ||
                                to_char(p_decl_id));
      END IF;
      CLOSE c_decl;
    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE = -54
        THEN
          -- resource busy and acquire with NOWAIT specified
          RETURN FALSE;
        END IF;
        RAISE; -- iets anders? Niet OK
    END;
    -- indien decl-status = V + verwerken: niet OK
    IF r_decl.status = 'V'
    THEN
      IF p_check_only
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al verwerkt! GEEN melding!
      ELSIF p_aantal = 1
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al verwerkt! GEEN melding!
      END IF;
    ELSIF r_decl.status = 'R'
    THEN
      IF p_check_only
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al teruggedraaid! GEEN melding!
      ELSIF p_aantal = -1
      THEN
        ROLLBACK;
        RETURN FALSE; -- Zinloos, is al teruggedraaid! GEEN melding!
      END IF;
    END IF;

    OPEN c_kafd(r_decl.kafd_id);
    FETCH c_kafd
      INTO v_decl_kafd_code;
    CLOSE c_kafd;

    -- verrichting - nu alleen nog maar ONDE/ONBE/METI/GESP
    v_debug := '3';
    IF r_decl.entiteit = 'ONDE'
    THEN
      v_debug   := v_debug || 'a';
      v_onde_id := r_decl.id;
    ELSIF r_decl.entiteit = 'ONBE'
    THEN
      v_debug := v_debug || 'b';
      OPEN c_onbe(r_decl.id);
      FETCH c_onbe
        INTO r_onbe;
      IF c_onbe%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': decl.ONBE_ID niet gevonden: ' ||
                                to_char(r_decl.id));
      END IF;
      CLOSE c_onbe;
      v_onde_id := r_onbe.onde_id;
    ELSIF r_decl.entiteit = 'METI'
    THEN
      v_debug := v_debug || 'c';
      OPEN c_meti(r_decl.id);
      FETCH c_meti
        INTO r_meti;
      IF c_meti%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': decl.METI_ID niet gevonden: ' ||
                                to_char(r_decl.id));
      END IF;
      CLOSE c_meti;
      v_onde_id := r_meti.onde_id;
    ELSIF r_decl.entiteit = 'GESP'
    THEN
      v_debug := v_debug || 'd';
      OPEN c_gesp(r_decl.id);
      FETCH c_gesp
        INTO r_gesp;
      IF c_gesp%NOTFOUND
      THEN
        raise_application_error(-20000
                               ,v_thisproc ||
                                ': decl.GESP_ID niet gevonden: ' ||
                                to_char(r_decl.id));
      END IF;
      CLOSE c_gesp;
      v_onde_id := r_gesp.onde_id;
    ELSE
      v_debug := v_debug || 'e';
      raise_application_error(-20000
                             ,v_thisproc ||
                              ': decl.entiteit wordt niet ondersteund: ' ||
                              r_decl.entiteit);
    END IF;
    -- onderzoek ophalen
    v_debug := '3a';
    OPEN c_onde(v_onde_id);
    FETCH c_onde
      INTO r_onde;
    IF c_onde%NOTFOUND
    THEN
      raise_application_error(-20000
                             ,v_thisproc || ': onde_id niet gevonden: ' ||
                              to_char(v_onde_id));
    END IF;
    CLOSE c_onde;
    -- aanvrager ophalen
    v_debug := '3b';
    OPEN c_rela(nvl(r_onde.rela_id_oorsprong
                   ,r_onde.rela_id));
    FETCH c_rela
      INTO r_rela;
    CLOSE c_rela;
    -- persoon ophalen
    v_debug := '3c';
    -- Mantis 1268: Bij alternatief persoon deze gebruiken i.p.v. de persoon waarop de declaratie betekking heeft
    IF r_decl.pers_id_alt_decl IS NULL
    THEN
      v_pers_trow.pers_id := r_decl.pers_id;
    ELSE
      v_pers_trow.pers_id := r_decl.pers_id_alt_decl;
    END IF;
    --  v_pers_trow.pers_id := r_decl.pers_id;
    cg$kgc_personen.slct(v_pers_trow); -- notfound is al afgevangen door kgc_zis
    v_pers_is_dummy := (v_pers_trow.zisnr IS NULL);
    IF v_pers_trow.rela_id IS NOT NULL
    THEN
      v_rela_trow.rela_id := v_pers_trow.rela_id;
      cg$kgc_relaties.slct(v_rela_trow);
    END IF;
    IF v_pers_trow.verz_id IS NOT NULL
    THEN
      v_verz_trow.verz_id := v_pers_trow.verz_id;
      cg$kgc_verzekeraars.slct(v_verz_trow);
    END IF;

    -- ** generieke controles **
    -- decl declareren?
    IF r_decl.declareren <> 'J'
    THEN
      add2msg('Declaratie.declareren staat op NEE');
    END IF;
    -- pers aanwezig?
    IF r_decl.pers_id IS NULL
    THEN
      add2msg('Declaratie.persoon is afwezig!');
    ELSE
      -- check persoon
      NULL; -- afhankelijk van de instelling
    END IF;
    -- kafd aanwezig?
    IF r_decl.kafd_id IS NULL
    THEN
      add2msg('Declaratie.Afdeling is afwezig!');
    END IF;
    -- check aantal
    IF p_aantal IN (1, -1)
    THEN
      NULL; -- OK
    ELSE
      add2msg('Aantal declaraties moet 1 of -1 zijn (aangeboden: ' ||
              to_char(p_aantal) || ')!');
    END IF;

    IF NVL(LENGTH(v_msg)
          ,0) > 0
    THEN
      -- nu al fout? niet verder gaan!
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Controle NIET OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    END IF;

    -- ** specifieke controles en samenstellen msg **
    -- algemene controles
    OPEN c_sypa(v_controle_sypa);
    FETCH c_sypa
      INTO r_sypa;
    IF c_sypa%NOTFOUND
    THEN
      raise_application_error(-20000
                             ,v_thisproc || ': systeemparameter ' ||
                              v_controle_sypa || ' werd niet gevonden!');
    END IF;
    CLOSE c_sypa;
    dbms_output.put_line('1; ' || r_sypa.standaard_waarde);
    v_tab_controles(1) := r_sypa.standaard_waarde;
    -- afdelingspecifieke controles
    OPEN c_spwa(r_sypa.sypa_id
               ,r_decl.kafd_id
               ,NULL);
    FETCH c_spwa
      INTO r_spwa;
    IF c_spwa%NOTFOUND
    THEN
      v_tab_controles(2) := '';
    ELSE
      v_tab_controles(2) := r_spwa.waarde;
    END IF;
    CLOSE c_spwa;
    dbms_output.put_line('2; ' || r_spwa.waarde);
    -- onderzoeksgroepspecifieke controles
    OPEN c_spwa(r_sypa.sypa_id
               ,r_decl.kafd_id
               ,r_decl.ongr_id);
    FETCH c_spwa
      INTO r_spwa;
    IF c_spwa%NOTFOUND
    THEN
      v_tab_controles(3) := '';
    ELSE
      v_tab_controles(3) := r_spwa.waarde;
    END IF;
    CLOSE c_spwa;
    dbms_output.put_line('3; ' || r_spwa.waarde);

    --*** A1 Van welke entiteiten mogen er declaraties verzonden worden?
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A1 is afwezig!');
    ELSIF INSTR('-' || v_ccode_parm_s || '-'
               ,'-' || r_decl.entiteit || '-') > 0
    THEN
      NULL; -- OK!
    ELSE
      add2msg('A1-entiteit ' || r_decl.entiteit ||
              ' mag NIET gedeclareerd worden!');
    END IF;

    -- **** UMCU specifiek
    -- er staat ook een datum in de declaratie, maar deze is niet flexibel genoeg:
    -- bv utrecht wil datum_autorisatie, nijmegen datum_binnen; dus: hier regelen (per centrum)
    v_spec_verdatum := r_onde.datum_autorisatie;
    IF v_spec_verdatum IS NULL
    THEN
      add2msg('Declaratie.verrichtingdatum ontbreekt!');
    ELSE
      --*** A2 Aantal dagen dat de verrichtingdatum in het verleden mag liggen (-1 betekent geen controle)
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'A2'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - A2 is afwezig!');
      ELSIF v_ccode_parm_n = -1
      THEN
        NULL; -- geen controle
      ELSE
        IF SYSDATE - v_spec_verdatum <= v_ccode_parm_n
        THEN
          NULL; -- OK
        ELSE
          add2msg('A2-Declaratie.verrichtingdatum (' ||
                  to_char(v_spec_verdatum
                         ,'dd-mm-yyyy') || ') ligt meer dan ' ||
                  NVL(to_char(v_ccode_parm_n)
                     ,'<LEEG!>') || ' dagen in het verleden!');
        END IF;
      END IF;
      -- tijdelijke extra check (totdat kgcdelc05 een update van deze datum aankan):
      IF TRUNC(v_spec_verdatum) <> TRUNC(r_decl.datum)
      THEN
        add2msg('Tijdelijk-Declaratie.datum (' ||
                to_char(r_decl.datum
                       ,'dd-mm-yyyy') ||
                ') is niet gelijk aan de verrichtingdatum (' ||
                to_char(v_spec_verdatum
                       ,'dd-mm-yyyy') ||
                ')! Draai eerst SQL-programma ''Zet Declaratie.datum goed''...');
      END IF;
    END IF;

    -- **** UMCU specifiek
    v_debug          := '8';
    v_spec_kafd_code := kgc_attr_00.waarde('KGC_KGC_AFDELINGEN'
                                          ,'PROD_CODE'
                                          ,r_decl.kafd_id);
    IF NVL(LENGTH(v_spec_kafd_code)
          ,0) = 0
    THEN
      add2msg('Declaratie.Afdeling-producerende afdeling ontbreekt!');
    END IF;
    v_debug := '9';
    IF v_pers_is_dummy
    THEN
      v_spec_aanvr_afdeling := NULL;
      IF v_decl_kafd_code = 'MET'
         AND r_decl.entiteit = 'METI'
         AND r_onde.rela_id IS NOT NULL
      THEN
        v_spec_aanvr_afdeling := kgc_attr_00.waarde('KGC_INSTELLINGEN'
                                                   ,'ACCODE'
                                                   ,r_rela.inst_id);
      END IF;
      IF NVL(LENGTH(v_spec_aanvr_afdeling)
            ,0) = 0
      THEN
        -- overig
        v_spec_aanvr_afdeling := 'ARTS';
      END IF;
    ELSE
      v_spec_aanvr_afdeling := v_spec_kafd_code;
    END IF;
    IF NVL(LENGTH(v_spec_aanvr_afdeling)
          ,0) = 0
    THEN
      add2msg('Declaratie.aanvragende afdeling ontbreekt!');
    END IF;
    -- **** UMCU specifiek
    v_debug := '10';
    IF v_decl_kafd_code = 'MET'
    THEN
      v_spec_prod_spec := 'LAB';
    ELSE
      v_spec_prod_spec := 'KLG';
    END IF;
    IF NVL(LENGTH(v_spec_prod_spec)
          ,0) = 0
    THEN
      add2msg('Declaratie.producerend specialisme ontbreekt!');
    END IF;
    -- **** UMCU specifiek
    v_debug := '11';
    IF v_pers_is_dummy
    THEN
      v_spec_aanvr_spec := 'EXT';
    ELSE
      v_spec_aanvr_spec := v_spec_prod_spec;
    END IF;
    IF NVL(LENGTH(v_spec_aanvr_spec)
          ,0) = 0
    THEN
      add2msg('Declaratie.aanvragend specialisme ontbreekt!');
    END IF;
    -- **** UMCU specifiek 8
    v_debug    := '12';
    v_spec_abw := NULL;
    IF v_pers_is_dummy
    THEN
      v_spec_abw := 'ZF';
    END IF;
    -- **** UMCU specifiek 9
    v_debug           := '13';
    v_spec_abi_verzcd := NULL;
    IF v_pers_is_dummy
    THEN
      v_spec_abi_verzcd := v_verz_trow.code;
    END IF;
    -- **** UMCU specifiek
    v_debug           := '14';
    v_spec_abi_verznr := NULL;
    IF v_pers_is_dummy
    THEN
      v_spec_abi_verznr := SUBSTR(REPLACE(v_pers_trow.verzekeringsnr
                                         ,'.'
                                         ,'')
                                 ,1
                                 ,10);
    END IF;
    -- **** UMCU specifiek
    --mantis 1268
    IF r_decl.pers_id_alt_decl IS NULL
    THEN
      v_spec_tekst := substr(r_onde.onderzoeknr || ' ' || r_rela.aanspreken || ' ' ||
                             r_rela.afdg_omschrijving
                            ,1
                            ,44);
    ELSE
      v_spec_tekst := substr(r_onde.onderzoeknr || ' aan ' ||
                             kgc_pers_00.persoon_info(p_pers_id => r_decl.pers_id)
                            ,1
                            ,44);
    END IF;
    -- **** UMCU specifiek ?
    v_debug := '17';

    v_spec_vercode := r_decl.verrichtingcode;
    -- tijdelijke extra check:
    IF p_aantal = 1 -- aanpassing rde 07-09-2010: alleen controleren indien de declaratie nog verzonden moet worden
    THEN
      IF v_decl_kafd_code = 'DNA'
         OR (v_decl_kafd_code = 'MET' AND r_decl.entiteit = 'ONDE')
      THEN
        -- nog niet voor KG
        v_deen_row.deen_id := kgc_decl_00.deen_id(p_entiteit => r_decl.entiteit
                                                 ,p_id       => r_decl.id);
        IF v_deen_row.deen_id IS NULL
        THEN
          IF (v_decl_kafd_code = 'DNA' AND r_decl.entiteit = 'ONBE')
             OR v_decl_kafd_code = 'MET'
          THEN
            NULL; -- geen actie: de bepaling van onbe gaat niet altijd goed (door 'tel'?)
          ELSE
            add2msg('Deen-id niet gevonden!');
          END IF;
        ELSE
          cg$KGC_DECL_ENTITEITEN.slct(v_deen_row);
          IF nvl(v_deen_row.verrichtingcode
                ,'@#@') <> nvl(v_spec_vercode
                              ,'@#@')
          THEN
            add2msg('Declaratie.Verrichtingcode (' || v_spec_vercode ||
                    ') <> deen.code (' || v_deen_row.verrichtingcode || ')!');
          END IF;
        END IF;
      END IF;
    END IF;
    IF NVL(LENGTH(v_spec_vercode)
          ,0) = 0
    THEN
      add2msg('Declaratie.Verrichtingcode ontbreekt!');
    END IF;

    -- **** UMCU specifiek
    v_debug := '18';
    IF v_decl_kafd_code = 'MET'
    THEN
      -- verschil DMG en Metabool!!!
      v_spec_systeemdeel := 'OR_METAB';
    ELSE
      v_spec_systeemdeel := 'OR_DMG';
    END IF;
    -- **** UMCU specifiek
    v_debug := '19';
    IF v_pers_is_dummy
    THEN
      v_spec_patnr := '0000000';
    ELSE
      v_spec_patnr := REPLACE(v_pers_trow.zisnr
                             ,'.'
                             ,'');
    END IF;

    -- algemene controles
    --*** A3 Moet de verzekeraar van de declaratie (indien aanwezig) niet-vervallen zijn? (A3:J = Ja; N = Nee)
    v_debug          := 'A3';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A3 is afwezig!');
    ELSIF v_ccode_parm_s = 'J'
    THEN
      IF v_verz_trow.verz_id IS NOT NULL
         AND v_verz_trow.vervallen = 'J'
      THEN
        add2msg('A3-Declaratie.persoon.verzekeraar (code=' ||
                v_verz_trow.code || ') is vervallen!');
      END IF;
    END IF;
    --*** A4 Moet er een indicatie en reden zijn bij het onderzoek (A4: N = Nee; B=Beide, I=Alleen indicatie, R=alleen reden)
    v_debug          := 'A4';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A4 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      OPEN c_onin(v_onde_id);
      FETCH c_onin
        INTO r_onin;
      IF c_onin%NOTFOUND
      THEN
        add2msg('A4-Een indicatie/reden bij onderzoek ' ||
                r_onde.onderzoeknr ||
                ' is verplicht maar werd niet gevonden!');
      ELSIF r_onin.indi_id IS NULL
            AND UPPER(v_ccode_parm_s) IN ('B', 'I')
      THEN
        add2msg('A4-Een indicatie bij onderzoek ' || r_onde.onderzoeknr ||
                ' is verplicht maar deze is niet gevuld!');
      ELSIF r_onin.onre_id IS NULL
            AND UPPER(v_ccode_parm_s) IN ('B', 'R')
      THEN
        add2msg('A4-Een reden bij onderzoek ' || r_onde.onderzoeknr ||
                ' is verplicht maar is niet gevuld!');
      ELSIF (r_onin.indi_id IS NULL OR r_onin.onre_id IS NULL)
            AND UPPER(v_ccode_parm_s) IN ('B')
      THEN
        add2msg('A4-Een indicatie en reden bij onderzoek ' ||
                r_onde.onderzoeknr ||
                ' zijn beide verplicht maar zijn niet allebei gevuld!');
      END IF;
      CLOSE c_onin;
    END IF;

    --*** A5 Mag de declaratiepersoon overleden zijn? (A5:J = Ja; N = Nee)
    v_debug          := 'A5';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A5'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A5 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'J'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_trow.overleden = 'J'
      THEN
        add2msg('A5-declaratiepersoon is overleden!');
      END IF;
    END IF;

    v_debug := 'A6';
    --*** A6 Moet de declaratie-persoon een ZISnummer hebben? (D1:J = Ja; N = Nee)
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A6'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A6 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_is_dummy
      THEN
        add2msg('A6-declaratiepersoon moet een ZISnummer hebben!');
      END IF;
    END IF;

    --*** A7 Moet de declaratiepersoon beheerd-in-ZIS zijn? (A7:J = Ja; N = Nee)
    v_debug          := 'A7';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A7'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A7 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF NVL(v_pers_trow.beheer_in_zis
            ,'N') <> 'J'
      THEN
        add2msg('A7-declaratiepersoon is niet beheerd in ZIS!');
      END IF;
    END IF;

    --*** A8 Moet de verrichtingcode bestaan als categorie van type VER_CODES? (A8:J = Ja; N = Nee)
    -- zie ook verderop voor 2 Utrecht-specifieke controles ivm METAB
    v_debug          := 'A8';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A8'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A8 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF NVL(LENGTH(v_spec_vercode)
            ,0) > 0
      THEN
        -- indien leeg, dan is er al een foutmelding gegeven!
        -- controleer of deze code genoemd wordt in categorie
        OPEN c_cate(r_decl.kafd_id
                   ,v_spec_vercode);
        FETCH c_cate
          INTO r_cate;
        IF c_cate%NOTFOUND
        THEN
          add2msg('A8-Declaratie.Verrichtingcode ''' || v_spec_vercode ||
                  ''' bestaat niet als Helix-categorie VER_CODES bij de afdeling ''' ||
                  v_decl_kafd_code || '''!');
        END IF;
        CLOSE c_cate;
      END IF;
    END IF;

    --*** A9 Indien de overlijdensdatum gevuld is, moet dan de verrichtingdatum voor of op deze overlijdensdatum liggen? (A9:J = Ja; N = Nee)
    v_debug          := 'A9';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A9'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - A9 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'N'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF v_pers_trow.overlijdensdatum IS NOT NULL
      THEN
        IF TRUNC(v_spec_verdatum) > TRUNC(v_pers_trow.overlijdensdatum)
        THEN
          add2msg('A9-Verrichtingdatum (' ||
                  to_char(v_spec_verdatum
                         ,'dd-mm-yyyy') ||
                  ' ligt na de overlijdensdatum (' ||
                  to_char(v_pers_trow.overlijdensdatum
                         ,'dd-mm-yyyy') || ')!');
        END IF;
      END IF;
    END IF;

    --*** B1 Is de functie 'Correctie' toegestaan? (B1:J = Ja; N = Nee)
    v_debug          := 'B1';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'B1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - B1 is afwezig!');
    ELSIF UPPER(v_ccode_parm_s) = 'J'
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF p_aantal < 0
      THEN
        add2msg('B1-De functie ''Correctie'' is NIET toegestaan!');
      END IF;
    END IF;

    IF v_pers_is_dummy
    THEN
      --*** D1 Moet de declaratiepersoon zonder ZISnummer een geboortedatum hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D1';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D1'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D1 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.geboortedatum IS NULL
        THEN
          add2msg('D1-Persoon(dummie).Geboortedatum ontbreekt!');
        END IF;
      END IF;
      --*** D2 Moet de declaratiepersoon zonder ZISnummer een geboortedatum hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D2';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D2'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D2 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.aanspreken IS NULL
        THEN
          add2msg('D2-Persoon(dummie).Aanspreeknaam ontbreekt!');
        END IF;
      END IF;
      --*** D3 Moet de declaratiepersoon zonder ZISnummer een adres hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D3';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D3'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D3 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.adres IS NULL
        THEN
          add2msg('D3-Persoon(dummie).Adres ontbreekt!');
        END IF;
      END IF;
      --*** XX tijdelijk: het adres mag geen CR/FL tekens bevatten!
      IF INSTR(v_pers_trow.adres
              ,CHR(10)) > 0
         OR INSTR(v_pers_trow.adres
                 ,CHR(13)) > 0
      THEN
        add2msg('XX-Tijdelijk:Persoon(dummie).Adres bestaat uit meerdere regels!');
      END IF;

      --*** D4 Wat is de maximale lengte van het adres van een declaratiepersoon zonder ZISnummer? (-1 betekent geen controle)
      v_debug          := 'D4';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D4'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D4 is afwezig!');
      ELSIF v_ccode_parm_n = -1
      THEN
        NULL; -- geen controle
      ELSE
        IF LENGTH(v_pers_trow.adres) > v_ccode_parm_n
        THEN
          add2msg('D4-Persoon(dummie).Adres mag max ' ||
                  to_char(v_ccode_parm_n) || ' posities lang zijn!');
        END IF;
      END IF;
      --*** D5 Moet de declaratiepersoon zonder ZISnummer een postcode hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D5';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D5'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D5 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.postcode IS NULL
        THEN
          add2msg('D5-Persoon(dummie).Postcode ontbreekt!');
        END IF;
      END IF;
      --*** D6 Moet de declaratiepersoon zonder ZISnummer een woonplaats hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D6';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D6'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D6 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.woonplaats IS NULL
        THEN
          add2msg('D6-Persoon(dummie).Woonplaats ontbreekt!');
        END IF;
      END IF;
      --*** D7 Moet de declaratiepersoon zonder ZISnummer een land hebben? (D1:J = Ja; N = Nee)
      v_debug          := 'D7';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D7'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D7 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        IF v_pers_trow.land IS NULL
        THEN
          add2msg('D7-Persoon(dummie).Land ontbreekt!');
        END IF;
      END IF;
      --*** D8 Moet de declaratie-persoon zonder ZISnummer voorzien zijn van een extra attribuut 'COV_DUMMY_CHECK', gevuld met 'OK'? (D8:J = Ja; N = Nee)
      v_debug          := 'D8';
      v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                         ,p_ccode        => 'D8'
                                         ,x_ccode_parm_s => v_ccode_parm_s
                                         ,x_ccode_parm_n => v_ccode_parm_n);
      IF NOT v_ccode_aanwezig
      THEN
        add2msg(v_controle_sypa || ' - D8 is afwezig!');
      ELSIF UPPER(v_ccode_parm_s) = 'N'
      THEN
        NULL; -- OK!
      ELSE
        -- controleer
        v_atwa_waarde := kgc_attr_00.waarde('KGC_PERSONEN'
                                           ,'COV_DUMMY_CHECK'
                                           ,r_decl.pers_id);
        IF NVL(UPPER(v_atwa_waarde)
              ,'@#@') <> 'OK'
        THEN
          add2msg('D8-Persoon.extra-attribuut COV_DUMMY_CHECK is ongelijk aan OK!');
        END IF;
      END IF;
    END IF;

    --*** Utrecht-specifieke controles ivm METAB
    --*** U1 Voor welke entiteiten moet de verrichtingcode bestaan als categorie van type VER_CODES? (U1:)
    -- zie ook controle A8: dit is vervanger1 ervan!
    v_debug          := 'U1';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'U1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - U1 is afwezig!');
    ELSIF INSTR('-' || v_ccode_parm_s || '-'
               ,'-' || r_decl.entiteit || '-') = 0
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF NVL(LENGTH(v_spec_vercode)
            ,0) > 0
      THEN
        -- indien leeg, dan is er al een foutmelding gegeven!
        -- controleer of deze code genoemd wordt in categorie
        OPEN c_cate(r_decl.kafd_id
                   ,v_spec_vercode);
        FETCH c_cate
          INTO r_cate;
        IF c_cate%NOTFOUND
        THEN
          add2msg('U1-Declaratie.Verrichtingcode ''' || v_spec_vercode ||
                  ''' bestaat niet als Helix-categorie VER_CODES bij de afdeling ''' ||
                  v_decl_kafd_code || '''!');
        END IF;
        CLOSE c_cate;
      END IF;
    END IF;

    --*** U2 Voor welke entiteiten moet de verrichtingcode bestaan als extra attribuut VERRICHTINGCODE bij KGC_STOFTESTGROEPEN? (U2:)
    -- zie ook controle A8: dit is vervanger2 ervan!
    v_debug          := 'U2';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'U2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      add2msg(v_controle_sypa || ' - U2 is afwezig!');
    ELSIF INSTR('-' || v_ccode_parm_s || '-'
               ,'-' || r_decl.entiteit || '-') = 0
    THEN
      NULL; -- OK!
    ELSE
      -- controleer
      IF NVL(LENGTH(v_spec_vercode)
            ,0) > 0
      THEN
        -- indien leeg, dan is er al een foutmelding gegeven!
        -- controleer of deze code genoemd wordt in categorie
        v_atwa_waarde := kgc_attr_00.waarde('KGC_STOFTESTGROEPEN'
                                           ,'VERRICHTINGCODE'
                                           ,r_meti.stgr_id); -- geen andere keuze!
        IF v_spec_vercode <> NVL(v_atwa_waarde
                                ,'@#@')
        THEN
          add2msg('U2-Declaratie.Verrichtingcode ''' || v_spec_vercode ||
                  ''' is ongelijk aan extra attribuut VERRICHTINGCODE bij KGC_STOFTESTGROEPEN met code ''' ||
                  r_meti.stgr_code || '''!');
        END IF;
      END IF;
    END IF;

    -- *** KLAAR met controleren
    IF NVL(LENGTH(v_msg)
          ,0) > 0
    THEN
      -- foutmelding?
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Controle NIET OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    END IF;

    IF p_check_only
    THEN
      -- klaar
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Controle OK op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || '!');
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN TRUE;
    END IF;
    -- onderstaande procedure is ook gemaakt om een extern script hiervan gebruik te laten maken (declareren monsteropslag!)
    -- de inhoud is te complex en gevoelig om te kopieren naar het script!
    v_debug := 'HL7';
    kgc_dft_utrecht.verstuur_dft_bericht(p_spec_systeemdeel    => v_spec_systeemdeel
                                        ,p_spec_patnr          => v_spec_patnr
                                        ,p_pers_trow           => v_pers_trow
                                        ,p_pers_is_dummy       => v_pers_is_dummy
                                        ,p_decl_id             => r_decl.decl_id
                                        ,p_spec_verdatum       => v_spec_verdatum
                                        ,p_spec_vercode        => v_spec_vercode
                                        ,p_spec_kafd_code      => v_spec_kafd_code
                                        ,p_spec_aanvr_afdeling => v_spec_aanvr_afdeling
                                        ,p_spec_prod_spec      => v_spec_prod_spec
                                        ,p_spec_aanvr_spec     => v_spec_aanvr_spec
                                        ,p_spec_abw            => v_spec_abw
                                        ,p_spec_tekst          => v_spec_tekst
                                        ,p_aantal              => p_aantal
                                        ,p_spec_abi_verzcd     => v_spec_abi_verzcd
                                        ,p_spec_abi_verznr     => v_spec_abi_verznr
                                        ,x_debug               => v_debug
                                        ,x_ok                  => v_ok
                                        ,x_msg                 => v_msg);

    IF NOT v_ok
    THEN
      v_msg := v_thisproc || ': ' || v_msg;
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Error op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
    ELSE
      -- verwerking OK - declaratie bijwerken!
      IF p_aantal > 0
      THEN
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Verwerkt op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ' door ' ||
                              kgc_mede_00.medewerker_code || '; Status:' ||
                              r_decl.status || '->V.');
        upd_decl(p_decl_id
                ,'V'
                ,trunc(SYSDATE)
                ,r_decl.verwerking);
        -- en ook: verrichtingdatum + code opbergen in declaratie
        /* nee - hoeft niet meer: code is altijd ok (en er wordt op getest); datum wordt ook getest!
        update kgc_declaraties decl
        set    datum = v_spec_verdatum
        ,      verrichtingcode = v_spec_vercode -- mogelijk is deze leeg (bv start, of metabool-via protocol!)
        where  decl.decl_id = r_decl.decl_id;
        commit;*/
      ELSE
        kgc_dft_interface.log(r_decl.verwerking
                             ,'Teruggedraaid op ' ||
                              to_char(SYSDATE
                                     ,'dd-mm-yyyy hh24:mi:ss') || ' door ' ||
                              kgc_mede_00.medewerker_code || '; Status:' ||
                              r_decl.status || '->R.');
        upd_decl(p_decl_id
                ,'R'
                ,trunc(SYSDATE)
                ,r_decl.verwerking);
      END IF;
    END IF;

    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      v_msg := v_thisproc || ': Debug: ' || v_debug || '; Error: ' ||
               SQLERRM;
      kgc_dft_interface.log(r_decl.verwerking
                           ,'Error op ' ||
                            to_char(SYSDATE
                                   ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                            v_msg);
      upd_decl(p_decl_id
              ,r_decl.status
              ,r_decl.datum_verwerking
              ,r_decl.verwerking);
      RETURN FALSE;
  END;

  /* globaal ******************/
  PROCEDURE controleer(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => 1
                                  ,p_check_only => TRUE);
  END controleer;

  PROCEDURE verwerk(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => 1
                                  ,p_check_only => FALSE);
  END verwerk;

  PROCEDURE draai_terug(p_decl_id IN NUMBER, x_ok OUT BOOLEAN) IS
  BEGIN
    x_ok := check_verstuur_dft_p03(p_decl_id    => p_decl_id
                                  ,p_aantal     => -1
                                  ,p_check_only => FALSE);
  END draai_terug;

  PROCEDURE test_bepaal_controle IS
    v_tab_controles  t_tab_controles;
    v_ccode_aanwezig BOOLEAN;
    v_ccode_parm_s   VARCHAR2(4000);
    v_ccode_parm_n   NUMBER;
  BEGIN
    dbms_output.put_line('Start test_bepaal_controle...');
    dbms_output.put_line('Test aanwezig zonder parameter - begin, midden, eind, afwezig');
    v_tab_controles(1) := 'A1,A2,A3';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test begin OK');
    ELSE
      dbms_output.put_line('Test begin FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test midden OK');
    ELSE
      dbms_output.put_line('Test midden FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test eind OK');
    ELSE
      dbms_output.put_line('Test eind FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test afwezig OK');
    ELSE
      dbms_output.put_line('Test afwezig FOUT');
    END IF;
    --
    dbms_output.put_line('Test aanwezig met parameter - begin, midden1, midden2, eind, afwezig');
    v_tab_controles(1) := 'A1:-1,A2:TEST,A3:-2,A4:9999999';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test begin OK');
      IF v_ccode_parm_n = -1
         AND v_ccode_parm_s = '-1'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test begin FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test midden1 OK');
      IF v_ccode_parm_n IS NULL
         AND v_ccode_parm_s = 'TEST'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test midden1 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test midden2 OK');
      IF v_ccode_parm_n = -2
         AND v_ccode_parm_s = '-2'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test midden2 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test eind OK');
      IF v_ccode_parm_n = 9999999
         AND v_ccode_parm_s = '9999999'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test eind FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A5'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test afwezig OK');
    ELSE
      dbms_output.put_line('Test afwezig FOUT');
    END IF;
    --
    dbms_output.put_line('Test nesting - level3, level2, level1, afwezig');
    v_tab_controles(1) := 'A1:-1,A2:TEST1,A3:-2';
    v_tab_controles(2) := 'A1:-2,A2:TEST2';
    v_tab_controles(3) := 'A1:-3';
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A1'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test level3 OK');
      IF v_ccode_parm_n = -3
         AND v_ccode_parm_s = '-3'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test level3 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A2'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test level2 OK');
      IF v_ccode_parm_n IS NULL
         AND v_ccode_parm_s = 'TEST2'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test level2 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A3'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test level1 OK');
      IF v_ccode_parm_n = -2
         AND v_ccode_parm_s = '-2'
      THEN
        dbms_output.put_line('... parameter OK');
      ELSE
        dbms_output.put_line('... parameter FOUT: s=>' || v_ccode_parm_s ||
                             '<; n=>' || to_char(v_ccode_parm_n) || '<');
      END IF;
    ELSE
      dbms_output.put_line('Test level1 FOUT');
    END IF;
    v_ccode_aanwezig := bepaal_controle(p_controles    => v_tab_controles
                                       ,p_ccode        => 'A4'
                                       ,x_ccode_parm_s => v_ccode_parm_s
                                       ,x_ccode_parm_n => v_ccode_parm_n);
    IF NOT v_ccode_aanwezig
    THEN
      dbms_output.put_line('Test afwezig OK');
    ELSE
      dbms_output.put_line('Test afwezig FOUT');
    END IF;
    dbms_output.put_line('Eind test_bepaal_controle...');
  END;

  PROCEDURE verstuur_dft_bericht(p_spec_systeemdeel    VARCHAR2
                                ,p_spec_patnr          VARCHAR2
                                ,p_pers_trow           cg$KGC_PERSONEN.cg$row_type
                                ,p_pers_is_dummy       BOOLEAN
                                ,p_decl_id             NUMBER
                                ,p_spec_verdatum       DATE
                                ,p_spec_vercode        VARCHAR2
                                ,p_spec_kafd_code      VARCHAR2
                                ,p_spec_aanvr_afdeling VARCHAR2
                                ,p_spec_prod_spec      VARCHAR2
                                ,p_spec_aanvr_spec     VARCHAR2
                                ,p_spec_abw            VARCHAR2
                                ,p_spec_tekst          VARCHAR2
                                ,p_aantal              NUMBER
                                ,p_spec_abi_verzcd     VARCHAR2
                                ,p_spec_abi_verznr     VARCHAR2
                                ,x_debug               IN OUT VARCHAR2
                                ,x_ok                  IN OUT BOOLEAN
                                ,x_msg                 IN OUT VARCHAR2) IS
    v_hl7_msg_bron   kgc_zis.hl7_msg;
    v_hl7_msg_res    kgc_zis.hl7_msg;
    v_strAntw        VARCHAR2(32767);
    v_strDFT         VARCHAR2(32767);
    v_MSA            VARCHAR2(10);
    v_MSH            VARCHAR2(10);
    v_sysdate        VARCHAR2(20) := to_char(SYSDATE
                                            ,'yyyymmddhh24miss');
    v_ok             BOOLEAN;
    v_msg_control_id VARCHAR2(10);
    v_msg            VARCHAR2(32767);
  BEGIN
    v_msg_control_id := SUBSTR(TO_CHAR(abs(dbms_random.random))
                              ,1
                              ,10);

    -- bericht samenstellen
    x_debug := '20';
    -- **** MSH
    v_hl7_msg_bron('MSH') := kgc_zis.init_msg('MSH'
                                             ,kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('MSH')(1)(1)(1) := '|';
    v_hl7_msg_bron('MSH')(2)(1)(1) := '^~\&';
    v_hl7_msg_bron('MSH')(3)(1)(1) := p_spec_systeemdeel;
    v_hl7_msg_bron('MSH')(5)(1)(1) := 'HISCOM-TOREN';
    v_hl7_msg_bron('MSH')(7)(1)(1) := v_sysdate;
    v_hl7_msg_bron('MSH')(9)(1)(1) := 'DFT^P03';
    v_hl7_msg_bron('MSH')(10)(1)(1) := v_msg_control_id;
    v_hl7_msg_bron('MSH')(11)(1)(1) := 'P';
    v_hl7_msg_bron('MSH')(12)(1)(1) := kgc_zis.g_sypa_hl7_versie;
    v_hl7_msg_bron('MSH')(15)(1)(1) := 'AL';
    v_hl7_msg_bron('MSH')(16)(1)(1) := 'NE';

    -- **** EVN
    x_debug := x_debug || 'b';
    v_hl7_msg_bron('EVN') := kgc_zis.init_msg('EVN'
                                             ,kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('EVN')(1)(1)(1) := 'P03';
    v_hl7_msg_bron('EVN')(2)(1)(1) := v_sysdate;

    -- **** PID
    x_debug := x_debug || 'c';
    v_hl7_msg_bron('PID') := kgc_zis.init_msg('PID'
                                             ,kgc_zis.g_sypa_hl7_versie);
    -- geen gebruik generieke functie vanwege verschil dummy - niet dummy!
    x_debug := x_debug || 'e';
    v_hl7_msg_bron('PID')(3)(1)(1) := p_spec_patnr;
    -- onderstaande regel: stond niet in oude specificatie, maar werd wel zo gedaan in actuele scripts!
    v_hl7_msg_bron('PID')(5)(1)(1) := substr(p_pers_trow.aanspreken
                                            ,1
                                            ,46); -- max 48 - 2 seps
    -- ruimte reserveren voor evt adres:
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 2
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 3
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 4
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF v_hl7_msg_bron('PID') (11) (1).COUNT < 5
    THEN
      v_hl7_msg_bron('PID')(11)(1) .EXTEND;
    END IF;
    IF p_pers_is_dummy
    THEN
      v_hl7_msg_bron('PID')(7)(1)(1) := TO_CHAR(p_pers_trow.geboortedatum
                                               ,'yyyymmdd');
      -- *** adres
      v_hl7_msg_bron('PID')(11)(1)(1) := SUBSTR(p_pers_trow.adres
                                               ,1
                                               ,61);
      v_hl7_msg_bron('PID')(11)(1)(3) := SUBSTR(p_pers_trow.woonplaats
                                               ,1
                                               ,30);
      v_hl7_msg_bron('PID')(11)(1)(4) := SUBSTR(p_pers_trow.land
                                               ,1
                                               ,15);
      v_hl7_msg_bron('PID')(11)(1)(5) := SUBSTR(REPLACE(p_pers_trow.postcode
                                                       ,' '
                                                       ,'')
                                               ,1
                                               ,6);
    END IF;

    -- **** FT1
    x_debug := x_debug || 'f';
    v_hl7_msg_bron('FT1') := kgc_zis.init_msg('FT1'
                                             ,kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('FT1')(2)(1)(1) := to_char(p_decl_id); -- declaratie-id
    v_hl7_msg_bron('FT1')(4)(1)(1) := to_char(p_spec_verdatum
                                             ,'yyyymmdd');

    v_hl7_msg_bron('FT1')(7)(1)(1) := p_spec_vercode;
    v_hl7_msg_bron('FT1')(13)(1)(1) := p_spec_kafd_code; -- producerende afdeling
    v_hl7_msg_bron('FT1')(16)(1)(1) := p_spec_aanvr_afdeling; -- aanvragende afdeling
    v_hl7_msg_bron('FT1')(20)(1)(1) := p_spec_prod_spec; -- producerende specialisme

    -- **** ZFU
    x_debug := x_debug || 'g';
    v_hl7_msg_bron('ZFU') := kgc_zis.init_msg('ZFU'
                                             ,kgc_zis.g_sypa_hl7_versie);

    v_hl7_msg_bron('ZFU')(11)(1)(1) := p_spec_aanvr_spec; -- aanvragende specialisme
    v_hl7_msg_bron('ZFU')(12)(1)(1) := p_spec_abw;
    v_hl7_msg_bron('ZFU')(14)(1)(1) := SUBSTR(p_spec_tekst
                                             ,1
                                             ,44);
    v_hl7_msg_bron('ZFU')(15)(1)(1) := LTRIM(to_char(p_aantal)); -- 1 of -1; is al gecontroleerd!

    -- **** IN1
    x_debug := x_debug || 'h';
    v_hl7_msg_bron('IN1') := kgc_zis.init_msg('IN1'
                                             ,kgc_zis.g_sypa_hl7_versie);
    v_hl7_msg_bron('IN1')(3)(1)(1) := p_spec_abi_verzcd;
    v_hl7_msg_bron('IN1')(36)(1)(1) := p_spec_abi_verznr;

    x_debug := x_debug || 'z';

    x_debug  := '30';
    v_strDFT := kgc_zis.g_sob_char ||
                kgc_zis.hl7seg2str('MSH'
                                  ,v_hl7_msg_bron('MSH')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('EVN'
                                  ,v_hl7_msg_bron('EVN')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('PID'
                                  ,v_hl7_msg_bron('PID')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('FT1'
                                  ,v_hl7_msg_bron('FT1')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('ZFU'
                                  ,v_hl7_msg_bron('ZFU')) || kgc_zis.g_cr ||
                kgc_zis.hl7seg2str('IN1'
                                  ,v_hl7_msg_bron('IN1')) || kgc_zis.g_cr ||
                kgc_zis.g_eob_char || kgc_zis.g_term_char;

    v_ok := kgc_dft_interface.send_msg(p_strHL7in  => v_strDFT
                                      ,p_timeout   => 150 -- 15 seconden
                                      ,x_strHL7out => v_strAntw
                                      ,x_msg       => v_msg);
    IF NOT v_ok
    THEN
      x_ok  := FALSE;
      x_msg := 'Verwerking NIET OK op ' ||
               to_char(SYSDATE
                      ,'dd-mm-yyyy hh24:mi:ss') || ': ' || v_msg;
      RETURN;
    END IF;

    IF NOT kgc_dft_interface.test_hl7
    THEN

      kgc_zis.log(v_strAntw
                 ,'1100XXXXXX');
      v_hl7_msg_res := kgc_zis.str2hl7msg(0
                                         ,v_strAntw);
      v_MSA         := kgc_zis.getSegmentCode('MSA'
                                             ,v_hl7_msg_res);
      v_MSH         := kgc_zis.getSegmentCode('MSH'
                                             ,v_hl7_msg_res);

      IF NVL(LENGTH(v_MSA)
            ,0) = 0
         OR NVL(LENGTH(v_MSH)
               ,0) = 0
      THEN
        x_ok  := FALSE;
        v_msg := 'MSA- of MSH-segment afwezig!';
        x_msg := 'Verwerking NIET OK op ' ||
                 to_char(SYSDATE
                        ,'dd-mm-yyyy hh24:mi:ss') || ': ' || v_msg;
        RETURN;
      END IF;
      -- check op control-id; soms wordt er iets aan vastgeplakt (=niet erg)
      IF SUBSTR(v_hl7_msg_res(v_MSH) (10) (1) (1)
               ,1
               ,LENGTH(v_msg_control_id)) <> v_msg_control_id
      THEN
        -- komt 'nooit' voor
        x_ok  := FALSE;
        v_msg := 'Fout bij ophalen gegevens: mismatch control-id! Verzonden: ' ||
                 v_msg_control_id || '; Ontvangen: ' ||
                 v_hl7_msg_res(v_MSH) (10) (1) (1) || '!';
        x_msg := 'Verwerking NIET OK op ' ||
                 to_char(SYSDATE
                        ,'dd-mm-yyyy hh24:mi:ss') || ': ' || v_msg;
        RETURN;
      END IF;
      IF NVL(v_hl7_msg_res(v_MSA) (1) (1) (1)
            ,'XX') NOT IN ('AA'
                          ,'CA')
      THEN
        x_ok  := FALSE;
        v_msg := 'MSA-1 = ' || v_hl7_msg_res(v_MSA) (1) (1)
                 (1) || '; Text: ' || v_hl7_msg_res(v_MSA) (3) (1) (1);
        x_msg := 'Verwerking NIET OK op ' ||
                 to_char(SYSDATE
                        ,'dd-mm-yyyy hh24:mi:ss') || ': ' || v_msg;
        RETURN;
      END IF;
    END IF;
    x_ok := TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      x_msg := 'Debug: ' || x_debug || '; Error: ' || SQLERRM;
      x_ok  := FALSE;
      RETURN;
  END; -- verstuur_dft_bericht;
END kgc_dft_utrecht;
/

/
QUIT
