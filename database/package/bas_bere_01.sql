CREATE OR REPLACE PACKAGE "HELIX"."BAS_BERE_01" IS
-- PL/SQL Specification
-- functie s_fractie kan gebruikt worden bij formule-parameters
-- en retourneert de meetwaarde van de stoftest bij de corresponderende
-- s-fractie die nodig IS om de recovery te
-- berekenen

FUNCTION S_FRACTIE
(
 p_meet_id IN NUMBER
)
RETURN NUMBER;
END BAS_BERE_01;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_BERE_01" IS
-- eerst het fractienummer ophalen van de s_fractie, daarna fractie id van de
-- bijbehorende H-fractie binnen hetzelfde onderzoek

g_stoftest_COX VARCHAR2 (3) := 'COX';
g_stoftest_CS  VARCHAR2 (2) := 'CS';

FUNCTION s_fractie
 (
  p_meet_id IN NUMBER
 )
RETURN NUMBER
IS

  v_waarde VARCHAR2(100);
  v_waarde_S VARCHAR2(100);
  v_waarde_H VARCHAR2(100);

-- s-fractiezoeken bij de recovery incl onderzoek
CURSOR zoek_s_cur
IS SELECT meti.onde_id,
	    frac.frac_id,
	    frac.fractienummer
   FROM   bas_fracties    frac,
	    bas_metingen    meti,
	    bas_meetwaarden meet
   WHERE  frac.frac_id = meti.frac_id AND
	    meti.meti_id = meet.meti_id AND
	    meet.meet_id = p_meet_id;
s_rec zoek_s_cur%rowtype;

-- zoeken van de meet_id binnen het onderzoek

CURSOR cox_s_cur
IS SELECT meet_id
   FROM   kgc_stoftesten  stof,
          bas_metingen    meti,
          bas_meetwaarden meet
   WHERE  meet.stof_id      = stof.stof_id
   AND    stof.omschrijving = g_stoftest_cox
   AND    meti.onde_id      = s_rec.onde_id
   AND    meti.frac_id      = s_rec.frac_id;
cox_s_rec cox_s_cur%rowtype;

-- corresponderende H-fractie zoeken
CURSOR zoek_h_cur
IS SELECT frac_id
   FROM   bas_fracties frac
   WHERE  SUBSTR(frac.fractienummer,-9) = SUBSTR(s_rec.fractienummer,-9)
   AND    SUBSTR(frac.fractienummer,1,1) = 'H';
h_rec zoek_h_cur%rowtype;

-- meet_id van h fractie zoeken

CURSOR cox_H_cur
IS SELECT meet_id
   FROM   kgc_stoftesten  stof,
          bas_metingen    meti,
          bas_meetwaarden meet
   WHERE  meet.stof_id      = stof.stof_id
   AND    stof.omschrijving = g_stoftest_cox
   AND    meti.onde_id      = s_rec.onde_id
   AND    meti.frac_id      = s_rec.frac_id ;
cox_h_rec cox_h_cur%rowtype;

-- PL/SQL Block
BEGIN

  OPEN  zoek_s_cur;
  FETCH zoek_s_cur
  INTO  s_rec;
  CLOSE zoek_s_cur;

  IF s_rec.frac_id IS NOT NULL
  THEN
      OPEN  cox_s_cur;
      FETCH cox_s_cur
      INTO  cox_s_rec;
      CLOSE cox_s_cur;
  END IF;
  IF cox_s_rec.meet_id IS NOT NULL
  THEN
    v_waarde_s := bas_bere_00.eigen_meetwaarde (p_meet_id => cox_s_rec.meet_id);
  END IF;

  OPEN  zoek_h_cur;
  FETCH zoek_h_cur
  INTO  h_rec;
  CLOSE zoek_h_cur;

  IF h_rec.frac_id IS NOT NULL
  THEN
     OPEN cox_h_cur;
     FETCH cox_h_cur
     INTO cox_h_rec;
     CLOSE cox_h_cur;
     IF cox_h_rec.meet_id IS NOT NULL THEN
        v_waarde_h := bas_bere_00.eigen_meetwaarde (p_meet_id => cox_h_rec.meet_id);
     END IF;
  END IF;
  IF NVL(v_waarde_h,0) <> 0
  THEN
    BEGIN
       v_waarde := v_waarde_s/v_waarde_h *100;
    EXCEPTION
       WHEN OTHERS
       THEN
         v_waarde := NULL;
    END;
  END IF;
  RETURN (v_waarde);
END s_fractie;


END bas_bere_01;
/

/
QUIT
