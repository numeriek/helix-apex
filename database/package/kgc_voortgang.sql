CREATE OR REPLACE PACKAGE "HELIX"."KGC_VOORTGANG" IS
-- voortgang van het fractie, uitgedrukt in een percentage
FUNCTION bereken_fractie
( p_frac_id IN NUMBER
)
RETURN NUMBER;
--PRAGMA RESTRICT_REFERENCES ( bereken_fractie, WNDS, WNPS, RNPS );
-- voortgang van het onderzoek, uitgedrukt in een percentage
FUNCTION bereken_onderzoek
( p_onde_id IN NUMBER
)
RETURN NUMBER;
--PRAGMA RESTRICT_REFERENCES ( bereken_onderzoek, WNDS, WNPS, RNPS );
-- item
PROCEDURE bereken_onderzoek
( p_onde_id IN NUMBER
, x_percentage OUT NUMBER
);
-- stel een lijst op met uitstaande aktiviteiten
PROCEDURE nog_te_doen
( p_onde_id IN NUMBER
, x_overzicht OUT VARCHAR2
);
END KGC_VOORTGANG;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_VOORTGANG" IS
/******************************************************************************
      NAME:       KGC_VOORTGANG

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.2        2016-02-17   SKA             Mantis 9355 TO_CHAR added for conclusie in function bereken_onderzoek release 8.11.0.2

   *****************************************************************************/
FUNCTION bereken_fractie
( p_frac_id IN NUMBER
)
RETURN NUMBER
IS
  v_pct NUMBER := 0;
-- deze cursor moet anders: voortgang mede bepalen op basis van kweekfracties!
  CURSOR frac_cur
  IS
    SELECT frac.frac_id
    ,      frac.creation_date
    ,      MIN(kwfr.datum_inzet) datum_inzet
    ,      MAX(kwfr.datum_uithaal) datum_uithaal
    ,      frac.status
    ,      MAX(kwfr.kweekduur) aantal_dagen_kweek
    FROM   bas_fractie_types frty
    ,      kgc_kweekfracties kwfr
    ,      bas_fracties frac
    WHERE  frac.frty_id = frty.frty_id (+)
    AND    frac.frac_id = kwfr.frac_id (+)
    AND    frac.frac_id = p_frac_id
    GROUP BY frac.frac_id
    ,      frac.creation_date
    ,      frac.status
    ;
  frac_rec frac_cur%rowtype;
BEGIN
  IF ( p_frac_id IS NULL )
  THEN
    v_pct := 0;
  ELSE
    OPEN  frac_cur;
    FETCH frac_cur
    INTO  frac_rec;
    CLOSE frac_cur;
    IF ( frac_rec.datum_uithaal IS NOT NULL )
    THEN -- al uitgehaald, fractie activiteiten klaar
      v_pct := 100;
    ELSIF ( frac_rec.datum_inzet IS NOT NULL )
    THEN -- ingezet, nog niet uitgehaald
      IF ( NVL( frac_rec.aantal_dagen_kweek, 0 ) <= 0 )
      THEN
        v_pct := 50;
      ELSIF ( ( frac_rec.datum_inzet + frac_rec.aantal_dagen_kweek ) - SYSDATE <= 0 )
      THEN -- uithaaldatum overschreden
        v_pct := 100;
      ELSE
        v_pct := ( ( SYSDATE - frac_rec.datum_inzet )
                 / frac_rec.aantal_dagen_kweek
                 ) * 75;
      END IF;
    ELSE -- niet ingezet
      IF ( NVL( frac_rec.aantal_dagen_kweek, 0 ) <= 0 )
      THEN
        v_pct := 100; -- geen kweektijd
      ELSE
        IF ( ( frac_rec.creation_date + frac_rec.aantal_dagen_kweek ) - SYSDATE <= 0 )
        THEN -- uithaaldatum overschreden
          v_pct := 100;
        ELSE
          v_pct := ( ( SYSDATE - frac_rec.creation_date )
                   / frac_rec.aantal_dagen_kweek
                   ) * 75;
        END IF;
      END IF;
    END IF;

    IF ( frac_rec.status = 'A' )
    THEN
      v_pct := v_pct * 0.5;
    END IF;
  END IF;
  RETURN( ROUND( v_pct, 1 ) );
END bereken_fractie;

FUNCTION bereken_onderzoek
( p_onde_id IN NUMBER
)
RETURN  NUMBER
IS
  v_pct NUMBER;
  CURSOR frac_cur
  IS
    SELECT frac.frac_id
    ,      onmo.mons_id
    FROM   bas_fracties frac
    ,      kgc_onderzoek_monsters onmo
    WHERE  onmo.mons_id = frac.mons_id (+)
    AND    onmo.onde_id = p_onde_id
    ;

  CURSOR meti_cur
  IS
    SELECT COUNT(*) aantal
    ,      SUM  ( DECODE( meti.afgerond
                        , 'J', 100
                        , DECODE( TO_CHAR(meti.conclusie) --Added TO_CHAR by Rajendra for Mantis 9355(16-06-2014)
                                                          --TO_CHAR(meti.conclusie) changed to  meti.conclusie by sachin 12-06-2015 for reverting 9355 changes in 201502 release rework
                                                          --TO_CHAR added by SKA for MANTIS 9355  2016-02-17 release 8.11.0.2
                                , NULL, DECODE( meet.afgerond
                                              , 'J', 75
                                              , 25
                                              )
                                , DECODE( meet.afgerond
                                        , 'J', 100
                                        , 75
                                        )
                                )
                        )
                ) aantal_afgerond
    FROM   bas_meetwaarden meet
    ,      bas_metingen meti
    WHERE  meti.meti_id = meet.meti_id (+)
    AND    meti.onde_id = p_onde_id
    ;

  v_aantal NUMBER := 0;

  v_pct_mons NUMBER := 0;
  v_pct_frac NUMBER := 0;
  v_pct_meti NUMBER := 0;
 BEGIN
  IF ( p_onde_id IS NULL )
  THEN
    v_pct := 0;
  ELSE
    v_aantal := 0;
    FOR r IN frac_cur
    LOOP
      v_aantal := v_aantal + 1;
      IF ( r.frac_id IS NOT NULL )
      THEN
        v_pct_mons := v_pct_mons + 100;
        v_pct_frac := v_pct_frac + bereken_fractie( r.frac_id );
      ELSE
        v_pct_mons := v_pct_mons + 50; -- wel monster, geen fractie
      END IF;
    END LOOP;
    IF ( NVL( v_aantal, 0 ) = 0 )
    THEN
      v_pct_mons := 0;
      v_pct_frac := 0;
    ELSE
      v_pct_mons := v_pct_mons
                  / v_aantal
                  ;
      v_pct_frac := v_pct_frac
                  / v_aantal
                  ;
    END IF;
    FOR r IN meti_cur -- ( 1x )
    LOOP
      IF ( r.aantal > 0 )
      THEN
        v_pct_meti := r.aantal_afgerond -- factor (100,...) zit in cursor
                    / r.aantal;
      ELSE
        v_pct_meti := 0;
      END IF;
    END LOOP;

    -- monster bepaalt eerste 5%
    -- fractie bepaalt volgende 20%
    -- meting bepaalt laatste 75%
-- te doen: uitslag, afronding, declaratie
    v_pct := ( v_pct_mons *  5 / 100 )
           + ( v_pct_frac * 20 / 100 )
           + ( v_pct_meti * 75 / 100 )
             ;
  END IF;
  RETURN( ROUND( v_pct, 1 ) );
END bereken_onderzoek;

PROCEDURE  bereken_onderzoek
( p_onde_id IN NUMBER
, x_percentage OUT NUMBER
)
IS
BEGIN
  x_percentage := bereken_onderzoek( p_onde_id );
END bereken_onderzoek;

PROCEDURE nog_te_doen
( p_onde_id IN NUMBER
, x_overzicht OUT VARCHAR2
)
IS
BEGIN
  -- fractie inzetten, uithalen, status niet op A
  -- meetwaarden vastleggen / afronden
  -- resultaat van meting vastleggen
  -- uitslag van onderzoek vastleggen
  -- onderzoek autoriseren / afronden
  -- uitslagbrief
  -- declaratie
  x_overzicht := 'Onder constructie!';
END nog_te_doen;
END kgc_voortgang;
/

/
QUIT
