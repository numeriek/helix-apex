CREATE OR REPLACE PACKAGE "HELIX"."KGC_MWST_00" IS
-- bepaal de meetwaarde structuur in een bepaalde setting
PROCEDURE meetwaarde_structuur
( p_kafd_id    IN NUMBER := NULL
, p_ongr_id    IN NUMBER := NULL
, p_onde_id    IN NUMBER := NULL
, p_mons_id    IN NUMBER := NULL
, p_frac_id    IN NUMBER := NULL
, p_meti_id    IN NUMBER := NULL
, p_stof_id    IN NUMBER := NULL
, p_meet_id    IN NUMBER := NULL
, p_wlst_id    IN NUMBER := NULL
, p_wlst_where IN VARCHAR2 := NULL -- Tbv KGCWLST03 en KGCWLST05A
, x_mwst_id    OUT NUMBER
, x_aantal     OUT NUMBER
);
-- overloaded als function
FUNCTION meetwaarde_structuur
( p_kafd_id    IN NUMBER := NULL
, p_ongr_id    IN NUMBER := NULL
, p_onde_id    IN NUMBER := NULL
, p_mons_id    IN NUMBER := NULL
, p_frac_id    IN NUMBER := NULL
, p_meti_id    IN NUMBER := NULL
, p_stof_id    IN NUMBER := NULL
, p_meet_id    IN NUMBER := NULL
, p_wlst_id    IN NUMBER := NULL
, p_wlst_where IN VARCHAR2 := NULL -- Tbv KGCWLST03 en KGCWLST05A
)
RETURN NUMBER;

PROCEDURE meetwaarde_details_aanmelden
( p_meet_id IN NUMBER
, p_mwst_id IN NUMBER := NULL
);

-- is er een vervolgactie gedefinieerd na een (niet-succesvolle) meetwaarde
-- zo retourneer een actie:
-- 0 = onbekend wat
-- 1 = zelfde stoftest aanmelden
-- 2 = zelfde stoftest aanmelden, incl.voortest
-- 3 = andere stoftest aanmelden
-- Noot: bepaal het succes aan de hand van de opgegeven waarde en NIET van de waarde in bas_mdet_ok.ok
--       want dat is mogelijk nog niet bijgewerkt: db-triggers
FUNCTION vervolgactie
( p_meet_id IN NUMBER
, p_volgorde IN NUMBER
, p_waarde IN VARCHAR2
)
RETURN VARCHAR2;

-- retourneer een waarde uit de meetwaardestructuur bij een meting
FUNCTION detailwaarde_meting
( p_meti_id in number
, p_prompt in varchar2
)
return varchar2;
END KGC_MWST_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_MWST_00" IS
  -- Globale package-variabelen (om performance-redenen nodig)
	g_wlit_query_laatst VARCHAR2(32767) := '?';
  g_aantal_laatst     NUMBER;
  g_mwst_id_laatst    NUMBER;

PROCEDURE meetwaarde_structuur
( p_kafd_id    IN NUMBER := NULL
, p_ongr_id    IN NUMBER := NULL
, p_onde_id    IN NUMBER := NULL
, p_mons_id    IN NUMBER := NULL
, p_frac_id    IN NUMBER := NULL
, p_meti_id    IN NUMBER := NULL
, p_stof_id    IN NUMBER := NULL
, p_meet_id    IN NUMBER := NULL
, p_wlst_id    IN NUMBER := NULL
, p_wlst_where IN VARCHAR2 := NULL -- Tbv KGCWLST03 en KGCWLST05B
, x_mwst_id    OUT NUMBER
, x_aantal     OUT NUMBER
)
IS
  v_aantal  NUMBER := 0;
  v_mwst_id NUMBER;

  CURSOR ongr_cur
  IS
    SELECT ongr.kafd_id
    ,      ongr.ongr_id
    FROM   kgc_onderzoeksgroepen ongr
    WHERE  ongr.kafd_id = NVL( p_kafd_id, ongr.kafd_id )
    AND    ongr.ongr_id = NVL( p_ongr_id, ongr.ongr_id )
    AND  ( p_onde_id IS NULL
        OR EXISTS
           ( SELECT NULL
             FROM   kgc_onderzoeken onde
             WHERE  onde.ongr_id = ongr.ongr_id
             AND    onde.onde_id = p_onde_id
           )
         )
    AND  ( p_mons_id IS NULL
        OR EXISTS
           ( SELECT NULL
             FROM   kgc_monsters mons
             WHERE  mons.ongr_id = ongr.ongr_id
             AND    mons.mons_id = p_mons_id
           )
         )
    AND  ( p_frac_id IS NULL
        OR EXISTS
           ( SELECT NULL
             FROM   kgc_monsters mons
             ,      bas_fracties frac
             WHERE  mons.ongr_id = ongr.ongr_id
             AND    mons.mons_id = frac.mons_id
             AND    frac.frac_id = p_frac_id
           )
         )
    AND  ( p_meti_id IS NULL
        OR EXISTS
           ( SELECT NULL
             FROM   kgc_onderzoeken onde
             ,      bas_metingen meti
             WHERE  onde.ongr_id = ongr.ongr_id
             AND    onde.onde_id = meti.onde_id
             AND    meti.meti_id = p_meti_id
           )
         );
  ongr_rec ongr_cur%rowtype;

  -- standaardwaarde meetwaardestructuur
  sypa_waarde VARCHAR2(100);
  v_sypa_mwst_id NUMBER;
  CURSOR mwst_cur
  IS
    SELECT mwst_id
    FROM   kgc_meetwaardestructuur
    WHERE  code = sypa_waarde
    ;

  CURSOR stof_cur -- stof_id
  IS
    SELECT 1 aantal
    ,      NVL( stof.mwst_id, v_sypa_mwst_id ) mwst_id
    FROM   kgc_stoftesten stof
    WHERE  stof.stof_id = p_stof_id
    ;
  CURSOR stof_cur1 -- kafd_id, ongr_id
  IS
    SELECT COUNT( DISTINCT NVL(stof.mwst_id, NVL(v_sypa_mwst_id,-9999999999) ) ) aantal
    ,      MIN( stof.mwst_id ) mwst_id
    FROM   kgc_stoftesten stof
    ,      kgc_monsters mons
    ,      kgc_onderzoek_monsters onmo
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  stof.stof_id = meet.stof_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.onmo_id = onmo.onmo_id
    AND    onmo.mons_id = mons.mons_id
    AND    meet.meet_reken IN ('M','V','S')
    AND    stof.kafd_id = NVL( p_kafd_id, stof.kafd_id )
    AND    mons.kafd_id = NVL( p_kafd_id, mons.kafd_id )
    AND    mons.ongr_id = NVL( p_ongr_id, mons.ongr_id )
    ;
  CURSOR stof_cur2 -- onde_id, mons_id
  IS
    SELECT COUNT( DISTINCT NVL(stof.mwst_id, NVL(v_sypa_mwst_id,-9999999999) ) ) aantal
    ,      MIN( stof.mwst_id ) mwst_id
    FROM   kgc_stoftesten stof
    ,      kgc_onderzoek_monsters onmo
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  stof.stof_id = meet.stof_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.onmo_id = onmo.onmo_id
    AND    meet.meet_reken IN ('M','V','S')
    AND    onmo.onde_id = NVL( p_onde_id, onmo.onde_id )
    AND    onmo.mons_id = NVL( p_mons_id, onmo.mons_id )
    ;
  CURSOR stof_cur3 -- frac_id
  IS
    SELECT COUNT( DISTINCT NVL(stof.mwst_id, NVL(v_sypa_mwst_id,-9999999999) ) ) aantal
    ,      MIN( stof.mwst_id ) mwst_id
    FROM   kgc_stoftesten stof
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  stof.stof_id = meet.stof_id
    AND    meet.meti_id = meti.meti_id
    AND    meet.meet_reken IN ('M','V','S')
--    AND    meti.frac_id = NVL( p_frac_id, meti.frac_id )
    AND    meti.frac_id = p_frac_id
    AND    meti.onde_id = NVL( p_onde_id, meti.onde_id )
    ;
  CURSOR stof_cur4 -- meti_id
  IS
    SELECT COUNT( DISTINCT NVL(stof.mwst_id, NVL(v_sypa_mwst_id,-9999999999) ) ) aantal
    ,      MIN( stof.mwst_id ) mwst_id
    FROM   kgc_stoftesten stof
    ,      bas_meetwaarden meet
    WHERE  stof.stof_id = meet.stof_id
    AND    meet.meet_reken IN ('M','V','S')
--    AND    meet.meti_id = NVL( p_meti_id, meet.meti_id )
    AND    meet.meti_id = p_meti_id
    ;
  CURSOR stof_cur5 -- wlst_id
  IS
    SELECT COUNT( DISTINCT NVL(stof.mwst_id, NVL(v_sypa_mwst_id,-9999999999) ) ) aantal
    ,      MIN( stof.mwst_id ) mwst_id
    FROM   kgc_stoftesten stof
    ,      kgc_werklijst_items wlit
    WHERE  stof.stof_id = wlit.stof_id
--    AND    wlit.wlst_id = NVL( p_wlst_id, wlit.wlst_id )
    AND    wlit.wlst_id = p_wlst_id
    AND    NVL( wlit.mons_id, 0 ) = NVL( p_mons_id
                                       , NVL( wlit.mons_id, 0 )
                                       )
    AND    wlit.stof_id = NVL( p_stof_id, wlit.stof_id )
    ;
  CURSOR stof_cur6 -- meet_id
  IS
    SELECT COUNT( DISTINCT NVL(stof.mwst_id, NVL(v_sypa_mwst_id,-9999999999) ) ) aantal
    ,      MIN( stof.mwst_id ) mwst_id
    FROM   kgc_stoftesten stof
    ,      bas_meetwaarden meet
    WHERE  stof.stof_id = meet.stof_id
    AND    meet.meet_id = p_meet_id
    ;
  -- CURSOR stof_cur7 -- p_wlst_where (dynamische cursor):
	-- query van KGCWLST03/KGCWLST05B WLIT1-block bij aanroep vanuit KGCOPTE01 simuleren.
	v_wlit_query VARCHAR2(32767) := NULL;
  v_wlit_result VARCHAR2(100)  := NULL;
BEGIN
  OPEN  ongr_cur;
  FETCH ongr_cur
  INTO  ongr_rec;
  CLOSE ongr_cur;

  BEGIN
    sypa_waarde := kgc_sypa_00.standaard_waarde
                   ( p_parameter_code => 'MEETWAARDE_STRUCTUUR'
                   , p_kafd_id => NVL( p_kafd_id, ongr_rec.kafd_id )
                   , p_ongr_id => NVL( p_ongr_id, ongr_rec.ongr_id )
                   );
    OPEN  mwst_cur;
    FETCH mwst_cur
    INTO  v_sypa_mwst_id;
    CLOSE mwst_cur;
  EXCEPTION
    WHEN OTHERS
    THEN
      sypa_waarde := NULL;
      v_sypa_mwst_id := NULL;
  END;

  IF ( p_meti_id IS NOT NULL
   AND p_stof_id IS NOT NULL
     )
  THEN -- zie bir-trigger op bas_meetwaarden
    OPEN  stof_cur;
    FETCH stof_cur
    INTO  v_aantal
    ,     v_mwst_id;
    CLOSE stof_cur;
  ELSIF ( p_meet_id IS NOT NULL )
  THEN
    OPEN  stof_cur6;
    FETCH stof_cur6
    INTO  v_aantal
    ,     v_mwst_id;
    CLOSE stof_cur6;
  ELSIF ( p_wlst_id IS NOT NULL )
  THEN
    OPEN  stof_cur5;
    FETCH stof_cur5
    INTO  v_aantal
    ,     v_mwst_id;
    CLOSE stof_cur5;
  ELSIF ( p_meti_id IS NOT NULL )
  THEN
    OPEN  stof_cur4;
    FETCH stof_cur4
    INTO  v_aantal
    ,     v_mwst_id;
    CLOSE stof_cur4;
  ELSIF ( p_frac_id IS NOT NULL )
  THEN
    OPEN  stof_cur3;
    FETCH stof_cur3
    INTO  v_aantal
    ,     v_mwst_id;
    CLOSE stof_cur3;
  ELSIF ( p_mons_id IS NOT NULL
       OR p_onde_id IS NOT NULL
        )
  THEN
    OPEN  stof_cur2;
    FETCH stof_cur2
    INTO  v_aantal
    ,     v_mwst_id;
    CLOSE stof_cur2;
  ELSIF ( p_wlst_where IS NOT NULL )
  THEN
    v_wlit_query :=
      ' SELECT COUNT( DISTINCT NVL(stof.mwst_id, NVL(''' || v_sypa_mwst_id || ''', -9999999999) ) ) ' ||
      '        || ''/'' || ' ||
      '        MIN( stof.mwst_id ) aantal_en_mwst_id ' ||
      ' FROM   kgc_stoftesten stof ' ||
      ' ,      kgc_werklijst_items wlit ' ||
      ' WHERE  stof.stof_id = wlit.stof_id ' ||
      ' AND    wlit.meet_id IN ' ||
      '          (SELECT meet.meet_id ' ||
      '             FROM bas_meetwaarden meet ' ||
      '                , kgc_stoftesten stof ' ||
      '                , kgc_openstaande_testen_vw opte ' ||
      '            WHERE meet.meti_id = opte.meti_id ' ||
      '              AND meet.stof_id = stof.stof_id ' ||
      '              AND ' || p_wlst_where || ')';
    IF ( v_wlit_query <> g_wlit_query_laatst )
    THEN -- om, performance-redenen alleen query uitvoeren indien nodig!
      v_wlit_result := kgc_util_00.dyn_exec(v_wlit_query);
      -- v_wlit_result ontleden in v_aantal en v_mwst_id
      v_aantal  := substr( v_wlit_result, 1, instr(  v_wlit_result, '/' ) - 1 );
      v_mwst_id := substr( v_wlit_result, instr(  v_wlit_result, '/' ) + 1 );
      -- Laatste waarden bewaren voor volgende aanroep
      g_wlit_query_laatst := v_wlit_query;
      g_aantal_laatst     := v_aantal;
      g_mwst_id_laatst    := v_mwst_id;
    ELSE
      v_aantal  := g_aantal_laatst;
      v_mwst_id := g_mwst_id_laatst;
    END IF;
  ELSIF ( p_kafd_id IS NOT NULL
       OR p_ongr_id IS NOT NULL
        )
  THEN
    OPEN  stof_cur1;
    FETCH stof_cur1
    INTO  v_aantal
    ,     v_mwst_id;
    CLOSE stof_cur1;
  ELSE
    v_aantal := 2; --??
  END IF;

  IF ( v_aantal <> 1 )
  THEN
    v_mwst_id := v_sypa_mwst_id;
  END IF;
  x_mwst_id := NVL( v_mwst_id, v_sypa_mwst_id );
  x_aantal := v_aantal;
EXCEPTION
  WHEN OTHERS
  THEN
    x_mwst_id := v_sypa_mwst_id;
    x_aantal := 1;
END meetwaarde_structuur;

FUNCTION meetwaarde_structuur
( p_kafd_id    IN NUMBER := NULL
, p_ongr_id    IN NUMBER := NULL
, p_onde_id    IN NUMBER := NULL
, p_mons_id    IN NUMBER := NULL
, p_frac_id    IN NUMBER := NULL
, p_meti_id    IN NUMBER := NULL
, p_stof_id    IN NUMBER := NULL
, p_meet_id    IN NUMBER := NULL
, p_wlst_id    IN NUMBER := NULL
, p_wlst_where IN VARCHAR2 := NULL -- Tbv KGCWLST03 en KGCWLST05A
)
RETURN NUMBER
IS
  v_aantal NUMBER;
  v_mwst_id NUMBER;
BEGIN
  meetwaarde_structuur
  ( p_kafd_id    => p_kafd_id
  , p_ongr_id    => p_ongr_id
  , p_onde_id    => p_onde_id
  , p_mons_id    => p_mons_id
  , p_frac_id    => p_frac_id
  , p_meti_id    => p_meti_id
  , p_stof_id    => p_stof_id
  , p_meet_id    => p_meet_id
  , p_wlst_id    => p_wlst_id
  , p_wlst_where => p_wlst_where
  , x_mwst_id    => v_mwst_id
  , x_aantal     => v_aantal
  );
  RETURN( v_mwst_id );
END meetwaarde_structuur;

PROCEDURE meetwaarde_details_aanmelden
( p_meet_id IN NUMBER
, p_mwst_id IN NUMBER := NULL
)
IS
  v_mwst_id NUMBER := p_mwst_id;
  CURSOR mwst_cur
  IS
    SELECT mwss_id
    ,      volgorde
    ,      prompt
    FROM   kgc_mwst_samenstelling mwss
    WHERE  mwss.mwst_id = v_mwst_id
    AND    nvl( mwss.aanmelden, 0 ) = 0
    ORDER BY volgorde ASC
    ;
  CURSOR mwtw_cur
  ( b_mwss_id IN NUMBER
  )
  IS
    SELECT db_waarde
    FROM   kgc_mwss_toegestane_waarden
    WHERE  mwss_id = b_mwss_id
    AND    standaard = 'J'
    ;
  v_default_waarde VARCHAR2(100);
BEGIN
  IF ( p_mwst_id IS NULL )
  THEN
    v_mwst_id := meetwaarde_structuur
                 ( p_meet_id => p_meet_id
                 );
  END IF;
  FOR r IN mwst_cur
  LOOP
    v_default_waarde := NULL;
    OPEN  mwtw_cur( r.mwss_id );
    FETCH mwtw_cur
    INTO  v_default_waarde;
    CLOSE mwtw_cur;
    BEGIN
      UPDATE bas_meetwaarde_details
      SET    waarde = v_default_waarde
      ,      prompt = r.prompt
      WHERE  meet_id = p_meet_id
      AND    volgorde = r.volgorde
      AND    prompt = r.prompt
      ;
      IF ( sql%rowcount = 0 )
      THEN
        INSERT INTO bas_meetwaarde_details
        ( meet_id
        , volgorde
        , prompt
        , waarde
        )
        VALUES
        ( p_meet_id
        , r.volgorde
        , r.prompt
        , v_default_waarde
        );
      END IF;
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END LOOP;
END meetwaarde_details_aanmelden;

FUNCTION  vervolgactie
(  p_meet_id  IN  NUMBER
,  p_volgorde  IN  NUMBER
,  p_waarde  IN  VARCHAR2
)
RETURN  VARCHAR2
IS
  CURSOR meet_cur
  IS
    SELECT mdet.mdet_id
    ,      meet.mwst_id
    ,      mwss.vervolgactie
    FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarden meet
    ,      bas_meetwaarde_details mdet
    WHERE  mdet.meet_id = meet.meet_id
    AND    meet.mwst_id = mwss.mwst_id
    AND    mwss.volgorde = p_volgorde
    AND    mdet.meet_id = p_meet_id
    AND    mdet.volgorde = p_volgorde
    ;
  meet_rec meet_cur%rowtype;

  v_ok VARCHAR2(1);
BEGIN
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  meet_rec;
  CLOSE meet_cur;
  v_ok := bas_mdok_00.meetwaarde_detail_ok
          ( p_meet_id => p_meet_id
          , p_volgorde => p_volgorde
          , p_mwst_id => meet_rec.mwst_id
          , p_waarde => p_waarde
          );
  IF ( v_ok = 'N' )
  THEN
    RETURN( meet_rec.vervolgactie );
  ELSE
    RETURN( NULL ); -- geen vervolgactie
  END IF;
END vervolgactie;

FUNCTION detailwaarde_meting
( p_meti_id in number
, p_prompt in varchar2
)
return varchar2
is
  v_return varchar2(1000);
  CURSOR meti_cur
  IS
    SELECT metd.waarde
    FROM   bas_meting_details metd
    WHERE  metd.meti_id = p_meti_id
    AND    upper(metd.prompt) = upper(p_prompt)
    order by metd.metd_id desc -- jonste eerst ???
    ;
begin
  OPEN  meti_cur;
  FETCH meti_cur
  INTO  v_return;
  CLOSE meti_cur;
  return( v_return );
end detailwaarde_meting;
END kgc_mwst_00;
/

/
QUIT
