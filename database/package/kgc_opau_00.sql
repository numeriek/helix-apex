CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPAU_00" IS
/* LST 26-07-2004                                              */
/* Package voor procedures op de tabel KGC_OPSLAG_AUTORISATIE   */
FUNCTION mag_actie_uitvoeren
(p_opel_id    IN NUMBER
,p_oracle_uid IN VARCHAR2
,p_actie      IN VARCHAR2 -- O = opslaan, U = uithalen
) RETURN NUMBER
;
END KGC_OPAU_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPAU_00" IS
/* LST 26-07-2004                                            */
/* Package voor procedures op de tabel KGC_OPSLAG_Eautorisatie  */

FUNCTION mag_actie_uitvoeren
(p_opel_id    IN NUMBER
,p_oracle_uid IN VARCHAR2
,p_actie      IN VARCHAR2 -- I = Inzien, O = opslaan, U = uithalen
) RETURN NUMBER
IS
  CURSOR c_opel
  (cp_opel_id NUMBER
  ) IS
  SELECT opel.opel_id
  FROM   kgc_opslag_elementen opel
  CONNECT BY PRIOR opel.parent_opel_id = opel.opel_id
  START WITH opel.opel_id = cp_opel_id
  ;
  CURSOR c_opau
  (cp_opel_id    NUMBER
  ,cp_oracle_uid VARCHAR2
  ,cp_actie      VARCHAR2
  ) IS
  SELECT '1'
  FROM   kgc_medewerkers mede
  ,      kgc_kgc_afdelingen kafd
  ,      kgc_mede_kafd meka
  ,      kgc_opslag_autorisatie opau
  WHERE  opau.opel_id = cp_opel_id
  AND    ((opau.opslaan = 'J' AND cp_actie = 'O')
          OR
          (opau.uithalen = 'J' AND cp_actie = 'U')
          OR
          (opau.inzien = 'J' AND cp_actie = 'I')
         )
  AND    ((opau.mede_id = mede.mede_id
           AND
                   mede.oracle_uid = cp_oracle_uid
                  ) OR
                  (opau.kafd_id = kafd.kafd_id
                   AND
                   kafd.KAFD_ID= meka.kafd_id
                   AND
                   meka.mede_id = mede.mede_id
                   AND
                   mede.oracle_uid = cp_oracle_uid
                  )
                 )
  ;
  v_dummy   VARCHAR2(1);
  v_return  NUMBER(1) := 0; -- 0 = false, 1 = true
  v_opel_id kgc_opslag_elementen.opel_id%type;
BEGIN
  IF ( p_opel_id IS NULL )
  THEN
    RETURN( 1 );
  END IF;
  OPEN c_opel(p_opel_id);
  FETCH c_opel INTO v_opel_id;
  WHILE c_opel%found
  AND v_return = 0
  LOOP
    OPEN c_opau(v_opel_id, p_oracle_uid, p_actie);
    FETCH c_opau INTO v_dummy;
    IF c_opau%found
    THEN
      v_return := 1;
    END IF;
    CLOSE c_opau;
    FETCH c_opel INTO v_opel_id;
  END LOOP;
  CLOSE c_opel;
  RETURN ( v_return );
EXCEPTION
  WHEN OTHERS THEN
    IF c_opau%isopen
    THEN
      CLOSE c_opau;
    END IF;
    IF c_opel%isopen
    THEN
      CLOSE c_opel;
    END IF;
    RAISE;
END mag_actie_uitvoeren;
END KGC_OPAU_00;
/

/
QUIT
