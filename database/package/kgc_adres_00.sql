CREATE OR REPLACE PACKAGE "HELIX"."KGC_ADRES_00" IS
FUNCTION persoon
( p_pers_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( persoon, WNDS, WNPS );

FUNCTION relatie
( p_rela_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( relatie, WNDS, WNPS );

FUNCTION instelling
( p_inst_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( instelling, WNDS, WNPS );

FUNCTION verzekeraar
( p_verz_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( verzekeraar, WNDS, WNPS );
END KGC_ADRES_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ADRES_00" IS
-- lokaal:
-- strip overtollige tekens: spaties, <cr>, tabs
FUNCTION strip
( p_tekst IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(32767) := p_tekst;
BEGIN
  v_return := LTRIM( RTRIM ( v_return ) );
  v_return := LTRIM( RTRIM ( v_return, CHR(10) ), CHR(10) );
  v_return := LTRIM( RTRIM ( v_return, CHR(32) ), CHR(32) );
  v_return := LTRIM( RTRIM ( v_return, CHR(10) ), CHR(10) );
  v_return := LTRIM( RTRIM ( v_return ) );
  RETURN( v_return );
END strip;

-- publiek:
FUNCTION persoon
( p_pers_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_adres VARCHAR2(2000);
  v_naam VARCHAR2(100);
  CURSOR pers_cur
  IS
    SELECT pers.aanspreken
    ,      pers.adres
    ,      pers.postcode
    ,      pers.woonplaats
    ,      pers.land
    FROM   kgc_personen pers
    WHERE  pers.pers_id = p_pers_id
    ;
  pers_rec pers_cur%rowtype;
BEGIN
  OPEN  pers_cur;
  FETCH pers_cur
  INTO  pers_rec;
  CLOSE pers_cur;
  IF ( p_met_naam = 'J' )
  THEN
    v_naam := pers_rec.aanspreken;
  ELSE
    v_naam := NULL;
  END IF;
  v_adres := kgc_formaat_00.volledig_adres
             ( p_naam => v_naam
             , p_adres => pers_rec.adres
             , p_postcode => pers_rec.postcode
             , p_woonplaats => pers_rec.woonplaats
             , p_land => pers_rec.land
             );
  RETURN( strip( v_adres ) );
END persoon;

FUNCTION relatie
( p_rela_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_adres VARCHAR2(2000);
  CURSOR rela_cur
  IS
    SELECT rela.aanspreken
    ,      rela.adres
    ,      rela.postcode
    ,      rela.woonplaats
    ,      rela.land
    ,      rela.inst_id
    ,      rela.afdg_id
    ,      rela.interne_post
    FROM   kgc_relaties rela
    WHERE  rela.rela_id = p_rela_id
    ;
  rela_rec rela_cur%rowtype;
  tmp_adres VARCHAR2(1000);
  CURSOR afdg_cur
  ( b_afdg_id IN NUMBER
  )
  IS
    SELECT omschrijving
    FROM   kgc_afdelingen afdg
    WHERE  afdg_id = b_afdg_id
    ;
  afdg_rec afdg_cur%rowtype;
BEGIN
  OPEN  rela_cur;
  FETCH rela_cur
  INTO  rela_rec;
  CLOSE rela_cur;
  tmp_adres := UPPER(rela_rec.adres);
  tmp_adres := LTRIM(RTRIM(rela_rec.adres));
  tmp_adres := REPLACE(rela_rec.adres,CHR(10),NULL);
  IF ( tmp_adres IS NULL )
  THEN
    v_adres := kgc_adres_00.instelling
               ( p_inst_id => rela_rec.inst_id
               , p_met_naam => 'J'
               );
  ELSE
    v_adres := kgc_formaat_00.volledig_adres
               ( p_naam       => NULL -- komt later
               , p_adres      => rela_rec.adres
               , p_postcode   => rela_rec.postcode
               , p_woonplaats => rela_rec.woonplaats
               , p_land       => rela_rec.land
               );
  END IF;
  -- eventueel afdeling toevoegen
  IF ( rela_rec.afdg_id IS NOT NULL )
  THEN
    OPEN  afdg_cur( rela_rec.afdg_id );
    FETCH afdg_cur
    INTO  afdg_rec;
    CLOSE afdg_cur;
    v_adres := afdg_rec.omschrijving||CHR(10)||v_adres;
  END IF;
  -- naam en interne post
  IF ( p_met_naam = 'J' )
  THEN
    IF rela_rec.interne_post IS NOT NULL
    THEN
      v_adres := rela_rec.interne_post||CHR(10)||v_adres;
    END IF;
    IF rela_rec.aanspreken IS NOT NULL
    THEN
      v_adres := rela_rec.aanspreken  ||CHR(10)||v_adres;
    END IF;
  END IF;

  RETURN( strip( v_adres ) );
END relatie;

FUNCTION instelling
( p_inst_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_adres VARCHAR2(2000);
  v_naam VARCHAR2(100);
  CURSOR inst_cur
  IS
    SELECT inst.naam
    ,      inst.adres
    ,      inst.postcode
    ,      inst.woonplaats
    ,      inst.land
    FROM   kgc_instellingen inst
    WHERE  inst.inst_id = p_inst_id
    ;
  inst_rec inst_cur%rowtype;
BEGIN
  OPEN  inst_cur;
  FETCH inst_cur
  INTO  inst_rec;
  CLOSE inst_cur;
  IF ( p_met_naam = 'J' )
  THEN
    v_naam := inst_rec.naam;
  ELSE
    v_naam := NULL;
  END IF;
  v_adres := kgc_formaat_00.volledig_adres
             ( p_naam => v_naam
             , p_adres => inst_rec.adres
             , p_postcode => inst_rec.postcode
             , p_woonplaats => inst_rec.woonplaats
             , p_land => inst_rec.land
             );
  RETURN( strip( v_adres ) );
END instelling;

FUNCTION verzekeraar
( p_verz_id IN NUMBER
, p_met_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_adres VARCHAR2(2000);
  v_naam VARCHAR2(100);
  CURSOR verz_cur
  IS
    SELECT verz.naam
    ,      verz.adres
    ,      verz.postcode
    ,      verz.woonplaats
    ,      verz.land
    FROM   kgc_verzekeraars verz
    WHERE  verz.verz_id = p_verz_id
    ;
  verz_rec verz_cur%rowtype;
BEGIN
  OPEN  verz_cur;
  FETCH verz_cur
  INTO  verz_rec;
  CLOSE verz_cur;
  IF ( p_met_naam = 'J' )
  THEN
    v_naam := verz_rec.naam;
  ELSE
    v_naam := NULL;
  END IF;
  v_adres := kgc_formaat_00.volledig_adres
             ( p_naam => v_naam
             , p_adres => verz_rec.adres
             , p_postcode => verz_rec.postcode
             , p_woonplaats => verz_rec.woonplaats
             , p_land => verz_rec.land
             );
  RETURN( strip( v_adres ) );
END verzekeraar;

END kgc_adres_00;
/

/
QUIT
