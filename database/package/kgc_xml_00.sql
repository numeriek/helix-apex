CREATE OR REPLACE PACKAGE "HELIX"."KGC_XML_00" IS

  SUBTYPE t_xml_regel IS VARCHAR2 (32767);

  TYPE t_kolom_waarden IS TABLE OF VARCHAR2 (4000)
    INDEX BY VARCHAR2 (30);                                      -- kolomnaam

  --  collection van XML regels
  TYPE t_xml_regel_collection IS TABLE OF t_xml_regel
    INDEX BY BINARY_INTEGER;

  -- collection van XML-documenten
  TYPE t_xml_doc_collection IS TABLE OF CLOB
    INDEX BY BINARY_INTEGER;

  FUNCTION naar_xml (
    p_kolom_waarden   IN   t_kolom_waarden
   ,p_groep           IN   VARCHAR2 := NULL
   ,p_prefix          IN   VARCHAR2 := NULL
  )
    RETURN t_xml_regel;

  PROCEDURE xml_naar_bestand (
    p_dir    IN   VARCHAR2
   ,p_file   IN   VARCHAR2
   ,p_clob   IN   CLOB
  );

  FUNCTION cr_naar_tags (
    p_str   IN   VARCHAR2
  )
    RETURN VARCHAR2;

  FUNCTION nieuwe_xml (
    p_naam   IN   VARCHAR2
  )
    RETURN CLOB;

  PROCEDURE voegtoe_xml_regel (
    p_xml_clob    IN OUT   CLOB
   ,p_tag         IN       VARCHAR2 DEFAULT NULL
   ,p_xml_regel   IN       t_xml_regel
  );

  PROCEDURE voegtoe_xml_regels (
    p_xml_clob               IN OUT   CLOB
   ,p_xml_regel_collection   IN       t_xml_regel_collection
  );

  PROCEDURE sluit_xml (
    p_xml_clob   IN OUT   CLOB
   ,p_naam       IN       VARCHAR2
  );

  PROCEDURE voegtoe_xml_doc (
    p_xml_doc   IN   CLOB
  );

  FUNCTION get_xml_doc_collection
    RETURN kgc_xml_00.t_xml_doc_collection;

  PROCEDURE init;

  FUNCTION neem_letterlijk (
    p_string   VARCHAR2
  )
    RETURN VARCHAR2;

  FUNCTION get_xml (
    p_vnr IN number
  , p_amount IN OUT binary_integer
  , p_offset IN integer
  )
   RETURN VARCHAR2;
END KGC_XML_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_XML_00" IS

  g_nl          CONSTANT VARCHAR2 (2)                    := chr (10);
  -- global collection, om gebruik icm Dynamic pl/sql gemakkelijker te maken (bind variabelen
  -- werken alleen met scalar types)
  g_xml_doc_collection   kgc_xml_00.t_xml_doc_collection;

/******************************************************************************
   NAME:       KGC_XML_00
   PURPOSE     ondersteunen van XML generatie

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/31/2006   HZO              Creatie
   1.1        24-07-2006  YAR              Functie neem_letterlijk aangepast
******************************************************************************/

  /******************************************************************************
     NAME:       init
     PURPOSE:    initialiseer de XML generatie

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        6-4-2006    HZO             Creatie

     NOTES:

  *****************************************************************************/
  PROCEDURE init
  IS
  BEGIN
    -- verwijder eventueel aanwezige elementen uit de collection
    g_xml_doc_collection.DELETE;
  END init;

  /******************************************************************************
     NAME:       neem_letterlijk
     PURPOSE:    Neem een string letterlijk: de string wordt door de parser genegeerd
                 Dit is nodig ivm tekens die vallen buiten a..z en 0..9


     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        13-06-2006  HZO              Creatie
     1.1        24-07-2006  YAR              Nieuwe code aangeleverd door Rob Debets
     1.2        10-08-2006  YAR              als p_string leeg is, moet NULL returned worden
     NOTES:

  *****************************************************************************/
  FUNCTION neem_letterlijk (
    p_string   VARCHAR2
  )
    RETURN VARCHAR2
  IS
    v_string varchar2(32767);
    v_char   varchar2(10);
  BEGIN
    if p_string is null then
      return null;
    end if;

    for i in 1..length(p_string) loop
      v_char := asciistr(substr(p_string, i, 1));
      -- bovenstaande levert het ascii-character (bv a) of de unicode-waarde igv non-ascii (bv \20AC voor euro-teken)
      if length(v_char) = 1 then
        if v_char = '&' then
          v_char := '&amp;';
        elsif v_char = '>' then
          v_char := '&gt;';
        elsif v_char = '<' then
          v_char := '&lt;';
        elsif v_char = '''' then
          v_char := '&apos;';
        elsif v_char = '"' then
          v_char := '&quot;';
        end if;
        v_string := v_string || v_char;
      else -- bepaal unicode waarde van deze char
        v_string := v_string || '&#x' || substr(v_char, 2) || ';';
      end if;
    end loop;
    return v_string;
  END neem_letterlijk;


  /******************************************************************************
     NAME:       naar_XML
     PURPOSE:    kolomwaarden omzetten naar XML regel

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        6-4-2006    HZO             Creatie
     1.1        13-06-2006  HZO             Maak gebruik van neem_letterlijk

     NOTES:

  *****************************************************************************/
  FUNCTION naar_xml (
    p_kolom_waarden   IN   t_kolom_waarden
   ,p_groep           IN   VARCHAR2 := NULL
   ,p_prefix          IN   VARCHAR2 := NULL
  )
    RETURN t_xml_regel
  IS
    v_xml_regel   t_xml_regel;
    v_colname     VARCHAR2 (100);
    v_tag         VARCHAR2 (100);
  BEGIN
    IF p_kolom_waarden.COUNT = 0
    THEN
      RETURN '';
    END IF;

    IF p_groep IS NOT NULL
    THEN
      v_xml_regel := v_xml_regel || '<' || lower (p_groep) || '>';
    END IF;

    v_colname := p_kolom_waarden.FIRST;

    FOR i IN 1 .. p_kolom_waarden.COUNT
    LOOP
      v_tag := lower (v_colname);

      IF p_prefix IS NOT NULL
      THEN
        v_tag := lower (p_prefix) || '_' || v_tag;
      END IF;

      IF nvl (LENGTH (v_xml_regel), 0) > 0
      THEN
        v_xml_regel := v_xml_regel || chr (10);
      END IF;

      v_xml_regel :=    v_xml_regel
                     || '<'
                     || v_tag
                     || '>'
                     || neem_letterlijk (p_kolom_waarden (v_colname))
                     || '</'
                     || v_tag
                     || '>';
      v_colname := p_kolom_waarden.NEXT (v_colname);
    END LOOP;

    IF p_groep IS NOT NULL
    THEN
      v_xml_regel := v_xml_regel || g_nl || '</' || lower (p_groep) || '>';
    END IF;

    RETURN v_xml_regel || g_nl;
  END naar_xml;

/******************************************************************************
   NAME:       cr_naar_tags
   PURPOSE:    Vervang gegevens met carriage returns (adressen) door XML tags
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        6-4-2006    HZO              Creatie
   1.1        15-08-2006  YAR              Aanpassing nav gewenste aanpassingen Rob Debets 14-08-2006
   NOTES:

*****************************************************************************/
  FUNCTION cr_naar_tags (
    p_str   IN   VARCHAR2
  )
    RETURN VARCHAR2
  IS
    v_str   t_xml_regel := p_str;
    v_ret   t_xml_regel;
    v_pos   NUMBER;
  BEGIN
    v_pos := INSTR(v_str, chr(10));
    WHILE v_pos > 0 LOOP
      v_ret := v_ret || '<r>' || neem_letterlijk(SUBSTR(v_str, 1, v_pos - 1)) || '</r>';
      v_str := SUBSTR(v_str, v_pos + 1);
      v_pos := INSTR(v_str, chr(10));
    END LOOP;
    v_ret := v_ret || '<r>' || neem_letterlijk(v_str) || '</r>';
    RETURN v_ret;
  END cr_naar_tags;


   /******************************************************************************
     NAME:       XML_naar_bestand
     PURPOSE:    Schrijft de XML in CLOB formaat naar een bestand

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        6-4-2006    HZO             Creatie
     1.1        31-07-2006  YAR              CLOB kan niet vanuit Forms meegegeven worden
     NOTES:

  *****************************************************************************/
  PROCEDURE xml_naar_bestand (
    p_dir    IN   VARCHAR2
   ,p_file   IN   VARCHAR2
   ,p_clob   IN   CLOB
  )
  IS
    v_output        utl_file.file_type;
    v_hoeveelheid   NUMBER             := 32000;
    v_overgezet     NUMBER             := 1;
    v_lengte_clob   NUMBER            := nvl (dbms_lob.getlength (p_clob), 0);
    v_chunk         VARCHAR2 (32760);
    v_stap          VARCHAR2 (100);
    v_xml_doc     kgc_xml_00.t_xml_doc_collection;
  BEGIN
    -- UTL file kan maximaal 32k per keer schrijven , de CLOB kan groter
    -- zijn. Werk daarom met chunks van 32 k
    v_stap := 'open bestand ';
    v_output := utl_file.fopen (LOCATION         => p_dir
                               ,filename         => p_file
                               ,open_mode        => 'W'
                               ,max_linesize     => 32760
                               );

    IF p_clob IS NULL
    THEN
      v_xml_doc     := kgc_xml_00.get_xml_doc_collection;
      v_lengte_clob := nvl (dbms_lob.getlength (p_clob), 0);
    END IF;

    -- moet groter zijn dan 32000, anders foutmelding
    WHILE (v_overgezet < v_lengte_clob)
    LOOP
      v_stap := ' lees clob ';
      dbms_lob.READ (lob_loc     => p_clob
                    ,amount      => v_hoeveelheid
                    ,offset      => v_overgezet
                    ,buffer      => v_chunk
                    );
      utl_file.put (v_output, v_chunk);
      utl_file.fflush (v_output);
      utl_file.new_line (v_output);
      v_overgezet := v_overgezet + v_hoeveelheid;
    END LOOP;

    utl_file.fclose (v_output);
  EXCEPTION
    WHEN OTHERS
    THEN
      utl_file.fclose (v_output);
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_XML_00.xml_naar_bestand'
                              ,p_param2     => sqlerrm
                              );
  END xml_naar_bestand;

/******************************************************************************
   NAME:       Bepaal_encoding
   PURPOSE:    Bepaal de te gebruiken encoding op basis van de character set
               van de database. UTL_file gebruikt deze character set om te bepalen
               welke encoding voor het XML document gebruikt moet worden.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        13-06-2006    HZO             Creatie

   NOTES:

*****************************************************************************/
  FUNCTION bepaal_encoding
    RETURN VARCHAR2
  IS
    CURSOR c_charset
    IS
      SELECT VALUE
        FROM v$nls_parameters
       WHERE parameter = 'NLS_CHARACTERSET';

    v_encoding   VARCHAR2 (100);
    v_stap       VARCHAR2 (100);
  BEGIN
    v_stap := 'open cursor ';

    OPEN c_charset;

    FETCH c_charset
     INTO v_encoding;

    CLOSE c_charset;

    v_stap := 'bepaal encoding  ';

    IF instr (v_encoding, 'UTF16') != 0
    THEN
      v_encoding := 'UTF-16';
    ELSIF instr (v_encoding, 'UTF8') != 0
    THEN
      v_encoding := 'UTF-8';
    ELSIF instr (v_encoding, '1252') != 0
    THEN
      v_encoding := 'windows-1252';
    ELSE
      -- gebruik in overige gevallen Latin-1
      v_encoding := 'ISO-8859-1';
    END IF;

    RETURN v_encoding;
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_XML_00.bepaal_encoding'
                              ,p_param2     => sqlerrm
                              );
  END bepaal_encoding;

/******************************************************************************
   NAME:       Nieuwe_XML
   PURPOSE:    Maak een nieuwe XML-defnitie aan
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        6-4-2006    HZO             Creatie
   1.1       13-06-2006  HZO             Encoding middels functie

   NOTES:

*****************************************************************************/
  FUNCTION nieuwe_xml (
    p_naam   IN   VARCHAR2
  )
    RETURN CLOB
  IS
    v_xml_clob   CLOB;
    v_stap       VARCHAR2 (100);
  BEGIN
    v_stap := 'createtemporary ';
    dbms_lob.createtemporary (v_xml_clob, FALSE);
    -- maak de heading
    v_stap := ' maak heading ';
    voegtoe_xml_regel (p_xml_clob      => v_xml_clob
                      ,p_xml_regel     =>    '<?xml version="1.0" encoding="'
                                          || bepaal_encoding
                                          || '"  standalone="no"?>'
                                          || g_nl);
    voegtoe_xml_regel
                     (p_xml_clob      => v_xml_clob
                     ,p_xml_regel     =>    '<'
                                         || p_naam
                                         || ' xmlns="Helix" xml:space="preserve">'
                                         || g_nl);
    RETURN v_xml_clob;
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_XML_00.nieuwe_xml'
                              ,p_param2     => sqlerrm
                              );
  END nieuwe_xml;

/******************************************************************************
   NAME:       Sluit_xml
   PURPOSE:    Sluit de XML-defnitie defintie
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        6-4-2006    HZO             Creatie

   NOTES:

 *****************************************************************************/
  PROCEDURE sluit_xml (
    p_xml_clob   IN OUT   CLOB
   ,p_naam       IN       VARCHAR2
  )
  IS
  BEGIN
    voegtoe_xml_regel (p_xml_clob      => p_xml_clob
                      ,p_xml_regel     => '</' || p_naam || '>');
  END sluit_xml;

/******************************************************************************
   NAME:       voegtoe_xml_regel
   PURPOSE:    Voeg een regel toe aan aan de de XML definitie

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        6-4-2006    HZO             Creatie
   1.1        13-06-2006  HZO             Maak gebruik van neem_letterlijk

   NOTES:

*****************************************************************************/
  PROCEDURE voegtoe_xml_regel (
    p_xml_clob    IN OUT   CLOB
   ,p_tag         IN       VARCHAR2 DEFAULT NULL
   ,p_xml_regel   IN       t_xml_regel
  )
  IS
    v_stap        VARCHAR2 (100);
    v_xml_regel   t_xml_regel;
  BEGIN
    v_stap := 'v_xml_regel';

    IF p_tag IS NOT NULL
    THEN
      v_xml_regel := '<' || p_tag || '>' || p_xml_regel;
    ELSE
      v_xml_regel := p_xml_regel;
    END IF;

    IF p_tag IS NOT NULL
    THEN
      v_xml_regel := v_xml_regel || '</' || p_tag || '>' || g_nl;
    END IF;

    v_stap := 'write apppend ';
    dbms_lob.writeappend (p_xml_clob
                         ,LENGTH (v_xml_regel)
                         ,v_xml_regel
                         );
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_XML_00.voegtoe_xml_regel'
                              ,p_param2     => sqlerrm
                              );
  END voegtoe_xml_regel;

/******************************************************************************
   NAME:       voegtoe_xml_regels
   PURPOSE:    Voeg een collection met XML regels toe aan de de XML definitie

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10-4-2006   HZO              Creatie

   NOTES:

*****************************************************************************/
  PROCEDURE voegtoe_xml_regels (
    p_xml_clob               IN OUT   CLOB
   ,p_xml_regel_collection   IN       t_xml_regel_collection
  )
  IS
    v_stap   VARCHAR2 (100);
  BEGIN
    v_stap := 'door loop collection ';

    FOR i IN 1 .. p_xml_regel_collection.COUNT
    LOOP
      dbms_lob.writeappend (p_xml_clob
                           ,LENGTH (p_xml_regel_collection (i))
                           , p_xml_regel_collection (i) || g_nl
                           );
    END LOOP;
  EXCEPTION
    WHEN OTHERS
    THEN
      qms$errors.show_message (p_mesg       => 'KGC-00213'
                              ,p_param0     => v_stap
                              ,p_param1     => 'KGC_XML_00.voegtoe_xml_regels'
                              ,p_param2     => sqlerrm
                              );
  END voegtoe_xml_regels;

/******************************************************************************
   NAME:       voegtoe_xml_doc
   PURPOSE:    Voeg een een xml document toe aan een collection van xml documenten

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10-4-2006   HZO              Creatie

   NOTES:

*****************************************************************************/
  PROCEDURE voegtoe_xml_doc (
    p_xml_doc   IN   CLOB
  )
  IS
  BEGIN
    g_xml_doc_collection (g_xml_doc_collection.COUNT + 1) := p_xml_doc;
  END;

/******************************************************************************
     NAME:       get_xml_doc_collection
     PURPOSE:    Return de global collection

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1.0        6-4-2006    HZO              Creatie

     NOTES:

  *****************************************************************************/
  FUNCTION get_xml_doc_collection
    RETURN kgc_xml_00.t_xml_doc_collection
  IS
  BEGIN
    RETURN g_xml_doc_collection;
  END get_xml_doc_collection;


/******************************************************************************
   NAME:       get_xml
   PURPOSE:    een stuk van CLOB ophalen en teruggeven

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        13-09-2006  YAR iov RD       Creatie

   NOTES:

*****************************************************************************/
  FUNCTION get_xml
  ( p_vnr IN number
  , p_amount IN OUT binary_integer
  , p_offset IN integer
  ) return varchar2 is
    v_buffer varchar2(32767);
    v_pos number;
    v_stap   VARCHAR2 (100);

  BEGIN
    v_stap := 'haal gedeelte clob bestand op ';
    dbms_lob.read(g_xml_doc_collection(p_vnr), p_amount, p_offset, v_buffer);

    v_buffer := rtrim(v_buffer);
    v_pos := INSTR(v_buffer, '>', -1);

    if v_pos > 0
    then
      if v_pos < nvl(length(v_buffer), 0)
      then -- ah: laatste is niet >? dan inkorten, anders geen correcte xml!
        v_buffer := substr(v_buffer, 1, v_pos);
      end if;
    end if;

    p_amount := nvl(length(v_buffer), 0);

    return v_buffer;

  EXCEPTION
    WHEN OTHERS
    THEN
      return null;
  END;
END kgc_xml_00;
/

/
QUIT
