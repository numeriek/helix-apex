CREATE OR REPLACE PACKAGE "HELIX"."BAS_RESE_00" IS

-- wat is de voorraad-mutatie voor een (stoftest in een) protocol
FUNCTION voorraad_mutatie
( p_stgr_id IN NUMBER
, p_stof_id in number := null
)
RETURN NUMBER;

-- hoeveel actuele voorraad is er
FUNCTION voorraad
( p_frac_id IN NUMBER
, p_cuty_id IN NUMBER
)
RETURN NUMBER;

-- een stoftest wordt aangemeld...
PROCEDURE aangemeld
( p_onmo_id IN NUMBER
, p_frac_id IN NUMBER
, p_stgr_id IN NUMBER := null
, p_stof_id IN NUMBER := null
, p_cuty_id IN NUMBER
);

END BAS_RESE_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_RESE_00" IS
FUNCTION voorraad_mutatie
( p_stgr_id IN NUMBER
, p_stof_id IN NUMBER := NULL
)
RETURN NUMBER
IS
  v_hoeveelheid NUMBER;
  CURSOR prst_cur
  IS
    SELECT SUM( NVL(prst.voorraad_mutatie,0) ) voorraad_mutatie
    FROM   kgc_protocol_stoftesten prst
    WHERE  prst.stgr_id = p_stgr_id
    AND  ( prst.stof_id = p_stof_id OR p_stof_id IS NULL )
    ;
BEGIN
  OPEN  prst_cur;
  FETCH prst_cur
  INTO  v_hoeveelheid;
  CLOSE prst_cur;
  RETURN( NVL( v_hoeveelheid, 0 ) );
END voorraad_mutatie;

FUNCTION voorraad
( p_frac_id IN NUMBER
, p_cuty_id IN NUMBER
)
RETURN NUMBER
IS
  -- oorspronkelijke voorraad
  CURSOR voor_cur
  IS
    SELECT SUM( hoeveelheid )
    FROM   bas_voorraad
    WHERE  frac_id = p_frac_id
    AND    cuty_id = p_cuty_id
    ;
  v_voorraad NUMBER;
  -- al gereserveerde hoeveelheid
  CURSOR rese_cur
  IS
    SELECT SUM( rese.hoeveelheid )
    FROM   bas_reserveringen rese
    ,      bas_meetwaarden meet
    ,      bas_metingen meti
    WHERE  rese.meet_id = meet.meet_id
    AND    meet.meti_id = meti.meti_id
    AND    meti.frac_id = p_frac_id
    AND    rese.cuty_id = p_cuty_id
    ;
  v_gereserveerd NUMBER;
BEGIN
  OPEN  voor_cur;
  FETCH voor_cur
  INTO  v_voorraad;
  CLOSE voor_cur;
  IF ( v_voorraad IS NOT NULL )
  THEN
    OPEN  rese_cur;
    FETCH rese_cur
    INTO  v_gereserveerd;
    CLOSE rese_cur;
  END IF;
  RETURN( v_voorraad - NVL( v_gereserveerd, 0 ) );
END voorraad;

PROCEDURE aangemeld
( p_onmo_id IN NUMBER
, p_frac_id IN NUMBER
, p_stgr_id IN NUMBER := null
, p_stof_id IN NUMBER := null
, p_cuty_id IN NUMBER
)
is
  cursor meet_cur
  is
    select meet.meet_id
    ,      meti.stgr_id
    ,      meet.stof_id
    from   bas_meetwaarden meet
    ,      bas_metingen meti
    where  meet.meti_id = meti.meti_id
    and    meet.afgerond = 'N'
    and    meti.afgerond = 'N'
    and    meti.onmo_id = p_onmo_id
    and    meti.frac_id = p_frac_id
    and  ( meti.stgr_id = p_stgr_id or p_stgr_id is null )
    and  ( meet.stof_id = p_stof_id or p_stof_id is null )
    ;

  cursor rese_cur
  ( b_meet_id in number
  )
  is
    select rese_id
    from   bas_reserveringen
    where  meet_id = b_meet_id
    and    cuty_id = p_cuty_id
    ;
  v_rese_id number;

  v_hoeveelheid number;
begin
  if ( p_stgr_id is not null
    or p_stof_id is not null
     )
  then
    for r in meet_cur
    loop
      v_hoeveelheid := voorraad_mutatie
                       ( p_stgr_id => r.stgr_id
                       , p_stof_id => r.stof_id
                       );
      if ( voorraad( p_frac_id, p_cuty_id ) is null )
      then
        null;
      elsif ( voorraad( p_frac_id, p_cuty_id ) - v_hoeveelheid >= 0 )
      then
        v_rese_id := null;
        open  rese_cur( r.meet_id );
        fetch rese_cur
        into  v_rese_id;
        close rese_cur;
        if ( v_rese_id is not null )
        then
          update bas_reserveringen
          set    hoeveelheid = nvl( hoeveelheid, 0 ) + v_hoeveelheid
          ,      datum_reservering = trunc(SYSDATE)
          where  rese_id = v_rese_id;
        else
          insert into bas_reserveringen
          ( meet_id
          , cuty_id
          , hoeveelheid
          , datum_reservering
          )
          values
          ( r.meet_id
          , p_cuty_id
          , v_hoeveelheid
          , trunc(SYSDATE)
          );
        end if;
      else
        raise_application_error( -20000, 'Er is niet voldoende voorraad voor de aanmelding' );
      end if;
    end loop;
  end if;
end aangemeld;
END bas_rese_00;
/

/
QUIT
