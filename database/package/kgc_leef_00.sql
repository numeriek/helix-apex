CREATE OR REPLACE PACKAGE "HELIX"."KGC_LEEF_00" IS
   --
   -- Bepaal leeftijdscategorie van een monster
   -- d.i. een categorie waarbij de afname_datum - de geboortedatum
   -- binnen de grenzen van een leeftijdscategorie valt.
   -- (in geval van overlap: het verschil tussen de grenzen zo klein mogelijk)
   FUNCTION leeftijdscategorie(p_kafd_id      IN NUMBER
                              ,p_aantal_dagen IN NUMBER) RETURN NUMBER;
   PRAGMA RESTRICT_REFERENCES(leeftijdscategorie
                             ,WNDS
                             ,WNPS
                             ,RNPS);
   --
   -- omschrijf de leeftijd van het monster
   FUNCTION omschrijving(p_dagen          IN NUMBER := NULL
                        ,p_mons_id        IN NUMBER := NULL
                        ,p_datum_afname   IN DATE := NULL
                        ,p_geboortedatum  IN DATE := NULL
                        ,p_datum_aterm    IN DATE := NULL -- ) RETURN VARCHAR2;
                        ,p_incl_voorloop0 IN VARCHAR2 -- 972 rework
                         ) RETURN VARCHAR2;
   PRAGMA RESTRICT_REFERENCES(omschrijving
                             ,WNDS
                             ,WNPS
                             ,RNPS);
   --
   -- omschrijf de leeftijd van het monster
   FUNCTION omschrijving(p_dagen         IN NUMBER := NULL
                        ,p_mons_id       IN NUMBER := NULL
                        ,p_datum_afname  IN DATE := NULL
                        ,p_geboortedatum IN DATE := NULL
                        ,p_datum_aterm   IN DATE := NULL) RETURN VARCHAR2;
   PRAGMA RESTRICT_REFERENCES(omschrijving
                             ,WNDS
                             ,WNPS
                             ,RNPS);
   --
   -- verkorte omschrijving van de leeftijd van het monster -- 972
   -- tekst inkorten tbv. bijv. rapport BASMEET53
   FUNCTION omschrijving_verkort(p_dagen         IN NUMBER := NULL
                                ,p_mons_id       IN NUMBER := NULL
                                ,p_datum_afname  IN DATE := NULL
                                ,p_geboortedatum IN DATE := NULL
                                ,p_datum_aterm   IN DATE := NULL) RETURN VARCHAR2;
   PRAGMA RESTRICT_REFERENCES(omschrijving_verkort
                             ,WNDS
                             ,WNPS
                             ,RNPS);
   --
   -- leeftijd van persoon bij monsterafname in dagen
   -- of aantal dagen tot aterme datum bij foetus (negatief)
   FUNCTION leeftijd(p_pers_id       IN NUMBER
                    ,p_foet_id       IN NUMBER := NULL
                    ,p_datum_afname  IN DATE := NULL
                    ,p_geboortedatum IN DATE := NULL
                    ,p_datum_aterm   IN DATE := NULL) RETURN NUMBER;
   --
END KGC_LEEF_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_LEEF_00" IS
   --
   FUNCTION leeftijdscategorie(p_kafd_id      IN NUMBER
                              ,p_aantal_dagen IN NUMBER) RETURN NUMBER IS

      CURSOR leef_cur IS
         SELECT leef_id
               ,(bovengrens - ondergrens) verschil
           FROM kgc_leeftijden
          WHERE kafd_id = p_kafd_id
            AND p_aantal_dagen BETWEEN ondergrens AND bovengrens
            AND vervallen = 'N'
          ORDER BY 2;

      leef_rec leef_cur%ROWTYPE;

      v_return NUMBER;

   BEGIN
      IF (p_aantal_dagen IS NOT NULL)
      THEN
         OPEN leef_cur;
         FETCH leef_cur
            INTO leef_rec;
         CLOSE leef_cur;
         v_return := leef_rec.leef_id;
      END IF;

      RETURN(v_return);
   END leeftijdscategorie;
   --
   FUNCTION omschrijving(p_dagen          IN NUMBER   := NULL
                        ,p_mons_id        IN NUMBER   := NULL
                        ,p_datum_afname   IN DATE     := NULL
                        ,p_geboortedatum  IN DATE     := NULL
                        ,p_datum_aterm    IN DATE     := NULL
                        ,p_incl_voorloop0 IN VARCHAR2 -- 972 rework
                        ) RETURN VARCHAR2 IS

      CURSOR mons_cur IS
         SELECT nvl(mons.datum_afname
                   ,mons.datum_aanmelding) datum_afname
               ,mons.leeftijd
               ,pers.geboortedatum
               ,mons.foet_id
           FROM kgc_personen pers
               ,kgc_monsters mons
          WHERE mons.pers_id = pers.pers_id
            AND mons.mons_id = p_mons_id;

      mons_rec mons_cur%ROWTYPE;

      CURSOR foet_cur(b_foet_id IN NUMBER) IS
         SELECT nvl(zwan.datum_aterm
                   ,foet.geplande_geboortedatum) datum_aterm
           FROM kgc_zwangerschappen zwan
               ,kgc_foetussen       foet
          WHERE foet.zwan_id = zwan.zwan_id(+)
            AND foet.foet_id = b_foet_id;

      foet_rec foet_cur%ROWTYPE;

      v_datum_afname  DATE := p_datum_afname;
      v_datum_aterm   DATE := p_datum_aterm;
      v_geboortedatum DATE := p_geboortedatum;
      v_dag   NUMBER;
      v_week  NUMBER;
      v_mnd   NUMBER;
      v_jaar  NUMBER;
      vc_dag  VARCHAR2(6); -- 972 rework
      vc_week VARCHAR2(6); -- 972 rework, kan negatief zijn bij foute berekening
      vc_mnd  VARCHAR2(6); -- 972 rework
      vc_jaar VARCHAR2(6); -- 972 rework
      v_oms   VARCHAR2(100);
      v_zwan  BOOLEAN := FALSE;

   BEGIN
      IF (   v_datum_afname IS NOT NULL
         AND (  v_geboortedatum IS NOT NULL
             OR v_datum_aterm IS NOT NULL))
      THEN
         IF (v_datum_aterm IS NOT NULL)
         THEN
            v_zwan := TRUE;
            v_week := trunc((280 + v_datum_afname - v_datum_aterm) / 7);
            v_dag  := trunc(MOD((280 + (v_datum_afname - v_datum_aterm))
                               ,7));
         ELSIF (v_geboortedatum IS NOT NULL)
         THEN
            v_mnd  := trunc(months_between(v_datum_afname
                                          ,v_geboortedatum));
            v_jaar := trunc(v_mnd / 12);
            v_dag  := trunc(v_datum_afname - add_months(v_geboortedatum
                                                       ,v_mnd));
            v_mnd  := trunc(MOD(v_mnd
                               ,12));
         END IF;

      ELSIF (p_mons_id IS NOT NULL)
      THEN
         OPEN mons_cur;
         FETCH mons_cur
            INTO mons_rec;
         CLOSE mons_cur;

         IF (mons_rec.foet_id IS NOT NULL)
         THEN
            OPEN foet_cur(mons_rec.foet_id);
            FETCH foet_cur
               INTO foet_rec;
            CLOSE foet_cur;
         END IF;

         v_datum_afname  := nvl(p_datum_afname
                               ,mons_rec.datum_afname);
         v_datum_aterm   := nvl(p_datum_aterm
                               ,foet_rec.datum_aterm);
         v_geboortedatum := nvl(p_geboortedatum
                               ,mons_rec.geboortedatum);

         IF (v_datum_afname IS NOT NULL)
         THEN
            IF (v_datum_aterm IS NOT NULL)
            THEN
               v_zwan := TRUE;
               v_week := trunc((280 + v_datum_afname - v_datum_aterm) / 7);
               v_dag  := trunc(MOD((280 + (v_datum_afname - v_datum_aterm))
                                  ,7));
            ELSIF (v_geboortedatum IS NOT NULL)
            THEN
               v_mnd  := trunc(months_between(v_datum_afname
                                             ,v_geboortedatum));
               v_jaar := trunc(v_mnd / 12);
               v_dag  := trunc(v_datum_afname - add_months(v_geboortedatum
                                                          ,v_mnd));
               v_mnd  := trunc(MOD(v_mnd
                                  ,12));
            END IF;
         ELSIF (mons_rec.leeftijd < 0)
         THEN
            v_zwan := TRUE;
            v_week := trunc((280 + mons_rec.leeftijd) / 7);
            v_dag  := trunc(MOD((280 + mons_rec.leeftijd)
                               ,7));
         ELSIF (mons_rec.leeftijd > 0)
         THEN
            v_datum_afname  := trunc(SYSDATE);
            v_geboortedatum := trunc(SYSDATE) - mons_rec.leeftijd;
            v_mnd           := trunc(months_between(v_datum_afname
                                                   ,v_geboortedatum));
            v_jaar          := trunc(v_mnd / 12);
            v_dag           := trunc(v_datum_afname - add_months(v_geboortedatum
                                                                ,v_mnd));
            v_mnd           := trunc(MOD(v_mnd
                                        ,12));
         END IF;

      ELSIF (p_dagen IS NULL)
      THEN
         v_oms := NULL;

      ELSIF (p_dagen < 0)
      THEN
         v_zwan := TRUE;
         v_week := trunc((280 + p_dagen) / 7);
         v_dag  := trunc(MOD((280 + p_dagen)
                            ,7));

      ELSE
         v_jaar := trunc(p_dagen / 365);
         v_mnd  := trunc((p_dagen - (v_jaar * 365)) / 30);
         v_dag  := trunc(p_dagen - (v_mnd * 30) - (v_jaar * 365));
      END IF;

      -- 972 rework: bepaling is gedaan, nu nog formatteren en samenstellen >>>
      IF p_incl_voorloop0 = 'J'
      THEN
         vc_dag  := to_char(v_dag,'FM09'); -- 972 rework
         vc_week := to_char(v_week,'FM09'); -- 972 rework
         vc_mnd  := to_char(v_mnd,'FM09'); -- 972 rework
         vc_jaar := to_char(v_jaar,'FM909'); -- 972 rework
         -- FM909: geen blanks vooraf;
         --        bij <100 2 digits en 1 voorloopnul, indien nodig;
         --        bij >99 3 digits;
         -- dus bijv. '89', '09', '113', '105'
      ELSE
         vc_dag  := to_char(v_dag); -- 972 rework
         vc_week := to_char(v_week); -- 972 rework
         vc_mnd  := to_char(v_mnd); -- 972 rework
         vc_jaar := to_char(v_jaar); -- 972 rework
      END IF;
      -- <<< 972 einde

      IF (v_zwan)
      THEN
         IF (v_week > 0)
         OR (v_week = 0 and p_incl_voorloop0 = 'J') -- 972 rework AT
         THEN
            v_oms := vc_week || ' weken';
         END IF;
         IF (v_dag >= 0)
         THEN
            IF (v_oms IS NULL)
            THEN
               v_oms := vc_dag || ' dag(en)';
            ELSE
               IF (v_dag > 0)
               OR (v_dag = 0 and p_incl_voorloop0 = 'J') -- 972 rework AT
               THEN
                  v_oms := v_oms || ' + ' || vc_dag || ' dag(en)';
               END IF;
            END IF;
         END IF;
         IF (v_oms IS NOT NULL)
         THEN
            v_oms := 'Zwangerschapsduur: ' || v_oms;
         END IF;

      ELSE
         IF (v_jaar > 0)
         OR (v_jaar = 0 and p_incl_voorloop0 = 'J') -- 972 rework AT
         THEN
            v_oms := vc_jaar || ' jaar';
         END IF;
         IF (v_mnd > 0)
         OR (v_mnd = 0 and p_incl_voorloop0 = 'J') -- 972 rework AT
         THEN
            IF (v_oms IS NULL)
            THEN
               v_oms := vc_mnd || ' maand';
            ELSE
               v_oms := v_oms || ', ' || vc_mnd || ' maand';
            END IF;
         END IF;
         IF (v_dag > 0)
         OR (v_dag = 0 and p_incl_voorloop0 = 'J') -- 972 rework AT
         THEN
            IF (v_oms IS NULL)
            THEN
               v_oms := vc_dag || ' dag(en)';
            ELSE
               v_oms := v_oms || ', ' || vc_dag || ' dag(en)';
            END IF;
         END IF;
      END IF;

      RETURN(v_oms);
   END omschrijving;
   --
   FUNCTION omschrijving(p_dagen         IN NUMBER := NULL
                        ,p_mons_id       IN NUMBER := NULL
                        ,p_datum_afname  IN DATE := NULL
                        ,p_geboortedatum IN DATE := NULL
                        ,p_datum_aterm   IN DATE := NULL) RETURN VARCHAR2 IS

      v_oms VARCHAR2(100);

   BEGIN
      v_oms := omschrijving(p_dagen          => p_dagen
                           ,p_mons_id        => p_mons_id
                           ,p_datum_afname   => p_datum_afname
                           ,p_geboortedatum  => p_geboortedatum
                           ,p_datum_aterm    => p_datum_aterm
                           ,p_incl_voorloop0 => 'N'); -- 972 rework

      RETURN(v_oms);
   END omschrijving;
   --
   FUNCTION omschrijving_verkort(p_dagen         IN NUMBER := NULL
                                ,p_mons_id       IN NUMBER := NULL
                                ,p_datum_afname  IN DATE := NULL
                                ,p_geboortedatum IN DATE := NULL
                                ,p_datum_aterm   IN DATE := NULL) RETURN VARCHAR2 IS

      v_oms VARCHAR2(100);

   BEGIN
      v_oms := omschrijving(p_dagen          => p_dagen
                           ,p_mons_id        => p_mons_id
                           ,p_datum_afname   => p_datum_afname
                           ,p_geboortedatum  => p_geboortedatum
                           ,p_datum_aterm    => p_datum_aterm
                           ,p_incl_voorloop0 => 'J'); -- 972 rework

      v_oms := REPLACE(v_oms
                      ,' jaar'
                      ,'j');
      v_oms := REPLACE(v_oms
                      ,' maand'
                      ,'m');
      v_oms := REPLACE(v_oms
                      ,' weken'
                      ,'w');
      v_oms := REPLACE(v_oms
                      ,' dag(en)'
                      ,'d');
      v_oms := REPLACE(v_oms
                      ,'Zwangerschapsduur'
                      ,'Zw');
      v_oms := REPLACE(v_oms
                      ,'+ '
                      ,'');
      v_oms := REPLACE(v_oms
                      ,','
                      ,'');

      RETURN(v_oms);
   END omschrijving_verkort;
   --
   FUNCTION leeftijd(p_pers_id       IN NUMBER
                    ,p_foet_id       IN NUMBER := NULL
                    ,p_datum_afname  IN DATE := NULL
                    ,p_geboortedatum IN DATE := NULL
                    ,p_datum_aterm   IN DATE := NULL) RETURN NUMBER IS

      CURSOR zwan_cur IS
         SELECT nvl(zwan.datum_aterm
                   ,foet.geplande_geboortedatum) datum_aterm
           FROM kgc_zwangerschappen zwan
               ,kgc_foetussen       foet
          WHERE foet.zwan_id = zwan.zwan_id(+)
            AND foet.foet_id = p_foet_id;

      v_aantal_dagen  NUMBER;
      v_datum_afname  DATE := p_datum_afname;
      v_geboortedatum DATE := p_geboortedatum;
      v_datum_aterm   DATE := p_datum_aterm;

   BEGIN

      IF (v_datum_afname IS NULL)
      THEN
         RETURN(NULL); -- v_datum_afname := SYSDATE;
      END IF;

      IF (v_geboortedatum IS NULL)
      THEN
         v_geboortedatum := kgc_pers_00.rekendatum(p_pers_id => p_pers_id);
      END IF;

      IF (v_datum_aterm IS NULL AND p_foet_id IS NOT NULL)
      THEN
         OPEN zwan_cur;
         FETCH zwan_cur
            INTO v_datum_aterm;
         CLOSE zwan_cur;
      END IF;

      IF (p_foet_id IS NOT NULL)
      THEN
         -- negatieve waarde!
         v_aantal_dagen := v_datum_afname - v_datum_aterm;
      ELSE
         v_aantal_dagen := v_datum_afname - v_geboortedatum;
      END IF;

      RETURN(round(v_aantal_dagen
                  ,1));
   END leeftijd;
   --
END kgc_leef_00;
/

/
QUIT
