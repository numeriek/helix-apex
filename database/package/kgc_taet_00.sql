CREATE OR REPLACE PACKAGE "HELIX"."KGC_TAET_00" IS
-- PL/SQL Specification
-- Vul een hulptabel met rijen
-- Deze tabel wordt ge-joined met de hoofdquery
-- (zie report KGCMONS51,BASFRAC51,...) om het juiste aantal etiketten
-- per monster/fractie te krijgen.
-- p_where wordt opgebouwd in BeforeReportTrigger van etiket-rapport
PROCEDURE vul_mons
( p_session_id IN NUMBER
, p_where IN VARCHAR2 := NULL
, p_aantal IN NUMBER := NULL
);
PROCEDURE vul_frac
( p_session_id IN NUMBER
, p_where IN VARCHAR2 := NULL
, p_aantal IN NUMBER := NULL
);
PROCEDURE vul_isol
( p_session_id IN NUMBER
, p_where IN VARCHAR2 := NULL
, p_aantal IN NUMBER := NULL
);
PROCEDURE leeg
( p_session_id IN NUMBER
);
END KGC_TAET_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_TAET_00" IS
PROCEDURE vul_mons
( p_session_id IN NUMBER
, p_where IN VARCHAR2 := NULL
, p_aantal IN NUMBER := NULL
)
IS
  v_statement VARCHAR2(4000);
-- PL/SQL Block
BEGIN
  v_statement := 'declare cursor c is'
     ||CHR(10)|| 'select monv.mons_id,nvl(monv.aantal_buizen,nvl(mate.aantal_etiketten,1)) aantal'
     ||CHR(10)|| 'from kgc_monsters_vw monv, kgc_materialen mate'
     ||CHR(10)|| 'where (monv.mate_id = mate.mate_id)'||p_where
     ||CHR(10)|| ';'
     ||CHR(10)|| 'v_aantal number := '||NVL(TO_CHAR(p_aantal), 'null')||';'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'for r in c loop'
     ||CHR(10)|| 'for i in 1..nvl(v_aantal,r.aantal) loop'
     ||CHR(10)|| 'insert into kgc_tmp_aantal_etiketten'
     ||CHR(10)|| '( session_id,mons_id,volgnr )'
     ||CHR(10)|| 'values( '||TO_CHAR(p_session_id)||',r.mons_id,i );'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'end;'
               ;
  kgc_util_00.dyn_exec( v_statement );
END vul_mons;

PROCEDURE vul_frac
( p_session_id IN NUMBER
, p_where IN VARCHAR2 := NULL
, p_aantal IN NUMBER := NULL
)
IS
  v_statement VARCHAR2(4000);
BEGIN
  v_statement := 'declare cursor c is'
     ||CHR(10)|| 'select monv.mons_id, frac.frac_id, voor.voor_id, nvl(voor.hoeveelheid,1) aantal from kgc_monsters_vw monv, bas_fracties frac, bas_voorraad voor'
     ||CHR(10)|| 'where (monv.mons_id = frac.mons_id and frac.frac_id (+) = voor.frac_id)'||p_where
     ||CHR(10)|| ';'
     ||CHR(10)|| 'v_aantal number := '||NVL(TO_CHAR(p_aantal), 'null')||';'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'for r in c loop'
     ||CHR(10)|| 'for i in 1..nvl(v_aantal,r.aantal) loop'
     ||CHR(10)|| 'insert into kgc_tmp_aantal_etiketten'
     ||CHR(10)|| '( session_id,mons_id,frac_id,voor_id,volgnr )'
     ||CHR(10)|| 'values( '||TO_CHAR(p_session_id)||',r.mons_id,r.frac_id,r.voor_id,i );'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'end;'
               ;
  kgc_util_00.dyn_exec( v_statement );
END vul_frac;

PROCEDURE vul_isol
( p_session_id IN NUMBER
, p_where IN VARCHAR2 := NULL
, p_aantal IN NUMBER := NULL
)
IS
  v_statement VARCHAR2(4000);
BEGIN
  v_statement := 'declare cursor c is'
     ||CHR(10)|| 'select distinct isol.mons_id, isol.frac_id from kgc_kgc_afdelingen kafd, bas_isolatielijsten isol'
     ||CHR(10)|| 'where (kafd.kafd_id = isol.kafd_id) '||p_where
     ||CHR(10)|| ';'
     ||CHR(10)|| 'v_aantal number := '||TO_CHAR(p_aantal)||';'
     ||CHR(10)|| 'begin'
     ||CHR(10)|| 'for r in c loop'
     ||CHR(10)|| 'for i in 1..nvl(v_aantal,1) loop'
     ||CHR(10)|| 'insert into kgc_tmp_aantal_etiketten'
     ||CHR(10)|| '( session_id,mons_id,frac_id,volgnr )'
     ||CHR(10)|| 'values( '||TO_CHAR(p_session_id)||',r.mons_id,r.frac_id,i );'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'end loop;'
     ||CHR(10)|| 'end;'
               ;
  kgc_util_00.dyn_exec( v_statement );
END vul_isol;

PROCEDURE leeg
( p_session_id IN NUMBER
)
IS
BEGIN
  DELETE FROM kgc_tmp_aantal_etiketten
  WHERE  session_id = p_session_id;
EXCEPTION
  WHEN OTHERS
  THEN
    NULL;
END leeg;

END kgc_taet_00;
/

/
QUIT
