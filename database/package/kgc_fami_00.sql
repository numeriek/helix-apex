CREATE OR REPLACE PACKAGE "HELIX"."KGC_FAMI_00" IS
-- haal een familie op bij een onderzoek
FUNCTION familie_bij_onderzoek
( p_onde_id IN NUMBER
, p_incl_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( familie_bij_onderzoek, WNDS, WNPS, RNPS );

-- zie (voorlopige) uitslagbrief DNA:
-- Onze referentie: onderzoeknr / familienummer (verwijst naar papieren status
FUNCTION dna_referentie
( p_onde_id IN NUMBER
, p_onderzoeknr IN VARCHAR2 -- al bekend
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( dna_referentie, WNDS, WNPS, RNPS );

-- heeft familielid een openstaand onderzoek?
-- tbv. sortering van familieoverzicht:
FUNCTION openstaand_onderzoek
( p_fami_id IN NUMBER
, p_pers_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( openstaand_onderzoek, WNDS, WNPS, RNPS );

-- haal de families op waartoe deze persoon behoort (gescheiden door /)
FUNCTION familie
( p_pers_id IN NUMBER
, p_incl_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2;
END KGC_FAMI_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FAMI_00" IS
FUNCTION familie_bij_onderzoek
( p_onde_id IN NUMBER
, p_incl_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(200);
  -- cursoren met identiek recordtype!
  -- betrokkene bij familieonderzoek (laatste)
  CURSOR fobe_cur
  IS
    SELECT fami.familienummer
    ,      fami.naam
    FROM   kgc_families fami
    ,      kgc_familie_onderzoeken faon
    ,      kgc_faon_betrokkenen fobe
    WHERE  fobe.faon_id = faon.faon_id
    AND    faon.fami_id = fami.fami_id
    AND    fobe.onde_id = p_onde_id
    ORDER BY fobe.fobe_id DESC
    ;
  -- lid van familie (laatste)
  CURSOR fale_cur
  IS
    SELECT fami.familienummer
    ,      fami.naam
    FROM   kgc_families fami
    ,      kgc_familie_leden fale
    ,      kgc_onderzoeken onde
    WHERE  fale.fami_id = fami.fami_id
    AND    fale.pers_id = onde.pers_id
    AND    onde.onde_id = p_onde_id
    ORDER BY fami.fami_id DESC
    ;
  fami_rec fobe_cur%rowtype;
-- PL/SQL Block
BEGIN
  OPEN  fobe_cur;
  FETCH fobe_cur
  INTO  fami_rec;
  CLOSE fobe_cur;
  IF ( fami_rec.familienummer IS NULL )
  THEN
    OPEN  fale_cur;
    FETCH fale_cur
    INTO  fami_rec;
    CLOSE fale_cur;
  END IF;
  IF ( p_incl_naam = 'J' )
  THEN
    v_return := fami_rec.familienummer||' '||fami_rec.naam;
  ELSE
    v_return := fami_rec.familienummer;
  END IF;
  RETURN( v_return );
END familie_bij_onderzoek;

FUNCTION dna_referentie
( p_onde_id IN NUMBER
, p_onderzoeknr IN VARCHAR2
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
BEGIN
  v_return := kgc_uits_00.referentie
              ( p_afdeling => null
              , p_onde_id => p_onde_id
              , p_onderzoeknr => null
              );
  RETURN( v_return );
END dna_referentie;

FUNCTION openstaand_onderzoek
( p_fami_id IN NUMBER
, p_pers_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1) := 'N';
  CURSOR c
  IS
    SELECT NULL
    FROM   kgc_onderzoeken onde
    ,      kgc_familie_onderzoeken faon
    ,      kgc_faon_betrokkenen fobe
    WHERE  fobe.faon_id = faon.faon_id
    AND    fobe.onde_id = onde.onde_id
    AND    faon.fami_id = p_fami_id
    AND    fobe.pers_id = p_pers_id
    AND    onde.afgerond = 'N'
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  c;
  FETCH c
  INTO  v_dummy;
  IF ( c%found )
  THEN
    v_return := 'J';
  END IF;
  CLOSE c;
  RETURN( v_return );
END openstaand_onderzoek;

FUNCTION familie
( p_pers_id IN NUMBER
, p_incl_naam IN VARCHAR2 := 'N'
)
RETURN VARCHAR2
is
  v_return varchar2(100);
  CURSOR fale_cur
  ( b_pers_id in number
  )
  IS
    SELECT fami.familienummer
    ,      fami.naam
    FROM   kgc_families fami
    ,      kgc_familie_leden fale
    WHERE  fale.fami_id = fami.fami_id
    AND    fale.pers_id = b_pers_id
    ORDER BY fami.fami_id DESC
    ;
begin
  for fale_rec in fale_cur( p_pers_id )
  loop
  IF ( p_incl_naam = 'J' )
  THEN
    if ( v_return is null )
    then
      v_return := fale_rec.familienummer||' '||fale_rec.naam;
    else
      v_return := v_return || ' / ' || fale_rec.familienummer||' '||fale_rec.naam;
    end if;
  ELSE
    if ( v_return is null )
    then
      v_return := fale_rec.familienummer;
    else
      v_return := v_return || ' / ' || fale_rec.familienummer;
    end if;
  END IF;
  end loop;
  RETURN( v_return );
end familie;

END kgc_fami_00;
/

/
QUIT
