CREATE OR REPLACE PACKAGE "HELIX"."KGC_ONIN_00" IS
-- dwing af dat het is toegestaan indicatiegegevens te wijzigen
PROCEDURE br_onin0001_ier
( p_onde_id IN NUMBER
)
;
END KGC_ONIN_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ONIN_00" IS
PROCEDURE br_onin0001_ier
( p_onde_id IN NUMBER
)
IS
BEGIN
  IF kgc_onde_00.goedgekeurd(p_onde_id => p_onde_id)
  THEN
    DECLARE
      CURSOR onde_cur(b_onde_id IN NUMBER) IS
        SELECT onde.kafd_id
              ,onde.ongr_id
          FROM kgc_onderzoeken onde
         WHERE onde.onde_id = b_onde_id;
      v_kafd_id kgc_onderzoeken.kafd_id%TYPE;
      v_ongr_id kgc_onderzoeken.ongr_id%TYPE;
    BEGIN
      OPEN onde_cur(p_onde_id);
      FETCH onde_cur
        INTO v_kafd_id, v_ongr_id;
      CLOSE onde_cur;
      IF INSTR(UPPER(kgc_sypa_00.standaard_waarde(p_parameter_code => 'BEVRIES_GOEDGEKEURD_ONDERZOEK'
                                                 ,p_kafd_id        => v_kafd_id
                                                 ,p_ongr_id        => v_ongr_id))
              ,'INDICATIE') > 0
      THEN
        qms$errors.show_message(p_mesg  => 'KGC-11290'
                               ,p_errtp => 'E'
                               ,p_rftf  => TRUE);
      END IF;
    END;
  END IF;
END br_onin0001_ier;
END KGC_ONIN_00;
/

/
QUIT
