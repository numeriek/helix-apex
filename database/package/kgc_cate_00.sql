CREATE OR REPLACE PACKAGE "HELIX"."KGC_CATE_00" IS
-- PL/SQL Specification
-- bepaal een (hoofd)categorie
FUNCTION categorie
( p_code IN VARCHAR2
, p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_tabel_naam IN VARCHAR2 := NULL
, p_id IN NUMBER := NULL
)
RETURN NUMBER;


END KGC_CATE_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_CATE_00" IS
-- bepaal een (hoofd)categorie
FUNCTION categorie
( p_code IN VARCHAR2
, p_kafd_id IN NUMBER
, p_ongr_id IN NUMBER := NULL
, p_tabel_naam IN VARCHAR2 := NULL
, p_id IN NUMBER := NULL
)
RETURN NUMBER
IS
  v_return NUMBER;
  v_cate_code kgc_categorieen.code%type;
  CURSOR cate_cur
  IS
    SELECT cate_id
    FROM   kgc_categorieen
    WHERE  code = v_cate_code
    AND  ( kafd_id = p_kafd_id OR kafd_id IS NULL )
    AND  ( ongr_id = p_ongr_id OR ongr_id IS NULL )
    ;
-- PL/SQL Block
BEGIN
  IF ( p_tabel_naam IS NOT NULL
   AND p_id IS NOT NULL
     )
  THEN
    v_cate_code := kgc_attr_00.waarde
                   ( p_tabel_naam => UPPER( p_tabel_naam )
                   , p_code => UPPER( p_code )
                   , p_id => p_id
                   );
  END IF;
  IF ( v_cate_code IS NULL )
  THEN
    v_cate_code := kgc_sypa_00.standaard_waarde
                   ( p_parameter_code => UPPER( p_code )
                   , p_kafd_id        => p_kafd_id
                   , p_ongr_id        => p_ongr_id
                   );
  END IF;
  IF ( v_cate_code IS NOT NULL )
  THEN
    OPEN  cate_cur;
    FETCH cate_cur
    INTO  v_return;
    CLOSE cate_cur;
  END IF;
  RETURN( v_return );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( NULL );
END categorie;

END kgc_cate_00;
/

/
QUIT
