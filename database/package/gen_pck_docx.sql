CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_DOCX"
AS
   /******************************************************************************
      NAME:       GEN_PCK_DOCX
      PURPOSE: Used to create docx documents based on a template and query.
      REVISIONS:
      Date        Author           Description
      ----------  ---------------  ------------------------------------
      23-12-2016  The DOC         Creation
   ******************************************************************************/
   --  FUNCTION f_stripped_subst_str(p_in_string VARCHAR2)
   --      RETURN VARCHAR2;
   FUNCTION f_get_file_from_template(i_report_definition_id IN NUMBER)
      RETURN BLOB;
   PROCEDURE p_download_file_from_template(i_report_definition IN VARCHAR2);
END gen_pck_docx;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_DOCX"
AS

   /******************************************************************************
      NAME:       GEN_PCK_DOCX
      PURPOSE: Used to create docx documents based on a template and query.
      REVISIONS:
      Date        Author           Description
      ----------  ---------------  ------------------------------------
      23-12-2016  The DOC          Creation
   ******************************************************************************/

   g_package  CONSTANT VARCHAR2(31) := $$plsql_unit || '.';
   g_file_type         VARCHAR2(4) := 'word';
   FUNCTION f_clob_to_blob(i_clob IN CLOB)
      RETURN BLOB
   AS
      l_returnvalue    BLOB;
      l_dest_offset    INTEGER := 1;
      l_source_offset  INTEGER := 1;
      l_lang_context   INTEGER := dbms_lob.default_lang_ctx;
      l_warning        INTEGER := dbms_lob.warn_inconvertible_char;
   BEGIN
      /*
      Purpose:    convert clob to blob
      Remarks:    see http://www.dbforums.com/oracle/1624851-clob-blob.html
      Who     Date        Description
      ------  ----------  -------------------------------------
      MBR     25.01.2011  Created
      */
      dbms_lob.createtemporary(l_returnvalue
                              ,TRUE);
      dbms_lob.converttoblob(dest_lob => l_returnvalue
                            ,src_clob => i_clob
                            ,amount => dbms_lob.getlength(i_clob)
                            ,dest_offset => l_dest_offset
                            ,src_offset => l_source_offset
                            ,blob_csid => dbms_lob.default_csid
                            ,lang_context => l_lang_context
                            ,warning => l_warning);
      RETURN l_returnvalue;
   END f_clob_to_blob;
   FUNCTION f_get_bind_variable(i_variable  IN VARCHAR
                               ,i_query_id  IN NUMBER)
      RETURN VARCHAR2
   IS
      l_item_value  VARCHAR2(4000);
      l_page_id     gen_report_query_params.page_id%TYPE;
      l_page_item   gen_report_query_params.page_item%TYPE;
   BEGIN
      logger.LOG('p_variable=' || i_variable);
      logger.LOG('p_query_id=' || i_query_id);
      SELECT page_id
            ,page_item
        INTO l_page_id
            ,l_page_item
        FROM gen_report_query_params
       WHERE rqe_id = i_query_id
         AND UPPER(param_name) = UPPER(SUBSTR(i_variable
                                             ,2));
      l_item_value   := apex_util.get_session_state(l_page_item);
      RETURN l_item_value;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         logger.LOG(SQLERRM);
         RETURN NULL;
   END;
   FUNCTION f_blob_to_clob(i_blob IN BLOB)
      RETURN CLOB
   AS
      l_returnvalue    CLOB;
      l_dest_offset    INTEGER := 1;
      l_source_offset  INTEGER := 1;
      l_lang_context   INTEGER := dbms_lob.default_lang_ctx;
      l_warning        INTEGER := dbms_lob.warn_inconvertible_char;
   BEGIN
      /*
      Purpose:    convert blob to clob
      Remarks:
      Who     Date        Description
      ------  ----------  -------------------------------------
      MBR     25.01.2011  Created
      */
      dbms_lob.createtemporary(l_returnvalue
                              ,TRUE);
      dbms_lob.converttoclob(dest_lob => l_returnvalue
                            ,src_blob => i_blob
                            ,amount => dbms_lob.lobmaxsize
                            ,dest_offset => l_dest_offset
                            ,src_offset => l_source_offset
                            ,blob_csid => dbms_lob.default_csid
                            ,lang_context => l_lang_context
                            ,warning => l_warning);
      RETURN l_returnvalue;
   END f_blob_to_clob;
   -- Function that creates a regexp string to strip all html tags within a string
   -- with brackets around it
   FUNCTION f_replace_fields_in_clob(i_query      CLOB
                                    ,i_clob       CLOB
                                    ,i_query_id   NUMBER)
      RETURN CLOB
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'f_replace_fields_in_clob';
      l_clob            CLOB;
      l_rec_clob        CLOB;
      l_loop_clob       CLOB;
      l_cursor          PLS_INTEGER;
      l_cursor_status   PLS_INTEGER;
      l_desc_tbl        SYS.dbms_sql.desc_tab2;
      l_col_val         VARCHAR2(32767);
      l_col_count       PLS_INTEGER;
      l_variable        VARCHAR2(4000);
      l_date            DATE;
   BEGIN
      l_cursor          := dbms_sql.open_cursor;
      dbms_sql.parse(l_cursor
                    ,i_query
                    ,dbms_sql.native);
      FOR r_vars
         IN (SELECT var
               FROM (    SELECT REGEXP_SUBSTR(str
                                             ,'(:([A-Za-z_0-9])*)'
                                             ,1
                                             ,LEVEL)
                                   AS var
                           FROM (SELECT i_query AS str
                                   FROM DUAL)
                     CONNECT BY LEVEL <= REGEXP_COUNT(str
                                                     ,'(:([A-Za-z_0-9])*)'))
              WHERE var IS NOT NULL)
      LOOP
         IF INSTR(r_vars.var
                 ,':') > 0
         THEN
            -- replace the bind variable with the value
            dbms_sql.bind_variable_char(
               c => l_cursor
              ,name => TRIM(r_vars.var)
              ,VALUE => f_get_bind_variable(TRIM(r_vars.var)
                                           ,i_query_id) );
         END IF;
      END LOOP;
      dbms_sql.describe_columns2(l_cursor
                                ,l_col_count
                                ,l_desc_tbl);
      -- define report columns
      FOR i IN 1 .. l_col_count
      LOOP
         dbms_sql.define_column(l_cursor
                               ,i
                               ,l_col_val
                               ,32767);
      END LOOP;
      l_cursor_status   := sys.dbms_sql.execute(l_cursor);
      LOOP
         EXIT WHEN dbms_sql.fetch_rows(l_cursor) <= 0;
         l_rec_clob   := i_clob;
         logger.LOG('f_replace_fields_in_clob'
                   ,'loop ' || l_col_count);
         FOR i IN 1 .. l_col_count
         LOOP
            dbms_sql.COLUMN_VALUE(l_cursor
                                 ,i
                                 ,l_col_val);
            l_date   := SYSDATE;
            LOOP
               EXIT WHEN SYSDATE > l_date + 20 / (24 * 60 * 60)
                      OR INSTR(REGEXP_REPLACE(l_rec_clob
                                             ,'(<.*?>)'
                                             ,NULL
                                             ,1
                                             ,0
                                             ,'in')
                              ,'[' || l_desc_tbl(i).col_name || ']') <= 0;
               l_rec_clob      :=
                  REGEXP_REPLACE(l_rec_clob
                                ,'\[' || l_desc_tbl(i).col_name || ']'
                                ,l_col_val
                                ,1
                                ,1 -- when 0 then hangs!!!
                                ,'nc'); -- replace [FIELD_NAME]  by fieldvalue
            END LOOP;
         END LOOP;
         l_clob       := l_clob || l_rec_clob; -- add record data to clob
      END LOOP;
      dbms_sql.close_cursor(l_cursor);
      RETURN l_clob;
   END f_replace_fields_in_clob;
   FUNCTION f_fill_data_from_queries(i_template               CLOB
                                    ,i_report_definition_id   NUMBER)
      RETURN CLOB
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'f_fill_data_from_queries';
      l_clob            CLOB := i_template;
      l_clob1           CLOB;
      l_strip_regexp    VARCHAR2(4000);
   BEGIN
      FOR r_subst IN (SELECT var
                            ,REGEXP_REPLACE(var
                                           ,'<[^>]+>'
                                           ,NULL)
                                real_var
                        FROM (    SELECT str
                                        ,REGEXP_SUBSTR(str
                                                      ,'\[[^]]+]'
                                                      ,1
                                                      ,LEVEL)
                                            AS var
                                    FROM (SELECT l_clob str
                                            FROM DUAL)
                              CONNECT BY LEVEL <= REGEXP_COUNT(str
                                                              ,'\[[^]]+]'))
                       WHERE var IS NOT NULL)
      LOOP
         -- remove all the html tags within the substitution strings
         l_clob      :=
            REPLACE(l_clob
                   ,r_subst.var
                   ,r_subst.real_var);
      END LOOP;
      FOR r_query IN (  SELECT REGEXP_INSTR(l_clob
                                           ,'\[BEGIN:' || rqe.query_name || ']'
                                           ,1
                                           ,1
                                           ,0
                                           ,'ni') -- get first occurence of querytag within stripped clob
                                  position
                              ,rqe.*
                          FROM gen_report_queries rqe
                         WHERE rdn_id = i_report_definition_id
                      ORDER BY 1 DESC -- start with query with the last begin tag
                                     )
      LOOP
         IF r_query.position > 0
         THEN
            IF COALESCE(r_query.query_type
                       ,'D') = 'T'
            THEN
               -- table, get the surrounding row, assumption: only one appearance
               -- of BEGIN:ROW  END:ROW within a template
               IF COALESCE(g_file_type
                          ,'word') = 'word'
               THEN
                  l_strip_regexp      :=
                        '(.*)(\<w:tr[ >].*?'
                     || '\[BEGIN:'
                     || r_query.query_name
                     || ']'
                     || '.*?'
                     || '\[END:'
                     || r_query.query_name
                     || ']'
                     || '(.)*?\<\/w:tr>)';
               ELSE
                  --ppt uses a instead of w
                  l_strip_regexp      :=
                        '(.*)(\<a:tr[ >].*?'
                     || '\[BEGIN:'
                     || r_query.query_name
                     || ']'
                     || '.*?'
                     || '\[END:'
                     || r_query.query_name
                     || ']'
                     || '(.)*?\<\/a:tr>)';
               END IF;
            ELSE
               l_strip_regexp      :=
                     '(.*)('
                  || '\[BEGIN:'
                  || r_query.query_name
                  || ']'
                  || '.*?'
                  || '\[END:'
                  || r_query.query_name
                  || ']'
                  || ')';
            END IF;
            l_clob      :=
                  REGEXP_REPLACE(l_clob
                                ,l_strip_regexp || '(.*)'
                                ,'\1'
                                ,1
                                ,1
                                ,'in') -- everything before the begin tag
               || f_replace_fields_in_clob(
                     r_query.query_text
                    ,REGEXP_REPLACE(
                        REGEXP_REPLACE(REGEXP_REPLACE(l_clob
                                                     ,l_strip_regexp || '(.*)$'
                                                     ,'\2'
                                                     ,1
                                                     ,1
                                                     ,'in')
                                      ,'\[BEGIN:' || r_query.query_name || ']'
                                      ,NULL)
                       ,'\[END:' || r_query.query_name || ']'
                       ,NULL)
                    ,r_query.id)
               || REGEXP_REPLACE(l_clob
                                ,l_strip_regexp
                                ,NULL
                                ,1
                                ,1
                                ,'in');
         -- remove everything in the regexp, i.e. everything before the end tag
         END IF;
      END LOOP;
      RETURN l_clob;
   END;
   FUNCTION f_get_file_from_template(i_report_definition_id IN NUMBER)
      RETURN BLOB
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'f_get_file_from_template';
      l_file_list       apex_zip.t_files;
      l_docx            BLOB;
      l_blob            BLOB;
      l_clob            CLOB;
      l_returnvalue     BLOB;
      CURSOR c_template
      IS
         SELECT *
           FROM gen_report_templates
          WHERE rdn_id = i_report_definition_id
            AND COALESCE(is_current_ind
                        ,'N') = 'Y';
      r_template        c_template%ROWTYPE;
   BEGIN
      OPEN c_template;
      FETCH c_template
         INTO r_template;
      CLOSE c_template;
      /*
      Purpose:      performs substitutions on a template
      Remarks:      template file can be Word (docx), Excel (xlsx), or Powerpoint (pptx)
      Who     Date        Description
      ------  ----------  --------------------------------
      MBR     25.01.2011  Created
      */
      l_file_list   := apex_zip.get_files (r_template.document);
      FOR i IN 1 .. l_file_list.COUNT
      LOOP
         l_blob      :=
            apex_zip.get_file_content(r_template.document
                                 ,l_file_list(i));
         IF l_file_list(i) IN ('word/document.xml'
                              ,'xl/sharedStrings.xml')
         OR l_file_list(i) LIKE 'word/header%.xml'
         OR l_file_list(i) LIKE 'word/footer%.xml'
         OR l_file_list(i) LIKE 'ppt/slides/slide%.xml'
         THEN
            l_clob   := f_blob_to_clob(l_blob);
            IF l_file_list(i) LIKE 'ppt%'
            OR l_file_list(i) LIKE 'word%'
            THEN
               g_file_type      :=
                  CASE
                     WHEN l_file_list(i) LIKE 'ppt%' THEN 'ppt'
                     ELSE 'word'
                  END;
            END IF;
            l_clob      :=
               f_fill_data_from_queries(l_clob
                                       ,i_report_definition_id);
            l_blob   := f_clob_to_blob(l_clob);
         END IF;
         apex_zip.add_file (l_returnvalue
                              ,l_file_list(i)
                              ,l_blob);
      END LOOP;
      apex_zip.finish (l_returnvalue);
      RETURN l_returnvalue;
   END f_get_file_from_template;
   FUNCTION f_get_report_def_id(
      i_name  IN gen_report_definitions.definition_name%TYPE)
      RETURN NUMBER
   IS
      v_id  gen_report_definitions.id%TYPE;
   BEGIN
      IF i_name IS NOT NULL
      THEN
         SELECT id
           INTO v_id
           FROM gen_report_definitions
          WHERE upper(definition_name) = UPPER(i_name);
      END IF;
      RETURN v_id;
   END;
   PROCEDURE p_download_file_from_template(i_report_definition IN VARCHAR2)
   IS
      l_scope  CONSTANT VARCHAR2(61)
                           := g_package || 'p_download_file_from_template' ;
      l_blob            BLOB;
      l_mimetype        VARCHAR2(255)
         := 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
      l_file            utl_file.file_type;
      l_buffer          RAW(32767);
      l_amount          BINARY_INTEGER := 32767;
      l_pos             INTEGER := 1;
      l_blob_len        INTEGER;
   BEGIN
      IF i_report_definition IS NOT NULL
      THEN
         l_blob      :=
            f_get_file_from_template(f_get_report_def_id(i_report_definition));
         -- set up HTTP header
         -- use an NVL around the mime type and
         -- if it is a null set it to application/octect
         -- application/octect may launch a download window from windows
         owa_util.mime_header(NVL(l_mimetype, 'application/octet')
                             ,FALSE);
         -- set the size so the browser knows how much to download
         htp.p('Content-length: ' || dbms_lob.getlength(l_blob));
         --     IF UPPER(p_save_show) = 'SHOW'
         --     THEN
         -- the file will be displayed and nos save dialog will be shown
         -- the filename will be used by the browser if the users does a save as
         htp.p(   'Content-Disposition: attachment;filename="'
               || 'report'
               || TO_CHAR(SYSDATE
                         ,'YYYYMMDD_HH24MISS')
               || CASE
                     WHEN g_file_type = 'ppt' THEN '.pptx'
                     ELSE '.docx'
                  END --l_filename
               || '"');
         -- close the headers
         owa_util.http_header_close;
         -- download the BLOB
         wpg_docload.download_file(l_blob);
      END IF;
   END;
END gen_pck_docx;
/

/
QUIT
