CREATE OR REPLACE PACKAGE "HELIX"."HL7_COMM" IS

TYPE ADR_2_2 IS RECORD
( qry_zis VARCHAR2(20)
, pid_zis VARCHAR2(9)
, b_synoniem BOOLEAN DEFAULT FALSE
, pid_name VARCHAR2(30)
, pid_vvoeg VARCHAR2(10)
, pid_pvlttr VARCHAR2(12)
, pid_birth VARCHAR2(11)
, pid_adr VARCHAR2(25)
, pid_pstcd VARCHAR2(7)
, pid_plaats VARCHAR2(20)
, pid_land VARCHAR2(15)
, pid_sex VARCHAR2(1)
, pid_tel VARCHAR2(15)
, pid_mar_stat VARCHAR2(1)
, pid_birth_plc VARCHAR2(20)
, pid_mult_birth VARCHAR2(1)
, zpi_ovl VARCHAR2(1)
, zpi_ename VARCHAR2(50)
, zpi_evvoeg VARCHAR2(10)
, zpi_ovl_dat VARCHAR2(20)
, in1_verz_code VARCHAR2(6)
, in1_z_of_p VARCHAR2(1)
, in1_verznr VARCHAR2(30)
, stf_hsarts VARCHAR2(15)
, stf_hname VARCHAR2(80)
, stf_sort1 VARCHAR2(30)
, stf_sort2 VARCHAR2(6)
, stf_sort3 VARCHAR2(10)
, stf_sort4 VARCHAR2(25)
, stf_sex VARCHAR2(1)
, stf_phn VARCHAR2(15)
, stf_straat VARCHAR2(35)
, stf_postcd VARCHAR2(7)
, stf_plaats VARCHAR2(20)
, stf_vvn VARCHAR2(2)
);

TYPE VER_2_2 IS RECORD
( ft1_bonnr dft_koppeling.bonnummer%TYPE
, ft1_verrcd dft_koppeling.verrichtingcode%TYPE
, send_appl dft_koppeling.systeemdeel%TYPE
, ft1_prod_afd dft_koppeling.prod_afdeling%TYPE
, pid_zisnr dft_koppeling.zisnr%TYPE
, ft1_verrdat dft_koppeling.verrichtingdat%TYPE
, ft1_aanvr_afd dft_koppeling.aanvr_afdeling%TYPE
, ft1_prod_spec dft_koppeling.prod_spec%TYPE
, zfu_aanvr_spec dft_koppeling.aanvr_spec%TYPE
, zfu_abw dft_koppeling.abw%TYPE
, in1_abi_verzcd dft_koppeling.abi_verzcd%TYPE
, in1_abi_verznr dft_koppeling.abi_verznr%TYPE
, zfu_tekst dft_koppeling.tekst%TYPE
, zfu_aantal dft_koppeling.aantal%TYPE
, pid_name dft_koppeling.naam%TYPE
, pid_birth dft_koppeling.gebdat%TYPE
, pid_adres dft_koppeling.adres%TYPE
, pid_postcd dft_koppeling.pstcd%TYPE
, pid_plaats dft_koppeling.wplts%TYPE
, pid_land dft_koppeling.land%TYPE
);

-- voorlopig volgens de gebruiker zitten er wel erg veel lege velden in de specificaties

PROCEDURE PACK_QRY_2_2
( p_zis IN kgc_personen.zisnr%TYPE
);

PROCEDURE UNPACK_ADR_2_2
( patient_rec OUT adr_2_2
);

PROCEDURE PACK_VER_2_2
( fact_rec IN ver_2_2
, p_status OUT VARCHAR2
);

PROCEDURE UNPACK_ACK_2_2
( p_dft_ok OUT VARCHAR2
, p_status OUT VARCHAR2
);

PROCEDURE SEND
( p_msg IN VARCHAR2
, p_status OUT VARCHAR2
);

PROCEDURE RECEIVE
( p_msg IN VARCHAR2
, p_status OUT VARCHAR2
);

-- tijdelijk publiek...
function strip
( p_string in varchar2
, p_pos_va in number := null
, p_lengte in number := null
, p_teken in varchar2 := null
, p_voorkomen in number := null
)
return varchar2;
END HL7_COMM;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."HL7_COMM" IS

session_id VARCHAR2(32767) := 000;
v_qry_zis VARCHAR2(7);
v_dummy VARCHAR2(10);
c_adt  CONSTANT VARCHAR2(5) := 'ADT';
c_dft  CONSTANT VARCHAR2(5) := 'DFT';
c_vrw  CONSTANT VARCHAR2(2) := 'V';
c_vval CONSTANT VARCHAR2(1) := 'J';
c_nvvn CONSTANT VARCHAR2(1) := 'N';
c_m    CONSTANT VARCHAR2(1) := 'M';
c_v    CONSTANT VARCHAR2(1) := 'V';
c_o    CONSTANT VARCHAR2(1) := 'O';

pack_rec_qry   VARCHAR2(32767);
pack_rec_ver   VARCHAR2(32767);
unpack_rec_adr   VARCHAR2(32767);
unpack_rec_ack VARCHAR2(32767);

TYPE dummy IS RECORD
( ack_code VARCHAR2(50)
, pid_name VARCHAR2(200)
, pid_adr  VARCHAR2(200)
, stf_adr  VARCHAR2(200)
);
dummy_rec dummy;

-- strip een gedeelte uit een string
-- een lengte vanaf een bepaalde positie (= substr)
-- het stuk vanaf het zoveelste voorkomen van een teken tot het volgende voorkomen van dat teken
function strip
( p_string in varchar2
, p_pos_va in number := null
, p_lengte in number := null
, p_teken in varchar2 := null
, p_voorkomen in number := null
)
return varchar2
is
  v_return varchar2(1000) := p_string;
  v_pos_va number := p_pos_va;
  v_pos_tm number;
  v_lengte number := p_lengte;
begin
  if ( p_pos_va = 0 )
  then
    v_return := null;
  elsif ( p_pos_va > 0 )
  then
    v_pos_tm := instr( p_string, p_teken, 1, nvl( p_voorkomen, 1 ) );
    if ( v_pos_tm < v_pos_va )
    then
      v_pos_tm := length( p_string );
    end if;
    v_lengte := nvl( p_lengte
                   , v_pos_tm - v_pos_va
                   );
    v_return := substr( p_string, p_pos_va, v_lengte );
  elsif ( p_teken is not null )
  then
    v_pos_va := instr( p_string, p_teken, 1, nvl( p_voorkomen, 1 ) )
              + length( p_teken )
              ;
    v_pos_tm := instr( p_string, p_teken, 1, nvl( p_voorkomen, 1 ) + 1 );
    if ( v_pos_tm < v_pos_va )
    then
      v_pos_tm := length( p_string );
    end if;
    v_lengte := nvl( p_lengte
                   , v_pos_tm - v_pos_va
                   );
    v_return := strip( p_string => p_string
                     , p_pos_va => v_pos_va
                     , p_lengte => v_lengte
                     );
  end if;
  return( v_return );
end strip;

-- formatteer zisnummer als 9.999.999
function formatteer_zisnr
( p_zisnr in varchar2
)
return varchar2
is
  v_return varchar2(10) := p_zisnr;
begin
  if ( instr( v_return, '.' ) > 0 )
  then
    v_return := replace( p_zisnr, '.', null );
  else
    v_return := SUBSTR( v_return, 1, 1 )
             || '.'
             || SUBSTR( v_return, 2, 3 )
             || '.'
             || SUBSTR( v_return, 5 )
              ;
  end if;
  return( v_return );
exception
  when others
  then
    return( p_zisnr );
end formatteer_zisnr;

-- formatteer datum van yyyymmdd naar dd-mm-yyyy, eventueel met 00-00-0000
function formatteer_datum
( p_datum in varchar2
)
return varchar2
is
  v_return varchar2(10) := substr( p_datum, 1, 10 );
begin
  IF ( LENGTH( v_return ) > 6 )
  THEN
    v_return := SUBSTR( v_return, 7, 2 )
             || '-'
             || SUBSTR( v_return, 5, 2 )
             || '-'
             || SUBSTR( v_return, 1, 4 )
              ;
  ELSIF LENGTH( v_return ) = 6
  THEN -- wanneer datum 6 lang is, zijn alleen de maand en het jaar bekend
    v_return := '00-'
             || SUBSTR( v_return, 5, 2 )
             || '-'
             || SUBSTR( v_return, 1, 4 )
              ;
  ELSIF LENGTH( v_return ) = 4
  THEN -- wanneer datum 4 lang is, is alleen het jaar bekend
    v_return := '00-00-'
             || v_return
              ;
  END IF;
  return( v_return );
exception
  when others
  then
    return( null );
end formatteer_datum;

-- geen betekenis vooralsnog: er is geen voorvoegseltabel in helix!!!
function formatteer_tussenvoegsel
( p_voorvoegsel in varchar2
)
return varchar2
is
  v_return varchar2(10) := substr( p_voorvoegsel, 1, 10);
begin
  return( v_return );
exception
  when others
  then
    return( p_voorvoegsel );
end formatteer_tussenvoegsel;

-- splits naam in voorletters, voornaam, voorvoegesel en achternaam
procedure formatteer_naam
( p_naam in out varchar2 -- retourneer gewijzigde naam
, x_voorletters out varchar2
, x_voorvoegsel out varchar2
, x_achternaam out varchar2
)
is
begin
  x_achternaam := strip
                  ( p_string => p_naam
                  , p_pos_va => 1
                  , p_teken => '^'
                  );
  x_voorletters := strip
                   ( p_string => p_naam
                   , p_teken => '^'
                   , p_voorkomen => 1
                   );
  -- voorvoegsel is deel na 1e spatie achternaam
  -- FOUT  mevr. Bos Vos
  x_voorvoegsel := strip
                   ( p_string => x_achternaam
                   , p_teken => ' '
                   , p_voorkomen => 1
                   );
  if ( length ( x_voorvoegsel ) > 10 )
  then
    -- te lang: laat voorvoegsel achter achternaam staan
    x_voorvoegsel := null;
  else
    x_achternaam := strip
                   ( p_string => x_achternaam
                   , p_pos_va => 1
                   , p_teken => ' '
                   );
  end if;
  p_naam := x_voorletters;
  if ( x_voorvoegsel is not null )
  then
    p_naam := p_naam || ' ' || x_voorvoegsel;
  end if;
  p_naam := p_naam || ' ' || x_achternaam;
exception
  when others
  then
    x_achternaam := substr( p_naam, 1, 30 );
end formatteer_naam;

PROCEDURE PACK_QRY_2_2
( p_zis IN kgc_personen.zisnr%TYPE
)
IS
  v_qry_date VARCHAR2(15);
BEGIN
  v_qry_date := TO_CHAR( SYSDATE, 'yyyymmddhh24mi' );
  v_qry_zis := REPLACE(p_zis,'.');
  pack_rec_qry := CHR(11)
               ||'MSH|^~\&|DMG-ORACLE||HISCOM-ADT-V01R13-NL|HIS|'
               || v_qry_date
               || '||QRY^Q01|4137|P|2.2|||NE|AL'
               || CHR(13)
               || 'QRD|'
               || v_qry_date
               || '|R|I|0000000001||||'
               || v_qry_zis
               || '|NAW|'
               || CHR(13)
               || CHR(28)
               || CHR(13)
                ;
  session_id := dbms_pipe.unique_session_name;
  dbms_pipe.pack_message (session_id);
  dbms_pipe.pack_message (pack_rec_qry);
  send(c_adt,v_dummy);
END PACK_QRY_2_2;

PROCEDURE UNPACK_ADR_2_2
( patient_rec OUT adr_2_2
)
IS
  v_postcd_ltr1 VARCHAR2(2);
  v_postcd_ltr2 VARCHAR2(2);
  v_post_nl NUMBER(1) := 0;
  b_voornaam BOOLEAN  DEFAULT  FALSE;

  v_pid_string varchar2(32767);
  v_zpi_string varchar2(32767);
BEGIN
  receive( c_adt, v_dummy );
  dbms_pipe.unpack_message( unpack_rec_adr );
  IF unpack_rec_adr IS NOT NULL
  THEN
    dummy_rec.ack_code := SUBSTR( unpack_rec_adr
                                , INSTR( unpack_rec_adr
                                       , 'MSA|'
                                       ) + 4
                                , 2
                                );
  ELSE
    raise_application_error(-20016,'Lege string ontvangen van het ZIS ??');
  END IF;

  IF dummy_rec.ack_code <> 'AA'
  THEN
    IF dummy_rec.ack_code = 'AE'
    THEN
      raise_application_error(-20012, 'Een applicatie fout: AE is teruggeven door TDM' );
    ELSE  -- code AR
      raise_application_error(-20012, 'Fout in berichtuitwisseling met het ZIS, AR ontvangen');
    END IF;
  ELSIF INSTR(unpack_rec_adr, 'PID|') = 0
  THEN
    raise_application_error(-20013, 'patient met dit nummer komt niet voor in ZIS');
  ELSE
    IF  INSTR(unpack_rec_adr,'MRG|' ) > 0
    THEN
      -- er is sprake van een synoniem patientnummer (ZIS-nr) en moet ter vergelijking het zisnr uit
      -- het MRG segment worden gehaald
      -- volgens de documentatie is het bewuste veld verplicht dus zou de tweede fieldseperator (|)
      -- niet mogen ontbreken, maar dat doet het wel vandaar ZPI
      patient_rec.qry_zis := strip
                             ( p_string => strip( p_string => unpack_rec_adr
                                                , p_pos_va => 1
                                                , p_teken => 'MRG|'
                                                )
                             , p_teken => '|'
                             );
      IF INSTR(patient_rec.qry_zis, 'ZPI') > 0
      THEN
        patient_rec.qry_zis := strip( p_string => patient_rec.qry_zis
                                    , p_pos_va => 1
                                    , p_lengte => LENGTH(patient_rec.qry_zis) - 4
                                    );
      END IF;
      patient_rec.b_synoniem := TRUE;
    ELSE
      patient_rec.qry_zis := strip
                             ( p_string => strip( p_string => unpack_rec_adr
                                                , p_pos_va => 1
                                                , p_teken => 'QRD|'
                                                )
                             , p_teken => '|'
                             , p_voorkomen => 8
                             );
    END IF;

    IF  patient_rec.qry_zis <> v_qry_zis
    THEN
      raise_application_error(-20013, 'Match mislukt. Opgehaalde informatie hoort niet bij opgegeven ZIS_nr');
    ELSE
      -- puntjes zetten in qry_zis zodat er geen replace in het update statement
      -- (proc Ververs_patient) hoeft te worden gebruikt (kost meer tijd). Pas hier
      -- omdat anders de bovenstaande vergelijking mislukt.
      patient_rec.qry_zis := formatteer_zisnr( patient_rec.qry_zis );
      -- het verschil tussen qry_zis en pid_zis is dat in qry_zis het nummer zit waarmee het bericht is gestart
      -- dit kan een synoniem zijn. Het pid_zis is het zisnr dat uit het Zis komt en kan een voorkeursnummer zijn
      -- het voorkeursnummer moet in het DMG systeem terecht komen en het qry_zis is nodig om de juiste rij in
      -- het DMG te vinden.

      v_pid_string := strip( p_string => unpack_rec_adr
                           , p_teken => 'PID|'
                           );
      -- identificerend nummer (eventueel voorkeursnummer)
      patient_rec.pid_zis := strip
                             ( p_string => v_pid_string
                             , p_teken => '|'
                             , p_voorkomen => 3
                             );
      -- puntjes zetten in het zisnr
      patient_rec.pid_zis := formatteer_zisnr( patient_rec.pid_zis );

      -- geboorte datum patient
      -- eerst geboortedatum dan naam om leeftijd te kunnen bepalen en aan de hand daarvan
      -- wel of niet de volledige voornaam te gebruiken
      patient_rec.pid_birth := strip
                               ( p_string => v_pid_string
                               , p_teken => '|'
                               , p_voorkomen => 7
                               );
      -- geboorte datum uitpakken, omdraaien en van streepjes voorzien
      patient_rec.pid_birth := formatteer_datum( patient_rec.pid_birth );

      -- leeftijd bepalen
      IF TO_NUMBER( TO_CHAR( SYSDATE, 'yyyy' ) ) - TO_NUMBER( SUBSTR( patient_rec.pid_birth, -4 ) ) < 18
      THEN
        b_voornaam := TRUE;
      END IF;

      -- naam patient (inclusief voorvoegsels, voornaam en voorletters)
      dummy_rec.pid_name := strip
                            ( p_string => v_pid_string
                            , p_teken => '|'
                            , p_voorkomen => 5
                            );
      -- naam verder uitsplitsen wanneer de persoon jonger dan 18 is de voornaam gebruiken anders alleen voorletters
      -- ook als er maar 1 voorletter is. Als er geen voorletters zijn dan de eerste van de voornaam nemen
      -- (if > 0  nodig om te voorkomen dat uit substr de hele string wordt genomen)

      -- als laatste de voorvoegsels (van, de, van der, etc.) scheiden van de achternaam
      -- om te bepalen of het om voorvoegsels gaat en niet om een dubbele naam,
      -- en om de juiste notatie te vinden wordt gebruik gemaakt van de referentietabel stc008
      -- let wel het is een generieke afkortingen tabel. De kolom VVOEG kan hier niet worden gebruikt
      -- en de distinct is nodig om een too_many_rows melding te voorkomen.
      -- Als veiligheidsmaatregel toch een too_many_rows afvangen door dan maar de zis notatie te gebruiken
      -- Kan eventueel zonder, omdat de when others alles afvangt en geen voorvoegsels registreert
      formatteer_naam
      ( p_naam => dummy_rec.pid_name
      , x_achternaam => patient_rec.pid_name
      , x_voorletters => patient_rec.pid_pvlttr
      , x_voorvoegsel => patient_rec.pid_vvoeg
      );

      -- geslacht patient (in zis is het F voor Female en in het DMG V voor vrouw, vertalen dus)
      patient_rec.pid_sex := replace( strip
                                      ( p_string => v_pid_string
                                      , p_teken => '|'
                                      , p_voorkomen => 8
                                      )
                                    , 'F', c_vrw
                                    );

      -- adres patient (inclusief huisnummer, postcode, woonplaats, land)
      dummy_rec.pid_adr := strip
                           ( p_string => v_pid_string
                           , p_teken => '|'
                           , p_voorkomen => 11
                           );
      -- adres verder uitsplitsen
      -- de straatnaam incl. huisnummer
      patient_rec.pid_adr := REPLACE( strip
                                      ( p_string => dummy_rec.pid_adr
                                      , p_pos_va => 1
                                      , p_teken => '^'
                                      , p_voorkomen => 2
                                      )
                                    , '>', ' '
                                    );

      -- de postcode
      patient_rec.pid_pstcd := strip
                               ( p_string => dummy_rec.pid_adr
                               , p_pos_va => 1
                               , p_teken => '^'
                               , p_voorkomen => 4
                               );
      -- de woonplaats
      patient_rec.pid_plaats  := upper( strip
                                        ( p_string => dummy_rec.pid_adr
                                        , p_pos_va => 1
                                        , p_teken => '^'
                                        , p_voorkomen => 3
                                        )
                                      );
      -- einde adres

      -- prive telefoonnummer patient (of dit nummer of het zakelijknummer is gevuld niet beide, dus gelijk maar even checken en schuiven)
      patient_rec.pid_tel := strip
                             ( p_string => v_pid_string
                             , p_teken => '|'
                             , p_voorkomen => 13
                             );
      IF LENGTH(patient_rec.pid_tel) < 4
      THEN
        -- business (zakelijk) telefoonnummer patient alleen vullen als er in het prive veld alleen maar "" staat
        patient_rec.pid_tel := strip
                               ( p_string => v_pid_string
                               , p_teken => '|'
                               , p_voorkomen => 14
                               );
      END IF;

      -- marital status (burgelijke staat) patient alleen vullen als het een vrouw is
      IF patient_rec.pid_sex = c_vrw
      THEN
        patient_rec.pid_mar_stat := strip
                                    ( p_string => v_pid_string
                                    , p_teken => '|'
                                    , p_voorkomen => 16
                                    );
        -- vertaling burgelijke staat naar DMG kodering J, N of O
        -- met deze vertaling krijgen alle weduwen, weduwnaars en gescheiden mensen ook de status 'N' (niet gehuwd) --
        IF patient_rec.pid_mar_stat = 'U'
        THEN
          patient_rec.pid_mar_stat := 'O';
        ELSIF patient_rec.pid_mar_stat = 'M'
        THEN
          patient_rec.pid_mar_stat := 'J';
        ELSE
          patient_rec.pid_mar_stat := 'N';
        END IF;
      END IF;

      v_zpi_string := strip
                      ( p_string => v_pid_string
                      , p_teken => 'ZPI|'
                      );

      -- patient overleden (Y/N)
      patient_rec.zpi_ovl := REPLACE( strip
                                      ( p_string => v_zpi_string
                                      , p_teken => '|'
                                      , p_voorkomen => 2
                                      )
                                    , 'Y', 'J'
                                    );

      -- wanneer patient getrouwde vrouw dan is zou dit veld gevuld moeten zijn met naam van de man
      -- echter er moet indirect gezocht worden, omdat bijvoorbeeld weduwen niet meer getrouwd zijn (N of O) en toch
      --  nog de naam van de man gebruiken
      IF   patient_rec.pid_sex = 'V'
      THEN
        patient_rec.zpi_ename := REPLACE( strip
                                          ( p_string => v_zpi_string
                                          , p_teken => '|'
                                          , p_voorkomen => 4
                                          )
                                        , '"', null
                                        );
        formatteer_naam
        ( p_naam => patient_rec.zpi_ename
        , x_achternaam => patient_rec.zpi_ename
        , x_voorletters => v_dummy
        , x_voorvoegsel => patient_rec.zpi_evvoeg
        );
      END IF;

      -- datum van overlijden
      -- als de patient overleden is, maar de overlijdensdatum is niet ingevuld dan worden er quotes ("") meegegeven
      -- in het bericht, dus weggooien door replace " met niets
      IF patient_rec.zpi_ovl = 'J'
      THEN
        IF LENGTH( SUBSTR( unpack_rec_adr
                         , INSTR( unpack_rec_adr
                                , '|'
                                , INSTR( unpack_rec_adr
                                       , 'ZPI|'
                                       , 11
                                       )
                                )
                         )
                 ) > 4
        THEN
          patient_rec.zpi_ovl_dat := REPLACE( SUBSTR( unpack_rec_adr
                                                    , INSTR( unpack_rec_adr
                                                           , '|'
                                                           , INSTR( unpack_rec_adr
                                                                  , 'ZPI|'
                                                                  )
                                                           , 11
                                                           ) + 1
                                                    , INSTR( unpack_rec_adr
                                                           , '|'
                                                           , INSTR( unpack_rec_adr
                                                                  , 'ZPI|'
                                                                  )
                                                           , 12
                                                           )
                                                    - INSTR( unpack_rec_adr
                                                           , '|'
                                                           , INSTR( unpack_rec_adr
                                                                  , 'ZPI|'
                                                                  )
                                                           , 11
                                                           ) - 1
                                                     )
                                            , '"'
                                            );
          -- het gaat hier om het laatste veld van het ZPI segment en in dit geval is het niet verplicht
          -- dus kan de laatste fieldseperator (|) ontbreken
          IF INSTR( patient_rec.zpi_ovl_dat
                  , 'IN1'
                  ) > 0
          THEN
            patient_rec.zpi_ovl_dat := SUBSTR( patient_rec.zpi_ovl_dat
                                             , 1
                                             , LENGTH( patient_rec.zpi_ovl_dat ) - 4
                                             );
          END IF;

          -- streepje zetten en omdraaien plus zelfde truc met nullen voor onvolledige datum
          -- als bij geboortedatum
          IF LENGTH( patient_rec.zpi_ovl_dat ) > 6
          THEN
            patient_rec.zpi_ovl_dat := SUBSTR( patient_rec.zpi_ovl_dat, 7, 2 )
                                    || '-'
                                    || SUBSTR( patient_rec.zpi_ovl_dat, 5, 2 )
                                    || '-'
                                    || SUBSTR( patient_rec.zpi_ovl_dat, 1, 4 )
                                     ;
          -- wanneer patient_rec.pid_birth 6 lang is, zijn alleen de maand en het jaar bekend
          ELSIF LENGTH( patient_rec.zpi_ovl_dat ) = 6
          THEN
            patient_rec.zpi_ovl_dat := '00-'
                                    || SUBSTR( patient_rec.zpi_ovl_dat, 5, 2 )
                                    || '-'
                                    || SUBSTR( patient_rec.zpi_ovl_dat, 1, 4 )
                                     ;
          -- in het geval dat patient_rec.zpi_ovl_dat 4 lang is, gaat het alleen om het jaar
          ELSIF LENGTH( patient_rec.zpi_ovl_dat ) = 4
          THEN
            patient_rec.zpi_ovl_dat := '00-00-'
                                    || patient_rec.zpi_ovl_dat
                                     ;
          END IF;
        END IF;
      END IF;

      -- check of er nuttige informatie staat in het verzekeringssegment en of ie wel voorkomt
      IF   INSTR( unpack_rec_adr, 'IN1|' ) > 0
       AND INSTR( SUBSTR( unpack_rec_adr
                        , INSTR( unpack_rec_adr
                               , 'IN1|'
                               )
                        , 20
                        )
                ,'STF|'
                ) = 0
      THEN
        -- verzekeringscode verzekeringsmaatschappij patient
        patient_rec.in1_verz_code  := REPLACE( SUBSTR( unpack_rec_adr
                                                     , INSTR( unpack_rec_adr
                                                            , '|'
                                                            , INSTR( unpack_rec_adr
                                                                   , 'IN1|'
                                                                   )
                                                            ,3
                                                            ) + 1
                                                     , INSTR( unpack_rec_adr
                                                            , '|'
                                                            , INSTR( unpack_rec_adr
                                                                   , 'IN1|'
                                                                   )
                                                            , 4
                                                            )
                                                     - INSTR( unpack_rec_adr
                                                            , '|'
                                                            , INSTR( unpack_rec_adr
                                                                   , 'IN1|'
                                                                   )
                                                            , 3
                                                            ) - 1
                                                     )
                                             , '"'
                                             );
        -- verzekeringswijze ziekenfonds of particulier (slechts 1 positie nodig, veld in DMG database is maar 1 lang)
        -- checken of  het eerste karakter een Z is van Ziekenfonds anders met P vullen van Particulier, code die
        -- dan uit ZIS komt is NZ (niet ziekenfonds), echter in het DMG kennen ze alleen Z en P.
        patient_rec.in1_z_of_p := SUBSTR( unpack_rec_adr
                                        , INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'IN1|'
                                                      )
                                                , 15
                                                ) + 1
                                        , 1
                                        );
        IF patient_rec.in1_z_of_p <> 'Z'
        THEN
          patient_rec.in1_z_of_p := 'P';
        END IF;

        -- verzekeringsnummer (polisnummer)
        patient_rec.in1_verznr := SUBSTR( unpack_rec_adr
                                        , INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'IN1|'
                                                      )
                                               , 36
                                               ) + 1
                                        , INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'IN1|'
                                                      )
                                               , 37
                                               )
                                        - INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'IN1|'
                                                      )
                                                , 36
                                                ) - 1
                                         );
        -- het gaat hier om het laatste veld van het IN1 segment en in dit geval is het niet verplicht
        -- dus kan de laatste fieldseperator (|) ontbreken
        IF  INSTR( patient_rec.in1_verznr, 'IN' ) > 0
         OR INSTR( patient_rec.in1_verznr, 'STF' ) > 0
        THEN
          patient_rec.in1_verznr := SUBSTR( patient_rec.in1_verznr
                                          , 1
                                          , LENGTH( patient_rec.in1_verznr ) - 4
                                          );
        END IF;
      END IF;

      -- check of er in het huisarts segment wel iets nuttigs staat en of ie wel voorkomt
      IF   INSTR( unpack_rec_adr, 'STF|' ) > 0
       AND LENGTH( SUBSTR( unpack_rec_adr
                         , INSTR( unpack_rec_adr
                                , 'STF|'
                                )
                         , 20
                         )
                 ) > 15
      THEN
        -- Het 13de veld van STF checken op inhoud, wanneer gevuld dan gaat het om een vervallen huisarts
        IF INSTR( unpack_rec_adr
                , '|'
                , INSTR( unpack_rec_adr
                        , 'STF|'
                        )
                , 13
                ) > 0
        THEN
          -- staat er dan ook echt iets in (format datum yyyymmdd)
          IF LENGTH( SUBSTR( unpack_rec_adr
                           , INSTR( unpack_rec_adr
                                  , '|'
                                  , INSTR( unpack_rec_adr
                                         , 'STF|'
                                         )
                                  , 13
                                  ) + 1
                           , 8
                           )
                   ) > 4
          THEN
            patient_rec.stf_vvn := c_vval;
          ELSE
            patient_rec.stf_vvn := c_nvvn;
          END IF;
        END IF;

        -- huisartscode van huisarts patient ook wel zorgverlenerscode (of liszcode)
        -- moet gecheckt worden op volledigheid.
        patient_rec.stf_hsarts := SUBSTR( unpack_rec_adr
                                        , INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'STF|'
                                                      )
                                               , 1
                                               ) + 1
                                        , INSTR( unpack_rec_adr
                                               , '^'
                                               , INSTR( unpack_rec_adr
                                                      , 'STF|'
                                                      )
                                               , 1
                                               )
                                        - INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'STF|'
                                                      )
                                               , 1
                                               ) - 1
                                        );
        IF   LENGTH( patient_rec.stf_hsarts ) < 15
         AND LENGTH( patient_rec.stf_hsarts ) > 8
        THEN
          -- De verbijzondering ontbreekt aanvullen met dubbel 0
          -- aanpassing gpagen 14052001: '00' vervangen door '' lege praktijkcode in ZIS is dan ook leeg in HL7-bericht
          -- patient_rec.stf_hsarts := substr(patient_rec.stf_hsarts,1,8)||'00'||substr(patient_rec.stf_hsarts,9);
-- MK: zinloos statement hieronder!!!
          patient_rec.stf_hsarts := SUBSTR( patient_rec.stf_hsarts
                                          , 1
                                          , 8
                                          )
                                 || ''
                                 || SUBSTR( patient_rec.stf_hsarts
                                          , 9
                                          )
                                  ;
        END IF;

        -- naam huisarts (incl. achternaam, voorletters, tussenvoegsel, titel)
        patient_rec.stf_hname  := SUBSTR( unpack_rec_adr
                                        , INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'STF|'
                                                      )
                                               , 3
                                               ) + 1
                                        , INSTR( unpack_rec_adr
                                               , '|'
                                               , INSTR( unpack_rec_adr
                                                      , 'STF|'
                                                      )
                                                , 4
                                                )
                                         - INSTR( unpack_rec_adr
                                                , '|'
                                                , INSTR( unpack_rec_adr
                                                       , 'STF|'
                                                       )
                                                , 3
                                                ) - 1
                                        );
        -- verder uisplitsen naam huisarts en stf_hname in de juiste volgorde plaatsen
        -- als eerste de titel
        IF   INSTR( patient_rec.stf_hname
                  , '^'
                  , 1
                  , 3
                  ) > 0
         AND LENGTH( SUBSTR( patient_rec.stf_hname
                           , INSTR( patient_rec.stf_hname
                                  , '^'
                                  , 1
                                  , 3
                                  ) + 1
                           )
                   ) > 1
        THEN
          patient_rec.stf_sort4 := INITCAP( SUBSTR( patient_rec.stf_hname
                                                  , INSTR( patient_rec.stf_hname
                                                         , '^'
                                                         , 1
                                                         , 3
                                                         ) + 1
                                                   )
                                           )
                                || '.';
        END IF;

        -- dan de voorletters (eerste en rest koppelen)
        IF LENGTH( SUBSTR( patient_rec.stf_hname
                         , INSTR( patient_rec.stf_hname
                                , '^'
                                )
                         , INSTR( patient_rec.stf_hname
                                , '^'
                                , 1
                                ,3
                                )
                         - INSTR( patient_rec.stf_hname
                                , '^'
                                )
                         )
                 ) > 3
        THEN
          patient_rec.stf_sort2 := UPPER( REPLACE( SUBSTR( patient_rec.stf_hname
                                                         , INSTR( patient_rec.stf_hname
                                                                , '^'
                                                                ) + 1
                                                         , INSTR( patient_rec.stf_hname
                                                                , '^'
                                                                , 1
                                                                , 3
                                                                )
                                                         - INSTR( patient_rec.stf_hname
                                                                , '^'
                                                                ) - 1
                                                          )
                                                 , '^'
                                                 )
                                        );
        END IF;

        -- gevolgd door de achternaam incl. tussenvoegsels (de, van de, den, etc)
        patient_rec.stf_hname := SUBSTR( patient_rec.stf_hname
                                       , 1
                                       , INSTR( patient_rec.stf_hname
                                              , '^'
                                              ) - 1
                                       );
        IF patient_rec.stf_vvn = c_vval
        THEN
          patient_rec.stf_hname := RTRIM( REPLACE( TRANSLATE( patient_rec.stf_hname
                                                            , '0123456789'
                                                            , '1'
                                                            )
                                                 , '1'
                                                 )
                                        );
        END IF;

        IF INSTR( patient_rec.stf_hname, ' ') > 0
        THEN
          -- er is sprake van tussenvoegsels
          -- zelfde procedure als bij de naam van de patient en echtgenoot
          BEGIN
--            SELECT DISTINCT leestekens
--            INTO patient_rec.stf_sort3
--            FROM stc008
--            WHERE LOWER(zis) = LOWER(SUBSTR(patient_rec.stf_hname,INSTR(patient_rec.stf_hname,' ',-1)+1)); --
            patient_rec.stf_sort1 := INITCAP( SUBSTR( patient_rec.stf_hname
                                                    , 1
                                                    , INSTR( patient_rec.stf_hname
                                                            , ' '
                                                            , -1
                                                            ) - 1
                                                    )
                                             );
          EXCEPTION
            WHEN TOO_MANY_ROWS
            THEN
              patient_rec.stf_sort3 := LOWER( SUBSTR( patient_rec.stf_hname
                                                    , INSTR( patient_rec.stf_hname
                                                           , ' '
                                                           , -1
                                                           ) + 1
                                                    )
                                            );
              patient_rec.stf_sort1 := INITCAP( SUBSTR( patient_rec.stf_hname
                                                      , 1
                                                      , INSTR( patient_rec.stf_hname
                                                             , ' '
                                                             , -1
                                                             ) - 1
                                                      )
                                              );
            WHEN NO_DATA_FOUND
            THEN
              patient_rec.stf_sort1 := patient_rec.stf_hname;
            WHEN OTHERS
            THEN NULL;
          END;
        ELSE
          patient_rec.stf_sort1 := INITCAP( patient_rec.stf_hname );
        END IF;

        -- daarna stf_hname opnieuw vullen maar dan in de goede volgorde
        IF patient_rec.stf_sort3 IS NULL
        THEN
          patient_rec.stf_hname := patient_rec.stf_sort4
                                || ' '
                                || patient_rec.stf_sort2
                                || ' '
                                || patient_rec.stf_sort1
                                 ;
        ELSE
          patient_rec.stf_hname := patient_rec.stf_sort4
                                || ' '
                                || patient_rec.stf_sort2
                                || ' '
                                || patient_rec.stf_sort3
                                || ' '
                                || patient_rec.stf_sort1
                                 ;
        END IF;

        -- als laatste een paar velden in het DMG format zetten (reden voor dit format is onduidelijk)
        patient_rec.stf_sort1 := UPPER( patient_rec.stf_sort1 );
        patient_rec.stf_sort2 := REPLACE( patient_rec.stf_sort2, '.' );

        -- geslacht huisarts
        patient_rec.stf_sex := SUBSTR( unpack_rec_adr
                                     , INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                            , 5
                                            ) + 1
                                     , INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                            , 6
                                            )
                                     - INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                            , 5
                                            ) - 1
                                     );
        -- telefoonnummer huisarts
        patient_rec.stf_phn := SUBSTR( unpack_rec_adr
                                     , INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                            , 10
                                            ) + 1
                                     , INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                            , 11
                                            )
                                     - INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                            , 10
                                            ) - 1
                                     );
        -- adres huisarts
        IF INSTR( unpack_rec_adr
                , '|'
                , INSTR( unpack_rec_adr
                       , 'STF|'
                       )
                , 12
                ) > 0
        THEN
          dummy_rec.stf_adr := SUBSTR( unpack_rec_adr
                                     , INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                             , 11
                                             ) + 1
                                      , INSTR( unpack_rec_adr
                                             , '|'
                                             , INSTR( unpack_rec_adr
                                                    , 'STF|'
                                                    )
                                              , 12
                                              )
                                       - INSTR( unpack_rec_adr
                                              , '|'
                                              , INSTR( unpack_rec_adr
                                                     , 'STF|'
                                                     )
                                              , 11
                                              ) - 1
                                       );
        ELSE
          dummy_rec.stf_adr := SUBSTR( unpack_rec_adr
                                     , INSTR( unpack_rec_adr
                                            , '|'
                                            , INSTR( unpack_rec_adr
                                                   , 'STF|'
                                                   )
                                            ,11
                                            ) + 1
                                     );
        END IF;

        -- splitsen adres huisarts
        patient_rec.stf_straat  := SUBSTR( dummy_rec.stf_adr
                                         , 1
                                         , INSTR( dummy_rec.stf_adr
                                                , '^'
                                                ) - 1
                                         );
        patient_rec.stf_plaats  := UPPER( SUBSTR( dummy_rec.stf_adr
                                                , INSTR( dummy_rec.stf_adr
                                                       , '^'
                                                       , 1
                                                       , 2
                                                       ) + 1
                                                , INSTR( dummy_rec.stf_adr
                                                       , '^'
                                                       , 1
                                                       , 3
                                                       )
                                                - INSTR( dummy_rec.stf_adr
                                                       , '^'
                                                       , 1
                                                       , 2
                                                       ) - 1
                                                )
                                        );
        patient_rec.stf_postcd  := SUBSTR( dummy_rec.stf_adr
                                         , INSTR( dummy_rec.stf_adr
                                                , '^'
                                                , 1
                                                , 4
                                                ) + 1
                                         , 6
                                         );

        -- opnieuw initialiseren variabele v_post_nl
        v_post_nl := 0;

        -- Wanneer het een Nederlandse postcode is een spatie plaatsen voor de letters
        IF LENGTH( patient_rec.stf_postcd ) = 6
        THEN
          v_postcd_ltr1 := UPPER( SUBSTR( patient_rec.stf_postcd, 5, 1 ) );
          v_postcd_ltr2 := UPPER( SUBSTR( patient_rec.stf_postcd, 6, 1 ) );
          if   ascii(v_postcd_ltr1) between 65 and 90
           and ascii(v_postcd_ltr2) between 65 and 90
          then
            begin
              select  1
              into    v_post_nl
              from    kgc_postcodes
              where   postcode = substr( patient_rec.stf_postcd, 1, 4 );
            exception
              when others
              then null;
            end;
          end if;
          IF v_post_nl = 1
          THEN
            -- het is een Nederlandse postcode dus een spatie
            patient_rec.stf_postcd := SUBSTR( patient_rec.stf_postcd, 1, 4 )
                                   || ' '
                                   || UPPER( SUBSTR( patient_rec.stf_postcd, 5, 2 ) );
          END IF;
        END IF;
      END IF;
    END IF;
  END IF;
  -- de nodige informatie uit het bericht zit nu in de daarvoor bestemde velden, gestart kan worden met de update
  -- de update vindt plaats in de stored procedure VERVERS_PATIENT (verplaatst naar kgc_interface.persoon_in_zis_utrecht)
END UNPACK_ADR_2_2;

PROCEDURE PACK_VER_2_2
( fact_rec IN ver_2_2
, p_status OUT VARCHAR2
)
IS
  v_qry_date  VARCHAR2(15);
  v_status  VARCHAR2(1);
BEGIN
  v_qry_date := TO_CHAR(SYSDATE,'yyyymmddhh24mi');
  -- LET OP !! Omdat de verrichtingen van DMG en Metabolisch onderzoek worden gescheiden moet de naam van de Sending
  -- Application (fact_rec.send_appl) verschillend zijn
  pack_rec_ver :=  CHR(11)
               || 'MSH|^~\&|'
               || fact_rec.send_appl
               || '||HISCOM-TOREN||'
               || v_qry_date
               || '||DFT^P03|4137|P|2.2|||AL|NE'
               || CHR(13)
               || 'EVN|P03|'
               || v_qry_date
               || CHR(13)
               || 'PID|||'
               || fact_rec.pid_zisnr
               || '||'
               || fact_rec.pid_name
               || '||'
               || fact_rec.pid_birth
               || '||||'
               || fact_rec.pid_adres
               || '^^'
               || fact_rec.pid_plaats
               || '^'
               || fact_rec.pid_land
               || '^'
               || fact_rec.pid_postcd
               || CHR(13)
               || 'FT1||'
               || fact_rec.ft1_bonnr
               || '||'
               || fact_rec.ft1_verrdat
               || '|||'
               || fact_rec.ft1_verrcd
               || '||||||'
               || fact_rec.ft1_prod_afd
               || '|||'
               || fact_rec.ft1_aanvr_afd
               || '||||'
               || fact_rec.ft1_prod_spec
               || '|'
               || CHR(13)
               || 'ZFU|||||||||||'
               || fact_rec.zfu_aanvr_spec
               || '|'
               || fact_rec.zfu_abw
               || '||'
               || fact_rec.zfu_tekst
               || '|'
               || fact_rec.zfu_aantal
               || CHR(13)
               || 'IN1|||'
               || fact_rec.in1_abi_verzcd
               || '|||||||||||||||||||||||||||||||||'
               || fact_rec.in1_abi_verznr
               || CHR(13)
               || CHR(28)
               || CHR(13)
                ;
  session_id  :=  dbms_pipe.unique_session_name;
  dbms_pipe.pack_message( session_id );
  dbms_pipe.pack_message( pack_rec_ver );
  send( c_dft, v_status );
  p_status := v_status;
EXCEPTION
  WHEN OTHERS
  THEN
    dbms_output.put_line('Er is iets fout gegaan bij het samenstellen van het DFT-bericht');
END PACK_VER_2_2;

PROCEDURE UNPACK_ACK_2_2
( p_dft_ok OUT VARCHAR2
, p_status OUT VARCHAR2
)
IS
  v_status  VARCHAR2(1);
BEGIN
  receive( c_dft, v_status );
  IF v_status = c_v
  THEN
    dbms_pipe.unpack_message(unpack_rec_ack);
    dummy_rec.ack_code := SUBSTR( unpack_rec_ack
                                , INSTR( unpack_rec_ack
                                       , 'MSA|'
                                       ) + 4
                                , 2
                                );
    p_dft_ok := dummy_rec.ack_code;
    p_status := c_v;
  ELSE
    p_status := c_o;
  END IF;
END UNPACK_ACK_2_2;

PROCEDURE SEND
( p_msg IN VARCHAR2
, p_status OUT VARCHAR2
)
IS
  send_ok   NUMBER;
BEGIN
  -- ADT berichten en DFT berichten moeten naar verschillende poorten van
  -- TDM (comm server) vandaar twee verschillende pipe namen (zodat ze te
  -- herkennen zijn door het C programma
  IF p_msg = c_adt
  THEN
    dbms_pipe.purge('HL7$SEND');
    send_ok := dbms_pipe.send_message('HL7$SEND', 30);
    IF send_ok <> 0
    THEN
      raise_application_error(-20010,'Fout tijdens zenden DFT bericht. Status = '||TO_CHAR(send_ok));
      -- send_ok kan zijn:
      --  0 Success.
      --    If the pipe already exists and the user attempting to create it is authorized to use it
      --    then Oracle returns 0, indicating success, and any data already in the pipe remains.
      --    If a user connected as SYSDBS/SYSOPER re-creates a pipe, then Oracle returns status 0,
      --    but the ownership of the pipe remains unchanged.
      --  1 Timed out.
      --    This procedure can timeout either because it cannot get a lock on the pipe,
      --    or because the pipe remains too full to be used. If the pipe was implicitly-created
      --    and is empty, then it is removed.
      --  3 An interrupt occurred.
      --    If the pipe was implicitly created and is empty, then it is removed.
      --    ORA-23322  Insufficient privileges. If a pipe with the same name exists and was created
      --    by a different user, then Oracle signals error ORA-23322, indicating the naming conflict.
    ELSE
      p_status := 'X';
      -- bij de ADT-koppeling heeft de out parameter (p_status) geen betekenis
      -- maar omdat een out-parameter geen default waarde mag hebben is en blijft
      -- verplicht bij het aanroepen van de procedure. In principe is
      -- 'p_status := 'X';' dus niet nodig, als de aanroep van SEND maar twee
      -- parameters (in de vorm van variabelen) meegeeft.
    END IF;
  ELSE
    send_ok := dbms_pipe.send_message('DFT$SEND', 30);
    IF send_ok <> 0
    THEN
      p_status := c_m;
    ELSE
      p_status := c_v;
    END IF;
  END IF;
END send;

PROCEDURE RECEIVE
( p_msg IN VARCHAR2
, p_status OUT VARCHAR2
)
IS
  receive_ok  NUMBER;
BEGIN
  -- Reacties op ADT en DFT berichten komen uit verschillende poorten van de
  -- TDM (comm server) vandaar twee verschillende pipe namen (zodat ze te
  -- herkennen zijn door het C programma
  -- session_id is deel van de pipename!
  IF p_msg = c_adt
  THEN
    receive_ok := dbms_pipe.receive_message('HL7$RECEIVE_'||session_id,30);
    IF receive_ok <> 0
    THEN
      raise_application_error(-20011,'Fout tijdens ontvangen ADT reactie. Status = '||TO_CHAR(receive_ok));
      -- receive_ok kan zijn:
      --  0 Success
      --  1 Timed out. If the pipe was implicitly-created and is empty, then it is removed.
      --  2 Record in the pipe is too large for the buffer. (This should not happen.)
      --  3 An interrupt occurred.
      --  ORA-23322 User has insufficient privileges to read from the pipe.
    ELSE
      p_status := 'X';
      -- zie reden hiervoor procedure SEND(c_adt)
    END IF;
  ELSE
    receive_ok := dbms_pipe.receive_message('DFT$RECEIVE_'||session_id,30);
    IF receive_ok <> 0
    THEN
      p_status := c_m;
    ELSE
      p_status := c_v;
    END IF;
  END IF;
END receive;
END HL7_COMM;
/

/
QUIT
