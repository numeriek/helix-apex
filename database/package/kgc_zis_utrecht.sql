CREATE OR REPLACE PACKAGE "HELIX"."KGC_ZIS_UTRECHT" IS
 /******************************************************************************
    NAME:      KGC_ZIS_UTRECHT
    PURPOSE:   Utrecht specifieke functionaliteit voor KGC_ZIS

    REVISIONS:
    Ver        Date        Author           Description
    ---------  ----------  ---------------  ------------------------------------
    5.	18-02-2013 SOJ	   Mantis 0182, program uint " hl7msg2rec" is modified
    4          26-08-2009  RDE              Mantis 3185 en 2608 - diverse kleine aanpassingen
    3.1        03-06-2009  RDE              Mantis 283: zisnr dat terugkomt nav qry ook vullen
    3.0        21-04-2009  RDE              Mantis 283-verbouwing
    2.0        19-10-2006  RDE              WP55: telefoon2
    1.0        15-08-2006  RDE              creatie
 *****************************************************************************/
  FUNCTION persoon_in_zis
  ( p_zisnr IN VARCHAR2
  , pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
  , rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
  , verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
  , x_msg        OUT VARCHAR2
  ) RETURN BOOLEAN;

  PROCEDURE hl7msg2rec -- vertaal HL7-message naar rec's
  ( p_hl7_msg IN     kgc_zis.hl7_msg
  , pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
  , rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
  , verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
  );

  PROCEDURE rec2hl7msg
  ( pers_trow IN     cg$KGC_PERSONEN.cg$row_type
  , rela_trow IN     cg$KGC_RELATIES.cg$row_type
  , verz_trow IN     cg$KGC_VERZEKERAARS.cg$row_type
  , p_extra   IN     VARCHAR2
  , x_hl7_msg IN OUT kgc_zis.hl7_msg
  );

  FUNCTION verwerk_ander_bericht -- specifiek
  ( p_hlbe_id   NUMBER
  , p_hl7_msg   kgc_zis.hl7_msg
  , x_entiteit  IN OUT VARCHAR2
  , x_id        IN OUT NUMBER
  ) RETURN VARCHAR2;

  FUNCTION decl2dft_p03
  ( p_decl_id        IN     NUMBER
  , p_aantal         IN     NUMBER
  , p_check_only     IN     BOOLEAN
  , p_msg_control_id IN     VARCHAR2
  , x_msg            IN OUT VARCHAR2
  ) RETURN BOOLEAN;
END KGC_ZIS_UTRECHT;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ZIS_UTRECHT" IS
-- lokale functie... indien ook elders nodig: verhuizen naar kgc_zis!
FUNCTION getAttrSub(p_str VARCHAR2, p_sub NUMBER) RETURN VARCHAR2 IS
  -- zoek 'p_sub'-ste stuk van p_str; scheidingsteken is ampersand
  v_return VARCHAR2(4000) := p_str;
  v_pos_b  NUMBER;
  v_pos_e  NUMBER;
BEGIN
  -- begin
  IF p_sub = 1 THEN
    v_pos_b := 1; -- vooraan
  ELSE
    v_pos_b := INSTR(p_str, '&', 1, p_sub - 1);
    IF v_pos_b = 0 THEN
      RETURN '';
    ELSE
      v_pos_b := v_pos_b + 1; -- voorbij de ampersand
    END IF;
  END IF;
  -- eind
  v_pos_e := INSTR(p_str, '&', 1, p_sub);
  IF v_pos_e = 0 THEN
    v_pos_e := LENGTH(p_str) + 1;
  END IF;

  RETURN SUBSTR(p_str, v_pos_b, v_pos_e - v_pos_b);
END getAttrSub;

-- lokale procedure, want kgc_zis-versie moet voor maastricht intact blijven!
   PROCEDURE hl7msg2rela -- alleen voor Utrecht
   (p_hl7_msg kgc_zis.hl7_msg
   ,p_extra   VARCHAR2
   ,rela_trow IN OUT cg$kgc_relaties.cg$row_type) IS
      v_rol          VARCHAR2(10);
      v_PD1          VARCHAR2(10);
      v_seg_code     VARCHAR2(10);
      v_rol4seqno    NUMBER;
      v_voorletters1 VARCHAR2(100);
      v_voorletters2 VARCHAR2(100);
      v_liszcode     VARCHAR2(100);
      v_praktijkcode VARCHAR2(100);
   BEGIN
      -- op zoek naar een geschikt ROL-segement (ROL-3.1.1 = PP).
      IF p_hl7_msg.COUNT > 0
      THEN
         v_seg_code := p_hl7_msg.FIRST; -- get subscript of first element
         WHILE v_seg_code IS NOT NULL
         LOOP
            IF v_seg_code LIKE '%ROL' THEN
               IF p_hl7_msg(v_seg_code)(3)(1)(1) = 'PP' THEN
                  v_rol := v_seg_code;
                  EXIT;
               END IF;
            END IF;
            v_seg_code := p_hl7_msg.NEXT(v_seg_code); -- get subscript of next element
         END LOOP;
      END IF;
      IF nvl(length(v_rol)
            ,0) = 0
      THEN
         -- geen ROL-segment aanwezig
         RETURN;
      END IF;
      -- in ROL(4) op zoek naar namespace VEKTIS
      v_rol4seqno := 1; -- default
      FOR i IN 1..p_hl7_msg(v_rol)(4).COUNT LOOP
        IF p_hl7_msg(v_rol)(4)(i)(9) LIKE 'VEKTIS%' THEN
          v_rol4seqno := i;
        END IF;
      END LOOP;

      -- code????
      kgc_zis.splitsName(p_hl7_msg(v_rol)(4)(v_rol4seqno)(2), rela_trow.achternaam, rela_trow.voorvoegsel);
      -- liszcode
      -- hl7 v2.4 liszcode bestaat uit de agb-code + praktijknr
      -- de agb-code staat in ROL-4.n.1 waarbij n=v_rol4seqno.
      -- de praktijkcode staat in PD1-3.1.3... minus praktijksoort (01) minus evt voorloopnull
      v_PD1 := kgc_zis.getSegmentCode('PD1', p_hl7_msg);
      IF NVL(LENGTH(v_PD1), 0) > 0 THEN
        v_liszcode := p_hl7_msg(v_rol) (4) (v_rol4seqno) (1);
        v_praktijkcode := p_hl7_msg(v_PD1) (3) (1) (3);
        IF LENGTH(v_praktijkcode) >= 7 AND SUBSTR(v_praktijkcode, 1,2) = '01' THEN -- verwijder praktijksoort
          v_praktijkcode := SUBSTR(v_praktijkcode, 3);
        END IF;
        IF LENGTH(v_praktijkcode) = 6 AND SUBSTR(v_praktijkcode, 1,1) = '0' THEN -- verwijder voorloopnul
          v_praktijkcode := SUBSTR(v_praktijkcode, 2);
        END IF;
--        IF LENGTH(v_praktijkcode) = 5 THEN
         v_liszcode := v_liszcode || v_praktijkcode;
  --      END IF;
      END IF;

      IF nvl(length(v_liszcode)
            ,0) = 15
      THEN
         -- rde 6 juni 2006: indien <> 15: niet uniek, niet gebruiken!
         rela_trow.liszcode := v_liszcode;
      END IF;

      kgc_zis.gethl7waarde(p_hl7_msg(v_rol)(4)(v_rol4seqno)(3)
                          ,v_voorletters1);
      kgc_zis.gethl7waarde(p_hl7_msg(v_rol)(4)(v_rol4seqno)(4)
                          ,v_voorletters2);
      rela_trow.voorletters := v_voorletters1 || v_voorletters2;

      kgc_zis.gethl7waarde(p_hl7_msg(v_rol)(4)(v_rol4seqno)(7)
                          ,rela_trow.briefaanhef);
      kgc_zis.gethl7waarde(p_hl7_msg(v_rol) (11) (1) (1)
                          ,rela_trow.adres);
      kgc_zis.gethl7waarde(p_hl7_msg(v_rol) (11) (1) (3)
                          ,rela_trow.woonplaats);
      kgc_zis.gethl7waarde(p_hl7_msg(v_rol) (11) (1) (5)
                          ,rela_trow.postcode);
      kgc_zis.gethl7waarde(p_hl7_msg(v_rol) (12) (1) (1)
                          ,rela_trow.telefoon);

      kgc_formaat_00.standaard_aanspreken(p_type        => 'RELA'
                                         ,p_voorletters => rela_trow.voorletters
                                         ,p_voorvoegsel => rela_trow.voorvoegsel
                                         ,p_achternaam  => rela_trow.achternaam
                                         ,p_prefix      => ltrim(rela_trow.briefaanhef || ' ')
                                         ,x_aanspreken  => rela_trow.aanspreken);
      -- vervallen
      rela_trow.vervallen := nvl(rela_trow.vervallen
                                ,'N'); -- default
      IF nvl(p_hl7_msg(v_rol) (3) (1) (1)
            ,'') <> ''
      THEN
         rela_trow.vervallen := 'J';
      END IF;
      -- postcode/provincie/land: lokaal: eerst evt bestaande land ophalen
      -- */
   END;


FUNCTION persoon_in_zis
( p_zisnr   IN     VARCHAR2
, pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
, x_msg        OUT VARCHAR2
) RETURN BOOLEAN
IS
  v_sypa_code_host  kgc_systeem_parameters.standaard_waarde%type;
  v_hl7_svr_poort_qry NUMBER := kgc_zis.getHL7Poort('ZIS_HL7_SVR_POORT_QRY');
  v_strHL7in        VARCHAR2(32767);
  v_strAntw         VARCHAR2(32767);
  v_ok              BOOLEAN;
  v_MSA             VARCHAR2(10);
  v_MSH             VARCHAR2(10);
  v_hl7_msg_bron    kgc_zis.hl7_msg;
  v_hl7_msg_res     kgc_zis.hl7_msg;
  v_timeout         NUMBER := 100; -- 10 seconden
  v_msg_control_id  VARCHAR2(10);
  v_sysdate         varchar2(15) := to_char(sysdate,'yyyymmddhh24mi');
  v_qry_zis         varchar2(10) := replace(p_zisnr, '.'); -- geen puntjes!
  v_thisproc        VARCHAR2(100) := 'kgc_zis_utrecht.persoon_in_zis';
  v_hl7_versie      VARCHAR2(10) := '2.4';
BEGIN
  kgc_zis.log(v_thisproc || ': Start!');
  v_sypa_code_host := kgc_zis.get_sypa_sywa('ZIS_HL7_SVR_NAAM_QRY', TRUE, TRUE, v_sypa_code_host);
  dbms_output.put_line('ZIS_HL7_SVR_NAAM_QRY = ' || v_sypa_code_host);
  dbms_output.put_line('ZIS_HL7_SVR_POORT_QRY = ' || to_char(v_hl7_svr_poort_qry));
  v_msg_control_id := SUBSTR(TO_CHAR(abs(dbms_random.random)), 1, 10);

  -- **** MSH
  v_hl7_msg_bron('MSH') := kgc_zis.init_msg('MSH', v_hl7_versie);
  v_hl7_msg_bron('MSH')(1)(1)(1) := '|';
  v_hl7_msg_bron('MSH')(2)(1)(1) := '^~\&';
  v_hl7_msg_bron('MSH')(3)(1)(1) := 'DMG-ORACLE';
  v_hl7_msg_bron('MSH')(5)(1)(1) := 'ZIS ADT';
  v_hl7_msg_bron('MSH')(7)(1)(1) := v_sysdate;
  v_hl7_msg_bron('MSH')(9)(1)(1) := 'QRY^Q01';
  v_hl7_msg_bron('MSH')(10)(1)(1) := v_msg_control_id;
  v_hl7_msg_bron('MSH')(11)(1)(1) := 'P';
  -- mantis 283: v 2.4 voor Utrecht
  v_hl7_msg_bron('MSH')(12)(1)(1) := v_hl7_versie;
  v_hl7_msg_bron('MSH')(15)(1)(1) := 'NE';
  v_hl7_msg_bron('MSH')(16)(1)(1) := 'AL';

  -- **** QRD
  v_hl7_msg_bron('QRD') := kgc_zis.init_msg('QRD', v_hl7_versie);
  v_hl7_msg_bron('QRD')(1)(1)(1) := v_sysdate;
  v_hl7_msg_bron('QRD')(2)(1)(1) := 'R';
  v_hl7_msg_bron('QRD')(3)(1)(1) := 'I';
  v_hl7_msg_bron('QRD')(4)(1)(1) := '0000000001';
  v_hl7_msg_bron('QRD')(8)(1)(1) := v_qry_zis; -- PI is default, dus niet nodig
  -- mantis 283; in 2.4 is voor QRD-9 NAW geen goede waarde; dan DEM
  v_hl7_msg_bron('QRD')(9)(1)(1) := 'DEM';

  v_strHL7in := kgc_zis.g_sob_char
    || kgc_zis.hl7seg2str('MSH', v_hl7_msg_bron('MSH')) || kgc_zis.g_cr
    || kgc_zis.hl7seg2str('QRD', v_hl7_msg_bron('QRD')) || kgc_zis.g_cr
    || kgc_zis.g_eob_char || kgc_zis.g_term_char
    ;
  v_ok := kgc_zis.send_msg
  ( p_host => v_sypa_code_host
  , p_port => to_char(v_hl7_svr_poort_qry)
  , p_strHL7in  => v_strHL7in
  , p_timeout => v_timeout
  , x_strHL7out => v_strAntw
  , x_msg => x_msg
  );
  kgc_zis.log(v_strAntw, '1100XXXXXX');
  IF NOT v_ok THEN
    RETURN FALSE; -- msg al gevuld
  END IF;
  v_hl7_msg_res := kgc_zis.str2hl7msg(0, v_strAntw);
  v_MSA := kgc_zis.getSegmentCode('MSA', v_hl7_msg_res);
  v_MSH := kgc_zis.getSegmentCode('MSH', v_hl7_msg_res);
  IF NVL(LENGTH(v_MSA), 0) = 0 OR NVL(LENGTH(v_MSH), 0) = 0 THEN
    x_msg := 'Ophalen gegevens NIET gelukt! (MSA- of MSH-segment afwezig)';
    RETURN FALSE;
  END IF;
  IF NVL(v_hl7_msg_res(v_MSA)(1)(1)(1), 'XX') <> 'AA' THEN
    x_msg := 'Ophalen gegevens NIET gelukt! (Code MSA-1: '
      || v_hl7_msg_res(v_MSA)(1)(1)(1) || '; Tekst: '
      || v_hl7_msg_res(v_MSA)(3)(1)(1) || ')';
    RETURN FALSE;
  ELSIF v_hl7_msg_res(v_MSH)(10)(1)(1) <> v_msg_control_id THEN -- komt 'nooit' voor
    x_msg := 'Fout bij ophalen gegevens: mismatch control-id! Verzonden: '
      || v_msg_control_id || '; Ontvangen: '
      || v_hl7_msg_res(v_MSH)(10)(1)(1) || '!';
    RETURN FALSE;
  END IF;

  -- omzetten naar input-parms (ook nodig voor A08!)
  hl7msg2rec
  ( p_hl7_msg => v_hl7_msg_res
  , pers_trow => pers_trow
  , rela_trow => rela_trow
  , verz_trow => verz_trow
  );

  x_msg := 'OK';
  RETURN TRUE;
EXCEPTION
  WHEN OTHERS THEN
    x_msg := v_thisproc || ' Error: ' || SQLERRM;
    RETURN FALSE;
END;

----------------------------------------------------------------------------
PROCEDURE hl7msg2rec -- vertaal HL7-message naar rec's
( p_hl7_msg IN     kgc_zis.hl7_msg
, pers_trow IN OUT cg$KGC_PERSONEN.cg$row_type
, rela_trow IN OUT cg$KGC_RELATIES.cg$row_type
, verz_trow IN OUT cg$KGC_VERZEKERAARS.cg$row_type
) IS
  CURSOR c_rela
  ( p_rela_trow IN cg$KGC_RELATIES.cg$row_type
  )
  IS
    -- in Utrecht wordt de huisarts geidentificeerd door
    -- 1. VERVALLEN!!! de interne code (STF-1.4) (opgeslagen met prefix ZIS0)
    -- 2. de LISZ-code
    -- 3. al bestaand is zonder interne/LISZ-code => dan check via attributen
    SELECT 2 sorteer
    ,      rela.rela_id
    FROM   kgc_relaties rela
    WHERE  rela.liszcode = p_rela_trow.liszcode
    UNION
    SELECT 3
    ,      rela.rela_id
    FROM   kgc_relaties rela
    WHERE  rela.achternaam = p_rela_trow.achternaam
    AND    NVL(UPPER(rela.voorletters), '@#@') = NVL(UPPER(p_rela_trow.voorletters), '@#@')
    AND    NVL(UPPER(rela.voorvoegsel), '@#@') = NVL(UPPER(p_rela_trow.voorvoegsel), '@#@')
    AND    ( rela.postcode = p_rela_trow.postcode OR
             rela.adres || rela.woonplaats = p_rela_trow.adres || p_rela_trow.woonplaats
           ) -- bah: buitenland: geen lisz; postcode kan leeg zijn!
    AND    rela.relatie_type = 'HA'
    ORDER BY 1
    ;

  cursor c_verz
  ( p_code VARCHAR2
  ) is
    select verz_id
    from   kgc_verzekeraars verz
    where  verz.code = p_code
    ;
  TYPE t_tab IS TABLE OF VARCHAR2(200) INDEX BY BINARY_INTEGER;
  v_telf_tab         t_tab;
  v_rela_id          NUMBER;
  v_rela_code        VARCHAR2(20);
  v_verz_id          NUMBER;
  v_pat_gehuwd       KGC_PERSONEN.GEHUWD%TYPE;
  v_PID              VARCHAR2(10);
  v_IN1              VARCHAR2(10);
  v_partnernaam      VARCHAR2(200);
  v_partnernaam_pref VARCHAR2(200);
  v_roepnaam         VARCHAR2(200);
  v_voornaam1        VARCHAR2(200);
  v_pos              NUMBER;
  v_dummy            NUMBER;
  v_tmp              VARCHAR2(4000);
  v_tmp2             VARCHAR2(4000);
  v_pid11_type       NUMBER;
  v_pid11_typeM      NUMBER;
  v_pid11_typeH      NUMBER;
  v_msg              VARCHAR2(2000);
  v_debug            VARCHAR2(100);
  v_thisproc         VARCHAR2(100) := 'kgc_zis_utrecht.hl7msg2rec';
  v_str VARCHAR2(32677);
BEGIN
  -- A08 Utr/Maas: MSH EVN PID ZPI PV1 { IN1 [IN2] } STF
  -- QRY Q01 Utr/Maas: MSH MSA QRD [ PID [ MRG ] ZPI [ PV1 { IN1 [IN2] } STF ] ]
  v_debug := '1';
--****** testing
  v_str := kgc_zis.hl7msg2userstr(p_hl7_msg);
  kgc_zis.log(v_str, '1100');
--****** testing

  v_debug := '1a';
  v_PID := kgc_zis.getSegmentCode('PID', p_hl7_msg);
  v_IN1 := kgc_zis.getSegmentCode('IN1', p_hl7_msg);

  -- hl7 nl profile!
  IF NVL(LENGTH(v_PID), 0) > 0 THEN
    -- *** ZISnr - nieuw - mantis 283
    -- *** BSN - nieuw - mantis 283
    FOR i IN 1 .. p_hl7_msg(v_PID) (3).COUNT LOOP
      IF p_hl7_msg(v_PID) (3) (i) (5) = 'PI' THEN -- zisnr
        kgc_zis.getHL7waarde(p_hl7_msg(v_PID) (3) (i) (1), pers_trow.zisnr);
        pers_trow.zisnr := kgc_zis.formatteer_zisnr_ut(pers_trow.zisnr);
      ELSIF p_hl7_msg(v_PID) (3) (i) (5) = 'NNNLD' THEN -- BSN
        kgc_zis.getHL7waarde(p_hl7_msg(v_PID) (3) (i) (1), v_tmp);
        IF LENGTH(v_tmp) > 0 THEN -- overnemen!
          pers_trow.bsn := TO_NUMBER(v_tmp);
          pers_trow.bsn_geverifieerd := 'J';
        ELSE --
          pers_trow.bsn := NULL;
          pers_trow.bsn_geverifieerd := 'N';
        END IF;
      END IF;
    END LOOP;

    -- *** geboortedatum / part_geboortedatum
    IF nvl(p_hl7_msg(v_PID) (7) (1) (1)
          ,'00000000') = '00000000'
    THEN
      pers_trow.geboortedatum := NULL;
    ELSE
      DECLARE
        v_jaar  VARCHAR2(4);
        v_maand VARCHAR2(2);
      BEGIN
        pers_trow.geboortedatum      := to_date(p_hl7_msg(v_PID) (7) (1) (1)
                                               ,'YYYYMMDD');
        pers_trow.part_geboortedatum := NULL;
      EXCEPTION
        WHEN OTHERS THEN
          -- opbergen in part_geboortedatum
          pers_trow.geboortedatum      := NULL;
          pers_trow.part_geboortedatum := NULL;
          v_jaar                       := ltrim(rtrim(substr(p_hl7_msg(v_PID) (7) (1) (1)
                                                            ,1
                                                            ,4)));
          v_jaar                       := REPLACE(v_jaar
                                                 ,'0000'
                                                 ,'');
          IF nvl(length(v_jaar)
                ,0) > 0
          THEN
            -- iets aanwezig > gebruiken
            pers_trow.part_geboortedatum := '00-'; -- dag
            -- maand
            v_maand := ltrim(rtrim(substr(p_hl7_msg(v_PID) (7) (1) (1)
                                         ,5
                                         ,2)));
            v_maand := REPLACE(v_maand
                              ,'00'
                              ,'');
            IF nvl(length(v_maand)
                  ,0) > 0
            THEN
              pers_trow.part_geboortedatum := pers_trow.part_geboortedatum ||
                                              lpad(v_maand
                                                  ,2
                                                  ,'0') || '-';
            ELSE
              pers_trow.part_geboortedatum := pers_trow.part_geboortedatum || '00-';
            END IF;
            -- jaar
            pers_trow.part_geboortedatum := pers_trow.part_geboortedatum ||
                                            lpad(v_jaar
                                                ,4
                                                ,'0');
          END IF;
      END;
    END IF;
    -- *** geslacht
    IF p_hl7_msg(v_PID) (8) (1) (1) = 'M'
    THEN
       pers_trow.geslacht := 'M';
    ELSIF p_hl7_msg(v_PID) (8) (1) (1) IN ('V'
                   ,'F')
    THEN
       pers_trow.geslacht := 'V';
    ELSE
       pers_trow.geslacht := 'O';
    END IF;
    -- *** meerling
    IF p_hl7_msg(v_PID) (24) (1) (1) IN ('Y'
                ,'J')
    THEN
       pers_trow.meerling := 'J';
    ELSE
       pers_trow.meerling := 'N';
    END IF;
    -- *** adres
    -- mantis 283 - adresformaat geheel gewijzigd
    -- binnen pid-11 opzoek naar type M (mailing) of H (home)
    v_pid11_type := 0;
    v_pid11_typeM := 0;
    v_pid11_typeH := 0;
    FOR i IN 1 .. p_hl7_msg(v_PID) (11).COUNT LOOP
      IF p_hl7_msg(v_PID) (11)(i).COUNT >= 7 THEN
        IF p_hl7_msg(v_PID) (11)(i)(7) = 'M' THEN
          v_pid11_typeM := i;
        ELSIF p_hl7_msg(v_PID) (11)(i)(7) = 'H' THEN
          v_pid11_typeH := i;
        ELSE
          v_pid11_type := i;
        END IF;
      END IF;
    END LOOP;

    IF v_pid11_typeH > 0 THEN
      v_pid11_type := v_pid11_typeH;
    ELSIF v_pid11_typeM > 0 THEN
      v_pid11_type := v_pid11_typeM;
    END IF;

    IF v_pid11_type > 0 THEN
      kgc_zis.getHL7waarde(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(11)(v_pid11_type)(1), 2), 1, 250), pers_trow.adres);
      v_tmp := NULL; -- huisnummer
      kgc_zis.getHL7waarde(getAttrSub(p_hl7_msg(v_PID)(11)(v_pid11_type)(1), 3), v_tmp);
      IF LENGTH(v_tmp) > 0 THEN
        pers_trow.adres := pers_trow.adres || ' ' || v_tmp;
      END IF;
      IF nvl(length(p_hl7_msg(v_pid) (11) (v_pid11_type) (2)),0) > 0
      THEN -- huisnr-toevoeging aanwezig
        pers_trow.adres := pers_trow.adres || ' ' || ltrim(p_hl7_msg(v_PID) (11) (v_pid11_type) (2));
      END IF;
      pers_trow.woonplaats := rtrim(substr(p_hl7_msg(v_PID) (11) (v_pid11_type) (3),1,50));
      kgc_formaat_00.formatteer(p_naamdeel => 'WOONPLAATS'
                               ,x_naam     => pers_trow.woonplaats);
      kgc_zis.gethl7waarde(rtrim(substr(p_hl7_msg(v_PID) (11) (v_pid11_type) (5),1,7))
                          ,pers_trow.postcode); -- formattering: specifiek!!!
      -- land
      IF p_hl7_msg(v_PID) (11) (v_pid11_type) (6) = 'NLD' THEN
        pers_trow.land := 'Nederland';
      ELSE
        pers_trow.land := NULL; -- waarschijnlijk land toegevoegd aan woonplaats...
      END IF;
    END IF;
    -- *** naam: gebruik altijd de Legal name - instance 1 van pid-5
    v_debug := '2';
    -- achternaam+
    kgc_zis.getHL7waarde(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(1)(1), 3), 1, 30), pers_trow.achternaam);
    kgc_zis.getHL7waarde(LOWER(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(1)(1), 2), 1, 10)), pers_trow.voorvoegsel);
    -- *** partner naam
    kgc_zis.getHL7waarde(LOWER(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(1)(1), 4), 1, 10)), pers_trow.voorvoegsel_partner);
    kgc_zis.getHL7waarde(SUBSTR(getAttrSub(p_hl7_msg(v_PID)(5)(1)(1), 5), 1, 30), pers_trow.achternaam_partner);
    -- *** voorletters / eerste VOORnaam
    v_debug := '3';
    kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(5)(1)(2), v_voornaam1);
    kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(5)(1)(3), pers_trow.voorletters);
    v_debug := v_debug || 'a';
    IF NVL(LENGTH(v_voornaam1), 0) > 0 THEN
      pers_trow.voorletters := SUBSTR(v_voornaam1, 1, 1) || '.' || pers_trow.voorletters;
      v_debug := v_debug || 'b';
      -- indien leeftijd < 18: voornaam!
      IF pers_trow.geboortedatum IS NOT NULL THEN
        IF months_between(sysdate, pers_trow.geboortedatum) < 216 THEN
       --    pers_trow.voorletters := SUBSTR(v_voornaam1, 1, 10); -- commented by saroj, mantis 0182
           pers_trow.voorletters := SUBSTR(v_voornaam1, 1, 30); -- added against previous commented row
        END IF;
      END IF;
    END IF;

    -- *** roepnaam
    v_roepnaam := NULL;
    -- te vinden in PID-5-x-2 waarbij PID-5-x-7 = "N" (nickname)
    -- *** gehuwd
    -- met deze vertaling krijgen alle weduwen en gescheiden mensen ook de status 'N' (niet gehuwd)
    v_debug := '4.1';
    IF p_hl7_msg(v_PID)(16)(1)(1) = 'M' THEN
      pers_trow.gehuwd := 'J';
    ELSE
      pers_trow.gehuwd := 'N';
    END IF;

    -- *** default_aanspreken
    v_debug := '5';
    IF NVL(LENGTH(pers_trow.default_aanspreken), 0) = 0 THEN
      pers_trow.default_aanspreken := 'J';
    END IF;
    -- *** aanspreken
    IF pers_trow.default_aanspreken = 'J'
    THEN
      kgc_formaat_00.standaard_aanspreken
      ( p_type => 'PERS'
      , p_voorletters => pers_trow.voorletters
      , p_voorvoegsel => pers_trow.voorvoegsel
      , p_achternaam => pers_trow.achternaam
      , p_achternaam_partner => pers_trow.achternaam_partner
      , p_voorvoegsel_partner => pers_trow.voorvoegsel_partner
      , p_gehuwd => pers_trow.gehuwd
      , p_geslacht => pers_trow.geslacht
      , x_aanspreken => pers_trow.aanspreken
      );
    END IF;

    -- formateren...
    kgc_zis.format_geo
    ( x_postcode => pers_trow.postcode
    , x_woonplaats => pers_trow.woonplaats
    , x_provincie => pers_trow.provincie
    , x_land => pers_trow.land
    , x_msg => v_msg
    );

    -- *** overleden
    -- in 2.4 komt dit niet meer uit het ZPI-segment, maar uit het pid-segment!
    v_debug := '7';
    IF p_hl7_msg(v_PID)(30)(1)(1) = 'Y' THEN
      pers_trow.overleden := 'J';
      BEGIN
        pers_trow.overlijdensdatum := TO_DATE(SUBSTR(p_hl7_msg(v_PID)(29)(1)(1),1,8), 'YYYYMMDD');
      EXCEPTION
        WHEN OTHERS THEN NULL;
      END;
    ELSE
      pers_trow.overleden := 'N';
      pers_trow.overlijdensdatum := NULL;
    END IF;
    -- *** telefoon; 2.4: zowel pid-13 als 14 zijn herhalende groepen!
    v_debug := '8';
    -- verzamel alle telefoonnummers, te beginnen bij pid-13, de prive-nummers
    v_telf_tab.delete;
    FOR i IN 1.. p_hl7_msg(v_PID)(13).COUNT LOOP
      IF p_hl7_msg(v_PID)(13)(i).COUNT >= 2 THEN
        IF p_hl7_msg(v_PID)(13)(i)(2) IN ('PRN', 'ORN') THEN
          v_tmp := NULL;
          kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(13)(i)(1), v_tmp);
          IF LENGTH(v_tmp) > 0 THEN
            v_telf_tab(v_telf_tab.count + 1) := SUBSTR(v_tmp, 1, 30);
          END IF;
        END IF;
      END IF;
    END LOOP;
    FOR i IN 1.. p_hl7_msg(v_PID)(14).COUNT LOOP
      IF p_hl7_msg(v_PID)(14)(i).COUNT >= 2 THEN
        IF p_hl7_msg(v_PID)(14)(i)(2) IN ('WPN') THEN
          v_tmp := NULL;
          kgc_zis.getHL7waarde(p_hl7_msg(v_PID)(14)(i)(1), v_tmp);
          IF LENGTH(v_tmp) > 0 THEN
            v_telf_tab(v_telf_tab.count + 1) := SUBSTR(v_tmp, 1, 30);
          END IF;
        END IF;
      END IF;
    END LOOP;
    -- toelichting netter maken: "06-43945846Cmobiel moeder" moet worden "06-43945846 mobiel moeder".
    IF v_telf_tab.COUNT > 0 THEN
      FOR i IN 1 .. v_telf_tab.COUNT LOOP
        v_pos := INSTR(v_telf_tab(i), 'C');
        IF v_pos > 0 THEN
          v_telf_tab(i) := SUBSTR(v_telf_tab(i), 1, v_pos - 1) || ' ' || SUBSTR(v_telf_tab(i), v_pos + 1);
        END IF;
      END LOOP;
    END IF;
    pers_trow.telefoon := NULL;
    pers_trow.telefoon2 := NULL;
    IF v_telf_tab.COUNT >= 1 THEN
      pers_trow.telefoon := v_telf_tab(1);
    END IF;
    IF v_telf_tab.COUNT >= 2 THEN
      pers_trow.telefoon2 := v_telf_tab(2);
    END IF;
  END IF;

  -- huisarts naar rela_trow
  v_debug := '9';
  v_debug := v_debug || 'a';
  -- zoek-argumenten ophalen
  hl7msg2rela
  ( p_hl7_msg
  , ''
  , rela_trow
  );
  -- code overnemen tbv zoek is verwijderd - rde 6 juni 2006, want: niet uniek!
  v_debug := v_debug || 'b';
  -- postcode ed formatteren
  kgc_zis.format_geo
  ( x_postcode => rela_trow.postcode
  , x_woonplaats => rela_trow.woonplaats
  , x_provincie => rela_trow.provincie
  , x_land => rela_trow.land
  , x_msg => v_msg
  );
  -- check of rela al aanwezig is: zie c_rela
  open c_rela(rela_trow);
  v_debug := v_debug || 'c';
  fetch c_rela into v_dummy, rela_trow.rela_id;
  IF c_rela%NOTFOUND THEN
    v_debug := v_debug || 'd';
    NULL;
  END IF;
  close c_rela;
  v_debug := v_debug || 'e';
  IF rela_trow.rela_id IS NOT NULL THEN -- relatie bestaat!
    v_debug := v_debug || 'f';
    -- eerst bestaande gegevens ophalen (incl. code!!!)
    cg$kgc_relaties.slct(rela_trow);
    pers_trow.rela_id := rela_trow.rela_id; -- opnemen bij persoon!
    -- daarna weer de nieuwe gegevens ophalen
    kgc_zis.hl7msg2rela
    ( p_hl7_msg
    , ''
    , rela_trow
    );
    -- postcode ed formatteren
    kgc_zis.format_geo
    ( x_postcode => rela_trow.postcode
    , x_woonplaats => rela_trow.woonplaats
    , x_provincie => rela_trow.provincie
    , x_land => rela_trow.land
    , x_msg => v_msg
    );
  END IF;

  -- verzekeraar naar verz_trow
  -- oei - hoe om te gaan met meerdere IN1-segmenten? Antwoord: altijd de 1e, los van Veld 15!
  v_debug := '9';
  IF NVL(LENGTH(v_IN1), 0) > 0 THEN -- IN1 is optioneel!
    -- *** persoon - verzekeringsnummer
    v_debug := v_debug || 'a';
    kgc_zis.getHL7waarde(p_hl7_msg(v_IN1)(36)(1)(1), pers_trow.verzekeringsnr);
    -- *** verzekeringswijze
    v_debug := v_debug || 'b';
    -- verzekeringswijze
    pers_trow.verzekeringswijze := 'B'; -- mantis 283
    -- *** verzekeraar
    v_debug := v_debug || 'c';
    verz_trow.code := REPLACE(p_hl7_msg(v_IN1)(3)(1)(1), '"', ''); -- leeg of "" - geen verzekeraar!
    IF NVL(LENGTH(verz_trow.code), 0) > 0 THEN -- geen code (veld is verplicht!): geen verzekeraar!!!
      v_debug := v_debug || 'b';
      -- en wordt de rela.code automatisch uitgegeven -
      open c_verz(verz_trow.code);
      fetch c_verz into v_verz_id;
      IF c_verz%NOTFOUND THEN
        NULL;
        v_debug := v_debug || 'e';
      ELSE
        verz_trow.verz_id := v_verz_id;
        cg$kgc_verzekeraars.slct(verz_trow); -- bestaande gegevens ophalen
        pers_trow.verz_id := verz_trow.verz_id; -- opnemen bij persoon!
        v_debug := v_debug || 'h';
      END IF;
      close c_verz;

      v_debug := v_debug || 'k';
      -- op de (evt. opgehaalde) Helix-waarden de veranderingen uitvoeren:
      kgc_zis.hl7msg2verz
      ( p_hl7_msg
      , ''
      , verz_trow
      );
      -- hmmm in Utrecht wordt de AGB-code in de naam meegestuurd (aan het eind) - weg ermee!
      v_pos := INSTR(verz_trow.naam, '  AGB ', 48);
      IF v_pos = 0 THEN
        v_pos := INSTR(verz_trow.naam, '    ', 48); -- evt zonder agb
      END IF;
      IF v_pos > 0 THEN
        verz_trow.naam := RTRIM(SUBSTR(verz_trow.naam, 1, v_pos));
      END IF;

      kgc_zis.format_geo
      ( x_postcode => verz_trow.postcode
      , x_woonplaats => verz_trow.woonplaats
      , x_provincie => verz_trow.provincie
      , x_land => verz_trow.land
      , x_msg => v_msg
      );
    END IF;
  END IF;

  -- *** wie heeft de wijziging uitgevoerd?
  pers_trow.last_updated_by := 'ZIS'; -- wordt niet meegeleverd!

  v_debug := '99';
EXCEPTION
  WHEN OTHERS THEN
    kgc_zis.log(v_thisproc || ': ' || v_debug || '/' || SQLERRM);
  RAISE; -- door naar aanroeper
END;

-- deze wordt b.v. gebruikt om in aanleverende berichten een persoon te identificeren
-- indien een segment niet aanwezig is in x_hl7_msg, dan wordt deze niet gevuld!
-- aangepast voor mantis 283: de hl7-versie gaat naar 2.4
PROCEDURE rec2hl7msg
( pers_trow IN     cg$KGC_PERSONEN.cg$row_type
, rela_trow IN     cg$KGC_RELATIES.cg$row_type
, verz_trow IN     cg$KGC_VERZEKERAARS.cg$row_type
, p_extra   IN     VARCHAR2
, x_hl7_msg IN OUT kgc_zis.hl7_msg
) IS
  v_PID              VARCHAR2(10);
  v_IN1              VARCHAR2(10);
  v_ROL              VARCHAR2(10);
  v_debug            VARCHAR2(100);
  v_thisproc         VARCHAR2(100) := 'kgc_zis_utrecht.rec2hl7msg';
BEGIN
  v_debug := '1';
  v_PID := kgc_zis.getSegmentCode('PID', x_hl7_msg);
  v_IN1 := kgc_zis.getSegmentCode('IN1', x_hl7_msg);
  v_ROL := kgc_zis.getSegmentCode('ROL', x_hl7_msg);
  IF NVL(LENGTH(v_PID), 0) > 0 THEN
    v_debug := v_debug || '2';

    -- *** aanvullend op generieke velden:
    -- *** geslacht
    IF pers_trow.geslacht = 'M' THEN
      x_hl7_msg('PID')(8)(1)(1) := pers_trow.geslacht;
    ELSIF pers_trow.geslacht = 'V' THEN
      x_hl7_msg('PID')(8)(1)(1) := 'F';
    END IF; -- en anders: leeg!
    -- *** meerling
    IF pers_trow.meerling = 'J' THEN
      x_hl7_msg(v_PID)(24)(1)(1) := 'Y';
    ELSE
      x_hl7_msg(v_PID)(24)(1)(1) := 'N';
    END IF;
    -- *** ZISnr / BSN
    x_hl7_msg(v_PID)(3)(1)(1) := REPLACE(pers_trow.zisnr, '.', '') || '^^^helix^PI~' || to_char(pers_trow.bsn) || '^^^^NNNLD';
    -- *** BSN geverifieerd
    -- *** naam / partner naam
    x_hl7_msg(v_PID)(5)(1)(1) := '&' || pers_trow.voorvoegsel || '&' || pers_trow.achternaam
      || '&'  || pers_trow.voorvoegsel_partner || '&' || pers_trow.achternaam_partner;
    IF x_hl7_msg(v_PID)(5)(1).COUNT < 2 THEN x_hl7_msg(v_PID)(5)(1).EXTEND; END IF;
    IF x_hl7_msg(v_PID)(5)(1).COUNT < 3 THEN x_hl7_msg(v_PID)(5)(1).EXTEND; END IF;
    IF INSTR(pers_trow.voorletters, '.') = 0 THEN
      x_hl7_msg(v_PID)(5)(1)(2) := pers_trow.voorletters;
    ELSE
      x_hl7_msg(v_PID)(5)(1)(3) := pers_trow.voorletters; -- 'voornaam'
    END IF;
    v_debug := v_debug || '3';
    -- *** gehuwd
    IF pers_trow.gehuwd = 'J' THEN
      x_hl7_msg(v_PID)(16)(1)(1) := 'M';
    END IF;

    -- *** adres
    -- adres - ai, in Helix is straat en huisnummer niet gescheiden; -in US-profile HL7 ook niet, maar het NL-profile wordt gebruikt!

    x_hl7_msg(v_PID)(11)(1)(1) := REPLACE(pers_trow.adres, CHR(10), ' ') || '&&^^' || pers_trow.woonplaats || '^^';
    IF nvl(upper(pers_trow.land), 'NEDERLAND') = 'NEDERLAND' THEN
      x_hl7_msg(v_PID)(11)(1)(1) := x_hl7_msg(v_PID)(11)(1)(1) || REPLACE(pers_trow.postcode, ' ', '') || '^NLD^H';
    ELSE
      x_hl7_msg(v_PID)(11)(1)(1) := x_hl7_msg(v_PID)(11)(1)(1) || pers_trow.postcode || '^^H';
    END IF;
    v_debug := v_debug || '4';

    -- *** overleden
    v_debug := v_debug || '5';
    IF pers_trow.overlijdensdatum IS NOT NULL THEN
      x_hl7_msg(v_PID)(29)(1)(1) := to_char(pers_trow.overlijdensdatum, 'YYYYMMDD');
    END IF;
    IF pers_trow.overleden = 'J' THEN
      x_hl7_msg(v_PID)(30)(1)(1) := 'Y';
    ELSE
      x_hl7_msg(v_PID)(30)(1)(1) := 'N';
    END IF;
    -- *** telefoon
    -- 030-1234567^PRN^PH~06-12345678^ORN^PH~030-3569999X1234^ORN^PH~030-3564488^ORN^PH~^NET^internet^mijnemail@hetnet.nl
    x_hl7_msg(v_PID)(13)(1)(1) := pers_trow.telefoon || '^PRN^PH';
    x_hl7_msg(v_PID)(14)(1)(1) := pers_trow.telefoon2 || '^WPN^PH';

    IF NVL(LENGTH(v_IN1), 0) > 0 THEN
      -- *** persoon - verzekeringsnummer
      v_debug := v_debug || '6';
      x_hl7_msg(v_IN1)(36)(1)(1) := pers_trow.verzekeringsnr;
    END IF;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE = -20000 THEN
      raise; -- bevat al toelichting
    ELSE
      raise_application_error(-20000, v_thisproc || ': Debug: ' || v_debug || '; Error: ' || SQLERRM);
    END IF;
END;

----------------------------------------------------------------------------
FUNCTION verwerk_ander_bericht -- specifiek
( p_hlbe_id   NUMBER
, p_hl7_msg   kgc_zis.hl7_msg
, x_entiteit  IN OUT VARCHAR2
, x_id        IN OUT NUMBER
) RETURN VARCHAR2 IS
----------------------------------------------------------------------------
  v_ret_status       kgc_hl7_berichten.status%TYPE := 'X';
  v_debug            VARCHAR2(100);
  v_thisproc         VARCHAR2(100) := 'kgc_zis_utrecht.verwerk_ander_bericht';
BEGIN
  -- nog geen specifieke berichten aanwezig voor utrecht
  -- komt wel: koppeling met MOSOS gaat via HL7
  RETURN v_ret_status;
END;

FUNCTION decl2dft_p03
( p_decl_id        IN     NUMBER
, p_aantal         IN     NUMBER
, p_check_only     IN     BOOLEAN
, p_msg_control_id IN     VARCHAR2
, x_msg            IN OUT VARCHAR2
) RETURN BOOLEAN IS
----------------------------------------------------------------------------
BEGIN
  RETURN FALSE; -- deze logica was al verplaatst naar kgc_zis_utrecht; hier weggehaald tegelijk met mantis 283 ter voorkoming verwarring
END;
END KGC_ZIS_UTRECHT;
/

/
QUIT
