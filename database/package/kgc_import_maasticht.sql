set define off
CREATE OR REPLACE PACKAGE       KGC_IMPORT_MAASTRICHT IS

   PROCEDURE vertaal_naar_helix(p_imfi_id NUMBER, x_imfi_verslag IN OUT VARCHAR2);

   FUNCTION specifieke_verwerking(p_imfi cg$kgc_import_files.cg$row_type
                                 ,p_plekvanaanroep VARCHAR2
                                 ,x_imfi_verslag IN OUT VARCHAR2
                                 ) RETURN VARCHAR2;

END KGC_IMPORT_MAASTRICHT;
/


CREATE OR REPLACE PACKAGE BODY HELIX.KGC_IMPORT_MAASTRICHT IS

   PROCEDURE vertaal_naar_helix(p_imfi_id NUMBER, x_imfi_verslag IN OUT VARCHAR2) IS
   BEGIN
      NULL;
   END vertaal_naar_helix;

   FUNCTION specifieke_verwerking(p_imfi cg$kgc_import_files.cg$row_type
                                 ,p_plekvanaanroep VARCHAR2 -- VOOR, NA_CHECK, NA_VERW
                                 ,x_imfi_verslag IN OUT VARCHAR2
                                 ) RETURN VARCHAR2 IS
$IF (kgc_coco_00.v_is_locatie_maastricht)
$THEN
      CURSOR c_imon(p_imfi_id NUMBER, p_imon_id VARCHAR2) IS
         SELECT *
           FROM kgc_im_onderzoeken imon
          WHERE imon.imfi_id = p_imfi_id
            AND imon.imon_id = p_imon_id;
      r_imon        c_imon%ROWTYPE;
      CURSOR c_mede(p_mede_id NUMBER) IS
         SELECT *
           FROM kgc_medewerkers mede
          WHERE mede.mede_id = p_mede_id;
      r_mede c_mede%ROWTYPE;
      v_count       NUMBER;
      v_msg         VARCHAR2(4000);
      v_imon_id     kgc_im_onderzoeken.imon_id%TYPE;
      v_onde_id     NUMBER;
      v_onde        cg$kgc_onderzoeken.cg$row_type;
   BEGIN
      IF p_imfi.specifieke_verwerking IS NULL
      THEN
         RETURN 'D'; -- doorgaan...
      END IF;
      -- specifieke verwerking: igv N-M: <fase>#<start>#<stap>
      IF p_plekvanaanroep = 'NA_CHECK' AND p_imfi.specifieke_verwerking LIKE 'F1#%#GA'
      THEN -- alleen onderzoek goedkeuren
         -- test voor nijmegen/maastricht
         -- Eerst controles
         v_msg := 'ERROR: Voor het goedkeuren van een onderzoek moet';
         SELECT COUNT(*), MAX(imon_id)
           INTO v_count, v_imon_id
           FROM kgc_im_onderzoeken imon
          WHERE imon.imfi_id = p_imfi.imfi_id;
         IF v_count != 1
         THEN
            kgc_import.addtext(x_text => x_imfi_verslag
                              ,p_extra_text =>  v_msg || ' er precies 1 onderzoek zijn aangeleverd');
            RETURN 'E'; -- Error
         END IF;
         v_onde_id := TO_NUMBER(SUBSTR(kgc_import.get_im2hlx('IMON' || v_imon_id), 5));
         IF v_onde_id IS NULL
         THEN
            kgc_import.addtext(x_text => x_imfi_verslag
                              ,p_extra_text =>  v_msg || ' het aangeleverde onderzoek gekoppeld zijn aan een Helix-onderzoek');
            RETURN 'E'; -- Error
         END IF;
         OPEN c_imon(p_imfi_id => p_imfi.imfi_id
                    ,p_imon_id => v_imon_id);
         FETCH c_imon INTO r_imon;
         CLOSE c_imon;
         IF kgc_import.im2onde( p_imon => r_imon
                              , p_id => 'dm'
                              , p_onde_old => v_onde
                              , x_onde => v_onde
                              , x_opmerkingen => x_imfi_verslag)
         THEN
            IF NVL(v_onde.status, 'X') != 'G'
            THEN
               kgc_import.addtext(x_text => x_imfi_verslag
                                 ,p_extra_text =>  v_msg || ' het aangeleverde onderzoek status ''G'' hebben');
               RETURN 'E'; -- Error
            END IF;
            IF v_onde.mede_id_controle IS NULL
            THEN
               kgc_import.addtext(x_text => x_imfi_verslag
                                 ,p_extra_text =>  v_msg || ' het aangeleverde onderzoek een gevulde "Controle door" hebben');
               RETURN 'E'; -- Error
            END IF;
            -- OK, bijwerken maar!
            OPEN c_mede(p_mede_id => v_onde.mede_id_controle);
            FETCH c_mede INTO r_mede;
            CLOSE c_mede;
            UPDATE kgc_onderzoeken onde
               SET onde.status = 'G'
                 , onde.mede_id_controle = v_onde.mede_id_controle
             WHERE onde.onde_id = v_onde_id;
            kgc_import.addtext(x_text => x_imfi_verslag
                              ,p_extra_text => 'Onderzoek ' || v_onde.onderzoeknr || ' is gewijzigd: Goedgekeurd door ' || r_mede.code || ' - ' || r_mede.naam);
            RETURN  'K'; -- klaar
         ELSE
            RETURN 'E'; -- Error
         END IF;
      END IF;
$ELSE
   BEGIN
$END
      RETURN  'D'; -- doorgaan...
   END specifieke_verwerking;

END KGC_IMPORT_MAASTRICHT;
/
