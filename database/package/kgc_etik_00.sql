CREATE OR REPLACE PACKAGE "HELIX"."KGC_ETIK_00" IS
/******************************************************************************
    NAME:      KGC_ETIK_00
    PURPOSE:   In deze package staat de samenstelling en inhoud van een etiket.
                          stel een regel samen voor etiket
    REVISIONS:
    Ver        Date             Author           Description
    ---------  ----------  ---------------  ------------------------------------
    1.0        24-06-2015  APL              Mantis 6672-9401
    2.0        06-05-2016  APL              Mantis 6672 -Release rework
 *****************************************************************************/

-- In deze package staat de samenstelling en inhoud van een etiket.

-- stel een regel samen voor etiket
FUNCTION regel(p_type            IN VARCHAR2
              ,p_id              IN NUMBER := NULL -- frac, mons, onde, kwfr, onbe, best, opat, ...
              ,p_omsluitteken    IN VARCHAR2 := NULL
              ,p_scheidingsteken IN VARCHAR2 := ';'
              ,p_regelstructuur  IN VARCHAR2 := NULL) RETURN VARCHAR2;

PROCEDURE dyn_sql(p_sql_stmt IN VARCHAR2
                 ,p_etpa_rec OUT kgc_etiket_parameters%ROWTYPE
                 ,p_gevonden OUT BOOLEAN);

-- Haal alle onderzoeksindicaties via het monster op, gescheiden door ',')
FUNCTION onde_indicatie(p_mons_id IN NUMBER)
RETURN VARCHAR2;
--Added by RBA for mantis 4273(13-09-13)
FUNCTION mons_indicatie(p_mons_id IN NUMBER)
RETURN VARCHAR2;
END KGC_ETIK_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_ETIK_00" IS
   FUNCTION regel(p_type            IN VARCHAR2
                 ,p_id              IN NUMBER := NULL -- frac, mons, onde, kwfr, onbe, ...
                 ,p_omsluitteken    IN VARCHAR2 := NULL
                 ,p_scheidingsteken IN VARCHAR2 := ';'
                 ,p_regelstructuur  IN VARCHAR2 := NULL) RETURN VARCHAR2 IS
      v_regel     VARCHAR2(4000);
      v_element   VARCHAR2(100);
      v_waarde    VARCHAR2(4000);
      v_statement VARCHAR2(32767);
      CURSOR etik_cur IS
         SELECT *
           FROM kgc_etiket_vw
          WHERE type_etiket = upper(p_type)
            AND ((type_etiket = 'FRAC' AND frac_id = p_id) OR (type_etiket = 'MONS' AND mons_id = p_id) OR
                 (type_etiket = 'ONDE' AND onde_id = p_id) OR (type_etiket = 'KWFR' AND kwfr_id = p_id) OR
                 (type_etiket = 'ONBE' AND onbe_id = p_id) OR (type_etiket = 'VOOP' AND voop_id = p_id) OR
                 (type_etiket = 'BEST' AND best_id = p_id) OR (type_etiket = 'OPAT' AND opat_id = p_id) OR
                 (type_etiket = 'WLST' AND wlst_id = p_id) OR (type_etiket = 'MEGR' AND megr_id = p_id) OR
                 (type_etiket = 'VOOR' AND voor_id = p_id));
      etik_rec etik_cur%ROWTYPE;

      --  voeg_toe een element aan het einde van een regel
      PROCEDURE voeg_toe(x_regel IN OUT VARCHAR2
                        ,b_item  IN VARCHAR2) IS
      BEGIN
         IF x_regel IS NOT NULL
         THEN
            x_regel := x_regel || p_scheidingsteken;
         END IF;
         x_regel := x_regel || p_omsluitteken || b_item || p_omsluitteken;
      END voeg_toe;

   BEGIN
      OPEN etik_cur;
      FETCH etik_cur
         INTO etik_rec;
      CLOSE etik_cur;
      IF (p_regelstructuur IS NOT NULL)
      THEN
         v_regel := p_regelstructuur;
         WHILE (instr(v_regel
                     ,'<') > 0 AND instr(v_regel
                                         ,'>') > 0)
         LOOP
            v_element := NULL;
            v_waarde  := NULL;
            v_element := kgc_util_00.strip(p_string => v_regel
                                          ,p_tag_va => '<'
                                          ,p_tag_tm => '>');
            -- Het volgende kan eleganter via een user-defined package function
            -- die overeenkomstig werkt aan NAME_IN(). Deze function werkt met
            -- een package variable zie metalink Doc ID Note: 292037.1
            -- To keep it simple is deze oplossing voorlopig niet gebruikt.
            IF (upper(v_element) = 'TYPE_ETIKET')
            THEN
               v_waarde := etik_rec.type_etiket;
            ELSIF (upper(v_element) = 'SORTERING')
            THEN
               v_waarde := etik_rec.sortering;
            ELSIF (upper(v_element) = 'KAFD_CODE')
            THEN
               v_waarde := etik_rec.kafd_code;
            ELSIF (upper(v_element) = 'ONGR_CODE')
            THEN
               v_waarde := etik_rec.ongr_code;
            ELSIF (upper(v_element) = 'FRACTIENUMMER')
            THEN
               v_waarde := etik_rec.fractienummer;
            ELSIF (upper(v_element) = 'FRTY_CODE')
            THEN
               v_waarde := etik_rec.frty_code;
            ELSIF (upper(v_element) = 'FRTY_OMSCHRIJVING')
            THEN
               v_waarde := etik_rec.frty_omschrijving;
            ELSIF (upper(v_element) = 'MONSTERNUMMER')
            THEN
               v_waarde := etik_rec.monsternummer;
           --Start Added by Anuja against Mantis 6672-9401 on 24/06/15
            ELSIF (upper(v_element) = 'MONS_COMMENTAAR')
            THEN
               v_waarde :=substr(etik_rec.mons_commentaar,1,30);  --MONS_COMMENTAAR is truncated to 30chars
            ELSIF (upper(v_element) = 'MONS_DATUM_AANMELDING')
            THEN
               v_waarde := to_char(etik_rec.mons_datum_aanmelding,'dd-mm-yyyy');  --to_char added by Anuja on 6/5/2016 for Mantis 6672 for release rework8.11.2.6
          --End Added by Anuja against Mantis 6672-9401 on 24/06/15
            ELSIF (upper(v_element) = 'ONDERZOEKNR')
            THEN
               v_waarde := etik_rec.onderzoeknr;
            ELSIF (upper(v_element) = 'ONDERZOEKSTYPE')
            THEN
               v_waarde := etik_rec.onderzoekstype;
            ELSIF (upper(v_element) = 'ONDERZOEKSWIJZE')
            THEN
               v_waarde := etik_rec.onderzoekswijze;
            ELSIF (upper(v_element) = 'ONDE_INDICATIE')
            THEN
               v_waarde := etik_rec.onde_indicatie;
            ELSIF (upper(v_element) = 'KWFR_CODE')
            THEN
               v_waarde := etik_rec.kwfr_code;
            ELSIF (upper(v_element) = 'DATUM')
            THEN
               v_waarde := to_char(etik_rec.datum
                                  ,'dd-mm-yyyy');
            ELSIF (upper(v_element) = 'MATE_CODE')
            THEN
               v_waarde := etik_rec.mate_code;
            ELSIF (upper(v_element) = 'MATE_OMSCHRIJVING')
            THEN
               v_waarde := etik_rec.mate_omschrijving;
            ELSIF (upper(v_element) = 'KWTY_CODE')
            THEN
               v_waarde := etik_rec.kwty_code;
            ELSIF (upper(v_element) = 'KWTY_OMSCHRIJVING')
            THEN
               v_waarde := etik_rec.kwty_omschrijving;
            ELSIF (upper(v_element) = 'KWFR_KWFR_ID_PARENT')
            THEN
               v_waarde := etik_rec.kwfr_kwfr_id_parent;
            ELSIF (upper(v_element) = 'KWFR_CODE_PARENT')
            THEN
               v_waarde := etik_rec.kwfr_code_parent;
            ELSIF (upper(v_element) = 'VOTY_CODE')
            THEN
               v_waarde := etik_rec.voty_code;
            ELSIF (upper(v_element) = 'VOTY_OMSCHRIJVING')
            THEN
               v_waarde := etik_rec.voty_omschrijving;
            ELSIF (upper(v_element) = 'PERS_ZISNR')
            THEN
               v_waarde := etik_rec.pers_zisnr;
         --Start Added by Anuja against Mantis 6672-9401 on 24/06/15
             ELSIF (upper(v_element) = 'PERS_BSN'
             AND etik_rec.bsn_geverifieerd = 'J' )
            THEN
                 v_waarde:=etik_rec.pers_bsn;  --PERS_BSN to be printed on label if PERS.BSN_GEVERIFIEERD = 'J'
         --End Added by Anuja against Mantis 6672-9401 on 24/06/15
            ELSIF (upper(v_element) = 'PERS_ACHTERNAAM')
            THEN
               v_waarde := etik_rec.pers_achternaam;
            ELSIF (upper(v_element) = 'PERS_VOORVOEGSEL')
            THEN
               v_waarde := etik_rec.pers_voorvoegsel;
            ELSIF (upper(v_element) = 'PERS_VOORLETTERS')
            THEN
               v_waarde := etik_rec.pers_voorletters;
            ELSIF (upper(v_element) = 'PERS_AANSPREKEN')
            THEN
               v_waarde := etik_rec.pers_aanspreken;
            ELSIF (upper(v_element) = 'PERS_GESLACHT')
            THEN
               v_waarde := etik_rec.pers_geslacht;
            ELSIF (upper(v_element) = 'PERS_GEBOORTEDATUM')
            THEN
               v_waarde := to_char(etik_rec.pers_geboortedatum
                                  ,'dd-mm-yyyy');
            ELSIF (upper(v_element) = 'PERS_TELEFOON')
            THEN
               v_waarde := etik_rec.pers_telefoon;
            ELSIF (upper(v_element) = 'PERS_ADRES')
            THEN
               v_waarde := etik_rec.pers_adres;
            ELSIF (upper(v_element) = 'PERS_POSTCODE')
            THEN
               v_waarde := etik_rec.pers_postcode;
            ELSIF (upper(v_element) = 'PERS_WOONPLAATS')
            THEN
               v_waarde := etik_rec.pers_woonplaats;
            ELSIF (upper(v_element) = 'VERZEKERAAR_CODE')
            THEN
               v_waarde := etik_rec.verzekeraar_code;
            ELSIF (upper(v_element) = 'VERZEKERAAR_NAAM')
            THEN
               v_waarde := etik_rec.verzekeraar_naam;
            ELSIF (upper(v_element) = 'VERZEKERINGSNR')
            THEN
               v_waarde := etik_rec.verzekeringsnr;
            ELSIF (upper(v_element) = 'PERSOON_INFO')
            THEN
               v_waarde := etik_rec.persoon_info;
            ELSIF (upper(v_element) = 'PERSOON_INFO_KORT')
            THEN
               v_waarde := etik_rec.persoon_info_kort;
            ELSIF (upper(v_element) = 'FAMILIENUMMER')
            THEN
               v_waarde := etik_rec.familienummer;
            ELSIF (upper(v_element) = 'FAMI_NAAM')
            THEN
               v_waarde := etik_rec.fami_naam;
            ELSIF (upper(v_element) = 'HUISARTS_ACHTERNAAM')
            THEN
               v_waarde := etik_rec.huisarts_achternaam;
            ELSIF (upper(v_element) = 'HUISARTS_AANSPREKEN')
            THEN
               v_waarde := etik_rec.huisarts_aanspreken;
            ELSIF (upper(v_element) = 'HUISARTS_WOONPLAATS')
            THEN
               v_waarde := etik_rec.huisarts_woonplaats;
            ELSIF (upper(v_element) = 'HUISARTS_TELEFOON')
            THEN
               v_waarde := etik_rec.huisarts_telefoon;
            ELSIF (upper(v_element) = 'BETROKKENE_A1')
            THEN
               v_waarde := etik_rec.betrokkene_a1;
            ELSIF (upper(v_element) = 'BETROKKENE_A2')
            THEN
               v_waarde := etik_rec.betrokkene_a2;
            ELSIF (upper(v_element) = 'MONS_DATUM_AFNAME')
            THEN
               v_waarde := etik_rec.mons_datum_afname;
            ELSIF (upper(v_element) = 'MONS_TIJD_AFNAME')
            THEN
               v_waarde := etik_rec.mons_tijd_afname;
            ELSIF (upper(v_element) = 'HOEVEELHEID_MONSTER')
            THEN
               v_waarde := etik_rec.hoeveelheid_monster;
            ELSIF (upper(v_element) = 'FRAC_ID')
            THEN
               v_waarde := etik_rec.frac_id;
            ELSIF (upper(v_element) = 'MONS_ID')
            THEN
               v_waarde := etik_rec.mons_id;
            ELSIF (upper(v_element) = 'OPSLAGPOSITIE')
            THEN
               v_waarde := etik_rec.opslagpositie;
            ELSIF (upper(v_element) = 'STAN_ID')
            THEN
               v_waarde := etik_rec.stan_id;
            ELSIF (upper(v_element) = 'ONDE_ID')
            THEN
               v_waarde := etik_rec.onde_id;
            ELSIF (upper(v_element) = 'KWFR_ID')
            THEN
               v_waarde := etik_rec.kwfr_id;
            ELSIF (upper(v_element) = 'ONBE_ID')
            THEN
               v_waarde := etik_rec.onbe_id;
            ELSIF (upper(v_element) = 'VOOP_ID')
            THEN
               v_waarde := etik_rec.voop_id;
            ELSIF (upper(v_element) = 'BEST_ID')
            THEN
               v_waarde := etik_rec.best_id;
            ELSIF (upper(v_element) = 'OPAT_ID')
            THEN
               v_waarde := etik_rec.opat_id;
            ELSIF (upper(v_element) = 'MEGR_ID')
            THEN
               v_waarde := etik_rec.megr_id;
            ELSIF (upper(v_element) = 'STGR_ID')
            THEN
               v_waarde := etik_rec.stgr_id;
            ELSIF (upper(v_element) = 'WLST_ID')
            THEN
               v_waarde := etik_rec.wlst_id;
            ELSIF (upper(v_element) = 'VOOR_ID')
            THEN
               v_waarde := etik_rec.voor_id;
            ELSIF (upper(v_element) = 'CUTY_ID')
            THEN
               v_waarde := etik_rec.cuty_id;
            ELSIF (upper(v_element) = 'CUPTYPE')
            THEN
               v_waarde := etik_rec.cuptype;
            ELSIF (upper(v_element) = 'OPSLAG_VOLGNR')
            THEN
               v_waarde := etik_rec.opslag_volgnr;
            ELSIF (upper(v_element) = 'TIJDSTEMPEL')
            THEN
               v_waarde := etik_rec.tijdstempel;
            ELSIF (upper(v_element) = 'OPSLAG_TYPE')
            THEN
               v_waarde := etik_rec.opslag_type;
            ELSIF (upper(v_element) = 'FOETUS_INFO')
            THEN
               v_waarde := etik_rec.foetus_info;
            ELSIF (upper(v_element) = 'MONS_ETIKET_TOEVOEGING')
            THEN
               v_waarde := etik_rec.mons_etiket_toevoeging;
            ELSIF (upper(v_element) = 'MEGR_NUMMER')
            THEN
               v_waarde := etik_rec.megr_nummer;
            ELSIF (upper(v_element) = 'WERKLIJST_NUMMER')
            THEN
               v_waarde := etik_rec.werklijst_nummer;
            ELSIF (upper(v_element) = 'AANGEMAAKT_DOOR')
            THEN
               v_waarde := etik_rec.aangemaakt_door;
            ELSIF (upper(v_element) = 'STGR_CODE')
            THEN
               v_waarde := etik_rec.stgr_code;
            ELSIF (upper(v_element) = 'STGR_OMSCHRIJVING')
            THEN
               v_waarde := etik_rec.stgr_omschrijving;
            ELSIF (upper(v_element) = 'DATUM_AANMAAK')
            THEN
               v_waarde := to_char(etik_rec.datum_aanmaak
                                  ,'dd-mm-yyyy');
            ELSIF (upper(v_element) = 'ONDE_AANVRAGER')
            THEN
               v_waarde := etik_rec.onde_aanvrager;
            ELSIF (upper(v_element) = 'RELA_INSTELLING')
            THEN
               v_waarde := etik_rec.rela_instelling;
            ELSIF (upper(v_element) = 'RELA_INSTELLING_CODE')
            THEN
               v_waarde := etik_rec.rela_instelling_code;
            ELSIF (upper(v_element) = 'RELA_AFDELING')
            --Added by RBA for mantis 4273(13-09-13)
            THEN
               v_waarde := etik_rec.rela_afdeling;
            ELSIF (upper(v_element) = 'ONTSTAAN_UIT')
            THEN
               v_waarde := etik_rec.ontstaan_uit;
            ELSIF (upper(v_element) = 'MONS_INDICATIE')
            THEN
               v_waarde := etik_rec.mons_indicatie;
             --end here
            END IF;
            v_regel := REPLACE(v_regel
                              ,'<' || v_element || '>'
                              ,v_waarde);
         END LOOP;
      END IF;
      RETURN(v_regel);
   END regel;

   PROCEDURE dyn_sql(p_sql_stmt IN VARCHAR2
                    ,p_etpa_rec OUT kgc_etiket_parameters%ROWTYPE
                    ,p_gevonden OUT BOOLEAN) IS

      -- N. ter Burg, Mantis 1610, procedure toegevoegd
      -- definieer weak REF CURSOR type
      TYPE etpacurtyp IS REF CURSOR;

      -- declareer cursor variable
      etpa_cur etpacurtyp; -- declareer cursor variable

   BEGIN
      OPEN etpa_cur FOR p_sql_stmt;
      FETCH etpa_cur
       INTO p_etpa_rec;
      p_gevonden := etpa_cur%FOUND;
      CLOSE etpa_cur;

   EXCEPTION
      WHEN OTHERS THEN
         qms$errors.unhandled_exception('kgc_etik_00.dyn_sql');

   END dyn_sql;

  -- Haal alle onderzoeksindicaties via het monster op, gescheiden door ',')
  FUNCTION onde_indicatie(p_mons_id IN NUMBER)
  RETURN VARCHAR2
  IS
    v_return VARCHAR2(4000) := NULL;
    CURSOR onmo_cur
          ( b_mons_id IN NUMBER
          )
    IS
    SELECT onde.onde_id
      FROM kgc_onderzoeken        onde
         , kgc_onderzoek_monsters onmo
     WHERE onde.onde_id = onmo.onde_id
       AND onmo.mons_id  = b_mons_id
     ORDER BY onde.onderzoeknr ASC
    ;
  BEGIN
    FOR r IN onmo_cur ( p_mons_id )
    LOOP
      IF ( v_return IS NULL )
      THEN
        v_return := kgc_indi_00.onde_indicatie_codes( r.onde_id );
      ELSE
        IF kgc_indi_00.onde_indicatie_codes( r.onde_id ) IS NOT NULL
        THEN
          v_return := v_return||', '||kgc_indi_00.onde_indicatie_codes( r.onde_id );
        END IF;
      END IF;
    END LOOP;
    RETURN( v_return );
  END onde_indicatie;
  --Added by RBA for mantis 4273(13-09-13)
  FUNCTION mons_indicatie(p_mons_id IN NUMBER)
  RETURN VARCHAR2
  IS
    v_return VARCHAR2(4000) := NULL;
    CURSOR mons_cur
          ( b_mons_id IN NUMBER
          )
    IS
    SELECT indi.code
    FROM   kgc_indicatie_teksten indi
    ,      kgc_monster_indicaties moin
    WHERE  moin.indi_id = indi.indi_id
    AND    moin.mons_id = b_mons_id
    ORDER BY moin.moin_id ASC
    ;
  BEGIN
    FOR r IN mons_cur ( p_mons_id )
    LOOP
        v_return := v_return||r.code||'/';
    END LOOP;
    v_return:=trim(both '/' from v_return);
    RETURN( v_return );
  END mons_indicatie;
END kgc_etik_00;
/

/
QUIT
