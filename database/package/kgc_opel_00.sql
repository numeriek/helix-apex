CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPEL_00" IS
/* LST 13-07-2004                                              */
/* Package voor procedures op de tabel KGC_OPSLAG_ELEEMENTEN   */
PROCEDURE chk_geen_parent
(p_opel_id IN NUMBER
,p_parent_opel_id IN NUMBER
);

PROCEDURE werk_oppo_bij
(p_opel_id IN NUMBER
,p_aantal_x_pos IN NUMBER
,p_aantal_y_pos IN NUMBER
,p_xas_type IN VARCHAR2
,p_yas_type IN VARCHAR2
)
;
END KGC_OPEL_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPEL_00" IS
/* LST 13-07-2004                                            */
/* Package voor procedures op de tabel KGC_OPSLAG_ELEMENTEN  */

/* element dat in de boom wordt opgenomen mag niet hoger in  */
/* de boom al voorkomen                                      */
PROCEDURE chk_geen_parent
(p_opel_id IN NUMBER
,p_parent_opel_id IN NUMBER
)
IS
  CURSOR c_opel
  (cp_opel_id NUMBER
  ,cp_parent_opel_id NUMBER
  )
  IS
  SELECT '1'
  FROM   kgc_opslag_elementen opel
  WHERE  opel.opel_id = cp_opel_id
  CONNECT BY opel_id = PRIOR parent_opel_id
  START WITH opel_id = cp_parent_opel_id
  ;
  v_dummy VARCHAR2(1);
BEGIN
  IF p_parent_opel_id IS NOT NULL THEN
    OPEN c_opel(p_opel_id, p_parent_opel_id);
    FETCH c_opel INTO v_dummy;
    IF c_opel%found THEN
      qms$errors.show_message(p_mesg => 'KGC-11148'
	                         ,p_errtp => 'E'
		   				     ,p_rftf => TRUE
						     );
    END IF;
    CLOSE c_opel;
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opel%isopen THEN
      CLOSE c_opel;
    END IF;
    RAISE;
END;

PROCEDURE werk_oppo_bij
(p_opel_id IN NUMBER
,p_aantal_x_pos IN NUMBER
,p_aantal_y_pos IN NUMBER
,p_xas_type IN VARCHAR2
,p_yas_type IN VARCHAR2
)
IS
  CURSOR c_oppo
  (cp_opel_id   NUMBER
  ,cp_x_positie NUMBER
  ,cp_y_positie NUMBER
  ) IS
  SELECT oppo.code
  ,      oppo.beschikbaar
  ,      oppo.opmerkingen
  FROM   kgc_opslag_posities oppo
  WHERE  oppo.opel_id = cp_opel_id
  AND    oppo.x_positie = cp_x_positie
  AND    oppo.y_positie = cp_y_positie
  ;
  v_x NUMBER(4);
  v_y NUMBER(4);
  v_x_code VARCHAR2(4);
  v_y_code VARCHAR2(4);
  v_oppo_rec kgc_opslag_posities%rowtype;
  v_oppo_id kgc_opslag_posities.oppo_id%type;
  r_oppo c_oppo%rowtype;
BEGIN
  FOR v_x IN 1..p_aantal_x_pos LOOP
    FOR v_y IN 1..p_aantal_y_pos LOOP
      r_oppo := NULL;
      OPEN c_oppo(p_opel_id, v_x, v_y);
      FETCH c_oppo INTO r_oppo;
      CLOSE c_oppo;
      IF p_xas_type = 'A'
      THEN
        v_x_code := CHR(v_x + 64);
      ELSE
        v_x_code := TO_CHAR(v_x);
      END IF;
      IF p_yas_type = 'A'
      THEN
        v_y_code := CHR(v_y + 64);
      ELSE
        v_y_code := TO_CHAR(v_y);
      END IF;
      v_oppo_rec.opel_id := p_opel_id;
      v_oppo_rec.x_positie := v_x;
      v_oppo_rec.y_positie := v_y;
      v_oppo_rec.created_by := USER;
      v_oppo_rec.creation_date := SYSDATE;
      v_oppo_rec.last_updated_by := USER;
      v_oppo_rec.last_update_date := SYSDATE;
      v_oppo_rec.beschikbaar := NVL(r_oppo.beschikbaar, 'J');
      v_oppo_rec.code := v_x_code||','||v_y_code;
      v_oppo_rec.opmerkingen := r_oppo.opmerkingen;
      DELETE FROM kgc_opslag_posities
      WHERE opel_id = p_opel_id
      AND   x_positie = v_x
      AND   y_positie = v_y
      ;
      kgc_oppo_00.insert_oppo( p_oppo_rec => v_oppo_rec
                             , p_oppo_id => v_oppo_id
                             );
    END LOOP;
  END LOOP;
  DELETE FROM kgc_opslag_posities
  WHERE opel_id = p_opel_id
  AND   (x_positie > p_aantal_x_pos
         OR
         y_positie > p_aantal_y_pos
        )
  ;
END werk_oppo_bij;

END KGC_OPEL_00;
/

/
QUIT
