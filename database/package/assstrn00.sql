CREATE OR REPLACE PACKAGE "HELIX"."ASSSTRN00" IS
  FUNCTION element_in_list(
      p_element    IN  VARCHAR2
	, p_list       IN  VARCHAR2
    , p_seperator  IN  VARCHAR2  DEFAULT ','
                          ) RETURN BOOLEAN;
  FUNCTION nxt_elem_in_list(
      p_list       IN      VARCHAR2
    , p_seperator  IN      VARCHAR2  DEFAULT ','
	, x_remainder  IN OUT  VARCHAR2
                           ) RETURN VARCHAR2;
  FUNCTION rem_dup_from_list(
      p_list       IN  VARCHAR2
    , p_seperator  IN  VARCHAR2  DEFAULT ','
                            ) RETURN VARCHAR2;
  FUNCTION add_to_list(
      p_element    IN  VARCHAR2
    , p_list       IN  VARCHAR2  DEFAULT NULL
    , p_seperator  IN  VARCHAR2  DEFAULT ','
                      ) RETURN VARCHAR2;

  PRAGMA RESTRICT_REFERENCES( element_in_list,   WNDS, RNDS, WNPS );
  PRAGMA RESTRICT_REFERENCES( nxt_elem_in_list,  WNDS, RNDS, WNPS );
  PRAGMA RESTRICT_REFERENCES( rem_dup_from_list, WNDS, RNDS, WNPS );
  PRAGMA RESTRICT_REFERENCES( add_to_list,       WNDS, RNDS, WNPS );
END assstrn00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."ASSSTRN00" IS
  --  ========================================================================
  --  GLOBAL PROCEDURES/FUNCTIONS
  --

  FUNCTION element_in_list(
      p_element   IN  VARCHAR2
	, p_list      IN  VARCHAR2
    , p_seperator IN  VARCHAR2  DEFAULT ','
                          ) RETURN BOOLEAN IS
  BEGIN

    IF   p_element = p_list
	OR   NVL(INSTR(p_list,p_seperator||p_element||p_seperator),0) > 0
	OR   NVL(INSTR(p_list,p_element||p_seperator),0) = 1
	OR  (NVL(INSTR(p_list,p_seperator||p_element,-1),0) > 0
	 AND LENGTH(p_seperator||p_element) = LENGTH(SUBSTR(p_list,INSTR(p_list,p_seperator||p_element,-1)))
	    )
	THEN
      RETURN TRUE;
	ELSE
      RETURN FALSE;
	END IF;
  END element_in_list;

  --  Function : NXT_ELEM_IN_LIST,  Return the next element (value) in a list
  --                                seperated by a specific character
  FUNCTION nxt_elem_in_list(
      p_list       IN      VARCHAR2
    , p_seperator  IN      VARCHAR2  DEFAULT ','
	, x_remainder  IN OUT  VARCHAR2
                           ) RETURN VARCHAR2 IS
	v_sep_pos NUMBER;
    v_element VARCHAR2(4000) := NULL;
    v_retval  VARCHAR2(4000) := NULL;
  BEGIN
    IF  p_list IS NOT NULL
	THEN
      v_sep_pos := INSTR(p_list, p_seperator);
      IF NVL(v_sep_pos,0) > 0
      THEN
        v_element   := SUBSTR(p_list, 1, v_sep_pos-1);
		x_remainder := SUBSTR(p_list, v_sep_pos+1 );
      ELSE
        v_element   := p_list;
		x_remainder := NULL;
      END IF;
	END IF;
    RETURN v_element;
  END nxt_elem_in_list;

  --  Function : REM_DUP_FROM_LIST, remove duplicates from a list of values
  --                                seperated by a character
  FUNCTION rem_dup_from_list(
      p_list       IN  VARCHAR2
    , p_seperator  IN  VARCHAR2  DEFAULT ','
	                        ) RETURN VARCHAR2 IS
    v_list    VARCHAR2(4000) := p_list;
    v_element VARCHAR2(4000) := '';
    v_retval  VARCHAR2(4000) := '';
  BEGIN
    WHILE v_list IS NOT NULL
	LOOP
      v_element := nxt_elem_in_list( p_list      => v_list
                                   , p_seperator => p_seperator
                                   , x_remainder => v_list
                                   );
      IF  v_element IS NOT NULL
	  AND NOT element_in_list( p_element   => v_element
	                         , p_list      => v_retval
                             , p_seperator => p_seperator
							 )
	  THEN
        IF  v_retval IS NOT NULL
		THEN
		  v_retval := v_retval||p_seperator;
	    END IF;
        v_retval := v_retval||v_element;
	  END IF;
	END LOOP;
    RETURN v_retval;
  END rem_dup_from_list;

  --  Function : ADD_TO_LIST, add an element to a list of elements
  --                          seperated by a seperator
  FUNCTION add_to_list(
      p_element    IN  VARCHAR2
    , p_list       IN  VARCHAR2  DEFAULT NULL
    , p_seperator  IN  VARCHAR2  DEFAULT ','
                      ) RETURN VARCHAR2 IS
    v_list  VARCHAR2(4000) := p_list;
  BEGIN
    IF  p_element IS NOT NULL
    THEN
      IF NVL(LENGTH(v_list),0)+NVL(LENGTH(p_element),0)+NVL(LENGTH(p_seperator),0) <= 4000
      THEN
        IF  v_list IS NOT NULL
        THEN
          v_list := v_list||p_seperator;
        END IF;
        v_list := v_list||p_element;
      END IF;
    END IF;
    RETURN v_list;
  END add_to_list;

END assstrn00;
/

/
QUIT
