CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_SYSTEMS"
IS
   /*
   ================================================================================
   Package Name :  gen_pck_systems
   Usage        :  Package with procedure for systems, system messages, etc.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   FUNCTION f_get_system_id(i_name IN gen_systems.name%TYPE)
      RETURN gen_systems.id%TYPE;
   FUNCTION f_get_system_id(i_identifier1  IN gen_systems.identifier1%TYPE
                           ,i_identifier2  IN gen_systems.identifier2%TYPE)
      RETURN gen_systems.id%TYPE;
   FUNCTION f_get_system_rec(i_name IN gen_systems.name%TYPE)
      RETURN gen_systems%ROWTYPE;
   FUNCTION f_get_sys_rec(i_id IN gen_systems.id%TYPE)
      RETURN gen_systems%ROWTYPE;
   FUNCTION f_get_system_message_type_id(
      i_name  IN gen_system_message_types.name%TYPE)
      RETURN gen_system_message_types.id%TYPE;
   FUNCTION f_get_active_message_type_name(
      i_ste_id  IN gen_system_message_types.id%TYPE)
      RETURN gen_system_message_types.name%TYPE;
   FUNCTION f_get_active_message_type_id(
      i_name  IN gen_system_message_types.name%TYPE)
      RETURN gen_system_message_types.id%TYPE;
   FUNCTION f_xml_extract_no_exception(i_xml        IN XMLTYPE
                                      ,i_xpath      IN VARCHAR2
                                      ,i_namespace  IN VARCHAR2 DEFAULT NULL)
      RETURN VARCHAR2;
   PROCEDURE p_cleanup_system_messages;
   PROCEDURE p_del_system_message(i_sme_id IN NUMBER);
   FUNCTION f_get_system_param_value(
      i_system_name        IN VARCHAR2
     ,i_param_name         IN VARCHAR2
     ,i_message_type_name  IN VARCHAR2 DEFAULT NULL)
      RETURN VARCHAR2;
   FUNCTION f_get_system_param_val(i_system_id        IN NUMBER
                                  ,i_param_name       IN VARCHAR2
                                  ,i_message_type_id  IN NUMBER DEFAULT NULL)
      RETURN VARCHAR2;
   FUNCTION f_ins_system_message(
      i_system_message_rec  IN gen_system_messages%ROWTYPE)
      RETURN gen_system_messages.id%TYPE;
   PROCEDURE p_ins_system_message(
      i_system_message_rec  IN gen_system_messages%ROWTYPE);
   PROCEDURE p_create_system_message(
      i_system_name             IN VARCHAR2
     ,i_message_type_name       IN VARCHAR2
     ,i_message                 IN XMLTYPE
     ,i_status_description      IN VARCHAR2
     ,i_functional_reference_1  IN VARCHAR2 DEFAULT NULL
     ,i_functional_reference_2  IN VARCHAR2 DEFAULT NULL);
   PROCEDURE p_mail_system_appl(i_system_id           IN NUMBER
                               ,i_status_id           IN NUMBER
                               ,i_message_type_id     IN NUMBER DEFAULT NULL
                               ,i_system_message_id   IN NUMBER
                               ,i_status_description  IN VARCHAR2);
   FUNCTION f_create_system_message(
      i_system_name             IN VARCHAR2
     ,i_message_type_name       IN VARCHAR2
     ,i_message                 IN XMLTYPE
     ,i_status_description      IN VARCHAR2
     ,i_functional_reference_1  IN VARCHAR2 DEFAULT NULL
     ,i_functional_reference_2  IN VARCHAR2 DEFAULT NULL)
      RETURN gen_system_messages.id%TYPE;
   ----------------------------------------------------------------------------
   -- Procedure that is called from the insert trigger of table gen_SYSTEM_MESSAGES
   -- and handles the incoming message in the appropriate way dependent on
   -- message type
   ----------------------------------------------------------------------------
   PROCEDURE p_receive_message(
      i_message_type          IN     gen_system_message_types.name%TYPE
     ,i_message_id            IN     gen_system_messages.id%TYPE
     ,i_system_id_from        IN     gen_system_messages.system_id_from%TYPE
     ,i_request_message       IN     gen_system_messages.MESSAGE%TYPE
     ,i_timestamp_message     IN     gen_system_messages.timestamp_message%TYPE
     ,i_message_reference_id  IN     gen_system_messages.message_reference_id%TYPE
     ,io_status_id            IN OUT gen_system_messages.status_id%TYPE
     ,io_status_description   IN OUT gen_system_messages.status_description%TYPE);
   PROCEDURE p_retry_messages;
   PROCEDURE p_resubmit_message(i_message_id IN gen_system_messages.id%TYPE);
   PROCEDURE p_cleanup_sys_messages_hist(
      i_nr_days_before_cleanup   NUMBER DEFAULT NULL);
   FUNCTION f_get_status_description(i_system_message_id IN NUMBER)
      RETURN VARCHAR2;
END gen_pck_systems;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_SYSTEMS"
IS
   /*
   ================================================================================
   Package Name :  gen_pck_systems
   Usage        :  Package with procedure for systems, system messages, etc.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   g_package  CONSTANT VARCHAR2(31) := $$plsql_unit || '.';
   FUNCTION f_get_system_id(i_name IN gen_systems.name%TYPE)
      RETURN gen_systems.id%TYPE
   IS
      CURSOR c_stm
      IS
         SELECT id
           FROM gen_systems
          WHERE name = i_name;
      r_stm  c_stm%ROWTYPE;
   BEGIN
      OPEN c_stm;
      FETCH c_stm
         INTO r_stm;
      CLOSE c_stm;
      RETURN r_stm.id;
   END f_get_system_id;
   FUNCTION f_get_system_id(i_identifier1  IN gen_systems.identifier1%TYPE
                           ,i_identifier2  IN gen_systems.identifier2%TYPE)
      RETURN gen_systems.id%TYPE
   IS
      CURSOR c_stm
      IS
         SELECT *
           FROM gen_systems
          WHERE identifier1 = i_identifier1
            AND identifier2 = i_identifier2;
      r_stm  c_stm%ROWTYPE;
   BEGIN
      OPEN c_stm;
      FETCH c_stm
         INTO r_stm;
      CLOSE c_stm;
      RETURN r_stm.id;
   END f_get_system_id;
   FUNCTION f_get_system_rec(i_name IN gen_systems.name%TYPE)
      RETURN gen_systems%ROWTYPE
   IS
      CURSOR c_stm
      IS
         SELECT *
           FROM gen_systems
          WHERE name = i_name;
      r_stm  c_stm%ROWTYPE;
   BEGIN
      OPEN c_stm;
      FETCH c_stm
         INTO r_stm;
      CLOSE c_stm;
      RETURN r_stm;
   END f_get_system_rec;
   FUNCTION f_get_sys_rec(i_id IN gen_systems.id%TYPE)
      RETURN gen_systems%ROWTYPE
   IS
      CURSOR c_stm
      IS
         SELECT *
           FROM gen_systems
          WHERE id = i_id;
      r_stm  c_stm%ROWTYPE;
   BEGIN
      OPEN c_stm;
      FETCH c_stm
         INTO r_stm;
      CLOSE c_stm;
      RETURN r_stm;
   END f_get_sys_rec;
   FUNCTION f_get_system_message_type_id(
      i_name  IN gen_system_message_types.name%TYPE)
      RETURN gen_system_message_types.id%TYPE
   IS
      CURSOR c_ste
      IS
         SELECT id
           FROM gen_system_message_types
          WHERE name = i_name;
      r_ste  c_ste%ROWTYPE;
   BEGIN
      OPEN c_ste;
      FETCH c_ste
         INTO r_ste;
      CLOSE c_ste;
      RETURN r_ste.id;
   END f_get_system_message_type_id;
   FUNCTION f_get_active_message_type_name(
      i_ste_id  IN gen_system_message_types.id%TYPE)
      RETURN gen_system_message_types.name%TYPE
   IS
      CURSOR c_ste
      IS
         SELECT name
           FROM gen_system_message_types
          WHERE id = i_ste_id
            AND active_ind = 'Y';
      r_ste  c_ste%ROWTYPE;
   BEGIN
      OPEN c_ste;
      FETCH c_ste
         INTO r_ste;
      CLOSE c_ste;
      RETURN r_ste.name;
   END f_get_active_message_type_name;
   FUNCTION f_get_active_message_type_id(
      i_name  IN gen_system_message_types.name%TYPE)
      RETURN gen_system_message_types.id%TYPE
   IS
      CURSOR c_ste
      IS
         SELECT id
           FROM gen_system_message_types
          WHERE name = i_name
            AND active_ind = 'Y';
      r_ste  c_ste%ROWTYPE;
   BEGIN
      OPEN c_ste;
      FETCH c_ste
         INTO r_ste;
      CLOSE c_ste;
      RETURN r_ste.id;
   END f_get_active_message_type_id;
   FUNCTION f_xml_extract_no_exception(i_xml        IN XMLTYPE
                                      ,i_xpath      IN VARCHAR2
                                      ,i_namespace  IN VARCHAR2 DEFAULT NULL)
      RETURN VARCHAR2
   AS
   BEGIN
      RETURN CASE
                WHEN i_xml.EXTRACT(i_xpath
                                  ,i_namespace)
                        IS NOT NULL
                THEN
                   i_xml.EXTRACT(i_xpath
                                ,i_namespace).getstringval()
                ELSE
                   NULL
             END;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END f_xml_extract_no_exception;
   ----------------------------------------------------------------------------
   -- Procedure that clean ups old records in the gen_system_messages table
   ----------------------------------------------------------------------------
   PROCEDURE p_cleanup_system_messages
   IS
      l_scope              VARCHAR2(61) := g_package || 'p_cleanup_system_messages';
      l_cleanup_after_hrs  NUMBER;
   BEGIN
      l_cleanup_after_hrs      :=
         COALESCE(
            TO_NUMBER(
               gen_pck_general.f_get_appl_parameter_value(
                  'CLEANUP_SYSTEM_MESSAGES'))
           ,24);
      DELETE FROM gen_system_messages
            WHERE created_date < SYSDATE - l_cleanup_after_hrs / 24;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
         logger.log_error(p_text => 'Error message ' || SQLERRM
                         ,p_scope => l_scope);
         ROLLBACK;
   END p_cleanup_system_messages;
   PROCEDURE p_del_system_message(i_sme_id IN NUMBER)
   IS
   BEGIN
      DELETE FROM gen_system_messages
            WHERE id = i_sme_id;
   END p_del_system_message;
   FUNCTION f_get_system_param_value(
      i_system_name        IN VARCHAR2
     ,i_param_name         IN VARCHAR2
     ,i_message_type_name  IN VARCHAR2 DEFAULT NULL)
      RETURN VARCHAR2
   IS
      CURSOR c_spm
      IS
         SELECT VALUE
           FROM gen_system_parameters spm
                INNER JOIN gen_systems stm ON spm.system_id = stm.id
                LEFT OUTER JOIN gen_system_message_types ste
                   ON ste.id = spm.message_type_id
          WHERE stm.name = i_system_name
            AND spm.name = i_param_name
            AND (i_message_type_name IS NULL
              OR ste.name = i_message_type_name);
      r_spm  c_spm%ROWTYPE;
   BEGIN
      OPEN c_spm;
      FETCH c_spm
         INTO r_spm;
      CLOSE c_spm;
      RETURN r_spm.VALUE;
   END f_get_system_param_value;
   FUNCTION f_get_system_param_val(i_system_id        IN NUMBER
                                  ,i_param_name       IN VARCHAR2
                                  ,i_message_type_id  IN NUMBER DEFAULT NULL)
      RETURN VARCHAR2
   IS
      CURSOR c_spm
      IS
         SELECT VALUE
           FROM gen_system_parameters spm
          WHERE spm.system_id = i_system_id
            AND spm.name = i_param_name
            AND (i_message_type_id IS NULL
              OR spm.message_type_id = i_message_type_id);
      r_spm  c_spm%ROWTYPE;
   BEGIN
      OPEN c_spm;
      FETCH c_spm
         INTO r_spm;
      CLOSE c_spm;
      RETURN r_spm.VALUE;
   END f_get_system_param_val;
   FUNCTION f_ins_system_message(
      i_system_message_rec  IN gen_system_messages%ROWTYPE)
      RETURN gen_system_messages.id%TYPE
   IS
      l_message_id  gen_system_messages.id%TYPE;
   BEGIN
      IF i_system_message_rec.message_type_id IS NOT NULL
      THEN
         -- Message type is active
         INSERT INTO gen_system_messages
              VALUES i_system_message_rec
           RETURNING id
                INTO l_message_id;
      END IF;
      RETURN l_message_id;
   END f_ins_system_message;
   PROCEDURE p_ins_system_message(
      i_system_message_rec  IN gen_system_messages%ROWTYPE)
   IS
      l_message_id  gen_system_messages.id%TYPE;
   BEGIN
      l_message_id   := f_ins_system_message(i_system_message_rec);
   END p_ins_system_message;
   FUNCTION f_create_system_message(
      i_system_name             IN VARCHAR2
     ,i_message_type_name       IN VARCHAR2
     ,i_message                 IN XMLTYPE
     ,i_status_description      IN VARCHAR2
     ,i_functional_reference_1  IN VARCHAR2 DEFAULT NULL
     ,i_functional_reference_2  IN VARCHAR2 DEFAULT NULL)
      RETURN gen_system_messages.id%TYPE
   IS
      l_scope        VARCHAR2(61) := g_package || 'p_create_system_message';
      l_message_rec  gen_system_messages%ROWTYPE;
      l_system_id    gen_system_messages.id%TYPE;
      PROCEDURE p_set_error(i_err_msg VARCHAR2)
      IS
      BEGIN
         logger.log_error(i_err_msg);
         l_message_rec.status_description   := i_err_msg;
         l_message_rec.status_id            :=
            gen_pck_general.f_get_status('SYSTEM_MESSAGES'
                                        ,'E');
      END p_set_error;
   BEGIN
      logger.log_information('Start'
                            ,l_scope);
      l_message_rec.system_id_from           :=
         gen_pck_systems.f_get_system_id(i_system_name);
      l_message_rec.message_type_id          :=
         gen_pck_systems.f_get_system_message_type_id(i_message_type_name);
      l_message_rec.MESSAGE                  := i_message;
      IF i_status_description IS NOT NULL
      THEN
         p_set_error(i_status_description);
      ELSIF l_message_rec.system_id_from IS NULL
      THEN
         p_set_error(i_system_name || ' system not found');
      ELSIF l_message_rec.message_type_id IS NULL
      THEN
         p_set_error('Message type ' || i_message_type_name || ' not found');
      ELSE
         l_message_rec.status_id      :=
            gen_pck_general.f_get_initial_status('SYSTEM_MESSAGES');
      END IF;
      l_message_rec.timestamp_processed      := SYSDATE;
      l_message_rec.functional_reference_1   := i_functional_reference_1;
      l_message_rec.functional_reference_2   := i_functional_reference_2;
      l_system_id                            :=
         f_ins_system_message(l_message_rec);
      logger.log_information('End'
                            ,l_scope);
      RETURN l_system_id;
   END f_create_system_message;
   PROCEDURE p_create_system_message(
      i_system_name             IN VARCHAR2
     ,i_message_type_name       IN VARCHAR2
     ,i_message                 IN XMLTYPE
     ,i_status_description      IN VARCHAR2
     ,i_functional_reference_1  IN VARCHAR2 DEFAULT NULL
     ,i_functional_reference_2  IN VARCHAR2 DEFAULT NULL)
   IS
      l_system_id  gen_system_messages.id%TYPE;
   BEGIN
      l_system_id      :=
         f_create_system_message(i_system_name
                                ,i_message_type_name
                                ,i_message
                                ,i_status_description
                                ,i_functional_reference_1
                                ,i_functional_reference_2);
   END p_create_system_message;
   ----------------------------------------------------------------------------
   -- Procedure that is called from the insert trigger of table gen_SYSTEM_MESSAGES
   -- and handles the incoming message in the appropriate way dependent on
   -- message type
   ----------------------------------------------------------------------------
   PROCEDURE p_receive_message(
      i_message_type          IN     gen_system_message_types.name%TYPE
     ,i_message_id            IN     gen_system_messages.id%TYPE
     ,i_system_id_from        IN     gen_system_messages.system_id_from%TYPE
     ,i_request_message       IN     gen_system_messages.MESSAGE%TYPE
     ,i_timestamp_message     IN     gen_system_messages.timestamp_message%TYPE
     ,i_message_reference_id  IN     gen_system_messages.message_reference_id%TYPE
     ,io_status_id            IN OUT gen_system_messages.status_id%TYPE
     ,io_status_description   IN OUT gen_system_messages.status_description%TYPE)
   IS
      l_status_code  gen_statuses.status_code%TYPE;
   BEGIN
      logger.LOG(
            'p_receive_message='
         || i_message_type
         || '-'
         || io_status_id
         || '-'
         || i_message_id);
      l_status_code      :=
         gen_pck_general.f_get_status_code('SYSTEM_MESSAGES'
                                          ,io_status_id);
      IF COALESCE(l_status_code
                 ,'I') != 'E'
      THEN
         CASE i_message_type
            WHEN 'EXAMPLE_MESSAGE_TYPE'
            THEN
               NULL;
            ELSE
               io_status_id            :=
                  gen_pck_general.f_get_status('SYSTEM_MESSAGES'
                                              ,'E');
               io_status_description   := 'Message type not configured';
         END CASE;
      END IF;
   END p_receive_message;
   PROCEDURE p_retry_messages
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'p_retry_messages';
   BEGIN
      FOR r_msg
         IN (  SELECT stm.package_name
                     ,ste.name message_type_name
                     ,sme.*
                 FROM gen_systems stm
                      INNER JOIN gen_system_parameters spe
                         ON stm.id = spe.system_id
                      INNER JOIN gen_system_messages sme
                         ON sme.system_id_to = stm.id
                      INNER JOIN gen_system_message_types ste
                         ON ste.id = sme.message_type_id
                      INNER JOIN gen_statuses sts ON sts.id = sme.status_id
                WHERE stm.package_name IS NOT NULL
                  AND spe.name = 'MAX_RETRIES'
                  AND spe.VALUE IS NOT NULL
                  AND sts.status_code = 'R'
                  AND ste.active_ind = 'Y'
                  AND sme.nr_retries <=
                         COALESCE(TO_NUMBER(REGEXP_REPLACE(spe.VALUE
                                                          ,'[^[:digit:]]'))
                                 ,0)
             ORDER BY sme.id)
      LOOP
         BEGIN
            r_msg.nr_retries   := r_msg.nr_retries + 1;
            EXECUTE IMMEDIATE
                  'begin '
               || r_msg.package_name
               || '.p_send_message(:p_message_type
                              ,:p_message
                              ,:p_system_id_to
                              ,:p_nr_retries
                              ,:p_status_id
                              ,:p_message_reference_id
                              ,:p_message_correlation_id
                              ,:p_status_description
                              ); end;'
               USING r_msg.message_type_name
                    ,r_msg.MESSAGE
                    ,r_msg.system_id_to
                    ,r_msg.nr_retries
                    ,OUT r_msg.status_id
                    ,OUT r_msg.message_reference_id
                    ,OUT r_msg.message_correlation_id
                    ,OUT r_msg.status_description;
            UPDATE gen_system_messages
               SET status_id                = r_msg.status_id
                  ,message_reference_id     = r_msg.message_reference_id
                  ,message_correlation_id   = r_msg.message_correlation_id
                  ,status_description       = r_msg.status_description
                  ,nr_retries               = r_msg.nr_retries
                  ,timestamp_processed      = SYSDATE
             WHERE id = r_msg.id;
         EXCEPTION
            WHEN OTHERS
            THEN
               logger.log_error(p_text => dbms_utility.format_error_backtrace
                               ,p_scope => l_scope);
         END;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
   END p_retry_messages;
   PROCEDURE p_push_queue
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      EXECUTE IMMEDIATE 'BEGIN APEX_MAIL.PUSH_QUEUE; END;';
   END;
   PROCEDURE p_mail_system_appl(i_system_id           IN NUMBER
                               ,i_status_id           IN NUMBER
                               ,i_message_type_id     IN NUMBER DEFAULT NULL
                               ,i_system_message_id   IN NUMBER
                               ,i_status_description  IN VARCHAR2)
   IS
      v_scope         VARCHAR2(61) := g_package || 'p_mail_system_appl';
      v_send_list     VARCHAR2(1000);
      v_groupsmail    VARCHAR2(500);
      v_mail_id       NUMBER;
      v_mte_rec       gen_mail_templates%ROWTYPE;
      l_status_code   gen_statuses.status_code%TYPE;
      CURSOR c_messages(cp_status_code IN VARCHAR2)
      IS
         SELECT DISTINCT p.VALUE send_mail_to
                        ,p.description subject
                        ,s.name
           FROM gen_systems s
               ,gen_system_parameters p
          WHERE p.system_id = i_system_id
            AND p.system_id = s.id
            AND ((p.message_type_id = i_message_type_id
              AND i_message_type_id IS NOT NULL)
              OR (p.message_type_id IS NULL))
            AND p.name = ('SEND_EMAIL_ON_' || cp_status_code);
      r_messages      c_messages%ROWTYPE;
      l_workspace_id  NUMBER;
      v_message_type  gen_system_message_types.name%TYPE;
   BEGIN

      logger.LOG('i_system_id=' || i_system_id);
      logger.LOG('i_status_id=' || i_status_id);
      logger.LOG('i_message_type_id=' || i_message_type_id);
      logger.LOG('i_system_message_id=' || i_system_message_id);
      logger.LOG('i_status_description=' || i_status_description);

      l_status_code      :=
         gen_pck_general.f_get_status_code('SYSTEM_MESSAGES'
                                          ,i_status_id);

      BEGIN
         SELECT name
           INTO v_message_type
           FROM gen_system_message_types
          WHERE id = i_message_type_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_message_type   := NULL;
      END;

      logger.LOG('i_system_id=' || i_system_id);
      logger.LOG('i_status_code=' || l_status_code);
      logger.LOG('i_message_type_id=' || i_message_type_id);
      OPEN c_messages(l_status_code);
      FETCH c_messages
         INTO r_messages;
      IF c_messages%FOUND
      THEN
         gen_pck_mail_templates.p_get_mail_template(
            'SYSTEM_MESSAGE_MAIL_' || l_status_code
           ,v_mte_rec);
         -- send_list samenstellen
         v_send_list   := r_messages.send_mail_to;
         v_mte_rec.subject      :=
            REPLACE(REPLACE(v_mte_rec.subject
                           ,'#MESSAGETYPE#'
                           ,v_message_type)
                   ,'#SYSTEM#'
                   ,r_messages.name);
         v_mte_rec.MESSAGE      :=
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(v_mte_rec.MESSAGE
                                                   ,'#MESSAGETYPE#'
                                                   ,v_message_type)
                                           ,'#DATE#'
                                           ,TO_CHAR(SYSDATE
                                                   ,'DD-MM-YYYY HH24:MI:SS'))
                                   ,'#SYSTEM#'
                                   ,r_messages.name)
                           ,'#MESSAGE_DESC#'
                           ,i_status_description)
                   ,'#PARAM_MESSAGE#'
                   ,r_messages.subject);
         v_mte_rec.message_html      :=
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(v_mte_rec.message_html
                                                   ,'#MESSAGETYPE#'
                                                   ,v_message_type)
                                           ,'#DATE#'
                                           ,TO_CHAR(SYSDATE
                                                   ,'DD-MM-YYYY HH24:MI:SS'))
                                   ,'#SYSTEM#'
                                   ,r_messages.name)
                           ,'#MESSAGE_DESC#'
                           ,i_status_description)
                   ,'#PARAM_MESSAGE#'
                   ,r_messages.subject);
         IF COALESCE(gen_pck_general.f_get_appl_parameter_value('SEND_MAIL_YN')
                    ,'N') = 'N'
         THEN
            v_send_list      :=
               gen_pck_general.f_get_appl_parameter_value('TEST_EMAIL_ADDRESS');
            v_groupsmail   := NULL;
         END IF;
         IF v_send_list IS NOT NULL
         THEN
            logger.LOG('Send email to:' || v_send_list);
            gen_pck_general.p_set_security_group_id;
            logger.LOG('v_mte_rec.sender:' || v_mte_rec.sender);
            logger.LOG('v_mte_rec.MESSAGE:' || v_mte_rec.MESSAGE);
            logger.LOG('v_mte_rec.message_html:' || v_mte_rec.message_html);
            logger.LOG('v_mte_rec.subject:' || v_mte_rec.subject);
            v_mail_id      :=
               apex_mail.send(p_to => v_send_list
                             ,p_from => v_mte_rec.sender
                             ,p_body => v_mte_rec.MESSAGE
                             ,p_body_html => v_mte_rec.message_html
                             ,p_subj => v_mte_rec.subject
                             ,p_cc => NULL
                             ,p_bcc => NULL);
            -- send mail
            p_push_queue;
         END IF;
      ELSE
         logger.LOG('no message found');
      END IF;
      CLOSE c_messages;
   EXCEPTION
      WHEN OTHERS
      THEN
         IF c_messages%ISOPEN
         THEN
            CLOSE c_messages;
         END IF;
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => v_scope);
   END p_mail_system_appl;
   PROCEDURE p_resubmit_message(i_message_id IN gen_system_messages.id%TYPE)
   IS
      CURSOR c_msg
      IS
         SELECT system_id_from
               ,system_id_to
               ,message_type_id
               ,gen_pck_general.f_get_initial_status('SYSTEM_MESSAGES')
                   status_id
               ,message_reference_id
               ,message_correlation_id
               ,SYSDATE timestamp_processed
               ,MESSAGE
               ,id
               ,functional_reference_1
               ,functional_reference_2
           FROM gen_system_messages
          WHERE id = i_message_id;
      r_msg  c_msg%ROWTYPE;
      CURSOR c_msg2
      IS
         SELECT system_id_from
               ,system_id_to
               ,message_type_id
               ,gen_pck_general.f_get_initial_status('SYSTEM_MESSAGES')
                   status_id
               ,message_reference_id
               ,message_correlation_id
               ,SYSDATE timestamp_message
               ,MESSAGE
               ,i_message_id id
               ,functional_reference_1
               ,functional_reference_2
           FROM gen_system_messages_history
          WHERE id = i_message_id;
   BEGIN
      OPEN c_msg;
      FETCH c_msg
         INTO r_msg;
      CLOSE c_msg;
      IF r_msg.id IS NOT NULL
      THEN
         INSERT INTO gen_system_messages(system_id_from
                                        ,system_id_to
                                        ,message_type_id
                                        ,status_id
                                        ,message_reference_id
                                        ,message_correlation_id
                                        ,timestamp_processed
                                        ,MESSAGE
                                        ,resubmit_original_msg_id
                                        ,functional_reference_1
                                        ,functional_reference_2)
              VALUES (r_msg.system_id_from
                     ,r_msg.system_id_to
                     ,r_msg.message_type_id
                     ,r_msg.status_id
                     ,r_msg.message_reference_id
                     ,r_msg.message_correlation_id
                     ,r_msg.timestamp_processed
                     ,r_msg.MESSAGE
                     ,r_msg.id
                     ,r_msg.functional_reference_1
                     ,r_msg.functional_reference_2);
         UPDATE gen_system_messages
            SET nr_resubmits      =
                     COALESCE(nr_resubmits
                             ,0)
                   + 1
          WHERE id = i_message_id;
      ELSE
         OPEN c_msg2;
         FETCH c_msg2
            INTO r_msg;
         CLOSE c_msg2;
         -- Copy history rec
         INSERT INTO gen_system_messages(system_id_from
                                        ,system_id_to
                                        ,message_type_id
                                        ,status_id
                                        ,message_reference_id
                                        ,message_correlation_id
                                        ,timestamp_processed
                                        ,MESSAGE
                                        ,resubmit_original_msg_id
                                        ,functional_reference_1
                                        ,functional_reference_2)
              VALUES (r_msg.system_id_from
                     ,r_msg.system_id_to
                     ,r_msg.message_type_id
                     ,r_msg.status_id
                     ,r_msg.message_reference_id
                     ,r_msg.message_correlation_id
                     ,r_msg.timestamp_processed
                     ,r_msg.MESSAGE
                     ,r_msg.id
                     ,r_msg.functional_reference_1
                     ,r_msg.functional_reference_2);
         UPDATE gen_system_messages_history
            SET nr_resubmits      =
                     COALESCE(nr_resubmits
                             ,0)
                   + 1
          WHERE id = i_message_id;
      END IF;
   END p_resubmit_message;
   PROCEDURE p_cleanup_sys_messages_hist(
      i_nr_days_before_cleanup  IN NUMBER DEFAULT NULL)
   IS
      l_scope  CONSTANT VARCHAR2(61)
                           := g_package || 'p_cleanup_sys_messages_hist' ;
      l_cleanup_date    DATE;
   BEGIN
      l_cleanup_date      :=
           TRUNC(SYSDATE)
         - COALESCE(i_nr_days_before_cleanup
                   ,365);
      logger.LOG(   'Gathering statistics of records from before '
                 || TO_CHAR(l_cleanup_date
                           ,'DD-MM-YYYY')
                ,l_scope);
      -- Merge number of messages per systems/type/month/status into
      -- gen_system_message_totals table
      MERGE INTO gen_system_message_totals sml
           USING (  SELECT COALESCE(sys_from.name
                                   ,gen_pck_general.f_get_appl_parameter_value('DEFAULT_SYSTEM',gen_pck_general.f_get_appl_parameter_value('APP_ID_FRAMEWORK')))
                              system_from
                          ,COALESCE(sys_to.name
                                   ,gen_pck_general.f_get_appl_parameter_value('DEFAULT_SYSTEM',gen_pck_general.f_get_appl_parameter_value('APP_ID_FRAMEWORK')))
                              system_to
                          ,sme.name MESSAGE_TYPE
                          ,TO_CHAR(COALESCE(timestamp_message
                                           ,timestamp_processed)
                                  ,'YYYY')
                              year
                          ,TO_CHAR(COALESCE(timestamp_message
                                           ,timestamp_processed)
                                  ,'MM')
                              month
                          ,sts.description status
                          ,COUNT(*) nr_messages
                      FROM gen_system_messages_history hist
                           LEFT OUTER JOIN gen_systems sys_from
                              ON (hist.system_id_from = sys_from.id)
                           LEFT OUTER JOIN gen_systems sys_to
                              ON (hist.system_id_to = sys_to.id)
                           INNER JOIN gen_system_message_types sme
                              ON sme.id = hist.message_type_id
                           INNER JOIN gen_statuses sts ON sts.id = hist.status_id
                     WHERE COALESCE(timestamp_message
                                   ,timestamp_processed) < l_cleanup_date
                  GROUP BY sys_from.name
                          ,sys_to.name
                          ,sme.name
                          ,TO_CHAR(COALESCE(timestamp_message
                                           ,timestamp_processed)
                                  ,'YYYY')
                          ,TO_CHAR(COALESCE(timestamp_message
                                           ,timestamp_processed)
                                  ,'MM')
                          ,sts.description) hist
              ON (sml.system_from = hist.system_from
              AND sml.system_to = hist.system_to
              AND sml.MESSAGE_TYPE = hist.MESSAGE_TYPE
              AND sml.year = hist.year
              AND sml.month = hist.month
              AND sml.status = hist.status)
      WHEN MATCHED
      THEN
         UPDATE SET nr_messages   = sml.nr_messages + hist.nr_messages
      WHEN NOT MATCHED
      THEN
         INSERT     (sml.system_from
                    ,sml.system_to
                    ,sml.MESSAGE_TYPE
                    ,sml.year
                    ,sml.month
                    ,sml.status
                    ,sml.nr_messages)
             VALUES (hist.system_from
                    ,hist.system_to
                    ,hist.MESSAGE_TYPE
                    ,hist.year
                    ,hist.month
                    ,hist.status
                    ,hist.nr_messages);
      -- and cleanup hist records
      logger.LOG(
            'Remove records from gen_system_messages_history from before '
         || TO_CHAR(l_cleanup_date
                   ,'DD-MM-YYYY')
        ,l_scope);
      DELETE FROM gen_system_messages_history hist
            WHERE COALESCE(timestamp_message
                          ,timestamp_processed) < l_cleanup_date;
      logger.LOG(SQL%ROWCOUNT || ' records removed'
                ,l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
         ROLLBACK;
   END p_cleanup_sys_messages_hist;
   FUNCTION f_get_status_description(i_system_message_id IN NUMBER)
      RETURN VARCHAR2
   IS
      CURSOR c_sys_msg
      IS
         SELECT sme.status_description
           FROM gen_system_messages sme
          WHERE sme.id = i_system_message_id;
      r_sys_msg  c_sys_msg%ROWTYPE;
   BEGIN
      OPEN c_sys_msg;
      FETCH c_sys_msg
         INTO r_sys_msg;
      CLOSE c_sys_msg;
      RETURN r_sys_msg.status_description;
   END f_get_status_description;
END gen_pck_systems;
/

/
QUIT
