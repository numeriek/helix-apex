CREATE OR REPLACE PACKAGE "HELIX"."KGC_INTERFACE_MOSOS" IS
-- PACKAGE SPEC kgc_interface_mosos
/*
   Wijzigingshistorie:
   ===================

   Bevindingnr	Wie	Wanneer
   Wat
   ---------------------------------------------------------------------------------------------------------
   Mantis 4265  JKO 2011-01-20 Rework
   - Mosos_result_bericht aangepast.

   Mantis 3047  GWE 2010-03-08 Rework
   - Bij meldingen na het openen van een verbinding altijd de verbinding sluiten.

   Mantis 3047  GWE 2010-01-19
   - Melding bij te grote berichten

   Mantis 3879	GWE	2009-12-21
   - Openen van de verbinding meermalen uitvoeren met een tussenliggende wachttijd van 0.1 seconden.
     Ananloog aan de ZIS interface.

   Rework Mantis 3184: Interface voor Dragon
                   RDE     2009-11-25
   - mail moet in html-formaat
   - hoeveelheid monster moet overgenomen worden
   Mantis 3184     RDE     2009-09-15
   - Interface voor Dragon: ook voor MOSOS kan opgegeven worden dat er direct gestart moet worden met
     de verwerking. Of niet, uiteraard.
   Mantis 3047     RDE     2009-09-15
   - Opmerking notitie: inkorten tot 2000
   Mantis 740      NtB     2009-06-11
   - Ivm. performance cursor c_onde in mosos_result_bericht aangepast:
     aanroep naar kgc_brie_00.eerder_geprint vervangen door not exists sub-select.
   - Routine vertaal_mosos_naar_helix aangepast: het zetten van beheer_in_zis
     toegevoegd in aanroep naar kgc_import.vertaal_naar_helix.

   Mantis 740      NtB     2009-05-08
   - Routine mosos_result_bericht aangepast nav. wijz. door rde, 06-04-2009:
     aanvragen zonder onderzoek-id worden nooit teruggestuurd; dit is bv het
     geval bij een extra aanvraag voor een bestaand materiaal.
   - Routines mosos_result_bericht en mosos_uitsl_spec_concl aangepast:
     wijzigingen tbv. verwerkingsverslag toegevoegd.

   Mantis 283      NtB     2009-04-29
   - Procedure merge_im_personen aangepast: bsn(_geverifieerd) opgenomen.

   Mantis 740      NtB     2009-04-29
   - Procedure mosos_uitsl_spec_concl weer gewijzigd in function en vergaand aangepast
     tbv. scherm KGCUITM01 en procedure mosos_result_bericht.
   - Function bepaal_mosos_onderzoekcode toegevoegd tbv. scherm KGCUITM01.
   - Procedure mosos_result_bericht aangepast: aanroep naar mosos_uitsl_spec_concl.

   Mantis 740      NtB     2009-03-16
   - Function mosos_uitsl_spec_concl gewijzigd in procedure.

   Mantis 740      NtB     2009-03-04
   - Procedure error_msg aangepast.
   - Procedure vertaal_mosos_naar_helix aangepast; opmerkingen retourneren.

   Mantis 740      NtB     2009-02-16
   - Packagenaam gewijzigd van KGC_MOSOS_CYTO in KGC_INTERFACE_MOSOS.
   - M.n. routine Mosos_result_bericht verdergaand aangepast.
   - Diverse aanpassingen aan alle routines.
   - Public global g_test ge-�roduceerd om verzenden van berichten af te vangen;
     voor het verzenden zijn momenteel nog geen voorzieningen.

   Mantis 740	NtB	2008-03-31
   - Dataflow gewiijzigd; autonomous transactions ge-introduceerd.
   - Overbodige code verwijderd of verplaatst naar andere packages.
   - Foutafhandeling verbeterd.
   - Code verbeterd nav. geconstateerde fouten/verbeteringen door R. Debets.

   Mantis 740	TTi/RPe	2007-09-25

   ...		RPe	2007-11
   Aanpassingen nav meldingen en overleg

   ...		RPe	2007-12
   Nieuwe meldingen en inzichten rde 05-12

   Beschrijving:
   =============
   Overzetten van de gegevens die via de HL7-listner ontvangen zijn naar de Helix-data.
   Aanmaken en verzenden van uitslagen in de vorm van HL7-berichten.
   Herverwerken van berichten die met status X in de berichtentabel gepplaatst zijn.
   Aansturen van de verschillende HL7-berichtenverwerkers.

   Het ontvangen bericht wordt vanuit HL7_berichten.bericht afhankelijk van het
   berichtensegment verdeeld over de verschillende IM-tabellen. Bij het ontleden van
   het bericht wordt programmatuur in het package KGC_ZIS-gebruikt.

   De vertaling van de binnengekomen berichten wordt gestart vanuit de After-Insert-trigger op
   de tabel HL7_Berichten. Een Correct verwerkt bericht wordt met de status V in de tabel-geplaatst
   Moso-berichten met de status X opnieuw verwerkt worden via een call naar de procedure
   rework_request-bericht.

   Via de procedure vertaal_naar_helix worden de gegevens van de im-tabellen overgezet naar de
   Helixtabellen. De procedure in kgc_import.vertaal_naar_helix wordt binen de vertaal naar helix
   procedure gebruikt. Informatie die niet via het package kgc_import verwerkt wordt, wordt
   binne dit package verwerkt. Een correct overget import krijgr de status afgehandeld in
   kgc_import_files.
   Zowel de rework als de vertaal-procedure worden gestart vanuit het scherm KGC_HL7010

   Afgesloten database-transacties in de aangeroepen packages zijn geisoleerd.

   Foutsituaties worden via E-mail gemeld.
   NB Nav meldingen rde van 05-12-2007 wordt er nu plotseling geen mail meer gestuurd
         als er wordt gewerkt vanaf scherm KGCIMPO01.

   Uitgaande berichten worden via het scherm KGC_HL7_10 gestart.
   De aanwezige berichten kunnen via het scherm KGC_IMPO_01 bewerkt worden.

   Let OP:
   1. De maximumlengte van een bericht Inclusief de technische velden is 4000 tekens. Als
      een bericht groter is dan 4000 tekens, moet de module die de insert uitvoert de exceptie
      afhandelen.
   2. Acknowledge berichten worden genegeerd.
   3. Het protocol wijkt op een aantal punten af van de internationale standaard voor HL7

   De programmatuur is gebaseerd op de scripts die door Numeriek aangeleverd zijn:
     1. cyto_mosos2helix.sql dd 01-06-2007
     2. cyto_helix2mosos.sql dd 29-05-2007
     3. package mosos_interface. ( 10-10-2007 )

   Bij de bouw is verder gebruik gemaakt van
     1.  Koppeling Mosos v0.5.
     2.  HL7 v2 berichtdefinitie dd 03-05-2007
   Het testen van het interface is gebaseerd op testing.sql dd 12-06-2007

   Overleg 16-10-2007:
      1.  De interfaceprogrammatuur uit KGC_MOSOS_INTERFACE opgenomen in dit package
      2.  Uitgaande berichten niet opslaan.
      3.  Mail verzending via KGC_ZIS
      4.  Starten via Kgc_zis inplaats van de trigger op kgc_hl7_berichten.

   Uitgaande Mail:
   1.  Verzendlijst Volgens systeemparameter g_sypa_mosos_import_oke
       Verzenden als het HL7_bericht Correct vertaald is naar de importtabellen ( status V )
       "Helix heeft een MOSOS-bericht ontvangen voor <zisnr>, <aanspreken> (<geslacht>) <geboortedatum>.
       Hetvolgende is in Helix aangemaakt: Monster <>; Onderzoek <>; Protocol <>.

   2.  Verzendlijst volgens systeemparameter g_sypa_MOSOS_import_niet_oke
       Verzonden als de status niet naar V gaat.
         Er is een fout geconstateerd bij de controles ("Indien niet alles goed is gegaan").
         Normaal gesproken zet de vertaling van de im_ tabellen de foutmelding in het veld Opmerkingen,
         en doet dan niets. Nu moet er ook nog een mail verzonden worden:
         Onderwerp: "Fout in nieuw MOSOS-bericht in Helix database <X>"
         Inhoud: "Helix heeft een MOSOS-bericht ontvangen voor <zisnr>, <aanspreken> (<geslacht>) <geboortedatum>.
         Bij de vertaling naar Helix is er echter een fout geconstateerd:
         <opmerkingen> Corrigeer de gegevens en verwerk opnieuw.

   3.  Verzendlijst volgens systeemparameter g_sypa_MOSOS_import_niet_oke
       Verzonden als de Rework goed verloopt.
         Helix heeft een MOSOS-bericht ontvangen voor <zisnr>, <aanspreken> (<geslacht>) <geboortedatum>.
         Dit is een aanvulling op een vorig bericht mbt Monster <> en Onderzoek <>:
         er wordt verzocht om ook een <code onderzoek> <naam onderzoek> uit te voeren!

   4.  Verzendlijst volgens systeemparameter g_sypa_MOSOS_ZIS_NIETGEVONDEN
       Verzonden als het zisnummer een op een match oplevert
         Er zijn in Helix echter mogelijk identieke personen gevonden - deze dienen achteraf handmatig samengevoegd
         te worden indien ze echt identiek blijken te zijn. Mogelijke dubbele: (hierna volgt een tabel)'

   NB  Om een genoemd mailtje te krijgen moet de betreffende gebruiker zicht aanmelden door voor zichzelf
       de genoemde syspar op J te zetten

   */

   /*** NtB: uitsluitend voor testdoeleinden, public global ***/
   g_test BOOLEAN DEFAULT FALSE;
   /***********************************************************/

   /*** van MOSOS ***********************************************************************************/

   -- verwerk een bericht met status X- opnieuw
   -- starten vanaf een console
   PROCEDURE rework_request_bericht(p_imfi_id kgc_import_files.imfi_id%TYPE);

   -- stuur het binnengekomen HL7 bericht naar de juiste verwerkingsstraat
   -- start vanuit de After-insert trigger.
   PROCEDURE routeer_hl7_berichten(p_hlbe_id     IN kgc_hl7_berichten.hlbe_id%TYPE
                                  ,p_hl7_msg     IN kgc_zis.hl7_msg -- NtB: ipv. p_bericht
                                  ,x_id          OUT kgc_hl7_berichten.id%TYPE
                                  ,x_entiteit    OUT kgc_hl7_berichten.entiteit%TYPE
                                  ,x_status      OUT kgc_hl7_berichten.status%TYPE --);
                                  ,x_opmerkingen OUT kgc_hl7_berichten.opmerkingen%TYPE); -- NtB: nieuw, resultaat van verwerking teruggeven

   -- deze is voor de aanroep vanuit scherm KGCIMPO01
   -- geisoleerde transactie.
   PROCEDURE vertaal_mosos_naar_helix(p_imfi_id     IN kgc_import_files.imfi_id%TYPE
                                     ,x_opmerkingen OUT kgc_import_files.opmerkingen%TYPE -- NtB: nieuw, resultaat van verwerking teruggeven
                                      );

   -- functie tbv de initiele waarde van de koppeling
   -- NtB: aanroep vanuit KGCIMPO01, vervallen
   -- FUNCTION get_initiele_waarde(p_imfi_id kgc_im_onderzoeken.imfi_id%TYPE) RETURN VARCHAR2;

   -- functie tbv het veranderen van de koppeling
   -- NtB: aanroep vanuit KGCIMPO01, vervallen
   -- FUNCTION change_koppeling(p_imfi_id kgc_im_onderzoeken.imfi_id%TYPE
   --                          ,p_status  VARCHAR2) RETURN VARCHAR2;
   /*** einde van MOSOS ******************************************************************************/
   --
   /*** naar MOSOS ***********************************************************************************/
   FUNCTION bepaal_mosos_onderzoekcode(p_hlbe_id      IN kgc_hl7_berichten.hlbe_id%TYPE
                                      ,p_hlbe_bericht IN kgc_hl7_berichten.bericht%TYPE)
      RETURN kgc_import_param_waarden.externe_id%TYPE;

   -- function voor het bepalen conclusie en specificatie
   FUNCTION mosos_uitsl_spec_concl(p_mosos_onderzoekcode IN kgc_import_param_waarden.externe_id%TYPE
                                  ,p_imlo_id             IN kgc_import_locaties.imlo_id%TYPE
                                  ,p_onde_id             IN kgc_onderzoeken.onde_id%TYPE
                                  ,x_status_uitslag      OUT VARCHAR2
                                  ,x_uitslag_conc        OUT VARCHAR2
                                  ,x_uitslag_spec        OUT VARCHAR2
                                  ,x_uitslag_tekst       OUT VARCHAR2
                                  ,x_fout_verslag        IN OUT VARCHAR2) RETURN BOOLEAN;

   PROCEDURE mosos_result_bericht(p_kafd_id     IN kgc_kgc_afdelingen.kafd_id%TYPE
                                 ,p_ongr_id     IN kgc_onderzoeksgroepen.ongr_id%TYPE
                                 ,p_onderzoeknr IN kgc_onderzoeken.onderzoeknr%TYPE
                                  -- ,p_definitieve_uitslagen VARCHAR2 -- NtB: alleen maar definitieve
                                 ,p_uitslagen_verzenden IN BOOLEAN
                                 ,x_status              OUT VARCHAR2 -- NtB: nieuw
                                 ,x_onde_msg_verz_count OUT NUMBER -- NtB: nieuw
                                 ,x_verslag             OUT VARCHAR2); -- NtB: nieuw
/*** einde naar MOSOS *****************************************************************************/
END KGC_INTERFACE_MOSOS;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_INTERFACE_MOSOS" IS
  -- PACKAGE BODY kgc_interface_mosos

  TYPE t_tab_waarden IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;

  CURSOR c_stgr(b_stgr_id NUMBER, b_onde_id NUMBER) IS

    SELECT stgr.*
          ,meti.meti_id
          ,meti.afgerond meti_afgerond
    FROM   kgc_stoftestgroepen stgr
          ,bas_metingen        meti
    WHERE  stgr.stgr_id = b_stgr_id
    AND    stgr.stgr_id = meti.stgr_id(+)
    AND    b_onde_id = meti.onde_id(+);

  r_stgr      c_stgr%ROWTYPE;
  r_stgr_null c_stgr%ROWTYPE; -- NtB: nieuw

  CURSOR c_meet(c_onde_id NUMBER, c_stgr_id NUMBER) IS
    SELECT meet.meti_id
          ,stof.code stof_code
          ,nvl(meet.meetwaarde
              ,mdet.waarde) meetwaarde
    FROM   bas_metingen           meti
          ,bas_meetwaarden        meet
          ,kgc_stoftesten         stof
          ,bas_meetwaarde_details mdet
    WHERE  meti.stgr_id = c_stgr_id
    AND    meti.onde_id = c_onde_id
    AND    meti.meti_id = meet.meti_id
    AND    meet.stof_id = stof.stof_id
    AND    meet.meet_id = mdet.meet_id(+)
    AND    1 = mdet.volgorde(+);

  g_datumformaatdoel     VARCHAR2(30) := 'DD-MM-YYYY';
  g_datumtijdformaatdoel VARCHAR2(30) := 'DD-MM-YYYY HH24:MI:SS'; -- RDE 20-02-2008 -- NtB: nav. aanpassingen RDE, 26-02-2008
  g_connection           utl_tcp.connection;
  g_connected            BOOLEAN := FALSE;
  g_database_name        VARCHAR2(64);

  g_imon_omschrijving kgc_im_onderzoeken.omschrijving%TYPE;
  g_imon_referentie   kgc_im_onderzoeken.referentie%TYPE;

  g_bestaand_materiaal BOOLEAN := FALSE; -- NtB: nieuw
  g_zis_fout_aanwezig  BOOLEAN := FALSE; -- rpe nav rde 05-12-2007
  g_soort_verwerking   VARCHAR2(1); -- rpe nav rde 05-12-2007
  -- T van Trigger
  -- S van scherm

  g_sypa_zis_locatie       kgc_systeem_parameters.standaard_waarde%TYPE;
  g_sypa_mosos_servernaam  kgc_systeem_parameters.standaard_waarde%TYPE;
  g_sypa_mosos_serverpoort kgc_systeem_parameters.standaard_waarde%TYPE;

  g_bool_dummy     BOOLEAN; -- NtB: nieuw
  g_char_dummy     VARCHAR2(4000); -- NtB: nieuw
  g_num_dummy      NUMBER; -- NtB: nieuw
  g_hl7_msg        kgc_zis.hl7_msg; -- NtB: ook als global beschikbaar geworden
  g_imfi_id        kgc_import_files.imfi_id%TYPE; -- NtB: global geworden
  g_imfo_id        kgc_im_foetussen.imfo_id%TYPE; -- NtB: nieuw
  g_imlo_id        kgc_import_locaties.imlo_id%TYPE; -- NtB: nieuw
  g_msh            VARCHAR2(10); -- NtB: global geworden
  g_nte            VARCHAR2(10); -- NtB: global geworden
  g_obr            VARCHAR2(10); -- NtB: global geworden
  g_orc            VARCHAR2(10); -- NtB: global geworden
  g_zbr            VARCHAR2(10); -- NtB: global geworden
  g_this_package   VARCHAR2(30) := 'KGC_INTERFACE_MOSOS';
  g_this_procedure VARCHAR2(30);
  g_zisnr          kgc_personen.zisnr%TYPE; -- NtB: global geworden

  -- NtB: forward declarations verwijderd, niet nodig
  --
  /*** hulproutines *********************************************************************************/

  -- vertaal datum van formaat-bron in p_datum masker  naar formaat-doel
  PROCEDURE formatteerdatum(x_datumstring  IN OUT VARCHAR2
                           ,p_datum_masker VARCHAR2) IS
  BEGIN

    x_datumstring := substr(x_datumstring
                           ,1
                           ,length(p_datum_masker));
    x_datumstring := to_char(to_date(x_datumstring
                                    ,p_datum_masker)
                            ,g_datumformaatdoel);

  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  -- NtB: nav. aanpassingen RDE, 26-02-2008
  PROCEDURE formatteerdatumtijd -- RDE 20-02-2008
  (x_datumtijdstring IN OUT VARCHAR2, p_datum_masker VARCHAR2) IS
  BEGIN

    x_datumtijdstring := substr(x_datumtijdstring
                               ,1
                               ,length(p_datum_masker));
    x_datumtijdstring := to_char(to_date(x_datumtijdstring
                                        ,p_datum_masker)
                                ,g_datumtijdformaatdoel);

  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  FUNCTION splits(p_pattern VARCHAR2, p_source VARCHAR2) RETURN t_tab_waarden IS
    v_tab_waarden t_tab_waarden;
    v_source      VARCHAR2(4000) := p_source;
    v_pos         NUMBER;
  BEGIN
    v_pos := instr(v_source
                  ,p_pattern);
    WHILE v_pos > 0
    LOOP
      v_tab_waarden(v_tab_waarden.COUNT + 1) := substr(v_source
                                                      ,1
                                                      ,v_pos - 1);
      v_source := substr(v_source
                        ,v_pos + 1);
      v_pos := instr(v_source
                    ,p_pattern);
    END LOOP;
    v_tab_waarden(v_tab_waarden.COUNT + 1) := v_source; -- restje
    RETURN v_tab_waarden;
  END splits;

  FUNCTION tohl7(p_tekst VARCHAR2) RETURN VARCHAR2 IS
    v_tekst VARCHAR2(32767) := p_tekst;
  BEGIN
    v_tekst := REPLACE(v_tekst
                      ,'|');
    v_tekst := REPLACE(v_tekst
                      ,'^');
    v_tekst := REPLACE(v_tekst
                      ,'~');
    v_tekst := REPLACE(v_tekst
                      ,'\');
    v_tekst := REPLACE(v_tekst
                      ,'&');
    RETURN v_tekst;
  END tohl7;

  /* NtB, 13-02-2009: niet meer in gebruik
  FUNCTION get_initiele_waarde(p_imfi_id kgc_im_onderzoeken.imfi_id%TYPE) RETURN VARCHAR2 IS
     CURSOR c_imon(b_imfi_id kgc_im_onderzoeken.imfi_id%TYPE) IS
        SELECT imon.referentie
              ,imon.omschrijving
          FROM kgc_im_onderzoeken imon
         WHERE imon.imfi_id = b_imfi_id;
     r_imon      c_imon%ROWTYPE;
     v_retwaarde VARCHAR2(32);

  BEGIN

     OPEN c_imon(p_imfi_id);
     FETCH c_imon
        INTO r_imon;

     --*  lijkt niet helemaal goed
     IF c_imon%FOUND
        AND g_imon_referentie IS NOT NULL
     THEN
        g_imon_referentie   := r_imon.referentie;
        g_imon_omschrijving := r_imon.omschrijving;
        v_retwaarde         := 'Ontkoppeld';

        UPDATE kgc_im_onderzoeken
           SET referentie   = NULL
              ,omschrijving = NULL
         WHERE imfi_id = p_imfi_id;

     ELSIF c_imon%FOUND
           AND g_imon_referentie IS NULL
     THEN
        g_imon_referentie   := NULL;
        g_imon_omschrijving := NULL;
        v_retwaarde         := 'Gekoppeld';
     ELSE

        v_retwaarde := 'Gekoppeld';

     END IF;
     CLOSE c_imon;

     /*
         if c_imon%found
         then
           if r_imon.referentie is null
           then
             \* niet gekoppeld *\
             v_retwaarde         := 'Ontkoppeld';
             g_imon_referentie   := null;
             g_imon_omschrijving := null;
           else
             \* wel gekoppeld *\
             v_retwaarde         := 'Gekoppeld';
             g_imon_referentie   := r_imon.referentie;
             g_imon_omschrijving := r_imon.omschrijving;
           end if;
         else
           \* notfound *\
           v_retwaarde := null;
         end if;
     **
     --    close c_imon;

     RETURN v_retwaarde;
  END get_initiele_waarde;
  */

  /* NtB, 13-02-2009: niet meer in gebruik
  FUNCTION change_koppeling(p_imfi_id kgc_im_onderzoeken.imfi_id%TYPE
                           ,p_status  VARCHAR2) RETURN VARCHAR2 IS
     CURSOR c_imon(b_imfi_id kgc_im_onderzoeken.imfi_id%TYPE) IS
        SELECT imon.referentie
              ,imon.omschrijving
          FROM kgc_im_onderzoeken imon
         WHERE imon.imfi_id = b_imfi_id;
     r_imon      c_imon%ROWTYPE;
     v_retwaarde VARCHAR2(32);

  BEGIN

     v_retwaarde := p_status;

     IF p_status NOT IN ('Gekoppeld', 'Ontkoppeld')
     THEN
        RETURN v_retwaarde;
     END IF;

     OPEN c_imon(p_imfi_id);
     FETCH c_imon
        INTO r_imon;

     IF c_imon%FOUND
        AND g_imon_referentie IS NOT NULL
        AND r_imon.referentie IS NULL
     THEN

        -- ontkoppeld --> gekoppeld

        UPDATE kgc_im_onderzoeken
           SET referentie   = g_imon_referentie
              ,omschrijving = g_imon_omschrijving
         WHERE imfi_id = p_imfi_id;

        g_imon_referentie   := NULL;
        g_imon_omschrijving := NULL;
        v_retwaarde         := 'Gekoppeld';

     ELSIF c_imon%FOUND
           AND g_imon_referentie IS NULL
           AND r_imon.referentie IS NOT NULL
     THEN

        -- gekoppeld --> ontkoppeld

        UPDATE kgc_im_onderzoeken
           SET referentie   = NULL
              ,omschrijving = NULL
         WHERE imfi_id = p_imfi_id;

        g_imon_referentie   := r_imon.referentie;
        g_imon_omschrijving := r_imon.omschrijving;
        v_retwaarde         := 'Ontkoppeld';

     END IF;
     CLOSE c_imon;

     COMMIT;
     RETURN v_retwaarde;
  END change_koppeling;
  */

  PROCEDURE send_mail_import_oke(p_hlbe_id kgc_hl7_berichten.hlbe_id%TYPE
                                ,p_hl7_msg kgc_zis.hl7_msg) IS
    -- complete verwerking
    CURSOR c_imfi(b_hlbe_id kgc_import_files.hlbe_id%TYPE) IS
      SELECT imfi.imfi_id
      FROM   kgc_import_files imfi
      WHERE  imfi.hlbe_id = b_hlbe_id;

    r_imfi       c_imfi%ROWTYPE;
    v_imfi_found BOOLEAN;

    v_mail_inhoud VARCHAR2(32767);
    v_pid         VARCHAR2(10);
    v_obr         VARCHAR2(10);
    v_onderwerp   VARCHAR2(256);
    v_pid_7_1_1   VARCHAR2(50); -- rde 19-11 pid 7-1 zou niet kunnen bestaan!!!!!
    -- v_zisnr       VARCHAR2(10); -- rde 19-11 nodig tbv formattering utrecht -- NtB: vervangen door g_zisnr

  BEGIN
    -- Verzendlijst1. Onderwerp: "Nieuw MOSOS-bericht ontvangen in Helix database <X>"
    -- Inhoud: "Helix heeft een MOSOS-bericht ontvangen voor <zisnr>, <aanspreken> (<geslacht>) <geboortedatum>.
    -- Het volgende is in Helix aangemaakt: Monster <>; Onderzoek <>; Protocol <>.

    -- nav opmerkingen rde 05-12. Alleen mail versturen bij 'trigger'-verwerking
    IF g_soort_verwerking = 'S' -- vanuit scherm IMPO01
    THEN
      RETURN;
    END IF;

    OPEN c_imfi(p_hlbe_id);
    FETCH c_imfi
      INTO r_imfi;
    v_imfi_found := c_imfi%FOUND;
    CLOSE c_imfi;

    IF v_imfi_found
    THEN
      -- NtB: dit is al eerder bepaald; gewoon doorgeven of in global stoppen
      v_pid := kgc_zis.getsegmentcode('PID'
                                     ,p_hl7_msg);
      v_obr := kgc_zis.getsegmentcode('OBR'
                                     ,p_hl7_msg);

      -- pid-7-1 zou evt niet aanwezig kunnen zijn?!
      v_pid_7_1_1 := NULL;

      IF p_hl7_msg(v_pid).COUNT > 6 -- dan is pid_7 aanwezig
      THEN
        v_pid_7_1_1 := p_hl7_msg(v_pid) (7) (1) (1);
      END IF;

      v_mail_inhoud := '<p>Helix heeft MOSOS-bericht ' || p_hlbe_id ||
                       ' ontvangen voor <blockquote><b>' || g_zisnr || ', ' ||
                       p_hl7_msg(v_pid) (5) (1)
                       (1) || ' (V) ' || v_pid_7_1_1 ||
                       '. </b></blockquote>' || chr(10) ||
                       'Het volgende daarvan is in Helix gebruikt: ' ||
                       chr(10) || '<blockquote>Monster ' ||
                       p_hl7_msg(v_obr) (15) (1)
                       (1) || --
                       '; Onderzoek ' || p_hl7_msg(v_obr) (4) (1)
                       (2) || --
                       ' ' || p_hl7_msg(v_obr) (4) (1)
                       (1) || --
                       '; Protocol ' || p_hl7_msg(v_obr) (4) (1)
                       (2) || '</blockquote></p>';
      -- NtB, 10-02-2009: chr(10) en html toegevoegd.

      v_onderwerp := 'Nieuw MOSOS-bericht ontvangen in Helix database ' ||
                     g_database_name;

      kgc_zis.post_mail_zis(p_sypa_van  => 'MOSOS_MAIL_AFZENDER'
                           ,p_sypa_aan  => 'MOSOS_IMPORT_HL7_OKE'
                           ,p_onderwerp => v_onderwerp
                           ,p_inhoud    => v_mail_inhoud
                           ,p_html      => TRUE -- Rework Mantis 3184: Interface voor Dragon
                           ,p_conditie  => 'J');

    END IF;

  END send_mail_import_oke;

  PROCEDURE send_mail_zis_controle(p_hlbe_id   kgc_hl7_berichten.hlbe_id%TYPE -- nw
                                  ,p_pers_trow cg$kgc_personen.cg$row_type
                                  ,p_hl7_msg   kgc_zis.hl7_msg
                                  ,p_imfi_id   kgc_import_files.imfi_id%TYPE) IS
    --------------------------------------------------------------------------------------
    -- Bepalen of er 'ZIS-problemen' zijn in dit bericht      --
    -- Kunnen evt twee mailtjes worden:                       --
    --    1 : Inhoud ZIS anders dan inhoud tabel              --
    --    2 : Zis-nr geeft mogelijke duplicaten               --
    -------------------------------------------------------------------------------------=

    CURSOR c_pers --
    (b_zisnr kgc_personen.zisnr%TYPE --
    , b_achternaam kgc_personen.achternaam%TYPE --
    , b_voorletters kgc_personen.voorletters%TYPE --
    , b_voorvoegsel kgc_personen.voorvoegsel%TYPE --
    , b_geboortedatum kgc_personen.geboortedatum%TYPE) IS
      SELECT pers.zisnr
            ,pers.achternaam
            ,pers.voorletters
            ,pers.voorvoegsel
            ,pers.geboortedatum
            ,pers.postcode
            ,pers.adres
            ,pers.woonplaats
      FROM   kgc_personen pers
      WHERE  pers.achternaam = b_achternaam
      AND    nvl(pers.voorletters
                ,'@@@') = nvl(b_voorletters
                              ,'@@@')
      AND    nvl(pers.voorvoegsel
                ,'@@@') = nvl(b_voorvoegsel
                              ,'@@@')
      AND    nvl(pers.geboortedatum
                ,to_date('01-01-3000'
                        ,'dd-mm-yyyy')) =
             nvl(b_geboortedatum
                 ,to_date('01-01-3000'
                         ,'dd-mm-yyyy'))
      AND    (pers.zisnr != b_zisnr OR pers.zisnr IS NULL); -- RDE 19-12-2007

    v_dup_aanwezig BOOLEAN;
    v_zis_gegevens VARCHAR2(32767); -- NtB: nieuw
    v_mail_inhoud  VARCHAR2(32767);
    v_onderwerp    VARCHAR2(256);
    v_pid          VARCHAR2(10);
    v_pid_5_1_1    VARCHAR2(50); -- NtB: nieuw
    v_pid_5_1_3    VARCHAR2(50); -- rde 19-11 pid 5-1-3 zou niet kunnen bestaan!!!!!
    v_pid_5_1_5    VARCHAR2(50); -- rde 19-11 pid 5-1-5 zou niet kunnen bestaan!!!!!
    v_pid_7_1_1    VARCHAR2(50); -- rde 19-11 pid 7-1   zou niet kunnen bestaan!!!!!
    -- v_zisnr        VARCHAR2(10); -- rde 19-11 nodig tbv formattering utrecht -- NtB: vervangen door g_zisnr

  BEGIN
    -- nav opmerkingen rde 05-12. Alleen mail versturen bij 'trigger'-verwerking
    -- NtB: en job???
    IF g_soort_verwerking = 'S' -- vanuit scherm IMPO01
    THEN
      RETURN;
    END IF;

    -- Init
    g_zis_fout_aanwezig := FALSE;
    v_pid               := kgc_zis.getsegmentcode('PID'
                                                 ,p_hl7_msg);
    v_pid_5_1_1         := p_hl7_msg(v_pid) (5) (1) (1);
    v_pid_5_1_3         := NULL;
    v_pid_5_1_5         := NULL;
    v_pid_7_1_1         := NULL;

    IF p_hl7_msg(v_pid) (5) (1).COUNT > 2 -- dan is pid_5-1-3 aanwezig
    THEN
      v_pid_5_1_3 := p_hl7_msg(v_pid) (5) (1) (3);
    END IF;

    IF p_hl7_msg(v_pid) (5) (1).COUNT > 4 -- dan is pid_5-1-5 aanwezig
    THEN
      v_pid_5_1_5 := p_hl7_msg(v_pid) (5) (1) (5);
    END IF;

    IF p_hl7_msg(v_pid).COUNT > 6 -- dan is pid_7 aanwezig
    THEN
      v_pid_7_1_1 := p_hl7_msg(v_pid) (7) (1) (1);
    END IF;

    /* RDE 4-2-2008 Onterechte mails verschil Helix en ZIS
    oud:
    ... kgc_util_00.verschil(p_hl7_msg(v_pid) (5) (1) (1)
                            ,p_pers_trow.achternaam)
    or  kgc_util_00.verschil(v_pid_5_1_5
                            ,p_pers_trow.voorvoegsel)
    or ...
    nieuw:
    */
    IF (kgc_util_00.verschil(upper(REPLACE(v_pid_5_1_1 ||
                                           REPLACE(v_pid_5_1_5
                                                  ,'.'
                                                  ,'')
                                          ,' '
                                          ,''))
                            ,upper(REPLACE(p_pers_trow.achternaam ||
                                           REPLACE(p_pers_trow.voorvoegsel
                                                  ,'.'
                                                  ,'')
                                          ,' '
                                          ,''))) -- NtB: evt. oplossing AVermeulen!!!!??????
       OR kgc_util_00.verschil(v_pid_5_1_3
                               ,p_pers_trow.voorletters) --
       OR kgc_util_00.verschil(v_pid_7_1_1
                               ,to_char(p_pers_trow.geboortedatum
                                       ,'yyyymmdd')))
    THEN
      -- PID wijkt af van tabel-inhoud
      -- mail 1a van verzendlijst2
      -- foutsituatie!
      -- NtB: Wel doorgaan met verwerken naar importtabellen, maar niet naar Helixtabellen
      -- NtB: status = 'V' en afgehandeld = 'N'
      g_zis_fout_aanwezig := TRUE;

      v_onderwerp := 'Fout in nieuw MOSOS-bericht in Helix database ' ||
                     g_database_name;

      -- NtB, 12-03-2009: indien geen ZIS-gegevens gevonden, bericht aanpassen
      v_zis_gegevens := nvl(TRIM(p_pers_trow.achternaam || ' ' ||
                                 p_pers_trow.voorletters || ' ' ||
                                 p_pers_trow.voorvoegsel || ' ' ||
                                 to_char(p_pers_trow.geboortedatum
                                        ,'yyyymmdd'))
                           ,'(geen ZIS-gegevens bekend)');

      v_mail_inhoud := '<p>Helix heeft MOSOS-bericht ' || p_hlbe_id ||
                       ' ontvangen met ZISnummer <b>' || g_zisnr ||
                       '</b>. ' || chr(10) ||
                       '<br>De gegevens in het bericht zijn <blockquote>' || --
                       v_pid_5_1_1 || ', ' || v_pid_5_1_3 || ', ' ||
                       v_pid_5_1_5 || ', ' || v_pid_7_1_1 || chr(10) || --
                       '</blockquote> en deze wijken af van de gegevens in ZIS, nl. <blockquote>' ||
                       v_zis_gegevens || --
                       '. </blockquote></p>';
      -- NtB, 10-02-2009: chr(10) en html toegevoegd.

      kgc_zis.post_mail_zis(p_sypa_van  => 'MOSOS_MAIL_AFZENDER'
                           ,p_sypa_aan  => 'MOSOS_ZIS_NIETGEVONDEN'
                           ,p_onderwerp => v_onderwerp
                           ,p_inhoud    => v_mail_inhoud
                           ,p_html      => TRUE -- Rework Mantis 3184: Interface voor Dragon
                           ,p_conditie  => 'J');

      -- NtB: Dit moet op een andere plek; in aanroepende routines; alleen opmerkingen retourneren
      UPDATE kgc_import_files imfi
      SET    opmerkingen = v_mail_inhoud || chr(10) || opmerkingen
      WHERE  imfi.imfi_id = p_imfi_id;

    ELSE
      -- Kontroleren op duplicaten
      -- mail 1b van verzend,lijst2
      -- Dit is meer een waarschuwing, geen echte fout!!

      v_dup_aanwezig := FALSE;

      v_onderwerp := 'Nieuw MOSOS-bericht ontvangen in Helix database ' ||
                     g_database_name;

      v_mail_inhoud := '<p>Helix heeft MOSOS-bericht ' || p_hlbe_id ||
                       ' ontvangen voor <blockquote><b>' || chr(10) ||
                       g_zisnr || ' ' || v_pid_5_1_1 || ', ' || v_pid_5_1_3 || ', ' ||
                       v_pid_5_1_5 || ' (V) ' || v_pid_7_1_1 ||
                       '. </b></blockquote>' || chr(10) ||
                       'Er zijn in Helix echter mogelijk identieke personen gevonden - ' ||
                       'deze dienen achteraf handmatig samengevoegd te worden, <br>indien ze echt identiek blijken te zijn. ' ||
                       chr(10) ||
                       'Mogelijke dubbele: (hierna volgt een tabel) </p>' ||
                       chr(10);
      -- NtB, 10-02-2009: chr(10) en html toegevoegd.

      FOR r_pers IN c_pers(g_zisnr -- NtB: vervangt p_hl7_msg(v_pid) (3) (1) (1)
                          ,v_pid_5_1_1
                          ,v_pid_5_1_3
                          ,v_pid_5_1_5
                          ,to_date(v_pid_7_1_1
                                  ,'yyyymmdd'))
      LOOP
        v_dup_aanwezig := TRUE;

        v_mail_inhoud := v_mail_inhoud || '<blockquote>' || r_pers.zisnr ||
                         chr(9) || r_pers.achternaam || chr(9) ||
                         r_pers.voorletters || chr(9) || r_pers.voorvoegsel ||
                         chr(9) || ' (V) ' || chr(9) ||
                         to_char(p_pers_trow.geboortedatum
                                ,'yyyymmdd') || chr(9) || r_pers.postcode ||
                         chr(9) || r_pers.adres || chr(9) ||
                         r_pers.woonplaats || '</blockquote>';
        -- NtB, 10-02-2009: chr(10) en html toegevoegd.
      END LOOP;

      IF v_dup_aanwezig
      THEN
        kgc_zis.post_mail_zis(p_sypa_van  => 'MOSOS_MAIL_AFZENDER'
                             ,p_sypa_aan  => 'MOSOS_ZIS_NIETGEVONDEN'
                             ,p_onderwerp => v_onderwerp
                             ,p_inhoud    => v_mail_inhoud
                             ,p_html      => TRUE -- Rework Mantis 3184: Interface voor Dragon
                             ,p_conditie  => 'J');

        /* er wordt GEEN melding gegeven!
        update kgc_import_files imfi
        set opmerkingen = 'Zisnummer mogelijk niet correct. ' || chr(10) || opmerkingen
        where imfi.imfi_id = p_imfi_id;*/

      END IF;

    END IF;

  END send_mail_zis_controle;

  /*
  3e mail: er is een fout geconstateerd bij de controles ("Indien niet alles goed is gegaan"). Normaal gesproken zet de vertaling van de
  im_ tabellen de foutmelding in het veld Opmerkingen, en doet dan niets. Nu moet er ook nog een mail verzonden worden:
  Onderwerp: "Fout in nieuw MOSOS-bericht in Helix database <X>"
  Inhoud: "Helix heeft een MOSOS-bericht ontvangen voor <zisnr>, <aanspreken> (<geslacht>) <geboortedatum>. Bij de vertaling naar Helix
  is er echter een fout geconstateerd:
  <opmerkingen>
  Corrigeer de gegevens en verwerk opnieuw.
  */
  PROCEDURE send_mail_import_niet_oke(p_hlbe_id   kgc_hl7_berichten.hlbe_id%TYPE
                                     ,p_opmerking kgc_hl7_berichten.opmerkingen%TYPE
                                     ,p_hl7_msg   kgc_zis.hl7_msg) IS

    v_mail_inhoud VARCHAR2(32767);
    v_pid         VARCHAR2(10);
    v_onderwerp   VARCHAR2(256);
    v_pid_7_1_1   VARCHAR2(50); -- rde 19-11 pid 7-1   zou niet kunnen bestaan!!!!!
    -- v_zisnr       VARCHAR2(10); -- rde 19-11 nodig tbv formattering utrecht -- NtB: vervangen door g_zisnr

  BEGIN
    -- nav opmerkingen rde 05-12. Alleen mail versturen bij 'trigger'-verwerking
    IF g_soort_verwerking = 'S' -- vanuit scherm IMPO01
       OR g_zis_fout_aanwezig -- de zismail is al een aanwijzing dat het niet goed is.
    -- afgeleide fouten leiden niet tot een mail.
    THEN
      RETURN;
    END IF;

    -- Init
    v_pid       := kgc_zis.getsegmentcode('PID'
                                         ,p_hl7_msg);
    v_pid_7_1_1 := NULL;

    IF p_hl7_msg(v_pid).COUNT > 6 -- dan is pid_7 aanwezig
    THEN
      v_pid_7_1_1 := p_hl7_msg(v_pid) (7) (1) (1);
    END IF;

    v_mail_inhoud := '<p>Helix heeft MOSOS-bericht ' || p_hlbe_id ||
                     ' ontvangen voor <blockquote><b>' || g_zisnr || ', ' ||
                     p_hl7_msg(v_pid) (5) (1)
                     (1) || ' ( V ) ' || v_pid_7_1_1 ||
                     '. </b></blockquote>' || chr(10) ||
                     'Bij de vertaling naar Helix is er echter een fout geconstateerd: <blockquote><i>' ||
                     chr(10) || p_opmerking || '. </i></blockquote>' ||
                     chr(10) ||
                     'Corrigeer de gegevens en verwerk opnieuw. </p>';
    -- NtB, 10-02-2009: chr(10) en html toegevoegd.

    v_onderwerp := 'Nieuw MOSOS-bericht ontvangen in Helix database ' ||
                   g_database_name;

    kgc_zis.post_mail_zis(p_sypa_van  => 'MOSOS_MAIL_AFZENDER'
                         ,p_sypa_aan  => 'MOSOS_IMPORT_HL7_NIET_OKE'
                         ,p_onderwerp => v_onderwerp
                         ,p_inhoud    => v_mail_inhoud
                         ,p_html      => TRUE -- Rework Mantis 3184: Interface voor Dragon
                         ,p_conditie  => 'J');

  END send_mail_import_niet_oke;

  /*
  Verzendlijst3. Onderwerp: "Nieuw MOSOS-bericht voor bestaand materiaal ontvangen in Helix database <X>"
  Inhoud: "Helix heeft een MOSOS-bericht ontvangen voor <zisnr>, <aanspreken> (<geslacht>) <geboortedatum>.
  Dit is een aanvulling op een vorig bericht mbt Monster <> en Onderzoek <>:
  er wordt verzocht om ook een <code onderzoek> <naam onderzoek> uit te voeren!
  */
  PROCEDURE send_mail_bestaand_materiaal(p_hlbe_id kgc_hl7_berichten.hlbe_id%TYPE
                                        ,p_hl7_msg kgc_zis.hl7_msg
                                        ,p_onde_id NUMBER) IS
    -- NtB: nav. aanpassingen RDE, 26-02-2008
    CURSOR c_onde(p_onde_id NUMBER) IS
      SELECT onderzoeknr
      FROM   kgc_onderzoeken onde
      WHERE  onde.onde_id = p_onde_id;

    r_onde        c_onde%ROWTYPE;
    v_mail_inhoud VARCHAR2(32767);
    v_onderwerp   VARCHAR2(256);
    v_obr         VARCHAR2(10);
    v_pid         VARCHAR2(10);
    v_pid_7_1_1   VARCHAR2(50); -- rde 19-11 pid 7-1   zou niet kunnen bestaan!!!!!
    -- v_zisnr       VARCHAR2(10); -- rde 19-11 nodig tbv formattering utrecht -- NtB: vervangen door g_zisnr

  BEGIN
    -- nav opmerkingen rde 05-12. Alleen mail versturen bij 'trigger'-verwerking
    IF g_soort_verwerking = 'S' -- vanuit scherm IMPO01
    THEN
      RETURN;
    END IF;

    -- Init
    v_pid       := kgc_zis.getsegmentcode('PID'
                                         ,p_hl7_msg);
    v_obr       := kgc_zis.getsegmentcode('OBR'
                                         ,p_hl7_msg);
    v_pid_7_1_1 := NULL; -- NtB: nieuw

    IF p_hl7_msg(v_pid).COUNT > 6 -- dan is pid_7 aanwezig
    THEN
      v_pid_7_1_1 := p_hl7_msg(v_pid) (7) (1) (1);
    END IF;

    v_mail_inhoud := '<p>Helix heeft MOSOS-bericht ' || p_hlbe_id ||
                     ' ontvangen voor <blockquote><b>' || --
                     g_zisnr || ', ' || p_hl7_msg(v_pid) (5) (1)
                     (1) || ' ( V ) ' || v_pid_7_1_1 ||
                     '. </b></blockquote>' || chr(10) ||
                     'Dit is een aanvulling op een vorig bericht mbt <br>Monster ' || --
                     p_hl7_msg(v_obr) (15) (1) (1) || ' en Onderzoek ' || --
                     p_hl7_msg(v_obr) (4) (1) (2) || ';<br>' || chr(10) ||
                     'er wordt verzocht om ook een ' || --
                     p_hl7_msg(v_obr) (4) (1) (1) || ' ' || --
                     p_hl7_msg(v_obr) (4) (1)
                     (2) || ' uit te voeren! </p><br>';
    -- NtB, 10-02-2009: chr(10) en html toegevoegd.

    -- toevoegen onderzoeksinfo   RDE 7-2-2008
    v_mail_inhoud := v_mail_inhoud || chr(10) || chr(10);

    OPEN c_onde(p_onde_id);
    FETCH c_onde
      INTO r_onde;
    IF c_onde%FOUND
    THEN
      v_mail_inhoud := v_mail_inhoud || 'Het betreft onderzoek ' ||
                       r_onde.onderzoeknr;
    ELSE
      v_mail_inhoud := v_mail_inhoud ||
                       'Het betreffende onderzoek is echter NOG NIET in Helix toegevoegd!';
    END IF;
    CLOSE c_onde;

    v_onderwerp := 'Nieuw MOSOS-bericht voor bestaand materiaal ontvangen in Helix database ' ||
                   g_database_name;

    kgc_zis.post_mail_zis(p_sypa_van  => 'MOSOS_MAIL_AFZENDER'
                         ,p_sypa_aan  => 'MOSOS_IMPORT_HL7_NIET_OKE'
                         ,p_onderwerp => v_onderwerp
                         ,p_inhoud    => v_mail_inhoud
                         ,p_html      => TRUE -- Rework Mantis 3184: Interface voor Dragon
                         ,p_conditie  => 'J');

  END send_mail_bestaand_materiaal;

  PROCEDURE send_mail_error(p_error_msg VARCHAR2, p_thisproc VARCHAR2) IS
  BEGIN
    IF nvl(length(p_error_msg)
          ,0) > 0
    THEN
      kgc_zis.post_mail_zis(p_sypa_aan  => kgc_zis.g_sypa_zis_mail_err --
                           ,p_onderwerp => p_thisproc ||
                                           ': Fout (database: ' ||
                                           upper(database_name) || ')!' --
                           ,p_inhoud    => 'Foutmelding: ' || p_error_msg
                            --,p_html      =>  -- := FALSE
                            --,p_conditie  =>  -- := NULL
                           ,p_sypa_van => 'MOSOS_MAIL_AFZENDER'); -- 04-03-2009. mantis 740
    END IF;
  END send_mail_error;

  -- TCP/IP connection to HL7-communicatie-engine (Cloverleaf/TDM/...):
  PROCEDURE open_verbinding(p_timeout IN NUMBER) -- tienden van seconden)
   IS
    v_sqlerrm VARCHAR2(4000);
    v_loop    NUMBER := 0;
  BEGIN

    <<debug_05_start>> -- uitsterren

    IF nvl(length(g_sypa_mosos_servernaam)
          ,0) = 0
    THEN
      qms$errors.show_message(p_mesg   => 'KGC-00000'
                             ,p_param0 => 'Geen standaardwaarde gevonden voor systeemparameter ZIS_HL7_SVR_NAAM_mosos!'
                             ,p_param1 => ''
                             ,p_errtp  => 'E'
                             ,p_rftf   => TRUE);
      RETURN;
    END IF;

    IF g_connected
    THEN
      qms$errors.show_message(p_mesg   => 'KGC-00000'
                             ,p_param0 => 'De verbinding naar ' ||
                                          g_sypa_mosos_servernaam || ':' ||
                                          to_char(g_sypa_mosos_serverpoort) ||
                                          ' is reeds geopend!'
                             ,p_param1 => SQLERRM
                             ,p_errtp  => 'E'
                             ,p_rftf   => TRUE);
    ELSE
      -- NtB: tbv. test connectie uitschakelen
      IF g_test
      THEN
        g_connected := TRUE;
      ELSE
        v_loop := 0;
        WHILE TRUE -- Mantis 3879 - 2009-12-21  GWE Loop toegevoegd
        LOOP
          v_loop := v_loop + 1;
          BEGIN
            g_connection := utl_tcp.open_connection(remote_host => g_sypa_mosos_servernaam
                                                   ,remote_port => g_sypa_mosos_serverpoort
                                                   ,tx_timeout  => 0.1 -- seconden! Zie doc: dit is niet de tijd voor wacht-op-connectie, maar wacht bij read!!! 0=nietwachten, null=waitforever
                                                    );

            g_connected := TRUE;
            EXIT; -- klaar met het leggen van de verbinding
          EXCEPTION
            WHEN OTHERS THEN
              IF SQLCODE = -29260
                 AND v_loop < p_timeout
              THEN
                -- network error: TNS:connection closed; geen fout! zolang
                -- effe wachten () en doorgaan
                dbms_lock.sleep(0.1); -- seconden! sys must grant execute on dbms_lock to ...;
              ELSE
                v_sqlerrm := SQLERRM; -- fout opbergen
                BEGIN
                  -- v2
                  utl_tcp.close_connection(g_connection);
                EXCEPTION
                  WHEN OTHERS THEN
                    NULL;
                END;
                g_connected := FALSE;
                qms$errors.show_message(p_mesg   => 'KGC-00000'
                                       ,p_param0 => 'Fout in verbinding naar ' ||
                                                    g_sypa_mosos_servernaam || ':' ||
                                                    to_char(g_sypa_mosos_serverpoort) || '!'
                                       ,p_param1 => v_sqlerrm
                                       ,p_errtp  => 'E'
                                       ,p_rftf   => TRUE);
              END IF;
          END;
        END LOOP;

      END IF; -- g_test
    END IF;
    <<debug_05_einde>>
    NULL;
  EXCEPTION
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg   => 'KGC-00000'
                             ,p_param0 => 'Verbinding is niet geopend!'
                             ,p_param1 => SQLERRM
                             ,p_errtp  => 'E'
                             ,p_rftf   => TRUE);
  END open_verbinding;

  FUNCTION send_msg(p_strhl7in  IN VARCHAR2
                   ,p_timeout   IN NUMBER -- tienden van seconden
                   ,x_strhl7out IN OUT VARCHAR2
                   ,x_msg       IN OUT VARCHAR2) RETURN BOOLEAN IS
    v_ret_val        BOOLEAN := TRUE;
    v_ret_conn       PLS_INTEGER;
    v_data_available PLS_INTEGER;
    v_nrcharrec      PLS_INTEGER;
    v_loop           NUMBER := 0;
    v_str            VARCHAR2(32000);
    v_strantw        VARCHAR2(32000);
    v_thisproc       VARCHAR2(100) := 'kgc_interface_mosos.send_msg'; --****afwijkend
  BEGIN
    IF NOT g_connected
    THEN
      qms$errors.show_message(p_mesg   => 'KGC-00000'
                             ,p_param0 => 'Fout in ' || v_thisproc ||
                                          ': eerst moet de verbinding geopend worden!'
                             ,p_param1 => SQLERRM
                             ,p_errtp  => 'E'
                             ,p_rftf   => TRUE);
    END IF;

    -- NtB: tbv. test connectie uitschakelen >>>
    IF g_test
    THEN
      NULL;
      /* NtB, tabel bestaat alleen in ontwikkelomgeving en funct. systeemtestomgeving
      INSERT INTO tmp_email_afvang
         (van
         ,aan
         ,message
         ,onderwerp)
      VALUES
         ('Helix'
         ,'Mosos'
         ,p_strhl7in
         ,'Uitslag');
      */
    ELSE
      -- <<<

      kgc_zis.log(v_thisproc ||
                  ': Connectie is geopend; message wordt verzonden...');
      v_ret_conn := utl_tcp.write_raw(g_connection
                                     ,utl_raw.cast_to_raw(p_strhl7in)); -- send request

      IF v_ret_conn = 0
      THEN
        -- aantal verzonden bytes
        x_msg := v_thisproc ||
                 ': Error: er kon geen bericht verzonden worden!';
        kgc_zis.log(x_msg);
        RETURN FALSE;
      END IF;

      kgc_zis.log(v_thisproc ||
                  ': Message is verzonden; wacht op antwoord...');

      LOOP
        v_loop           := v_loop + 1;
        v_data_available := utl_tcp.available(g_connection);
        -- terugkomende gegevens ophalen:
        IF v_data_available > 0
        THEN

          BEGIN
            LOOP
              IF v_data_available = 0
              THEN
                EXIT;
              END IF;
              v_nrcharrec := utl_tcp.read_text(g_connection
                                              ,v_str
                                              ,1024);
              -- zie doc:'On some platforms, this function (= available) may only return 1, to indicate that some data is available'
              v_strantw        := v_strantw || v_str;
              v_data_available := utl_tcp.available(g_connection); -- meer?
            END LOOP;

          EXCEPTION
            WHEN utl_tcp.end_of_input THEN
              NULL; -- end of input; geen fout!
            WHEN OTHERS THEN
              IF SQLCODE = -29260
              THEN
                NULL; -- network error: TNS:connection closed; geen fout!
              ELSE
                RAISE;
              END IF;
          END;

          kgc_zis.log(v_thisproc || ': Antwoord ingelezen!');
          EXIT;

        END IF;

        IF v_loop < p_timeout
        THEN
          dbms_lock.sleep(0.1); -- seconden!
        ELSE
          -- timeout!!!
          x_msg := 'Timeout na ' || to_char(p_timeout / 10) || ' seconden!';
          kgc_zis.log(v_thisproc || ': ' || x_msg);
          v_ret_val := FALSE;
          EXIT;
        END IF;

      END LOOP;

      IF nvl(length(v_strantw)
            ,0) = 0
         AND v_ret_val = TRUE
      THEN
        x_msg     := 'Lege reactie ontvangen!';
        v_ret_val := FALSE;
      END IF;

      x_strhl7out := v_strantw;

    END IF; -- g_test

    RETURN v_ret_val;

  EXCEPTION
    WHEN OTHERS THEN
      x_msg := v_thisproc || ': Error: ' || SQLERRM;
      kgc_zis.log(x_msg);
      RETURN FALSE;

  END send_msg;

  PROCEDURE sluit_verbinding IS
  BEGIN
    IF g_connected
    THEN
      g_connected := FALSE;
      -- NtB: tbv. test connectie uitschakelen
      IF g_test
      THEN
        NULL;
      ELSE
        utl_tcp.close_connection(g_connection);
      END IF; -- g_test
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg   => 'KGC-00000'
                             ,p_param0 => 'Verbinding is niet gesloten!'
                             ,p_param1 => SQLERRM
                             ,p_errtp  => 'E'
                             ,p_rftf   => TRUE);
  END sluit_verbinding;

  /*** van MOSOS ***********************************************************************************/

  -- relatie toevoegen
  -- verplaatst naar kgc_interface_generiek nav Mantis 3184: Interface voor Dragon

  -- verzekeraar toevoegen ***
  -- verplaatst naar kgc_interface_generiek nav Mantis 3184: Interface voor Dragon

  FUNCTION merge_im_onde_kopie(p_impw_id          IN NUMBER
                              ,p_code_kopiehouder IN kgc_im_onde_kopie.code_kopiehouder%TYPE
                               --,p_imok_trow        IN OUT cg$kgc_im_onde_kopie.cg$row_type
                               ) RETURN BOOLEAN IS

    CURSOR c_imok --
    (b_imfi_id kgc_import_files.imfi_id%TYPE --
    , b_imon_id kgc_im_onderzoeken.imon_id%TYPE --
    , b_impw_id NUMBER) IS
      SELECT imok.ROWID
      FROM   kgc_im_onde_kopie imok
      WHERE  imok.imfi_id = b_imfi_id
      AND    imok.imon_id = b_imon_id
      AND    imok.impw_id_rela = b_impw_id;

    v_imok_trow      cg$kgc_im_onde_kopie.cg$row_type;
    v_imok_trow_null cg$kgc_im_onde_kopie.cg$row_type;
    v_update_lijst   cg$kgc_im_onde_kopie.cg$ind_type;

  BEGIN
    -- Init
    g_this_procedure := 'merge_im_onde_kopie'; -- NtB: nieuw

    v_imok_trow              := v_imok_trow_null;
    v_imok_trow.imfi_id      := g_imfi_id;
    v_imok_trow.impw_id_rela := p_impw_id;
    v_imok_trow.imon_id      := TRIM(g_hl7_msg(g_obr) (2) (1)
                                     (1) || '-' || g_hl7_msg(g_obr) (4) (1) (1));
    v_imok_trow.imok_id      := NULL;
    -- eerste op 28,1 volgende 28,10 de derde op 28,19dus op 1 + i*9
    v_imok_trow.code_kopiehouder := p_code_kopiehouder; -- NtB: was g_hl7_msg(g_obr) (28) (i_koho) (1);

    -- OPEN c_imok(v_imok_trow.imok_id); -- NtB, 13-02-2009:
    -- vreemd, imok_id is altijd null hier, je zal dus niets vinden.
    -- Beter kijken of binnen bericht en onderzoek de relatie al voorkomt. In
    -- dat geval niet nog eens een keer nieuw opvoeren
    OPEN c_imok(v_imok_trow.imfi_id
               ,v_imok_trow.imon_id
               ,p_impw_id);
    FETCH c_imok
      INTO v_imok_trow.the_rowid;
    CLOSE c_imok;

    IF v_imok_trow.the_rowid IS NULL
    THEN
      cg$kgc_im_onde_kopie.ins(v_imok_trow
                              ,cg$kgc_im_onde_kopie.cg$ind_true);
    ELSE
      v_update_lijst               := cg$kgc_im_onde_kopie.cg$ind_true;
      v_update_lijst.created_by    := FALSE;
      v_update_lijst.creation_date := FALSE;
      cg$kgc_im_onde_kopie.upd(cg$rec => v_imok_trow
                              ,cg$ind => v_update_lijst);
    END IF;

    -- p_imok_trow := v_imok_trow;

    RETURN TRUE; -- NtB:

  EXCEPTION
    -- NtB: nieuw
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_IM_ONDE_KOPIE. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);
      RETURN FALSE;

  END merge_im_onde_kopie;

  FUNCTION merge_relaties(p_koho IN PLS_INTEGER, p_impw_id OUT NUMBER)
    RETURN BOOLEAN IS

    CURSOR c_rela(b_rela_trow IN cg$kgc_relaties.cg$row_type) IS
    -- in Utrecht wordt de relatie geidentificeerd door
    -- 2. de LISZ-code
    -- 3. al bestaand is zonder interne/LISZ-code => dan check via attributen
      SELECT 2 sorteer
            ,rela.rela_id
      FROM   kgc_relaties rela
      WHERE  rela.liszcode = b_rela_trow.liszcode
      UNION
      SELECT 3
            ,rela.rela_id
      FROM   kgc_relaties rela
      WHERE  rela.achternaam = b_rela_trow.achternaam
      AND    nvl(upper(rela.voorletters)
                ,'@#@') = nvl(upper(b_rela_trow.voorletters)
                              ,'@#@')
      AND    nvl(upper(rela.voorvoegsel)
                ,'@#@') = nvl(upper(b_rela_trow.voorvoegsel)
                              ,'@#@')
      AND    (rela.postcode = b_rela_trow.postcode OR
            rela.adres || rela.woonplaats =
            b_rela_trow.adres || b_rela_trow.woonplaats) -- bah: buitenland: geen lisz; postcode kan leeg zijn!
      ORDER  BY 1;

    -- NtB: nieuw
    v_helix_waarde   kgc_import_param_waarden.helix_waarde%TYPE;
    v_rela_trow      cg$kgc_relaties.cg$row_type;
    v_rela_trow_null cg$kgc_relaties.cg$row_type;
    -- v_impw_id         NUMBER;
    v_instelling_naam VARCHAR2(2000);
    v_msg_rela        VARCHAR2(32767);

  BEGIN

    -- Init
    g_this_procedure := 'merge_relaties'; -- NtB: nieuw

    -- alleen tonen... er zijn nog geen interface-tabellen om het op te bergen!
    -- OBR-28 bevat de kopiehouders
    -- let op: indien een kopiehouder de aanvrager is: wel registreren als relatie, maar niet toevoegen als kopiehouder (de eindbrief wordt immers aan de aanvrager gericht!)
    --  dbms_output.put_line('Verwerking OBX - Kopiehouders');

    -- code gevuld!
    -- impw-id bepalen (bestaat eventueel al)
    p_impw_id := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                     ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                     ,p_externe_id         => g_hl7_msg(g_obr) (28)
                                                              (p_koho) (1)
                                     ,p_hint               => 'Aanvragers/kopiehouders'
                                     ,p_toevoegen          => TRUE);
    SELECT helix_waarde
    INTO   v_helix_waarde
    FROM   kgc_import_param_waarden impw
    WHERE  impw.impw_id = p_impw_id; -- altijd 1 gevonden, want evt net toegevoegd!

    v_rela_trow             := v_rela_trow_null; -- init!
    v_rela_trow.voorletters := substr(g_hl7_msg(g_obr) (28) (p_koho) (4)
                                     ,1
                                     ,10); -- indien code gevuld, dan bestaat sub-4 ook
    IF nvl(length(v_rela_trow.voorletters)
          ,0) = 0
    THEN
      -- NtB: 2x voorletters!?, moet dit niet trow.voornaam zijn?????? -->
      v_rela_trow.voorletters := substr(g_hl7_msg(g_obr) (28) (p_koho) (3)
                                       ,1
                                       ,10); -- indien code gevuld, dan bestaat sub-3 ook
    END IF;

    v_rela_trow.achternaam := substr(ltrim(g_hl7_msg(g_obr) (28) (p_koho) (2))
                                    ,1
                                    ,30); -- indien code gevuld, dan bestaat sub-2 ook

    v_rela_trow.voorvoegsel := substr(ltrim(g_hl7_msg(g_obr) (28) (p_koho) (6))
                                     ,1
                                     ,10); -- indien code gevuld, dan bestaat sub-6 ook ( evt leeg )

    IF nvl(length(g_zbr)
          ,0) > 0
    THEN
      IF g_hl7_msg(g_zbr) (3).COUNT >= p_koho
      THEN
        IF g_hl7_msg(g_zbr) (3) (p_koho).COUNT >= 5
        THEN
          v_rela_trow.adres      := substr(ltrim(g_hl7_msg(g_zbr) (3)
                                                 (p_koho) (1))
                                          ,1
                                          ,30);
          v_rela_trow.postcode   := substr(g_hl7_msg(g_zbr) (3) (p_koho) (5)
                                          ,1
                                          ,7);
          v_rela_trow.woonplaats := substr(g_hl7_msg(g_zbr) (3) (p_koho) (3)
                                          ,1
                                          ,50);
          -- postcode ed formatteren
          kgc_zis.format_geo(x_postcode   => v_rela_trow.postcode
                            ,x_woonplaats => v_rela_trow.woonplaats
                            ,x_provincie  => v_rela_trow.provincie
                            ,x_land       => v_rela_trow.land
                            ,x_msg        => v_msg_rela);
        END IF;
      END IF;
      -- check instelling:
      kgc_zis.gethl7waarde(g_hl7_msg(g_zbr) (2) (p_koho) (1)
                          ,v_instelling_naam);
      IF nvl(length(v_instelling_naam)
            ,0) > 0
      THEN
        -- naam instelling is gevuld!
        IF nvl(length(v_rela_trow.achternaam)
              ,0) = 0
        THEN
          -- de kopiehouder is een instelling!
          v_rela_trow.achternaam := substr(v_instelling_naam
                                          ,1
                                          ,30);
        END IF;
      END IF;
      -- je zou er over kunnen denken om deze automatisch te gaan toevoegen als kgc_instellingen
      -- maar dit is niet voor de hand liggend: je moet sowieso de relatie toevoegen
      -- als dan de relatie een instelling blijkt te zijn, dan zit je 100% dezelfde gegevens op te slaan, en wellicht nog dubbel ook (match bestaande instellingen?)
      -- dus: niet doen!
    END IF;

    -- afgeleid:
    v_rela_trow.aanspreken := v_rela_trow.voorletters || ' ' ||
                              v_rela_trow.achternaam;

    -- indien nieuw, dan match proberen:
    IF v_helix_waarde IS NULL
    THEN
      OPEN c_rela(v_rela_trow);
      FETCH c_rela
        INTO g_num_dummy, v_rela_trow.rela_id;
      CLOSE c_rela;

      IF v_rela_trow.rela_id IS NULL
      THEN
        -- relatie bestaat niet - toevoegen!
        IF v_rela_trow.achternaam IS NOT NULL
        THEN
          -- toch wel de minste voorwaarde!
          -- automatisch code uitgeven!
          SELECT kgc_rela_seq.NEXTVAL INTO v_rela_trow.rela_id FROM dual;

          v_rela_trow.code      := 'ZIS' || lpad(to_char(v_rela_trow.rela_id)
                                                ,7
                                                ,'0');
          v_rela_trow.vervallen := 'N';
          cg$kgc_relaties.ins(v_rela_trow
                             ,cg$kgc_relaties.cg$ind_true);
          v_helix_waarde := v_rela_trow.rela_id;
        END IF;
      END IF;

      IF v_rela_trow.rela_id IS NOT NULL
      THEN
        -- koppelen!
        UPDATE kgc_import_param_waarden impw
        SET    helix_waarde = to_char(v_rela_trow.rela_id)
        WHERE  impw.impw_id = p_impw_id;
      END IF;
    END IF;

    -- relatie bestaat nu in kgc_relaties.
    RETURN TRUE; -- NtB:

  EXCEPTION
    -- NtB: nieuw
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_RELATIES. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);
      RETURN FALSE;

  END merge_relaties;

  FUNCTION merge_im_foetussen(p_imfo_trow IN OUT cg$kgc_im_foetussen.cg$row_type
                             ,p_imzw_trow IN cg$kgc_im_zwangerschappen.cg$row_type)
    RETURN BOOLEAN IS

    v_imfo_trow    cg$kgc_im_foetussen.cg$row_type; -- NtB: nieuw
    v_update_lijst cg$kgc_im_foetussen.cg$ind_type;

  BEGIN

    -- Init
    g_this_procedure    := 'merge_im_foetussen'; -- NtB: nieuw
    v_imfo_trow.imfi_id := g_imfi_id;
    v_imfo_trow.imfo_id := p_imzw_trow.nummer || '-' || g_hl7_msg(g_obr) (13) (1) (3); -- unieke id foetus: zwan.nummer + foetusnummer

    BEGIN
      -- NtB: hersteld
      cg$kgc_im_foetussen.slct(v_imfo_trow); -- eventueel bestaande gegevens ophalen
    EXCEPTION
      WHEN OTHERS THEN
        -- no data found, maar verpakt in 20999...
        NULL;
        -- NtB: errorstack leegmaken, deze exception willen we niet zien
        -- cg$errors.clear;
        g_bool_dummy := cg$errors.pop_head(g_char_dummy);
    END;

    v_imfo_trow.niet_geboren           := 'N'; --itt specs, nav opm rde 19-11
    v_imfo_trow.geplande_geboortedatum := p_imzw_trow.datum_aterm; -- overnemen!
    v_imfo_trow.imzw_id                := p_imzw_trow.imzw_id;
    v_imfo_trow.impe_id_moeder         := g_zisnr;
    --  v_imfo_trow.impw_id_positie
    --  v_imfo_trow.impe_id_geboren_als
    --  v_imfo_trow.commentaar

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 3
    THEN
      v_imfo_trow.volgnr := g_hl7_msg(g_obr) (13) (1) (3);
    END IF;

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 4
    THEN
      v_imfo_trow.impw_id_geslacht := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                          ,p_helix_table_domain => kgc_import.g_cimpw_dom_geslacht
                                                          ,p_externe_id         => nvl(g_hl7_msg(g_obr) (13) (1) (4)
                                                                                      ,'O') -- LET OP!!! igv leeg: Onbekend is de beste optie!
                                                          ,p_hint               => NULL
                                                          ,p_toevoegen          => TRUE);
    END IF;

    IF v_imfo_trow.the_rowid IS NULL
    THEN
      cg$kgc_im_foetussen.ins(cg$rec => v_imfo_trow
                             ,cg$ind => cg$kgc_im_foetussen.cg$ind_true);
    ELSE
      v_update_lijst               := cg$kgc_im_foetussen.cg$ind_true;
      v_update_lijst.created_by    := FALSE;
      v_update_lijst.creation_date := FALSE;
      cg$kgc_im_foetussen.upd(cg$rec => v_imfo_trow
                             ,cg$ind => v_update_lijst);
    END IF;

    p_imfo_trow := v_imfo_trow; -- NtB: nieuw

    RETURN TRUE;

  EXCEPTION
    -- NtB: nieuw
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_IM_FOETUSSEN. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);
      RETURN FALSE;

  END merge_im_foetussen;

  -- PROCEDURE merge_im_personen(p_impe_trow IN OUT cg$kgc_im_personen.cg$row_type) IS
  -- NtB: vernieuwd
  FUNCTION merge_im_personen(p_pers_trow IN OUT cg$kgc_personen.cg$row_type
                            ,p_rela_trow IN OUT cg$kgc_relaties.cg$row_type
                            ,p_verz_trow IN OUT cg$kgc_verzekeraars.cg$row_type)
    RETURN BOOLEAN IS

    v_impe_trow    cg$kgc_im_personen.cg$row_type; -- NtB: nieuw
    v_update_lijst cg$kgc_im_personen.cg$ind_type;

  BEGIN
    -- Init
    g_this_procedure    := 'merge_im_personen'; -- NtB: nieuw
    v_impe_trow.imfi_id := g_imfi_id;
    v_impe_trow.impe_id := g_zisnr;

    BEGIN
      cg$kgc_im_personen.slct(v_impe_trow); -- eventueel bestaande gegevens ophalen
    EXCEPTION
      WHEN OTHERS THEN
        -- no data found, maar verpakt in 20999...
        NULL;
        -- NtB: errorstack leegmaken, deze exception willen we niet zien
        -- cg$errors.clear;
        g_bool_dummy := cg$errors.pop_head(g_char_dummy);
    END;

    -- vertalen
    v_impe_trow.achternaam         := p_pers_trow.achternaam;
    v_impe_trow.gehuwd             := p_pers_trow.gehuwd;
    v_impe_trow.impw_id_geslacht   := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                          ,p_helix_table_domain => kgc_import.g_cimpw_dom_geslacht
                                                          ,p_externe_id         => p_pers_trow.geslacht
                                                          ,p_hint               => NULL
                                                          ,p_toevoegen          => TRUE);
    v_impe_trow.bloedverwant       := p_pers_trow.bloedverwant;
    v_impe_trow.meerling           := p_pers_trow.meerling;
    v_impe_trow.overleden          := p_pers_trow.overleden;
    v_impe_trow.default_aanspreken := p_pers_trow.default_aanspreken;

    -- huisarts
    kgc_interface_generiek.insert_huisarts(p_rela_trow);

    IF p_rela_trow.rela_id IS NULL
    THEN
      v_impe_trow.impw_id_rela := NULL;
    ELSE
      v_impe_trow.impw_id_rela := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                      ,p_externe_id         => p_rela_trow.rela_id
                                                      ,p_hint               => 'Reeds bekend'
                                                      ,p_helix_waarde       => p_rela_trow.rela_id
                                                      ,p_toevoegen          => TRUE);
    END IF;

    -- verzekeraar:
    kgc_interface_generiek.insert_verzekeraar(p_verz_trow);

    IF p_verz_trow.verz_id IS NULL
    THEN
      v_impe_trow.impw_id_verz := NULL;
    ELSE
      v_impe_trow.impw_id_verz := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_verz
                                                      ,p_externe_id         => p_verz_trow.verz_id
                                                      ,p_hint               => 'Reeds bekend'
                                                      ,p_helix_waarde       => p_verz_trow.verz_id
                                                      ,p_toevoegen          => TRUE);
    END IF;

    v_impe_trow.zisnr               := p_pers_trow.zisnr;
    v_impe_trow.bsn                 := p_pers_trow.bsn; -- NtB, 29-04-2009, 283-BSN
    v_impe_trow.bsn_geverifieerd    := p_pers_trow.bsn_geverifieerd; -- NtB, 29-04-2009, 283-BSN
    v_impe_trow.aanspreken          := p_pers_trow.aanspreken;
    v_impe_trow.geboortedatum       := to_char(p_pers_trow.geboortedatum
                                              ,'dd-mm-yyyy');
    v_impe_trow.part_geboortedatum  := p_pers_trow.part_geboortedatum; -- NtB: geen format??????
    v_impe_trow.voorletters         := p_pers_trow.voorletters;
    v_impe_trow.voorvoegsel         := p_pers_trow.voorvoegsel;
    v_impe_trow.achternaam_partner  := p_pers_trow.achternaam_partner;
    v_impe_trow.voorvoegsel_partner := p_pers_trow.voorvoegsel_partner;
    v_impe_trow.adres               := p_pers_trow.adres;
    v_impe_trow.postcode            := p_pers_trow.postcode;
    v_impe_trow.woonplaats          := p_pers_trow.woonplaats;
    v_impe_trow.impw_id_provincie   := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                           ,p_helix_table_domain => 'PROVINCIE'
                                                           ,p_externe_id         => p_pers_trow.provincie
                                                           ,p_hint               => NULL
                                                           ,p_helix_waarde       => p_pers_trow.provincie
                                                           ,p_toevoegen          => TRUE);
    v_impe_trow.land                := p_pers_trow.land;
    v_impe_trow.telefoon            := p_pers_trow.telefoon;
    v_impe_trow.telefax             := p_pers_trow.telefax;
    v_impe_trow.email               := p_pers_trow.email;
    v_impe_trow.impw_id_verzwijze   := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                           ,p_helix_table_domain => kgc_import.g_cimpw_dom_verzekeringswijze
                                                           ,p_externe_id         => p_pers_trow.verzekeringswijze
                                                           ,p_hint               => NULL
                                                           ,p_toevoegen          => TRUE);
    v_impe_trow.verzekeringsnr      := p_pers_trow.verzekeringsnr;
    v_impe_trow.overlijdensdatum    := to_char(p_pers_trow.overlijdensdatum
                                              ,'dd-mm-yyyy');
    v_impe_trow.hoeveelling         := p_pers_trow.hoeveelling;

    IF v_impe_trow.the_rowid IS NULL
    THEN
      cg$kgc_im_personen.ins(cg$rec => v_impe_trow
                            ,cg$ind => cg$kgc_im_personen.cg$ind_true);
    ELSE
      v_update_lijst                 := cg$kgc_im_personen.cg$ind_true;
      v_update_lijst.created_by      := FALSE;
      v_update_lijst.creation_date   := FALSE;
      v_update_lijst.last_updated_by := FALSE; -- NtB: ????
      cg$kgc_im_personen.upd(cg$rec => v_impe_trow
                            ,cg$ind => v_update_lijst);
    END IF;

    -- NtB: opzettelijke testfout!!!!!?????????????? tzt. weghalen!!!!!!!!!
    -- g_this_procedure := to_number(g_this_procedure);

    RETURN TRUE; -- NtB:

  EXCEPTION
    -- NtB: niew
    WHEN OTHERS THEN
      --cg$errors.push(msg => 'Fout tijdens verwerken van persoonsgegevens. ');
      --         qms$errors.unhandled_exception(p_procname => g_this_package || '.' || g_this_procedure
      --                                       ,p_errmsg   => 'Fout tijdens verwerken van persoonsgegevens: ' || SQLERRM);
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_IM_PERSONEN. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE
                              --,p_table_name => 'KGC_IM_PERSONEN'
                              --,p_table_rowid => v_impe_trow.the_rowid
                              );
      RETURN FALSE;

  END merge_im_personen;

  FUNCTION merge_im_zwangerschappen(p_imzw_trow IN OUT cg$kgc_im_zwangerschappen.cg$row_type)
    RETURN BOOLEAN IS

    v_imzw_trow    cg$kgc_im_zwangerschappen.cg$row_type; -- NtB: nieuw
    v_update_lijst cg$kgc_im_zwangerschappen.cg$ind_type;

  BEGIN

    -- Init
    g_this_procedure    := 'merge_im_zwangerschappen'; -- NtB: nieuw
    v_imzw_trow.imfi_id := g_imfi_id;
    v_imzw_trow.imzw_id := g_hl7_msg(g_obr) (13) (1) (1); -- Graviditeit

    BEGIN
      -- NtB: hersteld
      cg$kgc_im_zwangerschappen.slct(v_imzw_trow); -- eventueel bestaande gegevens ophalen
    EXCEPTION
      WHEN OTHERS THEN
        -- no data found, maar verpakt in 20999...
        NULL;
        -- NtB: errorstack leegmaken, deze exception willen we niet zien
        -- cg$errors.clear;
        g_bool_dummy := cg$errors.pop_head(g_char_dummy);
    END;

    v_imzw_trow.impe_id     := g_zisnr; -- verwijzing persoon
    v_imzw_trow.nummer      := v_imzw_trow.imzw_id;
    v_imzw_trow.hoeveelling := g_hl7_msg(g_obr) (13) (1) (2);
    v_imzw_trow.para        := g_hl7_msg(g_obr) (13) (1) (14);
    v_imzw_trow.abortussen  := g_hl7_msg(g_obr) (13) (1) (15); -- O niet nul
    v_imzw_trow.apla        := g_hl7_msg(g_obr) (13) (1) (16);
    v_imzw_trow.eug         := g_hl7_msg(g_obr) (13) (1) (17);
    v_imzw_trow.mola        := g_hl7_msg(g_obr) (13) (1) (18);
    v_imzw_trow.progenituur := g_hl7_msg(g_obr) (13) (1) (19);

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 7
    THEN
      v_imzw_trow.datum_lm := g_hl7_msg(g_obr) (13) (1) (7); -- Datum LM
      formatteerdatum(v_imzw_trow.datum_lm
                     ,'yyyymmdd');
    END IF;

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 8
    THEN
      v_imzw_trow.impw_id_zekerheid_datum_lm := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                                    ,p_helix_table_domain => kgc_import.g_cimpw_dom_zekerheid
                                                                    ,p_externe_id         => g_hl7_msg(g_obr) (13) (1) (8)
                                                                    ,p_hint               => NULL
                                                                    ,p_toevoegen          => TRUE);
    END IF;

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 9
    THEN
      v_imzw_trow.datum_aterm := g_hl7_msg(g_obr) (13) (1) (9); -- AtermeDatum
      formatteerdatum(v_imzw_trow.datum_aterm
                     ,'yyyymmdd');
    END IF;

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 10
    THEN
      v_imzw_trow.impw_id_berekeningswijze := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                                  ,p_helix_table_domain => kgc_import.g_cimpw_dom_berekeningswijze
                                                                  ,p_externe_id         => g_hl7_msg(g_obr) (13) (1) (10)
                                                                  ,p_hint               => NULL
                                                                  ,p_toevoegen          => TRUE);
    END IF;

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 5
    THEN
      v_imzw_trow.info_geslacht := g_hl7_msg(g_obr) (13) (1) (5); -- Info geslacht
    END IF;

    v_imzw_trow.consanguiniteit := nvl(v_imzw_trow.consanguiniteit
                                      ,'N');
    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 6
    THEN
      IF g_hl7_msg(g_obr) (13) (1) (6) = 'J'
      THEN
        -- consanguiniteit: mogelijke waarden: J/N/O
        v_imzw_trow.consanguiniteit := 'J';
      END IF;
    END IF;

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 12
    THEN
      IF nvl(length(g_hl7_msg(g_obr) (13) (1) (12))
            ,0) > 0
      THEN
        v_imzw_trow.opmerkingen := 'Biologische vader: ' ||
                                   g_hl7_msg(g_obr) (13) (1) (12);
        IF g_hl7_msg(g_obr) (13) (1).COUNT >= 13
        THEN
          IF nvl(length(g_hl7_msg(g_obr) (13) (1) (13))
                ,0) > 0
          THEN
            v_imzw_trow.opmerkingen := v_imzw_trow.opmerkingen || ' / ' ||
                                       substr(g_hl7_msg(g_obr) (13) (1) (13)
                                             ,7
                                             ,2) || '-' ||
                                       substr(g_hl7_msg(g_obr) (13) (1) (13)
                                             ,5
                                             ,2) || '-' ||
                                       substr(g_hl7_msg(g_obr) (13) (1) (13)
                                             ,1
                                             ,4);
          END IF;
        END IF;
      END IF;
    END IF;

    IF v_imzw_trow.the_rowid IS NULL
    THEN
      cg$kgc_im_zwangerschappen.ins(cg$rec => v_imzw_trow
                                   ,cg$ind => cg$kgc_im_zwangerschappen.cg$ind_true);
    ELSE
      v_update_lijst               := cg$kgc_im_zwangerschappen.cg$ind_true;
      v_update_lijst.created_by    := FALSE;
      v_update_lijst.creation_date := FALSE;
      cg$kgc_im_zwangerschappen.upd(cg$rec => v_imzw_trow
                                   ,cg$ind => v_update_lijst);
    END IF;

    p_imzw_trow := v_imzw_trow; -- NtB: nieuw

    RETURN TRUE; -- NtB:

  EXCEPTION
    -- NtB: nieuw
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_IM_ZWANGERSCHAPPEN. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);
      RETURN FALSE;

  END merge_im_zwangerschappen;

  FUNCTION merge_im_monsters(p_immo_trow IN OUT cg$kgc_im_monsters.cg$row_type)
    RETURN BOOLEAN IS

    CURSOR c_immo_exists(p_imlo_id NUMBER, p_zisnr VARCHAR2, p_mosos_materiaal VARCHAR2) IS
      SELECT kgc_mons_id
      FROM   kgc_im_monsters  immo
            ,kgc_import_files imfi
            ,kgc_im_personen  impe
      WHERE  imfi.imlo_id = p_imlo_id
      AND    imfi.imfi_id = immo.imfi_id
      AND    immo.kgc_mons_id IS NOT NULL
      AND    immo.immo_id LIKE p_mosos_materiaal
      AND    immo.impe_id = impe.impe_id
      AND    imfi.imfi_id = impe.imfi_id
      AND    impe.zisnr = p_zisnr;

    v_tab_waarden t_tab_waarden;

    v_immo_trow    cg$kgc_im_monsters.cg$row_type;
    v_update_lijst cg$kgc_im_monsters.cg$ind_type;

    v_externe_id      VARCHAR2(100);
    v_hint            VARCHAR2(2000);
    v_mat_code_oms    VARCHAR2(300); -- nav opm rde 19-11
    v_mosos_materiaal VARCHAR2(100);
    v_pos             NUMBER;

  BEGIN

    -- Init
    g_this_procedure    := 'merge_im_monsters'; -- NtB: nieuw
    v_immo_trow.imfi_id := g_imfi_id;
    v_immo_trow.immo_id := g_hl7_msg(g_obr) (2) (1) (1); -- unieke id monster (binnen bericht): het aangeleverde ordernummer

    BEGIN
      -- NtB: hersteld
      cg$kgc_im_monsters.slct(v_immo_trow); -- eventueel bestaande gegevens ophalen
    EXCEPTION
      WHEN OTHERS THEN
        -- no data found, maar verpakt in 20999...
        NULL;
        -- NtB: errorstack leegmaken, deze exception willen we niet zien
        -- cg$errors.clear;
        g_bool_dummy := cg$errors.pop_head(g_char_dummy);
    END;

    --?  v_immo_trow.monsternummer
    v_immo_trow.impw_id_kafd := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                    ,p_helix_table_domain => kgc_import.g_cimpw_tab_kafd
                                                    ,p_externe_id         => 'Onderzoek'
                                                    ,p_hint               => 'Cytogenetica'
                                                    ,p_toevoegen          => TRUE);
    v_immo_trow.impw_id_ongr := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                    ,p_helix_table_domain => kgc_import.g_cimpw_tab_ongr
                                                    ,p_externe_id         => 'Onderzoek'
                                                    ,p_hint               => 'Prenataal'
                                                    ,p_toevoegen          => TRUE);
    v_immo_trow.imfo_id      := g_imfo_id; -- verwijzing foetus
    v_immo_trow.impe_id      := g_zisnr; -- verwijzing persoon
    -- splits OBR-15 in 3 obv & (= MSH-2, pos 4)
    v_tab_waarden := splits(p_pattern => substr(g_hl7_msg(g_msh) (2) (1) (1)
                                               ,4
                                               ,1)
                           ,p_source  => g_hl7_msg(g_obr) (15) (1) (1));

    IF v_tab_waarden.COUNT > 2
    THEN
      -- code+waarde+codering is ingevuld
      v_mat_code_oms           := v_tab_waarden(1) || '-' ||
                                  v_tab_waarden(2); -- nav opm rde 19-11, itt specs!
      v_immo_trow.impw_id_mate := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_mate
                                                       --                ,p_externe_id         => v_tab_waarden(1)  -- nav opm rde 19-11 itt specs
                                                      ,p_externe_id => v_mat_code_oms -- nav opm rde 19-11 itt specs
                                                      ,p_hint       => v_tab_waarden(2)
                                                      ,p_toevoegen  => TRUE);
    END IF;

    --?  v_immo_trow.declareren
    --?  v_immo_trow.prematuur
    --?  v_immo_trow.bewaren
    --?  v_immo_trow.nader_gebruiken
    --?  v_immo_trow.impw_id_mede
    --?  v_immo_trow.impw_id_leef
    --?  v_immo_trow.leeftijd
    --?  v_immo_trow.impw_id_bepl_kort
    --?  v_immo_trow.impw_id_bepl_lang
    --?  v_immo_trow.periode_afname

    -- NtB: nav. aanpassingen RDE, 26-02-2008
    -- rde 27-02-2008 ckcl_nummer vullen met materiaalnr = deel voor '-99'.
    v_pos := instr(v_immo_trow.immo_id
                  ,'-');
    IF v_pos > 0
    THEN
      v_immo_trow.ckcl_nummer := substr(v_immo_trow.immo_id
                                       ,1
                                       ,v_pos - 1);
    END IF;

    --?  v_immo_trow.kweeknummer
    --?  v_immo_trow.aantal_buizen
    --?  v_immo_trow.datum_verwacht
    --?  v_immo_trow.hoeveelheid_buffer
    --?  v_immo_trow.medicatie
    --?  v_immo_trow.voeding

    v_immo_trow.datum_aanmelding := to_char(SYSDATE
                                           ,g_datumformaatdoel);
    v_externe_id                 := g_hl7_msg(g_obr) (10) (1) (1); -- OBR-10 (afnemer -> inzender) mag leeg zijn!
    v_hint                       := NULL;

    IF g_hl7_msg(g_obr) (10) (1).COUNT >= 2
    THEN
      -- bepaal hint
      v_hint := g_hl7_msg(g_obr) (10) (1) (2);
    END IF;

    IF nvl(length(v_externe_id)
          ,0) = 0
    THEN
      -- code kan leeg zijn; neem dan de hint!
      v_externe_id := v_hint;
    END IF;

    IF nvl(length(v_externe_id)
          ,0) > 0
    THEN
      v_immo_trow.impw_id_rela := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                      ,p_externe_id         => v_externe_id
                                                      ,p_hint               => v_hint
                                                      ,p_toevoegen          => TRUE);
    END IF;

    v_externe_id := g_hl7_msg(g_orc) (17) (1) (1); -- ORC-17 (herkomst) mag leeg zijn!
    v_hint       := NULL;

    IF g_hl7_msg(g_orc) (17) (1).COUNT >= 2
    THEN
      -- bepaal hint
      v_hint := g_hl7_msg(g_orc) (17) (1) (2);
    END IF;

    IF nvl(length(v_externe_id)
          ,0) = 0
    THEN
      -- code kan leeg zijn
      v_externe_id := v_hint;
    END IF;

    IF nvl(length(v_externe_id)
          ,0) > 0
    THEN
      v_immo_trow.impw_id_herk := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_herk
                                                      ,p_externe_id         => v_externe_id
                                                      ,p_hint               => v_hint
                                                      ,p_toevoegen          => TRUE);
    END IF;
    --?  v_immo_trow.impw_id_buff

    IF g_hl7_msg(g_obr) (13) (1).COUNT >= 11
    THEN
      v_immo_trow.impw_id_cond := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_cond
                                                      ,p_externe_id         => g_hl7_msg(g_obr) (13) (1) (11)
                                                      ,p_hint               => g_hl7_msg(g_obr) (13) (1) (11)
                                                      ,p_toevoegen          => TRUE);
    END IF;

    v_immo_trow.datum_afname := g_hl7_msg(g_obr) (7) (1) (1); --
    formatteerdatum(v_immo_trow.datum_afname
                   ,'yyyymmddhh24miss');
    v_immo_trow.tijd_afname := g_hl7_msg(g_obr) (7) (1) (1); -- RDE 20-02-2008
    formatteerdatumtijd(v_immo_trow.tijd_afname -- RDE 20-02-2008
                       ,'yyyymmddhh24miss'); -- RDE 20-02-2008
    v_immo_trow.hoeveelheid_monster := g_hl7_msg(g_obr) (9) (1) (1); -- RDE Rework Mantis 3184: Interface voor Dragon
    v_immo_trow.medische_indicatie  := g_hl7_msg(g_obr) (31) (1) (1);

    IF g_hl7_msg(g_obr) (31) (1).COUNT >= 2
    THEN
      v_immo_trow.medische_indicatie := v_immo_trow.medische_indicatie ||
                                        ' - ' || g_hl7_msg(g_obr) (31) (1) (2);
    END IF;

    -- v_immo_trow.commentaar
    --v_debug := '10e3';

    IF nvl(length(g_nte)
          ,0) > 0
    THEN
      IF g_hl7_msg(g_nte).COUNT > 2
      THEN
        IF nvl(length(g_hl7_msg(g_nte) (3) (1) (1))
              ,0) > 0
        THEN
          -- er is een opmerking
          IF nvl(length(v_immo_trow.commentaar)
                ,0) > 0
          THEN
            v_immo_trow.commentaar := substr(v_immo_trow.commentaar || '; '
                                            ,1
                                            ,2000);
          END IF;
          v_immo_trow.commentaar := substr(v_immo_trow.commentaar ||
                                           'Opmerkingen: ' ||
                                           g_hl7_msg(g_nte) (3) (1) (1)
                                          ,1
                                          ,2000);
        END IF;
      END IF;
    END IF;

    -- mogelijk is dit een tweede aanvraag bij een materiaal. In dat geval: koppelen aan het reeds bestaande helix-monster!
    IF v_immo_trow.kgc_mons_id IS NULL
    THEN
      -- zoek binnen persoon naar vergelijkbare immo
      v_pos := instr(v_immo_trow.immo_id
                    ,'-');
      IF v_pos > 0
      THEN
        v_mosos_materiaal := substr(v_immo_trow.immo_id
                                   ,1
                                   ,v_pos - 1);
        OPEN c_immo_exists(g_imlo_id
                          ,g_zisnr -- v_impe_trow.zisnr -- NtB: gewijzigd
                          ,v_mosos_materiaal || '%');

        FETCH c_immo_exists
          INTO v_immo_trow.kgc_mons_id;
        CLOSE c_immo_exists;

      END IF;
    END IF;

    IF v_immo_trow.the_rowid IS NULL
    THEN
      cg$kgc_im_monsters.ins(cg$rec => v_immo_trow
                            ,cg$ind => cg$kgc_im_monsters.cg$ind_true);
    ELSE

      v_update_lijst               := cg$kgc_im_monsters.cg$ind_true;
      v_update_lijst.created_by    := FALSE;
      v_update_lijst.creation_date := FALSE;
      cg$kgc_im_monsters.upd(cg$rec => v_immo_trow
                            ,cg$ind => v_update_lijst);
    END IF;

    p_immo_trow := v_immo_trow; -- NtB: nieuw

    RETURN TRUE; -- NtB:

  EXCEPTION
    -- NtB: nieuw
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_IM_MONSTERS. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);
      RETURN FALSE;

  END merge_im_monsters;

  FUNCTION merge_im_onderzoeken(p_imon_trow IN OUT cg$kgc_im_onderzoeken.cg$row_type)
    RETURN BOOLEAN IS

    CURSOR c_imon(b_imon_id kgc_im_onderzoeken.imon_id%TYPE, b_imfi_id kgc_im_onderzoeken.imfi_id%TYPE) IS
      SELECT imon.ROWID
      FROM   kgc_im_onderzoeken imon
      WHERE  imon.imfi_id = b_imfi_id
      AND    imon.imon_id = b_imon_id;

    CURSOR c_imon_exists(p_imlo_id NUMBER, p_zisnr VARCHAR2, p_mosos_materiaal VARCHAR2, p_imon_id_this VARCHAR2) IS
      SELECT kgc_onde_id
      FROM   kgc_im_onderzoeken imon
            ,kgc_import_files   imfi
            ,kgc_im_personen    impe
      WHERE  imfi.imlo_id = p_imlo_id
      AND    imfi.imfi_id = imon.imfi_id
            --      and    imon.kgc_onde_id is not null   RDE 7-2-2008
      AND    imon.imon_id LIKE p_mosos_materiaal
      AND    imon.imon_id <> p_imon_id_this -- RDE 19-12-2007
      AND    imon.impe_id = impe.impe_id
      AND    imfi.imfi_id = impe.imfi_id
      AND    impe.zisnr = p_zisnr
      ORDER  BY kgc_onde_id NULLS LAST; -- NtB: nav. aanpassingen RDE, 26-02-2008
    -- ORDER BY decode(kgc_onde_id
    --                ,NULL
    --                ,9
    --                ,1); -- gevulde kgc_onde_id eerst! RDE 7-2-2008

    v_imon_trow    cg$kgc_im_onderzoeken.cg$row_type;
    v_update_lijst cg$kgc_im_onderzoeken.cg$ind_type;

    v_externe_id      VARCHAR2(100);
    v_hint            VARCHAR2(2000);
    v_imon_found      BOOLEAN := FALSE;
    v_mosos_materiaal VARCHAR2(100);
    v_pos             NUMBER;

  BEGIN
    -- Init
    g_this_procedure    := 'merge_im_onderzoeken'; -- NtB: nieuw
    v_imon_trow.imfi_id := g_imfi_id;
    -- unieke id onderzoek (binnen bericht): het aangeleverde ordernummer + onderzoek (mogelijk: KAR / AFP / MLP (?) / FIS (?).):
    -- v_imon_trow.imon_id      := g_hl7_msg(g_obr) (2) (1) (1) || '-' || g_hl7_msg(g_obr) (4) (1) (1);
    v_imon_trow.imon_id := TRIM(g_hl7_msg(g_obr) (2) (1)
                                (1) || '-' || g_hl7_msg(g_obr) (4) (1) (1)); -- NtB: trim???

    BEGIN
      -- NtB: hersteld
      cg$kgc_im_onderzoeken.slct(v_imon_trow); -- eventueel bestaande gegevens ophalen
    EXCEPTION
      WHEN OTHERS THEN
        -- no data found, maar verpakt in 20999...
        NULL;
        -- NtB: errorstack leegmaken, deze exception willen we niet zien
        -- cg$errors.clear;
        g_bool_dummy := cg$errors.pop_head(g_char_dummy);
    END;

    v_imon_trow.impe_id      := g_zisnr; -- verwijzing persoon
    v_imon_trow.impw_id_kafd := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                    ,p_helix_table_domain => kgc_import.g_cimpw_tab_kafd
                                                    ,p_externe_id         => 'Onderzoek'
                                                    ,p_hint               => 'Cytogenetica'
                                                    ,p_toevoegen          => TRUE);
    v_imon_trow.impw_id_ongr := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                    ,p_helix_table_domain => kgc_import.g_cimpw_tab_ongr
                                                    ,p_externe_id         => 'Onderzoek'
                                                    ,p_hint               => 'Prenataal'
                                                    ,p_toevoegen          => TRUE);
    v_imon_trow.imfo_id      := g_imfo_id; -- verwijzing foetus

    -- v_imon_trow.impw_id_onderzoekstype: default (diagnostiek)
    --?  v_imon_trow.onderzoeknr

    -- aanvrager
    v_externe_id := g_hl7_msg(g_obr) (16) (1) (1); -- OBR-16 (aanvrager) mag leeg zijn!
    v_hint       := NULL;

    IF g_hl7_msg(g_obr) (16) (1).COUNT >= 2
    THEN
      -- bepaal hint uit voorvoegsels + achternaam + evt voornaam en voorletters

      IF g_hl7_msg(g_obr) (16) (1).COUNT >= 6 -- voorvoegsels aanwezig
      THEN
        v_hint := g_hl7_msg(g_obr) (16) (1) (6) || ' ';
      END IF;

      v_hint := v_hint || g_hl7_msg(g_obr) (16) (1) (2);
      IF g_hl7_msg(g_obr) (16) (1).COUNT >= 3
      THEN
        v_hint := v_hint || ' ' || g_hl7_msg(g_obr) (16) (1) (3);
        IF g_hl7_msg(g_obr) (16) (1).COUNT >= 4
        THEN
          -- bepaal hint uit achternaam + evt voornaam en voorletters
          v_hint := v_hint || ' ' || g_hl7_msg(g_obr) (16) (1) (4);
        END IF;
      END IF;
    END IF;

    IF nvl(length(v_externe_id)
          ,0) = 0
    THEN
      -- code kan leeg zijn
      v_externe_id := v_hint;
    END IF;

    IF nvl(length(v_externe_id)
          ,0) > 0
    THEN
      v_imon_trow.impw_id_rela := kgc_import.getimpwid(p_imlo_id            => g_imlo_id
                                                      ,p_helix_table_domain => kgc_import.g_cimpw_tab_rela
                                                      ,p_externe_id         => v_externe_id
                                                      ,p_hint               => v_hint
                                                      ,p_toevoegen          => TRUE);
    END IF;

    v_imon_trow.datum_binnen := to_char(SYSDATE
                                       ,g_datumformaatdoel);
    --  ONRE_ID niet aanwezig!!!
    --  oorspronkelijk aanvrager niet aanwezig!!!

    v_imon_trow.omschrijving := 'Mosos ' || g_hl7_msg(g_obr) (4) (1) (1);
    v_imon_trow.referentie   := g_hl7_msg(g_obr) (2) (1) (1); -- het MOSOS monsternummer tbv terugkoppeling
    --?  v_imon_trow.nota_adres
    --?  v_imon_trow.kariotypering

    -- mogelijk is dit een tweede aanvraag bij een materiaal. In dat geval: koppelen aan het reeds bestaande helix-monster!
    -- NtB: deze controle zit hier, die van zis in send_mail_zis_controle; niet in 1 lijn;
    --      deze dan ook in send_mail_bestaand_materiaal!???
    --      Nu controle hier zit, dan ook hier de global zetten
    IF v_imon_trow.kgc_onde_id IS NULL
    THEN
      -- zoek binnen persoon naar vergelijkbare imon
      v_pos := instr(v_imon_trow.imon_id
                    ,'-');
      IF v_pos > 0
      THEN
        v_mosos_materiaal := substr(v_imon_trow.imon_id
                                   ,1
                                   ,v_pos - 1);
        OPEN c_imon_exists(g_imlo_id
                          ,g_zisnr -- v_impe_trow.zisnr -- NtB: gewijzigd
                          ,v_mosos_materiaal || '%'
                          ,v_imon_trow.imon_id); -- RDE 19-12-2007
        FETCH c_imon_exists
          INTO v_imon_trow.kgc_onde_id;
        IF c_imon_exists%FOUND
        THEN
          g_bestaand_materiaal := TRUE; -- NtB: nieuw

          -- NtB: moet een niveau hoger >>>
          -- RDE 19-12-2007
          -- send_mail_bestaand_materiaal(g_hl7_msg
          --                             ,v_imon_trow.kgc_onde_id);
          -- v_afgehandeld := 'J'; -- RDE 19-12-2007
          -- v_opmerkingen := 'EMail verzonden tbv aanvraag extra protocol bij bestaand materiaal.'; -- RDE 19-12-2007
          -- <<<
        END IF; -- RDE 19-12-2007
        CLOSE c_imon_exists;

        /* -- NtB: 21-02-2008, verplaatst naar na merge
        g_bestaand_materiaal := TRUE; -- NtB: nieuw

        -- NtB: return status 'V' en afgehandeld = 'J'
        -- x_imfi_afgehandeld := 'J' >>>
        UPDATE kgc_import_files imfi
           SET afgehandeld = 'J'
         WHERE imfi.imfi_id = g_imfi_id;

        --RETURN 'V'; -- NtB: 21-02-2008 14:45, tijdelijk ongedaan gemaakt
        -- <<<
        */

      END IF;

    END IF;

    IF NOT v_imon_found
    THEN
      cg$kgc_im_onderzoeken.ins(cg$rec => v_imon_trow
                               ,cg$ind => cg$kgc_im_onderzoeken.cg$ind_true);
    ELSE
      -- uitsluiten bij de update
      v_update_lijst               := cg$kgc_im_onderzoeken.cg$ind_true;
      v_update_lijst.created_by    := FALSE;
      v_update_lijst.creation_date := FALSE;
      v_update_lijst.imfi_id       := FALSE; -- NtB: ??????
      v_update_lijst.imon_id       := FALSE;
      v_update_lijst.impe_id       := FALSE;
      v_update_lijst.imfo_id       := FALSE;
      cg$kgc_im_onderzoeken.upd(cg$rec => v_imon_trow
                               ,cg$ind => v_update_lijst);
    END IF;

    p_imon_trow := v_imon_trow; -- NtB: nieuw

    RETURN TRUE; -- NtB:

  EXCEPTION
    -- NtB: nieuw
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_IM_ONDERZOEKEN. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);
      RETURN FALSE;

  END merge_im_onderzoeken;

  FUNCTION koppel_im_onderzoek_monster(p_imon_trow IN OUT cg$kgc_im_onderzoeken.cg$row_type) --
   RETURN BOOLEAN IS
    -- NtB, 06-02-2009:
    -- Indien er nu een onderzoek en exact 1 monster is opgevoerd is, dan proberen we
    -- die in eerste instantie aan elkaar te koppelen door monster_id op te nemen
    -- in het onderzoekrecord (immo_id). We kijken daarbij eerst af op referentie
    -- (= monster_id), indien gevuld.
    -- Later tijdens vertaal_naar_helix bepalen we of die
    -- koppeling daadwerkelijk tot stand moet komen binnen helix.

    CURSOR c_immo --
    (p_imfi_id kgc_import_files.imfi_id%TYPE --
    , p_impe_id kgc_im_personen.impe_id%TYPE --
    , p_referentie kgc_im_onderzoeken.referentie%TYPE --
    ) IS
      SELECT immo.immo_id
      FROM   kgc_im_monsters immo
      WHERE  immo.imfi_id = p_imfi_id
      AND    immo.impe_id = p_impe_id
      AND    immo.immo_id = nvl(p_referentie
                               ,immo.immo_id);

  BEGIN
    -- Init
    g_this_procedure    := 'koppel_im_onderzoek_monster';
    p_imon_trow.immo_id := NULL; -- voor de zekerheid

    FOR r_immo IN c_immo(p_imon_trow.imfi_id
                        ,p_imon_trow.impe_id
                        ,p_imon_trow.referentie)
    LOOP
      -- Als we iets gevonden hebben dan vullen we immo_id
      p_imon_trow.immo_id := r_immo.immo_id;

      -- Worden er meer dan 1 gevonden, dan kunnen we geen keus maken
      -- en maken we immo_id weer leeg. Er kan dan evt. handmatig in IMPO01
      -- een monster gekozen worden.
      IF c_immo%ROWCOUNT > 1
      THEN
        p_imon_trow.immo_id := NULL;
        -- Doorgaan heeft geen zin
        EXIT;
      END IF;

      -- Relatie (koppeling) vastleggen tussen onderzoek en monster
      UPDATE kgc_im_onderzoeken imon
      SET    imon.immo_id = p_imon_trow.immo_id
      WHERE  imon.imfi_id = p_imon_trow.imfi_id
      AND    imon.imon_id = p_imon_trow.imon_id;

    END LOOP;

    RETURN TRUE;

  EXCEPTION
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens in KGC_IM_ONDERZOEKEN. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);
      RETURN FALSE;

  END koppel_im_onderzoek_monster;

  /*
  PROCEDURE koppel_onderzoek_monster(p_imfi_id kgc_import_files.imfi_id%TYPE) IS
     /*********************************************************************
      * Via imfi_id wordt pers_id opgehaald. Met pers_id                  *
      * worden mons_id en onde_id opgehaald om zo nodig de koppeling in   *
      * kgc_onderzoek_monsters aan te brengen                             *
      * NB Alleen koppelen als referentie van im_onderzoeken niet Null is *
      * RDE 20-03-2008: hier moet nog een nieuwe faciliteit voor komen (die ook voldoet voor Astraia)
                        bovenstaande opmerking (mbt referentie) lijkt me niet relevant
                        een tijdelijke workaround: het onderzoek en monster moeten in hetzelfde imfi zitten
                        (maar dit is niet voldoende voor Astraia)
     **********************************************************************
     CURSOR c_imon(b_imfi_id kgc_import_files.imfi_id%TYPE) IS
        SELECT imon.referentie
              ,imon.omschrijving
              ,imon.kgc_onde_id -- NtB: nav. rde 20-03-2008 nieuw.
              ,imon.immo_id -- NtB, 05-02-2009: nieuw, opt. relatie met kgc_im_monsters
          FROM kgc_im_onderzoeken imon
         WHERE imfi_id = b_imfi_id;

     r_imon c_imon%ROWTYPE;

     -- NtB: nav. rde 20-03-2008 nieuw:
     CURSOR c_immo --
     (b_imfi_id kgc_import_files.imfi_id%TYPE -- NtB:
     , b_immo_id kgc_im_monsters.immo_id%TYPE -- NtB:
     ) IS
        SELECT immo.kgc_mons_id
          FROM kgc_im_monsters immo
         WHERE imfi_id = b_imfi_id
           AND immo.immo_id = b_immo_id; -- NtB, 05-02-2009: zou autom. of handmatig
     -- bepaald kunnen zijn; dit bepaald of er gekoppeld moet worden en waaraan

     r_immo c_immo%ROWTYPE;

     CURSOR c_onmo --
     (b_onde_id kgc_onderzoek_monsters.onde_id%TYPE -- NtB:
     , b_mons_id kgc_onderzoek_monsters.mons_id%TYPE -- NtB:
     ) IS
        SELECT 'x'
          FROM kgc_onderzoek_monsters onmo
         WHERE onmo.onde_id = b_onde_id
           AND onmo.mons_id = b_mons_id;

     r_onmo c_onmo%ROWTYPE;

  BEGIN
     -- moet er wel gekoppeld worden ( adhv imon.referentie )
     OPEN c_imon(p_imfi_id);
     FETCH c_imon
        INTO r_imon;
     IF c_imon%NOTFOUND -- vreemd, zou niet mogen
     THEN
        CLOSE c_imon;
        RETURN;
     END IF;
     CLOSE c_imon;

     OPEN c_immo(p_imfi_id
                ,r_imon.immo_id -- NtB, 05-02-2009: extra voorwaarde
                 );
     FETCH c_immo
        INTO r_immo;
     IF c_immo%NOTFOUND -- vreemd, zou niet mogen
     THEN
        CLOSE c_immo;
        RETURN;
     END IF;
     CLOSE c_immo;

     IF r_imon.kgc_onde_id IS NULL
        OR r_immo.kgc_mons_id IS NULL -- goede reden om niet te koppelen!
     THEN
        RETURN;
     END IF;

     -- NtB: nav. rde 20-03-2008 tijdelijke verbetering:
     -- bestaat er al een koppeling ?
     OPEN c_onmo(r_imon.kgc_onde_id
                ,r_immo.kgc_mons_id);
     FETCH c_onmo
        INTO r_onmo;
     IF c_onmo%FOUND
     THEN
        CLOSE c_onmo;
        RETURN;
     END IF;
     CLOSE c_onmo;

     -- en insert into kgc_onderzoek_monsters
     INSERT INTO kgc_onderzoek_monsters
        (onde_id
        ,mons_id)
     VALUES
        (r_imon.kgc_onde_id
        ,r_immo.kgc_mons_id);

  EXCEPTION
     WHEN OTHERS THEN
        NULL;
        -- NtB: moet dit anders????

  END;
  */

  FUNCTION verwerk_orm_o01_bericht -- order tbv CYTO-PREC
  (p_hlbe_id     NUMBER
  ,p_hl7_msg     kgc_zis.hl7_msg
  ,x_entiteit    IN OUT kgc_hl7_berichten.entiteit%TYPE
  ,x_id          IN OUT kgc_hl7_berichten.id%TYPE
  ,x_opmerkingen IN OUT kgc_hl7_berichten.opmerkingen%TYPE) RETURN VARCHAR2 IS

    CURSOR c_imlo IS
      SELECT *
      FROM   kgc_import_locaties imlo
      WHERE  imlo.directorynaam = 'MOSOS';

    r_imlo c_imlo%ROWTYPE;

    CURSOR c_imfi(p_imlo_id NUMBER, p_filenaam VARCHAR2) IS
      SELECT imfi.imfi_id
            ,imfi.import_datum
            ,imfi.inhoud
      FROM   kgc_import_files imfi
      WHERE  imfi.imlo_id = p_imlo_id
      AND    imfi.filenaam = p_filenaam;

    r_imfi c_imfi%ROWTYPE;

    v_afgehandeld        VARCHAR2(1) := 'N'; -- RDE 19-12-2007
    v_helix_waarde       kgc_import_param_waarden.helix_waarde%TYPE;
    v_imfi_identificatie VARCHAR2(100);
    v_imfo_trow          cg$kgc_im_foetussen.cg$row_type;
    v_immo_trow          cg$kgc_im_monsters.cg$row_type;
    v_imok_trow          cg$kgc_im_onde_kopie.cg$row_type;
    v_imon_trow          cg$kgc_im_onderzoeken.cg$row_type;
    v_impw_id            NUMBER;
    v_imzw_trow          cg$kgc_im_zwangerschappen.cg$row_type;
    v_instelling_naam    VARCHAR2(2000);
    v_msg                VARCHAR2(32767);
    v_msg_rela           VARCHAR2(32767);
    v_ok                 BOOLEAN := TRUE;
    v_pers_trow          cg$kgc_personen.cg$row_type;
    v_pid                VARCHAR2(10);
    v_rela_trow          cg$kgc_relaties.cg$row_type;
    v_verz_trow          cg$kgc_verzekeraars.cg$row_type;

  BEGIN

    -- Logging
    g_this_procedure := 'verwerk_orm_o01_bericht';
    kgc_zis.log(g_this_package || '.' || g_this_procedure || ': Bericht ' ||
                to_char(p_hlbe_id) || ' wordt verwerkt!');

    -- NtB: evt. nog globals en records (op null) zetten voor de zekerheid
    -- Initialisatie
    g_imfi_id := NULL;
    g_imfo_id := NULL;
    g_zisnr   := NULL;
    g_hl7_msg := p_hl7_msg; -- NtB: nieuw tbv. merge-procedures
    g_msh     := kgc_zis.getsegmentcode('MSH'
                                       ,p_hl7_msg);
    v_pid     := kgc_zis.getsegmentcode('PID'
                                       ,p_hl7_msg);
    g_orc     := kgc_zis.getsegmentcode('ORC'
                                       ,p_hl7_msg);
    g_obr     := kgc_zis.getsegmentcode('OBR'
                                       ,p_hl7_msg);
    g_nte     := kgc_zis.getsegmentcode('NTE'
                                       ,p_hl7_msg);
    g_zbr     := kgc_zis.getsegmentcode('ZBR'
                                       ,p_hl7_msg);

    -- Id importlocatie
    OPEN c_imlo;
    FETCH c_imlo
      INTO r_imlo;
    v_ok := c_imlo%FOUND;
    CLOSE c_imlo;

    IF NOT v_ok
    THEN
      x_opmerkingen := 'Importlocatie MOSOS niet gevonden!'; -- NtB, 11-03-2009
      send_mail_error(x_opmerkingen
                     ,(g_this_package || '.' || g_this_procedure));
      RETURN 'X'; -- status hl7-bericht
    END IF;

    g_imlo_id := r_imlo.imlo_id; -- NtB: nieuw

    -- Zisnr
    g_zisnr := p_hl7_msg(v_pid) (3) (1) (1);

    IF g_sypa_zis_locatie = 'UTRECHT'
    THEN
      g_zisnr := kgc_zis.formatteer_zisnr_ut(g_zisnr);
    END IF;

    -- inleesrecord maken of bijwerken
    v_imfi_identificatie := 'ZIsnr ' || g_zisnr || '; hlbe=' ||
                            to_char(p_hlbe_id);

    OPEN c_imfi(g_imlo_id
               ,v_imfi_identificatie);
    FETCH c_imfi
      INTO r_imfi;
    IF c_imfi%NOTFOUND
    THEN
      -- toevoegen!
      -- nieuw import-id uitgeven
      SELECT nvl(MAX(imfi.imfi_id)
                ,0) + 1
      INTO   g_imfi_id
      FROM   kgc_import_files imfi;

      INSERT INTO kgc_import_files
        (imfi_id
        ,imlo_id
        ,filenaam
        ,import_datum
        ,afgehandeld -- 20071211
        ,hlbe_id)
      VALUES
        (g_imfi_id
        ,g_imlo_id
        ,v_imfi_identificatie
        ,trunc(SYSDATE)
        ,'N' -- 20071211
        ,p_hlbe_id);

    ELSE
      -- bijwerken
      UPDATE kgc_import_files imfi
      SET    import_datum = trunc(SYSDATE)
            ,afgehandeld  = 'N'
            ,opmerkingen  = NULL
            ,hlbe_id      = p_hlbe_id
      WHERE  imfi.imfi_id = r_imfi.imfi_id;

      g_imfi_id := r_imfi.imfi_id;
    END IF;
    CLOSE c_imfi;

    -- altijd de persoon uit ZIS ophalen
    -- NtB: nieuw, kgc_zis-versie ipv. eigen versie
    v_ok := kgc_zis.persoon_in_zis(p_zisnr   => g_zisnr
                                  ,pers_trow => v_pers_trow
                                  ,rela_trow => v_rela_trow
                                  ,verz_trow => v_verz_trow
                                  ,x_msg     => v_msg);

    IF NOT v_ok -- NtB: if is nieuw
    THEN
      x_opmerkingen := v_msg; -- NtB: nieuw

      UPDATE kgc_import_files imfi
      SET    opmerkingen = v_msg
      WHERE  imfi.imfi_id = g_imfi_id;

      -- COMMIT; -- NtB: gaat dit goed, indien uit??? We committen in de aanroepende routine.

      RETURN 'X'; -- NtB: nieuw, status hl7-bericht

    END IF;

    --*** persoon toevoegen of wijzigen
    -- als de info uit het PID-segment verschilt van de databaseinhoud, dan boodschap aanmaken.
    -- NtB: voor merge_im_personen gezet ipv. erna!
    send_mail_zis_controle(p_hlbe_id   => p_hlbe_id -- nw
                          ,p_pers_trow => v_pers_trow
                          ,p_hl7_msg   => p_hl7_msg
                          ,p_imfi_id   => g_imfi_id);
    -- NtB: als er een zis-fout is, dan wordt afgehandeld = 'J' en gaan we door;
    --      gegevens worden naar de import-tabellen verwerkt.
    -- Volgende afvraging komt op het eind na de import:
    --      IF g_zis_fout_aanwezig
    --      THEN
    --         v_afgehandeld := 'J';
    --         x_opmerkingen := 'Helix heeft een MOSOS-bericht ontvangen met ZISnummer ' || g_zisnr || '. ' ||
    --                          'De bijbehorende persoonsgegevens wijken af van de persoonsgegevens in ZIS. ';
    --
    --      ELSE

    -- merge_im_personen(p_impe_trow => v_impe_trow);
    v_ok := merge_im_personen(p_pers_trow => v_pers_trow
                             ,p_rela_trow => v_rela_trow
                             ,p_verz_trow => v_verz_trow);

    IF v_ok
    THEN
      --*** zwangerschap toevoegen of wijzigen
      v_ok := merge_im_zwangerschappen(p_imzw_trow => v_imzw_trow);

      IF v_ok
      THEN
        --*** foetus toevoegen of wijzigen
        v_ok := merge_im_foetussen(p_imfo_trow => v_imfo_trow
                                  ,p_imzw_trow => v_imzw_trow);
        -- NtB: global zetten tbv. monsters en onderzoeken
        g_imfo_id := v_imfo_trow.imfo_id;

        IF v_ok
        THEN
          --*** monster toevoegen of wijzigen
          v_ok := merge_im_monsters(p_immo_trow => v_immo_trow);

          IF v_ok
          THEN
            --*** onderzoek toevoegen of wijzigen
            v_ok := merge_im_onderzoeken(p_imon_trow => v_imon_trow);

            IF v_ok
            THEN
              -- NtB: er is mogelijk bestaand materiaal aanwezig,
              --      vastgelegd in g_bestaand_materiaal.
              --      Nagaan of er hier al gestopt moet worden, voorlopig niet.
              -- Volgende afvraging komt op het eind, na de import:
              --                        IF g_bestaand_materiaal
              --                        THEN
              --                           -- RDE 19-12-2007
              --                           send_mail_bestaand_materiaal(g_hl7_msg
              --                                                       ,v_imon_trow.kgc_onde_id);
              --                           v_afgehandeld := 'J'; -- RDE 19-12-2007
              --                           x_opmerkingen := 'EMail verzonden tbv aanvraag extra protocol bij bestaand materiaal.'; -- RDE 19-12-2007
              --
              --                        ELSE

              --*** onderzoek-kopiehouders toevoegen of wijzigen
              -- alleen tonen... er zijn nog geen interface-tabellen om het op te bergen!
              -- OBR-28 bevat de kopiehouders
              -- let op: indien een kopiehouder de aanvrager is: wel registreren als relatie, maar niet toevoegen als kopiehouder (de eindbrief wordt immers aan de aanvrager gericht!)
              -- NtB: moet hier nog wat mee???? v_imon_trow.IMPW_ID_RELA (aanvrager)
              --  dbms_output.put_line('Verwerking OBX - Kopiehouders');
              FOR i_koho IN 1 .. p_hl7_msg(g_obr) (28).COUNT
              LOOP
                IF nvl(length(p_hl7_msg(g_obr) (28) (i_koho) (1))
                      ,0) > 0
                   AND v_ok
                THEN
                  v_ok := merge_relaties(p_koho    => i_koho
                                        ,p_impw_id => v_impw_id); -- out

                  -- relatie bestaat nu in kgc_relaties.
                  IF v_ok
                  THEN
                    --*** ook naar kopiehouders:
                    v_ok := merge_im_onde_kopie(p_impw_id          => v_impw_id -- in
                                               ,p_code_kopiehouder => p_hl7_msg(g_obr) (28)
                                                                      (i_koho) (1));
                    --,p_imok_trow        => v_imok_trow);
                  END IF;
                END IF;
              END LOOP;
              -- END IF; -- bestaand materiaal

              IF v_ok
              THEN
                -- NtB, 06-02-2009: koppeling van onderzoek aan monster binnen hetzelfde bericht
                -- en persoon.
                v_ok := koppel_im_onderzoek_monster(p_imon_trow => v_imon_trow);
              END IF; -- koppel onde mons
            END IF; -- merge onderzoeken
          END IF; -- merge monsters
        END IF; -- merge foetussen
      END IF; -- merge zwangerschappen
    END IF; -- merge personen
    -- END IF; -- zis-fout

    -- NtB: Afhankelijk hoe het een en ander verlopen is, gaan we nu hier afgehandeld en de
    --      opmerkingen vastleggen in KGC_IMPORT_FILES. De berichtstatus gaan we retourneren.
    IF NOT v_ok
    THEN
      -- NtB: errorstack uitlezen, vastleggen en retourneren
      x_opmerkingen := cg$errors.geterrors; -- SQLERRM;
      -- END IF;

    ELSIF g_zis_fout_aanwezig
    THEN
      v_afgehandeld := 'N'; -- NtB: was eerder door mijzelf op 'J' gezet
      x_opmerkingen := 'Helix heeft een MOSOS-bericht ontvangen met ZISnummer ' ||
                       g_zisnr || '. ' ||
                       'De bijbehorende persoonsgegevens wijken af van de persoonsgegevens in ZIS. ';

    ELSIF g_bestaand_materiaal
    THEN
      -- RDE 19-12-2007
      send_mail_bestaand_materiaal(p_hlbe_id
                                  ,g_hl7_msg
                                  ,v_imon_trow.kgc_onde_id);
      v_afgehandeld := 'J'; -- RDE 19-12-2007
      x_opmerkingen := 'EMail verzonden tbv aanvraag extra protocol bij bestaand materiaal.'; -- RDE 19-12-2007
    END IF;

    -- NtB: nav. aanpassingen RDE, 26-02-2008, nog even de dataflow checken of dit hier moet!!!???
    -- mogelijk is de verwerking klaar -- RDE 19-12-2007
    -- NtB: altijd updaten met laatste stand
    -- IF v_afgehandeld = 'J'
    -- THEN
    -- RDE 19-12-2007
    UPDATE kgc_import_files imfi -- RDE 19-12-2007
    SET    afgehandeld = v_afgehandeld -- RDE 19-12-2007
          ,opmerkingen = x_opmerkingen -- RDE 19-12-2007
    WHERE  imfi.imfi_id = g_imfi_id; -- RDE 19-12-2007
    -- END IF; -- RDE 19-12-2007

    IF v_ok
    THEN
      RETURN 'V'; -- status hl7-bericht verwerkt!
    ELSE
      RETURN 'X'; -- status hl7-bericht
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens naar importtabellen. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);

      -- NtB: errorstack uitlezen, vastleggen en retourneren
      x_opmerkingen := cg$errors.geterrors;
      -- x_opmerkingen := SQLERRM;

      UPDATE kgc_import_files imfi
      SET    afgehandeld = 'N' -- v_afgehandeld
            ,opmerkingen = x_opmerkingen
      WHERE  imfi.imfi_id = g_imfi_id;

      /*
      -- NtB: evt. extra mail??? >>>
      kgc_zis.post_mail_zis(kgc_zis.g_sypa_zis_mail_err
                           ,g_this_package||'.'|| g_this_procedure || ': Fout (database: ' || upper(database_name) || ')!'
                           ,' SQLErrm: ' || x_opmerkingen);
      -- <<<
      */
      RETURN 'X'; -- status hl7-bericht

  END;

  -- stuur het binnengekomen HL7 bericht naar de juiste verwerkingsstraat
  -- start vanuit de After-insert statement trigger.
  PROCEDURE routeer_hl7_berichten(p_hlbe_id IN kgc_hl7_berichten.hlbe_id%TYPE
                                  --,p_bericht  in kgc_hl7_berichten.bericht%type
                                 ,p_hl7_msg     IN kgc_zis.hl7_msg -- NtB: ipv. p_bericht
                                 ,x_id          OUT kgc_hl7_berichten.id%TYPE
                                 ,x_entiteit    OUT kgc_hl7_berichten.entiteit%TYPE
                                 ,x_status      OUT kgc_hl7_berichten.status%TYPE -- NtB: IN toegevoegen???
                                 ,x_opmerkingen OUT kgc_hl7_berichten.opmerkingen%TYPE -- NtB: nieuw, resultaat van verwerking teruggeven
                                  ) IS
    PRAGMA AUTONOMOUS_TRANSACTION; -- NtB: nieuw
    -- NtB: Afhandeling MOSOS via deze procedure kan worden gestart vanuit een trigger of job.
    -- Afhandeling MOSOS vanuit scherm loopt via rework_request_bericht.

    -- NtB: Binnen deze procedure gaan we de tabel kgc_hl7_berichten niet
    -- meer muteren. We geven de mutatiegegevens (incl fouten) terug aan de
    -- aanroepende routine. Die moet dit verder afhandelen!
    -- Zeer verwarrend als dit ook hier gebeurt.

    -- NtB: Hetzelfde geldt voor de tabel kgc_import_files. Deze wordt binnen deze procedure
    -- geraadpleegd, default aangemaakt in verwerk_orm_o01_bericht en
    -- gemuteerd in vertaal_naar_helix. Slechts bij hoge uitzondering dit
    -- in andere routines doen.
    -- Uitzondering is kgc_import.vertaal_naar_helix. Daar kan afgehandeld op 'J' gezet worden.

    CURSOR c_imfi(b_hlbe_id kgc_import_files.hlbe_id%TYPE) IS
      SELECT imfi.imfi_id
            ,imfi.opmerkingen
            ,hlbe.status hlbe_status
            ,imfi.afgehandeld
      FROM   kgc_import_files  imfi
            ,kgc_hl7_berichten hlbe
      WHERE  imfi.hlbe_id = b_hlbe_id
      AND    hlbe.hlbe_id = imfi.hlbe_id;

    r_imfi       c_imfi%ROWTYPE;
    v_imfi_found BOOLEAN;

    CURSOR c_imfi2(b_hlbe_id kgc_import_files.hlbe_id%TYPE) IS
      SELECT imfi.imfi_id
            ,imfi.opmerkingen
            ,imfi.afgehandeld
            ,imlo.start_verwerking
      FROM   kgc_import_files    imfi
            ,kgc_import_locaties imlo
      WHERE  imfi.hlbe_id = b_hlbe_id
      AND    imfi.imlo_id = imlo.imlo_id;

    r_imfi2 c_imfi2%ROWTYPE;

  BEGIN

    -- Init
    g_this_procedure     := 'routeer_hl7_berichten';
    x_opmerkingen        := NULL; -- NtB: verplaatst
    g_soort_verwerking   := 'T'; -- rpe nav rde 05-12-2007 gestart vanuit trigger
    g_bestaand_materiaal := FALSE; -- NtB: nieuw
    g_zis_fout_aanwezig  := FALSE; -- NtB: nieuw

    OPEN c_imfi(p_hlbe_id);
    FETCH c_imfi
      INTO r_imfi;
    v_imfi_found := c_imfi%FOUND;
    CLOSE c_imfi;

    IF (v_imfi_found --
       AND r_imfi.hlbe_status IN ('N', 'X') --
       AND r_imfi.afgehandeld = 'N') --
      -- het bericht is bekend als importfile, maar in een eerdere verwerking is het bericht gelocked.
      -- nu gestart via een JOB
       OR NOT v_imfi_found
    -- ELSIF NOT v_imfi_found
    -- THEN
    -- Er is (nog) geen importfile aan het bericht gekoppeld.
    -- inkomende berichten mososkoppeling
    -- gestart via de trigger.
    -- NB mag dus geen commit in voorkomen!!
    THEN

      -- uitvoeren deel 1 van de import, vullen kgc_im* tabellen
      x_status := verwerk_orm_o01_bericht(p_hlbe_id     => p_hlbe_id
                                         ,p_hl7_msg     => p_hl7_msg
                                         ,x_id          => x_id
                                         ,x_entiteit    => x_entiteit
                                         ,x_opmerkingen => x_opmerkingen);

      -- NtB: de opgeslagen import-records vastleggen voor de volgende fase
      COMMIT;

      -- NtB: even om de tuin leiden. Hier krijgen we normaal gesproken alleen
      -- 'X' of 'V' terug. Nu doen we even of de import overnieuw moet. Dwz.
      -- een job moet het gaan proberen.
      -- x_status := 'N';
      -- einde om-de-tuin-actie

      IF x_status = 'V'
         AND NOT g_zis_fout_aanwezig
         AND NOT g_bestaand_materiaal -- NtB: nieuw
      THEN
        -- nog niet afgehandeld.
        -- Goed verwerkt in de eerste fase
        -- tweede deel uitvoeren, overzetten naar helix-tabellen
        -- pas op, er is hier nog geen imfi_id bekend!!!
        -- dus eerst ophalen, anders wordt vertaal naar helix met null aangeroepen!!!!
        -- NB c_imfi 'werkt' niet voor dit deel. Vanwege join.
        -- NtB: indien transactie gestart vanuit trigger, dan is hlbe-record
        --      op dit moment nog niet def.vastgelegd (niet gecommit)

        OPEN c_imfi2(p_hlbe_id);
        FETCH c_imfi2
          INTO r_imfi2;
        -- v_imfi_found := c_imfi2%FOUND;
        CLOSE c_imfi2;

        IF r_imfi2.start_verwerking = 'D' -- direct, anders niets (trigger is dan binnenkomst materiaal); Mantis 3184: Interface voor Dragon
        THEN
          -- uitvoeren deel 2 van de import, vullen hlx tabellen
          -- NtB: nu 2e nested autonome transactie
          vertaal_mosos_naar_helix(p_imfi_id     => r_imfi2.imfi_id
                                  ,x_opmerkingen => x_opmerkingen);

          -- NtB: refresh
          OPEN c_imfi2(p_hlbe_id);
          FETCH c_imfi2
            INTO r_imfi2;
          -- v_imfi_found := c_imfi2%FOUND;
          CLOSE c_imfi2;

          IF r_imfi2.afgehandeld = 'J'
          THEN
            -- koppel_onderzoek_monster( r_imfi2.imfi_id );
            -- NtB: gebeurt al in vertaal_naar_helix!

            -- Via trigger, afgehandeld , mail sturen??
            send_mail_import_oke(p_hlbe_id
                                ,p_hl7_msg); -- NtB: gereactiveerd

          ELSE
            -- Via trigger, niet afgehandeld, mail sturen ??
            send_mail_import_niet_oke(p_hlbe_id
                                     ,nvl(x_opmerkingen
                                         ,r_imfi2.opmerkingen) -- NtB, 11-03-2009: nw, er kunnen opm. komen vanuit vertaal_naar_helix
                                     ,p_hl7_msg);
          END IF;
        END IF;
      ELSIF (g_zis_fout_aanwezig OR g_bestaand_materiaal) -- NtB: nieuw g_bestaand...
            AND x_status = 'V'
      THEN
        -- trigger verwerking,
        -- er is een fout in de verwerking van het ZIS-nummer of
        -- bestaand materiaal, -- NtB: nieuw
        -- maar de import naar de _im_tabellen is wel goed verlopen.
        -- Verzenden van zis-mail is 'gelijk' aan de import_niet_oke
        -- NtB: send_mail_zis_controle en send_mail_bestaand_materiaal is al gebeurd.
        NULL;
        -- NtB: afgehandeld blijft 'N' voor g_zis_fout_aanwezig;
        --      afgehandeld is 'J' geworden voor g_bestaand_materiaal;
        --      in beide gevallen geen verwerking naar helixtabellen!!!
      ELSE
        -- triggerverwerking, status na import naar _im <> V
        -- mogelijk heb je al een zis-mail verstuurd (?)
        -- generieke fout. Toch iets melden per mail????
        send_mail_import_niet_oke(p_hlbe_id
                                 ,x_opmerkingen
                                 ,p_hl7_msg);
      END IF;

      -- ELSE
      -- geen trigger, geen job, dus niets doen
      -- NtB: dwz. status = 'V' en afgehandeld = 'J',
      -- NULL;

    END IF;

    g_soort_verwerking := NULL; -- rpe nav rde 05-12-2007

    COMMIT; -- NtB: nieuw, autonome transactie afsluiten

  EXCEPTION
    -- NtB: exception is nieuw
    WHEN OTHERS THEN
      -- x_opmerkingen := x_opmerkingen || '. ' || SQLERRM;
      -- NtB: misschien beter niet concateneren en alles op een stack plaatsen:
      --    cg$errors.push(msg => x_opmerkingen)
      --    of qms$errors.unhandled_exception(p_procname => 'bla die bla') (raises application error!!!)
      --    of zoiets
      -- en later uitvragen met
      -- x_opmerkingen OF x_fout := cg$errors.GetErrors;
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van berichtgegevens. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);

      -- NtB: errorstack uitlezen, vastleggen en retourneren
      x_opmerkingen := cg$errors.geterrors;

      x_status := 'X';

      ROLLBACK; -- de hele autonome transactie, voor zover nog niet gecommit.

      send_mail_import_niet_oke(p_hlbe_id
                               ,x_opmerkingen -- NtB: toegevoegd, wellicht nog een andere message nodig
                               ,p_hl7_msg);

  END routeer_hl7_berichten;

  -- verwerken bestanden met de status anders dan V
  -- wordt ALLEEN aangeroepen vanuit KGCIMPO01, PB_inlezen
  -- overnieuw verwerken, alsof de trigger is afgegaan, dus alsof bericht
  -- net is aangeboden.
  -- NtB: commit niet vanuit scherm.
  PROCEDURE rework_request_bericht(p_imfi_id kgc_import_files.imfi_id%TYPE) IS

    CURSOR c_hlbe(b_imfi_id kgc_import_files.imfi_id%TYPE) IS
      SELECT hlbe.hlbe_id
            ,hlbe.bericht
            ,hlbe.entiteit
            ,hlbe.id
            ,hlbe.status
            ,hlbe.opmerkingen
      FROM   kgc_hl7_berichten hlbe
            ,kgc_import_files  imfi
      WHERE  hlbe.status <> 'V'
      AND    hlbe.hlbe_id = imfi.hlbe_id
      AND    imfi.imfi_id = b_imfi_id
      AND    hlbe.bericht_type = 'ORM_O01';

    r_hlbe       c_hlbe%ROWTYPE;
    v_hlbe_found BOOLEAN;
    v_hl7_msg    kgc_zis.hl7_msg;

  BEGIN
    -- Init
    g_soort_verwerking := 'S'; -- Ntb, 13-02-2009: nw, gestart vanuit scherm

    OPEN c_hlbe(p_imfi_id);
    FETCH c_hlbe
      INTO r_hlbe;
    v_hlbe_found := c_hlbe%FOUND;
    CLOSE c_hlbe;

    IF v_hlbe_found
    THEN
      r_hlbe.opmerkingen := NULL;
      v_hl7_msg          := kgc_zis.str2hl7msg(p_hlbe_id => r_hlbe.hlbe_id
                                              ,p_str     => r_hlbe.bericht);

      BEGIN
        -- SAVEPOINT hl7_rework; -- NtB: niet nodig, hiervoor is nog geen DML geweest

        r_hlbe.status := verwerk_orm_o01_bericht(p_hlbe_id     => r_hlbe.hlbe_id
                                                ,p_hl7_msg     => v_hl7_msg
                                                ,x_entiteit    => r_hlbe.entiteit
                                                ,x_id          => r_hlbe.id
                                                ,x_opmerkingen => r_hlbe.opmerkingen);

        UPDATE kgc_hl7_berichten hlbe
        SET    status      = r_hlbe.status
              ,opmerkingen = r_hlbe.opmerkingen
        WHERE  hlbe_id = r_hlbe.hlbe_id;

        -- 1e deel nu vastleggen
        COMMIT;

        /* -- NtB: moet dit evt.nog??
        IF x_status = 'V'
           AND NOT g_zis_fout_aanwezig
           AND NOT g_bestaand_materiaal -- NtB: nieuw
        THEN
           ...
        */

        -- NtB: is autonome transactie hier gewenst? Is OK
        vertaal_mosos_naar_helix(p_imfi_id     => p_imfi_id
                                ,x_opmerkingen => r_hlbe.opmerkingen); -- NtB, 04-03-2009: nw

        -- NtB: even afvragen of extra mail hier gewenst is
        IF r_hlbe.status = 'V'
        THEN

          send_mail_import_oke(r_hlbe.hlbe_id
                              ,v_hl7_msg);
          -- niet_oke_2 lijkt niet van toepassing
          -- send_mail_bestaand_materiaal(r_hlbe.opmerkingen,v_hl7_msg);

        ELSE
          send_mail_import_niet_oke(r_hlbe.hlbe_id
                                   ,r_hlbe.opmerkingen
                                   ,v_hl7_msg);
        END IF;

      EXCEPTION
        WHEN OTHERS THEN

          r_hlbe.opmerkingen := SQLERRM;
          r_hlbe.status      := 'X';
          ROLLBACK; -- TO hl7_rework; -- NtB: niet nodig

          UPDATE kgc_hl7_berichten hlbe
          SET    status      = r_hlbe.status
                ,opmerkingen = r_hlbe.opmerkingen
          WHERE  hlbe_id = r_hlbe.hlbe_id;

          COMMIT; -- NtB: vastleggen status en opm.

          send_mail_import_niet_oke(r_hlbe.hlbe_id
                                   ,r_hlbe.opmerkingen
                                   ,v_hl7_msg);

      END;

      COMMIT; -- NtB, 04-02-2009: Final commit. Niet vanuit scherm committen. Wel requeryen in scherm na afloop.

    END IF;

    g_soort_verwerking := NULL; -- NtB: maar even resetten voor de zekerheid

  END rework_request_bericht;

  PROCEDURE vertaal_mosos_naar_helix(p_imfi_id     IN kgc_import_files.imfi_id%TYPE
                                    ,x_opmerkingen OUT kgc_import_files.opmerkingen%TYPE -- NtB: nieuw, resultaat van verwerking teruggeven
                                     ) IS
    PRAGMA AUTONOMOUS_TRANSACTION; -- NtB: gereactiveerd

    CURSOR c_imfi(b_imfi_id kgc_import_files.imfi_id%TYPE, b_imfi_afgehandeld kgc_import_files.afgehandeld%TYPE) IS
      SELECT imfi.imfi_id
            ,imfi.afgehandeld
            ,hlbe.hlbe_id
            ,hlbe.bericht
            ,hlbe.opmerkingen || chr(10) || imfi.opmerkingen opmerkingen
      FROM   helix.kgc_import_files imfi
            ,kgc_hl7_berichten      hlbe
      WHERE  imfi.afgehandeld = b_imfi_afgehandeld
      AND    hlbe.status = 'V'
      AND    imfi.hlbe_id = hlbe.hlbe_id
      AND    imfi.imfi_id = b_imfi_id;

    r_imfi       c_imfi%ROWTYPE;
    v_imfi_found BOOLEAN;

  BEGIN

    -- Logging
    g_this_procedure := 'vertaal_mosos_naar_helix';
    kgc_zis.log(g_this_package || '.' || g_this_procedure ||
                ': Importgegevens behorende bij imfi_id ' ||
                to_char(p_imfi_id) || ' worden vertaald naar helix!'); -- NtB: nieuw

    -- rpe nav rde 05-12-2007 gestart vanuit scherm of trigger
    -- belangrijk ivm sturen mailtjes
    IF g_soort_verwerking = 'T'
    THEN
      -- Blijkbaar gestart vanuit trigger

      -- NtB: igv. trigger is er een hlbe_id en is de status inmiddels al V
      -- anders komen we hier niet.
      -- Uitvragen van actuele hlbe_status lukt hier overigens niet ivm. auton.transactie
      -- NtB: we gebruiken hier geen cursor.

      kgc_import.vertaal_naar_helix(p_imfi_id       => p_imfi_id
                                   ,p_online        => FALSE -- 283
                                   ,p_beheer_in_zis => 'J'); -- NtB, 12-06-2009: itt. ASTRAIA moet deze juist op J
      -- NtB, 05-05-2009, 283: nav. BSN icm. autorisatie
      -- NtB: er zitten een commit en een rollback zonder savepoint in kgc_import.vertaal_naar_helix;
      -- als er iets fout gaat, dan wordt alleen deze autonome transactie teruggedraaid

    ELSE
      -- NtB: wordt voortaan gezet in rework_request_bericht
      -- g_soort_verwerking := 'S'; -- anders gestart vanuit Scherm

      OPEN c_imfi(p_imfi_id
                 ,'N'); -- werkt niet via trigger!!!!
      FETCH c_imfi
        INTO r_imfi;
      v_imfi_found := c_imfi%FOUND;
      CLOSE c_imfi;

      IF v_imfi_found
      THEN
        kgc_import.vertaal_naar_helix(p_imfi_id       => r_imfi.imfi_id
                                     ,p_online        => TRUE -- 283
                                     ,p_beheer_in_zis => 'J');
        -- NtB, 12-06-2009: online kunnen persoonsgegevens overschreven worden -->
        --                  via beheer in zis wordt dat later wel weer gecorrigeerd!
        -- NtB, 05-05-2009, 283: nav. BSN icm. autorisatie
        -- NtB: er zitten een commit en een rollback zonder savepoint in kgc_import.vertaal_naar_helix;
        -- als er iets fout gaat, dan wordt alleen deze autonome transactie teruggedraaid
      END IF;

    END IF;

    COMMIT;

  EXCEPTION
    -- NtB: in kgc_import.vertaal_naar_helix zit een
    -- raise_application_error(-20000)
    WHEN OTHERS THEN
      qms$errors.show_message(p_mesg  => 'Fout in ' || g_this_package || '.' ||
                                         g_this_procedure ||
                                         ' tijdens verwerken van gegevens naar helix. ' ||
                                         SQLERRM
                             ,p_errtp => 'E'
                             ,p_rftf  => FALSE);

      -- NtB: errorstack uitlezen, vastleggen en retourneren
      x_opmerkingen := cg$errors.geterrors;
      -- v_opmerking := SQLERRM;

      ROLLBACK; -- TO SAVEPOINT sp_naar_helix; -- NtB: nieuw

      UPDATE helix.kgc_import_files
      SET    opmerkingen = x_opmerkingen
            ,afgehandeld = 'N'
      WHERE  imfi_id = r_imfi.imfi_id;

      COMMIT;

  END vertaal_mosos_naar_helix;
  /*** einde van MOSOS ******************************************************************************/
  --
  /*** naar MOSOS ***********************************************************************************/
  FUNCTION bepaal_mosos_onderzoekcode(p_hlbe_id      IN kgc_hl7_berichten.hlbe_id%TYPE
                                     ,p_hlbe_bericht IN kgc_hl7_berichten.bericht%TYPE)
    RETURN kgc_import_param_waarden.externe_id%TYPE IS

    v_hl7_msg_orig kgc_zis.hl7_msg;
    v_obr          VARCHAR2(10);

  BEGIN
    v_hl7_msg_orig := kgc_zis.str2hl7msg(p_hlbe_id
                                        ,p_hlbe_bericht);
    IF v_hl7_msg_orig.COUNT = 0
    THEN
      /*
      x_verslag := 'Onderzoek ' || r_onde.onderzoeknr || '; filenaam ' || r_imfi.filenaam ||
                   '; hl7-bericht hlbe_id=' || v_hlbe_id || ': afwezig of fout verwerkt! '; -- NtB: nw
      raise_application_error(-20000
                             ,'Onderzoek ' || r_onde.onderzoeknr || '; filenaam ' || r_imfi.filenaam ||
                              '; hl7-bericht hlbe_id=' || v_hlbe_id || ': afwezig of fout verwerkt!');
      */
      RETURN NULL;
    ELSE
      v_obr := kgc_zis.getsegmentcode('OBR'
                                     ,v_hl7_msg_orig);
      RETURN v_hl7_msg_orig(v_obr)(4)(1)(1);
    END IF;
  END bepaal_mosos_onderzoekcode;
  /**************************************************************************************************/
  FUNCTION mosos_uitsl_spec_concl(p_mosos_onderzoekcode IN kgc_import_param_waarden.externe_id%TYPE
                                 ,p_imlo_id             IN kgc_import_locaties.imlo_id%TYPE
                                 ,p_onde_id             IN kgc_onderzoeken.onde_id%TYPE
                                 ,x_status_uitslag      OUT VARCHAR2
                                 ,x_uitslag_conc        OUT VARCHAR2
                                 ,x_uitslag_spec        OUT VARCHAR2
                                 ,x_uitslag_tekst       OUT VARCHAR2
                                 ,x_fout_verslag        IN OUT VARCHAR2)
    RETURN BOOLEAN IS

    v_deze_uitslag_verzenden BOOLEAN DEFAULT TRUE;
    v_stgr_id                NUMBER(10);

  BEGIN
    -- stoftestgroep ophalen uit vertaaltabel (kgc_import_parameters)
    v_stgr_id := kgc_import.helix_id(p_imlo_id    => p_imlo_id
                                    ,p_entiteit   => 'KGC_STOFTESTGROEPEN'
                                    ,p_externe_id => p_mosos_onderzoekcode
                                    ,p_hint       => NULL);

    IF v_stgr_id IS NULL
    THEN
      v_deze_uitslag_verzenden := FALSE;
      x_fout_verslag           := x_fout_verslag ||
                                  'Bij dit onderzoek is MOSOS-code ' ||
                                  p_mosos_onderzoekcode ||
                                  ' aanwezig; deze bestaat niet in de parameterwaarden voor deze locatie! ' ||
                                  chr(10); -- NtB: nw
    ELSE
      OPEN c_stgr(b_stgr_id => v_stgr_id
                 ,b_onde_id => p_onde_id);
      FETCH c_stgr
        INTO r_stgr;
      IF c_stgr%NOTFOUND
      THEN
        v_deze_uitslag_verzenden := FALSE;
        x_fout_verslag           := x_fout_verslag ||
                                    'Bij dit onderzoek is MOSOS-code ' ||
                                    p_mosos_onderzoekcode ||
                                    ' aanwezig; de bijbehorende waarde (' ||
                                    v_stgr_id ||
                                    ') bestaat niet als stoftestgroep! ' ||
                                    chr(10); -- NtB: nw
      ELSE
        -- controle of dit protocol ook uitgevoerd is:
        IF r_stgr.meti_id IS NULL
        THEN
          v_deze_uitslag_verzenden := FALSE;
          x_fout_verslag           := x_fout_verslag ||
                                      'Bij dit onderzoek is MOSOS-code ' ||
                                      p_mosos_onderzoekcode ||
                                      ' aanwezig; het bijbehorende protocol (' ||
                                      r_stgr.code ||
                                      ') is echter niet aangemeld bij het onderzoek! ' ||
                                      chr(10); -- NtB: nw
        ELSE
          -- meting aanwezig - bepaal status uitslag
          -- NtB: alleen definitieve >>>
          -- IF p_definitieve_uitslagen = 'J'
          -- THEN
          x_status_uitslag := 'F'; -- final
          -- ELSE
          --    v_status_uitslag := 'P'; -- voorlopig
          --   IF r_stgr.meti_afgerond = 'N'
          --   THEN
          --      v_deze_uitslag_verzenden := FALSE;
          --   END IF;
          -- END IF;
          -- NtB: <<<
        END IF;
      END IF;
      CLOSE c_stgr;
    END IF;

    -- bepaal conclusie en spec
    -- x_uitslag_conc  := 'Zie uitslag'; ???????
    x_uitslag_conc  := NULL;
    x_uitslag_spec  := NULL;
    x_uitslag_tekst := NULL; -- NtB: nieuw

    -- conclusie= prot-KAREIND+stof-MOSOS.meetwaarde; spcecificatie= prot-KAREIND+stof-KAREIND.meetwaarde
    FOR r_meet IN c_meet(p_onde_id
                        ,v_stgr_id)
    LOOP
      -- NtB: nav. aanpassingen RDE, check op lengte toegevoegd
      IF r_meet.stof_code = 'MOSOS'
      THEN
        x_uitslag_conc := nvl(r_meet.meetwaarde
                             ,x_uitslag_conc); -- rde 28-02-2008 later check op lengte
        IF nvl(length(x_uitslag_conc)
              ,0) > 54
        THEN
          x_uitslag_tekst := 'Conclusie: ' || x_uitslag_conc;
          x_uitslag_conc  := 'Zie uitslag (de tekst is te lang)';
        END IF;
      ELSIF r_meet.stof_code = r_stgr.code
      THEN
        x_uitslag_spec := r_meet.meetwaarde; -- rde 28-02-2008 later check op lengte
        IF nvl(length(x_uitslag_spec)
              ,0) > 54
        THEN
          IF nvl(length(x_uitslag_tekst)
                ,0) > 0
          THEN
            x_uitslag_tekst := x_uitslag_tekst || chr(10);
          END IF;
          x_uitslag_tekst := 'Specificatie: ' || x_uitslag_spec;
          x_uitslag_spec  := 'Zie uitslag (de tekst is te lang)';
        END IF;
      END IF;
    END LOOP;

    RETURN v_deze_uitslag_verzenden;

  END mosos_uitsl_spec_concl;
  /**************************************************************************************************/
  PROCEDURE mosos_result_bericht(p_kafd_id     IN kgc_kgc_afdelingen.kafd_id%TYPE
                                ,p_ongr_id     IN kgc_onderzoeksgroepen.ongr_id%TYPE
                                ,p_onderzoeknr IN kgc_onderzoeken.onderzoeknr%TYPE
                                 -- ,p_definitieve_uitslagen VARCHAR2 -- NtB: alleen maar definitieve
                                ,p_uitslagen_verzenden IN BOOLEAN
                                ,x_status              OUT VARCHAR2 -- NtB: nieuw
                                ,x_onde_msg_verz_count OUT NUMBER -- NtB: nieuw
                                ,x_verslag             OUT VARCHAR2 -- NtB: nieuw
                                 ) IS
    -- NtB, 27-01-2009: Aanpassingen nav. nieuw FO en:
    --                  rde 28-02-2008 aanpassing tbv 'niet-toestaan' voorlopige uitslagen
    --                  en beveiliging tegen te lange spec of conc!
    --
    CURSOR c_onde --
    (b_kafd_id NUMBER --
    , b_ongr_id NUMBER --
    , b_onderzoeknr VARCHAR2) IS
      SELECT onde.onde_id
            ,onde.onderzoeknr
            ,onui.code onui_code
            ,onui.omschrijving onui_omschrijving
            ,pers.aanspreken
            ,pers.geslacht
            ,pers.geboortedatum
      FROM   kgc_onderzoeken            onde
            ,kgc_onderzoek_uitslagcodes onui
            ,kgc_personen               pers
      WHERE  onde.kafd_id = b_kafd_id
      AND    onde.ongr_id = b_ongr_id
      AND    onde.pers_id = pers.pers_id
      AND    onde.onui_id = onui.onui_id(+)
      AND    onde.onderzoeknr = nvl(b_onderzoeknr
                                   ,onde.onderzoeknr)
      AND    onde.afgerond = 'J'
            -- AND kgc_brie_00.eerder_geprint(onde.onde_id,'UITS_HL7') = 'N' -- vervangen door:
      AND    NOT EXISTS (SELECT 1
              FROM   kgc_brieven    brie
                    ,kgc_brieftypes brty
              WHERE  brie.brty_id = brty.brty_id
              AND    brty.code = 'UITS_HL7'
              AND    brie.onde_id = onde.onde_id)
      AND    EXISTS (SELECT 1
              FROM   kgc_im_onderzoeken imon
              WHERE  onde.onde_id = imon.kgc_onde_id)
      --  nog toevoegen: bv een datum dat er gestart wordt met het interface zodat je niet alle oude onderzoeken langskrijgt
      -- NtB??????
      ORDER  BY onde.onderzoeknr; -- desc: alleen voor test

    CURSOR c_meka(b_kafd_id NUMBER, b_mede_id NUMBER) IS
      SELECT 1
      FROM   kgc_mede_kafd meka
      WHERE  meka.kafd_id = b_kafd_id
      AND    meka.mede_id = b_mede_id
      AND    autorisator = 'J';

    r_meka c_meka%ROWTYPE;

    -- rde 06-04-2009 aangepast - aanvragen zonder onderzoek-id worden nooit teruggestuurd
    -- dit is bv het geval bij een extra aanvraag voor een bestaand materiaal
    CURSOR c_imon(b_onde_id NUMBER) IS
    -- SELECT imon2.* -- NtB: distinct om te ontdubbelen.
      SELECT DISTINCT imon2.*
      FROM   kgc_im_onderzoeken imon1 -- terugkoppeling kan alleen indien toegevoegd via hl7
            ,kgc_im_onderzoeken imon2
      WHERE  imon1.kgc_onde_id = b_onde_id
      AND    imon2.imon_id LIKE
             substr(imon1.imon_id
                    ,1
                    ,instr(imon1.imon_id
                          ,'-') - 1) || '%' -- koppel op materiaal
      AND    imon1.impe_id = imon2.impe_id
      ORDER  BY imon2.imon_id; -- NtB: nieuw

    r_imon c_imon%ROWTYPE;

    CURSOR c_imfi(b_imfi_id NUMBER) IS
      SELECT * FROM kgc_import_files imfi WHERE imfi.imfi_id = b_imfi_id;
    -- AND hlbe_id IS NOT NULL; -- NtB: ?????

    r_imfi      c_imfi%ROWTYPE;
    r_imfi_null c_imfi%ROWTYPE; -- NtB: nieuw

    CURSOR c_hlbe(b_hlbe_id NUMBER) IS
      SELECT * FROM kgc_hl7_berichten hlbe WHERE hlbe.hlbe_id = b_hlbe_id;

    r_hlbe      c_hlbe%ROWTYPE;
    r_hlbe_null c_hlbe%ROWTYPE; -- NtB: nieuw

    -- NtB: nieuwe cursor
    CURSOR c_prot(b_onde_id NUMBER) IS
      SELECT stgr.*
      FROM   bas_metingen        meti
            ,kgc_stoftestgroepen stgr
      WHERE  meti.onde_id = b_onde_id
      AND    meti.stgr_id = stgr.stgr_id;

    CURSOR c_uits(b_onde_id NUMBER) IS
      SELECT onde.pers_id
            ,onde.kafd_id
            ,onde.ongr_id
            ,onde.onde_id
            ,onde.rela_id
            ,onde.taal_id
            ,uits.uits_id
            ,kgc_adres_00.relatie(uits.rela_id
                                 ,'J') geadresseerde
            ,datum_uitslag
            ,tekst
            ,toelichting
      FROM   kgc_uitslagen   uits
            ,kgc_brieftypes  brty
            ,kgc_onderzoeken onde
      WHERE  uits.onde_id = onde.onde_id
      AND    uits.brty_id = brty.brty_id
      AND    brty.code = 'EINDBRIEF'
      AND    onde.onde_id = b_onde_id;

    r_uits      c_uits%ROWTYPE;
    r_uits_null c_uits%ROWTYPE; -- NtB: nieuw

    CURSOR c_ramo(b_brty_id IN NUMBER) IS
      SELECT ramo.ramo_id
      FROM   kgc_rapport_modules ramo
            ,kgc_brieftypes      brty
      WHERE  brty.brty_id = b_brty_id
      AND    brty.ramo_id = ramo.ramo_id;

    v_brty_id                NUMBER;
    v_deze_uitslag_verzenden BOOLEAN;
    v_hl7_msg_orig           kgc_zis.hl7_msg;
    v_hlbe_id                NUMBER;
    v_msg                    VARCHAR2(32767);
    v_msh                    VARCHAR2(10);
    v_obr                    VARCHAR2(10);
    v_onde_count             NUMBER := 0;
    v_onde_msg_count         NUMBER := 0;
    v_onde_prot_ext          VARCHAR2(255);
    v_onderzoekcode          VARCHAR2(100);
    v_pid                    VARCHAR2(10);
    v_ramo_id                NUMBER;
    v_spec_kafd_code         VARCHAR2(4); -- UMCU specifiek 2
    v_status_uitslag         VARCHAR2(1);
    v_status_uitslag_ext     VARCHAR2(100);
    v_stgr_id                NUMBER;
    v_strhl7debug            VARCHAR2(32767);
    v_uitslag_conc           VARCHAR2(4000); -- NtB: nav. aanpassingen RDE, 28-02-2008 kan langer zijn dan 54!
    v_uitslag_spec           VARCHAR2(4000); -- NtB: nav. aanpassingen RDE, 28-02-2008 kan langer zijn dan 54!
    v_uitslag_tekst          VARCHAR2(32767);
    v_verslag                CLOB;
    v_omschrijving           kgc_notities.omschrijving%TYPE;
  BEGIN
    -- Init
    g_this_procedure      := 'mosos_result_bericht';
    x_status              := 'X'; -- NtB: nw, nog niet verwerkt
    x_onde_msg_verz_count := 0;
    x_verslag             := ''; --'Start ' || g_this_procedure; -- NtB: nw

    -- kgc_zis.g_sypa_hl7_versie := kgc_sypa_00.systeem_waarde('HL7_VERSIE');
    -- NtB: wordt ge�tieerd tijdens package-initiatie

    -- user geautoriseerd?
    OPEN c_meka(p_kafd_id
               ,kgc_mede_00.medewerker_id);
    FETCH c_meka
      INTO r_meka;
    IF c_meka%NOTFOUND
    THEN
      CLOSE c_meka;
      x_verslag := 'U bent geen autorisator voor deze afdeling! '; -- NtB: nw
      RETURN;
    END IF;
    CLOSE c_meka;

    -- producerende afdeling?
    v_spec_kafd_code := kgc_attr_00.waarde('KGC_KGC_AFDELINGEN'
                                          ,'PROD_CODE'
                                          ,p_kafd_id);
    IF nvl(length(v_spec_kafd_code)
          ,0) = 0
    THEN
      x_verslag := 'Bij de kgc-afdeling ontbreekt het extra-attribuut PROD_CODE! '; -- NtB: nw
      RETURN;
    END IF;

    /* NtB: generaliseren door evt. de generieke kgc_zis.send_msg versie te gebruiken
    **      (deze opent, verzend en sluit in 1x)?*/
    IF p_uitslagen_verzenden
    THEN
      open_verbinding(p_timeout => 150); -- 15 seconden maxinaal Mantis 3879 21-12-2009 GWE
    END IF;
    /**/

    FOR r_onde IN c_onde(p_kafd_id
                        ,p_ongr_id
                        ,p_onderzoeknr)
    --,p_definitieve_uitslagen) -- NtB: alleen maar definitief
    LOOP
      v_onderzoekcode := NULL;
      v_uitslag_spec  := NULL;
      v_uitslag_conc  := NULL;
      v_onde_count    := v_onde_count + 1;

      -- NtB: nieuw, met v_onde_prot_ext wordt echter verder niets gedaan??? >>>
      v_onde_prot_ext := NULL;
      -- gevonden protocollen:
      FOR r_prot IN c_prot(r_onde.onde_id)
      LOOP
        IF c_prot%ROWCOUNT > 1
        THEN
          v_onde_prot_ext := v_onde_prot_ext || '; ';
        END IF;
        v_onde_prot_ext := v_onde_prot_ext || r_prot.code;
      END LOOP;
      -- NtB: <<<

      -- bepaal uitslag tbv NTE
      r_uits := r_uits_null; -- NtB: nieuw
      OPEN c_uits(r_onde.onde_id);
      FETCH c_uits
        INTO r_uits;
      CLOSE c_uits;

      -- verstuur een bericht voor ieder 'import-onderzoek'
      -- extra toevoeging: ophalen van alle aanvragen van een specifiek materiaal!
      v_onde_msg_count := 0;
      FOR r_imon IN c_imon(r_onde.onde_id)
      LOOP

        v_onde_msg_count := v_onde_msg_count + 1;
        -- v_deze_uitslag_verzenden := p_uitslagen_verzenden; NtB, 28-04-2009
        r_stgr := r_stgr_null; -- NtB: nieuw??????????
        -- bepaal 'file'
        r_imfi := r_imfi_null; -- NtB: nieuw

        OPEN c_imfi(r_imon.imfi_id);
        FETCH c_imfi
          INTO r_imfi;
        CLOSE c_imfi;

        -- originele bericht ophalen
        v_hlbe_id := r_imfi.hlbe_id; -- NtB: nieuw
        r_hlbe    := r_hlbe_null; -- NtB: nieuw
        OPEN c_hlbe(v_hlbe_id);
        FETCH c_hlbe
          INTO r_hlbe;
        CLOSE c_hlbe;

        -- ophalen
        v_hl7_msg_orig := kgc_zis.str2hl7msg(v_hlbe_id
                                            ,r_hlbe.bericht);
        IF v_hl7_msg_orig.COUNT = 0
        THEN
          -- x_verslag := x_verslag || 'Onderzoek ' || r_onde.onderzoeknr || '; filenaam ' || r_imfi.filenaam ||
          --             '; hl7-bericht hlbe_id=' || v_hlbe_id || ': afwezig of fout verwerkt! ' || chr(10);
          -- NtB: melding wordt later in exception toegevoegd aan het verslag
          raise_application_error(-20000
                                 ,'Onderzoek ' || r_onde.onderzoeknr || --
                                  '; filenaam ' || r_imfi.filenaam || --
                                  '; hl7-bericht hlbe_id=' || v_hlbe_id || --
                                  ': afwezig of fout verwerkt! ' ||
                                  chr(10));
        END IF;

        v_msh           := kgc_zis.getsegmentcode('MSH'
                                                 ,v_hl7_msg_orig);
        v_pid           := kgc_zis.getsegmentcode('PID'
                                                 ,v_hl7_msg_orig);
        v_obr           := kgc_zis.getsegmentcode('OBR'
                                                 ,v_hl7_msg_orig);
        v_onderzoekcode := v_hl7_msg_orig(v_obr) (4) (1) (1);

        -- vertaal onderzoek naar Helix-waarde...
        v_deze_uitslag_verzenden := mosos_uitsl_spec_concl(p_mosos_onderzoekcode => v_onderzoekcode
                                                          ,p_imlo_id             => r_imfi.imlo_id
                                                          ,p_onde_id             => r_onde.onde_id
                                                          ,x_status_uitslag      => v_status_uitslag
                                                          ,x_uitslag_conc        => v_uitslag_conc
                                                          ,x_uitslag_spec        => v_uitslag_spec
                                                          ,x_uitslag_tekst       => v_uitslag_tekst
                                                          ,x_fout_verslag        => x_verslag);

        IF v_deze_uitslag_verzenden
           AND p_uitslagen_verzenden -- NtB, 28-04-2008, nieuw nav. van vervanging hierboven
        THEN

          <<verwerk_oru_001_bericht>>
          DECLARE
            v_hl7_msg_bron kgc_zis.hl7_msg;
            v_hl7_msg_res  kgc_zis.hl7_msg;
            v_strantw      VARCHAR2(32767);
            v_strhl7       VARCHAR2(32767);
            v_msa          VARCHAR2(10);
            v_sysdate      VARCHAR2(20) := to_char(SYSDATE
                                                  ,'yyyymmddhh24miss');
            -- NtB: nav. aanpassingen RDE, 21-01-2008: er zit geen tijd in de datum-uitslag; hierdoor problemen met de sync met MOSOS
            --                 gebruik van de systeemdatum lost dit op!
            --          v_uitslagdatum    VARCHAR2(20) := to_char(r_uits.datum_uitslag, 'yyyymmddhh24miss');
            v_uitslagdatum   VARCHAR2(20) := to_char(SYSDATE
                                                    ,'yyyymmddhh24miss');
            v_ok             BOOLEAN;
            v_msg_control_id VARCHAR2(10);
            v_msg            VARCHAR2(32767);
          BEGIN
            v_msg_control_id := substr(to_char(abs(dbms_random.random))
                                      ,1
                                      ,10);
            -- bericht samenstellen
            -- **** MSH
            v_hl7_msg_bron('MSH') := kgc_zis.init_msg('MSH'
                                                     ,kgc_zis.g_sypa_hl7_versie);
            v_hl7_msg_bron('MSH')(1)(1)(1) := '|';
            v_hl7_msg_bron('MSH')(2)(1)(1) := '^~\&'; -- JKO/4265: weggevallen slash weer toegevoegd.
            v_hl7_msg_bron('MSH')(3)(1)(1) := 'CytoApp';
            v_hl7_msg_bron('MSH')(4)(1)(1) := 'CytoLab';
            v_hl7_msg_bron('MSH')(5)(1)(1) := v_hl7_msg_orig(v_msh) (3) (1) (1); --'Mosos CS-2';
            v_hl7_msg_bron('MSH')(6)(1)(1) := v_hl7_msg_orig(v_msh) (4) (1) (1); --'Mosos';
            v_hl7_msg_bron('MSH')(7)(1)(1) := v_sysdate;
            v_hl7_msg_bron('MSH')(9)(1)(1) := 'ORU^R01';
            v_hl7_msg_bron('MSH')(10)(1)(1) := v_msg_control_id;
            v_hl7_msg_bron('MSH')(11)(1)(1) := 'P';
            v_hl7_msg_bron('MSH')(12)(1)(1) := kgc_zis.g_sypa_hl7_versie;
            v_hl7_msg_bron('MSH')(15)(1)(1) := 'AL';
            v_hl7_msg_bron('MSH')(16)(1)(1) := 'NE';

            -- **** PID
            v_hl7_msg_bron('PID') := kgc_zis.init_msg('PID'
                                                     ,kgc_zis.g_sypa_hl7_versie);
            v_hl7_msg_bron('PID')(1)(1)(1) := '1';
            v_hl7_msg_bron('PID')(3)(1)(1) := v_hl7_msg_orig(v_pid) (3) (1) (1);

            v_hl7_msg_bron('PID')(5)(1) := v_hl7_msg_orig(v_pid) (5) (1); -- compleet
            v_hl7_msg_bron('PID')(7)(1) := v_hl7_msg_orig(v_pid) (7) (1); -- compleet

            -- **** OBR
            v_hl7_msg_bron('OBR') := kgc_zis.init_msg('OBR'
                                                     ,kgc_zis.g_sypa_hl7_versie);
            v_hl7_msg_bron('OBR')(1)(1)(1) := v_hl7_msg_orig(v_obr) (1) (1) (1);
            v_hl7_msg_bron('OBR')(2)(1) := v_hl7_msg_orig(v_obr) (2) (1); -- compleet
            IF v_hl7_msg_bron('OBR') (3) (1).COUNT < 2
            THEN
              v_hl7_msg_bron('OBR')(3)(1) .EXTEND;
            END IF;
            v_hl7_msg_bron('OBR')(3)(1)(1) := r_onde.onderzoeknr;
            v_hl7_msg_bron('OBR')(3)(1)(2) := 'CytoApp';

            v_hl7_msg_bron('OBR')(4)(1) := v_hl7_msg_orig(v_obr) (4) (1); -- compleet
            v_hl7_msg_bron('OBR')(7)(1)(1) := v_hl7_msg_orig(v_obr) (7) (1) (1);
            v_hl7_msg_bron('OBR')(9)(1) := v_hl7_msg_orig(v_obr) (9) (1); -- compleet
            v_hl7_msg_bron('OBR')(10)(1) := v_hl7_msg_orig(v_obr) (10) (1); -- compleet
            v_hl7_msg_bron('OBR')(11)(1)(1) := v_hl7_msg_orig(v_obr) (11) (1) (1);
            v_hl7_msg_bron('OBR')(13)(1) := v_hl7_msg_orig(v_obr) (13) (1); -- compleet
            v_hl7_msg_bron('OBR')(15)(1) := v_hl7_msg_orig(v_obr) (15) (1); -- compleet
            v_hl7_msg_bron('OBR')(16)(1) := v_hl7_msg_orig(v_obr) (16) (1); -- compleet
            v_hl7_msg_bron('OBR')(24)(1)(1) := v_spec_kafd_code; -- producerende afdeling
            v_hl7_msg_bron('OBR')(25)(1)(1) := v_status_uitslag;
            v_hl7_msg_bron('OBR')(31)(1) := v_hl7_msg_orig(v_obr) (31) (1); -- compleet

            v_strhl7 := kgc_zis.g_sob_char ||
                        kgc_zis.hl7seg2str('MSH'
                                          ,v_hl7_msg_bron('MSH')) ||
                        kgc_zis.g_cr ||
                        kgc_zis.hl7seg2str('PID'
                                          ,v_hl7_msg_bron('PID')) ||
                        kgc_zis.g_cr ||
                        kgc_zis.hl7seg2str('OBR'
                                          ,v_hl7_msg_bron('OBR')) ||
                        kgc_zis.g_cr;

            -- **** NTE: ALTIJD versturen indien uitslag gevuld...
            -- v_uitslag_tekst := r_uits.tekst;
            -- NtB: vervangen door IF-statement nav. aanpassingen RDE, 28-02-2008
            IF nvl(length(r_uits.tekst)
                  ,0) > 0
            THEN
              -- 28-02-2008 rde aangepast omdat v_uitslag_tekst mogelijk al de spec of conc bevat!
              IF nvl(length(v_uitslag_tekst)
                    ,0) > 0
              THEN
                v_uitslag_tekst := v_uitslag_tekst || chr(10);
              END IF;
              v_uitslag_tekst := v_uitslag_tekst || r_uits.tekst;
            END IF;

            IF nvl(length(r_uits.toelichting)
                  ,0) > 0
            THEN
              IF nvl(length(v_uitslag_tekst)
                    ,0) > 0
              THEN
                v_uitslag_tekst := v_uitslag_tekst || chr(10);
              END IF;
              v_uitslag_tekst := v_uitslag_tekst || r_uits.toelichting;
            END IF;
            IF nvl(length(v_uitslag_tekst)
                  ,0) > 0
            THEN
              v_uitslag_tekst := tohl7(v_uitslag_tekst); -- weg met de stuurkarakters!
              v_uitslag_tekst := REPLACE(v_uitslag_tekst
                                        ,chr(10)
                                        ,'\.br\'); -- vervang CR!
              v_uitslag_tekst := REPLACE(v_uitslag_tekst
                                        ,chr(13)
                                        ,'');
              v_hl7_msg_bron('NTE') := kgc_zis.init_msg('NTE'
                                                       ,kgc_zis.g_sypa_hl7_versie);

              -- Mantis 3047 -- melding bij te grote berichten
              IF nvl(length(v_uitslag_tekst),0) > 4000
              THEN
                v_ok      := FALSE;
                v_msg     := 'De te verzenden uitslagtekst is ' ||
                             to_char(length(v_uitslag_tekst) - 4000) ||
                             ' karakters te lang! Pas deze aan en verzend opnieuw.';
                x_verslag := x_verslag || v_msg; -- NtB, 12-03-2009: nw
                -- connectie sluiten  gwe rework mantis 3047 08-03-2009 sluit verbinding toegevoegd
                IF p_uitslagen_verzenden
                THEN
                  sluit_verbinding;
                END IF;
                RETURN;
              END IF;

              -- ah, nte-3 is 4096 lang geworden!
              v_hl7_msg_bron('NTE')(1)(1)(1) := '1';
              v_hl7_msg_bron('NTE')(2)(1)(1) := 'L';
              v_hl7_msg_bron('NTE')(3)(1)(1) := substr(v_uitslag_tekst
                                                      ,1
                                                      ,4096);
              v_strhl7 := v_strhl7 ||
                          kgc_zis.hl7seg2str('NTE'
                                            ,v_hl7_msg_bron('NTE')) ||
                          kgc_zis.g_cr;
            END IF;

            -- **** OBX-en
            IF nvl(length(v_uitslag_spec)
                  ,0) > 0
            THEN
              v_hl7_msg_bron('OBX_1') := kgc_zis.init_msg('OBX'
                                                         ,kgc_zis.g_sypa_hl7_versie);
              v_hl7_msg_bron('OBX_1')(1)(1)(1) := '1';
              v_hl7_msg_bron('OBX_1')(2)(1)(1) := 'ST';
              v_hl7_msg_bron('OBX_1')(3)(1)(1) := v_onderzoekcode ||
                                                  '-Spec^specificatie^CytoApp';
              v_hl7_msg_bron('OBX_1')(5)(1)(1) := v_uitslag_spec;
              v_hl7_msg_bron('OBX_1')(11)(1)(1) := v_status_uitslag;
              v_hl7_msg_bron('OBX_1')(14)(1)(1) := v_uitslagdatum; -- uitslag datum/tijd
              -- toevoegen
              v_strhl7 := v_strhl7 ||
                          kgc_zis.hl7seg2str('OBX'
                                            ,v_hl7_msg_bron('OBX_1')) ||
                          kgc_zis.g_cr;
            END IF;

            IF nvl(length(v_uitslag_conc)
                  ,0) > 0
            THEN
              v_hl7_msg_bron('OBX_2') := kgc_zis.init_msg('OBX'
                                                         ,kgc_zis.g_sypa_hl7_versie);
              v_hl7_msg_bron('OBX_2')(1)(1)(1) := '2';
              v_hl7_msg_bron('OBX_2')(2)(1)(1) := 'ST';
              v_hl7_msg_bron('OBX_2')(3)(1)(1) := v_onderzoekcode ||
                                                  '-Conc^conclusie^CytoApp';
              v_hl7_msg_bron('OBX_2')(5)(1)(1) := v_uitslag_conc;
              v_hl7_msg_bron('OBX_2')(11)(1)(1) := v_status_uitslag;
              v_hl7_msg_bron('OBX_2')(14)(1)(1) := v_uitslagdatum; -- uitslag datum/tijd
              -- toevoegen
              v_strhl7 := v_strhl7 ||
                          kgc_zis.hl7seg2str('OBX'
                                            ,v_hl7_msg_bron('OBX_2')) ||
                          kgc_zis.g_cr;
            END IF;

            -- afsluiten
            v_strhl7 := v_strhl7 || kgc_zis.g_eob_char ||
                        kgc_zis.g_term_char;

            -- NtB, 10-02-2009: tijdelijke afvang!!!!?????!!!!!! >>>
            IF g_test
            THEN
              kgc_import.show_output(p_tekst => v_strhl7);
            END IF;
            -- <<<

            -- NtB: evt. de mogelijk betere generieke kgc_zis.send_msg
            --      versie gebruiken; deze opent, verzend en sluit wel in 1x!?
            v_ok := send_msg(p_strhl7in  => v_strhl7
                            ,p_timeout   => 150 -- 15 seconden
                            ,x_strhl7out => v_strantw
                            ,x_msg       => v_msg);

            IF NOT v_ok
            THEN
              v_ok      := FALSE;
              v_msg     := 'Verwerking NIET OK op ' ||
                           to_char(SYSDATE
                                  ,'dd-mm-yyyy hh24:mi:ss') || ': ' ||
                           v_msg;
              x_verslag := x_verslag || v_msg; -- NtB, 12-03-2009: nw
              -- connectie sluiten  gwe rework mantis 3047 08-03-2009 sluit verbinding toegevoegd
              IF p_uitslagen_verzenden
              THEN
                sluit_verbinding;
              END IF;
              RETURN;
            END IF;

            -- NtB: tbv. ONTW- en FST-tests uitschakelen (server bestaat niet)>>>
            IF NOT g_test
            THEN
              kgc_zis.log(v_strantw
                         ,'1100XXXXXX');
              v_hl7_msg_res := kgc_zis.str2hl7msg(0
                                                 ,v_strantw);
              v_msa         := kgc_zis.getsegmentcode('MSA'
                                                     ,v_hl7_msg_res);
              v_msh         := kgc_zis.getsegmentcode('MSH'
                                                     ,v_hl7_msg_res);

              IF nvl(length(v_msa)
                    ,0) = 0
                 OR nvl(length(v_msh)
                       ,0) = 0
              THEN
                v_ok      := FALSE;
                v_msg     := 'MSA- of MSH-segment afwezig!';
                x_verslag := x_verslag || v_msg; -- NtB, 12-03-2009: nw
                -- connectie sluiten  gwe rework mantis 3047 08-03-2009 sluit verbinding toegevoegd
                IF p_uitslagen_verzenden
                THEN
                  sluit_verbinding;
                END IF;
                RETURN;
              END IF;

              -- check op control-id; soms wordt er iets aan vastgeplakt (=niet erg)
              /* -- NtB: nav. aanpassingen RDE, niet correct
              IF substr(v_hl7_msg_res(v_msh) (10) (1) (1)
                       ,1
                       ,length(v_msg_control_id)) <> v_msg_control_id
              THEN
                 -- komt 'nooit' voor
                 v_ok  := FALSE;
                 v_msg := 'Fout bij ophalen gegevens: mismatch control-id! Verzonden: ' || v_msg_control_id ||
                          '; Ontvangen: ' || v_hl7_msg_res(v_msh) (10) (1) (1) || '!';
                 RETURN;
              END IF;
              */
              IF nvl(v_hl7_msg_res(v_msa) (1) (1) (1)
                    ,'XX') NOT IN ('AA'
                                  ,'CA')
              THEN
                v_ok      := FALSE;
                v_msg     := 'MSA-1 = ' || v_hl7_msg_res(v_msa) (1) (1)
                             (1) || '; Text: ' || v_hl7_msg_res(v_msa) (3) (1) (1);
                x_verslag := x_verslag || v_msg; -- NtB, 12-03-2009: nw
                -- connectie sluiten  gwe rework mantis 3047 08-03-2009 sluit verbinding toegevoegd
                IF p_uitslagen_verzenden
                THEN
                  sluit_verbinding;
                END IF;
                RETURN;
              END IF;
            END IF; -- <<<

            -- brief registreren: electronische verzending naar aanvrager: 'UITS_HL7' of voorlopig
            -- NtB: nav. aanpassingen RDE, 28-02-2008, voorlopige uitslagen mogen niet meer verzonden worden
            --      Wordt dat geregeld in Scherm KGC_HL710???
            -- v_brty_id := NULL;
            -- IF p_definitieve_uitslagen = 'J'
            -- THEN
            v_brty_id := kgc_uk2pk.brty_id('UITS_HL7');
            -- ELSE
            --    v_brty_id := kgc_uk2pk.brty_id('UITS_V_HL7');
            -- END IF;

            IF v_brty_id IS NOT NULL
            THEN
              OPEN c_ramo(v_brty_id);
              FETCH c_ramo
                INTO v_ramo_id;
              CLOSE c_ramo;

              kgc_brie_00.registreer(p_datum         => SYSDATE
                                    ,p_kopie         => 'N'
                                    ,p_brieftype     => 'UITSLAG' -- NtB: ipv. null
                                    ,p_brty_id       => v_brty_id
                                    ,p_pers_id       => r_uits.pers_id
                                    ,p_kafd_id       => r_uits.kafd_id
                                    ,p_onde_id       => r_uits.onde_id
                                    ,p_rela_id       => r_uits.rela_id
                                    ,p_uits_id       => r_uits.uits_id
                                    ,p_geadresseerde => r_uits.geadresseerde
                                    ,p_ongr_id       => r_uits.ongr_id
                                    ,p_taal_id       => r_uits.taal_id
                                    ,p_destype       => 'PREVIEW'
                                    ,p_best_id       => NULL
                                    ,p_ind_commit    => 'N' -- NtB, 12-03-2009: was 'J', commit-moment komt later
                                    ,p_ramo_id       => v_ramo_id);
            END IF;

            -- notitie registreren met code 'MOSOS'
            -- test: hl7bericht
            -- rde 15-9-2009: aanpassing om bericht ingekort op te slaan; mantis 3047.
            IF nvl(length(v_strhl7)
                  ,0) > 2000
            THEN
              v_omschrijving := substr(v_strhl7
                                      ,1
                                      ,1000) ||
                                ' *** de tekst was te lang - hier zijn ' ||
                                to_char(length(v_strhl7) - 1900) ||
                                ' karakters weggelaten *** ' ||
                                substr(v_strhl7
                                      ,-900);
            ELSE
              v_omschrijving := v_strhl7;
            END IF;
            INSERT INTO kgc_notities
              (entiteit
              ,id
              ,omschrijving
              ,code
              ,commentaar)
            VALUES
              ('UITS'
              ,r_uits.uits_id
              ,v_omschrijving
              ,'MOSOS'
              ,'HL7-bericht naar MOSOS; verzonden op ' ||
               to_char(SYSDATE
                      ,'dd-mm-yyyy hh24:mi:ss'));

            v_ok                  := TRUE;
            x_onde_msg_verz_count := x_onde_msg_verz_count + 1;

            COMMIT; -- NtB, 12-03-2009

          EXCEPTION
            WHEN OTHERS THEN
              -- NtB, 12-03-2009: moet anders nog!!!!?????
              ROLLBACK;

              x_verslag := x_verslag || SQLERRM;

            -- v_strhl7debug := v_strhl7debug || '; ' || SQLERRM;

            -- send_mail_error(p_error_msg => 'Exceptieafhandeling bij aanmaken bericht: ' || SQLERRM
            --                ,p_thisproc  => 'verwerk_ORU_001_bericht');
          END;

        ELSE
          NULL; -- x_verslag := x_verslag || 'Niet verzonden' || chr(10);
          -- v_strhl7debug := 'Niet verzonden';
        END IF;

        -- ***** Lijst
        IF v_status_uitslag = 'F'
        THEN
          v_status_uitslag_ext := 'Definitieve uitslag';
        ELSIF v_status_uitslag = 'P'
        THEN
          v_status_uitslag_ext := 'VOORLOPIGE uitslag';
        ELSE
          v_status_uitslag_ext := NULL;
        END IF;
      END LOOP; -- c_imon

      IF v_onde_msg_count = 0 -- NtB: nw
      THEN
        x_verslag := substr(rpad(to_char(r_onde.onderzoeknr)
                                ,18) ||
                            'Bij dit onderzoek kon geen bijbehorend MOSOS-bericht gevonden worden! Meld dit handmatig terug!'
                           ,1
                           ,255); -- NtB: nw
      END IF;

    END LOOP; -- c_onde

    /* NtB: evt. generaliseren, de generieke kgc_zis.send_msg versie gebruiken
    **      deze opent, verzend en sluit wel in 1x!? */
    -- connectie sluiten
    IF p_uitslagen_verzenden
    THEN
      sluit_verbinding;
    END IF;
    /**/

    IF v_onde_count = 0 -- NtB: nw
    THEN
      x_verslag := 'Er zijn geen onderzoeken gevonden voor de opgegeven selectiecriteria! '; -- NtB: nw
    ELSE
      -- x_verslag := 'Aantal onderzoeken: ' || to_char(v_onde_count); -- NtB: nw
      x_status := 'V'; -- Goed verwerkt
    END IF;

  EXCEPTION

    WHEN OTHERS THEN
      -- NtB: nw, moet anders nog >>>
      x_verslag := x_verslag ||
                   ' Fout in verwerking uitslagen CYTO-Prenataal! ' ||
                   chr(10);
      x_verslag := x_verslag || 'SQLERRM: ' || SQLERRM || chr(10);
      v_msg     := cg$errors.geterrors;
      IF nvl(length(v_msg)
            ,0) > 0
      THEN
        x_verslag := x_verslag || 'cg$errors: ' || v_msg || chr(10);
      END IF;
      -- <<<

      /* NtB: evt. generaliseren, de generieke kgc_zis.send_msg versie gebruiken
      **      deze opent, verzend en sluit wel in 1x!? */
      -- is verbinding open ??
      IF p_uitslagen_verzenden
      THEN
        sluit_verbinding;
      END IF;
      /**/

    /* NtB: nieuw fouten nog ergens loggen of gebeurt dit in scherm, bijv.:
    qms$errors.unhandled_exception(p_procname  => g_this_package || '.' || g_this_procedure
                                  ,p_errorcode => SQLCODE
                                  ,p_errmsg    => SQLERRM);
    */
  END mosos_result_bericht;
  /*** einde naar MOSOS *****************************************************************************/
BEGIN
  -- systeem parameters initieeren
  kgc_zis.g_sypa_hl7_versie := kgc_sypa_00.systeem_waarde('HL7_VERSIE');
  g_sypa_zis_locatie        := kgc_sypa_00.systeem_waarde('ZIS_LOCATIE');
  g_sypa_mosos_servernaam   := kgc_sypa_00.systeem_waarde('MOSOS_SERVERNAAM');
  g_sypa_mosos_serverpoort  := kgc_sypa_00.systeem_waarde('MOSOS_SERVERPOORT');
  g_database_name           := upper(sys.database_name);
  g_imon_referentie         := NULL;
  g_imon_omschrijving       := NULL;

END kgc_interface_mosos;
/

/
QUIT
