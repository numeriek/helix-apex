CREATE OR REPLACE PACKAGE "HELIX"."KGC_RAPP_01" IS
  --  Where-clause voor BASMEET01C, ook gebruikt in BASMEET53
  FUNCTION meet01c_bmfr
  ( p_view_name               IN  VARCHAR2  DEFAULT 'BMFR'
  , p_werklijst               IN  VARCHAR2  DEFAULT NULL
  , p_stgr_code               IN  VARCHAR2  DEFAULT NULL
  , p_mate_code               IN  VARCHAR2  DEFAULT NULL
  , p_meting_afgerond         IN  VARCHAR2  DEFAULT 'N'
  , p_datum_aanmelding_vanaf  IN  DATE      DEFAULT NULL
  , p_datum_aanmelding_tm     IN  DATE      DEFAULT NULL
  , p_kafd_id                 IN  NUMBER    DEFAULT NULL
  , p_meti_id                 IN  NUMBER    DEFAULT NULL
  , p_frac_id                 IN  NUMBER    DEFAULT NULL
  , p_inclusief_contr_mons    IN  VARCHAR2  DEFAULT 'J'
  ) RETURN VARCHAR2;
END KGC_RAPP_01;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_RAPP_01" IS
-- PL/SQL Block
function where_zoekcriterium
( p_waarde in varchar2
, p_exact in varchar2 := 'N'
, p_datatype in varchar2 := 'C' -- 'N', 'D'
, p_operator in varchar2 := null
)
return varchar2
is
  v_return varchar2(1000);
  v_waarde varchar2(1000) := p_waarde;
  v_operator varchar2(10) := p_operator;
  v_dummy_datum date;
begin
  if ( p_waarde is null )
  then
    return( null );
  end if;

  if ( p_exact = 'N'
   and p_datatype = 'C'
     )
  then
    if ( p_operator = '=' )
    then
      v_waarde := v_waarde;
    else
      v_waarde := '%'||v_waarde||'%';
    end if;
  elsif ( p_datatype = 'D' )
  then
    v_dummy_datum := trunc( to_date( v_waarde ) );
    v_waarde := to_char( v_dummy_datum, 'dd-mm-yyyy' );
  end if;

  if ( v_operator is null )
  then
    if ( instr( v_waarde, '%' ) = 0
     and instr( v_waarde, '_' ) = 0
       )
    then
      v_operator := ' = ';
    else
      v_operator := ' like ';
    end if;
  end if;

  if ( p_datatype = 'N' )
  then
    v_waarde := v_waarde; -- onveranderd
  elsif ( p_datatype = 'D' )
  then
    v_waarde := 'to_date('''||v_waarde||''',''dd-mm-yyyy'')';
  else
    v_waarde := ''''||REPLACE(v_waarde,'''','''''')||'''';
  end if;

  v_return := ' '||v_operator||v_waarde;
	return( v_return );

end where_zoekcriterium;

  --  =================================================================================
  --  PUBLIC PROCEDURE
  --
  FUNCTION meet01c_bmfr
  ( p_view_name               IN  VARCHAR2  DEFAULT 'BMFR'
  , p_werklijst               IN  VARCHAR2  DEFAULT NULL
  , p_stgr_code               IN  VARCHAR2  DEFAULT NULL
  , p_mate_code               IN  VARCHAR2  DEFAULT NULL
  , p_meting_afgerond         IN  VARCHAR2  DEFAULT 'N'
  , p_datum_aanmelding_vanaf  IN  DATE      DEFAULT NULL
  , p_datum_aanmelding_tm     IN  DATE      DEFAULT NULL
  , p_kafd_id                 IN  NUMBER    DEFAULT NULL
  , p_meti_id                 IN  NUMBER    DEFAULT NULL
  , p_frac_id                 IN  NUMBER    DEFAULT NULL
  , p_inclusief_contr_mons    IN  VARCHAR2  DEFAULT 'J'
  ) RETURN VARCHAR2
  IS
    sub_where  VARCHAR2(4000) := NULL;
    def_where  VARCHAR2(4000) := NULL;
    r_string   VARCHAR2(2000) := NULL;
    v_exact    VARCHAR2(1)    := 'J';
    v_view     VARCHAR2(30)   := p_view_name;
    -- hulpvariabelen om datumformaat mee te geven in returnstring.
    v_datum_vanaf VARCHAR(100) := NULL;
    v_datum_tm    VARCHAR(100) := NULL;
    FUNCTION add_and
    ( p_where IN VARCHAR2
    ) RETURN VARCHAR2 IS
    BEGIN
      IF NVL(LENGTH(P_WHERE),0) != 0
      THEN
        RETURN( P_WHERE || ' and ');
      ELSE
        RETURN( P_WHERE );
      END IF;
    END;

  BEGIN
  	IF  v_view IS NOT NULL
    AND substr( v_view, -1, 1 ) <> '.'
    THEN
      v_view := v_view||'.';
    END IF;

    def_where := add_and( def_where ) || '('||v_view||'KAFD_ID = '||NVL(p_kafd_id,-9999999999)||')';

    IF  p_meti_id IS NOT NULL
    THEN
      def_where := add_and( def_where ) || '('||v_view||'METI_ID = '||p_meti_id||')';
    END IF;

    IF  p_frac_id IS NOT NULL
    THEN
      def_where := add_and( def_where ) || '('||v_view||'FRAC_ID = '||p_frac_id||')';
    END IF;

    IF p_werklijst IS NOT NULL
    THEN
      sub_where := NULL;
      r_string := p_werklijst;
      sub_where := add_and( sub_where ) || where_zoekcriterium(p_waarde=>r_string,p_exact=>v_exact);
      def_where := add_and( def_where )
                || '('||v_view||'meti_id in '
		        		|| ' ( select wlit.meti_id from kgc_werklijst_items wlit, kgc_werklijsten wlst '
                ||   ' where wlit.wlst_id = wlst.wlst_id'
                ||   '   and wlst.werklijst_nummer'||sub_where
                || '))';
    END IF;

    IF p_meting_afgerond IN ( 'J', 'N' )
    THEN
      def_where := add_and( def_where ) || '('||v_view||'METI_AFGEROND = '''||p_meting_afgerond||''')';
    END IF;

    IF p_datum_aanmelding_vanaf IS NOT NULL
    THEN
      v_datum_vanaf := ' to_date('''||to_char( p_datum_aanmelding_vanaf, 'dd-mm-yyyy')||''',''dd-mm-yyyy'')'  ;
      def_where := add_and( def_where ) || '(TRUNC('||v_view||'METI_DATUM_AANMELDING) >= '|| v_datum_vanaf||')';
    END IF;

    IF p_datum_aanmelding_tm IS NOT NULL
    THEN
      v_datum_tm := ' to_date('''||to_char( p_datum_aanmelding_tm, 'dd-mm-yyyy')||''',''dd-mm-yyyy'')'  ;
      def_where := add_and( def_where ) || '(TRUNC('||v_view||'METI_DATUM_AANMELDING) <= '||v_datum_tm||')';
    END IF;

    sub_where := NULL;
    r_string := p_stgr_code;
    sub_where := add_and( sub_where ) || where_zoekcriterium(p_waarde=>r_string,p_exact=>v_exact);
    IF (sub_where IS NOT NULL) THEN
      def_where := add_and( def_where ) || '(STGR_CODE'||sub_where||')';
    END IF;

    sub_where := NULL;
    r_string := p_mate_code;
    sub_where := add_and( sub_where ) || where_zoekcriterium(p_waarde=>r_string,p_exact=>v_exact);
    IF (sub_where IS NOT NULL)
    THEN
      def_where := add_and( def_where ) || '(MATE_CODE'||sub_where||')';
    END IF;

    IF p_inclusief_contr_mons = 'N'
    THEN
      def_where := add_and( def_where ) || '('||v_view||'SOORT_MONSTER = ''O'')'; -- alleen onderzoeksmonsters
    END IF;

    RETURN def_where;

  END meet01c_bmfr;
END kgc_rapp_01;
/

/
QUIT
