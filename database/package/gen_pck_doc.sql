CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_DOC"
AS
   /*===========================================================================
   Package Name : gen_pck_doc
   Usage        : This package contains all procedures and functions on
                  table GEN_DOCUMENTS.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
  23-12-2016  The DOC      Creation
   =============================================================================
   */
   PROCEDURE p_del_document(i_doc_id IN gen_documents.id%TYPE);
   PROCEDURE p_open_document(i_document_id IN NUMBER);
   FUNCTION f_upload_document(
      i_document_name      IN gen_documents.document_name%TYPE
     ,i_document_category  IN gen_documents.document_category%TYPE
     ,i_application_id     IN NUMBER)
      RETURN gen_documents.id%TYPE;
END gen_pck_doc;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_DOC"
AS

   /*===========================================================================
   Package Name : gen_pck_doc
   Usage        : This package contains all procedures and functions on
                  table GEN_DOCUMENTS.
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
  23-12-2016  The DOC      Creation
   =============================================================================
   */

   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   --------------------------------------------------------------
   -- procedure to delete a document from GEN_DOCUMENTS table.
   --------------------------------------------------------------
   PROCEDURE p_del_document(i_doc_id IN gen_documents.id%TYPE)
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'p_del_document';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      DELETE FROM gen_documents
            WHERE id = i_doc_id;
      gen_pck_exception.p_json_error('');
      logger.log_information(p_text => 'END'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         gen_pck_exception.p_json_error(gen_pck_exception.f_get_err_message(
                                         SQLERRM
                                        ,v('LANGCODE')
                                        ,'N'
                                        ,'N'));
   END p_del_document;
   --
   -- ------------------------------------------------------------------------
   -- Procedure voor het openen van een document behorende bij een toelichting
   -- ------------------------------------------------------------------------
   PROCEDURE p_open_document(i_document_id IN NUMBER)
   IS
      l_document_locatie    VARCHAR2(100);
      l_document_categorie  VARCHAR2(100);
      l_soort_document      VARCHAR2(100);
      --l_categorie           VARCHAR2(100);
      l_length              NUMBER;
      l_length_clob         NUMBER;
      l_filename            VARCHAR2(2000);
      l_mimetype            gen_documents.mimetype%TYPE;
      l_blob                BLOB DEFAULT EMPTY_BLOB();
      l_clob                CLOB;
   BEGIN
      SELECT doc.document document
            ,dbms_lob.getlength(doc.document) document_length
            ,doc.mimetype
            ,doc.document_name
        INTO l_blob
            ,l_length
            ,l_mimetype
            ,l_filename
        FROM gen_documents doc
       WHERE doc.id = i_document_id;
      -- set up HTTP header
      -- use an NVL around the mime type and
      -- if it is a null set it to application/octect
      -- application/octect may launch a download window from windows
      owa_util.mime_header(COALESCE(l_mimetype
                                   ,'application/octet')
                          ,FALSE);
      -- set the size so the browser knows how much to download
      -- close the headers
      owa_util.http_header_close;
      -- download the BLOB
      wpg_docload.download_file(l_blob);
   END p_open_document;
   FUNCTION f_upload_document(
      i_document_name      IN gen_documents.document_name%TYPE
     ,i_document_category  IN gen_documents.document_category%TYPE
     ,i_application_id     IN NUMBER)
      RETURN gen_documents.id%TYPE
   IS
      l_scope  CONSTANT VARCHAR2(61) := g_package || 'f_upload_document';
      CURSOR c_temp_doc
      IS
         SELECT filename
               ,blob_content
               ,mime_type
           FROM apex_application_temp_files
          WHERE name = i_document_name
            AND application_id = i_application_id;
      r_temp_doc        c_temp_doc%ROWTYPE;
      l_id              NUMBER;
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      OPEN c_temp_doc;
      FETCH c_temp_doc
         INTO r_temp_doc;
      CLOSE c_temp_doc;
      IF r_temp_doc.filename IS NOT NULL
      THEN
         INSERT INTO gen_documents(document_category
                                  ,document_name
                                  ,document
                                  ,mimetype
                                  ,filename)
              VALUES (i_document_category
                     ,i_document_name
                     ,r_temp_doc.blob_content
                     ,r_temp_doc.mime_type
                     ,r_temp_doc.filename)
           RETURNING id
                INTO l_id;
         DELETE FROM apex_application_temp_files
               WHERE name = i_document_name
                 AND application_id = i_application_id;
      END IF;
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
      RETURN l_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => SQLERRM
                         ,p_scope => l_scope);
         RETURN NULL;
   END;
END gen_pck_doc;
/

/
QUIT
