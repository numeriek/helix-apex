CREATE OR REPLACE PACKAGE "HELIX"."CHECK_JOBS" IS
PROCEDURE post_mail
( SENDER   IN VARCHAR2
, RECEIVER IN VARCHAR2
, SUBJECT  IN VARCHAR2
, MESSAGE  IN VARCHAR2
);
PROCEDURE check_job;
END check_jobs;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."CHECK_JOBS" IS
PROCEDURE post_mail
( SENDER   IN VARCHAR2
, RECEIVER IN VARCHAR2
, SUBJECT  IN VARCHAR2
, MESSAGE  IN VARCHAR2
)
IS
  SendorAddress Varchar2(80);
  ReceiverAddress varchar2(80);
  EmailServer varchar2(30) := 'smtp.umcn.nl';
  Port number := 25;
  conn UTL_SMTP.CONNECTION;
  crlf VARCHAR2( 2 ):= CHR( 13 ) || CHR( 10 );
  mesg VARCHAR2( 4000 );
  mesg_body varchar2(4000);
BEGIN
  conn:= utl_smtp.open_connection( EmailServer, Port );
  utl_smtp.helo( conn, EmailServer );
  utl_smtp.mail( conn, SENDER);
  utl_smtp.rcpt( conn, RECEIVER );
  mesg:= 'Subject: ' || SUBJECT || crlf ||
    'Date: '||TO_CHAR( SYSDATE, 'Dy, dd Mon yyyy hh24:mi:ss' )||
    crlf ||
    'From:'||SENDER|| crlf ||
    'To: '||RECEIVER || crlf ||
    '' || crlf || MESSAGE ;
  utl_smtp.data( conn, mesg );
  utl_smtp.quit( conn );
END;
PROCEDURE check_job
IS
  v_opmerkingen  VARCHAR2(32000);
  CURSOR jobs_cur
  IS
    SELECT *
    FROM   all_jobs
    WHERE  log_user = 'HELIX'
    AND    INSTR(LOWER(what), 'verwerk_bestanden') > 0
    AND    last_date < sysdate - (1 / 24)
    AND    next_date IS NOT NULL -- dwz: alleen de actieve jobs selecteren!
    ;
  jobs_rec jobs_cur%ROWTYPE;
  v_newline VARCHAR2(10) := CHR(10);
  v_jobno  NUMBER;
  v_afzender VARCHAR2(100) := 'cukz070@umcn.nl';
  v_ontvanger VARCHAR2(100) := 'cukz070@umcn.nl';
BEGIN
  -- controleren of de job al een uur draait!
  open jobs_cur;
  fetch jobs_cur into jobs_rec;
  IF jobs_cur%NOTFOUND THEN
    close jobs_cur;
    RETURN; -- klaar!
  END IF;
  close jobs_cur;
  -- verwijder job
  DBMS_JOB.REMOVE(jobs_rec.job);
  -- voeg nieuwe job toe
  DBMS_JOB.SUBMIT
  ( job  => v_jobno
  , what => jobs_rec.what
  , next_date => SYSDATE
  , interval => jobs_rec.interval
  );
  commit;
  -- melding
  v_opmerkingen := 'Job verwerkbestanden zat vast!' || v_newline ||
    'De job is verwijderd en opnieuw opgestart' || v_newline ||
    'Op de machine van de database (b.v. flamingo), ' || v_newline ||
    'directory ...ora...\admin\prod(test)\bdump staan tracefiles: kijk wat er mis is!' || v_newline || v_newline ||
    'Groeten, ' || v_newline ||
    'Helix.Check_jobs.check_job.' ;
  post_mail(v_afzender, v_ontvanger, 'Oracle Job alert!', v_opmerkingen);
EXCEPTION
  WHEN OTHERS
  THEN
    v_opmerkingen := SQLCODE;
    v_opmerkingen := v_opmerkingen || ' ' || SUBSTR(SQLERRM, 1, 1800);
    post_mail(v_afzender, v_ontvanger, 'Oracle Job check_job error!', v_opmerkingen);
END check_job;
END check_jobs;
/

/
QUIT
