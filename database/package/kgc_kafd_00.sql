CREATE OR REPLACE PACKAGE "HELIX"."KGC_KAFD_00" IS
-- PL/SQL Specification
-- geef kafd_id van een afdeling
FUNCTION id
( p_afdeling IN VARCHAR2
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( id, WNDS, WNPS, RNPS );

FUNCTION eigen_afdeling
( p_kafd_id IN NUMBER
, p_user IN VARCHAR2 := NULL
)
RETURN BOOLEAN;
PRAGMA RESTRICT_REFERENCES( eigen_afdeling, WNDS, WNPS, RNPS );

-- Haal de standaard afdeling op
-- Zoek eerst in de systeemparameters
-- anders de kgc-afdeling, als er maar een is!
FUNCTION  default_afdeling
RETURN  VARCHAR2;
PROCEDURE  default_afdeling
( x_kafd_id OUT NUMBER
, x_kafd_code OUT VARCHAR2
, x_kafd_naam OUT VARCHAR2
);


END KGC_KAFD_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_KAFD_00" IS
FUNCTION id
( p_afdeling IN VARCHAR2
)
RETURN NUMBER
IS
  CURSOR kafd_cur
  IS
    SELECT kafd_id
    FROM   kgc_kgc_afdelingen
    WHERE  code = UPPER( p_afdeling )
    ;
  v_id NUMBER;
-- PL/SQL Block
BEGIN
  OPEN  kafd_cur;
  FETCH kafd_cur
  INTO  v_id;
  CLOSE kafd_cur;
  RETURN( v_id );
END id;

FUNCTION eigen_afdeling
( p_kafd_id IN NUMBER
, p_user IN VARCHAR2 := NULL
)
RETURN BOOLEAN
IS
  v_return BOOLEAN := FALSE;
  CURSOR c
  IS
    SELECT NULL
    FROM   kgc_mede_kafd
    WHERE  kafd_id = p_kafd_id
    AND    mede_id = kgc_mede_00.medewerker_id( p_user )
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  c;
  FETCH c
  INTO  v_dummy;
  v_return := c%found;
  CLOSE c;
  RETURN( v_return );
END eigen_afdeling;

FUNCTION  default_afdeling
RETURN  VARCHAR2
IS
  v_return_waarde VARCHAR2(10) := NULL;
  CURSOR kafd_cur
  IS
    SELECT kafd.code
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_mede_kafd meka
    WHERE  meka.kafd_id = kafd.kafd_id
    AND    meka.mede_id = kgc_mede_00.medewerker_id
    ;
  kafd_rec kafd_cur%rowtype;
BEGIN
  v_return_waarde := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'MEDEWERKER_AFDELING'
                     , p_kafd_id => NULL
                     );
  IF ( v_return_waarde IS NULL )
  THEN
    OPEN  kafd_cur;
    FETCH kafd_cur
    INTO  kafd_rec;
    IF ( kafd_cur%found )
    THEN
      v_return_waarde := kafd_rec.code;
      -- is er een tweede?
      FETCH kafd_cur
      INTO  kafd_rec;
      IF ( kafd_cur%found )
      THEN
        v_return_waarde := NULL;
      END IF;
    END IF;
    IF ( kafd_cur%isopen )
    THEN
      CLOSE kafd_cur;
    END IF;
  END IF;
  RETURN( v_return_waarde );
END default_afdeling;

PROCEDURE  default_afdeling
(  x_kafd_id  OUT  NUMBER
,  x_kafd_code  OUT  VARCHAR2
,  x_kafd_naam  OUT  VARCHAR2
)
 IS
  CURSOR kafd_cur
  IS
    SELECT kafd_id
    ,      naam
    FROM   kgc_kgc_afdelingen
    WHERE  code = x_kafd_code
    ;
  kafd_rec kafd_cur%rowtype;
 BEGIN
  x_kafd_code := default_afdeling;
  IF ( x_kafd_code IS NOT NULL )
  THEN
    OPEN  kafd_cur;
    FETCH kafd_cur
    INTO  x_kafd_id
    ,     x_kafd_naam;
    CLOSE kafd_cur;
  END IF;
END default_afdeling;

END kgc_kafd_00;
/

/
QUIT
