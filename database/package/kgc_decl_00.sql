CREATE OR REPLACE PACKAGE "HELIX"."KGC_DECL_00" IS
/*
Change History:

07-06-2015  Athar Shaikh        Mantis 3957: Code Fields increase in Helix up to 25 and the family survey number increase to 20
10-12-2014  Rob Debets          Mantis 3338: lengte kgc_declaratie_criteria.waarde naar 4000
01-09-2010  Jan Kokx            Mantis 1193 nieuwe function: overgezet_via_DFT
                                Mantis 1193 function declarabel(1): p_rnde_id toegevoegd
                                Mantis 1193 procedure vul_reden_niet_declarabel toegevoegd
31-08-2010  Gerrit Wiskerke     Mantis 1268 alternatief persoon t.b.v. declaraties
28-11-2008  Nico ter Burg       EFIX Mantis 2405 Procedure wijzig_declaratie aangepast:
                                deen_id ook opnieuw zetten, indien p_soort_gegevens is 'PERS'.
31-05-2007  Taeke Timmermans    Mantis 706  Indicatie_tektsten.code geactiveerd
08-10-2007  Peter Heddes        Mantis 746 procedure zet_actuele_decl_id van local naar global
10-10-2007  Peter Heddes        Mantis 1189 In functie deen_id de volgorde van indi_code en ingr_code van cursor onbe_cur omgedraaid.
            					extra voorwaarde toegevoegd in cursor indi_cur van functie deen_id
06-11-2007  Jan Kokx            Mantis 1102 Procedure declarabel aangepast: afleiding ongr_code toegevoegd.

*/


/* Werkwijze in situatie:
+ Nieuw criterium erbij?
- record-type in specificatie uitbreiden
- function declarabel (p_criteria) in body uitbreiden
- procedure check_criterium in body uitbreiden
- procedure criterium_omzetten in body uitbreiden
- criterium opnemen waar kgc_decl_00.declarabel wordt aangeroepen (forms/db/triggers/...)
- deen_id (entiteit/id): cursor(en) uitbreiden

+ ....
- ...
*/

-- verzameling van alle criteria die bij de declaratie-bepaling kunnen worden gebruikt
TYPE declareer_criteria_rec IS RECORD
( entiteit VARCHAR2(30)
, kafd_id NUMBER
, ongr_id NUMBER
, pers_id NUMBER
, rela_id NUMBER
, onde_id NUMBER
, onbe_id NUMBER
, mons_id NUMBER
, meti_id NUMBER
, gesp_id NUMBER
, kafd_code VARCHAR2(10)
, ongr_code VARCHAR2(10)
, stgr_code VARCHAR2(25) -- from 10 to 25 changed by athar for the Mantis-3957 on 07-06-2015
, stof_code VARCHAR2(25) -- from 10 to 25 changed by athar for the Mantis-3957 on 07-06-2015
, frty_code VARCHAR2(10)
, herk_code VARCHAR2(10)
, ingr_code VARCHAR2(10)
, indi_code VARCHAR2(25) -- from 10 to 25 changed by athar for the Mantis-3957 on 07-06-2015
, mate_code VARCHAR2(10)
, gety_code VARCHAR2(10)
, datum DATE
, onderzoekstype VARCHAR2(1)
, onderzoekswijze VARCHAR2(10)
, onde_declareren VARCHAR2(1)
, onde_afgerond VARCHAR2(1)
, onde_omschrijving VARCHAR2(2000)
, frac_controle VARCHAR2(1)
, pers_land VARCHAR2(30)
, rela_land VARCHAR2(30)
);
TYPE  declareer_criteria_tab_type  IS  TABLE  OF  declareer_criteria_rec
INDEX  BY  BINARY_INTEGER;
declareer_criteria_tab  declareer_criteria_tab_type;

procedure toon_verwerkt;

-- slechts een beperkt aantal criteria kan worden gebruikt (zie record definitie hierboven)
-- retourneer de omschrijving bij een id.
PROCEDURE check_criterium
( p_tabelnaam IN VARCHAR2 := NULL
, p_kolomnaam IN VARCHAR2 := NULL
);
/* Overbodig???
FUNCTION  check_criterium
( p_tabelnaam IN VARCHAR2 := NULL
, p_kolomnaam IN VARCHAR2 := NULL
, p_id IN NUMBER := NULL
)
RETURN  VARCHAR2;
*/

-- Eigenlijk een lokale functie., maar wel handig bij correctiewerk
-- Retourneert de deen_id van een geschikte declaratie-entiteit (set van criteria)
FUNCTION deen_id
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
)
RETURN  NUMBER;

-- is dit onderdeel (onderzoek, betrokkene, monsterafname, meting, couseling) declarabel
-- voor nieuwe records (default waarde voor declarabel)
FUNCTION declarabel
( p_declareer_criteria IN declareer_criteria_rec
, p_rnde_id            IN NUMBER := NULL
)
RETURN  VARCHAR2;

-- string bestaat uit <CRITERIUM>waarde<\CRITERIUM> voorkomens
FUNCTION declarabel
( p_criteria IN varchar2
)
RETURN  VARCHAR2;
 -- is nieuwe of bestaande rij declarabel

FUNCTION declarabel
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_inclusief_deze_rij IN BOOLEAN := FALSE
)
RETURN  VARCHAR2;

PROCEDURE  bepaal_waarden
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, x_declareren IN OUT VARCHAR2
, x_datum IN OUT DATE
, x_kafd_id IN OUT NUMBER
, x_ongr_id IN OUT NUMBER
, x_pers_id IN OUT NUMBER
, x_deen_id IN OUT NUMBER
, x_dewy_id IN OUT NUMBER
, x_ingr_id IN OUT NUMBER
, x_verz_id IN OUT NUMBER
, x_fare_id IN OUT NUMBER
, x_rnde_id IN OUT NUMBER
, x_betreft_tekst IN OUT VARCHAR2
, x_toelichting IN OUT VARCHAR2
, x_verrichtingcode IN OUT VARCHAR2
, x_nota_adres IN OUT VARCHAR2
, x_verzekeringswijze IN OUT VARCHAR2
, x_verzekeringsnr IN OUT VARCHAR2
, x_pers_id_alt_decl IN OUT NUMBER
);

-- maak (standaard) declaratie voor een van de onderdelen: onderzoek, betrokkene, monsterafname, deelonderzoek/meting
-- als in brontabel (ONDE,ONBE,MONS,METI) declareren = J dan declareren
FUNCTION standaard_declareren
( p_actie IN VARCHAR2 -- I/U/D
, p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_declareren IN VARCHAR2 := NULL
)
RETURN VARCHAR2;

-- Overloaded: procedure wordt aangeroepen in db-triggers!
PROCEDURE standaard_declareren
( p_actie IN VARCHAR2 -- I/U/D
, p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_declareren IN VARCHAR2 := NULL
);

-- wijzigingen in persoon, onderzoek, onderzoek_betrokkene, monster, meting kunnen
-- leiden tot wijzigingen in declaratie
PROCEDURE wijzig_declaratie
( p_decl_trow_nieuw IN OUT kgc_declaraties%rowtype
, p_soort_gegevens IN VARCHAR2 := NULL -- PERS, VERZ, INGR,
);

-- meeste declaraties zijn BInnenland, retourneer dewy_id
FUNCTION binnenland
( p_kafd_id IN NUMBER
)
RETURN NUMBER;
PRAGMA RESTRICT_REFERENCES( binnenland, WNDS, WNPS, RNPS );

-- bepaal machtigingsindicatie voor declaratie van onderzoek of monster
FUNCTION ingr_id
( p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_update_declaratie IN BOOLEAN := FALSE
)
RETURN NUMBER;

-- retourneer de machtigingsindicatie bij een declaratie
FUNCTION machtigingsindicatie
( p_decl_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( machtigingsindicatie, WNDS, WNPS, RNPS );

-- retourneer onderzoeknr of monsternummer als identificatie van een declaratie
-- views voor machtigingen; rapporten KGCONDE51,52,53 KGCINMA51
FUNCTION decl_identificatie
( p_decl_id IN NUMBER
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( decl_identificatie, WNDS, WNPS, RNPS );

-- soms moeten declaraties opnieuw worden bepaald, ook als er geen gegevens zijn gewijzigd
-- zie AUS-triggers op bv. kgc_onderzoeken, bas_metingen, ...
function forceer_declaratiebepaling
return boolean;

-- maak alle declaraties (opnieuw) aan in het kader van een onderzoek:
-- (onderzoek, onderzoek-betrokkene, meting, gesprek)

procedure bepaal_declaraties
( p_onde_id in number := null
, p_mons_id in number := null
);

-- retourneer J/N: moet er voor de declaratie een machtigingsbrief worden gemaakt?
function machtigingsbrief_nodig
( p_decl_id in number
)
return varchar2;

-- schakelaar om debugger aan/uit te zetten
procedure debuggen
( p_aan_uit in varchar2 := null
);
-- ... en uitvragen.
function debug_aan
return boolean;

-- verwerk een afwijkende declaratie (het veld declareren is in een scherm gewijzigd!)
procedure handmatige_declaratie
( p_entiteit in varchar2
, p_id in number
, p_declareren in varchar2
, p_commit in boolean := false
);

-- procedure om globale variabele te vullen (zie tel_aantal)
-- Mantis 746 global procedure van gemaakt
procedure zet_actuele_decl_id
( p_entiteit in varchar2 := null
, p_id in number := null
, p_decl_id in number := null
);

-- functie voor het bepalen van de onderzoek aanvrager behorend bij een declaratie t.b.v. sortering in kgcdecl05
FUNCTION decl_aanvrager
( p_decl_entiteit    IN VARCHAR2
, p_decl_entiteit_id IN NUMBER
)
RETURN VARCHAR2;

PROCEDURE gedeclareerd
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, x_declareren OUT VARCHAR2
, x_status OUT VARCHAR2
, x_datum_verwerking OUT DATE
);

FUNCTION  gedeclareerd
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
)
RETURN   VARCHAR2;

-- functie geeft TRUE als de declaratie is aangeboden aan het externe financiele systeem
function overgezet_via_DFT
( p_entiteit in varchar2
, p_id in number
)
return boolean;
END KGC_DECL_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_DECL_00" IS
g_debug boolean := false;
g_fout varchar2(32767) := null;
-- aanroepen in AUS-triggers op declarabele tabellen gaan alleen af als een betekenisvolle kolom wijzigt
-- om ervoor te zorgen dat de aanroep geforceerd afgaat moet een van onderstaande booleans op true gezet worden
-- bij afhankelijke_declaratie: tijdelijk per entiteit
-- bij bepaal_declaraties: voor de hele uitvoering
g_forceer_tmp_decl_bepaling boolean := FALSE;
g_forceer_decl_bepaling boolean := FALSE;
g_actuele_decl_id number := null; -- bij tellen van gedeclareerde aantal moet actuele rij worden uitgesloten

-- tbv. performance van function machtiging_nodig aangeroepen vanuit kgc_machtigingsbrief_vw
type sypa_macht_nodig_tabtype is table of varchar2(1000) -- sypa-waarde
INDEX BY BINARY_INTEGER; -- ongr_id
sypa_macht_nodig_tab sypa_macht_nodig_tabtype;

type verwerkte_rijen_tabtype is table of varchar2(1)
INDEX  BY  varchar2(15); -- BINARY_INTEGER;
verwerkte_rijen_tab verwerkte_rijen_tabtype;
-- zet verwerkte rij in pl/sql-table
procedure zet_verwerkte_rij
( p_entiteit in varchar2
, p_id in number
)
is
  i varchar2(15) := p_entiteit||lpad(p_id,10,'0');
begin
  verwerkte_rijen_tab(i) := 'J';
end zet_verwerkte_rij;
-- staat verwerkte rij al in pl/sql-table
function is_rij_verwerkt
( p_entiteit in varchar2
, p_id in number
)
return boolean
is
  i varchar2(15) := p_entiteit || lpad( p_id, 10, '0' );
begin
  return( verwerkte_rijen_tab.exists(i) );
end is_rij_verwerkt;
-- verwijder verwerkte rij weer uit pl/sql-table
procedure schoon_verwerkte_rij
( p_entiteit in varchar2
, p_id in number
)
is
  i varchar2(15) := p_entiteit||lpad(p_id,10,'0');
begin
  verwerkte_rijen_tab.delete(i);
end schoon_verwerkte_rij;
-- (debug) maak een lijstje van pl/sql-table
procedure toon_verwerkt
is
  i varchar2(15);
begin
  i := verwerkte_rijen_tab.first;
  while ( i is not null )
  loop
    p.l( 'Verwerkt '||i );
    i := verwerkte_rijen_tab.next(i);
  end loop;
end toon_verwerkt;

-- debug-optie (schakelaar)
procedure debuggen
( p_aan_uit in varchar2 := null
)
is
begin
  if ( upper(p_aan_uit) = 'UIT' )
  then
    g_debug := false;
  elsif ( upper(p_aan_uit) = 'AAN' )
  then
    g_debug := true;
  else
    g_debug := not g_debug;
  end if;
end debuggen;
-- debug-optie (stand uitvragen)
function debug_aan
return boolean
is
begin
  return( g_debug );
end debug_aan;
-- debug-optie (registratie)
procedure meld
( p_tekst in varchar2
)
is
  v_volgnr number;
begin
-- p.l( p_tekst );
  if ( g_debug )
  then
    select max( volgnr )
    into   v_volgnr
    from   kgc_decl_debug
    where  trunc(datum) = trunc(sysdate)
    ;
    if ( v_volgnr is not null )
    then
      v_volgnr := v_volgnr + 1;
    else
      v_volgnr := 1;
    end if;
    insert into kgc_decl_debug
    ( datum
    , volgnr
    , tekst
    )
    values
    ( sysdate
    , v_volgnr
    , SUBSTR(p_tekst, 1, 4000) -- mantis 3338, rde 10-12-2014
    );
  end if;
end meld;

-- bestaande declaratie-rijen kunnen op declareren=N worden gezet
-- door initialisatie (alles opnieuw bepalen) of door gewijzigde criteria of gegevens
-- een aantal declaratie-gegevens kan dan leeg worden gemaakt (is mooier om te zien)
procedure reset_declaratie
( p_decl_id in number
)
is
begin
  if ( p_decl_id is not null )
  then
    update kgc_declaraties decl
    set    decl.declareren = 'N'
    ,      decl.deen_id = null
    ,      decl.dewy_id = null
    ,      decl.verrichtingcode = null
    ,      decl.ingr_id = null
    ,      decl.betreft_tekst = null
    ,      decl.toelichting = null
    where  decl.decl_id = p_decl_id
    and    nvl( decl.status, '-' ) in ( '-', 'T', 'R' ) --  is null
    ;
  end if;
end reset_declaratie;

-- Mantis 1193/JKO: overnemen van rnde_id (alleen mogelijk bij ONBE) naar de declaratie
PROCEDURE vul_reden_niet_declarabel
( p_decl_id in NUMBER
)
IS
  CURSOR decl_cur
  ( b_decl_id IN NUMBER
  )
  IS
    SELECT decl.decl_id
         , decl.entiteit
         , decl.id
    FROM   kgc_declaraties decl
    WHERE  NVL( decl.status, '-' ) IN ( '-', 'T', 'R' )
    AND    decl.entiteit = 'ONBE'
    AND    decl.decl_id = p_decl_id
    ;
  decl_rec decl_cur%ROWTYPE := NULL;

  CURSOR onbe_cur
  ( b_onbe_id IN NUMBER
  )
  IS
    SELECT onbe.onbe_id
         , onbe.rnde_id
    FROM   kgc_onderzoek_betrokkenen onbe
    WHERE  onbe.onbe_id = b_onbe_id
    ;
   onbe_rec onbe_cur%ROWTYPE := NULL;
BEGIN
  IF ( p_decl_id IS NOT NULL )
  THEN
    OPEN  decl_cur( p_decl_id );
    FETCH decl_cur
    INTO  decl_rec;
    CLOSE decl_cur;
    IF decl_rec.decl_id IS NOT NULL -- wijzigbare declaratie van type ONBE
    THEN
      OPEN  onbe_cur( decl_rec.id );
      FETCH onbe_cur
      INTO  onbe_rec;
      CLOSE onbe_cur;
      IF onbe_rec.onbe_id IS NOT NULL -- ONBE-rij bestaat
      THEN
        UPDATE kgc_declaraties decl
        SET    decl.rnde_id = onbe_rec.rnde_id
        WHERE  NVL( decl.rnde_id, -9 ) <> NVL( onbe_rec.rnde_id, -9 )
        AND    decl.decl_id = decl_rec.decl_id
        ;
      END IF;
    END IF;
  END IF;
END vul_reden_niet_declarabel;

-- procedure om globale variabele te vullen (zie tel_aantal)
procedure zet_actuele_decl_id
( p_entiteit in varchar2 := null
, p_id in number := null
, p_decl_id in number := null
)
is
  cursor decl_cur
  is
    select decl_id
    from   kgc_declaraties
    where  entiteit = p_entiteit
    and    id = p_id
    and    declareren = 'J'
    order by decode( status -- meer dan 1 declaratie per entiteit-record?
                   , 'V', 1
                   , 'X', 2
                   , 'T', 3
                   , 'R', 4
                   , 'H', 5
                   , null, 6
                   , 9
                   )
    ,        decl_id
    ;
begin
  if ( p_decl_id is not null )
  then
    g_actuele_decl_id := p_decl_id;
  else
    g_actuele_decl_id := null;
    if ( p_entiteit is not null
     and p_id is not null
       )
    then
      open  decl_cur;
      fetch decl_cur
      into  g_actuele_decl_id;
      close decl_cur;
    end if;
  end if;
end zet_actuele_decl_id;

-- lokale functie om aantal declaraties te bepalen
-- voor deze persoon en afdeling en ... tot de peildatum
-- mogelijkheden (altijd per persoon en afdeling)
-- DEEN: aantal declaraties op basis van een decl-entiteit
-- INGR: aantal declaraties op basis van een machtigingsindicatie
-- ONWY: aantal declaraties op basis van een onderzoekswijze
-- JAAR: aantal declaraties in een jaar
-- combinaties zijn mogelijk
FUNCTION  tel_aantal
( p_entiteit IN VARCHAR2
, p_pers_id IN NUMBER
, p_kafd_id IN NUMBER
, p_peildatum in date
, p_deen_id in NUMBER := null
, p_ingr_code IN varchar2 := null
, p_onderzoekswijze IN varchar2 := null
, p_onde_id in number
, p_jaar IN VARCHAR2 := NULL
)
RETURN  NUMBER
IS
  v_aantal number := 0;
  v_statement varchar2(4000);
BEGIN
meld( 'actueel='||g_actuele_decl_id||'<<' );
  v_statement := 'select to_char(count(decl.decl_id)) from kgc_declaraties decl'
     ||chr(10)|| 'where decl.entiteit = '''||p_entiteit||''''
     ||chr(10)|| 'and decl.kafd_id = '||to_char(nvl( p_kafd_id, -9999999999 ))
     ||chr(10)|| 'and decl.pers_id = '||to_char(nvl( p_pers_id, -9999999999 ))
     ||chr(10)|| 'and decl.declareren = ''J'''
     ||chr(10)|| 'and trunc(decl.datum) <= to_date('''||to_char(p_peildatum,'dd-mm-yyyy')||''',''dd-mm-yyyy'' )'
     ||chr(10)|| 'and decl.decl_id != '||to_char(nvl( g_actuele_decl_id, -9999999999 ))
               ;
  if ( p_deen_id is not null )
  then
    v_statement := v_statement
       ||chr(10)|| 'and decl.deen_id = '||to_char(p_deen_id)
                 ;
  end if;
  if ( p_ingr_code is not null )
  then
    v_statement := v_statement
       ||chr(10)|| 'and decl.ingr_id in (select ingr.ingr_id from kgc_indicatie_groepen ingr'
                || ' where ingr.code = '''||p_ingr_code||''')'
                 ;
  end if;
  if ( p_onderzoekswijze is not null )
  then
    if ( p_entiteit = 'ONDE' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and '''||p_onderzoekswijze||''' in'
         ||chr(10)|| '(select onde.onderzoekswijze from kgc_onderzoeken onde'
         ||chr(10)|| ' where onde.onde_id = decl.id)'
                   ;
    elsif ( p_entiteit = 'ONBE' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and '''||p_onderzoekswijze||''' in'
         ||chr(10)|| '(select onde.onderzoekswijze from kgc_onderzoeken onde, kgc_onderzoek_betrokkenen onbe'
         ||chr(10)|| 'where onbe.onde_id = onde.onde_id and onbe.onbe_id = decl.id)'
                   ;
    elsif ( p_entiteit = 'METI' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and '''||p_onderzoekswijze||''' in'
         ||chr(10)|| '(select onde.onderzoekswijze from kgc_onderzoeken onde, bas_metingen meti'
         ||chr(10)|| 'where meti.onde_id = onde.onde_id and meti.meti_id = decl.id)'
                   ;
    elsif ( p_entiteit = 'MEET' ) -- vooruitlopen op de zaken
    then
      v_statement := v_statement
         ||chr(10)|| 'and '''||p_onderzoekswijze||''' in'
         ||chr(10)|| '(select onde.onderzoekswijze from kgc_onderzoeken onde, bas_metingen meti, bas_meetwaarden meet'
         ||chr(10)|| 'where meet.meti_id = meti.meti_id and meti.onde_id = onde.onde_id and meet.meet_id = decl.id)'
                   ;
    elsif ( p_entiteit = 'GESP' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and '''||p_onderzoekswijze||''' in'
         ||chr(10)|| '(select onde.onderzoekswijze from kgc_onderzoeken onde, kgc_gesprekken gesp'
         ||chr(10)|| 'where gesp.onde_id = onde.onde_id and gesp.gesp_id = decl.id)'
                   ;
    elsif ( p_entiteit = 'MONS' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and 1=2' -- geen geschikt attribuut voor monsters
                   ;
    end if;
  end if;
  if ( p_onde_id is not null )
  then
    if ( p_entiteit = 'ONDE' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and 1=2' -- geen geschikt attribuut voor onderzoeken
                   ;
    elsif ( p_entiteit = 'ONBE' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and exists '
         ||chr(10)|| '(select null from kgc_onderzoek_betrokkenen onbe'
         ||chr(10)|| 'where onbe.onbe_id = decl.id'
         ||chr(10)|| 'and onbe.onde_id = '||to_char(p_onde_id)||')'
                   ;
    elsif ( p_entiteit = 'METI' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and exists '
         ||chr(10)|| '(select null from bas_metingen meti'
         ||chr(10)|| 'where meti.meti_id = decl.id'
         ||chr(10)|| 'and meti.onde_id = '||to_char(p_onde_id)||')'
                   ;
    elsif ( p_entiteit = 'MEET' ) -- vooruitlopen op de zaken
    then
      v_statement := v_statement
         ||chr(10)|| 'and exists '
         ||chr(10)|| '(select null from bas_metingen meti, bas_meetwaarden meet'
         ||chr(10)|| 'where meet.meet_id = decl.id'
         ||chr(10)|| 'and meet.meti_id = meti.meti_id'
         ||chr(10)|| 'and meti.onde_id = '||to_char(p_onde_id)||')'
                   ;
    elsif ( p_entiteit = 'GESP' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and exists '
         ||chr(10)|| '(select null from kgc_gesprekken gesp'
         ||chr(10)|| 'where gesp.gesp_id = decl.id'
         ||chr(10)|| 'and gesp.onde_id = '||to_char(p_onde_id)||')'
                   ;
    elsif ( p_entiteit = 'MONS' )
    then
      v_statement := v_statement
         ||chr(10)|| 'and exists '
         ||chr(10)|| '(select null from kgc_onderzoek_monsters onmo'
         ||chr(10)|| 'where onmo.mons_id = decl.id'
         ||chr(10)|| 'and onmo.onde_id = '||to_char(p_onde_id)||')'
                   ;
    end if;
  end if;
  if ( p_jaar is not null )
  then
    v_statement := v_statement
       ||chr(10)|| 'and to_char(datum,''yyyy'') = '''||p_jaar||''''
                 ;
  end if;
  v_aantal := to_number(kgc_util_00.dyn_exec( v_statement ));
meld( 'tel'||chr(10)||v_statement );
meld( 'aantal='||to_char(v_aantal) );
  RETURN( v_aantal );
exception
when others then
g_fout := 'tel_aantal: '||sqlerrm;
raise;
END tel_aantal;

-- lokale functie om een criterium om te zetten naar een voorwaarde voor een where-clausule
FUNCTION criterium_omzetten
( p_tabelnaam IN VARCHAR2
, p_kolomnaam IN VARCHAR2
, p_voorwaarde IN VARCHAR2
, p_declareer_criteria IN declareer_criteria_rec
)
RETURN VARCHAR2
IS
  v_voorwaarde VARCHAR2(32767) := replace( p_voorwaarde, '<waarde>', '<WAARDE>' ); -- mantis 3338, rde 10-12-2014
  v_criterium VARCHAR2(100);
  v_waarde VARCHAR2(1000);
BEGIN
  IF ( p_tabelnaam IS NOT NULL )
  THEN
    v_criterium := kgc_util_00.tabel_alias( p_tabelnaam => p_tabelnaam );
    IF ( SUBSTR( p_kolomnaam, 1, 4 ) = v_criterium )
    THEN
      v_criterium := UPPER( p_kolomnaam );
    ELSIF ( p_kolomnaam IS NOT NULL )
	THEN
      v_criterium := UPPER( v_criterium || '_' || p_kolomnaam );
    ELSE
      v_criterium := UPPER( v_criterium );
    END IF;
  END IF;
  -- hoe doe ik het volgende wat eleganter? (dynamische variabele waarde?)
  IF ( v_criterium = 'KAFD_ID' )
  THEN
    v_waarde := p_declareer_criteria.kafd_id;
  ELSIF ( v_criterium = 'ONGR_ID' )
  THEN
    v_waarde := p_declareer_criteria.ongr_id;
  ELSIF ( v_criterium = 'PERS_ID' )
  THEN
    v_waarde := p_declareer_criteria.pers_id;
  ELSIF ( v_criterium = 'ONDE_ID' )
  THEN
    v_waarde := p_declareer_criteria.onde_id;
  ELSIF ( v_criterium = 'ONBE_ID' )
  THEN
    v_waarde := p_declareer_criteria.onbe_id;
  ELSIF ( v_criterium = 'MONS_ID' )
  THEN
    v_waarde := p_declareer_criteria.mons_id;
  ELSIF ( v_criterium = 'METI_ID' )
  THEN
    v_waarde := p_declareer_criteria.meti_id;
  ELSIF ( v_criterium = 'GESP_ID' )
  THEN
    v_waarde := p_declareer_criteria.gesp_id;
  ELSIF ( v_criterium = 'KAFD_CODE' )
  THEN
    v_waarde := p_declareer_criteria.kafd_code;
  ELSIF ( v_criterium = 'ONGR_CODE' )
  THEN
    v_waarde := p_declareer_criteria.ongr_code;
  ELSIF ( v_criterium = 'STGR_CODE' )
  THEN
    v_waarde := p_declareer_criteria.stgr_code;
  ELSIF ( v_criterium = 'FRTY_CODE' )
  THEN
    v_waarde := p_declareer_criteria.frty_code;
  ELSIF ( v_criterium = 'HERK_CODE' )
  THEN
    v_waarde := p_declareer_criteria.herk_code;
  ELSIF ( v_criterium = 'INGR_CODE' )
  THEN
    v_waarde := p_declareer_criteria.ingr_code;
  ELSIF ( v_criterium = 'INDI_CODE' )
  THEN
    v_waarde := p_declareer_criteria.indi_code;
  ELSIF ( v_criterium = 'MATE_CODE' )
  THEN
    v_waarde := p_declareer_criteria.mate_code;
  ELSIF ( v_criterium = 'GETY_CODE' )
  THEN
    v_waarde := p_declareer_criteria.gety_code;
  ELSIF ( v_criterium = 'ONDERZOEKSTYPE' )
  THEN
    v_waarde := p_declareer_criteria.onderzoekstype;
  ELSIF ( v_criterium = 'ONDERZOEKSWIJZE' )
  THEN
    v_waarde := p_declareer_criteria.onderzoekswijze;
  ELSIF ( v_criterium = 'ONDE_DECLAREREN' )
  THEN
    v_waarde := p_declareer_criteria.onde_declareren;
  ELSIF ( v_criterium = 'ONDE_AFGEROND' )
  THEN
    v_waarde := p_declareer_criteria.onde_afgerond;
  ELSIF ( v_criterium = 'ONDE_OMSCHRIJVING' )
  THEN
    v_waarde := p_declareer_criteria.onde_omschrijving;
  ELSIF ( v_criterium = 'FRAC_CONTROLE' )
  THEN
    v_waarde := p_declareer_criteria.frac_controle;
  ELSIF ( v_criterium = 'PERS_LAND' )
  THEN
    v_waarde := p_declareer_criteria.pers_land;
  ELSIF ( v_criterium = 'RELA_LAND' )
  THEN
    v_waarde := p_declareer_criteria.rela_land;
  ELSIF ( v_criterium = 'STOF_CODE' )
  THEN
    v_waarde := p_declareer_criteria.stof_code;
  ELSE
    v_waarde := v_criterium;
  END IF;
  -- terechte defaultwaarden???
  IF ( SUBSTR( v_criterium, 5, 3 ) = '_ID' )
  THEN -- numeriek
    v_waarde := NVL( v_waarde, -9999999999 );
  ELSE -- alfanumeriek?
    IF ( v_waarde IS NOT NULL )
    THEN
      v_waarde := ''''||v_waarde||'''';
    END IF;
  END IF;
  IF ( instr( v_voorwaarde, '<WAARDE>' ) > 0 )
  THEN
    v_voorwaarde := replace( v_voorwaarde, '<WAARDE>', nvl( v_waarde, 'NULL' ) );
  ELSE
    v_voorwaarde := nvl(v_waarde,'CHR(2)')||' '||v_voorwaarde;
  END IF;
  RETURN( v_voorwaarde );
exception
when others then
g_fout := 'criterium_omzetten: '||sqlerrm;
raise;
END criterium_omzetten;

-- lokale functie geeft aan of er een gefixeerde (=handmatige of verwerkte) declaratie is
-- dan namelijk geen onderhoud op het veld DECLAREREN door helix-programmatuur!
-- J= gefixeerde declaratie met declareren = J.
-- N= gefixeerde declaratie met declareren = N.
-- leeg= geen gefixeerde declaratie
function met_gefixeerde_declaratie
( p_entiteit in varchar2
, p_id in number
)
return varchar2
is
  v_return varchar2(1) := NULL;
  -- als de status is ingevuld, blijft de declaratie-aanduiding van de entiteit onveranderd
  cursor decl_cur
  ( b_entiteit in varchar2
  , b_id in number
  )
  is
    select declareren
    from   kgc_declaraties
    where  entiteit = b_entiteit
    and    id = b_id
    and    status in ( 'H', 'V', 'X' ) -- is not null
    ;
begin
  open  decl_cur( b_entiteit => p_entiteit
                , b_id => p_id
                );
  fetch decl_cur
  into  v_return;
  close decl_cur;
  return( v_return );
end met_gefixeerde_declaratie;

-- lokale procedure om de declaraties opnieuw te bepalen
procedure bepaal_opnieuw
( p_entiteit in varchar2
, p_id in number
)
is
  -- als de status is ingevuld, blijft de declaratie-aanduiding van de entiteit onveranderd
  -- onderliggende declaraties moeten misschien wel opnieuw worden bepaald
  v_met_decl_status varchar2(1);

  v_deen_id number;
  -- gevonden declaratie-entiteit
  CURSOR deen_cur
  ( b_deen_id in number
  )
  IS
    SELECT deen.deen_id
    ,      deen.declarabel
    from   kgc_decl_entiteiten deen
    where  deen.deen_id = b_deen_id
    ;
  deen_rec deen_cur%rowtype;
begin
meld( 'bepaal opnieuw '||p_entiteit||' '||p_id);
  v_met_decl_status := met_gefixeerde_declaratie
                       ( p_entiteit => p_entiteit
                       , p_id => p_id
                       );

  zet_actuele_decl_id( p_entiteit => p_entiteit
                     , p_id => p_id
                     );
  zet_verwerkte_rij( p_entiteit, p_id );
  g_forceer_tmp_decl_bepaling := TRUE;
  v_deen_id := kgc_decl_00.deen_id
               ( p_entiteit => p_entiteit
               , p_id => p_id
               );
  if ( v_deen_id is null )
  then
    deen_rec.declarabel := 'N';
  else
    open  deen_cur( v_deen_id );
    fetch deen_cur
    into  deen_rec;
    close deen_cur;
  end if;
  -- Let op: hieronder altijd een update (ook als declareren niet verandert)
  -- de AUS-trigger die afgaat zorgt voor de afhankelijke declaratiebepaling
  if ( p_entiteit = 'ONDE' )
  then
    update kgc_onderzoeken
    set    declareren = decode( v_met_decl_status
                              , null, deen_rec.declarabel
                              , v_met_decl_status
                              )
    where  onde_id = p_id
    ;
  elsif ( p_entiteit = 'ONBE' )
  then
    update kgc_onderzoek_betrokkenen
    set    declareren = decode( v_met_decl_status
                              , null, deen_rec.declarabel
                              , v_met_decl_status
                              )
    where  onbe_id = p_id
    ;
  elsif ( p_entiteit = 'METI' )
  then
    update bas_metingen
    set    declareren = decode( v_met_decl_status
                              , null, deen_rec.declarabel
                              , v_met_decl_status
                              )
    where  meti_id = p_id
    ;
  elsif ( p_entiteit = 'GESP' )
  then
    update kgc_gesprekken
    set    declareren = decode( v_met_decl_status
                              , null, deen_rec.declarabel
                              , v_met_decl_status
                              )
    where  gesp_id = p_id
    ;
  elsif ( p_entiteit = 'MONS' )
  then
    update kgc_monsters
    set    declareren = decode( v_met_decl_status
                              , null, deen_rec.declarabel
                              , v_met_decl_status
                              )
    where  mons_id = p_id
    ;
  end if;
  g_forceer_tmp_decl_bepaling := FALSE;
end bepaal_opnieuw;

-- lokale functie om het maximum aantal declaraties voor deze indicatie op te halen
FUNCTION max_aantal_per_indicatie
( p_ingr_code IN VARCHAR2
, p_ongr_id IN NUMBER
)
RETURN NUMBER
IS
  v_return NUMBER;
  CURSOR ingr_cur
  IS
    SELECT ingr.aantal_declaraties
    FROM   kgc_indicatie_groepen ingr
    WHERE  ingr.ongr_id = p_ongr_id
    AND    ingr.code = UPPER( p_ingr_code )
    AND    ingr.vervallen = 'N'
    AND    ingr.machtigingsindicatie IS NOT NULL
    ;
BEGIN
  IF ( p_ingr_code IS NOT NULL
   AND p_ongr_id IS NOT NULL
     )
  THEN
    OPEN  ingr_cur;
    FETCH ingr_cur
    INTO  v_return;
    CLOSE ingr_cur;
  END IF;

  RETURN( v_return );
exception
when others then
g_fout := 'max_aantal_per_indicatie: '||sqlerrm;
raise;
END max_aantal_per_indicatie;

-- lokale functie retourneert de verrichtingcode (extra attribuut bij ONDERZOEKSWIJZE, STOFTESTGROEP, ...
function verrichtingcode
( p_entiteit in varchar2
, p_id in number
)
return varchar2
is
  v_return varchar2(100);
  cursor onde_cur
  is
    select onwy.onwy_id
    from   kgc_onderzoekswijzen onwy
    ,      kgc_onderzoeken onde
    where  onde.onderzoekswijze = onwy.code
    and    onde.kafd_id = onwy.kafd_id
    and    onde.onde_id = p_id
    ;
  cursor meti_cur
  is
    select stgr.stgr_id
    from   kgc_stoftestgroepen stgr
    ,      bas_metingen meti
    where  meti.stgr_id = stgr.stgr_id
    and    meti.meti_id = p_id
    ;
  v_tabelnaam varchar2(30);
  v_id number;
begin
  if ( p_entiteit = 'ONDE' )
  then
    v_tabelnaam := 'KGC_ONDERZOEKSWIJZEN';
    open  onde_cur;
    fetch onde_cur
    into  v_id;
    close onde_cur;
  elsif ( p_entiteit = 'METI' )
  then
    v_tabelnaam := 'KGC_STOFTESTGROEPEN';
    open  meti_cur;
    fetch meti_cur
    into  v_id;
    close meti_cur;
  end if;
  if ( v_id is not null )
  then
    v_return := kgc_attr_00.waarde
                ( p_tabel_naam => v_tabelnaam
                , p_id => v_id
                , p_code => 'VERRICHTINGCODE'
                );
  end if;
  return( v_return);
end verrichtingcode;

-- zoek een declaratie-entiteit waarvan de criteria voldoen
FUNCTION deen_id
( p_declareer_criteria IN declareer_criteria_rec
)
 RETURN  NUMBER
 IS
  v_deen_id NUMBER := NULL;
  rec declareer_criteria_rec := p_declareer_criteria;
  v_criteria_ok BOOLEAN;
  v_statement VARCHAR2(32767); -- mantis 3338, rde 10-12-2014
  v_dummy VARCHAR2(1);
  v_gedeclareerde_aantal NUMBER;
  CURSOR indi_cur
  IS
    SELECT ingr.code
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_indicatie_teksten indi
    WHERE  indi.ingr_id = ingr.ingr_id
    AND    indi.ongr_id = rec.ongr_id
    AND    indi.code = UPPER( rec.indi_code )
   AND    ingr.machtigingsindicatie IS NOT NULL  -- Mantis 1189: voorwaarde toegevoegd
    ;
  CURSOR pers_cur
  IS
    SELECT pers.land
    FROM   kgc_personen pers
    WHERE  pers.pers_id = rec.pers_id
    ;
  CURSOR rela_cur
  IS
    SELECT rela.land
    FROM   kgc_relaties rela
    WHERE  rela.rela_id = rec.rela_id
    ;
  CURSOR deen_cur
  IS
    SELECT deen_id
    FROM   kgc_decl_entiteiten
    WHERE  kafd_id = rec.kafd_id
    AND  ( ongr_id = rec.ongr_id
        OR ongr_id IS NULL
         )
    AND    entiteit = rec.entiteit
    AND  ( declarabel = 'J' or declaratie_maken = 'J' ) -- uitgebreid tbv werkwijze Maastricht (ook registreren van niet-declarabele verrichtingen)
    and    vervallen = 'N'
    ORDER BY volgorde ASC
    ;
  deen_rec deen_cur%rowtype;

  CURSOR decr_cur
  ( b_deen_id IN NUMBER
  )
  IS
    SELECT tabelnaam
    ,      kolomnaam
    ,      waarde
    FROM   kgc_decl_criteria
    WHERE  deen_id = b_deen_id
    order by decode( tabelnaam
                   , 'TEL', 9
                   , 1
                   )
    ,        tabelnaam
    ,        kolomnaam
    ;
  decr_rec decr_cur%rowtype;

BEGIN
  IF ( rec.kafd_code IS NOT NULL
   AND rec.kafd_id IS NULL
     )
  THEN
    rec.kafd_id := kgc_uk2pk.kafd_id( rec.kafd_code );
  END IF;
  IF ( rec.ongr_code IS NOT NULL
   AND rec.ongr_id IS NULL
     )
  THEN
    rec.ongr_id := kgc_uk2pk.ongr_id( rec.kafd_id, rec.ongr_code );
  END IF;
  IF ( rec.ingr_code IS NULL
   AND rec.indi_code IS NOT NULL
     )
  THEN
    OPEN  indi_cur;
    FETCH indi_cur
    INTO  rec.ingr_code;
    CLOSE indi_cur;
  END IF;
  IF ( rec.rela_land is null
   AND rec.rela_id IS NOT NULL
     )
  THEN
    OPEN  rela_cur;
    FETCH rela_cur
    INTO  rec.rela_land;
    CLOSE rela_cur;
  END IF;
  IF ( rec.pers_land is null
   and rec.pers_id IS NOT NULL
     )
  THEN
    OPEN  pers_cur;
    FETCH pers_cur
    INTO  rec.pers_land;
    CLOSE pers_cur;
  END IF;

  -- zoek een declareerbare rij
  OPEN  deen_cur;
  FETCH deen_cur
  INTO  deen_rec;
  IF ( deen_cur%notfound )
  THEN
    v_deen_id := NULL;
  ELSE
    LOOP
      v_deen_id := deen_rec.deen_id;
      if ( deen_cur%notfound )
      then
        v_criteria_ok := false;
        v_deen_id := null;
        exit;
      end if;
meld( 'test deen='||v_deen_id||' entiteit='||rec.entiteit||' onde='||rec.onde_id );
      v_criteria_ok := TRUE;
      if ( v_criteria_ok )
      then
        -- loop door de criteria
        OPEN  decr_cur( deen_rec.deen_id );
        LOOP
          FETCH decr_cur
          INTO  decr_rec;
          if ( decr_cur%notfound )
          then
            exit;
          end if;
          IF ( decr_rec.tabelnaam IS NOT NULL
           AND decr_rec.kolomnaam IS NOT NULL
           AND decr_rec.waarde IS NOT NULL
             )
          THEN
            if ( decr_rec.tabelnaam = 'TEL'
             and decr_rec.kolomnaam = 'AANTAL'
               )
            then
              -- maak een tel-vergelijking
              declare
                l_waarde kgc_decl_criteria.waarde%TYPE := upper( decr_rec.waarde ); -- mantis 3338, rde 10-12-2014
                l_vergelijk varchar2(100);
                l_deen_id number;
                l_ingr_code varchar2(10);
                l_onderzoekswijze varchar2(10); -- eigenlijk maar (2)
                l_onde_id number;
                l_jaar varchar2(4);
              begin
                if ( instr( l_waarde, 'DEEN' ) > 0 )
                then
                  l_deen_id := deen_rec.deen_id;
                end if;
                if ( instr( l_waarde, 'INGR' ) > 0 )
                then
                  l_ingr_code := rec.ingr_code;
                end if;
                if ( instr( l_waarde, 'ONWY' ) > 0 )
                then
                  l_onderzoekswijze := rec.onderzoekswijze;
                end if;
                if ( instr( l_waarde, 'ONDE' ) > 0 )
                then
                  l_onde_id := rec.onde_id;
                end if;
                if ( instr( l_waarde, 'JAAR' ) > 0 )
                then
                  l_jaar := to_char( rec.datum, 'yyyy' );
                end if;
                v_gedeclareerde_aantal := tel_aantal
                                          ( p_entiteit => rec.entiteit
                                          , p_pers_id => rec.pers_id
                                          , p_kafd_id => rec.kafd_id
                                          , p_peildatum => rec.datum
                                          , p_deen_id => l_deen_id
                                          , p_ingr_code => l_ingr_code
                                          , p_onderzoekswijze => l_onderzoekswijze
                                          , p_onde_id => l_onde_id
                                          , p_jaar => l_jaar
                                          );
                -- doe alsof dit item ook al gedeclareerd
                v_gedeclareerde_aantal := nvl( v_gedeclareerde_aantal, 0 ) + 1;
                -- in waarde staat iets als "[DEEN/JAAR] <= 1" of "[INGR-ONWY]"
                l_vergelijk := kgc_util_00.strip
                               ( p_string => l_waarde
                               , p_tag_va => '['
                               , p_tag_tm => ']'
                               , p_rest => 'J'
                               );
                l_vergelijk := ltrim( rtrim( l_vergelijk ) );
                if ( l_ingr_code is not null
                 and l_vergelijk is null
                   )
                then
                  l_vergelijk := '<= '||to_char( max_aantal_per_indicatie
                                                 ( p_ongr_id => rec.ongr_id
                                                 , p_ingr_code => l_ingr_code
                                                 )
                                               );
                end if;
                v_statement := 'select ''X'' from dual where '
                            || to_char( nvl( v_gedeclareerde_aantal, 0 ) )
                            || ' '||nvl( l_vergelijk, '<= 1' )
                             ;
              end; -- maak een tel-vergelijking
            else
              v_statement := 'select ''X'' from dual where '
                          || criterium_omzetten
                             ( p_tabelnaam => decr_rec.tabelnaam
                             , p_kolomnaam => decr_rec.kolomnaam
                             , p_voorwaarde => decr_rec.waarde
                             , p_declareer_criteria => rec
                             );
            end if;
meld( v_statement );
            BEGIN
              v_dummy := kgc_util_00.dyn_exec( v_statement );
            EXCEPTION
              WHEN OTHERS
              THEN
                v_dummy := NULL; -- mis = fout
            END;
            IF ( v_dummy = 'X' )
            THEN
              NULL;
            ELSE
              v_criteria_ok := FALSE;
            END IF;
            EXIT WHEN NOT v_criteria_ok; -- 1 foute betekent niet-declareerbaar
          END IF;
        END LOOP;
        IF ( decr_cur%isopen )
        THEN
          CLOSE decr_cur;
        END IF;
      end if;
      EXIT WHEN v_criteria_ok; -- alle criteria ok, dus goede set
      FETCH deen_cur
      INTO  deen_rec;
    END LOOP;
  END IF;
  IF ( deen_cur%isopen )
  THEN
    CLOSE deen_cur;
  END IF;
  IF ( decr_cur%isopen )
  THEN
    CLOSE decr_cur;
  END IF;
  IF ( NOT v_criteria_ok )
  THEN
    v_deen_id := NULL;
  END IF;
meld( 'Gevonden deen='||v_deen_id);
  RETURN( v_deen_id );
EXCEPTION
  WHEN OTHERS
  THEN
    IF ( deen_cur%isopen )
    THEN
      CLOSE deen_cur;
    END IF;
    IF ( decr_cur%isopen )
    THEN
      CLOSE decr_cur;
    END IF;
g_fout := 'deen_id (1): '||sqlerrm;
    RAISE;
END deen_id;
-- overloaded, waarden worden eerst opgehaald voor een id van een entiteit
FUNCTION deen_id
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
)
RETURN  NUMBER
IS
  rec declareer_criteria_rec;
  v_deen_id NUMBER;
  v_declareren VARCHAR2(1) := NULL;

  CURSOR onde_cur
  IS
    SELECT kafd.code kafd_code
    ,      ongr.code ongr_code
    ,      onde.onderzoekstype
    ,      onde.onderzoekswijze
    ,      onde.datum_binnen datum
    ,      onde.omschrijving onde_omschrijving
    ,      onde.declareren onde_declareren
    ,      onde.afgerond onde_afgerond
    ,      herk.code herk_code
    ,      kgc_indi_00.ingr_code( onde.onde_id ) ingr_code
    ,      kgc_indi_00.indi_code( onde.onde_id ) indi_code -- mantis 706
    ,      onde.pers_id
    ,      onde.onde_id
    ,      onde.kafd_id
    ,      onde.ongr_id
    ,      onde.rela_id
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_onderzoeksgroepen ongr
    ,      kgc_herkomsten herk
    ,      kgc_onderzoeken onde
    WHERE  onde.kafd_id = kafd.kafd_id
    AND    onde.ongr_id = ongr.ongr_id
    AND    onde.herk_id = herk.herk_id (+)
    AND    onde.onde_id = p_id
    ;
  CURSOR onbe_cur
  IS
    SELECT kafd.code kafd_code
    ,      ongr.code ongr_code
    ,      onde.onderzoekstype
    ,      onde.onderzoekswijze
    ,      onde.datum_binnen datum
    ,      onde.omschrijving onde_omschrijving
    ,      onde.declareren onde_declareren
    ,      onde.afgerond onde_afgerond
    ,      frty.code frty_code
    ,      frac.controle frac_controle
    ,      mate.code mate_code
    ,      herk.code herk_code
    ,      kgc_indi_00.ingr_code( onbe.onde_id ) ingr_code
    ,      kgc_indi_00.indi_code( onbe.onde_id ) indi_code -- mantis 706
    ,      onbe.pers_id
    ,      onbe.onde_id
    ,      onbe.onbe_id
    ,      onbe.rnde_id
    ,      onde.kafd_id
    ,      onde.ongr_id
    ,      onde.rela_id
    ,      mons.pers_id pers_id_mons
    ,      mons.foet_id foet_id_mons
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_onderzoeksgroepen ongr
    ,      bas_fractie_types frty
    ,      kgc_materialen mate
    ,      kgc_herkomsten herk
    ,      kgc_onderzoeken onde
    ,      kgc_monsters mons
    ,      bas_fracties frac
    ,      kgc_onderzoek_betrokkenen onbe
    WHERE  onbe.frac_id = frac.frac_id
    AND    frac.frty_id = frty.frty_id (+)
    AND    frac.mons_id = mons.mons_id
    AND    mons.mate_id = mate.mate_id
    AND    mons.herk_id = herk.herk_id (+)
    AND    onbe.onde_id = onde.onde_id
    AND    onde.kafd_id = kafd.kafd_id
    AND    onde.ongr_id = ongr.ongr_id
    AND    onbe.onbe_id = p_id
    ;
  v_rnde_id NUMBER;
  v_pers_id_mons NUMBER;
  v_foet_id_mons NUMBER;
/*
  -- bestaat er al een declarabele ONBE, voor deze monster-persoon/foetus in dit onderzoek
  -- dan niet nog een keer declareren.
-- OVERBODIG GEWORDEN: KAN IN DE CRITERIA OPGELOST WORDEN MET: TEL, AANTAL, [ONDE] <= 1.
  CURSOR onbe2_cur
  ( b_onde_id IN NUMBER
  , b_pers_id IN NUMBER
  , b_foet_id IN NUMBER
  )
  IS
    SELECT NULL
    FROM   kgc_monsters mons
    ,      bas_fracties frac
    ,      kgc_onderzoek_betrokkenen onbe
    WHERE  onbe.declareren = 'J'
    AND    onbe.onde_id = b_onde_id
    AND    onbe.frac_id = frac.frac_id
    AND    frac.mons_id = mons.mons_id
    AND    mons.pers_id = b_pers_id
    AND    NVL( mons.foet_id, -9999999999 ) = NVL( b_foet_id, -9999999999 )
    AND    onbe.onbe_id <> p_id
and 1=2 -- even uitgeschakeld
    ;
  v_dummy VARCHAR2(1);
*/
  -- monsterafnames door obstetrie worden geregistreerd via "herkomst"
  CURSOR mons_cur
  IS
    SELECT kafd.code kafd_code
    ,      ongr.code ongr_code
    ,      mate.code mate_code
    ,      herk.code herk_code
    ,      mons.datum_aanmelding datum
    ,      mons.pers_id
    ,      mons.mons_id
    ,      mons.kafd_id
    ,      mons.ongr_id
    ,      mons.rela_id
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_herkomsten herk
    ,      kgc_materialen mate
    ,      kgc_onderzoeksgroepen ongr
    ,      kgc_monsters mons
    WHERE  mons.kafd_id = kafd.kafd_id
    AND    mons.ongr_id = ongr.ongr_id
    AND    mons.mate_id = mate.mate_id
    AND    mons.herk_id = herk.herk_id (+)
    AND    mons.mons_id = p_id
    ;
  CURSOR meti_cur
  IS
    SELECT kafd.code kafd_code
    ,      ongr.code ongr_code
    ,      onde.onderzoekstype
    ,      onde.onderzoekswijze
    ,      onde.datum_binnen datum
    ,      onde.omschrijving onde_omschrijving
    ,      onde.declareren onde_declareren
    ,      onde.afgerond onde_afgerond
    ,      frac.controle
    ,      frty.code frty_code
    ,      stgr.code stgr_code
    ,      mate.code mate_code
    ,      herk.code herk_code
    ,      kgc_indi_00.ingr_code( meti.onde_id ) ingr_code
    ,      kgc_indi_00.indi_code( meti.onde_id ) indi_code -- mantis 706
    ,      onde.pers_id -- ?? mons.pers_id??
    ,      meti.onde_id
    ,      meti.meti_id
    ,      frac.mons_id
    ,      onde.rela_id
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_onderzoeksgroepen ongr
    ,      bas_fractie_types frty
    ,      kgc_stoftestgroepen stgr
    ,      kgc_herkomsten herk
    ,      kgc_materialen mate
    ,      kgc_monsters mons
    ,      kgc_onderzoeken onde
    ,      bas_fracties frac
    ,      bas_metingen meti
    WHERE  meti.frac_id = frac.frac_id

    and    frac.frty_id = frty.frty_id (+)
    and    frac.mons_id = mons.mons_id
    and    mons.mate_id = mate.mate_id
    and    mons.herk_id = herk.herk_id (+)
    AND    meti.onde_id = onde.onde_id
    AND    onde.kafd_id = kafd.kafd_id
    AND    onde.ongr_id = ongr.ongr_id
    AND    meti.stgr_id = stgr.stgr_id (+)
    AND    meti.meti_id = p_id
    ;
  CURSOR gesp_cur
  IS
    SELECT kafd.code kafd_code
    ,      ongr.code ongr_code
    ,      onde.onderzoekstype
    ,      onde.onderzoekswijze
    ,      onde.datum_binnen datum
    ,      onde.omschrijving onde_omschrijving
    ,      onde.declareren onde_declareren
    ,      onde.afgerond onde_afgerond
    ,      kgc_indi_00.ingr_code( gesp.onde_id ) ingr_code
    ,      kgc_indi_00.indi_code( gesp.onde_id ) indi_code -- mantis 706
    ,      gety.code gety_code
    ,      gebe.pers_id
    ,      gesp.onde_id
    ,      gesp.gesp_id
    ,      onde.rela_id
    ,      gebe.persoon_rol
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_onderzoeksgroepen ongr
    ,      kgc_onderzoeken onde
    ,      kgc_gesprek_betrokkenen gebe
    ,      kgc_gesprek_types gety
    ,      kgc_gesprekken gesp
    WHERE  gesp.onde_id = onde.onde_id
    AND    onde.kafd_id = kafd.kafd_id
    AND    gesp.ongr_id = ongr.ongr_id
    AND    gesp.gety_id = gety.gety_id
    AND    gesp.gesp_id = gebe.gesp_id
    AND    gebe.declaratie = 'J'
    AND    gesp.gesp_id = p_id
    union all -- als geen gespreks-aanwezige, dan onderzoekspersoon; zie ook kgc_declaratie_items_vw
    SELECT kafd.code kafd_code
    ,      ongr.code ongr_code
    ,      onde.onderzoekstype
    ,      onde.onderzoekswijze
    ,      onde.datum_binnen datum
    ,      onde.omschrijving onde_omschrijving
    ,      onde.declareren onde_declareren
    ,      onde.afgerond onde_afgerond
    ,      kgc_indi_00.ingr_code( gesp.onde_id ) ingr_code
    ,      kgc_indi_00.indi_code( gesp.onde_id ) indi_code -- mantis 706
    ,      gety.code gety_code
    ,      onde.pers_id
    ,      gesp.onde_id
    ,      gesp.gesp_id
    ,      onde.rela_id
    ,      null -- rol
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_onderzoeksgroepen ongr
    ,      kgc_onderzoeken onde
    ,      kgc_gesprek_types gety
    ,      kgc_gesprekken gesp
    WHERE  gesp.onde_id = onde.onde_id
    AND    onde.kafd_id = kafd.kafd_id
    AND    gesp.ongr_id = ongr.ongr_id
    AND    gesp.gety_id = gety.gety_id
    AND    gesp.gesp_id = p_id
    and    not exists
           ( select null
             from   kgc_gesprek_betrokkenen gebe
             where  gebe.gesp_id = gesp.gesp_id
             and    gebe.pers_id is not null
             and    gebe.declaratie = 'J'
           )
    order by 15 asc -- persoon_rol (eerst A1, dan A2,, dan P..)
    ;
  v_rol kgc_gesprek_betrokkenen.persoon_rol%type;
BEGIN
  rec.entiteit := UPPER( p_entiteit );
  IF ( rec.entiteit = 'ONDE' )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  rec.kafd_code
    ,     rec.ongr_code
    ,     rec.onderzoekstype
    ,     rec.onderzoekswijze
    ,     rec.datum
    ,     rec.onde_omschrijving
    ,     rec.onde_declareren
    ,     rec.onde_afgerond
    ,     rec.herk_code
    ,     rec.ingr_code
    ,     rec.indi_code -- mantis 706
    ,     rec.pers_id
    ,     rec.onde_id
    ,     rec.kafd_id
    ,     rec.ongr_id
    ,     rec.rela_id;
    CLOSE onde_cur;
  ELSIF ( rec.entiteit = 'ONBE' )
  THEN
    OPEN  onbe_cur;
    FETCH onbe_cur
    INTO  rec.kafd_code
    ,     rec.ongr_code
    ,     rec.onderzoekstype
    ,     rec.onderzoekswijze
    ,     rec.datum
    ,     rec.onde_omschrijving
    ,     rec.onde_declareren
    ,     rec.onde_afgerond
    ,     rec.frty_code
    ,     rec.frac_controle
    ,     rec.mate_code
    ,     rec.herk_code
    ,     rec.ingr_code  -- mantis 1189 ingr_code en indi_code verwisseld
    ,     rec.indi_code -- mantis 706
    ,     rec.pers_id
    ,     rec.onde_id
    ,     rec.onbe_id
    ,     v_rnde_id
    ,     rec.kafd_id
    ,     rec.ongr_id
    ,     rec.rela_id
    ,     v_pers_id_mons
    ,     v_foet_id_mons;
    CLOSE onbe_cur;
    if ( v_rnde_id is not null )
    then
      v_declareren := 'N';
/* zie opmerking bij cursor
    else
      OPEN  onbe2_cur( rec.onde_id, v_pers_id_mons, v_foet_id_mons );
      FETCH onbe2_cur
      INTO  v_dummy;
      IF ( onbe2_cur%found )
      THEN
        v_declareren := 'N';
      END IF;
      CLOSE onbe2_cur;
*/
    end if;
  ELSIF ( rec.entiteit = 'MONS' )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  rec.kafd_code
    ,     rec.ongr_code
    ,     rec.mate_code
    ,     rec.herk_code
    ,     rec.datum
    ,     rec.pers_id
    ,     rec.mons_id
    ,     rec.kafd_id
    ,     rec.ongr_id
    ,     rec.rela_id;
    CLOSE mons_cur;
  ELSIF ( rec.entiteit = 'METI' )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  rec.kafd_code
    ,     rec.ongr_code
    ,     rec.onderzoekstype
    ,     rec.onderzoekswijze
    ,     rec.datum
    ,     rec.onde_omschrijving
    ,     rec.onde_declareren
    ,     rec.onde_afgerond
    ,     rec.frac_controle
    ,     rec.frty_code
    ,     rec.stgr_code
    ,     rec.mate_code
    ,     rec.herk_code
    ,     rec.ingr_code
    ,     rec.indi_code -- mantis 706
    ,     rec.pers_id
    ,     rec.onde_id
    ,     rec.meti_id
    ,     rec.mons_id
    ,     rec.rela_id;
    CLOSE meti_cur;
  ELSIF ( rec.entiteit = 'GESP' )
  THEN
    OPEN  gesp_cur;
    FETCH gesp_cur
    INTO  rec.kafd_code
    ,     rec.ongr_code
    ,     rec.onderzoekstype
    ,     rec.onderzoekswijze
    ,     rec.datum
    ,     rec.onde_omschrijving
    ,     rec.onde_declareren
    ,     rec.onde_afgerond
    ,     rec.ingr_code
    ,     rec.indi_code   -- mantis 706
    ,     rec.gety_code
    ,     rec.pers_id
    ,     rec.onde_id
    ,     rec.gesp_id
    ,     rec.rela_id
    ,     v_rol;
    CLOSE gesp_cur;
  END IF;
  IF ( v_declareren = 'N' )
  THEN
    v_deen_id := NULL;
  ELSE
    v_deen_id := deen_id
                 ( p_declareer_criteria => rec
                 );
  END IF;
  RETURN( v_deen_id );
exception
when others then
g_fout := 'deen_id(2): '||sqlerrm;
raise;
END deen_id;

-- procedure om te zien of een declaratie al bestaat
PROCEDURE gedeclareerd
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, x_declareren OUT VARCHAR2
, x_status OUT VARCHAR2
, x_datum_verwerking OUT DATE
)
IS
  CURSOR decl_cur
  IS
    SELECT declareren
    ,      status
    ,      datum_verwerking
    FROM   kgc_declaraties
    WHERE  entiteit = p_entiteit
    AND    id = p_id
    ;
  decl_rec decl_cur%rowtype;
BEGIN
  IF ( p_entiteit IS NOT NULL
   AND p_id IS NOT NULL
     )
  THEN
    OPEN  decl_cur;
    FETCH decl_cur
    INTO  decl_rec;
    CLOSE decl_cur;
  END IF;
  x_declareren := decl_rec.declareren;
  x_status := decl_rec.status;
  x_datum_verwerking := decl_rec.datum_verwerking;
exception
when others then
g_fout := 'gedeclareerd: '||sqlerrm;
raise;
END gedeclareerd;

FUNCTION  gedeclareerd
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
)
RETURN   VARCHAR2
IS
  v_declareren VARCHAR2(1);
  v_status VARCHAR2(1);
  v_datum_verwerking DATE;
BEGIN
  gedeclareerd
  ( p_entiteit => p_entiteit
  , p_id => p_id
  , x_declareren => v_declareren
  , x_status => v_status
  , x_datum_verwerking => v_datum_verwerking
  );
  RETURN( v_declareren );
END gedeclareerd;

-- lokale procedure om een declaratie-record aan te maken
PROCEDURE declareren
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_deen_id in number
, p_declareren IN VARCHAR2 := 'J'
)
IS
  v_gedeclareerd VARCHAR2(1) := NULL;

  CURSOR deen_cur
  IS
    SELECT deen.declarabel
    ,      deen.declaratie_maken
    from   kgc_decl_entiteiten deen
    where  deen.deen_id = p_deen_id
    ;
  deen_rec deen_cur%rowtype;

BEGIN
  v_gedeclareerd := gedeclareerd
                  ( p_entiteit => p_entiteit
                  , p_id => p_id
                  );
  -- een waarde hierboven betekent een rij gevonden (J of N), dus niet weer een insert
  IF ( v_gedeclareerd IS NULL )
  THEN
    open  deen_cur;
    fetch deen_cur
    into  deen_rec;
    close deen_cur;
    if ( deen_rec.declaratie_maken = 'J' )
    then
      BEGIN
        -- standaard vulling van overige kolommen in tabel gebeurt in kgc_decl_bir trigger
        INSERT INTO kgc_declaraties
        ( entiteit
        , id
        , declareren
        , deen_id
        )
        VALUES
        ( UPPER(p_entiteit)
        , p_id
        , nvl( deen_rec.declarabel, p_declareren )
        , p_deen_id
        );
      END;
    end if;
  END IF;
exception
when others then
g_fout := 'declareren: '||sqlerrm;
raise;
END declareren;

-- lokale procedure om te forceren dat declaraties op lagere niveaus (ook) opnieuw worden bekeken:
-- bv.: als onderzoeksgegevens wijzigen, kan dat gevolgen hebben voor declaraties van betrokkenen
procedure afhankelijke_declaraties
( p_entiteit in varchar2
, p_id in number
)
is
  -- tbv. performance onderstaande queries sterker beperken:
  -- bv. alleen als decl-eenheid is te vinden (kafd/ongr/entiteit)
  cursor onde_cur
  is
    select 'ONBE' entiteit
    ,      onbe.onbe_id id
    from   kgc_onderzoek_betrokkenen onbe
    ,      kgc_onderzoeken onde
    where  onbe.onde_id = onde.onde_id
    and    onde.onde_id = p_id
    and    onbe.rnde_id is null
    union all
    select 'METI'
    ,      meti.meti_id
    from   bas_metingen meti
    ,      kgc_onderzoeken onde
    where  meti.onde_id = onde.onde_id
    and    onde.onde_id = p_id
    union all
    select 'GESP'
    ,      gesp.gesp_id
    from   kgc_gesprekken gesp
    ,      kgc_onderzoeken onde
    where  gesp.onde_id = onde.onde_id
    and    onde.onde_id = p_id
    -- straks ook MEET
    ;
  cursor onbe_cur
  is
    select 'GESP' entiteit
    ,      gesp.gesp_id id
    FROM   kgc_gesprek_betrokkenen gebe
    ,      kgc_gesprekken gesp
    ,      kgc_onderzoek_betrokkenen onbe
    ,      kgc_onderzoeken onde
    WHERE  gesp.gesp_id = gebe.gesp_id
    AND    gesp.onde_id = onbe.onde_id
    AND    gebe.pers_id = onbe.pers_id
    and    gesp.onde_id = onde.onde_id
    and    onbe.onbe_id = p_id
    ;
  cursor meti_cur
  is
    select 'ONDE' entiteit
    ,      meti.onde_id id
    from   bas_metingen meti
    ,      kgc_onderzoeken onde
    where  meti.onde_id = onde.onde_id
    and    meti.meti_id = p_id
    -- straks ook MEET
    ;
  cursor gesp_cur
  is
    select 'GESP' entiteit
    ,      0 id
    from   dual
    where  1 = 2
    ;
  cursor mons_cur
  is
    select 'METI' entiteit
    ,      meti.meti_id id
    from   bas_metingen meti
    ,      kgc_onderzoeken onde
    ,      bas_fracties frac
    ,      kgc_monsters mons
    where  meti.onde_id = onde.onde_id
    and    onde.afgerond = 'N'
    and    meti.frac_id = frac.frac_id
    and    frac.mons_id = mons.mons_id
    and    mons.mons_id = p_id
    ;
begin
  if ( p_id is null )
  then
    return;
  end if;
  if ( p_entiteit = 'ONDE' )
  then
    for r in onde_cur
    loop
meld( 'afhankelijk van ONDE: '||r.entiteit||' '||r.id );
      if ( not is_rij_verwerkt( r.entiteit, r.id ) )
      then
        bepaal_opnieuw
        ( p_entiteit => r.entiteit
        , p_id => r.id
        );
      end if;
    end loop;
  elsif ( p_entiteit = 'ONBE' )
  then
    for r in onbe_cur
    loop
meld( 'afhankelijk van ONBE: '||r.entiteit||' '||r.id );
      if ( not is_rij_verwerkt( r.entiteit, r.id ) )
      then
        bepaal_opnieuw
        ( p_entiteit => r.entiteit
        , p_id => r.id
        );
      end if;
    end loop;
  elsif ( p_entiteit = 'METI' )
  then
    for r in meti_cur
    loop
meld( 'afhankelijk van METI: '||r.entiteit||' '||r.id );
      if ( not is_rij_verwerkt( r.entiteit, r.id ) )
      then
        bepaal_opnieuw
        ( p_entiteit => r.entiteit
        , p_id => r.id
        );
      end if;
    end loop;
  elsif ( p_entiteit = 'GESP' )
  then
    for r in gesp_cur
    loop
meld( 'afhankelijk van GESP: '||r.entiteit||' '||r.id );
      if ( not is_rij_verwerkt( r.entiteit, r.id ) )
      then
        bepaal_opnieuw
        ( p_entiteit => r.entiteit
        , p_id => r.id
        );
      end if;
    end loop;
  elsif ( p_entiteit = 'MONS' )
  then
    for r in mons_cur
    loop
meld( 'afhankelijk van MONS: '||r.entiteit||' '||r.id );
      if ( not is_rij_verwerkt( r.entiteit, r.id ) )
      then
        bepaal_opnieuw
        ( p_entiteit => r.entiteit
        , p_id => r.id
        );
      end if;
    end loop;
  end if;
exception
when others then
g_fout := 'afhankelijke_declaraties: '||sqlerrm;
raise;
end afhankelijke_declaraties;

-- PUBLIEK --
FUNCTION  check_criterium
 (  p_tabelnaam  IN  VARCHAR2  :=  NULL
 ,  p_kolomnaam  IN  VARCHAR2  :=  NULL
 ,  p_id  IN  NUMBER  :=  NULL
 )
 RETURN  VARCHAR2
 IS
  v_criterium VARCHAR2(100) := NVL( p_tabelnaam, '%' )||'.'||NVL( p_kolomnaam, '%' );

  TYPE criteria_tab_type IS TABLE OF VARCHAR2(100)
  INDEX BY BINARY_INTEGER;
  criteria_tab criteria_tab_type;
  i BINARY_INTEGER;

   procedure vul
   ( b_criterium in varchar2
   )
   is
     i binary_integer;
   begin
     i := nvl( criteria_tab.last, 0 ) + 1;
     criteria_tab(i) := b_criterium;
   end vul;
 BEGIN
  criteria_tab.delete;
  vul( 'TEL.AANTAL' );
  vul( 'KGC_ONDERZOEKEN.ONDERZOEKSTYPE' );
  vul( 'KGC_ONDERZOEKEN.ONDERZOEKSWIJZE' );
  vul( 'KGC_ONDERZOEKEN.DECLAREREN' );
  vul( 'KGC_ONDERZOEKEN.AFGEROND' );
  vul( 'KGC_ONDERZOEKEN.OMSCHRIJVING' );
  vul( 'KGC_STOFTESTGROEPEN.CODE' );
  vul( 'BAS_FRACTIES.CONTROLE' );
  vul( 'BAS_FRACTIE_TYPES.CODE' );
  vul( 'KGC_HERKOMSTEN.CODE' );
  vul( 'KGC_INDICATIE_GROEPEN.CODE' );
  vul( 'KGC_INDICATIE_TEKSTEN.CODE' );
  vul( 'KGC_MATERIALEN.CODE' );
  vul( 'KGC_KGC_AFDELINGEN.CODE' );
  vul( 'KGC_ONDERZOEKSGROEPEN.CODE' );
  vul( 'KGC_GESPREK_TYPES.CODE' );
  vul( 'KGC_PERSONEN.LAND' );
  vul( 'KGC_RELATIES.LAND' );
  vul( 'KGC_STOFTESTEN.CODE' ); -- nog niet in gebruik

  i := 1;
  LOOP
    EXIT WHEN NOT criteria_tab.exists(i);
    IF ( criteria_tab(i) LIKE v_criterium )
    THEN
      RETURN( NULL );
    END IF;
    i := criteria_tab.next(i);
  END LOOP;
  qms$errors.show_message
  ( p_mesg => 'Criterium '||p_tabelnaam||'.'||p_kolomnaam||' wordt (nog) niet ondersteund.'
  , p_errtp => 'E'
  , p_rftf => TRUE
  );
exception
when others then
g_fout := 'check_criterium: '||sqlerrm;
raise;
END check_criterium;
PROCEDURE  check_criterium
 (  p_tabelnaam  IN  VARCHAR2  :=  NULL
 ,  p_kolomnaam  IN  VARCHAR2  :=  NULL
 )
 IS
  v_dummy VARCHAR2(2000);
 BEGIN
  v_dummy := check_criterium
             ( p_tabelnaam => p_tabelnaam
             , p_kolomnaam => p_kolomnaam
             );
END check_criterium;

FUNCTION declarabel
( p_declareer_criteria IN declareer_criteria_rec
, p_rnde_id            IN NUMBER := NULL
)
RETURN  VARCHAR2
IS
  rec declareer_criteria_rec := p_declareer_criteria;
  v_id number;
  v_deen_id number;
  v_declarabel varchar2(1) := 'N';
BEGIN
  -- 1193/JKO: p_rnde_id gevuld betekent geen declaratie-criteria evalueren
  IF ( p_rnde_id IS NOT NULL )
  THEN
    v_deen_id := NULL;
  ELSE
    v_deen_id := kgc_decl_00.deen_id( p_declareer_criteria => p_declareer_criteria );
  END IF;

  IF ( rec.entiteit = 'ONDE' )
  THEN
    v_id := rec.onde_id;
  ELSIF ( rec.entiteit = 'ONBE' )
  THEN
    v_id := rec.onbe_id;
  ELSIF ( rec.entiteit = 'METI' )
  THEN
    v_id := rec.meti_id;
  ELSIF ( rec.entiteit = 'GESP' )
  THEN
    v_id := rec.gesp_id;
  ELSIF ( rec.entiteit = 'MONS' )
  THEN
    v_id := rec.mons_id;
  ELSE
    v_id := NULL;
  END IF;

  v_declarabel := met_gefixeerde_declaratie
                  ( p_entiteit => rec.entiteit
                  , p_id => v_id
                  );
  IF ( v_declarabel IS NULL ) -- niet gefixeerd
  THEN
    IF ( v_deen_id IS NOT NULL )
    THEN
      v_declarabel := 'J';
    else
      v_declarabel := 'N';
    end if;
  END IF;
  RETURN( v_declarabel );
exception
when others then
g_fout := 'declarabel (1): '||sqlerrm;
raise;
END declarabel;

FUNCTION declarabel
( p_criteria IN varchar2
)
RETURN  VARCHAR2
IS
  rec declareer_criteria_rec;
  v_declareren VARCHAR2(1) := 'N';
  v_rnde_id kgc_redenen_niet_declarabel.rnde_id%TYPE := NULL;
  v_een_id number; -- om even een binnengekomen id op te vangen

  cursor ongr_cur
  ( b_id in number
  )
  is
    select code
    from   kgc_onderzoeksgroepen
    where  ongr_id = b_id
    ;
  cursor ingr_cur
  ( b_id in number
  )
  is
    select code
    from   kgc_indicatie_groepen
    where  ingr_id = b_id
    ;
  cursor stgr_cur
  ( b_id in number
  )
  is
    select code
    from   kgc_stoftestgroepen
    where  stgr_id = b_id
    ;
  cursor stof_cur
  ( b_id in number
  )
  is
    select code
    from   kgc_stoftesten
    where  stof_id = b_id
    ;
  cursor frty_cur
  ( b_id in number
  )
  is
    select code
    from   bas_fractie_types
    where  frty_id = b_id
    ;
  cursor gety_cur
  ( b_id in number
  )
  is
    select code
    from   kgc_gesprek_types
    where  gety_id = b_id
    ;
  cursor mate_cur
  ( b_id in number
  )
  is
    select code
    from   kgc_materialen
    where  mate_id = b_id
    ;
  cursor herk_cur
  ( b_id in number
  )
  is
    select code
    from   kgc_herkomsten
    where  herk_id = b_id
    ;
  -- haal waarde uit tags
  function strip
  ( b_criterium in varchar2
  )
  return varchar2
  is
  begin
    return( kgc_util_00.strip
            ( p_string => p_criteria
            , p_tag_va => '<'||upper(b_criterium)||'>'
            , p_tag_tm => '</'||upper(b_criterium)||'>'
            , p_rest => 'N'
            )
          );
  end strip;
BEGIN
  rec.entiteit := strip( 'ENTITEIT' );
  rec.kafd_id := strip( 'KAFD_ID' );
  rec.ongr_id := strip( 'ONGR_ID' );
  rec.pers_id := strip( 'PERS_ID' );
  rec.rela_id := strip( 'RELA_ID' );
  rec.onde_id := strip( 'ONDE_ID' );
  rec.onbe_id := strip( 'ONBE_ID' );
  rec.mons_id := strip( 'MONS_ID' );
  rec.meti_id := strip( 'METI_ID' );
  rec.gesp_id := strip( 'GESP_ID' );
  rec.kafd_code := strip( 'KAFD_CODE' );
  rec.ongr_code := strip( 'ONGR_CODE' );
  rec.stgr_code := strip( 'STGR_CODE' );
  rec.frty_code := strip( 'FRTY_CODE' );
  rec.herk_code := strip( 'HERK_CODE' );
  rec.ingr_code := strip( 'INGR_CODE' );
  rec.indi_code := strip( 'INDI_CODE' );
  rec.mate_code := strip( 'MATE_CODE' );
  rec.gety_code := strip( 'GETY_CODE' );
  rec.datum := strip( 'DATUM' );
  rec.onderzoekstype := strip( 'ONDERZOEKSTYPE' );
  rec.onderzoekswijze := strip( 'ONDERZOEKSWIJZE' );
  rec.onde_declareren := strip( 'ONDE_DECLAREREN' );
  rec.onde_afgerond := strip( 'ONDE_AFGEROND' );
  rec.onde_omschrijving := strip( 'ONDE_OMSCHRIJVING' );
  rec.frac_controle := strip( 'FRAC_CONTROLE' );
  rec.pers_land := strip( 'PERS_LAND' );
  rec.rela_land := strip( 'RELA_LAND' );
  rec.stof_code := strip( 'STOF_CODE' );
 -- Mantis 1193/JKO: RNDE_ID is geen declaratie-criterium, maar overruled de criteria met declareren=N tenzij status=H
  v_rnde_id := strip( 'RNDE_ID' );

  -- zet sommige id's om naar code
  -- Mantis 1102/JKO: afleiding ongr_code toegevoegd
  v_een_id := strip( 'ONGR_ID' );
  if ( v_een_id is not null )
  then
    open  ongr_cur( v_een_id );
    fetch ongr_cur
    into  rec.ongr_code;
    close ongr_cur;
  end if;
  v_een_id := strip( 'INGR_ID' );
  if ( v_een_id is not null )
  then
    open  ingr_cur( v_een_id );
    fetch ingr_cur
    into  rec.ingr_code;
    close ingr_cur;
  end if;
  v_een_id := strip( 'HERK_ID' );
  if ( v_een_id is not null )
  then
    open  herk_cur( v_een_id );
    fetch herk_cur
    into  rec.herk_code;
    close herk_cur;
  end if;
  v_een_id := strip( 'MATE_ID' );
  if ( v_een_id is not null )
  then
    open  mate_cur( v_een_id );
    fetch mate_cur
    into  rec.mate_code;
    close mate_cur;
  end if;
  v_een_id := strip( 'STGR_ID' );
  if ( v_een_id is not null )
  then
    open  stgr_cur( v_een_id );
    fetch stgr_cur
    into  rec.stgr_code;
    close stgr_cur;
  end if;
  v_een_id := strip( 'STOF_ID' );
  if ( v_een_id is not null )
  then
    open  stof_cur( v_een_id );
    fetch stof_cur
    into  rec.stof_code;
    close stof_cur;
  end if;
  v_een_id := strip( 'FRTY_ID' );
  if ( v_een_id is not null )
  then
    open  frty_cur( v_een_id );
    fetch frty_cur
    into  rec.frty_code;
    close frty_cur;
  end if;
  v_een_id := strip( 'GETY_ID' );
  if ( v_een_id is not null )
  then
    open  gety_cur( v_een_id );
    fetch gety_cur
    into  rec.gety_code;
    close gety_cur;
  end if;
  v_declareren := declarabel
                    ( p_declareer_criteria => rec
                    , p_rnde_id            => v_rnde_id );
  RETURN( v_declareren );
exception
when others then
g_fout := 'declarabel (2): '||sqlerrm;
raise;
END declarabel;

FUNCTION  declarabel
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_inclusief_deze_rij IN BOOLEAN := FALSE -- niet meer in gebruik
)
RETURN   VARCHAR2
IS
  v_return VARCHAR2(1);
/* gebruik van deze parameter is vervangen door zet_actuele_decl_id
  CURSOR decl_cur
  IS
    SELECT declareren
    FROM   kgc_declaraties
    WHERE  entiteit = UPPER( p_entiteit )
    AND    id = p_id
    and    nvl( status, '-' ) <> 'H'
    ;
*/
  v_deen_id NUMBER;
 BEGIN
/* zie opmerking bij cursor
  IF ( p_inclusief_deze_rij )
  THEN
    OPEN  decl_cur;
    FETCH decl_cur
    INTO  v_return;
    CLOSE decl_cur;
    IF ( v_return IS NOT NULL )
    THEN
      RETURN( v_return );
    END IF;
  END IF;
*/
  zet_actuele_decl_id( p_entiteit => p_entiteit
                     , p_id => p_id
                     );
  v_deen_id := kgc_decl_00.deen_id
               ( p_entiteit => p_entiteit
               , p_id => p_id
               );

  IF ( v_deen_id IS NOT NULL )
  THEN
    v_return := 'J';
  ELSE
    v_return := 'N';
  END IF;
  RETURN( v_return );
exception
when others then
g_fout := 'declarabel (3): '||sqlerrm;
raise;
END declarabel;

PROCEDURE  bepaal_waarden
( p_entiteit IN VARCHAR2
, p_id IN NUMBER
, x_declareren IN OUT VARCHAR2
, x_datum IN OUT DATE
, x_kafd_id IN OUT NUMBER
, x_ongr_id IN OUT NUMBER
, x_pers_id IN OUT NUMBER
, x_deen_id IN OUT NUMBER
, x_dewy_id IN OUT NUMBER
, x_ingr_id IN OUT NUMBER
, x_verz_id IN OUT NUMBER
, x_fare_id IN OUT NUMBER
, x_rnde_id IN OUT NUMBER
, x_betreft_tekst IN OUT VARCHAR2
, x_toelichting IN OUT VARCHAR2
, x_verrichtingcode IN OUT VARCHAR2
, x_nota_adres IN OUT VARCHAR2
, x_verzekeringswijze IN OUT VARCHAR2
, x_verzekeringsnr IN OUT VARCHAR2
, x_pers_id_alt_decl IN OUT NUMBER
)
IS
  CURSOR dewy_cur
  ( b_kafd_id IN NUMBER
  , b_code IN VARCHAR2
  )
  IS
    SELECT dewy_id
    FROM   kgc_declaratiewijzen
    WHERE  kafd_id = b_kafd_id
    AND    code = b_code
    ;
  CURSOR pers_cur
  ( b_pers_id IN NUMBER
  )
  IS
    SELECT verz_id
    ,      verzekeringswijze
    ,      verzekeringsnr
    FROM   kgc_personen
    WHERE  pers_id = b_pers_id
    ;
  pers_rec pers_cur%rowtype;

  CURSOR onde_cur
  ( b_onde_id IN NUMBER
  )
  IS
    SELECT kafd_id
    ,      ongr_id
    ,      pers_id
    ,      verz_id
    ,      dewy_id
    ,      fare_id
    ,      datum_binnen
    ,      declareren
    ,      nota_adres
    ,      pers_id_alt_decl
    FROM   kgc_onderzoeken
    WHERE  onde_id = b_onde_id
    ;
  onde_rec onde_cur%rowtype;


  -- YAR 24-04-2006, selectie van fare_id uitgecommentarieerd
  CURSOR onbe_cur
  ( b_onbe_id IN NUMBER
  )
  IS
    SELECT onde_id
    ,      pers_id
    ,      rnde_id
--    ,      fare_id
    ,      declareren
    FROM   kgc_onderzoek_betrokkenen
    WHERE  onbe_id = b_onbe_id
    ;
  onbe_rec onbe_cur%rowtype;
  CURSOR mons_cur
  ( b_mons_id IN NUMBER
  )
  IS
    SELECT kafd_id
    ,      ongr_id
    ,      pers_id
    ,      datum_aanmelding
    ,      declareren
    FROM   kgc_monsters
    WHERE  mons_id = b_mons_id
    ;
  mons_rec mons_cur%rowtype;
  CURSOR meti_cur
  ( b_meti_id IN NUMBER
  )
  IS
    SELECT meti.onde_id
    ,      meti.declareren
    FROM   bas_metingen meti
    WHERE  meti.onde_id IS NOT NULL
    AND    meti.meti_id = b_meti_id
    ;
  meti_rec meti_cur%rowtype;
  CURSOR gesp_cur
  ( b_gesp_id IN NUMBER
  )
  IS
    SELECT gesp.onde_id
    ,      gesp.ongr_id
    ,      gebe.persoon_rol
    ,      gebe.pers_id
    ,      gesp.declareren
    FROM   kgc_gesprek_betrokkenen gebe
    ,      kgc_gesprekken gesp
    WHERE  gebe.gesp_id = gesp.gesp_id
    AND    gebe.pers_id IS NOT NULL
    AND    gebe.declaratie = 'J'
    AND    gesp.gesp_id = b_gesp_id
    union
    SELECT gesp.onde_id
    ,      gesp.ongr_id
    ,      null
    ,      onde.pers_id
    ,      gesp.declareren
    FROM   kgc_onderzoeken onde
    ,      kgc_gesprekken gesp
    WHERE  onde.onde_id = gesp.onde_id
    AND    gesp.gesp_id = b_gesp_id
    and    not exists
           ( select null
             from   kgc_gesprek_betrokkenen gebe
             where  gebe.gesp_id = gesp.gesp_id
             AND    gebe.pers_id IS NOT NULL
             AND    gebe.declaratie = 'J'
           )
    ORDER BY 3 asc -- rol_persoon
    ;
  gesp_rec gesp_cur%rowtype;
  CURSOR deen_cur
  ( b_deen_id in number
  )
  IS
    SELECT dewy_id
    ,      ingr_id
    ,      toelichting
    ,      betreft_tekst
    ,      verrichtingcode
    from   kgc_decl_entiteiten
    where  deen_id = b_deen_id
    ;
  deen_rec deen_cur%rowtype;

  PROCEDURE vul
  ( x_doel IN OUT VARCHAR2
  , p_bron IN VARCHAR2
  )
  IS
  -- alleen als het doel nog leeg is, mag het worden gevuld: geen al gevonden waarden overschrijven
  -- in het hoofdprogramma staat de juiste volgorde!
  BEGIN
    IF ( x_doel IS NULL )
    THEN
      x_doel := p_bron;
    END IF;
  END vul;

BEGIN
  IF ( x_deen_id is not null )
  THEN
    open  deen_cur( x_deen_id );
    fetch deen_cur
    into  x_dewy_id
    ,     x_ingr_id
    ,     x_toelichting
    ,     x_betreft_tekst
    ,     x_verrichtingcode;
    close deen_cur;
  END IF;
  -- haal afgeleide gegevens van entiteiten op
  IF ( p_entiteit = 'ONDE' )
  THEN
    OPEN  onde_cur( b_onde_id => p_id );
    FETCH onde_cur
    INTO  onde_rec;
    CLOSE onde_cur;
    vul( x_kafd_id , onde_rec.kafd_id );
    vul( x_ongr_id , onde_rec.ongr_id );
    vul( x_pers_id , onde_rec.pers_id );
    vul( x_dewy_id , onde_rec.dewy_id );
    if ( x_ingr_id is null )
    then
      vul( x_ingr_id , ingr_id( p_onde_id => p_id ) );
    end if;
    vul( x_fare_id , onde_rec.fare_id );
--  mantis 1268 later nadat alt persoon is uitgesloten
--    vul( x_verz_id , onde_rec.verz_id );
    vul( x_datum, onde_rec.datum_binnen );
    vul( x_declareren, onde_rec.declareren );
    vul( x_pers_id_alt_decl , onde_rec.pers_id_alt_decl );
  ELSIF ( p_entiteit = 'ONBE' )
  THEN
    OPEN  onbe_cur( b_onbe_id => p_id );
    FETCH onbe_cur
    INTO  onbe_rec;
    CLOSE onbe_cur;
    vul( x_pers_id , onbe_rec.pers_id );
-- YAR 24-04-2006, selectie van fare_id uitgecommentarieerd
--    vul( x_fare_id , onbe_rec.fare_id );
    vul( x_rnde_id , onbe_rec.rnde_id );
    if ( x_ingr_id is null )
    then
      vul( x_ingr_id , ingr_id( p_onde_id => onbe_rec.onde_id ) );
    end if;
    vul( x_declareren, onbe_rec.declareren );
    -- uit onderzoek
    OPEN  onde_cur( b_onde_id => onbe_rec.onde_id );
    FETCH onde_cur
    INTO  onde_rec;
    CLOSE onde_cur;
    vul( x_kafd_id , onde_rec.kafd_id );
    vul( x_ongr_id , onde_rec.ongr_id );
    vul( x_dewy_id , onde_rec.dewy_id );
--  mantis 1268 later nadat alt persoon is uitgesloten
--    vul( x_verz_id , onde_rec.verz_id );
    vul( x_datum, onde_rec.datum_binnen );
    vul( x_pers_id_alt_decl , onde_rec.pers_id_alt_decl );
  ELSIF ( p_entiteit = 'MONS' )
  THEN
    OPEN  mons_cur( b_mons_id => p_id );
    FETCH mons_cur
    INTO  mons_rec;
    CLOSE mons_cur;
    vul( x_pers_id , mons_rec.pers_id );
    vul( x_kafd_id , mons_rec.kafd_id );
    vul( x_ongr_id , mons_rec.ongr_id );
    if ( x_ingr_id is null )
    then
      vul( x_ingr_id , ingr_id ( p_mons_id => p_id ) );
    end if;
    vul( x_declareren, mons_rec.declareren );
    vul( x_datum, mons_rec.datum_aanmelding );
  ELSIF ( p_entiteit = 'METI' )
  THEN
    OPEN  meti_cur( b_meti_id => p_id );
    FETCH meti_cur
    INTO  meti_rec;
    CLOSE meti_cur;
    vul( x_declareren, meti_rec.declareren );
    if ( x_ingr_id is null )
    then
      vul( x_ingr_id , ingr_id ( p_meti_id => p_id, p_onde_id => meti_rec.onde_id ) );
    end if;
    -- uit onderzoek
    OPEN  onde_cur( b_onde_id => meti_rec.onde_id );
    FETCH onde_cur
    INTO  onde_rec;
    CLOSE onde_cur;
    vul( x_kafd_id , onde_rec.kafd_id );
    vul( x_ongr_id , onde_rec.ongr_id );
    vul( x_pers_id , onde_rec.pers_id );
    vul( x_dewy_id , onde_rec.dewy_id );
--  mantis 1268 later nadat alt persoon is uitgesloten
--    vul( x_verz_id , onde_rec.verz_id );
    vul( x_fare_id , onde_rec.fare_id );
    vul( x_datum, onde_rec.datum_binnen );
    vul( x_pers_id_alt_decl , onde_rec.pers_id_alt_decl );
  ELSIF ( p_entiteit = 'GESP' )
  THEN
    OPEN  gesp_cur( b_gesp_id => p_id );
    FETCH gesp_cur
    INTO  gesp_rec;
    CLOSE gesp_cur;
    vul( x_ongr_id , gesp_rec.ongr_id ); -- onderzoeksgroep bij gesprek kan afwijken van onderzoek!
    vul( x_pers_id , gesp_rec.pers_id );
    if ( x_ingr_id is null )
    then
      vul( x_ingr_id , ingr_id( p_onde_id => gesp_rec.onde_id ) );
    end if;
    vul( x_declareren, gesp_rec.declareren );
    -- uit onderzoek
    OPEN  onde_cur( b_onde_id => gesp_rec.onde_id );
    FETCH onde_cur
    INTO  onde_rec;
    CLOSE onde_cur;
    vul( x_kafd_id , onde_rec.kafd_id );
    vul( x_dewy_id , onde_rec.dewy_id );
--  mantis 1268 later nadat alt persoon is uitgesloten
--    vul( x_verz_id , onde_rec.verz_id );
    vul( x_datum, onde_rec.datum_binnen );
    vul( x_pers_id_alt_decl , onde_rec.pers_id_alt_decl );
  END IF;

  IF ( x_datum IS NULL )
  THEN
    x_datum := sysdate;
  END IF;
  x_datum := trunc( x_datum );

  IF ( x_dewy_id IS NULL )
  THEN
    OPEN  dewy_cur( x_kafd_id
                  , kgc_sypa_00.standaard_waarde
                    ( p_parameter_code => 'STANDAARD_DECLARATIEWIJZE'
                    , p_kafd_id => x_kafd_id
                    , p_ongr_id => x_ongr_id
                    )
                   );
    FETCH dewy_cur
    INTO  x_dewy_id;
    CLOSE dewy_cur;
    IF ( x_dewy_id IS NULL )
    THEN
      x_dewy_id := binnenland( p_kafd_id => x_kafd_id );
    END IF;
  END IF;

  -- Mantis 1268: Afhandeling alt persoon
  IF (x_pers_id_alt_decl IS NULL)
  THEN
    -- geen alternatief persoon => eerst verz id uit onde overnemen
    vul( x_verz_id , onde_rec.verz_id );

    IF ( x_pers_id IS NOT NULL )
    THEN
      OPEN  pers_cur( x_pers_id );
      FETCH pers_cur
      INTO  pers_rec;
      CLOSE pers_cur;
      vul( x_verz_id, pers_rec.verz_id );
      vul( x_verzekeringswijze, pers_rec.verzekeringswijze );
      vul( x_verzekeringsnr, pers_rec.verzekeringsnr );
    END IF;

  ELSE
    -- alternatief persoon
    OPEN  pers_cur( x_pers_id_alt_decl );
    FETCH pers_cur
    INTO  pers_rec;
    CLOSE pers_cur;
    vul( x_verz_id, pers_rec.verz_id );
    vul( x_verzekeringswijze, pers_rec.verzekeringswijze );
    vul( x_verzekeringsnr, pers_rec.verzekeringsnr );
  END IF;

  IF ( onde_rec.nota_adres IS NOT NULL )
  THEN
    x_nota_adres := onde_rec.nota_adres;
  ELSIF ( x_verz_id IS NOT NULL
   AND x_nota_adres IS NULL
   )
  THEN
    x_nota_adres := kgc_adres_00.verzekeraar( p_verz_id  => x_verz_id
                                            , p_met_naam => 'J'
                                            );
  END IF;

  if ( x_verrichtingcode is null )
  then
    x_verrichtingcode := verrichtingcode
                          ( p_entiteit => p_entiteit
                          , p_id => p_id
                          );
  end if;
exception
when others then
g_fout := 'bepaal_waarden: '||sqlerrm;
raise;
END bepaal_waarden;

PROCEDURE wijzig_declaratie
( p_decl_trow_nieuw IN OUT kgc_declaraties%rowtype
, p_soort_gegevens IN VARCHAR2 := NULL -- PERS, VERZ, INGR, ONBE
)
IS
  -- huidige waarden
  CURSOR decl_cur
  IS
    SELECT decl_id
    ,      entiteit
    ,      datum
    ,      status
    ,      declareren
    ,      deen_id
    ,      kafd_id
    ,      ongr_id
    ,      pers_id
    ,      dewy_id
    ,      ingr_id
    ,      verz_id
    ,      fare_id
    ,      rnde_id
    ,      toelichting
    ,      betreft_tekst
    ,      verrichtingcode
    ,      nota_adres
    ,      verzekeringswijze
    ,      verzekeringsnr
    ,      pers_id_alt_decl
    FROM   kgc_declaraties
    WHERE  entiteit = p_decl_trow_nieuw.entiteit
    AND    id = p_decl_trow_nieuw.id
    ;
  decl_rec decl_cur%rowtype;
  v_update BOOLEAN := FALSE;

  cursor onde_cur
  ( b_entiteit in varchar2
  , b_id in number
  )
  is
    select onde.nota_adres
    from   kgc_onderzoeken onde
    where  b_entiteit = 'ONDE'
    and    onde.onde_id = b_id
    union
    select onde.nota_adres
    from   kgc_onderzoeken onde
    ,      kgc_onderzoek_betrokkenen onbe
    where  onbe.onde_id = onde.onde_id
    and    b_entiteit = 'ONBE'
    and    onbe.onbe_id = b_id
    union
    select onde.nota_adres
    from   kgc_onderzoeken onde
    ,      bas_metingen meti
    where  meti.onde_id = onde.onde_id
    and    b_entiteit = 'METI'
    and    meti.meti_id = b_id
    union
    select onde.nota_adres
    from   kgc_onderzoeken onde
    ,      kgc_gesprekken gesp
    where  gesp.onde_id = onde.onde_id
    and    b_entiteit = 'GESP'
    and    gesp.gesp_id = b_id
    ;
  PROCEDURE wijzigen
  ( b_huidig IN VARCHAR2
  , b_nieuw IN OUT VARCHAR2
  , b_verplicht IN BOOLEAN := FALSE
  )
  IS
  BEGIN
    IF ( b_verplicht
     AND b_nieuw IS NULL
       )
    THEN
      b_nieuw := b_huidig;
    END IF;
    IF ( NVL( b_nieuw, CHR(2) ) <> NVL( b_huidig, CHR(2) ) )
    THEN
      v_update := TRUE;
    END IF;
  END wijzigen;
BEGIN
  OPEN  decl_cur;
  FETCH decl_cur
  INTO  decl_rec;
  CLOSE decl_cur;

  IF  ( decl_rec.status in ( 'V', 'X' ) ) -- was tot 10-07-2006:  IS NOT NULL
  THEN
    RETURN; -- niet meer wijzigen!
  END IF;

  IF ( NVL( p_soort_gegevens, 'alles' ) IN ( 'alles' ) )
  THEN
    if ( decl_rec.status = 'H' )
    then
      null; -- het al dan niet declareren is handmatig gezet (bewust tegen de criteria in)
    else
      wijzigen( decl_rec.declareren, p_decl_trow_nieuw.declareren, TRUE );
    end if;
    wijzigen( decl_rec.deen_id, p_decl_trow_nieuw.deen_id, TRUE );
    wijzigen( decl_rec.kafd_id, p_decl_trow_nieuw.kafd_id, TRUE );
    wijzigen( decl_rec.ongr_id, p_decl_trow_nieuw.ongr_id, TRUE );
    wijzigen( decl_rec.pers_id, p_decl_trow_nieuw.pers_id, TRUE );
    wijzigen( decl_rec.dewy_id, p_decl_trow_nieuw.dewy_id, TRUE );
    wijzigen( decl_rec.rnde_id, p_decl_trow_nieuw.rnde_id );
    wijzigen( decl_rec.datum, p_decl_trow_nieuw.datum );
    wijzigen( decl_rec.toelichting, p_decl_trow_nieuw.toelichting );
    wijzigen( decl_rec.betreft_tekst, p_decl_trow_nieuw.betreft_tekst );
    wijzigen( decl_rec.verrichtingcode, p_decl_trow_nieuw.verrichtingcode );
    wijzigen( decl_rec.pers_id_alt_decl, p_decl_trow_nieuw.pers_id_alt_decl );
  ELSE
   p_decl_trow_nieuw.declareren       := decl_rec.declareren;
   p_decl_trow_nieuw.deen_id          := decl_rec.deen_id; -- Mantis 2405, toegevoegd
   p_decl_trow_nieuw.kafd_id          := decl_rec.kafd_id;
   p_decl_trow_nieuw.ongr_id          := decl_rec.ongr_id;
   p_decl_trow_nieuw.pers_id          := decl_rec.pers_id;
   p_decl_trow_nieuw.dewy_id          := decl_rec.dewy_id;
   p_decl_trow_nieuw.rnde_id          := decl_rec.rnde_id;
   p_decl_trow_nieuw.datum            := decl_rec.datum;
   p_decl_trow_nieuw.toelichting      := decl_rec.toelichting;
   p_decl_trow_nieuw.betreft_tekst    := decl_rec.betreft_tekst;
   p_decl_trow_nieuw.verrichtingcode  := decl_rec.verrichtingcode;
   p_decl_trow_nieuw.pers_id_alt_decl := decl_rec.pers_id_alt_decl;
  END IF;
  IF ( NVL( p_soort_gegevens, 'alles' ) IN ( 'alles', 'INGR' ) )
  THEN
    wijzigen( decl_rec.ingr_id, p_decl_trow_nieuw.ingr_id );
  ELSE
    p_decl_trow_nieuw.ingr_id := decl_rec.ingr_id;
  END IF;
/* fare_id is uit ONBE verwijderd!
  IF ( NVL( p_soort_gegevens, 'alles' ) IN ( 'alles', 'ONBE' ) )
  THEN
    wijzigen( decl_rec.fare_id, p_decl_trow_nieuw.fare_id );
  ELSE
    p_decl_trow_nieuw.fare_id := decl_rec.fare_id;
  END IF;
*/
  IF ( NVL( p_soort_gegevens, 'alles' ) IN ( 'alles', 'PERS' ) )
  THEN
    wijzigen( decl_rec.verz_id, p_decl_trow_nieuw.verz_id );
    wijzigen( decl_rec.verzekeringswijze, p_decl_trow_nieuw.verzekeringswijze );
    wijzigen( decl_rec.verzekeringsnr, p_decl_trow_nieuw.verzekeringsnr );
  ELSE
    p_decl_trow_nieuw.verz_id := decl_rec.verz_id;
    p_decl_trow_nieuw.verzekeringswijze := decl_rec.verzekeringswijze;
    p_decl_trow_nieuw.verzekeringsnr := decl_rec.verzekeringsnr;
  END IF;

  IF ( NVL( p_soort_gegevens, 'alles' ) IN ( 'alles', 'PERS', 'VERZ' ) )
  THEN
    IF ( p_decl_trow_nieuw.nota_adres IS NULL )
    THEN
      if ( p_decl_trow_nieuw.entiteit in ( 'ONDE', 'ONBE', 'GESP' ) )
      then
        open  onde_cur( b_entiteit => p_decl_trow_nieuw.entiteit
                      , b_id => p_decl_trow_nieuw.id
                      );
        fetch onde_cur
        into  p_decl_trow_nieuw.nota_adres;
        close onde_cur;
      end if;
      IF ( p_decl_trow_nieuw.nota_adres IS NULL )
      THEN
        p_decl_trow_nieuw.nota_adres := kgc_adres_00.verzekeraar
                                        ( p_verz_id => p_decl_trow_nieuw.verz_id
                                        , p_met_naam => 'J'
                                        );
      END IF;
    END IF;
    wijzigen( decl_rec.nota_adres, p_decl_trow_nieuw.nota_adres );
  ELSE
    p_decl_trow_nieuw.nota_adres := decl_rec.nota_adres;
  END IF;

  IF ( v_update )
  THEN
    UPDATE kgc_declaraties
    SET    declareren = p_decl_trow_nieuw.declareren
    ,      deen_id = p_decl_trow_nieuw.deen_id
    ,      datum = p_decl_trow_nieuw.datum
--    ,      kafd_id = p_decl_trow_nieuw.kafd_id
    ,      ongr_id = p_decl_trow_nieuw.ongr_id
    ,      pers_id = p_decl_trow_nieuw.pers_id
    ,      dewy_id = p_decl_trow_nieuw.dewy_id
    ,      ingr_id = p_decl_trow_nieuw.ingr_id
--    ,      fare_id = p_decl_trow_nieuw.fare_id
    ,      rnde_id = p_decl_trow_nieuw.rnde_id
    ,      toelichting = p_decl_trow_nieuw.toelichting
    ,      betreft_tekst = p_decl_trow_nieuw.betreft_tekst
    ,      verrichtingcode = p_decl_trow_nieuw.verrichtingcode
    ,      verz_id = p_decl_trow_nieuw.verz_id
    ,      verzekeringswijze = p_decl_trow_nieuw.verzekeringswijze
    ,      verzekeringsnr = p_decl_trow_nieuw.verzekeringsnr
    ,      nota_adres = p_decl_trow_nieuw.nota_adres
    ,      pers_id_alt_decl = p_decl_trow_nieuw.pers_id_alt_decl
    WHERE  decl_id = decl_rec.decl_id
    ;
  END IF;
exception
when others then
g_fout := 'wijzig_declaratie: '||sqlerrm;
raise;
END wijzig_declaratie;

FUNCTION standaard_declareren
( p_actie IN VARCHAR2 -- I/U/D
, p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_declareren IN VARCHAR2 := NULL
)
RETURN   VARCHAR2
IS
  CURSOR decl_cur
  IS
    SELECT *
    FROM   kgc_declaraties
    WHERE  entiteit = p_entiteit
    AND    id = p_id
    ;
  decl_rec decl_cur%rowtype;
  decl_trow kgc_declaraties%rowtype;

  v_declareren VARCHAR2(1);
BEGIN
  g_actuele_decl_id := null;
  zet_verwerkte_rij( p_entiteit, p_id );
  IF ( p_actie = 'I' )
  THEN
    decl_trow.deen_id := kgc_decl_00.deen_id
                         ( p_entiteit => p_entiteit
                         , p_id => p_id
                         );
    if ( p_declareren = 'J' )
    then
      v_declareren := 'J';
    else -- N is de defaultwaarde voor kolom declareren.
      if ( decl_trow.deen_id is not null )
      then
        v_declareren := 'J';
      else
        v_declareren := 'N';
      end if;
    end if;

    declareren
    ( p_entiteit => p_entiteit
    , p_id => p_id
    , p_deen_id => decl_trow.deen_id
    , p_declareren => v_declareren
    );
    afhankelijke_declaraties
    ( p_entiteit => p_entiteit
    , p_id => p_id
    );
    schoon_verwerkte_rij( p_entiteit, p_id );
    RETURN( NVL( v_declareren, 'N' ) );
  ELSIF ( p_actie = 'U' )
  THEN
    OPEN  decl_cur;
    FETCH decl_cur
    INTO  decl_rec;
    CLOSE decl_cur;
    zet_actuele_decl_id( p_decl_id => decl_rec.decl_id );
    if ( nvl( decl_rec.status, '-' ) in ( '-', 'H', 'T', 'R' ) )-- is null )
    then
      decl_trow.entiteit := p_entiteit;
      decl_trow.id := p_id;
      decl_trow.deen_id := kgc_decl_00.deen_id
                           ( p_entiteit => p_entiteit
                           , p_id => p_id
                           );
      if ( decl_trow.deen_id is not null )
      then
        if ( decl_rec.status = 'H' )
        then
          decl_trow.declareren := decl_rec.declareren; -- blijft onveranderd.
        else
          decl_trow.declareren := 'J';
        end if;
        decl_trow.dewy_id := decl_rec.dewy_id; -- niet meer wijzigen vanuit een entiteit (ONDE)
        bepaal_waarden
        ( p_entiteit => decl_trow.entiteit
        , p_id => decl_trow.id
        , x_declareren => decl_trow.declareren
        , x_datum => decl_trow.datum
        , x_kafd_id => decl_trow.kafd_id
        , x_ongr_id => decl_trow.ongr_id
        , x_pers_id => decl_trow.pers_id
        , x_deen_id => decl_trow.deen_id
        , x_dewy_id => decl_trow.dewy_id
        , x_ingr_id => decl_trow.ingr_id
        , x_verz_id => decl_trow.verz_id
        , x_fare_id => decl_trow.fare_id
        , x_rnde_id => decl_trow.rnde_id
        , x_betreft_tekst => decl_trow.betreft_tekst
        , x_toelichting => decl_trow.toelichting
        , x_verrichtingcode => decl_trow.verrichtingcode
        , x_nota_adres => decl_trow.nota_adres
        , x_verzekeringswijze => decl_trow.verzekeringswijze
        , x_verzekeringsnr => decl_trow.verzekeringsnr
        , x_pers_id_alt_decl => decl_trow.pers_id_alt_decl
        );
        IF ( decl_rec.decl_id IS NOT NULL )
        THEN
          wijzig_declaratie
          ( p_decl_trow_nieuw => decl_trow
          , p_soort_gegevens => NULL -- alles
          );
        ELSE -- alsnog record aanmaken (niet-standaard, achteraf toch declareren)
          declareren
          ( p_entiteit => p_entiteit
          , p_id => p_id
          , p_declareren => 'J'
          , p_deen_id => decl_trow.deen_id
          );
        END IF;
      else
        if ( decl_rec.status = 'H' )
        then
          decl_trow.declareren := decl_rec.declareren; -- blijft onveranderd.
        else
          decl_trow.declareren := 'N';
          reset_declaratie( p_decl_id => decl_rec.decl_id );
          vul_reden_niet_declarabel( p_decl_id => decl_rec.decl_id );
        end if;
      END IF;
    END IF;
    afhankelijke_declaraties
    ( p_entiteit => p_entiteit
    , p_id => p_id
    );
    schoon_verwerkte_rij( p_entiteit, p_id );
    RETURN( decl_trow.declareren );
  ELSIF ( p_actie = 'D' )
  THEN
    DELETE FROM kgc_declaraties
    WHERE  entiteit = p_entiteit
    AND    id = p_id
    ;
    schoon_verwerkte_rij( p_entiteit, p_id );
    RETURN( NULL );
  END IF;
exception
when others then
g_fout := 'standaard_declareren: actie='||p_actie
||chr(10)||'enti='||p_entiteit||chr(10)||'id='||p_id
||chr(10)||'decl='||p_declareren||chr(10)||sqlerrm;
raise;
END standaard_declareren;
PROCEDURE standaard_declareren
( p_actie IN VARCHAR2 -- I/U/D
, p_entiteit IN VARCHAR2
, p_id IN NUMBER
, p_declareren IN VARCHAR2 := NULL
)
IS
  v_declareren VARCHAR2(1);
BEGIN
  v_declareren := standaard_declareren
                  ( p_actie => p_actie
                  , p_entiteit => p_entiteit
                  , p_id => p_id
                  , p_declareren => p_declareren
                  );
END standaard_declareren;

FUNCTION binnenland
( p_kafd_id IN NUMBER
)
RETURN NUMBER
IS
  v_return NUMBER;
  CURSOR c
  IS
    SELECT dewy_id
    FROM   kgc_declaratiewijzen
    WHERE  kafd_id = p_kafd_id
    AND    code = 'BI'
    ;
BEGIN
  OPEN  c;
  FETCH c
  INTO  v_return;
  CLOSE c;
  RETURN( v_return );
END binnenland;

FUNCTION ingr_id
( p_onde_id IN NUMBER := NULL
, p_mons_id IN NUMBER := NULL
, p_meti_id IN NUMBER := NULL
, p_update_declaratie IN BOOLEAN := FALSE
)
RETURN NUMBER
IS
  v_return NUMBER := NULL;
  CURSOR moin_cur
  IS
    SELECT indi.ingr_id
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_indicatie_teksten indi
    ,      kgc_monster_indicaties moin
    WHERE  moin.indi_id = indi.indi_id
    AND    indi.ingr_id = ingr.ingr_id
    AND    ingr.machtigingsindicatie IS NOT NULL
    AND    moin.mons_id = p_mons_id
    ORDER BY moin.moin_id DESC -- laatste eerst
    ;
  CURSOR onmo_cur
  IS
    SELECT onmo.onde_id onde_id
    FROM   kgc_onderzoek_monsters onmo
    WHERE  onmo.mons_id = p_mons_id
    ORDER BY onmo.onmo_id DESC -- laatste eerst
    ;
  v_onde_id NUMBER := NULL;
  CURSOR onbe_cur
  IS
    SELECT onbe_id
    FROM   kgc_onderzoek_betrokkenen
    WHERE  onde_id = p_onde_id
    ;
  CURSOR meti_cur
  IS
    SELECT meti_id
    FROM   bas_metingen
    WHERE  onde_id = p_onde_id
    ;
  CURSOR mons_cur
  IS
    SELECT mons_id
    FROM   kgc_onderzoek_monsters
    WHERE  onde_id = p_onde_id
    ;
  CURSOR gesp_cur
  IS
    SELECT gesp_id
    FROM   kgc_gesprekken
    WHERE  onde_id = p_onde_id
    ;
  decl_trow_null kgc_declaraties%rowtype;
  decl_trow_oud kgc_declaraties%rowtype;
  decl_trow_nieuw kgc_declaraties%rowtype;

-- Grotendeels/geheel vervallen functionaliteit: standaard ingr_id is opgenomen in kgc_decl_entiteiten
  -- declarabele FISH onderzoeken (cyto-metingen) hebben een
  -- vaste machtigingsindicatie, afwijkend van het bijbehorende onderzoek
  -- Postnataal: code 3.
  -- Tumoren: code 11.
  -- Prenataal: code 13.
  -- Analoog: monsterafnames krijgen ook een eigen machtigingsindicatie (?)
  -- Zie systeemparameter STANDAARD_FISH_INDICATIE,  STANDAARD_AFNAME_INDICATIE
  FUNCTION standaard_machtigingsindicatie
  ( b_meti_id in number := null
  , b_mons_id in number := null
  )
  RETURN NUMBER
  IS
    l_ingr_id NUMBER; -- return waarde
    l_kafd_id NUMBER;
    l_ongr_id NUMBER;
    l_ingr_code VARCHAR2(10);
    CURSOR l_mons_cur
    IS
      SELECT kafd_id
      ,      ongr_id
      FROM   kgc_monsters
      WHERE  mons_id = b_mons_id
      ;
    CURSOR l_meti_cur
    IS
      SELECT onde.kafd_id
      ,      onde.ongr_id
      FROM   kgc_onderzoeken onde
      ,      bas_metingen meti
      WHERE  meti.onde_id = onde.onde_id
      AND    meti.meti_id = b_meti_id
      ;
    CURSOR ingr_cur
    ( b_ingr_code IN VARCHAR2
    )
    IS
      SELECT ingr.ingr_id
      FROM   kgc_onderzoeksgroepen ongr
      ,      kgc_indicatie_groepen ingr
      WHERE  ongr.ongr_id = ingr.ongr_id
      AND    ongr.ongr_id = l_ongr_id
      AND    ingr.code = b_ingr_code
      AND    ingr.machtigingsindicatie IS NOT NULL
      ;
  BEGIN
    IF ( b_meti_id IS NOT NULL )
    THEN
      OPEN  l_meti_cur;
      FETCH l_meti_cur
      INTO  l_kafd_id
      ,     l_ongr_id;
      CLOSE l_meti_cur;
      l_ingr_code := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'STANDAARD_FISH_INDICATIE'
                     , p_kafd_id => l_kafd_id
                     , p_ongr_id => l_ongr_id
                     );
    ELSIF ( b_mons_id IS NOT NULL )
    THEN
      OPEN  l_mons_cur;
      FETCH l_mons_cur
      INTO  l_kafd_id
      ,     l_ongr_id;
      CLOSE l_mons_cur;
      l_ingr_code := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'STANDAARD_AFNAME_INDICATIE'
                     , p_kafd_id => l_kafd_id
                     , p_ongr_id => l_ongr_id
                     );
    END IF;
    IF ( l_ingr_code IS NOT NULL )
    THEN
      OPEN  ingr_cur( b_ingr_code => l_ingr_code
                    );
      FETCH ingr_cur
      INTO  l_ingr_id;
      CLOSE ingr_cur;
    END IF;
    RETURN( l_ingr_id );
  END standaard_machtigingsindicatie;

BEGIN
  IF ( p_meti_id IS NOT NULL )
  THEN
    -- standaard?
    v_return := standaard_machtigingsindicatie( b_meti_id => p_meti_id );
    IF ( v_return IS NULL )
    THEN
      v_return := kgc_indi_00.ingr_id( p_onde_id => p_onde_id );
    END IF;
    IF ( p_update_declaratie )
    THEN
      g_forceer_decl_bepaling := TRUE;
      bepaal_opnieuw
      ( p_entiteit => 'METI'
      , p_id => p_meti_id
      );
      g_forceer_decl_bepaling := FALSE;
    END IF;
  ELSIF ( p_mons_id IS NOT NULL )
  THEN
    -- standaard?
    v_return := standaard_machtigingsindicatie( b_mons_id => p_mons_id );
    IF ( v_return IS NULL )
    THEN
      -- uit monster-indicatie
      OPEN  moin_cur;
      FETCH moin_cur
      INTO  v_return;
      CLOSE moin_cur;
      -- of uit onderzoek-indicatie
      IF ( v_return IS NULL )
      THEN
        OPEN  onmo_cur;
        FETCH onmo_cur
        INTO  v_onde_id;
        CLOSE onmo_cur;
        IF ( v_onde_id IS NOT NULL )
        THEN
          v_return := kgc_indi_00.ingr_id( p_onde_id => v_onde_id );
        END IF;
      END IF;
    END IF;
    IF ( p_update_declaratie )
    THEN
      g_forceer_decl_bepaling := TRUE;
      bepaal_opnieuw
      ( p_entiteit => 'MONS'
      , p_id => p_mons_id
      );
      g_forceer_decl_bepaling := FALSE;
    END IF;
  ELSIF ( p_onde_id IS NOT NULL )
  THEN
    v_return := kgc_indi_00.ingr_id( p_onde_id => p_onde_id );
    IF ( p_update_declaratie )
    THEN
-- bepaal onderzoek opnieuw:
      g_forceer_decl_bepaling := TRUE;
      bepaal_opnieuw
      ( p_entiteit => 'ONDE'
      , p_id => p_onde_id
      );
      g_forceer_decl_bepaling := FALSE;
/*
      -- van betrokkenen
      FOR r IN onbe_cur
      LOOP
        g_forceer_decl_bepaling := TRUE;
        bepaal_opnieuw
        ( p_entiteit => 'ONBE'
        , p_id => r.onbe_id
        );
        g_forceer_decl_bepaling := FALSE;
      END LOOP;
      -- van metingen / deelonderzoeken
      FOR r IN meti_cur
      LOOP
        g_forceer_decl_bepaling := TRUE;
        bepaal_opnieuw
        ( p_entiteit => 'METI'
        , p_id => r.meti_id
        );
        g_forceer_decl_bepaling := FALSE;
      END LOOP;
      -- van monsters (eventueel, als geen monster-indicatie)
      FOR r IN mons_cur
      LOOP
        g_forceer_decl_bepaling := TRUE;
        bepaal_opnieuw
        ( p_entiteit => 'MONS'
        , p_id => r.mons_id
        );
        g_forceer_decl_bepaling := FALSE;
      END LOOP;
      -- voor gesprekken
      FOR r IN gesp_cur
      LOOP
        g_forceer_decl_bepaling := TRUE;
        bepaal_opnieuw
        ( p_entiteit => 'GESP'
        , p_id => r.gesp_id
        );
        g_forceer_decl_bepaling := FALSE;
      END LOOP;
*/
    END IF;
  END IF;
  RETURN( v_return );
exception
when others then
g_fout := 'ingr: '||sqlerrm;
raise;
END ingr_id;

FUNCTION machtigingsindicatie
( p_decl_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  CURSOR ingr_cur
  IS
    SELECT ingr.machtigingsindicatie
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_declaraties decl
    WHERE  decl.ingr_id = ingr.ingr_id
    AND    decl.decl_id = p_decl_id
    ;
BEGIN
  IF ( p_decl_id IS NOT NULL )
  THEN
    OPEN  ingr_cur;
    FETCH ingr_cur
    INTO  v_return;
    CLOSE ingr_cur;
  END IF;
  RETURN( v_return );
END machtigingsindicatie;

FUNCTION decl_identificatie
( p_decl_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);

  CURSOR decl_cur
  IS
    SELECT entiteit
    ,      id
    FROM   kgc_declaraties
    WHERE  decl_id = p_decl_id
    ;
  decl_rec decl_cur%rowtype;

  CURSOR onde_cur
  IS
    SELECT onde.onderzoeknr
    FROM   kgc_onderzoeken onde
    WHERE  onde.onde_id = decl_rec.id
    ;
  CURSOR onbe_cur
  IS
    SELECT onde.onderzoeknr
        || decode( onbe.rol, null, null, ' '||onbe.rol )
    FROM   kgc_onderzoeken onde
    ,      kgc_onderzoek_betrokkenen onbe
    WHERE  onbe.onde_id = onde.onde_id
    AND    onbe.onbe_id = decl_rec.id
    ;
  CURSOR mons_cur
  IS
    SELECT mons.monsternummer
    FROM   kgc_monsters mons
    WHERE  mons.mons_id = decl_rec.id
    ;
  CURSOR meti_cur
  IS
    SELECT onde.onderzoeknr
        || DECODE( stgr.code
                 , NULL, NULL
                 , ' '||stgr.code
                 )
    FROM   kgc_stoftestgroepen stgr
    ,      kgc_onderzoeken onde
    ,      bas_metingen meti
    WHERE  meti.onde_id = onde.onde_id
    AND    meti.stgr_id = stgr.stgr_id (+)
    AND    meti.meti_id = decl_rec.id
    ;
  CURSOR gesp_cur
  IS
    SELECT onde.onderzoeknr
    FROM   kgc_onderzoeken onde
    ,      kgc_gesprekken gesp
    WHERE  gesp.onde_id = onde.onde_id
    AND    gesp.gesp_id = decl_rec.id
    ;
BEGIN
  OPEN  decl_cur;
  FETCH decl_cur
  INTO  decl_rec;
  CLOSE decl_cur;
  IF ( decl_rec.entiteit = 'ONDE' )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_return;
    CLOSE onde_cur;
  ELSIF ( decl_rec.entiteit = 'ONBE' )
  THEN
    OPEN  onbe_cur;
    FETCH onbe_cur
    INTO  v_return;
    CLOSE onbe_cur;
  ELSIF ( decl_rec.entiteit = 'MONS' )
  THEN
    OPEN  mons_cur;
    FETCH mons_cur
    INTO  v_return;
    CLOSE mons_cur;
  ELSIF ( decl_rec.entiteit = 'METI' )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  v_return;
    CLOSE meti_cur;
  ELSIF ( decl_rec.entiteit = 'GESP' )
  THEN
    OPEN  gesp_cur;
    FETCH gesp_cur
    INTO  v_return;
    CLOSE gesp_cur;
  END IF;

  RETURN( v_return );
END decl_identificatie;

function forceer_declaratiebepaling
return boolean
is
begin
  if ( g_forceer_decl_bepaling )
  then
    return( g_forceer_decl_bepaling );
  else
    return( g_forceer_tmp_decl_bepaling );
  end if;
exception
  when others
  then
    return( false );
end forceer_declaratiebepaling;

procedure bepaal_declaraties
( p_onde_id in number := null
, p_mons_id in number := null
)
is
  -- verwijder bestaande declaraties (zet op niet-declareren)
  cursor decl_cur
  is
    select decl.decl_id
    from   kgc_declaraties decl
    where  decl.entiteit = 'ONDE'
    and    nvl( decl.status, '-' ) in ( '-', 'T', 'R' ) -- is null
    and    decl.id in
           ( select onde.onde_id
             from   kgc_onderzoeken onde
             where  onde.onde_id = p_onde_id
             and    onde.afgerond = 'N'
           )
    union all
    select decl.decl_id
    from   kgc_declaraties decl
    where  decl.entiteit = 'ONBE'
    and    nvl( decl.status, '-' ) in ( '-', 'T', 'R' ) -- is null
    and    decl.id in
           ( select onbe.onbe_id
             from   kgc_onderzoeken onde
             ,      kgc_onderzoek_betrokkenen onbe
             where  onbe.onde_id = onde.onde_id
             and    onde.afgerond = 'N'
             and    onde.onde_id = p_onde_id
           )
    union all
    select decl.decl_id
    from   kgc_declaraties decl
    where  decl.entiteit = 'METI'
    and    nvl( decl.status, '-' ) in ( '-', 'T', 'R' ) -- is null
    and    decl.id in
           ( select meti.meti_id
             from   kgc_onderzoeken onde
             ,      bas_metingen meti
             where  meti.onde_id = onde.onde_id
             and    onde.afgerond = 'N'
             and    onde.onde_id = p_onde_id
           )
    union all
    select decl.decl_id
    from   kgc_declaraties decl
    where  decl.entiteit = 'GESP'
    and    nvl( decl.status, '-' ) in ( '-', 'T', 'R' ) -- is null
    and    decl.id in
           ( select gesp.gesp_id
             from   kgc_onderzoeken onde
             ,      kgc_gesprekken gesp
             where  gesp.onde_id = onde.onde_id
             and    onde.afgerond = 'N'
             and    onde.onde_id = p_onde_id
           )
    union all
    select decl.decl_id
    from   kgc_declaraties decl
    where  decl.entiteit = 'MONS'
    and    nvl( decl.status, '-' ) in ( '-', 'T', 'R' ) -- is null
    and    decl.id in
           ( select mons.mons_id
             from   kgc_monsters mons
             where  mons.mons_id = p_mons_id
           )
    ;
  -- alleen voor nog niet afgeronde onderzoeken
  cursor onde_cur
  is
    select onde.onde_id
    from   kgc_onderzoeken onde
    where  onde.onde_id = p_onde_id
    and    onde.afgerond = 'N'
    ;
begin
  -- Verwijder eerst de bestaande declaraties ...
  for r in decl_cur
  loop
/*
    delete from kgc_brieven
    where  decl_id = r.decl_id
    ;
    delete from kgc_declaraties
    where  decl_id = r.decl_id
    ;
*/
    reset_declaratie( p_decl_id => r.decl_id );
  end loop;
  commit;
  -- ... en maak ze opnieuw ...
  g_forceer_decl_bepaling := TRUE; -- db-triggers kijken nu niet meer naar wijziging in gegevens
  if ( p_onde_id is not null )
  then
    for r in onde_cur
    loop
      bepaal_opnieuw
      ( p_entiteit => 'ONDE'
      , p_id => p_onde_id
      );
      -- via (ONDE)AUS-trigger, kgc_decl_00.standaard_declareren, kgc_decl_00.afhankelijke_declaraties
      -- worden de ONBE-, METI- en GESP-declaraties gedaan voor dit onderzoek
    end loop;
  elsif ( p_mons_id is not null )
  then
    bepaal_opnieuw
    ( p_entiteit => 'MONS'
    , p_id => p_mons_id
    );
  end if;
  g_forceer_decl_bepaling := FALSE;
  commit;
exception
  when others
  then
    g_forceer_decl_bepaling := FALSE;
    rollback;
-- raise_application_error( -20013, 'Fout: '||g_fout );
    raise;
end bepaal_declaraties;

function machtigingsbrief_nodig
( p_decl_id in number
)
return varchar2
is
  cursor decl_cur
  is
    select entiteit
    ,      kafd_id
    ,      ongr_id
    ,      verz_id
    from   kgc_declaraties
    where  decl_id = p_decl_id
    ;
  decl_rec decl_cur%rowtype;

  cursor verz_cur
  ( b_verz_id in number
  )
  is
    select machtiging_nodig
    from   kgc_verzekeraars
    where  verz_id = b_verz_id
    ;
  verz_rec verz_cur%rowtype;

  v_sypa_waarde varchar2(1000);
begin
  open  decl_cur;
  fetch decl_cur
  into  decl_rec;
  close decl_cur;
  -- pl/sql-table voorkomt overbodige aanroepen
  if ( sypa_macht_nodig_tab.exists( decl_rec.ongr_id ) )
  then
    v_sypa_waarde := sypa_macht_nodig_tab( decl_rec.ongr_id );
  else
    v_sypa_waarde := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'MACHTIGINGSBRIEF_NODIG'
                     , p_kafd_id => decl_rec.kafd_id
                     , p_ongr_id => decl_rec.ongr_id
                     );
    sypa_macht_nodig_tab( decl_rec.ongr_id ) := v_sypa_waarde;
  end if;
  if ( instr( upper(v_sypa_waarde), upper(decl_rec.entiteit) ) > 0 )
  then
    verz_rec.machtiging_nodig := null;
    if ( decl_rec.verz_id is not null )
    then
      open  verz_cur( decl_rec.verz_id );
      fetch verz_cur
      into  verz_rec;
      close verz_cur;
    end if;
    return( nvl( verz_rec.machtiging_nodig, 'J' ) );
  else
    return( 'N' );
  end if;
exception
  when others
  then
    return( 'J' );
end machtigingsbrief_nodig;

procedure handmatige_declaratie
( p_entiteit in varchar2
, p_id in number
, p_declareren in varchar2
, p_commit in boolean := false
)
is
  cursor decl_cur
  ( b_entiteit in varchar2
  , b_id in number
  )
  is
    select decl_id
    ,      declareren
    ,      status
    from   kgc_declaraties
    where  entiteit = b_entiteit
    and    id = b_id
    for update of declareren nowait
    ;
  decl_rec decl_cur%rowtype;
  cursor deen_cur
  ( b_deen_id in number
  )
  is
    select deen_id
    ,      declarabel
    ,      declaratie_maken
    ,      vervallen
    from   kgc_decl_entiteiten
    where  deen_id = b_deen_id
    ;
  deen_rec deen_cur%rowtype;
begin
  open  decl_cur( upper( p_entiteit ), p_id );
  fetch decl_cur
  into  decl_rec;
  if ( decl_rec.decl_id is null )
  then
    if ( p_declareren = 'J' )
    then -- maak een 'handmatige' rij aan
      deen_rec.deen_id := deen_id
                          ( p_entiteit => p_entiteit
                          , p_id => p_id
                          );
      if ( deen_rec.deen_id is not null )
      then
        open  deen_cur( deen_rec.deen_id );
        fetch deen_cur
        into  deen_rec;
        close deen_cur;
        if ( deen_rec.vervallen = 'J' )
        then -- wel een record gevonden, maar vervallen, dus niet meer gebruiken
          return;
        end if;
        if ( deen_rec.declaratie_maken = 'N' )
        then -- wel een record gevonden: er is wel een passende declaratie-entiteit
             -- maar er hoeft geen declaratie-rij te worden aangemaakt!!!
             -- Vb.: bij DNA-onderzoek wordt alleen geregistreerd of onderliggende betrokkenen declarabel zijn
          return;
        end if;
      end if;
      insert into kgc_declaraties
      ( entiteit
      , id
      , declareren
      , status
      )
      values
      ( p_entiteit
      , p_id
      , p_declareren
      , 'H'
      );
    end if;
  elsif ( nvl( decl_rec.status, '-' ) in ( '-', 'T', 'R' ) ) -- leeg/tegengehouden/teruggedraaid
  then
    update kgc_declaraties
    set    declareren = p_declareren
    ,      status = 'H'
    where  current of decl_cur
    ;
  elsif ( nvl( decl_rec.status, '-' ) = 'H' ) -- handmatig
  then
    update kgc_declaraties
    set    declareren = p_declareren
    ,      status = null -- terugzetten naar een andere status is vooralsnog niet mogelijk
    where  current of decl_cur
    ;
  elsif ( nvl( decl_rec.status, '-' ) in ( 'V', 'X' ) ) -- naar DFT: verwerkt/afgewezen
  then
    null; -- just-in-case: handmatig wijzigen was in de forms geblokkeerd bij deze statussen
  end if;

  if ( decl_cur%isopen )
  then
    close decl_cur;
  end if;
  if ( p_commit )
  then
    commit;
  end if;
exception
  when others
  then
    rollback;
    if ( decl_cur%isopen )
    then
      close decl_cur;
    end if;
    raise;
end handmatige_declaratie;

FUNCTION decl_aanvrager
( p_decl_entiteit    IN VARCHAR2
, p_decl_entiteit_id IN NUMBER
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);


  CURSOR onde_cur
  IS
    SELECT rela.aanspreken
    FROM   kgc_onderzoeken onde
    ,      kgc_relaties rela
    WHERE  onde.onde_id = p_decl_entiteit_id
      AND  rela.rela_id = onde.rela_id (+)
    ;
  CURSOR onbe_cur
  IS
    SELECT rela.aanspreken
    FROM   kgc_onderzoeken onde
    ,      kgc_onderzoek_betrokkenen onbe
    ,      kgc_relaties rela
    WHERE  onbe.onde_id = onde.onde_id
    AND    onbe.onbe_id = p_decl_entiteit_id
    AND    rela.rela_id = onde.rela_id (+)
    ;
  CURSOR meti_cur
  IS
    SELECT rela.aanspreken
    FROM   kgc_stoftestgroepen stgr
    ,      kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      kgc_relaties rela
    WHERE  meti.onde_id = onde.onde_id
    AND    meti.stgr_id = stgr.stgr_id (+)
    AND    meti.meti_id = p_decl_entiteit_id
    AND    rela.rela_id = onde.rela_id (+)
     ;
  CURSOR gesp_cur
  IS
    SELECT rela.aanspreken
    FROM   kgc_onderzoeken onde
    ,      kgc_gesprekken gesp
    ,      kgc_relaties rela
    WHERE  gesp.onde_id = onde.onde_id
    AND    gesp.gesp_id = p_decl_entiteit_id
    AND    rela.rela_id = onde.rela_id (+)
    ;
BEGIN

  IF ( p_decl_entiteit = 'ONDE' )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  v_return;
    CLOSE onde_cur;
  ELSIF ( p_decl_entiteit = 'ONBE' )
  THEN
    OPEN  onbe_cur;
    FETCH onbe_cur
    INTO  v_return;
    CLOSE onbe_cur;
  ELSIF ( p_decl_entiteit = 'METI' )
  THEN
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  v_return;
    CLOSE meti_cur;
  ELSIF ( p_decl_entiteit = 'GESP' )
  THEN
    OPEN  gesp_cur;
    FETCH gesp_cur
    INTO  v_return;
    CLOSE gesp_cur;
  END IF;

  RETURN( v_return );
END decl_aanvrager;

-- functie geeft TRUE als de declaratie is aangeboden aan het externe financiele systeem
function overgezet_via_DFT
( p_entiteit in varchar2
, p_id in number
)
return boolean
is
  v_return boolean;
  cursor decl_cur
  ( b_entiteit in varchar2
  , b_id in number
  )
  is
    select decl.decl_id
    from   kgc_declaraties decl
    where  decl.entiteit = b_entiteit
    and    decl.id = b_id
    and    nvl( decl.status, '-' ) in ( 'V', 'X' )
    ;
  decl_rec decl_cur%rowtype := null;
begin
  open  decl_cur( b_entiteit => p_entiteit
                , b_id => p_id
                );
  fetch decl_cur
  into  decl_rec;
  v_return  := decl_cur%found;
  close decl_cur;
  return( v_return );
end overgezet_via_DFT;
END   kgc_decl_00;
/

/
QUIT
