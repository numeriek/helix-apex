CREATE OR REPLACE PACKAGE "HELIX"."KGC_FORMAAT_00" IS
-- Een deel van de functionaliteit in deze package staat ook
-- in de forms library KGCLIB60 (KGC$FORMATTEER)
--
FUNCTION simplify_string
( p_tekenreeks IN VARCHAR2 := NULL
) RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( simplify_string, WNDS, WNPS, RNPS );
--
-- FORMATTEER
-- Standaardiseer de invoer van een kolom:
--  (achter)naam : initcap
--  voorletters  : upper als initialen(bevat .), ander initcap
--  voorvoegsel  : in tabel
--  (woon)plaats : upper
--  land         : in tabel
PROCEDURE formatteer
( p_naamdeel IN VARCHAR2
, x_naam IN OUT VARCHAR2
);
-- AANSPREKEN_PERSOON
PROCEDURE aanspreken_persoon
( x_pers_prow IN OUT cg$kgc_personen.cg$row_type
);

PROCEDURE aanspreken_relatie
( x_rela_prow IN OUT cg$kgc_relaties.cg$row_type
);

PROCEDURE standaard_aanspreken
 (  p_type  IN  VARCHAR2  :=  'PERS'  -- of RELA, ARTS, ...
 ,  p_voorletters  IN  VARCHAR2  :=  NULL
 ,  p_voorvoegsel  IN  VARCHAR2  :=  NULL
 ,  p_achternaam  IN  VARCHAR2  :=  NULL
 ,  p_achternaam_partner IN VARCHAR2 := NULL
 ,  p_voorvoegsel_partner IN VARCHAR2 := NULL
 ,  p_gehuwd IN VARCHAR2 := 'N'
 ,  p_geslacht IN VARCHAR2 := 'O'
 ,  p_prefix  IN  VARCHAR2  :=  NULL
 ,  p_postfix  IN  VARCHAR2  :=  NULL
 ,  x_aanspreken  IN  OUT  VARCHAR2
);

FUNCTION volledig_adres
( p_naam IN VARCHAR2 := NULL
, p_adres IN VARCHAR2 := NULL
, p_postcode IN VARCHAR2 := NULL
, p_woonplaats IN VARCHAR2 := NULL
, p_land IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( volledig_adres, WNDS, WNPS, RNDS, RNPS );

FUNCTION rekendatum
( p_datumtekst IN VARCHAR2 )
RETURN DATE;
PRAGMA RESTRICT_REFERENCES( rekendatum, WNDS, WNPS, RNPS );

-- toon numerieke waarde .123 als 0.123
FUNCTION nummer
( p_waarde IN NUMBER
, p_decimaal IN NUMBER := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( nummer, WNDS, WNPS, RNPS );

FUNCTION normaalwaarden
( p_onder IN VARCHAR2
, p_boven IN VARCHAR2
, p_gemiddeld IN VARCHAR2 := NULL
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( normaalwaarden, WNDS, WNPS, RNPS );

FUNCTION formatteer_postcode
( p_postcode IN VARCHAR2)
RETURN VARCHAR2;

-- zet juiste aantal decimalen bij de (numerieke) waarde
function waarde
( p_waarde in varchar2
, p_aantal_decimalen in number
)
return varchar2;
END KGC_FORMAAT_00;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_FORMAAT_00" IS
/******************************************************************************
      NAME:       KGC_FORMAAT_00

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.5        06-04-2016   Athar Shaikh      Mantis 3195: Enzym bijlage brief: normaalwaarden  met de juiste aantallen  decimalen tonen
                                                Code change in function normaalwaarden for the variable v_onder and v_boven
    ****************************************************************************** */
FUNCTION simplify_string
-----------------------------------------------------------
-- Function simplify_string.
-- 30-03-2004  ASSAI ESO.
-- TBV zoekopdrachten in kgcpers05, kgcmons05, etc.
-----------------------------------------------------------
( p_tekenreeks IN VARCHAR2 := NULL
) RETURN VARCHAR2
IS
 v_tekens     kgc_tekensvervanging.tekens%TYPE   := '';
 v_vervanging kgc_tekensvervanging.vervanging%TYPE   := '';
 v_tekenreeks  VARCHAR2(1000) := UPPER( replace( p_tekenreeks, ' ', '' ) );
 v_asciinummer NUMBER(3) := 0;
    -- variabele tbv errorhandling
 v_tekensvervangingregel VARCHAR2(200) := '';
 CURSOR teve_cur IS
  SELECT tekens
  ,      vervanging
  FROM   kgc_tekensvervanging
  ORDER BY tekens;
BEGIN
--
 FOR teve_rec IN teve_cur
 LOOP
  v_tekensvervangingregel := 'tekens = '||teve_rec.tekens || ' vervanging = '|| teve_rec.vervanging;
  v_tekens     := teve_rec.tekens;
  v_vervanging := teve_rec.vervanging;
    -- als er 'chr( n )' in de tekens staat moet de desbetreffende ascii letter vervangen worden. --
  IF SUBSTR( v_tekens,1,4) = 'CHR(' AND SUBSTR( v_tekens,-1,1) = ')'
  THEN
   -- strip de inhoud van de variabele tot alleen het asciinr overblijft:
   v_tekens := REPLACE(v_tekens,'CHR(');
   v_tekens := REPLACE(v_tekens, ')' );
   v_asciinummer := TO_NUMBER( v_tekens );
   v_tekens := CHR( v_asciinummer );
  END IF;
    -- als de vervangingsletterreeks een 'CHR( n ) bevat, dan moet een vergelijkbare actie
    -- uitgevoerd worden.
  IF SUBSTR( v_vervanging,1,4) = 'CHR(' AND SUBSTR( v_vervanging,-1,1) = ')'
  THEN
   -- strip de inhoud van de variabele tot alleen het asciinr overblijft:
   v_vervanging  := REPLACE(v_vervanging,'CHR(');
   v_vervanging  := REPLACE(v_vervanging, ')' );
   v_asciinummer := TO_NUMBER( v_vervanging );
   v_vervanging  := CHR( v_asciinummer );
  END IF;
   -- voer de replace actie uit evt mbv de gevonden ASCII character.
   v_tekenreeks := REPLACE( v_tekenreeks, v_tekens, v_vervanging );
 END LOOP;
 RETURN( v_tekenreeks );
/*  exception kan niet gezet worden ivm pragma restrictie!!
*/
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( v_tekenreeks );
END simplify_string;
--
-- lokale functie om het zisnr te formatteren
-- aangeroepen in procedure "formatteer"
-- NOG NIET IN GEBRUIK!!!
function zisnr
( p_nummer in varchar2
, p_formaat in varchar2
)
return varchar2
is
  v_return varchar2(10);
  v_nummer varchar2(100) := p_nummer;
  v_formaat varchar2(100) := p_formaat;
begin
  if ( v_formaat is null )
  then
    v_formaat := kgc_sypa_00.systeem_waarde
                 ( p_parameter_code => 'ZISNUMMER_FORMAAT'
                 );
  end if;
  if ( v_formaat is not null )
  then
    for i in 1..length( v_formaat )
    loop
      if ( substr( v_formaat, i, 1 ) = '9' )
      then
        v_return := v_return || substr( v_nummer, 1, 1 );
        v_nummer := substr( v_nummer, 2 );
      else
        v_return := v_return || substr( v_formaat, i, 1 );
      end if;
    end loop;
    if ( v_nummer is not null )
    then
      raise_application_error( -20000, 'Opgegeven nummer '||p_nummer||' past niet in het formaat '||p_formaat );
    end if;
  else
    v_return := p_nummer;
  end if;
  return( v_return );
end zisnr;

PROCEDURE formatteer
( p_naamdeel IN VARCHAR2
, x_naam IN OUT VARCHAR2
)
IS
  v_naam VARCHAR2(200) := x_naam;
BEGIN
  IF ( UPPER( p_naamdeel ) LIKE '%NAAM%' )
  THEN
    v_naam := LTRIM( RTRIM( v_naam ) );
    v_naam := INITCAP( v_naam );
  ELSIF ( UPPER( p_naamdeel ) LIKE '%VOORL%' )
  THEN
    v_naam := LTRIM( RTRIM( v_naam ) );
    IF ( INSTR( v_naam, '.' ) > 0 )
    THEN
      v_naam := UPPER( v_naam );
    ELSE
      v_naam := INITCAP( v_naam );
    END IF;
  ELSIF ( UPPER( p_naamdeel ) LIKE '%VOORV%' )
  THEN
    v_naam := LOWER( v_naam );
  ELSIF ( UPPER( p_naamdeel ) LIKE '%PLAATS%' )
  THEN
    v_naam := UPPER( v_naam );
  ELSIF ( UPPER( p_naamdeel ) LIKE '%LAND%' )
  THEN
    kgc_land_00.controleer( v_naam );
  ELSIF ( UPPER( p_naamdeel ) LIKE 'ZISNR%' ) -- NOG NIET IN GEBRUIK!!!
  THEN
    -- na 'ZISNR' komt mogelijk de formaataanduiding, om overbodige sypa-aanroepen te voorkomen
    v_naam := zisnr
              ( p_nummer => v_naam
              , p_formaat => substr( p_naamdeel, 6 )
              );
  ELSE
    x_naam := v_naam;
  END IF;
  x_naam := v_naam;
END formatteer;
--
PROCEDURE aanspreken_persoon
( x_pers_prow IN OUT cg$kgc_personen.cg$row_type
)
IS
--  v_aanspreken VARCHAR2(100) := NULL;  - commented by saroj for mantis 0182
    v_aanspreken VARCHAR2(32767) := NULL;  -- added by saroj for mantis 0182

  PROCEDURE voeg_toe
  ( p_deel IN VARCHAR2
  )
  IS
  BEGIN
    IF ( v_aanspreken IS NULL )
    THEN
      v_aanspreken := p_deel;
    ELSE
      IF ( p_deel IS NOT NULL )
      THEN
        v_aanspreken := v_aanspreken ||' '|| p_deel;
      END IF;
    END IF;
  END voeg_toe;

BEGIN
  voeg_toe( x_pers_prow.voorletters );
  IF ( x_pers_prow.gehuwd = 'J'
   AND x_pers_prow.geslacht = 'V'
     )
  THEN
    IF x_pers_prow.achternaam_partner IS NOT NULL
    THEN
       voeg_toe( x_pers_prow.voorvoegsel_partner);
       voeg_toe( x_pers_prow.achternaam_partner||' -');
    END IF;
  END IF;
  voeg_toe( x_pers_prow.voorvoegsel );
  voeg_toe( x_pers_prow.achternaam );
  --
  x_pers_prow.aanspreken := v_aanspreken;
END aanspreken_persoon;

PROCEDURE aanspreken_relatie
( x_rela_prow IN OUT cg$kgc_relaties.cg$row_type
)
IS
  v_aanspreken VARCHAR2(100) := NULL;

  PROCEDURE voeg_toe
  ( p_deel IN VARCHAR2
  )
  IS
  BEGIN
    IF ( v_aanspreken IS NULL )
    THEN
      v_aanspreken := p_deel;
    ELSE
      IF ( p_deel IS NOT NULL )
      THEN
        v_aanspreken := v_aanspreken ||' '|| p_deel;
      END IF;
    END IF;
  END voeg_toe;
BEGIN
  voeg_toe( x_rela_prow.voorletters );
  voeg_toe( x_rela_prow.voorvoegsel );
  voeg_toe( x_rela_prow.achternaam );
  --
  x_rela_prow.aanspreken := v_aanspreken;
END aanspreken_relatie;

PROCEDURE standaard_aanspreken
 (  p_type  IN  VARCHAR2  :=  'PERS'
 ,  p_voorletters  IN  VARCHAR2  :=  NULL
 ,  p_voorvoegsel  IN  VARCHAR2  :=  NULL
 ,  p_achternaam  IN  VARCHAR2  :=  NULL
 ,  p_achternaam_partner IN VARCHAR2 := NULL
 ,  p_voorvoegsel_partner IN VARCHAR2 := NULL
 ,  p_gehuwd IN VARCHAR2 := 'N'
 ,  p_geslacht IN VARCHAR2 := 'O'
 ,  p_prefix  IN  VARCHAR2  :=  NULL
 ,  p_postfix  IN  VARCHAR2  :=  NULL
 ,  x_aanspreken  IN  OUT  VARCHAR2
)
IS
  pers_prow cg$kgc_personen.cg$row_type;
  rela_prow cg$kgc_relaties.cg$row_type;
BEGIN
  IF ( p_type = 'PERS' )
  THEN
    pers_prow.voorletters := p_voorletters;
    pers_prow.voorvoegsel := p_voorvoegsel;
    pers_prow.achternaam := p_achternaam;
    pers_prow.achternaam_partner := p_achternaam_partner;
    pers_prow.voorvoegsel_partner := p_voorvoegsel_partner;
    pers_prow.gehuwd := p_gehuwd;
    pers_prow.geslacht := p_geslacht;
    aanspreken_persoon( pers_prow );
    x_aanspreken := pers_prow.aanspreken;
  ELSIF ( p_type IN ( 'ARTS', 'RELA' ) )
  THEN
    rela_prow.voorletters := p_voorletters;
    rela_prow.voorvoegsel := p_voorvoegsel;
    rela_prow.achternaam := p_achternaam;
    aanspreken_relatie( rela_prow );
    x_aanspreken := rela_prow.aanspreken;
  END IF;
  --
  IF ( p_prefix IS NOT NULL )
  THEN
    x_aanspreken := p_prefix||x_aanspreken;
  END IF;
  IF ( p_postfix IS NOT NULL )
  THEN
    x_aanspreken := x_aanspreken||p_postfix;
  END IF;
END standaard_aanspreken;

FUNCTION volledig_adres
( p_naam IN VARCHAR2 := NULL
, p_adres IN VARCHAR2 := NULL
, p_postcode IN VARCHAR2 := NULL
, p_woonplaats IN VARCHAR2 := NULL
, p_land IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_volledig_adres VARCHAR2(2000) := NULL;
  PROCEDURE voeg_toe
  ( p_regel IN VARCHAR2
  )
  IS
  BEGIN
    IF ( p_regel IS NOT NULL )
    THEN
      IF ( v_volledig_adres IS NULL )
      THEN
        v_volledig_adres := p_regel;
      ELSE
        v_volledig_adres := v_volledig_adres
                         || CHR(10)
                         || p_regel;
      END IF;
    END IF;
  END voeg_toe;
BEGIN
  voeg_toe( p_naam );

  --  voeg_toe( p_adres ); -- commented for mantis 7286

 ---------------------   replaced above commented line with the following block of code ( mantis 7286)

  IF ( p_adres IS NOT NULL )
  THEN
    --voeg_toe( p_adres||' ' );       --Commented by Rajendra for mantis 8220(27-06-2013)
    voeg_toe( TRIM (both chr(10) FROM p_adres) || '' );  --Added by rajendra for mantis 8220(27-06-2013)
  ELSE
    v_volledig_adres := v_volledig_adres
                     || CHR(10)
                      ;
  END IF;

  --- Mantis 7286 ends here -------------------------------------

  IF ( p_postcode IS NOT NULL )
  THEN
    --voeg_toe( p_postcode||' ' ); --Commented by sachin for mantis 4828: Two sapces added between postcode as city (14-04-2015)
    voeg_toe( p_postcode||'  ' );
  ELSE
    v_volledig_adres := v_volledig_adres
                     || CHR(10)
                      ;
  END IF;
  IF ( p_woonplaats IS NOT NULL )
  THEN
    v_volledig_adres := v_volledig_adres
                     || p_woonplaats
                      ;
  END IF;
  IF ( p_land IS NOT NULL )
  THEN
    IF ( UPPER( p_land ) <> 'NEDERLAND' )
    THEN
      voeg_toe( p_land );
    END IF;
  END IF;
  v_volledig_adres := REPLACE( v_volledig_adres, CHR(10)||CHR(10), CHR(10) );
  v_volledig_adres := REPLACE( v_volledig_adres, CHR(10)||CHR(10), CHR(10) );
  RETURN( v_volledig_adres );
END volledig_adres;

FUNCTION rekendatum
( p_datumtekst IN VARCHAR2 ) -- formaat 00-00-0000
RETURN DATE
IS
  v_return_datum DATE;
  v_datum VARCHAR2(10) := p_datumtekst;
BEGIN
  IF ( SUBSTR( v_datum, 1, 2 ) = '00' )
  THEN
    v_datum := '01'
            || SUBSTR( v_datum, 3 )
             ;
  END IF;
  IF ( SUBSTR( v_datum, 4, 2 ) = '00' )
  THEN
    v_datum := SUBSTR( v_datum, 1, 3 )
            || '01'
            || SUBSTR( v_datum, 6 )
             ;
  END IF;
  IF ( SUBSTR( v_datum, 7, 4 ) = '0000' )
  THEN
    v_datum := SUBSTR( v_datum, 1, 6 )
            || TO_CHAR( SYSDATE, 'yyyy' )
             ;
  END IF;
  v_return_datum := TO_DATE( v_datum, 'dd-mm-yyyy' );
  RETURN( v_return_datum );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( NULL );
END rekendatum;

FUNCTION nummer
( p_waarde IN NUMBER
, p_decimaal IN NUMBER := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(1000);
BEGIN
  IF ( p_decimaal <= 0 )
  THEN
    v_return := TO_CHAR( p_waarde, 'FM9999999990' );
  ELSIF ( p_decimaal IS NOT NULL )
  THEN
    v_return := TO_CHAR( p_waarde, 'FM9999999990.'||LPAD( '0', p_decimaal, '0' ) );
  ELSIF ( p_waarde = TRUNC( p_waarde ) )
  THEN
    v_return := TO_CHAR( p_waarde );
  ELSE
    v_return := TO_CHAR( p_waarde, 'FM9999999990.999999' );
  END IF;
  RETURN( v_return );
END nummer;

FUNCTION normaalwaarden
( p_onder IN VARCHAR2
, p_boven IN VARCHAR2
, p_gemiddeld IN VARCHAR2 := NULL
)
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  v_dec NUMBER;
  v_onder VARCHAR2(1000);
  v_boven VARCHAR2(1000);
  v_gemiddeld VARCHAR2(1000);
BEGIN
/* doe toch maar niet, rare effecten!!!
  -- formatteer eenduidig, zelfde aantal decimalen
  v_dec := NULL;
  IF ( INSTR( p_onder, '.' ) > 0 )
  THEN
    v_dec := GREATEST( v_dec, LENGTH( SUBSTR( p_onder, INSTR( p_onder, '.' ) + 1 ) ) );
  END IF;
  IF ( INSTR( p_boven, '.' ) > 0 )
  THEN
    v_dec := GREATEST( v_dec, LENGTH( SUBSTR( p_boven, INSTR( p_boven, '.' ) + 1 ) ) );
  END IF;
  IF ( INSTR( p_gemiddeld, '.' ) > 0 )
  THEN
    v_dec := GREATEST( v_dec, LENGTH( SUBSTR( p_gemiddeld, INSTR( p_gemiddeld, '.' ) + 1 ) ) );
  END IF;
*/
 /*
  v_onder := nummer( kgc_reken.rekenwaarde( p_onder ), v_dec );
  v_boven := nummer( kgc_reken.rekenwaarde( p_boven ), v_dec ); */ -- Commented by Athar Shaikh for Mantis-3195 on 06-04-2016
  -- Start Added by Athar Shaikh for Mantis-3195 on 06-04-2016
  v_onder := p_onder;
  v_boven := p_boven;
  -- End Added by Athar Shaikh for Mantis-3195 on 06-04-2016
  v_gemiddeld := nummer( kgc_reken.rekenwaarde( p_gemiddeld ), v_dec );

  IF ( v_onder IS NOT NULL
   AND v_boven IS NOT NULL
     )
  THEN
    v_return := v_onder
             || ' - '
             || v_boven
              ;
  ELSIF ( v_boven IS NOT NULL )
  THEN
    v_return := '< '||v_boven;
  ELSIF ( v_onder IS NOT NULL )
  THEN
    v_return := '> '||v_onder;
  END IF;
  IF ( v_gemiddeld IS NOT NULL )
  THEN
    v_return := v_return||' ('||v_gemiddeld||')';
  END IF;
  RETURN( v_return );
END normaalwaarden;

FUNCTION formatteer_postcode
( p_postcode IN VARCHAR2)
RETURN VARCHAR2 IS
  v_postcode VARCHAR2(2000);
BEGIN
  v_postcode := kgc_util_00.multi_replace(ltrim(rtrim(upper(p_postcode))),'  ',' ');
  IF instr(p_postcode,' ') = 0
  AND  length(p_postcode) = 6
    THEN
      v_postcode := substr(v_postcode,1,4)||' '||substr(v_postcode,5);
    END IF;
  RETURN (v_postcode);
END;

function waarde
( p_waarde in varchar2
, p_aantal_decimalen in number
)
return varchar2
is
  v_formaat varchar2(100);
  v_num_waarde number;
  v_return_waarde varchar2(1000);
begin
  v_num_waarde := kgc_reken.numeriek( p_waarde );
  if ( v_num_waarde is null )
  then
    return( p_waarde );
  end if;

  IF ( p_aantal_decimalen IS NOT NULL )
  THEN
    if ( p_aantal_decimalen = 0 )
    then
      v_formaat := lpad('9',20,'9')||'0';
    else
      v_formaat := lpad('9',20,'9')||'0.'||lpad('9',p_aantal_decimalen,'9' );
    end if;
    v_return_waarde := ltrim( to_char( v_num_waarde, v_formaat ) );
  ELSIF ( v_num_waarde > -1
      and v_num_waarde < 0
        )
  THEN
    v_return_waarde := '-0'||abs(v_num_waarde);
  ELSIF ( v_num_waarde > 0
      and v_num_waarde < 1
        )
  THEN
    v_return_waarde := '0'||v_num_waarde;
  ELSE
    v_return_waarde := p_waarde;
  END IF;
  return( v_return_waarde );
end waarde;
END kgc_formaat_00;
/

/
QUIT
