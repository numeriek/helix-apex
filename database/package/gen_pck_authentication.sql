CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_AUTHENTICATION"
AS
   /*
   =============================================================================
   Package Name : gen_pck_authentication
   Usage        : This package contains functions and procedure concerning (
                  application) authentication
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   -- Associative array returning all the attributes of a single LDAP user,
   -- the key of this array is the LDAP attribute (see LDAP attributes contstants above)
   TYPE t_ldap_attribute_values IS TABLE OF VARCHAR2(1024)
      INDEX BY VARCHAR2(1024);
   --
   --    Associative array returning all the user roles of a given user
   TYPE t_ldap_user_roles IS TABLE OF VARCHAR2(1024); -- INDEX BY BINARY_INTEGER;
   g_login_type  VARCHAR2(4) := 'LDAP';
   --
   -- Function...
   FUNCTION f_auth_login(i_username  IN VARCHAR2
                        ,i_password  IN VARCHAR2)
      RETURN BOOLEAN;
   -- Function....
      -- package parameters must start with p_ in stead of i_
   FUNCTION f_ldap_authentication(p_username  IN VARCHAR2
                                 ,p_password  IN VARCHAR2)
      RETURN BOOLEAN;
   -- Retrieve a single attribute from the LDAP repository.
   -- A fetch is made for this attribute belonging to a given user
   FUNCTION f_get_ldap_attribute(i_username   IN VARCHAR2
                                ,i_attribute  IN VARCHAR2)
      RETURN VARCHAR2;
   --
   -- Basis APEX authentication function, used by APEX to authenticate the logged on user

   FUNCTION f_my_authentication(i_username  IN VARCHAR2
                               ,i_password  IN VARCHAR2)
      RETURN BOOLEAN;
   --
   -- Retrieve multiple attributes from the LDAP repository.
   -- A fetch is made for this attribute belonging to a given user
   FUNCTION f_get_ldap_attributes(i_username    IN VARCHAR2
                                 ,i_attributes  IN dbms_ldap.string_collection)
      RETURN t_ldap_attribute_values;
   --
   -- Fetch all the LDAP roles of a given user
   FUNCTION f_get_ldap_roles(i_username IN VARCHAR2)
      RETURN t_ldap_user_roles
      PIPELINED;
--
END gen_pck_authentication;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_AUTHENTICATION"
AS
   /*
     =============================================================================
     Package Name : gen_pck_authentication
     Usage        : This package contains functions and procedure concerning (
                    application) authentication
     =============================================================================
     HISTORY
     =======
     Date        Author       Description
     ----------  ------------ ----------------------------------------------------
     23-12-2016  The DOC      Creation
     =============================================================================
     */

   g_package  CONSTANT VARCHAR2(31) := $$plsql_unit || '.';
   -- Function....
   FUNCTION f_auth_login(i_username  IN VARCHAR2
                        ,i_password  IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_found  NUMBER;
   BEGIN
      SELECT COUNT(*)
        INTO l_found
        FROM gen_appl_users aus
       WHERE UPPER(aus.user_id) = UPPER(i_username)
         AND aus.password = gen_pck_encryption.f_checksum(i_password)
         AND NVL(aus.active_ind, 'N') = 'Y';
      IF NVL(l_found, 0) > 0
      THEN
         gen_pck_authentication.g_login_type   := 'USER';
         RETURN (TRUE);
      ELSE
         RETURN (FALSE);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (FALSE);
   END f_auth_login;
   -- Function...

   -- package parameters must start with p_ in stead of i_
   FUNCTION f_ldap_authentication(p_username  IN VARCHAR2
                                 ,p_password  IN VARCHAR2)
      RETURN BOOLEAN
   AS
      l_ldap_port       NUMBER;
      l_ldap_host       VARCHAR2(100);
      l_ldap_domain     VARCHAR2(100);
      --
      l_retval          PLS_INTEGER;
      l_retval2         PLS_INTEGER;
      l_session         dbms_ldap.session;
      l_application_id  NUMBER;
      l_proc   CONSTANT VARCHAR2(61) := g_package || 'f_ldap_authentication';
      CURSOR c_user(cp_application_id gen_appl_roles.application_id%TYPE)
      IS
         SELECT ldap_domain
               ,arl.id
           FROM gen_appl_users aus
                INNER JOIN gen_appl_user_roles aue ON aue.appl_user_id = aus.id
                INNER JOIN gen_appl_roles arl ON aue.appl_role_id = arl.id
          WHERE LOWER(user_id) = LOWER(p_username)
            AND arl.application_id = cp_application_id;
      r_user            c_user%ROWTYPE;
   BEGIN
      -- initialize l_application_id
      l_application_id   := v('APP_ID');
      OPEN c_user(l_application_id);
      FETCH c_user
         INTO r_user;
      CLOSE c_user;
      IF r_user.id IS NULL
      THEN
         RETURN FALSE;
      ELSE
         --
         --
         IF gen_pck_general.f_get_appl_parameter_value('LDAP_ACTIVE'
                                                    ,l_application_id) = 'Y'
         THEN
            l_ldap_port               :=
               gen_pck_general.f_get_appl_parameter_value('LDAP_PORT'
                                                       ,l_application_id);
            l_ldap_host               :=
               gen_pck_general.f_get_appl_parameter_value('LDAP_HOST'
                                                       ,l_application_id);
            l_ldap_domain             :=
               COALESCE(r_user.ldap_domain
                       ,gen_pck_general.f_get_appl_parameter_value(
                           'LDAP_DOMAIN'
                          ,l_application_id));
            IF p_password IS NULL
            THEN
               RETURN gen_pck_appl_cookie_auth.f_is_cookie_valid();
            --return false;
            END IF;
            l_retval                  := -1;
            dbms_ldap.use_exception   := TRUE;
            BEGIN
               l_session      :=
                  dbms_ldap.init(l_ldap_host
                                ,l_ldap_port);
               l_retval      :=
                  dbms_ldap.simple_bind_s(l_session
                                         ,l_ldap_domain || '\' || p_username
                                         ,p_password);
               l_retval2   := dbms_ldap.unbind_s(l_session);
               RETURN TRUE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  IF SQLCODE = -31202
                  THEN
                     -- if trying to login in LDAP with the wrong user credentials, a message is logged.
                     -- -31202,ORA-31202: DBMS_LDAP: LDAP client/server error: Invalid credentials. 80090308: LdapErr: DSID-0C0903A9, comment: AcceptSecurityContext error, data 52e, v1db1
                     logger.LOG(SQLCODE || ',' || SQLERRM);
                  ELSE
                     -- if something else is wrong an error is logged
                     logger.log_error(
                        p_text => dbms_utility.format_error_backtrace
                       ,p_scope => l_proc);
                     BEGIN
                        l_retval2   := dbms_ldap.unbind_s(l_session);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           NULL;
                     END;
                  END IF;
                  RETURN f_auth_login(p_username
                                     ,p_password);
            END;
         ELSE
            IF p_password IS NULL
            THEN
               --return false;
               RETURN gen_pck_appl_cookie_auth.f_is_cookie_valid();
            ELSE
               RETURN f_auth_login(p_username
                                  ,p_password);
            END IF;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_proc);
         RETURN FALSE;
   END f_ldap_authentication;
   --
   -- Authentication function, called by the APEX application
   FUNCTION f_my_authentication(i_username  IN VARCHAR2
                               ,i_password  IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_session         dbms_ldap.session;
      l_retval          PLS_INTEGER;
      l_retval_dis      PLS_INTEGER;
      l_dn_user         VARCHAR2(256);
      l_application_id  NUMBER;
   BEGIN
      -- initialize l_application_id
      l_application_id          := v('APP_ID');
      --
      IF i_username IS NULL
      OR i_password IS NULL
      THEN
         RETURN FALSE;
      END IF;
      -- Choose to raise exceptions.
      dbms_ldap.use_exception   := FALSE;
      -- Connect to the LDAP server.
      l_session                 :=
         dbms_ldap.init(hostname => gen_pck_general.f_get_appl_parameter_value(
                                      'LDAP_HOST'
                                     ,l_application_id)
                       ,portnum => gen_pck_general.f_get_appl_parameter_value(
                                     'LDAP_PORT'
                                    ,l_application_id));
      l_dn_user                 :=
         gen_pck_general.f_get_appl_parameter_value('DN_USER'
                                                 ,l_application_id);
      l_dn_user                 :=
         REPLACE(l_dn_user
                ,'#USER_ID#'
                ,i_username);
      l_retval                  :=
         dbms_ldap.simple_bind_s(ld => l_session
                                ,dn => l_dn_user
                                ,passwd => i_password);
      -- Disconnect from the LDAP server.
      l_retval_dis              := dbms_ldap.unbind_s(ld => l_session);
      IF (l_retval = 0)
      THEN
         RETURN (TRUE);
      ELSE
         RETURN (FALSE);
      END IF;
   END f_my_authentication;
   -- Retrieve a single attribute from the LDAP repository.
   -- A fetch is made for this attribute belonging to a given user
   FUNCTION f_get_ldap_attribute(i_username   IN VARCHAR2
                                ,i_attribute  IN VARCHAR2)
      RETURN VARCHAR2
   IS
      -- Adjust as necessary.
      l_ldap_host       VARCHAR2(256);
      l_ldap_port       VARCHAR2(256);
      l_ldap_user       VARCHAR2(256);
      l_ldap_passwd     VARCHAR2(256);
      l_dn_user         VARCHAR2(256);
      l_retval          PLS_INTEGER;
      l_session         dbms_ldap.session;
      l_attrs           dbms_ldap.string_collection;
      l_message         dbms_ldap.MESSAGE;
      l_entry           dbms_ldap.MESSAGE;
      l_attr_name       VARCHAR2(256);
      l_ber_element     dbms_ldap.ber_element;
      l_vals            dbms_ldap.string_collection;
      l_value           VARCHAR(256);
      l_application_id  NUMBER;
   BEGIN
      -- initialize l_application_id
      l_application_id          := v('APP_ID');
      --
      -- Choose not to raise exceptions.
      dbms_ldap.use_exception   := FALSE;
      -- Connect to the LDAP server.
      --
      l_session                 :=
         dbms_ldap.init(hostname => gen_pck_general.f_get_appl_parameter_value(
                                      'LDAP_HOST'
                                     ,l_application_id)
                       ,portnum => gen_pck_general.f_get_appl_parameter_value(
                                     'LDAP_PORT'
                                    ,l_application_id));
      l_retval                  :=
         dbms_ldap.simple_bind_s(ld => l_session
                                ,dn => gen_pck_general.f_get_appl_parameter_value(
                                         'DN_BASE'
                                        ,l_application_id)
                                ,passwd => gen_pck_general.f_get_appl_parameter_value(
                                             'LDAP_PASSWORD'
                                            ,l_application_id));
      l_dn_user                 :=
         gen_pck_general.f_get_appl_parameter_value('DN_USER'
                                                 ,l_application_id);
      l_dn_user                 :=
         REPLACE(l_dn_user
                ,'#USER_ID#'
                ,i_username);
      -- Get user's attribute
      l_attrs(1)                := i_attribute;
      l_retval                  :=
         dbms_ldap.search_s(ld => l_session
                           ,base => l_dn_user
                           ,scope => dbms_ldap.scope_subtree
                           ,filter => 'objectclass=*'
                           ,attrs => l_attrs
                           ,attronly => 0
                           ,res => l_message);
      IF dbms_ldap.count_entries(ld => l_session
                                ,msg => l_message) > 0
      THEN
         -- Get all the entries returned by our search.
         l_entry      :=
            dbms_ldap.first_entry(ld => l_session
                                 ,msg => l_message);
         l_attr_name      :=
            dbms_ldap.first_attribute(ld => l_session
                                     ,ldapentry => l_entry
                                     ,ber_elem => l_ber_element);
         -- Get all the values for this attribute.
         l_vals      :=
            dbms_ldap.get_values(ld => l_session
                                ,ldapentry => l_entry
                                ,attr => l_attr_name);
         l_value      :=
            SUBSTR(l_vals(l_vals.FIRST)
                  ,1
                  ,200);
         RETURN (l_value);
      ELSE
         RETURN NULL;
      END IF;
      -- Disconnect from the LDAP server.
      l_retval                  := dbms_ldap.unbind_s(ld => l_session);
      RETURN (NULL);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END f_get_ldap_attribute;
   -- Retrieve a single attribute from the LDAP repository.
   -- A fetch is made for this attribute belonging to a given user
   FUNCTION f_get_ldap_attributes(i_username    IN VARCHAR2
                                 ,i_attributes  IN dbms_ldap.string_collection)
      RETURN t_ldap_attribute_values
   IS
      -- Adjust as necessary.
      l_ldap_host       VARCHAR2(256);
      l_ldap_port       VARCHAR2(256);
      l_ldap_user       VARCHAR2(256);
      l_ldap_passwd     VARCHAR2(256);
      l_dn_user         VARCHAR2(256);
      l_retval          PLS_INTEGER;
      l_session         dbms_ldap.session;
      l_attrs           dbms_ldap.string_collection;
      l_message         dbms_ldap.MESSAGE;
      l_entry           dbms_ldap.MESSAGE;
      l_attr_name       VARCHAR2(256);
      l_ber_element     dbms_ldap.ber_element;
      l_vals            dbms_ldap.string_collection;
      l_value           VARCHAR(1024);
      l_values          t_ldap_attribute_values;
      l_application_id  NUMBER;
   BEGIN
      -- initialize l_application_id
      l_application_id          := v('APP_ID');
      -- Choose not to raise exceptions.
      dbms_ldap.use_exception   := FALSE;
      -- Connect to the LDAP server.
      l_session                 :=
         dbms_ldap.init(hostname => gen_pck_general.f_get_appl_parameter_value(
                                      'LDAP_HOST'
                                     ,l_application_id)
                       ,portnum => gen_pck_general.f_get_appl_parameter_value(
                                     'LDAP_PORT'
                                    ,l_application_id));
      l_retval                  :=
         dbms_ldap.simple_bind_s(ld => l_session
                                ,dn => gen_pck_general.f_get_appl_parameter_value(
                                         'DN_BASE'
                                        ,l_application_id)
                                ,passwd => gen_pck_general.f_get_appl_parameter_value(
                                             'LDAP_PASSWORD'
                                            ,l_application_id));
      l_dn_user                 :=
         gen_pck_general.f_get_appl_parameter_value('DN_USER'
                                                 ,l_application_id);
      l_dn_user                 :=
         REPLACE(l_dn_user
                ,'#USER_ID#'
                ,i_username);
      -- Get user's attribute
      l_retval                  :=
         dbms_ldap.search_s(ld => l_session
                           ,base => l_dn_user
                           ,scope => dbms_ldap.scope_subtree
                           ,filter => 'objectclass=*'
                           ,attrs => i_attributes
                           ,attronly => 0
                           ,res => l_message);
      IF dbms_ldap.count_entries(ld => l_session
                                ,msg => l_message) > 0
      THEN
         -- Get all the entries returned by our search.
         l_entry      :=
            dbms_ldap.first_entry(ld => l_session
                                 ,msg => l_message);
         l_attr_name      :=
            dbms_ldap.first_attribute(ld => l_session
                                     ,ldapentry => l_entry
                                     ,ber_elem => l_ber_element);
         WHILE l_attr_name IS NOT NULL
         LOOP
            -- Get all the values for this attribute.
            l_vals                  :=
               dbms_ldap.get_values(ld => l_session
                                   ,ldapentry => l_entry
                                   ,attr => l_attr_name);
            l_value                 :=
               SUBSTR(l_vals(l_vals.FIRST)
                     ,1
                     ,1024);
            l_values(l_attr_name)   := l_value;
            l_attr_name             :=
               dbms_ldap.next_attribute(ld => l_session
                                       ,ldapentry => l_entry
                                       ,ber_elem => l_ber_element);
         END LOOP attibutes_loop;
         RETURN (l_values);
      ELSE
         RETURN l_values;
      END IF;
      -- Disconnect from the LDAP server.
      l_retval                  := dbms_ldap.unbind_s(ld => l_session);
      RETURN l_values;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_values;
   END f_get_ldap_attributes;
   -- Fetch all the LDAP roles of a given user
   FUNCTION f_get_ldap_roles(i_username IN VARCHAR2)
      RETURN t_ldap_user_roles
      PIPELINED
   IS
      l_retval           PLS_INTEGER;
      l_session          dbms_ldap.session;
      l_attrs            dbms_ldap.string_collection;
      l_message          dbms_ldap.MESSAGE;
      l_entry            dbms_ldap.MESSAGE;
      l_attr_name        VARCHAR2(256);
      l_ber_element      dbms_ldap.ber_element;
      l_vals             dbms_ldap.string_collection;
      l_user_handle      dbms_ldap_utl.handle;
      l_user_groups      dbms_ldap_utl.property_set_collection;
      l_properties       dbms_ldap.string_collection;
      l_property_values  dbms_ldap.string_collection;
      -- l_index            BINARY_INTEGER := 0;
      -- l_roles            t_ldap_user_roles;
      l_dn_user          VARCHAR2(256);
      l_application_id   NUMBER;
   BEGIN
      -- initialize l_application_id
      l_application_id          := v('APP_ID');
      -- Choose to raise exceptions.
      dbms_ldap.use_exception   := FALSE;
      -- Connect to the LDAP server.
      l_session                 :=
         dbms_ldap.init(hostname => gen_pck_general.f_get_appl_parameter_value(
                                      'LDAP_HOST'
                                     ,l_application_id)
                       ,portnum => gen_pck_general.f_get_appl_parameter_value(
                                     'LDAP_PORT'
                                    ,l_application_id));
      l_retval                  :=
         dbms_ldap.simple_bind_s(ld => l_session
                                ,dn => gen_pck_general.f_get_appl_parameter_value(
                                         'DN_BASE'
                                        ,l_application_id)
                                ,passwd => gen_pck_general.f_get_appl_parameter_value(
                                             'LDAP_PASSWORD'
                                            ,l_application_id));
      --
      l_dn_user                 :=
         gen_pck_general.f_get_appl_parameter_value('DN_USER'
                                                 ,l_application_id);
      l_dn_user                 :=
         REPLACE(l_dn_user
                ,'#USER_ID#'
                ,i_username);
      -- Create user handle
      l_retval                  :=
         dbms_ldap_utl.create_user_handle(l_user_handle
                                         ,dbms_ldap_utl.type_dn
                                         ,l_dn_user); --Use parameter value instead 'cn=' || i_username|| ',ou=users,o=asml');
      l_attrs(1)                := 'cn';
      -- Fetch the user groups of this user
      l_retval                  :=
         dbms_ldap_utl.get_group_membership(l_session
                                           ,l_user_handle
                                           ,dbms_ldap_utl.direct_membership
                                           ,l_attrs
                                           ,l_user_groups);
      IF l_user_groups.COUNT > 0
      THEN
         FOR i IN l_user_groups.FIRST .. l_user_groups.LAST
         LOOP
            l_retval      :=
               dbms_ldap_utl.get_property_names(l_user_groups(i)
                                               ,l_properties);
            IF l_properties.COUNT > 0
            THEN
               FOR j IN l_properties.FIRST .. l_properties.LAST
               LOOP
                  l_retval      :=
                     dbms_ldap_utl.get_property_values(l_user_groups(i)
                                                      ,l_properties(j)
                                                      ,l_property_values);
                  FOR k IN l_property_values.FIRST .. l_property_values.LAST
                  LOOP
                     IF l_property_values(k) IS NOT NULL
                     THEN
                        --l_roles(l_index) :=  l_property_values(k);
                        --l_index          := l_index + 1;
                        PIPE ROW (l_property_values(k));
                     END IF;
                  END LOOP;
               END LOOP;
            END IF;
         END LOOP;
      END IF;
      dbms_ldap_utl.free_handle(l_user_handle);
      -- Disconnect from the LDAP server.
      l_retval                  := dbms_ldap.unbind_s(ld => l_session);
   --RETURN l_roles;
   END f_get_ldap_roles;
END gen_pck_authentication;
/

/
QUIT
