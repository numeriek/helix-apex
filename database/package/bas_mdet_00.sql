CREATE OR REPLACE PACKAGE "HELIX"."BAS_MDET_00" IS

--  PL/SQL-table om mdet_id'op te slaan (zie After IUD Row triggers)
-- vooralsnog worden alleen mdet_id en meet_id gebruikt!
TYPE mdet_row_type IS RECORD
(  MDET_ID  bas_meetwaarde_details.mdet_id%type
,  MEET_ID  bas_meetwaarde_details.meet_id%type
-- , VOLGORDE bas_meetwaarde_details.volgorde%type
-- , PROMPT bas_meetwaarde_details.prompt%type
-- , WAARDE bas_meetwaarde_details.waarde%type
);
TYPE mdet_tab_type IS TABLE OF mdet_row_type
INDEX BY BINARY_INTEGER;
mdet_tab  mdet_tab_type;

-- initialiseer pl/sql-table
PROCEDURE init_plsql_table;

-- zet mdet_id in pl/sql-table als waarde wijzigt
PROCEDURE add_plsql_table
( p_mdet_id IN NUMBER
, p_meet_id IN NUMBER
, p_oude_waarde IN VARCHAR2
, p_nieuwe_waarde IN VARCHAR2
);

-- bepaal meetwaarde uit de meetwaarde details
FUNCTION meetwaarde
( p_meet_id IN NUMBER
)
RETURN VARCHAR2;

-- bepaal waarde van een detail-element via uitvoeren van een berekening
FUNCTION bereken
( p_meet_id IN NUMBER
, p_berekening IN VARCHAR2
)
RETURN VARCHAR2;

-- zet juiste detail waarde in bas_meetwaarden
-- aangeroepen vanuit AfterStatement triggers op bas_meetwaarde_details
PROCEDURE zet_meetwaarde
( p_mdet_id   IN   NUMBER
, p_meet_id   IN   NUMBER  :=  NULL
);

-- in forms worden velden (meet.meetwaarde en berekeningen) al
-- gevuld: zie library KGCLIB60.DETAILS package
-- de database triggers hoeven niks te doen
PROCEDURE in_form
( p_ja IN BOOLEAN := TRUE
);

-- aangroepen vanuit after-statement triggers
PROCEDURE  after_statement_trg;

-- in kgc_toegestane_waarden staat de te tonen waarde (ipv. de database waarde)
FUNCTION getoonde_waarde
( p_mwss_id IN NUMBER
, p_db_waarde IN VARCHAR2
)
RETURN VARCHAR2;
PRAGMA RESTRICT_REFERENCES( getoonde_waarde, WNDS, WNPS );

-- retourneer de (detail-)waarde van een element, bv. meetwaarde1...
FUNCTION  detail_waarde
(  p_meet_id  IN  NUMBER
,  p_code  IN  VARCHAR2  -- volgorde mag ook!!
)
RETURN  VARCHAR2;
PRAGMA  RESTRICT_REFERENCES( detail_waarde, WNDS, WNPS );

-- Mantis 711 nieuwe functie
-- retourneer indicatie gevuld_jn van een kolom in een werklijst, dus is er een gevulde waarde
-- voor een extra kolom ( prompt ) van een werklijst
-- Wordt gebruikt voor het vullen van mdet_waarde_gevuld_jn van kgc_wlst_mdet_vw
FUNCTION  prompt_gevuld_jn
(  p_wlst_id   IN  NUMBER
,  p_volgorde  IN  NUMBER
)
RETURN  VARCHAR2;

FUNCTION stoftest_waarde
( p_onde_id IN NUMBER
, p_stof_code IN VARCHAR2
)
RETURN  VARCHAR2;

-- controleer waarde op type en lengte
-- retourneer de verherformatteerde waarde
PROCEDURE controleer_waarde
( p_mdet_id IN NUMBER
, x_waarde IN OUT VARCHAR2
);
END BAS_MDET_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."BAS_MDET_00" IS

updates_in_form BOOLEAN := FALSE;

PROCEDURE in_form
( p_ja IN BOOLEAN := TRUE
)
IS
BEGIN
  updates_in_form := NVL( p_ja, TRUE );
END in_form;

PROCEDURE init_plsql_table
IS
BEGIN
  mdet_tab.delete;
END init_plsql_table;

PROCEDURE add_plsql_table
( p_mdet_id IN NUMBER
, p_meet_id IN NUMBER
, p_oude_waarde IN VARCHAR2
, p_nieuwe_waarde IN VARCHAR2
)
IS
  i BINARY_INTEGER := NVL(mdet_tab.last,0) + 1;
BEGIN
  IF ( p_mdet_id IS NOT NULL )
  THEN
--  MK, 26-02-2004: ook als de waarde niet wijzigt, moet berekening plaatsvinden (mdet_ok is mogelijk gewijzigd!)
--    IF ( NVL( p_oude_waarde, CHR(2) ) <> NVL( p_nieuwe_waarde, CHR(2) ) )
--    THEN
      mdet_tab(i).mdet_id := p_mdet_id;
      mdet_tab(i).meet_id := p_meet_id;
--    END IF;
  END IF;
END add_plsql_table;

FUNCTION meetwaarde
( p_meet_id IN NUMBER
)
RETURN  VARCHAR2
IS
  CURSOR mwss_cur
  ( b_meet_id in number
  )
  IS
    SELECT null
	FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarden meet
    WHERE  meet.mwst_id = mwss.mwst_id
    AND    mwss.gebruik IS NOT NULL
    AND    meet.meet_id = b_meet_id
    ;
  CURSOR mdet_cur
  ( b_meet_id in number
  )
  IS
    SELECT getoonde_waarde( mwss.mwss_id, mdet.waarde) waarde
    FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
	,      bas_mdet_ok mdok
    WHERE  meet.meet_id = mdet.meet_id
    AND    meet.mwst_id = mwss.mwst_id
    AND    mdet.volgorde = mwss.volgorde
	AND    mwss.gebruik IS NOT NULL
    AND    mdet.waarde IS NOT NULL
    AND    meet.meet_id = b_meet_id
	AND    mdet.mdet_id = mdok.mdet_id (+)
	AND    nvl( mdok.ok, 'J' ) = 'J'
    ORDER BY mwss.gebruik ASC
    ,        mwss.volgorde ASC
    ;
  CURSOR meet_cur
  ( b_meet_id in number
  )
  IS
    SELECT meet.meetwaarde
    FROM   bas_meetwaarden meet
    WHERE  meet.meet_id = b_meet_id
    ;
  v_dummy varchar2(1);
  v_return VARCHAR2(1000);
  v_num NUMBER;
BEGIN
  OPEN  mwss_cur( b_meet_id => p_meet_id );

  FETCH mwss_cur
  INTO  v_dummy;
  IF ( mwss_cur%NOTFOUND )
  THEN
    -- indien geen meetwaardestructuur (met gebruik) gevonden, haal dan meet.meetwaarde op
    OPEN  meet_cur( b_meet_id => p_meet_id );
    FETCH meet_cur
    INTO  v_return;
    CLOSE meet_cur;
  ELSE
    OPEN  mdet_cur ( b_meet_id => p_meet_id );
    FETCH mdet_cur
    INTO  v_return;
    CLOSE mdet_cur;
  END IF;
  CLOSE mwss_cur;

  -- formatteer numerieke waarde
  BEGIN
     v_num := TO_NUMBER( v_return );
     v_return := bas_meet_00.formatteer
                 ( p_meet_id => p_meet_id
                 , p_waarde => v_return
                 );
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END;
  RETURN( v_return );
END meetwaarde;

FUNCTION bereken
( p_meet_id IN NUMBER
, p_berekening IN VARCHAR2
)
RETURN   VARCHAR2
IS
  v_return VARCHAR2(1000);
  v_functie VARCHAR2(1000);
  v_gem VARCHAR2(1000);
  v_teller VARCHAR2(1000);
  v_noemer INTEGER := 0;
  v_waarde VARCHAR2(1000);
  v_num_waarde NUMBER;
  CURSOR gem_cur
  ( b_berekening IN VARCHAR2
  )
  IS
    SELECT mdet.waarde
    ,      mwss.code
    FROM   bas_mdet_ok mdok
    ,      bas_meetwaarde_details mdet
    ,      kgc_mwst_samenstelling mwss
    ,      bas_meetwaarden meet
    WHERE  mdet.meet_id = meet.meet_id
    AND    mdet.volgorde = mwss.volgorde
    AND    mwss.mwst_id = meet.mwst_id
    AND    meet.meet_id = p_meet_id
    AND    INSTR( b_berekening, mwss.code ) > 0
    AND    mdet.mdet_id = mdok.mdet_id (+)
    AND    NVL( mdok.ok, 'J' ) = 'J'
    AND    mdet.waarde IS NOT NULL
    ;
  CURSOR mwss_cur
  ( b_berekening IN VARCHAR2
  )
  IS
    SELECT mwss.code
    ,      mdet.waarde
    FROM   bas_meetwaarde_details mdet
    ,      kgc_mwst_samenstelling mwss
    ,      bas_meetwaarden meet
    WHERE  mdet.meet_id = meet.meet_id
    AND    mdet.volgorde = mwss.volgorde
    AND    mwss.mwst_id = meet.mwst_id
    AND    meet.meet_id = p_meet_id
    AND    INSTR( b_berekening, mwss.code ) > 0
    ;
  mwss_rec mwss_cur%rowtype;
  v_nog_niet BOOLEAN := FALSE;
BEGIN
  v_functie := p_berekening;
  v_gem := kgc_util_00.strip
           ( p_string => v_functie
           , p_tag_va => 'AVG('
           , p_tag_tm => ')'
           , p_rest => 'N'
           );
  IF ( v_gem IS NOT NULL )
  THEN
    v_teller := 0;
    v_noemer := 0;
    FOR r IN gem_cur( v_gem )
    LOOP
      v_num_waarde := kgc_reken.rekenwaarde( r.waarde );
      IF ( v_num_waarde IS NOT NULL )
      THEN
        BEGIN
          v_teller := v_teller + v_num_waarde;
          v_noemer := v_noemer + 1;
        EXCEPTION
          WHEN VALUE_ERROR
          THEN
            NULL;
        END;
      END IF;
    END LOOP;
    BEGIN
      v_functie := REPLACE( v_functie, 'AVG('||v_gem||')', TO_CHAR( v_teller / v_noemer ) );
    EXCEPTION
      WHEN OTHERS  -- value_error, zero_divide
      THEN
        v_functie := REPLACE( v_functie, 'AVG('||v_gem||')', NULL );
    END;
  END IF;

  OPEN  mwss_cur( v_functie );
  FETCH mwss_cur
  INTO  mwss_rec;
  IF ( mwss_cur%found )
  THEN
    LOOP
      v_waarde := mwss_rec.waarde;
      IF ( v_waarde IS NULL )
      THEN
        v_nog_niet := TRUE;
        EXIT;
      ELSE
        v_functie := REPLACE( v_functie, mwss_rec.code, v_waarde );
      END IF;
      FETCH mwss_cur
      INTO  mwss_rec;
      IF ( mwss_cur%notfound )
      THEN
        EXIT;
      END IF;
    END LOOP;
  END IF;
  IF ( mwss_cur%isopen )
  THEN
    CLOSE mwss_cur;
  END IF;
  IF ( v_functie IS NOT NULL
   AND NOT v_nog_niet
     )
  THEN
    v_return := kgc_util_00.dyn_exec( 'begin :result := '||v_functie||'; end;' );
  END IF;
  RETURN( v_return );
END bereken;

/****************************************************************************
   NAME:       zet_meetwaarde
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        31-05-2006  HZO              melding 11266
*****************************************************************************/
PROCEDURE zet_meetwaarde
( p_mdet_id IN NUMBER
, p_meet_id IN NUMBER := NULL
)
IS
  v_meetwaarde VARCHAR2(1000);

  CURSOR mdet_cur
  IS
    SELECT meet_id
    ,      volgorde
    ,      prompt
    ,      waarde
    FROM   bas_meetwaarde_details
    WHERE  mdet_id = p_mdet_id
    ;
  mdet_rec mdet_cur%rowtype;

  CURSOR mwss_cur
  IS
    SELECT mwss.volgorde
    ,      mwss.prompt
    ,      mwss.berekening
    FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarden meet
    WHERE  mwss.berekening IS NOT NULL
    AND    mwss.volgorde <> mdet_rec.volgorde
    AND    mwss.mwst_id = meet.mwst_id
    AND    meet.meet_id = mdet_rec.meet_id
    ;

  CURSOR chk_mdet_cur
  ( b_meet_id IN NUMBER
  , b_volgorde IN NUMBER
  )
  IS
    SELECT mdet_id
    FROM   bas_meetwaarde_details
    WHERE  meet_id = b_meet_id
    AND    volgorde = b_volgorde
    ;
  v_mdet_id NUMBER;
BEGIN
  OPEN  mdet_cur;
  FETCH mdet_cur
  INTO  mdet_rec;
  CLOSE mdet_cur;
  -- bij deletes id mdet-record al verdwenen
  IF ( mdet_rec.meet_id IS NULL )
  THEN
    mdet_rec.meet_id := p_meet_id;
  END IF;

  -- voer eerst eventuele berekeningen uit
  FOR mwss_rec IN mwss_cur
  LOOP
    v_meetwaarde := NULL;
    v_meetwaarde := bereken( p_meet_id => mdet_rec.meet_id
                           , p_berekening => mwss_rec.berekening
                           );
    controleer_waarde
    ( p_mdet_id => p_mdet_id
    , x_waarde => v_meetwaarde
    );
    v_mdet_id := NULL;
    OPEN  chk_mdet_cur( b_meet_id => mdet_rec.meet_id
                      , b_volgorde => mwss_rec.volgorde
                      );
    FETCH chk_mdet_cur
    INTO  v_mdet_id;
    CLOSE chk_mdet_cur;
    IF ( v_mdet_id IS NOT NULL )
    THEN
      BEGIN
        UPDATE bas_meetwaarde_details
        SET    waarde = v_meetwaarde
        WHERE  mdet_id = v_mdet_id
        AND    NVL(waarde,CHR(2)) <> NVL(v_meetwaarde,CHR(2))
        ;
      EXCEPTION
        WHEN TIMEOUT_ON_RESOURCE
        THEN
          raise_application_error
          ( -20000, 'Berekening kan niet worden verwerkt!'
          );
      END;
    ELSE
      BEGIN
        -- Normaliter bestaat deze rij al!!!
        INSERT INTO bas_meetwaarde_details
        ( meet_id
        , volgorde
        , prompt
        , waarde
        )
        VALUES
        ( mdet_rec.meet_id
        , mwss_rec.volgorde
        , mwss_rec.prompt
        , v_meetwaarde
        );
      EXCEPTION
        WHEN OTHERS
        THEN
          NULL;
      END;
    END IF;
  END LOOP;

  -- zet juiste waarde uit bas_meetwaarde_details in bas_meetwaarden
  v_meetwaarde := NULL;
  v_meetwaarde := meetwaarde( p_meet_id => mdet_rec.meet_id );
  BEGIN
    UPDATE bas_meetwaarden
     SET   meetwaarde = v_meetwaarde
     ,     datum_meting = trunc(SYSDATE)
     ,     mede_id = kgc_mede_00.medewerker_id
     WHERE meet_id = mdet_rec.meet_id
     AND   NVL(meetwaarde,CHR(2)) <> NVL(v_meetwaarde,CHR(2))
     ;
  EXCEPTION
    WHEN TIMEOUT_ON_RESOURCE
    THEN
      raise_application_error
      ( -20000, 'Meetwaarde kan niet worden bijgewerkt!'
      );
  END;
END zet_meetwaarde;

PROCEDURE after_statement_trg
 IS
  i BINARY_INTEGER := NULL;
  CURSOR mwss_cur
  ( b_mdet_id IN NUMBER
  )
  IS
    SELECT meet.meet_id
    ,      mdet_ber.mdet_id
    ,      mwss.berekening
    FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarde_details mdet_ber
    ,      bas_meetwaarden meet
    ,      bas_meetwaarde_details mdet
    WHERE  mdet.meet_id = meet.meet_id
    AND    meet.mwst_id = mwss.mwst_id
    AND    mdet.mdet_id <> mdet_ber.mdet_id
    AND    meet.meet_id = mdet_ber.meet_id
    AND    mwss.volgorde = mdet_ber.volgorde
    AND    mwss.berekening IS NOT NULL
    AND    mdet.mdet_id = b_mdet_id
    ;
  v_berekende_waarde VARCHAR2(1000);
  v_mdet_id NUMBER;
  v_meet_id NUMBER;
BEGIN
  i := mdet_tab.FIRST;
  WHILE i IS NOT NULL
  LOOP
    v_mdet_id := mdet_tab(i).MDET_ID;
    v_meet_id := mdet_tab(i).MEET_ID;
    FOR r IN mwss_cur( v_mdet_id )
    LOOP
      v_berekende_waarde := NULL;
      v_berekende_waarde := bereken( p_meet_id => r.MEET_ID
                                   , p_berekening => r.berekening
                                   );
      v_berekende_waarde := bas_meet_00.formatteer
                            ( p_meet_id => r.meet_id
                            , p_waarde => v_berekende_waarde
                            );
      -- ook NULL-waarden opslaan     IF ( v_berekende_waarde IS NOT NULL )
      --THEN
        UPDATE bas_meetwaarde_details
        SET    waarde = v_berekende_waarde
        WHERE  mdet_id = r.mdet_id
        ;
      --END IF;
    END LOOP;
    -- zet juiste waarde uit bas_meetwaarde_details in bas_meetwaarden
    IF ( updates_in_form )
    THEN
      -- Omzeil "Record has been changed by another user. Re-query to see the changes"
      NULL;
    ELSE
      zet_meetwaarde
      ( p_mdet_id => v_mdet_id
      , p_meet_id => v_meet_id
      );
    END IF;
    i := mdet_tab.NEXT(i);
  END LOOP;
END after_statement_trg;

FUNCTION getoonde_waarde
( p_mwss_id IN NUMBER
, p_db_waarde IN VARCHAR2
)
RETURN VARCHAR2
IS
  CURSOR mwtw_cur
  IS
    SELECT waarde
    FROM   kgc_mwss_toegestane_waarden
    WHERE  mwss_id = p_mwss_id
    AND    db_waarde = p_db_waarde
    ;
  mwtw_rec mwtw_cur%rowtype;
BEGIN
  OPEN  mwtw_cur;
  FETCH mwtw_cur
  INTO  mwtw_rec;
  IF ( mwtw_cur%notfound )
  THEN
    mwtw_rec.waarde := p_db_waarde;
  END IF;
  CLOSE mwtw_cur;
  RETURN( mwtw_rec.waarde );
EXCEPTION
  WHEN OTHERS
  THEN
    RETURN( p_db_waarde );
END getoonde_waarde;

FUNCTION detail_waarde
( p_meet_id IN NUMBER
, p_code IN VARCHAR2
)
RETURN  VARCHAR2
IS
  CURSOR mdet_cur
  IS
    SELECT mwss.mwss_id
    ,      mdet.waarde
    FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
    WHERE  mwss.mwst_id = meet.mwst_id
    AND    mdet.volgorde = mwss.volgorde
    AND    mdet.meet_id = meet.meet_id
    AND    meet.meet_id = p_meet_id
    AND    mwss.code = UPPER( p_code )
    ;
  -- 2e cursor met identiek rowtype
  CURSOR mdet2_cur
  ( p_volgorde IN NUMBER
  )
  IS
    SELECT mwss.mwss_id
    ,      mdet.waarde
    FROM   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
    WHERE  mwss.mwst_id (+) = meet.mwst_id
    AND    mdet.volgorde = NVL( mwss.volgorde, p_volgorde )
    AND    mdet.meet_id = meet.meet_id
    AND    meet.meet_id = p_meet_id
    AND    mdet.volgorde = p_volgorde
    ;
  mdet_rec mdet_cur%rowtype;
  v_num NUMBER;
BEGIN
  OPEN  mdet_cur;
  FETCH mdet_cur
  INTO  mdet_rec;
  IF ( mdet_cur%notfound )
  THEN
    BEGIN
      v_num := TO_NUMBER( p_code );
      OPEN  mdet2_cur( p_code );
      FETCH mdet2_cur
      INTO  mdet_rec;
      CLOSE mdet2_cur;
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;
    END;
  END IF;
  CLOSE mdet_cur;
  mdet_rec.waarde := getoonde_waarde
                     ( p_mwss_id => mdet_rec.mwss_id
                     , p_db_waarde => mdet_rec.waarde
                     );
  RETURN( mdet_rec.waarde );
END detail_waarde;

-- Mantis 711 nieuwe functie
-- Mantis 1556 aangepast, na CYTO-Utrecht is query extreem traag
--             kgc_werklijst_items ipv kgc_wlst_mdet_vw zou de oplossing zijn

FUNCTION prompt_gevuld_jn
( p_wlst_id  IN NUMBER
, p_volgorde IN NUMBER
)
RETURN  VARCHAR2
IS
  CURSOR wlst_prompt
      IS
  SELECT count(*)
  FROM   kgc_mwst_samenstelling   mwss
  ,      bas_meetwaarde_details   mdet
  ,      bas_meetwaarden          meet
  WHERE  mwss.mwst_id  = meet.mwst_id
  AND    mdet.volgorde = mwss.volgorde
  AND    mdet.meet_id  = meet.meet_id
  AND    meet.meet_id  in (select meet_id
                           --from kgc_wlst_mdet_vw             --1556
                             from kgc_werklijst_items wlit     --1556
                            where wlit.wlst_id = p_wlst_id )
  AND    mwss.volgorde =  p_volgorde
  AND    mdet.waarde  is not null
  ;

  l_aantal NUMBER;
BEGIN
  l_aantal := 0;

  OPEN  wlst_prompt;
  FETCH wlst_prompt
  INTO  l_aantal;
  CLOSE wlst_prompt;

  if l_aantal = 0
  then
    return ('N');
  else
    return ('J');
  end if;

END prompt_gevuld_jn;

FUNCTION stoftest_waarde
( p_onde_id IN NUMBER
, p_stof_code IN VARCHAR2
)
RETURN  VARCHAR2
IS
  v_return VARCHAR2(1000);
  CURSOR meet_cur
  IS
    SELECT meet.meet_id
    FROM   kgc_stoftesten stof
    ,      kgc_onderzoeken onde
    ,      bas_metingen meti
    ,      bas_meetwaarden meet
    WHERE  meet.meti_id = meti.meti_id
    AND    meti.onde_id = onde.onde_id
    AND    meet.stof_id = stof.stof_id
    AND    meti.onde_id = p_onde_id
    AND    stof.kafd_id = onde.kafd_id
    AND    stof.code = UPPER( p_stof_code )
    ORDER BY meet.meet_id DESC -- laatste eerst
    ;
  v_meet_id NUMBER;
BEGIN
  OPEN  meet_cur;
  FETCH meet_cur
  INTO  v_meet_id;
  CLOSE meet_cur;
  v_return := meetwaarde( v_meet_id );
  RETURN( v_return );
END stoftest_waarde;

PROCEDURE toegestane_waarde
( p_mwss_id IN NUMBER
, p_presentatie IN VARCHAR2
, p_waarde IN OUT VARCHAR2
)
IS
  -- toegestane waarde ?
  CURSOR mwtw_cur
  IS
    SELECT NULL
    FROM   kgc_mwss_toegestane_waarden
    WHERE  mwss_id = p_mwss_id
    ;
  -- database waarde
  CURSOR mwtw_cur2
  IS
    SELECT db_waarde
    FROM   kgc_mwss_toegestane_waarden
    WHERE  mwss_id = p_mwss_id
    AND    p_waarde IN ( waarde, db_waarde )
    ;
  v_db_waarde VARCHAR2(1000);
  v_dummy VARCHAR2(1);
  v_melding VARCHAR2(100);
BEGIN
  OPEN  mwtw_cur;
  FETCH mwtw_cur
  INTO  v_dummy;
  IF ( mwtw_cur%found ) -- toegestane waarden gedefinieerd
  THEN
    OPEN  mwtw_cur2;
    FETCH mwtw_cur2
    INTO  v_db_waarde;
    CLOSE mwtw_cur2;
    -- alleen poplist en checkbox (geen extra controle) zijn restrictief beperkt
    IF ( 1=1 -- p_presentatie = 'PLIST' --
     AND p_waarde IS NOT NULL
     AND v_db_waarde IS NULL
       )
    THEN
      CLOSE mwtw_cur;
      qms$errors.show_message
      ( 'KGC-00000'
      , p_param0 => p_waarde || ' is niet toegestaan'
      , p_param1 => p_waarde || ' komt niet voor in de lijst met toegestane waarde bij dit veld (zie meetwaardestructuur samenstelling)'
      , p_errtp => 'E'
      , p_rftf => TRUE
      );
    ELSE
    	p_waarde := NVL( v_db_waarde, p_waarde );
    END IF;
  END IF;
  CLOSE mwtw_cur;
END toegestane_waarde;

PROCEDURE controleer_waarde
( p_mdet_id IN NUMBER
, x_waarde IN OUT VARCHAR2
)
IS
  v_waarde VARCHAR2(1000) := x_waarde;
  CURSOR mdet_cur
  IS
    SELECT mdet.meet_id
    ,      meet.mwst_id
    ,      mdet.volgorde
    ,      mdet.prompt
    FROM   bas_meetwaarden meet
    ,      bas_meetwaarde_details mdet
    WHERE  mdet.meet_id = meet.meet_id
    AND    mdet.mdet_id = p_mdet_id
    ;
  mdet_rec mdet_cur%rowtype;

  CURSOR mwss_cur
  IS
    SELECT mwss_id
    ,      volgorde
    ,      code
    ,      presentatie
    ,      soort
    FROM   kgc_mwst_samenstelling
    WHERE  mwst_id = mdet_rec.mwst_id
    AND    volgorde = mdet_rec.volgorde
    AND    prompt = mdet_rec.prompt
    ;
  mwss_rec mwss_cur%rowtype;

  v_test_nummer NUMBER;
BEGIN
  OPEN  mdet_cur;
  FETCH mdet_cur
  INTO  mdet_rec;
  CLOSE mdet_cur;

  OPEN  mwss_cur;
  FETCH mwss_cur
  INTO  mwss_rec;
  CLOSE mwss_cur;
  IF ( mwss_rec.soort = 'N' )
  THEN
    BEGIN
      v_test_nummer := TO_NUMBER( v_waarde );
      x_waarde := bas_meet_00.formatteer
                  ( p_meet_id => mdet_rec.meet_id
                  , p_waarde => x_waarde
                  );
    EXCEPTION
      WHEN VALUE_ERROR
      THEN
        qms$errors.show_message
        ( p_mesg => 'KGC-00000'
        , p_param0 => 'Waarde moet numeriek zijn!'
        , p_errtp => 'E'
        , p_rftf => TRUE
        );
    END;
  END IF;
  toegestane_waarde
  ( p_mwss_id => mwss_rec.mwss_id
  , p_presentatie => mwss_rec.presentatie
  , p_waarde => x_waarde
  );
END controleer_waarde;
END bas_mdet_00;
/

/
QUIT
