CREATE OR REPLACE PACKAGE "HELIX"."KGC_PAPP_00" IS
  FUNCTION registry_code(
      p_pcap_id  IN  NUMBER
    , p_mede_id  IN  NUMBER DEFAULT NULL
    , p_kafd_id  IN  NUMBER DEFAULT NULL
    , p_ongr_id  IN  NUMBER DEFAULT NULL
                        ) RETURN VARCHAR2;
  FUNCTION applicatie_programma(
      p_pcap_id  IN  NUMBER
    , p_mede_id  IN  NUMBER DEFAULT NULL
    , p_kafd_id  IN  NUMBER DEFAULT NULL
    , p_ongr_id  IN  NUMBER DEFAULT NULL
                        ) RETURN VARCHAR2;
  PRAGMA RESTRICT_REFERENCES( registry_code, WNDS, WNPS, RNPS );
  PRAGMA RESTRICT_REFERENCES( applicatie_programma, WNDS, WNPS, RNPS );
END KGC_PAPP_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_PAPP_00" IS
  --  =======================================================================
  --  G L O B A L E   C U R S O R E N   E T C.
  --
  CURSOR papp_cur(
      p_pcap_id  IN  NUMBER
    , p_mede_id  IN  NUMBER
    , p_kafd_id  IN  NUMBER
    , p_ongr_id  IN  NUMBER
                 ) IS
    SELECT papp.registry_code
    ,      papp.applicatie_programma
    ,      1
    FROM   kgc_pc_applicatie_programmas  papp
    WHERE  papp.pcap_id = p_pcap_id
    AND   (papp.mede_id = p_mede_id
      AND  p_mede_id    IS NOT NULL )
    UNION
    SELECT papp.registry_code
    ,      papp.applicatie_programma
    ,      2
    FROM   kgc_pc_applicatie_programmas  papp
    WHERE  papp.pcap_id = p_pcap_id
    AND   (papp.ongr_id = p_ongr_id
      AND  p_ongr_id    IS NOT NULL )
    UNION
    SELECT papp.registry_code
    ,      papp.applicatie_programma
    ,      3
    FROM   kgc_pc_applicatie_programmas  papp
    WHERE  papp.pcap_id = p_pcap_id
    AND   (papp.kafd_id = p_kafd_id
      AND  p_kafd_id    IS NOT NULL )
    UNION
    SELECT pcap.registry_code
    ,      pcap.applicatie_programma
    ,      4
    FROM   kgc_pc_applicaties  pcap
    WHERE  pcap.pcap_id = p_pcap_id
    ORDER BY 3
    ;

  --  =======================================================================
  --  L O K A L E    P R O C E D U R E S / F U N C T I O N S
  --
  --  =======================================================================
  --  P U B L I E K E   P R O C E D U R E S / F U N C T I O N S
  --
  FUNCTION registry_code(
      p_pcap_id  IN  NUMBER
    , p_mede_id  IN  NUMBER DEFAULT NULL
    , p_kafd_id  IN  NUMBER DEFAULT NULL
    , p_ongr_id  IN  NUMBER DEFAULT NULL
                        ) RETURN VARCHAR2 IS
    papp_rec  papp_cur%ROWTYPE;
    v_mede_id  NUMBER := NVL( p_mede_id, kgc_mede_00.medewerker_id ( USER ));
  BEGIN
    IF  p_pcap_id IS NOT NULL
    THEN
      OPEN  papp_cur( p_pcap_id  => p_pcap_id
                    , p_mede_id  => v_mede_id
                    , p_kafd_id  => p_kafd_id
                    , p_ongr_id  => p_ongr_id
                    );
      FETCH papp_cur
      INTO  papp_rec;
      CLOSE papp_cur;
    END IF;
    RETURN papp_rec.registry_code;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN RETURN NULL;
    WHEN OTHERS        THEN RAISE;
  END;

  FUNCTION applicatie_programma(
      p_pcap_id  IN  NUMBER
    , p_mede_id  IN  NUMBER DEFAULT NULL
    , p_kafd_id  IN  NUMBER DEFAULT NULL
    , p_ongr_id  IN  NUMBER DEFAULT NULL
                        ) RETURN VARCHAR2 IS
    papp_rec  papp_cur%ROWTYPE;
    v_mede_id  NUMBER := NVL( p_mede_id, kgc_mede_00.medewerker_id ( USER ));
  BEGIN
    IF  p_pcap_id IS NOT NULL
    THEN
      OPEN  papp_cur( p_pcap_id  => p_pcap_id
                    , p_mede_id  => v_mede_id
                    , p_kafd_id  => p_kafd_id
                    , p_ongr_id  => p_ongr_id
                    );
      FETCH papp_cur
      INTO  papp_rec;
      CLOSE papp_cur;
    END IF;
    RETURN papp_rec.applicatie_programma;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN RETURN NULL;
    WHEN OTHERS        THEN RAISE;
  END;

END kgc_papp_00;
/

/
QUIT
