CREATE OR REPLACE PACKAGE "HELIX"."KGC_LOGS_00" IS
-- PL/SQL Specification
-- Foutieve scans worden geregistreerd.
PROCEDURE log_isolatie_scan
( p_afdeling IN VARCHAR2
, p_isolatielijst IN VARCHAR2
, p_stap IN VARCHAR2
, p_fractie IN VARCHAR2
, p_monster IN VARCHAR2
, p_opmerking IN VARCHAR2
, x_waarschuwing OUT VARCHAR2
);


END KGC_LOGS_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_LOGS_00" IS
PROCEDURE log_isolatie_scan
( p_afdeling IN VARCHAR2
, p_isolatielijst IN VARCHAR2
, p_stap IN VARCHAR2
, p_fractie IN VARCHAR2
, p_monster IN VARCHAR2
, p_opmerking IN VARCHAR2
, x_waarschuwing OUT VARCHAR2
)
IS
  v_sypa_log VARCHAR2(1);
  v_user     VARCHAR2(40) := USER;
BEGIN
  x_waarschuwing := NULL;
  v_sypa_log := kgc_sypa_00.standaard_waarde
                ( p_parameter_code => 'LOG_ISOLATIE_SCANS'
                , p_afdeling => p_afdeling
                );
  IF ( v_sypa_log IN ( 'J', 'W' ) ) -- Ja, Met waarschuwing
  THEN
    INSERT INTO bas_log_isolatie_scans
    ( oracle_uid
    , tijdstempel
    , afdeling
    , medewerker
    , isolatielijst
    , stap
    , monster
    , fractie
    , opmerking
    )
    VALUES
    ( v_user
    , SYSDATE
    , p_afdeling
    , kgc_mede_00.medewerker_naam
    , p_isolatielijst
    , p_stap
    , p_monster
    , p_fractie
    , p_opmerking
    );
    COMMIT;
    IF ( v_sypa_log = 'W' )
    THEN
      x_waarschuwing := 'Deze foutieve scan is gelogd.';
    END IF;
  END IF;
END log_isolatie_scan;
END kgc_logs_00;
/

/
QUIT
