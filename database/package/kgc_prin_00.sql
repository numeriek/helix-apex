CREATE OR REPLACE PACKAGE "HELIX"."KGC_PRIN_00" IS
-- aangeroepen in helix.helix_print.init / run_report
procedure printer_parameters
( p_uits_id in number
, x_kafd_id out number
, x_ongr_id out number
, x_taal_id out number
);

-- aangeroepen in helix.helix_print.init / run_report
procedure printer_parameters
( p_onde_id in number
, x_kafd_id out number
, x_ongr_id out number
, x_taal_id out number
);

-- aangeroepen in helix.helix_print.init
procedure bepaal_printer
( p_rapport in varchar2
, p_uits_id in number
, x_printer_jn  out varchar2
, x_printercode out varchar2
);

-- aangeroepen in helix.helix_print.init
procedure bepaal_printer
( p_rapport in varchar2
, p_onde_id in number
, x_printer_jn  out varchar2
, x_printercode out varchar2
);
-- algemene procedure
procedure bepaal_printer
( p_rapport in varchar2
, p_kafd_id in number
, p_ongr_id in number DEFAULT NULL
, p_taal_id in number DEFAULT NULL
, x_printer_jn  out varchar2
, x_printercode out varchar2
);
END KGC_PRIN_00;


/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_PRIN_00" IS
procedure printer_parameters
( p_uits_id in number
, x_kafd_id out number
, x_ongr_id out number
, x_taal_id out number
)
is
  cursor c_par_uits
  ( b_uits_id in number
  )
  is
    select onde.kafd_id
    ,      onde.ongr_id
    ,      onde.taal_id
    from   kgc_uitslagen uits
    ,      kgc_onderzoeken onde
    where  uits.onde_id = onde.onde_id
    and    uits.uits_id = b_uits_id
    ;
begin
  -- met de uitslag id kun je alle benodigde parameters ophalen.
  -- met deze parameters kun je bepalen welk rapportdefinitie gebruikt wordt
  open  c_par_uits (b_uits_id => p_uits_id );
  fetch c_par_uits
  into  x_kafd_id
  ,     x_ongr_id
  ,     x_taal_id;
  close c_par_uits;
end printer_parameters;

procedure printer_parameters
( p_onde_id in number
, x_kafd_id out number
, x_ongr_id out number
, x_taal_id out number
)
is
  cursor c_par_onde (b_onde_id in number)
  is
    select onde.kafd_id
    ,      onde.ongr_id
    ,      onde.taal_id
    from kgc_onderzoeken onde
    where onde.onde_id = b_onde_id
    ;
begin
  -- met de onderzoek id kun je alle benodigde parameters ophalen.
  -- met deze parameters kun je bepalen welk rapportdefinitie gebruikt wordt
  open  c_par_onde (b_onde_id => p_onde_id );
  fetch c_par_onde
  into  x_kafd_id
  ,     x_ongr_id
  ,     x_taal_id;
  close c_par_onde;
end printer_parameters;

procedure bepaal_printer
( p_rapport varchar2
, p_kafd_id in number
, p_ongr_id in number DEFAULT NULL
, p_taal_id in number DEFAULT NULL
, x_printer_jn  out varchar2
, x_printercode out varchar2
)
is
  cursor prin_cur
  ( b_prin_code in varchar2
  )
  is
    select prin.code
    from   kgc_printers prin
    where  prin.code = b_prin_code
    ;
  cursor rapr_cur
  ( b_rapp_id in number
  )
  is
    select prin.code
    from   kgc_rapport_printers rapr
    ,      kgc_printers prin
    where  rapr.prin_id = prin.prin_id
    and  ( rapr.mede_id = kgc_mede_00.medewerker_id
        or rapr.mede_id is null
         )
    and    rapr.rapp_id  = b_rapp_id
    order by decode( rapr.mede_id
                   , null, 2
                   , 1
                   )
    ;
  cursor rapp_cur
  ( b_rapport in varchar2
  , b_kafd_id in number
  , b_ongr_id in number
  , b_taal_id in number
  )
  is
    select rapp.rapp_id
              , rapp.direct_naar_printer
    from   kgc_rapporten        rapp
    where  rapp.rapport  = b_rapport
    and    rapp.kafd_id  = b_kafd_id
    and  ( rapp.ongr_id = b_ongr_id
        or rapp.ongr_id is null
         )
    and  ( rapp.taal_id =  b_taal_id
        or rapp.taal_id is null
         )
    order by  rapp.kafd_id, rapp.ongr_id,rapp.taal_id
    ;
  v_rapp_id number;
  v_prin_code varchar2(10);
  v_direct_naar_printer varchar2(1);
begin
  begin
    v_prin_code := substr( kgc_sypa_00.standaard_waarde
                           ( p_parameter_code => 'STANDAARD_PRINTER'
                           , p_kafd_id => p_kafd_id
                           , p_ongr_id => p_ongr_id
                           )
                         , 1, 10
                         );
  exception when others
  then
    v_prin_code := NULL;
  end;
  if v_prin_code is not null
  then
    -- controleer of de opgegeven printer bestaat
    open  prin_cur( b_prin_code => v_prin_code );
    fetch prin_cur
    into  v_prin_code;
    close prin_cur;
  end if;

  -- als v_printer gevuld is, dan is er een standaard printer (systeemparameter)
  if ( v_prin_code is not null )
  then
    x_printer_jn := 'J';
  else
    -- haal de rapportafhandling ID uit KGC_RAPPORTEN
    open  rapp_cur( b_rapport => p_rapport
                  , b_kafd_id => p_kafd_id
                  , b_ongr_id => p_ongr_id
                  , b_taal_id => p_taal_id
                   );
    fetch rapp_cur
    into  v_rapp_id,v_direct_naar_printer;
    close rapp_cur;
    -- Als v_rapp_id null is, dan is er geen record gevonden in in KGC_RAPPORTEN
    -- dat bepaalt of het rapport direct naar de printer moet gaan.
    if v_rapp_id is not null
    and v_direct_naar_printer = 'J'
    then
       x_printer_jn := 'J';
      -- Als deze niet bestaat, geef een dummy waarde terug dat duidelijk maakt dat er geen printer
      -- gedefinieerd is.
      open  rapr_cur( b_rapp_id => v_rapp_id );
      fetch rapr_cur
      into  v_prin_code;
      close rapr_cur;
    else
      x_printer_jn := 'N';
    end if;
  end if;
  x_printercode := v_prin_code;
end bepaal_printer;

procedure bepaal_printer
( p_rapport in varchar2
, p_uits_id in number
, x_printer_jn out varchar2
, x_printercode out varchar2
)
is
  v_kafd_id number;
  v_ongr_id number;
  v_taal_id number;
  v_rapp_id number;
begin
  printer_parameters
  ( p_uits_id => p_uits_id
  , x_kafd_id => v_kafd_id
  , x_ongr_id => v_ongr_id
  , x_taal_id => v_taal_id
  );

  bepaal_printer
  ( p_rapport => p_rapport
  , p_kafd_id => v_kafd_id
  , p_ongr_id => v_ongr_id
  , p_taal_id => v_taal_id
  , x_printer_jn => x_printer_jn
  , x_printercode => x_printercode
  );
end bepaal_printer;

procedure bepaal_printer
( p_rapport in varchar2
, p_onde_id in number
, x_printer_jn out varchar2
, x_printercode out varchar2
)
is
  v_kafd_id number;
  v_ongr_id number;
  v_taal_id number;
  v_rapp_id number;
begin
  printer_parameters
  ( p_onde_id => p_onde_id
  , x_kafd_id => v_kafd_id
  , x_ongr_id => v_ongr_id
  , x_taal_id => v_taal_id
  );

  bepaal_printer
  ( p_rapport => p_rapport
  , p_kafd_id => v_kafd_id
  , p_ongr_id => v_ongr_id
  , p_taal_id => v_taal_id
  , x_printer_jn => x_printer_jn
  , x_printercode => x_printercode
  );
end bepaal_printer;
end   kgc_prin_00;
/

/
QUIT
