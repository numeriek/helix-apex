CREATE OR REPLACE PACKAGE "HELIX"."GEN_PCK_MAIL_TEMPLATES"
AS
   /*
   =============================================================================
      NAME:       gen_pck_mail_templates
      PURPOSE:    API for the gen_mail_templates table
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */
   -----------------------------------------------------------------------------
   -- delete procedure for gen_mail_templates
   -----------------------------------------------------------------------------
   PROCEDURE p_del_mail_template(i_mte_id IN gen_mail_templates.id%TYPE);
   --------------------------------------------------------------
   -- get procedure for table gen_mail_templates
   -----------------------------------------------------------------------------
   PROCEDURE p_get_mail_template(i_name     IN     gen_mail_templates.name%TYPE
                              ,o_mte_rec     OUT gen_mail_templates%ROWTYPE);
   -----------------------------------------------------------------------------
   -- procedure that sends a mail basend on a mail-template (possibly after
   -- replacements
   -----------------------------------------------------------------------------
   PROCEDURE p_send_mail(i_mte_rec     IN gen_mail_templates%ROWTYPE
                      ,i_cc_list       IN VARCHAR2 DEFAULT NULL
                      ,i_bcc_list      IN VARCHAR2 DEFAULT NULL
                      ,i_init_session  IN BOOLEAN DEFAULT FALSE);
   -----------------------------------------------------------------------------
   -- function to replace the string in a text from the template
   -----------------------------------------------------------------------------
   FUNCTION f_replace_template_value(i_message             IN VARCHAR2
                                    ,i_string              IN VARCHAR2
                                    ,i_replacement_string  IN VARCHAR2)
      RETURN VARCHAR2;
END gen_pck_mail_templates;
/
CREATE OR REPLACE PACKAGE BODY "HELIX"."GEN_PCK_MAIL_TEMPLATES"
AS
   /*
   =============================================================================
      NAME:       gen_pck_mail_templates
      PURPOSE:    API for the gen_mail_templates table
   =============================================================================
   HISTORY
   =======
   Date        Author       Description
   ----------  ------------ ----------------------------------------------------
   23-12-2016  The DOC      Creation
   =============================================================================
   */

   g_package  CONSTANT VARCHAR2(30) := $$plsql_unit || '.';
   -----------------------------------------------------------------------------
   -- delete procedure for gen_mail_templates
   -----------------------------------------------------------------------------
   PROCEDURE p_del_mail_template(i_mte_id IN gen_mail_templates.id%TYPE)
   IS
      l_scope  VARCHAR2(61) := g_package || 'p_del_mail_template';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      DELETE FROM gen_mail_templates
            WHERE id = i_mte_id;
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
   END p_del_mail_template;
   -----------------------------------------------------------------------------
   -- get procedure for table gen_mail_templates
   -----------------------------------------------------------------------------
   PROCEDURE p_get_mail_template(i_name     IN     gen_mail_templates.name%TYPE
                              ,o_mte_rec     OUT gen_mail_templates%ROWTYPE)
   IS
   BEGIN
      FOR c1 IN (SELECT *
                   FROM gen_mail_templates
                  WHERE name = i_name)
      LOOP
         o_mte_rec.id               := c1.id;
         o_mte_rec.name             := c1.name;
         o_mte_rec.description      := c1.description;
         o_mte_rec.subject          := c1.subject;
         o_mte_rec.sender           := c1.sender;
         o_mte_rec.MESSAGE          := c1.MESSAGE;
         o_mte_rec.message_html     := c1.message_html;
         o_mte_rec.recipient        := c1.recipient;
         o_mte_rec.created_on       := c1.created_on;
         o_mte_rec.created_by       := c1.created_by;
         o_mte_rec.modified_on      := c1.modified_on;
         o_mte_rec.modified_by      := c1.modified_by;
         o_mte_rec.application_id   := c1.application_id;
      END LOOP;
   END p_get_mail_template;
   FUNCTION f_replace_template_value(i_message             IN VARCHAR2
                                    ,i_string              IN VARCHAR2
                                    ,i_replacement_string  IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_scope  VARCHAR2(61) := g_package || 'f_replace_template_value';
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      /* Example:
       replace in the message_html the #APPLICATION_ID# by a value
          f_replace_template_value(message_html,'#APPLICATION_ID#, application_id)
     */
      RETURN REPLACE(i_message
                    ,i_string
                    ,i_replacement_string);
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
   END;
   -----------------------------------------------------------------------------
   -- procedure that sends a mail basend on a mail-template (possibly after
   -- replacements
   -----------------------------------------------------------------------------
   PROCEDURE p_send_mail(i_mte_rec       IN gen_mail_templates%ROWTYPE
                      ,i_cc_list       IN VARCHAR2 DEFAULT NULL
                      ,i_bcc_list      IN VARCHAR2 DEFAULT NULL
                      ,i_init_session  IN BOOLEAN DEFAULT FALSE)
   IS
      l_scope      VARCHAR2(61) := g_package || 'p_send_mail';
      l_recipient  gen_mail_templates.recipient%TYPE;
      l_cc_list    VARCHAR2(4000);
      l_bcc_list   VARCHAR2(4000);
   BEGIN
      logger.log_information(p_text => 'Start'
                            ,p_scope => l_scope);
      IF i_init_session
      THEN
         gen_pck_general.p_set_security_group_id;
      END IF;
      l_recipient   := i_mte_rec.recipient;
      l_cc_list     := i_cc_list;
      l_bcc_list    := i_bcc_list;
      -- When parameter SEND_MAIL_YN is set to N, then send the mail only to
      -- the test email address
      IF COALESCE(gen_pck_general.f_get_appl_parameter_value('SEND_MAIL_YN')
                 ,'N') = 'N'
      THEN
         l_recipient      :=
            gen_pck_general.f_get_appl_parameter_value('TEST_EMAIL_ADDRESS');
         l_cc_list    := NULL;
         l_bcc_list   := NULL;
      END IF;
      IF l_recipient IS NOT NULL
      THEN
         -- Use apex mail to send the actual mail
         apex_mail.send(p_to => l_recipient
                       ,p_from => i_mte_rec.sender
                       ,p_body => i_mte_rec.MESSAGE
                       ,p_body_html => i_mte_rec.message_html
                       ,p_subj => i_mte_rec.subject
                       ,p_cc => l_cc_list
                       ,p_bcc => l_bcc_list);
         apex_mail.push_queue;
      END IF;
      logger.log_information(p_text => 'End'
                            ,p_scope => l_scope);
   EXCEPTION
      WHEN OTHERS
      THEN
         logger.log_error(p_text => dbms_utility.format_error_backtrace
                         ,p_scope => l_scope);
   END p_send_mail;
END gen_pck_mail_templates;
/

/
QUIT
