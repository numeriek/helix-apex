CREATE OR REPLACE PACKAGE "HELIX"."KGC_TATO_00" IS
-- PL/SQL Specification
-- bepaal of de tabel toegankelijk is voor een medewerker (direct of via een rol)
-- voor de betreffende actie (Insert, Update, Delete)
-- registratie staat in kgc_tabel_toegang
-- aannames:
-- de rol beheer mag alles
-- niet-geregistreerde tabellen zijn toegankelijk voor iedereen en elke actie

FUNCTION  toegang
 (  p_tabel  IN  VARCHAR2
 ,  p_actie IN VARCHAR2
 ,  p_kafd_id IN NUMBER := NULL
 )
RETURN  BOOLEAN;

PROCEDURE  toegang
 (  p_tabel  IN  VARCHAR2
 ,  p_actie IN VARCHAR2
 ,  p_kafd_id IN NUMBER := NULL
 );


END KGC_TATO_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_TATO_00" IS
FUNCTION toegang
( p_tabel IN VARCHAR2
, p_actie IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
)
RETURN BOOLEAN
IS
  v_mede_id NUMBER;
  CURSOR tato_cur
  IS
    SELECT MIN(tato.ins) ins
    ,      MIN(tato.upd) upd
    ,      MIN(tato.del) del
    FROM   kgc_tabel_toegang tato
    WHERE  tato.tabel = UPPER( p_tabel )
    AND  ( tato.mede_id = v_mede_id
        OR EXISTS
           ( SELECT NULL
             FROM   kgc_mede_kafd meka
             WHERE  meka.mede_id = v_mede_id
             AND    meka.kafd_id = NVL( p_kafd_id, meka.kafd_id )
             AND  ( meka.rol = tato.rol
                 OR meka.rol = 'BEHEER'
                  )
           )
         )
    ;
  tato_rec tato_cur%rowtype;
  v_dummy VARCHAR2(1) := NULL;
-- PL/SQL Block
BEGIN
  -- applicatie-eigenaar mag alles
  -- vanwege aanroep in db-triggers .._A_TRG (autorisatie) op systeemparameteryabellen
  -- mag hier niet een selectie op die tabellen worden gedaan (ORA-04091)
  -- daarom hardgecodeerde oracle-user(s), die als eigenaar optreden
  IF ( USER IN ( 'KGCN', 'HELIX' ) )
  THEN
    RETURN( TRUE );
  END IF;
  -- geen registratie van tabel/actie betekent vrije toegang
  SELECT MIN( '1' )
  INTO   v_dummy
  FROM   kgc_tabel_toegang
  WHERE  tabel = UPPER( p_tabel )
  ;
  IF ( v_dummy IS NULL )
  THEN
    RETURN( TRUE );
  END IF;
  --
  IF ( p_tabel <> 'KGC_MEDEWERKERS' )
  THEN
    v_mede_id := kgc_mede_00.medewerker_id;
  END IF;
  --
  OPEN  tato_cur;
  FETCH tato_cur
  INTO  tato_rec;
  IF ( tato_cur%notfound )
  THEN
    CLOSE tato_cur;
    RETURN( FALSE );
  ELSE
    CLOSE tato_cur;
    IF ( p_actie = 'I' AND tato_rec.ins = 'N' )
    THEN
      RETURN( FALSE );
    ELSIF ( p_actie = 'U' AND tato_rec.upd = 'N' )
    THEN
      RETURN( FALSE );
    ELSIF ( p_actie = 'D' AND tato_rec.del = 'N' )
    THEN
      RETURN( FALSE );
    END IF;
    RETURN( TRUE );
  END IF;
END toegang;

-- overloaded als procedure
PROCEDURE  toegang
( p_tabel  IN  VARCHAR2
, p_actie IN VARCHAR2
, p_kafd_id IN NUMBER := NULL
)
IS
  v_ok BOOLEAN := TRUE;
  v_actie VARCHAR2(30);
  v_afdeling VARCHAR2(30);
  CURSOR kafd_cur
  IS
    SELECT 'Afdeling = '||code
    FROM   kgc_kgc_afdelingen
    WHERE  kafd_id = p_kafd_id
    ;
BEGIN
  v_ok := toegang
          ( p_tabel => p_tabel
          , p_actie => p_actie
          , p_kafd_id => p_kafd_id
          );
  IF ( NOT v_ok )
  THEN
    IF ( p_actie = 'I' )
    THEN
      v_actie := 'invoeren';
    ELSIF ( p_actie = 'U' )
    THEN
      v_actie := 'wijzigen';
    ELSIF ( p_actie = 'D' )
    THEN
      v_actie := 'verwijderen';
    END IF;
    IF ( p_kafd_id IS NOT NULL )
    THEN
      OPEN  kafd_cur;
      FETCH kafd_cur
      INTO  v_afdeling;
      CLOSE kafd_cur;
    END IF;
    qms$errors.show_message
    ( p_mesg => 'KGC-00010'
    , p_param0 => v_actie
    , p_param1 => p_tabel||CHR(10)||v_afdeling
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
    -- om ervoor te zorgen dat beveiliging werkt in Forms en Plus:
    raise_application_error( -20000, 'KGC-00010 # '||v_actie||' # '||p_tabel||CHR(10)||v_afdeling );
  END IF;
END toegang;

END  kgc_tato_00;
/

/
QUIT
