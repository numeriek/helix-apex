CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPIN_00" IS
/* LST 13-07-2004                                            */
/* Package voor procedures op de tabel KGC_OPSLAG_INHOUD     */
FUNCTION chk_opslag_type
(p_opat_id IN NUMBER
,p_oppo_id IN NUMBER
)
RETURN BOOLEAN;

FUNCTION chk_pos_beschikbaar
(p_oppo_id IN NUMBER
) RETURN BOOLEAN
;

PROCEDURE chk_inhoud
(p_opat_id IN NUMBER
,p_oppo_id IN NUMBER
,p_mede_id IN NUMBER
);

FUNCTION exists_opin
(p_oppo_id IN NUMBER
) RETURN BOOLEAN
;

FUNCTION exists_opin_voor_opel
(p_opel_id IN NUMBER
) RETURN BOOLEAN
;

PROCEDURE chk_delete
(p_oppo_id    IN NUMBER
,p_oracle_uid IN VARCHAR2
);

FUNCTION exists_opin_voor_opat
(p_helix_id IN NUMBER
) RETURN BOOLEAN
;

FUNCTION get_oppo_code
(p_oppo_id IN NUMBER
) RETURN VARCHAR2
;

FUNCTION get_opel_code
(p_oppo_id IN NUMBER
) RETURN VARCHAR2
;
END KGC_OPIN_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPIN_00" IS
/* LST 13-07-2004                             */
/* attribuut moet van hetzelfde type zijn als */
/* het type van de positie waar het attribuut */
/* wordt geplaatst in kgc_opslag_inhoud.      */
FUNCTION chk_opslag_type
(p_opat_id IN NUMBER
,p_oppo_id IN NUMBER
)
RETURN BOOLEAN
IS
  CURSOR c_opty
  (cp_opat_id NUMBER
  ,cp_oppo_id NUMBER
  )
  IS
  SELECT '1'
  FROM kgc_opslag_typen opty
  ,    kgc_opslag_attributen opat
  ,    kgc_opslag_entiteiten OPEN
  ,    kgc_opslag_structuren opst
  ,    kgc_opslag_posities oppo
  ,    kgc_opslag_elem_typen opet
  ,    kgc_opslag_elementen opel
  WHERE opat.opat_id = cp_opat_id
  AND   opat.open_id = open.open_id
  AND   open.open_id = opst.open_id
  AND   opst.opty_id = opty.opty_id
  AND   opty.opty_id = opet.opty_id
  AND   opet.opel_id = opel.opel_id
  AND   opel.opel_id = oppo.opel_id
  AND   oppo.oppo_id = cp_oppo_id
  ;
  v_oke BOOLEAN;
  v_dummy VARCHAR2(1);
-- PL/SQL Block
BEGIN
  OPEN c_opty(p_opat_id, p_oppo_id);
  FETCH c_opty INTO v_dummy;
  IF c_opty%found THEN
    v_oke := TRUE;
  ELSE
    v_oke := FALSE;
  END IF;
  CLOSE c_opty;
  RETURN v_oke;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opty%isopen THEN
      CLOSE c_opty;
    END IF;
    RAISE;
END;


/* kijk of positie beschikbaar is voor opslag */
FUNCTION chk_pos_beschikbaar
(p_oppo_id IN NUMBER
) RETURN BOOLEAN
IS
  CURSOR c_oppo
  (cp_oppo_id NUMBER
  ) IS
  SELECT '1'
  FROM   kgc_opslag_posities oppo
  WHERE  oppo.oppo_id = cp_oppo_id
  AND    oppo.beschikbaar = 'N'
  ;
  v_dummy VARCHAR2(1);
  v_oke   BOOLEAN := FALSE;
BEGIN
  OPEN c_oppo(p_oppo_id);
  FETCH c_oppo INTO v_dummy;
  IF c_oppo%found THEN
    v_oke := FALSE;
  ELSE
    v_oke := TRUE;
  END IF;
  CLOSE c_oppo;
  RETURN v_oke;
EXCEPTION
  WHEN OTHERS THEN
    IF c_oppo%isopen THEN
    CLOSE c_oppo;
  END IF;
  RAISE;
END;

/* controleer of medewerker object mag plaatsen */
FUNCTION chk_aut_actie
(p_oppo_id IN NUMBER
,p_oracle_uid IN VARCHAR2
,p_actie   IN VARCHAR2
)
RETURN BOOLEAN
IS
  CURSOR c_opel
  (cp_oppo_id IN NUMBER
  ) IS
  SELECT oppo.opel_id
  FROM   kgc_opslag_posities oppo
  WHERE  oppo.oppo_id = cp_oppo_id
  ;
  v_opel_id kgc_opslag_elementen.opel_id%type;
  v_return NUMBER(1);
  v_oke   BOOLEAN;
BEGIN
  OPEN c_opel(p_oppo_id);
  FETCH c_opel INTO v_opel_id;
  CLOSE c_opel;
  v_return := kgc_opau_00.mag_actie_uitvoeren(p_opel_id    => v_opel_id
                                             ,p_oracle_uid => p_oracle_uid
                           ,p_actie      => p_actie
                         );
  IF v_return = 0
  THEN
    v_oke := FALSE;
  ELSE
    v_oke := TRUE;
  END IF;
  RETURN v_oke;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opel%isopen THEN
    CLOSE c_opel;
  END IF;
  RAISE;
END;


/* controles op kgc_opslag_inhoud */
PROCEDURE chk_inhoud
(p_opat_id IN NUMBER
,p_oppo_id IN NUMBER
,p_mede_id IN NUMBER
) IS
  v_oke BOOLEAN := FALSE;
  CURSOR c_mede
  (cp_mede_id NUMBER
  ) IS
  SELECT mede.oracle_uid
  FROM   kgc_medewerkers mede
  WHERE  mede.mede_id = cp_mede_id
  ;
  v_oracle_uid kgc_medewerkers.oracle_uid%type;
BEGIN
  v_oke := chk_opslag_type( p_opat_id => p_opat_id
                          , p_oppo_id => p_oppo_id);
  IF NOT v_oke THEN
    qms$errors.show_message(p_mesg =>'KGC-11137'
                         ,p_errtp => 'E'
               ,p_rftf => TRUE
               );
  END IF;
  v_oke := chk_pos_beschikbaar(p_oppo_id);
  IF NOT v_oke THEN
    qms$errors.show_message(p_mesg =>'KGC-11139'
                         ,p_errtp => 'E'
               ,p_rftf => TRUE
               );
  END IF;
  OPEN c_mede(p_mede_id);
  FETCH c_mede INTO v_oracle_uid;
  CLOSE c_mede;
  v_oke := chk_aut_actie(p_oppo_id, v_oracle_uid, 'O' ); -- bij insert
  IF NOT v_oke THEN
    qms$errors.show_message(p_mesg =>'KGC-11140'
                         ,p_errtp => 'E'
               ,p_rftf => TRUE
               );
  END IF;
EXCEPTION
  WHEN OTHERS THEN
    IF c_mede%isopen THEN
      CLOSE c_mede;
    END IF;
    RAISE;
END;

FUNCTION exists_opin
(p_oppo_id IN NUMBER
) RETURN BOOLEAN
IS
  CURSOR c_opin
  (cp_oppo_id NUMBER
  )
  IS
  SELECT '1'
  FROM   kgc_opslag_inhoud opin
  WHERE  opin.oppo_id = cp_oppo_id
  ;
  v_dummy  VARCHAR2(1);
  v_exists BOOLEAN := TRUE;
BEGIN
  OPEN c_opin(p_oppo_id);
  FETCH c_opin INTO v_dummy;
  IF c_opin%found
  THEN
    v_exists := TRUE;
  ELSE
    v_exists := FALSE;
  END IF;
  CLOSE c_opin;
  RETURN v_exists;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opin%found THEN
      CLOSE c_opin;
    END IF;
    RAISE;
END exists_opin;

FUNCTION exists_opin_voor_opel
(p_opel_id IN NUMBER
) RETURN BOOLEAN
IS
  CURSOR c_opin
  (cp_opel_id NUMBER
  )
  IS
  SELECT '1'
  FROM   kgc_opslag_inhoud opin
  ,      kgc_opslag_posities oppo
  WHERE  opin.oppo_id = oppo.oppo_id
  AND    oppo.opel_id = cp_opel_id
  ;
  v_dummy  VARCHAR2(1);
  v_exists BOOLEAN := TRUE;
BEGIN
  OPEN c_opin(p_opel_id);
  FETCH c_opin INTO v_dummy;
  IF c_opin%found
  THEN
    v_exists := TRUE;
  ELSE
    v_exists := FALSE;
  END IF;
  CLOSE c_opin;
  RETURN v_exists;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opin%found THEN
      CLOSE c_opin;
    END IF;
    RAISE;
END exists_opin_voor_opel;

PROCEDURE chk_delete
(p_oppo_id IN NUMBER
,p_oracle_uid IN VARCHAR2
) IS
  v_oke BOOLEAN := FALSE;
BEGIN
  v_oke := chk_aut_actie(p_oppo_id, p_oracle_uid, 'U' ); -- delete
  IF NOT v_oke THEN
    qms$errors.show_message(p_mesg =>'KGC-11140'
                         ,p_errtp => 'E'
               ,p_rftf => TRUE
               );
  END IF;
END chk_delete;

FUNCTION exists_opin_voor_opat
(p_helix_id IN NUMBER
) RETURN BOOLEAN
IS
  CURSOR c_opat
  (cp_helix_id NUMBER
  )
  IS
  SELECT '1'
  FROM   kgc_opslag_inhoud opin
  ,      kgc_opslag_attributen opat
  WHERE  opin.opat_id = opat.opat_id
  AND    opat.helix_id = cp_helix_id
  ;
  v_dummy  VARCHAR2(1);
  v_exists BOOLEAN := TRUE;
BEGIN
  OPEN c_opat(p_helix_id);
  FETCH c_opat INTO v_dummy;
  IF c_opat%found
  THEN
    v_exists := TRUE;
  ELSE
    v_exists := FALSE;
  END IF;
  CLOSE c_opat;
  RETURN v_exists;
EXCEPTION
  WHEN OTHERS THEN
    IF c_opat%found THEN
      CLOSE c_opat;
    END IF;
    RAISE;
END exists_opin_voor_opat;

FUNCTION get_oppo_code
(p_oppo_id IN NUMBER
)
RETURN VARCHAR2
IS
  CURSOR c_oppo
  (cp_oppo_id NUMBER
  )
  IS
  SELECT oppo.code
  FROM   kgc_opslag_posities oppo
  WHERE  oppo.oppo_id = cp_oppo_id
  ;
  v_code kgc_opslag_posities.code%TYPE;
BEGIN
  OPEN c_oppo(p_oppo_id);
  FETCH c_oppo INTO v_code;
  CLOSE c_oppo;
  RETURN (v_code);
EXCEPTION
  WHEN OTHERS THEN
    IF c_oppo%isopen THEN
      CLOSE c_oppo;
    END IF;
    RAISE;
END get_oppo_code;

FUNCTION get_opel_code
(p_oppo_id IN NUMBER
)
RETURN VARCHAR2
IS
  CURSOR c_opel
  (cp_oppo_id NUMBER
  )
  IS
  SELECT opel.code
  FROM   kgc_opslag_elementen opel
  ,      kgc_opslag_posities  oppo
  WHERE  oppo.opel_id = opel.opel_id
  AND    oppo.oppo_id = cp_oppo_id
  ;
  v_code kgc_opslag_elementen.code%TYPE;
BEGIN
  OPEN c_opel(p_oppo_id);
  FETCH c_opel INTO v_code;
  CLOSE c_opel;
  RETURN (v_code);
EXCEPTION
  WHEN OTHERS THEN
    IF c_opel%isopen THEN
      CLOSE c_opel;
    END IF;
    RAISE;
END get_opel_code;

END;
/

/
QUIT
