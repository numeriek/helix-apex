CREATE OR REPLACE PACKAGE "HELIX"."KGC_GENIOS" IS
-- genios bestand wordt ingelezen in een tmp-tabel
-- van daaruit worden de wwaarden verwerkt in de helix-tabel bas_meetwaarden, bas_meetwaarde_details
procedure verwerk_bestand
( p_bestand in varchar2
, p_idef_id in number
);

-- Valt de gemeten concentratie door Genios binnen de grenzen?
-- ofwel: is de meetwaarde van de stoftest op de fractie ok?
function concentratie_ok
( p_stoftest in varchar2
, p_fractienummer in varchar2
)
return varchar2;
END KGC_GENIOS;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_GENIOS" IS
-- verwerk gegevens na het inlezen
procedure verwerk_bestand
( p_bestand in varchar2
, p_idef_id in number
)
is
  cursor idef_cur
  is
    select code
    ,      tabel_naam
    from   kgc_interface_definities
    where  idef_id = p_idef_id
    ;
  idef_rec idef_cur%rowtype;

  cursor ifss_cur
  is
    select code
    ,      functie
    ,      default_waarde
    from   kgc_interface_samenstelling
    where  idef_id = p_idef_id
    order by volgorde
    ,        positie_va
    ;

  cursor best_cur
  is
    select entiteit_pk
    from   kgc_bestanden
    where  entiteit_code = 'TRGE'
    and    upper(bestandsnaam) = upper(p_bestand)
  ;
  v_trge_id number;

  v_melding varchar2(4000);
  v_tmp_tabel varchar2(30);

  v_statement varchar2(2000);
  v_cur INTEGER;
  v_rows INTEGER;

  v_positie varchar2(100);
  v_waarde1 varchar2(10); -- Ratio
  v_waarde2 varchar2(10); -- Od
  v_waarde3 varchar2(10); -- Concentratie

  v_x_pos varchar2(3);
  v_y_pos varchar2(3);
  v_aantal number := 0;

  cursor trvu_cur
  ( b_trge_id in number
  , b_x_pos in varchar2
  , b_y_pos in varchar2
  )
  is
    select trvu.meet_id
    from   kgc_tray_vulling trvu
    where  trvu.trge_id = b_trge_id
    and    trvu.x_positie = b_x_pos
    and    trvu.y_positie = b_y_pos
    and    trvu.z_positie = '0'
    ;
  v_meet_id number;

  procedure vul_detailwaarde
  ( b_meet_id in number
  , b_volgorde in number
  , b_alt_prompt in varchar2
  , b_waarde in varchar2
  )
  is
    cursor mwss_cur
    is
      select mwss.prompt
      from   kgc_mwst_samenstelling mwss
      ,      bas_meetwaarden meet
      where  meet.mwst_id = mwss.mwst_id
      and    mwss.volgorde = b_volgorde
      and    meet.meet_id = b_meet_id
      ;
    l_prompt varchar2(30);
  begin
    update bas_meetwaarde_details
    set    waarde = b_waarde
    where  meet_id = b_meet_id
    and    volgorde = b_volgorde
    ;
    if ( sql%rowcount = 0 )
    then
      open  mwss_cur;
      fetch mwss_cur
      into  l_prompt;
      close mwss_cur;
      if ( l_prompt is null )
      then
        l_prompt := b_alt_prompt;
      end if;
      insert into bas_meetwaarde_details
      ( meet_id
      , volgorde
      , prompt
      , waarde
      )
      values
      ( b_meet_id
      , b_volgorde
      , l_prompt
      , b_waarde
      );
    end if;
  end vul_detailwaarde;

begin
  open  idef_cur;
  fetch idef_cur
  into  idef_rec;
  close idef_cur;
  if ( idef_rec.code is null )
  then
    v_melding := 'Interface-definitie met id = '||p_idef_id||' is onbekend';
    raise no_data_found;
  end if;
  v_tmp_tabel := NVL( idef_rec.tabel_naam, 'TMP_'||SUBSTR(idef_rec.code,1,26) );

  open  best_cur;
  fetch best_cur
  into  v_trge_id;
  close best_cur;
  if ( v_trge_id is null )
  then
    v_melding := 'Bestand '||p_bestand||' is niet verbonden aan een traygebruik';
    raise no_data_found;
  end if;

  for r in ifss_cur
  loop
    if ( ifss_cur%rowcount = 1 )
    then
      v_statement := 'select '||r.code;
    else
      v_statement := v_statement||chr(10)||', '||r.code;
    end if;
  end loop;
  v_statement := v_statement||chr(10)||'from '||v_tmp_tabel;

  v_cur := dbms_sql.open_cursor;
  dbms_sql.parse(v_cur, v_statement, dbms_sql.native);
  dbms_sql.define_column(v_cur, 1, v_positie, 10);
  dbms_sql.define_column(v_cur, 2, v_waarde1, 10);
  dbms_sql.define_column(v_cur, 3, v_waarde2, 10);
  dbms_sql.define_column(v_cur, 4, v_waarde3,10);
  v_rows := dbms_sql.execute(v_cur);
  LOOP
    IF dbms_sql.fetch_rows(v_cur) = 0
    THEN
      EXIT;
    END IF;
    dbms_sql.column_value(v_cur, 1, v_positie);
    dbms_sql.column_value(v_cur, 2, v_waarde1); -- Ratio
    dbms_sql.column_value(v_cur, 3, v_waarde2); -- Od
    dbms_sql.column_value(v_cur, 4, v_waarde3); -- Concentratie

    v_y_pos := substr( v_positie, 1, 1 ); -- aanname
    v_x_pos := substr( v_positie, 2 );    -- aanname
    v_meet_id := null;
    open  trvu_cur( b_trge_id => v_trge_id
                  , b_x_pos => v_x_pos
                  , b_y_pos => v_y_pos
                  );
    fetch trvu_cur
    into  v_meet_id;
    close trvu_cur;
    if ( v_meet_id is not null )
    then
      v_aantal := v_aantal + 1;
      vul_detailwaarde
      ( b_meet_id => v_meet_id
      , b_volgorde => 1
      , b_alt_prompt => 'Ratio'
      , b_waarde => v_waarde1
      );
      vul_detailwaarde
      ( b_meet_id => v_meet_id
      , b_volgorde => 2
      , b_alt_prompt => 'Od'
      , b_waarde => v_waarde2
      );
      vul_detailwaarde
      ( b_meet_id => v_meet_id
      , b_volgorde => 3
      , b_alt_prompt => 'Concentratie'
      , b_waarde => v_waarde3
      );
      -- direct afronden, om bas_mdok_00.meet_ok goed te laten werken voor function concentratie_ok
      update bas_meetwaarden
      set    afgerond = 'J'
      where  meet_id = v_meet_id;
    end if;
  END LOOP;
  dbms_sql.close_cursor(v_cur);
  commit;
  qms$errors.show_message
  ( p_mesg => 'KGC-00000'
  , p_param0 => 'Er is bij '||v_aantal||' aangemelde stoftest(en) de ingelezen gegegens verwerkt.'
  , p_errtp => 'I'
  , p_rftf => false
  );
exception
  when others
  then
--    raise_application_error( -20023, nvl( v_melding, sqlerrm ) );
    qms$errors.show_message
    ( p_mesg => 'KGC-00000'
    , p_param0 => 'Fout tijdens het verwerken van de ingelezen gegegens'
    , p_param1 => v_melding
    , p_errtp => 'E'
    , p_rftf => true
    );
end verwerk_bestand;

function concentratie_ok
( p_stoftest in varchar2
, p_fractienummer in varchar2
)
return varchar2
is
  v_return varchar2(1);
  -- aanvullende gegevens uit parameter p_fractienummer
  cursor frac_cur
  is
    select mons.kafd_id
    ,      frac.frac_id
    from   kgc_monsters mons
    ,      bas_fracties frac
    where  frac.mons_id = mons.mons_id
    and    frac.fractienummer = p_fractienummer
    ;
  frac_rec frac_cur%rowtype;
  -- aanvullende gegevens uit parameter p_stoftest
  cursor stof_cur
  ( b_kafd_id in number
  )
  is
    select stof.stof_id
    from   kgc_stoftesten stof
    where  stof.kafd_id = b_kafd_id
    and    stof.code = p_stoftest
    ;
  stof_rec stof_cur%rowtype;

  -- Indien de betreffende stoftest (GENIOS) vaker bij de fractie is aangemeld
  -- kunnen er onverwachte situaties ontstaan.
  -- In dat geval is de laatst aangemelde stoftest beslissend
  cursor mdet_cur
  ( b_frac_id in number
  , b_stof_id in number
  )
  is
    select meet.meet_id
    from   kgc_mwst_samenstelling mwss
    ,      bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
    ,      bas_metingen meti
    where  meet.meti_id = meti.meti_id
    and    meet.meet_id = mdet.meet_id
    and    meet.mwst_id = mwss.mwst_id
    and    mdet.volgorde = mwss.volgorde
    and    mwss.succes_voorwaarde is not null
    and    mdet.waarde is not null
    and    meet.stof_id = b_stof_id
    and    meti.frac_id = b_frac_id
    order by meet.meet_id desc -- laatste eerst
    ;
  v_meet_id number;
begin
  open  frac_cur;
  fetch frac_cur
  into  frac_rec;
  close frac_cur;

  open  stof_cur( b_kafd_id => frac_rec.kafd_id );
  fetch stof_cur
  into  stof_rec;
  close stof_cur;

  open  mdet_cur( b_frac_id => frac_rec.frac_id
                , b_stof_id => stof_rec.stof_id
                );
  fetch mdet_cur
  into  v_meet_id;
  if ( mdet_cur%notfound )
  then -- geen concentratie-waarde geregistreerd
    v_return := 'N';
  else
    v_return := bas_mdok_00.meet_ok( p_meet_id => v_meet_id );
  end if;
  close mdet_cur;

  return( v_return );
end concentratie_ok;
END KGC_GENIOS;
/

/
QUIT
