CREATE OR REPLACE PACKAGE "HELIX"."KGC_OPEN_00" IS
-- PL/SQL Specification
/* LST 28-07-2004                                              */
/* Package voor procedures op de tabel KGC_OPSLAG_ENTITEITEN   */
FUNCTION exists_open_code
(p_open_code IN VARCHAR2
) RETURN BOOLEAN
;


END KGC_OPEN_00;

/
CREATE OR REPLACE PACKAGE BODY "HELIX"."KGC_OPEN_00" IS
/* LST 28-07-2004                                            */
/* Package voor procedures op de tabel KGC_OPSLAG_ENTITEITEN  */

FUNCTION exists_open_code
(p_open_code IN VARCHAR2
) RETURN BOOLEAN
IS
  CURSOR c_open
  (cp_open_code VARCHAR2
  ) IS
  SELECT '1'
  FROM   kgc_opslag_entiteiten OPEN
  WHERE  open.code = cp_open_code
  ;
  v_dummy VARCHAR2(1);
  v_oke   BOOLEAN := FALSE;
-- PL/SQL Block
BEGIN
  OPEN c_open(p_open_code);
  FETCH c_open INTO v_dummy;
  IF c_open%found THEN
    v_oke := TRUE;
  ELSE
    v_oke := FALSE;
  END IF;
  CLOSE c_open;
  RETURN(v_oke);
EXCEPTION
  WHEN OTHERS THEN
    IF c_open%isopen THEN
      CLOSE c_open;
    END IF;
    RAISE;
END exists_open_code;

END KGC_OPEN_00;
/

/
QUIT
