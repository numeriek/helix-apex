
BEGIN
dbms_scheduler.create_job('"BEH_DIGIDOS_CONSOLIDATIE"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
HELIX.BEH_DIGIDOS_ORIGINELEN.VERWERK_ORIGINELEN(p_consolidatie_run => ''J'');
END;
'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('15-FEB-2016 11.06.30.000000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY; BYHOUR=01;BYMINUTE=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Controleer 1x per dag of er nog bestanden verwerkt moeten worden van de afgelopen maand.'
);
dbms_scheduler.set_attribute('"BEH_DIGIDOS_CONSOLIDATIE"','schedule_limit','+000 01:00:00');
dbms_scheduler.set_attribute('"BEH_DIGIDOS_CONSOLIDATIE"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_DIGIDOS_CONSOLIDATIE"');
COMMIT;
END;
/

/
QUIT
