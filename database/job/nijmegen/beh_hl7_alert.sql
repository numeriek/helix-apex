
BEGIN
dbms_scheduler.create_job('"BEH_HL7_ALERT"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
  beh_hl7_listener_alert;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('05-NOV-2013 10.14.58.566000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY;BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Na een uur inactiviteit van de hl7 listener, wordt een alter email verzonden'
);
dbms_scheduler.enable('"BEH_HL7_ALERT"');
COMMIT;
END;
/

/
QUIT
