
BEGIN
dbms_scheduler.create_job('"DNA7_VOORTEST_WLST_AANMAKEN"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
-- be sure to substitute in actual values
-- where the parameter names are!
HELIX.BEH_DNA_VOORTEST_WLST_AANMAKEN.START_WERKLIJST_TRAY_ETIKET
  (''O'' /* VARCHAR2 */ , --P_F_STATUS /* VARCHAR2 */ ,
   ''G'' /* VARCHAR2 */ , --P_O_STATUS /* VARCHAR2 */ ,
   ''%Robot protocol 360'' /* VARCHAR2 */ , --P_PROTOCOL /* VARCHAR2 */ ,
   ''S'' /* VARCHAR2 */ , --P_O_WIJZE /* VARCHAR2 */ ,
   ''PCR'' /* VARCHAR2 */ , --P_TECHNIEK /* VARCHAR2 */ ,
   ''GENOOM'' /* VARCHAR2 */ , --P_AFDELING /* VARCHAR2 */ ,
   ''96-WELL'' /* VARCHAR2 */ , --P_TRTY /* VARCHAR2 */ ,
   ''DNAPCRFacility@antrg.umcn.nl; XHLXAUT@umcn.nl'',
   NULL /* VARCHAR2 */ --P_TRAY_GRD /* VARCHAR2 */
   );
END;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('10-MAR-2015 11.30.36.915000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYDAY=MON,TUE,WED,THU,FRI;BYHOUR=6'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'BEH_VOORTEST_WLST_AANMAKEN dagelijks om  06.00'
);
dbms_scheduler.set_attribute('"DNA7_VOORTEST_WLST_AANMAKEN"','logging_level',DBMS_SCHEDULER.LOGGING_FULL);
dbms_scheduler.set_attribute('"DNA7_VOORTEST_WLST_AANMAKEN"','raise_events',172);
dbms_scheduler.enable('"DNA7_VOORTEST_WLST_AANMAKEN"');
COMMIT;
END;
/

/
QUIT
