
BEGIN
dbms_scheduler.create_job('"BEH_ZET_RELA_ID_OORSP"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
beh_prepare_rela_id_oorsp;
beh_interfaces.zet_oorspr_aanvragers;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('21-FEB-2014 11.48.43.538000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=WEEKLY;BYDAY=MON;BYHOUR=6;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'kgc_onderzoeken.rela_id_oosprong invullen'
);
dbms_scheduler.enable('"BEH_ZET_RELA_ID_OORSP"');
COMMIT;
END;
/

/
QUIT
