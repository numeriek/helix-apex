
BEGIN
dbms_scheduler.create_job('"FLOW_BROKEN_JOB_FIXER"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_BROKEN_JOB_FIXER'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-DEC-2012 12.00.00.709000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=30;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"FLOW_BROKEN_JOB_FIXER"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"FLOW_BROKEN_JOB_FIXER"','restartable',TRUE);
dbms_scheduler.enable('"FLOW_BROKEN_JOB_FIXER"');
COMMIT;
END;
/

/
QUIT
