
BEGIN
dbms_scheduler.create_job('"DNA_INS_INDI_VAN_PAKKET"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_INS_INDI_VAN_PAKKET'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('06-DEC-2012 10.20.07.223000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=15;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"DNA_INS_INDI_VAN_PAKKET"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"DNA_INS_INDI_VAN_PAKKET"');
COMMIT;
END;
/

/
QUIT
