
BEGIN
dbms_scheduler.create_job('"HERSTEL_LANDEN"',
job_type=>'STORED_PROCEDURE', job_action=>
'"HELIX"."BEH_HERSTEL_LANDEN"'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-OCT-2012 12.00.00.000000000 AM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYHOUR=0;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"HERSTEL_LANDEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"HERSTEL_LANDEN"','restartable',TRUE);
COMMIT;
END;
/

/
QUIT
