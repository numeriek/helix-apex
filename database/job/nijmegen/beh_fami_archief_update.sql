
BEGIN
dbms_scheduler.create_job('"BEH_FAMI_ARCHIEF_UPDATE"',
job_type=>'PLSQL_BLOCK', job_action=>
'update kgc_families
                                  set archief =  ARCHIEF || '' /planlijst''
                                  where FAMILIENUMMER in (select familienr from BEH_FLOW_DNA_PROCES_OV05_VW)
                                        and nvl(UPPER(ARCHIEF), ''ONB'') not like ''%PLANLIJST%'';
                                  commit;
                                  update kgc_families
                                  set archief =  ARCHIEF || '' / DD''
                                  where FAMI_ID in (select BEST.ENTITEIT_PK from kgc_bestanden best, KGC_BESTAND_TYPES btyp where BEST.ENTITEIT_CODE = ''FAMI'' and BEST.BTYP_ID= BTYP.BTYP_ID and UPPER(BTYP.CODE) = ''ARCHIEF'' and BEST.LAST_UPDATE_DATE > sysdate -10 )
                                    and nvl(UPPER(ARCHIEF),''ONB'') not like ''%DD%'';
                                  Commit;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('06-NOV-2015 02.46.20.416000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;BYMINUTE=05,15, 25, 35, 45, 55;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_FAMI_ARCHIEF_UPDATE"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_FAMI_ARCHIEF_UPDATE"');
COMMIT;
END;
/

/
QUIT
