
BEGIN
dbms_scheduler.create_job('"CLIA_ORU_BERICHTEN_INLEZEN"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_CLIA_01.START_VERWERKING_VIA_JOB'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('22-MAR-2012 12.01.56.064000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"CLIA_ORU_BERICHTEN_INLEZEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"CLIA_ORU_BERICHTEN_INLEZEN"','restartable',TRUE);
dbms_scheduler.enable('"CLIA_ORU_BERICHTEN_INLEZEN"');
COMMIT;
END;
/

/
QUIT
