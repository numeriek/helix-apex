
BEGIN
dbms_scheduler.create_job('"BEH_ZET_EINDDT_INT_NGS_ONDE"',
job_type=>'PLSQL_BLOCK', job_action=>
'
declare
  --
  -- Tijdelijke oplossing om de geplande eiddatum voor NGS onderzoeken van interne
  -- medewerkers op datum_binnen + 100 dagen te zetten.
  --
  -- RB 18-10-2016 voor Mantis 13568
  --
  date_format varchar2(10) := ''YYYY-MM-DD'';

  cursor update_cur is
    select kafd.code as afdeling
         , ongr.code as onderzoeksgroep
         , onde.onde_id
         , onde.onderzoeknr
         , onde.datum_binnen
         , onde.geplande_einddatum
         , onde.geplande_einddatum - onde.datum_binnen as aantal_dagen_verschil
         , onde.datum_binnen + 100 as nieuwe_geplande_einddatum
         , rela.code as relatie_code
         , rela.liszcode as relatie_liszcode
    from   kgc_onderzoeken          onde
      join kgc_kgc_afdelingen       kafd on kafd.kafd_id = onde.kafd_id
      join kgc_onderzoeksgroepen    ongr on ongr.ongr_id = onde.ongr_id
      join kgc_relaties             rela on rela.rela_id = onde.rela_id
    where  kafd.code = ''GENOOM''
      and  ongr.code = ''NGS''
      and  onde.afgerond = ''N''
      and  onde.datum_binnen >= to_date(''2016-09-01'', ''YYYY-MM-DD'')     --Alleen onderzoeken van na 1-sep-2016.
      and  nvl(onde.geplande_einddatum, (onde.datum_binnen+100)) >= sysdate --Geen onderzoeken waarvan de geplande einddatum in het verleden ligt of in het verleden zou komen te liggen.
      and  (onde.datum_binnen+100) < onde.geplande_einddatum                --Geen onderzoeken waarbij het aantal dagen tussen datum_binnen en de geplande einddatum < 100 dagen.
      and  rela.liszcode is not null;                                       --Alleen onderzoeken met een interne aanvrager.
                                                                            --beh_onde_buitenland_00.beh_rela_is_intern(onde.rela_id) = ''J''
begin
  for update_rec in update_cur loop
    --
    dbms_output.put_line( ''Update onderzoek '' || update_rec.onderzoeknr
                       || '' met datum_binnen '' || to_char(update_rec.datum_binnen, date_format)
                       || '' van geplande_einddatum '' || nvl(to_char(update_rec.geplande_einddatum, date_format), ''<NULL>'')
                       || '' naar '' || to_char(update_rec.nieuwe_geplande_einddatum, date_format) || ''.'');
    --
    update kgc_onderzoeken
    set    geplande_einddatum = update_rec.nieuwe_geplande_einddatum
    where  onde_id = update_rec.onde_id;
    --
  end loop;
end;

commit;
'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-OCT-2016 02.41.43.000000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY; BYHOUR=4; BYMINUTE=30;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_ZET_EINDDT_INT_NGS_ONDE"','schedule_limit','+000 01:00:00');
dbms_scheduler.set_attribute('"BEH_ZET_EINDDT_INT_NGS_ONDE"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_ZET_EINDDT_INT_NGS_ONDE"');
COMMIT;
END;
/

/
QUIT
