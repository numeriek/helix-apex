
BEGIN
dbms_scheduler.create_job('"BEH_DUMMY_BESTAND_INS_DEL"',
job_type=>'PLSQL_BLOCK', job_action=>
'
--MGOL 11-12-2015 toevoegen van dummy bestand om onderzoeksrecords in Digitaal (Lab) dossier te zien
--voeg dummy bestand aan een onderzoek zonder enkele bestand in Helix (dus ook geen bestand bij de gerelateerde entiteiten
insert into KGC_BESTANDEN (entiteit_code, entiteit_pk, btyp_id, bestand_specificatie, volgorde, commentaar)
select  ''ONDE'',
           ONDE.ONDE_ID,
           (SELECT BTYP.BTYP_ID from KGC_BESTAND_TYPES btyp where BTYP.CODE = ''DUMMY''),
           CASE WHEN (select global_name from GLOBAL_NAME) LIKE ''PROD%'' THEN ''\\umcms005\gen_stg$\helix\prod\digidos\dummy\Dummy_document_voor_onderzoeken.pdf''  ELSE ''\\umcms005\gen_stg$\helix\acci\digidos\dummy\Dummy_document_voor_onderzoeken.pdf'' END,
           1,
           ''Tijdelijke dummy document''
from kgc_onderzoeken onde, kgc_kgc_afdelingen kafd
where ONDE.KAFD_ID = KAFD.KAFD_ID
    and ONDE.CREATION_DATE > sysdate -1/24
    and KAFD.CODE in (''GENOOM'')
    and not exists (select 1 from kgc_bestanden best where BEST.ENTITEIT_PK = ONDE.ONDE_ID and BEST.ENTITEIT_CODE= ''ONDE'')
    --and not exists (select 1 from KGC_KGCBEST02_DOCUMENTEN_VW bedo where ONDE.ONDE_ID = BEDO.ONDE_ID)
;
-- verwijder dummy bestand indien bij onderzoek of gerelateerde entiteiten bestand aangemaakt wordt
delete
from KGC_BESTANDEN best
where
--        exists (select BEDO.ONDE_ID
--                    from KGC_KGCBEST02_DOCUMENTEN_VW bedo
--                    where BEST.ENTITEIT_PK = BEDO.ONDE_ID
--                    group by BEDO.ONDE_ID
--                    having count (BEDO.ONDE_ID) >1
--                    )
        exists (select 1
                    from kgc_bestanden best1
                    where BEST1.ENTITEIT_PK = BEST.ENTITEIT_PK
                    group by BEST1.ENTITEIT_PK
                    having count (BEST1.ENTITEIT_PK) >1
                    )
    and BEST.ENTITEIT_CODE=''ONDE''
    and BEST.BTYP_ID in (SELECT BTYP.BTYP_ID from KGC_BESTAND_TYPES btyp where BTYP.CODE = ''DUMMY'')
;
Commit;
'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('06-NOV-2015 02.46.20.416000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;BYMINUTE=02,05,07,10,12,15,17,20,22,25,27,30,32,37,40,42,45,47,50,52,55,57; BYSECOND=30'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_DUMMY_BESTAND_INS_DEL"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"BEH_DUMMY_BESTAND_INS_DEL"','restartable',TRUE);
dbms_scheduler.enable('"BEH_DUMMY_BESTAND_INS_DEL"');
COMMIT;
END;
/

/
QUIT
