
BEGIN
dbms_scheduler.create_job('"BEH_METAB_ONDE_ZONDER_INDI_JOB"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin BEH_METAB_ONDE_ZONDER_INDI; end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('04-MAY-2015 04.32.12.722000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN;BYHOUR=23;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Onderzoeken zonder indicatie (metab)'
);
dbms_scheduler.enable('"BEH_METAB_ONDE_ZONDER_INDI_JOB"');
COMMIT;
END;
/

/
QUIT
