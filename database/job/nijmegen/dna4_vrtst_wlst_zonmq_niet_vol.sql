
BEGIN
dbms_scheduler.create_job('"DNA4_VRTST_WLST_ZONMQ_NIET_VOL"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
-- be sure to substitute in actual values
-- where the parameter names are!
HELIX.BEH_DNA_VOORTEST_WLST_GEEN_MQ.START_WERKLIJST_TRAY_ETIKET
(
     ''O''
   , ''G''
   , ''Robot protocol 360''
   , ''A''
   , ''PCR''
   , ''GENOOM''
   , ''96-WELL''
   , ''DNAPCRFAcility@gen.umcn.nl; XHLXAUT@umcn.nl''
   , NULL
   , ''N''
   , ''WERK''
   );
END;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('29-APR-2013 11.30.36.915000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYDAY=MON,TUE,WED,THU,FRI;BYHOUR=5;BYMINUTE=45; BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"DNA4_VRTST_WLST_ZONMQ_NIET_VOL"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"DNA4_VRTST_WLST_ZONMQ_NIET_VOL"','raise_events',172);
dbms_scheduler.enable('"DNA4_VRTST_WLST_ZONMQ_NIET_VOL"');
COMMIT;
END;
/

/
QUIT
