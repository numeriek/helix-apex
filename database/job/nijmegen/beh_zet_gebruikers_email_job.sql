
BEGIN
dbms_scheduler.create_job('"BEH_ZET_GEBRUIKERS_EMAIL_JOB"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
beh_zet_gebruikers_email;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('05-MAR-2014 02.41.51.749000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=WEEKLY;BYDAY=SUN;BYHOUR=20;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Gebruikers email in Helix (sypa:BEH_MIJN_HELIX_EMAIL_ADRES) bijwerken'
);
dbms_scheduler.enable('"BEH_ZET_GEBRUIKERS_EMAIL_JOB"');
COMMIT;
END;
/

/
QUIT
