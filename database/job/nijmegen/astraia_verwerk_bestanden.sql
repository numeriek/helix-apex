
BEGIN
dbms_scheduler.create_job('"ASTRAIA_VERWERK_BESTANDEN"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.KGC_IMPORT.VERWERK_BESTANDEN'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('29-MAR-2012 10.20.07.223000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=5;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"ASTRAIA_VERWERK_BESTANDEN"','logging_level',DBMS_SCHEDULER.LOGGING_FULL);
dbms_scheduler.set_attribute('"ASTRAIA_VERWERK_BESTANDEN"','restartable',TRUE);
dbms_scheduler.enable('"ASTRAIA_VERWERK_BESTANDEN"');
COMMIT;
END;
/

/
QUIT
