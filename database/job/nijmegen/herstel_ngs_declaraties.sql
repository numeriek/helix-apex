
BEGIN
dbms_scheduler.create_job('"HERSTEL_NGS_DECLARATIES"',
job_type=>'STORED_PROCEDURE', job_action=>
'"HELIX"."BEH_HERSTEL_NGS_DECL"'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-OCT-2012 04.31.00.000000000 PM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=1;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"HERSTEL_NGS_DECLARATIES"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"HERSTEL_NGS_DECLARATIES"','restartable',TRUE);
dbms_scheduler.enable('"HERSTEL_NGS_DECLARATIES"');
COMMIT;
END;
/

/
QUIT
