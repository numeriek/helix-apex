
BEGIN
dbms_scheduler.create_job('"DNA2_VRTST_OPN_AANMELDEN_2"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
   -- Insert PL/SQL code here
   HELIX.BEH_DNA_VOORTEST_OPN_AANMELDEN.START_ROBOT_STOFTEST_AANMELDEN
        ( ''GENOOM'', ''alwin.rikken@radboudumc.nl'');
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('21-JUN-2016 03.30.00.000000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
COMMIT;
END;
/

/
QUIT
