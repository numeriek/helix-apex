
BEGIN
dbms_scheduler.create_job('"EPD_ALERT_UITSLAG_VERSTUREN"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_EPD_00.VERWERKING_ALERT_UITSLAG'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-OCT-2012 04.45.00.000000000 PM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=30;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"EPD_ALERT_UITSLAG_VERSTUREN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"EPD_ALERT_UITSLAG_VERSTUREN"','restartable',TRUE);
dbms_scheduler.enable('"EPD_ALERT_UITSLAG_VERSTUREN"');
COMMIT;
END;
/

/
QUIT
