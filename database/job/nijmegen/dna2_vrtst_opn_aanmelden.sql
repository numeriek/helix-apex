
BEGIN
dbms_scheduler.create_job('"DNA2_VRTST_OPN_AANMELDEN"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
-- be sure to substitute in actual values
-- where the parameter names are!
HELIX.BEH_DNA_VOORTEST_OPN_AANMELDEN.START_ROBOT_STOFTEST_AANMELDEN
( ''GENOOM''                     -- P_AFDELING IN VARCHAR2
, ''alwin.rikken@radboudumc.nl'' -- P_EMAIL    IN VARCHAR2
);
END;
'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('28-JUN-2016 01.58.33.111000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"DNA2_VRTST_OPN_AANMELDEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"DNA2_VRTST_OPN_AANMELDEN"','raise_events',172);
dbms_scheduler.enable('"DNA2_VRTST_OPN_AANMELDEN"');
COMMIT;
END;
/

/
QUIT
