
BEGIN
dbms_scheduler.create_job('"FLOW_UPD_ONDE_DATA_ON_DEMAND"',
job_type=>'PLSQL_BLOCK', job_action=>
'DECLARE
  P_ONDE_ID NUMBER;
  P_FLOW_ID NUMBER;
  X_MESSAGE VARCHAR2(32767);
BEGIN
-- be sure to substitute in actual values
-- where the parameter names are!
HELIX.BEH_FLOW_00.INSERT_BEH_FLOW_ONDE_DATA
  (NULL /* NUMBER */ ,
   1 /* NUMBER */ ,
   X_MESSAGE /* VARCHAR2 */  );
END;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('14-OCT-2016 08.00.00.000000000 PM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYHOUR=21;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"FLOW_UPD_ONDE_DATA_ON_DEMAND"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
COMMIT;
END;
/

/
QUIT
