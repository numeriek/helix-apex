
BEGIN
dbms_scheduler.create_job('"ZET_VERZ_ID_OP_ONBEKEND"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
update kgc_personen
set verz_id = (select verz_id from kgc_verzekeraars where naam = ''ONBEKEND'')
where verz_id is null
and beheer_in_zis = ''J''
and overleden = ''N''
and zisnr is not null
;
commit;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('24-MAR-2014 10.06.33.749000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=WEEKLY;BYDAY=SUN;BYHOUR=6;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'lege kgc_personen.verz_id wordt op ONBEKEND gezet; zie mantisnr 9545'
);
dbms_scheduler.enable('"ZET_VERZ_ID_OP_ONBEKEND"');
COMMIT;
END;
/

/
QUIT
