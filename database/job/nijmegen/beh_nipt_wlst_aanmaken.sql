
BEGIN
dbms_scheduler.create_job('"BEH_NIPT_WLST_AANMAKEN"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_NIPT_00.WRITE2FILE'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('15-SEP-2014 07.00.00.000000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;BYMINUTE=07,17, 27, 37, 47, 57;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_NIPT_WLST_AANMAKEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_NIPT_WLST_AANMAKEN"');
COMMIT;
END;
/

/
QUIT
