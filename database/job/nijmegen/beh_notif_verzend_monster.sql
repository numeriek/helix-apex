
BEGIN
dbms_scheduler.create_job('"BEH_NOTIF_VERZEND_MONSTER"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_NOTIF_VERZEND_MONSTER_00.VERWERKING_MAIL_MONSTER'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('09-APR-2014 09.50.38.202000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY;BYMINUTE=11;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_NOTIF_VERZEND_MONSTER"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"BEH_NOTIF_VERZEND_MONSTER"','restartable',TRUE);
COMMIT;
END;
/

/
QUIT
