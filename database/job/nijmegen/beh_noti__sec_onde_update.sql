
BEGIN
dbms_scheduler.create_job('"BEH_NOTI_$SEC_ONDE_UPDATE"',
job_type=>'PLSQL_BLOCK', job_action=>
'UPDATE kgc_notities
SET    code = substr(CODE, 2)
where  noti_id in
( select noti.noti_id
  from   kgc_onderzoeken onde
    join kgc_notities    noti on noti.id = onde.onde_id and noti.entiteit = ''ONDE''
  where  noti.code in (''$SEC_ONDE_NIEUW'', ''$SEC_NIEUW_SCANNING'', ''$SEC_NIEUW_SCREENING'')
    and  onde.afgerond = ''J''
);
Commit;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('06-NOV-2015 02.46.20.416000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;BYMINUTE=05,15, 25, 35, 45, 55;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_NOTI_$SEC_ONDE_UPDATE"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"BEH_NOTI_$SEC_ONDE_UPDATE"','restartable',TRUE);
dbms_scheduler.enable('"BEH_NOTI_$SEC_ONDE_UPDATE"');
COMMIT;
END;
/

/
QUIT
