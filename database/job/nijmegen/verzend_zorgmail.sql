
BEGIN
dbms_scheduler.create_job('"VERZEND_ZORGMAIL"',
job_type=>'STORED_PROCEDURE', job_action=>
'"HELIX"."BEH_ZOMA_00"."VERWERK_ZORGMAIL"'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('11-APR-2013 01.38.38.711000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY;INTERVAL=1;BYMINUTE=00;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Verzenden van uitslagbestanden voor relaties met een zorgmail adres.'
);
dbms_scheduler.set_attribute('"VERZEND_ZORGMAIL"','schedule_limit','+000 01:00:00');
dbms_scheduler.enable('"VERZEND_ZORGMAIL"');
COMMIT;
END;
/

/
QUIT
