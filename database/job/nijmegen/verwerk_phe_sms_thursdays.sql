
BEGIN
dbms_scheduler.create_job('"VERWERK_PHE_SMS_THURSDAYS"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_PHE_00.VERWERK_SMS'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('06-SEP-2013 01.31.02.140000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=WEEKLY;BYDAY=THU;BYHOUR=14,15,16,17,18,19,20,21,22;BYMINUTE=5,10,15,20,25,30,35,40,45,50,55;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>TRUE,comments=>
NULL
);
dbms_scheduler.enable('"VERWERK_PHE_SMS_THURSDAYS"');
COMMIT;
END;
/

/
QUIT
