
BEGIN
dbms_scheduler.create_job('"BEH_DIGIDOS_CONTROLEER"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
HELIX.BEH_DIGIDOS_ORIGINELEN.controleer_originelen;
END;
'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('15-FEB-2016 11.06.30.000000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY; BYDAY=MON,TUE,WED,THU,FRI; BYHOUR=21; BYMINUTE=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Controleer 1x per dag of er de afgelopen 24 uur originelen aangemaakt zijn en stuur email naar postbus.'
);
dbms_scheduler.set_attribute('"BEH_DIGIDOS_CONTROLEER"','schedule_limit','+000 01:00:00');
dbms_scheduler.set_attribute('"BEH_DIGIDOS_CONTROLEER"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_DIGIDOS_CONTROLEER"');
COMMIT;
END;
/

/
QUIT
