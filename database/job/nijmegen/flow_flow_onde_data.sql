
BEGIN
dbms_scheduler.create_job('"FLOW_FLOW_ONDE_DATA"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_FLOW_00.INSERT_FLOD_VIA_JOB'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-OCT-2012 06.00.00.000000000 PM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=5;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"FLOW_FLOW_ONDE_DATA"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"FLOW_FLOW_ONDE_DATA"','restartable',TRUE);
dbms_scheduler.enable('"FLOW_FLOW_ONDE_DATA"');
COMMIT;
END;
/

/
QUIT
