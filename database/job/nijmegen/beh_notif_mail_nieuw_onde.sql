
BEGIN
dbms_scheduler.create_job('"BEH_NOTIF_MAIL_NIEUW_ONDE"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_MAIL_NOTIFICATIE_00.VERWERKING_MAIL_NIEUW_ONDE'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('09-APR-2014 09.50.38.202000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY;BYMINUTE=09,29,49;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_NOTIF_MAIL_NIEUW_ONDE"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"BEH_NOTIF_MAIL_NIEUW_ONDE"','restartable',TRUE);
dbms_scheduler.enable('"BEH_NOTIF_MAIL_NIEUW_ONDE"');
COMMIT;
END;
/

/
QUIT
