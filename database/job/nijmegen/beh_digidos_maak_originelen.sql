
BEGIN
dbms_scheduler.create_job('"BEH_DIGIDOS_MAAK_ORIGINELEN"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
HELIX.BEH_DIGIDOS_ORIGINELEN.VERWERK_ORIGINELEN;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('13-APR-2016 05.40.44.000000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY; BYHOUR=05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21; BYMINUTE=0,5,10,15,20,25,30,35,40,45,50,55;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_DIGIDOS_MAAK_ORIGINELEN"','schedule_limit','+000 01:00:00');
dbms_scheduler.set_attribute('"BEH_DIGIDOS_MAAK_ORIGINELEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_DIGIDOS_MAAK_ORIGINELEN"');
COMMIT;
END;
/

/
QUIT
