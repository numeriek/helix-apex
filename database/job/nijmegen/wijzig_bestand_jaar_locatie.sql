
BEGIN
dbms_scheduler.create_job('"WIJZIG_BESTAND_JAAR_LOCATIE"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_WIJZIG_MAP_LOCATIE'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('31-DEC-2013 11.55.00.000000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MONTHLY;BYMONTH=1,4,7,10; BYMONTHDAY=1; BYHOUR=0;BYMINUTE=5; BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"WIJZIG_BESTAND_JAAR_LOCATIE"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"WIJZIG_BESTAND_JAAR_LOCATIE"','restartable',TRUE);
COMMIT;
END;
/

/
QUIT
