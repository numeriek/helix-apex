
BEGIN
dbms_scheduler.create_job('"VERWERK_PHE_SMS_FRIDAYS"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_PHE_00.VERWERK_SMS'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('06-SEP-2013 01.29.17.102000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=WEEKLY;BYDAY=FRI;BYHOUR=08,09,10,11,12,13;BYMINUTE=5,10,15,20,25,30,35,40,45,50,55;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>TRUE,comments=>
NULL
);
dbms_scheduler.enable('"VERWERK_PHE_SMS_FRIDAYS"');
COMMIT;
END;
/

/
QUIT
