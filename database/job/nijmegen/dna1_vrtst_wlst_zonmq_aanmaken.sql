
BEGIN
dbms_scheduler.create_job('"DNA1_VRTST_WLST_ZONMQ_AANMAKEN"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
-- be sure to substitute in actual values
-- where the parameter names are!
HELIX.BEH_DNA_VOORTEST_WLST_GEEN_MQ.START_WERKLIJST_TRAY_ETIKET
  (''O'' /* VARCHAR2 */ , --P_F_STATUS /* VARCHAR2 */ ,
   ''G'' /* VARCHAR2 */ , --P_O_STATUS /* VARCHAR2 */ ,
   ''Robot protocol 360'' /* VARCHAR2 */ , --P_PROTOCOL /* VARCHAR2 */ ,
   ''A'' /* VARCHAR2 */ , --P_O_WIJZE /* VARCHAR2 */ ,
   ''PCR'' /* VARCHAR2 */ , --P_TECHNIEK /* VARCHAR2 */ ,
   ''GENOOM'' /* VARCHAR2 */ , --P_AFDELING /* VARCHAR2 */ ,
   ''96-WELL'' /* VARCHAR2 */ , --P_TRTY /* VARCHAR2 */ ,
   ''DNAPCRFacility@antrg.umcn.nl; XHLXAUT@umcn.nl''
   );
END;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('29-APR-2013 11.30.36.915000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=45;BYDAY=MON,TUE,WED,THU,FRI;BYHOUR=7,8,9,10,11,12,13,14,15,16,17,18'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"DNA1_VRTST_WLST_ZONMQ_AANMAKEN"','logging_level',DBMS_SCHEDULER.LOGGING_FULL);
dbms_scheduler.set_attribute('"DNA1_VRTST_WLST_ZONMQ_AANMAKEN"','raise_events',172);
dbms_scheduler.enable('"DNA1_VRTST_WLST_ZONMQ_AANMAKEN"');
COMMIT;
END;
/

/
QUIT
