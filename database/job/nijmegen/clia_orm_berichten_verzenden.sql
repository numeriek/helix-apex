
BEGIN
dbms_scheduler.create_job('"CLIA_ORM_BERICHTEN_VERZENDEN"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_CLIA_00.START_VERWERKING_VIA_JOB'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('21-MAR-2012 04.50.22.709000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=5;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"CLIA_ORM_BERICHTEN_VERZENDEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"CLIA_ORM_BERICHTEN_VERZENDEN"','restartable',TRUE);
dbms_scheduler.enable('"CLIA_ORM_BERICHTEN_VERZENDEN"');
COMMIT;
END;
/

/
QUIT
