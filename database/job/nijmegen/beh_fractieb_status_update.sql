
BEGIN
dbms_scheduler.create_job('"BEH_FRACTIEB_STATUS_UPDATE"',
job_type=>'PLSQL_BLOCK', job_action=>
'
--zie mantis 0012255
Insert into BEH_FRACTIES_B_UPDATE (FRAC_ID, FRACTIENUMMER, OUD_STATUS, NIEUW_STATUS, UPDATE_DATE)
select FRAC.FRAC_ID, FRAC.FRACTIENUMMER, FRAC.STATUS, ''A'', sysdate
                            FROM bas_fracties frac
                            WHERE FRAC.FRAC_ID_PARENT IS NOT NULL
                                AND FRAC.FRACTIENUMMER LIKE ''DNA%B''
                                AND FRAC.STATUS = ''O''
                                AND FRAC.KAFD_ID=1
                                AND NOT EXISTS (select * from bas_isolatielijsten isol where FRAC.FRAC_ID = ISOL.FRAC_ID)
                                AND NOT EXISTS (select * from bas_isolatielijsten_jn isjn where FRAC.FRAC_ID = ISJN.FRAC_ID)
                                AND FRAC.LAST_UPDATE_DATE > sysdate-1
                                ;
UPDATE bas_fracties frac
SET STATUS = ''A''
WHERE exists (select 1
                            FROM BEH_FRACTIES_B_UPDATE frac1
                            WHERE FRAC.FRAC_ID = FRAC1.FRAC_ID
                                and FRAC1.UPDATE_DATE > sysdate -1/24/60
                            )
;
Commit;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('17-NOV-2015 08.46.20.416000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYHOUR=6;BYMINUTE=35; BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_FRACTIEB_STATUS_UPDATE"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"BEH_FRACTIEB_STATUS_UPDATE"','restartable',TRUE);
dbms_scheduler.enable('"BEH_FRACTIEB_STATUS_UPDATE"');
COMMIT;
END;
/

/
QUIT
