
BEGIN
dbms_scheduler.create_job('"MONS_AFNAME_GEB_DAT_CHECK"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_MONS_AFNAME_GEB_DAT'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-OCT-2012 05.00.00.000000000 PM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY;INTERVAL=1;BYMINUTE=00;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"MONS_AFNAME_GEB_DAT_CHECK"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"MONS_AFNAME_GEB_DAT_CHECK"','restartable',TRUE);
dbms_scheduler.enable('"MONS_AFNAME_GEB_DAT_CHECK"');
COMMIT;
END;
/

/
QUIT
