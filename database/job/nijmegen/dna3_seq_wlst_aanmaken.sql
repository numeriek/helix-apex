
BEGIN
dbms_scheduler.create_job('"DNA3_SEQ_WLST_AANMAKEN"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
-- be sure to substitute in actual values
-- where the parameter names are!
HELIX.BEH_DNA_SEQ_WLST_AANMAKEN.START_ROBOT_WERKLIJST_AANMAKEN
  (''SEQ''  ,
   ''GENOOM''  ,
   ''96-WELL'',
   ''\\umcseqfac01\TEMP_IONTORRENT_LOG$\SEQ_WLST''
   );
END;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('10-JAN-2012 02.17.16.200000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=90'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"DNA3_SEQ_WLST_AANMAKEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"DNA3_SEQ_WLST_AANMAKEN"','raise_events',172);
dbms_scheduler.enable('"DNA3_SEQ_WLST_AANMAKEN"');
COMMIT;
END;
/

/
QUIT
