
BEGIN
dbms_scheduler.create_job('"DNA6_ION_TORRENT_WLST_EVEN_D"',
job_type=>'PLSQL_BLOCK', job_action=>
'BEGIN
-- be sure to substitute in actual values
-- where the parameter names are!
HELIX.BEH_DNA_ION_TORRENT.START_ION_TORRENT
(
     ''O''
   , ''G''
   , ''%Robot protocol ION-%B''
   , ''A''
   , ''PCR''
   , ''GENOOM''
   , ''96-WELL''
   , NULL
   , 1
   , 80
   , 95
   , ''J''
   , 50
   , ''J''
   , 50
   , ''\\umcseqfac01\temp_iontorrent_log$''
   );
END;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('23-OCT-2014 07.30.36.915000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYMONTHDAY=2,4,6,8,10,12,14,16,18,20,22,24,26,28,30;BYDAY=MON,TUE,WED,THU,FRI;BYHOUR=6;BYMINUTE=0; BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"DNA6_ION_TORRENT_WLST_EVEN_D"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"DNA6_ION_TORRENT_WLST_EVEN_D"','raise_events',172);
dbms_scheduler.enable('"DNA6_ION_TORRENT_WLST_EVEN_D"');
COMMIT;
END;
/

/
QUIT
