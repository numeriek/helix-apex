BEGIN
dbms_scheduler.create_job('"BEH_HERANALYSE_AANVRAAG"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
 helix.beh_heranalyse.start_heranalyse;
 end;'
, number_of_arguments => 0
, start_date          => TO_TIMESTAMP_TZ( to_char(sysdate, 'DD-MON-YYYY HH24.MI.SS') || ' EUROPE/AMSTERDAM', 'DD-MON-YYYY HH24.MI.SS TZR','NLS_DATE_LANGUAGE=english')
, repeat_interval     => 'FREQ=MINUTELY; BYHOUR=05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21; BYMINUTE=0,15,30,45;BYSECOND=0'
, end_date            => NULL
, job_class           => '"DEFAULT_JOB_CLASS"'
, enabled             => FALSE
, auto_drop           => FALSE
,comments             => NULL
);
dbms_scheduler.set_attribute('"BEH_HERANALYSE_AANVRAAG"','schedule_limit','+000 01:00:00');
dbms_scheduler.set_attribute('"BEH_HERANALYSE_AANVRAAG"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_HERANALYSE_AANVRAAG"');
COMMIT;
END;
/
