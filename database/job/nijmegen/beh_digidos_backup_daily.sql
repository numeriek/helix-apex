BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB(job_name  => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
  ( job_name        => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
  , start_date      => sysdate
  , repeat_interval => 'FREQ=DAILY; BYHOUR=4; BYMINUTE=45;BYSECOND=0'
  , end_date        => NULL
  , enabled         => FALSE  
  , job_class       => 'DEFAULT_JOB_CLASS'
  , job_type        => 'PLSQL_BLOCK'
  , job_action      => '
begin
  --
  -- Controleer het gehele sub-path ongeacht de bestandstypen
  -- en verwerkt bestanden met een wijzigingsdatum t/m een week geleden.
  -- 1) Standaad GENOOM dossier \\umcms005\gen_stg$\helix\acci voor Accilix
  --                            \\umcms005\gen_stg$\helix\prod voor Prodilix, etc.
  --
  beh_digidos_backup.maak_backup( p_changed_within_minutes => (60*24*7)
                                , p_path_werkbestanden     => ''\\umcms005\gen_stg$\helix\'' || lower(substr(ora_database_name, 1, 4))
                                , p_path_backup            => ''\\umcms36\gen_stg2$\Backup\'' || lower(substr(ora_database_name, 1, 4))
                                );
end;
commit;
'
  , comments        => NULL
  );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_RUNS);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
     ,attribute => 'MAX_RUNS');
  BEGIN
    SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
      ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
       ,attribute => 'STOP_ON_WINDOW_CLOSE'
       ,value     => FALSE);
  EXCEPTION
    -- could fail if program is of type EXECUTABLE...
    WHEN OTHERS THEN
      NULL;
  END;
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
     ,attribute => 'SCHEDULE_LIMIT'
     ,value     => '+000 01:00:00');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name       => 'HELIX.BEH_DIGIDOS_BACKUP_DAILY');
END;
/
