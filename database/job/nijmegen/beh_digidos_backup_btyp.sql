BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB(job_name  => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
  ( job_name        => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
  , start_date      => sysdate
  , repeat_interval => 'FREQ=MINUTELY;BYMINUTE=2,7,12,17,22,27,32,37,42,47,52,57;BYSECOND=0'
  , end_date        => NULL
  , enabled         => FALSE
  , job_class       => 'DEFAULT_JOB_CLASS'
  , job_type        => 'PLSQL_BLOCK'
  , job_action      => '
begin
  --
  -- Controleer de subpaden die voorkomen als bestandstype 
  -- en verwerkt bestanden met een wijzigingsdatum t/m 2 uur geleden.
  -- 1) Standaad GENOOM dossier \\umcms005\gen_stg$\helix\acci voor Accilix
  --                            \\umcms005\gen_stg$\helix\prod voor Prodilix, etc.
  --
  beh_digidos_backup.maak_backup_btyp( p_changed_within_minutes  => 120
                                     , p_path_btyp_werkbestanden => ''\\umcms005\gen_stg$\helix\'' || lower(substr(ora_database_name, 1, 4))
                                     , p_path_backup             => ''\\umcms36\gen_stg2$\Backup\'' || lower(substr(ora_database_name, 1, 4))
                                     );
end;
commit;
'
  , comments        => NULL
  );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_RUNS);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
     ,attribute => 'MAX_RUNS');
  BEGIN
    SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
      ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
       ,attribute => 'STOP_ON_WINDOW_CLOSE'
       ,value     => FALSE);
  EXCEPTION
    -- could fail if program is of type EXECUTABLE...
    WHEN OTHERS THEN
      NULL;
  END;
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
     ,attribute => 'SCHEDULE_LIMIT'
     ,value     => '+000 01:00:00');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name       => 'HELIX.BEH_DIGIDOS_BACKUP_BTYP');
END;
/
