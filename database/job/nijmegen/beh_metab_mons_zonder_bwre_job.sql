
BEGIN
dbms_scheduler.create_job('"BEH_METAB_MONS_ZONDER_BWRE_JOB"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
--parameter wordt zo gebruikt:
-- sysdate-parameter
-- aantal dagen terug
BEH_METAB_MONS_ZONDER_BWRE(31);
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('04-MAY-2015 04.32.12.722000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MONTHLY;BYMONTHDAY=1;BYHOUR=23;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Monsters zonder bewaarreden (metab)'
);
dbms_scheduler.enable('"BEH_METAB_MONS_ZONDER_BWRE_JOB"');
COMMIT;
END;
/

/
QUIT
