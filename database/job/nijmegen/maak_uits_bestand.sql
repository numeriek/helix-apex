
BEGIN
dbms_scheduler.create_job('"MAAK_UITS_BESTAND"',
job_type=>'STORED_PROCEDURE', job_action=>
'"HELIX"."BEH_UITS_00"."START_VERWERKING_VIA_JOB"'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('11-DEC-2012 10.25.08.000000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MINUTELY;INTERVAL=5;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'aanmaken uitslag bestanden'
);
dbms_scheduler.set_attribute('"MAAK_UITS_BESTAND"','schedule_limit','+000 01:00:00');
dbms_scheduler.set_attribute('"MAAK_UITS_BESTAND"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"MAAK_UITS_BESTAND"');
COMMIT;
END;
/

/
QUIT
