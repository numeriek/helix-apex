
BEGIN
dbms_scheduler.create_job('"DIPO_SMS_OVERZICHTEN"',
job_type=>'PLSQL_BLOCK', job_action=>
'declare
v_dagen_terug number := 1; -- wordt van sysdate afgetrokken.
v_afzender varchar2(100) := ''cukz124@umcn.nl'';
v_ontvanger varchar2(1000):= ''cukz124@umcn.nl,h.bourchi@sb.umcn.nl'';
v_subject varchar2(100) := ''foute bij uitvoer: beh_dipo_00.sms_overzichten'';
v_ok   boolean;
v_msg varchar2(32767);
begin
    v_ok :=beh_dipo_00.sms_overzichten
                         (
                             p_dagen_terug => v_dagen_terug,
                             x_msg => v_msg
                        );
    if not v_ok then
        UTL_MAIL.SEND(  sender => v_afzender,
                recipients => v_ontvanger, -- komma gescheiden
                subject => v_subject,
                message => v_msg);
    end if;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('18-OCT-2012 05.00.00.000000000 PM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYHOUR=6;BYMINUTE=00; BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"DIPO_SMS_OVERZICHTEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.set_attribute('"DIPO_SMS_OVERZICHTEN"','restartable',TRUE);
dbms_scheduler.enable('"DIPO_SMS_OVERZICHTEN"');
COMMIT;
END;
/

/
QUIT
