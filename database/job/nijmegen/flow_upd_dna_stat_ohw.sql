
BEGIN
dbms_scheduler.create_job('"FLOW_UPD_DNA_STAT_OHW"',
job_type=>'STORED_PROCEDURE', job_action=>
'HELIX.BEH_FLOW_DNA_STAT_OHW_UPD'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('06-OCT-2012 12.26.20.879000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYHOUR=23;BYMINUTE=55; BYSECOND=0;BYDAY=SUN'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"FLOW_UPD_DNA_STAT_OHW"','restartable',TRUE);
dbms_scheduler.enable('"FLOW_UPD_DNA_STAT_OHW"');
COMMIT;
END;
/

/
QUIT
