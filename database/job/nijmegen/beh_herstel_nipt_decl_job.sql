
BEGIN
dbms_scheduler.create_job('"BEH_HERSTEL_NIPT_DECL_JOB"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
  beh_herstel_nipt_decl;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('29-DEC-2014 10.41.11.918000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY;BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Hestellen van NIPT declaraties met een foutieve verrichtingcode'
);
dbms_scheduler.enable('"BEH_HERSTEL_NIPT_DECL_JOB"');
COMMIT;
END;
/

/
QUIT
