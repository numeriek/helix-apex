
BEGIN
dbms_scheduler.create_job('"DNA_STOF_OP_WLST_PLAATSEN"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
  --
  -- Plaats MIP en ION stoftesten op werklijst voor Mantis 12416
  --
  beh_maak_wlst_trge.plaats_op_werklijst
  ( p_email_aan       => ''alwin.rikken@radboudumc.nl''
  , p_kafd_code       => ''GENOOM''
  , p_tech_code       => ''MIP''
  , p_trty_code       => ''384-WELL''
  , p_trgr_code       => ''384-STAN''
  , p_stoftest_filter => ''MIP%S''
  );
  commit;
  --
  beh_maak_wlst_trge.plaats_op_werklijst
  ( p_email_aan       => ''alwin.rikken@radboudumc.nl''
  , p_kafd_code       => ''GENOOM''
  , p_tech_code       => ''ION''
  , p_trty_code       => ''384-WELL''
  , p_trgr_code       => ''384-STAN''
  , p_stoftest_filter => ''ION%S''
  );
  commit;
  --
end;
'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('02-SEP-2016 03.21.07.467000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYDAY=MON,TUE,WED,THU,FRI;BYHOUR=4;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Plaats gefilterde MIP en ION stoftesten op werklijst voor GENOOM'
);
dbms_scheduler.set_attribute('"DNA_STOF_OP_WLST_PLAATSEN"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"DNA_STOF_OP_WLST_PLAATSEN"');
COMMIT;
END;
/

/
QUIT
