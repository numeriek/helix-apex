
BEGIN
dbms_scheduler.create_job('"BEH_ONCO_LOAD"',
job_type=>'STORED_PROCEDURE', job_action=>
'helix.beh_onco_00.laad_onco'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('16-OCT-2015 04.00.00.000000000 AM +02:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY; BYDAY=MON,TUE,WED,THU,FRI; BYHOUR=4; BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
NULL
);
dbms_scheduler.set_attribute('"BEH_ONCO_LOAD"','schedule_limit','+000 01:00:00');
dbms_scheduler.set_attribute('"BEH_ONCO_LOAD"','logging_level',DBMS_SCHEDULER.LOGGING_RUNS);
dbms_scheduler.enable('"BEH_ONCO_LOAD"');
COMMIT;
END;
/

/
QUIT
