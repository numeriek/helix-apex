
BEGIN
dbms_scheduler.create_job('"BEH_USERS_LAST_LOGIN_JOB"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
beh_users_last_login;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('05-MAR-2014 02.44.39.264000000 PM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=MONTHLY;BYMONTHDAY=1;BYHOUR=18;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'Helix gebruikers die in afgelopen jaar niet ingelogd zijn'
);
dbms_scheduler.enable('"BEH_USERS_LAST_LOGIN_JOB"');
COMMIT;
END;
/

/
QUIT
