
BEGIN
dbms_scheduler.create_job('"BEH_ZET_HOM_ICODES_JOB"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin
beh_zet_hom_icodes;
end;'
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('12-FEB-2014 10.49.03.932000000 AM EUROPE/BERLIN','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY;BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN;BYHOUR=22;BYMINUTE=0;BYSECOND=0'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>FALSE,comments=>
'hom wds declaraties krijgen een vaste icode in onderzoek nivo'
);
dbms_scheduler.enable('"BEH_ZET_HOM_ICODES_JOB"');
COMMIT;
END;
/

/
QUIT
