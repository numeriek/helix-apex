
BEGIN
dbms_scheduler.create_job('"LOGGER_UNSET_PREFS_BY_CLIENT"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin logger.unset_client_level; end; '
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('24-FEB-2017 01.17.10.352000000 PM +01:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=HOURLY; BYHOUR=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>TRUE,comments=>
'Clears logger prefs by client_id'
);
dbms_scheduler.enable('"LOGGER_UNSET_PREFS_BY_CLIENT"');
COMMIT;
END;
/

/
QUIT
