
BEGIN
dbms_scheduler.create_job('"LOGGER_PURGE_JOB"',
job_type=>'PLSQL_BLOCK', job_action=>
'begin logger.purge; end; '
, number_of_arguments=>0,
start_date=>TO_TIMESTAMP_TZ('24-FEB-2017 01.17.10.320000000 PM +01:00','DD-MON-RRRR HH.MI.SSXFF AM TZR','NLS_DATE_LANGUAGE=english'), repeat_interval=>
'FREQ=DAILY; BYHOUR=1'
, end_date=>NULL,
job_class=>'"DEFAULT_JOB_CLASS"', enabled=>FALSE, auto_drop=>TRUE,comments=>
'Purges LOGGER_LOGS using default values defined in logger_prefs.'
);
dbms_scheduler.enable('"LOGGER_PURGE_JOB"');
COMMIT;
END;
/

/
QUIT
