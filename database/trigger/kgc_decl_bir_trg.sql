CREATE OR REPLACE TRIGGER "HELIX"."KGC_DECL_BIR_TRG"
 BEFORE INSERT
 ON KGC_DECLARATIES
 FOR EACH ROW
BEGIN
  IF ( :new.entiteit IS NULL
    OR :new.id IS NULL
     )
  THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00109'
    , p_errtp => 'E'
    , p_rftf => FALSE
    );
  END IF;
  -- bepaal waarden uit bron-tabellen
  kgc_decl_00.bepaal_waarden
  ( p_entiteit => :new.entiteit
  , p_id => :new.id
  , x_declareren => :new.declareren
  , x_kafd_id => :new.kafd_id
  , x_ongr_id => :new.ongr_id
  , x_pers_id => :new.pers_id
  , x_deen_id => :new.deen_id
  , x_dewy_id => :new.dewy_id
  , x_ingr_id => :new.ingr_id
  , x_verz_id => :new.verz_id
  , x_fare_id => :new.fare_id
  , x_rnde_id => :new.rnde_id
  , x_datum => :new.datum
  , x_betreft_tekst => :new.betreft_tekst
  , x_toelichting => :new.toelichting
  , x_verrichtingcode => :new.verrichtingcode
  , x_nota_adres => :new.nota_adres
  , x_verzekeringswijze => :new.verzekeringswijze
  , x_verzekeringsnr => :new.verzekeringsnr
  , x_pers_id_alt_decl => :new.pers_id_alt_decl
  );
END;

/
ALTER TRIGGER "HELIX"."KGC_DECL_BIR_TRG" ENABLE;

/
QUIT
