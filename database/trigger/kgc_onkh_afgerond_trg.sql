CREATE OR REPLACE TRIGGER "HELIX"."KGC_ONKH_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON KGC_ONDERZOEK_KOPIEHOUDERS
 FOR EACH ROW
DECLARE


  --CURSOR Added by Anuja against Mantis 6492
  CURSOR ongr_kafd_cur
  IS
    SELECT kafd_id,ongr_id
    FROM kgc_onderzoeken
    WHERE onde_id=:new.onde_id;
BEGIN
    FOR r in ongr_kafd_cur
     LOOP
        IF kgc_sypa_00.standaard_waarde ('KOPIEHOUDER_AFGEROND_ONDERZOEK',p_kafd_id =>r.kafd_id,p_ongr_id =>r.ongr_id)= 'N'
        THEN --IF Added by Anuja against Mantis 6492
          IF ( kgc_onde_00.afgerond( p_onde_id => NVL(:new.onde_id,:old.onde_id) ) )
          THEN
            qms$errors.show_message
            ( p_mesg => 'KGC-00045'
            , p_errtp => 'E'
            , p_rftf => TRUE
            );
          END IF;
        END IF;
      END LOOP;
END;
/
ALTER TRIGGER "HELIX"."KGC_ONKH_AFGEROND_TRG" ENABLE;

/
QUIT
