CREATE OR REPLACE TRIGGER "HELIX"."KGC_ONMO_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON KGC_ONDERZOEK_MONSTERS
 FOR EACH ROW
BEGIN
  IF ( kgc_onde_00.afgerond( p_onde_id => NVL(:new.onde_id,:old.onde_id) ) )
  THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00045'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_ONMO_AFGEROND_TRG" ENABLE;

/
QUIT
