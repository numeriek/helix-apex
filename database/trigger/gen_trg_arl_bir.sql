CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_ARL_BIR"
   BEFORE INSERT
   ON GEN_APPL_ROLES
   FOR EACH ROW
BEGIN
   -- Vullen primary Key ID veld indien niet reeds meegegeven
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_arl_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_ARL_BIR" ENABLE;

/
QUIT
