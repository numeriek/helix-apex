CREATE OR REPLACE TRIGGER "HELIX"."KGC_DIAG_BIUR_TRG"
 BEFORE INSERT OR UPDATE OF ONBE_ID
 ON KGC_DIAGNOSES
 FOR EACH ROW
DECLARE

  cursor onbe_cur
  is
    select pers_id
    from   kgc_onderzoek_betrokkenen
    where  onbe_id = :new.onbe_id
    ;

--  cursor onde_cur
-- is
--    select pers_id
--    from   kgc_onderzoeken
--    where  onde_id = :new.onbe_id
--    ;
begin
  if ( :new.onbe_id is not null )
  then
    open  onbe_cur;
    fetch onbe_cur
    into  :new.pers_id;
    close onbe_cur;
-- Mantis 3914: bepaling pers id o.b.v. onde_id is niet terecht
-- ook niet als in de cursor :new.onbe_id wordt vervangen door :new.onde_id
--  elsif ( :new.onde_id is not null )
--  then
--    open  onde_cur;
--    fetch onde_cur
--    into  :new.pers_id;
--    close onde_cur;
  end if;
end;

/
ALTER TRIGGER "HELIX"."KGC_DIAG_BIUR_TRG" ENABLE;

/
QUIT
