CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_STN_BIR"
   BEFORE INSERT
   ON GEN_STATUS_TRANSITIONS
   FOR EACH ROW
BEGIN
   -- Determine primary Key ID when no value inserted
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_stn_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   --
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_STN_BIR" ENABLE;

/
QUIT
