CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_AUS_BUR"
   BEFORE UPDATE
   ON GEN_APPL_USERS
   FOR EACH ROW
BEGIN
   IF :new.password <> :old.password
   THEN
      :new.password   := gen_pck_encryption.f_checksum(:new.password);
   END IF;
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_AUS_BUR" ENABLE;

/
QUIT
