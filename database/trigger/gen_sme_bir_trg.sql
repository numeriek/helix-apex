CREATE OR REPLACE TRIGGER "HELIX"."GEN_SME_BIR_TRG"
   BEFORE INSERT
   ON GEN_SYSTEM_MESSAGES
   FOR EACH ROW
DECLARE
   CURSOR c_sys(b_sys_id GEN_systems.id%TYPE)
   IS
      SELECT package_name
        FROM GEN_systems
       WHERE id = b_sys_id;
   r_sys            c_sys%ROWTYPE;
   l_msg_type_name  GEN_system_message_types.name%TYPE;
BEGIN
   -- Get id from sequence if null
   IF :new.id IS NULL
   THEN
      SELECT GEN_seq_sme_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   :new.created_date        := SYSDATE;
   :new.created_by          := NVL(apex_application.g_user, USER);
   :new.modified_date       := SYSDATE;
   :new.modified_by         := NVL(apex_application.g_user, USER);
   :new.timestamp_message   := SYSDATE;
   :new.nr_retries          := 0;
   :new.nr_resubmits        := 0;
   l_msg_type_name          :=
      GEN_pck_systems.f_get_active_message_type_name(:new.message_type_id);

  -- logger.log('in insert trigger :new.system_id_to='||:new.system_id_to);
   --logger.log('in insert trigger :new.system_id_from='||:new.system_id_from);

   IF :new.system_id_to IS NULL
   THEN
      -- to APEX, receive message
      GEN_pck_systems.p_receive_message(
         i_message_type => l_msg_type_name
        ,i_message_id => :new.id
        ,i_system_id_from => :new.system_id_from
        ,i_request_message => :new.MESSAGE
        ,i_timestamp_message => :new.timestamp_message
        ,i_message_reference_id => :new.message_reference_id
        ,io_status_id => :new.status_id
        ,io_status_description => :new.status_description);


          :new.timestamp_processed   := SYSTIMESTAMP;

         IF :new.status_id IS NOT NULL THEN
            -- send an email
            GEN_pck_systems.p_mail_system_appl(:new.system_id_from
                              ,:new.status_id
                              ,:new.message_type_id
                              ,:new.id
                              ,:new.status_description
                               );
         END IF;
   ELSIF :new.system_id_from IS NULL
   THEN
      -- from APEX
      -- get the to interface package
      OPEN c_sys(:new.system_id_to);
      FETCH c_sys
         INTO r_sys;
      CLOSE c_sys;
      IF r_sys.package_name IS NOT NULL
      THEN
         EXECUTE IMMEDIATE
               'begin '
            || r_sys.package_name
            || '.p_send_message(:p_message_type
                              ,:p_message
                              ,:p_system_id_to
                              ,:p_nr_retries
                              ,:p_status_id
                              ,:p_message_reference_id
                              ,:p_message_correlation_id
                              ,:p_status_description); end;'
            USING l_msg_type_name
                 ,:new.MESSAGE
                 ,:new.system_id_to
                 ,:new.nr_retries
                 ,OUT :new.status_id
                 ,OUT :new.message_reference_id
                 ,OUT :new.message_correlation_id
                 ,OUT :new.status_description;
         :new.timestamp_processed   := SYSTIMESTAMP;

        -- logger.log('status='||:new.status_id);

         IF :new.status_id IS NOT NULL THEN
            -- send an email
            GEN_pck_systems.p_mail_system_appl(:new.system_id_to
                              ,:new.status_id
                              ,:new.message_type_id
                              ,:new.id
                              ,:new.status_description
                               );
         END IF;

      ELSE
         :new.status_id            :=
            GEN_pck_general.f_get_status('SYSTEM_MESSAGES'
                                      ,'E');
         :new.status_description   := 'No system package defined';
      END IF;
   END IF;
END;
/
ALTER TRIGGER "HELIX"."GEN_SME_BIR_TRG" ENABLE;

/
QUIT
