CREATE OR REPLACE TRIGGER "HELIX"."BAS_MDET_AR_IUD_TRG"
 AFTER DELETE OR INSERT OR UPDATE
 ON BAS_MEETWAARDE_DETAILS
 FOR EACH ROW
DECLARE

  v_aktie VARCHAR2(1);
BEGIN
  bas_mdet_00.add_plsql_table
  ( p_mdet_id => NVL( :new.mdet_id, :old.mdet_id )
  , p_meet_id => NVL( :new.meet_id, :old.meet_id )
  , p_oude_waarde => :old.waarde
  , p_nieuwe_waarde => :new.waarde
  );
  IF ( inserting )
  THEN
    v_aktie := 'I';
  ELSIF ( deleting )
  THEN
    v_aktie := 'D';
  ELSE
    v_aktie := 'U';
  END IF;
  bas_mdok_00.zet_mdet_ok
  ( p_aktie => v_aktie
  , p_waarde => NVL(:new.waarde,:old.waarde)
  , p_mdet_id => NVL(:new.mdet_id,:old.mdet_id)
  , p_meet_id => NVL(:new.meet_id,:old.meet_id)
  , p_volgorde => NVL(:new.volgorde,:old.volgorde)
  );
END;

/
ALTER TRIGGER "HELIX"."BAS_MDET_AR_IUD_TRG" ENABLE;

/
QUIT
