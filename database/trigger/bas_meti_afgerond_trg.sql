CREATE OR REPLACE TRIGGER "HELIX"."BAS_METI_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON BAS_METINGEN
 FOR EACH ROW
BEGIN
  -- door het wijzigen van een onderzoek worden declaraties opnieuw bepaald, ook op meting/deelonderzoek niveau.
  -- alleen het veld declareren (en de audit-kolommen nartuurlijk) wordt dan (mogelijk) geupdate:
  -- de rest mag niet veranderen als het onderzoek is afgerond
  if ( kgc_util_00.verschil( :old.conclusie, :new.conclusie )
    or kgc_util_00.verschil( :old.datum_conclusie, :new.datum_conclusie )
    or kgc_util_00.verschil( :old.afgerond, :new.afgerond )
    or kgc_util_00.verschil( :old.datum_afgerond, :new.datum_afgerond )
    or kgc_util_00.verschil( :old.mede_id, :new.mede_id )
    or kgc_util_00.verschil( :old.setnaam, :new.setnaam )
    or kgc_util_00.verschil( :old.herhalen, :new.herhalen )
    or kgc_util_00.verschil( :old.prioriteit, :new.prioriteit )
    or kgc_util_00.verschil( :old.snelheid, :new.snelheid )
     )
  then
    IF ( kgc_onde_00.afgerond( p_onde_id => NVL(:new.onde_id,:old.onde_id)
                             , p_rftf => FALSE
                             )
       )
    THEN
      qms$errors.show_message
      ( p_mesg => 'KGC-00045'
      , p_errtp => 'E'
      , p_rftf => TRUE
      );
    END IF;
  END IF;
END;


/
ALTER TRIGGER "HELIX"."BAS_METI_AFGEROND_TRG" ENABLE;

/
QUIT
