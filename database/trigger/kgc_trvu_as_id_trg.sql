CREATE OR REPLACE TRIGGER "HELIX"."KGC_TRVU_AS_ID_TRG"
 AFTER DELETE OR INSERT
 ON KGC_TRAY_VULLING
DECLARE

  v_actie VARCHAR2(1); -- I,D
  i BINARY_INTEGER := cg$KGC_TRAY_VULLING.cg$table.FIRST;
  v_meet_id NUMBER;
  v_trge_id NUMBER;
  v_trvu_id NUMBER;
BEGIN
  IF ( inserting )
  THEN
    v_actie := 'I';
  ELSIF ( deleting )
  THEN
    v_actie := 'D';
  END IF;
  IF ( v_actie IS NOT NULL )
  THEN
    WHILE ( i IS NOT NULL )
    LOOP
      v_meet_id := cg$KGC_TRAY_VULLING.cg$TABLE(i).meet_id;
      v_trge_id := cg$KGC_TRAY_VULLING.cg$TABLE(i).trge_id;
      v_trvu_id := cg$KGC_TRAY_VULLING.cg$TABLE(i).trvu_id;
      kgc_wlst_00.sync_tray_werklijst
      ( p_actie => v_actie
      , p_meet_id => v_meet_id
      , p_trge_id => v_trge_id
      , p_trvu_id => v_trvu_id
      );
      i := cg$KGC_TRAY_VULLING.cg$table.NEXT(i);
    END LOOP;
  END IF;
END;
/
ALTER TRIGGER "HELIX"."KGC_TRVU_AS_ID_TRG" ENABLE;

/
QUIT
