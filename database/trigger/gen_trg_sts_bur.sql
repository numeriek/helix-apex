CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_STS_BUR"
   BEFORE UPDATE
   ON GEN_STATUSES
   FOR EACH ROW
BEGIN
   :new.category        := UPPER(:new.category);
   :new.status_code     := UPPER(:new.status_code);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_STS_BUR" ENABLE;

/
QUIT
