CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_APR_BUR"
   before update
   ON GEN_APPL_PARAMETERS    for each row
begin
   :new.modified_on   := sysdate;
   :new.modified_by     := nvl(apex_application.g_user, user);
end;
/
ALTER TRIGGER "HELIX"."GEN_TRG_APR_BUR" ENABLE;

/
QUIT
