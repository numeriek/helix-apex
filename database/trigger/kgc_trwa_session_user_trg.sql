CREATE OR REPLACE TRIGGER "HELIX"."KGC_TRWA_SESSION_USER_TRG"
 BEFORE INSERT
 ON KGC_TRAY_WIZARD_STAN_TEMP
 FOR EACH ROW
begin
  if :new.session_id is null
  then
    :new.session_id := sys_context ('USERENV', 'SESSIONID');
  end if;
  if :new.username is null
  then
    :new.username := user;
  end if;
end;

/
ALTER TRIGGER "HELIX"."KGC_TRWA_SESSION_USER_TRG" ENABLE;

/
QUIT
