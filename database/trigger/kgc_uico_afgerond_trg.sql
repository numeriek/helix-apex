CREATE OR REPLACE TRIGGER "HELIX"."KGC_UICO_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON KGC_UITSLAG_CONCLUSIES
 FOR EACH ROW
BEGIN
  IF ( kgc_onde_00.afgerond( p_uits_id => NVL(:new.uits_id,:old.uits_id) ) )
  THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00045'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_UICO_AFGEROND_TRG" ENABLE;

/
QUIT
