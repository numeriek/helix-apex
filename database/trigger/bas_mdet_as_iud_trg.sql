CREATE OR REPLACE TRIGGER "HELIX"."BAS_MDET_AS_IUD_TRG"
 AFTER DELETE OR INSERT OR UPDATE
 ON BAS_MEETWAARDE_DETAILS
BEGIN
  bas_mdet_00.after_statement_trg;
END;

/
ALTER TRIGGER "HELIX"."BAS_MDET_AS_IUD_TRG" ENABLE;

/
QUIT
