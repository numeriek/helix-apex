CREATE OR REPLACE TRIGGER "HELIX"."KGC_UITS_FOETUS_GESLACHT_TRG"
 AFTER INSERT OR UPDATE
 ON KGC_UITSLAGEN
 FOR EACH ROW
BEGIN
  kgc_uits_00.wijzig_foetus_geslacht
    ( p_toelichting => :new.toelichting
    , p_tekst       => :new.tekst
    , p_onde_id     => :new.onde_id
    );
END;

/
ALTER TRIGGER "HELIX"."KGC_UITS_FOETUS_GESLACHT_TRG" ENABLE;

/
QUIT
