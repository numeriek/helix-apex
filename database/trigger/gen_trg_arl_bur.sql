CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_ARL_BUR"
   BEFORE UPDATE
   ON GEN_APPL_ROLES
   FOR EACH ROW
BEGIN
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_ARL_BUR" ENABLE;

/
QUIT
