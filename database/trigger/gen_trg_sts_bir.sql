CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_STS_BIR"
   BEFORE INSERT
   ON GEN_STATUSES
   FOR EACH ROW
BEGIN
   -- Determine primary Key ID when no value inserted
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_sts_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   --
   :new.category        := UPPER(:new.category);
   :new.status_code     := UPPER(:new.status_code);
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_STS_BIR" ENABLE;

/
QUIT
