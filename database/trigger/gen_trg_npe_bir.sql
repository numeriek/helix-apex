CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_NPE_BIR"
   before insert
   ON GEN_NAMED_PREFERENCES    for each row
declare
   l_npe_prefix   varchar2 (30);
begin
   if :new.id is null
   then
      select   gen_seq_npe_id.nextval into :new.id from dual;
   end if;
   l_npe_prefix := 'F_';
   if :new.preference_name not like l_npe_prefix || '%'
   then
      :new.preference_name := l_npe_prefix || :new.preference_name;
   end if;
   :new.preference_name := upper (:new.preference_name);
   :new.created_on := sysdate;
   :new.created_by := nvl (apex_application.g_user, user);
   :new.modified_on := sysdate;
   :new.modified_by := nvl (apex_application.g_user, user);
end;
/
ALTER TRIGGER "HELIX"."GEN_TRG_NPE_BIR" ENABLE;

/
QUIT
