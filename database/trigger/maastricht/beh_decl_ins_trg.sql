CREATE OR REPLACE TRIGGER "HELIX"."BEH_DECL_INS_TRG"
after insert ON HELIX.KGC_DECLARATIES
  for each row
declare
   Cursor C_Meti(c_meti_Id Number)
   Is
     Select Onde_id
     From   bas_metingen
     where  meti_id = c_meti_id;

   Cursor C_ONBE(c_Onbe_Id Number)
   Is
     Select onde_id
     From   kgc_onderzoek_betrokkenen
     Where  onbe_id = c_onbe_id;

   r_meti c_Meti%Rowtype;
   r_onbe c_Onbe%Rowtype;

begin
    /* Op basis van de entiteit in de declaratie tabel wordt een referentie tabel gevuld
       Met behulp van de ID en de Entiteit wordt het juiste onde_ID opgehaald en gekoppeld
       aan de decl_id
    */

    Case :new.entiteit
      When  'ONDE' Then
         Insert into beh_onde_decl (Onde_id, decl_id)
         Values(:new.ID, :new.decl_id);

      When  'ONBE' Then

         Open C_onbe(:New.Id);
         Fetch C_Onbe into R_Onbe ;

         If C_Onbe%Found Then
            Insert into beh_onde_decl (Onde_id, decl_id)
            Values(R_onbe.Onde_Id, :new.decl_id);
         End If;
         Close C_Onbe ;
      When  'METI' Then
         Open C_meti(:New.Id);
         Fetch C_meti into R_Meti ;

         If C_Meti%Found Then
            Insert into beh_onde_decl (Onde_id, decl_id)
            Values(R_meti.Onde_Id, :new.decl_id);
            null;
         End If;
         Close C_Meti ;

      Else
         Null;

      End Case;


end ;

/
ALTER TRIGGER "HELIX"."BEH_DECL_INS_TRG" ENABLE;

/
QUIT
