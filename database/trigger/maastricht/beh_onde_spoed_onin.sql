CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONDE_SPOED_ONIN"
  Before Insert Or Update Or Delete ON HELIX.KGC_ONDERZOEK_INDICATIES
  for each row
Declare
  Cursor c_onde(c_onde_id Integer) Is
    Select ongr.code ongr_code
      From kgc_onderzoeksgroepen ongr
      ,    kgc_onderzoeken onde
     Where onde.onde_id = c_onde_id
     and   ongr.ongr_id = onde.ongr_id;

  Cursor c_indi Is
    Select indi.indi_id
      From kgc_indicatie_teksten indi
     Where indi.code In ('DPD','11');

  r_onde c_onde%Rowtype;
  r_indi c_indi%Rowtype;

  -- local variables here
Begin
  Open c_indi;
  Fetch c_indi
    Into r_indi;
  Close c_indi;

  If r_indi.indi_id = :new.indi_id
  Then
    Open c_onde(:new.onde_id);
    Fetch c_onde
      Into r_onde;
    Close c_onde;
    case when r_onde.ongr_code in ('DNA','CYTOM','CYTOV')
      Then
        Update kgc_onderzoeken o
        Set    o.spoed = 'J'
        Where  o.onde_id = :new.onde_id;
      Else
          NULL;
    End case;
  End If;

End beh_onde_spoed;

/
ALTER TRIGGER "HELIX"."BEH_ONDE_SPOED_ONIN" ENABLE;

/
QUIT
