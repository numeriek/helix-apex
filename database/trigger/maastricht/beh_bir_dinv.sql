CREATE OR REPLACE TRIGGER "HELIX"."BEH_BIR_DINV"
before insert
ON HELIX.BEH_INVOER_DIAGNOSES for each row
  WHEN (
new.dinv_id IS NULL
      ) begin
  :new.dinv_id := beh_dinv_seq.nextval;
end;

/
ALTER TRIGGER "HELIX"."BEH_BIR_DINV" ENABLE;

/
QUIT
