CREATE OR REPLACE TRIGGER "HELIX"."BEH_PERSONEN_UPDATE_TRG"
  before update ON HELIX.KGC_PERSONEN
  for each row
declare
    v_conv_zisnr varchar2(1);
    -- local variables here
begin
   IF :NEW.ZISNR <> :OLD.ZISNR OR :NEW.ZISNR IS NULL
   THEN
      v_conv_zisnr := 'J';
   else
      v_conv_zisnr := 'N';
   END IF;
      INSERT INTO BEH_PERSONEN_UPDATE ( conversie_zisnr
                                       ,converted_id
                                       ,UPDATE_DATE
                                       ,PERS_ID
                                       ,ACHTERNAAM
                                       ,GEHUWD
                                       ,GESLACHT
                                       ,BLOEDVERWANT
                                       ,MEERLING
                                       ,OVERLEDEN
                                       ,DEFAULT_AANSPREKEN
                                       ,ZOEKNAAM
                                       ,CREATED_BY
                                       ,CREATION_DATE
                                       ,LAST_UPDATED_BY
                                       ,LAST_UPDATE_DATE
                                       ,RELA_ID
                                       ,VERZ_ID
                                       ,AANSPREKEN
                                       ,GEBOORTEDATUM
                                       ,Part_Geboortedatum
                                       ,VOORLETTERS
                                       ,VOORVOEGSEL
                                       ,ACHTERNAAM_PARTNER
                                       ,VOORVOEGSEL_PARTNER
                                       ,ADRES
                                       ,POSTCODE
                                       ,WOONPLAATS
                                       ,PROVINCIE
                                       ,LAND
                                       ,TELEFOON
                                       ,TELEFAX
                                       ,EMAIL
                                       ,VERZEKERINGSWIJZE
                                       ,VERZEKERINGSNR
                                       ,OVERLIJDENSDATUM
                                       ,HOEVEELLING
                                       ,ZOEKFAMILIE
                                       ,ZISNR
                                       ,BEHEER_IN_ZIS
                                       ,TELEFOON2
                                       ,BSN
                                       ,BSN_GEVERIFIEERD
                                      )


      VALUES                         (  v_conv_zisnr
                                       ,:NEW.ZISNR
                                       ,sysdate
                                       ,:OLD.PERS_ID
                                       ,:OLD.ACHTERNAAM
                                       ,:OLD.GEHUWD
                                       ,:OLD.GESLACHT
                                       ,:OLD.BLOEDVERWANT
                                       ,:OLD.MEERLING
                                       ,:OLD.OVERLEDEN
                                       ,:OLD.DEFAULT_AANSPREKEN
                                       ,:OLD.ZOEKNAAM
                                       ,:OLD.CREATED_BY
                                       ,:OLD.CREATION_DATE
                                       ,:OLD.LAST_UPDATED_BY
                                       ,:OLD.LAST_UPDATE_DATE
                                       ,:OLD.RELA_ID
                                       ,:OLD.VERZ_ID
                                       ,:OLD.AANSPREKEN
                                       ,:OLD.GEBOORTEDATUM
                                       ,:OLD.Part_Geboortedatum
                                       ,:OLD.VOORLETTERS
                                       ,:OLD.VOORVOEGSEL
                                       ,:OLD.ACHTERNAAM_PARTNER
                                       ,:OLD.VOORVOEGSEL_PARTNER
                                       ,:OLD.ADRES
                                       ,:OLD.POSTCODE
                                       ,:OLD.WOONPLAATS
                                       ,:OLD.PROVINCIE
                                       ,:OLD.LAND
                                       ,:OLD.TELEFOON
                                       ,:OLD.TELEFAX
                                       ,:OLD.EMAIL
                                       ,:OLD.VERZEKERINGSWIJZE
                                       ,:OLD.VERZEKERINGSNR
                                       ,:OLD.OVERLIJDENSDATUM
                                       ,:OLD.HOEVEELLING
                                       ,:OLD.ZOEKFAMILIE
                                       ,:OLD.ZISNR
                                       ,:OLD.BEHEER_IN_ZIS
                                       ,:OLD.TELEFOON2
                                       ,:OLD.BSN
                                       ,:OLD.BSN_GEVERIFIEERD);


end BEH_PERSONEN_UPDATE_TRG;

/
ALTER TRIGGER "HELIX"."BEH_PERSONEN_UPDATE_TRG" ENABLE;

/
QUIT
