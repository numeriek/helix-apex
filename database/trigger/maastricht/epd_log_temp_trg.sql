CREATE OR REPLACE TRIGGER "HELIX"."EPD_LOG_TEMP_TRG"
  before insert ON HELIX.EPD_LOG_TEMP
  for each row
declare
  -- local variables here
begin
    :new.epd_logtemp_id := EPD_LOG_TEMP_SEQ.NEXTVAL;
end EPD_LOG_TEMP_TRG;

/
ALTER TRIGGER "HELIX"."EPD_LOG_TEMP_TRG" ENABLE;

/
QUIT
