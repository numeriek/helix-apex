CREATE OR REPLACE TRIGGER "HELIX"."BEH_BERICHT_TYPES_TRG_BUR"
before update ON HELIX.BEH_BERICHT_TYPES
for each row
begin
  :new.last_updated_by  := user;
  :new.last_update_date := sysdate;
end;

/
ALTER TRIGGER "HELIX"."BEH_BERICHT_TYPES_TRG_BUR" ENABLE;

/
QUIT
