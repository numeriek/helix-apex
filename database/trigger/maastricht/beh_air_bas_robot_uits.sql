CREATE OR REPLACE TRIGGER "HELIX"."BEH_AIR_BAS_ROBOT_UITS"
--
-- METAB gaat met het fractienummer werken dus er moet een inleesprocedure gedefinieerd worden
-- die het fractienummer naar het monsternummer converteert en in de tabel BAS_ROBOT_UITSLAGEN plaatst.
--
before insert ON HELIX.BAS_ROBOT_UITSLAGEN for each row
declare
  cursor c_frac(b_monsternummer in kgc_monsters.monsternummer%type)
  is
    select mons.monsternummer
    from   kgc_monsters mons
    ,      bas_fracties frac
    where  frac.fractienummer = b_monsternummer
    and    mons.mons_id = frac.mons_id
  ;
  pl_monsternummer kgc_monsters.monsternummer%type;
begin
  open  c_frac(:new.monsternummer);
  fetch c_frac
  into  pl_monsternummer;
  close c_frac;
  :new.monsternummer := pl_monsternummer;
end;

/
ALTER TRIGGER "HELIX"."BEH_AIR_BAS_ROBOT_UITS" ENABLE;

/
QUIT
