CREATE OR REPLACE TRIGGER "HELIX"."ORU_MONSTERS_ORDER_AFD"
  after delete ON HELIX.KGC_MONSTERS
  for each row
declare


begin
   Delete from Interface.ORU_Monster_Order where Mons_id = :old.mons_id ;
end ORU_Monsters_order_AFI;

/
ALTER TRIGGER "HELIX"."ORU_MONSTERS_ORDER_AFD" ENABLE;

/
QUIT
