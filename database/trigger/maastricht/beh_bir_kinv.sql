CREATE OR REPLACE TRIGGER "HELIX"."BEH_BIR_KINV"
before insert
ON HELIX.BEH_KASPINVOER for each row
  WHEN (
new.kinv_id IS NULL
      ) begin
  :new.kinv_id := beh_kinv_seq.nextval;
end;

/
ALTER TRIGGER "HELIX"."BEH_BIR_KINV" ENABLE;

/
QUIT
