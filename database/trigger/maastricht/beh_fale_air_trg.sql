CREATE OR REPLACE TRIGGER "HELIX"."BEH_FALE_AIR_TRG"
--
  AFTER INSERT
  ON HELIX.KGC_FAMILIE_LEDEN
  FOR EACH ROW
declare
    cursor c_fami(b_fami_id in kgc_families.fami_id%type)
    is
      select fami.familienummer
      from   kgc_families fami
      where  fami.fami_id = b_fami_id
    ;
  --
  pl_familienummer kgc_families.familienummer%type;
BEGIN
  open  c_fami(:new.fami_id);
  fetch c_fami
  into  pl_familienummer;
  close c_fami;
  --
  insert into kgc_bestanden ( best_id
                            , entiteit_code
                            , entiteit_pk
                            , btyp_id
                            , bestand_specificatie
                            , volgorde
                            )
  values ( kgc_best_seq.nextval
         , 'PERS'
         , :new.pers_id
         , 863
         , case
             when to_number(pl_familienummer) > 10000
             then '\\PRD.CORP\APPDATA\KGLE_archief\'
                    || (((to_number(substr(pl_familienummer,1,length(pl_familienummer)-3)))*1000)+1)
                    || '-'
                    || (((to_number(substr(pl_familienummer,1,length(pl_familienummer)-3))) * 1000)+1000)
                    || '\'
                    || pl_familienummer
                    || '\'
             when to_number(pl_familienummer) < 1001
             then '\\PRD.CORP\APPDATA\KGLE_archief\00001-01000'||'\'||pl_familienummer||'\'
             when to_number(pl_familienummer) between 9001 and 10000
             then '\\PRD.CORP\APPDATA\KGLE_archief\09001-10000'||'\'||pl_familienummer||'\'
             when to_number(pl_familienummer) between 1001 and 9000
             then '\\PRD.CORP\APPDATA\KGLE_archief\'
                    ||'0'
                    || (((to_number(substr(pl_familienummer,1,length(pl_familienummer)-3)))*1000)+1)
                    || '-'
                    || '0'
                    || (((to_number(substr(pl_familienummer,1,length(pl_familienummer)-3)))*1000)+1000)
                    || '\'
                    || pl_familienummer
                    || '\'
             else null
           end
         , 5
         );
END;

/
ALTER TRIGGER "HELIX"."BEH_FALE_AIR_TRG" ENABLE;

/
QUIT
