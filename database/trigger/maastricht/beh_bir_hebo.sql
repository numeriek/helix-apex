CREATE OR REPLACE TRIGGER "HELIX"."BEH_BIR_HEBO"
before insert
ON HELIX.BEH_HEBON for each row
begin
  :new.hebo_id := beh_hebo_seq.nextval;
end;

/
ALTER TRIGGER "HELIX"."BEH_BIR_HEBO" ENABLE;

/
QUIT
