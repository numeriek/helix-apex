CREATE OR REPLACE TRIGGER "HELIX"."TRG_GEN_APPL_VALIDATIONS_IOU"
   instead of update
   ON V_GEN_APPL_VALIDATIONS
   referencing new as new old as old
   for each row
begin
   update gen_appl_validations
      set application_id            = :new.application_id
         ,domain_name               = :new.domain_name
         ,domain_datatype           = :new.domain_datatype
         ,domain_value_low_n        =
             case :new.domain_datatype
                when 'N' then :new.domain_value_low
                else ''
             end
         ,domain_value_high_n       =
             case :new.domain_datatype
                when 'N' then :new.domain_value_high
                else ''
             end
         ,domain_value_low_c        =
             case :new.domain_datatype
                when 'C' then :new.domain_value_low
                else ''
             end
         ,domain_value_high_c       =
             case :new.domain_datatype
                when 'C' then :new.domain_value_high
                else ''
             end
         ,domain_value_low_d        =
             case :new.domain_datatype
                when 'D' then to_date( :new.domain_value_low, 'DD-MM-YYYY')
                else to_date(null)
             end
         ,domain_value_high_d       =
             case :new.domain_datatype
                when 'D' then to_date( :new.domain_value_high, 'DD-MM-YYYY')
                else to_date(null)
             end
         ,low_value_included_ind    = :new.low_value_included_ind
         ,high_value_included_ind   = :new.high_value_included_ind
         ,active_ind                 = :new.active_ind
    where id = :new.id;
end trg_gen_appl_validations_iou;

/
ALTER TRIGGER "HELIX"."TRG_GEN_APPL_VALIDATIONS_IOU" ENABLE;

/
QUIT
