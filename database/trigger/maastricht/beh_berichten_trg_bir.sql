CREATE OR REPLACE TRIGGER "HELIX"."BEH_BERICHTEN_TRG_BIR"
before insert ON HELIX.BEH_BERICHTEN
for each row
begin
  :new.beri_id          := beh_beri_seq.nextval;
  :new.created_by       := user;
  :new.creation_date    := sysdate;
  :new.last_updated_by  := user;
  :new.last_update_date := sysdate;
end;

/
ALTER TRIGGER "HELIX"."BEH_BERICHTEN_TRG_BIR" ENABLE;

/
QUIT
