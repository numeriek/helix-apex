CREATE OR REPLACE TRIGGER "HELIX"."BEH_BEFORE_DELETE_UITSLAGEN"
  before Delete ON HELIX.KGC_UITSLAGEN
  for each row
declare

E_Error Exception;

  -- local variables here
begin
    Update Epd_Uitslag euits
    set epd_status = 'D'
    where euits.uits_id = :old.uits_id;


end BEH_BEFORE_CHANGE_UITSLAGEN;

/
ALTER TRIGGER "HELIX"."BEH_BEFORE_DELETE_UITSLAGEN" ENABLE;

/
QUIT
