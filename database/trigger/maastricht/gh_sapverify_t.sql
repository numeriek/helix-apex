CREATE OR REPLACE TRIGGER "HELIX"."GH_SAPVERIFY_T"
before insert
ON HELIX.GH_SAPVERIFY for each row
  WHEN (
new.SAPV_ID IS NULL
      ) begin
  :new.sapv_id := gh_sapv_seq.nextval;
end;

/
ALTER TRIGGER "HELIX"."GH_SAPVERIFY_T" ENABLE;

/
QUIT
