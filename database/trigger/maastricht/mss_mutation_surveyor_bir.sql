CREATE OR REPLACE TRIGGER "HELIX"."MSS_MUTATION_SURVEYOR_BIR"
  BEFORE INSERT
  ON HELIX.MSS_MUTATION_SURVEYOR   FOR EACH ROW
DECLARE
  cursor c_sessie
  is
    select dbms_session.unique_session_id sessionid
    from   dual
  ;
  --
  pl_sessie varchar2(30);
  --
BEGIN
--  Application_logic Pre-After-Insert-statement <<Start>>
--
  open  c_sessie;
  fetch c_sessie
  into  pl_sessie;
  close c_sessie;
  --
  :new.sessie := pl_sessie;
END;

/
ALTER TRIGGER "HELIX"."MSS_MUTATION_SURVEYOR_BIR" ENABLE;

/
QUIT
