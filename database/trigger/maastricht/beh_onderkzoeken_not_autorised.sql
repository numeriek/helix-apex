CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONDERKZOEKEN_NOT_AUTORISED"
  before update ON HELIX.KGC_ONDERZOEKEN
  for each row
Declare
  Cursor c_mede(c_mede_id number)
  Is
    Select mede.mede_id
    from kgc_medewerkers mede
    where mede.code in ('HELIX','DB','AJET','LH')
    And  mede.mede_id = c_mede_id;

  r_mede c_mede%RowType;
  E_No_Autorisation_Right Exception;
begin
   Open c_mede(:new.mede_id_autorisator) ;
    fetch c_mede into r_mede ;
    If c_mede%Found  Then
       :new.mede_id_autorisator := null;
       :new.datum_autorisatie := null;
       raise E_No_Autorisation_Right ;

    End If;
    Close c_mede;
Exception
   When E_No_Autorisation_Right Then
        RAISE_APPLICATION_ERROR(-20000, 'U bent niet geautoriseerd om een onderzoek te autoriseren');


end BEH_ONDERKZOEKEN_NOT_AUTORISED;

/
ALTER TRIGGER "HELIX"."BEH_ONDERKZOEKEN_NOT_AUTORISED" ENABLE;

/
QUIT
