CREATE OR REPLACE TRIGGER "HELIX"."EPD_EMBARGO_METHODE_BFI"
  before insert ON HELIX.EPD_EMBARGO_METHODE
  for each row
declare
  -- local variables here
begin
    :NEW.EEMT_ID := EPD_embargo_methode_seq.Nextval ;
    :New.Vervallen := 'N';
end ORU_EMBARGO_METHODE_BFI;

/
ALTER TRIGGER "HELIX"."EPD_EMBARGO_METHODE_BFI" ENABLE;

/
QUIT
