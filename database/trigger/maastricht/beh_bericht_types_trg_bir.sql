CREATE OR REPLACE TRIGGER "HELIX"."BEH_BERICHT_TYPES_TRG_BIR"
before insert ON HELIX.BEH_BERICHT_TYPES
for each row
begin
  :new.bety_id          := beh_bety_seq.nextval;
  :new.created_by       := user;
  :new.creation_date    := sysdate;
  :new.last_updated_by  := user;
  :new.last_update_date := sysdate;
end;

/
ALTER TRIGGER "HELIX"."BEH_BERICHT_TYPES_TRG_BIR" ENABLE;

/
QUIT
