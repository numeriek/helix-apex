CREATE OR REPLACE TRIGGER "HELIX"."BEH_AIR_ONIN_TRG"
after insert
ON HELIX.KGC_ONDERZOEK_INDICATIES for each row
begin
  --
  -- Mantis M0008713.
  -- Zodra er bij een pati�nt een onderzoek wordt aangemaakt met als indicatiecode 'BC1','BC2' of 'BC3', dient het pers_id bij het eerstvolgende
  -- vrije Hebonnummer te worden vastgelegd, evenals de datum van deze uitreiking en de onde_id van het onderzoek.
  -- Het gaat hierbij alleen om onderzoeken van de afdeling DNA. Daarnaast moet het onderzoek aangevraagd zijn door een interne counseler.
  -- Wanneer een pati�nt zowel een BC1- als een BC2-onderzoek krijgt is er maar ��n Hebonregistratie nodig.
  -- Voer na een Hebon registratie een telling uit naar het aantal overgebleven vrije Hebonnummers. Als dat beneden een bepaald pijl komt dient
  -- een mail verstuurd te worden.
  --
  beh_hebo.update_hebo( :new.onde_id
                      , :new.indi_id
                      );
  --
end beh_air_onin_trg;

/
ALTER TRIGGER "HELIX"."BEH_AIR_ONIN_TRG" ENABLE;

/
QUIT
