CREATE OR REPLACE TRIGGER "HELIX"."BEH_DECL_DEL_TRG"
Before Delete ON HELIX.KGC_DECLARATIES
  for each row
declare
Begin
   Delete from beh_onde_decl BOD Where BOD.DECL_ID = :old.decl_id ;

End;

/
ALTER TRIGGER "HELIX"."BEH_DECL_DEL_TRG" ENABLE;

/
QUIT
