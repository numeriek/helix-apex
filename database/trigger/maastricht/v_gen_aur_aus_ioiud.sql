CREATE OR REPLACE TRIGGER "HELIX"."V_GEN_AUR_AUS_IOIUD"
   INSTEAD OF INSERT OR UPDATE OR DELETE
   ON V_GEN_AUR_AUS
   FOR EACH ROW
DECLARE
   l_nw_aus_id  NUMBER;
   l_list       apex_application_global.vc_arr2;
   l_cnt_aur    NUMBER;
   CURSOR c_assigned(b_aus_id IN NUMBER)
   IS
      SELECT *
        FROM gen_appl_user_roles aur
       WHERE aur.appl_user_id = b_aus_id;
BEGIN
   IF INSERTING
   THEN
      INSERT INTO gen_appl_users(user_id
                                ,user_abbr
                                ,user_name
                                ,email
                                ,active_ind
                                ,visitor_allowed_ind
                                ,prod_user
                                ,obi_role
                                ,password
                                ,ldap_domain)
           VALUES (:new.user_id
                  ,:new.user_abbr
                  ,:new.user_name
                  ,:new.email
                  ,:new.active_ind
                  ,:new.visitor_allowed_ind
                  ,:new.prod_user
                  ,:new.obi_role
                  ,:new.password
                  ,:new.ldap_domain)
        RETURNING id
             INTO l_nw_aus_id;
      IF :new.user_roles IS NOT NULL
      THEN
         l_list   := apex_util.string_to_table(:new.user_roles);
         FOR i IN 1 .. l_list.COUNT
         LOOP
            INSERT INTO gen_appl_user_roles(appl_user_id
                                           ,appl_role_id)
                 VALUES (l_nw_aus_id
                        ,l_list(i));
         END LOOP;
      END IF;
   ELSIF UPDATING
   THEN
      UPDATE gen_appl_users
         SET user_id               = :new.user_id
            ,user_abbr             = :new.user_abbr
            ,user_name             = :new.user_name
            ,email                 = :new.email
            ,active_ind            = :new.active_ind
            ,visitor_allowed_ind   = :new.visitor_allowed_ind
            ,prod_user             = :new.prod_user
            ,obi_role              = :new.obi_role
            ,ldap_domain           = :new.ldap_domain
       WHERE id = :new.id;
      IF :new.user_roles IS NULL
      THEN
         DELETE FROM gen_appl_user_roles
               WHERE appl_user_id = :new.id;
      ELSE
         l_list   := apex_util.string_to_table(:new.user_roles);
         FOR i IN 1 .. l_list.COUNT
         LOOP
            l_cnt_aur   := 0;
            SELECT COUNT(*)
              INTO l_cnt_aur
              FROM gen_appl_user_roles aur
             WHERE aur.appl_role_id = l_list(i)
               AND aur.appl_user_id = :new.id;
            IF l_cnt_aur = 0
            THEN -- bestaat nog niet
               INSERT INTO gen_appl_user_roles(appl_user_id
                                              ,appl_role_id)
                    VALUES (:new.id
                           ,l_list(i));
            END IF;
         END LOOP;
         -- records verwijderen
         FOR r_assigned IN c_assigned(b_aus_id => :new.id)
         LOOP
            l_cnt_aur   := 0;
            FOR i IN 1 .. l_list.COUNT
            LOOP
               IF l_list(i) = r_assigned.appl_role_id
               THEN
                  l_cnt_aur   := 1;
                  EXIT;
               END IF;
            END LOOP;
            -- record komt niet meer voor in de lijst en is dus uit de lijst gehaald
            -- dus ook verwijderen uit de DB
            IF l_cnt_aur = 0
            THEN
               DELETE FROM gen_appl_user_roles
                     WHERE id = r_assigned.id;
            END IF;
         END LOOP;
      END IF;
   ELSIF DELETING
   THEN
      DELETE FROM gen_appl_user_roles
            WHERE appl_user_id = :old.id;
      --
      DELETE FROM gen_appl_users
            WHERE id = :old.id;
   END IF;
END;

/
ALTER TRIGGER "HELIX"."V_GEN_AUR_AUS_IOIUD" ENABLE;

/
QUIT
