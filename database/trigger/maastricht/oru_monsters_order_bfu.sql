CREATE OR REPLACE TRIGGER "HELIX"."ORU_MONSTERS_ORDER_BFU"
  before update ON HELIX.KGC_MONSTERS
  for each row
declare
     v_StartStr Integer := 0 ;
    v_commentaar varchar2(2000);
    v_str1 varchar2(2000);
    v_OrderNummer varchar2(15);
    -- local variables here
    Cursor c_OMOR(c_mons_id number, C_ordernummer varchar2)
    is
      Select ext_ordernummer
      From  interface.Oru_Monster_Order
      where mons_id = c_mons_id
      and  ext_ordernummer = c_ordernummer;


    v_aanwezig boolean:= false;

begin
   If :new.commentaar is not null Then
      v_startStr := instr(:new.commentaar,'$') ;
      If v_startStr <> 0 Then
         v_str1 := substr(:new.commentaar,instr(:new.commentaar,'$')+1);
         v_OrderNummer  := substr(v_str1,1,instr(v_str1,'$')-1);
         v_commentaar := replace( :new.commentaar,'$'||v_OrderNummer||'$','');
         :new.commentaar := v_commentaar;
      End If;
      If trim(v_OrderNummer) is not null Then
         Open c_Omor(:new.mons_id, v_OrderNummer );
         v_aanwezig := c_omor%Found;
         Close c_Omor;
         If v_aanwezig Then
            null;
         else
            Insert into interface.Oru_Monster_Order(mons_id,ext_ordernummer)
           Values (:new.mons_id, v_OrderNummer);
         End If;
      End If;

   End If;
end ORU_Monsters_order_AFI;

/
ALTER TRIGGER "HELIX"."ORU_MONSTERS_ORDER_BFU" ENABLE;

/
QUIT
