CREATE OR REPLACE TRIGGER "HELIX"."KGC_WERKLIJST_ITEMS_CDM_TRG"
After INSERT
ON HELIX.KGC_WERKLIJST_ITEMS FOR EACH ROW
BEGIN
   IF :new.Ongr_Id in (kgc_ongr_00.id('CYTOM'),kgc_ongr_00.id('CYTOV'))
   Then
      interface.cdm_api.Create_QueueRecord( :new.wlst_id,
                                     :new.meet_id,
                                     :new.mons_id
                                   );
   End If;

END;

/
ALTER TRIGGER "HELIX"."KGC_WERKLIJST_ITEMS_CDM_TRG" ENABLE;

/
QUIT
