CREATE OR REPLACE TRIGGER "HELIX"."BEH_AIR_KGC_MONSTERS"
after insert ON HELIX.KGC_MONSTERS for each row
begin
  if :new.ongr_id = kgc_ongr_00.id('CYTOM')
  and :new.monsternummer like 'PMF%'
  then
    insert into kgc_attribuut_waarden(attr_id, id, waarde)
    values(mss_algemeen.bep_attr_id('KGC_MONSTERS','ANALYSE_LOCATIE'),:new.mons_id,'M');
  else
    null;
  end if;
end;

/
ALTER TRIGGER "HELIX"."BEH_AIR_KGC_MONSTERS" ENABLE;

/
QUIT
