CREATE OR REPLACE TRIGGER "HELIX"."BEH_BIR_DINE"
before insert
ON HELIX.BEH_INVERR_DIAGNOSES for each row
  WHEN (
new.dine_id IS NULL
      ) begin
  :new.dine_id := beh_dine_seq.nextval;
end;

/
ALTER TRIGGER "HELIX"."BEH_BIR_DINE" ENABLE;

/
QUIT
