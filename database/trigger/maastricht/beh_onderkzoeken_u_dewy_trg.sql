CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONDERKZOEKEN_U_DEWY_TRG"
  after update of dewy_id ON HELIX.KGC_ONDERZOEKEN
  for each row
begin

   For R_BON in (  Select BON.DEcl_id
                   From Beh_Onde_Decl BON
                   Where BON.Onde_id = :new.onde_id)
   Loop



      Update kgc_declaraties decl
      set decl.dewy_id = :new.dewy_id
      Where decl.decl_id = R_bon.decl_id;

   End Loop;


end BEH_ONDERKZOEKEN_U_DEWY_TRG;

/
ALTER TRIGGER "HELIX"."BEH_ONDERKZOEKEN_U_DEWY_TRG" ENABLE;

/
QUIT
