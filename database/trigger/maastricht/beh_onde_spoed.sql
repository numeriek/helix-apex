CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONDE_SPOED"
  Before Insert Or Update ON HELIX.KGC_ONDERZOEKEN
  For Each Row
Declare
  Cursor c_ongr(c_ongr_id Number) Is
    Select ongr.code
      From kgc_onderzoeksgroepen ongr
     Where ongr.ongr_id = c_ongr_id;

  /*Cursor c_indi(c_onde_id Number) Is
   Select indi.code
     From kgc_indicatie_teksten indi, kgc_onderzoek_indicaties onin
    Where onin.onde_id = c_onde_id
      And indi.indi_id = onin.indi_id
      And indi.code = '11';
  */
  Cursor c_pers(c_pers_id Number) Is
    Select pers.geboortedatum
      From kgc_personen pers
     Where pers.pers_id = c_pers_id;

  --r_indi c_indi%Rowtype;
  r_ongr c_ongr%Rowtype;
  r_pers c_pers%Rowtype;

Begin
  Open c_ongr(:new.ongr_id);
  Fetch c_ongr
    Into r_ongr;
  Close c_ongr;

  If r_ongr.code In ('FARMA')
  Then
    :new.spoed := 'J';
  End If;

  If r_ongr.code In ('DNA','CYTOM','CYTOV')
  Then
    Open c_pers(:new.pers_id);
    Fetch c_pers
      Into r_pers;
    Close c_pers;

    If r_pers.geboortedatum > (Sysdate - 61)
    Then
      :new.spoed := 'J';
    End If;
  End If;

End;

/
ALTER TRIGGER "HELIX"."BEH_ONDE_SPOED" ENABLE;

/
QUIT
