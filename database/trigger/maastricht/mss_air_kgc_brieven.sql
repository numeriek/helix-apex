CREATE OR REPLACE TRIGGER "HELIX"."MSS_AIR_KGC_BRIEVEN"
after insert
ON HELIX.KGC_BRIEVEN for each row
begin
  --
  -- Tbv de emailnotificatie bij afronden onderzoeken van interne aanvragers
  --
  mss_brie_enotificatie.maak_log_rec( :new.brie_id
                                    , :new.uits_id
                                    , :new.onde_id
                                    , :new.rela_id
                                    , :new.brty_id
                                    );
  --
end mss_air_kgc_brieven;

/
ALTER TRIGGER "HELIX"."MSS_AIR_KGC_BRIEVEN" ENABLE;

/
QUIT
