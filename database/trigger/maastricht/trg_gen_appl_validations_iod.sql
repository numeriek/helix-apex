CREATE OR REPLACE TRIGGER "HELIX"."TRG_GEN_APPL_VALIDATIONS_IOD"
   instead of delete
   ON V_GEN_APPL_VALIDATIONS
   for each row
begin
   delete from gen_appl_validations
         where id = :old.id;
end trg_gen_appl_validations_iod;

/
ALTER TRIGGER "HELIX"."TRG_GEN_APPL_VALIDATIONS_IOD" ENABLE;

/
QUIT
