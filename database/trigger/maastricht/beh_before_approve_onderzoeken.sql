CREATE OR REPLACE TRIGGER "HELIX"."BEH_BEFORE_APPROVE_ONDERZOEKEN"
  before update ON HELIX.KGC_ONDERZOEKEN
  for each row
declare
  Cursor c_uitsH (c_uits_id number)
         Is
           Select max(EPD_versienr) versie
           from   EPD_UITSLAG
           Where  uits_id = c_uits_id;

 Cursor c_EVUT (c_uits_id number, c_versienr number)
  Is
    Select EVUT.*
    From   EPD_UITSLAG EVUT
    Where  EVUT.Uits_Id = c_uits_id
    and    evut.epd_versienr = c_versienr   ;


 Cursor C_uits(c_onde_id number)
 is
  Select uits.*
  from kgc_uitslagen uits
  Where uits.onde_id = c_onde_id;

  Cursor c_rela(c_rela_id number)
   is
   Select aanspreken
   from kgc_relaties
   where rela_id = c_rela_id;



r_rela c_rela%rowType;
r_uitsh c_uitsh%RowType;
r_evut c_evut%RowType;
v_versie number := 1;
E_Error Exception;
v_uitslag clob;
v_uits_gen clob;
v_embargo_Termijn number;
v_error varchar2(1000);

  -- local variables here
begin
  --Insert into EPD_LOG_TEMP (CREATION_DATE,ONDERZOEK,UITSLAG,MELDING,STAP)
  --             values (sysdate, :new.onde_id , 0, v_error , 'TRIGGER_MAAK');
  --    In verband met Kwaliteits eisen worden meerdere versies van Uitslagen Record Bewaard in Historie tabel
  --    KGC_UITSLAGEN_HISTORIE  FUNCTIONALITEIT

    For r_uits in c_uits(:new.onde_id) Loop


       Open c_uitsh(r_uits.uits_id);
            fetch c_uitsh into r_uitsh;
            If r_uitsh.versie is null then
               v_versie := 1;
            Else
               v_versie := r_uitsh.versie + 1;
            End if;
       Close c_uitsh;

       Open c_rela(:new.rela_id);
       fetch c_rela into r_rela;
       Close c_rela;
         Insert into EPD_LOG_TEMP (CREATION_DATE,ONDERZOEK,UITSLAG,MELDING,STAP)
         values (sysdate, :new.onde_id , r_uits.uits_id, v_error , 'BEPAALAUTO');


-- EPD FUNCTIONALITEIT UITSLAGEN --

       If :new.mede_id_autorisator is not null Then
          Insert into EPD_LOG_TEMP (CREATION_DATE,ONDERZOEK,UITSLAG,MELDING,STAP)
               values (sysdate, :new.onde_id , r_uits.uits_id, v_error , 'TRIGGER_AUTH');


         Open c_evut(r_uits.uits_id, r_uitsh.versie );
         Fetch c_evut into r_evut ;

         If c_evut%Found  Then
            Update Epd_uitslag evut
               set evut.epd_status = 'O',
                   evut.epd_status_date = sysdate
            Where evut.epd_uits_id= r_evut.epd_uits_id ;

         End If;
         Close c_evut;


         EPD_LUS_INTERFACE_MAASTRICHT.SetUitslagTekstEPD( r_uits.onde_id,
                                                          :new.ongr_id,
                                                          r_uits.uits_id,
                                                          r_uits.brty_id,
                                                          r_uits.tekst,
                                                          r_uits.toelichting,
                                                          r_uits.kafd_id,
                                                          r_uits.commentaar,
                                                          :new.datum_autorisatie,
                                                          r_rela.aanspreken,
                                                          v_uitslag,
                                                          v_uits_gen,
                                                          v_embargo_Termijn
                                                         );

               Insert into EPD_LOG_TEMP (CREATION_DATE,ONDERZOEK,UITSLAG,MELDING,STAP)
               values (sysdate, :new.onde_id , r_uits.Uits_id, v_error , 'INSERT');

           Insert into Epd_uitslag (  uits_id,
                                                onde_id,
                                                kafd_id,
                                                rela_id,
                                                created_by,
                                                creation_date,
                                                last_updated_by,
                                                last_update_date,
                                                brty_id,
                                                mede_id,
                                                datum_uitslag,
                                                datum_autorisatie,
                                                volledig_adres,
                                                mede_id2,
                                                datum_mede_id,
                                                datum_mede_id2,
                                                mede_id3,
                                                datum_mede_id3,
                                                mede_id4,
                                                datum_mede_id4,
                                                uitslag_gen,
                                                uitslag_alg,
                                                epd_status,
                                                epd_creation_date,
                                                epd_status_date,
                                                epd_versienr,
                                                Embargo_termijn,
                                                epd_uits_id
                                              )
            Values  (                      r_uits.uits_id,
                                           r_uits.onde_id,
                                           r_uits.kafd_id,
                                           r_uits.rela_id,
                                           r_uits.created_by,
                                           r_uits.creation_date,
                                           r_uits.last_updated_by,
                                           r_uits.last_update_date,
                                           r_uits.brty_id,
                                           r_uits.mede_id,
                                           r_uits.datum_uitslag,
                                           r_uits.datum_autorisatie,
                                           r_uits.volledig_adres,
                                           r_uits.mede_id2,
                                           r_uits.datum_mede_id,
                                           r_uits.datum_mede_id2,
                                           r_uits.mede_id3,
                                           r_uits.datum_mede_id3,
                                           r_uits.mede_id4,
                                           r_uits.datum_mede_id4,
                                           v_uits_gen,
                                           v_uitslag,
                                           'N',
                                           sysdate,
                                           sysdate,
                                           v_versie,
                                           v_embargo_termijn,
                                           epd_uits_seq.nextval  )      ;

            --
            -- Begin Mantis 12639
            --
            -- Indien een cardio-onderzoek met uitslag, een MM-onderzoek met uitslag waarbij ook een cardio-onderzoek bestaat
            -- of een Haemochromatose-onderzoek met uitslag waarbij een cardio-onderzoek bestaat, wordt afgerond, dient er
            -- een mail met de uitslag gestuurd te worden.
            --
            -- Triggermoment: het verschijnen van een record in de tabel EPD_UITSLAG met als voorwaarde dat het om een
            --                cardiogenetisch onderzoek (Indicatiegroepcode = CARDIO) gaat.
            -- Normaal gesproken zou de code hiervoor in een after update row tigger op EPD_UITSLAGO geplaatst worden.
            -- Echter, omdat er onderzoekgegevens bij de bepaling nodig zijn is de code nu geplaatst in deze update row trigger
            -- op KGC_ONDERZOEKEN.

            beh_enot_cardio( r_uits.onde_id
                           , r_uits.uits_id
                           , :new.pers_id
                           , r_uits.rela_id
                           , :new.onui_id
                           , v_uits_gen
                           );

            --
            -- Einde Mantis 12639
            --


         Else if :old.mede_id_autorisator is not null and :new.mede_id_autorisator is null then

                Insert into EPD_LOG_TEMP (CREATION_DATE,ONDERZOEK,UITSLAG,MELDING,STAP)
               values (sysdate, :new.onde_id , r_uits.uits_id, v_error , 'DEAUTOR');

                   Open c_evut(r_uits.uits_id, r_uitsh.versie);
                   Fetch c_evut into r_evut ;
                   If c_evut%Found  Then
                    Update Epd_uitslag evut
                       set evut.epd_status = 'O',
                           evut.epd_status_date = sysdate
                     Where evut.epd_uits_id= r_evut.epd_uits_id ;

                   End If;
                   Close c_evut;
             end  if;

         End If;




End Loop;

   Exception when Others Then

          Begin
             v_error := SQLERRM;

              Insert into EPD_LOG_TEMP (CREATION_DATE,ONDERZOEK,UITSLAG,MELDING,STAP)
               values (sysdate, :new.onde_id ,  0 , v_error , 'ERROR_TRIGGER');

          End;


end BEH_BEFORE_Approve_onderzoeken;

/
ALTER TRIGGER "HELIX"."BEH_BEFORE_APPROVE_ONDERZOEKEN" ENABLE;

/
QUIT
