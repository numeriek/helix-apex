CREATE OR REPLACE TRIGGER "HELIX"."BEH_BEFORE_CHANGE_UITSLAGEN"
  before update ON HELIX.KGC_UITSLAGEN
  for each row
declare
  Cursor c_uitsH (c_uits_id number)
         Is
           Select max(EPD_versienr) versie
           from   EPD_UITSLAG
           Where  uits_id = c_uits_id;

  Cursor c_EVUT (c_uits_id number, c_versienr number)
  Is
    Select EVUT.*
    From   EPD_UITSLAG EVUT
    Where  EVUT.Uits_Id = c_uits_id
    and    evut.epd_versienr = c_versienr   ;

  Cursor c_onde(c_onde_id number)
  Is
    Select onde.ongr_id,
           rela.aanspreken,
           Decode( status, 'J'
                 , datum_autorisatie
                 , :new.datum_mede_id4
                 ) datum_autorisatie
    from   kgc_onderzoeken onde,
           kgc_relaties rela
    Where  onde.onde_id = c_onde_id
    and    onde.rela_id = rela.rela_id;

r_onde c_onde%RowType;
r_uits c_uitsh%RowType;
r_evut c_evut%RowType;
v_versie number := 1;
E_Error Exception;
v_uitslag clob;
v_uits_gen clob;
v_embargo_Termijn number;

  -- local variables here
begin

  --    In verband met Kwaliteits eisen worden meerdere versies van Uitslagen Record Bewaard in Historie tabel
  --    KGC_UITSLAGEN_HISTORIE  FUNCTIONALITEIT
    Open c_uitsh(:new.uits_id);
      fetch c_uitsh into r_uits;
         If r_uits.versie is null then
            v_versie := 1;
         Else
            v_versie := r_uits.versie + 1;
         End if;
    Close c_uitsh;


/*  Door Tijds en status registratie in de tabel EPD_uitslag is onderstaande functie overbodig geworden.
    In EPD_uitslag wordt een bevroren uitslag van moment X getoond.
*/

-- EPD FUNCTIONALITEIT UITSLAGEN --

   If :new.Mede_Id4 is not null Then
        Open c_onde(:new.onde_id);
       fetch c_onde into  r_onde;
       close c_onde;

       Open c_evut(:new.uits_id, r_uits.versie);
       Fetch c_evut into r_evut ;

       If c_evut%Found  Then
          Update Epd_uitslag evut
             set evut.epd_status = 'O',
                 evut.epd_status_date = sysdate
           Where evut.epd_uits_id= r_evut.epd_uits_id ;

       End If;
       Close c_evut;


       EPD_LUS_INTERFACE_MAASTRICHT.SetUitslagTekstEPD( :new.onde_id,
                                                        r_onde.ongr_id,
                                                        :new.uits_id,
                                                        :new.brty_id,
                                                        :new.tekst,
                                                        :new.toelichting,
                                                        :new.kafd_id,
                                                        :new.commentaar,
                                                         r_onde.datum_autorisatie,
                                                        r_onde.aanspreken,
                                                        v_uitslag,
                                                        v_uits_gen,
                                                        v_embargo_Termijn
                                                       );



         Insert into Epd_uitslag (  uits_id,
                                              onde_id,
                                              kafd_id,
                                              rela_id,
                                              created_by,
                                              creation_date,
                                              last_updated_by,
                                              last_update_date,
                                              brty_id,
                                              mede_id,
                                              datum_uitslag,
                                              datum_autorisatie,
                                              volledig_adres,
                                              mede_id2,
                                              datum_mede_id,
                                              datum_mede_id2,
                                              mede_id3,
                                              datum_mede_id3,
                                              mede_id4,
                                              datum_mede_id4,
                                              uitslag_gen,
                                              uitslag_alg,
                                              epd_status,
                                              epd_creation_date,
                                              epd_status_date,
                                              epd_versienr,
                                              Embargo_termijn,
                                              epd_uits_id
                                            )
          Values  (                      :new.uits_id,
                                         :new.onde_id,
                                         :new.kafd_id,
                                         :new.rela_id,
                                         :new.created_by,
                                         :new.creation_date,
                                         :new.last_updated_by,
                                         :new.last_update_date,
                                         :new.brty_id,
                                         :new.mede_id,
                                         :new.datum_uitslag,
                                         :new.datum_autorisatie,
                                         :new.volledig_adres,
                                         :new.mede_id2,
                                         :new.datum_mede_id,
                                         :new.datum_mede_id2,
                                         :new.mede_id3,
                                         :new.datum_mede_id3,
                                         :new.mede_id4,
                                         :new.datum_mede_id4,
                                         v_uits_gen,
                                         v_uitslag,
                                         'N',
                                         sysdate,
                                         sysdate,
                                         v_versie,
                                         v_embargo_termijn,
                                         epd_uits_seq.nextval  )      ;

       End If;






end BEH_BEFORE_CHANGE_UITSLAGEN;

/
ALTER TRIGGER "HELIX"."BEH_BEFORE_CHANGE_UITSLAGEN" ENABLE;

/
QUIT
