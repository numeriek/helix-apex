CREATE OR REPLACE TRIGGER "HELIX"."MSS_AIR_KGC_ONDERZOEKEN"
after insert
ON HELIX.KGC_ONDERZOEKEN for each row
begin
      --
    -- Maak bij elk nieuw onderzoek een record aan in KGC_BESTANDEN tbv verwijzing naar de
    -- directory waarin de bij het onderzoek behorende documenten staan (digitaal opslaan documenten).
    --
    mss_cre_best_rec_dig_cyto( :new.onde_id
                             , :new.onderzoeknr
                             );
end mss_air_kgc_onderzoeken;

/
ALTER TRIGGER "HELIX"."MSS_AIR_KGC_ONDERZOEKEN" ENABLE;

/
QUIT
