CREATE OR REPLACE TRIGGER "HELIX"."BEH_FAMI_AIR_TRG"
 AFTER INSERT
 ON HELIX.KGC_FAMILIES
 FOR EACH ROW
BEGIN
  insert into kgc_bestanden ( best_id
                            , entiteit_code
                            , entiteit_pk
                            , btyp_id
                            , bestand_specificatie
                            , volgorde
                            )
  values ( kgc_best_seq.nextval
         , 'FAMI'
         , :new.fami_id
         , 862
         , '\\PRD.CORP\APPDATA\KGLE_archief\'
             || (((to_number (substr(:new.familienummer,1,length(:new.familienummer)-3)))*1000)+1)
             || '-'
             || (((to_number(substr(:new.familienummer,1,length(:new.familienummer)-3))) * 1000)+1000)
             || '\'
             || :new.familienummer
             ||'\'
         , 5
         );
END;

/
ALTER TRIGGER "HELIX"."BEH_FAMI_AIR_TRG" ENABLE;

/
QUIT
