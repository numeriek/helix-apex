CREATE OR REPLACE TRIGGER "HELIX"."TRG_GEN_APPL_VALIDATIONS_IOI"
   instead of insert
   ON V_GEN_APPL_VALIDATIONS
   for each row
begin
   insert into gen_appl_validations(application_id
                                   ,domain_name
                                   ,domain_datatype
                                   ,domain_value_low_n
                                   ,domain_value_high_n
                                   ,domain_value_low_c
                                   ,domain_value_high_c
                                   ,domain_value_low_d
                                   ,domain_value_high_d
                                   ,low_value_included_ind
                                   ,high_value_included_ind
                                   ,active_ind)
        values (:new.application_id
               ,:new.domain_name
               ,:new.domain_datatype
               ,case :new.domain_datatype
                   when 'N' then :new.domain_value_low
                   else ''
                end
               ,case :new.domain_datatype
                   when 'N' then :new.domain_value_high
                   else ''
                end
               ,case :new.domain_datatype
                   when 'C' then :new.domain_value_low
                   else ''
                end
               ,case :new.domain_datatype
                   when 'C' then :new.domain_value_high
                   else ''
                end
               ,case :new.domain_datatype
                   when 'D' then to_date( :new.domain_value_low, 'DD-MM-YYYY')
                   else to_date(null)
                end
               ,case :new.domain_datatype
                   when 'D' then to_date( :new.domain_value_high, 'DD-MM-YYYY')
                   else to_date(null)
                end
               ,:new.low_value_included_ind
               ,:new.high_value_included_ind
               ,:new.active_ind);
end trg_gen_appl_validations_ioi;

/
ALTER TRIGGER "HELIX"."TRG_GEN_APPL_VALIDATIONS_IOI" ENABLE;

/
QUIT
