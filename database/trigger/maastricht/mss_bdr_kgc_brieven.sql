CREATE OR REPLACE TRIGGER "HELIX"."MSS_BDR_KGC_BRIEVEN"
before delete
ON HELIX.KGC_BRIEVEN for each row
begin
  --
  -- Trigger tbv emailnotificatie bij afronden onderzoeken van interne aanvragers
  --
  update mss_brie_enotificatie_log enot
  set    enot.brie_deleted = 'J'
  where  enot.brie_id = :old.brie_id;
  --
end mss_bdr_kgc_brieven;

/
ALTER TRIGGER "HELIX"."MSS_BDR_KGC_BRIEVEN" ENABLE;

/
QUIT
