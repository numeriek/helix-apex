CREATE OR REPLACE TRIGGER "HELIX"."KGC_ONDE_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON KGC_ONDERZOEKEN
 FOR EACH ROW
   WHEN (old.afgerond = 'J' and new.afgerond = 'J') BEGIN
  -- Afdwingen dat geen enkel onderzoeksgegeven gewijzigd is,
  -- tenzij het 'declareren' betreft, EN de gebruiker bevoegd is.
  IF ( kgc_util_00.verschil( :old.onde_id, :new.onde_id )
    OR kgc_util_00.verschil( :old.ongr_id, :new.ongr_id )
    OR kgc_util_00.verschil( :old.kafd_id, :new.kafd_id )
    OR kgc_util_00.verschil( :old.pers_id, :new.pers_id )
    OR kgc_util_00.verschil( :old.onderzoekstype, :new.onderzoekstype )
    OR kgc_util_00.verschil( :old.onderzoeknr, :new.onderzoeknr )
    OR kgc_util_00.verschil( :old.rela_id, :new.rela_id )
    OR kgc_util_00.verschil( :old.spoed, :new.spoed )
    --> declareren:
    OR ( ( kgc_util_00.verschil( :old.declareren, :new.declareren ) )
     AND ( NOT kgc_onde_00.bevoegd ) )
    --< declareren
    OR kgc_util_00.verschil( :old.afgerond, :new.afgerond )
    OR kgc_util_00.verschil( :old.foet_id, :new.foet_id )
    OR kgc_util_00.verschil( :old.datum_binnen, :new.datum_binnen )
    OR kgc_util_00.verschil( :old.geplande_einddatum, :new.geplande_einddatum )
    OR kgc_util_00.verschil( :old.herk_id, :new.herk_id )
    OR kgc_util_00.verschil( :old.inst_id, :new.inst_id )
    OR kgc_util_00.verschil( :old.onderzoekswijze, :new.onderzoekswijze )
    OR kgc_util_00.verschil( :old.status, :new.status )
    OR kgc_util_00.verschil( :old.mede_id_beoordelaar, :new.mede_id_beoordelaar )
    OR kgc_util_00.verschil( :old.mede_id_autorisator, :new.mede_id_autorisator )
    OR kgc_util_00.verschil( :old.mede_id_controle, :new.mede_id_controle )
    OR kgc_util_00.verschil( :old.mede_id_dia_autorisator, :new.mede_id_dia_autorisator )
    OR kgc_util_00.verschil( :old.onui_id, :new.onui_id )
    OR kgc_util_00.verschil( :old.coty_id, :new.coty_id )
    OR kgc_util_00.verschil( :old.diagnose_code, :new.diagnose_code )
    OR kgc_util_00.verschil( :old.ermo_id, :new.ermo_id )
    OR kgc_util_00.verschil( :old.dist_id, :new.dist_id )
    OR kgc_util_00.verschil( :old.herk_id_dia, :new.herk_id_dia )
    OR kgc_util_00.verschil( :old.taal_id, :new.taal_id )
    OR kgc_util_00.verschil( :old.verz_id, :new.verz_id )
    OR kgc_util_00.verschil( :old.proj_id, :new.proj_id )
    OR kgc_util_00.verschil( :old.fare_id, :new.fare_id )
    OR kgc_util_00.verschil( :old.dewy_id, :new.dewy_id )
    OR kgc_util_00.verschil( :old.onre_id, :new.onre_id )
    OR kgc_util_00.verschil( :old.rela_id_oorsprong, :new.rela_id_oorsprong )
    OR kgc_util_00.verschil( :old.zekerheid_diagnose, :new.zekerheid_diagnose )
    OR kgc_util_00.verschil( :old.zekerheid_status, :new.zekerheid_status )
    OR kgc_util_00.verschil( :old.zekerheid_erfmodus, :new.zekerheid_erfmodus )
    OR kgc_util_00.verschil( :old.datum_autorisatie, :new.datum_autorisatie )
    OR kgc_util_00.verschil( :old.kariotypering, :new.kariotypering )
    OR kgc_util_00.verschil( :old.referentie, :new.referentie )
    OR kgc_util_00.verschil( :old.nota_adres, :new.nota_adres )
    OR kgc_util_00.verschil( :old.omschrijving, :new.omschrijving )
    OR kgc_util_00.verschil( :old.pers_id_alt_decl, :new.pers_id_alt_decl )
     )
  THEN
    qms$errors.show_message
      ( p_mesg => 'KGC-00045'
      , p_errtp => 'E'
      , p_rftf => TRUE
      );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_ONDE_AFGEROND_TRG" ENABLE;

/
QUIT
