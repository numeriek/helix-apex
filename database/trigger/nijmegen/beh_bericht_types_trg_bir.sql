CREATE OR REPLACE TRIGGER "HELIX"."BEH_BERICHT_TYPES_TRG_BIR"
before insert ON HELIX.BEH_BERICHT_TYPES for each row
declare
  v_bety_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select beh_bety_seq.nextval into v_bety_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.bety_id       := v_bety_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;

/
ALTER TRIGGER "HELIX"."BEH_BERICHT_TYPES_TRG_BIR" ENABLE;

/
QUIT
