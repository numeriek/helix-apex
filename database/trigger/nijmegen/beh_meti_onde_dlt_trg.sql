CREATE OR REPLACE TRIGGER "HELIX"."BEH_METI_ONDE_DLT_TRG"
FOR INSERT OR DELETE ON HELIX.BAS_METINGEN
COMPOUND TRIGGER

  -- Zet de geplande einddatum van een onderzoek op basis van
  -- de protocollen van een onderzoek.

  type onderzoeklijst is table of number index by pls_integer;

  v_onderzoeken OnderzoekLijst;
  v_onde_id     number;

  before statement is
  begin
    v_onderzoeken.delete;
  end before statement;

  after each row is
  begin
    if INSERTING then
      if :new.onde_id is not null then
        v_onderzoeken(:new.onde_id) := :new.onde_id;
      end if;
    else --DELETING
      if :old.onde_id is not null then
        v_onderzoeken(:old.onde_id) := :old.onde_id;
      end if;
    end if;
  end after each row;

  after statement is
  begin
    v_onde_id := v_onderzoeken.FIRST;
    while v_onde_id is not null loop
      beh_loop_00.zet_einddatum_via_snelheid(v_onde_id);
      v_onde_id := v_onderzoeken.next(v_onde_id);
    end loop;
  end after statement;

END BEH_METI_ONDE_DLT_TRG;
/
ALTER TRIGGER "HELIX"."BEH_METI_ONDE_DLT_TRG" ENABLE;

/
QUIT
