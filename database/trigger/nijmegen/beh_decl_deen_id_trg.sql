CREATE OR REPLACE TRIGGER "HELIX"."BEH_DECL_DEEN_ID_TRG"
 AFTER INSERT OR UPDATE ON HELIX.KGC_DECLARATIES FOR EACH ROW
  WHEN (
    new.deen_id is null and
    old.deen_id is not null and
    upper(new.entiteit) = 'ONDE' and
    upper(new.declareren) = upper('J')
   ) DECLARE

 v_newline VARCHAR2(10) := CHR(10);

 v_afzender VARCHAR2(50) := 'cukz124@umcn.nl';
 v_ontvanger VARCHAR2(50) := 'h.bourchi@sb.umcn.nl';
 v_titel VARCHAR2(100);
 v_boodschap  VARCHAR2(1000);

 v_db_naam VARCHAR2(10);
 v_gebruiker VARCHAR2(50);

 v_afdeling kgc_kgc_afdelingen.code%TYPE;
 v_groep kgc_onderzoeksgroepen.code%TYPE;
 v_onderzoeknr kgc_onderzoeken.onderzoeknr%TYPE;

BEGIN
select sys_context ('USERENV', 'DB_NAME') into v_db_naam from dual;
select sys_context('USERENV', 'SESSION_USER') into v_gebruiker FROM dual;

select code into v_afdeling from kgc_kgc_afdelingen where kafd_id = :new.kafd_id;
select code into v_groep from kgc_onderzoeksgroepen where ongr_id = :new.ongr_id;

select onderzoeknr into v_onderzoeknr from kgc_onderzoeken where onde_id = :new.id;

v_titel := 'DB=' || v_db_naam || ' declaratie zonder deen_id';

v_boodschap := 'Beste,'
    || v_newline
    || v_newline
    || 'Op: ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
    || ' is door ' || v_gebruiker || ' een declaratie zonder deen_id aangemaakt/gewijzigd:'
    || v_newline
    || v_newline
    || ' DataBase: ' || v_db_naam
    || v_newline
    || v_newline
    || ' Afdeling: ' || v_afdeling
    || v_newline
    || ' Groep: ' || v_groep
    || v_newline
    || v_newline
    || ' onderzoeknr: ' || v_onderzoeknr
    || v_newline
    || v_newline
    || ' old deen_id: ' || :old.deen_id
    || v_newline
    || ' new deen_id: ' || :new.deen_id
    || v_newline
    || v_newline
    || ' old status: ' || :old.status
    || v_newline
    || ' new status: ' || :new.status
    || v_newline
    || v_newline
    || 'Stuur maar deze mail door naar afdeling ' || v_afdeling || ', groep ' || v_groep || ' ter controle.'
    || v_newline
    || v_newline
    || 'Helix systeembeheer';
    Helix.Check_jobs.post_mail(v_afzender, v_ontvanger, v_titel, v_boodschap);
END;

/
ALTER TRIGGER "HELIX"."BEH_DECL_DEEN_ID_TRG" ENABLE;

/
QUIT
