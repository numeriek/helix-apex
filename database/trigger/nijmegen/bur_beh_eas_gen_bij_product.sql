CREATE OR REPLACE TRIGGER "HELIX"."BUR_BEH_EAS_GEN_BIJ_PRODUCT"
before update ON BEH_EAS_GEN_bij_product for each row
declare
  v_user varchar2(50);
  v_nu   date;
begin
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.last_updated_by  := v_user;
  :new.last_update_date := v_nu;

end;
/
ALTER TRIGGER "HELIX"."BUR_BEH_EAS_GEN_BIJ_PRODUCT" ENABLE;

/
QUIT
