CREATE OR REPLACE TRIGGER "HELIX"."BEH_OPSL_INLS_TRG"
before insert on helix.beh_opslag_inlees for each row
declare
v_opve_id varchar2(10);
begin
select to_char(beh_opve_seq.nextval) into v_opve_id from dual;

:new.kolom01 := v_opve_id;
end;

/
ALTER TRIGGER "HELIX"."BEH_OPSL_INLS_TRG" ENABLE;

/
QUIT
