  CREATE OR REPLACE TRIGGER "HELIX"."BEH_B_AD_TRG_BUR" 
before update ON BEH_ARCHIEF2DA_DOSSIERS for each row
begin
  :new.last_updated_by    := user;
  :new.last_update_date := sysdate;

end;
/
ALTER TRIGGER "HELIX"."BEH_B_AD_TRG_BUR" ENABLE;

/
QUIT
