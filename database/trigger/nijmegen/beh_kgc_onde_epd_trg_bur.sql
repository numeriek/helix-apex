CREATE OR REPLACE TRIGGER "HELIX"."BEH_KGC_ONDE_EPD_TRG_BUR"
before update ON HELIX.BEH_KGC_ONDERZOEKEN_EPD for each row
declare
  v_user varchar2(50);
  v_nu   date;
begin
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.last_updated_by  := v_user;
  :new.last_update_date := v_nu;

end;

/
ALTER TRIGGER "HELIX"."BEH_KGC_ONDE_EPD_TRG_BUR" ENABLE;

/
QUIT
