CREATE OR REPLACE TRIGGER "HELIX"."BEH_PROCES_UITS_PDF_TRG"
after insert on kgc_onde_autorisatie_historie
for each row
begin
    if (:new.actie = 'D') then
        beh_uits_00.verwijder_uits_bestand(:new.onde_id);
    end if;
end;
/
ALTER TRIGGER "HELIX"."BEH_PROCES_UITS_PDF_TRG" ENABLE;

/
QUIT
