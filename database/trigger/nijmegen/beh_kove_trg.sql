CREATE OR REPLACE TRIGGER "HELIX"."BEH_KOVE_TRG"
before insert ON BEH_KONEMPS_VERWERKT for each row
declare
  v_user varchar2(50);
  v_nu   date;
begin
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
 End;

/
ALTER TRIGGER "HELIX"."BEH_KOVE_TRG" ENABLE;

/
QUIT
