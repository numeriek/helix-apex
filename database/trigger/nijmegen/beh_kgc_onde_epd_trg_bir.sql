CREATE OR REPLACE TRIGGER "HELIX"."BEH_KGC_ONDE_EPD_TRG_BIR"
before insert ON HELIX.BEH_KGC_ONDERZOEKEN_EPD for each row
declare
  v_onep_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select beh_onep_seq.nextval into v_onep_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.onep_id       := v_onep_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;

/
ALTER TRIGGER "HELIX"."BEH_KGC_ONDE_EPD_TRG_BIR" ENABLE;

/
QUIT
