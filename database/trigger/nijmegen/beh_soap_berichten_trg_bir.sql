CREATE OR REPLACE TRIGGER "HELIX"."beh_soap_berichten_TRG_BIR"
before insert ON HELIX.beh_soap_berichten for each row
declare
  --v_sobe_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  --select beh_sobe_seq.nextval into v_sobe_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  --:new.sobe_id       := v_sobe_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;
end;
/
ALTER TRIGGER "HELIX"."beh_soap_berichten_TRG_BIR" ENABLE;

/
QUIT
