CREATE OR REPLACE TRIGGER "HELIX"."BEH_GEN_INST_CODE_TRG"
before insert or update ON HELIX.KGC_INSTELLINGEN for each row

declare

v_afzender VARCHAR2(50) := 'H.Bourchi@sb.umcn.nl';
v_ontvanger VARCHAR2(500) := 'PostbusHelixRelatiecodes@umcn.nl';
--v_ontvanger varchar2(500) := 'h.bourchi@gen.umcn.nl';
v_titel VARCHAR2(500);
v_db_naam VARCHAR2(10) := SYS_CONTEXT ('USERENV', 'DB_NAME');
v_inst_code kgc_instellingen.code%type;

v_html varchar2(32767);

v_changed_color varchar2(20) := '"#F5BCA9"'; --licht rood

v_code_bgcolor varchar2(20) := null;
v_naam_bgcolor varchar2(20) := null;
v_vervallen_bgcolor varchar2(20) := null;
v_adres_bgcolor varchar2(20) := null;
v_postcode_bgcolor varchar2(20) := null;
v_woonplaats_bgcolor varchar2(20) := null;
v_provincie_bgcolor varchar2(20) := null;
v_land_bgcolor varchar2(20) := null;
v_telefoon_bgcolor varchar2(20) := null;
v_telefax_bgcolor varchar2(20) := null;
v_email_bgcolor varchar2(20) := null;

begin
if updating then
    if :new.code != :old.code then
        v_code_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.naam, 'XXXX') != nvl(:old.naam, 'XXXX') then
        v_naam_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if :new.vervallen != :old.vervallen then
        v_vervallen_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.adres, 'XXXX') != nvl(:old.adres, 'XXXX') then
        v_adres_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.postcode, 'XXXX') != nvl(:old.postcode, 'XXXX') then
        v_postcode_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.woonplaats, 'XXXX') != nvl(:old.woonplaats, 'XXXX') then
        v_woonplaats_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.provincie, 'XXXX') != nvl(:old.provincie, 'XXXX') then
        v_provincie_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.land, 'XXXX') != nvl(:old.land, 'XXXX') then
        v_land_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.telefoon, 'XXXX') != nvl(:old.telefoon, 'XXXX') then
        v_telefoon_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.telefax, 'XXXX') != nvl(:old.telefax, 'XXXX') then
        v_telefax_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.email, 'XXXX') != nvl(:old.email, 'XXXX') then
        v_email_bgcolor := ' bgcolor='||v_changed_color;
    end if;
end if;

if inserting then
    select beh_geco_00.gen_code_inst(:new.naam, :new.woonplaats, :new.postcode)
    into v_inst_code
    from dual;

    :new.code := v_inst_code;
end if;

v_html :=
'
<table border="1" align="center">
<tr style="font-weight:bold;"><td align="center">Kolom</td><td align="center">Oude waarde</td><td align="center">Nieuwe waarde</td></tr>
<tr'||v_code_bgcolor||'><td>Code</td><td>'||:old.code|| '</td><td>'||:new.code||'</td></tr>
<tr'||v_naam_bgcolor||'><td>Naam</td><td>'||:old.naam|| '</td><td>'||:new.naam||'</td></tr>
<tr'||v_vervallen_bgcolor||'><td>Vervallen</td><td>'||:old.vervallen|| '</td><td>'||:new.vervallen||'</td></tr>
<tr><td>last_updated_by</td><td>'||:old.last_updated_by|| '</td><td>'||user ||'</td></tr>
<tr><td>last_update_date</td><td>'||to_char(:old.last_update_date, 'dd-mm-yyyy HH24:MI:SS')|| '</td><td>'||to_char(sysdate, 'dd-mm-yyyy HH24:MI:SS')||'</td></tr>
<tr'||v_adres_bgcolor||'><td>Adres</td><td>' ||:old.adres|| '</td><td>'||:new.adres||'</td></tr>
<tr'||v_postcode_bgcolor||'><td>Postcode</td><td>' ||:old.postcode|| '</td><td>'||:new.postcode||'</td></tr>
<tr'||v_woonplaats_bgcolor||'><td>Woonplaats</td><td>' ||:old.woonplaats|| '</td><td>'||:new.woonplaats||'</td></tr>
<tr'||v_provincie_bgcolor||'><td>Provincie</td><td>' ||:old.provincie|| '</td><td>'||:new.provincie||'</td></tr>
<tr'||v_land_bgcolor||'><td>Land</td><td>' ||:old.land|| '</td><td>'||:new.land||'</td></tr>
<tr'||v_telefoon_bgcolor||'><td>Telefoon</td><td>' ||:old.telefoon|| '</td><td>'||:new.telefoon||'</td></tr>
<tr'||v_telefax_bgcolor||'><td>Telefax</td><td>' ||:old.telefax|| '</td><td>'||:new.telefax||'</td></tr>
<tr'||v_email_bgcolor||'><td>Email</td><td>' ||:old.email|| '</td><td>'||:new.email||'</td></tr>
<tr><td>AGB-code</td><td></td><td>'||kgc_attr_00.waarde('KGC_INSTELLINGEN', 'AGB_CODE', :new.inst_id)||'</td></tr>
</table>
<br>
Deze e-mail is automatisch, door Helix trigger: "helix.beh_gen_inst_code_trg" gegenereerd en verzonden.'
;

if upper(v_db_naam) like 'PROD%' then
    v_titel := v_db_naam ||';inst_code=' || :new.code;
    send_mail_html
            (
                p_from => v_afzender,
                p_to   => v_ontvanger,
                p_subject => v_titel,
                p_html => v_html
            );
end if;
end;
/
ALTER TRIGGER "HELIX"."BEH_GEN_INST_CODE_TRG" ENABLE;

/
QUIT
