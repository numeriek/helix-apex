CREATE OR REPLACE TRIGGER "HELIX"."beh_clia_hl7_berichten_TRG_BIR"
before insert ON HELIX.beh_clia_hl7_berichten for each row
declare
  v_user varchar2(50);
  v_nu   date;
begin
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;
end;

/
ALTER TRIGGER "HELIX"."beh_clia_hl7_berichten_TRG_BIR" ENABLE;

/
QUIT
