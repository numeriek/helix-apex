CREATE OR REPLACE TRIGGER "HELIX"."BEH_MOLG_VERW_TRG"
before insert on helix.beh_molgen_inlees for each row
declare
v_mole_id varchar2(10);
begin
select to_char(beh_move_seq.nextval) into v_mole_id from dual;

:new.kolom30 := v_mole_id;
end;

/
ALTER TRIGGER "HELIX"."BEH_MOLG_VERW_TRG" ENABLE;

/
QUIT
