CREATE OR REPLACE TRIGGER "HELIX"."BEH_LOG_IN_GEGEVENS_TRG_BIR"
before insert ON BEH_LOG_IN_GEGEVENS for each row
declare
  v_logi_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select beh_logi_seq.nextval into v_logi_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.logi_id       := v_logi_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;
/
ALTER TRIGGER "HELIX"."BEH_LOG_IN_GEGEVENS_TRG_BIR" ENABLE;

/
QUIT
