CREATE OR REPLACE TRIGGER "HELIX"."BEH_RAADPLEEG_LOG_TRG_BIR"
before insert ON HELIX.BEH_RAADPLEEG_LOG for each row
declare
  v_ralo_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select beh_ralo_seq.nextval into v_ralo_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.ralo_id       := v_ralo_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;

/
ALTER TRIGGER "HELIX"."BEH_RAADPLEEG_LOG_TRG_BIR" ENABLE;

/
QUIT
