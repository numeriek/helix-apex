CREATE OR REPLACE TRIGGER "HELIX"."BEH_WLST_NGS_LIB_TRG"
after insert on kgc_werklijst_items
for each row
declare

v_stof_id number;

cursor c_stof is
select stof.stof_id
from kgc_stoftesten stof, kgc_meetwaardestructuur mwst, kgc_protocol_stoftesten prst, kgc_stoftestgroepen stgr
where STGR.STGR_ID = PRST.STGR_ID
and PRST.STOF_ID = STOF.STOF_ID
and STOF.MWST_ID = MWST.MWST_ID
and STGR.CODE = 'NGS_ST_1'
and MWST.CODE = 'LIB_ST2'
;

begin
-- MGOL 02-10-2014
-- Trigger gemaakt om meetwaarde detail SEQUENCING automatisch te vullen met de datum waarop deze specifieke stoftest op werklijst gezet wordt
-- deze werkwijze geldt alleen voor stoftest in protocol NGS_ST_1 (NGS Library protocol)
-- meetwaarde detail SEQUENCING wordt gevuld met deze systeemdatum
-- meetwaarde detail FACILITEIT wordt gevuld met waarde HUIS (indien meting op NOT '%B' fractie aangemeld is) en met waarde BGI (indien meting op '%B' fractie aangemeld is)
-- zie mantis: ....
/* vervangen door HBOU open fetch vanwege no data found error
select stof.stof_id into v_stof_id
from kgc_stoftesten stof, kgc_meetwaardestructuur mwst, kgc_protocol_stoftesten prst, kgc_stoftestgroepen stgr
where STGR.STGR_ID = PRST.STGR_ID
    and PRST.STOF_ID = STOF.STOF_ID
    and STOF.MWST_ID = MWST.MWST_ID
    and STGR.CODE = 'NGS_ST_1'
    and MWST.CODE = 'LIB_ST2';
*/
open c_stof;
fetch c_stof into v_stof_id;

  if c_stof%found then

    if (:new.stof_id = v_stof_id)
    then
        Update bas_meetwaarde_details
        set WAARDE = sysdate
        where MEET_ID = :new.meet_id
        and UPPER(PROMPT) = 'SEQUENCING';

        Update bas_meetwaarde_details
        set WAARDE = 'BGI'
        where MEET_ID = :new.meet_id
        and UPPER(PROMPT) = 'FACILITEIT'
        and meet_id in (select meet_id
                               from bas_meetwaarden meet, bas_metingen meti, bas_fracties frac
                               where MEET.METI_ID = METI.METI_ID
                                    and METI.FRAC_ID = FRAC.FRAC_ID
                                    and (FRAC.FRACTIENUMMER like '%B' OR FRAC.FRACTIENUMMER like '%B2')
                               )
        ;

        Update bas_meetwaarde_details
        set WAARDE = 'HUIS'
        where MEET_ID = :new.meet_id
        and UPPER(PROMPT) = 'FACILITEIT'
        and meet_id in (select meet_id
                               from bas_meetwaarden meet, bas_metingen meti, bas_fracties frac
                               where MEET.METI_ID = METI.METI_ID
                                    and METI.FRAC_ID = FRAC.FRAC_ID
                                    and FRAC.FRACTIENUMMER not like '%B'
                                    and FRAC.FRACTIENUMMER not like '%B2'
                               )
        ;
    end if;
  end if;
close c_stof;
end;
/
ALTER TRIGGER "HELIX"."BEH_WLST_NGS_LIB_TRG" ENABLE;

/
QUIT
