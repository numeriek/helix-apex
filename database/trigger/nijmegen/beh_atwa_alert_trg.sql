CREATE OR REPLACE TRIGGER "HELIX"."BEH_ATWA_ALERT_TRG"
before insert or delete on helix.kgc_attribuut_waarden for each row


declare
v_afzender varchar2(50) := 'cukz124@umcn.nl';
v_ontvanger varchar2(500) := 'PostbusHelixRelatiecodes@umcn.nl';
v_titel varchar2(200);
v_db_naam VARCHAR2(10) := SYS_CONTEXT ('USERENV', 'DB_NAME');
v_kolom_titel varchar2(100);
v_entiteit_code kgc_instellingen.code%type;

--v_attr_code kgc_attributen.code%type;
v_attr_id kgc_attributen.attr_id%type;
v_waarde kgc_attribuut_waarden.waarde%type;
v_actie varchar2(20);
v_id kgc_attribuut_waarden.id%type;
v_html varchar2(32767);

cursor c_attr (p_attr_id number) is
select *
from kgc_attributen
where attr_id = p_attr_id
;

r_attr c_attr%rowtype;

cursor c_rela (p_rela_id number) is
select *
from kgc_relaties
where rela_id = p_rela_id
;

r_rela c_rela%rowtype;

cursor c_inst (p_inst_id number) is
select *
from kgc_instellingen
where inst_id = p_inst_id
;

r_inst c_inst%rowtype;

begin
if inserting then
    v_attr_id := :new.attr_id;
    v_actie := 'Ingevoerd';
    v_id := :new.id;
    v_waarde := :new.waarde;
end if;

if deleting then
    v_attr_id := :old.attr_id;
    v_actie := 'Verwijderd';
    v_id := :old.id;
    v_waarde := :old.waarde;
end if;

open c_attr(v_attr_id);
fetch c_attr into r_attr;
close c_attr;

--v_attr_code := r_attr.code;

if r_attr.tabel_naam = 'KGC_RELATIES' and
  r_attr.code in ('ZORGMAIL', 'AGB_CODE') then

    open c_rela(v_id);
    fetch c_rela into r_rela;
    close c_rela
    ;

    v_entiteit_code := r_rela.code;

    v_titel := v_db_naam ||';' || r_attr.code ||';rela_code=' || v_entiteit_code;

elsif r_attr.tabel_naam = 'KGC_INSTELLINGEN' and
  r_attr.code = 'AGB_CODE' then

    open c_inst(v_id);
    fetch c_inst into r_inst;
    close c_inst
    ;

    v_entiteit_code := r_inst.code;
    v_titel := v_db_naam ||';' || r_attr.code ||';inst_code=' || v_entiteit_code;
end if;

if r_attr.code in ('ZORGMAIL', 'AGB_CODE') then
  v_html :=
  '
  <table border="1" align="center">
  <tr style="font-weight:bold;"><td align="center">Tabel</td><td align="center">Kolom</td><td align="center">Code</td><td align="center">Actie</td><td align="center">Door</td><td align="center">Waarde</td></tr>
  <tr><td>'|| r_attr.tabel_naam ||'</td><td>'|| r_attr.code ||'</td><td>'||v_entiteit_code ||'</td><td>'||  v_actie ||'</td><td>'||  user ||'</td><td>'|| v_waarde ||'</td></tr>
  </table>
  <br>
  Deze e-mail is automatisch, door Helix trigger: "helix.beh_atwa_alert_trg" gegenereerd en verzonden.'
  ;

  if upper(v_db_naam) like 'PROD%' then
      send_mail_html
              (
                  p_from => v_afzender,
                  p_to   => v_ontvanger,
                  p_subject => v_titel,
                  p_html => v_html
              );
  end if;
end if;

end;
/
ALTER TRIGGER "HELIX"."BEH_ATWA_ALERT_TRG" ENABLE;

/
QUIT
