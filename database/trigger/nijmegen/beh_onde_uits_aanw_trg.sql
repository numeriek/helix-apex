CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONDE_UITS_AANW_TRG"
BEFORE INSERT OR UPDATE OF AFGEROND ON KGC_ONDERZOEKEN FOR EACH ROW
 WHEN (old.afgerond = 'N' and new.afgerond = 'J') DECLARE
  -----
  -- Trigger gemaakt voor Mantis melding:
  -- 0010518: een onderzoek kan geautoriseerd worden, zonder dat er een brief is.
  -----

  v_kafd_id_metab          kgc_kgc_afdelingen.kafd_id%type;          --kafd_id van adfeling METAB
  v_onui_id_geen_uitslag   kgc_onderzoek_uitslagcodes.onui_id%type;  --onui_id van onderzoeks uitslag 'Geen Uislag'
  v_brty_id_eindbrief      kgc_brieftypes.brty_id%type;              --brty_id van eindbrief.
  v_count_uitslagen        integer;

  v_missende_uitslag       boolean := true;
  v_bericht_tekst          varchar2(1000);

  CURSOR c_uitslagen_zonder_tekst(p_kafd_id_metab kgc_kgc_afdelingen.kafd_id%type, p_onde_id kgc_onderzoeken.onde_id%type) IS
  select brty.code
       , count(*) as count_brief
       , sum(decode(uits.tekst, null, 0, 1)) as count_niet_leeg
  from   kgc_uitslagen uits join kgc_brieftypes brty on uits.brty_id = brty.brty_id
  where  uits.kafd_id = p_kafd_id_metab
    and  uits.onde_id = p_onde_id
    and  brty.code in ('EINDBRIEF', 'CONTROLE', 'EIND_AANV')
  group by brty.code;

  r_uitslagen_zonder_tekst c_uitslagen_zonder_tekst%rowtype;

BEGIN
  -- Bij het afsluiten van een onderzoek (kgc_onderzoeken) (scherm Onderzoek) voor METAB
  -- moet er minstens 1 uitslag record zijn.

  v_kafd_id_metab  := kgc_uk2pk.kafd_id('METAB');

  -- Wat is het onui_id van uitslagcode 04 (Geen uitslag)?
  select onui_id
  into   v_onui_id_geen_uitslag
  from   kgc_onderzoek_uitslagcodes
  where  kafd_id = v_kafd_id_metab
    and  vervallen = 'N'
    and  code = '04';

  IF ( :new.kafd_id          = v_kafd_id_metab
   and nvl(:new.onui_id, 0) <> v_onui_id_geen_uitslag
     )
  THEN

    open c_uitslagen_zonder_tekst(v_kafd_id_metab, :new.onde_id);
    fetch c_uitslagen_zonder_tekst into r_uitslagen_zonder_tekst;

    if c_uitslagen_zonder_tekst%found then
      -- Als het onderzoek de uislagen heeft die we verwachen is het in principe OK, mits de uitslagen niet leeg zijn.
      v_missende_uitslag := false;

      while c_uitslagen_zonder_tekst%found loop
        if r_uitslagen_zonder_tekst.count_brief <> r_uitslagen_zonder_tekst.count_niet_leeg then

          -- E�n van de uitslagen is leeg.
          v_missende_uitslag := true;
          if r_uitslagen_zonder_tekst.count_brief = 1 then
            v_bericht_tekst := v_bericht_tekst || 'De uitslag van brief ' || r_uitslagen_zonder_tekst.code
                                               || ' is leeg. ';
          else
            v_bericht_tekst := v_bericht_tekst || 'Van de ' || to_char(r_uitslagen_zonder_tekst.count_brief)
                                               || ' uitslagen met brieftype ' || r_uitslagen_zonder_tekst.code
                                               || ' zijn er ' || (r_uitslagen_zonder_tekst.count_brief-r_uitslagen_zonder_tekst.count_niet_leeg)
                                               || ' leeg. ';
          end if;

        end if;
        fetch c_uitslagen_zonder_tekst into r_uitslagen_zonder_tekst;
      end loop;
    end if;

    if v_missende_uitslag then
      qms$errors.show_message( p_mesg   => 'BEH-00001'
                             , p_param0 => v_bericht_tekst
                             );
    end if;

  END IF;

END;
/
ALTER TRIGGER "HELIX"."BEH_ONDE_UITS_AANW_TRG" ENABLE;

/
QUIT
