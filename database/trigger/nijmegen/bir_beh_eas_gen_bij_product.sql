CREATE OR REPLACE TRIGGER "HELIX"."BIR_BEH_EAS_GEN_BIJ_PRODUCT"
before insert ON BEH_EAS_GEN_bij_product for each row
declare
  v_gepr_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select  beh_gepr_SEQ.nextval into v_gepr_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.gepr_id       := v_gepr_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;
/
ALTER TRIGGER "HELIX"."BIR_BEH_EAS_GEN_BIJ_PRODUCT" ENABLE;

/
QUIT
