CREATE OR REPLACE TRIGGER "HELIX"."BEH_BEST_BIJLAGE_PAD_TRG"
before insert or update on kgc_bestanden for each row
declare

cursor c_btyp is
select *
from   kgc_bestand_types
where  btyp_id = :new.btyp_id
;

r_btyp c_btyp%rowtype;

cursor c_mobe is
select     regexp_replace(upper(:new.bestand_specificatie), '[A-Z]:', mobe.standaard_pad) bestand_specificatie
,        regexp_replace(upper(:new.locatie), '[A-Z]:', mobe.standaard_pad) locatie
from  kgc_onderzoeken onde
,     kgc_uitslagen uits
,     kgc_bestand_types btyp
,     kgc_model_bestanden mobe
where onde.onde_id = uits.onde_id
and   uits.uits_id = :new.entiteit_pk
and   :new.btyp_id = btyp.btyp_id
and   btyp.btyp_id = mobe.btyp_id
and   mobe.kafd_id = onde.kafd_id
and   btyp.code like 'BIJLAGE%'
;

r_mobe c_mobe%rowtype;

begin
open c_btyp;
fetch c_btyp into r_btyp;
close c_btyp;

if r_btyp.code like 'BIJLAGE%' then
    open c_mobe;
    fetch c_mobe into r_mobe;
    close c_mobe;

    :new.bestand_specificatie := r_mobe.bestand_specificatie;
    :new.locatie := r_mobe.locatie;
end if;
end;
/
ALTER TRIGGER "HELIX"."BEH_BEST_BIJLAGE_PAD_TRG" ENABLE;

/
QUIT
