CREATE OR REPLACE TRIGGER "HELIX"."BEH_RELA_CHECK_TRG"
after insert or update on kgc_relaties for each row
 WHEN (user != 'HELIX') declare
e_liszcode_verplicht exception;
e_liszcode_formaat_error exception;

begin
if :new.liszcode is null then
	if (upper(:new.adres) like '%ALHIER%' or :new.relatie_type ='IN') then	
		raise e_liszcode_verplicht;
	end if;
else
    if (upper(:new.liszcode) not like 'Z%' or
		length(trim(:new.liszcode)) != 7 or
		beh_alfu_00.is_nummer(substr(:new.liszcode, 2)) = 'N') then
		raise e_liszcode_formaat_error;		
	end if;	
end if;
exception
	when e_liszcode_verplicht then
	    raise_application_error(-20000, SQLERRM || ' => '|| 'Liszcode (znr) bij een interne relatie is verplicht.');
	when e_liszcode_formaat_error then
	    raise_application_error(-20000, SQLERRM || ' => '|| 'Verkeerde waarde ('|| :new.liszcode||') voor liszcode (znr) is ingevoerd.');
	when others then
		raise;
end;
/
ALTER TRIGGER "HELIX"."BEH_RELA_CHECK_TRG" ENABLE;

/
QUIT
