CREATE OR REPLACE TRIGGER "HELIX"."BEH_GEN_RELA_CODE_TRG"
before insert or update ON HELIX.KGC_RELATIES for each row
declare
v_afzender varchar2(50) := 'cukz124@umcn.nl';
v_ontvanger varchar2(500) := 'PostbusHelixRelatiecodes@umcn.nl';
v_titel varchar2(200);
v_db_naam VARCHAR2(10) := SYS_CONTEXT ('USERENV', 'DB_NAME');
v_adres kgc_relaties.adres%type;
v_rela_code kgc_relaties.code%type;
v_agb_code varchar(2500);
v_bevoegd varchar2(20);
v_html varchar2(32767);

v_spec_code_old kgc_afdelingen.code%type := null;
v_spec_code_new kgc_afdelingen.code%type;

v_inst_code_old kgc_instellingen.code%type := null;
v_inst_code_new kgc_instellingen.code%type;

v_afdg_code_old kgc_afdelingen.code%type := null;
v_afdg_code_new kgc_afdelingen.code%type;

v_changed_color varchar2(20) := '"#F5BCA9"'; --licht rood

v_code_bgcolor varchar2(20) := null;
v_achternaam_bgcolor varchar2(20) := null;
v_vervallen_bgcolor varchar2(20) := null;
v_aanspreken_bgcolor varchar2(20) := null;
v_relatie_type_bgcolor varchar2(20) := null;
v_spec_id_bgcolor varchar2(20) := null;
v_inst_id_bgcolor varchar2(20) := null;
v_afdg_id_bgcolor varchar2(20) := null;
v_briefaanhef_bgcolor varchar2(20) := null;
v_voorletters_bgcolor varchar2(20) := null;
v_voorvoegsel_bgcolor varchar2(20) := null;
v_adres_bgcolor varchar2(20) := null;
v_postcode_bgcolor varchar2(20) := null;
v_woonplaats_bgcolor varchar2(20) := null;
v_provincie_bgcolor varchar2(20) := null;
v_land_bgcolor varchar2(20) := null;
v_telefoon_bgcolor varchar2(20) := null;
v_telefax_bgcolor varchar2(20) := null;
v_email_bgcolor varchar2(20) := null;
v_liszcode_bgcolor varchar2(20) := null;
v_interne_post_bgcolor varchar2(20) := null;
v_attr_id NUMBER;

-- wordt relaties door een bevoegde gebruiker ingevoerd/gewijzigd
-- of niet? bevoegd zijn mensen met een record in kgc_module_toegang
-- onbevoegden zijn gebruikers die relaties ophalen vanuit ROCs (door het
-- ophalen van patient)
cursor c_moto is
select count(*) aantal
from  kgc_module_toegang
where mede_id = (select mede_id from kgc_medewerkers where oracle_uid = user)
and module = 'KGCRELA01'
;

r_moto c_moto%rowtype;

cursor c_spec(p_spec_id number) is
select *
from kgc_specialismen
where spec_id = p_spec_id
;
r_spec c_spec%rowtype;

cursor c_inst(p_inst_id number) is
select *
from kgc_instellingen
where inst_id = p_inst_id
;
r_inst c_inst%rowtype;

cursor c_afdg(p_afdg_id number) is
select *
from kgc_afdelingen
where afdg_id = p_afdg_id
;
r_afdg c_afdg%rowtype;

begin
if updating then
    if :new.code != :old.code then
        v_code_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.achternaam, 'XXXX') != nvl(:old.achternaam, 'XXXX') then
        v_achternaam_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if :new.vervallen != :old.vervallen then
        v_vervallen_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.aanspreken, 'XXXX') != nvl(:old.aanspreken, 'XXXX') then
        v_aanspreken_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.relatie_type, 'XXXX') != nvl(:old.relatie_type, 'XXXX') then
        v_relatie_type_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.spec_id, -1) != nvl(:old.spec_id, -1) then
        v_spec_id_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.inst_id, -1) != nvl(:old.inst_id, -1) then
        v_inst_id_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.afdg_id, -1) != nvl(:old.afdg_id, -1) then
        v_afdg_id_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.briefaanhef, 'XXXX') != nvl(:old.briefaanhef, 'XXXX') then
        v_briefaanhef_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.voorletters, 'XXXX') != nvl(:old.voorletters, 'XXXX') then
        v_voorletters_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.voorvoegsel, 'XXXX') != nvl(:old.voorvoegsel, 'XXXX') then
        v_voorvoegsel_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.adres, 'XXXX') != nvl(:old.adres, 'XXXX') then
        v_adres_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.postcode, 'XXXX') != nvl(:old.postcode, 'XXXX') then
        v_postcode_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.woonplaats, 'XXXX') != nvl(:old.woonplaats, 'XXXX') then
        v_woonplaats_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.provincie, 'XXXX') != nvl(:old.provincie, 'XXXX') then
        v_provincie_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.land, 'XXXX') != nvl(:old.land, 'XXXX') then
        v_land_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.telefoon, 'XXXX') != nvl(:old.telefoon, 'XXXX') then
        v_telefoon_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.telefax, 'XXXX') != nvl(:old.telefax, 'XXXX') then
        v_telefax_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.email, 'XXXX') != nvl(:old.email, 'XXXX') then
        v_email_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.liszcode, 'XXXX') != nvl(:old.liszcode, 'XXXX') then
        v_liszcode_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    if nvl(:new.interne_post, 'XXXX') != nvl(:old.interne_post, 'XXXX') then
        v_interne_post_bgcolor := ' bgcolor='||v_changed_color;
    end if;

    open c_spec (:old.spec_id);
    fetch c_spec into r_spec;
    close c_spec;

    v_spec_code_old := r_spec.code;

    open c_inst (:old.inst_id);
    fetch c_inst into r_inst;
    close c_inst;

    v_inst_code_old := r_inst.code;

    open c_afdg (:old.afdg_id);
    fetch c_afdg into r_afdg;
    close c_afdg;

    v_afdg_code_old := r_afdg.code;

end if;

open c_spec (:new.spec_id);
fetch c_spec into r_spec;
close c_spec;

v_spec_code_new := r_spec.code;

open c_inst (:new.inst_id);
fetch c_inst into r_inst;
close c_inst;

v_inst_code_new := r_inst.code;

open c_afdg (:new.afdg_id);
fetch c_afdg into r_afdg;
close c_afdg;

v_afdg_code_new := r_afdg.code;

open c_moto;
fetch c_moto into r_moto;
close c_moto;

if r_moto.aantal > 0 then
    v_bevoegd := ' (bevoegd)';
else
    v_bevoegd := ' (onbevoegd)';
end if;

v_agb_code :=
        'Rela=' ||
        kgc_attr_00.waarde('KGC_RELATIES', 'AGB_CODE', :new.rela_id) ||
        ' Inst=' || kgc_attr_00.waarde('KGC_INSTELLINGEN', 'AGB_CODE', :new.inst_id)
        ;


if inserting then
    --weghalen van enter, tab, new line
    v_adres := replace(replace(replace(trim(upper(:new.adres)), chr(10)), chr(13)), chr(9) );

    select beh_geco_00.gen_code_rela
    (
    :new.achternaam,
    :new.voorletters,
    decode(v_adres, 'ALHIER', 'NYMEGEN', :new.woonplaats),
    decode(v_adres, 'ALHIER', '6500 HB', :new.postcode),
    decode(v_adres, 'ALHIER', 'NEDERLAND', :new.land)
    )
    into v_rela_code
    from dual
    ;
    :new.code := v_rela_code;
    -- begin rde 20-08-2013: igv huisartsen: liszcode overzetten naar extra attribuut SER_ID
    IF :new.relatie_type = 'HA' AND :new.liszcode IS NOT NULL THEN
      SELECT attr_id
      INTO   v_attr_id
      FROM   kgc_attributen attr
      WHERE  attr.tabel_naam = 'KGC_RELATIES'
      AND    attr.code = 'SER_ID';
      INSERT INTO kgc_attribuut_waarden atwa (attr_id, id, waarde) VALUES (v_attr_id, :new.rela_id, :new.liszcode);
      :new.liszcode := NULL;
    END IF;
    -- eind rde 20-08-2013
end if;

v_html :=
'
<table border="1" align="center">
<tr style="font-weight:bold;"><td align="center">Kolom</td><td align="center">Oude waarde</td><td align="center">Nieuwe waarde</td></tr>
<tr'||v_code_bgcolor||'><td>Code</td><td>'||:old.code|| '</td><td>'||:new.code||'</td></tr>
<tr'||v_achternaam_bgcolor||'><td>Achternaam</td><td>'||:old.achternaam|| '</td><td>'||:new.achternaam||'</td></tr>
<tr'||v_vervallen_bgcolor||'><td>Vervallen</td><td>'||:old.vervallen|| '</td><td>'||:new.vervallen||'</td></tr>
<tr><td>last_updated_by</td><td>'||:old.last_updated_by|| '</td><td>'||user|| v_bevoegd ||'</td></tr>
<tr><td>last_update_date</td><td>'||to_char(:old.last_update_date, 'dd-mm-yyyy HH24:MI:SS')|| '</td><td>'||to_char(sysdate, 'dd-mm-yyyy HH24:MI:SS')||'</td></tr>
<tr'||v_aanspreken_bgcolor||'><td>Aanspreken</td><td>'||:old.aanspreken|| '</td><td>'||:new.aanspreken||'</td></tr>
<tr'||v_relatie_type_bgcolor||'><td>Relatie_type</td><td>'||:old.relatie_type|| '</td><td>'||:new.relatie_type||'</td></tr>
<tr'||v_spec_id_bgcolor||'><td>Specialisme</td><td>' ||v_spec_code_old|| '</td><td>'||v_spec_code_new||'</td></tr>
<tr'||v_inst_id_bgcolor||'><td>Instelling</td><td>' ||v_inst_code_old|| '</td><td>'||v_inst_code_new||'</td></tr>
<tr'||v_afdg_id_bgcolor||'><td>Afdeling</td><td>' ||v_afdg_code_old|| '</td><td>'||v_afdg_code_new||'</td></tr>
<tr'||v_briefaanhef_bgcolor||'><td>Briefaanhef</td><td>' ||:old.briefaanhef|| '</td><td>'||:new.briefaanhef||'</td></tr>
<tr'||v_voorletters_bgcolor||'><td>Voorletters</td><td>' ||:old.voorletters|| '</td><td>'||:new.voorletters||'</td></tr>
<tr'||v_voorvoegsel_bgcolor||'><td>Voorvoegsel</td><td>' ||:old.voorvoegsel|| '</td><td>'||:new.voorvoegsel||'</td></tr>
<tr'||v_adres_bgcolor||'><td>Adres</td><td>' ||:old.adres|| '</td><td>'||:new.adres||'</td></tr>
<tr'||v_postcode_bgcolor||'><td>Postcode</td><td>' ||:old.postcode|| '</td><td>'||:new.postcode||'</td></tr>
<tr'||v_woonplaats_bgcolor||'><td>Woonplaats</td><td>' ||:old.woonplaats|| '</td><td>'||:new.woonplaats||'</td></tr>
<tr'||v_provincie_bgcolor||'><td>Provincie</td><td>' ||:old.provincie|| '</td><td>'||:new.provincie||'</td></tr>
<tr'||v_land_bgcolor||'><td>Land</td><td>' ||:old.land|| '</td><td>'||:new.land||'</td></tr>
<tr'||v_telefoon_bgcolor||'><td>Telefoon</td><td>' ||:old.telefoon|| '</td><td>'||:new.telefoon||'</td></tr>
<tr'||v_telefax_bgcolor||'><td>Telefax</td><td>' ||:old.telefax|| '</td><td>'||:new.telefax||'</td></tr>
<tr'||v_email_bgcolor||'><td>Email</td><td>' ||:old.email|| '</td><td>'||:new.email||'</td></tr>
<tr'||v_liszcode_bgcolor||'><td>Liszcode</td><td>' ||:old.liszcode|| '</td><td>'||:new.liszcode||'</td></tr>
<tr'||v_interne_post_bgcolor||'><td>Interne_post</td><td>' ||:old.interne_post|| '</td><td>'||:new.interne_post||'</td></tr>
<tr><td>AGB-code</td><td></td><td>'|| v_agb_code ||'</td></tr>
</table>
<br>
Deze e-mail is automatisch, door Helix trigger: "helix.beh_gen_rela_code_trg" gegenereerd en verzonden.'
;

if upper(v_db_naam) like 'PROD%' then
    v_titel := v_db_naam ||';rela_code=' || :new.code;
    send_mail_html
            (
                p_from => v_afzender,
                p_to   => v_ontvanger,
                p_subject => v_titel,
                p_html => v_html
            );
end if;
end;
/
ALTER TRIGGER "HELIX"."BEH_GEN_RELA_CODE_TRG" ENABLE;

/
QUIT
