CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONCO_LOG_TRG_AIS"
AFTER INSERT
ON HELIX.BEH_ONCO_LOG
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW


DECLARE
   v_subject   VARCHAR2 (1000);
   v_message   VARCHAR2 (2500);
   v_newline   VARCHAR2 (10) := CHR (10);
BEGIN
   v_subject :=
         'pers_id => '
      || :new.cust_ExternalID
      || ' cust_customerid => '
      || TO_CHAR (:new.cust_customerid);

   v_message :=
         'Beste, '
      || v_newline
      || v_newline
      || 'Op: '
      || :new.datum_log
      || v_newline
      || 'Het overzetten van records van Pedigree db naar Helix db '
      || v_newline
      || 'is, met onderstaande foutmelding, onderbroken: '
      || v_newline
      || v_newline
      || :new.sql_error
      || v_newline
      || v_newline
      || 'Deze email is automatisch gegenereerd door trigger:'
      || v_newline
      || 'beh_onco_log_trg_ais';
 /*
UTL_MAIL.SEND(  sender => 'HelpdeskHelixCUKZ@cukz.umcn.nl',
                recipients => 'h.bourchi@sb.umcn.nl', -- komma gescheiden
                cc => null,
                bcc => null,
                subject => v_subject,
                message => v_message
             );
 */
END;
/
ALTER TRIGGER "HELIX"."BEH_ONCO_LOG_TRG_AIS" ENABLE;

/
QUIT
