CREATE OR REPLACE TRIGGER "HELIX"."BEH_FLOW_ONDE_DATA_TRG_BIR"
before insert ON HELIX.beh_flow_onde_data for each row
declare
  v_flod_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select beh_flod_seq.nextval into v_flod_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.flod_id       := v_flod_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;

/
ALTER TRIGGER "HELIX"."BEH_FLOW_ONDE_DATA_TRG_BIR" ENABLE;

/
QUIT
