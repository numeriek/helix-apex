CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONDE_DATUM_BINNEN_TRG"
 AFTER INSERT OR UPDATE ON HELIX.KGC_ONDERZOEKEN FOR EACH ROW
DECLARE

 v_newline VARCHAR2(10) := CHR(10);

 v_afzender VARCHAR2(50) := 'cukz124@umcn.nl';
 v_ontvanger VARCHAR2(50) := 'w.heys@cukz.umcn.nl';
-- v_ontvanger VARCHAR2(50) := 'h.bourchi@sb.umcn.nl';
 v_titel VARCHAR2(100);
 v_boodschap  VARCHAR2(1000);

 v_db_naam VARCHAR2(10);
 v_gebruiker VARCHAR2(50);

 v_afdeling kgc_kgc_afdelingen.code%TYPE;
 v_groep kgc_onderzoeksgroepen.code%TYPE;

 v_zisnr kgc_personen.zisnr%TYPE;
 v_aanspreken kgc_personen.aanspreken%TYPE;
 v_geboortedatum kgc_personen.geboortedatum%TYPE;
 v_overlijdensdatum kgc_personen.overlijdensdatum%TYPE;

 v_nota_adres_count number;

BEGIN
select sys_context ('USERENV', 'DB_NAME') into v_db_naam from dual;
select sys_context('USERENV', 'SESSION_USER') into v_gebruiker FROM dual;

select code into v_afdeling from kgc_kgc_afdelingen where kafd_id = :new.kafd_id;
select code into v_groep from kgc_onderzoeksgroepen where ongr_id = :new.ongr_id;

select  zisnr, aanspreken, geboortedatum, overlijdensdatum
        into v_zisnr, v_aanspreken, v_geboortedatum, v_overlijdensdatum
        from kgc_personen
        where pers_id = :new.pers_id;
select count(*) into v_nota_adres_count from kgc_personen where zisnr = trim(:new.nota_adres);

if  (
    v_overlijdensdatum is not null and
    :new.datum_binnen is not null and
    :new.datum_binnen <> :old.datum_binnen and
    v_overlijdensdatum < :new.datum_binnen and
    upper(v_afdeling) = upper('METAB') and
    v_nota_adres_count < 1 and
    upper(:new.declareren) = upper('J')
    ) then

v_titel := 'DB=' || v_db_naam || ' datum_binnen groter dan overlijdensdatum';
v_boodschap := 'Beste,'
    || v_newline
    || v_newline
    || 'Op: ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
    || ' is door ' || v_gebruiker || ' een onderzoek met datum binnen groter dan overlijdensdatum van patient ingevoerd/gewijzigd.'
    || ' Alt. nota adres is ook niet (goed) ingevoerd: '
    || v_newline
    || v_newline
    || ' DataBase: ' || v_db_naam
    || v_newline
    || v_newline
    || ' Afdeling: ' || v_afdeling
    || v_newline
    || ' Groep: ' || v_groep
    || v_newline
    || v_newline
    || ' onderzoeknr: ' || :new.onderzoeknr
    || v_newline
    || ' datum_binnen onderzoek: ' || TO_CHAR(:new.datum_binnen, 'DD-MON-YYYY')
    || v_newline
    || ' Alt. nota adres: ' || trim(:new.nota_adres)
    || v_newline
    || v_newline
    || ' ZirNr: ' || v_zisnr
    || v_newline
    || ' Patient: ' || v_aanspreken
    || v_newline
    || ' geboortedatum: ' || TO_CHAR(v_geboortedatum, 'DD-MON-YYYY')
    || v_newline
    || ' overlijdensdatum: ' || TO_CHAR(v_overlijdensdatum, 'DD-MON-YYYY')
    || v_newline
    || v_newline
    || 'Stuur maar deze mail door naar afdeling ' || v_afdeling || ', groep ' || v_groep || ' ter controle.'
    || v_newline
    || v_newline
    || 'Helix systeembeheer';
    Helix.Check_jobs.post_mail(v_afzender, v_ontvanger, v_titel, v_boodschap);
end if;
END;

/
ALTER TRIGGER "HELIX"."BEH_ONDE_DATUM_BINNEN_TRG" ENABLE;

/
QUIT
