CREATE OR REPLACE TRIGGER "HELIX"."beh_flow_details_TRG_BUR"
before update ON HELIX.beh_flow_details for each row
declare
  v_user varchar2(50);
  v_nu   date;
begin
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;
end;
/
ALTER TRIGGER "HELIX"."beh_flow_details_TRG_BUR" ENABLE;

/
QUIT
