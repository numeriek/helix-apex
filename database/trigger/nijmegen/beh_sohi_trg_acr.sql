CREATE OR REPLACE TRIGGER "HELIX"."BEH_SOHI_TRG_ACR"
AFTER CREATE ON HELIX.SCHEMA
-- zie ook mantisnr 8293
begin
 beh_sohi_00.log_object(ora_dict_obj_name, ora_dict_obj_type);
exception
when others then
    raise_application_error(-20000, SQLERRM);
end;
/
ALTER TRIGGER "HELIX"."BEH_SOHI_TRG_ACR" ENABLE;

/
QUIT
