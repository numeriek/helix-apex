CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONIN_INDI_MAASTRICHT_TRG"
AFTER INSERT ON KGC_ONDERZOEK_INDICATIES
DECLARE
    idx      BINARY_INTEGER := cg$KGC_ONDERZOEK_INDICATIES.cg$table.FIRST;
    cg$rec   cg$KGC_ONDERZOEK_INDICATIES.cg$row_type;
    cg$old_rec   cg$KGC_ONDERZOEK_INDICATIES.cg$row_type;
    fk_check INTEGER;
BEGIN

  DECLARE

    cursor c_indi (b_onde_id in number
                  ,b_indi_codes in varchar2)
    is
    select indi.indi_code, onde.onderzoeknr
    from KGC_ONDERZOEK_INDICATIES_VW indi, kgc_onderzoeken onde
    where  instr(b_indi_codes, ''''||indi.indi_code||'''') > 0
    and    indi.onde_id = onde.onde_id
    and    indi.onde_id = b_onde_id
    ;

    v_afzender VARCHAR2(50) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
    v_ontvanger VARCHAR2(100) := 'yuri.arts@radboudumc.nl, Cootje.Vermaat@radboudumc.nl';
    v_indi_codes VARCHAR2(2000);

    v_titel VARCHAR2(1000);
    v_boodschap  VARCHAR2(2000);
    v_indi_code kgc_indicatie_teksten.code%type;
    v_onderzoeknr kgc_onderzoeken.onderzoeknr%type;

    -- voor beh_berichten
    v_bety_id beh_bericht_types.bety_id%type;
    v_datum_verzonden date := sysdate;
    v_oracle_uid kgc_medewerkers.oracle_uid%type;


    i BINARY_INTEGER := cg$KGC_ONDERZOEK_INDICATIES.cg$table.FIRST;

    /***********************************************************************
    Auteur/bedrijf  : Yuri Arts, YA IT-Services
    Datum creatie   : 23-06-2015

    BESCHRIJVING
    Zodra een onderzoek met een bepaalde indicatie wordt aangemaakt, zoals in de
    systeemparameter NOTIFICATIE_MUMC_INDI_CODES staat, dan wordt een email
    verstuurd naar bepaalde mensen in Maastricht. Deze email adressen staan in
    de systeemparameter NOTIFICATIE_MUMC_ONIN_ONTVANGE.
    De verzender staat in de systeemparameter NOTIFICATIE_MUMC_ONIN_AFZENDER.

    Naast het versturen van een email wordt een record aangemaakt in de
    BEH_BERICHTEN tabel.

    HISTORIE

    Wanneer /      Wie             Wat
    Versie
    ------------------------------------------------------------------------
    23-06-2015     Y.Arts          Creatie
    1.0.0
    ***********************************************************************/

  BEGIN

    v_afzender := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'NOTIFICATIE_MUMC_ONIN_AFZENDER'
                     , p_kafd_id        => null);
    v_ontvanger := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'NOTIFICATIE_MUMC_ONIN_ONTVANGE'
                     , p_kafd_id        => null);
    v_indi_codes := kgc_sypa_00.standaard_waarde
                     ( p_parameter_code => 'NOTIFICATIE_MUMC_INDI_CODES'
                     , p_kafd_id        => null);

    select bety_id into v_bety_id
    from beh_bericht_types
    where code = 'EMAIL'
    and upper(vervallen) = upper('N')
    ;

    select sys_context('USERENV', 'SESSION_USER')
    into v_oracle_uid
    from dual
    ;

    WHILE i IS NOT NULL
    LOOP

      open  c_indi(b_onde_id => cg$KGC_ONDERZOEK_INDICATIES.cg$TABLE(i).onde_id
                  ,b_indi_codes => v_indi_codes);
      fetch c_indi into v_indi_code, v_onderzoeknr;
      close c_indi;

      v_titel := 'Onderzoek '||v_onderzoeknr ||' is in Radboud UMC aangemaakt met indicatie '||v_indi_code;
      v_boodschap := 'In het Radboud UMC is een onderzoek met nummer '||v_onderzoeknr||' aangemaakt met indicatie '||v_indi_code||'. Dit is een geautomatiseerde email.';

      if v_indi_code is not null then
        UTL_MAIL.SEND
          ( sender => v_afzender
          , recipients => v_ontvanger
          , subject => v_titel
          , message => v_boodschap);

        insert into beh_berichten
        (  bety_id,
        verzonden,
        datum_verzonden,
        oracle_uid,
        ontvanger_id_intern,
        ontvanger_id_extern,
        ontvanger_type,
        adres_verzonden,
        log_tekst,
        entiteit,
        entiteit_id,
        bericht_tekst
        )
        values
        (v_bety_id,
        'J',
        v_datum_verzonden,
        v_oracle_uid,
        null,
        v_ontvanger,
        'leeg',
        v_afzender,
        'OK',
        'ONDE',
        cg$KGC_ONDERZOEK_INDICATIES.cg$TABLE(i).onde_id,
        v_boodschap
        )
        ;

        insert into kgc_notities
        (entiteit,
        ID,
        omschrijving,
        code,
        commentaar
        )
        values
        ('ONDE',
        cg$KGC_ONDERZOEK_INDICATIES.cg$TABLE(i).onde_id,
        v_ontvanger,
        'EMAIL MAASTRICHT',
        v_boodschap
        )
        ;

      end if;

      i := cg$KGC_ONDERZOEK_INDICATIES.cg$table.NEXT(i);

    END LOOP;

    idx := cg$KGC_ONDERZOEK_INDICATIES.cg$table.FIRST;

    -- vervolg leidt soms tot NO_DATA_FOUND fout (ORA-01403)

    IF ( NOT cg$KGC_ONDERZOEK_INDICATIES.cg$table.exists(idx) )
    THEN
      RETURN;
    END IF;

  END;

END;
/
ALTER TRIGGER "HELIX"."BEH_ONIN_INDI_MAASTRICHT_TRG" ENABLE;

/
QUIT
