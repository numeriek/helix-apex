CREATE OR REPLACE TRIGGER "HELIX"."BEH_HEBON_WAARDEN_TRG_BIR"
before insert ON BEH_HEBON_WAARDEN  for each row
declare
  v_HEWA_ID number;
  v_user varchar2(50);
  v_nu   date;
begin
  select beh_hewa_seq.nextval into v_HEWA_ID from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.HEWA_ID       := v_HEWA_ID;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;
/
ALTER TRIGGER "HELIX"."BEH_HEBON_WAARDEN_TRG_BIR" ENABLE;

/
QUIT
