CREATE OR REPLACE TRIGGER "HELIX"."BEH_CHECK_LISZCODE_TRG"
after insert or update of liszcode on kgc_relaties for each row
 WHEN (new.liszcode is not null and new.vervallen = 'N') declare
v_afzender varchar2(50) := 'cukz124@umcn.nl';
v_ontvanger varchar2(500) := 'PostbusHelixRelatiecodes@umcn.nl,cukz124@umcn.nl';
v_db_naam VARCHAR2(10) := SYS_CONTEXT ('USERENV', 'DB_NAME');		
v_titel varchar2(200) := 'Ongelidig znr in ' || v_db_naam;
v_message varchar2(4000);

begin
	if v_db_naam like 'PROD%' and
    (upper(:new.liszcode) not like 'Z%' or
		length(trim(:new.liszcode)) != 7 or
		beh_alfu_00.is_nummer(substr(:new.liszcode, 2)) = 'N')
		then
			v_message := 'Beste,' ||
			chr(10) ||
			chr(10) ||
			'Door: ' || user ||
      chr(10) ||
      'Op: ' || to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss') ||
      chr(10) ||
      'Is er een ongeldig znr (' || :new.liszcode || ') bij relatie: ' ||
			:new.code || ' (' || :new.aanspreken || ') ingevoerd. ' ||
			'Dit kan problemen voor andere verwerkingen in Helix veroorzaken (onder andere declaraties).' ||
			chr(10) ||
			chr(10) ||
			'Dit bericht is automatisch door Helix trigger: beh_check_liszcode gegenereerd en verzonden.';
			
			UTL_MAIL.SEND(  sender => v_afzender,
					recipients => v_ontvanger,
					subject => v_titel,
					message => v_message);
	
		end if;
exception
  when others then
  raise;
end;
/
ALTER TRIGGER "HELIX"."BEH_CHECK_LISZCODE_TRG" ENABLE;

/
QUIT
