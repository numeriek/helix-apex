CREATE OR REPLACE TRIGGER "HELIX"."BEH_DRAGON_AANVRAAG_VERWIJDERD"
AFTER DELETE
ON HELIX.KGC_IMPORT_FILES
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
 WHEN (
old.imlo_id=84
      ) declare

v_sender varchar2(500) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
v_recipients varchar2(500) := 'M.Golebiowska@sb.umcn.nl, dna@umcn.nl'; -- komma gescheiden
v_cc varchar2(500) := ''; -- komma gescheiden
v_bcc varchar2(500) :=''; -- komma gescheiden
v_subject varchar2(1000);
v_message varchar2(4000);
v_newline varchar2(10) := chr(10);
v_AanvraagNR kgc_import_files.filenaam%type;
v_ZisNR kgc_import_files.filenaam%type;
v_db_name varchar2(100);

begin
SELECT * into v_db_name FROM GLOBAL_NAME;
v_AanvraagNR := substr(:old.filenaam, 0, instr(:old.filenaam, '(')-1);
v_ZisNR := substr(:old.filenaam, 8, instr(:old.filenaam, 'p=')+1);

IF v_db_name not like '%PROD%'
then v_subject := 'NIET BELANGRIJK! DIT IS EEN TEST. Aanvraag verwijderd: ' || v_AanvraagNR ;
ELSE v_subject := 'Aanvraag verwijderd: ' || v_AanvraagNR ;
END IF;

v_message := 'Beste Postbus DNA Diagnostiek,'
    || v_newline
    || v_newline
    || v_subject
    || v_newline
    || v_newline
    || 'Dragon aanvraag is in Helix verwijderd.'
    || v_newline
    || 'De aanvraag nummer was: ' || v_AanvraagNR
    || v_newline
    || 'De ZIS nummer van de patient was: ' || v_ZisNR
    || v_newline
    || v_newline
    || 'Mogelijk is de aanvraag in Dragon geannuleerd of verwijderd.'
    || v_newline
    || v_newline
    || v_newline
    || 'met vriendelijke groeten,'
    || v_newline
    || 'Helix Beheer';

UTL_MAIL.SEND(  sender => v_sender,
                recipients => v_recipients,
                cc => v_cc,
                bcc => v_bcc,
                subject => v_subject,
                message => v_message);

end;
/
ALTER TRIGGER "HELIX"."BEH_DRAGON_AANVRAAG_VERWIJDERD" ENABLE;

/
QUIT
