CREATE OR REPLACE TRIGGER "HELIX"."BEH_LEGE_DRAGON_AANVRAAG"
AFTER UPDATE
ON HELIX.KGC_IMPORT_FILES
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
 WHEN (
new.filenaam LIKE '%p=<P>%'
and new.imlo_id = 84
      ) declare

v_sender varchar2(500) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
v_recipients varchar2(500) := 'M.Golebiowska@sb.umcn.nl, C.Vermaat@sb.umcn.nl, MH.Merks@sb.umcn.nl'; -- komma gescheiden
v_subject varchar2(1000);
v_message varchar2(4000);
v_newline varchar2(10) := chr(10);
v_db_name varchar2(100);

begin
SELECT * into v_db_name FROM GLOBAL_NAME;

IF v_db_name not like '%PROD%'
then v_subject := 'DIT IS GEEN PRODUCTIE PROBLEEM. Probleem aanvraag: ' || :new.filenaam;
ELSE v_subject := 'PRODUCTIE PROBLEEM -> Probleem aanvraag: ' || :new.filenaam;
END IF;


v_message := 'Beste ***,'
    || v_newline
    || v_newline
    || v_subject
    || v_newline
    || v_newline
    || 'Er is een nieuwe Dragon aanvraag ZONDER gegevens in Helix binnengekomen.'
    || v_newline
    || 'De aanvraag nummer is: ' || :new.filenaam
    || v_newline
    || v_newline
    || v_newline
    || v_newline
    || v_newline
    || 'met vriendelijke groeten,'
    || v_newline
    || 'Helix Beheer';


UTL_MAIL.SEND(  sender => v_sender,
                recipients => v_recipients,
                subject => v_subject,
                message => v_message);
end;
/
ALTER TRIGGER "HELIX"."BEH_LEGE_DRAGON_AANVRAAG" ENABLE;

/
QUIT
