CREATE OR REPLACE TRIGGER "HELIX"."BEH_PERS_TELF_WIJZ"

BEFORE UPDATE ON KGC_PERSONEN FOR EACH ROW
declare

  v_afzender  varchar2(50) := 'cukz124@umcn.nl';
  v_ontvanger varchar2(50) := 'Marleen.Huigen@radboudumc.nl';
  v_titel     varchar2(100) := 'SMS PHE TELEFOONNUMMER GEWIJZIGD';
  v_boodschap varchar2(100) := 'Telefoonummer van zisnr  <> is gewijzigd';
  v_sms_phe  varchar2(20) := null;

BEGIN
-- zie 0010115: email als bij patient telefoonnr wordt gewijzgd
-- in verband  met sms van meetwaarde  naar PATIENT bij phe test
-- cv, juli 2014
  if :old.telefoon  <> :new.telefoon then
    select kgc_attr_00.waarde('KGC_PERSONEN','SMS_PHE_TOESTEMMING',:old.pers_id) into v_SMS_PHE from dual;
    if upper(v_sms_phe ) like '%J%' then
      insert into kgc_notities(entiteit, id, code, omschrijving)
      values('PERS', :old.pers_id, 'TEL_WIJZ',to_char(sysdate, 'dd-mm-yy HH24:mi:ss')||', Telefoonnummer was oorspronkelijk: '||:old.telefoon);

      select kgc_attr_00.waarde('KGC_PERSONEN','SMS_PHE_TOESTEMMING',:old.pers_id) into v_SMS_PHE from dual;

       v_boodschap := replace(v_boodschap, '<>', :old.zisnr);
      -- verstuur de mail naar het eerste adres
      v_ontvanger := 'LKNbasisdiagnostiek@cukz.umcn.nl';
      Helix.Check_jobs.post_mail(v_afzender, v_ontvanger, v_titel, v_boodschap);


    end if;

  end if;
  exception
  when others then
  null;

end;
/
ALTER TRIGGER "HELIX"."BEH_PERS_TELF_WIJZ" ENABLE;

/
QUIT
