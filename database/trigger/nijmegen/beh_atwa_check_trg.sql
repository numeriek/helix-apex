CREATE OR REPLACE TRIGGER "HELIX"."BEH_ATWA_CHECK_TRG"
after insert or update on kgc_attribuut_waarden for each row
declare
v_afzender varchar2(50) := 'cukz124@umcn.nl';
v_ontvanger varchar2(500) := 'cukz124@umcn.nl';

v_db_naam VARCHAR2(10) := SYS_CONTEXT ('USERENV', 'DB_NAME');
v_atwa_ok boolean;
v_message varchar2(4000);

begin
v_atwa_ok := beh_atwa_check
				(p_attr_id => :new.attr_id
				,p_waarde => :new.waarde
                ,x_message => v_message);
			
if not v_atwa_ok and v_db_naam like 'PROD%' then
	v_message := 'Beste,' ||
	chr(10) ||
	chr(10) ||
	'Door: ' || user ||
    chr(10) ||
    'Op: ' || to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss') ||
    chr(10) ||
    'Is er een ' || v_message || ' ingevoerd.' ||
	chr(10) ||
	chr(10) ||
	'Dit bericht is automatisch door Helix trigger: beh_atwa_check_trg gegenereerd en verzonden.';
	
	UTL_MAIL.SEND(  sender => v_afzender,
					recipients => v_ontvanger,
					subject => v_db_naam || ': ongeldige attribuutwaarde',
					message => v_message);

end if;
end;
/
ALTER TRIGGER "HELIX"."BEH_ATWA_CHECK_TRG" ENABLE;

/
QUIT
