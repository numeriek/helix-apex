CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONIN_ONDE_EINDDATUM_TRG"
  after insert on kgc_onderzoek_indicaties
  for each row
  follows helix.kgc_onin_onde_einddatum_trg
declare

  v_geplande_einddatum date;

begin
  -- Corrigeer indien nodig de geplande einddatum om verschil te kunnen
  -- maken tussen binnenlands- en buitenlandse looptijden
  --
  -- Mantis 12168
  --
  v_geplande_einddatum := beh_loop_00.geplande_einddatum( p_onde_id => :new.onde_id
                                                        , p_indi_id => :new.indi_id
                                                        );

  if v_geplande_einddatum is not null
  then
    update kgc_onderzoeken
    set    geplande_einddatum = v_geplande_einddatum
    where  onde_id = :new.onde_id
    and  ( geplande_einddatum is null
        or geplande_einddatum > v_geplande_einddatum
         );
  end if;

end;
/
ALTER TRIGGER "HELIX"."BEH_ONIN_ONDE_EINDDATUM_TRG" ENABLE;

/
QUIT
