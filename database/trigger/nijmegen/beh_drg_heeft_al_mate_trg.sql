CREATE OR REPLACE TRIGGER "HELIX"."BEH_DRG_HEEFT_AL_MATE_TRG"
AFTER INSERT
ON HELIX.KGC_IM_MONSTERS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
 WHEN (
new.monsternummer = '*KOPPEL*'
OR
new.impw_id_mate = '15801'    -- DNA materiaal
OR
new.impw_id_mate = '29702'    -- OVERIG materiaal
--
-- toegevoegd op 05 maart 2015 door CV nav mail van Anita, mantos 11021
--
OR
new.impw_id_mate = '16330'    -- OVERIG materiaal: Overig
OR
new.impw_id_mate = '29600'    -- OVERIG materiaal: OV
OR
new.impw_id_mate = '29642'    -- OVERIG materiaal: OV_CYTO
      ) declare
 v_job_id varchar2(200);
 v_sql varchar2(2500);

BEGIN
select 'BEH_DRG_HEEFT_AL_MATE_' ||:new.imfi_id into v_job_id from dual;
 v_sql := 'HELIX.BEH_DRG_HEEFT_AL_MATE(' || :new.imfi_id || ');';
SYS.DBMS_JOB.SUBMIT
    ( job       => v_job_id
     ,what      => v_sql
     ,next_date => sysdate + (10/(60*60*24))
     ,interval  => null
     ,no_parse  => true
    );
END;
/
ALTER TRIGGER "HELIX"."BEH_DRG_HEEFT_AL_MATE_TRG" ENABLE;

/
QUIT
