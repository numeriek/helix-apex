CREATE OR REPLACE TRIGGER "HELIX"."BEH_ZET_DATUM_VRIJ_TRG_AUR"
AFTER UPDATE
ON HELIX.kgc_onderzoeken FOR EACH ROW
BEGIN

  IF nvl(:new.datum_autorisatie,to_date('01-01-2000','dd-mm-yyyy')) <> nvl(:old.datum_autorisatie,to_date('01-01-2000','dd-mm-yyyy'))THEN
    beh_zet_datum_vrijgeven(  :new.onde_id
                            , :new.kafd_id
                            , :new.ongr_id
                            , :new.rela_id
                            , :new.datum_autorisatie
                            );
  END IF;

END;

/
ALTER TRIGGER "HELIX"."BEH_ZET_DATUM_VRIJ_TRG_AUR" ENABLE;

/
QUIT
