CREATE OR REPLACE TRIGGER "HELIX"."BEH_AANMELD_ONDE_PERS_TRG_BIR"
before insert ON BEH_AANMELDEN_ONDERZOEKEN_PERS for each row
declare
  v_aaop_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select BEH_AAOP_SEQ.nextval into v_aaop_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.aaop_id       := v_aaop_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;

/
ALTER TRIGGER "HELIX"."BEH_AANMELD_ONDE_PERS_TRG_BIR" ENABLE;

/
QUIT
