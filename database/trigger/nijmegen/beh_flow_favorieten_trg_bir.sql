CREATE OR REPLACE TRIGGER "HELIX"."BEH_FLOW_FAVORIETEN_TRG_BIR"
before insert ON HELIX.beh_flow_favorieten for each row
declare
  v_flfa_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select beh_flfa_seq.nextval into v_flfa_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.flfa_id       := v_flfa_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;

/
ALTER TRIGGER "HELIX"."BEH_FLOW_FAVORIETEN_TRG_BIR" ENABLE;

/
QUIT
