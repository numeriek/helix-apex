CREATE OR REPLACE TRIGGER "HELIX"."BEH_FLOW_MEDE_SETTINGS_TRG_BIR"
before insert ON HELIX.beh_flow_mede_settings for each row
declare
  v_flms_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  v_flms_id := nvl(:new.flms_id, beh_flms_seq.nextval);

  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.flms_id       := v_flms_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;
  :new.filter_naam := upper(:new.filter_naam);
end;
/
ALTER TRIGGER "HELIX"."BEH_FLOW_MEDE_SETTINGS_TRG_BIR" ENABLE;

/
QUIT
