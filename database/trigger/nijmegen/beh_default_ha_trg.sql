CREATE OR REPLACE TRIGGER "HELIX"."BEH_DEFAULT_HA_TRG"
after delete or update on kgc_relaties for each row
 WHEN (old.code = 'ONBHUISART') declare

e_niet_toegestaan exception;

begin
	raise e_niet_toegestaan;

exception
	when e_niet_toegestaan then
	    raise_application_error(-20000, SQLERRM || ' => '|| 'Het wijzigen of verwijderen van default huisarts (ONBHUISART) is niet toegestaan.');
	when others then
		raise;
end;
/
ALTER TRIGGER "HELIX"."BEH_DEFAULT_HA_TRG" ENABLE;

/
QUIT
