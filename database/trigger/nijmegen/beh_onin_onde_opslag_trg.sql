CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONIN_ONDE_OPSLAG_TRG"
  after insert ON HELIX.KGC_ONDERZOEK_INDICATIES   for each row
declare
v_aantal         number;
v_Onde_id        kgc_uitslagen.onde_id%type;
v_Kafd_id        kgc_uitslagen.kafd_id%type;
v_Rela_id        kgc_uitslagen.rela_id%type;
v_Brty_id        kgc_uitslagen.brty_id%type;
v_Mede_id        kgc_uitslagen.mede_id%type;
v_Datum_uitslag  kgc_uitslagen.datum_uitslag%type;
v_Tekst          kgc_uitslagen.tekst%type;
v_Commentaar     kgc_uitslagen.commentaar%type;
v_Toelichting    kgc_uitslagen.toelichting%type;
v_Datum_mede_id  kgc_uitslagen.datum_mede_id%type;
v_Volledig_adres kgc_uitslagen.volledig_adres%type;

begin


  select count(*) into v_aantal
  from kgc_indicatie_teksten indi
  , kgc_onderzoeksgroepen ongr
  where indi_id = :NEW.INDI_ID
  and ongr.kafd_id = 1
  and indi.ongr_id = ongr.ongr_id
  and ongr.code not in ('POST_ENS','POST_NIJM','TUM_ENS','TUM_NIJM','PRE_NIJM')
  and upper(indi.code) like upper('%OPSLAG%');

  if v_aantal <> 0 then
  v_onde_id := :NEW.ONDE_ID;


  select kafd_id, rela_id into v_Kafd_id, v_rela_id from kgc_onderzoeken where onde_id = v_onde_id;

  select brty_id into v_Brty_id from kgc_brieftypes where code = 'EINDBRIEF';

  select standaard_waarde into v_tekst from kgc_systeem_parameters where code = 'BEH_UITSLAG_TEKST_BIJ_OPSLAG';

  v_Datum_uitslag := sysdate;
  v_Datum_mede_id := sysdate;
  v_Volledig_adres:= kgc_rapp_00.volledig_adres_rela(v_rela_id);

  select mede_id into v_mede_id from kgc_medewerkers where SYS_CONTEXT ('USERENV', 'SESSION_USER') = oracle_uid;

  if (  v_onde_id is not null
    and v_kafd_id is not null
    and v_rela_id is not null
    and v_brty_id is not null
    and v_mede_id is not null
    and v_tekst   is not null
    )
  then

    insert into kgc_uitslagen
    (Onde_id,
    Kafd_id,
    Rela_id,
    Brty_id,
    Mede_id,
    Datum_uitslag,
    Tekst,
    Datum_mede_id,
    Volledig_adres
    )
    values(v_Onde_id,
    v_Kafd_id,
    v_Rela_id,
    v_Brty_id,
    v_Mede_id,
    v_Datum_uitslag,
    v_Tekst,
    v_Datum_mede_id,
    v_Volledig_adres
    );
  end if;

end if;
EXCEPTION
WHEN OTHERS THEN NULL;
end;
/
ALTER TRIGGER "HELIX"."BEH_ONIN_ONDE_OPSLAG_TRG" ENABLE;

/
QUIT
