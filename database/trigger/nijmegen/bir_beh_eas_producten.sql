CREATE OR REPLACE TRIGGER "HELIX"."BIR_BEH_EAS_PRODUCTEN"
before insert ON  BEH_EAS_PRODUCTEN for each row
declare
  v_PROD_id number;
  v_user varchar2(50);
  v_nu   date;
begin
  select  BEH_PROD_SEQ.nextval into v_PROD_id from dual;
  select user                 into v_user    from dual;
  select sysdate              into v_nu      from dual;
  :new.PROD_id       := v_PROD_id;
  :new.created_by    := v_user;
  :new.creation_date := v_nu;
  :new.last_updated_by    := v_user;
  :new.last_update_date := v_nu;

end;
/
ALTER TRIGGER "HELIX"."BIR_BEH_EAS_PRODUCTEN" ENABLE;

/
QUIT
