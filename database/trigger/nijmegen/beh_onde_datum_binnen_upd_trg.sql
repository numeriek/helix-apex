CREATE OR REPLACE TRIGGER "HELIX"."BEH_ONDE_DATUM_BINNEN_UPD_TRG"
BEFORE UPDATE OF DATUM_BINNEN ON HELIX.KGC_ONDERZOEKEN FOR EACH ROW
DECLARE
  -----
  -- Trigger gemaakt voor Mantis melding: 12387
  -- Bereken nieuwe geplande einddatum na een aanpassing van de datum_binnen.
  -----
  v_geplande_einddatum     date; -- Geplande einddatum.
  v_min_geplande_einddatum date; -- Minimum van geplande einddatum over alle indicaties.
  v_verkort_doorlooptijd   number;
  v_doorlooptijd           number;

  CURSOR c_onderzoek_indicaties(p_onde_id kgc_onderzoeken.onde_id%type) IS
  select indi.indi_id
  from   kgc_onderzoek_indicaties onin
    join kgc_indicatie_teksten indi on indi.indi_id = onin.indi_id
  where  onin.onde_id = p_onde_id
    and  indi.vervallen = 'N';

BEGIN
  -- Geplande einddatum voor de datum binnen is een uitzondering.
  -- GENOOM gebruikt bv. geplande einddatum 11-11-1111 als indicatie dat er handmatig
  -- gepland wordt. METAB gaat ook zoiets gebruiken.
  IF nvl(:NEW.geplande_einddatum, sysdate) < nvl(:NEW.datum_binnen, sysdate) THEN
    return;
  END IF;

  v_min_geplande_einddatum := null;

  -- Zoek de minimum geplande_einddatum over alle onderzoek indicaties.
  -- Deze methode op basis van indicaties wordt gebruikt door GENOOM.
  -- METAB gebruikt een methode op basis van protocollen.
  FOR r_onderzoek_indicaties IN c_onderzoek_indicaties(:NEW.onde_id) LOOP

    -- Het aanroepen van beh_loop_00.geplande_einddatum en daarna geplande_einddatum updaten
    -- resulteert in een mutating table error. Daarom is de onderstaande aanroep hieronder
    -- op een andere manier uitgewerkt.
    --
    -- ** v_geplande_einddatum := beh_loop_00.geplande_einddatum( p_onde_id => :NEW.onde_id
    -- **                                                       , p_indi_id => r_onderzoek_indicaties.indi_id
    -- **                                                       );

    --Haal de einddatum op via de standaard manier.
    v_min_geplande_einddatum := kgc_loop_00.geplande_einddatum(:NEW.datum_binnen, r_onderzoek_indicaties.indi_id, :NEW.onderzoekswijze);

    --Corrigeer de einddatum alleen als het een buitenlands onderzoek betreft.
    if beh_onde_buitenland_00.get_rela_land(:NEW.rela_id) = 'BUITENLAND'  and r_onderzoek_indicaties.indi_id is not null then

      --Haal extra attribuut waarde VERKORT_BU_DOORLOOPTIJD op voor de indicatie.
      begin
        select to_number(atwa.waarde) as verkort_looptijd
        into   v_verkort_doorlooptijd
        from   kgc_attributen attr
          join kgc_attribuut_waarden atwa on atwa.id = r_onderzoek_indicaties.indi_id and atwa.attr_id = attr.attr_id
        where  attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
          and  attr.code = 'VERKORT_BU_DOORLOOPTIJD';
      exception when others then
        v_verkort_doorlooptijd := 0;
      end;

      --Corrigeer de datum.
      v_min_geplande_einddatum := v_min_geplande_einddatum - v_verkort_doorlooptijd;

    end if;

    IF (v_geplande_einddatum < v_min_geplande_einddatum) or (v_min_geplande_einddatum is null) THEN
      v_min_geplande_einddatum := v_geplande_einddatum;
    END IF;

  END LOOP;

  -- Als er een nieuw minimum is, dan update het onderzoek met deze nieuwe datum.
  -- NB: Deze datum kan ook later zijn dan de oorspronkelijke geplande_einddatum.
  --
  -- Er wordt uitgegaan van dat er voor METAB op dit moment geen v_min_geplande_einddatum
  -- berekend wordt via indicaties, en dat de berekening via protocollen voor
  -- GENOOM niets oplevert. Dit wordt echter niet afgevangen. Beide methodes kunnen door
  -- alle afdelingen gebruike worden als de juiste extra attrubuut warden gezet worden.
  --
  IF (v_min_geplande_einddatum is not null) THEN
    :NEW.geplande_einddatum := v_min_geplande_einddatum;
  ELSE
    v_doorlooptijd := beh_loop_00.get_dlt_via_snelheid(:NEW.onde_id);
    IF v_doorlooptijd is not null THEN
      :NEW.geplande_einddatum := :NEW.datum_binnen + v_doorlooptijd;
    END IF;
  END IF;

END;
/
ALTER TRIGGER "HELIX"."BEH_ONDE_DATUM_BINNEN_UPD_TRG" ENABLE;

/
QUIT
