CREATE OR REPLACE TRIGGER "HELIX"."BAS_MEET_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON HELIX.BAS_MEETWAARDEN  FOR EACH ROW
DECLARE

  CURSOR meti_cur
  IS
    SELECT afgerond
    FROM   bas_metingen
    WHERE  meti_id = :new.meti_id
    ;
  v_afgerond VARCHAR2(1) := 'N';
  v_null_char VARCHAR2(1) := CHR(2);
  v_null_num NUMBER := -9E-99;
  v_null_date DATE := TO_DATE('23051800','ddmmyyyy');
BEGIN

  -- als meetwaarde, datum_meting en analist (en andere) ongewijzigd blijven
  -- mag een meetwaarde worden gewijzigd:
  -- alleen mest_id, mede_id_controle en commentaar en natuurlijk de audit-kolommen
  -- mogen na afronding worden gewijzigd
  IF ( updating
   AND NVL(:old.meetwaarde,v_null_char) = NVL(:new.meetwaarde,v_null_char)
-- de volgende twee velden worden gewijzigd bij het wijzigen van de status in KGCWLST05A:
-- en dat is toegestaan.
--   AND NVL(:old.datum_meting,v_null_date) = NVL(:new.datum_meting,v_null_date)
--   AND NVL(:old.mede_id,v_null_num) = NVL(:new.mede_id,v_null_num)
   -- ter volledigheid
   AND NVL(:old.meet_id,v_null_num) = NVL(:new.meet_id,v_null_num)
   AND NVL(:old.meti_id,v_null_num) = NVL(:new.meti_id,v_null_num)
   AND NVL(:old.stof_id,v_null_num) = NVL(:new.stof_id,v_null_num)
   AND NVL(:old.meet_reken,v_null_char) = NVL(:new.meet_reken,v_null_char)
   AND NVL(:old.meeteenheid,v_null_char) = NVL(:new.meeteenheid,v_null_char)
   AND NVL(:old.mwst_id,v_null_num) = NVL(:new.mwst_id,v_null_num)
   AND NVL(:old.runs_id,v_null_num) = NVL(:new.runs_id,v_null_num)
   AND NVL(:old.normaalwaarde_ondergrens,v_null_num) = NVL(:new.normaalwaarde_ondergrens,v_null_num)
   AND NVL(:old.normaalwaarde_bovengrens,v_null_num) = NVL(:new.normaalwaarde_bovengrens,v_null_num)
   AND NVL(:old.analysenr,v_null_char) = NVL(:new.analysenr,v_null_char)
   AND NVL(:old.fractievolume,v_null_num) = NVL(:new.fractievolume,v_null_num)
     )
  THEN
    RETURN;
  END IF;
  IF ( updating
   AND :new.meet_reken = 'R' -- directe berekeningen mogen na afronding wijzigen
     )
  THEN
    RETURN;
  END IF;

  IF ( :old.afgerond = 'J'
   AND :new.afgerond = 'J'
     )
  THEN
    v_afgerond := 'J';
  ELSE
    OPEN  meti_cur;
    FETCH meti_cur
    INTO  v_afgerond;
    CLOSE meti_cur;
  END IF;
  IF ( v_afgerond = 'J' )
  THEN
    qms$errors.show_message
    ( p_mesg => 'BAS-00025'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."BAS_MEET_AFGEROND_TRG" ENABLE;

/
QUIT
