CREATE OR REPLACE TRIGGER "HELIX"."GEN_STM_BUR_TRG"
   BEFORE UPDATE
   ON GEN_systems
   FOR EACH ROW
BEGIN
   :new.modified_date   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_STM_BUR_TRG" ENABLE;

/
QUIT
