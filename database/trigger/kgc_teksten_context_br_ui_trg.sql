CREATE OR REPLACE TRIGGER "HELIX"."KGC_TEKSTEN_CONTEXT_BR_UI_TRG"
 BEFORE INSERT OR UPDATE OF INDI_ID
, KAFD_ID
, ONGR_ID
, STGR_ID
 ON KGC_TEKSTEN_CONTEXT
 FOR EACH ROW
DECLARE

  -- leidt kafd_id af uit �f stgr_id �f indi_id,
  -- igv indicaties kan ook ongr_id afgeleid worden.
  v_stgr_rec cg$kgc_stoftestgroepen.cg$row_type;
  v_indi_rec cg$kgc_indicatie_teksten.cg$row_type;
BEGIN
  IF ( :new.kafd_id IS NULL
    OR :new.kafd_id <> :old.kafd_id
    OR nvl( :new.ongr_id,0 ) <> nvl(:old.ongr_id,0)
     )
  THEN
   IF :new.indi_id IS NOT NULL
   THEN
    v_indi_rec.indi_id := :new.indi_id;
    cg$kgc_indicatie_teksten.slct( v_indi_rec );
    :new.kafd_id := v_indi_rec.kafd_id;
    :new.ongr_id := v_indi_rec.ongr_id;
   ELSIF :new.stgr_id IS NOT NULL
   THEN
    v_stgr_rec.stgr_id := :new.stgr_id;
    cg$kgc_stoftestgroepen.slct( v_stgr_rec );
    :new.kafd_id := v_stgr_rec.kafd_id;
   END IF;
  END IF;
 END;

/
ALTER TRIGGER "HELIX"."KGC_TEKSTEN_CONTEXT_BR_UI_TRG" ENABLE;

/
QUIT
