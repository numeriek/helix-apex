CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_MTE_BIR"
BEFORE INSERT
ON GEN_MAIL_TEMPLATES REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
begin
  -- Determine primary Key ID when no value inserted
   if :new.id is null
   then
      select GEN_seq_mte_id.nextval
        into :new.id
        from dual;
   end if;
   --
   :new.created_on    := sysdate;
   :new.created_by      := nvl(apex_application.g_user, user);
   :new.modified_on   := sysdate;
   :new.modified_by     := nvl(apex_application.g_user, user);
   :new.name            := upper(:new.name);
end;
/
ALTER TRIGGER "HELIX"."GEN_TRG_MTE_BIR" ENABLE;

/
QUIT
