CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_RQM_BUR" BEFORE
  UPDATE ON GEN_REPORT_QUERY_PARAMS FOR EACH ROW
BEGIN :new.modified_on := SYSDATE;
  :new.modified_by                                                         := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_RQM_BUR" ENABLE;

/
QUIT
