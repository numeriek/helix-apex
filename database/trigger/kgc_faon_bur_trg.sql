CREATE OR REPLACE TRIGGER "HELIX"."KGC_FAON_BUR_TRG"
 BEFORE UPDATE
 ON KGC_FAMILIE_ONDERZOEKEN
 FOR EACH ROW
DECLARE

  v_sypa_waarde varchar2(1000);
BEGIN
  BEGIN
    v_sypa_waarde := upper( kgc_sypa_00.standaard_waarde
                            ( p_parameter_code => 'FAMILIEONDERZOEK'
                            , p_kafd_id => :new.kafd_id
                            , p_ongr_id => :new.ongr_id
                            )
                          );
  EXCEPTION
    WHEN OTHERS
    THEN
      v_sypa_waarde := 'SPECIFIEK';
  END;
  -- ALGEMEEN = familieonderzoek is een algemeen dossier voor onderzoek naar een indicatie bij een familie: geen synchronisatie van onderliggende onderzoeken!
  -- SPECIFIEK = familieonderzoek is een specifieke aanvraag (datum,aanvrager,wijze) voor onderzoek naar een indicatie bij een familie
  IF ( v_sypa_waarde = 'SPECIFIEK' )
  THEN
    -- relatie doorkopieeren naar onde en uits
    IF ( :old.rela_id <> :new.rela_id )
    THEN
      UPDATE kgc_onderzoeken
      SET   rela_id = :new.rela_id
      WHERE rela_id = :old.rela_id
      AND   afgerond = 'N'
      AND   onde_id IN
            ( SELECT onde_id
              FROM kgc_faon_betrokkenen
              WHERE faon_id = :new.faon_id
            )
      ;
/* gebeurt al in db-trigger op kgc_onderzoeken (AUS)
      IF ( sql%rowcount > 0 )
      THEN
        UPDATE kgc_uitslagen
        SET   rela_id = :new.rela_id
        ,     volledig_adres = kgc_adres_00.relatie( :new.rela_id )
        WHERE rela_id = :old.rela_id
        AND   onde_id IN
              ( SELECT onde_id
                FROM kgc_faon_betrokkenen
                WHERE faon_id = :new.faon_id
              )
        ;
      END IF;
*/
    END IF;

    -- indicatie bij onderzoek
    -- als oude faon.indicatie bestaat bij onin, dan die eerst weggooien
    IF ( NVL( :old.indi_id, -99999999999 ) <> NVL( :new.indi_id, -99999999999 ) )
    THEN
      IF ( :new.indi_id IS NOT NULL )
      THEN
        DECLARE
          CURSOR onin_cur
          IS
            SELECT onin.onin_id
            ,      onin.volgorde
            ,      onde.onde_id
            FROM   kgc_onderzoek_indicaties onin
            ,      kgc_onderzoeken onde
            ,      kgc_faon_betrokkenen fobe
            WHERE  fobe.onde_id = onde.onde_id
            AND    onde.afgerond = 'N'
            AND    onde.onde_id = onin.onde_id (+)
            AND    :old.indi_id = onin.indi_id (+)
            AND    fobe.faon_id = :new.faon_id
            FOR UPDATE OF onin.onin_id nowait
            ;
        BEGIN
          FOR r IN onin_cur
          LOOP
            DELETE FROM kgc_onderzoek_indicaties
            WHERE  onin_id = r.onin_id
            ;
            INSERT INTO kgc_onderzoek_indicaties
            ( onde_id
            , indi_id
            , volgorde
            )
            VALUES
            ( r.onde_id
            , :new.indi_id
            , r.volgorde
            );
          END LOOP;
        END;
      END IF;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_FAON_BUR_TRG" ENABLE;

/
QUIT
