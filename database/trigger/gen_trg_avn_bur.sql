CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_AVN_BUR"
   before update
   ON GEN_APPL_VALIDATIONS    for each row
begin
   :new.modified_on   := sysdate;
   :new.modified_by     := nvl(apex_application.g_user, user);
end;
/
ALTER TRIGGER "HELIX"."GEN_TRG_AVN_BUR" ENABLE;

/
QUIT
