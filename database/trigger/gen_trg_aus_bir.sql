CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_AUS_BIR"
   BEFORE INSERT
   ON GEN_APPL_USERS
   FOR EACH ROW
BEGIN
   -- Vullen primary Key ID veld indien niet reeds meegegeven
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_aus_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   :new.user_id         := LOWER(:new.user_id);
   :new.password        := gen_pck_encryption.f_checksum(:new.password);
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_AUS_BIR" ENABLE;

/
QUIT
