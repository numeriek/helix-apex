CREATE OR REPLACE TRIGGER "HELIX"."KGC_WLST_MEET_AFGEROND"
 BEFORE DELETE
 ON KGC_WERKLIJST_ITEMS
 FOR EACH ROW
DECLARE

  CURSOR wlia_cur
  IS
    SELECT NULL
    FROM   kgc_werklijst_items_archief
    WHERE  wlst_id = :old.wlst_id
    AND    meet_id = :old.meet_id
    ;
  v_dummy VARCHAR2(1);
BEGIN
  OPEN  wlia_cur;
  FETCH wlia_cur
  INTO  v_dummy;
  IF ( wlia_cur%notfound )
  THEN
    CLOSE wlia_cur;
    IF ( bas_meet_00.afgerond( p_meet_id => :old.meet_id ) = 'J' )
    THEN
      qms$errors.show_message
      ( p_mesg => 'BAS-00025'
      , p_errtp => 'E'
      , p_rftf => TRUE
      );
    END IF;
  ELSE
    CLOSE wlia_cur;
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_WLST_MEET_AFGEROND" ENABLE;

/
QUIT
