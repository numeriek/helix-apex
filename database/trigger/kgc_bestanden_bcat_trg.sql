CREATE OR REPLACE TRIGGER "HELIX"."KGC_BESTANDEN_BCAT_TRG"
before insert or update on kgc_bestanden for each row
declare

cursor c_btyp is
select btyp.code
,      btyp.bcat_id
from   kgc_bestand_types btyp
where  btyp.btyp_id = :new.btyp_id
;

r_btyp c_btyp%rowtype;

/******************************************************************************
NAME:      kgc_bestanden_bcat_trg
PURPOSE:   lege velden aanvullen

REVISIONS:
Ver        Date        Author       Description
---------  ----------  ------------ ---------------------------------------
1.0	       08/04/2015  YAR          Mantis 8633, Bestandscategorie vullen indien leeg
*****************************************************************************/

begin

  open  c_btyp;
  fetch c_btyp into r_btyp;
  close c_btyp;

  if :new.bcat_id is null then
    :new.bcat_id := r_btyp.bcat_id;
  end if;

end;
/
ALTER TRIGGER "HELIX"."KGC_BESTANDEN_BCAT_TRG" ENABLE;

/
QUIT
