CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_MTE_BUR"
BEFORE UPDATE
ON GEN_MAIL_TEMPLATES REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
begin
   :new.modified_on   := sysdate;
   :new.modified_by     := coalesce(apex_application.g_user, user);
   :new.name            := upper(:new.name);
end;
/
ALTER TRIGGER "HELIX"."GEN_TRG_MTE_BUR" ENABLE;

/
QUIT
