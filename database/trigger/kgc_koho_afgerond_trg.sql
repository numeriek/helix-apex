CREATE OR REPLACE TRIGGER "HELIX"."KGC_KOHO_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON KGC_KOPIEHOUDERS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
BEGIN
DECLARE
  CURSOR uits_cur
  IS
    SELECT DISTINCT onde_id
    FROM   kgc_uitslagen
    WHERE  uits_id = NVL( :new.uits_id, :old.uits_id )
    ;
    --CURSOR Added by Anuja against Mantis 6492
   CURSOR ongr_kafd_cur(v_onde_id NUMBER)
   IS
     SELECT kafd_id,ongr_id
     FROM kgc_onderzoeken
     WHERE onde_id=v_onde_id;
BEGIN
  FOR r IN uits_cur
  LOOP
     FOR r1 IN ongr_kafd_cur(r.onde_id)--IF Added by Anuja against Mantis 6492
      LOOP
 IF kgc_sypa_00.standaard_waarde ('KOPIEHOUDER_AFGEROND_ONDERZOEK',p_kafd_id =>r1.kafd_id,p_ongr_id =>r1.ongr_id)= 'N'
    THEN --IF Added by Anuja against Mantis 6492

    IF ( kgc_onde_00.afgerond( p_onde_id => r.onde_id ) )
    THEN
      qms$errors.show_message
      ( p_mesg => 'KGC-00045'
      , p_errtp => 'E'
      , p_rftf => TRUE
      );
    END IF;
    END IF;
      END LOOP;
   END LOOP;
END;
END KGC_KOHO_AFGEROND_TRG;
/
ALTER TRIGGER "HELIX"."KGC_KOHO_AFGEROND_TRG" ENABLE;

/
QUIT
