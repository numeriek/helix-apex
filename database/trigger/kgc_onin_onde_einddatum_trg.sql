CREATE OR REPLACE TRIGGER "HELIX"."KGC_ONIN_ONDE_EINDDATUM_TRG"
 AFTER INSERT
 ON KGC_ONDERZOEK_INDICATIES
 FOR EACH ROW
DECLARE

  CURSOR onde_cur ( b_onde_id IN NUMBER )
  IS
    SELECT datum_binnen
    ,      onderzoekswijze
    FROM   kgc_onderzoeken
    WHERE  onde_id = b_onde_id;

  onde_rec onde_cur%ROWTYPE;
  v_geplande_einddatum date;
BEGIN
  OPEN onde_cur ( b_onde_id => :new.onde_id );
  FETCH onde_cur
  INTO onde_rec;
  CLOSE onde_cur;

  v_geplande_einddatum := kgc_loop_00.geplande_einddatum
                        ( p_datum => onde_rec.datum_binnen
                        , p_indi_id => :new.indi_id
                        , p_onderzoekswijze => onde_rec.onderzoekswijze
                        );

  IF v_geplande_einddatum IS NOT NULL
  THEN
    -- HLX-KGCONDE20-11405
    -- alleen update als nieuwe einddatum VOOR al ingevulde einddatum ligt!
    UPDATE kgc_onderzoeken
    SET    geplande_einddatum = v_geplande_einddatum
    WHERE  onde_id = :new.onde_id
    AND  ( geplande_einddatum IS NULL
        OR geplande_einddatum > v_geplande_einddatum
         )
    ;
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_ONIN_ONDE_EINDDATUM_TRG" ENABLE;

/
QUIT
