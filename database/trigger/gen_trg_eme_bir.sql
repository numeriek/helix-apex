CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_EME_BIR"
   BEFORE INSERT
   ON GEN_ERROR_MESSAGES
   FOR EACH ROW
BEGIN
   -- determine primary Key ID when no value inserted
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_eme_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_EME_BIR" ENABLE;

/
QUIT
