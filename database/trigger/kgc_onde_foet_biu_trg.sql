CREATE OR REPLACE TRIGGER "HELIX"."KGC_ONDE_FOET_BIU_TRG"
 BEFORE INSERT OR UPDATE
 ON KGC_ONDERZOEKEN
 FOR EACH ROW
BEGIN
  kgc_pers_00.controleer_moeder_foetus
  ( p_pers_id => :new.pers_id
  , p_foet_id => :new.foet_id
  );
END;

/
ALTER TRIGGER "HELIX"."KGC_ONDE_FOET_BIU_TRG" ENABLE;

/
QUIT
