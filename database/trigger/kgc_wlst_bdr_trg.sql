CREATE OR REPLACE TRIGGER "HELIX"."KGC_WLST_BDR_TRG"
 BEFORE DELETE
 ON KGC_WERKLIJSTEN
 FOR EACH ROW
-- Werklijsten verwijderen mag
-- alleen door bevoegd personeel
-- en enkel vanuit het archief
BEGIN
  IF ( :old.archief = 'N' )
  THEN
    raise_application_error(-20004, 'KGC-00105' );
  ELSIF ( NOT kgc_wlst_00.bevoegd( p_kafd_id => :old.kafd_id ) )
  THEN
    raise_application_error(-20004, 'KGC-00104' );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_WLST_BDR_TRG" ENABLE;

/
QUIT
