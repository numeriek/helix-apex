CREATE OR REPLACE TRIGGER "HELIX"."GEN_STE_BUR_TRG"
   BEFORE UPDATE
   ON GEN_system_message_types
   FOR EACH ROW
BEGIN
   :new.modified_date   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_STE_BUR_TRG" ENABLE;

/
QUIT
