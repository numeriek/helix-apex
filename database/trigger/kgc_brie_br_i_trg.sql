CREATE OR REPLACE TRIGGER "HELIX"."KGC_BRIE_BR_I_TRG"
 BEFORE INSERT
 ON KGC_BRIEVEN
 FOR EACH ROW
DECLARE

  CURSOR onde_cur
  IS
    SELECT kafd_id
    ,      pers_id
    FROM   kgc_onderzoeken
    WHERE  onde_id = :new.onde_id
    ;
  v_dummy NUMBER;
  CURSOR zwan_cur
  IS
    SELECT pers_id
    FROM   kgc_zwangerschappen
    WHERE  zwan_id = :new.zwan_id
    ;
BEGIN
  IF ( :new.pers_id IS NULL )
  THEN
    IF ( :new.onde_id IS NOT NULL )
    THEN
      OPEN  onde_cur;
      FETCH onde_cur
      INTO  :new.kafd_id
      ,     :new.pers_id;
      CLOSE onde_cur;
    ELSIF ( :new.zwan_id IS NOT NULL )
    THEN
      OPEN  zwan_cur;
      FETCH zwan_cur
      INTO  :new.pers_id;
      CLOSE zwan_cur;
    END IF;
  ELSIF ( :new.kafd_id IS NULL
      AND :new.onde_id IS NOT NULL
        )
  THEN
    OPEN  onde_cur;
    FETCH onde_cur
    INTO  :new.kafd_id
    ,     v_dummy;
    CLOSE onde_cur;
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_BRIE_BR_I_TRG" ENABLE;

/
QUIT
