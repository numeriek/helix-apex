CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_RQE_BIR" BEFORE
  INSERT ON GEN_REPORT_QUERIES FOR EACH ROW
BEGIN
    IF :new.id IS NULL THEN
      :new.id  := gen_seq_rqe_id.NEXTVAL;
    END IF;

    :new.created_on      := SYSDATE;
    :new.created_by      := NVL(apex_application.g_user, USER);
    :new.modified_on     := SYSDATE;
    :new.modified_by     := NVL(apex_application.g_user, USER);
  END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_RQE_BIR" ENABLE;

/
QUIT
