CREATE OR REPLACE TRIGGER "HELIX"."KGC_FAMI_BIR_TRG"
 BEFORE INSERT
 ON KGC_FAMILIES
 FOR EACH ROW
BEGIN
  IF ( :new.fami_id IS NULL )
  THEN
    -- fami_id wordt anders te laat gegenereerd
    SELECT kgc_fami_seq.nextval
    INTO   :new.fami_id
    FROM   dual;
  END IF;
  IF ( :new.familienummer IS NULL )
  THEN
    :new.familienummer := TO_CHAR(:new.fami_id);
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_FAMI_BIR_TRG" ENABLE;

/
QUIT
