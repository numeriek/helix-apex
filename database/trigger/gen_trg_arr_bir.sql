CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_ARR_BIR"
   BEFORE INSERT
   ON GEN_APPL_ROLE_ROLES
   FOR EACH ROW
BEGIN
   -- Vullen primary Key ID veld indien niet reeds meegegeven
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_arr_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_ARR_BIR" ENABLE;

/
QUIT
