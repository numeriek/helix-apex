CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_RDN_BIR" BEFORE
  INSERT ON GEN_REPORT_DEFINITIONS FOR EACH ROW
BEGIN
    IF :new.id IS NULL THEN
      :new.id  := gen_seq_rdn_id.NEXTVAL;
    END IF;

    :new.created_on      := SYSDATE;
    :new.created_by      := NVL(apex_application.g_user, USER);
    :new.modified_on     := SYSDATE;
    :new.modified_by     := NVL(apex_application.g_user, USER);
  END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_RDN_BIR" ENABLE;

/
QUIT
