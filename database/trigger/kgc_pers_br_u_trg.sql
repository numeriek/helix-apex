CREATE OR REPLACE TRIGGER "HELIX"."KGC_PERS_BR_U_TRG"
 BEFORE UPDATE OF GEBOORTEDATUM
, ZISNR
, BSN
, VOORLETTERS
, VOORVOEGSEL
, DEFAULT_AANSPREKEN
, GESLACHT
, AANSPREKEN
, ACHTERNAAM_PARTNER
, BSN_GEVERIFIEERD
, ACHTERNAAM
, VOORVOEGSEL_PARTNER
, BEHEER_IN_ZIS
 ON KGC_PERSONEN
 FOR EACH ROW
BEGIN
  -- als identificerende gegevens worden gewijzigd wordt de bevoegdheid gecontroleerd
  IF ( NVL(:old.beheer_in_zis,CHR(2)) <> NVL(:new.beheer_in_zis,CHR(2))
    OR NVL(:old.voorletters,CHR(2)) <> NVL(:new.voorletters,CHR(2))
    OR NVL(:old.voorvoegsel,CHR(2)) <> NVL(:new.voorvoegsel,CHR(2))
    OR NVL(:old.achternaam,CHR(2)) <> NVL(:new.achternaam,CHR(2))
    OR NVL(:old.voorvoegsel_partner,CHR(2)) <> NVL(:new.voorvoegsel_partner,CHR(2))
    OR NVL(:old.achternaam_partner,CHR(2)) <> NVL(:new.achternaam_partner,CHR(2))
    OR NVL(:old.zisnr,CHR(2)) <> NVL(:new.zisnr,CHR(2))
    OR NVL(:old.geslacht,CHR(2)) <> NVL(:new.geslacht,CHR(2))
    OR NVL(TO_CHAR(:old.geboortedatum,'dd-mm-yyyy'),CHR(2)) <> NVL(TO_CHAR(:new.geboortedatum,'dd-mm-yyyy'),CHR(2))
    OR NVL(:old.default_aanspreken,CHR(2)) <> NVL(:new.default_aanspreken,CHR(2))
    OR NVL(:old.aanspreken,CHR(2)) <> NVL(:new.aanspreken,CHR(2))
    OR NVL(:old.bsn,-999999999) <> NVL(:new.bsn,-999999999)
    OR NVL(:old.bsn_geverifieerd,CHR(2)) <> NVL(:new.bsn_geverifieerd,CHR(2))
     )
  THEN
    IF ( NOT kgc_pers_00.bevoegd )
    THEN
      qms$errors.show_message
      ( p_mesg => 'KGC-00102'
      );
    END IF;
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_PERS_BR_U_TRG" ENABLE;

/
QUIT
