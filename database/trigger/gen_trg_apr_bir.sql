CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_APR_BIR"
   before insert
   ON GEN_APPL_PARAMETERS    for each row
begin
   if :new.id is null
   then
      select gen_seq_apr_id.nextval
        into :new.id
        from dual;
   end if;
   :new.created_on    := sysdate;
   :new.created_by      := nvl(apex_application.g_user, user);
   :new.modified_on   := sysdate;
   :new.modified_by     := nvl(apex_application.g_user, user);
end;
/
ALTER TRIGGER "HELIX"."GEN_TRG_APR_BIR" ENABLE;

/
QUIT
