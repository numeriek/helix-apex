CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_GAF_BUR"
   BEFORE UPDATE
   ON GEN_APPL_FEEDBACK
   FOR EACH ROW
BEGIN
   DECLARE
      v_status_id  NUMBER;
   BEGIN
      SELECT id
        INTO v_status_id
        FROM gen_statuses
       WHERE UPPER(category) = 'FEEDBACK'
         AND UPPER(status_code) = 'C';
      --
      IF :new.status = v_status_id
      THEN
         :new.date_closed   := SYSDATE;
      END IF;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;
   --
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
--
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_GAF_BUR" ENABLE;

/
QUIT
