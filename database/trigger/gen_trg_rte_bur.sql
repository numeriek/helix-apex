CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_RTE_BUR" BEFORE
  UPDATE ON GEN_REPORT_TEMPLATES FOR EACH ROW
BEGIN :new.modified_on := SYSDATE;
  :new.modified_by                                                         := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_RTE_BUR" ENABLE;

/
QUIT
