CREATE OR REPLACE TRIGGER "HELIX"."GEN_STM_BIR_TRG"
   BEFORE INSERT
   ON GEN_systems
   FOR EACH ROW
BEGIN
   -- Vullen primary Key ID veld indien niet reeds meegegeven
   IF :new.id IS NULL
   THEN
      SELECT GEN_seq_stm_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   :new.created_date    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_date   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_STM_BIR_TRG" ENABLE;

/
QUIT
