CREATE OR REPLACE TRIGGER "HELIX"."BAS_METI_UNIEK_TRG"
 BEFORE INSERT
 ON BAS_METINGEN
 FOR EACH ROW
DECLARE

  CURSOR c
  IS
    SELECT NULL
    FROM   bas_metingen
    WHERE  onmo_id = :new.onmo_id
    AND    stgr_id = :new.stgr_id
    AND    frac_id = :new.frac_id
    AND    afgerond = 'N'
    ;
  CURSOR c2
  IS
    SELECT NULL
    FROM   bas_metingen
    WHERE  onmo_id = :new.onmo_id
    AND    stgr_id IS NULL
    AND    frac_id = :new.frac_id
    AND    afgerond = 'N'
    ;
  v_dummy VARCHAR2(1);
BEGIN
return; -- uitgeschakeld door MKL op 05-09-2006 ivm. FISH-programmatuur
  IF ( :new.onmo_id IS NOT NULL
   AND :new.stgr_id IS NOT NULL
   AND :new.frac_id IS NOT NULL
     )
  THEN
    OPEN  c;
    FETCH c
    INTO  v_dummy;
    IF ( c%found )
    THEN
      CLOSE c;
      qms$errors.show_message
      ( p_mesg => 'BAS-00029'
      , p_errtp => 'E'
      , p_rftf => TRUE
      );
    ELSE
      CLOSE c;
    END IF;
  END IF;
  IF ( :new.onmo_id IS NOT NULL
   AND :new.stgr_id IS NULL
   AND :new.frac_id IS NOT NULL
     )
  THEN
    OPEN  c2;
    FETCH c2
    INTO  v_dummy;
    IF ( c2%found )
    THEN
      CLOSE c2;
      qms$errors.show_message
      ( p_mesg => 'BAS-00029'
      , p_errtp => 'E'
      , p_rftf => TRUE
      );
    ELSE
      CLOSE c2;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "HELIX"."BAS_METI_UNIEK_TRG" ENABLE;

/
QUIT
