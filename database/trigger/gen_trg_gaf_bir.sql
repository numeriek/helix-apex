CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_GAF_BIR"
   BEFORE INSERT
   ON GEN_APPL_FEEDBACK
   FOR EACH ROW
BEGIN
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_gaf_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   --
   SELECT MAX(sts.id)
     INTO :new.status
     FROM gen_statuses sts
    WHERE sts.category = 'FEEDBACK'
      AND sts.active_ind = 'Y'
      AND sts.initial_ind = 'Y';
   --
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
--
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_GAF_BIR" ENABLE;

/
QUIT
