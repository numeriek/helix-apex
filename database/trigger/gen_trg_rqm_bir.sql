CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_RQM_BIR" BEFORE
  INSERT ON GEN_REPORT_QUERY_PARAMS FOR EACH ROW
BEGIN
    IF :new.id IS NULL THEN
      :new.id  := gen_seq_rqm_id.NEXTVAL;
    END IF;

    :new.created_on      := SYSDATE;
    :new.created_by      := NVL(apex_application.g_user, USER);
    :new.modified_on     := SYSDATE;
    :new.modified_by     := NVL(apex_application.g_user, USER);
  END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_RQM_BIR" ENABLE;

/
QUIT
