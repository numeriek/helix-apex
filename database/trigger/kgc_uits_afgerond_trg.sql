CREATE OR REPLACE TRIGGER "HELIX"."KGC_UITS_AFGEROND_TRG"
/*OR INSERT event is Added by Anuja on 07-01-2016 for Release rework-201503(8.9.1.3)for Mantis12167
  so as to avoid generation(creation) of new letters when Onderzoek is Afgerond*/
 BEFORE DELETE OR UPDATE OR INSERT
 ON KGC_UITSLAGEN
 FOR EACH ROW
BEGIN
 IF ( kgc_onde_00.afgerond( p_onde_id => NVL(:new.onde_id,:old.onde_id) ) )
  THEN
   qms$errors.show_message
    ( p_mesg => 'KGC-00045'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
END;
/
ALTER TRIGGER "HELIX"."KGC_UITS_AFGEROND_TRG" ENABLE;

/
QUIT
