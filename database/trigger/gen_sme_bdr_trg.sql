CREATE OR REPLACE TRIGGER "HELIX"."GEN_SME_BDR_TRG"
   BEFORE DELETE
   ON GEN_SYSTEM_MESSAGES
   FOR EACH ROW
DECLARE
   CURSOR c_ste(b_ste_id GEN_system_message_types.id%TYPE)
   IS
      SELECT name
        FROM GEN_system_message_types
       WHERE id = b_ste_id
         AND active_ind = 'Y';
   r_ste          c_ste%ROWTYPE;
   CURSOR c_sys(b_sys_id GEN_systems.id%TYPE)
   IS
      SELECT package_name
        FROM GEN_systems
       WHERE id = b_sys_id;
   r_sys          c_sys%ROWTYPE;
   l_status_code  GEN_statuses.status_code%TYPE;

BEGIN
   INSERT INTO GEN_system_messages_history(id
                                          ,system_id_from
                                          ,system_id_to
                                          ,message_type_id
                                          ,status_id
                                          ,message_reference_id
                                          ,message_correlation_id
                                          ,message
                                          ,timestamp_processed
                                          ,timestamp_message
                                          ,status_description
                                          ,nr_retries
                                          ,nr_resubmits
                                          ,resubmit_original_msg_id
                                          ,functional_reference_1
                                          ,functional_reference_2
                                          ,created_date
                                          ,created_by
                                          ,modified_date
                                          ,modified_by)
        VALUES (:old.id
               ,:old.system_id_from
               ,:old.system_id_to
               ,:old.message_type_id
               ,:old.status_id
               ,:old.message_reference_id
               ,:old.message_correlation_id
               ,:old.message
               ,:old.timestamp_processed
               ,:old.timestamp_message
               ,:old.status_description
               ,:old.nr_retries
               ,:old.nr_resubmits
               ,:old.resubmit_original_msg_id
               ,:old.functional_reference_1
               ,:old.functional_reference_2
               ,:old.created_date
               ,:old.created_by
               ,:old.modified_date
               ,:old.modified_by);
   l_status_code      :=
      GEN_pck_general.f_get_status_code('SYSTEM_MESSAGES'
                                       ,:old.status_id);
   IF :old.system_id_from IS NULL
   THEN
      OPEN c_ste(:old.message_type_id);
      FETCH c_ste
         INTO r_ste;
      CLOSE c_ste;
      -- get the to interface package
      OPEN c_sys(:old.system_id_to);
      FETCH c_sys
         INTO r_sys;
      CLOSE c_sys;
      IF r_sys.package_name IS NOT NULL
      THEN
         EXECUTE IMMEDIATE
               'begin '
            || r_sys.package_name
            || '.p_remove_message(:p_message_type,:p_message_reference_id,:p_status_code); end;'
            USING r_ste.name
                 ,:old.message_reference_id
                 ,l_status_code;
      END IF;
   END IF;
END;
/
ALTER TRIGGER "HELIX"."GEN_SME_BDR_TRG" ENABLE;

/
QUIT
