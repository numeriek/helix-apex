CREATE OR REPLACE TRIGGER "HELIX"."BAS_RESE_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON BAS_RESERVERINGEN
 FOR EACH ROW
BEGIN
  IF ( kgc_onde_00.afgerond( p_meet_id => NVL(:new.meet_id,:old.meet_id) ) )
  THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00045'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."BAS_RESE_AFGEROND_TRG" ENABLE;

/
QUIT
