CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_DOC_BUR"
   BEFORE UPDATE
   ON GEN_DOCUMENTS
   FOR EACH ROW
BEGIN
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_DOC_BUR" ENABLE;

/
QUIT
