CREATE OR REPLACE TRIGGER "HELIX"."KGC_KARY_AFGEROND_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON KGC_KARYOGRAMMEN
 FOR EACH ROW
BEGIN
  -- is onderzoek afgerond?
  IF ( kgc_onde_00.afgerond( p_meet_id => NVL(:new.meet_id,:old.meet_id) ) )
  THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00045'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;

  -- is meting/meetwaarde afgerond?
  IF ( bas_meet_00.afgerond( p_meet_id => NVL(:new.meet_id,:old.meet_id) ) = 'J' )
  THEN
    qms$errors.show_message
    ( p_mesg => 'BAS-00025'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."KGC_KARY_AFGEROND_TRG" ENABLE;

/
QUIT
