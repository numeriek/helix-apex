CREATE OR REPLACE TRIGGER "HELIX"."BAS_MEET_ONDE_AF_TRG"
 BEFORE DELETE OR INSERT OR UPDATE
 ON BAS_MEETWAARDEN
 FOR EACH ROW
BEGIN
  -- is onderzoek afgerond?
  IF ( kgc_onde_00.afgerond( p_meti_id => NVL(:new.meti_id,:old.meti_id) ) )
  THEN
    qms$errors.show_message
    ( p_mesg => 'KGC-00045'
    , p_errtp => 'E'
    , p_rftf => TRUE
    );
  END IF;
END;

/
ALTER TRIGGER "HELIX"."BAS_MEET_ONDE_AF_TRG" ENABLE;

/
QUIT
