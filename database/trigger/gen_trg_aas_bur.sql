CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_AAS_BUR"
   BEFORE UPDATE
   ON GEN_APPL_AUTHORIZATIONS
   FOR EACH ROW
BEGIN
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_AAS_BUR" ENABLE;

/
QUIT
