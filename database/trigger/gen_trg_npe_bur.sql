CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_NPE_BUR"
   before update
   ON GEN_NAMED_PREFERENCES    for each row
declare
   l_npe_prefix   varchar2 (30);
begin
   if :new.preference_name <> :old.preference_name
   then
      l_npe_prefix := 'F_';
      if :new.preference_name not like l_npe_prefix || '%'
      then
         :new.preference_name := l_npe_prefix || :new.preference_name;
      end if;
      :new.preference_name := upper (:new.preference_name);
   end if;
   :new.modified_on := sysdate;
   :new.modified_by := nvl (apex_application.g_user, user);
end;
/
ALTER TRIGGER "HELIX"."GEN_TRG_NPE_BUR" ENABLE;

/
QUIT
