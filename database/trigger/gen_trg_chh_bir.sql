CREATE OR REPLACE TRIGGER "HELIX"."GEN_TRG_CHH_BIR"
   BEFORE INSERT
   ON GEN_CHANGE_HISTORY
   FOR EACH ROW
BEGIN
   IF :new.id IS NULL
   THEN
      SELECT gen_seq_chh_id.NEXTVAL
        INTO :new.id
        FROM DUAL;
   END IF;
   :new.created_on    := SYSDATE;
   :new.created_by      := NVL(apex_application.g_user, USER);
   :new.modified_on   := SYSDATE;
   :new.modified_by     := NVL(apex_application.g_user, USER);
END;
/
ALTER TRIGGER "HELIX"."GEN_TRG_CHH_BIR" ENABLE;

/
QUIT
