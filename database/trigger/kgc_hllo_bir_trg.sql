CREATE OR REPLACE TRIGGER "HELIX"."KGC_HLLO_BIR_TRG"
 BEFORE INSERT
 ON KGC_HL7_LOG
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
begin
  if :new.session_id is null
  then
    :new.session_id := sys_context ('USERENV', 'SESSIONID');
  end if;
  if :new.username is null
  then
    :new.username := user;
  end if;
end;

/
ALTER TRIGGER "HELIX"."KGC_HLLO_BIR_TRG" ENABLE;

/
QUIT
