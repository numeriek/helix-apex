CREATE OR REPLACE PROCEDURE "HELIX"."PK2UK"
 (P_ENTITEIT IN VARCHAR2
 ,P_ID IN NUMBER
 ,P_KOLOMMEN IN VARCHAR2 := NULL
 )
 IS
BEGIN
  kgc_util_00.pk2uk
  ( p_entiteit => p_entiteit
  , p_id => p_id
  , p_kolommen => p_kolommen
  );
END pk2uk;

/

/
QUIT
