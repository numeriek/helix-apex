CREATE OR REPLACE PROCEDURE "HELIX"."MSS_START_HL7" is
begin
  SET SERVEROUTPUT ON SIZE 1000000 begin kgc_zis.beheer_hl7(p_action      => 'B',
                                                            p_check_email => 'N');
end;
/

/
QUIT
