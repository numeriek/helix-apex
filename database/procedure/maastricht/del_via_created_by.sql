CREATE OR REPLACE PROCEDURE "HELIX"."DEL_VIA_CREATED_BY" (
  p_table_name   VARCHAR2,
  p_kafd_id      PLS_INTEGER DEFAULT 48,
  p_created_by   VARCHAR2 DEFAULT 'CONVERSIE_DNA',
  p_and_clause   VARCHAR2 DEFAULT '1=1',
  p_date         DATE DEFAULT TRUNC (SYSDATE)
) IS
  v_sql   VARCHAR2 (5000);
BEGIN
  v_sql := 'begin toggle_triggers (''' || p_table_name || ''', false); end;';

  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION
    WHEN OTHERS
    THEN
      DBMS_OUTPUT.put_line (v_sql);
  END;

  BEGIN
    v_sql :=
         'DELETE FROM '
      || p_table_name
      || '@phelix t '
      || '    WHERE ( ( t.created_by = UPPER ('''
      || p_created_by
      || '''))'
      || '          OR EXISTS ( SELECT m.code'
      || '                        FROM kgc_medewerkers@phelix m, kgc_mede_kafd@phelix k'
      || '                       WHERE ( m.ORACLE_UID = t.created_by'
      || '                             AND m.mede_id = k.mede_id'
      || '                             AND k.kafd_id = '
      || p_kafd_id
      || '                             )'
      || '                     )'
      || '           )'
      || '       AND '
      || p_and_clause
      || ' AND creation_date > '
      || p_date;

    EXECUTE IMMEDIATE v_sql;
  EXCEPTION
    WHEN OTHERS
    THEN
      DBMS_OUTPUT.put_line (v_sql);
  END;

  COMMIT;
  v_sql := 'begin toggle_triggers (''' || p_table_name || ''', true); end;';

  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION
    WHEN OTHERS
    THEN
      DBMS_OUTPUT.put_line (v_sql);
  END;
END;
/

/
QUIT
