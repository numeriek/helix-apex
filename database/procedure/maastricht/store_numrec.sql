CREATE OR REPLACE PROCEDURE "HELIX"."STORE_NUMREC"
IS
BEGIN
   DECLARE
      CURSOR c_all_tabs
      IS
         SELECT table_name
           FROM user_tables;
   BEGIN
      FOR r_tab IN c_all_tabs
      LOOP
         EXECUTE IMMEDIATE    'INSERT INTO MIG_COUNT SELECT '''
                           || r_tab.table_name
                           || ''', sysdate, count(1) FROM '
                           || r_tab.table_name;
      END LOOP;

      COMMIT;
   END;
END;
/

/
QUIT
