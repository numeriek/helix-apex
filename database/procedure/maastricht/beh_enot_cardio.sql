CREATE OR REPLACE PROCEDURE "HELIX"."BEH_ENOT_CARDIO" ( p_onde_id     in kgc_onderzoeken.onde_id%type
                                           , p_uits_id     in kgc_uitslagen.uits_id%type
                                           , p_pers_id     in kgc_personen.pers_id%type
                                           , p_rela_id     in kgc_relaties.rela_id%type
                                           , p_onui_id     in kgc_onderzoek_uitslagcodes.onui_id%type
                                           , p_uitslag_gen in epd_uitslag.uitslag_gen%type
                                           )
is
  --
  -- Indien een cardio-onderzoek met uitslag, een MM-onderzoek met uitslag waarbij ook een cardio-onderzoek bestaat
  -- of een Haemochromatose-onderzoek met uitslag waarbij een cardio-onderzoek bestaat, wordt afgerond, dient er
  -- een mail met de uitslag gestuurd te worden.
  --
  --
  cursor c_epdu ( b_onde_id in kgc_onderzoeken.onde_id%type
                , b_uits_id in kgc_uitslagen.uits_id%type
                , b_pers_id in kgc_personen.pers_id%type
                , b_rela_id in kgc_relaties.rela_id%type
                , b_onui_id in kgc_onderzoek_uitslagcodes.onui_id%type
                )
  is
    select kgc_sypa_00.systeem_waarde('ENOT_EMAILADRES_VAN')          emailadres_van
    ,      kgc_sypa_00.systeem_waarde('ENOT_CARDIO_EMAILADRES_AAN')   emailadres_aan
    ,      pers.zisnr                                                 sapnr
    ,      pers.aanspreken                                            persoon
    ,      pers.overleden                                             overleden
    ,      fami.familienummer                                         familienummer
    ,      fami.naam                                                  familienaam
    ,      indi.omschrijving                                          indicatie
    ,      rela.aanspreken                                            aanvrager
    ,      brty.omschrijving                                          brieftype
    ,      ( select onui.omschrijving
             from   kgc_onderzoek_uitslagcodes onui
             where  onui.onui_id = b_onui_id
           )                                                          uitslag_omschr
    from   kgc_indicatie_groepen       ingr
    ,      kgc_indicatie_teksten       indi
    ,      kgc_onderzoek_indicaties    onin
    ,      kgc_families                fami
    ,      kgc_familie_leden           fale
    ,      kgc_brieftypes              brty
    ,      kgc_relaties                rela
    ,      kgc_personen                pers
    ,      kgc_uitslagen               uits
    where  uits.uits_id = b_uits_id
    and    pers.pers_id = b_pers_id
    and    rela.rela_id = b_rela_id
    and    rela.relatie_type = 'IN'
    and    brty.brty_id = uits.brty_id
    and    fale.pers_id = pers.pers_id
    and    fami.fami_id = fale.fami_id
    and    onin.onde_id = b_onde_id
    and    indi.indi_id = onin.indi_id
    and    ingr.ingr_id = indi.ingr_id
    and    ingr.code = 'CARDIO'
    union
    select kgc_sypa_00.systeem_waarde('ENOT_EMAILADRES_VAN')          emailadres_van
    ,      kgc_sypa_00.systeem_waarde('ENOT_CARDIO_EMAILADRES_AAN')   emailadres_aan
    ,      pers.zisnr                                                 sapnr
    ,      pers.aanspreken                                            persoon
    ,      pers.overleden                                             overleden
    ,      fami.familienummer                                         familienummer
    ,      fami.naam                                                  familienaam
    ,      indi.omschrijving                                          indicatie
    ,      rela.aanspreken                                            aanvrager
    ,      brty.omschrijving                                          brieftype
    ,      ( select onui.omschrijving
             from   kgc_onderzoek_uitslagcodes onui
             where  onui.onui_id = b_onui_id
           )                                                          uitslag_omschr
    from   kgc_indicatie_groepen       ingr
    ,      kgc_indicatie_teksten       indi
    ,      kgc_onderzoek_indicaties    onin
    ,      kgc_families                fami
    ,      kgc_familie_leden           fale
    ,      kgc_brieftypes              brty
    ,      kgc_relaties                rela
    ,      kgc_personen                pers
    ,      kgc_uitslagen               uits
    where  uits.uits_id = b_uits_id
    and    pers.pers_id = b_pers_id
    and    rela.rela_id = b_rela_id
    and    rela.relatie_type = 'IN'
    and    brty.brty_id = uits.brty_id
    and    fale.pers_id = pers.pers_id
    and    fami.fami_id = fale.fami_id
    and    onin.onde_id = b_onde_id
    and    indi.indi_id = onin.indi_id
    and    ingr.ingr_id = indi.ingr_id
    and    (   (ingr.code = 'MITO')
            or (indi.code in ('HE','H2A','HE3'))
           )
    and    exists
             ( select null
               from   kgc_indicatie_groepen      ingr2
               ,      kgc_indicatie_teksten      indi2
               ,      kgc_onderzoek_indicaties   onin2
               ,      kgc_onderzoek_betrokkenen  onbe2   -- Check of er bij de betreffende persoon uit het onderzoek een CARDIO onderzoeken bestaat.
               where  onbe2.pers_id = b_pers_id          -- Omdat procedure beh_enot_cardio wordt aangeroepen vanuit een update row trigger op tabel
               and    onbe2.rol = 'A1'                   -- KGC_ONDERZOEKEN, kan er niet gezocht worden op deze tabel (Mutating Table probleem).
               and    onin2.onde_id = onbe2.onde_id      -- Alternatief is te zoeken binnen tabel KGC_ONDERZOEK_BETROKKENEN (elk onderzoek is
               and    indi2.indi_id = onin2.indi_id      -- 1-1, via onde_id en pers_id, gekoppeld aan een onderzoek betrokkene in de rol A1.
               and    ingr2.ingr_id = indi2.ingr_id
               and    ingr2.code = 'CARDIO'
             )
  ;
  --
  r_epdu_rec           c_epdu%rowtype;
  pl_dummy             varchar2(1);
  pl_email_onderwerp   varchar2(100) := 'Nieuwe (cardio-)genetische uitslag';
  pl_email_kopregel    varchar2(256) := '<span style="font-family:arial;font-size:10pt;color:black">'||
                                         'De volgende genetische uitslag is onlangs naar de aanvrager van het onderzoek verstuurd:<br><br>';
  pl_email_tekst       varchar2(2048);
  pl_email_sluitregel  varchar2(256) := '<br><br> </p> <p style="font-family:arial;font-size:8pt;color:gray">'||
                                         '*** Dit is een automatisch gegenereerd emailbericht. Antwoorden op deze mail is niet mogelijk. ***</span>';
  pl_err_msg           varchar2(100);
  --
begin
  open  c_epdu( p_onde_id
              , p_uits_id
              , p_pers_id
              , p_rela_id
              , p_onui_id
              );
  fetch c_epdu
  into  r_epdu_rec;
  --
  if c_epdu%FOUND
  then
    --
    -- Het betreft:
    --   een cardio-onderzoek met uitslag
    --   of een MM-onderzoek met uitslag waarbij ook een cardio-onderzoek bestaat
    --   of een Haemochromatose-onderzoek met uitslag waarbij een cardio-onderzoek bestaat
    -- Verstuur een email
    --
    pl_email_tekst := pl_email_kopregel||
                        '<table border="1">'||
                        '<tr><td><strong>SAPnr</strong></td><td>'||r_epdu_rec.sapnr||'</td></tr>'||
                        '<tr><td><strong>Persoon</strong></td><td>'||r_epdu_rec.persoon||'</td></tr>'||
                        '<tr><td><strong>Overleden</strong></td><td>'||r_epdu_rec.overleden||'</td></tr>'||
                        '<tr><td><strong>Familienummer</strong></td><td>'||r_epdu_rec.familienummer||'</td></tr>'||
                        '<tr><td><strong>Familienaam</strong></td><td>'||r_epdu_rec.familienaam||'</td></tr>'||
                        '<tr><td><strong>Indicatie</strong></td><td>'||r_epdu_rec.indicatie||'</td></tr>'||
                        '<tr><td><strong>Aanvrager</strong></td><td>'||r_epdu_rec.aanvrager||'</td></tr>'||
                        '<tr><td><strong>Brieftype</strong></td><td>'||r_epdu_rec.brieftype||'</td></tr>'||
                        '<tr><td><strong>Omschrijving</strong></td><td>'||r_epdu_rec.uitslag_omschr||'</td></tr>'||
                        '</table>';
    --
    begin
      mss_mail.post_mail( r_epdu_rec.emailadres_van
                        , r_epdu_rec.emailadres_aan
                        , pl_email_onderwerp
                        , pl_email_tekst||'<br>'||p_uitslag_gen||pl_email_sluitregel
                        , TRUE -- HTML
                        );
      --
    exception
      when others
      then begin
             pl_err_msg := substr(SQLERRM, 1, 100);
             --
             mss_mail.post_mail( r_epdu_rec.emailadres_van
                               , 'functioneelbeheer.kg@mumc.nl'
                               , '!!! FOUT BIJ VERSTUREN CARDIO EMAILNOTIFICATIE NAAR '||r_epdu_rec.emailadres_aan||' !!!'
                               , 'Foutmelding: '||pl_err_msg
                               , TRUE -- HTML
                               );
           end;
    end;
    --
  else
    null;
  end if;
  --
  close c_epdu;
  --
end beh_enot_cardio;
/

/
QUIT
