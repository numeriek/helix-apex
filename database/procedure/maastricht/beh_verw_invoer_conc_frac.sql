CREATE OR REPLACE PROCEDURE "HELIX"."BEH_VERW_INVOER_CONC_FRAC"
is
  --
  cursor c_check_icof
  is
    select trim(translate(icof.concentratie,',','.'))   concentratie
    ,      icof.datum                                   datum
    ,      frac.fractienummer                           fractienummer
    from   bas_fracties         frac
    ,      beh_invoer_conc_frac icof
    where  frac.fractienummer (+) = trim(upper(icof.dna_nummer))
    for update of icof.verwerking
  ;
  --
  cursor c_icof
  is
    select icof.dna_nummer                            dna_nummer
    ,      trim(translate(icof.concentratie,',','.')) concentratie
    ,      icof.datum                                 datum
    ,      eenh.eenh_id                               eenh_id
    from   kgc_eenheden         eenh
    ,      beh_invoer_conc_frac icof
    where  icof.verwerking is null
    and    icof.datum > trunc(sysdate) - 7
    and    eenh.eenheid = 'ng/�l' -- eenheid is UK
    for update of icof.verwerking
  ;
  --
  pl_concentratie  number(6,2);
  pl_datum         date;
  --
begin
  for r_check_icof in c_check_icof
  loop
    begin
      if r_check_icof.fractienummer is null
      then
        update beh_invoer_conc_frac icof
        set    icof.verwerking = 'Geen fractie gevonden voor gegeven DNA-nummer.'
        where  current of c_check_icof;
      else
        pl_concentratie := to_number(r_check_icof.concentratie,'9999.99');
        pl_datum := to_date(r_check_icof.datum,'dd-mm-yyyy');
      end if;
    exception
      when VALUE_ERROR -- ORA-06502: PL/SQL: numerieke fout of fout in waarde
      then update beh_invoer_conc_frac icof
           set    icof.verwerking = 'Ongeldige concentratie.'
           where  current of c_check_icof;
      when others
      then update beh_invoer_conc_frac icof
           set    icof.verwerking = 'Ongeldige datum.'
           where  current of c_check_icof;
    end;
  end loop;
  --
  for r_icof in c_icof
  loop
    update bas_fracties frac
    set    frac.concentratie = case
                                 when to_number(r_icof.concentratie,'9999.99') between 100.00 and 366.00
                                 and  frac.frty_id in ( select frty.frty_id
                                                        from   bas_fractie_types      frty
                                                        ,      kgc_onderzoeksgroepen  ongr
                                                        ,      kgc_kgc_afdelingen     kafd
                                                        where  kafd.code = 'LAB'
                                                        and    ongr.kafd_id = kafd.kafd_id
                                                        and    frty.ongr_id = ongr.ongr_id
                                                        and    frty.code = 'DNA'
                                                      )
                                 then 100.00
                                 else to_number(r_icof.concentratie,'9999.99')
                               end
    ,      frac.datum_concentratie_bepaling = to_date(r_icof.datum,'dd-mm-yyyy')
    ,      frac.eenh_id = r_icof.eenh_id
    where  frac.fractienummer = trim(upper(r_icof.dna_nummer));
    --
    update beh_invoer_conc_frac icof
           set    icof.verwerking = 'OK.'
           where  current of c_icof;
    --
  end loop;
end beh_verw_invoer_conc_frac;
/

/
QUIT
