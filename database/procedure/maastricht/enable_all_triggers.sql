CREATE OR REPLACE PROCEDURE "HELIX"."ENABLE_ALL_TRIGGERS" IS
BEGIN
  FOR r IN (SELECT trigger_name
              FROM user_triggers
             WHERE status = 'DISABLED')
  LOOP
    IF INSTR (r.trigger_name, 'BIN$') = 0
    THEN
      EXECUTE IMMEDIATE 'ALTER TRIGGER "' || r.trigger_name || '" ENABLE';
    END IF;
  END LOOP;
END enable_all_triggers;
/

/
QUIT
