CREATE OR REPLACE PROCEDURE "HELIX"."MSS_VERW_AANVULLEN_FRACNR"
is
  cursor c_frac
  is
    select avfr.fractienummer     fractienummer
    ,      mons.datum_aanmelding  fractiedatum
    ,      mons.monsternummer     monsternaam
    ,      mons.datum_aanmelding  monsterdatum
    ,      mate.omschrijving      materiaalnaam
    ,      pers.pers_id           patientnummer
    ,      pers.zisnr             patient_zisnr
    ,      fami.familienummer     familienummer
    ,      fami.fami_id           helix_familie_id
    ,      pers.geslacht          geslacht
    from   ( select distinct
                    fale.pers_id pers_id
             ,      fale.fami_id fami_id
             from   kgc_familie_leden fale
           )                    fale
    ,      kgc_families         fami
    ,      kgc_materialen       mate
    ,      kgc_personen         pers
    ,      kgc_monsters         mons
    ,      bas_fracties         frac
    ,      mss_aanvullen_fracnr avfr
    where  frac.fractienummer = avfr.fractienummer
    and    mons.mons_id = frac.mons_id
    and    pers.pers_id = mons.pers_id
    and    mate.mate_id = mons.mate_id
    and    fale.pers_id = pers.pers_id
    and    fami.fami_id = fale.fami_id
    for update of avfr.fractienummer
  ;
begin
  for r_frac_rec in c_frac
  loop
    update mss_aanvullen_fracnr avfr
    set    avfr.fractiedatum     = r_frac_rec.fractiedatum
    ,      avfr.monsternaam      = r_frac_rec.monsternaam
    ,      avfr.monsterdatum     = r_frac_rec.monsterdatum
    ,      avfr.materiaalnaam    = r_frac_rec.materiaalnaam
    ,      avfr.patientnummer    = r_frac_rec.patientnummer
    ,      avfr.patient_zisnr    = r_frac_rec.patient_zisnr
    ,      avfr.familienummer    = r_frac_rec.familienummer
    ,      avfr.helix_familie_id = r_frac_rec.helix_familie_id
    ,      avfr.geslacht         = r_frac_rec.geslacht
    where  current of c_frac;
  end loop;
end mss_verw_aanvullen_fracnr;
/

/
QUIT
