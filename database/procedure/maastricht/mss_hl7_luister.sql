CREATE OR REPLACE PROCEDURE "HELIX"."MSS_HL7_LUISTER" (P_action varchar2) is
begin
  If P_action = '1' then
    kgc_zis.beheer_hl7(p_action => 'E', p_check_email => 'N');
    kgc_zis.beheer_hl7(p_action => 'B', p_check_email => 'N');
  Else
    kgc_zis.beheer_hl7(p_action => P_Action, p_check_email => 'N');
  End If;

end MSS_HL7_luister;
/

/
QUIT
