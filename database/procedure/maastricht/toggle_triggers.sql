CREATE OR REPLACE PROCEDURE "HELIX"."TOGGLE_TRIGGERS" (
      p_table_name   IN   VARCHAR2 DEFAULT NULL,
      p_enabled      IN   BOOLEAN DEFAULT TRUE
   )
   IS
      CURSOR trig_cur
      IS
         SELECT trigger_name
           FROM user_triggers
          WHERE table_name = NVL (UPPER (p_table_name), table_name);

      v_stmt   VARCHAR2 (240);
   BEGIN
      FOR trig_rec IN trig_cur
      LOOP
         IF p_enabled
         THEN
            v_stmt := 'alter trigger ' || trig_rec.trigger_name || ' enable;';
         ELSE
            v_stmt :=
                     'alter trigger ' || trig_rec.trigger_name || ' disable;';
         END IF;

         kgc_util_00.dyn_exec (v_stmt);
      END LOOP;
   END toggle_triggers;
/

/
QUIT
