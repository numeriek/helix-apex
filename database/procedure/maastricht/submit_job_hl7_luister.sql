CREATE OR REPLACE PROCEDURE "HELIX"."SUBMIT_JOB_HL7_LUISTER" IS
v_jobnum NUMBER;
BEGIN
  DBMS_JOB.SUBMIT(v_jobnum,'kgc_zis.start_hl7_luister;',SYSDATE,'sysdate+1/(24*4)');
END;
/

/
QUIT
