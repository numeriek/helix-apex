CREATE OR REPLACE PROCEDURE "HELIX"."MSS_ARCHIVEER_WERKLIJSTEN" ( p_wlst_nr_like        in varchar2
                                                     , p_alle_items_afgerond in varchar2 )
is
  cursor c_wlst( b_wlst_nr_like        in varchar2
               , b_alle_items_afgerond in varchar2 )
  is
    SELECT wlst.wlst_id
    FROM   kgc_werklijsten  wlst
    WHERE  wlst.werklijst_nummer LIKE b_wlst_nr_like||'%'
    AND    wlst.archief = 'N'
    AND    (   (     (upper(b_alle_items_afgerond) = 'J')
                 AND NOT EXISTS
                       ( SELECT null
                         FROM   bas_meetwaarde_info_vw  mwiv
                         ,      kgc_werklijst_items     wlit
                         WHERE  wlit.wlst_id = wlst.wlst_id
                         AND    mwiv.meet_id = wlit.meet_id
                         AND    least(mwiv.meet_afgerond, mwiv.meti_afgerond) = 'N'
                       )
               )
            OR (upper(b_alle_items_afgerond) = 'N')
           )


  ;
  --
begin
  --
  for r_wlst_rec in c_wlst( p_wlst_nr_like
                          , p_alle_items_afgerond )
  loop
    kgc_wlst_00.archiveer( p_wlst_id => r_wlst_rec.wlst_id
                         , p_in_uit => 'IN'
                         , p_alleen_items => FALSE
                         , p_commit => TRUE
                         );
  end loop;
  --
end mss_archiveer_werklijsten;
/

/
QUIT
