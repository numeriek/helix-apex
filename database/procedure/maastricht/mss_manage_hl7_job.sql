CREATE OR REPLACE PROCEDURE "HELIX"."MSS_MANAGE_HL7_JOB" is
begin

  Mss_stop_hl7();

  Kgc_zis.start_hl7_luister();
  Insert Into MSS_interface_log
    (Hl7logid, Info, Date_Created)
  Values
    (MSS_LOGID.NEXTVAL,
     'HL7_Luister Proces gecontroleert gestart',
     sysdate);
  commit;

end MSS_Manage_HL7_JOB;
/

/
QUIT
