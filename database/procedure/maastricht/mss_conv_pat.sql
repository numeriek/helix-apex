CREATE OR REPLACE PROCEDURE "HELIX"."MSS_CONV_PAT"
As
I integer;
Cursor PAT_SPOA is Select patnr,
       sap_patid,
       geb_datum,
       geslacht,
       pat_voorkeursnr
from   patient@pspoa.azm.nl
where patnr in (Select zisnr from kgc_personen where zisnr is not null );




Begin

   I:= 0;
   --Open Pat_SPOA ;
   For Pat_SPOA_R in Pat_SPOA
   Loop
   begin
      IF Pat_SPOA_R.pat_voorkeursnr is null
      THEN
         UPDATE kgc_personen
         set zisnr = pat_spoa_r.sap_patid
         where zisnr = pat_spoa_r.patnr;

      ELSE
        Insert into MSS_Patconv_result(sap_id, isoft_id, errorc)
        values (pat_spoa_r.sap_patid, pat_spoa_r.patnr, 'Error voorkeursnummer gevonden' );
      END IF;
       i:= i+1;
   if i = 100 then
      commit;
      i:= 0;
   end if;

  Exception when others
  Then Insert into MSS_Patconv_result(sap_id, isoft_id, ERRORC) values (pat_spoa_r.sap_patid, pat_spoa_r.patnr,'EXCEPTION' );

  end;
   End Loop;
  -- close pat_SPOA;
   commit;




End;
/

/
QUIT
