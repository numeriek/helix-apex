CREATE OR REPLACE PROCEDURE "HELIX"."DEL_VIA_KAFDID" (
  p_table_name   VARCHAR2,
  p_kafd_id      PLS_INTEGER DEFAULT 48,
  p_and_clause   VARCHAR2 DEFAULT '1=1'
) IS
  v_sql   VARCHAR2 (500);
BEGIN
  v_sql := 'begin toggle_triggers (''' || p_table_name || ''', false); end;';

  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION
    WHEN OTHERS
    THEN
      DBMS_OUTPUT.put_line (v_sql);
  END;

  BEGIN
    v_sql :=
         'DELETE FROM '
      || p_table_name
      || ' WHERE kafd_id = '
      || p_kafd_id
      || ' AND '
      || p_and_clause;

    EXECUTE IMMEDIATE v_sql;
  EXCEPTION
    WHEN OTHERS
    THEN
      DBMS_OUTPUT.put_line (v_sql);
  END;

  COMMIT;
  v_sql := 'begin toggle_triggers (''' || p_table_name || ''', true); end;';

  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION
    WHEN OTHERS
    THEN
      DBMS_OUTPUT.put_line (v_sql);
  END;
END;
/

/
QUIT
