CREATE OR REPLACE PROCEDURE "HELIX"."ZOEKNAAM"
As
  Cursor C1 is
     Select  PERS.PERS_ID,
             PERS.ACHTERNAAM an,
             PERS.ACHTERNAAM_PARTNER  anp,
             PERS.VOORLETTERS vl,
             PERS.VOORVOEGSEL  vv,
             PERS.VOORVOEGSEL_PARTNER  vp
      From kgc_personen pers ;
     c1_rec C1%rowtype;
     zknm  varchar2(200);
     i integer := 0;
Begin
   Open c1;
   Loop
      Fetch C1 into c1_rec ;
      If C1%notfound Then
         exit;
      end if;

      Zknm := c1_rec.an||';'||c1_rec.vl;

      If trim(c1_rec.anp) is not null Then
         if trim(c1_rec.vp) is not null Then
            zknm := zknm||c1_rec.vp||c1_rec.anp;
         else
            zknm := zknm||c1_rec.anp;
         end if;
      end if;

      if Trim(c1_rec.vv) is not null
      then
         zknm:= zknm||c1_rec.vv||c1_rec.an;
      else
         zknm:=zknm||c1_rec.an;

      End if;

      if i = 1000 then
         i:= 0;
         commit;
      end if;

      i := i + 1 ;

      update kgc_personen
      set KGC_PERSONEN.ZOEKNAAM = zknm
      where pers_id = c1_rec.pers_id;

   End loop;
   commit;

End;
/

/
QUIT
