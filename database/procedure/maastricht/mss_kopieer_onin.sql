CREATE OR REPLACE PROCEDURE "HELIX"."MSS_KOPIEER_ONIN" ( p_onderzoeknr_bron in  varchar
                                            , p_onderzoeknr_doel in  varchar
                                            , p_resultaat        out varchar
                                            )
as
  --
  -- Procedure zorgt ervoor dat de indicaties (KGC_ONDERZOEK_INDICATIES [ONIN])
  -- van een (basis)onderzoek van een bepaalde patient gekopieerd worden naar
  -- een ander (enzym)onderzoek van dezelfde patient.
  --
  cursor c_onde_bron(b_onderzoeknr in kgc_onderzoeken.onderzoeknr%type)
  is
    select onde.onde_id
    ,      onde.pers_id
    ,      onde.afgerond
    from   kgc_onderzoeken onde
    where  onde.onderzoeknr = b_onderzoeknr
  ;
  --
  cursor c_onde_doel(b_onderzoeknr in kgc_onderzoeken.onderzoeknr%type)
  is
    select onde.onde_id
    ,      onde.pers_id
    ,      onde.afgerond
    from   kgc_onderzoeken onde
    where  onde.onderzoeknr = b_onderzoeknr
  ;
  --
  cursor c_onin_bron(b_onderzoeknr in kgc_onderzoeken.onderzoeknr%type)
  is
    select onin.*
    ,      indi.code
    from   kgc_indicatie_groepen     ingr
    ,      kgc_indicatie_teksten     indi
    ,      kgc_onderzoek_indicaties  onin
    ,      kgc_onderzoeken           onde
    where  onde.onderzoeknr = b_onderzoeknr
    and    onin.onde_id = onde.onde_id
    and    indi.indi_id = onin.indi_id
    and    ingr.ingr_id = indi.ingr_id
    and    ingr.ongr_id = indi.ongr_id
    and    ingr.ongr_id = onde.ongr_id
  ;
  --
  cursor c_indi_doel( b_onderzoeknr in kgc_onderzoeken.onderzoeknr%type
                    , b_code        in kgc_indicatie_teksten.code%type
                    , b_onre_id     in kgc_onderzoek_indicaties.onre_id%type )
  is
    select indi.indi_id
    ,      ingr.ingr_id
    from   kgc_indicatie_groepen     ingr
    ,      kgc_indicatie_teksten     indi
    ,      kgc_onderzoeken           onde
    where  onde.onderzoeknr = b_onderzoeknr
    and    indi.code = b_code
    and    indi.kafd_id = onde.kafd_id
    and    ingr.ingr_id = indi.ingr_id
    and    ingr.ongr_id = indi.ongr_id
    and    ingr.ongr_id = onde.ongr_id
    and    not exists  -- De indicatie mag al niet hangen aan het doel onderzoek
             ( select null
               from   kgc_onderzoek_indicaties onin
               where  onin.onde_id = onde.onde_id
               and    onin.indi_id = indi.indi_id
             )
  ;
  --
  r_onde_bron_rec  c_onde_bron%rowtype;
  r_onde_doel_rec  c_onde_doel%rowtype;
  r_indi_doel_rec  c_indi_doel%rowtype;
  pl_teller        number(5);
begin
  --
  -- De volgende controles zijn van toepassing op de input parameters:
  --
  -- 1) Bron en doel onderzoek (p_onderzoeknr_bron en p_onderzoeknr_doel) mogen niet leeg zijn
  -- 2) Bron en doel onderzoek moeten bestaan
  -- 3) Bron en doel onderzoek moeten van dezelfde patient zijn
  -- 4) Bron en doel onderzoek mogen niet afgerond zijn
  --
  if p_onderzoeknr_bron is null
  then
    p_resultaat := 'Kopieer actie is niet gelukt: geen brononderzoeknummer meegegeven.';
  elsif p_onderzoeknr_doel is null
  then
    p_resultaat := 'Kopieer actie is niet gelukt: geen doelonderzoeknummer meegegeven.';
  else
    open  c_onde_bron(p_onderzoeknr_bron);
    fetch c_onde_bron
    into  r_onde_bron_rec;
    close c_onde_bron;
    --
    open  c_onde_doel(p_onderzoeknr_doel);
    fetch c_onde_doel
    into  r_onde_doel_rec;
    close c_onde_doel;
    --
    if r_onde_bron_rec.pers_id is null
    then
      p_resultaat := 'Kopieer actie is niet gelukt: brononderzoek bestaat niet.';
    elsif r_onde_bron_rec.afgerond = 'J'
    then
      p_resultaat := 'Kopieer actie is niet gelukt: brononderzoek is al afgerond.';
    elsif r_onde_doel_rec.pers_id is null
    then
      p_resultaat := 'Kopieer actie is niet gelukt: doelonderzoek bestaat niet.';
    elsif r_onde_doel_rec.afgerond = 'J'
    then
      p_resultaat := 'Kopieer actie is niet gelukt: doelonderzoek is al afgerond.';
    elsif r_onde_doel_rec.pers_id != r_onde_bron_rec.pers_id
    then
      p_resultaat := 'Kopieer actie is niet gelukt: bron en doelonderzoek zijn niet van dezelfde patient.';
    else
      pl_teller := 0;
      --
      for r_onin_bron_rec in c_onin_bron(p_onderzoeknr_bron)
      loop
        open  c_indi_doel( p_onderzoeknr_doel
                         , r_onin_bron_rec.code
                         , r_onin_bron_rec.onre_id);
        fetch c_indi_doel
        into  r_indi_doel_rec;
        --
        if c_indi_doel%FOUND
        then
          insert into kgc_onderzoek_indicaties
          ( ONDE_ID
          , INGR_ID
          , VOLGORDE
          , INDI_ID
          , OPMERKING
          , HOOFD_IND
          , ONRE_ID
          , DATUM
          )
          values
          ( r_onde_doel_rec.onde_id
          , null
          , r_onin_bron_rec.volgorde
          , r_indi_doel_rec.indi_id
          , r_onin_bron_rec.opmerking
          , r_onin_bron_rec.hoofd_ind
          , r_onin_bron_rec.onre_id
          , r_onin_bron_rec.datum
          );
          --
          pl_teller := pl_teller + 1;
          --
        else
          null;
        end if;
        --
        close c_indi_doel;
        --
      end loop;
      --
      p_resultaat := to_char(pl_teller)||' onderzoekindicatie record(s) gekopieerd.';
      commit;
    end if;
  end if;
  --
exception
  when others
  then p_resultaat := 'Kopieer actie is niet gelukt om onbekende reden.';
end mss_kopieer_onin;
/

/
QUIT
