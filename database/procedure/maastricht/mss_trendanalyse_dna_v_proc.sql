CREATE OR REPLACE PROCEDURE "HELIX"."MSS_TRENDANALYSE_DNA_V_PROC" ( p_begindatum     in date
                                                      , p_typebegindatum in varchar2
                                                      , p_einddatum      in date
                                                      , p_indicatie      in kgc_indicatie_teksten.code%type
                                                      , p_maxxwaarde     in number
                                                      , p_sessie_id      in varchar2
                                                      )
is
begin
  delete
  from   mss_trendanalyse_dna tren
  where  tren.sessie_id = p_sessie_id;
  --
  insert
  into   mss_trendanalyse_dna tren
  (
    SELECT onde.onde_id                                                             onde_id
    ,      onwy.omschrijving                                                        onderzoekswijze
    ,      indi_v.indi_code                                                         indicatiecode
    ,      onde.datum_autorisatie                                                   datum_autorisatie
    ,      onde.datum_binnen                                                        datum_binnen
    ,      trunc(mss_nominale_looptijd(indi.indi_id, onde.onderzoekswijze)/7) + 1   nom_doorlooptijd
    ,      trunc((onde.datum_autorisatie - audl.datum_tijd) /7) + 1                 looptijd_werkelijk
    ,      p_sessie_id                                                              sessie_id
    FROM   kgc_indicatie_teksten  indi
    ,      mss_indicatie_vw       indi_v
    ,      kgc_audit_log          audl
    ,      kgc_onderzoekswijzen   onwy
    ,      kgc_kgc_afdelingen     kafd
    ,      kgc_onderzoeken        onde
    WHERE  onde.datum_autorisatie is not null
    AND    decode( p_typebegindatum
                 , 'G', onde.datum_autorisatie
                 , 'I', onde.datum_binnen
                 , null
                 ) between p_begindatum
                   and     p_einddatum
    AND    kafd.kafd_id = onde.kafd_id
    AND    kafd.code = 'LAB'
    AND    onwy.code = onde.onderzoekswijze
    AND    onwy.kafd_id = onde.kafd_id
    AND    audl.id = onde.onde_id
    AND    audl.tabel_naam = 'KGC_ONDERZOEKEN'
    AND    audl.kolom_naam = 'STATUS'
    AND    audl.waarde_oud is null
    AND    audl.waarde_nieuw = 'G'
    AND    trunc((onde.datum_autorisatie - audl.datum_tijd) /7) + 1 <= p_maxxwaarde
    AND    indi_v.onde_id = onde.onde_id
    AND    (   indi_v.indi_code like p_indicatie
            or p_indicatie = '%'
           )
    AND    indi.ongr_id = onde.ongr_id
    AND    indi.code = indi_v.indi_code
  );
  --
  commit;
  --
end mss_trendanalyse_dna_v_proc;
/

/
QUIT
