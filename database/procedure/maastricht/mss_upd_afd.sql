CREATE OR REPLACE PROCEDURE "HELIX"."MSS_UPD_AFD" (Afd_oud in  integer, Afd_nieuw in  integer ) is

begin

   Update kgc_relaties t
   set    t.afdg_id = Afd_nieuw
   where  t.afdg_id = afd_oud;

   Commit;

   DELETE
   FROM    kgc_afdelingen t
   Where  t.afdg_id = afd_oud ;

   Commit;

end MSS_Upd_afd;
/

/
QUIT
