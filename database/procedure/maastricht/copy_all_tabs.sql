CREATE OR REPLACE PROCEDURE "HELIX"."COPY_ALL_TABS" IS
BEGIN
--done  copy_tab ('PHELIX', 'KGC_APPARATEN', 'OWNER');
--done  copy_tab ('PHELIX', 'KGC_CONCLUSIE_TEKSTEN', 'KAFD_ID');
--done  copy_tab ('PHELIX', 'kgc_materialen', 'KAFD_ID');
--done  copy_tab ('PHELIX', 'kgc_functies', 'OWNER');
  -- op andere manier -> via kgc_mede_kafd copy_tab('PHELIX', 'kgc_medewerkers','KAFD_ID');
/* done INSERT INTO kgc_medewerkers@PHELIX
    SELECT   mc.*
        FROM kgc_mede_kafd@chelix ac, kgc_medewerkers@chelix mc
       WHERE mc.mede_id NOT IN (1, 1300)
         AND ac.kafd_id = 48
         AND ac.mede_id = mc.mede_id
    ORDER BY mc.mede_id;

  copy_tab ('PHELIX',
            'kgc_mede_kafd',
            'KAFD_ID',
            ' AND MEDE_ID NOT IN (1,1300)'
           );

  INSERT INTO kgc_medewerker_menu@PHELIX
    SELECT   kgc_medewerker_menu.*
        FROM kgc_mede_kafd@chelix, kgc_medewerker_menu@chelix
       WHERE kgc_medewerker_menu.mede_id NOT IN (1, 1300)
         AND kafd_id = 48
         AND kgc_mede_kafd.mede_id = kgc_medewerker_menu.mede_id
    ORDER BY kgc_medewerker_menu.mede_id;

  copy_tab ('PHELIX', 'kgc_condities', 'OWNER');
  copy_tab ('PHELIX', 'kgc_cup_types', 'OWNER');

  copy_tab ('PHELIX', 'KGC_MATERIALEN', 'KAFD_ID');

  copy_tab ('PHELIX', 'kgc_meetwaardestructuur', 'OWNER');
  copy_tab ('PHELIX', 'KGC_MWST_SAMENSTELLING', 'OWNER');

  copy_tab ('PHELIX', 'kgc_module_toegang', 'OWNER');
  copy_tab ('PHELIX', 'kgc_onderzoek_uitslagcodes', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_onderzoeksgroepen', 'KAFD_ID');
  copy_tab ('PHELIX', 'bas_fractie_types', 'OWNER');
  copy_tab ('PHELIX', 'BAS_FRTY_CUTY', 'OWNER');
  copy_tab ('PHELIX', 'bas_stand_fractietypes', 'OWNER');

  copy_tab ('PHELIX', 'kgc_onderzoekswijzen', 'KAFD_ID');

  copy_tab ('PHELIX', 'KGC_NUMMERSTRUCTUUR', 'OWNER');
  copy_tab ('PHELIX', 'KGC_NUST_SAMENSTELLING', 'OWNER');
  copy_tab ('PHELIX', 'kgc_opslag_elementen', 'OWNER');
  copy_tab ('PHELIX', 'kgc_opslag_autorisatie', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_opslag_typen', 'OWNER');
  copy_tab ('PHELIX', 'kgc_opslag_elem_typen', 'OWNER');
  copy_tab ('PHELIX', 'kgc_resultaat_teksten', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_standaardteksten', 'KAFD_ID');

  copy_tab ('PHELIX', 'kgc_technieken', 'OWNER');
  copy_tab ('PHELIX', 'kgc_stoftestgroepen', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_stoftesten', 'KAFD_ID');


  copy_tab ('PHELIX', 'KGC_PROTOCOL_STOFTESTEN', 'OWNER');


  copy_tab ('PHELIX', 'kgc_tray_types', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_tray_grids', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_opslag_posities', 'OWNER');
  copy_tab ('PHELIX', 'kgc_opslag_structuren', 'OWNER');
  copy_tab ('PHELIX', 'kgc_redenen_niet_declarabel', 'OWNER');

  copy_tab ('PHELIX',
            'KGC_SYSTEEM_PAR_WAARDEN',
            'KAFD_ID',
            ' OR upper(WAARDE) like ''%DNA%'''
           );

  copy_tab ('PHELIX', 'KGC_MWSS_TOEGESTANE_WAARDEN', 'OWNER');
  copy_tab ('PHELIX', 'KGC_ETIKET_PARAMETERS', 'KAFD_ID');
  copy_tab ('PHELIX', 'KGC_PRINTERS', 'OWNER');
  copy_tab ('PHELIX', 'KGC_PROCESSEN', 'KAFD_ID');

  copy_tab ('PHELIX', 'bas_stand_fractietypes', 'OWNER');


  copy_tab ('PHELIX', 'KGC_PC_APPLICATIES', 'OWNER');
     */
  copy_tab ('PHELIX', 'KGC_BESTAND_TYPES', 'OWNER');
  copy_tab ('PHELIX', 'kgc_indicatie_groepen', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_indicatie_teksten', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_interface_definities', 'OWNER');
  copy_tab ('PHELIX', 'kgc_looptijden', 'OWNER');
  copy_tab ('PHELIX', 'kgc_stoftestgroep_gebruik', 'KAFD_ID');
  copy_tab ('PHELIX', 'kgc_teksten_context', 'KAFD_ID');

END;
/

/
QUIT
