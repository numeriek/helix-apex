CREATE OR REPLACE PROCEDURE "HELIX"."BEH_PREADTDATA"
IS
  --
  -- Script voor geautomatiseerde correctie van persoonsgegevens van v��r de ADT-koppelinglivegang.
  -- De KGC_PERSONEN-tabel dient gecorrigeerd te worden middels de data uit de in tabel gh_sapverify ingelezen excel-file sap dump voor helix.xlsx.
  -- MANTIS-vraag 0011088 oplossing. Bijwerken persoonsgegevens uit SAP
  --
  -- 2015-05-08 Versie 0.4      - Guido Herben - Voorwaarde alleen updaten records van v��r 15 april 2015 toegevoegd aan update-statement.
  -- 2015-04-30 Versie 0.3      - Guido Herben - Script opgeleverd met BSN-aanpassingen. Dit t.b.v. MANTIS-vraag 0011088, het bijwerken van persoonsgegevens uit SAP.
  -- 2015-04-30 Versie 0.2      - Guido Herben - Script opgeleverd met huisarts- en checkvolgorde-aanpassingen. Dit t.b.v. MANTIS-vraag 0011088, het bijwerken van persoonsgegevens uit SAP.
  -- 2015-04-29 Versie 0.1      - Guido Herben - Script opgeleverd t.b.v. MANTIS-vraag 0011088, het bijwerken van persoonsgegevens uit SAP.
  -- 2015-04-21 Versie 0.0.1    - Guido Herben - Initi�le script.
  --
  --
  -- Definieer cursoren
  --
  cursor c_sapv --SAP-brontabel-cursor
  is
    select sapv.*
      from GH_SAPVERIFY              sapv
     where sapv.opmerking is null
     order by sapv.patientnr
  ;
  --
  cursor c_cbeh -- Crosscheck behandelaar met KGC_RELATIES
    ( b_code                KGC_RELATIES.CODE%type
    )
  is
    select rela.rela_id
         , rela.code
      from KGC_RELATIES rela
     where lower(trim(rela.code)) = lower(b_code)
     ;
  --
  cursor c_cpin -- Crosscheck persoon in naamlijst ok.
    ( b_patientnr           KGC_PERSONEN.zisnr%type
 --   , b_geboortedatum       KGC_PERSONEN.geboortedatum%type
    , b_achternaam          KGC_PERSONEN.achternaam%type
    , b_meisjesnaam         KGC_PERSONEN.achternaam%type
    )
  is
    select pers.zisnr
         , pers.geboortedatum
         , pers.achternaam
         , pers.achternaam_partner
      from KGC_PERSONEN              pers
     where to_number(trim(pers.zisnr)) = to_number(trim(b_patientnr))
       and trim(pers.zisnr) is not null
 --      and pers.geboortedatum = b_geboortedatum
       and ((lower(b_achternaam) = trim(lower(pers.achternaam))
             or lower(b_achternaam) = trim(lower(pers.achternaam_partner)))
           or ((lower(b_meisjesnaam) = trim(lower(pers.achternaam))
             or lower(b_meisjesnaam) = trim(lower(pers.achternaam_partner)))))
     ;
  --
  cursor c_cpil -- Crosscheck persoon in lijst met geboortedatum.
    ( b_patientnr           KGC_PERSONEN.zisnr%type
    , b_geboortedatum       KGC_PERSONEN.geboortedatum%type)
  is
    select pers.zisnr
         , pers.geboortedatum
         , pers.achternaam
         , pers.achternaam_partner
      from KGC_PERSONEN              pers
     where to_number(trim(pers.zisnr)) = to_number(trim(b_patientnr))
       and trim(pers.zisnr) is not null
       and pers.geboortedatum = b_geboortedatum
     ;
  --
  --
  -- Definieer rowcounters
  --
  r_cbeh_rec c_cbeh%rowtype;
  r_cpin_rec c_cpin%rowtype;
  r_cpil_rec c_cpil%rowtype;
  --
  -- Definieer locale variabelen
  --
  pl_errnote   varchar2(100);               --errornote
  pl_pers_id   KGC_PERSONEN.pers_id%type;     --persoonstype uit persoonstabel. Bewaar persoon.
  pl_zisnr     KGC_PERSONEN.zisnr%type;       --persoonsnummer ZIS. Bewaar ZISnr HELIXtabel.
  pl_patnr     GH_SAPVERIFY.patientnr%type; --sapnummer ZIS. Bewaar ZISnr SAPtabel.
  pl_relatie   KGC_RELATIES.RELA_ID%type;     --relatienummer uit relatietabel. Bewaar rela_id.
  pl_prgcount  numeric(10);                 --tellertje voor aantal loops.
  --
  -- Programma
  --
  -- Pre-update-checks
  -- 01 - Als refpat is ingevuld in XLS dan betreffend record parkeren.
  -- 02 - Match huisarts als relatie.
  -- 03 - Is meisjesnaam ingevuld? Zo ja, check of deze overeenkomt met Helix achternaam
  -- 04 - Match geboortedatum, niet ok als naam niet ok dan record parkeren, anders doorvoeren.
  -- 05 - Check achternaam met achternaam indien geen meisjesnaam en anders met achternaam_partner
  -- 06 - Tellertje commit per 500 records...


BEGIN
  pl_prgcount:=0;
  for r_sapv_rec in c_sapv
  loop
    pl_errnote:=NULL ;
    pl_pers_id:=NULL ;
    pl_zisnr:=NULL ;
    pl_patnr:=to_number(trim(r_sapv_rec.patientnr)) ;
    pl_relatie:=NULL ;
    if trim(r_sapv_rec.refpat) is not NULL -- Check op aanwezigheid refererend zisnr
      then
        pl_errnote:='Dit record is vervangen door record ' || to_char(r_sapv_rec.refpat) || ' uit de kolom refpat. ' ;
    end if;
    --
    if pl_errnote is NULL -- Check op aanwezigheid relatie in relatietabel
    then
      if trim(r_sapv_rec.huisarts) is NULL -- Check of huisarts is ingevuld in SAPtabel
        then
          pl_relatie:=NULL;
        else
          BEGIN
            open c_cbeh( trim(r_sapv_rec.huisarts)
                       );
            fetch c_cbeh
            into r_cbeh_rec;
            if c_cbeh%NOTFOUND
              then
                pl_errnote:='Niet-bestaande huisarts ingevuld bij record ' || to_char(r_sapv_rec.patientnr) || '. ' ;
              else
                pl_relatie:=trim(r_cbeh_rec.rela_id) ;
            end if;
            close c_cbeh;
          END;
        end if;
    end if;
    --
    if pl_errnote is NULL
    then
      open c_cpin( r_sapv_rec.patientnr
--                 , to_date(to_char(substr(trim(r_sapv_rec.geboortedatum),1,2)||'-'||substr(trim(r_sapv_rec.geboortedatum),4,2)||'-'||substr(trim(r_sapv_rec.geboortedatum),7,4)),'dd-mm-yyyy')
                 , trim(r_sapv_rec.achternaam)
                 , trim(r_sapv_rec.meisjesnaam)
                 );
      fetch c_cpin
      into r_cpin_rec;
      if c_cpin%NOTFOUND
      then
        open c_cpil( r_sapv_rec.patientnr
                   , to_date(to_char(substr(trim(r_sapv_rec.geboortedatum),1,2)||'-'||substr(trim(r_sapv_rec.geboortedatum),4,2)||'-'||substr(trim(r_sapv_rec.geboortedatum),7,4)),'dd-mm-yyyy') -- Kan effici�nter to_char e.d.
                   );
          fetch c_cpil
          into r_cpil_rec;
          if c_cpil%NOTFOUND
          then
            pl_errnote:='Persoon verschilt qua naam en geboortedatum ' || to_char(r_sapv_rec.patientnr) || '. ' ;
          else
            pl_errnote:='Persoon verschilt qua naam bij record ' || to_char(r_sapv_rec.patientnr) || '. ' ;
        end if;
        close c_cpil;
      else
          pl_patnr:=r_sapv_rec.patientnr ;
      end if;
      close c_cpin;
    else
      pl_patnr:=r_sapv_rec.patientnr ;
    end if;
    --
    if pl_errnote is not NULL
      then -- Er is een foutief record gevonden, dus voer bericht in in SAPVERIFYtabel.
        update GH_SAPVERIFY
           set OPMERKING = pl_errnote
         where to_number(trim(patientnr)) = to_number(trim(pl_patnr))
        ;
      else
        update KGC_PERSONEN -- Update in KGC_PERSONEN als record wel ok.
           set achternaam =
               case
                 when trim(r_sapv_rec.meisjesnaam) is null
                   then trim(r_sapv_rec.achternaam)
                 when trim(r_sapv_rec.meisjesnaam) is not null
                   then trim(r_sapv_rec.meisjesnaam)
               end
             , voorletters = r_sapv_rec.voornm_of_lt
             , voorvoegsel =
               case
                 when trim(r_sapv_rec.meisjesnaam) is null
                   then r_sapv_rec.voorv_achn
                 when trim(r_sapv_rec.meisjesnaam) is not null
                   then r_sapv_rec.voorv_meisjesnaam
               end
             , geslacht =
               case
                 when to_number(r_sapv_rec.sexe) = 1
                   then 'M'
                 when to_number(r_sapv_rec.sexe) = 2
                   then 'V'
                 when to_number(r_sapv_rec.sexe) = 3
                   then 'O'
               end
             , geboortedatum = to_date(r_sapv_rec.geboortedatum, 'dd-mm-yyyy')
             , overleden =
               case
                 when r_sapv_rec.overleden = 'X'
                   then 'J'
                 else 'N'
               end
             , overlijdensdatum = to_date(r_sapv_rec.datum_overleden, 'dd-mm-yyyy')
             , gehuwd =
               case
                 when to_number(r_sapv_rec.burg_stand) = 0
                   then 'N'
                 when to_number(r_sapv_rec.burg_stand) = 1
                   then 'J'
                 when trim(r_sapv_rec.meisjesnaam) is null
                   then 'N'
                 else 'J'
               end
             , achternaam_partner =
               case
                 when trim(r_sapv_rec.meisjesnaam) is not null
                   then (r_sapv_rec.achternaam)
                 else NULL
               end
             , voorvoegsel_partner =
               case
                 when trim(r_sapv_rec.meisjesnaam) is not null
                   then r_sapv_rec.voorv_achn
                   else NULL
               end
             , meerling =
               case
                 when r_sapv_rec.meerling = 'X'
                   then 'J'
                 else 'N'
               end
             , rela_id = pl_relatie
             , adres = r_sapv_rec.straat_met_huisnr
             , woonplaats =
                case
                  when length(trim(r_sapv_rec.postcode))>7
                    then trim(to_char(substr(trim(r_sapv_rec.postcode),8,length(trim(r_sapv_rec.postcode))-7))) || ' ' || trim(r_sapv_rec.plaats)
                  when length(trim(r_sapv_rec.postcode)) < 8
                    then trim(r_sapv_rec.plaats)
                end
             , postcode = substr(trim(r_sapv_rec.postcode),1,7)
             , land = r_sapv_rec.land
             , telefoon = r_sapv_rec.telefoon
             , bsn =
                 case
                   when trim(r_sapv_rec.sts) = 'A'
                     then
                       case
                         when length(to_number(r_sapv_rec.bsn)) = 8
                           then
                             case
                               when mod((8*substr(to_number(r_sapv_rec.bsn),1,1))
                                       +(7*substr(to_number(r_sapv_rec.bsn),2,1))
                                       +(6*substr(to_number(r_sapv_rec.bsn),3,1))
                                       +(5*substr(to_number(r_sapv_rec.bsn),4,1))
                                       +(4*substr(to_number(r_sapv_rec.bsn),5,1))
                                       +(3*substr(to_number(r_sapv_rec.bsn),6,1))
                                       +(2*substr(to_number(r_sapv_rec.bsn),7,1))
                                       -  (substr(to_number(r_sapv_rec.bsn),8,1)),11) = 0
                                 then to_number(r_sapv_rec.bsn)
                                 else NULL
                             end
                         when length(to_number(r_sapv_rec.bsn)) = 9
                           then
                             case
                               when mod((9*substr(to_number(BSN),1,1))
                                     +(8*substr(to_number(BSN),2,1))
                                     +(7*substr(to_number(BSN),3,1))
                                     +(6*substr(to_number(BSN),4,1))
                                     +(5*substr(to_number(BSN),5,1))
                                     +(4*substr(to_number(BSN),6,1))
                                     +(3*substr(to_number(BSN),7,1))
                                     +(2*substr(to_number(BSN),8,1))
                                     -  (substr(to_number(BSN),9,1)),11) = 0
                                 then to_number(r_sapv_rec.bsn)
                                 else NULL
                             end
                         else NULL
                       end
                     else NULL
                 end
             , bsn_geverifieerd =
                 case
                   when trim(r_sapv_rec.sts) = 'A'
                     then
                       case
                         when length(to_number(r_sapv_rec.bsn)) = 8
                           then
                             case
                               when mod((8*substr(to_number(r_sapv_rec.bsn),1,1))
                                       +(7*substr(to_number(r_sapv_rec.bsn),2,1))
                                       +(6*substr(to_number(r_sapv_rec.bsn),3,1))
                                       +(5*substr(to_number(r_sapv_rec.bsn),4,1))
                                       +(4*substr(to_number(r_sapv_rec.bsn),5,1))
                                       +(3*substr(to_number(r_sapv_rec.bsn),6,1))
                                       +(2*substr(to_number(r_sapv_rec.bsn),7,1))
                                       -  (substr(to_number(r_sapv_rec.bsn),8,1)),11) = 0
                                 then 'J'
                                 else 'N'
                               end
                         when length(to_number(r_sapv_rec.bsn)) = 9
                           then
                             case
                               when mod((9*substr(to_number(BSN),1,1))
                                     +(8*substr(to_number(BSN),2,1))
                                     +(7*substr(to_number(BSN),3,1))
                                     +(6*substr(to_number(BSN),4,1))
                                     +(5*substr(to_number(BSN),5,1))
                                     +(4*substr(to_number(BSN),6,1))
                                     +(3*substr(to_number(BSN),7,1))
                                     +(2*substr(to_number(BSN),8,1))
                                     -  (substr(to_number(BSN),9,1)),11) = 0
                                 then 'J'
                                 else 'N'
                               end
                         else 'N'
                       end
                     else 'N'
                 end
             , beheer_in_zis = 'J' -- aanzetten beheer in ZIS
             , last_update_date = sysdate
         where to_number(trim(zisnr)) = to_number(trim(pl_patnr))
           and last_update_date < to_date('15-04-2015','dd-mm-yyyy')
      ;
    end if;
    --
    pl_prgcount:=pl_prgcount + SQL%rowcount;
    --
    if pl_prgcount >= 500
    then
      BEGIN
        pl_prgcount := 0;
        commit;
      END;
    end if;
    --
  end loop;
  --
  commit;
END beh_preadtdata;
/

/
QUIT
