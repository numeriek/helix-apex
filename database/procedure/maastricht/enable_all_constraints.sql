CREATE OR REPLACE PROCEDURE "HELIX"."ENABLE_ALL_CONSTRAINTS" IS
  CURSOR c_uk_constraints IS
    SELECT constraint_name, table_name
      FROM user_constraints
     WHERE constraint_type IN ('U', 'P')     -- R: FK, U: UK, P: PK, C: Check
       AND status = 'DISABLED'
       AND constraint_name NOT LIKE 'BIN$%';

  CURSOR c_other_constraints IS
    SELECT constraint_name, table_name
      FROM user_constraints
     WHERE constraint_type NOT IN ('U', 'P')  -- R: FK, U: UK, P: PK, C: Check
       AND status = 'DISABLED'
       AND constraint_name NOT LIKE 'BIN$%';
BEGIN
  EXECUTE IMMEDIATE 'truncate table exceptions';

-- UK/PK's eerst!!
  FOR r_cons IN c_uk_constraints
  LOOP
    BEGIN
      EXECUTE IMMEDIATE    'alter table '
                        || r_cons.table_name
                        || ' enable constraint "'
                        || r_cons.constraint_name
                        || '" exceptions into exceptions';
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;                                              -- gewoon doorgaan
    END;
  END LOOP;

  FOR r_cons IN c_other_constraints
  LOOP
    BEGIN
      EXECUTE IMMEDIATE    'alter table '
                        || r_cons.table_name
                        || ' enable constraint "'
                        || r_cons.constraint_name
                        || '" exceptions into exceptions';
    EXCEPTION
      WHEN OTHERS
      THEN
        NULL;                                              -- gewoon doorgaan
    END;
  END LOOP;
END;
/

/
QUIT
