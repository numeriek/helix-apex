CREATE OR REPLACE PROCEDURE "HELIX"."DISABLE_ALL_CONSTRAINTS" IS
  CURSOR c_fk_constraints IS
    SELECT constraint_name, table_name
      FROM user_constraints
     WHERE constraint_type = 'R'             -- R: FK, U: UK, P: PK, C: Check
       AND status = 'ENABLED'
       AND constraint_name NOT LIKE 'BIN$%';

  CURSOR c_other_constraints IS
    SELECT constraint_name, table_name
      FROM user_constraints
     WHERE constraint_type <> 'R'
       AND status = 'ENABLED'
       AND constraint_name NOT LIKE 'BIN$%';

  v_sql   VARCHAR2 (200);
BEGIN
-- FK's eerst!!
  FOR r_cons IN c_fk_constraints
  LOOP
    v_sql :=
         'alter table '
      || r_cons.table_name
      || ' disable constraint "'
      || r_cons.constraint_name
      || '"';

    BEGIN
      EXECUTE IMMEDIATE v_sql;
    EXCEPTION
      WHEN OTHERS
      THEN
        DBMS_OUTPUT.put_line (v_sql);
    END;
  END LOOP;

  FOR r_cons IN c_other_constraints
  LOOP
    v_sql :=
         'alter table '
      || r_cons.table_name
      || ' disable constraint "'
      || r_cons.constraint_name
      || '"';

    BEGIN
      EXECUTE IMMEDIATE v_sql;
    EXCEPTION
      WHEN OTHERS
      THEN
        DBMS_OUTPUT.put_line (v_sql);
    END;
  END LOOP;
END;
/

/
QUIT
