CREATE OR REPLACE PROCEDURE "HELIX"."MSS_STOP_HL7" is
begin
  kgc_zis.beheer_hl7(p_action => 'E', p_check_email => 'N');

  Insert Into MSS_interface_log
    (Hl7logid, Info, Date_Created)
  Values
    (MSS_LOGID.NEXTVAL,
     'HL7_Luister Proces gecontroleert gestopt',
     sysdate);
  commit;

end MSS_Stop_HL7;
/

/
QUIT
