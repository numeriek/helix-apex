CREATE OR REPLACE PROCEDURE "HELIX"."MSS_VERW_MUT_SURVEYOR" ( p_bestandsnaam in varchar2
                                                       , p_sessie       in varchar2
                                                       , p_progvariant  in varchar2
                                                       )
is
  --
  -- Programmavariant (parameter p_progvariant) geeft aan welke variant van het programma gedraaid moet worden.
  -- Op dit moment zijn er twee programmavarianten, A en B.
  -- A: Posities van samples in de file komen overeen met de positie in de werklijst
  -- B: Posities van samples in de file komen niet overeen met de positie in de werklijst
  --
  cursor c_wlst( b_werklijst_nummer in kgc_werklijsten.werklijst_nummer%type
               )
  is
    select wlst.*
    from   kgc_kgc_afdelingen  kafd
    ,      kgc_werklijsten     wlst
    where  upper(wlst.werklijst_nummer) = upper(b_werklijst_nummer)
    --and    wlst.kafd_id = kafd.kafd_id -- verwijderd omdat de interface ook bij METAB nodig is
    --and    kafd.code = 'DNA'
  ;
  --
  cursor c_muts( b_sessie in mss_mutation_surveyor.sessie%type
               )
  is
    select muts.*
    ,      decode( substr(muts.sample_name,4,1)
                 , '0', substr(muts.sample_name,5,1)
                 , substr(muts.sample_name,4,2)
                 )                                          x_pos
    ,      substr(muts.sample_name,3,1)                     y_pos
    ,      instr(upper(muts.sample_name), '_F_')            f_pos
    ,      instr(upper(muts.sample_name), '_R_')            r_pos
    ,      decode(muts.variant1,null,'LEEG','GEVULD')       status_variant1
    ,      decode(muts.variant2,null,'LEEG','GEVULD')       status_variant2
    ,      decode(muts.variant3,null,'LEEG','GEVULD')       status_variant3
    ,      decode(muts.variant4,null,'LEEG','GEVULD')       status_variant4
    ,      decode(muts.variant5,null,'LEEG','GEVULD')       status_variant5
    from   mss_mutation_surveyor muts
    where  muts.sessie = b_sessie
  ;
  --
  cursor c_meetA( b_wlst_id   in kgc_werklijsten.wlst_id%type
                , b_x_positie in kgc_tray_vulling.x_positie%type
                , b_y_positie in kgc_tray_vulling.y_positie%type
                )
  is
    select meet.*
    ,      replace(upper(frac.fractienummer),'DNA','') fracnr_zdna
    from   bas_fracties         frac
    ,      bas_metingen         meti
    ,      bas_meetwaarden      meet
    ,      kgc_tray_vulling     trvu
    ,      kgc_werklijst_items  wlit
    where  wlit.wlst_id = b_wlst_id
    and    trvu.meet_id = wlit.meet_id
    and    trvu.x_positie = b_x_positie
    and    trvu.y_positie = b_y_positie
    and    meet.meet_id = wlit.meet_id
    and    meet.afgerond != 'J' -- Als meetwaarde al op afgerond staat, moeten de bijbehorende mdet niet meer gevuld worden
    and    meti.meti_id = meet.meti_id
    and    frac.frac_id (+) = meti.frac_id
    for update of meet.afgerond nowait
  ;
  --
  cursor c_meetB( b_wlst_id     in kgc_werklijsten.wlst_id%type
                , b_sample_name in mss_mutation_surveyor.sample_name%type
                )
  is
    select meet.*
    ,      null fracnr_zdna -- dummy kolom
    from   bas_fracties         frac
    ,      bas_metingen         meti
    ,      kgc_stoftesten       stof
    ,      bas_meetwaarden      meet
    ,      kgc_werklijst_items  wlit
    where  wlit.wlst_id = b_wlst_id
    and    meet.meet_id = wlit.meet_id
    and    meet.afgerond != 'J' -- Als meetwaarde al op afgerond staat, moeten de bijbehorende mdet niet meer gevuld worden
    and    stof.stof_id = meet.stof_id
    and    meti.meti_id = meet.meti_id
    and    frac.frac_id = meti.frac_id
    and    instr(b_sample_name, frac.fractienummer) > 0
    and    instr(b_sample_name, stof.omschrijving) > 0
    for update of meet.afgerond nowait
  ;
  --
  r_wlst1_rec                 c_wlst%rowtype;
  r_wlst2_rec                 c_wlst%rowtype;
  r_meet_rec                  c_meetA%rowtype;
  pl_meet_rec_found           boolean;
  pl_aant_werklijsten         number(1);
  pl_werklijst_nummer1        kgc_werklijsten.werklijst_nummer%type;
  pl_werklijst_nummer2        kgc_werklijsten.werklijst_nummer%type;
  pl_wlst_id                  kgc_werklijsten.wlst_id%type;
  pl_punt_pos                 number(2);
  pl_underscore_pos           number(2);
  pl_comma_pos                number(2);
  pl_quotjes_pos              number(2);
  pl_groter_dan_pos           number(2);
  pl_p_punt_pos               number(2);
  pl_1e_spatie_na_p_punt_pos  number(2);
  pl_1e_spatie_na_gr_dan_pos  number(2);
  pl_1e_comma_na_p_punt_pos   number(2);
  pl_least_spatie_comma_pos   number(2);
  pl_eff_vd_afwijking         varchar2(40);
  pl_eff_vd_afw_deel1         varchar2(40);
  pl_eff_vd_afw_deel2         varchar2(40);
  pl_waarde                   bas_meetwaarde_details.waarde%type;
  pl_eerste_var               mss_mutation_surveyor.variant1%type  := null;
  pl_tweede_var               mss_mutation_surveyor.variant1%type  := null;
  pl_derde_var                mss_mutation_surveyor.variant1%type  := null;
  pl_aant_verw_mut            number;
  --
  procedure update_mdet( p_meet_id in bas_meetwaarden.meet_id%type
                       , p_prompt  in bas_meetwaarde_details.prompt%type
                       , p_waarde  in bas_meetwaarde_details.waarde%type
                       )
  is
    cursor c_mdet ( b_meet_id in bas_meetwaarden.meet_id%type
                  , b_prompt  in bas_meetwaarde_details.prompt%type
                  )
    is
      select mdet.*
      from   bas_meetwaarde_details mdet
      where  mdet.meet_id = b_meet_id
      and    mdet.prompt = b_prompt
      for update of mdet.waarde nowait
    ;
    --
  begin
    for r_mdet_rec in c_mdet( p_meet_id
                            , p_prompt
                            )
    loop
      update bas_meetwaarde_details mdet
      set    mdet.waarde = trim(p_waarde)
      where  current of c_mdet;
    end loop;
  end update_mdet;
  --
begin
  --
  -- Inhoud van de input parameter p_bestandsnaam kan 2 vormen aannemen
  -- <werklijstnummer>.csv
  -- <werklijstnummer>_<werklijstnummer>.csv als samples van 2 werklijsten in 1 Mutation Surveyorfile voorkomen
  --
  pl_punt_pos          := instr(p_bestandsnaam, '.');
  pl_underscore_pos    := instr(p_bestandsnaam, '_');
  --
  if pl_underscore_pos > 0 -- p_bestandsnaam is van de vorm <werklijstnummer>_<werklijstnummer>.csv
  then
    pl_werklijst_nummer1 := substr(p_bestandsnaam, 1, pl_underscore_pos - 1);
    pl_werklijst_nummer2 := substr(p_bestandsnaam, pl_underscore_pos + 1, pl_punt_pos - pl_underscore_pos - 1);
    pl_aant_werklijsten  := 2;
  else -- p_bestandsnaam is van de vorm <werklijstnummer>.csv
    pl_werklijst_nummer1 := substr(p_bestandsnaam, 1, pl_punt_pos - 1);
    pl_werklijst_nummer2 := null;
    pl_aant_werklijsten  := 1;
  end if;
  --
  open  c_wlst(pl_werklijst_nummer1);
  fetch c_wlst
  into  r_wlst1_rec;
  close c_wlst;
  --
  if pl_aant_werklijsten = 2
  then
    open  c_wlst(pl_werklijst_nummer2);
    fetch c_wlst
    into  r_wlst2_rec;
    close c_wlst;
  else
    null;
  end if;
  --
  if (   r_wlst1_rec.wlst_id is null -- Werklijst1 is niet gevonden)
      or (    r_wlst2_rec.wlst_id is null
          and pl_aant_werklijsten = 2
         )-- Er zijn 2 werklijsten en Werklijst2 is niet gevonden
     )
  then
    null;
  else -- Werklijst(ten) is/zijn gevonden
    --
    -- Maak eerst alle waarden in de meetwaarde details leeg die horen bij de werklijst(en)
    --
    update bas_meetwaarde_details mdet2
    set    mdet2.waarde = null
    where  mdet2.mdet_id in
             ( select mdet.mdet_id
               from   bas_meetwaarde_details  mdet
               ,      bas_meetwaarden         meet
               ,      kgc_werklijst_items     wlit
               where  (   wlit.wlst_id = r_wlst1_rec.wlst_id
                       or (    pl_aant_werklijsten = 2
                           and wlit.wlst_id = r_wlst2_rec.wlst_id
                          )
                      )
               and    meet.meet_id = wlit.meet_id
               and    meet.afgerond != 'J' -- Als meetwaarde al op afgerond staat, moeten de bijbehorende mdet niet meer gevuld worden
               and    mdet.meet_id = meet.meet_id
             );
    --
    -- Gooi alle records weg die niet verwerkt hoeven worden.
    -- De enige records die bruikbaar zijn beginnen met een getal in de eerste kolom (trace_nr).
    --
    delete
    from   mss_mutation_surveyor muts
    where  muts.sessie = p_sessie
    and    muts.trace_nr not in (select to_char(waarde) from mss_waarden_1_1000);
    --
    -- Indien programma variant B, gooi alle records waarvan de combinatie fractienummer en stoftestomschrijving meer dan 1 maal voorkomt, weg.
    -- Stoftestomschrijving begint met SEQ en eindigt op of _R of _F.
    --
    if p_progvariant = 'B'
    then
      --
      -- Stoftest eindigt op _F
      --
      delete
      from   mss_mutation_surveyor muts
      where  ( decode( instr(muts.sample_name, '-', instr(muts.sample_name,'DNA'), 1) - instr(muts.sample_name, 'DNA')
                     , 5, substr(muts.sample_name, instr(muts.sample_name,'DNA'), 10) -- Format fractienummer is DNAyy-####
                     , 7, substr(muts.sample_name, instr(muts.sample_name,'DNA'), 12) -- Format fractienummer is DNAyyyy-####
                     )
               || substr(muts.sample_name, instr(muts.sample_name,'SEQ'), instr(muts.sample_name,'_F', instr(muts.sample_name,'SEQ'), 1))
             )
             in
             (
               select decode( instr(muts2.sample_name, '-', instr(muts2.sample_name,'DNA'), 1) - instr(muts2.sample_name, 'DNA')
                            , 5, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 10) -- Format fractienummer is DNAyy-####
                            , 7, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 12) -- Format fractienummer is DNAyyyy-####
                            )
                      || substr(muts2.sample_name, instr(muts2.sample_name,'SEQ'), instr(muts2.sample_name,'_F', instr(muts2.sample_name,'SEQ'), 1))
               from   mss_mutation_surveyor muts2
               where  muts2.sessie = p_sessie
               and    instr(muts2.sample_name,'_F', instr(muts2.sample_name,'SEQ'), 1) > 0
               group
               by     decode( instr(muts2.sample_name, '-', instr(muts2.sample_name,'DNA'), 1) - instr(muts2.sample_name, 'DNA')
                            , 5, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 10) -- Format fractienummer is DNAyy-####
                            , 7, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 12) -- Format fractienummer is DNAyyyy-####
                            )
                      || substr(muts2.sample_name, instr(muts2.sample_name,'SEQ'), instr(muts2.sample_name,'_F', instr(muts2.sample_name,'SEQ'), 1))
               having count(1) > 1
             );
      --
      -- Stoftest eindigt op _R
      --
      delete
      from   mss_mutation_surveyor muts
      where  ( decode( instr(muts.sample_name, '-', instr(muts.sample_name,'DNA'), 1) - instr(muts.sample_name, 'DNA')
                     , 5, substr(muts.sample_name, instr(muts.sample_name,'DNA'), 10) -- Format fractienummer is DNAyy-####
                     , 7, substr(muts.sample_name, instr(muts.sample_name,'DNA'), 12) -- Format fractienummer is DNAyyyy-####
                     )
               || substr(muts.sample_name, instr(muts.sample_name,'SEQ'), instr(muts.sample_name,'_R', instr(muts.sample_name,'SEQ'), 1))
             )
             in
             (
               select decode( instr(muts2.sample_name, '-', instr(muts2.sample_name,'DNA'), 1) - instr(muts2.sample_name, 'DNA')
                            , 5, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 10) -- Format fractienummer is DNAyy-####
                            , 7, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 12) -- Format fractienummer is DNAyyyy-####
                            )
                      || substr(muts2.sample_name, instr(muts2.sample_name,'SEQ'), instr(muts2.sample_name,'_R', instr(muts2.sample_name,'SEQ'), 1))
               from   mss_mutation_surveyor muts2
               where  muts2.sessie = p_sessie
               and    instr(muts2.sample_name,'_R', instr(muts2.sample_name,'SEQ'), 1) > 0
               group
               by     decode( instr(muts2.sample_name, '-', instr(muts2.sample_name,'DNA'), 1) - instr(muts2.sample_name, 'DNA')
                            , 5, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 10) -- Format fractienummer is DNAyy-####
                            , 7, substr(muts2.sample_name, instr(muts2.sample_name,'DNA'), 12) -- Format fractienummer is DNAyyyy-####
                            )
                      || substr(muts2.sample_name, instr(muts2.sample_name,'SEQ'), instr(muts2.sample_name,'_R', instr(muts2.sample_name,'SEQ'), 1))
               having count(1) > 1
             );
      --
    else -- if p_progvariant = 'B'
      null;
    end if;
    --
    for r_muts_rec in c_muts(p_sessie)
    loop
      --
      -- Bepaal welke variantkolommen zijn gevuld. Alleen de eerste drie mutaties (gevulde variantkolom) worden verwerkt.
      -- pl_eerste_var wordt gevuld met de eerste gevulde variantkolom (gezien van links naar rechts, van variant1 t/m variant5)
      -- pl_tweede_var wordt gevuld met de tweede gevulde variantkolom (gezien van links naar rechts, van variant1 t/m variant5)
      -- pl_derde_var  wordt gevuld met de derde  gevulde variantkolom (gezien van links naar rechts, van variant1 t/m variant5)
      --
      pl_aant_verw_mut := 0;
      --
      if r_muts_rec.status_variant1 = 'GEVULD'
      then
        pl_aant_verw_mut := pl_aant_verw_mut + 1;
        pl_eerste_var := r_muts_rec.variant1;
      end if;
      --
      if r_muts_rec.status_variant2 = 'GEVULD'
      then
        pl_aant_verw_mut := pl_aant_verw_mut + 1;
        if pl_aant_verw_mut = 1
        then
          pl_eerste_var := r_muts_rec.variant2;
        else
          pl_tweede_var := r_muts_rec.variant2;
        end if;
      end if;
      --
      if r_muts_rec.status_variant3 = 'GEVULD'
      then
        pl_aant_verw_mut := pl_aant_verw_mut + 1;
        if pl_aant_verw_mut = 1
        then
          pl_eerste_var := r_muts_rec.variant3;
        elsif pl_aant_verw_mut = 2
        then
          pl_tweede_var := r_muts_rec.variant3;
        else
          pl_derde_var := r_muts_rec.variant3;
        end if;
      end if;
      --
      if pl_aant_verw_mut < 3
      then
        if r_muts_rec.status_variant4 = 'GEVULD'
        then
          pl_aant_verw_mut := pl_aant_verw_mut + 1;
          if pl_aant_verw_mut = 1
          then
            pl_eerste_var := r_muts_rec.variant4;
          elsif pl_aant_verw_mut = 2
          then
            pl_tweede_var := r_muts_rec.variant4;
          else
            pl_derde_var := r_muts_rec.variant4;
          end if;
        end if;
      end if;
      --
      if pl_aant_verw_mut < 3
      then
        if r_muts_rec.status_variant5 = 'GEVULD'
        then
          pl_aant_verw_mut := pl_aant_verw_mut + 1;
          if pl_aant_verw_mut = 1
          then
            pl_eerste_var := r_muts_rec.variant5;
          elsif pl_aant_verw_mut = 2
          then
            pl_tweede_var := r_muts_rec.variant5;
          else
            pl_derde_var := r_muts_rec.variant5;
          end if;
        end if;
      end if;
      --
      if pl_aant_werklijsten = 1
      then
        pl_wlst_id := r_wlst1_rec.wlst_id;
      else
        if  r_muts_rec.f_pos > 0
        and r_muts_rec.r_pos = 0
        then
          --
          -- Traces die _F_ bevatten moet verwerkt worden in 1e werklijst
          --
          pl_wlst_id := r_wlst1_rec.wlst_id;
          --
        elsif r_muts_rec.f_pos = 0
        and   r_muts_rec.r_pos > 0
        then
          --
          -- Traces die _R_ bevatten moet verwerkt worden in 2e werklijst
          --
          pl_wlst_id := r_wlst2_rec.wlst_id;
          --
        else
          pl_wlst_id := null;
        end if;
      end if;
      --
      if pl_wlst_id is null
      then
        null;
      else
        if p_progvariant = 'A'
        then
          open  c_meetA( pl_wlst_id
                       , r_muts_rec.x_pos
                       , r_muts_rec.y_pos
                       );
          fetch c_meetA
          into  r_meet_rec;
          pl_meet_rec_found := c_meetA%FOUND;
        else
          open  c_meetB( pl_wlst_id
                       , r_muts_rec.sample_name
                       );
          fetch c_meetB
          into  r_meet_rec;
          pl_meet_rec_found := c_meetB%FOUND;
        end if;
        --
        if pl_meet_rec_found
        then
          --
          -- Controle match DNA nummer (alleen voor programma variant A):
          -- BASFRACTIES.FRACTIENUMMER dat correspondeert met de aangegeven plek op de werklijst moet ook
          -- in de regel in het csv-bestand van Mutation Surveyor voorkomen. Hierbij moeten echter de letters
          -- 'DNA' van het fractienummer worden afgeknipt. De overblijvende string moet dus worden teruggevonden
          -- in de samplenaam van Mutation Surveyor.
          --
          if (upper(r_muts_rec.reference_name) = 'UNMATCHED')
          or ((instr(r_muts_rec.sample_name, r_meet_rec.fracnr_zdna) = 0) and p_progvariant = 'A')  -- Geen match op DNA nummer
          or (    r_muts_rec.variant1 is not null
              and r_muts_rec.variant2 is not null
              and r_muts_rec.variant3 is not null
              and r_muts_rec.variant4 is not null
             ) -- Alle 4 variantkolommen zijn gevuld
          then
            update_mdet(r_meet_rec.meet_id, 'OK?', 'N');
          else
            update_mdet(r_meet_rec.meet_id, 'OK?', 'J');
            --
            if r_muts_rec.number_of_mutations = 0
            then
              update_mdet(r_meet_rec.meet_id, 'Afwijking?', 'N');
              update_mdet(r_meet_rec.meet_id, 'Soort afw.', null);
              update_mdet(r_meet_rec.meet_id, 'Soort afwijking (2e)', null);
            elsif r_muts_rec.number_of_mutations > 0
            then
              update_mdet(r_meet_rec.meet_id, 'Afwijking?', 'J');
              --
              -- Neem eerstgevulde kolom variant1 of variant2 of variant3 of variant4
              --
              pl_waarde := pl_eerste_var;
              --
              if pl_waarde is not null
              then
                pl_comma_pos                   := instr(pl_waarde, ',');
                pl_quotjes_pos                 := instr(pl_waarde, '""');
                pl_groter_dan_pos              := instr(pl_waarde, '>');
                pl_p_punt_pos                  := instr(pl_waarde, 'p.');
                pl_1e_spatie_na_gr_dan_pos     := instr(pl_waarde, ' ', pl_groter_dan_pos, 1);
                pl_1e_spatie_na_p_punt_pos     := instr(pl_waarde, ' ', pl_p_punt_pos, 1);
                pl_1e_comma_na_p_punt_pos      := instr(pl_waarde, ',', pl_p_punt_pos, 1);
                --
                if  pl_1e_spatie_na_p_punt_pos > 0
                and pl_1e_comma_na_p_punt_pos > 0
                then
                  pl_least_spatie_comma_pos := least(pl_1e_spatie_na_p_punt_pos, pl_1e_comma_na_p_punt_pos);
                elsif pl_1e_spatie_na_p_punt_pos > 0
                and   pl_1e_comma_na_p_punt_pos = 0
                then
                  pl_least_spatie_comma_pos := pl_1e_spatie_na_p_punt_pos;
                elsif pl_1e_spatie_na_p_punt_pos = 0
                and   pl_1e_comma_na_p_punt_pos > 0
                then
                  pl_least_spatie_comma_pos := pl_1e_comma_na_p_punt_pos;
                else
                  pl_least_spatie_comma_pos := 0;
                end if;
                --
                -- Indien geen comma in de eerstgevulde kolom aanwezig, dan hele kolominhoud nemen tot aan ""
                --
                if  pl_comma_pos > 0
                then
                  update_mdet(r_meet_rec.meet_id, 'Naam vd afw.',substr(pl_waarde, 1, pl_comma_pos - 1));
                else
                  update_mdet(r_meet_rec.meet_id, 'Naam vd afw.',rtrim(substr(pl_waarde, 1, pl_quotjes_pos - 1)));
                end if;
                --
                if instr(pl_waarde, 'dbSNP') > 0
                then
                  --
                  -- Eerstgevulde kolom variant1 of variant2 of variant3 of variant4 bevat string 'dbSNP'
                  --
                  update_mdet(r_meet_rec.meet_id, 'Soort afw.', 'Pol');
                else
                  update_mdet(r_meet_rec.meet_id, 'Soort afw.', 'UV');
                end if;
                --
                -- Bepaal het aantal letters (1 of 2) in de eerst gevulde kolom Variant1 of Variant2 of Variant3 of varaint4 tussen de ?>? en de ?,? of een spatie
                --
                if (   (pl_comma_pos - pl_groter_dan_pos - 1) = 1
                    or (pl_1e_spatie_na_gr_dan_pos - pl_groter_dan_pos - 1) = 1
                   )
                then
                  update_mdet(r_meet_rec.meet_id, 'Zygotie', 'Homozygoot');
                elsif (   (pl_comma_pos - pl_groter_dan_pos - 1) = 2
                       or (pl_1e_spatie_na_gr_dan_pos - pl_groter_dan_pos - 1) = 2
                      )
                then
                  update_mdet(r_meet_rec.meet_id, 'Zygotie', 'Heterozygoot');
                else
                  null;
                end if;
                --
                if pl_p_punt_pos > 0
                then
                  --
                  -- De string die in `Effect van de afwijking? wordt geplaatst moet in bepaalde gevallen een ander format krijgen.
                  -- Algemeen: (# is getal tussen 1 en 100000)
                  -- p.Xxx#XxxXxx  ? p.=
                  -- p.Xxx#Xxx     ? p.=
                  -- p.Xxx#XxxYyy  ? p.Xxx#XxxYyy
                  -- p.Xxx#YyyXxx  ? p.Xxx#YyyXxx
                  --
                  -- Vb p.Leu73LeuLeu moet p.= worden
                  --    p.Leu73ArgLeu blijft p.Leu73ArgLeu
                  --
                  -- Eerstgevulde kolom variant1 of variant2 of variant3 of variant4 bevat string 'p.'
                  --
                  if  pl_least_spatie_comma_pos > 0
                  then
                    pl_eff_vd_afwijking := substr(pl_waarde, pl_p_punt_pos, pl_least_spatie_comma_pos - pl_p_punt_pos);
                    pl_eff_vd_afw_deel1 := substr(pl_eff_vd_afwijking, 3, 3);  -- De eerste 3 karakters na de p.
                    pl_eff_vd_afw_deel2 := regexp_substr(pl_eff_vd_afwijking, '\D{3,}',5);  -- De (3 of 6) karakters na het getal (#)
                    --
                    if (   (    substr(pl_eff_vd_afw_deel2,1,3) = pl_eff_vd_afw_deel1
                            and length(pl_eff_vd_afw_deel2) = 3
                           )
                        or (    substr(pl_eff_vd_afw_deel2,1,3) = pl_eff_vd_afw_deel1
                            and substr(pl_eff_vd_afw_deel2,4,3) = pl_eff_vd_afw_deel1
                            and length(pl_eff_vd_afw_deel2) = 6
                           )
                       )
                    then
                      update_mdet(r_meet_rec.meet_id, 'Effect van de afwijking','p.=');
                    else
                      update_mdet(r_meet_rec.meet_id, 'Effect van de afwijking',pl_eff_vd_afwijking);
                    end if;
                  else
                    null;
                  end if;
                else
                  update_mdet(r_meet_rec.meet_id, 'Effect van de afwijking', null);
                end if;
              else
                null;
              end if;
              --
              if r_muts_rec.number_of_mutations > 1
              then
                --
                -- Neem tweede gevulde kolom variant2 of variant3 of variant4
                --
                pl_waarde := pl_tweede_var;
                --
                if pl_waarde is not null
                then
                  pl_comma_pos               := instr(pl_waarde, ',');
                  pl_quotjes_pos             := instr(pl_waarde, '""');
                  pl_groter_dan_pos          := instr(pl_waarde, '>');
                  pl_p_punt_pos              := instr(pl_waarde, 'p.');
                  pl_1e_spatie_na_gr_dan_pos := instr(pl_waarde, ' ', pl_groter_dan_pos, 1);
                  pl_1e_spatie_na_p_punt_pos := instr(pl_waarde, ' ', pl_p_punt_pos, 1);
                  pl_1e_comma_na_p_punt_pos  := instr(pl_waarde, ',', pl_p_punt_pos, 1);
                  --
                  if  pl_1e_spatie_na_p_punt_pos > 0
                  and pl_1e_comma_na_p_punt_pos > 0
                  then
                    pl_least_spatie_comma_pos := least(pl_1e_spatie_na_p_punt_pos, pl_1e_comma_na_p_punt_pos);
                  elsif pl_1e_spatie_na_p_punt_pos > 0
                  and   pl_1e_comma_na_p_punt_pos = 0
                  then
                    pl_least_spatie_comma_pos := pl_1e_spatie_na_p_punt_pos;
                  elsif pl_1e_spatie_na_p_punt_pos = 0
                  and   pl_1e_comma_na_p_punt_pos > 0
                  then
                    pl_least_spatie_comma_pos := pl_1e_comma_na_p_punt_pos;
                  else
                    pl_least_spatie_comma_pos := 0;
                  end if;
                  --
                  -- Indien geen comma in de tweede gevulde kolom aanwezig, dan hele kolominhoud nemen tot aan ""
                  --
                  if  pl_comma_pos > 0
                  then
                    update_mdet(r_meet_rec.meet_id, 'Naam van de 2e afwijking',substr(pl_waarde, 1, pl_comma_pos - 1));
                  else
                    update_mdet(r_meet_rec.meet_id, 'Naam van de 2e afwijking',rtrim(substr(pl_waarde, 1, pl_quotjes_pos - 1)));
                  end if;
                  --
                  if instr(pl_waarde, 'dbSNP') > 0
                    then
                    --
                    -- Tweede gevulde kolom variant2 of variant3 of variant4 bevat string 'dbSNP'
                    --
                    update_mdet(r_meet_rec.meet_id, 'Soort afwijking (2e)', 'Pol');
                  else
                    update_mdet(r_meet_rec.meet_id, 'Soort afwijking (2e)', 'UV');
                  end if;
                  --
                  -- Bepaal het aantal letters (1 of 2) in de eerst gevulde kolom Variant1 of Variant2 of Variant3 of varaint4 tussen de ?>? en de ?,? of een spatie
                  --
                  if (   (pl_comma_pos - pl_groter_dan_pos - 1) = 1
                      or (pl_1e_spatie_na_gr_dan_pos - pl_groter_dan_pos - 1) = 1
                     )
                  then
                    update_mdet(r_meet_rec.meet_id, 'Zygotie 2e afwijking', 'Homozygoot');
                  elsif (   (pl_comma_pos - pl_groter_dan_pos - 1) = 2
                         or (pl_1e_spatie_na_gr_dan_pos - pl_groter_dan_pos - 1) = 2
                        )
                  then
                    update_mdet(r_meet_rec.meet_id, 'Zygotie 2e afwijking', 'Heterozygoot');
                  else
                    null;
                  end if;
                  --
                  if pl_p_punt_pos > 0
                  then
                    --
                    -- De string die in `Effect van de afwijking? wordt geplaatst moet in bepaalde gevallen een ander format krijgen.
                    -- Algemeen: (# is getal tussen 1 en 100000)
                    -- p.Xxx#XxxXxx  ? p.=
                    -- p.Xxx#Xxx     ? p.=
                    -- p.Xxx#XxxYyy  ? p.Xxx#XxxYyy
                    -- p.Xxx#YyyXxx  ? p.Xxx#YyyXxx
                    --
                    -- Vb p.Leu73LeuLeu moet p.= worden
                    --    p.Leu73ArgLeu blijft p.Leu73ArgLeu
                    --
                    -- Tweede gevulde kolom variant1 of variant2 of variant3 of variant4 bevat string 'p.'
                    --
                    if  pl_least_spatie_comma_pos > 0
                    then
                      pl_eff_vd_afwijking := substr(pl_waarde, pl_p_punt_pos, pl_least_spatie_comma_pos - pl_p_punt_pos);
                      pl_eff_vd_afw_deel1 := substr(pl_eff_vd_afwijking, 3, 3);  -- De eerste 3 karakters na de p.
                      pl_eff_vd_afw_deel2 := regexp_substr(pl_eff_vd_afwijking, '\D{3,}',5);  -- De (3 of 6) karakters na het getal (#)
                      --
                      if (   (    substr(pl_eff_vd_afw_deel2,1,3) = pl_eff_vd_afw_deel1
                              and length(pl_eff_vd_afw_deel2) = 3
                             )
                          or (    substr(pl_eff_vd_afw_deel2,1,3) = pl_eff_vd_afw_deel1
                              and substr(pl_eff_vd_afw_deel2,4,3) = pl_eff_vd_afw_deel1
                              and length(pl_eff_vd_afw_deel2) = 6
                             )
                         )
                      then
                        update_mdet(r_meet_rec.meet_id, 'Effect van de 2e afwijking','p.=');
                      else
                        update_mdet(r_meet_rec.meet_id, 'Effect van de 2e afwijking',pl_eff_vd_afwijking);
                      end if;
                    else
                      null;
                    end if;
                  else
                    update_mdet(r_meet_rec.meet_id, 'Effect van de 2e afwijking', null);
                  end if;
                else
                  null;
                end if;
                if r_muts_rec.number_of_mutations > 2
                then
                  --
                  -- Neem derde gevulde kolom variant3 of variant4
                  --
                  pl_waarde := pl_derde_var;
                  --
                  if pl_waarde is not null
                  then
                    pl_comma_pos               := instr(pl_waarde, ',');
                    pl_quotjes_pos             := instr(pl_waarde, '""');
                    pl_groter_dan_pos          := instr(pl_waarde, '>');
                    pl_p_punt_pos              := instr(pl_waarde, 'p.');
                    pl_1e_spatie_na_gr_dan_pos := instr(pl_waarde, ' ', pl_groter_dan_pos, 1);
                    pl_1e_spatie_na_p_punt_pos := instr(pl_waarde, ' ', pl_p_punt_pos, 1);
                    pl_1e_comma_na_p_punt_pos  := instr(pl_waarde, ',', pl_p_punt_pos, 1);
                    --
                    if  pl_1e_spatie_na_p_punt_pos > 0
                    and pl_1e_comma_na_p_punt_pos > 0
                    then
                      pl_least_spatie_comma_pos := least(pl_1e_spatie_na_p_punt_pos, pl_1e_comma_na_p_punt_pos);
                    elsif pl_1e_spatie_na_p_punt_pos > 0
                    and   pl_1e_comma_na_p_punt_pos = 0
                    then
                      pl_least_spatie_comma_pos := pl_1e_spatie_na_p_punt_pos;
                    elsif pl_1e_spatie_na_p_punt_pos = 0
                    and   pl_1e_comma_na_p_punt_pos > 0
                    then
                      pl_least_spatie_comma_pos := pl_1e_comma_na_p_punt_pos;
                    else
                      pl_least_spatie_comma_pos := 0;
                    end if;
                    --
                    -- Indien geen comma in de derde gevulde kolom aanwezig, dan hele kolominhoud nemen tot aan ""
                    --
                    if  pl_comma_pos > 0
                    then
                      update_mdet(r_meet_rec.meet_id, 'Naam van de 3e afwijking',substr(pl_waarde, 1, pl_comma_pos - 1));
                    else
                      update_mdet(r_meet_rec.meet_id, 'Naam van de 3e afwijking',rtrim(substr(pl_waarde, 1, pl_quotjes_pos - 1)));
                    end if;
                    --
                    if instr(pl_waarde, 'dbSNP') > 0
                      then
                      --
                      -- Derde gevulde kolom variant3 of variant4 bevat string 'dbSNP'
                      --
                      update_mdet(r_meet_rec.meet_id, 'Soort afwijking (3e)', 'Pol');
                    else
                      update_mdet(r_meet_rec.meet_id, 'Soort afwijking (3e)', 'UV');
                    end if;
                    --
                    -- Bepaal het aantal letters (1 of 2) in de eerst gevulde kolom Variant1 of Variant2 of Variant3 of varaint4 tussen de ?>? en de ?,? of een spatie
                    --
                    if (   (pl_comma_pos - pl_groter_dan_pos - 1) = 1
                        or (pl_1e_spatie_na_gr_dan_pos - pl_groter_dan_pos - 1) = 1
                       )
                    then
                      update_mdet(r_meet_rec.meet_id, 'Zygotie 3e afwijking', 'Homozygoot');
                    elsif (   (pl_comma_pos - pl_groter_dan_pos - 1) = 2
                           or (pl_1e_spatie_na_gr_dan_pos - pl_groter_dan_pos - 1) = 2
                          )
                    then
                      update_mdet(r_meet_rec.meet_id, 'Zygotie 3e afwijking', 'Heterozygoot');
                    else
                      null;
                    end if;
                    --
                    if pl_p_punt_pos > 0
                    then
                      --
                      -- De string die in `Effect van de afwijking? wordt geplaatst moet in bepaalde gevallen een ander format krijgen.
                      -- Algemeen: (# is getal tussen 1 en 100000)
                      -- p.Xxx#XxxXxx  ? p.=
                      -- p.Xxx#Xxx     ? p.=
                      -- p.Xxx#XxxYyy  ? p.Xxx#XxxYyy
                      -- p.Xxx#YyyXxx  ? p.Xxx#YyyXxx
                      --
                      -- Vb p.Leu73LeuLeu moet p.= worden
                      --    p.Leu73ArgLeu blijft p.Leu73ArgLeu
                      --
                      -- Tweede gevulde kolom variant1 of variant2 of variant3 of variant4 bevat string 'p.'
                      --
                      if  pl_least_spatie_comma_pos > 0
                      then
                        pl_eff_vd_afwijking := substr(pl_waarde, pl_p_punt_pos, pl_least_spatie_comma_pos - pl_p_punt_pos);
                        pl_eff_vd_afw_deel1 := substr(pl_eff_vd_afwijking, 3, 3);  -- De eerste 3 karakters na de p.
                        pl_eff_vd_afw_deel2 := regexp_substr(pl_eff_vd_afwijking, '\D{3,}',5);  -- De (3 of 6) karakters na het getal (#)
                        --
                        if (   (    substr(pl_eff_vd_afw_deel2,1,3) = pl_eff_vd_afw_deel1
                                and length(pl_eff_vd_afw_deel2) = 3
                               )
                            or (    substr(pl_eff_vd_afw_deel2,1,3) = pl_eff_vd_afw_deel1
                                and substr(pl_eff_vd_afw_deel2,4,3) = pl_eff_vd_afw_deel1
                                and length(pl_eff_vd_afw_deel2) = 6
                               )
                           )
                        then
                          update_mdet(r_meet_rec.meet_id, 'Effect van de 3e afwijking','p.=');
                        else
                          update_mdet(r_meet_rec.meet_id, 'Effect van de 3e afwijking',pl_eff_vd_afwijking);
                        end if;
                      else
                        null;
                      end if;
                    else
                      update_mdet(r_meet_rec.meet_id, 'Effect van de 3e afwijking', null);
                    end if;
                  else
                    null;
                  end if;
                else
                  null;
                end if; -- r_muts_rec.number_of_mutations > 2
              else
                null;
              end if; -- r_muts_rec.number_of_mutations > 1
            else
              null;
            end if; -- r_muts_rec.number_of_mutations > 0
          end if;
          --
          -- Indien > 1 afwijkingen (> 1 variant kolom gevuld), dan wordt aan begin in bijbehorende meet.commentaar 'Meerdere afw.' gezet
          -- en dan alleen als dit commentaar er nog niet inzit.
          -- Daarnaast dient de ingelogde gebruiker te worden vastgelegd in BAS_MEETWAARDEN.MEDE_ID wanneer deze de interface gebruikt (Mantis 0012916).
          --
          if  r_muts_rec.number_of_mutations > 1
          and instr(nvl(r_meet_rec.commentaar,'DUMMY'),'Meerdere afw.') = 0
          then
            if p_progvariant = 'A'
            then
              update bas_meetwaarden meet
              set    meet.afgerond = 'J'
              ,      meet.commentaar = 'Meerdere afw. '||meet.commentaar
              ,      meet.mede_id = kgc_mede_00.medewerker_id(null)
              where  current of c_meetA;
            else
              update bas_meetwaarden meet
              set    meet.afgerond = 'J'
              ,      meet.commentaar = 'Meerdere afw. '||meet.commentaar
              ,      meet.mede_id = kgc_mede_00.medewerker_id(null)
              where  current of c_meetB;
            end if;
          else
            if p_progvariant = 'A'
            then
              update bas_meetwaarden meet
              set    meet.afgerond = 'J'
              ,      meet.mede_id = kgc_mede_00.medewerker_id(null)
              where  current of c_meetA;
            else
              update bas_meetwaarden meet
              set    meet.afgerond = 'J'
              ,      meet.mede_id = kgc_mede_00.medewerker_id(null)
              where  current of c_meetB;
            end if;
          end if;
          --
        else
          null;
        end if; -- r_meet_rec.meet_id is not null
        --
        if p_progvariant = 'A'
        then
          close c_meetA;
        else
          close c_meetB;
        end if;
        --
      end if; -- pl_wlst_id is null
    end loop;
  end if;
  --
  delete
  from   mss_mutation_surveyor mss
  where  mss.sessie = p_sessie;
  --
  commit;
  --
end mss_verw_mut_surveyor;
/

/
QUIT
