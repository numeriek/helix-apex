CREATE OR REPLACE PROCEDURE "HELIX"."BEREKENEN_UITSLAGTERMIJN" (Dat1 in date,
                                                     dat2 in date) is

  Cursor C(d1 date, d2 Date) is
    Select (b.datum_afgerond - b.datum_aanmelding) / 7 Weken,
           stgr_id,
           o.ongr_id
      From bas_metingen b, kgc_onderzoeken o
     Where b.datum_aanmelding between d1 and d2
       and o.onde_id = b.Onde_id
       and b.afgerond = 'J'
       and o.kafd_id = 46;

  c_rec C%rowtype;
  Cnt   Integer;
  Tel   Integer;
  M_rec MSS_uitslagterm%RowType;

Begin
  Delete From MSS_uitslagterm;
  Commit;
  Open C(dat1, Dat2);
  Loop
    fetch c
      into C_rec;
    if C%NotFound then
      Exit;
    End If;

    Select count(*)
      into cnt
      from MSS_uitslagterm m
     Where m.onderzoeksgrp = C_rec.ongr_id
       and m.protocol = c_rec.stgr_id;

    If cnt = 0 then
      If c_rec.weken <= 2 Then
        Insert into MSS_uitslagterm
          (Onderzoeksgrp, Protocol, Wk2)
        Values
          (C_rec.Ongr_Id, C_rec.Stgr_Id, 1);
      Else
        If (c_rec.weken < 3) Then
          Insert into MSS_uitslagterm
            (Onderzoeksgrp, Protocol, Wk3)
          Values
            (C_rec.Ongr_Id, C_rec.Stgr_Id, 1);
        Else
          If c_rec.weken < 4 Then
            Insert into MSS_uitslagterm
              (Onderzoeksgrp, Protocol, Wk4)
            Values
              (C_rec.Ongr_Id, C_rec.Stgr_Id, 1);
          Else
            If c_rec.weken < 5 Then
              Insert into MSS_uitslagterm
                (Onderzoeksgrp, Protocol, Wk5)
              Values
                (C_rec.Ongr_Id, C_rec.Stgr_Id, 1);
            Else
              If c_rec.weken < 6 Then
                Insert into MSS_uitslagterm
                  (Onderzoeksgrp, Protocol, Wk6)
                Values
                  (C_rec.Ongr_Id, C_rec.Stgr_Id, 1);
              Else
                If c_rec.weken < 7 Then
                  Insert into MSS_uitslagterm
                    (Onderzoeksgrp, Protocol, Wk7)
                  Values
                    (C_rec.Ongr_Id, C_rec.Stgr_Id, 1);
                Else
                  If c_rec.weken >= 8 Then
                    Insert into MSS_uitslagterm
                      (Onderzoeksgrp, Protocol, Wk8)
                    Values
                      (C_rec.Ongr_Id, C_rec.Stgr_Id, 1);

                  End if;
                End if;
              End if;
            End if;
          End if;
        End if;
      End if;

    Else

      select *
        into M_rec
        from MSS_uitslagterm m
       Where m.onderzoeksgrp = C_rec.ongr_id
         and m.protocol = c_rec.stgr_id;

      If c_rec.weken <= 2 Then
        Update MSS_uitslagterm
           Set Wk2 = M_rec.Wk2 + 1
         Where onderzoeksgrp = C_rec.ongr_id
           and protocol = c_rec.stgr_id;
      Else
        If (c_rec.weken < 3) Then
          Update MSS_uitslagterm
             Set Wk3 = M_rec.Wk3 + 1
           Where onderzoeksgrp = C_rec.ongr_id
             and protocol = c_rec.stgr_id;
        Else
          If c_rec.weken < 4 Then
            Update MSS_uitslagterm
               Set Wk4 = M_rec.Wk4 + 1
             Where onderzoeksgrp = C_rec.ongr_id
               and protocol = c_rec.stgr_id;
          Else
            If c_rec.weken < 5 Then
              Update MSS_uitslagterm
                 Set Wk5 = M_rec.Wk5 + 1
               Where onderzoeksgrp = C_rec.ongr_id
                 and protocol = c_rec.stgr_id;
            Else
              If c_rec.weken < 6 Then
                Update MSS_uitslagterm
                   Set Wk6 = M_rec.Wk6 + 1
                 Where onderzoeksgrp = C_rec.ongr_id
                   and protocol = c_rec.stgr_id;
              Else
                If c_rec.weken < 7 Then
                  Update MSS_uitslagterm
                     Set Wk7 = M_rec.Wk7 + 1
                   Where onderzoeksgrp = C_rec.ongr_id
                     and protocol = c_rec.stgr_id;
                Else
                  If c_rec.weken > 7 Then
                    Update MSS_uitslagterm
                       Set Wk8 = M_rec.Wk8 + 1
                     Where onderzoeksgrp = C_rec.ongr_id
                       and protocol = c_rec.stgr_id;

                  End if;
                End if;
              End if;
            End if;
          End if;
        End if;
      End if;
    End If;
    If tel = 10 then
      Commit;
      tel := 0;
    End If;
    Tel := Tel + 1;
  End Loop;
  commit;
End;
/

/
QUIT
