CREATE OR REPLACE PROCEDURE "HELIX"."BEH_VERW_INGELEZEN_QTOF"
is
begin
  insert into kgc_bestanden ( best_id
                            , entiteit_code
                            , entiteit_pk
                            , btyp_id
                            , bestand_specificatie
                            , volgorde
                            , locatie
                            , bestandsnaam
                            , bcat_id
                            )
  Select kgc_best_seq.nextval                  best_id
  ,      'METI'                                entiteit_code
  ,      meti.meti_id                          entiteit_pk
  ,      ( select btyp_id
           from   kgc_bestand_types btyp
           where  btyp.code = 'QTOF_GRAFIEK'
         )                                     btyp_id
  ,      qtof.pad                              bestand_specificatie
  ,      1                                     volgorde
  ,      qtof.pad                              locatie
  ,      qtof.bestandsnaam                     bestandsnaam
  ,      ( select bcat_id
           from   kgc_bestand_categorieen bcat
           where  bcat.code = 'DEFAULT'
         )                                     bcat_id
  from   kgc_stoftestgroepen  stgr
  ,      bas_fracties         frac
  ,      bas_metingen         meti
  ,      kgc_onderzoeken      onde
  ,      beh_inlees_qtof      qtof
  where  onde.onderzoeknr = qtof.onderzoeknr
  and    meti.onde_id = onde.onde_id
  and    frac.frac_id = meti.frac_id
  and    frac.fractienummer = qtof.fractienummer
  and    stgr.stgr_id = meti.stgr_id
  and    stgr.code = 'OZ QTOF';
end beh_verw_ingelezen_qtof;
/

/
QUIT
