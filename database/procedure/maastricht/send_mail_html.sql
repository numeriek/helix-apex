CREATE OR REPLACE PROCEDURE "HELIX"."SEND_MAIL_HTML" (
    p_from        in varchar2,
    p_to            in varchar2,    --puntkomma is de separator
    p_cc            in varchar2 := null,
    p_subject     in varchar2,
    p_text          in varchar2 := null,
    p_html         in clob,
    p_smtp_hostname in varchar2 := 'smtp.umcn.nl',
    p_smtp_portnum  in varchar2 := '25')
is
    v_to varchar2(2500);

    l_boundary      varchar2(255) default 'a1b2c3d4e3f2g1';
    l_connection    utl_smtp.connection;
    l_body_html     clob := empty_clob;  --This LOB will be the email message
    l_offset        number;
    l_amount        number;
    l_temp          varchar2(32767) default null;
    l_html_lengte   number;

    cursor c_rcpt (p_to2 varchar2)is
    select *
    from table(cast(beh_alfu_00.explode(';', p_to2) as beh_listtable))
    ;

    begin
    if p_cc is not null then
        v_to := p_to ||';' ||p_cc;
    else
        v_to := p_to;
    end if;

    l_connection := utl_smtp.open_connection( p_smtp_hostname, to_number(p_smtp_portnum) );
    utl_smtp.helo( l_connection, p_smtp_hostname );
    utl_smtp.mail( l_connection, p_from );

    for r_rcpt in c_rcpt(v_to) loop
        utl_smtp.rcpt(l_connection, r_rcpt.column_value);
    end loop;

    l_temp := l_temp || 'MIME-Version: 1.0' ||  chr(13) || chr(10);
    l_temp := l_temp || 'To: ' || p_to || chr(13) || chr(10);
    l_temp := l_temp || 'cc: ' || p_cc || chr(13) || chr(10);
    l_temp := l_temp || 'From: ' || p_from || chr(13) || chr(10);
    l_temp := l_temp || 'Subject: ' || p_subject || chr(13) || chr(10);
    l_temp := l_temp || 'Reply-To: ' || p_from ||  chr(13) || chr(10);
    l_temp := l_temp || 'Content-Type: multipart/alternative; boundary=' ||
                         chr(34) || l_boundary ||  chr(34) || chr(13) ||
                         chr(10);

    ----------------------------------------------------
    -- Write the headers
    dbms_lob.createtemporary( l_body_html, false, 10 );
    dbms_lob.write(l_body_html,length(l_temp),1,l_temp);


    ----------------------------------------------------
    -- Write the text boundary
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    l_temp   := '--' || l_boundary || chr(13)||chr(10);
    l_temp   := l_temp || 'content-type: text/plain; charset=us-ascii' ||
                  chr(13) || chr(10) || chr(13) || chr(10);
    dbms_lob.write(l_body_html,length(l_temp),l_offset,l_temp);

    ----------------------------------------------------
    -- Write the plain text portion of the email
    if p_text is not null then
        l_offset := dbms_lob.getlength(l_body_html) + 1;
        dbms_lob.write(l_body_html,length(p_text),l_offset,p_text);
    end if;
    ----------------------------------------------------
    -- Write the HTML boundary
    l_temp   := chr(13)||chr(10)||chr(13)||chr(10)||'--' || l_boundary ||
                    chr(13) || chr(10);
    l_temp   := l_temp || 'content-type: text/html;' ||
                   chr(13) || chr(10) || chr(13) || chr(10);
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html,length(l_temp),l_offset,l_temp);

    ----------------------------------------------------
    -- Write the HTML portion of the message
--    l_offset := dbms_lob.getlength(l_body_html) + 1;
--    dbms_lob.write(l_body_html,length(p_html),l_offset,p_html);
    -- Write the HTML portion of the message (voor het geval dat het bericht > 32 kb)
/*
    l_offset  := 1;
    l_amount := 1900;
    l_html_lengte := dbms_lob.getlength(p_html);
    while l_offset < l_html_lengte loop
      dbms_lob.writeAppend(l_body_html, l_amount, dbms_lob.substr(p_html, l_amount, l_offset));
      l_offset  := l_offset + l_amount ;
      l_amount := least(1900, l_html_lengte - l_offset);
    end loop;
*/
    dbms_lob.append (l_body_html, p_html);

    ----------------------------------------------------
    -- Write the final html boundary
    l_temp   := chr(13) || chr(10) || '--' ||  l_boundary || '--' || chr(13);
    l_offset := dbms_lob.getlength(l_body_html) + 1;
    dbms_lob.write(l_body_html,length(l_temp),l_offset,l_temp);


    ----------------------------------------------------
    -- Send the email in 1900 byte chunks to UTL_SMTP
    l_offset  := 1;
    l_amount := 1900;
    utl_smtp.open_data(l_connection);
    while l_offset < dbms_lob.getlength(l_body_html) loop
        utl_smtp.write_data(l_connection,
                            dbms_lob.substr(l_body_html,l_amount,l_offset));
        l_offset  := l_offset + l_amount ;
        l_amount := least(1900,dbms_lob.getlength(l_body_html) - l_amount);
    end loop;
    utl_smtp.close_data(l_connection);
    utl_smtp.quit( l_connection );
    dbms_lob.freetemporary(l_body_html);
exception
    when others then
--    utl_smtp.close_data(l_connection);
    utl_smtp.quit( l_connection );
    dbms_lob.freetemporary(l_body_html);
    dbms_output.put_line(substr(sqlerrm, 1, 4000));
end send_mail_html;
/

/
QUIT
