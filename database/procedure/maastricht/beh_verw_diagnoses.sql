CREATE OR REPLACE PROCEDURE "HELIX"."BEH_VERW_DIAGNOSES"
IS
  --
  -- Script voor geautomatiseerde invoer van diagnoses.
  -- De KGC_DIAGNOSES-tabel dient aangevuld te worden met data uit een csv-file.
  --
  -- 2015-06-04 Versie 1.2   - Guido Herben - Door wijziging van de systematiek door Marc Woutersen procedure moeten aanpassen...CIN_HFD_44 is nu CIN_HFD geworden.
  -- 2015-02-05 Versie 1.1   - Guido Herben - Afvangen foutloze rapportages en lege diagnose-datum werkend.
  -- 2015-01-28 Versie 1.0   - Guido Herben - MANTIS-vraag 009589 opgelost. Invoeren csv-file met diagnoses ter afronding onderzoeken.
  --
  --
  -- Definieer cursoren
  --
  cursor c_dinv --diagnose-invoer-cursor
  is
    select dinv.*
      from beh_invoer_diagnoses dinv
  ;
  --
  cursor c_dchk --diagnose-invoer-check-cursor
  ( b_patientnaam   kgc_personen.achternaam%type
  , b_geboortedatum kgc_personen.geboortedatum%type
  , b_x_nr          kgc_onderzoeken.onderzoeknr%type
  )
  is
    select pers.pers_id
      from kgc_personen              pers
         , kgc_onderzoek_betrokkenen onbe
         , kgc_onderzoeken           onde
     where (   (upper(pers.achternaam) like '%'||upper(b_patientnaam)||'%')
            or (upper(pers.achternaam_partner) like '%'||upper(b_patientnaam)||'%')
           )
       and pers.geboortedatum = b_geboortedatum
       and onde.onderzoeknr = b_x_nr
       and onbe.pers_id = pers.pers_id
       and onbe.onde_id = onde.onde_id
  ;
  --
  cursor c_dvdc --validate-diagnose-code-cursor
  ( b_diagnose_code beh_invoer_diagnoses.diagnose_code%type )
  is
    select ziektecode
      from cin_hfd cinh
     where lpad(b_diagnose_code,4,'0') = cinh.ziektecode
  ;
  --
  -- Definieer rowcounters
  --
  r_dchk_rec c_dchk%rowtype;
  --
  r_dvdc_rec c_dchk%rowtype;
  --
  --
  -- Definieer locale variabelen
  --
  pl_misn    varchar2(100); --errornote
  pl_pers_id kgc_personen.pers_id%type; --persoonstype uit persoonstabel. Bewaar persoon.
  pl_count   numeric(4); --tellertje voor correcte records.
  --
  -- Programma
  --
BEGIN
  pl_count:=0;
  for r_dinv_rec in c_dinv
  loop
    pl_misn:=NULL;
    if r_dinv_rec.datum_diagnose is NULL -- Check op aanwezigheid diagnose_datum
    then
      pl_misn:='De diagnose_datum is niet ingevuld' ;
    end if;
   --
    if r_dinv_rec.diagnose_code is NULL -- Check op aanwezigheid diagnose_code
    then
      pl_misn:='De diagnose_code is niet ingevuld' ;
    end if;
    --
    if length(r_dinv_rec.diagnose_code) > 4 -- Check op te grote diagnose_code
    then
      pl_misn:='De diagnose_code is te lang en dus ongeldig' ;
    end if;
    --
     if pl_misn is NULL -- Check of diagnose_code voorkomt in CIN_HFD
    then
      open c_dvdc( lpad(r_dinv_rec.diagnose_code,4,'0') );
        fetch c_dvdc into r_dvdc_rec;
        if c_dvdc%NOTFOUND
        then
          pl_misn:='De diagnose_code is ongeldig' ;
        end if;
      close c_dvdc;
    end if;
    --
    if pl_misn is NULL -- Check of er een juiste persoon gebonden is aan de invoer.
    then
      open c_dchk( r_dinv_rec.patientnaam
                , to_date(r_dinv_rec.geboortedatum,'dd-mm-yyyy')
                , r_dinv_rec.x_nr
                );
        fetch c_dchk
        into r_dchk_rec;
        case
          when c_dchk%ROWCOUNT = 0
            then pl_misn:='Geen juiste persoon gevonden';
          when c_dchk%ROWCOUNT = 1
            then pl_pers_id:=r_dchk_rec.pers_id;
          else pl_misn:='Meerdere personen gevonden'; -- rowcount > 1 m.a.w. meerdere personen gevonden.
        end case;
      close c_dchk;
    end if;
    --
    if pl_misn is NULL -- Insert in KGC_DIAGNOSES als record wel ok.
    then
      insert into kgc_diagnoses
                   ( onde_id
                   , onbe_id
                   , dist_id
                   , diagnose_code
                   , mede_id_autorisator
                   , coty_id
                   , ermo_id
                   , herk_id
                   , zekerhed_diagnose
                   , zekerheid_status
                   , zekerheid_erfmodus
                   , commentaar
                   , geverifieerd
                   , pers_id
                   , datum
                   )
                   values
                   ( (select onde.onde_id
                        from kgc_onderzoeken onde
                       where upper(onde.onderzoeknr) = upper(r_dinv_rec.x_nr)) -- GH 2015-01-15 Na opdrachtwijziging door DB alsnog meegenomen i.p.v. NULL-waarde.
                   , NULL
                   , NULL
                   , lpad(r_dinv_rec.diagnose_code,4,'0')
                   , (select mede.mede_id
                        from kgc_medewerkers mede
                       where upper(mede.code) = upper(r_dinv_rec.counceler)
                     )
                   , 4
                   , NULL
                   , NULL
                   , CASE
                       WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='UI' then '0'
                       WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='ON' then '1'
                       WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='MO' then '2'
                       WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='WA' then '3'
                       WHEN substr(upper(r_dinv_rec.ZEKERHEID),1,2)='ZE' then '4'
                       WHEN r_dinv_rec.ZEKERHEID is NULL then NULL -- GH 2015-01-09 Antwoord DB ... Indien in te importeren csv-bestand NULL dan moet dit NULL blijven.
                     END
                   , NULL
                   , NULL
                   , NULL
                   , 'N' -- GH 2015-01-09 Antwoord DB ... Als default N (Nee) hanteren.
                   , pl_pers_id
                   , to_date(r_dinv_rec.datum_diagnose,'dd-mm-yyyy')
                   );
    else -- Er is een foutief record gevonden, dus invoer in fouttabel.
      insert into beh_inverr_diagnoses
                ( DATUM_DIAGNOSE
                , PATIENTNAAM
                , GEBOORTEDATUM
                , X_NR
                , DIAGNOSE_CODE
                , ZEKERHEID
                , COUNCELER
                , OVERIGE_INFO
                , FOUTTIJDSTEMPEL
                , FOUTREDEN
                )
                values
                ( CASE
                    WHEN r_dinv_rec.DATUM_DIAGNOSE is NULL then 'Geen datum'
                      ELSE r_dinv_rec.DATUM_DIAGNOSE
                  END
                , r_dinv_rec.PATIENTNAAM
                , r_dinv_rec.GEBOORTEDATUM
                , r_dinv_rec.X_NR
                , lpad(r_dinv_rec.DIAGNOSE_CODE,4,'0')
                , r_dinv_rec.ZEKERHEID
                , r_dinv_rec.COUNCELER
                , r_dinv_rec.OVERIGE_INFO
                , to_char (sysdate, 'YYYY-MM-DD HH24:MI:SS')
                , pl_misn
                );
    end if;
      --
    if pl_misn is NULL -- Insert in KGC_DIAGNOSES als record wel ok.
      then
      pl_count:=pl_count + 1 ;
    end if;
    --
    --
    -- Sluit onderzoek af --
    --
    update kgc_onderzoeken onde
         set onde.afgerond = 'J'
         ,   onde.mede_id_autorisator = (select mede.mede_id
                                           from kgc_medewerkers mede
                                          where upper(mede.code) = upper(r_dinv_rec.counceler)
                                         )
         ,   onde.datum_autorisatie = to_date(r_dinv_rec.datum_diagnose,'dd-mm-yyyy')
       where upper(onde.onderzoeknr) = upper(r_dinv_rec.x_nr)
         and onde.afgerond != 'J'
      ;
    --
  end loop;
  commit;
  --
  insert into beh_inverr_diagnoses
              ( DATUM_DIAGNOSE
              , PATIENTNAAM
              , GEBOORTEDATUM
              , X_NR
              , DIAGNOSE_CODE
              , ZEKERHEID
              , COUNCELER
              , OVERIGE_INFO
              , FOUTTIJDSTEMPEL
              , FOUTREDEN
              )
              values
              ( to_char (sysdate, 'YYYY-MM-DD')
              , NULL
              , NULL
              , NULL
              , NULL
              , NULL
              , NULL
              , NULL
              , to_char (sysdate, 'YYYY-MM-DD HH24:MI:SS')
              , 'Er zijn '||pl_count|| ' records goed gegaan.'
              );
  commit;
END beh_verw_diagnoses;
/

/
QUIT
