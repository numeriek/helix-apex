CREATE OR REPLACE PROCEDURE "HELIX"."MSS_UPD_INST" is
/* Procedure That Updates the Instellingen and Relaties table */

Cursor C1 is Select  I.INST_ID,
                     I.naam,
                     I.Postcode,
                     I.Adres,
                     I.Woonplaats
             from kgc_instellingen I
             Where Upper(Naam) like '%XXX%';

C1_Rec C1%RowType;

Cursor C2 Is Select RELA_ID,
                    Postcode,
                    ADRES,
                    Woonplaats
             From Kgc_relaties
             where inst_id = C1_rec.Inst_Id;

C2_Rec C2%RowType;
DelRec Boolean;


begin
  --Make sure you process following commands manual before calling this procedure
  --ALter trigger KGC_ONDE_AFGEROND_TRG disable ;
  --Alter trigger cg$BDS_KGC_INSTELLINGEN disable ;

  Open C1 ;
  Loop
     Fetch C1 Into C1_Rec;
     If C1%NotFound then
        Exit;
     End If;
     DelRec := False;
     Open C2;
     Loop
        Fetch C2 into C2_Rec;
         If C2%NotFound then
            Exit;
         End If;
        If Trim(C2_Rec.Postcode)   is  null and
           Trim(C2_rec.Adres)      is  null and
           Trim(C2_Rec.Woonplaats) is  null
        Then
            Update Kgc_Relaties
            Set    Postcode    = C1_Rec.Postcode,
                   Adres       = C1_Rec.Adres,
                   Woonplaats  = C1_Rec.Woonplaats,
                   inst_id     = null
            Where  Rela_id = C2_Rec.Rela_Id ;
            Commit;
            DelRec := True;
         Else
              If Trim(C2_Rec.Postcode)   is not null and
                 Trim(C2_rec.Adres)      is  null and
                 Trim(C2_Rec.Woonplaats) is  null
              Then

                 Update Kgc_Relaties
                 Set    Postcode    = C1_Rec.Postcode,
                        Adres       = C1_Rec.Adres,
                        Woonplaats  = C1_Rec.Woonplaats,
                        inst_id     = null
                 Where  Rela_id = C2_Rec.Rela_Id ;
                 Commit;
                 DelRec := True;

              Else
                 Update Kgc_Relaties
                 Set    inst_id     = null
                 Where  Rela_id = C2_Rec.Rela_Id ;
                 Commit;
              End if;
        End IF;

     End Loop;
     Close C2;
        Update Kgc_onderzoeken
        Set    Inst_id = null
        Where Inst_id = c1_rec.Inst_Id ;
        Commit;

        Delete From kgc_instellingen where Inst_ID = c1_rec.inst_id ;
        Commit;



  End Loop;
  Close C1;

  --ALter trigger KGC_ONDE_AFGEROND_TRG enable ;
  --Alter trigger cg$BDS_KGC_INSTELLINGEN enable ;



end MSS_Upd_Inst;
/

/
QUIT
