CREATE OR REPLACE PROCEDURE "HELIX"."MSS_ZOEK_PERSOON_IN_ZIS"
( p_zisnmr in kgc_personen.zisnr%type
)
AS
  pers_trow  cg$KGC_PERSONEN.cg$row_type;
  rela_trow  cg$KGC_RELATIES.cg$row_type;
  verz_trow  cg$KGC_VERZEKERAARS.cg$row_type;
  --
  pl_achternaam     kgc_personen.achternaam%type;
  pl_geboortedatum  kgc_personen.geboortedatum%type;
  pl_geslacht       kgc_personen.geslacht%type;
  pl_bsn            kgc_personen.bsn%type;
  --
BEGIN
  --
  kgc_interface.persoon_in_zis
  ( p_zisnr => p_zisnmr
  , pers_trow => pers_trow
  , rela_trow => rela_trow
  , verz_trow => verz_trow
  );

  pl_achternaam    := pers_trow.achternaam;
  pl_geboortedatum := pers_trow.geboortedatum;
  pl_geslacht      := pers_trow.geslacht;
  pl_bsn           := pers_trow.bsn;

  IF ( pers_trow.rela_id IS NULL
   AND rela_trow.achternaam IS NOT NULL
     )
  THEN
    SELECT kgc_rela_seq.nextval
    INTO   rela_trow.rela_id
    FROM   dual;
    INSERT INTO kgc_relaties
    ( rela_id
    , code
    , achternaam
    , aanspreken
    , voorletters
    , voorvoegsel
    , adres
    , postcode
    , woonplaats
    , provincie
    , land
    , telefoon
    , telefax
    , email
    , relatie_type
    )
    VALUES
    ( rela_trow.rela_id
    , 'ZIS'||LPAD(TO_CHAR( rela_trow.rela_id ),7,'0')
    , rela_trow.achternaam
    , rela_trow.aanspreken
    , rela_trow.voorletters
    , rela_trow.voorvoegsel
    , rela_trow.adres
    , rela_trow.postcode
    , rela_trow.woonplaats
    , rela_trow.provincie
    , rela_trow.land
    , rela_trow.telefoon
    , rela_trow.telefax
    , rela_trow.email
    , 'HA'
    );
    pers_trow.rela_id := rela_trow.rela_id;
  END IF;

  IF ( pers_trow.verz_id IS NULL
   AND verz_trow.naam IS NOT NULL
     )
  THEN
    SELECT kgc_verz_seq.nextval
    INTO   verz_trow.verz_id
    FROM   dual;
    INSERT INTO kgc_verzekeraars
    ( verz_id
    , code
    , naam
    , adres
    , postcode
    , woonplaats
    , provincie
    , land
    , telefoon
    , telefax
    , email
    )
    VALUES
    ( verz_trow.verz_id
    , verz_trow.code
    , verz_trow.naam
    , verz_trow.adres
    , verz_trow.postcode
    , verz_trow.woonplaats
    , verz_trow.provincie
    , verz_trow.land
    , verz_trow.telefoon
    , verz_trow.telefax
    , verz_trow.email
    );
    pers_trow.verz_id := verz_trow.verz_id;
  END IF;

  IF ( pers_trow.achternaam IS NOT NULL )
  THEN
    insert into kgc_personen
    (
      zisnr
    , bsn
    , bsn_geverifieerd
    , achternaam
    , geslacht
    , meerling
    , overleden
    , zoeknaam
    , rela_id
    , verz_id
    , aanspreken
    , default_aanspreken
    , geboortedatum
    , voorletters
    , voorvoegsel
    , gehuwd
    , achternaam_partner
    , voorvoegsel_partner
    , adres
    , postcode
    , woonplaats
    , land
    , telefoon
    , telefoon2
    , verzekeringswijze
    , verzekeringsnr
    , overlijdensdatum
    , beheer_in_zis
    )
    values
    (
      pers_trow.zisnr
    , pers_trow.bsn
    , pers_trow.bsn_geverifieerd
    , pers_trow.achternaam
    , pers_trow.geslacht
    , pers_trow.meerling
    , pers_trow.overleden
    , pers_trow.zoeknaam
    , pers_trow.rela_id
    , pers_trow.verz_id
    , pers_trow.aanspreken
    , pers_trow.default_aanspreken
    , pers_trow.geboortedatum
    , pers_trow.voorletters
    , pers_trow.voorvoegsel
    , pers_trow.gehuwd
    , pers_trow.achternaam_partner
    , pers_trow.voorvoegsel_partner
    , pers_trow.adres
    , pers_trow.postcode
    , pers_trow.woonplaats
    , pers_trow.land
    , pers_trow.telefoon
    , pers_trow.telefoon2
    , pers_trow.verzekeringswijze
    , pers_trow.verzekeringsnr
    , pers_trow.overlijdensdatum
    ,'J'
    );
  END IF;
  --
EXCEPTION
  when others
  then
    insert into marc_fout
    values (p_zisnmr);
  --
END mss_zoek_persoon_in_zis;
/

/
QUIT
