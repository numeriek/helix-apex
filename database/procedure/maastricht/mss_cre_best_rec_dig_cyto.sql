CREATE OR REPLACE PROCEDURE "HELIX"."MSS_CRE_BEST_REC_DIG_CYTO"
( p_onde_id     in kgc_onderzoeken.onde_id%type
, p_onderzoeknr in kgc_onderzoeken.onderzoeknr%type
)
is
  cursor c_btyp
  is
    select btyp.btyp_id
    from   kgc_bestand_types btyp
    where  btyp.code = 'MAP'
  ;
  --
  cursor c_sypa
  is
    select sypa.standaard_waarde
    from   kgc_systeem_parameters sypa
    where  sypa.code = 'MSS_DIR_ONDE_MAPJES_CYTO'
  ;
  --
  pl_btyp_id       kgc_bestanden.btyp_id%type;
  pl_hoofddir      kgc_systeem_parameters.standaard_waarde%type;
  pl_bestand_spec  varchar2(256);
  --
begin
  open  c_btyp;
  fetch c_btyp
  into  pl_btyp_id;
  close c_btyp;
  --
  open c_sypa;
  fetch c_sypa
  into  pl_hoofddir;
  close c_sypa;
  --
  if  pl_btyp_id is not null
  and pl_hoofddir is not null
  then
    pl_bestand_spec := pl_hoofddir||to_char(sysdate,'YYYY')||'\'||replace(p_onderzoeknr,'/','-');
    --
    insert into kgc_bestanden
    ( entiteit_code
    , entiteit_pk
    , btyp_id
    , bestand_specificatie
    , volgorde
    , commentaar
    , locatie
    , bestandsnaam
    )
    values
    ( 'ONDE'
    , p_onde_id
    , pl_btyp_id
    , pl_bestand_spec
    , 1
    , null
    , null
    , null
    );
  else
    null;
  end if;
end mss_cre_best_rec_dig_cyto;
/

/
QUIT
