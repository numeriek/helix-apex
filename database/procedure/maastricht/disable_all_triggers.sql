CREATE OR REPLACE PROCEDURE "HELIX"."DISABLE_ALL_TRIGGERS" IS
BEGIN
  FOR r IN (SELECT trigger_name
              FROM user_triggers
             WHERE status = 'ENABLED')
  LOOP
    IF INSTR (r.trigger_name, 'BIN$') = 0
    THEN
      EXECUTE IMMEDIATE 'ALTER TRIGGER "' || r.trigger_name || '" DISABLE';
    END IF;
  END LOOP;
END disable_all_triggers;
/

/
QUIT
