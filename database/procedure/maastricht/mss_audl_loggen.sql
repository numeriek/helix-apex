CREATE OR REPLACE PROCEDURE "HELIX"."MSS_AUDL_LOGGEN"
( p_tabel                    IN VARCHAR2
, p_kolom                    IN VARCHAR2
, p_aktie                    IN VARCHAR2
, p_id                       IN NUMBER
, p_beschrijving             IN VARCHAR2
, p_waarde_oud               IN VARCHAR2
, p_waarde_nieuw             IN VARCHAR2
, p_identificerende_kolommen IN VARCHAR2
)
IS
  v_dummy VARCHAR2(1) := NULL;
BEGIN
  INSERT INTO MSS_AUDIT_LOG
  ( audl_id
  , gebruiker
  , datum_tijd
  , tabel_naam
  , kolom_naam
  , aktie
  , id
  , beschrijving
  , waarde_oud
  , waarde_nieuw
  , identificatie
  )
  VALUES
  ( mss_audl_seq.nextval
  , USER
  , SYSDATE
  , UPPER(p_tabel)
  , UPPER(p_kolom)
  , UPPER(p_aktie)
  , p_id
  , p_beschrijving
  , p_waarde_oud
  , p_waarde_nieuw
  , p_identificerende_kolommen
  );
END mss_audl_loggen;
/

/
QUIT
