CREATE OR REPLACE PROCEDURE "HELIX"."MSS_TRENDANALYSE_DNA_N_PROC" (p_sessie_id in varchar2)
is
begin
  delete
  from   mss_trendanalyse_dna tren
  where  tren.sessie_id = p_sessie_id;
  --
  commit;
  --
end mss_trendanalyse_dna_n_proc;
/

/
QUIT
