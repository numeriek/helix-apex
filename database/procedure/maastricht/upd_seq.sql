CREATE OR REPLACE PROCEDURE "HELIX"."UPD_SEQ" IS
  CURSOR c_seq IS
    SELECT table_name, column_name,
              CASE
                WHEN table_name LIKE 'BAS%'
                  THEN 'BAS_'
                ELSE 'KGC_'
              END
           || SUBSTR (column_name, 1, 4)
           || '_SEQ' seq_name
      FROM all_tab_columns atc, all_sequences aseq
     WHERE (table_name LIKE 'KGC_%' OR table_name LIKE 'BAS_%')
       AND column_id = 1
       AND SUBSTR (column_name, 5, 3) = '_ID'
       AND    CASE
                WHEN table_name LIKE 'BAS%'
                  THEN 'BAS_'
                ELSE 'KGC_'
              END
           || SUBSTR (column_name, 1, 4)
           || '_SEQ' = aseq.sequence_name
       AND table_name NOT LIKE '%_VW';

  l_id    NUMBER;
  l_seq   NUMBER;
  s       VARCHAR2 (4000);
BEGIN
  FOR r_seq IN c_seq
  LOOP
    EXECUTE IMMEDIATE    'select nvl(max('
                      || r_seq.column_name
                      || '),0) from '
                      || r_seq.table_name
                 INTO l_id;

    --DBMS_OUTPUT.put_line (r_seq.column_name || ' ; ' || l_id);
    IF l_id > 0
    THEN
      EXECUTE IMMEDIATE 'select ' || r_seq.seq_name || '.nextval from dual'
                   INTO l_seq;

--      DBMS_OUTPUT.put_line (r_seq.seq_name || ' ; ' || l_seq);
      IF l_seq < l_id
      THEN
--        DBMS_OUTPUT.put_line ('Seq bijwerken');
        EXECUTE IMMEDIATE    'alter sequence '
                          || r_seq.seq_name
                          || ' increment by '
                          || (l_id - l_seq);

        EXECUTE IMMEDIATE 'select ' || r_seq.seq_name || '.nextval from dual'
                     INTO l_id;

--        DBMS_OUTPUT.put_line (r_seq.column_name || ' ; ' || l_id);
        EXECUTE IMMEDIATE    'alter sequence '
                          || r_seq.seq_name
                          || ' increment by 1';
      END IF;
    END IF;
  END LOOP;
END;
/

/
QUIT
