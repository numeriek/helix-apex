CREATE OR REPLACE PROCEDURE "HELIX"."KGC_UPDATE_ZOEKNAAM"
 IS
cursor c
is
  select pers_id
  ,      kgc_pers_00.formatteer_zoeknaam( achternaam, aanspreken ) zoeknaam
  from kgc_personen
  where zoeknaam <> kgc_pers_00.formatteer_zoeknaam( achternaam, aanspreken )
  for update of zoeknaam nowait
  ;
  u number := 0;
begin
  kgc_util_00.dyn_exec( 'alter table kgc_personen disable all triggers' );
  for r in c
  loop
    update kgc_personen
    set zoeknaam = r.zoeknaam
    where current of c;
    u := u + sql%rowcount;
  end loop;
  kgc_util_00.dyn_exec( 'alter table kgc_personen enable all triggers' );
  p.l( 'updates', u );
end kgc_update_zoeknaam;

/

/
QUIT
