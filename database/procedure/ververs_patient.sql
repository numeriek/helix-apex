CREATE OR REPLACE PROCEDURE "HELIX"."VERVERS_PATIENT"
( p_zisnr in kgc_personen.zisnr%TYPE
)
IS
  pers_trow cg$KGC_PERSONEN.cg$row_type;
  rela_trow cg$KGC_RELATIES.cg$row_type;
  verz_trow cg$KGC_VERZEKERAARS.cg$row_type;
BEGIN
  kgc_interface.persoon_in_zis
  ( p_zisnr => p_zisnr
  , pers_trow => pers_trow
  , rela_trow => rela_trow
  , verz_trow => verz_trow
  );
END VERVERS_PATIENT;

/

/
QUIT
