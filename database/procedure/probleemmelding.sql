CREATE OR REPLACE PROCEDURE "HELIX"."PROBLEEMMELDING"
  (P_MODULE  VARCHAR2
  ,P_MELDER  VARCHAR2
  ,P_MELDING  VARCHAR2
  )
  IS
 prob_trow  kgc_probleemregistratie%rowtype;
   CURSOR  prio_cur
   IS
     SELECT  prio_id
     FROM    kgc_prioriteiten
     WHERE   code  =  '?'
     ;
 BEGIN
  prob_trow.bron := 'KGCN';
  OPEN  prio_cur;
  FETCH prio_cur
  INTO  prob_trow.prio_id;
  CLOSE prio_cur;
  prob_trow.mede_id := kgc_mede_00.medewerker_id;
  prob_trow.module_naam := p_module;
  prob_trow.melddatum := SYSDATE;
  prob_trow.probleem := p_melding;
  prob_trow.opmerkingen := 'Gemeld door '||p_melder;
  INSERT INTO kgc_probleemregistratie
  ( bron
  , prio_id
  , mede_id
  , module_naam
  , melddatum
  , probleem
  , opmerkingen
  )
  VALUES
  ( prob_trow.bron
  , prob_trow.prio_id
  , prob_trow.mede_id
  , prob_trow.module_naam
  , prob_trow.melddatum
  , prob_trow.probleem
  , prob_trow.opmerkingen
  );
END;

/

/
QUIT
