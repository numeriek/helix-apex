CREATE OR REPLACE PROCEDURE "HELIX"."MESSAGE_SCRIPT"
( p_code in varchar2
)
is
  cursor qmsp_cur
  is
    select code
    ,      description
    ,      severity
    ,      logging_ind
    ,      suppr_wrng_ind
    ,      suppr_always_ind
    ,      constraint_name
    from   qms_message_properties
    where  code like upper( p_code )
 --   where  creation_date >= to_date( '01-08-2003', 'dd-mm-yyyy' )
 -- where code >= 'KGC-10869'
       ;
  cursor qmpm_cur
  ( b_code in varchar2
  )
  is
    select msp_code
    ,      language
    ,      text
    ,      help_text
    from   qms_message_text
    where  msp_code = b_code
    ;
  function breek
  ( p_regel in varchar2
  )
  return varchar2
  is
    v_return varchar2(4000) := replace(substr(p_regel,1,4000),'''','''''');
    v_regels number := ceil(length(v_return)/100);
  begin
    if ( length(v_return) > 100 )
    then
      for i in 1..nvl(v_regels,0)
      loop
        v_return := substr( v_return
                          , 1
                          , i*100
                          )
                 || chr(10)
                 || substr( v_return
                          , i*100 + 1
                          , 3890
                          );
      end loop;
    end if;
    return( v_return );
  exception
    when others
    then
      return( p_regel );
  end breek;

  function splijt
  ( p_regel in varchar2
  )
  return varchar2
  is
    v_return varchar2(4000) := replace(substr(p_regel,1,4000),'''','''''');
  begin
    v_return := breek( v_return );
    while ( instr( v_return, chr(10) ) > 0 )
    loop
      v_return := substr( v_return
                        , 1
                        , instr( v_return, chr(10) ) - 1
                        )
               || '''||chr(10)||'''
               || substr( v_return
                        , instr( v_return, chr(10) ) + 1
                        );
    end loop;
    return( v_return );
  end splijt;

  procedure wl
  ( p_regel in varchar2
  )
  is
    v_regel varchar2(4000) := p_regel;
  begin
    while ( instr( v_regel, '||chr(10)||' ) > 0 )
    loop
      dbms_output.put_line( substr( v_regel
                                  , 1
                                  , instr( v_regel, '||chr(10)||' ) - 1
                                  )
                          );
      dbms_output.put_line( '||chr(10)||' );
      v_regel := substr( v_regel
                       , instr( v_regel, '||chr(10)||' ) + 11
                       );
    end loop;
    if ( v_regel is not null )
    then
      dbms_output.put_line( substr( v_regel, 1, 250 ) );
    end if;
  end wl;
begin
  for qmsp_rec in qmsp_cur
  loop
    wl( '-- message property' );
    wl( 'begin' );
    wl( 'update qms_message_properties' );
    wl( 'set description = '''||splijt(qmsp_rec.description)||'''' );
    wl( ', severity = '''||qmsp_rec.severity||'''' );
    wl( ', logging_ind = '''||qmsp_rec.logging_ind||'''' );
    wl( ', suppr_wrng_ind = '''||qmsp_rec.suppr_wrng_ind||'''' );
    wl( ', suppr_always_ind = '''||qmsp_rec.suppr_always_ind||'''' );
    wl( ', constraint_name = '''||qmsp_rec.constraint_name||''''  );
    wl( 'where code = '''||qmsp_rec.code||''';' );
    wl( 'if ( sql%rowcount = 0 ) then' );
    wl( 'insert into qms_message_properties' );
    wl( '( code' );
    wl( ', description' );
    wl( ', severity' );
    wl( ', logging_ind' );
    wl( ', suppr_wrng_ind');
    wl( ', suppr_always_ind');
    wl( ', constraint_name' );
    wl( ')' );
    wl( 'values' );
    wl( '( '''||qmsp_rec.code||'''' );
    wl( ', '''||splijt(qmsp_rec.description)||'''' );
    wl( ', '''||qmsp_rec.severity||'''' );
    wl( ', '''||qmsp_rec.logging_ind||'''' );
    wl( ', '''||qmsp_rec.suppr_wrng_ind||'''');
    wl( ', '''||qmsp_rec.suppr_always_ind||'''');
    wl( ', '''||qmsp_rec.constraint_name||'''' );
    wl( ');' );
    wl( 'end if;' );
    wl( 'end;' );
    wl( '/' );
    wl( '-- message_text' );
    for qmpm_rec in qmpm_cur( qmsp_rec.code )
    loop
      wl( 'begin' );
      wl( 'update qms_message_text' );
      wl( 'set text = '''||splijt(qmpm_rec.text)||'''' );
      wl( ', help_text = '''||splijt(qmpm_rec.help_text)||'''' );
      wl( 'where msp_code = '''||qmsp_rec.code||'''' );
      wl( 'and language = '''||qmpm_rec.language||''';' );
      wl( 'if ( sql%rowcount = 0 ) then' );
      wl( 'insert into qms_message_text' );
      wl( '( msp_code' );
      wl( ', language' );
      wl( ', text' );
      wl( ', help_text' );
      wl( ')' );
      wl( 'values' );
      wl( '( '''||qmsp_rec.code||'''' );
      wl( ', '''||qmpm_rec.language||'''' );
      wl( ', '''||splijt(qmpm_rec.text)||'''' );
      wl( ', '''||splijt(qmpm_rec.help_text)||'''' );
      wl( ');' );
      wl( 'end if;' );
      wl( 'end;' );
      wl( '/' );
    end loop QMPM;
  end loop QMSP;
end;

/

/
QUIT
