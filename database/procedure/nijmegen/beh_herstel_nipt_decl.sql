CREATE OR REPLACE PROCEDURE "HELIX"."BEH_HERSTEL_NIPT_DECL" is
-- deze procedure herstelt NIPT declaraties met een verkeerde verrichtingscode
-- de juiste verrichtingcode wordt bepaald door kolom verrichtingcode_goed
-- in view beh_herstel_nipt_decl_vw
-- Deze procedure wordt 1 keer per uur door de database job:
-- beh_herstel_nipt_decl_job uitgevoerd
-- de bedoeling is dat na paar maanden (wanneer alle < 2014 decl. records verwerkt zijn),
-- alle hieraan gerelateerde db objecten te verwijderen. en deze zijn:
-- 1 - view beh_herstel_nipt_decl_vw
-- 2 - procedure beh_herstel_nipt_decl
-- 3 - database job beh_herstel_nipt_decl_job
v_decl_id kgc_declaraties.decl_id%type;

cursor c_decl is
select *
from beh_herstel_nipt_decl_vw
order by datum
;
begin
for r_decl in c_decl loop

  v_decl_id := r_decl.decl_id;

  update kgc_declaraties
  set verrichtingcode = r_decl.verrichtingcode_goed
  where decl_id = r_decl.decl_id
  ;

end loop;
exception
when others then
rollback;
  send_mail_html(
    p_from => 'hamid.bourchi@radboudumc.nl',
    p_to => 'hamid.bourchi@radboudumc.nl;HelpdeskHelixCUKZ@cukz.umcn.nl',
    p_subject => 'beh_herstel_nipt_decl:' || to_char(v_decl_id),
    p_html => SUBSTR('SQLErrm ' || sqlerrm, 1, 2500)
    );
end;
/

/
QUIT
