CREATE OR REPLACE PROCEDURE "HELIX"."BEH_MONS_AFNAME_GEB_DAT" is
-- zie mantisnr 2649 voor commentaar
-- er is ook een db job aangemaakt die deze procedure 1 keer per uur uitvoert.
v_afzender VARCHAR2(50) := 'H.Bourchi@sb.umcn.nl';
v_ontvanger VARCHAR2(50) := 'cukz124@umcn.nl';
v_titel VARCHAR2(50) := 'datum_afname kleiner dan geboortedatum';
v_boodschap  VARCHAR2(500);
v_newline VARCHAR2(10) := CHR(10);
v_db_naam VARCHAR2(10);

cursor c_pers_geb_mons_afn_datum is
select  pers.aanspreken aanspreken,
        mons.monsternummer monsternummer,
        pers.geboortedatum geboortedatum,
        mons.datum_afname datum_afname,
        kafd.code kafd_code,
        ongr.code ongr_code
from kgc_personen pers, kgc_monsters mons, kgc_kgc_afdelingen kafd, kgc_onderzoeksgroepen ongr
where pers.pers_id = mons.pers_id
and   mons.kafd_id = kafd.kafd_id
and   mons.ongr_id = ongr.ongr_id
and pers.geboortedatum > mons.datum_afname
and (
        to_char(pers.last_update_date, 'DD-MON-YYYY HH') = to_char(sysdate, 'DD-MON-YYYY HH') or
        to_char(mons.last_update_date, 'DD-MON-YYYY HH') = to_char(sysdate, 'DD-MON-YYYY HH')
    )
;
begin
select sys_context ('USERENV', 'DB_NAME') into v_db_naam from dual;
for r_pers_geb_mons_afn_datum in c_pers_geb_mons_afn_datum loop
    v_boodschap :=  'Beste systeembeheerders,'
        || v_newline
        || v_newline
        || 'Op: ' || TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
        || ' is een afname datum kleiner dan geboortedatum gevonden:'
        || v_newline
        || v_newline
        || ' DataBase: ' || v_db_naam
        || v_newline
        || ' Afdeling: ' || r_pers_geb_mons_afn_datum.kafd_code
        || v_newline
        || ' Groep: ' || r_pers_geb_mons_afn_datum.ongr_code
        || v_newline
        || ' monsternr: ' || r_pers_geb_mons_afn_datum.monsternummer
        || v_newline
        || ' PatientNaam: ' || r_pers_geb_mons_afn_datum.aanspreken
        || v_newline
        || ' geboortedatum: ' || TO_CHAR(r_pers_geb_mons_afn_datum.geboortedatum, 'DD-MON-YYYY')
        || v_newline
        || ' datum_afname: ' || TO_CHAR(r_pers_geb_mons_afn_datum.datum_afname, 'DD-MON-YYYY')
        || v_newline
        || v_newline
        || 'Stuur maar deze mail door naar afdeling ' || r_pers_geb_mons_afn_datum.kafd_code
        || ', groep ' || r_pers_geb_mons_afn_datum.ongr_code || ' ter info/correctie.';

        Helix.Check_jobs.post_mail(v_afzender, v_ontvanger, v_titel, v_boodschap);
end loop;
end;

/

/
QUIT
