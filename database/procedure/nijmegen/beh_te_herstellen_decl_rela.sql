CREATE OR REPLACE PROCEDURE "HELIX"."BEH_TE_HERSTELLEN_DECL_RELA" is
cursor c_rela is
select *
from beh_te_herstellen_decl_rela_vw
where rela_code in ('ADMIRRNYM1', 'ARNTZRNYME', 'HENDRYAMST')
;
v_afzender varchar2(50) := 'cukz124@umcn.nl';
--v_ontvanger varchar2(500) := 'PostbusHelixRelatiecodes@umcn.nl';
v_ontvanger varchar2(500) := 'h.bourchi@gen.umcn.nl';
v_this_prog varchar2(100) := 'beh_te_herstellen_decl_rela';
v_html varchar2(32767);
v_count number :=0;

begin
v_html :=
'
<table align="center" border="1">
<tr style="font-weight:bold;">
<td align="center">Volgnr</td>
<td align="center">Code</td>
<td align="center">Aanspreken</td>
<td align="center">Specialisme</td>
<td align="center">AGB</td>
<td align="center">ActieHouder</td>
</tr>
';
for r_rela in c_rela loop

	v_count := v_count+1;
	v_html := 	v_html ||
				'<tr>' ||
				'<td>' || to_char(v_count) || '</td>' ||
				'<td>' || r_rela.rela_code || '</td>' ||
				'<td>' || r_rela.aanspreken || '</td>' ||
				'<td>' || r_rela.specialisme || '</td>' ||
				'<td>' || r_rela.agb || '</td>' ||
				'<td>' || r_rela.actiehouder || '</td>' ||
				'</tr>';

end loop;

v_html := v_html || '</table>';
v_html := v_html || '<br><br>Deze e-mail is automatisch gegenereerd, door Helix programma: ' || v_this_prog;

if sys_context('userenv','db_name') like 'PROD%' then
    send_mail_html
            (
                p_from => v_afzender,
                p_to   => v_ontvanger,
                p_subject => 'Overzicht relaties',
                p_html => v_html
            );
end if;
exception
 when others then
    dbms_output.put_line(substr(sqlerrm, 1, 2500));
end;
/

/
QUIT
