CREATE OR REPLACE PROCEDURE "HELIX"."BASIS_CONV_2" IS
  CURSOR meti_cur IS
    SELECT meti.meti_id
	,      meti.stgr_id
    FROM   kgc_kgc_afdelingen kafd
    ,      kgc_onderzoeken onde
    ,      bas_metingen  meti
    WHERE  meti.onde_id = onde.onde_id
    and    onde.kafd_id = kafd.kafd_id
    and    kafd.code = 'METAB'
    and    meti.afgerond = 'N'
    AND    meti.stgr_id  IS NOT NULL
    AND    EXISTS
         ( SELECT prst.stof_id
    	   FROM   kgc_stoftesten stof
           ,      kgc_protocol_stoftesten  prst
    	   WHERE  prst.stgr_id = meti.stgr_id
           and    prst.stof_id = stof.stof_id
           and    stof.vervallen = 'N'
    	   AND    NOT EXISTS
    	        ( SELECT '1'
    			  FROM   bas_meetwaarden  meet
    			  WHERE  meet.meti_id = meti.meti_id
                  AND    meet.stof_id = prst.stof_id
    			)
         )
    ;
  CURSOR stof_cur
  ( p_meti_id  IN  NUMBER
  , p_stgr_id  IN  NUMBER
  ) IS
    SELECT prst.stof_id
    ,      prst.volgorde
    ,      nvl( prst.eenheid, stof.eenheid ) eenheid
    FROM   kgc_stoftesten stof
    ,      kgc_protocol_stoftesten  prst
    WHERE  prst.stof_id = stof.stof_id
    and    prst.stgr_id = p_stgr_id
    AND    NOT EXISTS
         ( SELECT '1'
           FROM   bas_meetwaarden  meet
           WHERE  meet.meti_id = p_meti_id
           AND    meet.stof_id = prst.stof_id
         )
    ;

  CURSOR meet_cur IS
    SELECT meti.stgr_id
	,      meet.stof_id
	,      meet.meet_id
	FROM   bas_metingen     meti
	,      bas_meetwaarden  meet
	WHERE  meti.meti_id     = meet.meti_id
	AND    meti.stgr_id     IS NOT NULL
	AND    meet.volgorde    IS NULL
	AND    EXISTS
	     ( SELECT '1'
		   FROM   kgc_protocol_stoftesten  prst
		   WHERE  prst.stgr_id  = meti.stgr_id
		   AND    prst.stof_id  = meet.stof_id
		   AND    prst.volgorde IS NOT NULL
	     )
    ;
BEGIN
  --  Vul 'ontbrekende' meetwaarde rijen aan met stoftesten uit protocol in meting
  --
  FOR meti_rec IN meti_cur
  LOOP
    FOR stof_rec IN stof_cur( meti_rec.meti_id, meti_rec.stgr_id )
    LOOP
      BEGIN
	    INSERT INTO bas_meetwaarden (
               meet_id
        ,      meti_id
        ,      stof_id
        ,      afgerond
        ,      meet_reken
        ,      meeteenheid
        ,      created_by
        ,      creation_date
        ,      last_updated_by
        ,      last_update_date
        ,      volgorde
		)
        SELECT bas_meet_seq.nextval
        ,      meti_rec.meti_id
        ,      stof_rec.stof_id
        ,      stof_rec.eenheid
        ,      'N'
        ,      'M'
        ,      'CONVERSIE'
        ,      SYSDATE
        ,      'CONVERSIE'
        ,      SYSDATE
        ,      stof_rec.volgorde
        FROM   dual
		WHERE  rownum <= 1
		;
      EXCEPTION
	    WHEN OTHERS THEN NULL;
	  END;
    END LOOP;
	COMMIT;
  END LOOP;
  --  Vul volgorde in voor meetwaarde rijen (overnemen uit stoftest)
  --
  FOR meet_rec IN meet_cur
  LOOP
    UPDATE bas_meetwaarden  meet
	SET    meet.volgorde    =
         ( SELECT prst.volgorde
		   FROM   kgc_protocol_stoftesten  prst
		   WHERE  prst.stgr_id = meet_rec.stgr_id
		   AND    prst.stof_id = meet_rec.stof_id
		 )
    WHERE  meet.meet_id = meet_rec.meet_id;
  END LOOP;
  COMMIT;

END;

/

/
QUIT
