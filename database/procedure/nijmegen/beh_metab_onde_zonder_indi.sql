CREATE OR REPLACE PROCEDURE "HELIX"."BEH_METAB_ONDE_ZONDER_INDI" (p_dagen_terug in number default 3) as
--mantisnr 9185
v_this_prog varchar2(100) := 'beh_metab_onde_zonder_indi';
v_email_from kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_STGEN_EMAIL_FROM'));
v_email_to varchar2(200) := 'LKNbasisdiagnostiek@cukz.umcn.nl';
v_msg clob := '<table align=center border=1><tr><td>Nr</td><td>Onderzoeknr</td><td>Onde_type</td><td>Datum_binnen</td><td>Groep</td></tr>';
v_count number(10) := 0;
v_subject varchar2(500) := 'Onderzoeken zonder indicatie (sinds: ' || trunc(sysdate-p_dagen_terug) || ')';

cursor c_onde is
select onde.onderzoeknr, onde.onderzoekstype, onde.datum_binnen, ongr.code groep
from kgc_onderzoeken onde, kgc_kgc_afdelingen kafd, kgc_onderzoeksgroepen ongr
where onde.kafd_id = kafd.kafd_id
and onde.ongr_id = ongr.ongr_id
-- alleen onderzoeken die <p_dagen_terug> gemeld zijn.
and trunc(onde.creation_date) = trunc(sysdate-p_dagen_terug)
and kafd.code = 'METAB'
and kgc_indi_00.indi_code( onde.onde_id ) is null
order by onde.creation_date desc
;

begin
for r_onde in c_onde loop
  v_count := v_count + 1;

  v_msg := v_msg || '<tr><td>'|| to_char(v_count) ||'</td><td>' || r_onde.onderzoeknr || '</td><td>' || r_onde.onderzoekstype || '</td><td>' || r_onde.datum_binnen ||'</td><td>' || r_onde.groep ||'</td></tr>';

end loop;

v_msg := v_msg || '</table><br><br><br>';

v_msg := v_msg || 'Deze e-mail is automatisch door Helix procedure: ' || v_this_prog || ' gegenereerd en verzonden.<br><br><br>';
v_msg := v_msg || 'Met vriendelijke groeten,<br>';
v_msg := v_msg || 'Serviceteam Genetica<br>';

if (substr(sys_context('userenv','db_name'), 1, 4) = 'PROD' and v_count > 0) then
  send_mail_html(
      p_from => v_email_from
    , p_to => v_email_to
    , p_subject => v_subject
    , p_html => v_msg
  );
end if;

exception
  when others then
  raise;
end;
/

/
QUIT
