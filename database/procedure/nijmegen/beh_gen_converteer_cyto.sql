CREATE OR REPLACE PROCEDURE "HELIX"."BEH_GEN_CONVERTEER_CYTO"
IS

  -- medewerkers
  cursor c_mede
  is
  select mede_id, autorisator, func_id, rol, ondertekenaar
  from kgc_mede_kafd
  where mede_id in
  (
  select mede_id from (
  select mede_id from kgc_mede_kafd
  where kafd_id = 3
  minus
  select mede_id from kgc_mede_kafd
  where kafd_id = 1 )
  )
  and kafd_id = 3
  ;

  -- conclusie teksten
  cursor c_conc
  is
  select conc_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from kgc_conclusie_teksten conc
  where kafd_id = 3
  ;

  -- toelichting teksten
  cursor c_tote
  is
  select tote_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from kgc_toelichting_teksten tote
  where kafd_id = 3
  and code not in (select code from kgc_toelichting_teksten where kafd_id = 1)
  ;

  -- materialen
  cursor c_mate
  is
  select mate_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      aantal_etiketten
  ,      letter_monsternr
  ,      eenheid
  ,      vervallen
  from kgc_materialen
  where kafd_id = 3
  and vervallen = 'N'
  ;

  -- stoftestgroepen
  cursor c_stgr
  is
  select stgr_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      protocol
  ,      vervallen
  from   kgc_stoftestgroepen
  where  kafd_id = 3
  and    code not in (select code from kgc_stoftestgroepen where kafd_id = 1)
  and vervallen = 'N'
  ;

  -- stoftestgroep gebruik
  cursor c_stgg
  is
  select stgg_id
  ,      stgg.stgr_id       oud_stgr_id
  ,      stgr.nieuw_stgr_id nieuw_stgr_id
  ,      stgg.indi_id       oud_indi_id
  ,      indi.nieuw_indi_id nieuw_indi_id
  ,      stgg.frty_id       oud_frty_id
  ,      frty.nieuw_frty_id nieuw_frty_id
  ,      stgg.onwy_id       oud_onwy_id
  ,      onwy.nieuw_onwy_id nieuw_onwy_id
  ,      stgg.mate_id       oud_mate_id
  ,      mate.nieuw_mate_id nieuw_mate_id
  ,      kafd_id
  ,      standaard
  ,      screeningtest
  ,      getal_opnemen
  ,      snelheid
  ,      volgorde
  ,      afnametijd
  ,      vervallen
  from   kgc_stoftestgroep_gebruik stgg
  ,      beh_gen_indi_oud_nieuw indi
  ,      beh_gen_stgr_oud_nieuw stgr
  ,      beh_gen_frty_oud_nieuw frty
  ,      beh_gen_onwy_oud_nieuw onwy
  ,      beh_gen_mate_oud_nieuw mate
  where  stgg.indi_id = indi.oud_indi_id(+)
  and    stgg.stgr_id = stgr.oud_stgr_id
  and    stgg.frty_id = frty.oud_frty_id(+)
  and    stgg.onwy_id = onwy.oud_onwy_id(+)
  and    stgg.mate_id = mate.oud_mate_id(+)
  and    kafd_id = 3
  ;

  -- onderzoeksgroepen
  cursor c_ongr
  is
  select ongr_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from   kgc_onderzoeksgroepen
  where  kafd_id = 3
  and    vervallen = 'N'
  ;


  -- onderzoekswijzen
  cursor c_onwy
  is
  select onwy_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from   kgc_onderzoekswijzen
  where  kafd_id = 3
  and    vervallen = 'N'
  ;

  -- STOFTESTEN
  cursor c_stof
  is

  select stof_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      meet_reken
  ,      aantal_decimalen
  ,      eenheid
  ,      stof_id_voor
  ,      tech_id
  ,      mwst_id
  ,      datum_vervallen
  ,      omschrijving_lang
  ,      afgeleide_stoftest
  ,      scherm_voor_details
  ,      vervallen
  from  kgc_stoftesten
  where kafd_id = 3
  and   vervallen = 'N'
  and   code not in (select code from kgc_stoftesten where kafd_id = 1)
  ;

  -- onderzoek uitslagcodes
  cursor c_onui
  is
  select onui_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from   kgc_onderzoek_uitslagcodes
  where  kafd_id = 3
  and    vervallen = 'N'
  ;

  -- onderzoeksgredenen
  cursor c_onre (b_ongr_id in number)
  is
  select ongr_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from   kgc_onderzoeksredenen
  where  kafd_id = 3
  and    ongr_id = b_ongr_id
  ;

  -- bewaarredenen
  cursor c_bwre (b_ongr_id in number)
  is
  select ongr_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from   kgc_bewaarredenen
  where  kafd_id = 3
  and    ongr_id = b_ongr_id
  ;


  -- Fractietypes
  cursor c_frty (b_ongr_id in number)
  is
  select frty_id
  ,      ongr_id
  ,      code
  ,      omschrijving
  ,      standaard
  ,      vervallen
  ,      letter_fractienr
  ,      volgorde
  from   bas_fractie_types
  where  ongr_id = b_ongr_id
  ;

  -- indicatie teksten
  cursor c_indi
  is
  select   indi.indi_id
  ,        ingr.nieuw_ingr_id
  ,        ongr.nieuw_ongr_id
  ,        indi.kafd_id
  ,        indi.code
  ,        indi.omschrijving
  ,        indi.vervallen
  ,        indi.omschrijving_reden
  ,        indi.afwijking
  ,        indi.fare_id
  ,        indi.entiteit
  ,        indi.locus
  from   kgc_indicatie_teksten indi
  ,      beh_gen_ongr_oud_nieuw    ongr
  ,      beh_gen_ingr_oud_nieuw    ingr
  where  indi.kafd_id = 3
  and    indi.ongr_id = ongr.oud_ongr_id
  and    indi.ingr_id = ingr.oud_ingr_id
  ;

  -- kweekfractie types
  cursor c_ftkt (b_kwty_id in number)
  is
  select ftkt.ftkt_id
  ,      frty.nieuw_frty_id
  ,      ftkt.kwty_id
  ,      ftkt.standaard
  from   kgc_frty_kwty       ftkt
  ,      beh_gen_frty_oud_nieuw  frty
  where  ftkt.frty_id = frty.oud_frty_id
  and    ftkt.kwty_id = b_kwty_id
  ;

  cursor c_kwty_2 (b_mate_id in  number, b_code in varchar2, b_indi_id in number, b_opslagmonster in varchar2)
  is
  select 1
  from kgc_kweektypes
  where mate_id = b_mate_id
  and   code = b_code
  and   nvl(indi_id,1) = nvl(b_indi_id,1)
  and   nvl(opslagmonster,1) = nvl(b_opslagmonster,1)
  ;

  -- kweektype
  cursor c_kwty
  is
  select kwty.kwty_id
  ,      kwty.mate_id       oud_mate_id
  ,      mate.nieuw_mate_id nieuw_mate_id
  ,      kwty.code
  ,      kwty.standaard
  ,      kwty.volgorde
  ,      kwty.aantal_kweken
  ,      kwty.kweekduur
  ,      kwty.prefix
  ,      kwty.omschrijving
  ,      kwty.nummering_na_prefix
  ,      kwty.kafd_id
  ,      kwty.indi_id       oud_indi_id
  ,      indi.nieuw_indi_id nieuw_indi_id
  ,      kwty.opslagmonster
  ,      kwty.vervallen
  from   kgc_kweektypes kwty
  ,      beh_gen_indi_oud_nieuw  indi
  ,      beh_gen_mate_oud_nieuw  mate
  where  kwty.kafd_id = 3
  and    kwty.mate_id = mate.oud_mate_id(+)
  and    kwty.indi_id = indi.oud_indi_id(+)
  and not exists (select 1 from kgc_kweektypes a where A.CODE = kwty.code and a.opslagmonster = kwty.opslagmonster and a.mate_id = mate.nieuw_mate_id and a.indi_id = indi.nieuw_indi_id)
  ;

  v_dummy number;

  cursor c_stft
  is

  select stft_id
  ,      stft.mate_id       oud_mate_id
  ,      mate.nieuw_mate_id nieuw_mate_id
  ,      stft.ongr_id       oud_ongr_id
  ,      ongr.nieuw_ongr_id nieuw_ongr_id
  ,      stft.frty_id       oud_frty_id
  ,      frty.nieuw_frty_id nieuw_frty_id
  from   BAS_STAND_FRACTIETYPES stft
  ,      beh_gen_ongr_oud_nieuw  ongr
  ,      beh_gen_mate_oud_nieuw  mate
  ,      beh_gen_frty_oud_nieuw  frty
  where  stft.frty_id = frty.oud_frty_id
  and    stft.mate_id = mate.oud_mate_id
  and    stft.ongr_id = ongr.oud_ongr_id
  ;


  -- indicatie groepen
  cursor c_ingr (b_ongr_id in number)
  is
  select ingr_id
  ,      ongr_id
  ,      code
  ,      omschrijving
  ,      vervallen
  ,      machtigingsindicatie
  ,      aantal_declaraties
  from   kgc_indicatie_groepen
  where  kafd_id = 3
  and    ongr_id = b_ongr_id
  ;


  -- looptijden
  cursor c_loop (b_indi_id in number)
  is
  select loop_id
  ,      onderzoekswijze
  ,      doorlooptijd
  ,      signaleren_na
  ,      indi_id
  from   kgc_looptijden
  where  indi_id = b_indi_id
  ;

  -- brieven opmaak
  cursor c_rapp (b_ongr_id in number)
  is
  select rapp_id
  ,      ramo_id
  ,      rapport
  ,      taal_id
  ,      ongr_id
  ,      omschrijving
  ,      direct_naar_printer
  ,      registreer_via_preview
  from   kgc_rapporten
  where  kafd_id = 3
  and    ongr_id = b_ongr_id
  ;

  -- rapport teksten
  cursor c_rate (b_rapp_id in number)
  is
  select   rate_id
  ,        rapp_id
  ,        code
  ,        volgorde
  ,        tekst
  from   kgc_rapport_teksten
  where  rapp_id = b_rapp_id
  ;

  -- rapport printers
  cursor c_rapr (b_rapp_id in number)
  is
  select     rapr_id
  ,          rapp_id
  ,          prin_id
  ,          mede_id
  from   kgc_rapport_printers
  where  rapp_id = b_rapp_id
  ;

  -- rapporttekst waarden
  cursor c_rtwa (b_rate_id in number)
  is
  select rtwa_id
  ,      rate_id
  ,      code
  ,      waarde
  from   kgc_rapporttekst_waarden
  where  rate_id = b_rate_id
  ;

  -- systeemparameter waarden
  cursor c_spwa (b_ongr_id in number)
  is
  select spwa_id
  ,      sypa_id
  ,      kafd_id
  ,      ongr_id
  ,      mede_id
  ,      waarde
  from   kgc_systeem_par_waarden
  where  ongr_id = b_ongr_id
  ;

  cursor c_leef
  is
  select leef_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      bovengrens
  ,      ondergrens
  ,      vervallen
  from kgc_leeftijden
  where kafd_id = 3
  ;

  cursor c_rete
  is
  select rete_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      vervallen
  from kgc_resultaat_teksten
  where kafd_id = 3
  and code not in (select code from kgc_resultaat_teksten where kafd_id = 1)
  ;

  cursor c_reui
  is
  select reui.reui_id
  ,      ongr.nieuw_ongr_id nieuw_ongr_id
  ,      reui.stgr_id       oud_stgr_id
  ,      stgr.nieuw_stgr_id nieuw_stgr_id
  ,      reui.stof_id       oud_stof_id
  ,      stof.nieuw_stof_id nieuw_stof_id
  ,      reui.tekst
  ,      reui.als_afgerond
  ,      reui.volgorde
  ,      reui.in_tekst
  ,      reui.in_toelichting
  from kgc_resultaat_uitslag reui
  ,    beh_gen_ongr_oud_nieuw ongr
  ,    beh_gen_stgr_oud_nieuw stgr
  ,    beh_gen_stof_oud_nieuw stof
  where reui.stof_id = stof.oud_stof_id(+)
  and   reui.stgr_id = stgr.oud_stgr_id(+)
  and   reui.ongr_id = ongr.oud_ongr_id
  ;


  cursor c_rang
  is
  select rang.rang_id
  ,      kafd_id
  ,      mate.nieuw_mate_id nieuw_mate_id
  ,      leef.nieuw_leef_id nieuw_leef_id
  ,      rang.frty_id       oud_frty_id
  ,      frty.nieuw_frty_id nieuw_frty_id
  ,      rang.stgr_id       oud_stgr_id
  ,      stgr.nieuw_stgr_id nieuw_stgr_id
  ,      stof.nieuw_stof_id nieuw_stof_id
  ,      rang.vervallen
  ,      rang.datum_vervallen
  ,      rang.bety_id
  ,      rang.geslacht
  ,      rang.laag
  ,      rang.hoog
  ,      rang.gemiddeld
  from bas_ranges rang
  ,    beh_gen_frty_oud_nieuw frty
  ,    beh_gen_stgr_oud_nieuw stgr
  ,    beh_gen_stof_oud_nieuw stof
  ,    beh_gen_leef_oud_nieuw leef
  ,    beh_gen_mate_oud_nieuw mate
  where rang.stof_id = stof.oud_stof_id
  and   rang.stgr_id = stgr.oud_stgr_id(+)
  and   rang.frty_id = frty.oud_frty_id(+)
  and   rang.leef_id = leef.oud_leef_id
  and   rang.mate_id = mate.oud_mate_id
  and   rang.kafd_id = 3
  ;

  cursor c_mere
  is

  select mere_id
  ,      stof.nieuw_stof_id nieuw_stof_id
  ,      mere.stgr_id       oud_stgr_id
  ,      stgr.nieuw_stgr_id nieuw_stgr_id
  ,      meting_overschrijven
  ,      als_meetwaarde_afgerond
  ,      excl_grenzen
  ,      waarde_va
  ,      waarde_tm
  ,      controle_functie
  ,      controle_strengheid
  ,      controle_foutmelding
  ,      tekst
  from kgc_meetwaarde_resultaat mere
  ,    beh_gen_stgr_oud_nieuw stgr
  ,    beh_gen_stof_oud_nieuw stof
  where mere.stof_id = stof.oud_stof_id
  and   mere.stgr_id = stgr.oud_stgr_id(+)
  ;

  -- Probes
  cursor c_pros
  is
  select pros_id
  ,      kafd_id
  ,      omschrijving
  ,      chromosoom
  ,      locus
  ,      gebruiken
  ,      code
  ,      stof_id oud_stof_id
  ,      stof.nieuw_stof_id nieuw_stof_id
  ,      gen
  ,      mb_positie_va
  ,      mb_positie_tm
  ,      kleur
  ,      status
  ,      indicatie
  ,      selectie_marker
  ,      datum_ontvangst
  ,      versienr
  ,      datum_laatste_versie
  ,      bron
  ,      leverancier
  ,      catalogusnr
  ,      opmerkingen
  from kgc_probes pros
  ,    beh_gen_stof_oud_nieuw stof
  where kafd_id = 3
  and   pros.stof_id = stof.oud_stof_id(+)
  ;

  cursor c_prst
  is
  select  prst_id
  ,       stof.nieuw_stof_id stof_id
  ,       standaard
  ,       volgorde
  ,       aantal_decimalen
  ,       eenheid
  ,       stgr.nieuw_stgr_id  stgr_id
  ,       voorraad_mutatie
  ,       afnametijd
  from kgc_protocol_stoftesten prst
  ,    beh_gen_stof_oud_nieuw stof
  ,    beh_gen_stgr_oud_nieuw stgr
  where prst.stof_id = stof.oud_stof_id
  and   prst.stgr_id = stgr.oud_stgr_id
  ;

  cursor c_teco
  is
  select  teco_id
  ,       kafd_id
  ,       ongr_id            oud_ongr_id
  ,       ongr.nieuw_ongr_id nieuw_ongr_id
  ,       stgr_id            oud_stgr_id
  ,       stgr.nieuw_stgr_id nieuw_stgr_id
  ,       indi_id            oud_indi_id
  ,       indi.nieuw_indi_id nieuw_indi_id
  ,       rete_id            oud_rete_id
  ,       rete.nieuw_rete_id nieuw_rete_id
  ,       tote_id            oud_tote_id
  ,       tote.nieuw_tote_id nieuw_tote_id
  ,       conc_id            oud_conc_id
  ,       conc.nieuw_conc_id nieuw_conc_id
  from kgc_teksten_context teco
  ,    beh_gen_stgr_oud_nieuw stgr
  ,    beh_gen_ongr_oud_nieuw ongr
  ,    beh_gen_indi_oud_nieuw indi
  ,    beh_gen_rete_oud_nieuw rete
  ,    beh_gen_tote_oud_nieuw tote
  ,    beh_gen_conc_oud_nieuw conc
  where teco.kafd_id = 3
  and   teco.stgr_id = stgr.oud_stgr_id(+)
  and   teco.ongr_id = ongr.oud_ongr_id(+)
  and   teco.indi_id = indi.oud_indi_id(+)
  and   teco.rete_id = rete.oud_rete_id(+)
  and   teco.tote_id = tote.oud_tote_id(+)
  and   teco.conc_id = conc.oud_conc_id(+)
  ;

  cursor c_stan
  is
  select  stan_id
  ,       ongr_id            oud_ongr_id
  ,       ongr.nieuw_ongr_id nieuw_ongr_id
  ,       mate_id            oud_mate_id
  ,       mate.nieuw_mate_id nieuw_mate_id
  ,       mede_id
  ,       standaardfractienummer
  ,       datum_aanmaken
  ,       commentaar
  from bas_standaardfracties stan
  ,    beh_gen_mate_oud_nieuw mate
  ,    beh_gen_ongr_oud_nieuw ongr
  where stan.mate_id = mate.oud_mate_id
  and   stan.ongr_id = ongr.oud_ongr_id
  and   stan.vervallen = 'N'
  ;

  -- etiket parameters
  cursor c_etpa
  is
  select etpa_id
  ,      kafd_id
  ,      code
  ,      omschrijving
  ,      type_etiket
  ,      omsluitteken
  ,      scheidingsteken
  ,      locatie
  ,      kop
  ,      ongr_id            oud_ongr_id
  ,      ongr.nieuw_ongr_id nieuw_ongr_id
  ,      regel
  from kgc_etiket_parameters etpa
  ,    beh_gen_ongr_oud_nieuw ongr
  where etpa.kafd_id = 3
  and   etpa.ongr_id = ongr.oud_ongr_id(+)
  ;

  -- etiket voorkeuren
  cursor c_etvo
  is
  select etvo_id
  ,      etpa.nieuw_etpa_id etpa_id
  ,      mede_id
  from kgc_etiket_voorkeuren etvo
  ,    beh_gen_etpa_oud_nieuw etpa
  where etvo.etpa_id = etpa.oud_etpa_id
  ;

  cursor c_ifmd
  is
  select ifmd_id
  ,      kafd_id
  ,      meta_type
  ,      code
  ,      waarde
  ,      tech_id
  from kgc_interface_meta_data
  where kafd_id = 3
  and   meta_type||code not in (select meta_type||code from KGC_INTERFACE_META_DATA where kafd_id = 1)
  ;

  cursor c_dewy_cyto
  is
  select dewy_id dewy_id_cyto
  ,      code
  from   kgc_declaratiewijzen
  where  kafd_id = 3
  ;

  cursor c_dewy_dna(b_code in varchar2)
  is
  select dewy_id dewy_id_dna
  from   kgc_declaratiewijzen
  where  kafd_id = 1
  and    code = b_code
  ;

  cursor c_deen
  is
  select ongr.nieuw_ongr_id  nieuw_ongr_id
  ,      deen.ongr_id        oud_ongr_id
  ,      ingr.nieuw_ingr_id  nieuw_ingr_id
  ,      ingr_id             oud_ingr_id
  ,      dewy.nieuw_dewy_id  nieuw_dewy_id
  ,      dewy_id             oud_dewy_id
  ,      deen_id
  ,      volgorde
  ,      entiteit
  ,      declarabel
  ,      declaratie_maken
  ,      toelichting
  ,      betreft_tekst
  ,      verrichtingcode
  ,      datum_wijziging_doorgevoerd
  ,      datum_vervallen
  ,      opmerkingen
  from kgc_decl_entiteiten deen
  ,    beh_gen_ongr_oud_nieuw ongr
  ,    beh_gen_ingr_oud_nieuw ingr
  ,    beh_gen_dewy_oud_nieuw dewy
  where kafd_id = 3
  and   deen.ongr_id = ongr.oud_ongr_id(+)
  and   deen.ingr_id = ingr.oud_ingr_id(+)
  and   deen.dewy_id = dewy.oud_dewy_id(+)
  and   deen.vervallen = 'N'
  ;

  cursor c_decr (b_deen_id in number)
  is
  select deen_id
  ,      tabelnaam
  ,      kolomnaam
  ,      waarde
  from kgc_decl_criteria
  where deen_id = b_deen_id
  ;

  cursor c_beh_gen_stof (b_oud_stof_id in number)
  is
  select nieuw_stof_id
  from beh_gen_stof_oud_nieuw
  where oud_stof_id = b_oud_stof_id
  ;

  cursor c_max_deen
  is
  select max(volgorde)
  from kgc_decl_entiteiten
  where kafd_id = 1
  ;


  v_rapr_id number;
  v_rate_id number;
  v_rtwa_id number;
  v_spwa_id number;
  v_ftkt_id number;
  v_kwty_id number;
  v_loop_id number;
  v_indi_id number;

  v_ongr_id number;
  v_onre_id number;
  v_bwre_id number;
  v_frty_id number;
  v_ingr_id number;
  v_reui_id number;
  v_rapp_id number;
  v_meka_id number;
  v_onwy_id number;
  v_onui_id number;
  v_stof_id_voor_nieuw number;
  v_stof_id number;
  v_leef_id number;
  v_rete_id number;
  v_rang_id number;
  v_stgr_id number;
  v_stgg_id number;
  v_mate_id number;
  v_mere_id number;
  v_pros_id number;
  v_prst_id number;
  v_tote_id number;
  v_conc_id number;
  v_teco_id number;
  v_etpa_id number;
  v_etvo_id number;
  v_ifmd_id number;
  v_stft_id number;
  v_max_deen_volgorde number := 0;
  v_dewy_id number;
  v_deen_id number;
  v_decr_id number;
  v_dewy_id_dna number;
  v_stan_id number;

/***********************************************************************
Auteur/bedrijf  : Y.Arts, YA IT-Services
Datum onderhoud : 22-03-2010

DOEL
Deze procedure start de conversie van CYTO referentiedata naar GENOOM referentie data.

INPUT PARAMETERS

OUTPUT PARAMETERS

HISTORIE

Wanneer/    Wie             Wat
Versie
------------------------------------------------------------------------
22-03-2010  Y.Arts       Creatie
1.0.0
***********************************************************************/
BEGIN


  execute immediate 'alter table kgc_systeem_par_waarden disable all triggers';

  -- disable trigger CG$BUR_KGC_SYSTEEM_PAR_WAARDEN

  update kgc_systeem_par_waarden
  set waarde = 'GENOOM'
  where (UPPER(waarde) = 'DNA' or UPPER(waarde)  = 'CYTO')
  and sypa_id = (select sypa_id from kgc_systeem_parameters where code = 'MEDEWERKER_AFDELING')
  ;


  execute immediate 'alter table kgc_systeem_par_waarden enable all triggers';


  for r_mede in c_mede loop

    select kgc_meka_seq.nextval into v_meka_id from dual;

    -- kopieer de medewerker
    insert into kgc_mede_kafd
    (meka_id
    ,mede_id
    ,kafd_id
    ,autorisator
    ,func_id
    ,rol
    ,ondertekenaar
    ) values
    (v_meka_id
    ,r_mede.mede_id
    ,1
    ,r_mede.autorisator
    ,r_mede.func_id
    ,r_mede.rol
    ,r_mede.ondertekenaar
    );

  end loop;

  Update kgc_prog_parameters
  set default_waarde = 'GENOOM'
  where default_waarde = 'DNA' or default_waarde = 'CYTO'
  ;

  Update kgc_medewerker_criteria
  set waarde = 'GENOOM'
  where waarde = 'DNA' or waarde = 'CYTO'
  ;

  Update kgc_interface_samenstelling
  set default_waarde = 'GENOOM'
  where default_waarde = 'DNA' or default_waarde = 'CYTO'
  ;

  -- KGC_MATERIALEN
  -- eerst de overeenkomende codes van cyto updaten om ze unike te krijgen
  update kgc_materialen
  set code = code||'_'
  where kafd_id = 3
  and code in (select code from kgc_materialen where kafd_id = 1)
  ;


  -- KGC_STOFTESTEN
  -- eerst de overeenkomende codes van dna updaten om ze uniek te krijgen
  update kgc_stoftesten
  set code = code||'_DNA'
  where stof_id in (
    select b.stof_id
    from kgc_stoftesten a
    ,    kgc_stoftesten b
    where a.kafd_id = 3
    and   b.kafd_id = 1
    and   a.code = b.code
    and   a.omschrijving <> b.omschrijving
    and   a.vervallen = 'N'
  )
  ;

  update KGC_ONDERZOEK_UITSLAGCODES
  set code = code||'_'
  where kafd_id = 3
  and   code in (select code from KGC_ONDERZOEK_UITSLAGCODES where kafd_id = 1)
  ;


  -- toelichting teksten
  for r_tote in c_tote loop

    select kgc_tote_seq.nextval into v_tote_id from dual;

    -- kopieer de toelichting teksten
    insert into kgc_toelichting_teksten
    (tote_id
    ,kafd_id
    ,code
    ,omschrijving
    ,vervallen
    ) values
    (v_tote_id
    ,1
    ,r_tote.code
    ,r_tote.omschrijving
    ,r_tote.vervallen
    );

    insert into beh_gen_tote_oud_nieuw values (r_tote.tote_id, v_tote_id);

  end loop;


  -- conclusie teksten
  for r_conc in c_conc loop

    select kgc_conc_seq.nextval into v_conc_id from dual;

    -- kopieer de conclusie teksten
    insert into kgc_conclusie_teksten
    (conc_id
    ,kafd_id
    ,code
    ,omschrijving
    ,vervallen
    ) values
    (v_conc_id
    ,1
    ,r_conc.code
    ,r_conc.omschrijving
    ,r_conc.vervallen
    );

    insert into beh_gen_conc_oud_nieuw values (r_conc.conc_id, v_conc_id);

  end loop;


  -- Materialen onder GENOOM hangen
  for r_mate in c_mate loop

    select kgc_mate_seq.nextval into v_mate_id from dual;

    -- kopieer de materialen
    insert into kgc_materialen
    (mate_id
    ,kafd_id
    ,code
    ,omschrijving
    ,aantal_etiketten
    ,letter_monsternr
    ,eenheid
    ,vervallen
    ) values
    (v_mate_id
    ,1
    ,r_mate.code
    ,r_mate.omschrijving
    ,r_mate.aantal_etiketten
    ,r_mate.letter_monsternr
    ,r_mate.eenheid
    ,r_mate.vervallen
    );

    insert into beh_gen_mate_oud_nieuw values (r_mate.mate_id, v_mate_id);

  end loop; -- c_mate

  -- Stoftestgroepen
  for r_stgr in c_stgr loop

    select kgc_stgr_seq.nextval into v_stgr_id from dual;

    -- kopieer de stoftesten
    insert into kgc_stoftestgroepen
    (stgr_id
    ,kafd_id
    ,code
    ,omschrijving
    ,protocol
    ,vervallen
    ) values
    (v_stgr_id
    ,1
    ,r_stgr.code
    ,r_stgr.omschrijving
    ,r_stgr.protocol
    ,r_stgr.vervallen
    );

    insert into beh_gen_stgr_oud_nieuw values (r_stgr.stgr_id, v_stgr_id);

  end loop; -- c_stgr



  -- Onderzoekswijzen
  for r_onwy in c_onwy loop

    select kgc_onwy_seq.nextval into v_onwy_id from dual;

    -- kopieer de medewerker
    insert into kgc_onderzoekswijzen
    (onwy_id
    ,kafd_id
    ,code
    ,omschrijving
    ,vervallen
    ) values
    (v_onwy_id
    ,1
    ,r_onwy.code
    ,r_onwy.omschrijving
    ,r_onwy.vervallen
    );

    insert into beh_gen_onwy_oud_nieuw values (r_onwy.onwy_id, v_onwy_id);

  end loop;

  -- Onderzoek uitslagcodes
  for r_onui in c_onui loop

    select kgc_onui_seq.nextval into v_onui_id from dual;

    -- kopieer de medewerker
    insert into kgc_onderzoek_uitslagcodes
    (onui_id
    ,kafd_id
    ,code
    ,omschrijving
    ,vervallen
    ) values
    (v_onui_id
    ,1
    ,r_onui.code
    ,r_onui.omschrijving
    ,r_onui.vervallen
    );

  end loop;


  -- leeftijden
  for r_leef in c_leef loop

    select kgc_leef_seq.nextval into v_leef_id from dual;

    -- kopieer leeftijden
    insert into kgc_leeftijden
    (leef_id
    ,kafd_id
    ,code
    ,omschrijving
    ,bovengrens
    ,ondergrens
    ,vervallen
    ) values
    (v_leef_id
    ,1
    ,r_leef.code
    ,r_leef.omschrijving
    ,r_leef.bovengrens
    ,r_leef.ondergrens
    ,r_leef.vervallen
    );

    insert into beh_gen_leef_oud_nieuw values (r_leef.leef_id, v_leef_id);


  end loop; -- c_leef


  -- Resultaat teksten
  for r_rete in c_rete loop

    select kgc_rete_seq.nextval into v_rete_id from dual;

    -- kopieer de reultaat teksten
    insert into kgc_resultaat_teksten
    (rete_id
    ,kafd_id
    ,code
    ,omschrijving
    ,vervallen
    ) values
    (v_rete_id
    ,1
    ,r_rete.code
    ,r_rete.omschrijving
    ,r_rete.vervallen
    );

    insert into beh_gen_rete_oud_nieuw values (r_rete.rete_id, v_rete_id);

  end loop; -- c_rete


  -- Stoftesten
  update kgc_stoftesten
  set code = '10_DNA'
  where kafd_id = 1
  and code = '10'
  ;

  for r_stof in c_stof loop

    select kgc_stof_seq.nextval into v_stof_id from dual ;

    if r_stof.stof_id_voor is not null
    then
      open c_beh_gen_stof (b_oud_stof_id => r_stof.stof_id_voor);
      fetch c_beh_gen_stof into v_stof_id_voor_nieuw;
      close c_beh_gen_stof;
    end if;

    -- kopieer de stoftest
    insert into kgc_stoftesten
    (stof_id
    ,kafd_id
    ,code
    ,omschrijving
    ,meet_reken
    ,aantal_decimalen
    ,eenheid
    ,stof_id_voor
    ,tech_id
    ,mwst_id
    ,datum_vervallen
    ,omschrijving_lang
    ,afgeleide_stoftest
    ,scherm_voor_details
    ,vervallen
    ) values
    (v_stof_id
    ,1
    ,r_stof.code
    ,r_stof.omschrijving
    ,r_stof.meet_reken
    ,r_stof.aantal_decimalen
    ,r_stof.eenheid
    ,v_stof_id_voor_nieuw
    ,r_stof.tech_id
    ,r_stof.mwst_id
    ,r_stof.datum_vervallen
    ,r_stof.omschrijving_lang
    ,r_stof.afgeleide_stoftest
    ,r_stof.scherm_voor_details
    ,r_stof.vervallen
    );

    v_stof_id_voor_nieuw := null;

    insert into beh_gen_stof_oud_nieuw values (r_stof.stof_id, v_stof_id);

  end loop; --c_stof

  -- doorloop alle onderzoeksgroepen van CYTO
  for r_ongr in c_ongr loop--ongr

    select kgc_ongr_seq.nextval into v_ongr_id from dual ;

    -- kopieer het onderzoeksgroep
    insert into kgc_onderzoeksgroepen
    (ongr_id
    ,kafd_id
    ,code
    ,omschrijving
    ,vervallen
    ) values
    (v_ongr_id
    ,1
    ,r_ongr.code
    ,r_ongr.omschrijving
    ,r_ongr.vervallen
    );

    insert into beh_gen_ongr_oud_nieuw values (r_ongr.ongr_id, v_ongr_id);

    for r_onre in c_onre (b_ongr_id => r_ongr.ongr_id) loop

      select kgc_onre_seq.nextval into v_onre_id from dual ;

      -- kopieer het onderzoeksgroep
      insert into kgc_onderzoeksredenen
      (onre_id
      ,ongr_id
      ,kafd_id
      ,code
      ,omschrijving
      ,vervallen
      ) values
      (v_onre_id
      ,v_ongr_id
      ,1
      ,r_onre.code
      ,r_onre.omschrijving
      ,r_onre.vervallen
      );

    end loop;

    for r_bwre in c_bwre (b_ongr_id => r_ongr.ongr_id) loop

      select kgc_bwre_seq.nextval into v_bwre_id from dual ;

      -- kopieer het onderzoeksgroep
      insert into kgc_bewaarredenen
      (bwre_id
      ,ongr_id
      ,kafd_id
      ,code
      ,omschrijving
      ,vervallen
      ) values
      (v_bwre_id
      ,v_ongr_id
      ,1
      ,r_bwre.code
      ,r_bwre.omschrijving
      ,r_bwre.vervallen
      );

    end loop;

    for r_frty in c_frty (b_ongr_id => r_ongr.ongr_id) loop--frty

      select bas_frty_seq.nextval into v_frty_id from dual ;

      -- kopieer de fractie types
      insert into bas_fractie_types
      (frty_id
      ,ongr_id
      ,code
      ,omschrijving
      ,standaard -- toevoeging MG 14-12-2010
      ,vervallen
      ,letter_fractienr
      ,volgorde
      ) values
      (v_frty_id
      ,v_ongr_id
      ,r_frty.code
      ,r_frty.omschrijving
      ,r_frty.standaard
      ,r_frty.vervallen
      ,r_frty.letter_fractienr
      ,r_frty.volgorde
      );

      insert into beh_gen_frty_oud_nieuw values (r_frty.frty_id, v_frty_id);

    end loop;--frty

    for r_ingr in c_ingr (b_ongr_id => r_ongr.ongr_id) loop--ingr

      select kgc_ingr_seq.nextval into v_ingr_id from dual ;

      -- kopieer de indicatie groepen
      insert into kgc_indicatie_groepen
      (ingr_id
      ,ongr_id
      ,code
      ,omschrijving
      ,vervallen
      ,machtigingsindicatie
      ,aantal_declaraties
      ) values
      (v_ingr_id
      ,v_ongr_id
      ,r_ingr.code
      ,r_ingr.omschrijving
      ,r_ingr.vervallen
      ,r_ingr.machtigingsindicatie
      ,r_ingr.aantal_declaraties
      );

      insert into beh_gen_ingr_oud_nieuw values (r_ingr.ingr_id, v_ingr_id);

    end loop;--ingr

    for r_rapp in c_rapp(b_ongr_id => r_ongr.ongr_id) loop--rapp

      select kgc_rapp_seq.nextval into v_rapp_id from dual ;

      -- kopieer de  rapport opmaak
      insert into kgc_rapporten
      (rapp_id
      ,ongr_id
      ,kafd_id
      ,ramo_id
      ,rapport
      ,omschrijving
      ,taal_id
      ,direct_naar_printer
      ,registreer_via_preview
      ) values
      (v_rapp_id
      ,v_ongr_id
      ,1
      ,r_rapp.ramo_id
      ,r_rapp.rapport
      ,r_rapp.omschrijving
      ,r_rapp.taal_id
      ,r_rapp.direct_naar_printer
      ,r_rapp.registreer_via_preview
      );

      for r_rapr in c_rapr(b_rapp_id => r_rapp.rapp_id) loop--rapr

        select kgc_rapr_seq.nextval into v_rapr_id from dual ;

        insert into kgc_rapport_printers
        (rapr_id
        ,rapp_id
        ,prin_id
        ,mede_id
        ) values
        (v_rapr_id
        ,v_rapp_id
        ,r_rapr.prin_id
        ,r_rapr.mede_id
        );

      end loop;--rapr

      for r_rate in c_rate(b_rapp_id => r_rapp.rapp_id) loop--rate

        select kgc_rate_seq.nextval into v_rate_id from dual ;

        insert into kgc_rapport_teksten
        (rate_id
        ,rapp_id
        ,code
        ,volgorde
        ,tekst
        ) values
        (v_rate_id
        ,v_rapp_id
        ,r_rate.code
        ,r_rate.volgorde
        ,r_rate.tekst
        );

        for r_rtwa in c_rtwa(b_rate_id => r_rate.rate_id) loop

          select kgc_rtwa_seq.nextval into v_rtwa_id from dual ;

          insert into kgc_rapporttekst_waarden
          (rtwa_id
          ,rate_id
          ,code
          ,waarde
          ) values
          (v_rtwa_id
          ,v_rtwa_id
          ,r_rtwa.code
          ,r_rtwa.waarde
          );

        end loop;--rtwa

      end loop;--rate

    end loop;--rapp

    for r_spwa in c_spwa (b_ongr_id => r_ongr.ongr_id) loop--spwa

      select kgc_spwa_seq.nextval into v_spwa_id from dual ;

      insert into kgc_systeem_par_waarden
      (spwa_id
      ,sypa_id
      ,kafd_id
      ,ongr_id
      ,mede_id
      ,waarde
      ) values
      (v_spwa_id
      ,r_spwa.sypa_id
      ,1
      ,v_ongr_id
      ,r_spwa.mede_id
      ,r_spwa.waarde
      );

    end loop;--spwa

  end loop;--ongr

  for r_indi in c_indi loop--indi

    select kgc_indi_seq.nextval into v_indi_id from dual ;

    insert into kgc_indicatie_teksten
    (indi_id
    ,ingr_id
    ,ongr_id
    ,kafd_id
    ,code
    ,omschrijving
    ,vervallen
    ,omschrijving_reden
    ,afwijking
    ,fare_id
    ,entiteit
    ,locus
    ) values
    (v_indi_id
    ,r_indi.nieuw_ingr_id
    ,r_indi.nieuw_ongr_id
    ,1
    ,r_indi.code
    ,r_indi.omschrijving
    ,r_indi.vervallen
    ,r_indi.omschrijving_reden
    ,r_indi.afwijking
    ,r_indi.fare_id
    ,r_indi.entiteit
    ,r_indi.locus
    );

    insert into beh_gen_indi_oud_nieuw values (r_indi.indi_id, v_indi_id);


    for r_loop in c_loop(b_indi_id => r_indi.indi_id) loop--loop

      select kgc_loop_seq.nextval into v_loop_id from dual ;

      insert into kgc_looptijden
      (loop_id
      ,indi_id
      ,onderzoekswijze
      ,doorlooptijd
      ,signaleren_na
      ) values
      (v_loop_id
      ,v_indi_id
      ,r_loop.onderzoekswijze
      ,r_loop.doorlooptijd
      ,r_loop.signaleren_na
      );

    end loop;--loop

  end loop;--indi


  for r_kwty in c_kwty loop--kwty

    if (r_kwty.nieuw_mate_id is null and r_kwty.oud_mate_id is not null) or
       (r_kwty.nieuw_indi_id is null and r_kwty.oud_indi_id is not null)
    then
      null;
    else

      open c_kwty_2 (b_mate_id => r_kwty.nieuw_mate_id
                   , b_code => r_kwty.code
                   , b_indi_id =>r_kwty.nieuw_indi_id
                   , b_opslagmonster =>r_kwty.opslagmonster );
      fetch c_kwty_2 into v_dummy;
      if v_dummy is null then

      select kgc_kwty_seq.nextval into v_kwty_id from dual ;

        insert into kgc_kweektypes
        (kwty_id
        ,indi_id
        ,kafd_id
        ,mate_id
        ,code
        ,standaard
        ,volgorde
        ,aantal_kweken
        ,kweekduur
        ,prefix
        ,omschrijving
        ,nummering_na_prefix
        ,opslagmonster
        ,vervallen
        ) values
        (v_kwty_id
        ,r_kwty.nieuw_indi_id
        ,1
        ,r_kwty.nieuw_mate_id
        ,r_kwty.code
        ,r_kwty.standaard
        ,r_kwty.volgorde
        ,r_kwty.aantal_kweken
        ,r_kwty.kweekduur
        ,r_kwty.prefix
        ,r_kwty.omschrijving
        ,r_kwty.nummering_na_prefix
        ,r_kwty.opslagmonster
        ,r_kwty.vervallen
        );

      for r_ftkt in c_ftkt(b_kwty_id => r_kwty.kwty_id) loop--ftkt

        select kgc_ftkt_seq.nextval into v_ftkt_id from dual ;

        insert into kgc_frty_kwty
        (ftkt_id
        ,frty_id
        ,kwty_id
        ,standaard
        ) values
        (v_ftkt_id
        ,r_ftkt.nieuw_frty_id
        ,v_kwty_id
        ,r_ftkt.standaard
        );

      end loop;--ftkt
    end if;
    close c_kwty_2;
    v_dummy := null;


    end if;

  end loop;--kwty

  for r_reui in c_reui loop

    if (r_reui.oud_stgr_id is not null and r_reui.nieuw_stgr_id is null) OR
       (r_reui.oud_stof_id is not null and r_reui.nieuw_stof_id is null)
    then
      null; -- behoort blijkbaar niet tot CYTO
    else

      select kgc_reui_seq.nextval into v_reui_id from dual ;

      -- kopieer de  resultaat uitslagen
      insert into kgc_resultaat_uitslag
      (reui_id
      ,ongr_id
      ,in_tekst
      ,in_toelichting
      ,als_afgerond
      ,volgorde
      ,stgr_id
      ,stof_id
      ,tekst
      ) values
      (v_reui_id
      ,r_reui.nieuw_ongr_id
      ,r_reui.in_tekst
      ,r_reui.in_toelichting
      ,r_reui.als_afgerond
      ,r_reui.volgorde
      ,r_reui.nieuw_stgr_id
      ,r_reui.nieuw_stof_id
      ,r_reui.tekst
      );
    end if;

  end loop;


  for r_rang in c_rang loop

    if (r_rang.oud_frty_id is not null and r_rang.nieuw_frty_id is null) OR
       (r_rang.oud_stgr_id is not null and r_rang.nieuw_stgr_id is null)
    then
      null;
    else

      select bas_rang_seq.nextval into v_rang_id from dual ;

      -- kopieer de ranges
      insert into bas_ranges
      (rang_id
      ,kafd_id
      ,mate_id
      ,leef_id
      ,frty_id
      ,stgr_id
      ,stof_id
      ,vervallen
      ,datum_vervallen
      ,bety_id
      ,geslacht
      ,laag
      ,hoog
      ,gemiddeld
      ) values
      (v_rang_id
      ,1
      ,r_rang.nieuw_mate_id
      ,r_rang.nieuw_leef_id
      ,r_rang.nieuw_frty_id
      ,r_rang.nieuw_stgr_id
      ,r_rang.nieuw_stof_id
      ,r_rang.vervallen
      ,r_rang.datum_vervallen
      ,r_rang.bety_id
      ,r_rang.geslacht
      ,r_rang.laag
      ,r_rang.hoog
      ,r_rang.gemiddeld
      );

    end if;

  end loop;


  for r_stgg in c_stgg loop

    if  (r_stgg.nieuw_stgr_id is null and r_stgg.oud_stgr_id is not null)
     OR (r_stgg.nieuw_indi_id is null and r_stgg.oud_indi_id is not null)
     OR (r_stgg.nieuw_frty_id is null and r_stgg.oud_frty_id is not null)
     OR (r_stgg.nieuw_onwy_id is null and r_stgg.oud_onwy_id is not null)
     OR (r_stgg.nieuw_mate_id is null and r_stgg.oud_mate_id is not null)
    then
      null;
    else

      select kgc_stgg_seq.nextval into v_stgg_id from dual ;

      -- kopieer de stoftestgroep gebruik
      insert into kgc_stoftestgroep_gebruik
      (stgg_id
      ,kafd_id
      ,stgr_id
      ,indi_id
      ,frty_id
      ,onwy_id
      ,mate_id
      ,standaard
      ,screeningtest
      ,getal_opnemen
      ,snelheid
      ,volgorde
      ,afnametijd
      ,vervallen
      ) values
      (v_stgg_id
      ,1
      ,r_stgg.nieuw_stgr_id
      ,r_stgg.nieuw_indi_id
      ,r_stgg.nieuw_frty_id
      ,r_stgg.nieuw_onwy_id
      ,r_stgg.nieuw_mate_id
      ,r_stgg.standaard
      ,r_stgg.screeningtest
      ,r_stgg.getal_opnemen
      ,r_stgg.snelheid
      ,r_stgg.volgorde
      ,r_stgg.afnametijd
      ,r_stgg.vervallen
      );

    end if;

  end loop;

  -- meetwaarde resultaat
  for r_mere in c_mere loop

    if  r_mere.nieuw_stgr_id is null and r_mere.oud_stgr_id is not null
    then
      null;
    else

    select kgc_mere_seq.nextval into v_mere_id from dual ;

    -- kopieer de
    insert into kgc_meetwaarde_resultaat
    (mere_id
    ,stof_id
    ,stgr_id
    ,meting_overschrijven
    ,als_meetwaarde_afgerond
    ,excl_grenzen
    ,waarde_va
    ,waarde_tm
    ,controle_functie
    ,controle_strengheid
    ,controle_foutmelding
    ,tekst
    ) values
    (v_mere_id
    ,r_mere.nieuw_stof_id
    ,r_mere.nieuw_stgr_id
    ,r_mere.meting_overschrijven
    ,r_mere.als_meetwaarde_afgerond
    ,r_mere.excl_grenzen
    ,r_mere.waarde_va
    ,r_mere.waarde_tm
    ,r_mere.controle_functie
    ,r_mere.controle_strengheid
    ,r_mere.controle_foutmelding
    ,r_mere.tekst
    );

    end if;

  end loop;

  -- probes
  for r_pros in c_pros loop

    if r_pros.nieuw_stof_id is null and r_pros.oud_stof_id is not null
    then
      null;
    else

    select kgc_pros_seq.nextval into v_pros_id from dual ;

    -- kopieer de probes
    insert into kgc_probes
    (pros_id
    ,stof_id
    ,kafd_id
    ,omschrijving
    ,chromosoom
    ,locus
    ,gebruiken
    ,code
    ,gen
    ,mb_positie_va
    ,mb_positie_tm
    ,kleur
    ,status
    ,indicatie
    ,selectie_marker
    ,datum_ontvangst
    ,versienr
    ,datum_laatste_versie
    ,bron
    ,leverancier
    ,catalogusnr
    ,opmerkingen
    ) values
    (v_pros_id
    ,r_pros.nieuw_stof_id
    ,1
    ,r_pros.omschrijving
    ,r_pros.chromosoom
    ,r_pros.locus
    ,r_pros.gebruiken
    ,r_pros.code
    ,r_pros.gen
    ,r_pros.mb_positie_va
    ,r_pros.mb_positie_tm
    ,r_pros.kleur
    ,r_pros.status
    ,r_pros.indicatie
    ,r_pros.selectie_marker
    ,r_pros.datum_ontvangst
    ,r_pros.versienr
    ,r_pros.datum_laatste_versie
    ,r_pros.bron
    ,r_pros.leverancier
    ,r_pros.catalogusnr
    ,r_pros.opmerkingen
    );

    end if;

  end loop;

  -- protocol stoftesten
  for r_prst in c_prst loop

    select kgc_prst_seq.nextval into v_prst_id from dual ;

    -- kopieer de protocol_stoftesten
    insert into kgc_protocol_stoftesten
    ( prst_id
    , stof_id
    , standaard
    , volgorde
    , aantal_decimalen
    , eenheid
    , stgr_id
    , voorraad_mutatie
    , afnametijd
    ) values
    (v_prst_id
    ,r_prst.stof_id
    ,r_prst.standaard
    ,r_prst.volgorde
    ,r_prst.aantal_decimalen
    ,r_prst.eenheid
    ,r_prst.stgr_id
    ,r_prst.voorraad_mutatie
    ,r_prst.afnametijd
    );

  end loop;


  -- teksten context
  for r_teco in c_teco loop

    if  (r_teco.nieuw_stgr_id is null and r_teco.oud_stgr_id is not null)
     OR (r_teco.nieuw_ongr_id is null and r_teco.oud_ongr_id is not null)
     OR (r_teco.nieuw_indi_id is null and r_teco.oud_indi_id is not null)
     OR (r_teco.nieuw_rete_id is null and r_teco.oud_rete_id is not null)
     OR (r_teco.nieuw_tote_id is null and r_teco.oud_tote_id is not null)
     OR (r_teco.nieuw_conc_id is null and r_teco.oud_conc_id is not null)
    then
      null;
    else

    select kgc_teco_seq.nextval into v_teco_id from dual ;

    -- kopieer de teksten context
    insert into kgc_teksten_context
    ( teco_id
    , kafd_id
    , ongr_id
    , stgr_id
    , indi_id
    , rete_id
    , tote_id
    , conc_id
    ) values
    (v_teco_id
    ,1
    ,r_teco.nieuw_ongr_id
    ,r_teco.nieuw_stgr_id
    ,r_teco.nieuw_indi_id
    ,r_teco.nieuw_rete_id
    ,r_teco.nieuw_tote_id
    ,r_teco.nieuw_conc_id
    );

    end if;

  end loop;


  -- etiket parameters
  for r_etpa in c_etpa loop

    if r_etpa.nieuw_ongr_id is null and r_etpa.oud_ongr_id is not null
    then
      null;
    else

      select kgc_etpa_seq.nextval into v_etpa_id from dual ;

      -- kopieer de etiket parameters
      insert into kgc_etiket_parameters
      (etpa_id
      ,kafd_id
      ,code
      ,omschrijving
      ,type_etiket
      ,omsluitteken
      ,scheidingsteken
      ,locatie
      ,kop
      ,ongr_id
      ,regel
      ) values
      (v_etpa_id
      ,1
      ,r_etpa.code
      ,r_etpa.omschrijving
      ,r_etpa.type_etiket
      ,r_etpa.omsluitteken
      ,r_etpa.scheidingsteken
      ,r_etpa.locatie
      ,r_etpa.kop
      ,r_etpa.nieuw_ongr_id
      ,r_etpa.regel
      );

      insert into beh_gen_etpa_oud_nieuw values (r_etpa.etpa_id, v_etpa_id);

    end if;

  end loop;

  -- etiket voorkeuren
  for r_etvo in c_etvo loop

    select kgc_etvo_seq.nextval into v_etvo_id from dual ;

    -- kopieer de etiket voorkeuren
    insert into kgc_etiket_voorkeuren
    (etvo_id
    ,etpa_id
    ,mede_id
    ) values
    (v_etvo_id
    ,r_etvo.etpa_id
    ,r_etvo.mede_id
    );

  end loop;


 -- interface meta data
  for r_ifmd in c_ifmd loop

    select kgc_ifmd_seq.nextval into v_ifmd_id from dual ;

    -- kopieer de interface meta data
    insert into kgc_interface_meta_data
    (ifmd_id
    ,kafd_id
    ,meta_type
    ,code
    ,waarde
    ,tech_id
    ) values
    (v_ifmd_id
    ,1
    ,r_ifmd.meta_type
    ,r_ifmd.code
    ,r_ifmd.waarde
    ,r_ifmd.tech_id
    );

  end loop;

 -- bas_standaard_fracties
  for r_stft in c_stft loop

    select bas_stft_seq.nextval into v_stft_id from dual ;

    -- kopieer de interface meta data
    insert into bas_stand_fractietypes
    (stft_id
    ,mate_id
    ,ongr_id
    ,frty_id
    ) values
    (v_stft_id
    ,r_stft.nieuw_mate_id
    ,r_stft.nieuw_ongr_id
    ,r_stft.nieuw_frty_id
    );

  end loop;


 -- kgc_declaratiewijzen
  for r_dewy_cyto in c_dewy_cyto loop

    open c_dewy_dna(b_code => r_dewy_cyto.code);
    fetch c_dewy_dna into v_dewy_id_dna;
    close c_dewy_dna;

    insert into beh_gen_dewy_oud_nieuw values (r_dewy_cyto.dewy_id_cyto, v_dewy_id_dna);

  end loop;


  open  c_max_deen;
  fetch c_max_deen into v_max_deen_volgorde;
  close c_max_deen;

 -- kgc_declaratie_entiteiten
  for r_deen in c_deen loop

    if  (r_deen.nieuw_ingr_id is null and r_deen.oud_ingr_id is not null)
     OR (r_deen.nieuw_ongr_id is null and r_deen.oud_ongr_id is not null)
     OR (r_deen.nieuw_dewy_id is null and r_deen.oud_dewy_id is not null)
    then

      null;

    else

      select kgc_deen_seq.nextval into v_deen_id from dual ;

      v_max_deen_volgorde := v_max_deen_volgorde + 1;

      insert into kgc_decl_entiteiten
      (deen_id
      ,ongr_id
      ,ingr_id
      ,dewy_id
      ,kafd_id
      ,volgorde
      ,entiteit
      ,declarabel
      ,declaratie_maken
      ,toelichting
      ,betreft_tekst
      ,verrichtingcode
      ,datum_wijziging_doorgevoerd
      ,datum_vervallen
      ,opmerkingen
      ) values
      (v_deen_id
      ,r_deen.nieuw_ongr_id
      ,r_deen.nieuw_ingr_id
      ,r_deen.nieuw_dewy_id
      ,1
      ,v_max_deen_volgorde
      ,r_deen.entiteit
      ,r_deen.declarabel
      ,r_deen.declaratie_maken
      ,r_deen.toelichting
      ,r_deen.betreft_tekst
      ,r_deen.verrichtingcode
      ,r_deen.datum_wijziging_doorgevoerd
      ,r_deen.datum_vervallen
      ,r_deen.opmerkingen||' '||'Codering: GenoomCONV, Notitie: '||Sysdate||': toegevoegd in verband met de conversie gegevens van CYTO naar GENOOM.'
      );


      for r_decr in c_decr(b_deen_id => r_deen.deen_id) loop

        select kgc_decr_seq.nextval into v_decr_id from dual ;

        insert into kgc_decl_criteria
        (deen_id
        ,tabelnaam
        ,kolomnaam
        ,waarde
        ) values
        (v_deen_id
        ,r_decr.tabelnaam
        ,r_decr.kolomnaam
        ,r_decr.waarde
        );

      end loop;


    end if;

  end loop;


 -- bas_standaardfracties
  for r_stan in c_stan loop

    update bas_standaardfracties
    set    standaardfractienummer = standaardfractienummer||'_CYTO'
    where  stan_id = r_stan.stan_id
    ;

    select bas_frac_seq.nextval into v_stan_id from dual ;

    -- kopieer de interface meta data
    insert into bas_standaardfracties
    (stan_id
    ,mate_id
    ,ongr_id
    ,mede_id
    ,standaardfractienummer
    ,datum_aanmaken
    ,commentaar
    ) values
    (v_stan_id
    ,r_stan.nieuw_mate_id
    ,r_stan.nieuw_ongr_id
    ,r_stan.mede_id
    ,r_stan.standaardfractienummer
    ,r_stan.datum_aanmaken
    ,r_stan.commentaar
    );

  end loop;

  update kgc_materialen
  set code = replace(code,'_')
  where kafd_id = 3
  and code like ('%_')
  and trunc(last_update_date) = trunc(sysdate)
  ;


EXCEPTION
  when others then
    rollback;
    raise;

END;

/

/
QUIT
