CREATE OR REPLACE PROCEDURE "HELIX"."BEH_ARRAY_INLEZEN"
is
-- zie mantisnr 1062 voor commentaar
v_statement varchar2(3000);
v_waarde varchar2(100);
v_onderzoeknr varchar2(100);

cursor c_beh_array_tmp
  is
    select trim(kolom1) interpretatie
    ,      trim(kolom2) onderzoeknr
    ,      trim(kolom3) HMM_LOH
    ,      trim(kolom4) snp_in_regio
    ,      trim(kolom5) grootte
    ,      trim(kolom6) s_chr
    ,      trim(kolom7) start_band
    ,      trim(kolom8) eind_band
    ,      trim(kolom9) start_pos
    ,      trim(kolom10) eind_pos
    ,      trim(kolom11) start_snp
    ,      trim(kolom12) eind_snp
    ,      trim(kolom13) fractienr
    ,      trim(kolom14) stoftest
    from   beh_array_tmp
 ;

cursor c_kgc_mwst_samenstelling
  is
    select trim(upper(mwss.prompt)) prompt
    from   kgc_meetwaardestructuur mwst, kgc_mwst_samenstelling mwss
 where  mwst.mwst_id = mwss.mwst_id
 and    upper(mwst.code) = upper('ARRAY_ST')
 order by mwss.volgorde
 ;

begin
  for r_kgc_mwst_samenstelling in c_kgc_mwst_samenstelling
  loop

 for r_beh_array_tmp in c_beh_array_tmp
 loop
 -- soms wordt onderzoeknr met een unscore + getal aan het eind aageboden bijv. PN07-1272_01. De underscore en wat daarachter staat (volgnr) wordt hier weggehaald.
 v_onderzoeknr := case
 when ((instr(r_beh_array_tmp.onderzoeknr,'_')-1) = -1) then r_beh_array_tmp.onderzoeknr
 else substr(r_beh_array_tmp.onderzoeknr, 1, instr(r_beh_array_tmp.onderzoeknr,'_')-1)
 end;

 v_waarde := case
    when (r_kgc_mwst_samenstelling.prompt = upper('Interpretatie')) then r_beh_array_tmp.interpretatie
    when (r_kgc_mwst_samenstelling.prompt = upper('HMM/LOH')) then r_beh_array_tmp.hmm_loh
    when (r_kgc_mwst_samenstelling.prompt = upper('SNP in regio')) then r_beh_array_tmp.snp_in_regio
    when (r_kgc_mwst_samenstelling.prompt = upper('Grootte')) then r_beh_array_tmp.grootte
    when (r_kgc_mwst_samenstelling.prompt = upper('Chr')) then r_beh_array_tmp.s_chr
    when (r_kgc_mwst_samenstelling.prompt = upper('Start band')) then r_beh_array_tmp.start_band
    when (r_kgc_mwst_samenstelling.prompt = upper('Eind band')) then r_beh_array_tmp.eind_band
    when (r_kgc_mwst_samenstelling.prompt = upper('Start pos')) then r_beh_array_tmp.start_pos
    when (r_kgc_mwst_samenstelling.prompt = upper('Eind pos')) then r_beh_array_tmp.eind_pos
    when (r_kgc_mwst_samenstelling.prompt = upper('Start SNP')) then r_beh_array_tmp.start_snp
    when (r_kgc_mwst_samenstelling.prompt = upper('Eind SNP')) then r_beh_array_tmp.eind_snp
    else '?'
    end;


  v_statement := 'update bas_meetwaarde_details mdet1
  set mdet1.waarde = ''' || trim(v_waarde) ||  '''
   where mdet1.mdet_id = (
        select mdet2.mdet_id
        from kgc_onderzoeken onde,
          kgc_onderzoek_monsters onmo,
          kgc_monsters mons,
          bas_fracties frac,
          bas_metingen meti,
          bas_meetwaarden meet,
          bas_meetwaarde_details mdet2,
          kgc_stoftesten stof
        where onde.onde_id = onmo.onde_id
        and   onmo.mons_id = mons.mons_id
        and   mons.mons_id = frac.mons_id
        and   frac.frac_id = meti.frac_id
        and   meti.meti_id = meet.meti_id
        and   onde.onde_id = meti.onde_id(+)
        and   meet.meet_id = mdet2.meet_id
        and   meet.stof_id = stof.stof_id
        and   onde.onderzoeknr = ''' || trim(v_onderzoeknr) || '''
        and   frac.fractienummer = ''' || r_beh_array_tmp.fractienr || '''
        and   meet.afgerond = ''N''
        and   meti.afgerond = ''N''
        and   upper(stof.code) = upper(''' || r_beh_array_tmp.stoftest || ''')
        and   upper(mdet2.prompt) = upper(''' || r_kgc_mwst_samenstelling.prompt || ''')
        )
        ';
  execute immediate v_statement;
  --Helix.Check_jobs.post_mail('cukz124@umcn.nl', 'h.bourchi@cukz.umcn.nl', 'array', v_statement);

 end loop;
   end loop;
  commit;
  exception
    when TOO_MANY_ROWS then
        rollback;
        dbms_output.put_line('Te veel data zijn gevonden! Controleer de ingevoerde parameters.');

    when NO_DATA_FOUND then
        rollback;
        dbms_output.put_line('Geen data zijn gevonden. Controleer de ingevoerde parameters.');

    when others then
        rollback;
        dbms_output.put_line('Er is een fout opgetreden!');
        dbms_output.put_line(substr('SQLErrm ' || sqlerrm, 1, 255));
end;

/

/
QUIT
