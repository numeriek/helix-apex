CREATE OR REPLACE PROCEDURE "HELIX"."BEH_FLOW_DNA_STAT_OHW_UPD" IS
/******************************************************************************
   NAME:       BEH_FLOW_DNA_STAT_OHW_UPD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        8-3-2012   Z268164       1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     BEH_FLOW_DNA_STAT_OHW
      Sysdate:         8-3-2012
      Date and Time:   8-3-2012, 19:40:45, and 8-3-2012 19:40:45
      Username:        Z268164 (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
insert into BEH_FLOW_DNA_STAT_OHW
select
       '#FFFFFF' as VERBORGEN_KLEUR,
       1 as VERBORGEN_SORTERING,
       ' ' as VERBORGEN_TELLING,
       ONGR.CODE as GROEP,
       SUBSTR(INGR.CODE, INSTR(INGR.CODE, '_')+1,3) as INDIGROEP,
       DECODE (ONDE.ONDERZOEKSTYPE,
                'R', 'Research',
                'D', 'Diagnostiek',
                '') as ONDE_TYPE,
       ONWY.OMSCHRIJVING as WIJZE,
       TO_CHAR(SYSDATE-3, 'YYYY-MM') as JAAR_MAAND,
       TO_CHAR(SYSDATE, 'IW') as WEEK,
       SUM
                (CASE WHEN WIDTH_BUCKET (ROUND(trunc(sysdate) - trunc(ONDE.DATUM_BINNEN)), 0, 31, 1) = 1
                            THEN 1
                            ELSE 0
                END) as "<=30 dagen",
       SUM
                (CASE WHEN WIDTH_BUCKET (ROUND(trunc(sysdate) - trunc(ONDE.DATUM_BINNEN)), 31, 91, 1) = 1
                            THEN 1
                            ELSE 0
                END) as ">30 & <=90 dagen",
       SUM
                (CASE WHEN WIDTH_BUCKET (ROUND(trunc(sysdate) - trunc(ONDE.DATUM_BINNEN)), 91, 366, 1) = 1
                            THEN 1
                            ELSE 0
                END) as ">90 & <=365 dagen",
       SUM
                (CASE WHEN WIDTH_BUCKET (ROUND(trunc(sysdate) - trunc(ONDE.DATUM_BINNEN)), 366, 100000, 1) = 1
                            THEN 1
                            ELSE 0
                END) as ">365 dagen"
FROM    kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_onderzoekswijzen onwy,
            kgc_kgc_afdelingen kafd,
            kgc_onderzoeksgroepen ongr,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr
WHERE     ONDE.KAFD_ID = 1
            AND KAFD.KAFD_ID = ONDE.KAFD_ID
            AND ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONGR.ONGR_ID = ONDE.ONGR_ID
            AND ONDE.ONDE_ID = ONIN.ONDE_ID
            AND ONIN.INDI_ID = INTE.INDI_ID
            AND INTE.INGR_ID = INGR.INGR_ID
            AND ONGR.CODE IN ('ARR', 'LTG', 'NGS', 'P', 'PR', 'RFD', 'TML')
            AND ONDE.AFGEROND = 'N'
GROUP BY
       ONGR.CODE,
       SUBSTR(INGR.CODE, INSTR(INGR.CODE, '_')+1,3),
       DECODE (ONDE.ONDERZOEKSTYPE,
                'R', 'Research',
                'D', 'Diagnostiek',
                ''),
       ONWY.OMSCHRIJVING
ORDER BY
        ONGR.CODE,
        SUBSTR(INGR.CODE, INSTR(INGR.CODE, '_')+1,3),
        DECODE (ONDE.ONDERZOEKSTYPE,
                'R', 'Research',
                'D', 'Diagnostiek',
                ''),
        ONWY.OMSCHRIJVING;
COMMIT;

END BEH_FLOW_DNA_STAT_OHW_UPD;
/

/
QUIT
