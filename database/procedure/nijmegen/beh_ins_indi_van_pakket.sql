CREATE OR REPLACE PROCEDURE "HELIX"."BEH_INS_INDI_VAN_PAKKET"
IS
/******************************************************************************
   NAME:       BEH_INS_INDI_VAN_PAKKET
   PURPOSE:  Aanmelden (via job) van indicaties bij onderzoek op basis van een pakket indicatie

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5-12-2012   M.Golebiowska       1. Created this procedure.

   NOTES:
    Gemaakt voor Martijn Schliekelmann nav van invoering van pakketten van indicaties.
    Per pakket wordt er vastgelegd in Attribuutwaarden (Code => Pakket), welke indicaties automatisch bij het onderzoek met deze specifiek pakket door Helix aangemeld moeten worden.
    Deze automatische aanmelding gebeurt met onderstaand procedure.
    Procedure is via Scheduled Jobs (job heet DNA_INS_INDI_VAN_PAKKET) gescheduled.

   Automatically available Auto Replace Keywords:
      Object Name:     BEH_INS_INDI_VAN_PAKKET
      Sysdate:         5-12-2012
      Date and Time:   5-12-2012, 17:11:09, and 5-12-2012 17:11:09
      Username:        M.Golebiowska

******************************************************************************/
   v_db_naam        VARCHAR2 (100) := SYS_CONTEXT ('USERENV', 'DB_NAME');
   v_job_naam       VARCHAR2 (100) := 'DNA_INS_INDI_VAN_PAKKET';
   v_peildatum       DATE := beh_alfu_00.jobs_last_date (v_job_naam);
   v_onin_id           NUMBER;

CURSOR onin_cur
-- in deze cursor worden onderzoeken met 'pakket' indicatie geselecteerd en de bijbehorende indicaties
IS
SELECT ONIN.ONDE_ID
        , ONIN.INDI_ID
        , ONDE.ONGR_ID
        , INTE2.INDI_ID as indi_id2
        , ONDE.ONDERZOEKNR
FROM kgc_onderzoek_indicaties onin
    , kgc_onderzoeken onde
    , kgc_indicatie_teksten inte
    , KGC_ATTRIBUUT_WAARDEN atwa
    , KGC_ATTRIBUTEN attr
    , kgc_indicatie_teksten inte2
where ONDE.ONDE_ID = ONIN.ONDE_ID
    and ONIN.INDI_ID = INTE.INDI_ID
    and INTE.INDI_ID = ATWA.ID
    and ATTR.ATTR_ID = ATWA.ATTR_ID
    and ATTR.CODE = 'PAKKET'
    and ONDE.AFGEROND = 'N'
    and NVL (ONDE.STATUS, 'onb') NOT IN ('G', 'V')
    and ONIN.CREATION_DATE >= v_peildatum - 10/(24*60*60)
    and INTE2.CODE IN (SELECT *
                                 FROM table(cast(beh_alfu_00.explode(',', UPPER( (SELECT ATWA.WAARDE
                                                                                                          FROM kgc_indicatie_teksten inte3
                                                                                                            , KGC_ATTRIBUUT_WAARDEN atwa3
                                                                                                            , KGC_ATTRIBUTEN attr3
                                                                                                          WHERE INTE3.INDI_ID = ATWA3.ID
                                                                                                            and ATTR3.ATTR_ID = ATWA3.ATTR_ID
                                                                                                            and ATTR3.CODE = 'PAKKET'
                                                                                                            and ATWA3.ID =ATWA.ID
                                                                                                         )
                                                                                                       )
                                                                                        ) as beh_listtable
                                                        )
                                                 )
                                 )
                 and INTE2.ONGR_ID =inte.ongr_id
                                  and onin.onde_id not in ( select ONIN2.ONDE_ID
                                                FROM kgc_onderzoek_indicaties onin2
                                                WHERE ONIN2.INDI_ID= INTE2.INDI_ID
                                                and ONIN2.ONDE_ID = onin.onde_id)
;
onin_rec onin_cur%ROWTYPE;

BEGIN

  OPEN onin_cur;
  FETCH onin_cur INTO onin_rec;
  CLOSE onin_cur;

  FOR onin_rec  IN  onin_cur
  LOOP
          -- eerst worden lege metingen verwijderd, anders worden door Helix in het vervolg stoftesten in deze lege meting aangemeld. Dit is niet de bedoeling, stoftesten dienen bij de bijbehorden protocol aangemeld worden.
          DELETE FROM bas_metingen
          WHERE METI_ID IN (SELECT METI.METI_ID
                                     FROM bas_metingen meti, kgc_onderzoeken onde
                                     WHERE ONDE.ONDE_ID = METI.ONDE_ID
                                         AND METI.STGR_ID IS NULL -- lege meting!
                                         AND METI.METI_ID NOT IN (SELECT MEET.METI_ID FROM bas_meetwaarden meet)
                                         AND ONDE.ONDE_ID = onin_rec.ONDE_ID);
           COMMIT;

  --dbms_output.put_line ('1ste loop ongr_id '|| onin_rec.ongr_id || ' onde_id '|| onin_rec.onde_id || ' onin_id '|| onin_rec.indi_id || ' onin_id '|| onin_rec.indi_id2 || ' ' || onin_rec.onderzoeknr);
           SELECT kgc_onin_seq.nextval
           INTO   v_onin_id
           FROM   dual;

           INSERT INTO kgc_onderzoek_indicaties ( onin_id, onde_id, indi_id)
           VALUES ( v_onin_id, onin_rec.ONDE_ID, onin_rec.indi_id2);

  END LOOP;
  COMMIT;

END BEH_INS_INDI_VAN_PAKKET;
/

/
QUIT
