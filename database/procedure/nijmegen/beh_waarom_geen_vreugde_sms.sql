CREATE OR REPLACE PROCEDURE "HELIX"."BEH_WAAROM_GEEN_VREUGDE_SMS"
-- overzicht van onderzoeken met de juiste indicaties en uitslagcodes die toch geen sms hebben gehad
is
  cursor c_onde is
  select *
  from beh_nog_geen_vreugde_sms_vw
  ;

  r_onde c_onde%rowtype;

  cursor c_zwan (v_foet_id in number) is
  select zwan.zwan_id
  , zwan.datum_aterm
  from KGC_ZWANGERSCHAPPEN zwan
  , kgc_foetussen foet
  where zwan.zwan_id = foet.zwan_id
  and foet.foet_id = v_foet_id;

  r_zwan c_zwan%rowtype;

  cursor c_pahi (v_onde_id in number, v_zwan_id  in number)is
  select onde.onderzoeknr
  from kgc_onderzoeken onde
  , KGC_FOETUSSEN foet
  , kgc_zwangerschappen zwan
  where onde.foet_id = foet.foet_id
  and foet.zwan_id = zwan.zwan_id
  and onde.kafd_id = 1
  and onde.ongr_id = 271
  and onde.onde_id <> v_onde_id
  --and onde.afgerond = 'N'
  and zwan.zwan_id = v_zwan_id
  ;
  r_pahi c_pahi%rowtype;



  v_vreugde varchar2 (2) := null;
  v_telefoon_ok varchar2 (2) := null;
  v_sms_verzenden varchar2 (2) := null;
  v_in_termijn varchar2 (200) := null;
  v_ok varchar2(20) := ' Dit is oke' ;
  v_nok varchar2(50) := ' Dit is  een reden om geen sms te versturen' ;

begin
  -- loop de onderzoeken een voor een langs
  open c_onde;
  fetch c_onde into r_onde;
  while c_onde%found
  loop
    DBMS_output.put_line('+++++++++++++++++++++++++++++++++++++++++++++++++++++++');
    DBMS_output.put_line('onderzoeknr: '||r_onde.onderzoeknr||', uitslagcode: '||r_onde.uitslagcode||', onderzoeksindicaties: '||r_onde.onderzoeksindicaties);
  --bepaal of ze aan de zes criteria voldoen
  --1 vreugde
  --2 sms_verzenden bij zwan toegestaan
  --3 onderzoek valt binnen de zwangerschapstermijn
    if r_onde.foet_id is not null then

      open c_zwan(r_onde.foet_id);
      fetch c_zwan into r_zwan;
      close c_zwan;
      v_vreugde :=  kgc_vreugdebrief_00.vreugde(r_zwan.zwan_id, null);
      v_sms_verzenden := kgc_attr_00.waarde('KGC_ZWANGERSCHAPPEN','SMS_VERZENDEN',r_zwan.zwan_id);
      if r_onde.datum_binnen between (r_zwan.datum_aterm - 280) and r_zwan.datum_aterm then
        v_in_termijn := 'Ja'||v_ok;
      else
        v_in_termijn := 'Nee, de binnenkomst datum van het onderzoek = '|| r_onde.datum_binnen||', de termijn van de zwangerschap is van '||(r_zwan.datum_aterm - 280)||' tot '||r_zwan.datum_aterm||v_nok;
      end if;
    DBMS_output.put_line('valt het onderzoek binnen de termijn van de zwangerschap? '||v_in_termijn);
    if v_vreugde = 'J' then
      DBMS_output.put_line('is er sprake van vreugde? '||v_vreugde||v_ok);
    else
      DBMS_output.put_line('is er sprake van vreugde? '||v_vreugde||v_nok);
    end if;
    DBMS_output.put_line('is er bij deze zwangerschap toestemming om de sms te verzenden? '||nvl(v_sms_verzenden, 'J'));
    end if;
  --4 telefoon
    if substr(r_onde.telefoon, 1, 2) = '06' then
      v_telefoon_ok := 'Ja';
    end if;
    DBMS_output.put_line('is het telefoonnummer een 06 nummer? : '||v_telefoon_ok);

  --5 andere nog niet afgeronde onderzoeken bij deze zwan
     open c_pahi(r_onde.onde_id, r_zwan.zwan_id);
     fetch c_pahi into r_pahi;
     if c_pahi%notfound then
       DBMS_output.put_line('Er zijn geen andere onderzoeken bij deze zwangerschap aanwezig');
     end if;
     while c_pahi%found
     loop
       DBMS_output.put_line('ander onderzoek bij deze zwangerschap,  onderzoeknr: '||r_pahi.onderzoeknr);
       fetch c_pahi into r_pahi;
     end loop;
     close c_pahi;

    DBMS_output.put_line('vreugde : '||v_vreugde);
    fetch c_onde into r_onde;
  end loop;
  close c_onde;
exception
  when others then
    null;
end;
/

/
QUIT
