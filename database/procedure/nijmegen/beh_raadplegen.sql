CREATE OR REPLACE PROCEDURE "HELIX"."BEH_RAADPLEGEN"
(      p_entiteit_id             IN NUMBER   := NULL
     , p_entiteit                IN varchar2 := NULL
     , p_datum                   IN date     := NULL
     , p_applicatie_vanuit       IN varchar2 := NULL
     , p_programma_waarmee       IN varchar2 := NULL
     , p_viewer_type             IN varchar2 := NULL
     , p_viewer_interne_id       IN NUMBER   := NULL
     , p_viewer_externe_id       IN varchar2 := NULL
     , p_viewer_extra_info       IN varchar2 := NULL
)
-- loggen van het bekijken/raadplegen van gegevens bv uitslagen via het epd
--
--
IS

v_datum date;

BEGIN

  select sysdate into v_datum from dual;

   INSERT INTO helix.beh_raadpleeg_log
     ( entiteit_id
     , entiteit
     , datum_bekeken
     , applicatie_vanuit
     , programma_waarmee
     , viewer_type
     , viewer_interne_id
     , viewer_externe_id
     , viewer_extra_info
     )
     values(
       p_entiteit_id
     , p_entiteit
     , v_datum
     , p_applicatie_vanuit
     , p_programma_waarmee
     , p_viewer_type
     , p_viewer_interne_id
     , p_viewer_externe_id
     , p_viewer_extra_info
     )
    ;
  COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    NULL;

END;

/

/
QUIT
