CREATE OR REPLACE PROCEDURE "HELIX"."BEH_MITO_DNA_INLEZEN" is

v_stof_code kgc_stoftesten.code%type;
v_stof_id kgc_stoftesten.stof_id%type;
v_stgr_code kgc_stoftestgroepen.code%type := 'D_MITODNA';
v_meti_id bas_metingen.meti_id%type;
v_mwst_code kgc_meetwaardestructuur.code%type := 'DDMITOCHIP';
v_mwst_id kgc_meetwaardestructuur.mwst_id%type;
v_mede_id kgc_medewerkers.mede_id%type;
r_bas_meetwaarde_details bas_meetwaarde_details%rowtype;
v_stof_prefix varchar2(5) := 'ZDM_';

v_referentie varchar2(100);
v_meetwaarde varchar2(100);
v_waarde varchar2(100);

cursor c_kgc_mwst_samenstelling is
    select trim(upper(mwss.prompt)) prompt, mwss.volgorde
    from   kgc_meetwaardestructuur mwst, kgc_mwst_samenstelling mwss
    where  mwst.mwst_id = mwss.mwst_id
    and    upper(mwst.code) = upper(v_mwst_code)
    order by mwss.volgorde
;

cursor c_beh_mito_dna_tmp is
    select trim(kolom1) onderzoeknr
    ,      trim(kolom2) monsternummer
    ,      trim(kolom3) fractienummer
    ,      trim(kolom4) protocol
    ,      trim(kolom5) input_string
    ,      trim(kolom6) categorie
    ,      trim(kolom7) interpretatie
    from   beh_mito_dna_tmp
    order by kolom1
 ;

cursor c_bas_meetwaarde_details (p_onderzoeknr varchar2, p_monsternummer varchar2, p_fractienummer varchar2, p_stgr_code varchar2, p_stof_code varchar2, p_prompt varchar2) is
    select mdet.*
    from kgc_onderzoeken onde
    ,   kgc_onderzoek_monsters onmo
    ,   kgc_monsters mons
    ,   bas_fracties frac
    ,   bas_metingen meti
    ,   bas_meetwaarden meet
    ,   bas_meetwaarde_details mdet
    ,   kgc_stoftesten stof
    ,   kgc_stoftestgroepen stgr
    where onde.onde_id = onmo.onde_id
    and   onmo.mons_id = mons.mons_id
    and   mons.mons_id = frac.mons_id
    and   frac.frac_id = meti.frac_id
    and   meti.meti_id = meet.meti_id
    and   meti.onde_id = onde.onde_id(+)
    and   meet.meet_id = mdet.meet_id
    and   meet.stof_id = stof.stof_id
    and   meti.stgr_id = stgr.stgr_id
    and   onde.onderzoeknr = p_onderzoeknr
    and   mons.monsternummer = p_monsternummer
    and   frac.fractienummer = p_fractienummer
    and   meti.afgerond = 'N'
    and   meet.afgerond = 'N'
    and   upper(stgr.code) = upper(p_stgr_code)
    and   upper(stof.code) = upper(p_stof_code)
    and   upper(mdet.prompt) = upper(p_prompt)
;

begin
    select mwst_id into v_mwst_id from kgc_meetwaardestructuur where upper(code) = upper(v_mwst_code);
    select mede_id into v_mede_id from kgc_medewerkers where upper(oracle_uid) = upper(user) and vervallen = 'N';

    -- loop begint met het ophalen van de kolommen van meetwaarde structuur zoals: Ref., meetw., categorie en interpretatie
    for r_kgc_mwst_samenstelling in c_kgc_mwst_samenstelling loop
    -- Vervolgens alle regels van de tussen tabel lezen
        for r_beh_mito_dna_tmp in c_beh_mito_dna_tmp loop
    -- kolom 5 van tussen tabel bevat stoftest code, referentie en meetwaarde. Deze halen we hier uit elkaar.
            v_stof_code  := trim(v_stof_prefix || substr(r_beh_mito_dna_tmp.input_string, instr(r_beh_mito_dna_tmp.input_string, ' ') + 1, 5));
            v_referentie := trim(substr(r_beh_mito_dna_tmp.input_string, instr(r_beh_mito_dna_tmp.input_string, ' ') + 6, 1));
            v_meetwaarde := trim(substr(r_beh_mito_dna_tmp.input_string, instr(r_beh_mito_dna_tmp.input_string, ' ') + 8));

  -- Afhanelijk van over welke kolom van meetwaarde structuur gaat, wordt de waarde bepaald.
            v_waarde :=
                case
                    when (r_kgc_mwst_samenstelling.prompt = upper('Ref.')) then v_referentie
                    when (r_kgc_mwst_samenstelling.prompt = upper('Meetw.')) then v_meetwaarde
                    when (r_kgc_mwst_samenstelling.prompt = upper('Categorie')) then r_beh_mito_dna_tmp.categorie
                    when (r_kgc_mwst_samenstelling.prompt = upper('Interpretatie')) then r_beh_mito_dna_tmp.interpretatie
                    else '?'
                end;

            open c_bas_meetwaarde_details
                (r_beh_mito_dna_tmp.onderzoeknr, r_beh_mito_dna_tmp.monsternummer, r_beh_mito_dna_tmp.fractienummer
                , v_stgr_code, v_stof_code, r_kgc_mwst_samenstelling.prompt);
            fetch c_bas_meetwaarde_details into r_bas_meetwaarde_details;
-- Eerst moet de stoftesten (bas_meetwaarden) ingevoerd worden
            if (c_bas_meetwaarde_details%notfound) then
                select meti_id into v_meti_id
                from kgc_onderzoeken onde
                ,   kgc_onderzoek_monsters onmo
                ,   kgc_monsters mons
                ,   bas_fracties frac
                ,   bas_metingen meti
                ,   kgc_stoftestgroepen stgr
                where onde.onde_id = onmo.onde_id
                and   onmo.mons_id = mons.mons_id
                and   mons.mons_id = frac.mons_id
                and   frac.frac_id = meti.frac_id
                and   meti.stgr_id = stgr.stgr_id
				and   meti.onde_id = onde.onde_id(+)
                and   onde.onderzoeknr = r_beh_mito_dna_tmp.onderzoeknr
                and   mons.monsternummer = r_beh_mito_dna_tmp.monsternummer
                and   frac.fractienummer = r_beh_mito_dna_tmp.fractienummer
                and   stgr.code = upper(v_stgr_code)
                and   meti.afgerond = 'N'
                ;

                select stof_id into v_stof_id from kgc_stoftesten where upper(code) = upper(v_stof_code);

                if (
                        v_meti_id is not null and
                        v_stof_id is not null and
                        v_mwst_id is not null and
                        r_kgc_mwst_samenstelling.volgorde = 1
                   ) then
                        insert into bas_meetwaarden (meti_id, stof_id, mwst_id, datum_meting)
                        values(v_meti_id, v_stof_id, v_mwst_id, sysdate);

                end if;
            end if;
-- Stoftesten (bas_meetwaarden) zijn nu ingevoerd (dus ook meetwaardestructuren --> bas_meetwaarde_details) we kunnen nu de ingelezen waardes
-- wegschijven in kolommen van meetwaarde details..
            update bas_meetwaarde_details mdet1
            set mdet1.waarde = trim(v_waarde)
            where mdet1.mdet_id = (
                    select mdet2.mdet_id
                    from kgc_onderzoeken onde
                    ,   kgc_onderzoek_monsters onmo
                    ,   kgc_monsters mons
                    ,   bas_fracties frac
                    ,   bas_metingen meti
                    ,   bas_meetwaarden meet
                    ,   bas_meetwaarde_details mdet2
                    ,   kgc_stoftesten stof
                    ,   kgc_stoftestgroepen stgr
                    where onde.onde_id = onmo.onde_id
                    and   onmo.mons_id = mons.mons_id
                    and   mons.mons_id = frac.mons_id
                    and   frac.frac_id = meti.frac_id
                    and   meti.meti_id = meet.meti_id
                    and   meti.onde_id = onde.onde_id(+)
                    and   meet.meet_id = mdet2.meet_id
                    and   meet.stof_id = stof.stof_id
                    and   meti.stgr_id = stgr.stgr_id
                    and   onde.onderzoeknr = r_beh_mito_dna_tmp.onderzoeknr
                    and   mons.monsternummer = r_beh_mito_dna_tmp.monsternummer
                    and   frac.fractienummer = r_beh_mito_dna_tmp.fractienummer
                    and   meti.afgerond = 'N'
                    and   meet.afgerond = 'N'
                    and   stgr.code = upper(v_stgr_code)
                    and   upper(stof.code) = upper(v_stof_code)
                    and   upper(mdet2.prompt) = upper(r_kgc_mwst_samenstelling.prompt)
                    )
                    ;

            close c_bas_meetwaarde_details;
--Helix.Check_jobs.post_mail('cukz124@umcn.nl', 'h.bourchi@cukz.umcn.nl', 'mito', v_waarde);
        end loop;
    end loop;

    -- De net ingevoerde stoftesten moeten nog afgerond worden en dit gebeurt als laatste stap
    for r_beh_mito_dna_tmp in c_beh_mito_dna_tmp loop

        v_stof_code := trim(v_stof_prefix || substr(r_beh_mito_dna_tmp.input_string, instr(r_beh_mito_dna_tmp.input_string, ' ') + 1, 5));

        update bas_meetwaarden meet
            set meet.afgerond = 'J'
            ,   meet.mede_id = v_mede_id
            ,   meet.mede_id_controle = v_mede_id
            ,   meet.datum_afgerond = sysdate
        where meet.meet_id = (
                select meet2.meet_id
                from kgc_onderzoeken onde
                ,   kgc_onderzoek_monsters onmo
                ,   kgc_monsters mons
                ,   bas_fracties frac
                ,   bas_metingen meti
                ,   bas_meetwaarden meet2
                ,   kgc_stoftesten stof
                ,   kgc_stoftestgroepen stgr
                where onde.onde_id = onmo.onde_id
                and   onmo.mons_id = mons.mons_id
                and   mons.mons_id = frac.mons_id
                and   frac.frac_id = meti.frac_id
                and   meti.meti_id = meet2.meti_id
                and   meti.onde_id = onde.onde_id(+)
                and   meet2.stof_id = stof.stof_id
                and   meti.stgr_id = stgr.stgr_id
                and   onde.onderzoeknr = r_beh_mito_dna_tmp.onderzoeknr
                and   mons.monsternummer = r_beh_mito_dna_tmp.monsternummer
                and   frac.fractienummer = r_beh_mito_dna_tmp.fractienummer
                and   meti.afgerond = 'N'
                and   meet2.afgerond = 'N'
                and   stgr.code = upper(v_stgr_code)
                and   upper(stof.code) = upper(v_stof_code)
                )
                ;
    end loop;
    commit;

exception
    when TOO_MANY_ROWS then
        rollback;
        dbms_output.put_line('Te veel data zijn gevonden! Controleer de ingevoerde waarde bij een van de variabelen.');

    when NO_DATA_FOUND then
        rollback;
        dbms_output.put_line('Geen data zijn gevonden. Controleer de ingevoerde waarde bij een van de variabelen.');

    when others then
        rollback;
        dbms_output.put_line('Er is een fout opgetreden!');
        dbms_output.put_line(substr('SQLErrm ' || sqlerrm, 1, 255));
end;

/

/
QUIT
