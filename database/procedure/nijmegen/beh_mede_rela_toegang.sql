CREATE OR REPLACE PROCEDURE "HELIX"."BEH_MEDE_RELA_TOEGANG" (
    p_oracle_uid kgc_medewerkers.oracle_uid%type ,
    p_scherm VARCHAR2 DEFAULT 'N' -- modules toegang
    ,
    p_delete VARCHAR2 DEFAULT 'N' -- mogen deleten in relatie tabellen
    ,
    p_insert VARCHAR2 DEFAULT 'N' -- mogen inserten in relatie tabellen
    ,
    p_update VARCHAR2 DEFAULT 'N' -- mogen updateden in relatie tabellen
  )
IS
  -- naar aanleiding van relatiebeheer project
  -- hiermee worden autorisiatie van een helix gebruiken
  -- uitbreid of beperkt voor relatie beheer tabellen en schermen.
  -- en deze zijn:
  -- tabellen: kgc_relaties, kgc_instellingen, kgc_afdelingen en kgc_specialismen
  -- schermen: KGCINST01, KGCRELA01, KGCAFDG01, KGCSPEC01
  v_oracle_uid kgc_medewerkers.oracle_uid%type := trim(upper(p_oracle_uid));
  v_mede_id kgc_medewerkers.mede_id%type       := kgc_mede_00.medewerker_id(v_oracle_uid);
  v_profile  VARCHAR2(50);
  v_scherm   VARCHAR2(1)    := upper(trim(p_scherm));
  v_delete   VARCHAR2(1)    := upper(trim(p_delete));
  v_insert   VARCHAR2(1)    := upper(trim(p_insert));
  v_update   VARCHAR2(1)    := upper(trim(p_update));
  v_modules  VARCHAR2(2500) := ('KGCINST01;KGCRELA01;KGCAFDG01;KGCSPEC01');
  v_tabellen VARCHAR2(2500) := ('KGC_RELATIES;KGC_INSTELLINGEN;KGC_AFDELINGEN;KGC_SPECIALISMEN');
  CURSOR c_list(p_str IN VARCHAR2)
  IS
    SELECT *
    FROM TABLE(CAST(beh_alfu_00.explode(';', upper(p_str)) AS beh_listtable)) ;
  CURSOR c_prof
  IS
    SELECT upper(profile) profile
    FROM sys.dba_users
    WHERE username = trim(upper(p_oracle_uid)) ;
  e_ongeldige_waarde           EXCEPTION;
  e_mede_niet_bestaat          EXCEPTION;
  e_mag_alleen_door_beheerders EXCEPTION;
BEGIN
  IF v_mede_id IS NULL THEN
    raise e_mede_niet_bestaat;
  END IF;
  IF ( v_scherm NOT IN ('J', 'N') OR v_delete NOT IN ('J', 'N') OR v_insert NOT IN ('J', 'N') OR v_update NOT IN ('J', 'N') ) THEN
    raise e_ongeldige_waarde;
  END IF;
  OPEN c_prof;
  FETCH c_prof INTO v_profile;
  CLOSE c_prof;
  -- helix gebruikers hebben default profile.
  IF v_profile != 'DEFAULT' THEN
    raise e_mag_alleen_door_beheerders;
  END IF;
  -- opschone oude data in kgc_module_toegang en kgc_tabel_toegang
  DELETE
  FROM kgc_module_toegang
  WHERE module IN
    (SELECT     *
    FROM TABLE(CAST(beh_alfu_00.explode(';', upper(v_modules)) AS beh_listtable))
    )
  AND mede_id = v_mede_id ;
  DELETE
  FROM kgc_tabel_toegang
  WHERE tabel IN
    (SELECT    *
    FROM TABLE(CAST(beh_alfu_00.explode(';', upper(v_tabellen)) AS beh_listtable))
    )
  AND mede_id = v_mede_id ;
  -- Invoeren nieuwe data in kgc_module_toegang en kgc_tabel_toegang
  IF v_scherm = 'N' THEN
    NULL; -- geen scherm en tabellen toegang nodig
  ELSE
    FOR r_moto IN c_list(v_modules)
    LOOP
      INSERT
      INTO kgc_module_toegang
        (
          module,
          andere_kafd,
          mede_id
        )
        VALUES
        (
          r_moto.column_value,
          'J',
          v_mede_id
        ) ;
    END LOOP;
    FOR r_tato IN c_list
    (
      v_tabellen
    )
    LOOP
      INSERT
      INTO kgc_tabel_toegang
        (
          tabel,
          ins,
          upd,
          del,
          mede_id
        )
        VALUES
        (
          r_tato.column_value,
          v_insert,
          v_update,
          v_delete,
          v_mede_id
        ) ;
    END LOOP;
  END IF;
  dbms_output.put_line('Module en tabel toegang van ' || v_oracle_uid || ' is succesvol bijgewerkt.');
  COMMIT;
EXCEPTION
WHEN e_ongeldige_waarde THEN
  ROLLBACK;
  dbms_output.put_line('De enige geldige waarden zijn J en N');
WHEN e_mede_niet_bestaat THEN
  ROLLBACK;
  dbms_output.put_line('Medewerker: ' || p_oracle_uid ||' bestaat niet in Helix database.');
WHEN e_mag_alleen_door_beheerders THEN
  ROLLBACK;
  dbms_output.put_line('De rechten van user: ' || p_oracle_uid ||' mag alleen door Helix beheerders aangepast worden.');
WHEN OTHERS THEN
  ROLLBACK;
  dbms_output.put_line(SUBSTR(sqlerrm, 1, 2000));
END beh_mede_rela_toegang;
/

/
QUIT
