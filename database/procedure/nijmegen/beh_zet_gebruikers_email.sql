CREATE OR REPLACE PROCEDURE "HELIX"."BEH_ZET_GEBRUIKERS_EMAIL" as
v_db_naam varchar2(50) := sys_context('userenv','db_name');
v_subject varchar2(500) := 'Verwerken gebruikers emails ('||v_db_naam||')';
v_this_prog varchar2(100) := 'beh_zet_gebruikers_email';
v_afzender varchar2(500);

paramlist beh_SOAP_PARAMLIST;
v_soap_service varchar2(100) := 'ad';
v_handshake kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_UITS_RAPPORT_USER'));
v_return varchar2(32767);
v_html_msg varchar2(32767);
v_error_txt varchar2(4000);
begin
select kgc_sypa_00.systeem_waarde('BEH_MIJN_HELIX_EMAIL_ADRES', null, null, (select mede_id from kgc_medewerkers where code = 'HELIX'))
into v_afzender
FROM dual
;
--initialisatie van TYPE
paramlist := beh_SOAP_PARAMLIST(beh_SOAP_PARAMETER('',''));

-- toevoegen van soap parameters (naam en waarde)
beh_soap_00.add_parameter(paramlist, 'dbName', v_db_naam);
beh_soap_00.add_parameter(paramlist, 'handshake', v_handshake);

--aanroep soapservice
v_return := beh_soap_00.call_soap_service(v_soap_service, paramlist);

v_return := replace(replace(v_return, '&lt;', '<'), '&gt;', '>');

v_html_msg := v_return || '<br><br>' ||
    'Deze email is automatisch gegenereerd en verzonden door procedure: ' || v_this_prog;

send_mail_html
    (
        p_from => v_afzender,
        p_to   => v_afzender,
        p_subject => v_subject,
        p_html => v_html_msg
        );

exception
    when others then
    v_error_txt := v_this_prog || ' => ' || ':' || SUBSTR(SQLERRM, 1, 2000);
    dbms_output.put_Line(v_error_txt);

end beh_zet_gebruikers_email;
/

/
QUIT
