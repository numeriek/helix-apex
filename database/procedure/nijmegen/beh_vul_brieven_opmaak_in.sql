CREATE OR REPLACE PROCEDURE "HELIX"."BEH_VUL_BRIEVEN_OPMAAK_IN"
(  p_rapp_id_goede IN NUMBER := NULL
,  p_rapp_id_slechte IN NUMBER := NULL
)
-- p_rapp_id_goede = copieren van
-- p_rapp_id_slechte = copieren naar
-- dus exec vul_brieven_in(18,78) copieert kgc_rapport_teksten met rapp_id 18, naar kgc_rapport_teksten met rapp_id 78
IS
v_rapp_id number;
v_code VARCHAR2(10);
v_volgorde number;
v_tekst VARCHAR2(2000);

CURSOR c_kgc_rapport_teksten IS
SELECT p_rapp_id_slechte rapp_id, code, volgorde, tekst
FROM   kgc_rapport_teksten
where rapp_id = p_rapp_id_goede;

BEGIN
 OPEN c_kgc_rapport_teksten;
   LOOP
   FETCH c_kgc_rapport_teksten INTO v_rapp_id, v_code, v_volgorde, v_tekst;
   EXIT WHEN c_kgc_rapport_teksten%NOTFOUND;
   INSERT INTO helix.kgc_rapport_teksten (rapp_id, code, volgorde, tekst) VALUES (v_rapp_id, v_code, v_volgorde, v_tekst);
  END LOOP;
  CLOSE c_kgc_rapport_teksten;
COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    NULL;

END;


/

/
QUIT
