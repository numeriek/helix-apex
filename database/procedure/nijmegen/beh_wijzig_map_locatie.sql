create or replace procedure "HELIX"."BEH_WIJZIG_MAP_LOCATIE"
is
  -- Pas alle standaard_pad entries van bestandstypes aan zodat het juiste jaar/kwartaal hierin verwerkt wordt.
  -- Tevens wordt de bijbehorende map aangemaakt, indien nog niet bestaand.
  -- Dit wordt gedaan voor alle paden eindigend op een jaar met de vorm: ^\\.*\<YYYY>\\q[1234]\\$
  --                of alle paden eindigend op een kwartaal met de vorm: ^\\.*\<YYYY>\\$
  --
  -- Datum      Wie V  Omschrijving
  -- ---------- --- -- ------------------------------------------------------
  -- 29-03-2016 RDE V1 Eerste versie tbv Mantis 11083
  -- 31-07-2017 RB     Alle kgc_bestand_types met een kwartaal of jaar erin
  --                   meenemen.
  --
  cursor c_btyp is
    with btyp_old_to_new as
    ( -- De standaard paden van de bestandstypes waarbij het pad eindigt op een jaar en een kwartaal.
      select code  
           , btyp_id
           , standaard_pad
           , REGEXP_REPLACE(translate(lower(standaard_pad), '/' , '\'), '(\\\\.+\\)([0123456789]{4}\\)(q[1234]\\)?', '\1' || to_char(sysdate, 'YYYY') || '\q' || to_char(sysdate, 'Q') || '\') as nieuw_standaard_pad
      from   kgc_bestand_types
      where  REGEXP_LIKE(translate(lower(standaard_pad), '/' , '\'), '^\\\\.*\\20[012][0123456789]\\q[1234]\\$')
      --
      union
      -- De standaard paden van de bestandstypes waarbij het pad eindigt op alleen een jaar.
      select code  
           , btyp_id
           , standaard_pad
           , REGEXP_REPLACE(translate(lower(standaard_pad), '/' , '\'), '(\\\\.+\\)([0123456789]{4}\\)(q[1234]\\)?', '\1' || to_char(sysdate, 'YYYY') || '\') as nieuw_standaard_pad
      from   kgc_bestand_types btyp
      where  REGEXP_LIKE(translate(lower(standaard_pad), '/' , '\'), '^\\\\.*\\20[012][0123456789]\\$')
    )
    select *
    from   btyp_old_to_new
    where  lower(standaard_pad) <> lower(nieuw_standaard_pad);

      
  v_error   boolean := false;
  v_this    varchar2(100) := 'beh_wijzig_map_locatie';
  v_verslag clob;
  v_sep     varchar2(10);
  
  v_dbname VARCHAR2(100)     := SYS_CONTEXT('userenv','db_name');  
  v_dbshort VARCHAR2(100)    := SUBSTR(v_dbname, 1, 4);  
  v_afzender VARCHAR2(100)   := 'helpdeskhelixcukz@cukz.umcn.nl';  
  v_ontvanger VARCHAR2(100)  := 'helpdeskhelixcukz@cukz.umcn.nl';  
  v_count_upd NUMBER := 0;

  procedure put_line(p_tekst varchar2) is
  begin
    v_verslag := v_verslag || v_sep || p_tekst;
    if p_tekst is not null then
      v_sep := chr(10) ;
    end if;
  end;
   
begin
  put_line('Start ' || v_this || ' (' || to_char(SYSDATE, 'dd-mm-yyyy hh24:mi:ss') || ')');
  
  for r_btyp in c_btyp loop
    
    v_count_upd := v_count_upd + 1;
    put_line('Update ' || r_btyp.code || ': ' || r_btyp.standaard_pad || ' -> ' || r_btyp.nieuw_standaard_pad);

    -- Maak het pad aan
    if not beh_file_00.create_file_directory(p_path => r_btyp.nieuw_standaard_pad) then
      v_error := true;
      put_line('ERROR: ' || r_btyp.code || ', map ' || r_btyp.nieuw_standaard_pad || ' kon niet gemaakt worden!') ;
    else
      update kgc_bestand_types
      set    standaard_pad = r_btyp.nieuw_standaard_pad
      where  btyp_id = r_btyp.btyp_id;
      commit; -- meteen vastleggen!
    end if;

  end loop;

  put_line('Einde ' || v_this || ' (' || TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss') || '; #updated=' || v_count_upd || ')');
   
  if v_error then
    -- Verstuur ERROR mail.
    utl_mail.send( sender => v_afzender
                 , recipients => v_ontvanger
                 , subject => 'ERROR in ' || v_this || ', db=' || v_dbname
                 , message => v_verslag
                 );
  else
    -- Verstuur SUCCESS mail.
    utl_mail.send( sender => v_afzender
                 , recipients => v_ontvanger
                 , subject => 'OK - Verslag vsn ' || v_this || ', db=' || v_dbname
                 , message => v_verslag
                 );
  end if;

exception when others then
  -- Verstuur EXCEPTION mail.
  utl_mail.send( sender => v_afzender
               , recipients => v_ontvanger
               , subject => 'SYSTEEM-ERROR in ' || v_this || ', db=' || v_dbname
               , message => 'SQLERRM=' || sqlerrm || chr(10) || v_verslag
               ) ;

end beh_wijzig_map_locatie;
/
