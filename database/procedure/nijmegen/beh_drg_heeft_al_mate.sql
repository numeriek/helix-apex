CREATE OR REPLACE PROCEDURE "HELIX"."BEH_DRG_HEEFT_AL_MATE"
(p_imfi_id varchar2)
IS
/******************************************************************************
   NAME:       BEH_DRG_HEEFT_AL_MATE
   PURPOSE:
   Deze procedure wordt wordt gebruikt om emails te versturen naar de DNA monster ontvangst in het geval dat Dragon Aanvraag "Materiaal Reeds Aanwezig"

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        17-5-2011   z268164       1. Created this procedure.
   1.1        23-04-2015 Cootje Vermaat   select voor overig materiaal stond een getal bij imfi_id, veranderd in p_imfi_id

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     BEH_DRG_HEEFT_AL_MATE
      Sysdate:         17-5-2011
      Date and Time:   17-5-2011, 16:00:10, and 17-5-2011 16:00:10
      Username:        z268164 (set in TOAD Options, Procedure Editor)
      Table Name:      KGC_IM_MONSTERS

******************************************************************************/
v_sender varchar2(500) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
v_cc varchar2(500) := 'XHLXAUT@umcn.nl, marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
v_bcc varchar2(500) :=''; -- komma gescheiden
v_recipients varchar2(500);
v_subject varchar2(1000);
v_message varchar2(4000);
v_newline varchar2(10) := chr(10);
v_filenaam kgc_import_files.filenaam%type;
v_db_name varchar2(100);
v_meer_mate varchar2(400);

BEGIN
SELECT filenaam INTO v_filenaam
FROM kgc_import_files
WHERE imfi_id = p_imfi_id;

SELECT global_name INTO v_db_name
FROM GLOBAL_NAME;

v_filenaam := substr(v_filenaam, 0, instr(v_filenaam, '(')-1);

SELECT * INTO v_meer_mate
FROM
       (SELECT 'meer'
        FROM  kgc_im_monsters immo
        WHERE  IMMO.IMFI_ID = P_IMFI_ID
             AND IMMO.IMFI_ID IN (SELECT IMMO1.IMFI_ID
                                              FROM kgc_im_monsters immo1
                                              WHERE IMMO1.MONSTERNUMMER = ( '*KOPPEL*')
                                                    AND IMMO1.IMFI_ID IN (SELECT  IMMO2.IMFI_ID
                                                                                      FROM kgc_im_monsters immo2
                                                                                      HAVING COUNT(IMFI_ID) > 1
                                                                                      GROUP BY  IMMO2.IMFI_ID
                                                                                      )
                                             )
        UNION ALL
        SELECT 'een'
        FROM  kgc_im_monsters immo
        WHERE  IMMO.IMFI_ID = P_IMFI_ID
              AND IMMO.IMFI_ID IN (SELECT IMMO1.IMFI_ID
                                               FROM kgc_im_monsters immo1
                                               WHERE IMMO1.MONSTERNUMMER = ( '*KOPPEL*')
                                                    AND IMMO1.IMFI_ID IN (SELECT IMMO2.IMFI_ID
                                                                                       FROM kgc_im_monsters immo2
                                                                                       HAVING COUNT(IMFI_ID) = 1
                                                                                       GROUP BY  IMMO2.IMFI_ID)
                                               )
        UNION ALL
        SELECT  'DNA'
        FROM  kgc_im_monsters immo
                , kgc_import_param_waarden impw
                , kgc_im_personen impe
        WHERE IMMO.IMFI_ID = P_IMFI_ID
            AND IMMO.IMPW_ID_MATE = IMPW.IMPW_ID
            AND IMMO.IMFI_ID = IMPE.IMFI_ID
            AND IMPW.EXTERNE_ID = 'DNA'
            AND (TRIM (LEADING '0' FROM IMPE.ZISNR), 1) IN (SELECT TRIM(LEADING '0' FROM pers.zisnr), ( SELECT COUNT(1)
                                                                            FROM kgc_monsters mons, bas_fracties frac, kgc_materialen mate, kgc_kgc_afdelingen kafd, bas_fractie_types frty, kgc_onderzoeksgroepen ongr
                                                                            WHERE PERS.PERS_ID = MONS.PERS_ID
                                                                                AND   MONS.MONS_ID = FRAC.MONS_ID
                                                                                AND   FRAC.FRTY_ID = FRTY.FRTY_ID
                                                                                AND   MONS.MATE_ID = MATE.MATE_ID
                                                                                AND   MATE.KAFD_ID = KAFD.KAFD_ID
                                                                                AND   MONS.ONGR_ID = ONGR.ONGR_ID
                                                                                --AND   UPPER(KAFD.CODE) = UPPER('GENOOM')
                                                                                AND kafd.code IN ('GENOOM', 'METAB')
                                                                                --AND   UPPER(ONGR.CODE) NOT IN ('POST_NIJM', 'POST_ENS', 'PRE_NIJM', 'TUM_ENS', 'TUM_NIJM')
                                                                                AND   UPPER(FRTY.CODE) = UPPER('DNA')
                                                                                AND   UPPER(FRAC.STATUS) IN ('A', 'O') -- aangemeld en opgewerkt
                                                                                --AND UPPER (ONGR.CODE) NOT IN ('AGORA', 'MOLGEN', 'MULTIFACT')
                                                                                AND MATE.CODE IN ('EDTA', 'DNA', 'BL')
                                                                         ) has_material
                                              from kgc_personen pers)
        UNION ALL
        SELECT  'DNA-NA' -- DNA gekozen, we hebben zelf geen DNA. Komt DNA nog?
        FROM  kgc_im_monsters immo
                , kgc_import_param_waarden impw
                , kgc_im_personen impe
        WHERE IMMO.IMFI_ID = P_IMFI_ID
            AND IMMO.IMPW_ID_MATE = IMPW.IMPW_ID
            AND IMMO.IMFI_ID = IMPE.IMFI_ID
            AND IMPW.EXTERNE_ID = 'DNA'
        UNION ALL
        SELECT  'OVERIG' -- Overig materiaal gekozen
        FROM  kgc_im_monsters immo
                , kgc_import_param_waarden impw
                , kgc_im_personen impe
        WHERE IMMO.IMFI_ID = P_IMFI_ID  -- aangepast 23-04-2015 CV
            AND IMMO.IMPW_ID_MATE = IMPW.IMPW_ID
            AND IMMO.IMFI_ID = IMPE.IMFI_ID
            AND IMPW.EXTERNE_ID = 'OV_DRAGON'
    )
WHERE ROWNUM = 1;

IF v_meer_mate ='meer' AND v_db_name NOT LIKE '%PROD%'
THEN v_subject := 'NIET UITVOEREN! DIT IS EEN TEST <materiaal reeds aanwezig> - nr: ' || v_filenaam ||' - MEERDERE MATERIALEN bij deze aanvraag!';
         v_recipients := 'XHLXAUT@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
ELSE
    IF v_meer_mate ='een' AND v_db_name NOT LIKE '%PROD%'
    THEN v_subject := 'NIET UITVOEREN! DIT IS EEN TEST <materiaal reeds aanwezig> - nr: ' || v_filenaam;
             v_recipients := 'XHLXAUT@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
    ELSE
        IF v_meer_mate LIKE 'DNA%'  AND v_db_name NOT LIKE '%PROD%'
        THEN v_subject := 'NIET UITVOEREN! DIT IS EEN TEST materiaal DNA gekozen - nr: ' || v_filenaam;
                 v_recipients := 'XHLXAUT@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
        ELSE
            IF v_meer_mate = 'OVERIG'  AND v_db_name NOT LIKE '%PROD%'
            THEN v_subject := 'NIET UITVOEREN! DIT IS EEN TEST materiaal OVERIG gekozen - nr: ' || v_filenaam;
                     v_recipients := 'XHLXAUT@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
            ELSE
                IF v_meer_mate ='meer' AND v_db_name LIKE '%PROD%'
                THEN v_subject := 'Nieuwe Dragon aanvraag <materiaal reeds aanwezig> - nr: ' || v_filenaam ||' - MEERDERE MATERIALEN bij deze aanvraag!';
                         v_recipients := 'dna@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
                ELSE
                    IF v_meer_mate ='een' AND v_db_name LIKE '%PROD%'
                    THEN v_subject := 'Nieuwe Dragon aanvraag <materiaal reeds aanwezig> - nr: ' || v_filenaam;
                             v_recipients := 'dna@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
                    ELSE
                        IF v_meer_mate LIKE 'DNA%' AND v_db_name LIKE '%PROD%'
                        THEN v_subject := 'Nieuwe Dragon aanvraag materiaal DNA gekozen - nr: ' || v_filenaam;
                                 v_recipients := 'dna@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
                        ELSE
                            IF v_meer_mate LIKE 'OVERIG' AND v_db_name LIKE '%PROD%'
                            THEN v_subject := 'Nieuwe Dragon aanvraag materiaal OVERIG gekozen - nr: ' || v_filenaam;
                                    v_recipients := 'dna@umcn.nl,marzena.golebiowska@radboudumc.nl'; -- komma gescheiden
                            END IF;
                        END IF;
                    END IF;
                END IF;
            END IF;
        END IF;
    END IF;
END IF;

IF v_meer_mate ='meer'
THEN
v_message := 'Beste Postbus DNA Diagnostiek,'
    || v_newline
    || v_newline
    || 'Nieuwe Dragon aanvraag <materiaal reeds aanwezig> - nr: ' || v_filenaam
    || v_newline
    || v_newline
    || 'Er is een nieuwe Dragon aanvraag <materiaal reeds aanwezig> in Helix binnengekomen.'
    || v_newline
    || 'De aanvraag nummer is: ' || v_filenaam
    || v_newline
    || v_newline
    || 'Bij deze aanvraag zijn MEERDERE MATERIALEN door de aanvrager opgegeven!'
    || v_newline
    || 'Controleer of deze materialen daadwerkelijk gestuurd worden. Indien nodig verwijder overbodige monsters via Ingelezen Gegevens scherm, tabblad Monsters!'
    || v_newline
    || v_newline
    || v_newline
    || 'Verwerk deze aanvraag zo spoedig mogelijk in het geval dat deze door Helix zelf nog niet verwerkt is. Anders controleer of de aanvraag correct en volledig verwerkt is.'
    || v_newline
    || 'Print alle benodigde documenten voor de status uit.'
    || v_newline
    || v_newline
    || v_newline
    || v_newline
    || 'met vriendelijke groeten,'
    || v_newline
    || 'Serviceteam Genetica';
ELSE
    IF v_meer_mate ='een'
    THEN
    v_message := 'Beste Postbus DNA Diagnostiek,'
        || v_newline
        || v_newline
        || 'Nieuwe Dragon aanvraag <materiaal reeds aanwezig> - nr: ' || v_filenaam
        || v_newline
        || v_newline
        || 'Er is een nieuwe Dragon aanvraag <materiaal reeds aanwezig> in Helix binnengekomen.'
        || v_newline
        || 'De aanvraag nummer is: ' || v_filenaam
        || v_newline
        || v_newline
        || 'Verwerk deze aanvraag zo spoedig mogelijk in het geval dat deze door Helix zelf nog niet verwerkt is. Anders controleer of de aanvraag correct en volledig verwerkt is.'
        || v_newline
        || 'Print alle benodigde documenten voor de status uit.'
        || v_newline
        || v_newline
        || v_newline
        || v_newline
        || 'met vriendelijke groeten,'
        || v_newline
        || 'Serviceteam Genetica';
        ELSE
            IF v_meer_mate ='DNA'
            THEN
            v_message := 'Beste Postbus DNA Diagnostiek,'
                || v_newline
                || v_newline
                || 'Nieuwe Dragon aanvraag materiaal DNA gekozen - nr: ' || v_filenaam
                || v_newline
                || v_newline
                || 'Er is een nieuwe Dragon aanvraag in Helix binnengekomen, waarbij aanvrager DNA materiaal gekozen heeft.'
                || v_newline
                || 'De aanvraag nummer is: ' || v_filenaam
                || v_newline
                || v_newline
                || 'Deze persoon heeft al in Helix een DNA fractie die aangemeld of opgewerkt is. Controleer of je deze DNA fractie voor het aangevraagde onderzoek niet zou kunnen gebruiken. Indien dit het wel geval is, verwerk deze aanvraag zo spoedig mogelijk. Het wachten op het binnenkomen van materiaal is dan niet meer nodig.'
                || v_newline
                || 'Print alle benodigde documenten voor de status uit.'
                || v_newline
                || v_newline
                || v_newline
                || v_newline
                || 'met vriendelijke groeten,'
                || v_newline
                || 'Serviceteam Genetica';
            ELSE
                IF v_meer_mate ='DNA-NA'
                THEN
                v_message := 'Beste Postbus DNA Diagnostiek,'
                    || v_newline
                    || v_newline
                    || 'Nieuwe Dragon aanvraag materiaal DNA gekozen - nr: ' || v_filenaam
                    || v_newline
                    || v_newline
                    || 'Er is een nieuwe Dragon aanvraag in Helix binnengekomen, waarbij aanvrager DNA materiaal gekozen heeft.'
                    || v_newline
                    || 'De aanvraag nummer is: ' || v_filenaam
                    || v_newline
                    || v_newline
                    || 'Deze persoon heeft in Helix WAARSCHIJNLIJK GEEN DNA fractie die aangemeld of opgewerkt is. Controleer of je deze aanvraag correct is.'
                    || v_newline
                    || 'Print alle benodigde documenten voor de status uit.'
                    || v_newline
                    || v_newline
                    || v_newline
                    || v_newline
                    || 'met vriendelijke groeten,'
                    || v_newline
                    || 'Serviceteam Genetica';
                ELSE
                    IF v_meer_mate ='OVERIG'
                    THEN
                    v_message := 'Beste Postbus DNA Diagnostiek,'
                        || v_newline
                        || v_newline
                        || 'Nieuwe Dragon aanvraag materiaal OVERIG gekozen - nr: ' || v_filenaam
                        || v_newline
                        || v_newline
                        || 'Er is een nieuwe Dragon aanvraag in Helix binnengekomen, waarbij aanvrager OVERIG materiaal gekozen heeft.'
                        || v_newline
                        || 'De aanvraag nummer is: ' || v_filenaam
                        || v_newline
                        || v_newline
                        || 'Controleer wanneer en hoe deze aanvraag verwerkt kan worden.'
                        || v_newline
                        || 'Print alle benodigde documenten voor de status uit.'
                        || v_newline
                        || v_newline
                        || v_newline
                        || v_newline
                        || 'met vriendelijke groeten,'
                        || v_newline
                        || 'Serviceteam Genetica';
                    END IF;
                END IF;
            END IF;
    END IF;
END IF;

UTL_MAIL.SEND(  sender => v_sender,
                recipients => v_recipients,
                cc => v_cc,
                bcc => v_bcc,
                subject => v_subject,
                message => v_message);
END;
/

/
QUIT
