set define off;

  CREATE OR REPLACE PROCEDURE "HELIX"."BEH_PREPARE_RELA_ID_OORSP" as

cursor c_onde is
select onde.*,
kgc_attr_00.waarde('KGC_RELATIES', 'DFT_SUPERVISOR', onde.rela_id) supervisor
from kgc_onderzoeken onde
where exists
(select null
from beh_decl_check_vw decl
where onde.onde_id = decl.onde_id
and   decl.rela_check = 'ONBEVOEGD'
and   decl.pers_check = 'OK'
and   decl.inst_check = 'OK'
and   decl.decl_check = 'OK'
and   decl.dewy_code = 'BI'
and   decl.decl_type = 'INTERN'
and   decl.aanvrager_type = 'PERSOON'
and   decl.spec_code in ('AA', 'GCINT', 'GCEXT', 'GENWP', 'VER', 'BAKG')
and   decl.kafd_code in ('GENOOM', 'DNA-P')
and   decl.onde_afgerond = 'J'
)
and kgc_attr_00.waarde('KGC_RELATIES', 'DFT_SUPERVISOR', onde.rela_id) is not null
and onde.rela_id_oorsprong is null
;

cursor c_rela (p_liszcode varchar2) is
select *
from kgc_relaties
where upper(liszcode) = upper(p_liszcode)
--and vervallen = 'N'
;
r_rela c_rela%rowtype;

begin
for r_onde in c_onde loop
	
	open c_rela(r_onde.supervisor);
	fetch c_rela into r_rela;
	if c_rela%found then
		insert into beh_oorspr_aanvragers
		(onderzoeknr, rela_code)
		values(r_onde.onderzoeknr, r_rela.code)
		;
	end if;
	close c_rela;
	
end loop;
commit;
exception
	when others then
	rollback;
	raise;
end;

/
