CREATE OR REPLACE PROCEDURE "HELIX"."BEH_CHECK_MEET_BIJ_FRAC"
IS
/******************************************************************************
   NAME:       BEH_CHECK_MEET_BIJ_FRAC
   PURPOSE:
   Deze procedure wordt wordt gebruikt om een overzicht te maken van afwijkende KREAT_UP/KREAT waarden bij 1 fractie.

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        02-07-2015   z173141         1. Created this procedure.

   NOTES: zie Mantis 10782

******************************************************************************/
   -- mantis 10782: kijk of er afwijkende meetwaarden zijn
   v_stof_code kgc_stoftesten.code%TYPE := 'KREAT';
   CURSOR c_frac IS
      SELECT DISTINCT frac.fractienummer
      ,      frac.frac_id
      ,      mons.monsternummer
      ,      stgr.stgr_id
      ,      stgr.code stgr_code
      FROM   bas_metingen meti
      ,      bas_fracties frac
      ,      kgc_monsters mons
      ,      kgc_stoftestgroepen stgr
      ,      kgc_kgc_afdelingen kafd
      WHERE  meti.stgr_id = stgr.stgr_id
      AND    stgr.code = 'KREAT_UP'
      AND    stgr.kafd_id = kafd.kafd_id
      AND    kafd.code = 'METAB'
      AND    meti.frac_id = frac.frac_id
      AND    frac.mons_id = mons.mons_id
--AND frac.fractienummer = '-94048'
      AND    meti.last_update_date > SYSDATE - 7
      ORDER BY frac.fractienummer DESC
      ;
   CURSOR c_meti(p_frac_id NUMBER, p_stgr_id NUMBER) IS
      SELECT meti.meti_id
      ,      meti.onde_id
      ,      meti.afgerond meti_afgerond
      ,      onde.onderzoeknr
      ,      onde.afgerond onde_afgerond
      FROM   bas_metingen meti
      ,      kgc_onderzoeken onde
      WHERE  meti.stgr_id = p_stgr_id
      AND    meti.frac_id = p_frac_id
      AND    meti.onde_id = onde.onde_id
      ORDER BY DECODE(meti.afgerond, 'J', 1, 2)
      ,      meti.meti_id
      ;
   r_meti_ref c_meti%ROWTYPE;
   CURSOR c_meet(p_meti_id NUMBER, p_stgr_code VARCHAR2) IS
      SELECT meet.meet_id
      ,      mdet1.waarde waarde1
      ,      mdet2.waarde waarde2
      ,      mdet3.waarde waarde3
      FROM   bas_meetwaarden meet
      ,      kgc_stoftesten stof
      ,      bas_meetwaarde_details mdet1
      ,      bas_meetwaarde_details mdet2
      ,      bas_meetwaarde_details mdet3
      WHERE  meet.meti_id = p_meti_id
      AND    meet.stof_id = stof.stof_id
      AND    (p_stgr_code = 'KREAT_UP' AND stof.code = 'KREAT') -- uitbereiding: OR (p_stgr_code = 'X' AND stof.code IN ('Y'))
      AND    meet.meet_id = mdet1.meet_id
      AND    mdet1.volgorde = 1
      AND    meet.meet_id = mdet2.meet_id
      AND    mdet2.volgorde = 2
      AND    meet.meet_id = mdet3.meet_id
      AND    mdet3.volgorde = 3
      ;
   r_meet_ref c_meet%ROWTYPE;
   r_meet c_meet%ROWTYPE;
   r_meet_null c_meet%ROWTYPE;

   v_found BOOLEAN;
   v_error_mwst BOOLEAN;
   v_error_diff BOOLEAN;
   v_count NUMBER := 0;
   v_count_error_mwst NUMBER := 0;
   v_count_error_diff NUMBER := 0;
   v_verslag VARCHAR2(32767);
   v_output CLOB;
   v_newline varchar2(10) := '<br>'; --chr(10) || chr(13);
   v_db_name varchar2(100);
   v_sender varchar2(500) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
   v_cc varchar2(500); -- komma gescheiden
   v_bcc varchar2(500) :=''; -- komma gescheiden
--   v_recipients varchar2(500) := 'Rob.Debets@radboudumc.nl';
   v_recipients varchar2(500) := 'Hanneke.Kwast@radboudumc.nl';
   v_subject varchar2(1000);
   PROCEDURE dbms_output_put_line(p_tekst VARCHAR2) IS
   BEGIN
      v_output := v_output || v_newline || p_tekst;
   END dbms_output_put_line;
BEGIN
   SELECT global_name INTO v_db_name
   FROM GLOBAL_NAME;
   dbms_output_put_line('Start');
   FOR r_frac IN c_frac LOOP -- alle fracties met genoemde stoftestgroepen
IF c_frac%ROWCOUNT > 1000 THEN EXIT; END IF;
      -- Zijn er andere metingen bij deze fractie?
      v_error_mwst := FALSE;
      v_error_diff := FALSE;
      v_verslag := NULL;
      v_count := v_count + 1;
      FOR r_meti IN c_meti(p_frac_id => r_frac.frac_id, p_stgr_id => r_frac.stgr_id) LOOP -- alle relevante metingen
         IF c_meti%ROWCOUNT = 1 THEN
            r_meti_ref := r_meti;
            r_meet_ref := r_meet_null;
            OPEN c_meet(p_meti_id => r_meti_ref.meti_id, p_stgr_code => r_frac.stgr_code);
            FETCH c_meet INTO r_meet_ref;
            IF c_meet%NOTFOUND THEN
               v_error_mwst := TRUE;
               v_count_error_mwst := v_count_error_mwst + 1;
               v_verslag := 'ERROR Fractie ' || r_frac.fractienummer || ' / monster ' || r_frac.monsternummer || ' zit in onderzoek ' || r_meti_ref.onderzoeknr || '; meetwaarden niet gevonden!';
            ELSE
               v_verslag := 'Fractie ' || r_frac.fractienummer || ' / monster ' || r_frac.monsternummer || ' zit in onderzoek ' || r_meti_ref.onderzoeknr || '; af=' || r_meti_ref.onde_afgerond  || '; meti-af=' || r_meti_ref.meti_afgerond || ' (meti_id=' || r_meti_ref.meti_id || '); waarde1/2/3=' || r_meet_ref.waarde1 ||  '/' || r_meet_ref.waarde2 || '/' || r_meet_ref.waarde3;
            END IF;
            CLOSE c_meet;
            IF v_error_mwst THEN
               EXIT;
            END IF;
         ELSE -- vergelijk met vorige meting; haal meetwaardedetails op
            r_meet := r_meet_null;
            OPEN c_meet(p_meti_id => r_meti.meti_id, p_stgr_code => r_frac.stgr_code);
            FETCH c_meet INTO r_meet;
            IF c_meet%NOTFOUND THEN
               v_error_mwst := TRUE;
               v_count_error_mwst := v_count_error_mwst + 1;
               v_verslag := 'ERROR ' || v_verslag || v_newline || '... bij onderzoek ' || r_meti.onderzoeknr || ' werden geen meetwaarden gevonden!';
            END IF;
            CLOSE c_meet;
            IF v_error_mwst THEN
               EXIT;
            END IF;
            v_verslag := v_verslag || v_newline || '... en in ' || r_meti.onderzoeknr || '; af=' || r_meti.onde_afgerond  || '; meti-af=' || r_meti.meti_afgerond || ' (meti_id=' || r_meti.meti_id || '); waarde1/2/3=' || r_meet.waarde1 ||  '/' || r_meet.waarde2 || '/' || r_meet.waarde3;
            IF    NVL(r_meet_ref.waarde1, CHR(2)) <> NVL(r_meet.waarde1, CHR(2))
               OR NVL(r_meet_ref.waarde2, CHR(2)) <> NVL(r_meet.waarde2, CHR(2))
               OR NVL(r_meet_ref.waarde3, CHR(2)) <> NVL(r_meet.waarde3, CHR(2))
               OR r_meti_ref.meti_afgerond <> r_meti.meti_afgerond
            THEN
               v_error_diff := TRUE;
               v_count_error_diff := v_count_error_diff + 1;
               v_verslag := v_verslag || ' <<< ERROR - afwijkende waarden of status (afgerond)!';
            END IF;
         END IF;
      END LOOP;
      IF v_error_mwst OR v_error_diff THEN
         dbms_output_put_line(v_verslag);
      END IF;
   END LOOP;
   dbms_output_put_line('Klaar; #frac=' || v_count || '; fouten in meetwaardenstructuur=' || v_count_error_mwst || '; verschillen in meetwaarden=' || v_count_error_diff);

   IF (v_count_error_mwst + v_count_error_diff) = 0 THEN
      v_subject := 'HELIX - OK: de controle op gelijke KREAT-waarden leverde geen verschillen';
      v_output := '<p>Geachte ontvanger,' || v_newline || v_newline
            || '<p>Bij controle op gelijke KREAT-waarden werden geen verschillen gevonden!'
            || v_newline
            || v_newline
            || 'met vriendelijke groet,'
            || v_newline
            || 'Helix';
   ELSE
      v_subject := 'HELIX - ERROR: de controle op gelijke KREAT-waarden leverde ' || v_count_error_mwst || '/' || v_count_error_diff || ' verschillen';
      v_output := '<p>Geachte ontvanger,' || v_newline || v_newline
            || '<p>Bij controle op gelijke KREAT-waarden werden verschillen gevonden! Deze zijn aan het eind van deze mail opgenomen.'
            || v_newline || v_newline
            || 'met vriendelijke groet,'
            || v_newline
            || 'Helix'
            || v_newline || v_newline
            || v_output
            ;
   END IF;
   IF v_db_name NOT LIKE '%PROD%' THEN
      v_subject := v_subject || ' #### LET OP: TEST (db=' || v_db_name || ')';
      v_output := '#### LET OP: dit is een TEST-mail, en deze is afkomstig uit Helix-database ' || v_db_name || '!' || CHR(10) || v_output;
   END IF;

   UTL_MAIL.SEND( sender => v_sender
                , recipients => v_recipients
                , cc => v_cc
                , bcc => v_bcc
                , subject => v_subject
                , message => v_output
                , mime_type => 'text/html; charset=us-ascii'
                );
END;
/

/
QUIT
