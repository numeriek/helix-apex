CREATE OR REPLACE PROCEDURE "HELIX"."BEH_KASP_INLEZEN"
is


v_aantal        number := null;
v_meet_id       number := null;
v_fractienummer varchar2(50):= null;
v_volgnummer    varchar2(4):= null;
v_marker_name   varchar2(50):= null;
v_waarde        varchar2(500):= null;
v_log           varchar2(2000):= null;
v_os_user       varchar2(50):= null;
v_session_user  varchar2(50):= null;
v_sysdate       varchar2(25):= null;
v_sender        varchar2(50) := trim(kgc_sypa_00.systeem_waarde('BEH_STGEN_EMAIL_FROM'));
v_recipient     varchar2(100):= 'Simone.vandenHeuvel@radboudumc.nl; Martine.vanZweeden@radboudumc.nl; Wendy.Buijsman@radboudumc.nl';
--v_recipient     varchar2(100):= 'Cootje.Vermaat@radboudumc.nl';
--v_recipient     varchar2(100):= 'Simone.vandenHeuvel@radboudumc.nl;Cootje.Vermaat@radboudumc.nl';
v_title         varchar2(100) := 'Inlezen KASP data';
v_message       varchar2(2000):= null;
v_cc            varchar2(50) := v_sender;
v_db_name       varchar2(20) := null;
v_wlst_name     varchar2(30) := null;

    cursor c_kasp
    is
    select trim(kain.K_well)                                               volgnummer
         , trim(kain.k_sample_name)                                        sample_name
         , substr(kain.k_sample_name, 1, (instr(k_sample_name, '_')- 1))   fractienummer
         , trim(substr(kain.k_sample_name, (instr(k_sample_name, '_')+1))) meet_id
         , replace(replace(trim(k_marker_name), 'PCR_', '') , ' (seq)', '')   marker_name
         , decode(substr(upper(kain.k_call), 1, 4), 'BOTH', trim(kain.k_call)
                                                  , 'PCR_', substr(kain.k_call,length(kain.k_call))
                                                  , null)  k_call
         , decode(substr(upper(kain.k_call), 1, 4), 'BOTH', beh_kasp_allel(replace(replace(trim(k_marker_name), 'PCR_', '') , ' (seq)', ''),'ALLEL_1')
                                                  , 'PCR_', substr(kain.k_call,length(kain.k_call)), null )allel_1
         , decode(substr(upper(kain.k_call), 1, 4), 'BOTH', beh_kasp_allel(replace(replace(trim(k_marker_name), 'PCR_', '') , ' (seq)', ''),'ALLEL_2')
                                                  , 'PCR_', substr(kain.k_call,length(kain.k_call)), null )allel_2
         , decode(substr(upper(kain.k_call), 1, 4), 'BOTH', trim(kain.k_quality_value)
                                                  , 'PCR_', trim(kain.k_quality_value)
                                                  , null )  quality_value
         , decode(substr(upper(kain.k_call), 1, 4), 'BOTH', trim(kain.k_call_type)
                                                  , 'PCR_', trim(kain.k_call_type)
                                                  , null ) k_call_type
         ,  beh_kasp_allel(replace(replace(trim(k_marker_name), 'PCR_', '') , ' (seq)', ''),'STRAND') Strand
    from BEH_KASP_INLEES kain
    where substr(k_sample_name, 1, 3) = 'DNA'
--    and trim(substr(kain.k_sample_name, (instr(k_sample_name, '_')+1))) in ( 14912770, 1491277, 114912772, 14912773, 14912774, 14912775, 14912776)
    ;

    r_kasp c_kasp%rowtype;

  cursor c_mdet (v_meet_id in number) is
  select mdet_id
  , volgorde
  , prompt
  , waarde
  from bas_meetwaarde_details
  where meet_id = v_meet_id
  and prompt in ('Allel_1', 'Allel_2', 'Call', 'Quality Value', 'Call Type', 'Strand')
  for update of waarde;

  r_mdet c_mdet%rowtype;

  -- van te voren te tesetn fouten
  e_geen_data_in_file   exception; -- als er geen waarde is in de kolom call
  e_geen_data_in_call   exception; -- als er geen fracties zijn in de hele in leestabel
  -- fouten die je onderweg tegenkomt
  e_reeds_data_aanwezig exception; -- als er al meetwaarde details aanweizg zijn
--  e_frac_meet_fout      exception; -- de fractie heeft deze meet_id niet

begin

  select k_sample_name into v_wlst_name from BEH_KASP_INLEES where upper(k_well) = 'FILENAME';
  v_cc  := null;
  -- context gegevens ophalen
  select sys_context('userenv', 'db_name') into v_db_name from dual;
  select sys_context('userenv', 'os_user') into v_os_user from dual;
  select sys_context('userenv', 'session_user') into v_session_user from dual;

  select to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss') into v_sysdate from dual;

  v_title := 'Omgeving: '||v_db_name||' '||v_title||' van '||v_wlst_name;

  -- bepaal of er wel gegevens in de inlees tabel zijn om in te lezen
  select count(*) into v_aantal
    from BEH_KASP_INLEES
    where substr(k_sample_name, 1, 3) = 'DNA';

  if v_aantal = 0 then
    raise e_geen_data_in_file;
  else

      for r_kasp in c_kasp loop

        v_fractienummer := r_kasp.fractienummer ;
        v_meet_id := r_kasp.meet_id;
        v_volgnummer := r_kasp.volgnummer;

        --als de kolom call geen waarde heeft is er een fout in de file,
        if nvl(r_kasp.k_call,'ONB') = 'ONB' then

          if v_log is null then
            v_log := 'Geen data in kolom call bij volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id||'.';
          else
            if length(v_log) < 1900 then
              v_log:= v_log||chr(13)||chr(10)||'Geen data in kolom call bij volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id||'.';
            end if;
          end if;

        else

          --als bij deze fractie deze meet_id niet gevonden wordt dan is er een fout
          select count(*) into v_aantal --from bas_meetwaarden_details where meet_id = v_meet_id and prompt in ('Allel_1', 'Allel_2', 'Call', 'Quality Value', 'Call Type');
                from bas_fracties frac
          ,    bas_metingen meti
          ,    bas_meetwaarden meet
          where frac.frac_id = meti.frac_id
          and   meti.meti_id = meet.meti_id
          and   meet.afgerond = 'N'
          and   meti.afgerond = 'N'
          and   frac.fractienummer = v_fractienummer
          and   meet.meet_id  = v_meet_id;

          if v_aantal = 0 then
            if v_log is null then
              v_log := ' -ERROR- Verwerking van deze regel is mislukt. '||chr(13)||chr(10)||'Deze meet_id is niet gevonden bij deze fractie, of is al afgerond. Volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id;
            else
              if length(v_log) < 1900 then
                v_log := v_log||chr(13)||chr(10)||' -ERROR- Verwerking van deze regel is mislukt. '||chr(13)||chr(10)||'Deze meet_id is niet gevonden bij deze fractie, of is al afgerond. Volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id;
              end if;
            end if;
          else

              for r_mdet in c_mdet ( v_meet_id)loop

                if nvl(r_mdet.waarde, 'ONB') <> 'ONB' then
                  if v_log is null then
                    v_log := v_log||' -ERROR- Reeds data aanwezig bij volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id||', prompt: '||r_mdet.prompt||chr(13)||chr(10);
                  else
                    if length(v_log) < 1900 then
                     v_log := v_log||chr(13)||chr(10)||' -ERROR- Reeds data aanwezig bij volgnummer: '||v_volgnummer||', fractienummer: '||v_fractienummer||', meet_id: '||v_meet_id||', prompt: '||r_mdet.prompt||chr(13)||chr(10);
                    end if;
                  end if;

                elsif r_mdet.prompt = 'Allel_1' then
                dbms_output.put_line('Allel_1: '|| r_kasp.allel_1);
                    update bas_meetwaarde_details
                    set waarde = r_kasp.allel_1
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Allel_2' then
                dbms_output.put_line('Allel_2: '|| r_kasp.allel_2);
                    update bas_meetwaarde_details
                    set waarde = r_kasp.allel_2
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Call' then
                dbms_output.put_line('call: '|| r_kasp.k_call);
                    update bas_meetwaarde_details
                    set waarde = r_kasp.k_call
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Quality Value' then
                dbms_output.put_line('quality Value: '|| r_kasp.quality_value);
                    update bas_meetwaarde_details
                    set waarde = r_kasp.quality_value
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Call Type' then
                dbms_output.put_line('call type: '|| r_kasp.k_call_type);
                    update bas_meetwaarde_details
                    set waarde = r_kasp.k_call_type
                    where current of c_mdet;

                elsif r_mdet.prompt = 'Strand' then
                dbms_output.put_line('Strand: '|| r_kasp.strand);
                    update bas_meetwaarde_details
                    set waarde = r_kasp.strand
                    where current of c_mdet;

                else
                  null;

                end if;

              end loop;


          end if;

        end if;

      end loop;

  end if;
  --verzamelen logtekst en versturen
  commit;

  if v_log is null then
    v_log := 'Verwerking van file succesvol.';
  else
    if length(v_log) < 1900 then
      v_log := v_log||chr(13)||chr(10)||'Verwerking van file succesvol.';
    end if;
  end if;

  insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user)
    values(v_log, v_sysdate, v_os_user, v_session_user);

  if length(v_log) < 1900 then
    v_log := v_log||chr(13)||chr(10)||'Verwerkingsdatum en tijd:= '||v_sysdate||chr(13)||chr(10)||'SESSION_user: '||v_session_user;
  end if;

  v_message := substr(v_log, 1, 2000);

  UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);


  exception

    when e_geen_data_in_file then
    rollback;
    v_log := ' -ERROR- Verwerking van file is mislukt. In deze file zijn geen fracties gevonden';
    insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values(v_log, v_sysdate, v_os_user, v_session_user);
    v_message := v_log||chr(13)||chr(10)||' Datum en tijd:= '||v_sysdate||chr(13)||chr(10)||'SESSION_user: '||v_session_user;
    v_message := substr(v_message, 1, 2000);

    UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);

    when others then
    rollback;
    v_log := '-ERROR- Verwerking van file is mislukt.'||SQLCODE||'  '||SUBSTR(SQLERRM, 1, 200);
    if v_db_name is null then
      select sys_context('userenv', 'db_name') into v_db_name from dual;
    end if;
    if v_os_user is null then
      select sys_context('userenv', 'os_user') into v_os_user from dual;
    end if;
    if v_session_user is null then
      select sys_context('userenv', 'session_user') into v_session_user from dual;
    end if;
    if v_sysdate is null then
      select to_char(sysdate, 'dd-mm-yyyy HH24:mi:ss') into v_sysdate from dual;
    end if;
    if v_title is null then
      v_title := 'Omgeving: '||v_db_name||' '||v_title||' van '||v_wlst_name;
    end if;
    insert into beh_kasp_inlees_log(log_tekst, datum_tijd, os_user, session_user) values(v_log, v_sysdate, v_os_user, v_session_user);
    v_message := v_log||chr(13)||chr(10)||' Datum en tijd:= '||v_sysdate||chr(13)||chr(10)||'SESSION_user: '||v_session_user;
    v_message := substr(v_message, 1, 2000) ;

    UTL_MAIL.SEND(sender => v_sender,
                recipients => v_recipient,
                cc => v_cc,
                subject => v_title,
                message => v_message);
end; --beh_kasp_inlezen;
/

/
QUIT
