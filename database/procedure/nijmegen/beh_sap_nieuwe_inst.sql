CREATE OR REPLACE PROCEDURE "HELIX"."BEH_SAP_NIEUWE_INST"
IS
   v_db_naam      VARCHAR2 (100) := SYS_CONTEXT ('USERENV', 'DB_NAME');
   v_newline      VARCHAR2 (10) := CHR (10);
   v_job_naam     VARCHAR2 (100) := 'SAP_NIEUWE_INST_CHECK';
   v_peildatum    DATE := beh_alfu_00.jobs_last_date (v_job_naam);
   v_sender       VARCHAR2 (100) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
   v_recipients   VARCHAR2 (2500)
                     := 'FEZBEHIFPA@umcn.nl,HelpdeskHelixCUKZ@cukz.umcn.nl';
   v_subject      VARCHAR2 (100) := v_db_naam || ';nieuwe HELIX instelling';
   v_message      VARCHAR2 (2500);

   CURSOR c_inst
   IS
        SELECT *
          FROM kgc_instellingen
         WHERE     vervallen = 'N'
               AND UPPER (NVL (land, 'NEDERLAND')) != 'NEDERLAND'
               AND creation_date >= v_peildatum
      ORDER BY inst_id;
BEGIN
   IF UPPER (v_db_naam) LIKE 'PROD%'
   THEN
      FOR r_inst IN c_inst
      LOOP
         v_message :=
               'beste,'
            || v_newline
            || 'Op '
            || TO_CHAR (r_inst.creation_date, 'dd-mm-yyyy hh24:ss:mm')
            || ' is een nieuwe BU instelling in Helix ingevoerd:'
            || v_newline
            || v_newline
            || 'Code: '
            || r_inst.code
            || v_newline
            || 'Naam: '
            || r_inst.naam
            || v_newline
            || 'Adres: '
            || r_inst.adres
            || v_newline
            || 'Woonplaats: '
            || r_inst.woonplaats
            || v_newline
            || 'Land: '
            || r_inst.land
            || v_newline
            || v_newline
            || 'Met vriendelijke groeten,'
            || v_newline
            || 'Serviceteam Genetica';

         UTL_MAIL.SEND (sender       => v_sender,
                        recipients   => v_recipients,
                        subject      => v_subject,
                        MESSAGE      => v_message);
      END LOOP;
   END IF;
END beh_sap_nieuwe_inst;
/

/
QUIT
