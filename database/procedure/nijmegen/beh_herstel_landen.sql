CREATE OR REPLACE PROCEDURE "HELIX"."BEH_HERSTEL_LANDEN" is
-- ###############################
-- NL versie van landnamen worden vervangen door Engelse versie
-- ###############################
cursor c_kgc_instellingen is
select inst_id,
        land,
        case upper(land)
        when upper('afghanistan') then 'Afghanistan'
        when upper('andorra') then 'Andorra'
        when upper('australie') then 'Australia'
        when upper('australia') then 'Australia'
        when upper('australi�') then 'Australia'
        when upper('austria') then 'Austria'
        when upper('amerika') then 'United States of America'
        when upper('arab. emiraten') then 'United Arab Emirates'
        when upper('argentinie') then 'Argentina'
        when upper('argentina') then 'Argentina'
        when upper('bahrein') then 'Bahrain'
        when upper('bahrain') then 'Bahrain'
        when upper('belgie') then 'Belgium'
        when upper('brazilie') then 'Brazil'
        when upper('brazil') then 'Brazil'
        when upper('bulgarije') then 'Bulgaria'
        when upper('bulgaria') then 'Bulgaria'
        when upper('belgium') then 'Belgium'
        when upper('Belgi�') then 'Belgium'
        when upper('canada') then 'Canada'
        when upper('china') then 'China'
        when upper('costa rica') then 'Costa Rica'
        when upper('croatia') then 'Croatia'
        when upper('denemarken') then 'Denmark'
        when upper('denmark') then 'Denmark'
        when upper('deutschland') then 'Germany'
        when upper('duitsland') then 'Germany'
        when upper('engeland') then 'Great Britain'
        when upper('egypte') then 'Egypt'
        when upper('egypt') then 'Egypt'
        when upper('estland') then 'Estonia'
        when upper('finland') then 'Finland'
        when upper('france') then 'France'
        when upper('frankrijk') then 'France'
        when upper('germany') then 'Germany'
        when upper('great britain') then 'Great Britain'
        when upper('greece') then 'Greece'
        when upper('griekenland') then 'Greece'
        when upper('groot brittanie') then 'Great Britain'
        when upper('groot brittannie') then 'Great Britain'
        when upper('groot brittanni�') then 'Great Britain'
        when upper('groot-brittanni�') then 'Great Britain'
        when upper('hongarije') then 'Hungary'
        when upper('hungary') then 'Hungary'
        when upper('ierland') then 'Ireland'
        when upper('ijsland') then 'Iceland'
        when upper('indonesie') then 'Indonesia'
        when upper('indonesi�') then 'Indonesia'
        when upper('israel') then 'Israel'
        when upper('italie') then 'Italy'
        when upper('itali�') then 'Italy'
        when upper('italy') then 'Italy'
        when upper('india') then 'India'
        when upper('irak') then 'Iraq'
        when upper('iraq') then 'Iraq'
        when upper('ireland') then 'Ireland'
        when upper('japan') then 'Japan'
        when upper('koeweit') then 'Kuwait'
        when upper('kuwait') then 'Kuwait'
        when upper('letland') then 'Latvia'
        when upper('latvia') then 'Latvia'
        when upper('luxemburg') then 'Luxembourg'
        when upper('luxembourg') then 'Luxembourg'
        when upper('libanon') then 'Libanon'
        when upper('litouwen') then 'Lithuania'
        when upper('lithuania') then 'Lithuania'
        when upper('malta') then 'Malta'
        when upper('mexico') then 'Mexico'
        when upper('maleisie') then 'Malaysia'
        when upper('malaysia') then 'Malaysia'
        when upper('nieuw zeeland') then 'New Zealand'
        when upper('new zealand') then 'New Zealand'
        when upper('noorwegen') then 'Norway'
        when upper('norway') then 'Norway'
        when upper('oostenrijk') then 'Austria'
        when upper('pakistan') then 'Pakistan'
        when upper('Philippines') then 'Philippines'
        when upper('Philipijnen') then 'Philippines'
        when upper('poland') then 'Poland'
        when upper('polen') then 'Poland'
        when upper('portugal') then 'Portugal'
        when upper('qatar') then 'Qatar'
        when upper('rusland') then 'Russian Federation'
        when upper('russian federation') then 'Russian Federation'
        when upper('russia') then 'Russian Federation'
        when upper('roemenie') then 'Romania'
        when upper('romania') then 'Romania'
        when upper('saoedi arabie') then 'Saudi Arabia'
        when upper('saudi arabia') then 'Saudi Arabia'
        when upper('slovenia') then 'Slovenia'
        when upper('south africa') then 'South Africa'
        when upper('spain') then 'Spain'
        when upper('spanje') then 'Spain'
        when upper('suriname') then 'Suriname'
        when upper('switzerland') then 'Switzerland'
        when upper('sweden') then 'Sweden'
        when upper('tanzania') then 'Tanzania'
        when upper('czech republic') then 'Czech Republic'
        when upper('turkey') then 'Turkey'
        when upper('turkije') then 'Turkey'
        when upper('taiwan') then 'Taiwan'
        when upper('tunesie') then 'Tunisia'
        when upper('tunisia') then 'Tunisia'
        when upper('U.S.A.') then 'United States of America'
        when upper('united states of america') then 'United States of America'
        when upper('united arab emirates') then 'United Arab Emirates'
        when upper('united kingdom') then 'Great Britain'
        when upper('great britain') then 'Great Britain'
        when upper('uruguay') then 'Uruguay'
        when upper('venezuela') then 'Venezuela'
        when upper('zweden') then 'Sweden'
        when upper('zwitserland') then 'Switzerland'
        when upper('zuid afrika') then 'South Africa'
        else '?'
        end land_eng
from kgc_instellingen
where land is not null
and   upper(land) <> upper('NEDERLAND')
and   land not in
(
'Afghanistan', 'Andorra', 'Argentina', 'Australia', 'Austria',
'Bahrain', 'Belgium', 'Brazil', 'Bulgaria',
'Canada', 'Czech Republic', 'China', 'Costa Rica', 'Croatia',
'Denmark',
'Egypt', 'Estonia',
'Finland', 'France',
'Germany', 'Great Britain', 'Greece',
'Hungary',
'Iceland', 'India', 'Indonesia', 'Iraq', 'Iran', 'Ireland', 'Israel', 'Italy',
'Japan',
'Kuwait',
'Latvia', 'Libanon', 'Lithuania', 'Luxembourg',
'Malta', 'Mexico', 'Malaysia',
'New Zealand', 'Norway',
'Pakistan', 'Philippines', 'Poland', 'Portugal',
'Qatar',
'Romania', 'Russian Federation',
'Saudi Arabia', 'Slovenia', 'South Africa', 'Spain', 'Suriname', 'Switzerland', 'Sweden',
'Tanzania', 'Turkey', 'Taiwan', 'Tunisia',
'United States of America', 'United Arab Emirates', 'Uruguay',
'Venezuela'
)
;
r_kgc_instellingen c_kgc_instellingen%rowtype;
--#########################################

cursor c_kgc_personen is
select pers_id,
        land,
        case upper(land)
        when upper('afghanistan') then 'Afghanistan'
        when upper('andorra') then 'Andorra'
        when upper('australie') then 'Australia'
        when upper('australia') then 'Australia'
        when upper('australi�') then 'Australia'
        when upper('austria') then 'Austria'
        when upper('amerika') then 'United States of America'
        when upper('arab. emiraten') then 'United Arab Emirates'
        when upper('argentinie') then 'Argentina'
        when upper('argentina') then 'Argentina'
        when upper('bahrein') then 'Bahrain'
        when upper('bahrain') then 'Bahrain'
        when upper('belgie') then 'Belgium'
        when upper('brazilie') then 'Brazil'
        when upper('brazil') then 'Brazil'
        when upper('bulgarije') then 'Bulgaria'
        when upper('bulgaria') then 'Bulgaria'
        when upper('belgium') then 'Belgium'
        when upper('Belgi�') then 'Belgium'
        when upper('canada') then 'Canada'
        when upper('china') then 'China'
        when upper('costa rica') then 'Costa Rica'
        when upper('croatia') then 'Croatia'
        when upper('denemarken') then 'Denmark'
        when upper('denmark') then 'Denmark'
        when upper('deutschland') then 'Germany'
        when upper('duitsland') then 'Germany'
        when upper('engeland') then 'Great Britain'
        when upper('egypte') then 'Egypt'
        when upper('egypt') then 'Egypt'
        when upper('estland') then 'Estonia'
        when upper('finland') then 'Finland'
        when upper('france') then 'France'
        when upper('frankrijk') then 'France'
        when upper('germany') then 'Germany'
        when upper('great britain') then 'Great Britain'
        when upper('greece') then 'Greece'
        when upper('griekenland') then 'Greece'
        when upper('groot brittanie') then 'Great Britain'
        when upper('groot brittannie') then 'Great Britain'
        when upper('groot brittanni�') then 'Great Britain'
        when upper('groot-brittanni�') then 'Great Britain'
        when upper('hongarije') then 'Hungary'
        when upper('hungary') then 'Hungary'
        when upper('ierland') then 'Ireland'
        when upper('ijsland') then 'Iceland'
        when upper('indonesie') then 'Indonesia'
        when upper('indonesi�') then 'Indonesia'
        when upper('israel') then 'Israel'
        when upper('italie') then 'Italy'
        when upper('itali�') then 'Italy'
        when upper('italy') then 'Italy'
        when upper('india') then 'India'
        when upper('irak') then 'Iraq'
        when upper('iraq') then 'Iraq'
        when upper('ireland') then 'Ireland'
        when upper('japan') then 'Japan'
        when upper('koeweit') then 'Kuwait'
        when upper('kuwait') then 'Kuwait'
        when upper('letland') then 'Latvia'
        when upper('latvia') then 'Latvia'
        when upper('luxemburg') then 'Luxembourg'
        when upper('luxembourg') then 'Luxembourg'
        when upper('libanon') then 'Libanon'
        when upper('litouwen') then 'Lithuania'
        when upper('lithuania') then 'Lithuania'
        when upper('malta') then 'Malta'
        when upper('mexico') then 'Mexico'
        when upper('maleisie') then 'Malaysia'
        when upper('malaysia') then 'Malaysia'
        when upper('nieuw zeeland') then 'New Zealand'
        when upper('new zealand') then 'New Zealand'
        when upper('noorwegen') then 'Norway'
        when upper('norway') then 'Norway'
        when upper('oostenrijk') then 'Austria'
        when upper('pakistan') then 'Pakistan'
        when upper('Philippines') then 'Philippines'
        when upper('Philipijnen') then 'Philippines'
        when upper('poland') then 'Poland'
        when upper('polen') then 'Poland'
        when upper('portugal') then 'Portugal'
        when upper('qatar') then 'Qatar'
        when upper('rusland') then 'Russian Federation'
        when upper('russian federation') then 'Russian Federation'
        when upper('russia') then 'Russian Federation'
        when upper('roemenie') then 'Romania'
        when upper('romania') then 'Romania'
        when upper('saoedi arabie') then 'Saudi Arabia'
        when upper('saudi arabia') then 'Saudi Arabia'
        when upper('slovenia') then 'Slovenia'
        when upper('south africa') then 'South Africa'
        when upper('spain') then 'Spain'
        when upper('spanje') then 'Spain'
        when upper('suriname') then 'Suriname'
        when upper('switzerland') then 'Switzerland'
        when upper('sweden') then 'Sweden'
        when upper('tanzania') then 'Tanzania'
        when upper('czech republic') then 'Czech Republic'
        when upper('turkey') then 'Turkey'
        when upper('turkije') then 'Turkey'
        when upper('taiwan') then 'Taiwan'
        when upper('tunesie') then 'Tunisia'
        when upper('tunisia') then 'Tunisia'
        when upper('U.S.A.') then 'United States of America'
        when upper('united states of america') then 'United States of America'
        when upper('united arab emirates') then 'United Arab Emirates'
        when upper('united kingdom') then 'Great Britain'
        when upper('great britain') then 'Great Britain'
        when upper('uruguay') then 'Uruguay'
        when upper('venezuela') then 'Venezuela'
        when upper('zweden') then 'Sweden'
        when upper('zwitserland') then 'Switzerland'
        when upper('zuid afrika') then 'South Africa'
        else '?'
        end land_eng
from kgc_personen
where land is not null
and   upper(land) <> upper('NEDERLAND')
and   land not in
(
'Afghanistan', 'Andorra', 'Argentina', 'Australia', 'Austria',
'Bahrain', 'Belgium', 'Brazil', 'Bulgaria',
'Canada', 'Czech Republic', 'China', 'Costa Rica', 'Croatia',
'Denmark',
'Egypt', 'Estonia',
'Finland', 'France',
'Germany', 'Great Britain', 'Greece',
'Hungary',
'Iceland', 'India', 'Indonesia', 'Iraq', 'Iran', 'Ireland', 'Israel', 'Italy',
'Japan',
'Kuwait',
'Latvia', 'Libanon', 'Lithuania', 'Luxembourg',
'Malta', 'Mexico', 'Malaysia',
'New Zealand', 'Norway',
'Pakistan', 'Philippines', 'Poland', 'Portugal',
'Qatar',
'Romania', 'Russian Federation',
'Saudi Arabia', 'Slovenia', 'South Africa', 'Spain', 'Suriname', 'Switzerland', 'Sweden',
'Tanzania', 'Turkey', 'Taiwan', 'Tunisia',
'United States of America', 'United Arab Emirates', 'Uruguay',
'Venezuela'
)
;
r_kgc_personen c_kgc_personen%rowtype;
--#######################################
cursor c_kgc_relaties is
select rela_id,
        land,
        case upper(land)
        when upper('afghanistan') then 'Afghanistan'
        when upper('andorra') then 'Andorra'
        when upper('australie') then 'Australia'
        when upper('australia') then 'Australia'
        when upper('australi�') then 'Australia'
        when upper('austria') then 'Austria'
        when upper('amerika') then 'United States of America'
        when upper('arab. emiraten') then 'United Arab Emirates'
        when upper('argentinie') then 'Argentina'
        when upper('argentina') then 'Argentina'
        when upper('bahrein') then 'Bahrain'
        when upper('bahrain') then 'Bahrain'
        when upper('belgie') then 'Belgium'
        when upper('brazilie') then 'Brazil'
        when upper('brazil') then 'Brazil'
        when upper('bulgarije') then 'Bulgaria'
        when upper('bulgaria') then 'Bulgaria'
        when upper('belgium') then 'Belgium'
        when upper('Belgi�') then 'Belgium'
        when upper('canada') then 'Canada'
        when upper('china') then 'China'
        when upper('costa rica') then 'Costa Rica'
        when upper('croatia') then 'Croatia'
        when upper('denemarken') then 'Denmark'
        when upper('denmark') then 'Denmark'
        when upper('deutschland') then 'Germany'
        when upper('duitsland') then 'Germany'
        when upper('engeland') then 'Great Britain'
        when upper('egypte') then 'Egypt'
        when upper('egypt') then 'Egypt'
        when upper('estland') then 'Estonia'
        when upper('finland') then 'Finland'
        when upper('france') then 'France'
        when upper('frankrijk') then 'France'
        when upper('germany') then 'Germany'
        when upper('great britain') then 'Great Britain'
        when upper('greece') then 'Greece'
        when upper('griekenland') then 'Greece'
        when upper('groot brittanie') then 'Great Britain'
        when upper('groot brittannie') then 'Great Britain'
        when upper('groot brittanni�') then 'Great Britain'
        when upper('groot-brittanni�') then 'Great Britain'
        when upper('hongarije') then 'Hungary'
        when upper('hungary') then 'Hungary'
        when upper('ierland') then 'Ireland'
        when upper('ijsland') then 'Iceland'
        when upper('indonesie') then 'Indonesia'
        when upper('indonesi�') then 'Indonesia'
        when upper('israel') then 'Israel'
        when upper('italie') then 'Italy'
        when upper('itali�') then 'Italy'
        when upper('italy') then 'Italy'
        when upper('india') then 'India'
        when upper('irak') then 'Iraq'
        when upper('iraq') then 'Iraq'
        when upper('ireland') then 'Ireland'
        when upper('japan') then 'Japan'
        when upper('koeweit') then 'Kuwait'
        when upper('kuwait') then 'Kuwait'
        when upper('letland') then 'Latvia'
        when upper('latvia') then 'Latvia'
        when upper('luxemburg') then 'Luxembourg'
        when upper('luxembourg') then 'Luxembourg'
        when upper('libanon') then 'Libanon'
        when upper('litouwen') then 'Lithuania'
        when upper('lithuania') then 'Lithuania'
        when upper('malta') then 'Malta'
        when upper('mexico') then 'Mexico'
        when upper('maleisie') then 'Malaysia'
        when upper('malaysia') then 'Malaysia'
        when upper('nieuw zeeland') then 'New Zealand'
        when upper('new zealand') then 'New Zealand'
        when upper('noorwegen') then 'Norway'
        when upper('norway') then 'Norway'
        when upper('oostenrijk') then 'Austria'
        when upper('pakistan') then 'Pakistan'
        when upper('Philippines') then 'Philippines'
        when upper('Philipijnen') then 'Philippines'
        when upper('poland') then 'Poland'
        when upper('polen') then 'Poland'
        when upper('portugal') then 'Portugal'
        when upper('qatar') then 'Qatar'
        when upper('rusland') then 'Russian Federation'
        when upper('russian federation') then 'Russian Federation'
        when upper('russia') then 'Russian Federation'
        when upper('roemenie') then 'Romania'
        when upper('romania') then 'Romania'
        when upper('saoedi arabie') then 'Saudi Arabia'
        when upper('saudi arabia') then 'Saudi Arabia'
        when upper('slovenia') then 'Slovenia'
        when upper('south africa') then 'South Africa'
        when upper('spain') then 'Spain'
        when upper('spanje') then 'Spain'
        when upper('suriname') then 'Suriname'
        when upper('switzerland') then 'Switzerland'
        when upper('sweden') then 'Sweden'
        when upper('tanzania') then 'Tanzania'
        when upper('czech republic') then 'Czech Republic'
        when upper('turkey') then 'Turkey'
        when upper('turkije') then 'Turkey'
        when upper('taiwan') then 'Taiwan'
        when upper('tunesie') then 'Tunisia'
        when upper('tunisia') then 'Tunisia'
        when upper('U.S.A.') then 'United States of America'
        when upper('united states of america') then 'United States of America'
        when upper('united arab emirates') then 'United Arab Emirates'
        when upper('united kingdom') then 'Great Britain'
        when upper('great britain') then 'Great Britain'
        when upper('uruguay') then 'Uruguay'
        when upper('venezuela') then 'Venezuela'
        when upper('zweden') then 'Sweden'
        when upper('zwitserland') then 'Switzerland'
        when upper('zuid afrika') then 'South Africa'
        else '?'
        end land_eng
from kgc_relaties
where land is not null
and   upper(land) <> upper('NEDERLAND')
and   land not in
(
'Afghanistan', 'Andorra', 'Argentina', 'Australia', 'Austria',
'Bahrain', 'Belgium', 'Brazil', 'Bulgaria',
'Canada', 'Czech Republic', 'China', 'Costa Rica', 'Croatia',
'Denmark',
'Egypt', 'Estonia',
'Finland', 'France',
'Germany', 'Great Britain', 'Greece',
'Hungary',
'Iceland', 'India', 'Indonesia', 'Iraq', 'Iran', 'Ireland', 'Israel', 'Italy',
'Japan',
'Kuwait',
'Latvia', 'Libanon', 'Lithuania', 'Luxembourg',
'Malta', 'Mexico', 'Malaysia',
'New Zealand', 'Norway',
'Pakistan', 'Philippines', 'Poland', 'Portugal',
'Qatar',
'Romania', 'Russian Federation',
'Saudi Arabia', 'Slovenia', 'South Africa', 'Spain', 'Suriname', 'Switzerland', 'Sweden',
'Tanzania', 'Turkey', 'Taiwan', 'Tunisia',
'United States of America', 'United Arab Emirates', 'Uruguay',
'Venezuela'
)
;
r_kgc_relaties c_kgc_relaties%rowtype;
--###############################
cursor c_kgc_verzekeraars is
select verz_id,
        land,
        case upper(land)
        when upper('afghanistan') then 'Afghanistan'
        when upper('andorra') then 'Andorra'
        when upper('australie') then 'Australia'
        when upper('australia') then 'Australia'
        when upper('australi�') then 'Australia'
        when upper('austria') then 'Austria'
        when upper('amerika') then 'United States of America'
        when upper('arab. emiraten') then 'United Arab Emirates'
        when upper('argentinie') then 'Argentina'
        when upper('argentina') then 'Argentina'
        when upper('bahrein') then 'Bahrain'
        when upper('bahrain') then 'Bahrain'
        when upper('belgie') then 'Belgium'
        when upper('brazilie') then 'Brazil'
        when upper('brazil') then 'Brazil'
        when upper('bulgarije') then 'Bulgaria'
        when upper('bulgaria') then 'Bulgaria'
        when upper('belgium') then 'Belgium'
        when upper('Belgi�') then 'Belgium'
        when upper('canada') then 'Canada'
        when upper('china') then 'China'
        when upper('costa rica') then 'Costa Rica'
        when upper('croatia') then 'Croatia'
        when upper('denemarken') then 'Denmark'
        when upper('denmark') then 'Denmark'
        when upper('deutschland') then 'Germany'
        when upper('duitsland') then 'Germany'
        when upper('engeland') then 'Great Britain'
        when upper('egypte') then 'Egypt'
        when upper('egypt') then 'Egypt'
        when upper('estland') then 'Estonia'
        when upper('finland') then 'Finland'
        when upper('france') then 'France'
        when upper('frankrijk') then 'France'
        when upper('germany') then 'Germany'
        when upper('great britain') then 'Great Britain'
        when upper('greece') then 'Greece'
        when upper('griekenland') then 'Greece'
        when upper('groot brittanie') then 'Great Britain'
        when upper('groot brittannie') then 'Great Britain'
        when upper('groot brittanni�') then 'Great Britain'
        when upper('groot-brittanni�') then 'Great Britain'
        when upper('hongarije') then 'Hungary'
        when upper('hungary') then 'Hungary'
        when upper('ierland') then 'Ireland'
        when upper('ijsland') then 'Iceland'
        when upper('indonesie') then 'Indonesia'
        when upper('indonesi�') then 'Indonesia'
        when upper('israel') then 'Israel'
        when upper('italie') then 'Italy'
        when upper('itali�') then 'Italy'
        when upper('italy') then 'Italy'
        when upper('india') then 'India'
        when upper('irak') then 'Iraq'
        when upper('iraq') then 'Iraq'
        when upper('ireland') then 'Ireland'
        when upper('japan') then 'Japan'
        when upper('koeweit') then 'Kuwait'
        when upper('kuwait') then 'Kuwait'
        when upper('letland') then 'Latvia'
        when upper('latvia') then 'Latvia'
        when upper('luxemburg') then 'Luxembourg'
        when upper('luxembourg') then 'Luxembourg'
        when upper('libanon') then 'Libanon'
        when upper('litouwen') then 'Lithuania'
        when upper('lithuania') then 'Lithuania'
        when upper('malta') then 'Malta'
        when upper('mexico') then 'Mexico'
        when upper('maleisie') then 'Malaysia'
        when upper('malaysia') then 'Malaysia'
        when upper('nieuw zeeland') then 'New Zealand'
        when upper('new zealand') then 'New Zealand'
        when upper('noorwegen') then 'Norway'
        when upper('norway') then 'Norway'
        when upper('oostenrijk') then 'Austria'
        when upper('pakistan') then 'Pakistan'
        when upper('Philippines') then 'Philippines'
        when upper('Philipijnen') then 'Philippines'
        when upper('poland') then 'Poland'
        when upper('polen') then 'Poland'
        when upper('portugal') then 'Portugal'
        when upper('qatar') then 'Qatar'
        when upper('rusland') then 'Russian Federation'
        when upper('russian federation') then 'Russian Federation'
        when upper('russia') then 'Russian Federation'
        when upper('roemenie') then 'Romania'
        when upper('romania') then 'Romania'
        when upper('saoedi arabie') then 'Saudi Arabia'
        when upper('saudi arabia') then 'Saudi Arabia'
        when upper('slovenia') then 'Slovenia'
        when upper('south africa') then 'South Africa'
        when upper('spain') then 'Spain'
        when upper('spanje') then 'Spain'
        when upper('suriname') then 'Suriname'
        when upper('switzerland') then 'Switzerland'
        when upper('sweden') then 'Sweden'
        when upper('tanzania') then 'Tanzania'
        when upper('czech republic') then 'Czech Republic'
        when upper('turkey') then 'Turkey'
        when upper('turkije') then 'Turkey'
        when upper('taiwan') then 'Taiwan'
        when upper('tunesie') then 'Tunisia'
        when upper('tunisia') then 'Tunisia'
        when upper('U.S.A.') then 'United States of America'
        when upper('united states of america') then 'United States of America'
        when upper('united arab emirates') then 'United Arab Emirates'
        when upper('united kingdom') then 'Great Britain'
        when upper('great britain') then 'Great Britain'
        when upper('uruguay') then 'Uruguay'
        when upper('venezuela') then 'Venezuela'
        when upper('zweden') then 'Sweden'
        when upper('zwitserland') then 'Switzerland'
        when upper('zuid afrika') then 'South Africa'
        else '?'
        end land_eng
from kgc_verzekeraars
where land is not null
and   upper(land) <> upper('NEDERLAND')
and   land not in
(
'Afghanistan', 'Andorra', 'Argentina', 'Australia', 'Austria',
'Bahrain', 'Belgium', 'Brazil', 'Bulgaria',
'Canada', 'Czech Republic', 'China', 'Costa Rica', 'Croatia',
'Denmark',
'Egypt', 'Estonia',
'Finland', 'France',
'Germany', 'Great Britain', 'Greece',
'Hungary',
'Iceland', 'India', 'Indonesia', 'Iraq', 'Iran', 'Ireland', 'Israel', 'Italy',
'Japan',
'Kuwait',
'Latvia', 'Libanon', 'Lithuania', 'Luxembourg',
'Malta', 'Mexico', 'Malaysia',
'New Zealand', 'Norway',
'Pakistan', 'Philippines', 'Poland', 'Portugal',
'Qatar',
'Romania', 'Russian Federation',
'Saudi Arabia', 'Slovenia', 'South Africa', 'Spain', 'Suriname', 'Switzerland', 'Sweden',
'Tanzania', 'Turkey', 'Taiwan', 'Tunisia',
'United States of America', 'United Arab Emirates', 'Uruguay',
'Venezuela'
)
;
r_kgc_verzekeraars c_kgc_verzekeraars%rowtype;
--############################
begin
for r_kgc_instellingen in c_kgc_instellingen loop
    if r_kgc_instellingen.land_eng <> '?' then
        update kgc_instellingen
        set land = r_kgc_instellingen.land_eng
        where inst_id = r_kgc_instellingen.inst_id
        ;
		insert into beh_herstel_landen_log
		(tabel_naam, id, waarde_oud, waarde_nieuw, creation_datum)
		values
		('KGC_INSTELLINGEN', r_kgc_instellingen.inst_id, r_kgc_instellingen.land, r_kgc_instellingen.land_eng, sysdate)
		;
    end if;
end loop;
/* zolang check_pers job draait, heeft geen zin
om kgc_personen bij te werken.
for r_kgc_personen in c_kgc_personen loop
    if r_kgc_personen.land_eng <> '?' then
        update kgc_personen
        set land = r_kgc_personen.land_eng
        where pers_id = r_kgc_personen.pers_id
        ;
		insert into beh_herstel_landen_log
		(tabel_naam, id, waarde_oud, waarde_nieuw, creation_datum)
		values
		('KGC_PERSONEN', r_kgc_personen.pers_id, r_kgc_personen.land, r_kgc_personen.land_eng, sysdate)
		;
    end if;
end loop;
*/
for r_kgc_relaties in c_kgc_relaties loop
    if r_kgc_relaties.land_eng <> '?' then
        update kgc_relaties
        set land = r_kgc_relaties.land_eng
        where rela_id = r_kgc_relaties.rela_id
        ;
		insert into beh_herstel_landen_log
		(tabel_naam, id, waarde_oud, waarde_nieuw, creation_datum)
		values
		('KGC_RELATIES', r_kgc_relaties.rela_id, r_kgc_relaties.land, r_kgc_relaties.land_eng, sysdate)
		;
    end if;
end loop;

for r_kgc_verzekeraars in c_kgc_verzekeraars loop
    if r_kgc_verzekeraars.land_eng <> '?' then
        update kgc_verzekeraars
        set land = r_kgc_verzekeraars.land_eng
        where verz_id = r_kgc_verzekeraars.verz_id
        ;
		insert into beh_herstel_landen_log
		(tabel_naam, id, waarde_oud, waarde_nieuw, creation_datum)
		values
		('KGC_VERZEKERAARS', r_kgc_verzekeraars.verz_id, r_kgc_verzekeraars.land, r_kgc_verzekeraars.land_eng, sysdate)
		;
    end if;
end loop;

commit;

exception
    when others then
        rollback;
        dbms_output.put_line('Er is een fout opgetreden!');
        dbms_output.put_line(substr('SQLErrm ' || sqlerrm, 1, 255));
end;

/

/
QUIT
