CREATE OR REPLACE PROCEDURE "HELIX"."BEH_ZET_HOM_ICODES" as
v_icode varchar2(10) := 'I000203';

cursor c_decl is
select distinct(onde_id) onde_id
--from beh_open_decl_vw
from beh_decl_check_vw
where kafd_code = 'METAB'
and ongr_code = 'HOM'
and decl_type = 'INTERN'
and dewy_code = 'WDS'
AND spec_code != 'KG'
and onde_afgerond = 'J'
and kgc_attr_00.waarde('KGC_ONDERZOEKEN', 'FEZ_ALT_ICODE', onde_id) is null
;

cursor c_attr is
select *
from kgc_attributen
where tabel_naam = 'KGC_ONDERZOEKEN'
and code = 'FEZ_ALT_ICODE'
;
r_attr c_attr%rowtype;

begin
open c_attr;
fetch c_attr into r_attr;
if c_attr%found then
	for r_decl in c_decl loop
		Insert into kgc_attribuut_waarden
		(attr_id, id, waarde)
		values (r_attr.attr_id, r_decl.onde_id, v_icode)
		;
	end loop;
end if;
close c_attr;
	
commit;
exception
  when others then
  rollback;
  raise;
end;
/

/
QUIT
