CREATE OR REPLACE PROCEDURE "HELIX"."NAAR_TOREN"
( p_kafd_code in varchar2 := null
)
IS
  cursor decl_cur
  is
    select *
    from   dft_koppeling_vw
    where  kafd_code = nvl( upper( p_kafd_code ), kafd_code )
    ;
--  rec dft_koppeling@koppel79%rowtype;
  rec dft_koppeling%rowtype;
BEGIN
  for r in decl_cur
  loop
    begin
--      insert into dft_koppeling@koppel79
      insert into dft_koppeling
      ( odbnr
      , bonnummer
      , verrichtingcode
      , systeemdeel
      , prod_afdeling
      , zisnr
      , verrichtingdat
      , aanvr_afdeling
      , prod_spec
      , aanvr_spec
      , abw
      , abi_verzcd
      , abi_verznr
      , tekst
      , aantal
      , naam
      , gebdat
      , adres
      , wplts
      , pstcd
      , land
      , status
      , stat_oms
      )
      values
      ( r.odbnr
      , r.bonnummer
      , r.verrichtingcode
      , r.systeemdeel
      , r.prod_afdeling
      , r.zisnr
      , r.verrichtingdat
      , r.aanvr_afdeling
      , r.prod_spec
      , r.aanvr_spec
      , r.abw
      , r.abi_verzcd
      , r.abi_verznr
      , r.tekst
      , r.aantal
      , r.naam
      , r.gebdat
      , r.adres
      , r.wplts
      , r.pstcd
      , r.land
      , r.status
      , r.stat_oms
      );
      update kgc_declaraties
      set    status = 'V'
      ,      datum_verwerking = sysdate
      where  decl_id = r.decl_id
      ;
    exception
      when others
      then
        raise;
    end;
  end loop;
  commit;
  -- en weer leegmaken...
--  delete from dft_koppeling@koppel79
  delete from dft_koppeling
  where systeemdeel = nvl( upper( p_kafd_code ), systeemdeel )
  ;
  commit;
END naar_toren;

/

/
QUIT
