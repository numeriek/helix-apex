CREATE OR REPLACE PROCEDURE "HELIX"."BEH_PROCES_MODO_MEDE"
(p_oracle_uid kgc_medewerkers.oracle_uid%type, p_actie varchar2)
is
-- naar aanleiding van relatiebeheer project
-- hiermee worden autorisiatie van een helix gebruiken
-- uitbreid of beperkt voor relatie beheer modules.
-- en deze zijn
-- relaties, instellingen, afdelinge en specialismen schermen
v_mede_id kgc_medewerkers.mede_id%type;
v_oracle_uid kgc_medewerkers.oracle_uid%type := trim(upper(p_oracle_uid));
v_actie varchar2(10) := trim(upper(p_actie));
-- relaties, instellingen, afdelinge en specialismen schermen
v_modules varchar2(2500) :=
    ('KGCINST01;KGCRELA01;KGCAFDG01;KGCSPEC01');

v_return varchar2(4000);

cursor c_moto is
select *
from table(cast(beh_alfu_00.explode(';', upper(v_modules)) as beh_listtable))
;

begin
select mede_id
into v_mede_id
from kgc_medewerkers
where oracle_uid = v_oracle_uid
;
v_return :='Module toegang van mede_id: ' || to_char(v_mede_id) || ' (' || v_oracle_uid || ') voor modules: ';
for r_moto in c_moto loop
    if v_actie = 'INSERT' then
        insert into
        kgc_module_toegang
        (module, andere_kafd, mede_id)
        values (r_moto.column_value, 'J', v_mede_id)
        ;

    elsif v_actie = 'DELETE' then
        delete
        from kgc_module_toegang
        where upper(module) = r_moto.column_value
        and mede_id = v_mede_id
        ;
   else
      dbms_output.put_line(v_actie || ' is een onbekende actie.');
    end if;
    v_return :=v_return || r_moto.column_value || ', ';
end loop;

commit;
v_return := v_return || ' is succesvol verwerkt.';
dbms_output.put_line(v_return);
exception
  when others then
    rollback;
    dbms_output.put_line(substr(sqlerrm, 1, 4000) || ' => ' ||v_return);
end;
/

/
QUIT
