CREATE OR REPLACE PROCEDURE "HELIX"."BEH_IMP_ICODES_PRC" is
v_attr_code kgc_attributen.code%type := 'FEZ_ICODE';
v_attr_tabel_naam kgc_attributen.code%type := 'KGC_INSTELLINGEN';

cursor c_attr is
select *
from kgc_attributen
where tabel_naam = v_attr_tabel_naam
and   code = v_attr_code
;
r_attr c_attr%rowtype;

cursor c_icodes is
select  imic.inst_id,
        imic.inst_code,
        inst.code,
        case
            when imic.inst_code = inst.code then '=='
            else '<>'
        end inst_code_verg,
        imic.icode,
        kgc_attr_00.waarde(v_attr_tabel_naam, v_attr_code, imic.inst_id) fez_icode,
        case
            when imic.icode = kgc_attr_00.waarde(v_attr_tabel_naam, v_attr_code, imic.inst_id) then '=='
            else '<>'
        end icode_verg
from kgc_instellingen inst, beh_imp_icodes imic
where imic.inst_id = inst.inst_id(+)
order by imic.inst_id
;
begin
open  c_attr;
fetch c_attr into r_attr;
close c_attr;

for r_icodes in c_icodes loop
    if r_icodes.fez_icode is null and r_icodes.inst_code_verg = '==' then
        insert into kgc_attribuut_waarden
        (attr_id, id, waarde)
        values(r_attr.attr_id, r_icodes.inst_id, r_icodes.icode)
        ;
    else
        dbms_output.put_line(to_char(r_icodes.inst_id) || ' Niet ingelezen.');
    end if;
end loop;
commit;
exception
 when others then
    rollback;
    dbms_output.put_line(substr(sqlerrm, 1, 2500));
end;

/

/
QUIT
