CREATE OR REPLACE PROCEDURE "HELIX"."BEH_HERSTEL_NGS_DECL"
is
-- zie mantisnr 6568 voor toelichting


v_log_tekst varchar2(2000);

v_decl_meti_zisnr kgc_personen.zisnr%type;
v_decl_meti_aanspreken kgc_personen.aanspreken%type;

-- te herstellen declaratie records;
-- alleen protocollen
cursor c_decl_meti is
select     pers.zisnr
,         pers.aanspreken
,         meti.onde_id
,         frac.frac_id
,         frac.fractienummer
,         decl.decl_id
,         mons.pers_id mons_pers_id
,         decl.pers_id decl_persoon_id
,         stgr.code protocol
from kgc_personen pers
,       kgc_monsters mons
,       bas_fracties frac
,       bas_metingen meti
,       kgc_declaraties decl
,       kgc_kgc_afdelingen kafd
,       kgc_onderzoeksgroepen ongr
,       kgc_stoftestgroepen stgr
where   pers.pers_id = mons.pers_id
and     mons.mons_id = frac.mons_id
and     frac.frac_id = meti.frac_id
and     meti.meti_id = decl.id
and     meti.stgr_id = stgr.stgr_id
and     decl.kafd_id = kafd.kafd_id
and     decl.ongr_id = ongr.ongr_id
and     decl.entiteit = 'METI'
and     decl.status is null
and     decl.declareren  = 'J'
and     kafd.code = 'GENOOM'
and     ongr.code = 'NGS'
and     mons.pers_id <> decl.pers_id
and     decl.creation_date > to_date('01-01-2011','dd-mm-yyyy')
--and     meti.onde_id =
--        (select onde_id from kgc_onderzoeken where onderzoeknr = 'D11-10007')
;
-- iedere protocol declaratie heeft in dit geval
-- een betrokkene declaratie; deze halen we hier op
cursor c_decl_onbe(p_pers_id number, p_onde_id number, p_frac_id number) is
select decl.*
from     kgc_declaraties decl
,       kgc_kgc_afdelingen kafd
,       kgc_onderzoeksgroepen ongr
where   decl.pers_id = p_pers_id
and     decl.kafd_id = kafd.kafd_id
and     decl.ongr_id = ongr.ongr_id
and     decl.entiteit = 'ONBE'
and     decl.status is null
and     decl.declareren  = 'J'
and     kafd.code = 'GENOOM'
and     ongr.code = 'NGS'
and     decl.id in
            (
                select     onbe_id
                from     kgc_onderzoek_betrokkenen
                where     pers_id = p_pers_id
                and     onde_id = p_onde_id
                and frac_id = p_frac_id
            )
and     decl.creation_date > to_date('01-01-2011','dd-mm-yyyy')
;

begin
for r_decl_meti in c_decl_meti loop
    for r_decl_onbe in c_decl_onbe(r_decl_meti.mons_pers_id, r_decl_meti.onde_id, r_decl_meti.frac_id) loop

        select zisnr, aanspreken
        into  v_decl_meti_zisnr, v_decl_meti_aanspreken
        from kgc_personen
        where pers_id =  r_decl_meti.decl_persoon_id
        ;

        v_log_tekst := '@' || to_char(sysdate, 'dd-mm-yyyy HH24:MM:SS' ) ||
                            ': door herstel script (beh_herstel_ngs_decl) is de declaratie persoon van: ' ||
                            v_decl_meti_zisnr || ' (' || v_decl_meti_aanspreken || ') naar: ' ||
                            r_decl_meti.zisnr || ' (' || r_decl_meti.aanspreken || ') veranderd.@';
        /*
        Logging in aparte tabel is uitgezet.

        -- loggen van record voor het geval dat!
        insert into kgc_declaraties_6568
        select *
        from kgc_declaraties
        where decl_id = r_decl_meti.decl_id
        ;

        update kgc_declaraties_6568
        set verwerking = v_log_tekst || verwerking
        where decl_id = r_decl_meti.decl_id
        ;
        */
        -- updaten van record;
        -- protocol declaratie record trekken we eigenlijk
        -- gelijk aan betrokkene declaratie record (niet alles natuurlijk!)
        update     kgc_declaraties
        set        pers_id = r_decl_onbe.pers_id,
                nota_adres = r_decl_onbe.nota_adres,
                verz_id = r_decl_onbe.verz_id,
                verzekeringsnr = r_decl_onbe.verzekeringsnr,
                verzekeringswijze = r_decl_onbe.verzekeringswijze,
                verwerking = v_log_tekst || verwerking
        where     decl_id = r_decl_meti.decl_id
        ;

    end loop;
end loop;
commit;
end;
/

/
QUIT
