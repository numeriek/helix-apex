CREATE OR REPLACE PROCEDURE "HELIX"."BEH_MACHTIGING_NODIG" IS
-- Kolom machtiging_nodig in kgc_verzekeraars wordt op 'J' gezet.
-- Dit zou eigenlijk default waarde van de kolom moeten zijn maar zo is het niet
-- vandaar deze procedure

BEGIN

 UPDATE kgc_verzekeraars verz1
 SET verz1.machtiging_nodig = 'J'
 WHERE verz1.verz_id in ( SELECT verz2.verz_id
      FROM kgc_verzekeraars verz2
      WHERE verz2.machtiging_nodig = 'N'
      AND verz2.vervallen = 'N');

 COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    NULL;

END;


/

/
QUIT
