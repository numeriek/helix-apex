CREATE OR REPLACE PROCEDURE "HELIX"."BEH_BROKEN_JOB_FIXER"
AS
/******************************************************************************
   NAME:       BEH_BROKEN_JOB_FIXER
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19-12-2013   Z268164       1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     BEH_BROKEN_JOB_FIXER
      Sysdate:         19-12-2013
      Date and Time:   19-12-2013, 12:05:43, and 19-12-2013 12:05:43
      Username:        Z268164 (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
   /*
   || calls DBMS_JOB.BROKEN to try and set
   || FLOW broken jobs to unbroken
   */

   /* cursor selects user's broken jobs */
   CURSOR broken_jobs_cur
   IS
   SELECT job
     FROM user_jobs
    WHERE broken = 'Y'
    and what like '%dbms_refresh%';

BEGIN
   FOR job_rec IN broken_jobs_cur
   LOOP
      DBMS_JOB.BROKEN(job_rec.job,FALSE);
   END LOOP;
   COMMIT;
END BEH_BROKEN_JOB_FIXER;
/

/
QUIT
