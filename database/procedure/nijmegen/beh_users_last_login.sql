CREATE OR REPLACE PROCEDURE "HELIX"."BEH_USERS_LAST_LOGIN" as

cursor c_lalo is
select moni.oracle_uid, max(trunc(moni.tijdstip)) last_login
from kgc_monitoring moni, kgc_medewerkers mede
where moni.oracle_uid = mede.oracle_uid
and mede.vervallen = 'N'
group by moni.oracle_uid
having max(trunc(moni.tijdstip)) < (trunc(sysdate) - (365*1))
order by moni.oracle_uid
;
v_count number :=0;
v_db_naam varchar2(50) := sys_context('userenv','db_name');
v_subject varchar2(500) := 'Gebruikers last_login ('||v_db_naam||')';
v_this_prog varchar2(100) := 'beh_users_last_login';
v_afzender varchar2(500);
v_html_msg varchar2(32767);
v_error_txt varchar2(4000);
begin
select kgc_sypa_00.systeem_waarde('BEH_MIJN_HELIX_EMAIL_ADRES', null, null, (select mede_id from kgc_medewerkers where code = 'HELIX'))
into v_afzender
FROM dual
;

for r_lalo in c_lalo loop
v_count := v_count+1;
if v_count = 1 then
  v_html_msg := '<table border=1>' ||
            '<tr><td colspan=3 align=center><b>Sinds vorige jaar niet ingelog:</b></td></tr>' ||
            '<tr><th>nr</th><th>oracle_uid</th><th>last_login</th></tr>';
end if;
v_html_msg := v_html_msg ||
      '<tr><td>'|| to_char(v_count)|| '</td><td>'|| r_lalo.oracle_uid|| '</td><td>' || to_char(r_lalo.last_login, 'dd-mm-yyyy')|| '</td></tr>';
end loop;

if length(v_html_msg) > 0 then
v_html_msg := v_html_msg || '</table><br><br>' ||
    'Deze email is automatisch gegenereerd en verzonden door procedure: ' || v_this_prog;
end if;

send_mail_html
    (
        p_from => v_afzender,
        p_to   => v_afzender,
        p_subject => v_subject,
        p_html => v_html_msg
        );
exception
    when others then
    v_error_txt := v_this_prog || ' => ' || ':' || SUBSTR(SQLERRM, 1, 2000);
    dbms_output.put_Line(v_error_txt);

end beh_users_last_login;
/

/
QUIT
