CREATE OR REPLACE PROCEDURE "HELIX"."BEH_ZET_DATUM_VRIJGEVEN" (p_onde_id in number
, p_kafd_id in number
, p_ongr_id in number
, P_rela_id in number
, p_datum_autorisatie date) is

  cursor c_ongr  (p_ongr_id in number)
  is
  select code
  from kgc_onderzoeksgroepen ongr
  where ongr_id      = p_ongr_id
  ;
  r_ongr c_ongr%ROWTYPE;

  cursor c_uits  (p_onde_id in number)
  is
  select uits_id
  from kgc_uitslagen uits
  ,    kgc_brieftypes brty
  where onde_id      = p_onde_id
  and   uits.brty_id = brty.brty_id
  and   brty.code    = 'EINDBRIEF'
  order by uits.last_update_date desc
  ;
  r_uits c_uits%ROWTYPE;

  cursor c_onep  (p_onde_id in number, p_uits_id in number)
  is
  select onep_id
  from beh_kgc_onderzoeken_epd
  where onde_id = p_onde_id
  and uits_id = p_uits_id
  for update of datum_beschikbaar_vanaf
  ;
  r_onep c_onep%ROWTYPE;

  cursor c_rela  (p_rela_id in number) -- aanvragers van klinishce genetica van het het UMCN
  is
  select rela_id
  from kgc_relaties rela
  , kgc_afdelingen afdg
  where rela_id = p_rela_id
  and afdg.afdg_id = rela.afdg_id
--  and afdg.code = 'KGEI'
--  and afdg.code = 'KLIN_849'-- aangepast op 07nov2011door cv
  and afdg.code = 'KLIN_836'
  and instr(upper(rela.adres), 'ALHIER') > 0
  ;
  r_rela c_rela%ROWTYPE;


  v_uits number := null;

  v_onep number := null ;
  v_term varchar2(100) := null ;
  v_vrijgeven date;

BEGIN
  IF (   p_onde_id is null
      OR p_kafd_id is null
      OR p_rela_id is null
      OR p_ongr_id is null
      OR p_datum_autorisatie is null
      )
  THEN
      NULL;
  ELSE

  -- gaat het om een onderzoek van een prenatale onderzoeksgroep? Dan geen embargo!
    OPEN c_ongr(p_ongr_id);
    FETCH c_ongr INTO r_ongr;
    IF c_ongr%FOUND THEN
      IF substr(r_ongr.code, 1, 4) = 'PRE_' THEN NULL;
      ELSE

        OPEN c_rela(p_rela_id);
        FETCH c_rela INTO r_rela;
        IF c_rela%FOUND THEN
          v_onep:= null;
          -- haal de termijn op
          select  kgc_sypa_00.standaard_waarde ('BEH_EPD_EMBARGO_TERMIJN',
                                        p_kafd_id,
                                        p_ongr_id,
                                        NULL)
            INTO v_term from dual;
          IF v_term is null THEN v_term := 0; END IF;
          v_vrijgeven := (p_datum_autorisatie +  to_number(v_term));
          -- bepaal de uitslag
          OPEN c_uits(p_onde_id);
          FETCH c_uits INTO r_uits;
          IF c_uits%FOUND THEN
            v_uits := r_uits.uits_id;
          END IF;
          CLOSE c_uits;
          IF v_uits is null
            THEN NULL;
          ELSE

            OPEN C_onep( p_onde_id, v_uits);
            FETCH C_onep INTO R_onep;
            IF C_onep%FOUND THEN
              update beh_kgc_onderzoeken_epd
              set datum_beschikbaar_vanaf = v_vrijgeven
              where current of c_onep;

            ELSE
              insert into beh_kgc_onderzoeken_epd
              ( onde_id
              , uits_id
              , datum_beschikbaar_vanaf
               )
              values
              (  p_onde_id
              , v_uits
              , v_vrijgeven
              );

            END IF;
            CLOSE C_onep;
          END IF;
        END IF;
        CLOSE c_rela;
      END IF;
    END IF;
  END IF;
  CLOSE c_ongr;
EXCEPTION
    WHEN OTHERS THEN
     null;
     -- raise_application_error( -20000, 'probleem met procedure' );
END;

/

/
QUIT
