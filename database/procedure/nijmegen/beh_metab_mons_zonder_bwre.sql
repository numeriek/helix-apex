CREATE OR REPLACE PROCEDURE "HELIX"."BEH_METAB_MONS_ZONDER_BWRE" (p_dagen_terug in number default 31) as
--mantisnr 9919
v_this_prog varchar2(100) := 'beh_metab_mons_zonder_bwre';
v_email_from kgc_systeem_parameters.standaard_waarde%type :=
         trim(kgc_sypa_00.systeem_waarde('BEH_STGEN_EMAIL_FROM'));
v_email_to varchar2(200) := 'LKNbasisdiagnostiek@cukz.umcn.nl';
v_msg clob := '<table align=center border=1><tr><td>Nr</td><td>monsternummer</td><td>Bewaren</td><td>Datum_aanmelding</td><td>Melding</td></tr>';
v_count number(10) := 0;
v_subject varchar2(500) := 'Monsters zonder bewaarreden (sinds: ' || trunc(sysdate-p_dagen_terug) || ')';

cursor c_mons is
select mons.monsternummer, mons.bewaren, mons.datum_aanmelding, 'Bewaren is aangevinkt maar geen bewaarreden ingevoerd.' melding
from kgc_monsters mons, kgc_kgc_afdelingen kafd
where mons.kafd_id = kafd.kafd_id
and kafd.code = 'METAB'
and mons.bewaren = 'J'
and mons.creation_date >= (sysdate-p_dagen_terug)
and not exists
(
select null
from kgc_monster_bewaarredenen
where mons_id = mons.mons_id
)
union
select mons.monsternummer, mons.bewaren, mons.datum_aanmelding, 'Bewaren is afgevinkt maar wel bewaarreden ingevoerd.' melding
from kgc_monsters mons, kgc_kgc_afdelingen kafd
where mons.kafd_id = kafd.kafd_id
and kafd.code = 'METAB'
and mons.bewaren = 'N'
and mons.creation_date >= (sysdate-p_dagen_terug)
and exists
(
select null
from kgc_monster_bewaarredenen
where mons_id = mons.mons_id
)
order by 3 desc
;

begin
for r_mons in c_mons loop
  v_count := v_count + 1;

  v_msg := v_msg || '<tr><td>'|| to_char(v_count) ||'</td><td>' || r_mons.monsternummer || '</td><td>' || r_mons.bewaren || '</td><td>' || r_mons.datum_aanmelding ||'</td><td>' || r_mons.melding ||'</td></tr>';

end loop;

v_msg := v_msg || '</table><br><br><br>';

v_msg := v_msg || 'Deze e-mail is automatisch door Helix procedure: ' || v_this_prog || ' gegenereerd en verzonden.<br><br><br>';
v_msg := v_msg || 'Met vriendelijke groeten,<br>';
v_msg := v_msg || 'Serviceteam Genetica<br>';

if (substr(sys_context('userenv','db_name'), 1, 4) = 'PROD' and v_count > 0) then
  send_mail_html(
      p_from => v_email_from
    , p_to => v_email_to
    , p_subject => v_subject
    , p_html => v_msg
  );
end if;

exception
  when others then
  raise;
end;
/

/
QUIT
