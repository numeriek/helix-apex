CREATE OR REPLACE PROCEDURE "HELIX"."BEH_DEBUG_MEETWAARDEN2" is
v_gf varchar2(1);
cursor c_vw is
select mons.monsternummer, meet.meet_id, meet.meetwaarde
from kgc_monsters mons,
    bas_fracties frac,
    bas_metingen meti,
    bas_meetwaarden meet,
    kgc_kgc_afdelingen kafd
where mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   meti.meti_id = meet.meti_id
and   frac.kafd_id = kafd.kafd_id
and    kafd.code = 'METAB'
;
begin
for r_vw in c_vw loop
    select BEH_DEBUG_MEETWAARDEN(r_vw.meet_id) into v_gf from dual;
    if v_gf = 'F' then
        insert into beh_foute_meetwaarden values
            (r_vw.monsternummer, r_vw.meet_id, r_vw.meetwaarde);
        commit;
    end if;
end loop;
end;

/

/
QUIT
