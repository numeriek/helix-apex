CREATE OR REPLACE PROCEDURE "HELIX"."BEH_SER_MATCH" as

cursor c_match is
select rela.rela_id, to_number(ser.id) ser_id
from kgc_relaties rela, beh_ser ser
where upper(rela.postcode) = upper(ser.addr_zip_code)
and upper(rela.achternaam) = upper(ser.achternaam)
and nvl(upper(rela.land), 'NEDERLAND') = 'NEDERLAND'
and rela.vervallen = 'N'
order by rela.rela_id
;
begin

delete
from beh_ser_helix_matches
;
commit;

for r_match in c_match loop
    insert into beh_ser_helix_matches
    values(r_match.rela_id, r_match.ser_id)
    ;
end loop;
commit;
exception
when others then
  rollback;
  raise;
end;
/

/
QUIT
