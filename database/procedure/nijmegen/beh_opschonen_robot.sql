CREATE OR REPLACE PROCEDURE "HELIX"."BEH_OPSCHONEN_ROBOT" IS
-- Tabellen BAS_ROBOT_INVOER en BAS_ROBOT_UITSLAGEN worden dagelijks opgeschoond (alles wordt verwijderd).

BEGIN

 DELETE FROM bas_robot_invoer;
 DELETE FROM bas_robot_uitslagen;

 COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    NULL;

END;


/

/
QUIT
