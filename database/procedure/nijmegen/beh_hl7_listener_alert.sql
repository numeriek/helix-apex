CREATE OR REPLACE PROCEDURE "HELIX"."BEH_HL7_LISTENER_ALERT" is
v_afzender varchar2(100) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
v_ontvanger varchar2(100) := 'HelpdeskHelixCUKZ@cukz.umcn.nl';
v_boodschap varchar2(2500);
v_newline varchar2(10) := chr(10);

cursor c_hlbe is
select max(hlbe_id) hlbe_id, max(creation_date) creation_date,
case
  when max(creation_date) > (sysdate-(1/24)) then 'GOED'
  else 'FOUT'
  end status
from kgc_hl7_berichten
where bericht_type = 'ADT_A08'
;
r_hlbe c_hlbe%rowtype;
begin
open c_hlbe;
fetch c_hlbe into r_hlbe;
close c_hlbe;

if r_hlbe.status = 'FOUT' and SYS_CONTEXT ('USERENV', 'DB_NAME') like 'PROD%' then
  v_boodschap :=
    'Het is nu: ' || to_char(sysdate, 'dd-mm-yyyy HH24:MI:SS') ||
    ' en de laatste ADT08 bericht (hlbe_id=' || to_char(r_hlbe.hlbe_id) ||
    ') is meer dan 1 uur geleden ('|| to_char(r_hlbe.creation_date, 'dd-mm-yyyy HH24:MI:SS') ||
    ') ontvangen.' ||
    v_newline ||
    v_newline ||
    'Waarschijnlijk HL7 listener, luistert niet meer.' ||
    v_newline ||
    v_newline ||
    'Helix systeembeheer' ||
    v_newline ||
    v_newline ||
    v_newline ||
    v_newline ||
    'Dit bericht is automatisch door Helix gegenereerd en verzonden.';
  UTL_MAIL.SEND(  sender => v_afzender,
                recipients => v_ontvanger,
                subject => 'HL7 listener',
                message => v_boodschap);

end if;

exception
  when others then
  raise;
end beh_hl7_listener_alert;
/

/
QUIT
