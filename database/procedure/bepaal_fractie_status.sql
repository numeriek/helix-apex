CREATE OR REPLACE PROCEDURE "HELIX"."BEPAAL_FRACTIE_STATUS"
 (P_CONC_ID IN NUMBER := NULL
 ,P_EENHEID_ID IN NUMBER := NULL
 ,P_FRTY_TYPE_ID IN NUMBER := NULL
 ,P_STATUS IN OUT VARCHAR2
 )
 IS
/*Column name FRACTIE_TYPE changed to FRTY_ID and EENHEID to EENH_ID
by Anuja against Release rework 8.8.1.2 for Mantis3021 on 05-06-2015 */
CURSOR c_status
   IS
      SELECT fsco.fractie_status
        FROM bas_fractie_status_conc fsco
       WHERE (   (p_conc_id BETWEEN fsco.concentratie_vanaf
                                AND fsco.concentratie_tot_aan
                 )
              OR     fsco.concentratie_vanaf IS NULL
                 AND fsco.concentratie_tot_aan < p_conc_id
             )
         AND fsco.vervallen = 'N'
         AND (   (    fsco.FRTY_ID IS NOT NULL
                  AND fsco.FRTY_ID = p_frty_type_id
                 )
              OR (fsco.FRTY_ID IS NULL)
             )
         --AND fsco.EENH_ID = p_eenheid_id;                  -- Commentd by Sachin patel on   20-12-2015 for Mantis 3021(Release rework 8.9.1.2);
         AND fsco.eenh_id = nvl(p_eenheid_id, fsco.eenh_id); -- Code added by Sachin patel on 20-12-2015 for Mantis 3021(Release rework 8.9.1.2);

   v_status   bas_fractie_status_conc.fractie_status%TYPE;
BEGIN
   OPEN c_status;

   FETCH c_status
    INTO v_status;

   CLOSE c_status;

   IF v_status IS NOT NULL
   THEN
      p_status := v_status;
   END IF;
END bepaal_fractie_status;
/

/
QUIT
