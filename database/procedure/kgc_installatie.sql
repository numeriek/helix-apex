CREATE OR REPLACE PROCEDURE "HELIX"."KGC_INSTALLATIE"
 (P_ACTIE IN VARCHAR2 := 'START'
 ,P_VERSIE IN VARCHAR2
 )
 IS
  v_actie VARCHAR2(5) := UPPER( p_actie );
  v_versie VARCHAR2(20) := UPPER( p_versie );
  v_datum DATE := SYSDATE;
  v_user VARCHAR2(30) := USER;
  CURSOR inve_cur
  IS
    SELECT afgerond
    FROM   kgc_geinstalleerde_versies
    WHERE  versie = v_versie
    ORDER BY datum DESC
    FOR UPDATE OF afgerond nowait
    ;
  inve_rec inve_cur%rowtype;
BEGIN
  IF ( v_actie IN ( 'BEGIN', 'START' ) )
  THEN
    INSERT INTO kgc_geinstalleerde_versies
    ( datum
    , versie
    , username
    , afgerond
    )
    VALUES
    ( v_datum
    , v_versie
    , v_user
    , 'N'
    );
    COMMIT;
  ELSIF ( v_actie IN ( 'EIND', 'EINDE', 'END' ) )
  THEN
    -- rond alleen de laatste rij af!
    OPEN  inve_cur;
    FETCH inve_cur
    INTO  inve_rec;
    UPDATE kgc_geinstalleerde_versies
    SET    afgerond = 'J'
    ,      datum = v_datum
    ,      username = v_user
    WHERE CURRENT OF inve_cur;
    COMMIT;
    CLOSE inve_cur;
  ELSIF ( v_actie IN ( 'CONTROLE', 'CHECK', 'TEST' ) )
  THEN
    -- is vorige versie geinstalleerd?
    inve_rec.afgerond := 'N';
    OPEN  inve_cur;
    FETCH inve_cur
    INTO  inve_rec;
    CLOSE inve_cur;
    IF ( inve_rec.afgerond = 'J' )
    THEN
      NULL;
    ELSE
      raise_application_error( -20000, 'Versie '||p_versie||' is nog niet geinstalleerd!' );
    END IF;
  END IF;
END kgc_installatie;

/

/
QUIT
