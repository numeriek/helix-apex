CREATE MATERIALIZED VIEW "HELIX"."BEH_FLOW_DNA_MACHINES" ("MACH_ID", "CODE", "TYPE", "DESCRIPTION", "OBSOLETE")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXNOLOG"
  BUILD IMMEDIATE
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXNOLOG"
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT sysdate + (1/240)
  WITH PRIMARY KEY USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT "MACHINES"."MACH_ID" "MACH_ID",
       "MACHINES"."CODE" "CODE",
       "MACHINES"."TYPE" "TYPE",
       "MACHINES"."DESCRIPTION" "DESCRIPTION",
       "MACHINES"."OBSOLETE" "OBSOLETE"
  FROM "MACHINES"@STORAGE.UMCN.NL "MACHINES";

/
QUIT
