CREATE MATERIALIZED VIEW "HELIX"."BEH_FLOW_METAB_STAT_AFG" ("VERBORGEN_KLEUR", "VERBORGEN_SORTERING", "VERBORGEN_TELLING", "JAAR", "JAAR_KWARTAAL", "JAAR_MAAND", "WEEK", "GROEP", "ONDE_TYPE", "WIJZE", "AANTAL_ONDE", "GEM_TAT", "GEM_VERWACHTE_DOORLOOPTIJD", "GEM_AFWIJKING_DOORLOOPTIJDEN")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"
  BUILD IMMEDIATE
  USING INDEX
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT trunc(next_day(sysdate,'ZON'))+23/24+ 55/24/60
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS ENABLE QUERY REWRITE
  AS SELECT '#FFFFFF' AS VERBORGEN_KLEUR,
         1 AS VERBORGEN_SORTERING,
         ' ' AS VERBORGEN_TELLING,
         TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY') AS JAAR,
         TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-Q') AS JAAR_KWARTAAL,
         TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM') AS JAAR_MAAND,
         TO_CHAR (ONDE.DATUM_AUTORISATIE, 'IW') AS WEEK,
         ONGR.CODE AS GROEP,
         --INGR.CODE AS INDIGROEP, --SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3) AS INDIGROEP,
         DECODE (ONDE.ONDERZOEKSTYPE,
                 'R', 'Research',
                 'D', 'Diagnostiek',
                 'C', 'Controle',
                 '')
            AS ONDE_TYPE,
         ONWY.OMSCHRIJVING AS WIJZE,
         --INTE.CODE AS INDICODE,
         COUNT (ONDE.ONDERZOEKNR) AS AANTAL_ONDE,
         AVG (ONDE.DATUM_AUTORISATIE - ONDE.DATUM_BINNEN) AS GEM_TAT,
         AVG (ONDE.GEPLANDE_EINDDATUM - ONDE.DATUM_BINNEN)
            AS GEM_VERWACHTE_DOORLOOPTIJD,
         COUNT (ONDE.ONDERZOEKNR)
         * (AVG (ONDE.DATUM_AUTORISATIE - ONDE.DATUM_BINNEN))
            AS GEM_AFWIJKING_DOORLOOPTIJDEN
    FROM kgc_onderzoeken onde,
         kgc_onderzoek_indicaties onin,
         kgc_onderzoekswijzen onwy,
         kgc_kgc_afdelingen kafd,
         kgc_onderzoeksgroepen ongr,
         kgc_indicatie_teksten inte,
         kgc_indicatie_groepen ingr
   WHERE     ONDE.KAFD_ID = 6
         AND KAFD.KAFD_ID = ONDE.KAFD_ID
         AND ONDE.KAFD_ID = ONWY.KAFD_ID(+)
         AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE(+)
         AND ONGR.ONGR_ID = ONDE.ONGR_ID
         AND ONDE.ONDE_ID = ONIN.ONDE_ID
         AND ONIN.INDI_ID = INTE.INDI_ID
         AND INTE.INGR_ID = INGR.INGR_ID
         AND ONDE.AFGEROND = 'J'
         AND TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY') > 2007
GROUP BY TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY'),
         TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-Q'),
         TO_CHAR (ONDE.DATUM_AUTORISATIE, 'YYYY-MM'),
         TO_CHAR (ONDE.DATUM_AUTORISATIE, 'IW'),
         ONGR.CODE,
         --INTE.CODE,
         --INGR.CODE,       --SUBSTR (INGR.CODE, INSTR (INGR.CODE, '_') + 1, 3),
         ONWY.OMSCHRIJVING,
         ONDE.ONDERZOEKSTYPE;

/
QUIT
