CREATE MATERIALIZED VIEW "HELIX"."BEH_FLOW_NGS_EXA_S_MDET" ("ONDE_ID", "ONDERZOEKNR", "MEET_ID", "'Data gedeeld'", "'1e interpretatie klaar'", "BEVESTIGEN")
  ORGANIZATION HEAP PCTFREE 0 PCTUSED 0 INITRANS 2 MAXTRANS 255
 COMPRESS BASIC LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"
  BUILD IMMEDIATE
  USING INDEX
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT trunc(sysdate) + 1
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT "ONDE_ID",
       "ONDERZOEKNR",
       "MEET_ID",
       "'Data gedeeld'",
       "'1e interpretatie klaar'",
       CASE
          WHEN    UPPER ("'Bevestigen'") IS NULL
               OR UPPER ("'Bevestigen'") = 'N'
               OR UPPER ("'Bevestigen'") = 'NEE'
          THEN
             'N'
          ELSE
             'J'
       END
          AS Bevestigen
  FROM (SELECT mdet.meet_id,
               ONDE.ONDE_ID,
               ONDE.ONDERZOEKNR,
               PROMPT,
               WAARDE
          FROM kgc_onderzoeken onde,
               bas_metingen meti,
               bas_meetwaarden meet,
               bas_meetwaarde_details mdet,
               kgc_onderzoeksgroepen ongr,
               kgc_meetwaardestructuur mwst
         WHERE     ONDE.ONDE_ID = METI.ONDE_ID
               AND METI.METI_ID = MEET.METI_ID
               AND MEET.MEET_ID = MDET.MEET_ID
               AND ONDE.ONGR_ID = ONGR.ONGR_ID
               AND MEET.MWST_ID = MWST.MWST_ID
               AND MDET.WAARDE IS NOT NULL
               AND MEET.MEET_REKEN = 'S'
               AND MWST.CODE = 'EXA_S'
               AND ONGR.CODE = 'NGS') PIVOT (MAX (WAARDE)
                                      FOR (PROMPT)
                                      IN  ('Data gedeeld',
                                          '1e interpretatie klaar',
                                          'Bevestigen'));

/
QUIT
