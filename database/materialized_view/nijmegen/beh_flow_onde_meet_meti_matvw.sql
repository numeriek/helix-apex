CREATE MATERIALIZED VIEW "HELIX"."BEH_FLOW_ONDE_MEET_METI_MATVW" ("ONDE_ID", "TOTAAL_METI", "METI_AF", "METI_NIET_AF", "TOTAAL_MEET", "MEET_AF", "MEET_NIET_AF", "NCONT", "WCONT", "NCONT_V", "NCONT_S", "NCONT_M", "WCONT_V", "WCONT_S", "WCONT_M", "WLST_AF_WCONT", "WLST_AF_NCONT_V", "WLST_AF_NCONT_S", "WLST_AF_NCONT_M", "WLST_NIET_AF", "NIET_OP_WLST_AF", "NIET_OP_WLST_NIET_AF")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"
  BUILD IMMEDIATE
  USING INDEX
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT sysdate + (1/24/12)
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS WITH with_kgc_onderzoeken AS (SELECT onde_id
                                   FROM kgc_onderzoeken
                                  WHERE afgerond = 'N'),
        with_bas_metingen
           AS (SELECT onde.onde_id, meti.meti_id, meti.afgerond
                 FROM with_kgc_onderzoeken onde, bas_metingen meti
                WHERE onde.onde_id = meti.onde_id),
        with_bas_meetwaarden
           AS (SELECT meti.onde_id,
                      meet.meet_id,
                      meti.afgerond meti_afgerond,
                      meet.afgerond meet_afgerond,
                      meet.meet_reken,
                      meet.mest_id,
                      WLIT.WLST_ID,
                      WLIT.AFGEROND wlit_afgerond
                 FROM with_bas_metingen meti, bas_meetwaarden meet, kgc_werklijst_items wlit
                WHERE meti.meti_id = meet.meti_id
                    and MEET.MEET_ID = WLIT.MEET_ID (+)
                ),
        with_bas_meetwaarden_1
           AS (SELECT onde_id,
                      meet_id,
                      CASE WHEN METI_AFGEROND = 'J' THEN 'J' ELSE MEET_AFGEROND END AFGEROND
                 FROM with_bas_meetwaarden),
    with_bas_meetwaarden_2
           AS (SELECT onde_id,
                      meet_id,
                      CASE
                         WHEN METI_AFGEROND = 'J'
                         THEN
                            'WCONT'
                         ELSE
                            CASE
                               WHEN MEET_REKEN = 'V' AND MEST_ID IS NOT NULL
                               THEN
                                  'WCONT_V'
                               ELSE
                                  CASE
                                     WHEN MEET_REKEN = 'S'
                                          AND MEST_ID IS NOT NULL
                                     THEN
                                        'WCONT_S'
                                     ELSE
                                        CASE
                                           WHEN MEET_REKEN = 'M'
                                                AND MEST_ID IS NOT NULL
                                           THEN
                                              'WCONT_M'
                                           ELSE
                                              CASE
                                                 WHEN MEET_REKEN = 'V'
                                                      AND MEST_ID IS NULL
                                                 THEN
                                                    'NCONT_V'
                                                 ELSE
                                                    CASE
                                                       WHEN MEET_REKEN = 'S'
                                                            AND MEST_ID
                                                                   IS NULL
                                                       THEN
                                                          'NCONT_S'
                                                       ELSE
                                                          CASE
                                                             WHEN MEET_REKEN =
                                                                     'M'
                                                                  AND MEST_ID
                                                                         IS NULL
                                                             THEN
                                                                'NCONT_M'
                                                          END
                                                    END
                                              END
                                        END
                                  END
                            END
                      END
                         AS CONT
                 FROM with_bas_meetwaarden),
        with_bas_meetwaarden_3
            AS (SELECT meet.onde_id,
                                    meet.meet_id,
                                  CASE WHEN meet.wlst_id is null and meet.meet_afgerond = 'J' THEN 'NIET_OP_WLST_AF'
                                    ELSE CASE WHEN meet.wlst_id is null and meet.meet_afgerond = 'N' THEN 'NIET_OP_WLST_NIET_AF'
                                        ELSE CASE WHEN meet.meet_afgerond = 'J' and  meet.mest_id is not null THEN 'WLST_AF_WCONT'
                                            ELSE CASE WHEN meet.meet_afgerond = 'J' and meet.mest_id is null and meet.meet_reken = 'V' THEN 'WLST_AF_NCONT_V'
                                                ELSE CASE WHEN meet.meet_afgerond = 'J' and meet.mest_id is null and meet.meet_reken = 'S' THEN 'WLST_AF_NCONT_S'
                                                    ELSE CASE WHEN meet.meet_afgerond = 'J' and meet.mest_id is null and meet.meet_reken = 'M' THEN 'WLST_AF_NCONT_M'
                                                        ELSE 'WLST_NIET_AF'
                                                    END
                                                END
                                            END
                                        END
                                    END
                                  END AFGEROND
                             FROM with_bas_meetwaarden meet)
   SELECT SQLMETI.ONDE_ID,
          SQLMETI.METI_AF + SQLMETI.METI_NIET_AF AS TOTAAL_METI,
          SQLMETI.METI_AF,
          SQLMETI.METI_NIET_AF,
          SQLMEET.MEET_AF + SQLMEET.MEET_NIET_AF AS TOTAAL_MEET,
          SQLMEET.MEET_AF,
          SQLMEET.MEET_NIET_AF,
          SQLCONT.NCONT_V + SQLCONT.NCONT_S + SQLCONT.NCONT_M AS NCONT,
          SQLCONT.WCONT + SQLCONT.WCONT_V + SQLCONT.WCONT_S + SQLCONT.WCONT_M AS WCONT,
          SQLCONT.NCONT_V,
          SQLCONT.NCONT_S,
          SQLCONT.NCONT_M,
          SQLCONT.WCONT_V,
          SQLCONT.WCONT_S,
          SQLCONT.WCONT_M,
          SQLWLIT.WLST_AF_WCONT,
          SQLWLIT.WLST_AF_NCONT_V,
          SQLWLIT.WLST_AF_NCONT_S,
          SQLWLIT.WLST_AF_NCONT_M,
          SQLWLIT.WLST_NIET_AF,
          SQLWLIT.NIET_OP_WLST_AF,
          SQLWLIT.NIET_OP_WLST_NIET_AF
     FROM (SELECT *
             FROM with_bas_metingen PIVOT (COUNT (meti_id)
                                    FOR AFGEROND
                                    IN ('J' AS METI_AF, 'N' AS METI_NIET_AF))) SQLMETI,
          (SELECT *
             FROM with_bas_meetwaarden_1 PIVOT (COUNT (meet_id)
                                         FOR AFGEROND
                                         IN  ('J' AS MEET_AF,
                                             'N' AS MEET_NIET_AF))) SQLMEET,
          (SELECT *
             FROM with_bas_meetwaarden_2 PIVOT (COUNT (meet_id)
                                         FOR CONT
                                         IN  ('NCONT_V' AS NCONT_V,
                                             'NCONT_S' AS NCONT_S,
                                             'NCONT_M' AS NCONT_M,
                                             'WCONT' AS WCONT,
                                             'WCONT_V' AS WCONT_V,
                                             'WCONT_S' AS WCONT_S,
                                             'WCONT_M' AS WCONT_M))) SQLCONT,
          (SELECT *
             FROM with_bas_meetwaarden_3 PIVOT (COUNT (meet_id)
                                         FOR AFGEROND
                                         IN  ('WLST_AF_WCONT' AS WLST_AF_WCONT,
                                                'WLST_AF_NCONT_V' AS WLST_AF_NCONT_V,
                                                'WLST_AF_NCONT_S' AS WLST_AF_NCONT_S,
                                                'WLST_AF_NCONT_M' AS WLST_AF_NCONT_M,
                                                'WLST_NIET_AF' AS WLST_NIET_AF,
                                                'NIET_OP_WLST_AF' AS NIET_OP_WLST_AF,
                                                'NIET_OP_WLST_NIET_AF' AS NIET_OP_WLST_NIET_AF))) SQLWLIT
    WHERE SQLMETI.onde_id = SQLMEET.onde_id
          AND SQLMETI.onde_id = SQLCONT.onde_id
          and SQLMETI.onde_id = SQLWLIT.onde_id;

/
QUIT
