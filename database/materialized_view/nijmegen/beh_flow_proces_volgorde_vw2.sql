CREATE MATERIALIZED VIEW "HELIX"."BEH_FLOW_PROCES_VOLGORDE_VW2" ("FLOW_ID", "PADNR", "FLDE_ID_VOORG_STAP", "FLDE_ID_VOLG_STAP", "PADNR_PLUS_FLDE_ID_VOORG_STAP", "PADNR_PLUS_FLDE_ID_VOLG_STAP")
  ORGANIZATION HEAP PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"
  BUILD IMMEDIATE
  USING INDEX
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT sysdate + (1/240)
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS WITH t AS (SELECT flow_id,
                  padnr,
                  flde_id_voorg_stap,
                  flde_id_volg_stap
             FROM (SELECT flow_id,
                          ROW_NUMBER ()
                             OVER (PARTITION BY flow_id ORDER BY flow_id)
                             padnr,
                          SUBSTR (pad, 2) flde_id_voorg_stap,
                          pad flde_id_volg_stap
                     FROM (           SELECT flow.flow_id,
                                             SYS_CONNECT_BY_PATH (
                                                flpv.flde_id_voorg_stap,
                                                ',')
                                                pad,
                                             CONNECT_BY_ISLEAF
                                        FROM beh_flows flow,
                                             beh_flow_details flde,
                                             beh_flow_proces_volgorde flpv,
                                             kgc_kgc_afdelingen kafd
                                       WHERE     flow.flow_id = flde.flow_id
                                             AND flde.flde_id = flpv.flde_id_voorg_stap
                                             AND flow.kafd_id = kafd.kafd_id
                                             AND flow.vervallen = 'N'
                                             AND flde.vervallen = 'N'
                                             AND UPPER (kafd.code) NOT LIKE
                                                    UPPER ('%VERVALLEN%')
                                             AND CONNECT_BY_ISLEAF = 1
                                  START WITH flde_id_volg_stap IS NULL
                                  CONNECT BY PRIOR flpv.flde_id_voorg_stap =
                                                flpv.flde_id_volg_stap
                           ORDER SIBLINGS BY flpv.flde_id_volg_stap)))
    SELECT flow_id,
           padnr,
           SUBSTR (flde_id_voorg_stap,
                   INSTR (',' || flde_id_voorg_stap,
                          ',',
                          1,
                          LEVEL),
                   INSTR (flde_id_voorg_stap || ',',
                          ',',
                          1,
                          LEVEL)
                   - INSTR (',' || flde_id_voorg_stap,
                            ',',
                            1,
                            LEVEL))
              flde_id_voorg_stap,
           SUBSTR (flde_id_volg_stap,
                   INSTR (',' || flde_id_volg_stap,
                          ',',
                          1,
                          LEVEL),
                   INSTR (flde_id_volg_stap || ',',
                          ',',
                          1,
                          LEVEL)
                   - INSTR (',' || flde_id_volg_stap,
                            ',',
                            1,
                            LEVEL))
              flde_id_volg_stap,
           DECODE (
              SUBSTR (flde_id_voorg_stap,
                      INSTR (',' || flde_id_voorg_stap,
                             ',',
                             1,
                             LEVEL),
                      INSTR (flde_id_voorg_stap || ',',
                             ',',
                             1,
                             LEVEL)
                      - INSTR (',' || flde_id_voorg_stap,
                               ',',
                               1,
                               LEVEL)),
              NULL, NULL,
              TO_CHAR (padnr) || '-'
              || SUBSTR (flde_id_voorg_stap,
                         INSTR (',' || flde_id_voorg_stap,
                                ',',
                                1,
                                LEVEL),
                         INSTR (flde_id_voorg_stap || ',',
                                ',',
                                1,
                                LEVEL)
                         - INSTR (',' || flde_id_voorg_stap,
                                  ',',
                                  1,
                                  LEVEL)))
              padnr_plus_flde_id_voorg_stap,
           DECODE (
              SUBSTR (flde_id_volg_stap,
                      INSTR (',' || flde_id_volg_stap,
                             ',',
                             1,
                             LEVEL),
                      INSTR (flde_id_volg_stap || ',',
                             ',',
                             1,
                             LEVEL)
                      - INSTR (',' || flde_id_volg_stap,
                               ',',
                               1,
                               LEVEL)),
              NULL, NULL,
              TO_CHAR (padnr) || '-'
              || SUBSTR (flde_id_volg_stap,
                         INSTR (',' || flde_id_volg_stap,
                                ',',
                                1,
                                LEVEL),
                         INSTR (flde_id_volg_stap || ',',
                                ',',
                                1,
                                LEVEL)
                         - INSTR (',' || flde_id_volg_stap,
                                  ',',
                                  1,
                                  LEVEL)))
              padnr_plus_flde_id_volg_stap
      FROM t
CONNECT BY LEVEL <=
                LENGTH (flde_id_volg_stap)
              - LENGTH (REPLACE (flde_id_volg_stap, ','))
              + 1
           AND PRIOR flde_id_volg_stap = flde_id_volg_stap
           AND PRIOR DBMS_RANDOM.VALUE IS NOT NULL;

/
QUIT
