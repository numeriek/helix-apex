CREATE MATERIALIZED VIEW "HELIX"."BEH_FLOW_NGS_LIBRARY_MDET" ("ONDE_ID", "ONDERZOEKNR", "MEET_ID", "'Sequencing'", "'Faciliteit'", "'Status'", "'Data beschikbaar'", "'Data opgehaald'", "'Data geback-upt'")
  ORGANIZATION HEAP PCTFREE 0 PCTUSED 0 INITRANS 2 MAXTRANS 255
 COMPRESS BASIC LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"
  BUILD IMMEDIATE
  USING INDEX
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT trunc(sysdate) + 1
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT *
  FROM (SELECT ONDE.ONDE_ID,
               ONDE.ONDERZOEKNR,
               mdet.meet_id,
               PROMPT,
               WAARDE
          FROM kgc_onderzoeken onde,
               bas_metingen meti,
               bas_meetwaarden meet,
               bas_meetwaarde_details mdet,
               kgc_onderzoeksgroepen ongr,
               kgc_meetwaardestructuur mwst
         WHERE     ONDE.ONDE_ID = METI.ONDE_ID
               AND METI.METI_ID = MEET.METI_ID
               AND MEET.MEET_ID = MDET.MEET_ID
               AND ONDE.ONGR_ID = ONGR.ONGR_ID
               AND MEET.MWST_ID = MWST.MWST_ID
               AND MDET.WAARDE IS NOT NULL
               AND MWST.CODE = 'LIB_ST2') PIVOT (MAX (WAARDE)
                                          FOR (PROMPT)
                                          IN  ('Sequencing',
                                              'Faciliteit',
                                              'Status',
                                              'Data beschikbaar',
                                              'Data opgehaald',
                                              'Data geback-upt'));

/
QUIT
