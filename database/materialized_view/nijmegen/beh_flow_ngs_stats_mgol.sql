CREATE MATERIALIZED VIEW "HELIX"."BEH_FLOW_NGS_STATS_MGOL" ("ONDE_ID", "ONDERZOEKNR", "AFGEROND", "BINNEN_OP", "GOEDGEKEURD_OP", "WERKLIJST_OP", "STOFTESTGROEP", "SEQUENCING", "SEQUENCING_ORIG", "DATA_BESCHIKBAAR", "DATA_BESCHIKBAAR_ORIG", "DATA_GEDEELD", "DATA_GEDEELD_ORIG", "E_INTERPRETATIE_KLAAR", "E_INTERPRETATIE_KLAAR_ORIG", "UITSLAG_DAT", "AUTORISATIE_DAT")
  ORGANIZATION HEAP PCTFREE 0 PCTUSED 0 INITRANS 2 MAXTRANS 255
 COMPRESS BASIC LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"
  BUILD IMMEDIATE
  USING INDEX
  REFRESH FORCE ON DEMAND START WITH sysdate+0 NEXT trunc(sysdate) + 1
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT NGSO.ONDE_ID,
       NGSO.ONDERZOEKNR,
       NGSO.AFGEROND,
       NGSO.BINNEN_OP,
       NGSO.GOEDGEKEURD_OP,
       NGSO.WERKLIJST_OP,
       NGSM.STOFTESTGROEP,
       NGSM.SEQUENCING,
       NGSM.SEQUENCING_ORIG,
       NGSM.DATA_BESCHIKBAAR,
       NGSM.DATA_BESCHIKBAAR_ORIG,
       NGSM.DATA_GEDEELD,
       NGSM.DATA_GEDEELD_ORIG,
       NGSM.E_INTERPRETATIE_KLAAR,
       NGSM.E_INTERPRETATIE_KLAAR_ORIG,
       NGSO.UITSLAG_DAT,
       NGSO.AUTORISATIE_DAT
  FROM (SELECT ONDE.ONDE_ID,
               ONDE.ONDERZOEKNR,
               ONDE.AFGEROND,
               CASE
                  WHEN TRUNC (ONDE.CREATION_DATE) <=
                          TRUNC (ONDE.DATUM_BINNEN)
                  THEN
                     ONDE.CREATION_DATE
                  ELSE
                     ONDE.DATUM_BINNEN
               END
                  AS BINNEN_OP,
               AULO.GOEDGEKEURD_OP,
               WLSTDAT.WERKLIJST_OP,
               UITSDAT.UITSLAG_DAT,
               OAHIDAT.AUTORISATIE_DAT
          FROM kgc_onderzoeken onde,
               kgc_onderzoeksgroepen ongr,
               (  SELECT AULO.ID AS ONDE_ID,
                         MIN (AULO.DATUM_TIJD) AS GOEDGEKEURD_OP
                    FROM kgc_audit_log aulo
                   WHERE AULO.KOLOM_NAAM = 'STATUS' AND AULO.WAARDE_NIEUW = 'G'
                GROUP BY aulo.id) AULO,
               (  SELECT MIN (WLST.CREATION_DATE) AS WERKLIJST_OP, METI.ONDE_ID
                    FROM bas_metingen meti,
                         kgc_werklijst_items wlit,
                         kgc_werklijsten wlst
                   WHERE     METI.METI_ID = WLIT.METI_ID
                         AND WLIT.WLST_ID = WLST.WLST_ID
                         AND METI.CREATION_DATE <= WLST.CREATION_DATE
                GROUP BY METI.ONDE_ID) WLSTDAT,
               (  SELECT MIN (UITS.CREATION_DATE) AS UITSLAG_DAT,
                         UITS.ONDE_ID,
                         MIN (UITS.DATUM_MEDE_ID) AS DATUM_MEDE_ID
                    FROM kgc_uitslagen uits
                GROUP BY UITS.ONDE_ID) UITSDAT,
               (  SELECT MIN (OAHI.CREATION_DATE) AS AUTORISATIE_DAT,
                         OAHI.ONDE_ID
                    FROM kgc_onde_autorisatie_historie oahi
                GROUP BY OAHI.ONDE_ID) OAHIDAT
         WHERE     ONDE.ONGR_ID = ONGR.ONGR_ID
               AND ONDE.ONDE_ID = AULO.ONDE_ID(+)
               AND ONDE.ONDE_ID = WLSTDAT.ONDE_ID(+)
               AND ONDE.ONDE_ID = UITSDAT.ONDE_ID(+)
               AND ONDE.ONDE_ID = OAHIDAT.ONDE_ID(+)
               AND ONGR.CODE = 'NGS') NGSO,
       (SELECT CASE
                  WHEN LIBPIP.ONDE_ID IS NOT NULL THEN LIBPIP.ONDE_ID
                  ELSE EXA.ONDE_ID
               END
                  AS ONDE_ID,
               CASE
                  WHEN LIBPIP.ONDERZOEKNR IS NOT NULL THEN LIBPIP.ONDERZOEKNR
                  ELSE EXA.ONDERZOEKNR
               END
                  AS ONDERZOEKNR,
               CASE
                  WHEN EXA.STOFTESTGROEP IS NULL THEN LIBPIP.STOFTESTGROEP
                  ELSE EXA.STOFTESTGROEP
               END
                  AS STOFTESTGROEP,
               CASE WHEN TRANSLATE(LIBPIP.SEQUENCING, '012345678', '999999999') = '99-99-9999 99:99:99' THEN LIBPIP.SEQUENCING ELSE CASE WHEN TRANSLATE(LIBPIP.SEQUENCING, '012345678', '999999999') = '99-99-9999 99:99' THEN LIBPIP.SEQUENCING ELSE CASE WHEN TRANSLATE(LIBPIP.SEQUENCING, '012345678', '999999999') = '99-99-9999' THEN LIBPIP.SEQUENCING ELSE NULL END END END as SEQUENCING,
               LIBPIP.SEQUENCING as SEQUENCING_ORIG,
               CASE WHEN TRANSLATE(LIBPIP.DATA_BESCHIKBAAR, '012345678', '999999999') = '99-99-9999 99:99:99' THEN LIBPIP.DATA_BESCHIKBAAR ELSE CASE WHEN TRANSLATE(LIBPIP.DATA_BESCHIKBAAR, '012345678', '999999999') = '99-99-9999 99:99' THEN LIBPIP.DATA_BESCHIKBAAR ELSE CASE WHEN TRANSLATE(LIBPIP.DATA_BESCHIKBAAR, '012345678', '999999999') = '99-99-9999' THEN LIBPIP.DATA_BESCHIKBAAR ELSE NULL END END END as DATA_BESCHIKBAAR,
               LIBPIP.DATA_BESCHIKBAAR as DATA_BESCHIKBAAR_ORIG,
               CASE WHEN TRANSLATE(EXA.DATA_GEDEELD, '012345678', '999999999') = '99-99-9999 99:99:99' THEN EXA.DATA_GEDEELD ELSE CASE WHEN TRANSLATE(EXA.DATA_GEDEELD, '012345678', '999999999') = '99-99-9999 99:99' THEN EXA.DATA_GEDEELD ELSE CASE WHEN TRANSLATE(EXA.DATA_GEDEELD, '012345678', '999999999') = '99-99-9999' THEN EXA.DATA_GEDEELD ELSE NULL END END END as DATA_GEDEELD,
               EXA.DATA_GEDEELD as DATA_GEDEELD_ORIG,
               CASE WHEN TRANSLATE(EXA.E_INTERPRETATIE_KLAAR, '012345678', '999999999') = '99-99-9999 99:99:99' THEN EXA.E_INTERPRETATIE_KLAAR ELSE CASE WHEN TRANSLATE(EXA.E_INTERPRETATIE_KLAAR, '012345678', '999999999') = '99-99-9999 99:99' THEN EXA.E_INTERPRETATIE_KLAAR ELSE CASE WHEN TRANSLATE(EXA.E_INTERPRETATIE_KLAAR, '012345678', '999999999') = '99-99-9999' THEN EXA.E_INTERPRETATIE_KLAAR ELSE NULL END END END as E_INTERPRETATIE_KLAAR,
               EXA.E_INTERPRETATIE_KLAAR as E_INTERPRETATIE_KLAAR_ORIG
          FROM    (SELECT ONDE.ONDE_ID,
                          ONDE.ONDERZOEKNR,
                          'ALLEEN_LIBRARY_PIPELINE' AS STOFTESTGROEP,
                          PROMPT,
                          WAARDE
                     FROM kgc_onderzoeken onde,
                          bas_metingen meti,
                          bas_meetwaarden meet,
                          bas_meetwaarde_details mdet,
                          kgc_onderzoeksgroepen ongr,
                          kgc_meetwaardestructuur mwst
                    WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                          AND METI.METI_ID = MEET.METI_ID
                          AND MEET.MEET_ID = MDET.MEET_ID
                          AND ONDE.ONGR_ID = ONGR.ONGR_ID
                          AND MEET.MWST_ID = MWST.MWST_ID
                          AND (MWST.CODE = 'LIB_ST2'
                               OR MWST.CODE = 'EXA_PIPE')) PIVOT (MAX (
                                                                     WAARDE)
                                                           FOR (PROMPT)
                                                           IN  ('Sequencing' AS "SEQUENCING",
                                                               'Data beschikbaar' AS "DATA_BESCHIKBAAR")) LIBPIP
               FULL OUTER JOIN
                  (SELECT ONDE.ONDE_ID,
                          ONDE.ONDERZOEKNR,
                          STGR.CODE AS STOFTESTGROEP,
                          PROMPT,
                          WAARDE
                     FROM kgc_onderzoeken onde,
                          bas_metingen meti,
                          bas_meetwaarden meet,
                          bas_meetwaarde_details mdet,
                          kgc_stoftestgroepen stgr,
                          kgc_onderzoeksgroepen ongr,
                          kgc_meetwaardestructuur mwst
                    WHERE     ONDE.ONDE_ID = METI.ONDE_ID
                          AND METI.METI_ID = MEET.METI_ID
                          AND METI.STGR_ID = STGR.STGR_ID
                          AND MEET.MEET_ID = MDET.MEET_ID
                          AND ONDE.ONGR_ID = ONGR.ONGR_ID
                          AND MEET.MWST_ID = MWST.MWST_ID
                          AND ONGR.CODE = 'NGS'
                          AND MWST.CODE = 'EXA_S') PIVOT (MAX (WAARDE)
                                                   FOR (PROMPT)
                                                   IN  ('Data gedeeld' AS "DATA_GEDEELD",
                                                       '1e interpretatie klaar' AS "E_INTERPRETATIE_KLAAR")) EXA
               ON LIBPIP.ONDE_ID = EXA.ONDE_ID-- WHERE                                     -- LIBPIP.ONDE_ID = EXA.ONDE_ID (+)
                                              --       (libpip.onderzoeknr IN
                                              --           ('R14-15398',
                                              --            'R14-21688',
                                              --            'R15-11386',
                                              --            'R15-21579',
                                              --            'R15-20759',
                                              --            'R15-20506',
                                              --            'R14-16377')
                                              --        OR exa.onderzoeknr IN
                                              --              ('R14-15398',
                                              --               'R14-21688',
                                              --               'R15-11386',
                                              --               'R15-21579',
                                              --               'R15-20759',
                                              --               'R15-20506',
                                              --               'R14-16377'))
       ) NGSM
 WHERE ngso.onde_id = ngsm.onde_id;

/
QUIT
