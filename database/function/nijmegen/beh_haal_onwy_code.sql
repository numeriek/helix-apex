CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_ONWY_CODE" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       BEH_HAAL_ONWY_CODE
   PURPOSE:  ophalen van onderzoekwijze code op basis van onde_id om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2015  Marzena Golebiowska 1. Created this function.

******************************************************************************/

v_onwy varchar2(4000);

cursor c_onwy is
    select ONWY.OMSCHRIJVING
    from kgc_onderzoeken onde,
            kgc_onderzoekswijzen onwy
    where ONDE.KAFD_ID = ONWY.KAFD_ID
            AND ONDE.ONDERZOEKSWIJZE = ONWY.CODE
            AND ONDE.ONDE_ID = p_onde_id
    ;

BEGIN
    open c_onwy;
    fetch c_onwy into v_onwy;
        if c_onwy%found then
            v_onwy := v_onwy;
        end if;
    close c_onwy;
    RETURN v_onwy;
END;
/

/
QUIT
