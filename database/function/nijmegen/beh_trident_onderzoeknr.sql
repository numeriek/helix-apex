CREATE OR REPLACE FUNCTION "HELIX"."BEH_TRIDENT_ONDERZOEKNR" (p_onde_id IN NUMBER) RETURN VARCHAR2 IS
    v_tekst VARCHAR2(20):= null ;
----------------------------------------------------------------
-- doel:         ophalen trident onderzoeknr
-- gebruik:      om te gebruiken in de uitslag brief van prenataal
-- gemaakt:     CV, 18-04-2014
---------------------------------------------------------------
    CURSOR reon_cur IS
    --    SELECT onty.omschrijving||' '||nvl( onde.onderzoeknr, externe_onderzoeknr ) onderzoek --
      SELECT '(TRIDENT '||reon.externe_onderzoeknr||')' TRIDENTonderzoeknr
      FROM   kgc_onderzoekrelatie_types   onty
            ,kgc_gerelateerde_onderzoeken reon
      WHERE  reon.onty_id = onty.onty_id
      and onty.code = 'STUDIE'
      and upper(REON.OPMERKING) like '%TRIDENT%'
      AND    reon.onde_id = p_onde_id
      ;

  BEGIN
    FOR r IN reon_cur
    LOOP
      IF (v_tekst IS NULL)
      THEN
        v_tekst := r.tridentonderzoeknr;
      END IF;
    END LOOP;
    RETURN(v_tekst);
  END;
/

/
QUIT
