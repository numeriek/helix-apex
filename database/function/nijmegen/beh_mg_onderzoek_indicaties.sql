CREATE OR REPLACE FUNCTION "HELIX"."BEH_MG_ONDERZOEK_INDICATIES" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_mg_onderzoek_indicaties
   PURPOSE:    onderzoeksindicatie in een tekst string zetten bij een onde_id
               tegelijk onderscheid maken in proband en aangedaan zoals dat gebruikt wordt bij moleculaire genetica
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        29-12-2009  Cootje Vermaat      1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_mg_onderzoek_indicaties
      Sysdate:         29-12-20097
      Date and Time:   29-12-2009 12:10:02,
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  cursor c_indi is
    select indi.code
    from   kgc_onderzoek_indicaties onin
    ,      kgc_indicatie_teksten indi
    where  onin.onde_id = p_onde_id
    and    onin.indi_id = indi.indi_id
    ;

  r_indi c_indi%ROWTYPE;
  v_indi      varchar2(100);
  v_proband   varchar2(10)   := null;
  v_aangedaan varchar2(10)   := null;

BEGIN
  v_indi := null;
  OPEN c_indi;
  FETCH C_indi INTO r_indi;
  WHILE c_indi%FOUND
  LOOP
    IF R_indi.code in ('P', 'R', 'C')
    THEN
      v_aangedaan := r_indi.code;
    ELSIF R_indi.code in ('A', 'A?', 'D','NA','NA?')
    THEN
      v_proband := r_indi.code;
    ELSIF v_indi is null
    THEN
      v_indi := r_indi.code;
    ELSE
      v_indi := v_indi || ', ' || r_indi.code;
    END IF;
    FETCH C_indi INTO r_indi;
  END LOOP;
  CLOSE c_indi;
  v_indi := v_indi||';'||v_aangedaan||';'||v_proband;
  RETURN v_indi;
END;

/

/
QUIT
