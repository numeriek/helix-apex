CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_WPD_GRP"
( p_onde_id in  number
)/******************************************************************************
   NAME: BEH_onde_wpd_grp
   PURPOSE:   bepaal in welke WPD groep de indicati evan het onderzoek valt
            om te gebruiken in het prenatale wpd jaarverslag
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        27-dec-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);

  cursor c_lft is
  select kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN', 'WPD_CAT_JV',onin.indi_id) waarde
     from KGC_ONDERZOEK_INDICATIES onin
     ,    kgc_indicatie_teksten indi
     where onin.indi_id = indi.indi_id
     and onin.onde_id = p_onde_id
     and kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN', 'WPD_CAT_JV',onin.indi_id) <> 'LEEFTIJD'
     order by to_number(nvl(volgorde, onin.onin_id))
   ;
  r_lft c_lft%rowtype;


begin
  v_return := null;
  OPEN  c_lft;
  fetch c_lft into r_lft;
  IF  c_lft%found
  THEN
    v_return := r_lft.waarde ;
  end if;
  Close c_lft;

  RETURN( v_return );

  EXCEPTION
      WHEN others
      THEN
        v_return := null;
      RETURN( v_return );
END;
/

/
QUIT
