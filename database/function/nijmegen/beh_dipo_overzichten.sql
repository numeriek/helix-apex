CREATE OR REPLACE FUNCTION "HELIX"."BEH_DIPO_OVERZICHTEN"
(p_dagen_terug in number, x_msg in out varchar2)
return boolean as
v_conn UTL_SMTP.connection;
v_smtp_host kgc_systeem_parameters.standaard_waarde%type :=
            kgc_sypa_00.systeem_waarde ('MAIL_SERVER');

v_verzender_email varchar2(100) := 'cukz124@umcn.nl';
--v_ontvanger_email varchar2(1000):= 'dna@umcn.nl,cukz124@umcn.nl';
v_ontvanger_email varchar2(1000):= 'h.bourchi@sb.umcn.nl';

v_subject varchar2(500) := 'SMS overzichten';

v_boundary varchar2(20) := 'KGCNNIJMEGENBOUND';

cursor c_rcpt is
select *
from table(cast(beh_alfu_00.explode(',', v_ontvanger_email) as beh_listtable))
;

cursor c_beri is
select
    pers.aanspreken
,    onde.onderzoeknr
,     to_char(beri.creation_date_laatste_keer, 'dd-mm-yyyy hh24:mi:ss') datum_verzonden
,     beri.verzonden_laatste_keer verzonden
from
kgc_personen pers,
kgc_onderzoeken onde,
kgc_zwangerschappen zwan,
kgc_foetussen foet,
kgc_kgc_afdelingen kafd,
kgc_onderzoeksgroepen ongr,
(
select
    ROW_NUMBER () OVER (PARTITION BY beri2.entiteit_id ORDER BY beri2.creation_date) volgnr,
    beri2.entiteit_id zwan_id,
    MAX (beri2.creation_date) KEEP (DENSE_RANK LAST ORDER BY beri2.creation_date) over (PARTITION BY beri2.entiteit_id) creation_date_laatste_keer,
    MAX (beri2.verzonden) KEEP (DENSE_RANK LAST ORDER BY beri2.creation_date) over (PARTITION BY beri2.entiteit_id) verzonden_laatste_keer,
    MAX (beri2.log_tekst) KEEP (DENSE_RANK LAST ORDER BY beri2.creation_date) over (PARTITION BY beri2.entiteit_id) log_tekst_laatste_keer
from beh_berichten beri2, beh_bericht_types bety
where beri2.bety_id = bety.bety_id
and upper (beri2.entiteit) = 'ZWAN'
and upper (beri2.ontvanger_type) = 'PERS'
and upper(bety.code) = 'SMS'
order by beri2.beri_id, beri2.creation_date
) beri
where pers.pers_id = onde.pers_id
and   onde.foet_id = foet.foet_id
and   foet.zwan_id = zwan.zwan_id
and   zwan.zwan_id = beri.zwan_id
and   onde.kafd_id = kafd.kafd_id
and   onde.ongr_id = ongr.ongr_id
and   kafd.code in ('GENOOM', 'CYTO')
and   ongr.code like 'PRE%'
and   beri.volgnr = 1
and trunc(beri.creation_date_laatste_keer) = (trunc(sysdate)-p_dagen_terug)
order by onde.creation_date
;
cursor c_tel is
select pers.aanspreken, onde.onderzoeknr, pers.telefoon
from kgc_personen pers,
kgc_onderzoeken onde,
kgc_zwangerschappen zwan,
kgc_foetussen foet,
kgc_kgc_afdelingen kafd,
kgc_onderzoeksgroepen ongr
where pers.pers_id = onde.pers_id
and onde.foet_id = foet.foet_id
and foet.zwan_id = zwan.zwan_id
and  onde.kafd_id = kafd.kafd_id
and  onde.ongr_id = ongr.ongr_id
and  kafd.code in ('GENOOM', 'CYTO')
and  ongr.code like 'PRE%'
and nvl( kgc_attr_00.waarde ('KGC_ZWANGERSCHAPPEN', 'SMS_VERZENDEN', zwan.zwan_id), 'J') != 'N'
and trunc(onde.creation_date) = (trunc(sysdate)-p_dagen_terug)
order by onde.creation_date
;
BEGIN
if (sys_context ('USERENV', 'DB_NAME') like 'PROD%') then
    v_conn:= UTL_SMTP.OPEN_CONNECTION(host => v_smtp_host);
    UTL_SMTP.HELO(v_conn, v_smtp_host);
    UTL_SMTP.MAIL(v_conn, v_verzender_email);

    -- loop om alle email adressen als ontvanger door te geven (in rcpt)
    -- komma is de separator
    for r_rcpt in c_rcpt loop
        UTL_SMTP.RCPT(v_conn, r_rcpt.column_value);
    end loop;

    UTL_SMTP.OPEN_DATA(v_conn);

    -- header hoofd bericht
    UTL_SMTP.WRITE_DATA(v_conn, 'From: <' || v_verzender_email || '>' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'To: <' || v_ontvanger_email || '>' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Subject: '|| v_subject || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'MIME-Version: 1.0' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Content-Type: multipart/mixed; boundary= "' || v_boundary || '"' || UTL_TCP.CRLF || UTL_TCP.CRLF);

    -- sub bericht body
    UTL_SMTP.WRITE_DATA(v_conn, '--' || v_boundary|| UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Content-Type: text/html; charset=UTF-8' || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, 'Content-Transfer-Encoding: 8bit'|| UTL_TCP.CRLF || UTL_TCP.CRLF);
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<i>Deze e-mail is automatisch door Helix (_dipo_overzichten) gegenereerd.</i><br><br>'));
    -- Eerste overzicht: verstuurde sms
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<table border="1" align="center">'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr><td align="center" colspan="4"><font color="green" size="4">Verzonden smsjes op ' || to_char(trunc(sysdate)-p_dagen_terug, 'dd-mm-yyyy') || '</td></tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Aanspreken</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Onderzoeknr</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Datum verzonden</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Verzonden</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));
    for r_beri in c_beri loop
        if (upper(r_beri.verzonden) != 'J') then
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr bgcolor="red">'));
        else
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_beri.aanspreken ||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_beri.onderzoeknr||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_beri.datum_verzonden||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center">'||r_beri.verzonden||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));
        end if;
    end loop;
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</table><br><br>'));
    -- tweede overzicht: ongeldige telefoonrs
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<table border="1" align="center">'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr><td align="center" colspan="3"><font color="green" size="4">Ongeldige mobielnrs, ingevoerd op ' || to_char(trunc(sysdate)-p_dagen_terug, 'dd-mm-yyyy') || '</td></tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Aanspreken</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Onderzoeknr</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td align="center"><b>Telefoon</b></td>'));
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));
    for r_tel in c_tel loop
        if not(beh_alfu_00.is_mobielnr_goed(nvl(regexp_replace(r_tel.telefoon, '[^[:digit:]]'), 'Geen TelNr.'))) then
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<tr>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_tel.aanspreken ||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_tel.onderzoeknr||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('<td>'||r_tel.telefoon||'</td>'));
            UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</tr>'));
        end if;
    end loop;
    UTL_SMTP.WRITE_RAW_DATA(v_conn, UTL_RAW.CAST_TO_RAW('</table>'));

    UTL_SMTP.WRITE_DATA(v_conn, UTL_TCP.CRLF);
    UTL_SMTP.WRITE_DATA(v_conn, UTL_TCP.CRLF);
    -- afsluiten van smtp connectie
    UTL_SMTP.CLOSE_DATA(v_conn);
    UTL_SMTP.QUIT(v_conn);
    return true;
end if;
EXCEPTION
WHEN OTHERS THEN
    UTL_SMTP.QUIT(v_conn);
    x_msg := ': Error: ' || SQLERRM;
    return false;
end beh_dipo_overzichten;

/

/
QUIT
