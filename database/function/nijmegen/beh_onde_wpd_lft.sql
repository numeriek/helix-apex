CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_WPD_LFT"
( p_onde_id in  number
)/******************************************************************************
   NAME: BEH_onde_wpd_lft
   PURPOSE:   bepaal of het onderzoek een indicatie heeft die in de wpd categorie leeftijd valt
            om te gebruiken in het prenatale wpd jaarverslag
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        27-dec-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(1);

  cursor c_lft is
  select kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN', 'WPD_CAT_JV',onin.indi_id) waarde
     from KGC_ONDERZOEK_INDICATIES onin
     ,    kgc_indicatie_teksten indi
     where onin.indi_id = indi.indi_id
     and onin.onde_id = p_onde_id
   ;
  r_lft c_lft%rowtype;


begin
  v_return := 'N';
  OPEN  c_lft;
  fetch c_lft into r_lft;
  WHile  c_lft%found
  LOOP
    if r_lft.waarde = 'LEEFTIJD' then
      v_return := 'J';
    end if;
    fetch c_lft into r_lft;
  end loop;
  Close c_lft;

  RETURN( v_return );

  EXCEPTION
      WHEN others
      THEN
        v_return := 'N';
      RETURN( v_return );
END;
/

/
QUIT
