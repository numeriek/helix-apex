CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_GESP_DATUM_TEMP" (p_onderzoeknr varchar2)
return varchar2 is

v_datum date;

cursor c_gesp is
select to_char(datum, 'dd-mm-yyyy') datum
from kgc_onderzoeken onde, kgc_gesprekken gesp
where onde.onde_id = gesp.onde_id
and onde.onderzoeknr = p_onderzoeknr --'P11-3600'
order by gesp.datum asc
;
begin
for r_gesp in c_gesp loop
    v_datum := r_gesp.datum;
end loop;
return v_datum;
end;

/

/
QUIT
