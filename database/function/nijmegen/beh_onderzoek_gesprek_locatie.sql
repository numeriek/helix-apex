CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDERZOEK_GESPREK_LOCATIE" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onderzoek_gesprek_locatie
   PURPOSE:    _gesprek_locatie in een tekst string zetten bij een onde_id
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12-04-2010  Cootje Vermaat      1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_onderzoek_gesprek_tekst
      Sysdate:         12-04-2010
      Date and Time:   16-11-2007, 8:47:02, and 16-11-2007 8:47:02
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  cursor c_gesp (p_onde_id  in number)is
    select loca.code locatie
    from   kgc_gesprekken gesp
    ,      kgc_locaties loca
    where  gesp.onde_id = p_onde_id
    and    gesp.loca_id = loca.loca_id(+)
    order by gesp.gesp_id
    ;

  r_gesp c_gesp%ROWTYPE;
  v_locatie varchar2(100);

BEGIN
  v_locatie := null;
  OPEN c_gesp(p_onde_id);
  FETCH C_gesp INTO r_gesp;
  WHILE c_gesp%FOUND
  LOOP
    IF v_locatie is null
    THEN v_locatie := r_gesp.locatie;
    ELSE
      v_locatie := v_locatie || ', ' || r_gesp.locatie;
    END IF;
    FETCH C_gesp INTO r_gesp;
  END LOOP;
  CLOSE c_gesp;
  RETURN v_locatie;
END;

/

/
QUIT
