CREATE OR REPLACE FUNCTION "HELIX"."BEH_DATUM_PUNCTIE"
( p_zwan_id IN NUMBER
)/******************************************************************************
   NAME:       beh_datum_punctie
   PURPOSE:    bepaal de meest recente afname datum van monsters bij een zwangerpschap
      om te gebruiken bij het rapport kgcvreu51 akties
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        15-06-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR c_mons (p_zwan_id in number)
  IS
    SELECT to_char(mons.datum_afname, 'dd-mm-yyyy') datum_afname
    FROM   kgc_monsters mons
    ,      kgc_foetussen foet
    WHERE  mons.foet_id = foet.foet_id
    AND    foet.zwan_id = p_zwan_id
    ORDER BY  mons.datum_afname desc
    ;
    R_mons C_mons%rowtype;
BEGIN

  OPEN c_mons(p_zwan_id);

  FETCH c_mons INTO R_mons;
  IF c_MONS%FOUND
  THEN
    v_return := r_mons.datum_afname;
  ELSE
        v_return := 'Onbekend';
  END IF;
  CLOSE c_mons;
  if v_return is null then v_return := 'Onbekend'; end if;
  RETURN( v_return );

  EXCEPTION
      WHEN VALUE_ERROR
      THEN
        NULL;

END;

/

/
QUIT
