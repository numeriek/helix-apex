CREATE OR REPLACE FUNCTION "HELIX"."BEH_NGS_INDI_TXT"
( p_indi_id in  number, p_taal_id IN NUMBER
)/******************************************************************************
   NAME: BEH_NGS_INDI_TXT
   PURPOSE:   bepaal de vaste tekst bij de indicatie voor de uitslagbrief van onderzoeksgroep NGS

               om te gebruiken bij het rapport kgcuits51DNA
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        08-12-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  cursor c_indi is
  select sypa.standaard_waarde
     from KGC_ATTRIBUTEN attr
     , kgc_attribuut_waarden atwa
     , kgc_systeem_parameters sypa
     where attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
     and attr.code = 'UITSLAG_TXT'
     and ATWA.ATTR_ID = attr.attr_id
     and atwa.id = p_indi_id
     and atwa.waarde = sypa.code
   ;

  cursor c_indi_e is
  select sypa.standaard_waarde
     from KGC_ATTRIBUTEN attr
     , kgc_attribuut_waarden atwa
     , kgc_systeem_parameters sypa
     where attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
     and attr.code = 'UITSLAG_TXT'
     and ATWA.ATTR_ID = attr.attr_id
     and atwa.id = p_indi_id
     and atwa.waarde||'E' = sypa.code
   ;


v_taal varchar2(10)  := null;


begin

  if p_taal_id is not null then
    select code into v_taal from kgc_talen where taal_id = p_taal_id;
  end if;

  if p_taal_id  is null or v_taal = 'NED' then

      OPEN  c_indi;
      fetch c_indi into v_return;
      If c_indi%notfound then
        v_return := null;
      end if;

  else
      OPEN  c_indi_e;
      fetch c_indi_e into v_return;
      If c_indi_e%notfound then
        v_return := null;
      end if;


  end if;

  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := null;

END;

/

/
QUIT
