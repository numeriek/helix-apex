CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDERZOEK_METINGEN" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onderzoek_metingen
   PURPOSE:    stoftestgroepcodes van metingen in een tekst string zetten bij een onde_id
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18-12-2008  Cootje Vermaat      1. Created this function.

   1.1        14-07-2009  Cootje Vermaat      2.v_meti groter gedefinieerd: 100 --> 150
******************************************************************************/
  cursor c_meti is
    select stgr.code
    from   bas_metingen meti
    , kgc_stoftestgroepen stgr
    where  meti.onde_id = p_onde_id
    and    meti.stgr_id = stgr.stgr_id
    ;

  r_meti c_meti%ROWTYPE;
  v_meti varchar2(1000);

BEGIN
  v_meti := 'Geen metingen gevonden';
  OPEN c_meti;
  FETCH C_meti INTO r_meti;
  WHILE c_meti%FOUND
  LOOP
    IF v_meti ='Geen metingen gevonden'
	THEN v_meti := r_meti.code;
	ELSE
      v_meti := v_meti || ', ' || r_meti.code;
    END IF;
    FETCH C_meti INTO r_meti;
  END LOOP;
  CLOSE c_meti;
  RETURN v_meti;
END;

/

/
QUIT
