CREATE OR REPLACE FUNCTION "HELIX"."BEH_NGS_TST_BESCHR_NEW"
( p_onde_id in  number, p_indi_id in  number, p_taal_id IN NUMBER
)/******************************************************************************
   NAME: BEH_NGS_TST_BESCHR
   PURPOSE:   bepaal de vaste tekst bij het isolatie proces van de betrokken fratcies
              of de indicatie voor de uitslagbrief van onderzoeksgroep NGS

               om te gebruiken bij het rapport kgcuits51DNA
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05-07-2013  Cootje Vermaat      1. Created this function.
   1.1        apr-2016    cootje Vermaat     aangepast nav aanpassing aan testbeschrijvingen: mantis 0012770:



******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  v_ongr_code varchar2(100) := null;
  v_taal varchar2(10)  := null;
  v_indi_code varchar2(100)  := null;

begin

  if p_onde_id is not null then
  select ongr.code into v_ongr_code
    from kgc_onderzoeken onde
    , kgc_onderzoeksgroepen ongr
    where onde.ongr_id = ongr.ongr_id
    and onde.onde_id = p_onde_id;
  end if;

  if v_ongr_code = 'NGS' then
      if p_taal_id is not null then
        select code into v_taal from kgc_talen where taal_id = p_taal_id;
      end if;

      if p_taal_id  is null or v_taal = 'NED' then  -- Nederlands
        if beh_onde_mons_dummy(p_onde_id) = 'J' then

          select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TEST_BESCHR_DUMMY';

        elsif beh_onde_mons_opgestuurd(p_onde_id) = 'J' then

          select BEH_ONDERZOEK_INDICATIES(p_onde_id) into v_indi_code from dual;
          if instr(v_indi_code, 'CNV')  > 0 then
            select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_CNV';
          ELSE
              select  substr(beh_onderzoek_indicaties(p_onde_id), (length(beh_onderzoek_indicaties(p_onde_id)) - 2 ) )
               into v_indi_code from dual;

               if  (v_indi_code = '_02' or substr(v_indi_code, 2) = '_2') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST_2';

               elsif (v_indi_code = '_03' or substr(v_indi_code, 2) = '_3') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST_3';

               elsif substr(v_indi_code, 2) = '_C' then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST_C';

               else

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST';

               end if;
          END IF;
        else
          select  substr(beh_onderzoek_indicaties(p_onde_id), (length(beh_onderzoek_indicaties(p_onde_id)) - 2 ) )
           into v_indi_code from dual;
               if  (v_indi_code = '_02' or substr(v_indi_code, 2) = '_2') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500_2';

               elsif (v_indi_code = '_03' or substr(v_indi_code, 2) = '_3') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500_3';

               elsif substr(v_indi_code, 2) = '_C' then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500_C';

               else

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500';

               end if;


        end if;

      else -- ENGELS
        if beh_onde_mons_dummy(p_onde_id) = 'J' then

          select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TEST_BESCHR_DUMMY_E';

        elsif beh_onde_mons_opgestuurd(p_onde_id) = 'J' then

          select BEH_ONDERZOEK_INDICATIES(p_onde_id) into v_indi_code from dual;
          if instr(v_indi_code, 'CNV')  > 0 then
            select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_CNV_E';
          ELSE

              select  substr(beh_onderzoek_indicaties(p_onde_id), (length(beh_onderzoek_indicaties(p_onde_id)) - 2 ) )
               into v_indi_code from dual;
               if  (v_indi_code = '_02' or substr(v_indi_code, 2) = '_2') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST_2E';

               elsif (v_indi_code = '_03' or substr(v_indi_code, 2) = '_3') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST_3E';

               elsif substr(v_indi_code, 2) = '_C' then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST_CE';

               else
                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_MONS_VRST_E';
               end if;
          END IF;

        else
          select  substr(beh_onderzoek_indicaties(p_onde_id), (length(beh_onderzoek_indicaties(p_onde_id)) - 2 ) )
           into v_indi_code from dual;
               if  (v_indi_code = '_02' or substr(v_indi_code, 2) = '_2') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500_2E';

               elsif (v_indi_code = '_03' or substr(v_indi_code, 2) = '_3') then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500_3E';

               elsif substr(v_indi_code, 2) = '_C' then

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500_CE';

               else

                 select standaard_waarde into v_return from kgc_systeem_parameters where code = 'BEH_NGS_TST_BSCHR_500_E';

               end if;

        end if;


      end if;
    else
      v_return:= null;
    end if;

  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := null;

END;
/

/
QUIT
