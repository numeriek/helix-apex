CREATE OR REPLACE FUNCTION "HELIX"."BEH_DATUM_AUTO_ZWAN_ONDE"
( p_zwan_id IN NUMBER
)
/******************************************************************************
   NAME:       beh_datum_auto_zwan_onde
   PURPOSE:    bepaal de meets recente autorisetie datum van onderzoeken bij een zwangerpschap
      om te gebruiken bij het rapport kgcvreu51 akties
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        15-06-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  CURSOR c_onde
  IS
    SELECT to_char(onde.datum_autorisatie, 'dd-mm-yyyy') datum_autorisatie
    FROM   kgc_onderzoeken onde
    ,      kgc_prenataal_zwan_onde_vw zwon
    WHERE  zwon.onde_id = onde.onde_id
    AND    zwon.zwan_id = p_zwan_id
    and not exists (select *     -- er mogen geen niet afgronde of niet afgeautoriseerd onderzoeken zijn bij deze zwangerschap
                    from kgc_onderzoeken onde2
                    ,    kgc_prenataal_zwan_onde_vw zwon2
                    where zwon2.onde_id = onde2.onde_id
                    and  zwon2.onde_id = p_zwan_id
                    and ( onde.afgerond <>'J'
                        or onde.datum_autorisatie is null)
                    )
    ORDER BY onde.datum_autorisatie desc
    ;
  r_onde c_onde%rowtype;
BEGIN

  OPEN c_onde;
  FETCH c_onde INTO r_onde;
  IF c_onde%FOUND THEN
        v_return := r_onde.datum_autorisatie;
  ELSE
        v_return := 'Onbekend';
  END IF;
  CLOSE c_onde;
  return (v_return);
EXCEPTION
      WHEN VALUE_ERROR
      THEN
        NULL;
END;

/

/
QUIT
