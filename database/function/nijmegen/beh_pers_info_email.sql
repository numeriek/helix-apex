CREATE OR REPLACE FUNCTION "HELIX"."BEH_PERS_INFO_EMAIL" (p_pers_id number) return varchar is
/******************************************************************************
   NAME:       beh_pers_info_email
   PURPOSE:    geeft de persoons gegevesn om te gebruiken in een e-mail
              wordt gebruikt bij de email alert ten behoeve van het epd


   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        17-03-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_pers is
      SELECT achternaam
      ,      aanspreken
      ,      geboortedatum
      ,      zisnr
      ,      part_geboortedatum
      ,      geslacht
      ,      DECODE( achternaam_partner
                     , NULL, SUBSTR(achternaam,1,3)
                     , SUBSTR(achternaam,1,3)||'-'||SUBSTR(achternaam_partner,1,3)
                     ) afkorting
      ,      DECODE( overleden
                     , 'J', 'Overleden '||TO_CHAR(overlijdensdatum,'dd-mm-yyyy')
                     , NULL
                     ) datum_overleden
      FROM   kgc_personen
      WHERE  pers_id = p_pers_id
      ;
  r_pers c_pers%ROWTYPE;

  v_return varchar2(200);
BEGIN
  OPEN c_pers;
  Fetch c_pers INTO r_pers;
  IF   c_pers%FOUND then
   v_return := 'Registratienr '||r_pers.zisnr ;
  ELSE
   v_return := 'Onbekend';
  END if;
  CLOSE c_pers;
  RETURN v_return;
END;
/

/
QUIT
