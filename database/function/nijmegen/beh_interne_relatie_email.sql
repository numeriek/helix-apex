CREATE OR REPLACE FUNCTION "HELIX"."BEH_INTERNE_RELATIE_EMAIL" (p_rela_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_interne_relatie_email
   PURPOSE:    relatie_email in een tekst string zetten bij een onde_id
               om te gebruiken in prccedure voor het verzenden van email
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        13-04-2010  Cootje Vermaat      1. Created this function.
   1.1         31-08-2012 Marzena Golebiowska   2. Extra email adres toegevoegd nav mantis 7471

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_onderzoek_gesprek_tekst
      Sysdate:         13-04-2010

******************************************************************************/

 cursor c_rela (p_rela_id in number)is
  select liszcode
  from kgc_relaties rela
  where RELA.RELA_ID = p_rela_id
  ;


-- onderstaand cursor is toegevoegd nav mantis 7471,
 cursor c_extra (p_rela_id in number)is
  select atwa.waarde
  from kgc_relaties rela, kgc_attribuut_waarden atwa, kgc_attributen attr
  where RELA.RELA_ID = ATWA.ID
  and ATWA.ATTR_ID = ATTR.ATTR_ID
  and ATTR.CODE = 'EPD_EXTRA_MAIL_ADRES'
  and RELA.RELA_ID = p_rela_id
  ;

  r_rela c_rela%ROWTYPE;
  r_extra c_extra%ROWTYPE;

  v_email varchar2(1000);

BEGIN
  v_email := null;
  OPEN c_rela(p_rela_id);
  FETCH C_rela INTO r_rela;

  OPEN c_extra(p_rela_id);
  FETCH C_extra INTO r_extra;

  IF c_rela%FOUND
    THEN IF c_extra%FOUND
                THEN v_email := r_rela.liszcode||'@umcn.nl' || ', '|| r_extra.waarde;
                ELSE v_email := r_rela.liszcode||'@umcn.nl';
    END IF;
  END IF;
  CLOSE c_rela;
  CLOSE c_extra;

  RETURN v_email;
END;

/

/
QUIT
