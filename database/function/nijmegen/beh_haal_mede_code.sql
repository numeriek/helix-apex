CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_MEDE_CODE" (p_mede_id NUMBER) RETURN VARCHAR2 IS
  v_return VARCHAR2(1000);
/******************************************************************************
   NAME:       BEH_HAAL_MEDE_CODE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4-11-2008          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     BEH_HAAL_MEDE_CODE
      Sysdate:         4-11-2008
      Date and Time:   4-11-2008, 18:05:41, and 4-11-2008 18:05:41
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  CURSOR mede_id_cur
  IS
    SELECT code
    FROM   KGC_MEDEWERKERS
    WHERE  mede_id = p_mede_id
    ;
BEGIN
  OPEN  mede_id_cur;
  FETCH mede_id_cur INTO  v_return;
  CLOSE mede_id_cur;
  RETURN( v_return );
END BEH_HAAL_MEDE_CODE;

/

/
QUIT
