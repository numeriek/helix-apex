CREATE OR REPLACE FUNCTION "HELIX"."BEH_MEETWAARDE_DETAILS"
(p_meet_id in number, p_prompt in varchar)
-- name:         beh_meetwaarde_details
-- purpose:      ophalen meetwaarde details met een bepaalde prompt bij een bepaalde meetwaarde
--               om te gebruiken in select scripts
--
-- created by:   cv on 03 -04-2012
--
return varchar2
is
  cursor c_mdet is
   SELECT mdet.waarde
    FROM   bas_meetwaarde_details mdet
    ,      bas_meetwaarden meet
    WHERE  mdet.meet_id = meet.meet_id
    AND    meet.meet_id = p_meet_id
    AND    mdet.prompt =  p_prompt ;
 v_return varchar2(1000);

begin

  open c_mdet;
  fetch c_mdet  into v_return;
  if c_mdet%notfound then
    v_return := null;
  end if;
  close c_mdet;

  return(v_return);
end;

/

/
QUIT
