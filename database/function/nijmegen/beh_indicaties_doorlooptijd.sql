CREATE OR REPLACE FUNCTION "HELIX"."BEH_INDICATIES_DOORLOOPTIJD" (p_indi_id number, p_onderzoekswijze varchar2) return number is
/******************************************************************************
   NAME:       beh_indicaties_doorlooptijd
   PURPOSE:    doorlooptijd opzoeken bij een indicatie en onderzoekswijze
               om te gebruiken in select scripts
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        24-2-2009          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_indicatie_doorlooptijd
      Sysdate:         24-2-2009
      Date and Time:   24-2-2009, 13:26:11, and 24-2-2009 13:26:11
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  cursor c_loop is
    select loop.doorlooptijd
    from   kgc_looptijden loop
    where  loop.indi_id = p_indi_id
    and    loop.onderzoekswijze = p_onderzoekswijze
    ;

  r_loop c_loop%ROWTYPE;
  v_doorlooptijd   number(10)   := null;

BEGIN
  v_doorlooptijd := null;
  OPEN c_loop;
  FETCH C_loop INTO r_loop;
  IF c_loop%FOUND
  THEN
    v_doorlooptijd := r_loop.doorlooptijd;
  END IF;
  CLOSE c_loop;
  RETURN v_doorlooptijd;
END;

/

/
QUIT
