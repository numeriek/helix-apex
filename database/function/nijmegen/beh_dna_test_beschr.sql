CREATE OR REPLACE FUNCTION "HELIX"."BEH_DNA_TEST_BESCHR"
( p_onde_id in  number
, p_taal_id in number
)/******************************************************************************
   NAME: BEH_DNA__TEST_BESCHR
   PURPOSE:   bepaal of de testbeschrijvingt in de uitslagbrief verschijnt , op basis van het gebruikte protocol bij het onderzoek

               om te gebruiken bij het rapport kgcuits51DNA
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        28-feb-2014  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(2000);
  v_taal   varchar2(40);

  cursor c_stgr is
  select meti.meti_id
     from KGC_stoftestgroepen stgr
     ,    bas_metingen meti
     where meti.stgr_id = stgr.stgr_id
     and stgr.omschrijving like '%Robot protocol ION%'
     and stgr.code like '%_I'
     and meti.onde_id = p_onde_id
   ;
  r_stgr c_stgr%rowtype;


begin

  OPEN  c_stgr;
  fetch c_stgr into r_stgr;
  If c_stgr%notfound then
    v_return := null;
  else
      if p_taal_id is not null then
        select code into v_taal from kgc_talen where taal_id = p_taal_id;
      end if;

      if p_taal_id  is null or v_taal = 'NED' then
        v_return := kgc_sypa_00.standaard_waarde('BEH_LTG_TST_BSCHR_IONTORRENT', 'GENOOM', null, null);
      else
        v_return := kgc_sypa_00.standaard_waarde('BEH_LTG_TST_BSCHR_IONTORRENT_E', 'GENOOM', null, null);
      end if;

  end if;
  CLOSE c_stgr;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := null;
        return(v_return);

END;
/

/
QUIT
