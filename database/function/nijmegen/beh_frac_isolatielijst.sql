CREATE OR REPLACE FUNCTION "HELIX"."BEH_FRAC_ISOLATIELIJST" (P_FRAC_id number) return varchar is
/******************************************************************************
   NAME: BEH_FRAC_ISOLATIELIJST
   PURPOSE:    zoek de isolatielijst bij een FRACTIE
      om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12-04-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
    cursor c_isol (p_FRAC_id in varchar)
    is
    select isol.isolatielijst_nummer
    from bas_isolatielijsten isol
    where  isol.FRAC_id = p_FRAC_id
    ;

    r_isol c_isol%ROWTYPE;

  v_isol varchar2(1000);


BEGIN

  v_isol := null;

  IF  p_FRAC_id is not null  THEN

    OPEN c_isol(p_FRAC_id);
    FETCH c_isol INTO r_isol;
    IF c_isol%FOUND THEN
      v_isol := r_isol.isolatielijst_nummer;
    ELSE
      v_isol := null;
    END IF;
    CLOSE c_isol;

  END IF;

  RETURN v_isol;

END;

/

/
QUIT
