CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_INDI_CODE" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       BEH_HAAL_indi_CODE
   PURPOSE:  ophalen van indicatie code op basis van onde_id om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2015  Marzena Golebiowska 1. Created this function.

******************************************************************************/

v_indi varchar2(4000);

cursor c_indi is
    select LISTAGG(INDI.CODE, ', ') WITHIN GROUP (ORDER BY INDI.CODE) AS INDICODE
    from kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten indi
    where ONDE.ONDE_ID = ONIN.ONDE_ID
        and ONIN.INDI_ID = INDI.INDI_ID
        and ONDE.ONDE_ID = p_onde_id
    group by ONDE.ONDE_ID
    ;

BEGIN
    open c_indi;
    fetch c_indi into v_indi;
        if c_indi%found then
            v_indi := v_indi;
        end if;
    close c_indi;
    RETURN v_indi;
END;
/

/
QUIT
