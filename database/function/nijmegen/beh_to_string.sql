CREATE OR REPLACE FUNCTION "HELIX"."BEH_TO_STRING" (nt_in        IN beh_varchar2_ntt,
        delimiter_in IN VARCHAR2 DEFAULT ','
        ) RETURN VARCHAR2 IS

        v_idx PLS_INTEGER;
        v_str VARCHAR2(32767);
        v_dlm VARCHAR2(10);
        BEGIN
            v_idx := nt_in.FIRST;
            WHILE v_idx IS NOT NULL
                LOOP
                    v_str := v_str || v_dlm || nt_in(v_idx);
                    v_dlm := delimiter_in;
                    v_idx := nt_in.NEXT(v_idx);
                    EXIT WHEN  length(v_str) >= 4000-1256;
                END LOOP;

                RETURN CASE WHEN length(v_str) >= 4000-1256 THEN  'test'|| v_str ELSE v_str END;
                --v_str;
                END beh_to_string;
/

/
QUIT
