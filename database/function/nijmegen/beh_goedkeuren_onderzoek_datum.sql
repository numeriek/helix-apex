CREATE OR REPLACE FUNCTION "HELIX"."BEH_GOEDKEUREN_ONDERZOEK_DATUM" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_goedkeuren_onderzoek_datum
   PURPOSE:   opzoeken datum dat onderzoek is goedgekeurd om te gebruiken in een script

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16-1-2009          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_goedkeuren_onderzoek_datum
      Sysdate:         16-1-2009
      Date and Time:   16-1-2009, 11:54:04, and 16-1-2009 11:54:04
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  cursor c_audi is
    select to_char(audi.datum_tijd, 'dd-mm-yyyy') goedgekeurd
    from kgc_audit_log audi
    where audi.id = p_onde_id
    and   audi.tabel_naam   =  'KGC_ONDERZOEKEN'
    and   audi.kolom_naam   =  'STATUS'
    and   nvl(audi.waarde_oud, '-')   <> 'G'
    and   audi.waarde_nieuw =  'G'
    ;

  r_audi c_audi%ROWTYPE;
  v_audi      varchar2(100);

BEGIN
  v_audi := null;
  OPEN c_audi;
  FETCH C_audi INTO r_audi;
  WHILE c_audi%FOUND
  LOOP
    IF V_audi is null
    then
      v_audi := r_audi.goedgekeurd ;
    else
      v_audi := v_audi||', '||r_audi.goedgekeurd ;
    end if;
    FETCH C_audi INTO r_audi;
  END LOOP;
  CLOSE c_audi;
  RETURN v_audi;
END;

/

/
QUIT
