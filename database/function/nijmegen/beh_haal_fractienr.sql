CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_FRACTIENR" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       BEH_HAAL_FRACTIENR
   PURPOSE:  ophalen van fractienummers op basis van onde_id om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2015  Marzena Golebiowska 1. Created this function.

******************************************************************************/

v_frac varchar2(4000);

cursor c_frac is
    select LISTAGG(FRAC.FRACTIENUMMER, ', ') WITHIN GROUP (ORDER BY FRAC.FRACTIENUMMER ) AS FRACTIENR
    from
    kgc_onderzoeken onde,
            kgc_onderzoek_monsters onmo,
            kgc_monsters mons,
            bas_fracties frac
    where ONDE.ONDE_ID = ONMO.ONDE_ID
        and ONMO.MONS_ID = MONS.MONS_ID
        and MONS.MONS_ID = FRAC.MONS_ID
        and ONDE.ONDE_ID = p_onde_id
        and exists (select meti.frac_id from bas_metingen meti, kgc_onderzoeken onde1
                where ONDE.ONDE_ID =METI.ONDE_ID
                    and ONDE1.ONDE_ID = ONDE.ONDE_ID
                    and METI.FRAC_ID = FRAC.FRAC_ID
                )
    ;

BEGIN
    open c_frac;
    fetch c_frac into v_frac;
        if c_frac%found then
            v_frac := v_frac;
        end if;
    close c_frac;
    RETURN v_frac;
END;
/

/
QUIT
