CREATE OR REPLACE FUNCTION "HELIX"."BEH_DATUM_NA_ELEK_AUTO" (p_datum date) return varchar is
/******************************************************************************
   NAME:       BEH_DATUM_NA_ELEK_AUTO
   PURPOSE:    bepaal of de datum valt na de ipn van de elektronische handtekening

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22-10-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/

  v_voor varchar2(1);

BEGIN
  If p_datum >= to_date('04-10-2010','dd-mm-yyyy') THEN
    v_voor:= 'J';
  ELSE
    v_voor:= 'N';
  END IF;
  RETURN v_voor;
END;

/

/
QUIT
