CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDERZOEK_GESPREK_TEKST" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onderzoek_gesprek_tekst
   PURPOSE:    _gesprek_tekst in een tekst string zetten bij een onde_id
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        06-04-2010  Cootje Vermaat      1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_onderzoek_gesprek_tekst
      Sysdate:         06-04-2010
      Date and Time:   06-04-2010, 8:47:02, and 16-11-2007 8:47:02
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  cursor c_gesp (p_onde_id  in number)is
    select replace(gesp.tekst,CHR(10) , ' ') tekst
    from   kgc_gesprekken gesp
    where  gesp.onde_id = p_onde_id
    ;

  r_gesp c_gesp%ROWTYPE;
  v_tekst varchar2(10000);

BEGIN
  v_tekst := null;
  OPEN  c_gesp(p_onde_id);
  FETCH c_gesp INTO r_gesp;
  WHILE c_gesp%FOUND
  LOOP
    IF   v_tekst is null
    THEN v_tekst := r_gesp.tekst;
    ELSE
      v_tekst := v_tekst || ', ' || r_gesp.tekst;
    END IF;
    FETCH C_gesp INTO r_gesp;
  END LOOP;
  CLOSE  c_gesp;
  RETURN v_tekst;
END;

/

/
QUIT
