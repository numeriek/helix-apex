CREATE OR REPLACE FUNCTION "HELIX"."BEH_NGS_DISCL_TXT"
( p_indi_id in  number
)/******************************************************************************
   NAME: BEH_NGS_DISCL_TXT
   PURPOSE:   bepaal of  de disclaimer label en tekst in de uitslagbrief verschijnt , op basis van de indicatie bij onderzoeksgroep NGS

               om te gebruiken bij het rapport kgcuits51DNA
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        01-02-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(20);
  cursor c_indi is
  select atwa.waarde
     from KGC_ATTRIBUTEN attr
     , kgc_attribuut_waarden atwa
     where attr.tabel_naam = 'KGC_INDICATIE_TEKSTEN'
     and attr.code = 'DISCL_TXT'
     and ATWA.ATTR_ID = attr.attr_id
     and atwa.id = p_indi_id
   ;

begin

  OPEN  c_indi;
  fetch c_indi into v_return;
  If c_indi%notfound then
    v_return := null;
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := null;

END;
/

/
QUIT
