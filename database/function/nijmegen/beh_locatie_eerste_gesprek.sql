CREATE OR REPLACE FUNCTION "HELIX"."BEH_LOCATIE_EERSTE_GESPREK" (p_onde_id number) return varchar is
/******************************************************************************
   NAME:       BEH_locatie_eerste_gesprek
   PURPOSE:   bepaal de locatie van het eerste gesprek bij een onderzoek
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        27-10-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_loca is
    select nvl(loca.omschrijving, 'Onbekend') omschrijving
    from   KGC_GESPREKKEN gesp
    ,      kgc_locaties   loca
    where  gesp.onde_id = p_onde_id
    and    gesp.loca_id = loca.loca_id(+)
    order by gesp.gesp_id
    ;

  r_loca c_loca%ROWTYPE;

  v_loca varchar2(100);

BEGIN
  OPEN c_loca;
  FETCH C_loca INTO r_loca;
  IF c_loca%FOUND THEN
     v_loca := r_loca.omschrijving;
  ELSE
     v_loca := 'Onbekend';
  END IF;
  CLOSE c_loca;
  RETURN v_loca;
END;

/

/
QUIT
