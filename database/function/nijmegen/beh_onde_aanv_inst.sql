CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_AANV_INST" (p_onde_id number) return varchar is
/******************************************************************************
   NAME:       beh_mach_aanv_inst
   PURPOSE:    zoek de instelling van de aanvrager  bij een onderzoek ,
               alleen als de aanvrager niet in het radboud zit
               en wel in Nederland
               om te gebruiken in select scripts, bv jaarverslag van metab

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19-03-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/

  cursor c_onde (p_id in number)
  is
    select rela_id
    from   kgc_onderzoeken
    where  onde_id = p_id
    ;

  r_onde c_onde%ROWTYPE;


  cursor c_rela  (p_rela_id in number)
  is
    select nvl(inst.naam, 'ONB') inst_naam
    ,      nvl(inst.woonplaats, 'ONB') inst_woonplaats
    ,      nvl(inst.land, 'ONB') inst_land
    from   kgc_relaties rela
    ,      kgc_specialismen spec
    ,      kgc_instellingen inst
    ,      kgc_afdelingen afdg
    where  rela.spec_id = spec.spec_id(+)
    and    rela.inst_id = inst.inst_id(+)
    and    rela.afdg_id = afdg.afdg_id(+)
    and    rela.rela_id = p_rela_id
    and    nvl(UPPER(inst.code), 'ONB') not   in ('UMCN','AZN','NYMESTRADB','NYMEUMCNNY')
    and    instr(upper(nvl(rela.adres,'ONB')),'ALHIER') = 0
    and    upper(nvl(nvl(inst.land, rela.land),'NEDERLAND')) = 'NEDERLAND'
    ;

  r_rela c_rela%ROWTYPE;

  v_rela_id number;
  v_tekst  varchar2(1000);

BEGIN
  v_rela_id := null;
  v_tekst   := null;
  OPEN  c_onde(p_onde_id);
  FETCH c_onde INTO r_onde;
  IF    c_onde%FOUND THEN v_rela_id := r_onde.rela_id; END IF;
  CLOSE c_onde;
  IF v_rela_id is not null THEN
    OPEN c_rela (v_rela_id);
    FETCH c_rela INTO r_rela;
    IF c_rela%FOUND THEN
      v_tekst := r_rela.inst_naam||', '||r_rela.inst_woonplaats||', '||r_rela.inst_land;
    END IF;
    CLOSE c_rela;
  END IF;
  RETURN v_tekst;
END;

/

/
QUIT
