CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_FAMILIENR" (p_pers_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_haal_familienr
   PURPOSE:  ophalen van familienummers om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2015  Marzena Golebiowska 1. Created this function.

******************************************************************************/

v_fami varchar2(100);

cursor c_fami is
select LISTAGG(FAMI.FAMILIENUMMER, ',') WITHIN GROUP (ORDER BY FALE.PERS_ID) AS FAMILIENR
from   kgc_families fami, kgc_familie_leden fale
where FAMI.FAMI_ID = FALE.FAMI_ID
    and FALE.PERS_ID = p_pers_id
group by FALE.PERS_ID
;

BEGIN
    open c_fami;
    fetch c_fami into v_fami;
        if c_fami%found then
            v_fami := v_fami;
        end if;
    close c_fami;
    RETURN v_fami;
END;
/

/
QUIT
