CREATE OR REPLACE FUNCTION "HELIX"."BEH_INGR_ID" (p_onde_id number) return number is
/******************************************************************************
   NAME:       ingr_id
   PURPOSE:    zoek de ingr_id van de indicatiegroep bij een onderzoek ,
               om te gebruiken in select scripts,
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31-05-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  v_return number:= null;


  CURSOR ingr_cur
  IS
    SELECT ingr.ingr_id
    ,      onin.volgorde
    ,      onin.onin_id
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_indicatie_teksten indi
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.indi_id = indi.indi_id
    AND    indi.ingr_id = ingr.ingr_id
    AND    onin.onde_id = p_onde_id
    UNION
    SELECT ingr.ingr_id
    ,      onin.volgorde
    ,      onin.onin_id
    FROM   kgc_indicatie_groepen ingr
    ,      kgc_onderzoek_indicaties onin
    WHERE  onin.ingr_id = ingr.ingr_id
    AND    ingr.machtigingsindicatie IS NOT NULL
    AND    onin.onde_id = 901598
    ORDER BY 2 ASC
    ,        3 ASC
    ;

  ingr_rec ingr_cur%rowtype;

BEGIN
  IF ( p_onde_id IS NOT NULL )
  THEN
    OPEN  ingr_cur;
    FETCH ingr_cur
    INTO  ingr_rec;
    CLOSE ingr_cur;
  END IF;
  RETURN( ingr_rec.ingr_id );
END;

/

/
QUIT
