CREATE OR REPLACE FUNCTION "HELIX"."BEH_LABV_XML"
RETURN varchar2
AS
  l_request   soap_api.t_request;
  l_response  soap_api.t_response;
  l_return    VARCHAR2(32767);

  l_url          VARCHAR2(32767);
  l_namespace    VARCHAR2(32767);
  l_method       VARCHAR2(32767);
  l_soap_action  VARCHAR2(32767);
  l_result_name  VARCHAR2(32767);

BEGIN
  l_url         := 'http://helix.umcn.nl/soap/soap_005/soap_005.php';
  l_namespace   := 'xmlns="http://helix.umcn.nl/soap/soap_005/soap_005.wsdl"';
  l_method      := 'soap_005_operation';
  l_soap_action := 'http://helix.umcn.nl/soap/soap_005/soap_005.php';
  l_result_name := 'soap_005_response_part';

  l_request := soap_api.new_request(p_method       => l_method,
                                    p_namespace    => null);

  soap_api.add_parameter(p_request => l_request,
                         p_name    => 'handshake',
                         p_type    => 'xsd:string',
                         p_value   => 'B9B36F0D25AF02E3EAED4E20F7B359C4');

  l_response := soap_api.invoke(p_request => l_request,
                                p_url     => l_url,
                                p_action  => l_soap_action);

  l_return := soap_api.get_return_value(p_response  => l_response,
                                        p_name      => l_result_name,
                                        p_namespace => NULL);
return l_return;
END;

/

/
QUIT
