CREATE OR REPLACE FUNCTION "HELIX"."BEH_METAB_DD_VARIATIE"
( p_meet_id IN NUMBER
)/******************************************************************************
   NAME:       beh_metab_dd_variatie
   PURPOSE:    bepaal de variatie van een meetwaarde bij onderzoeksgroep DD,
               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        14-02-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_stof VARCHAR2(100) := null;
  v_ref VARCHAR2(100) := null;
  v_meet VARCHAR2(100) := null;
  v_return VARCHAR2(100) := null;

  cursor c_stof is
  select stof.omschrijving
     from bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mwst.code = 'DDMITOCHIP'
   ;
  cursor c_ref is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Ref.'
     and mwst.code = 'DDMITOCHIP'
   ;
  cursor c_meet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Meetw.'
     and mwst.code = 'DDMITOCHIP'
   ;
begin

  OPEN  c_stof;
  fetch c_stof into v_stof;
  If c_stof%notfound then
    v_stof := null;
  end if;

  OPEN  c_ref;
  fetch c_ref into v_ref;
  If c_ref%notfound then
    v_ref := null;
  end if;

  OPEN  c_meet;
  fetch c_meet into v_meet;
  If c_meet%notfound then
    v_meet := null;
  end if;

  v_return := v_stof||v_ref||'>'||v_meet;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END;

/

/
QUIT
