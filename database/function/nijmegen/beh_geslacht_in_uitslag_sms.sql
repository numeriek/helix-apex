CREATE OR REPLACE FUNCTION "HELIX"."BEH_GESLACHT_IN_UITSLAG_SMS" (p_onde_id IN NUMBER) return varchar is
/******************************************************************************
   NAME: beh_geslacht_in_uitslag_sms
   PURPOSE:   bepalen of er een vruegde sms  is verstuurd
             wordt gebruikt in de NPDN uitslgabrief

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        21-02-2012  Cootje Vermaat      1. Created this function.


******************************************************************************/

  v_return VARCHAR2(1) := NULL;
  CURSOR zwan_cur
  IS
    SELECT zwan.info_geslacht
    FROM   kgc_zwangerschappen zwan
    ,      kgc_foetussen foet
    ,      kgc_onderzoeken onde
    ,      beh_berichten beri
    ,      beh_bericht_types bety
    WHERE  onde.foet_id = foet.foet_id
    AND    foet.zwan_id = zwan.zwan_id
    AND    zwan.zwan_id = beri.entiteit_id
    AND    beri.bety_id = bety.bety_id
    and    beri.entiteit = 'ZWAN'
    and    beri.verzonden = 'J'
    AND    bety.code = 'SMS'
    AND    onde.onde_id = p_onde_id
    AND    NVL(zwan.vreugdebrief,CHR(2)) <> 'N'
    ;
BEGIN
  OPEN  zwan_cur;
  FETCH zwan_cur
  INTO  v_return;
  CLOSE zwan_cur;
  RETURN( v_return );
END;

/

/
QUIT
