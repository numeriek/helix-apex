CREATE OR REPLACE FUNCTION "HELIX"."BEH_ATWA_CHECK" (
    p_attr_id NUMBER,
    p_waarde  VARCHAR2,
    x_message OUT VARCHAR2)
  RETURN BOOLEAN
IS
  v_return BOOLEAN := true;
  v_count  NUMBER  :=0;
  CURSOR c_attr IS
    SELECT *
    FROM kgc_attributen
    WHERE attr_id = p_attr_id ;
	
  r_attr c_attr%rowtype;

  CURSOR c_rela (p_liszcode VARCHAR2) IS
    SELECT 1
    FROM kgc_relaties
    WHERE liszcode = p_liszcode ;
  r_rela c_rela%rowtype;

  CURSOR c_list(str VARCHAR2, separator VARCHAR2) IS
   SELECT *
    FROM TABLE(CAST(beh_alfu_00.explode(separator, str) AS beh_listtable)) ;
BEGIN
  OPEN c_attr;
  FETCH c_attr INTO r_attr;
  CLOSE c_attr;

  IF r_attr.code in ('DFT_SERVICE_PROVIDER', 'DFT_SUPERVISOR') THEN
    OPEN c_rela(p_waarde);
    FETCH c_rela INTO r_rela;
    IF c_rela%notfound THEN
      x_message := 'Ongeldige waarde ('|| p_waarde ||') voor attribuut: ' || r_attr.code;
    END IF;
    CLOSE c_rela;
  ELSIF r_attr.code                      = 'SER_ID' THEN
    IF (beh_alfu_00.is_nummer(p_waarde)) = 'N' THEN
      x_message                         := 'Ongeldige waarde ('|| p_waarde ||
      ') voor attribuut: ' || r_attr.code;
    END IF;
  ELSIF r_attr.code                        = 'ZORGMAIL' THEN
    if ( instr(upper(p_waarde), '.NL') < 1 or instr(upper(p_waarde), '@')  < 1 ) then
      x_message                         := 'Ongeldige waarde ('|| p_waarde ||
      ') voor attribuut: ' || r_attr.code;
    end if;
  ELSIF r_attr.code                        = 'KORTING' THEN
    IF r_attr.tabel_naam                   = 'KGC_ONDERZOEKEN' THEN
      IF (beh_alfu_00.is_nummer(p_waarde)) = 'N' THEN
        x_message                         := 'Ongeldige waarde ('|| p_waarde ||
        ') voor attribuut: ' || r_attr.code;
      END IF;
    ELSIF r_attr.tabel_naam = 'KGC_INSTELLINGEN' THEN
      FOR r_list IN c_list(p_waarde, ';')
      LOOP
        v_count                                                := v_count+1;
        IF v_count                                              = 1 THEN
          IF (beh_alfu_00.is_nummer(trim(r_list.column_value))) = 'N' THEN
            x_message                                          :=
            'Ongeldige waarde ('|| p_waarde || ') voor attribuut: ' ||
            r_attr.code;
          END IF;
        ELSE
          IF NOT
            (
              beh_alfu_00.is_datum_goed(trim(r_list.column_value))
            )
            THEN
            x_message := 'Ongeldige waarde ('|| p_waarde ||
            ') voor attribuut: ' || r_attr.code;
          END IF;
        END IF;
      END LOOP;
    END IF;
  END IF;
  IF x_message IS NOT NULL THEN
    RETURN false;
  ELSE
    RETURN true;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  x_message := SUBSTR(sqlerrm, 1, 4000);
  RETURN false;
END;
/

/
QUIT
