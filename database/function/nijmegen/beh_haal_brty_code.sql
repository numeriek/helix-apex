CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_BRTY_CODE" (p_brty_id NUMBER) RETURN VARCHAR2 IS
  v_return VARCHAR2(1000);
  CURSOR brty_cur
  IS
    SELECT code
    FROM   KGC_BRIEFTYPES
    WHERE  brty_id = p_brty_id
    ;
BEGIN
  OPEN  brty_cur;
  FETCH brty_cur INTO  v_return;
  CLOSE brty_cur;
  RETURN( v_return );
END beh_haal_brty_code;

/

/
QUIT
