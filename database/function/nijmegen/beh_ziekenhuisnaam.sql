CREATE OR REPLACE FUNCTION "HELIX"."BEH_ZIEKENHUISNAAM" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:      beh_ziekenhuisnaam
   PURPOSE:    ziekenhuis naam die getoond wordt in het rapport voor uitslagen van PRE-onderzoeksgroepen


   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        02-11-2010  Cootje Vermaat      1. Created this function.

   NOTES:


******************************************************************************/
  cursor c_atwa ( p_onde_id in number)is
    select atwa.waarde
    from   kgc_attributen         attr
    ,      kgc_attribuut_waarden  atwa
    ,      kgc_onderzoek_monsters onmo
    ,      kgc_onderzoeken onde
    ,      kgc_monsters           mons
    ,      kgc_herkomsten         herk
    where  attr.attr_id = atwa.attr_id
    and    atwa.id = herk.herk_id
    and    mons.herk_id = herk.herk_id
    and    onmo.mons_id = mons.mons_id
    and    onde.onde_id = onmo.onde_id
    and    onde.onde_id = p_onde_id
    and    attr.code = 'ZIEKENHUISNAAM'
    and    tabel_naam = 'KGC_HERKOMSTEN' ;

  r_atwa c_atwa%ROWTYPE;

  v_herk varchar2(100);

BEGIN
  v_herk := null;
  OPEN C_atwa(p_onde_id);
  FETCH c_atwa into r_atwa;
  IF c_atwa%FOUND THEN
    v_herk := r_atwa.waarde;
  ELSE
    select atwa.waarde into v_herk  -- geef de ziekenhuisnaam van Obst_nijm
        from kgc_attributen attr
        , kgc_attribuut_waarden atwa
        , kgc_herkomsten herk
        where atwa.attr_id = attr.attr_id
        and atwa.id = herk.herk_id
        and attr.code = 'ZIEKENHUISNAAM'
        and attr.tabel_naam= 'KGC_HERKOMSTEN'
        and herk.code = 'OBST_NIJM';
  END IF;
  CLOSE c_atwa;
  RETURN v_herk;
END;

/

/
QUIT
