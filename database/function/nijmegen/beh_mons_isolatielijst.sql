CREATE OR REPLACE FUNCTION "HELIX"."BEH_MONS_ISOLATIELIJST" (P_mons_id number) return varchar is
/******************************************************************************
   NAME: BEH_MONS_ISOLATIELIJST
   PURPOSE:    zoek de isolatielijst bij een monster
      om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12-04-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
    cursor c_isol (p_mons_id in varchar)
    is
    select isol.isolatielijst_nummer
    from bas_isolatielijsten isol
    where  isol.mons_id = p_mons_id
    ;

    r_isol c_isol%ROWTYPE;

  v_isol varchar2(1000);


BEGIN

  v_isol := null;

  IF  p_mons_id is not null  THEN

    OPEN c_isol(p_mons_id);
    FETCH c_isol INTO r_isol;
    IF c_isol%FOUND THEN
      v_isol := r_isol.isolatielijst_nummer;
    ELSE
      v_isol := null;
    END IF;
    CLOSE c_isol;

  END IF;

  RETURN v_isol;

END;

/

/
QUIT
