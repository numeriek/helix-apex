CREATE OR REPLACE FUNCTION "HELIX"."BEH_PERS_INFO_TAAL" (p_pers_id number, p_taal_id number) return varchar is
/******************************************************************************
   NAME:       beh_pers_info_taal
   PURPOSE:    geeft de persoons gegevens met een vertaling van het geslach
              wordt gebruikt in het rapport kgcuits51M_DD



   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        23-03-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_pers is
      SELECT achternaam
      ,      aanspreken
      ,      geboortedatum
      ,      zisnr
      ,      kgc_vert_00.vertaling('PERS_GESL',null,geslacht,p_taal_id) geslacht
      ,      BSN
      FROM   kgc_personen
      WHERE  pers_id = p_pers_id
      ;
  r_pers c_pers%ROWTYPE;

  v_return varchar2(200);
BEGIN
  OPEN c_pers;
  Fetch c_pers INTO r_pers;
  IF   c_pers%FOUND then
   v_return := r_pers.aanspreken||' ('||r_pers.geboortedatum||', '||r_pers.geslacht||') BSN: '||r_pers.BSN;
  ELSE
   v_return := 'Onbekend';
  END if;
  CLOSE c_pers;
  RETURN v_return;
END;

/

/
QUIT
