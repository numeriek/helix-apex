CREATE OR REPLACE FUNCTION "HELIX"."BEH_MEERLING" ( p_foet_id number) RETURN varchar2 IS
/******************************************************************************
   NAME:       beh_meerling
   PURPOSE:    bepalen of een foetus deel uit maakt van een meerlingzwangerschap
               ten behoeve van FEZ machtingingsrapport

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        21-8-2009   cootje vermaat     1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_meerling
      Sysdate:         21-8-2009
      Date and Time:   21-8-2009, 16:45:04, and 21-8-2009 16:45:04
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
cursor c_zwan ( p_foet_id number) is
       select zwan_id
       from kgc_foetussen
       where foet_id = p_foet_id
       ;
r_zwan c_zwan%rowtype;

cursor c_meerling (p_zwan_id number)is
       select count(*) aantal
       from kgc_foetussen
       where zwan_id = p_zwan_id
       group by zwan_id
       ;

r_meerling c_meerling %rowtype;
v_meerling  varchar2(35);


BEGIN
  open c_zwan( p_foet_id);
  fetch c_zwan into r_zwan;
  if c_zwan%found then
    open c_meerling(r_zwan.zwan_id);
    fetch c_meerling into r_meerling;
    if c_meerling%found then
      if r_meerling.aantal > 1 then
        v_Meerling := 'Meerling';
      else
        v_Meerling := 'Geen meerling';
      end if;
     else
      v_Meerling := 'Geen zwangerschap gevonden';
     end if;
     close c_meerling;
  else
    v_Meerling := 'Geen foetus gevonden';
  end if;
  close c_zwan;
     RETURN v_Meerling;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         NULL;
       WHEN OTHERS THEN
         -- Consider logging the error and then re-raise
         RAISE;
END beh_meerling;

/

/
QUIT
