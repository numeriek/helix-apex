CREATE OR REPLACE FUNCTION "HELIX"."BEH_DNA_DISCL_TXT"
( p_onde_id in  number
)/******************************************************************************
   NAME: BEH_DNA_DISCL_TXT
   PURPOSE:   bepaal of  de disclaimer label en tekst in de uitslagbrief verschijnt , op basis van het gebruikte protocol bij het onderzoek

               om te gebruiken bij het rapport kgcuits51DNA
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        28-nov-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(20);

  cursor c_stgr is
  select meti.meti_id
     from KGC_stoftestgroepen stgr
     ,    bas_metingen meti
     where meti.stgr_id = stgr.stgr_id
     and stgr.omschrijving like '%Robot protocol ION%'
     and stgr.code like '%_I'
     and meti.onde_id = p_onde_id
   ;
  r_stgr c_stgr%rowtype;


begin

  OPEN  c_stgr;
  fetch c_stgr into r_stgr;
  If c_stgr%notfound then
    v_return := 'N';
  else
    v_return := 'J';
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := null;

END;
/

/
QUIT
