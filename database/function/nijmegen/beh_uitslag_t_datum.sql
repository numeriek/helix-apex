CREATE OR REPLACE FUNCTION "HELIX"."BEH_UITSLAG_T_DATUM" (p_onde_id number) return date is
/******************************************************************************
   NAME:       beh_uitslag_t_datum
   PURPOSE:    zoek de datum van de tussentijdse uitslag brief bij een onderzoek
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05-02-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_uits is
    select uits.datum_uitslag
    from   kgc_uitslagen  uits
    , kgc_brieftypes brty
    , kgc_brieven brie
    where  uits.onde_id = p_onde_id
    and brie.uits_id= uits.uits_id
    and brie.brty_id = brty.brty_id
    and brty.code = 'UITSLAG_T'
    order by uits.uits_id
    ;

  r_uits c_uits%ROWTYPE;
  v_datum date;

BEGIN
  v_datum := null;
  OPEN c_uits;
  FETCH C_uits INTO r_uits;
  If c_uits%FOUND THEN
     v_datum := r_uits.datum_uitslag;
  END IF;
  CLOSE c_uits;
  RETURN v_datum;
END;

/

/
QUIT
