CREATE OR REPLACE FUNCTION "HELIX"."BEH_ZET_PDF_BEKEKEN"
/******************************************************************************************
  name:      beh_zet_PDF_bekeken
  purpose:   loggen dat een pdf bekeken is vanuit het epd
  results:   indien  ok,  dan J, anders N. Nadere in fo in x_message
  revisions:
  version date        author description
  ------- ----------- ------ ------------------------------------------
  1       23 nov 2010 CV     eerste versie
*******************************************************************************************/
( p_znummer               varchar2
, p_email                 varchar2
, p_uits_id               number
--, X_MESSAGE           out VARCHAR2
) return varchar2 IS

  v_viewer_extra_info varchar2(400);
  v_datum date;
  V_OK VARCHAR2(1);
  v_email varchar2(200);
BEGIN

  IF p_email is null then
       v_email := p_znummer||'@umcn.nl';
  ELSE v_email := p_email;
  END IF;

  v_viewer_extra_info := 'Znr='||p_znummer||';Email='||v_email||';' ;

dbms_output.put_line('voor insert');
  select sysdate into v_datum from dual;
  beh_raadplegen ( p_uits_id, 'UITS',v_datum, 'EPD', 'PDF',  null, null, p_znummer,  v_viewer_extra_info );
  V_OK := 'J';
  RETURN V_OK;
EXCEPTION
WHEN OTHERS THEN
--  x_message := SUBSTR(SQLERRM, 1, 4000);
  dbms_output.put_line(SUBSTR(SQLERRM, 1, 4000));
  V_OK := 'N';
  RETURN V_OK;
END;

/

/
QUIT
