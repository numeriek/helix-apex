CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_BU" (p_onde_id number) return varchar is
/******************************************************************************
   NAME:       beh_onde_bu
   PURPOSE:    bepaal of het onderzoek buitenlands is, dwz de aanvrager zit in het buitenland.
               om te gebruiken in select scripts, bv jaarverslag van DNA

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05-10-2010  Cootje Vermaat   Created this function.
   1.1        19-01-2016  Rene Brand       Uitvoering van functie verplaatst
                                           naar package beh_onde_buitenland_00
                                           zodat de functies los aan te roepen
                                           zijn.


******************************************************************************/

BEGIN
  return beh_onde_buitenland_00.beh_onde_bu(p_onde_id);
END;
/

/
QUIT
