CREATE OR REPLACE FUNCTION "HELIX"."BEH_KASP_ALLEL" (p_marker_name in  char, p_kolom CHAR)
return varchar
is

  cursor c_allel_1
  is
  select allel_1
  from beh_kasp_markers
  where marker_name = p_marker_name
  ;


  cursor c_allel_2
  is
  select allel_2
  from beh_kasp_markers
  where marker_name = p_marker_name
  ;

  cursor c_strand
  is
  select strand
  from beh_kasp_markers
  where marker_name = p_marker_name
  ;
  v_return varchar2(1) := null;

begin

  if upper(p_kolom) = 'ALLEL_1' then
    open c_allel_1;
    fetch  c_allel_1 into v_return;
    if c_allel_1%notfound then
      v_return := null;
    end if;
    close c_allel_1;

  elsif upper(p_kolom) = 'ALLEL_2' then
    open c_allel_2;
    fetch  c_allel_2 into v_return;
    if c_allel_2%notfound then
      v_return := null;
    end if;
    close c_allel_2;

  elsif upper(p_kolom) = 'STRAND' then
    open c_strand;
    fetch  c_strand into v_return;
    if c_strand%notfound then
      v_return := null;
    end if;
    close c_strand;

  end if;

  return v_return;

exception
  when others then
    v_return := null;
    return v_return;

end;
/

/
QUIT
