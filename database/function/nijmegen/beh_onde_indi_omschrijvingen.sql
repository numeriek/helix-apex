CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_INDI_OMSCHRIJVINGEN" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onde_indi_omschrijvingen
   PURPOSE:    onderzoeksindicatie in een tekst string zetten bij een onde_id
               om te gebruiken in select scripts en epd view

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        23-02-2010  Cootje Vermaat      1. Created this function.

   NOTES:


******************************************************************************/
  cursor c_indi is
    select indi.omschrijving
    from   kgc_onderzoek_indicaties onin
    ,      kgc_indicatie_teksten indi
    where  onin.onde_id = p_onde_id
    and    onin.indi_id = indi.indi_id
    ;

  r_indi c_indi%ROWTYPE;
  v_indi varchar2(10000);

BEGIN
  v_indi := null;
  OPEN c_indi;
  FETCH C_indi INTO r_indi;
  WHILE c_indi%FOUND
  LOOP
    IF v_indi is null
    THEN v_indi := r_indi.omschrijving;
    ELSE
      v_indi := v_indi || ', ' || r_indi.omschrijving;
    END IF;
    FETCH C_indi INTO r_indi;
  END LOOP;
  CLOSE c_indi;
  RETURN v_indi;
END;

/

/
QUIT
