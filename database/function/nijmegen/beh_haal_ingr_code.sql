CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_INGR_CODE" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       BEH_HAAL_INGR_CODE
   PURPOSE:  ophalen van indicatiegroep code op basis van onde_id om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2015  Marzena Golebiowska 1. Created this function.

******************************************************************************/

v_ingr varchar2(4000);

cursor c_ingr is
    select LISTAGG(INGR.CODE, ', ') WITHIN GROUP (ORDER BY INGR.CODE) AS INGRCODE
    from kgc_onderzoeken onde,
            kgc_onderzoek_indicaties onin,
            kgc_indicatie_teksten inte,
            kgc_indicatie_groepen ingr
    where ONDE.ONDE_ID = ONIN.ONDE_ID
        and ONIN.INDI_ID = INTE.INDI_ID
        and INTE.INGR_ID = INGR.INGR_ID
        and ONDE.ONDE_ID = p_onde_id
    group by ONDE.ONDE_ID
    ;

BEGIN
    open c_ingr;
    fetch c_ingr into v_ingr;
        if c_ingr%found then
            v_ingr := v_ingr;
        end if;
    close c_ingr;
    RETURN v_ingr;
END;
/

/
QUIT
