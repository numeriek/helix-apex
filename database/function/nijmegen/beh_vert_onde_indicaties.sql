CREATE OR REPLACE FUNCTION "HELIX"."BEH_VERT_ONDE_INDICATIES" (p_onde_id  number, p_taal_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_vert_onde_indicaties
   PURPOSE:    omschijrivingen van onderzoeksindicaties vertalen
               om te gebruiken in een report voor een uistlag brief

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16-11-2007  Cootje Vermaat      1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     behvert__onde_indicaties
      Sysdate:         24-02-2012
      Date and Time:   24-02-2012,
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  cursor c_indi is
    select decode( p_taal_id, null, indi.omschrijving,
                   1, indi.omschrijving,
                   kgc_vert_00.vertaling('INDI',INDI.INDI_ID,null,p_TAAL_ID)) INDICATIE
    from   kgc_onderzoek_indicaties onin
    ,      kgc_indicatie_teksten indi
    where  onin.onde_id = p_onde_id
    and    onin.indi_id = indi.indi_id
    ;

  r_indi c_indi%ROWTYPE;
  v_indi varchar2(1000);

BEGIN
  v_indi := null;
  OPEN c_indi;
  FETCH C_indi INTO r_indi;
  WHILE c_indi%FOUND
  LOOP
    IF v_indi is null
    THEN v_indi := r_indi.indicatie;
    ELSE
      v_indi := v_indi || ', ' || r_indi.indicatie;
    END IF;
    FETCH C_indi INTO r_indi;
  END LOOP;
  CLOSE c_indi;
  RETURN v_indi;
END;

/

/
QUIT
