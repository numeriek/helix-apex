CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDERZOEK_SMS_AFGEROND" (P_onde_id number) return varchar is
/******************************************************************************
   NAME:       beh_onderzoek_SMS_afgerond
   PURPOSE:    bepaal of het onderzoek is afgerond volgens de criteria voor het versturen van een SMS
      om te gebruiken in de view voor het versturen van sms berichten : beh_digipoli_vreugde_vw
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18-08-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
    cursor c_afro (p_onde_id in number)
    is
    select audl_id
    , datum_tijd
    from kgc_audit_log
    where tabel_naam   = 'KGC_ONDERZOEKEN'
    and kolom_naam   = 'AFGEROND'
    and aktie        = 'UPD'
    and waarde_oud   = 'N'
    and waarde_nieuw = 'J'
    and id = p_onde_id
    order by audl_id desc;

    r_afro c_afro%ROWTYPE;

    cursor c_onde( p_onde_id in number)
    is
    select afgerond
    ,      datum_autorisatie
    from kgc_onderzoeken
    where onde_id = p_onde_id
    ;
    r_onde c_onde%ROWTYPE;

  v_datum_tijd date;

  v_afgerond varchar2(1);
  v_datum_autorisatie date;
  v_vertraging number;

BEGIN
  v_datum_tijd        := null;
  v_datum_autorisatie := null;
  v_afgerond          := 'N';
  select to_number(standaard_waarde ) into v_vertraging
  from kgc_systeem_parameters
  where code = 'BEH_SMS_VERTRAGING_AUTORISATIE';
  v_vertraging := 60/v_vertraging;

  IF  p_onde_id is not null  THEN
    OPEN c_ONDE(p_onde_id);
    FETCH c_ONDE INTO r_ONDE;
    IF c_ONDE%FOUND THEN
      v_afgerond := r_onde.afgerond;
      v_datum_autorisatie := r_onde.datum_autorisatie;
    END IF;
    CLOSE c_onde;
  END IF;

  IF ( v_afgerond ='N'
       or
       ( v_afgerond = 'J' and v_datum_autorisatie <= to_date('08-06-2006','dd-mm-yyyy')
        )
      )
  THEN
    RETURN v_afgerond;

  ELSE

    IF  v_afgerond = 'J' and v_datum_autorisatie > to_date('08-06-2006','dd-mm-yyyy') THEN
      OPEN c_afro(p_onde_id);
      FETCH c_afro INTO r_afro;
      IF c_afro%FOUND THEN
        v_datum_tijd := r_afro.datum_tijd;
      END IF;
      CLOSE c_afro;
    END IF;

    IF v_datum_tijd is  not null THEN

      IF (    v_datum_tijd < (sysdate - (1/(24*v_vertraging)) )
          and v_afgerond = 'J'
         )
      THEN
        v_afgerond := 'J';
      ELSE
        v_afgerond := 'N';

      END IF;
    END IF;
  END IF;

  RETURN v_afgerond;

END;

/

/
QUIT
