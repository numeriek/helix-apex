CREATE OR REPLACE FUNCTION "HELIX"."BEH_DECL_VERRICHTINGCODE" (p_id number, p_entiteit varchar) return varchar is
/******************************************************************************
   NAME:      beh_decl_verrichtingcode
   PURPOSE:    zoek of eer een declaratie is met declareren = J en geef daar de verrichtingcode van
               om te gebruiken in select scripts,
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31-05-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
  v_return VARCHAR2(100):= null;

  v_ingr_id NUMBER := NULL;

  CURSOR c_decl
  IS
    SELECT verrichtingcode
    FROM   kgc_declaraties
    WHERE  id = p_id
    and    entiteit = p_entiteit
    and    declareren = 'J'
    ;

BEGIN
  IF ( p_id IS NOT NULL and p_entiteit is not null)
  THEN
    OPEN  c_decl;
    FETCH c_decl
    INTO  v_return;
    CLOSE c_decl;
  END IF;
  RETURN( v_return );
END;

/

/
QUIT
