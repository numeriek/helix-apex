CREATE OR REPLACE FUNCTION "HELIX"."BEH_DEBUG_MEETWAARDEN" (p_meet_id number) return varchar is
v_meet_id bas_meetwaarden.meet_id%type;
v_meetwaarde bas_meetwaarden.meetwaarde%type;
v_meetwaarde_num number;
v_monsternummer kgc_monsters.monsternummer%type;

cursor c_vw is
select mons.monsternummer, meet.meet_id, meet.meetwaarde
from kgc_monsters mons,
    bas_fracties frac,
    bas_metingen meti,
    bas_meetwaarden meet,
    kgc_kgc_afdelingen kafd
where mons.mons_id = frac.mons_id
and   frac.frac_id = meti.frac_id
and   meti.meti_id = meet.meti_id
and   frac.kafd_id = kafd.kafd_id
and    kafd.code = 'METAB'
and   meet_id = p_meet_id
;

begin
for r_vw in c_vw loop
    v_meet_id := r_vw.meet_id;
    v_meetwaarde := r_vw.meetwaarde;
    v_monsternummer := r_vw.monsternummer;
    select DECODE ( (REPLACE (TRANSLATE (TRIM (r_vw.meetwaarde),
                                            ',.0123456789',
                                            '000000000000'
                                 ),
                                 '0',
                                 NULL
                        )),
                      NULL,
                      (TO_NUMBER (REPLACE (TRIM (r_vw.meetwaarde), '.', ',')))
              )
     into v_meetwaarde_num
    from dual
    ;
end loop;
return 'G';
exception
 when others then
/*
 insert into beh_foute_meetwaarden values
            (v_monsternummer, v_meet_id, v_meetwaarde);
 commit;


    Helix.Check_jobs.post_mail

                'h.bourchi@sb.umcn.nl',
                'h.bourchi@sb.umcn.nl',
                v_monsternummer || ' => ' || to_char(v_meet_id) || ' => ' || v_meetwaarde,
                v_monsternummer || ' => ' || to_char(v_meet_id) || ' => ' || v_meetwaarde
               );
*/
return 'F';
end;

/

/
QUIT
