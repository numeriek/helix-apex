CREATE OR REPLACE FUNCTION "HELIX"."BEH_METAB_D_MITODNA"
( p_onde_id IN NUMBER
)/******************************************************************************
   NAME:      beh_metab_d_MITODNA
   PURPOSE:    bepaal of er een meting is met de stoftestgroep  D_MITODNA
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        21-02-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN varchar2
IS
  v_return varchar2(1);

  cursor c_meti is
  select meti.meti_id
  from bas_metingen meti
  , kgc_stoftestgroepen stgr
  where meti.stgr_id = stgr.stgr_id
  and stgr.code = 'D_MITODNA'
  and METI.ONde_ID = p_onde_id
  ;
  r_meti c_meti%rowtype;

begin

  v_return := 'N';

  OPEN c_meti;
  Fetch C_meti into r_meti;
  If c_meti%FOUND then
    v_return := 'J';
  else
    v_return :='N';
  end if;
  Close c_meti;

  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN v_return := 'N';

END;

/

/
QUIT
