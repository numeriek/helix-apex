CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_WLST_NR" (p_wlst_id NUMBER) RETURN VARCHAR2 IS
  v_return VARCHAR2(1000);
/******************************************************************************
   NAME:       BEH_HAAL_WLST_NR
   PURPOSE:    ophalen van werklijst_nummer

   REVISIONS:
   Ver        Date              Author                  Description
   ---------  ----------        ---------------        ------------------------------------
   1.0        05-03-2012    M.Golebiowska      1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     BEH_HAAL_WLST_NR
      Sysdate:         4-11-2008
      Date and Time:   4-11-2008, 18:05:41, and 4-11-2008 18:05:41
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  CURSOR wlst_id_cur
  IS
    SELECT werklijst_nummer
    FROM   KGC_WERKLIJSTEN
    WHERE  wlst_id = p_wlst_id
    ;
BEGIN
  OPEN  wlst_id_cur;
  FETCH wlst_id_cur INTO  v_return;
  CLOSE wlst_id_cur;
  RETURN( v_return );
END BEH_HAAL_WLST_NR;

/

/
QUIT
