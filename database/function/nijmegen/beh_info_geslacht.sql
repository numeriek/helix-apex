CREATE OR REPLACE FUNCTION "HELIX"."BEH_INFO_GESLACHT" (p_zwan_id IN NUMBER) return varchar is
/******************************************************************************
   NAME: beh_info_geslacht
   PURPOSE:   bepalen van de juiste sms tekst met bertekking tot de info_gelsacht
             wordt gebruikt in de vreugde SMS

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        07-03-2014  Cootje Vermaat      1. Created this function.


******************************************************************************/
  v_info_geslacht varchar2(1) := 'J';
  v_return VARCHAR2(160) := NULL;
  CURSOR c_zwan
  IS
    SELECT zwan.info_geslacht
    ,      foet.geslacht foet_geslacht
    ,      beh_onderzoek_indicaties(onde.onde_id) indicaties
    ,      onde.kafd_id
    ,      zwan.zwan_id
    ,      nvl(kgc_attr_00.waarde('KGC_INDICATIE_TEKSTEN','NIPT_INDICATIE',onin.indi_id), 'N') NIPT_INDICATIE
    FROM   kgc_zwangerschappen zwan
    ,      kgc_foetussen foet
    ,      kgc_onderzoeken onde
    ,      kgc_onderzoek_indicaties onin
    WHERE  onde.foet_id = foet.foet_id
    AND    onde.onde_id = onin.onde_id
    AND    foet.zwan_id = zwan.zwan_id
    AND    zwan.zwan_id = p_zwan_id
    AND    NVL(zwan.vreugdebrief,CHR(2)) <> 'N'
    ;

    r_zwan c_zwan%rowtype;

BEGIN
-- om het geslacht te kunnen tonen moet er aan drie voorwaarden worden voldaan:
-- 1.indicatie mag niet zijn NIPT...
-- 2.info geslacht moet J zijn
-- 3.bij alle foetussen in de zwangerschap moet geslacht bekend zijn

  OPEN  c_zwan;
  FETCH c_zwan   INTO  r_zwan;
  IF C_zwan%notfound then v_info_geslacht := 'O';
  else
      while (c_zwan%found and v_info_geslacht = 'J' ) LOOP
        IF r_zwan.info_geslacht <> 'J' then v_info_geslacht := 'N';
        elsif r_zwan.foet_geslacht not in ('M', 'V') then v_info_geslacht := 'N';
        elsif r_zwan.nipt_indicatie = 'J' then v_info_geslacht := 'N';
        end if;
        fetch c_zwan into r_zwan;
      end loop;
  end if;
  CLOSE c_zwan;
  if v_info_geslacht = 'O' then
    v_return := null;
  elsif v_info_geslacht = 'J' then
    v_return := REPLACE (
                            kgc_sypa_00.
                             standaard_waarde ('BEH_SMS_TEKST_VREUGDE_MET_GESL',
                                                     r_zwan.kafd_id,
                                                     null,
                                                     NULL
                                                    ),
                            '<waarde>',
                            kgc_vreugdebrief_00.info_geslacht (r_zwan.zwan_id));

  else
    v_return :=  kgc_sypa_00.
                         standaard_waarde ('BEH_SMS_TEKST_VREUGDE_ZND_GESL',
                                                 r_zwan.kafd_id,
                                                 null,
                                                 NULL
                                                );
  end if;

  RETURN( v_return );
  exception
    when no_data_found then
    v_return := null;
    RETURN( v_return );

    when others then
    v_return := null;
    RETURN( v_return );
END;
/

/
QUIT
