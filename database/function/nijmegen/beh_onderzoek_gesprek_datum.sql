CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDERZOEK_GESPREK_DATUM" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onderzoek_gesprek_datum
   PURPOSE:   datum eerste gesprek zoeken bij een onderzoek
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        20-10-2011  Cootje Vermaat      1. Created this function.

   NOTES:


******************************************************************************/
  cursor c_gesp (p_onde_id  in number)is
    select nvl(to_char(datum,'yyyy-mm-dd'),  '0000-00-00') datum
    from   kgc_gesprekken gesp
    where  gesp.onde_id = p_onde_id
    order by datum
    ;

  r_gesp c_gesp%ROWTYPE;
  v_datum varchar2(10);

BEGIN
  v_datum := null;
  OPEN  c_gesp(p_onde_id);
  FETCH c_gesp INTO r_gesp;

  IF c_gesp%FOUND THEN

     v_datum := r_gesp.datum;
  else
     v_datum:=  '0000-00-00';
  END IF;
  CLOSE  c_gesp;

  RETURN v_datum;
END;

/

/
QUIT
