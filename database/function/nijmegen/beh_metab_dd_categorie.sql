CREATE OR REPLACE FUNCTION "HELIX"."BEH_METAB_DD_CATEGORIE"
( p_meet_id IN NUMBER
)/******************************************************************************
   NAME:       beh_metab_dd_categorie
   PURPOSE:    bepaal de categorie van een meetwaarde bij onderzoeksgroep DD,
               indicatie D_MM en protocol D_MITODNA en meetw.structuur DDMITOCHIP
               om te gebruiken bij het rapport kgcuits51M_DD
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        14-02-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
RETURN VARCHAR2
IS
  v_return VARCHAR2(100);
  cursor c_mdet is
  select mdet.waarde
     from bas_meetwaarde_details mdet
     ,  bas_meetwaarden meet
     ,  kgc_stoftesten stof
     ,  kgc_meetwaardestructuur mwst
     where mdet.meet_id = meet.meet_id
     and meet.stof_id = stof.stof_id
     and stof.mwst_id = mwst.mwst_id
     and meet.meet_id = p_meet_id
     and mdet.prompt = 'Categorie'
     and mwst.code = 'DDMITOCHIP'
   ;

begin

  OPEN  c_mdet;
  fetch c_mdet into v_return;
  If c_mdet%notfound then
    v_return := '-';
  end if;


  RETURN( v_return );
  EXCEPTION
      WHEN others
      THEN
        v_return := '-';

END;

/

/
QUIT
