CREATE OR REPLACE FUNCTION "HELIX"."BEH_BESTANDEN_TO_STRING" (nt_in IN beh_bestanden_link, delimiter_in IN VARCHAR2 DEFAULT ',') RETURN VARCHAR2 IS
        v_idx PLS_INTEGER;
        v_best VARCHAR2(32767);
        v_dlm VARCHAR2(10);
        BEGIN
            v_idx := nt_in.FIRST;
            WHILE v_idx IS NOT NULL
                LOOP
                    v_best := v_best || v_dlm || nt_in(v_idx);
                    v_dlm := delimiter_in;
                    v_idx := nt_in.NEXT(v_idx);
                    EXIT WHEN  length(v_best) >= 4000-256;
                END LOOP;
            RETURN CASE WHEN length(v_best) >= 4000-256 THEN v_best || ' Er zijn meer bestanden, zie Helix.'ELSE v_best END;
        END beh_bestanden_to_string;
/

/
QUIT
