CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_DECL_PERS" (p_onde_id number) return varchar is
/******************************************************************************
   NAME:       beh_onde_decl_pers
   PURPOSE:    zoek de persoon bij het onderzoek die gedeclareerd gaat worden ,
               houdt rekening met dealetrnatieve delcartei persoon idmv het zisnr in het noatders bij hetonderzoek
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09-12-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/

  cursor c_pers (p_onde_id in number)
  is
    select pers_id
    ,      substr(nota_adres, 1, 20) nota_adres
    from   kgc_onderzoeken onde
    where  onde_id = p_onde_id
    ;

  r_pers c_pers%ROWTYPE;
  v_zisnr varchar2(20):= null;

BEGIN
  OPEN  c_pers(p_onde_id);
  FETCH c_pers INTO r_pers;
  IF    c_pers%FOUND THEN
    IF r_pers.nota_adres is not null then
      v_zisnr := r_pers.nota_adres;
      select zisnr into v_zisnr
        from kgc_personen
        where zisnr = v_zisnr;
    else
      select zisnr into v_zisnr
        from kgc_personen
        where pers_id = r_pers.pers_id;
    END if;
  END IF;
  CLOSE c_pers;
  IF v_zisnr is null then v_zisnr := 'ONB'; END IF;
  RETURN v_zisnr;
exception
  WHEN no_data_found then v_zisnr := 'ONB';
  WHEN too_many_rows then v_zisnr := 'ONB';
  WHEN others then v_zisnr := 'ONB';
END;

/

/
QUIT
