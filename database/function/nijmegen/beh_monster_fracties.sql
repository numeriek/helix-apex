CREATE OR REPLACE FUNCTION "HELIX"."BEH_MONSTER_FRACTIES" (p_mons_id in number) RETURN varchar2 IS
tmpVar NUMBER;
/******************************************************************************
   NAME:       beh_monster_fracties
   PURPOSE:   fracties bij een monster ophalen  te gebruiken in zoekvragen

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        13-11-2008       Cootje Vermaat   1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_monster_fracties
      Sysdate:         13-11-2008
      Date and Time:   13-11-2008, 9:53:01, and 13-11-2008 9:53:01
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
cursor c_frac is
    select frty.code ||': '||frac.fractienummer fractie
    from   bas_fracties frac
    ,      bas_fractie_types frty
    where  frac.mons_id= p_mons_id
    and    frac.frty_id = frty.frty_id
    ;

  r_frac c_frac%ROWTYPE;
  v_frac varchar2(100);

BEGIN
  v_frac := 'Geen fracties gevonden';
  OPEN c_frac;
  FETCH C_frac INTO r_frac;
  WHILE c_frac%FOUND
  LOOP
    IF v_frac ='Geen fracties gevonden'
    THEN
      v_frac := r_frac.fractie;
    ELSE
      v_frac := v_frac || ', ' || r_frac.fractie;
    END IF;
    FETCH C_frac INTO r_frac;
  END LOOP;
  CLOSE c_frac;
  RETURN v_frac;
END;

/

/
QUIT
