CREATE OR REPLACE FUNCTION "HELIX"."BEH_KOPPEL_DIGIPOLI"
/******************************************************************************
   NAME:       beh_koppel_digipoli
   PURPOSE:    bericht naar digipoli sturen tbv beschikbaar stellen Helix
               voor Nijmegen via specifiek SOAP-protocol.
   RESULT:     indien OK, dan TRUE, anders FALSE. Nadere info in x_message
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1          03-05-2010  RDE              Eerste versie
***************************************************************************** */
( p_bsn                     NUMBER
, p_zisnr                   VARCHAR2
, p_koppel                  BOOLEAN
, x_message             OUT VARCHAR2
) RETURN BOOLEAN IS
  c  utl_tcp.connection;  -- TCP/IP connection to the Web server
  ret_val pls_integer;
  v_digipolihost    VARCHAR2(200);
  v_msg             VARCHAR2(32000);
  v_msg_tmp         VARCHAR2(32000);
  v_header          VARCHAR2(32000);
  v_content         VARCHAR2(32000);
  v_str             VARCHAR2(2000);
  v_strAtt          VARCHAR2(2000);
  v_strVal          VARCHAR2(2000);
  v_strXML          VARCHAR2(32000);
  v_sep             VARCHAR2(2) := CHR(10);
  v_n               VARCHAR2(2) := CHR(10);
  v_r               VARCHAR2(2) := CHR(13);
  v_pos             NUMBER;
  v_koppel          VARCHAR2(10);
  v_XMLp            xmlparser.Parser := xmlparser.newParser;
  v_XMLdoc          xmldom.DOMDocument;
  v_XMLn1           xmldom.DOMNode;
  v_XMLNL           xmldom.DOMNodeList;
  v_ns_xsi          VARCHAR2(4000);
  v_ok              BOOLEAN;
  v_toon_debug      BOOLEAN := true;
  PROCEDURE toon_debug (p_text VARCHAR2) IS
  BEGIN
    IF v_toon_debug THEN
      dbms_output.put_line(SUBSTR(p_text, 1, 255));
    END IF;
  END;
BEGIN
  v_digipolihost := kgc_sypa_00.standaard_waarde( p_parameter_code => 'BEH_DIGIPOLI_HOST', p_kafd_id => NULL);
  -- open connection
  c := utl_tcp.open_connection(remote_host => v_digipolihost
                              ,remote_port =>  81
--                              ,tx_timeout  => 5
                              );
  IF p_koppel THEN
    v_koppel := 'true';
  ELSE
    v_koppel := 'false';
  END IF;

  v_content :=
    '<?xml version=''1.0'' encoding=''UTF-8''?>' || v_n ||
    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">' || v_sep ||
    '   <soapenv:Header/>' || v_sep ||
    '   <soapenv:Body>' || v_sep ||
    '      <tem:CoupleUnCouplePatientToRole>' || v_sep ||
    '         <tem:koppelen>' || v_koppel || '</tem:koppelen>' || v_sep ||
    '         <tem:bsnNr>' || p_bsn || '</tem:bsnNr>' || v_sep ||
    '         <tem:radboudNr>' || p_zisnr || '</tem:radboudNr>' || v_sep ||
    '         <tem:digipoli>PND</tem:digipoli>' || v_sep ||
    '         <tem:mijnDossier>false</tem:mijnDossier>' || v_sep ||
    '      </tem:CoupleUnCouplePatientToRole>' || v_sep ||
    '   </soapenv:Body>' || v_sep ||
    '</soapenv:Envelope>';
  v_header := 'POST /PatientAdministrationService/PatientAdministrationService.svc HTTP/1.0' || v_sep
    || 'Accept-Encoding: gzip,deflate' || v_sep
    || 'Content-Type: text/xml;charset=UTF-8' || v_sep
    || 'SOAPAction: "http://tempuri.org/IPatientAdministration/CoupleUnCouplePatientToRole"' || v_sep
--    || 'Host: umcdpprof-tst01.umcn.nl:81' || v_sep
    || 'Host: ' || v_digipolihost || v_sep
    || 'Content-Length: ' || LENGTH(v_content) || v_sep;
  v_msg := v_header || v_sep || v_content;
  v_msg_tmp := replace(replace(v_msg, chr(10), '[\n]'|| chr(10)), chr(13), '[\r]');
  toon_debug(substr(v_msg_tmp, 1, 255));
  toon_debug(substr(v_msg_tmp, 256, 255));
  ret_val := utl_tcp.write_line(c, v_msg); -- send HTTP request
  -- terugkomende gegevens ophalen:
  BEGIN
    v_strXML := utl_tcp.get_text(c, 32000);
    toon_debug('lengte: ' || to_char(length(v_strXML)));
    toon_debug(substr(v_strXML,1,255));
    toon_debug(substr(v_strXML,256,255));
    toon_debug(substr(v_strXML,511,255));
    toon_debug(substr(v_strXML,766,255));

  EXCEPTION
    WHEN utl_tcp.end_of_input THEN
      NULL; -- end of input
  END;
  utl_tcp.close_connection(c);
  v_ok := TRUE;
--  v_XMLp := xmlparser.newParser;
  xmlparser.setValidationMode(v_XMLp, FALSE);

  -- eerste stuk eraf:
  v_pos := INSTR(v_strXML, '<');
  IF v_pos > 0 THEN
    v_strXML := SUBSTR(v_strXML, v_pos);
  END IF;
  -- parse:
  toon_debug('7-parse');
  xmlparser.parseBuffer(v_XMLp, v_strXML);
  -- lees document
  toon_debug('8-lees doc');
  v_XMLdoc := xmlparser.getDocument(v_XMLp);
  v_XMLn1 := xmldom.makeNode(xmldom.getDocumentElement(v_XMLdoc));
  -- gevonden: s:Envelope
  toon_debug('9-envelope=' || xmldom.getNodeName(v_XMLn1));
  -- namespaces - nodig en ophalen!
  v_ns_xsi := xmldom.getAttribute(xmldom.makeElement(v_XMLn1), 's', 'xmlns');
  toon_debug('10-ns-attr=' || v_ns_xsi);
  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
  -- gevonden: s:Body
  toon_debug('11-body=' || xmldom.getNodeName(v_XMLn1));
  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
  -- gevonden: CoupleUnCouplePatientToRoleResponse
  toon_debug('12-respons=' || xmldom.getNodeName(v_XMLn1));
  v_XMLn1 := xmldom.getFirstChild(v_XMLn1);
  -- gevonden: CoupleUnCouplePatientToRoleResult
  toon_debug('13-result=' || xmldom.getNodeName(v_XMLn1));
  v_str := xmldom.getNodeName(v_XMLn1);
  -- onderliggende message en status ophalen
  v_XMLNL := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), 'Message');
  -- 1e ophalen; aanvullende navigatie naar node met de waarde!!!
  x_message := SUBSTR(xmldom.getNodeValue(xmldom.getFirstChild(xmldom.item(v_XMLNL, 0))), 1, 2000);
  toon_debug('14-message=' || SUBSTR(x_message, 1, 240));
  v_XMLNL := xmldom.getChildrenByTagName(xmldom.makeElement(v_XMLn1), 'Status');
  v_str := SUBSTR(xmldom.getNodeValue(xmldom.getFirstChild(xmldom.item(v_XMLNL, 0))), 1, 255);
  toon_debug('15-status=' || SUBSTR(v_str, 1, 240));
  IF UPPER(v_str) in ('SUCCESS', 'ALREADYCOUPLED') THEN
    v_ok := TRUE;
  ELSE
    v_ok := FALSE;
  END IF;

  IF NOT xmldom.isNull(v_XMLdoc) THEN
    xmldom.freeDocument(v_XMLdoc);
  END IF;
  xmlparser.freeParser(v_XMLp);
  RETURN v_ok;
EXCEPTION
WHEN OTHERS THEN
  x_message := SUBSTR(SQLERRM, 1, 4000);
  utl_tcp.close_all_connections;
  IF NOT xmldom.isNull(v_XMLdoc) THEN
    xmldom.freeDocument(v_XMLdoc);
  END IF;
  xmlparser.freeParser(v_XMLp);
  RETURN FALSE;
END;

/

/
QUIT
