CREATE OR REPLACE FUNCTION "HELIX"."BEH_ZOEK_LOCATIE" (P_onde_id number) return varchar is
/******************************************************************************
   NAME:       beh_zoek_laocatie
   PURPOSE:    zoek de locatienaam van het ziekenhuis bij een herkomstcode van prenataal
               op basis van de onde_id van het onderzoek
      om te gebruiken in de uitslagbrief van prenataal

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        23-03-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
    cursor c_herk (p_onde_id in varchar)
    is
    select herk.code
    from kgc_onderzoek_monsters onmo
    ,    kgc_monsters mons
    ,    kgc_herkomsten herk
    where  onmo.onde_id = p_onde_id
    and    onmo.mons_id = mons.mons_id
    and    mons.herk_id = herk.herk_id(+)
    ;

    r_herk c_herk%ROWTYPE;

  v_herk varchar2(1000);


BEGIN

  v_herk := null;

  IF  p_onde_id is not null  THEN

    OPEN c_herk(p_onde_id);
    FETCH c_herk INTO r_herk;
    IF c_herk%FOUND THEN
      v_herk := r_herk.code;
    END IF;
    CLOSE c_herk;

  END IF;

  RETURN v_herk;

END;

/

/
QUIT
