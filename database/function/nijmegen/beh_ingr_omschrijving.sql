CREATE OR REPLACE FUNCTION "HELIX"."BEH_INGR_OMSCHRIJVING" (p_onde_id number) return varchar is
/******************************************************************************
   NAME:       ingr_omschrijving
   PURPOSE:    zoek de omschijriving van de indicatiegroep bij een onderzoek ,
               om te gebruiken in select scripts,
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31-05-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  v_return VARCHAR2(100):= null;

  v_ingr_id NUMBER := NULL;

  CURSOR ingr_cur
  IS
    SELECT omschrijving
    FROM   kgc_indicatie_groepen
    WHERE  ingr_id = v_ingr_id
    ;

BEGIN
  v_ingr_id := beh_ingr_id(p_onde_id);
  IF ( v_ingr_id IS NOT NULL )
  THEN
    OPEN  ingr_cur;
    FETCH ingr_cur
    INTO  v_return;
    CLOSE ingr_cur;
  END IF;
  RETURN( v_return );
END;

/

/
QUIT
