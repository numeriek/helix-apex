CREATE OR REPLACE FUNCTION "HELIX"."BEH_DATUM_CONVERSIE_EPIC" (p_datum date) return varchar is
/******************************************************************************
   NAME:       BEH_DATUM_CONVERSIE_EPIC"
   PURPOSE:    bepaal of de datum valt na datum x zodta bekeen kan worden of
               de extra zin moet worden toegevoegd bij het maken van uitslagen
               ten behoeve van de conversie  voor epic

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0  16-08-2013  Cootje Vermaat      1. Created this function.


******************************************************************************/

  v_voor varchar2(1);

BEGIN
  If p_datum <= to_date('04-10-2010','dd-mm-yyyy') THEN
    v_voor:= 'J';
  ELSE
    v_voor:= 'N';
  END IF;
  RETURN v_voor;
END;
/

/
QUIT
