CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_MONSTERNR" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       BEH_HAAL_MONSTERNR
   PURPOSE:  ophalen van monsternummer op basis van onde_id om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04-02-2015  Marzena Golebiowska 1. Created this function.

******************************************************************************/

v_mons varchar2(4000);

cursor c_mons is
    select LISTAGG(MONS.MONSTERNUMMER, ', ') WITHIN GROUP (ORDER BY MONS.MONSTERNUMMER ) AS MONSTERNR
    from kgc_onderzoek_monsters onmo,
            kgc_monsters mons
    where ONMO.MONS_ID = MONS.MONS_ID
        and ONMO.ONDE_ID = p_onde_id
    group by onmo.ONDE_ID
    ;

BEGIN
    open c_mons;
    fetch c_mons into v_mons;
        if c_mons%found then
            v_mons := v_mons;
        end if;
    close c_mons;
    RETURN v_mons;
END;
/

/
QUIT
