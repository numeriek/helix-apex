CREATE OR REPLACE FUNCTION "HELIX"."BEH_MG_MONSTER_INDICATIES" (p_mons_id number) RETURN varchar2 IS
/******************************************************************************
   NAME:       beh_mg_monster_indicaties
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        7-1-2009          1. Created this function.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_mg_monster_indicaties
      Sysdate:         7-1-2009
      Date and Time:   7-1-2009, 12:24:57, and 7-1-2009 12:24:57
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
 cursor c_indi is
    select indi.code
    from   kgc_monster_indicaties onin
    ,      kgc_indicatie_teksten indi
    where  onin.mons_id = p_mons_id
    and    onin.indi_id = indi.indi_id
    ;

  r_indi c_indi%ROWTYPE;
  v_indi      varchar2(100);
  v_proband   varchar2(10)   := null;
  v_aangedaan varchar2(10)   := null;

BEGIN
  v_indi := null;
  OPEN c_indi;
  FETCH C_indi INTO r_indi;
  WHILE c_indi%FOUND
  LOOP
    IF R_indi.code in ('P', 'R', 'C')
    THEN
      v_proband := r_indi.code;
    ELSIF R_indi.code in ('A', 'A?', 'D','NA','NA?')
    THEN
      v_aangedaan := r_indi.code;
    ELSIF v_indi is null
    THEN
      v_indi := r_indi.code;
    ELSE
      v_indi := v_indi || ', ' || r_indi.code;
    END IF;
    FETCH C_indi INTO r_indi;
  END LOOP;
  CLOSE c_indi;
  v_indi := v_indi||';'||v_proband||';'||v_aangedaan;
  RETURN v_indi;
END;

/

/
QUIT
