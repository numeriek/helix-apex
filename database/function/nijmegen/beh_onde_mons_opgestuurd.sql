CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_MONS_OPGESTUURD" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onde_mons_opgestuurd
   PURPOSE:    bij alle fracties betrokken bij een onderzoek nagaan of het isolatie proces de extra attwa MONS_NAAR_LOCATIE_ELDERS heeft
               om te gebruiken in ngs uitslagbrief kgcuits51dna.rdf

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        05-07-2013  Cootje Vermaat      1. Created this function.

   NOTES:


******************************************************************************/
  cursor c_atwa is
    select onmo.onde_id
    , frac.fractienummer
    , kgc_attr_00.waarde('KGC_PROCESSEN','MONS_NAAR_LOCATIE_ELDERS',isol.proc_id) waarde
--    , kgc_attr_00.waarde('KGC_PROCESSEN','MONS_NAAR_LOCATIE_ELDERS',isol.proc_id) MONS_NAAR_LOCATIE_ELDERS
    from bas_fracties frac
    , KGC_MONSTERS mons
    , kgc_onderzoek_monsters onmo
    , kgc_onderzoeken onde
    , kgc_onderzoeksgroepen ongr
    , bas_isolatielijsten isol
    where frac.monS_id = mons.mons_id
    and MONS.MONS_ID = onmo.mons_id
    and frac.frac_id = isol.frac_id
    and onmo.onde_id = onde.onde_id
    and onde.ongr_id = ongr.ongr_id
--    and onmo.onde_id = 1052644
    and onmo.onde_id = p_onde_id
    and ongr.code = 'NGS'
    ;
  r_atwa c_atwa%ROWTYPE;
  v_atwa varchar2(200);

BEGIN
  v_atwa := null;
  OPEN c_atwa;
  FETCH C_atwa INTO r_atwa;
  WHILE c_atwa%FOUND
  LOOP
    IF upper(r_atwa.waarde) = 'J'
    THEN
      v_atwa := upper(r_atwa.waarde);
    END IF;
    FETCH C_atwa INTO r_atwa;
  END LOOP;
  CLOSE c_atwa;
  RETURN v_atwa;
END;
/

/
QUIT
