CREATE OR REPLACE FUNCTION "HELIX"."DATUM"
  ( p_dat in varchar2
  , p_tijdaanw boolean := false
  )
  return date
  is
    v_return date;
  begin
    begin
      if p_tijdaanw then
        v_return := to_date( p_dat, 'dd-mm-yyyy hh24:mi' );
      else
        v_return := to_date( p_dat, 'dd-mm-yyyy' );
      end if;
    exception
      when others then
        v_return := null;
    end;
    return( v_return );
end datum;

/

/
QUIT
