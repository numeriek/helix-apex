CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDERZOEK_INDICATIES" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onderzoek_indicaties
   PURPOSE:    onderzoeksindicatie in een tekst string zetten bij een onde_id
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16-11-2007  Cootje Vermaat      1. Created this function.
   1.1        20-09-2016 cootje vermaat       vergroot: v_inid_id van 200 naar 400

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     beh_onderzoek_indicaties
      Sysdate:         16-11-2007
      Date and Time:   16-11-2007, 8:47:02, and 16-11-2007 8:47:02
      Username:         (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
  cursor c_indi is
    select indi.code
    from   kgc_onderzoek_indicaties onin
    ,      kgc_indicatie_teksten indi
    where  onin.onde_id = p_onde_id
    and    onin.indi_id = indi.indi_id
    ;

  r_indi c_indi%ROWTYPE;
  v_indi varchar2(400);

BEGIN
  v_indi := 'Geen indicaties gevonden';
  OPEN c_indi;
  FETCH C_indi INTO r_indi;
  WHILE c_indi%FOUND
  LOOP
    IF v_indi ='Geen indicaties gevonden'
    THEN v_indi := r_indi.code;
    ELSE
      v_indi := v_indi || ', ' || r_indi.code;
    END IF;
    FETCH C_indi INTO r_indi;
  END LOOP;
  CLOSE c_indi;
  RETURN v_indi;
END;
/

/
QUIT
