CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDERZOEK_METINGRESULTATEN" (p_onde_id number) return varchar2 is
/******************************************************************************
   NAME:       beh_onderzoek_metingresultaten
   PURPOSE:    stoftestgroepcodes en resultaten van metingen in een tekst string zetten bij een onde_id
               om te gebruiken in select scripts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        20-05-2009  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_meti is
    select stgr.code||': '||replace(meti.conclusie, chr(10), ' ') code
    from   bas_metingen meti
    , kgc_stoftestgroepen stgr
    where  meti.onde_id = p_onde_id
    and    meti.stgr_id = stgr.stgr_id
    ;

  r_meti c_meti%ROWTYPE;
  v_meti varchar2(2000);

BEGIN
  v_meti := 'Geen metingen gevonden';
  OPEN c_meti;
  FETCH C_meti INTO r_meti;
  WHILE c_meti%FOUND
  LOOP
    IF v_meti ='Geen metingen gevonden'
	THEN v_meti := r_meti.code;
	ELSE
      v_meti := v_meti || chr(10) || r_meti.code;
    END IF;
    FETCH C_meti INTO r_meti;
  END LOOP;
  CLOSE c_meti;
  RETURN v_meti;
END;

/

/
QUIT
