CREATE OR REPLACE FUNCTION "HELIX"."BEH_DRAGON_FGC" (p_schema in varchar2, p_object in varchar2)
    return varchar2 as
    v_where varchar2 (20);
begin
    if user = 'DRAGON' then
        case p_object
        when 'BAS_FRACTIES'                 then v_where := '1=1';
        when 'BAS_FRACTIE_TYPES'            then v_where := '1=1';
        when 'KGC_ATTRIBUTEN'               then v_where := '1=1';
        when 'KGC_ATTRIBUUT_WAARDEN'        then v_where := '1=1';
        when 'KGC_FAMILIES'                 then v_where := '1=1';
        when 'KGC_FAMILIE_LEDEN'            then v_where := '1=1';
        when 'KGC_INDICATIE_TEKSTEN'        then v_where := '1=1';
        when 'KGC_KGC_AFDELINGEN'           then v_where := '1=1';
        when 'KGC_MONSTERS'                 then v_where := '1=1';
        when 'KGC_NOTITIES'                 then v_where := '1=1';
        when 'KGC_NUMMERSTRUCTUUR'          then v_where := '1=1';
        when 'KGC_NUST_SAMENSTELLING'       then v_where := '1=1';
        when 'KGC_ONDERZOEKEN'              then v_where := '1=1';
        when 'KGC_ONDERZOEKSGROEPEN'        then v_where := '1=1';
        when 'KGC_ONDERZOEK_KOPIEHOUDERS'   then v_where := '1=1';
        when 'KGC_ONDERZOEK_MONSTERS'       then v_where := '1=1';
        when 'KGC_PERSONEN'                 then v_where := '1=1';
        when 'KGC_RELATIES'                 then v_where := '1=1';
        when 'KGC_SYSTEEM_PARAMETERS'       then v_where := '1=1';
        when 'KGC_SYSTEEM_PAR_WAARDEN'      then v_where := '1=1';
        else                                     v_where := '1=2';
        end case;
    elsif user = 'POEMA' then
        case p_object
        when 'BAS_FRACTIES'                 then v_where := '1=1';
        when 'BAS_MEETWAARDEN'              then v_where := '1=1';
        when 'BAS_MEETWAARDE_DETAILS'       then v_where := '1=1';
        when 'BAS_METINGEN'                 then v_where := '1=1';
        when 'KGC_INDICATIE_TEKSTEN'        then v_where := '1=1';
        when 'KGC_KGC_AFDELINGEN'           then v_where := '1=1';
        when 'KGC_MATERIALEN'               then v_where := '1=1';
        when 'KGC_MONSTERS'                 then v_where := '1=1';
        when 'KGC_ONDERZOEKEN'              then v_where := '1=1';
        when 'KGC_ONDERZOEKSGROEPEN'        then v_where := '1=1';
        when 'KGC_ONDERZOEK_INDICATIES'     then v_where := '1=1';
        when 'KGC_ONDERZOEK_MONSTERS'       then v_where := '1=1';
        when 'KGC_PERSONEN'                 then v_where := '1=1';
        when 'KGC_RELATIES'                 then v_where := '1=1';
        when 'KGC_STOFTESTEN'               then v_where := '1=1';
        when 'KGC_STOFTESTGROEPEN'          then v_where := '1=1';
        else                                     v_where := '1=2';
        end case;
    else
        v_where := '1=1';
    end if;
  return (v_where);
end beh_dragon_fgc;

/

/
QUIT
