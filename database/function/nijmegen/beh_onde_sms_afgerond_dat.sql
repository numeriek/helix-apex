CREATE OR REPLACE FUNCTION "HELIX"."BEH_ONDE_SMS_AFGEROND_DAT" (P_zwan_id number) return varchar is
/******************************************************************************
   NAME:       beh_onde_SMS_afgerond_dat
   PURPOSE:    bepaal het moment dat bij deze zwangerschap het laatste prenatale onderzoek is afgerond
      om te gebruiken in het script sms_vreugdebrief_rapportage.sql
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        23-08-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/


  cursor c_audl ( P_zwan_id in number)is
  select to_char(max(datum_tijd), 'dd-mm-yyyy-HH24:mi:ss') moment
    from kgc_audit_log audl
    , kgc_onderzoeken onde
    , kgc_foetussen foet
    , kgc_onderzoeksgroepen ongr
    WHERE onde.ongr_id = ongr.ongr_id
    AND   ongr.code LIKE 'PRE%'
    AND   onde.foet_id = foet.foet_id
    AND   foet.zwan_id = p_zwan_id
    and   audl.tabel_naam   = 'KGC_ONDERZOEKEN'
    and   audl.kolom_naam   = 'AFGEROND'
    and   audl.aktie        = 'UPD'
    and   audl.waarde_oud   = 'N'
    and   audl.waarde_nieuw = 'J'
    and   audl.id = onde.onde_id
    ;
  r_audl c_audl%rowtype;

  v_datum_tijd varchar2(20);

BEGIN

  OPEN C_audl(p_zwan_id);
  FETCH C_audl INTO R_audl;
  IF C_audl%FOUND THEN
    v_datum_tijd := R_audl.moment;
  END IF;
  CLOSE C_audl;

    return  v_datum_tijd;

END;

/

/
QUIT
