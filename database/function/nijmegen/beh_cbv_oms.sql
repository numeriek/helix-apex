CREATE OR REPLACE FUNCTION "HELIX"."BEH_CBV_OMS" (p_verrichtingcode varchar) return varchar is
/******************************************************************************
   NAME:       beh_cbv_omschrijving
   PURPOSE:    zoek de beschrijving  van de CBVcode bij een verrichtingcode ,
               om te gebruiken in select scripts,
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        01-03-2011  Cootje Vermaat      1. Created this function.


******************************************************************************/
  v_return VARCHAR2(100):= null;

  cursor c_besc is
    select cawa.beschrijving
    from kgc_categorie_waarden cawa
    , kgc_categorieen cate
    , kgc_categorie_types caty
    where cawa.cate_id = cate.cate_id
    and cate.caty_id = caty.caty_id
    and caty.code = 'VIC'
    and cawa.waarde = p_verrichtingcode
    ;

BEGIN

  IF ( p_verrichtingcode  IS NOT NULL )
  THEN
    OPEN  c_besc;
    FETCH c_besc
    INTO  v_return;
    CLOSE c_besc;
  END IF;
  RETURN( v_return );

END;

/

/
QUIT
