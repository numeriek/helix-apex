CREATE OR REPLACE FUNCTION "HELIX"."BEH_ZOEK_DECL_AANV" (p_decl_id number) return varchar is
/******************************************************************************
   NAME:       beh_zoek_decl_aanv
   PURPOSE:    zoek de aanvrager  bij een declaratie , alleen als de aanvrager in het radboud zit
               om te gebruiken in select scripts, bv jaarverslag van metab

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        17-03-2010  Cootje Vermaat      1. Created this function.


******************************************************************************/
  cursor c_decl  (p_decl_id in number)
  is
    select entiteit
    ,      id
    from   kgc_declaraties
    where  decl_id = p_decl_id
    ;

  r_decl c_decl%ROWTYPE;

  cursor c_onde (p_id in number)
  is
    select rela_id
    from   kgc_onderzoeken
    where  onde_id = p_id
    ;

  r_onde c_onde%ROWTYPE;

  cursor c_onbe  (p_id in number)
  is
    select onde.rela_id
    from   kgc_onderzoeken onde
    ,      kgc_onderzoek_betrokkenen onbe
    where  onbe.onbe_id = p_id
    and    onbe.onde_id = onde.onde_id
    ;

  r_onbe c_onbe%ROWTYPE;

  cursor c_meti  (p_id in number)
  is
    select onde.rela_id
    from   kgc_onderzoeken onde
    ,      bas_metingen meti
    where  meti.meti_id = p_id
    and    meti.onde_id = onde.onde_id
    ;

  r_meti c_meti%ROWTYPE;

  cursor c_rela  (p_rela_id in number)
  is
    select nvl(spec.code, 'ONB') spec_code
    ,      nvl(spec.omschrijving, 'ONB') spec_oms
    ,      nvl(afdg.code, 'ONB') afdg_code
    from   kgc_relaties rela
    ,      kgc_specialismen spec
    ,      kgc_instellingen inst
    ,      kgc_afdelingen afdg
    where  rela.spec_id = spec.spec_id(+)
    and    rela.inst_id = inst.inst_id(+)
    and    rela.afdg_id = afdg.afdg_id(+)
    and    rela.rela_id = p_rela_id
    and    ( nvl(UPPER(inst.code), 'ONB')  in ('UMCN','AZN','NYMESTRADB','NYMEUMCNNY')
            or instr(upper(nvl(rela.adres,'ONB')),'ALHIER') > 0
            )
    ;

  r_rela c_rela%ROWTYPE;

  v_rela_id number;
  v_tekst  varchar2(100);

BEGIN
  v_rela_id := null;
  v_tekst   := null;
  OPEN c_decl(p_decl_id);
  Fetch c_decl INTO r_decl;
  IF     r_decl.entiteit = 'ONDE' THEN
    OPEN  c_onde(r_decl.id);
    FETCH c_onde INTO r_onde;
    IF    c_onde%FOUND THEN v_rela_id := r_onde.rela_id; END IF;
    CLOSE c_onde;
  ELSIF  r_decl.entiteit = 'ONBE' THEN
    OPEN  c_onbe(r_decl.id);
    FETCH c_onbe INTO r_onbe;
    IF    c_onbe%FOUND THEN v_rela_id := r_onbe.rela_id; END IF;
    CLOSE c_onbe;
  ELSIF  r_decl.entiteit = 'METI' THEN
    OPEN  c_meti(r_decl.id);
    FETCH c_meti INTO r_meti;
    IF    c_meti%FOUND THEN v_rela_id := r_meti.rela_id; END IF;
    CLOSE c_meti;
  ELSE
    null;
  END IF;
  CLOSE c_decl;
  IF v_rela_id is not null THEN
    OPEN c_rela (v_rela_id);
    FETCH c_rela INTO r_rela;
    IF c_rela%FOUND THEN
      v_tekst := r_rela.afdg_code||','||r_rela.spec_oms;
    END IF;
    CLOSE c_rela;
  END IF;
  RETURN v_tekst;
END;

/

/
QUIT
