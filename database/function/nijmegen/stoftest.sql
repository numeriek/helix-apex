CREATE OR REPLACE FUNCTION "HELIX"."STOFTEST"
  ( p_code in varchar2
  )
  return number
  is
    v_kafd_id number := kgc_uk2pk.kafd_id( 'DNA' );
    l_code varchar2(20) := upper( p_code );
    l_return number;
  begin
    l_return := kgc_uk2pk.stof_id(v_kafd_id,l_code);
    if ( l_return is null )
    then
      -- zonder punten
      l_code := replace(l_code,'.',null);
      l_return := kgc_uk2pk.stof_id(v_kafd_id,l_code);
      if ( l_return is null )
      then
        -- extra 0: 421203010 ipv 42120310
        l_code := substr( l_code, 1, 6 )||'0'||substr( l_code, -2 );
        l_return := kgc_uk2pk.stof_id(v_kafd_id,l_code);
      end if;
      if ( l_return is null )
      then
        -- 4 ipv 0 op eerste positie
        l_code := '4'||substr( l_code, 2 );
        l_return := kgc_uk2pk.stof_id(v_kafd_id,l_code);
      end if;
    end if;
    return( l_return );
  end stoftest;

/

/
QUIT
