CREATE OR REPLACE FUNCTION "HELIX"."BEH_HAAL_PROTOCOL_CODE"
-- wordt gebruikt in de entiteiten tabel bij de entiteit metingen ten behoeve van de notities
-- Cootje Vermaat
-- 22 -02 2013
--
--
--

  ( p_meti_id IN NUMBER
  )
RETURN varchar2 IS

  cursor c_meti (p_meti_id in number)
  is
  select code
  from KGC_STOFTESTGROEPEN stgr
  , bas_metingen meti
  where meti.stgr_id = stgr.stgr_id
  and meti_id = p_meti_id
  ;

  V_PROTOCOL kgc_stoftestgroepen.code%type;

BEGIN

  open c_meti (p_meti_id);
  fetch c_meti into V_PROTOCOL;
  close c_meti;

  return (V_PROTOCOL);

END;
/

/
QUIT
