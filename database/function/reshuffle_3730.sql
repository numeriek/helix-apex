CREATE OR REPLACE FUNCTION "HELIX"."RESHUFFLE_3730"
 (P_TRVU_ID IN number
 )
 RETURN VARCHAR2
 IS

/******************************************************************************
   NAME:       reshuffle_3730
   PURPOSE:    bepaal afwijkend well_nummer voor robotstraat
               als een 3730-bestand voor de sequencer wordt gemaakt voor een niet volle tray,
               dan moet de inhoud van de tray worden "gereshuffeld" bij het maken van het
               bestand (niet in helix!):
               de oneven kolommen 1, 3, 5, 7, 9 en 11 moeten in die volgorde het eerst worden
               gebruikt; de even kolommen 2, 4, 6, 8, 10 en 12 daarna in die volgorde.
               (vb: tray is op eerste 6 kolommen gevuld + op 7e kolom 3 posities;
               reshuffle leidt tot een bestand waarin kolom 2 verplaatst is naar kolom 3, 3 naar 5,
               4 naar 7, 5 naar 9, 6 naar 11 en 7 naar 2).

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19-05-2006  MKL              HLX-KGCIDEF12-11330.
***************************************************************************** */
 v_return varchar2(100);
  cursor trvu_cur
  ( b_trvu_id in number
  )
  is
    select trge.trty_id
    ,      trvu.trge_id
    ,      trvu.x_positie
    ,      trvu.y_positie
    ,      trty.indicatie_hor
    ,      trty.horizontaal
    ,      trty.verticaal
    from   kgc_tray_types trty
    ,      kgc_tray_gebruik trge
    ,      kgc_tray_vulling trvu
    where  trvu.trge_id = trge.trge_id
    and    trge.trty_id = trty.trty_id
    and    trvu.trvu_id = b_trvu_id
   and   trty.vervallen='N' --Added by abhijit for Mantis 2831
    ;
  trvu_rec trvu_cur%rowtype;
  v_y varchar2(3);
  v_x varchar2(3);
  v_x_num number;

FUNCTION is_nummer
( b_waarde IN VARCHAR2
, b_indicatie_hor varchar2
)
RETURN NUMBER
-- Positie kan alfanummeriek worden uitgedrukt; wordt hier vertaald in een nummer
IS
  l_return NUMBER;
BEGIN
  BEGIN
    RETURN( TO_NUMBER( b_waarde ) ); -- waarde is al numeriek
  EXCEPTION
    WHEN OTHERS
    THEN
      NULL;
  END;
  l_return := ASCII( b_waarde ) - ASCII( b_indicatie_hor ) + 1;
  RETURN( l_return );
END is_nummer;

FUNCTION is_letter
( b_waarde IN VARCHAR2
, b_indicatie_hor varchar2
)
RETURN VARCHAR2
-- positie kan alfanummeriek worden uitgedrukt; nummer wordt hier vertaalt
IS
  l_return VARCHAR2(3);
  l_nummer NUMBER;
BEGIN
  BEGIN
    l_nummer := TO_NUMBER( b_waarde );
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN( b_waarde ); -- waarde is niet numeriek
  END;
  IF ( b_indicatie_hor = '1' )
  THEN
    l_return := b_waarde;
  ELSE
    l_return := CHR( ASCII( b_indicatie_hor) + l_nummer - 1 );
  END IF;
  RETURN( l_return );
END is_letter;
BEGIN
  open  trvu_cur( p_trvu_id );
  fetch trvu_cur
  into  trvu_rec;
  close trvu_cur;
  if ( kgc_trge_00.aantal_open_plaatsen
       ( p_trge_id => trvu_rec.trge_id
       , p_trty_id => trvu_rec.trty_id
       ) < 2 * trvu_rec.verticaal
     )
  then
    -- nb.: er zijn meer situaties waarin niet gereshuffled kan worden (bv. alle oneven kolommen gevuld)
    v_return := trvu_rec.y_positie||trvu_rec.x_positie;
  else
    -- reshuffle
    v_return := null;
    v_y := trvu_rec.y_positie; -- y (verticaal) blijft onveranderd
    v_x_num := is_nummer( trvu_rec.x_positie, trvu_rec.indicatie_hor );
    if ( v_x_num <= ( trvu_rec.horizontaal + 1 ) / 2 )
    then -- eerste helft verschuift naar rechts
      v_x_num := ( 2 * v_x_num ) - 1;
    else -- tweede helft verschuift naar links
      v_x_num := ( 2 * v_x_num ) - trvu_rec.horizontaal;
    end if;
    v_x := is_letter( to_char(v_x_num), trvu_rec.indicatie_hor );
    v_return := v_y||v_x;
  end if;
  return( v_return );
END reshuffle_3730;
/

/
QUIT
