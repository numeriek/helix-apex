CREATE OR REPLACE FUNCTION "HELIX"."OADESTROY" (
	a binary_integer)
    return binary_integer as external
	library utils_lib
	name "OADestroy"
	language C;

/

/
QUIT
