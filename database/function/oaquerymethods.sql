CREATE OR REPLACE FUNCTION "HELIX"."OAQUERYMETHODS" (
	a binary_integer,
	b out VARCHAR2)
    return binary_integer as external
	library utils_lib
	name "OAQueryMethods"
	language C;

/

/
QUIT
