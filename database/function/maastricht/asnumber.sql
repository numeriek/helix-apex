CREATE OR REPLACE FUNCTION "HELIX"."ASNUMBER" (p_val IN VARCHAR2) RETURN NUMBER IS
l_val NUMBER;
BEGIN
   l_val := TO_NUMBER(p_val);
   RETURN l_val;
EXCEPTION WHEN VALUE_ERROR THEN
   RETURN null;
END;
/

/
QUIT
