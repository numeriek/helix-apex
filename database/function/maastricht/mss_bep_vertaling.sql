CREATE OR REPLACE FUNCTION "HELIX"."MSS_BEP_VERTALING" ( p_taal_id    in kgc_talen.taal_id%type
                         , p_tabel_naam in kgc_attributen.tabel_naam%type
                         , p_id         in kgc_attribuut_waarden.id%type
                         )
  return kgc_attribuut_waarden.waarde%type
  is
    cursor c_vert ( b_taal_id    in kgc_talen.taal_id%type
                  , b_tabel_naam in kgc_attributen.tabel_naam%type
                  , b_id         in kgc_attribuut_waarden.id%type
                  )
    is
      select atwa.waarde  vertaalde_tekst
      from   kgc_attribuut_waarden  atwa
      ,      kgc_talen              taal
      ,      kgc_attributen         attr
      where  attr.tabel_naam = b_tabel_naam
      and    taal.taal_id = b_taal_id
      and    instr(upper(attr.code), upper(taal.code)) > 0
      and    atwa.attr_id = attr.attr_id
      and    atwa.id = b_id
    ;
    pl_vertaalde_tekst kgc_attribuut_waarden.waarde%type;
    --
  begin
    open  c_vert ( p_taal_id
                 , p_tabel_naam
                 , p_id
                 );
    fetch c_vert
    into  pl_vertaalde_tekst;
    close c_vert;
    --
    return pl_vertaalde_tekst;
  end mss_bep_vertaling;
/

/
QUIT
