CREATE OR REPLACE FUNCTION "HELIX"."MSS_NOMINALE_LOOPTIJD" ( p_indi_id          in kgc_looptijden.indi_id%type
                                                 , p_onderzoekswijze  in kgc_looptijden.onderzoekswijze%type )
return number
is
  cursor c_nom_ltijd ( b_indi_id          in kgc_looptijden.indi_id%type
                     , b_onderzoekswijze  in kgc_looptijden.onderzoekswijze%type )
  is
    select loop.doorlooptijd
    from   kgc_looptijden  loop
    where  loop.indi_id = b_indi_id
    and    loop.onderzoekswijze = b_onderzoekswijze
  ;
  --
  pl_nom_ltijd  number(5);
begin
  open  c_nom_ltijd( p_indi_id
                   , p_onderzoekswijze);
  fetch c_nom_ltijd
  into  pl_nom_ltijd;
  --
  if c_nom_ltijd%NOTFOUND
  then
    pl_nom_ltijd := null;
  else
    null;
  end if;
  --
  close c_nom_ltijd;
  --
  return pl_nom_ltijd;
end mss_nominale_looptijd;
/

/
QUIT
