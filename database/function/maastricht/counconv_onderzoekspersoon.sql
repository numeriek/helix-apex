CREATE OR REPLACE FUNCTION "HELIX"."COUNCONV_ONDERZOEKSPERSOON"
( p_onderznr in varchar2
)
return number -- = persnr
is
  v_persnr number;
  cursor c
  is
    select po.persnr
    from   persoon@kgcl_maas_oud ps
    ,      pers_onderz@kgcl_maas_oud po
    ,      grp_onderz@kgcl_maas_oud go
    where  go.onderznr = p_onderznr
    and    go.odbnr = po.odbnr
    and    po.persnr = ps.persnr
    order by decode( go.proband
                   , 'A', 1
                   , 2
                   ) asc
    ,        decode( ps.gesl
                   , 'V', 1
                   , 'M', 2
                   , 3
                   ) asc
    ;
begin
  if ( p_onderznr not like 'X%' )
  then
    return( null );
  end if;
  open c;
  fetch c into v_persnr;
  close c;
  return( v_persnr );
end;
/

/
QUIT
