CREATE OR REPLACE FUNCTION "HELIX"."BEH_UITS_USERCODE" (p_uits_id number) return varchar2 is
-- externe identificatie van de entiteit UITS - kgc_uitslagen
  cursor c_uits is
    select onde.onderzoeknr || ' ' || brty.code id
    from   kgc_uitslagen uits
    ,      kgc_onderzoeken onde
    ,      kgc_brieftypes brty
    where  uits.uits_id = p_uits_id
    and    uits.onde_id = onde.onde_id
    and    uits.brty_id = brty.brty_id
    ;
  r_uits c_uits%rowtype;
begin
  open c_uits;
  fetch c_uits into r_uits;
  close c_uits;
  return r_uits.id;
end;
/

/
QUIT
