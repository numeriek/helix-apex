CREATE OR REPLACE FUNCTION "HELIX"."TEST_GETXML" return varchar2 is
CR_VertBrf                   Kgc_Bouwsteen_00.t_bouwsteen_cursor;
  CR_Rec MSS_VERTRAGINGSBRFVW%rowtype;
  v_xml_doc                CLOB;
  v_xml_hoofdtag           VARCHAR2 (100) := 'Vertragingsbrieven';
begin
      Kgc_Xml_00.init;

     v_xml_doc := kgc_xml_00.nieuwe_xml (p_naam => v_xml_hoofdtag);
    Open Cr_VertBrf for  'SELECT * FROM  Mss_Vertragingsbrfvw where onderzoeknr = '|| '''D09/0387''';

      If Cr_vertBrf%notfound then
          Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'No Data'
                         ,p_xml_regel     => 'NO DATA FOUND'
                         );
      Else

         Fetch CR_VertBrf into Cr_Rec ;


     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Onderzoeknr'
                         ,p_xml_regel     => Cr_rec.Onderzoeknr
                         );


     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'zisnr'
                         ,p_xml_regel     =>Cr_rec.ZisNr
                         );
     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Geboortedatum'
                         ,p_xml_regel     =>  Cr_rec.Geboortedatum
                         );

     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Geslacht'
                         ,p_xml_regel     => Cr_rec.Geslacht
                         );
     Kgc_Xml_00.voegtoe_xml_regel
                         (p_xml_clob      => v_xml_doc
                         ,p_tag           => 'Aanspreken'
                         ,p_xml_regel     => Cr_rec.Aanspreken
                         );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Datum_Binnen'
                          ,p_xml_regel     => Cr_rec.Datum_Binnen
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Geplande_Einddatum'
                          ,p_xml_regel     => Cr_rec.Geplande_Einddatum
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'AAnvragend_arts'
                          ,p_xml_regel     => Kgc_Xml_00.cr_naar_tags(Cr_rec.Aanvr_arts)
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Systeemdatum'
                          ,p_xml_regel     => cr_rec.SysteemDat
                          );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'gebruiker'
                          ,p_xml_regel     => cr_rec.Gebruiker
                           );
     Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Indicatie'
                          ,p_xml_regel     => cr_rec.Omschrijving);

      Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Datum_binnen2'
                          ,p_xml_regel     => cr_rec.Datum_Binnen);
       Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'functie'
                          ,p_xml_regel     => cr_rec.functie);
      Kgc_Xml_00.voegtoe_xml_regel
                          (p_xml_clob      => v_xml_doc
                          ,p_tag           => 'Aanvr_datum'
                          ,p_xml_regel     => cr_rec.aanvr_datum);



   End If;

    kgc_xml_00.sluit_xml (p_xml_clob     => v_xml_doc
                         ,p_naam         => v_xml_hoofdtag);

   --  kgc_xml_00.voegtoe_xml_doc (p_xml_doc     => v_xml_doc);

    Close CR_VertBrf;
  return(v_xml_doc);
end Test_GetXML;
/

/
QUIT
