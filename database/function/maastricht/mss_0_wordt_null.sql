CREATE OR REPLACE FUNCTION "HELIX"."MSS_0_WORDT_NULL" (p_waarde in number)
return number
is
  pl_return number(5);
begin
  if p_waarde = 0
  then
    pl_return := null;
  else
    pl_return := p_waarde;
  end if;
  --
  return pl_return;
end mss_0_wordt_null;
/

/
QUIT
