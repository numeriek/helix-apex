CREATE OR REPLACE FUNCTION "HELIX"."MY_TO_NUMBER" ( p_str IN VARCHAR2 )
  RETURN NUMBER
IS
  l_num NUMBER;
BEGIN
  BEGIN
    l_num := to_number( p_str );
  EXCEPTION
    WHEN others THEN
      l_num := 0;
  END;

  RETURN l_num;
END;
/

/
QUIT
