CREATE OR REPLACE FUNCTION "HELIX"."SAFE_TO_NUMBER" (p varchar2) return number is
    v number;
  begin
    v := to_number(p);
    return v;
  exception when others then return 0;
end;
/

/
QUIT
