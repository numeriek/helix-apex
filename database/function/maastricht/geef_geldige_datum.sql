CREATE OR REPLACE FUNCTION "HELIX"."GEEF_GELDIGE_DATUM" (p_datum IN VARCHAR2)
    RETURN DATE IS
    v_datum   DATE;
  BEGIN
    v_datum := TO_DATE (p_datum, 'dd-mm-yyyy');
    RETURN v_datum;
  EXCEPTION
    WHEN OTHERS
    THEN
      v_datum := TO_DATE (NULL);
      RETURN v_datum;
  END geef_geldige_datum;
/

/
QUIT
