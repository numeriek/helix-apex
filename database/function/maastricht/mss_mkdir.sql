CREATE OR REPLACE FUNCTION "HELIX"."MSS_MKDIR"
(
 p_dirname IN VARCHAR2
) RETURN VARCHAR2
AS
  LANGUAGE JAVA
  NAME 'MarcWtest.mkdir(java.lang.String) return java.lang.String';
/

/
QUIT
