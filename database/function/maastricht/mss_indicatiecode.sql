CREATE OR REPLACE FUNCTION "HELIX"."MSS_INDICATIECODE" (ONDE_ID Integer) return varchar2
as
 Cursor GetIND
 (ONDEID in Integer)
 is Select IND.CODE
                  From   Kgc_Indicatie_Teksten IND,
                         Kgc_Onderzoek_Indicaties ONIN
                  Where  IND.INDI_ID = ONIN.INDI_ID
                  and    ONIN.ONDE_ID = ONDEID;
 IND_REC  GETIND%Rowtype;
 INDIC varchar(4000);
Begin



   Open GetIND(ONDE_ID) ;
   Loop
     Fetch GetIND into IND_REC;
     If GetIND%NotFound then
        Exit;
     End IF;

     INDIC := INDIC||IND_rec.Code||', '||CHR(10) ;



   End Loop;

   Return(INDIC);

End;
/

/
QUIT
