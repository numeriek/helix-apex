CREATE OR REPLACE FUNCTION "HELIX"."MSS_AUDL_MIN"
(
  p_id             mss_audit_log.id%type
, p_identificatie  mss_audit_log.identificatie%type
)
return varchar2
as
  cursor c_audl(b_id mss_audit_log.id%type)
  is
    select min(to_char(audl.audl_id)||';'||audl.identificatie)
    from   mss_audit_log audl
    where  audl.id = b_id
  ;
  --
  pl_id             varchar2(40);
  pl_identificatie  mss_audit_log.identificatie%type;
  pl_return         varchar2(1);
  --
begin
  open  c_audl(p_id);
  fetch c_audl
  into  pl_id;
  --
  if c_audl%NOTFOUND
  then
    pl_return := 'N';
  else
    pl_identificatie := substr(pl_id, instr(pl_id, ';') + 1, 20);
    if pl_identificatie = p_identificatie
    then
      pl_return := 'J';
    else
      pl_return := 'N';
    end if;
  end if;
  --
  close c_audl;
  return pl_return;
  --
end mss_audl_min;
/

/
QUIT
