CREATE OR REPLACE FUNCTION "HELIX"."BEH_GETONDE_ID" (penti varchar2, pid integer) return integer is
  Result integer;

  cursor c_onbe(ID Integer)
  is
  Select onbe.onde_id
  from kgc_onderzoek_betrokkenen onbe
  Where onbe.onbe_id = id;



  Cursor c_meti(ID Integer)
  is
   Select meti.onde_id
  from bas_metingen meti
  Where meti.meti_id = id;

  ID integer;

begin
   Case penti
     When 'ONDE' Then
         id := pid;
     When 'ONBE' then
       begin
          Open c_onbe(pid);
            fetch c_onbe into id;
          Close c_onbe;
        end;
     When 'METI' Then
          begin
          Open c_meti(pid);
           fetch c_meti into id;
          Close c_meti;

          end;
     else
         null;

    end case ;



  return(id);
end BEH_GETONDE_ID;
/

/
QUIT
