CREATE OR REPLACE FUNCTION "HELIX"."MSS_OPMERKINGSAN" (ONDE_ID Integer) return varchar2
as
 Cursor GetIND
 (ONDEID in Integer)
 is Select  ONIN.OPMERKING
                  From
                         Kgc_Onderzoek_Indicaties ONIN
                  Where
                 ONIN.ONDE_ID = ONDEID;
 IND_REC  GETIND%Rowtype;
 INDIC varchar(4000);
Begin



   Open GetIND(ONDE_ID) ;
   Loop
     Fetch GetIND into IND_REC;
     If GetIND%NotFound then
        Exit;
     End IF;

     INDIC := INDIC||nvl(IND_rec.Opmerking,'-')||','||CHR(10) ;



   End Loop;

   Return(INDIC);

End;
/

/
QUIT
