set define off
---------------------------------------------------------
create or replace and resolve java source named helix."BEH_Backup" as
import java.lang.*;
import java.sql.*;
import oracle.sql.*;
import oracle.jdbc.*;
import java.io.*;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import Bestanden.*;

public class BEH_Backup
{
  private static SimpleDateFormat df_log = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private static SimpleDateFormat df_dir = new SimpleDateFormat("yyyyMMdd_HHmmss");
  private static SimpleDateFormat df_db  = new SimpleDateFormat("yyyyMMddHHmmss");

  private static Connection DBconn = null;
  private static String bestTypeCodeBackup = "BACKUP";
  private static String cattegorieOmschrBackup = "BACKUP";

  private static int btyp_id_backup = -1;
  private static int bcat_id_backup = -1;

  private static int countFiles       = 0;
  private static int countFileChecked = 0;
  private static int countCopies      = 0;

  private static boolean verbose_log = false;


 /*-----------------------------------------------------------------------------
  --- Kopieer de bestanden die minder dan <change_minutes> minuten geleden   ---
  --- gewijzigt zijn naar de backup locatie en registreer het nieuwe bestand ---
  --- in de kgc_bestanden tabel.                                             ---
  -----------------------------------------------------------------------------*/
  public static int backupFiles( int    changeMinutes
                               , String pathWerkbestanden
                               , String pathBackup
                               , String subPathBackup
                               , String logVerbose
                               )
  {
    int return_val = 0;

    countFiles       = 0;
    countFileChecked = 0;
    countCopies      = 0;

    Date startDatum = new Date();
    Calendar cal = Calendar.getInstance();
    cal.setTime(startDatum);
    cal.add(Calendar.MINUTE, -1 * changeMinutes);
    Date controlDatum = cal.getTime();

    /*Maak het backup path met datum en tijd*/
    String reportDate = df_dir.format(startDatum);

    /*Zet de global voor verbose log gebaseerd op de parameter */
    verbose_log = logVerbose.equals("J");
    
    /* subPathBackup wordt gebruikt om de backup op een bepaalde diepte
     * onder de backup directory te beginnen.
     */
    if (subPathBackup != null) {
      pathBackup = pathBackup + reportDate + "\\" + subPathBackup;
    }
    else {
      pathBackup = pathBackup + reportDate;
    }
    
    try
    {
      DBconn = (new oracle.jdbc.driver.OracleDriver()).defaultConnection();
      btyp_id_backup = getBtypID(bestTypeCodeBackup);
      bcat_id_backup = getBcatID(cattegorieOmschrBackup);

      log("Backup bron: " + pathWerkbestanden);
      log("Backup doel: " + pathBackup);
      log("Backup start " + startDatum + ", controle " + controlDatum + "<BR/>");
    }
    catch(SQLException e)
    {
      log("SQL exception in backupFiles: " + e);
    }

    return_val = checkDirectories(controlDatum, pathWerkbestanden, pathBackup);
    
    log("</br>Aantal bestanden gecontroleerd: " + countFiles);
    log("Aantal bestanden tegen DB gecontroleerd: " + countFileChecked);
    log("Aantal bestanden gekopieerd: " + countCopies + "</br></br>");

    return return_val;
  }


  /*----------------------------------------------------------------------------
   * Loop recursief door de directories heen en controleer of er backups
   * gemaakt moeten worden van de bestanden.
   */
  private static int checkDirectories( Date   controleDatum
                                     , String pathWerkbestanden
                                     , String pathBackup
                                     )
  {
    try
    {
      File dir_originelen = new File(pathWerkbestanden);

      if (dir_originelen.exists() && dir_originelen.isDirectory())
      {
        File[] children = dir_originelen.listFiles();
        if (children != null)
        {
          for (File child : children)
          {
            String path_org = child.getCanonicalPath();
            String path_bck = pathBackup + "\\" + getFilename(path_org);

            if (child.isDirectory())
            { checkDirectories(controleDatum, path_org, path_bck);
            }
            else
            {
              countFiles++;

              /* Controleer of lastModifiedDate van het werkBestand na de controleDatum ligt
               * Dit is voornamelijk bedoeld om het aantal te controleren bestanden te beperken.
               */
              if (child.lastModified() >= controleDatum.getTime())
              {
                Date lastModifiedDate = new Date(child.lastModified());
                log("Werkbestand [" + path_org + "] is gewijzigd op " +  df_log.format(lastModifiedDate) + ".");
                checkAndBackup(lastModifiedDate, path_org, path_bck);
                countFileChecked++;                
              }
            }
          }
        }
      }
      else
      {
        log("Het path voor de werkbestanden bestaat niet of is geen directory!");
        return 1; /*pathWerkbestanden bestaat niet of is geen directory!*/
      }
    }
    catch (Exception e)
    {
      log("Exception in checkDirectories: " + e);
      return 2; /*Exception*/
    }

    return 0;
  }


  /*---------------------------------------------------------------------------*/
  private static void checkAndBackup( Date   lastModifiedDate
                                    , String werkBestand
                                    , String backupBestand
                                    )
  {
    /* Controleer of het werkBestand bestaat in de kgc_bestanden tabel en nieuwer is
     * dan de laatste backup in de tabel.
     */
    if (checkAndRegisterBackupInDB(lastModifiedDate, werkBestand, backupBestand))
    {
      return;
    }

    /* Maak de backup aan op de backup locatie. */
    try
    {
      log("Maak kopie naar [" + backupBestand + "]");

      /* Controleer of het directory path bestaat en maak het aan indien nodig.
       */
      String backupBestandBase = getBasename(backupBestand);
      File fileBackupBestandBase = new File(backupBestandBase);

      if (!fileBackupBestandBase.exists())
      {
        fileBackupBestandBase.mkdirs();
      }

      /* Maak de kopie */
      Bestanden.copy(werkBestand, backupBestand);
      countCopies++;
      
      if (verbose_log) { 
        log("<BR/>");
      }      
    }
    catch (Exception e)
    {
      log("Exception in checkAndBackup: " + e);
      return;
    }
  }


  /*----------------------------------------------------------------------------
   * Vergelijk de last_update_date van het werkbestand in de kgc_bestanden tabel
   * met de lastModified date van het bestand.
   * Als deze gelijk of later is, is het bestand al gebackuped en geeft de method True terug.
   * Als deze niet gelijk is wordt er een nieuwe backup geregistreerd in kgc_bestanden
   * en geeft deze method False terug.
   * Een bestand opgeslagen als ORIGINEEL wordt hierbij ook beschouwd als gebackuped.
   */
  private static boolean checkAndRegisterBackupInDB( Date   lastModifiedDate
                                                   , String werkBestand
                                                   , String backupBestand
                                                   )
  {
    boolean backupFound = false;

    try
    {
      String sql = "with backup_best as "
                 +      "( select  best_copy.best_id "
                 +      "       , btyp_copy.code "
                 +      "       , best_copy.entiteit_code "
                 +      "       , best_copy.entiteit_pk "
                 +      "       , best_copy.bestand_specificatie "
                 +      "       , best_copy.last_update_date "
                 +      "  from   kgc_bestanden best_copy "
                 +      "    join kgc_bestand_types btyp_copy on btyp_copy.btyp_id = best_copy.btyp_id "
                 +      "  where  btyp_copy.code = :btyp_backup "
                 +      "    and  best_copy.entiteit_code = 'BEST' "
                 +      ") "
                 +      "select best_copy.best_id  "
                 +      ", best_copy.code  "
                 +      ", best_copy.entiteit_code "
                 +      ", best_copy.entiteit_pk "
                 +      ", best_copy.bestand_specificatie "
                 +      ", best_copy.last_update_date "
                 +      ", best_werk.best_id as best_werk_id "
                 +      ", best_werk.bestand_specificatie as best_werk_specificatie "
                 +      "from   kgc_bestanden best_werk "
                 +      "join kgc_bestand_types btyp_werk on btyp_werk.btyp_id = best_werk.btyp_id and btyp_werk.code <> :btyp_backup "
                 +      "left join backup_best best_copy on best_copy.entiteit_pk = best_werk.best_id "
                 +      "where lower(best_werk.bestand_specificatie) = lower(:werk_bestand) "
                 +      "order by best_copy.last_update_date desc, best_copy.best_id desc";

                 

      String bestand_specificatie = null;
      int    best_werk_id = -1;
      Date   last_update_date = null;

      OraclePreparedStatement stmt = (OraclePreparedStatement)DBconn.prepareStatement(sql);
      stmt.setStringAtName("werk_bestand", werkBestand);
      stmt.setStringAtName("btyp_backup", bestTypeCodeBackup);

      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        bestand_specificatie = rs.getString("bestand_specificatie");
        best_werk_id = rs.getInt("best_werk_id");
        last_update_date = rs.getTimestamp("last_update_date");
      }
      rs.close();
      stmt.close();

      if (best_werk_id != -1) /* Er is een werkbestand gevonden */
      {
        if (last_update_date == null)
        {
          if (verbose_log) {
            log("Geen backup bestand in DB gevonden voor werkbestand." );
            log("Nieuw backup bestand [" + backupBestand + "] wordt aangemaakt." );
          }

          /* Willen we een backup maken als er nog geen ORIGINEEL is?
           * Op dit moment wordt dit wel gedaan!
           */
          registerNewBackupInDB(lastModifiedDate, backupBestand, best_werk_id);
          DBconn.commit();
        }
        else
        {
          if (last_update_date.getTime() < lastModifiedDate.getTime())
          {
            if (verbose_log) {
              log( "Laatste backup bestand gevonden in DB voor werkbestand "
                 + "is bestand [" + bestand_specificatie
                 + "] van datum " + df_log.format(last_update_date) + "."
                 );
              log("Nieuw backup bestand [" + backupBestand + "] wordt aangemaakt." );
            }

            registerNewBackupInDB(lastModifiedDate, backupBestand, best_werk_id);
            DBconn.commit();
          }
          else
          {
            if (verbose_log) {
              log( "Laatste backup bestand gevonden in DB voor werkbestand "
                 + "is bestand [" + bestand_specificatie
                 + "] van LATERE backup datum " + df_log.format(last_update_date) + "."
                 );
            }

            /* Er is een nieuwere backup gevonden, het bestand wordt NIET
             * gekopieerd. */
            backupFound = true;
          }
        }
      }
      else  /* Het werkbestand is niet gevonden in kgc_bestanden, er kan geen
             * backup gemaakt worden omdat deze niet geregistreerd kan worden! */
      {
        log( "Het werkbestand bestaat niet in Helix kgc_bestanden. "
           + "Hierdoor kan er geen backup bestand aangemaakt worden."
           );

        /* Return true zodat het bestand NIET gekopieerd word naar de backup */
        backupFound = true;
      }
    }
    catch(SQLException e)
    {
      log("SQL exception in checkAndRegisterBackupInDB: " + e.getMessage() );
    }

    return backupFound;
  }


  /*----------------------------------------------------------------------------
   * Registreer een nieuw backup bestand in de Helix database tabel kgc_bestanden.
   *
   * Let op: De last_modified_date in kgc_bestanden kan niet handmatig gezet worden.
   * dit is altijd de datumtijd wanneer het record geinsert is.
   */
  private static void registerNewBackupInDB( Date   lastModifiedDate
                                           , String backupBestand
                                           , int    werkbestand_id
                                           )
  {
    try
    {
      String sql = "insert into kgc_bestanden "
                 + "( entiteit_code, entiteit_pk, btyp_id, bestand_specificatie "
                 + ", volgorde, commentaar, locatie, bestandsnaam, rela_id, bcat_id "
                 + ") "
                 + "values "
                 + "( 'BEST', :best_werk_id, :btyp_id_backup, :backup_bestand "
                 + ", null, null, :backup_locatie, :backup_bestandsnaam, null, :bcat_id_backup "
                 + ")";

      OraclePreparedStatement stmt = (OraclePreparedStatement)DBconn.prepareStatement(sql);

      stmt.setIntAtName("best_werk_id", werkbestand_id);
      stmt.setIntAtName("btyp_id_backup", btyp_id_backup);
      stmt.setStringAtName("backup_bestand", backupBestand);
      stmt.setStringAtName("backup_locatie", getBasename(backupBestand));
      stmt.setStringAtName("backup_bestandsnaam", getFilename(backupBestand));
      stmt.setIntAtName("bcat_id_backup", bcat_id_backup);

      stmt.execute();
      stmt.close();
    }
    catch(SQLException e)
    {
      log("SQL exception in registerNewBackupInDB (ins backup): " + e.getMessage() );
    }

    try
    {
      /* Update het veld BEST_LAST_CHG_DATE van het werkbestand in KGC_BETANDEN
       * met de lastModifiedDate van het bestand.
       */
      String sql = "update kgc_bestanden "
                 + "set    best_last_chg_date = to_date(:last_chg_date, 'YYYYMMDDHH24MISS') "
                 + "where  best_id = :best_id ";
                 
      OraclePreparedStatement stmt = (OraclePreparedStatement)DBconn.prepareStatement(sql);

      stmt.setIntAtName("best_id", werkbestand_id);
      stmt.setStringAtName("last_chg_date", df_db.format(lastModifiedDate));

      stmt.execute();
      stmt.close();
      
      /* log("Werkbestand (" + werkbestand_id + ") best_last_chg_date is gewijzigd naar " +  df_db.format(lastModifiedDate) + "."); */
      
    }
    catch(SQLException e)
    {
      log("SQL exception in registerNewBackupInDB (upd werkbest): " + e.getMessage() );
    }

  }


  /*---------------------------------------------------------------------------*/
  /* Get the last part of the path, the parent directory.
   */
  private static String getBasename(String fileName)
  {
    int dir_pos = fileName.lastIndexOf('\\');
    return fileName.substring(0, dir_pos);
  }


  /*---------------------------------------------------------------------------*/
  /* Get the last part of the path, this can be a filename or subdirectory.
   */
  private static String getFilename(String fileName)
  {
    int dir_pos = fileName.lastIndexOf('\\');
    return fileName.substring(dir_pos + 1);
  }


  /*---------------------------------------------------------------------------*/
  private static int getBcatID( String cattegorieOmschrijving )
  {
    int bcat_id = -1;

    try
    {
      String sql = "select bcat_id from kgc_bestand_categorieen where omschrijving = :omschr";
      OraclePreparedStatement stmt = (OraclePreparedStatement)DBconn.prepareStatement(sql);
      stmt.setStringAtName("omschr", cattegorieOmschrijving);
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        bcat_id = rs.getInt("bcat_id");
      }
      rs.close();
      stmt.close();
    }
    catch(SQLException e)
    {
      log("SQL exception in getBcatID: " + e.getMessage() );
    }

    return bcat_id;
  }


  /*---------------------------------------------------------------------------*/
  private static int getBtypID( String bestandTypeCode )
  {
    int btyp_id = -1;

    try
    {
      String sql = "select btyp_id from kgc_bestand_types where code = :btyp_code";
      OraclePreparedStatement stmt = (OraclePreparedStatement)DBconn.prepareStatement(sql);
      stmt.setStringAtName("btyp_code", bestandTypeCode);
      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        btyp_id = rs.getInt("btyp_id");
      }
      rs.close();
      stmt.close();
    }
    catch(SQLException e)
    {
      log("SQL exception in getBcatID: " + e.getMessage() );
    }

    return btyp_id;
  }

  /*---------------------------------------------------------------------------
   * Log de meegegeven text zodat deze via email verstuurd kan worden
   */
  private static void log(String messageText)
  {
    /*System.out.println(messageText);*/

    messageText = messageText + "</br>";

    try
    {
      String sql = "begin beh_digidos_backup.log(:text); end;";
      OraclePreparedStatement stmt = (OraclePreparedStatement)DBconn.prepareStatement(sql);
      stmt.setStringAtName("text", messageText);
      stmt.execute();
      stmt.close();
    }
    catch(SQLException e)
    {
      System.out.println("SQL exception in log: " + e.getMessage() );
    }
  }
}
/


