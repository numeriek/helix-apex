PROMPT Creating Java HELIX."Bestanden2" 
CREATE OR REPLACE AND RESOLVE JAVA SOURCE NAMED HELIX."Bestanden2" as import java.lang.*;
import java.sql.*;
import java.text.*;
import oracle.sql.*;
import oracle.jdbc.driver.*;
import java.io.*;
public class Bestanden2
{
 /******************************************************************************
     NAME:       Bestanden2
     PURPOSE:    Diverse extra functies mbt bestanden

     Deze functies kunnen zo in andere onderdelen van Helix opgenomen worden!

     REVISIONS:
     Ver        Date        Author           Description
     ---------  ----------  ---------------  ------------------------------------
     1          08-09-2010  RDE              Init
     2          02-11-2010  RDE              FileExists toegevoegd
     2          22-04-2013  RDE              MkDir toegevoegd

     NOTES:

  *****************************************************************************
     NAME:       DirList
     PURPOSE:    DirList geeft een array terug met files

                 Voorheen gebeurde dit met een Host-commando "Dir > c:\temp\x.txt"
                 en vervolgens deze uitlezen... problemen met multi-usergebruik!

                 beperking: in de filter kan max 1 * opgenomen worden!

opm: deze functie kan zo in 'Bestanden' worden opgenomen!
opm2: varchar2 is beperkt tot 32K. Liever een CLOB. Echter: pas in java-versie 1.4
      zijn er functies om deze clob normaal te vullen. Huidige versie is 1.3.1
      (opvragen via begin put_line('Versie='|| kgc_zis.testjava); end;)

  *****************************************************************************/
  public static String DirList(String strFileNaam, String strFilter, String strGetAttr, oracle.sql.ARRAY[] listFiles) throws Exception
  {
    String resultString = "OK" ;
    String strFilterUseBeginP = "";
    String strFilterUseEndP = "";
    int v_posWildcard = -1;
    if (strFilter != null) {
      v_posWildcard = strFilter.indexOf("*");
      if (v_posWildcard > -1) {
        if (strFilter.length() > 1) {
          if (v_posWildcard == 0) {
            strFilterUseEndP = strFilter.substring(1);
          }
          else if (v_posWildcard == strFilter.length()) {
            strFilterUseBeginP = strFilter.substring(0, v_posWildcard);
          }
          else {
            strFilterUseBeginP = strFilter.substring(0, v_posWildcard);
            strFilterUseEndP = strFilter.substring(v_posWildcard + 1);
          }
        }
      }
    }
    final String strFilterUseBeginF = strFilterUseBeginP;
    final String strFilterUseEndF = strFilterUseEndP;
    final String strFilterF = strFilter;
    final int v_posWildcardF = v_posWildcard;
    FilenameFilter filter = new FilenameFilter() {
      public boolean accept(File dir, String name) {
        // ai. regexp pas in 1.4!!!
        if (strFilterF == null)
          return true;
        if (strFilterF.length() == 0)
          return true;
        if (v_posWildcardF == -1) { // exact match
          return name.equals(strFilterF);
        }
        // er is een wildcard opgegeven. kijk of name begint met filterbegin en eindigt met filtereind
        boolean v_match = true;
        if (!name.startsWith(strFilterUseBeginF)) {
          v_match = false;
        }
        if (!name.endsWith(strFilterUseEndF)) {
          v_match = false;
        }
        return v_match;
      }
    };
    try{
      File anyDir = new File(strFileNaam);
      if (!anyDir.exists()) {
        resultString = "Bestand " + strFileNaam + " bestaat niet!";
      }
      else {
        String[] chld = anyDir.list(filter);
        if (chld == null) {
          resultString = "Bestand " + strFileNaam + " is geen directory!";
        }
        else {
          if (strGetAttr.equals("J")) { // haal van iedere file de attributen op
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hhmmss");
            for(int i = 0; i < chld.length; i++) {
              String myFileNaam = strFileNaam + "\\" + chld[i];
              File myFile = new File (myFileNaam);
              // type
              if (myFile.isDirectory())
                chld[i] += ">D";
              else if (myFile.isFile())
                chld[i] += ">F";
              else
                chld[i] += ">?";
              // datumtijd modified
              chld[i] += ">" + formatter.format(new Timestamp(myFile.lastModified()));
              // size
              chld[i] += ">" + myFile.length();
            }
          }
          // overzetten naar out-parameter
          Connection conn = new OracleDriver().defaultConnection();
          ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor(listFiles[0].getSQLTypeName(), conn);
          listFiles[0] = new ARRAY(descriptor, conn, chld);
        }
      }
    }
    catch (Exception e) {
      resultString =  ""+e;
    }
    return resultString;
  }

 /******************************************************************************
     NAME:       ImportBlob
     PURPOSE:    File2Blob leest een bestand naar een blob

opm: deze functie kan zo in 'BlobHandler' worden opgenomen!


  *****************************************************************************/
  public static String File2Blob(String strFileNaam, BLOB[] bl) throws Exception
  {
    String resultString = "OK";
    try{
      File binaryFile = new File(strFileNaam);
      if (!binaryFile.exists()) {
        resultString = "Bestand " + strFileNaam + " bestaat niet!";
      }
      else {
        FileInputStream inStream = new FileInputStream(binaryFile);
//        OutputStream outStream = bl[0].getBinaryOutputStream(); rde 03-09-2014 mantis 8960
        // getBinaryOutputStream: Deprecated. This method is deprecated. Use setBinaryStream(1L).
        OutputStream outStream = bl[0].setBinaryStream(1L);

        // Get the optimum buffer size and use this to create the read/write buffer
        int size = inStream.available();

//        resultString += "#size=" + size;
        byte[] buffer = new byte[size];
        int length = -1;
        // Transfer the data
        while ((length = inStream.read(buffer)) != -1)
        {
          outStream.write(buffer, 0, length);
          outStream.flush();
        }
        // Close everything down
        inStream.close();
        outStream.close();
      }
//      resultString += "Klaar";
    }
    catch (Exception e) {
      resultString = ""+e;
    }
    return resultString;
  }
 /******************************************************************************
     NAME:       FileExists
     PURPOSE:    FileExists kijkt of een bestand bestaat

opm: deze functie kan zo in 'Bestanden' worden opgenomen!


  *****************************************************************************/

  public static String FileExists(String strFileNaam) throws Exception
  {
    String resultString = "OK";
    try{
      File binaryFile = new File(strFileNaam);
      if (!binaryFile.exists()) {
        resultString = "Bestand " + strFileNaam + " bestaat niet!";
      }
    }
    catch (Exception e) {
      resultString = ""+e;
    }
    return resultString;
  }

 /******************************************************************************
     NAME:       MkDir
     PURPOSE:    MkDir maakt een map

opm: deze functie kan zo in 'Bestanden' worden opgenomen!


  *****************************************************************************/

  public static String MkDir(String strMapNaam) throws Exception
  {
    String resultString = "OK";
    try {
      File file = new File(strMapNaam);
      if (!file.exists()) {
         if (!file.mkdir()) {
           resultString = "Map " + strMapNaam + " aanmaken is niet gelukt!";
         }
      }
    }
    catch (Exception e) {
      resultString = ""+e;
    }
    return resultString;
  }

 /******************************************************************************
     NAME:       FileLocked
     PURPOSE:    FileLocked kijkt of een bestand gelocked is

opm: deze functie kan zo in 'Bestanden' worden opgenomen!


  *****************************************************************************/
  public static String FileLocked(String strFileNaam) throws Exception
  {
    String resultString = "OK";
    File file = new File(strFileNaam); 

    // try to rename the file with the same name 
    File sameFileName = new File(strFileNaam); 

    if (file.renameTo(sameFileName)) { // if the file is renamed 
    }
    else { // if the file didnt accept the renaming operation 
      resultString = "Bestand " + strFileNaam + " is gelocked!";
    } 
    return resultString;
  }


}

/
