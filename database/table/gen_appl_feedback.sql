CREATE TABLE "HELIX"."GEN_APPL_FEEDBACK"
   (	"ID" NUMBER,
	"APPLICATION_ID" NUMBER,
	"APPLICATION_NAME" VARCHAR2(500 CHAR),
	"PAGE_ID" NUMBER,
	"PAGE_NAME" VARCHAR2(500 CHAR),
	"FEEDBACK_USER" VARCHAR2(240 CHAR),
	"FEEDBACK_DATE" DATE,
	"FEEDBACK_TYPE" VARCHAR2(240 CHAR),
	"FEEDBACK" VARCHAR2(4000 CHAR),
	"STATUS" VARCHAR2(100 CHAR),
	"DATE_CLOSED" DATE,
	"DESCRIPTION" VARCHAR2(4000 CHAR),
	"CREATED_ON" DATE,
	"CREATED_BY" VARCHAR2(30 CHAR),
	"MODIFIED_ON" DATE,
	"MODIFIED_BY" VARCHAR2(30 CHAR),
	 CHECK ("PAGE_ID"        IS NOT NULL) ENABLE,
	 CHECK ("APPLICATION_ID" IS NOT NULL) ENABLE,
	 CHECK ("ID"             IS NOT NULL) ENABLE,
	 CONSTRAINT "GEN_GAF_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"  ENABLE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
