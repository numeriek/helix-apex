CREATE TABLE "HELIX"."LOGGER_PREFS_BY_CLIENT_ID"
   (	"CLIENT_ID" VARCHAR2(64) NOT NULL ENABLE,
	"LOGGER_LEVEL" VARCHAR2(20) NOT NULL ENABLE,
	"INCLUDE_CALL_STACK" VARCHAR2(5) NOT NULL ENABLE,
	"CREATED_DATE" DATE DEFAULT sysdate NOT NULL ENABLE,
	"EXPIRY_DATE" DATE NOT NULL ENABLE,
	 CONSTRAINT "LOGGER_PREFS_BY_CLIENT_ID_CK2" CHECK (expiry_date >= created_date) ENABLE,
	 CONSTRAINT "LOGGER_PREFS_BY_CLIENT_ID_CK3" CHECK (include_call_stack in ('TRUE', 'FALSE')) ENABLE,
	 CONSTRAINT "LOGGER_PREFS_BY_CLIENT_ID_PK" PRIMARY KEY ("CLIENT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"  ENABLE,
	 CONSTRAINT "LOGGER_PREFS_BY_CLIENT_ID_CK1" CHECK (logger_level in ('OFF','PERMANENT','ERROR','WARNING','INFORMATION','DEBUG','TIMING', 'APEX', 'SYS_CONTEXT')) ENABLE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
