CREATE TABLE "HELIX"."EPD_EMBARGO_SETTING"
   (	"EREM_ID" NUMBER NOT NULL ENABLE,
	"CODE" VARCHAR2(50),
	"CREATION_DATUM" DATE,
	"UPDATE_DATE" DATE,
	"VERVALLEN" VARCHAR2(1),
	"KAFD_ID" NUMBER(10,0),
	"ONGR_ID" NUMBER(10,0),
	"EEMT_ID" NUMBER(4,0),
	"WAARDE" NUMBER,
	 CONSTRAINT "ORU_EMBARGO_SETTING_PK" PRIMARY KEY ("EREM_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"  ENABLE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
