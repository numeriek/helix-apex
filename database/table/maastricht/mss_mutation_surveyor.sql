CREATE TABLE "HELIX"."MSS_MUTATION_SURVEYOR"
   (	"TRACE_NR" VARCHAR2(1000),
	"SAMPLE_NAME" VARCHAR2(1000),
	"REFERENCE_NAME" VARCHAR2(1000),
	"NUMBER_OF_MUTATIONS" NUMBER(5,0),
	"ROI_COVERAGE" VARCHAR2(1000),
	"AMPLICON_ID" VARCHAR2(100),
	"VARIANT1" VARCHAR2(1000),
	"VARIANT2" VARCHAR2(1000),
	"VARIANT3" VARCHAR2(1000),
	"VARIANT4" VARCHAR2(1000),
	"VARIANT5" VARCHAR2(1000),
	"SESSIE" VARCHAR2(30)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 131072 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
