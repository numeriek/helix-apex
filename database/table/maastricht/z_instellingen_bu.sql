CREATE TABLE "HELIX"."Z_INSTELLINGEN_BU"
   (	"INST_ID" NUMBER(10,0) NOT NULL ENABLE,
	"CODE" VARCHAR2(10) NOT NULL ENABLE,
	"NAAM" VARCHAR2(100) NOT NULL ENABLE,
	"VERVALLEN" VARCHAR2(1) NOT NULL ENABLE,
	"CREATED_BY" VARCHAR2(30) NOT NULL ENABLE,
	"LAST_UPDATE_DATE" DATE NOT NULL ENABLE,
	"CREATION_DATE" DATE NOT NULL ENABLE,
	"LAST_UPDATED_BY" VARCHAR2(30) NOT NULL ENABLE,
	"ADRES" VARCHAR2(250),
	"POSTCODE" VARCHAR2(7),
	"WOONPLAATS" VARCHAR2(50),
	"PROVINCIE" VARCHAR2(2),
	"LAND" VARCHAR2(50),
	"TELEFOON" VARCHAR2(30),
	"TELEFAX" VARCHAR2(30),
	"EMAIL" VARCHAR2(30)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
