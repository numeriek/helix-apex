CREATE TABLE "HELIX"."Z_SPECIALISME_BU"
   (	"SPEC_ID" NUMBER(10,0) NOT NULL ENABLE,
	"CODE" VARCHAR2(6) NOT NULL ENABLE,
	"OMSCHRIJVING" VARCHAR2(60) NOT NULL ENABLE,
	"VERVALLEN" VARCHAR2(1) NOT NULL ENABLE,
	"CREATED_BY" VARCHAR2(30) NOT NULL ENABLE,
	"CREATION_DATE" DATE NOT NULL ENABLE,
	"LAST_UPDATED_BY" VARCHAR2(30) NOT NULL ENABLE,
	"LAST_UPDATE_DATE" DATE NOT NULL ENABLE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
