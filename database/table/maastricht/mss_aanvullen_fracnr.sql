CREATE TABLE "HELIX"."MSS_AANVULLEN_FRACNR"
   (	"FRACTIENUMMER" VARCHAR2(20),
	"FRACTIEDATUM" DATE,
	"MONSTERNAAM" VARCHAR2(100),
	"MONSTERDATUM" DATE,
	"MATERIAALNAAM" VARCHAR2(100),
	"PATIENTNUMMER" NUMBER(10,0),
	"PATIENT_ZISNR" VARCHAR2(10),
	"FAMILIENUMMER" VARCHAR2(20),
	"HELIX_FAMILIE_ID" NUMBER(10,0),
	"GESLACHT" VARCHAR2(1)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
