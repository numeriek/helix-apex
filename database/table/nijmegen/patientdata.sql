CREATE TABLE "HELIX"."PATIENTDATA"
   (	"DATA_ID" NUMBER(10,0),
	"NR" NUMBER(12,0),
	"GEWICHT_SPIER_MG" NUMBER(15,5),
	"PLUS_AANTAL_ML_SETH" NUMBER(15,5),
	"INC1" NUMBER(15,5),
	"INC2" NUMBER(15,5),
	"INC3" NUMBER(15,5),
	"INC4" NUMBER(15,5),
	"INC5" NUMBER(15,5),
	"INC6" NUMBER(15,5),
	"INC7" NUMBER(15,5),
	"INC8" NUMBER(15,5),
	"INC9" NUMBER(15,5),
	"TOTPYR" NUMBER(15,5),
	"TOTMAL" NUMBER(15,5),
	"TOTSUC" NUMBER(15,5),
	"ZUIVERHEID" NUMBER(15,5),
	"INCTIJD" NUMBER(15,5),
	"NMOLPYR" NUMBER(15,5),
	"NMOLMAL" NUMBER(15,5),
	"ATPPYR" NUMBER(15,5),
	"ATPSUCC" NUMBER(15,5),
	"EIWITH" NUMBER(15,5),
	"COXH" VARCHAR2(50),
	"CSH" VARCHAR2(50),
	"EIWITFIBSUS" VARCHAR2(50),
	"COXFIBSUS" VARCHAR2(50),
	"CSFIBSUS" VARCHAR2(50),
	"RECOVERYCOX" NUMBER(15,5),
	"RECOVERYCS" NUMBER(15,5),
	"CIH" VARCHAR2(50),
	"CIIS" VARCHAR2(50),
	"CIIN" VARCHAR2(50),
	"SCCH" VARCHAR2(50),
	"RATIO_SCCH_DUH2" NUMBER(15,5),
	"CIIIS" VARCHAR2(50),
	"PDHC" VARCHAR2(50),
	"E1" VARCHAR2(50),
	"E3" VARCHAR2(50),
	"AKGDH" VARCHAR2(50),
	"EIWITS" NUMBER(15,5),
	"COXS" VARCHAR2(50),
	"CSS" VARCHAR2(50),
	"CIS" VARCHAR2(50),
	"SCCS" VARCHAR2(50),
	"RATIO_SCCS_DUH2" NUMBER(15,5),
	"E3S" VARCHAR2(50),
	"CV" VARCHAR2(50),
	"CV_CCCP" VARCHAR2(50),
	"COX_CV" VARCHAR2(50),
	"CV_E" VARCHAR2(50),
	"SPIER_EXTRA" VARCHAR2(5),
	"AANTAL_MG_EXTRA_SPIER" VARCHAR2(50),
	"DNA" VARCHAR2(5),
	"PELLET" VARCHAR2(5),
	"HOMOGENAAT" VARCHAR2(50),
	"HOMOGENAAT_50UL" VARCHAR2(50),
	"HOMOGENAAT_EXTRA" VARCHAR2(50),
	"HOMOGENAAT_EXTRA2" VARCHAR2(50),
	"VERBRUIKT_HOMOGENAAT" VARCHAR2(50),
	"PTDT_600GSUP" VARCHAR2(50),
	"PTDT_600GSUP_50UL" VARCHAR2(240),
	"PTDT_600GSUPEXTRA" VARCHAR2(50),
	"PTDT_600GSUPVERDEELD" VARCHAR2(50),
	"VERBRUIKT_600GSUP" VARCHAR2(50),
	"PDHCFTOT" VARCHAR2(50),
	"CSPDHCF" VARCHAR2(50),
	"EIWITPDHCF" VARCHAR2(50),
	"PDHCFBAS" VARCHAR2(50),
	"PCF" VARCHAR2(50),
	"CSPCF" VARCHAR2(50),
	"EIWITPCF" VARCHAR2(50),
	"OPMERKINGEN_WERKBLADEN" VARCHAR2(4000),
	"OPMERKINGEN_OXIDATIES" VARCHAR2(4000),
	"WERKLIJST_CIH" VARCHAR2(5),
	"WERKLIJST_CIS" VARCHAR2(5),
	"WERKLIJST_CIF" VARCHAR2(5),
	"WERKLIJST_CIIS" VARCHAR2(5),
	"WERKLIJST_CIIF" VARCHAR2(5),
	"WERKLIJST_CIIIS" VARCHAR2(5),
	"WERKLIJST_CIIIF" VARCHAR2(5),
	"WERKLIJST_SCCH" VARCHAR2(5),
	"WERKLIJST_SCCS" VARCHAR2(5),
	"WERKLIJST_SCCF" VARCHAR2(5),
	"WERKLIJST_COXH" VARCHAR2(5),
	"WERKLIJST_COXS" VARCHAR2(5),
	"WERKLIJST_COXF" VARCHAR2(5),
	"WERKLIJST_COXFIBSUS" VARCHAR2(5),
	"WERKLIJST_CSH" VARCHAR2(5),
	"WERKLIJST_CSS" VARCHAR2(5),
	"WERKLIJST_CSF" VARCHAR2(5),
	"WERKLIJST_CSFIBSUS" VARCHAR2(5),
	"WERKLIJST_LOWRYH" VARCHAR2(5),
	"WERKLIJST_LOWRYS" VARCHAR2(5),
	"WERKLIJST_LOWRYF" VARCHAR2(5),
	"WERKLIJST_LOWRYFIBSUS" VARCHAR2(5),
	"WERKLIJST_PDHCH" VARCHAR2(5),
	"WERKLIJST_PDHCF" VARCHAR2(5),
	"WERKLIJST_PDHCFCS" VARCHAR2(5),
	"WERKLIJST_PCF" VARCHAR2(5),
	"WERKLIJST_PCFCS" VARCHAR2(5),
	"WERKLIJST_PC_LEVER" VARCHAR2(5),
	"WERKLIJST_E1" VARCHAR2(5),
	"WERKLIJST_E3H" VARCHAR2(5),
	"WERKLIJST_E3S" VARCHAR2(5),
	"WERKLIJST_AKGDH" VARCHAR2(5),
	"WERKLIJST_CV" VARCHAR2(5),
	"WERKLIJST_ATP_PYR" VARCHAR2(5),
	"WERKLIJST_ATP_SUCC" VARCHAR2(5),
	"OPMERKINGEN_CI" VARCHAR2(255),
	"OPMERKINGEN_CII" VARCHAR2(255),
	"OPMERKINGEN_CIII" VARCHAR2(50),
	"OPMERKINGEN_MARKERS" VARCHAR2(50),
	"OPMERKINGENPDHC" VARCHAR2(50),
	"OPMERKINGEN_CV" VARCHAR2(50),
	"OPMERKINGEN_PC" VARCHAR2(50),
	"OPMERKINGEN_PDHCF" VARCHAR2(50),
	"OPMERKINGEN_SCC" VARCHAR2(50),
	"OPMERKINGEN_ATPPYR" VARCHAR2(50),
	"OPMERKINGEN_ATP_SUCC" VARCHAR2(50)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 2097152 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
