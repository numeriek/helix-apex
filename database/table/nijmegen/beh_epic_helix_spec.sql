CREATE TABLE "HELIX"."BEH_EPIC_HELIX_SPEC"
   (	"SPEC_ID" NUMBER(10,0),
	"CODE" VARCHAR2(10),
	"OMSCHRIJVING" VARCHAR2(100),
	"VERVALLEN" VARCHAR2(1),
	"EPIC_CATE_NUMBER" VARCHAR2(10)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
