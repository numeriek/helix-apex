CREATE TABLE "HELIX"."BEH_MITO_DNA_TMP"
   (	"KOLOM1" VARCHAR2(2000),
	"KOLOM2" VARCHAR2(2000),
	"KOLOM3" VARCHAR2(2000),
	"KOLOM4" VARCHAR2(2000),
	"KOLOM5" VARCHAR2(2000),
	"KOLOM6" VARCHAR2(2000),
	"KOLOM7" VARCHAR2(2000),
	"KOLOM8" VARCHAR2(2000),
	"KOLOM9" VARCHAR2(2000),
	"KOLOM10" VARCHAR2(2000),
	"KOLOM11" VARCHAR2(2000),
	"KOLOM12" VARCHAR2(2000),
	"KOLOM13" VARCHAR2(2000),
	"KOLOM14" VARCHAR2(2000),
	"KOLOM15" VARCHAR2(2000),
	"KOLOM16" VARCHAR2(2000),
	"KOLOM17" VARCHAR2(2000),
	"KOLOM18" VARCHAR2(2000),
	"KOLOM19" VARCHAR2(2000),
	"KOLOM20" VARCHAR2(2000)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
