CREATE TABLE "HELIX"."BEH_KASP_MARKERS"
   (	"MARKER_NAME" VARCHAR2(2000),
	"ALLEL_1" VARCHAR2(2000),
	"ALLEL_2" VARCHAR2(2000),
	"OPMERKINGEN" VARCHAR2(2000),
	"STRAND" VARCHAR2(1)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
