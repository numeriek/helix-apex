CREATE TABLE "HELIX"."BEH_HELIX_SER_MATCH_ERNST_03"
   (	"SER_ID" VARCHAR2(10),
	"RELA_ID" VARCHAR2(10),
	"RELA_CODE" VARCHAR2(10),
	"GESLACHT" VARCHAR2(1),
	"TITULATUUR" VARCHAR2(10),
	"RELA_VOORLETTERS" VARCHAR2(10),
	"RELA_VOORVOEGSEL" VARCHAR2(10),
	"RELA_ACHTERNAAM" VARCHAR2(100),
	"RELA_AANSPREKEN" VARCHAR2(100),
	"RELA_SPECIALISME" VARCHAR2(20),
	"RELA_ADRES" VARCHAR2(100),
	"RELA_WOONPLAATS" VARCHAR2(50),
	"RELA_POSTCODE" VARCHAR2(10),
	"RELA_LAND" VARCHAR2(10),
	"RELA_TELEFOON" VARCHAR2(20),
	"RELA_SEIN" VARCHAR2(10),
	"RELA_TELEFAX" VARCHAR2(20),
	"RELA_EMAIL" VARCHAR2(100),
	"RELA_INTERNE_POST" VARCHAR2(50),
	"RELA_AGB_CODE" VARCHAR2(10),
	"INST_ID" VARCHAR2(10),
	"INST_CODE" VARCHAR2(10),
	"INST_NAAM" VARCHAR2(100),
	"INST_ADRES" VARCHAR2(200),
	"INST_POSTCODE" VARCHAR2(10),
	"INST_WOONPLAATS" VARCHAR2(50),
	"INST_LAND_ISO_CODE" VARCHAR2(5),
	"INST_TELEFOON" VARCHAR2(20),
	"INST_TELEFAX" VARCHAR2(20),
	"INST_EMAIL" VARCHAR2(50),
	"INST_ICODE" VARCHAR2(10),
	"INST_AGB_CODE" VARCHAR2(20),
	"EAF_ID" VARCHAR2(20)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
