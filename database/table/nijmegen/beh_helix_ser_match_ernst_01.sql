CREATE TABLE "HELIX"."BEH_HELIX_SER_MATCH_ERNST_01"
   (	"RELA_ID" NUMBER(10,0),
	"SER_ID" NUMBER(10,0),
	"RELA_CODE" VARCHAR2(10),
	"RELA_BRIEFAANHEF" VARCHAR2(23),
	"RELA_VOORLETTERS" VARCHAR2(8),
	"RELA_VOORVOEGSEL" VARCHAR2(7),
	"RELA_ACHTERNAAM" VARCHAR2(25),
	"RELA_AANSPREKEN" VARCHAR2(40),
	"RELA_SPECIALISME" VARCHAR2(37),
	"RELA_ADRES" VARCHAR2(32),
	"RELA_WOONPLAATS" VARCHAR2(18),
	"RELA_POSTCODE" VARCHAR2(7),
	"RELA_LAND" VARCHAR2(9),
	"RELA_TELEFOON" VARCHAR2(18),
	"RELA_TELEFAX" VARCHAR2(12),
	"RELA_EMAIL" VARCHAR2(30),
	"RELA_INTERNE_POST" VARCHAR2(26),
	"RELA_AGB_CODE" VARCHAR2(8),
	"INST_ID" NUMBER(10,0),
	"INST_CODE" VARCHAR2(10),
	"INST_ADRES" VARCHAR2(34),
	"INST_POSTCODE" VARCHAR2(7),
	"INST_WOONPLAATS" VARCHAR2(18),
	"INST_LAND" VARCHAR2(9),
	"INST_TELEFOON" VARCHAR2(15),
	"INST_TELEFAX" VARCHAR2(15),
	"INST_EMAIL" VARCHAR2(20),
	"INST_ICODE" VARCHAR2(7),
	"INST_AGB_CODE" VARCHAR2(8)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
