CREATE TABLE "HELIX"."BEH_FOLDER_FILES_SVZ"
   (	"FILENAAM" VARCHAR2(100),
	"FILE_EXTENSIE" VARCHAR2(10),
	"DIRECTORY" VARCHAR2(100),
	"DATUM_LAATSTE_WIJZ" VARCHAR2(20),
	"TIJD_LAATSTE_WIJZ" VARCHAR2(20),
	"OPMERKING" VARCHAR2(1000),
	"DATUM_SVZ" VARCHAR2(20),
	"MODULE_NAAM" VARCHAR2(100)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 131072 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
