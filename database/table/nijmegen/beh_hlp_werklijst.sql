CREATE TABLE "HELIX"."BEH_HLP_WERKLIJST"
   (	"SESSION_ID" NUMBER,
	"MEET_ID" NUMBER,
	"WERKLIJST_NUMMER" VARCHAR2(20),
	"TRGE_ID" NUMBER,
	"VERWERKT" VARCHAR2(1) DEFAULT 'N',
	"SORTEER_NUMMER" NUMBER,
	"AVAILABLE" VARCHAR2(10),
	"STOF_ID" NUMBER
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
