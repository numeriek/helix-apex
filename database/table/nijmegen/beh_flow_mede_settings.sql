CREATE TABLE "HELIX"."BEH_FLOW_MEDE_SETTINGS"
   (	"FLMS_ID" NUMBER(10,0) NOT NULL ENABLE,
	"MEDE_ID" NUMBER(10,0) NOT NULL ENABLE,
	"FLOW_ID" NUMBER(10,0),
	"COOKIES" VARCHAR2(4000),
	"CREATED_BY" VARCHAR2(30) NOT NULL ENABLE,
	"CREATION_DATE" DATE DEFAULT sysdate NOT NULL ENABLE,
	"LAST_UPDATED_BY" VARCHAR2(30) NOT NULL ENABLE,
	"LAST_UPDATE_DATE" DATE DEFAULT sysdate NOT NULL ENABLE,
	"FILTER_NAAM" VARCHAR2(100),
	 CONSTRAINT "BEH_FLMS_PK" PRIMARY KEY ("FLMS_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"  ENABLE,
	 CONSTRAINT "BEH_FLMS_UK" UNIQUE ("MEDE_ID", "FLOW_ID", "FILTER_NAAM")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"  ENABLE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 0 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
