CREATE TABLE "HELIX"."BEH_FLOW_DNA_STAT_OHW"
   (	"VERBORGEN_KLEUR" CHAR(7),
	"VERBORGEN_SORTERING" NUMBER,
	"VERBORGEN_TELLING" CHAR(1),
	"GROEP" VARCHAR2(10) NOT NULL ENABLE,
	"INDIGROEP" VARCHAR2(6),
	"ONDE_TYPE" VARCHAR2(11),
	"WIJZE" VARCHAR2(100) NOT NULL ENABLE,
	"JAAR_MAAND" VARCHAR2(7),
	"WEEK" VARCHAR2(2),
	"<=30 dagen" NUMBER,
	">30 & <=90 dagen" NUMBER,
	">90 & <=365 dagen" NUMBER,
	">365 dagen" NUMBER
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
