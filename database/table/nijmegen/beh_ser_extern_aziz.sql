CREATE TABLE "HELIX"."BEH_SER_EXTERN_AZIZ"
   (	"ID" VARCHAR2(6),
	"PROVIDER_NAME" VARCHAR2(50),
	"CLINICIAN_TITLE" VARCHAR2(50),
	"EXTERNAL_NAME" VARCHAR2(50),
	"ALIASES" VARCHAR2(50),
	"ABBREVIATION" VARCHAR2(50),
	"TYPE_OF_RESOURCE" VARCHAR2(50),
	"STATUS" VARCHAR2(50),
	"CONTACT_COMMENT" VARCHAR2(100),
	"DEPARTMENTS" VARCHAR2(50),
	"INACTIVE_CADENCE_DEPT" VARCHAR2(50),
	"OUTLOOK_LINK" VARCHAR2(50),
	"TEAM_SUBGROUP" VARCHAR2(50),
	"REF_SOURCE_TYPE" VARCHAR2(50),
	"USE_DEPARTMENT_VISIT_TYPE_LIMI" VARCHAR2(50),
	"INT_REF_SOURCE" VARCHAR2(50),
	"PROVIDER_TYPE" VARCHAR2(50),
	"PROVIDER_SPECIALTY" VARCHAR2(50),
	"SEX" VARCHAR2(50),
	"ORDERS_AUTHORIZING_PROVIDERS" VARCHAR2(50),
	"PREFERRED_COMMUNICATION_METHOD" VARCHAR2(50),
	"MPI_ID_TYPE" VARCHAR2(50),
	"MPI_ID" VARCHAR2(50),
	"MPI_ID_FROM_DATE" VARCHAR2(50),
	"MPI_ID_TO_DATE" VARCHAR2(50),
	"MPI_RETRVL_PP" VARCHAR2(50),
	"MPI_RETRVL_RULE" VARCHAR2(50),
	"ADDR_UNIQUE_ID" VARCHAR2(50),
	"ADDR_STR_LINE_1" VARCHAR2(100),
	"ADDR_STR_LINE_2" VARCHAR2(100),
	"ADDR_STR_LINE_3" VARCHAR2(100),
	"ADDR_CITY" VARCHAR2(50),
	"ADDR_STATE" VARCHAR2(50),
	"ADDR_ZIP_CODE" VARCHAR2(50),
	"ADDR_COUNTY" VARCHAR2(50),
	"ADDR_COUNTRY" VARCHAR2(50),
	"ADDR_HOUSE_NUM" VARCHAR2(50),
	"ADDR_DISTRICT" VARCHAR2(50),
	"ADDR_IS_PRIMARY" VARCHAR2(50),
	"ADDR_PHONE" VARCHAR2(50),
	"ADDR_FAX" VARCHAR2(50),
	"ADDR_FAX_REGISTER_DATE" VARCHAR2(50),
	"ADDR_ACTIVE" VARCHAR2(50),
	"ADDR_EMAIL" VARCHAR2(50),
	"ADDR_PRCTC_NAME" VARCHAR2(50),
	"ADDR_CONTACT_METHOD" VARCHAR2(50),
	"ADDR_PRINTER_DEVICE" VARCHAR2(50),
	"ADDR_EXT_ID" VARCHAR2(50),
	"ADDR_DIRECT_ADDRESS" VARCHAR2(50),
	"ADDR_ORGANIZATION" VARCHAR2(50),
	"ADDR_SHARED" VARCHAR2(50),
	"ADDR_INTERNAL" VARCHAR2(50),
	"ADDR_LOCATION" VARCHAR2(50),
	"EAF_LINK" VARCHAR2(50)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
