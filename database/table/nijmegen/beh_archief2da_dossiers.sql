CREATE TABLE "HELIX"."BEH_ARCHIEF2DA_DOSSIERS" 
   (	"B_AD_ID" NUMBER, 
	"DOOS" VARCHAR2(20 BYTE) NOT NULL ENABLE, 
	"DOSSIER" VARCHAR2(20 BYTE) NOT NULL ENABLE, 
	"DATUM_INTAKE" DATE, 
	"DATUM_VERWERKT" DATE, 
	"DATUM_CONTROLE" DATE, 
	"FAMILIENUMMER" VARCHAR2(20 BYTE), 
	"BESTANDSNAAM" VARCHAR2(1000 BYTE), 
	"BEST_ID" NUMBER, 
	"OPMERKINGEN" VARCHAR2(4000 BYTE), 
	"DATUM_BESTAND" DATE, 
	"CREATED_BY" VARCHAR2(30 BYTE) NOT NULL ENABLE, 
	"CREATION_DATE" DATE NOT NULL ENABLE, 
	"LAST_UPDATED_BY" VARCHAR2(30 BYTE) NOT NULL ENABLE, 
	"LAST_UPDATE_DATE" DATE NOT NULL ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;
/
QUIT
