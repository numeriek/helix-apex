CREATE TABLE "HELIX"."BEH_HERSTEL_LANDEN_LOG"
   (	"TABEL_NAAM" VARCHAR2(100),
	"ID" NUMBER(10,0),
	"WAARDE_OUD" VARCHAR2(100),
	"WAARDE_NIEUW" VARCHAR2(100),
	"CREATION_DATUM" DATE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
