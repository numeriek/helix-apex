CREATE TABLE "HELIX"."BEH_DECL_DUBBEL_5"
   (	"PATIENTNR" VARCHAR2(9),
	"PATIENTNAAM" VARCHAR2(25),
	"FACTUURNUMMER" VARCHAR2(15),
	"ZGV" VARCHAR2(12),
	"BEDRAG" VARCHAR2(8),
	"DECL_ID" VARCHAR2(6),
	"UITVOERDATUM" VARCHAR2(10),
	"BESTANDSNAAM" VARCHAR2(100)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
