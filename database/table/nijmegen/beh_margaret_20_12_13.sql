CREATE TABLE "HELIX"."BEH_MARGARET_20_12_13"
   (	"RELA_ID" NUMBER(10,0),
	"SER_ID" VARCHAR2(10),
	"RELA_CODE" VARCHAR2(10),
	"RELA_AANSPREKEN" VARCHAR2(100),
	"RELA_ADRES" VARCHAR2(2000),
	"RELA_POSTCODE" VARCHAR2(7),
	"RELA_LAND" VARCHAR2(24),
	"RELA_WOONPLAATS" VARCHAR2(22),
	"RELA_BRIEFAANHEF" VARCHAR2(32),
	"RELA_VOORLETTERS" VARCHAR2(10),
	"RELA_VOORVOEGSEL" VARCHAR2(7),
	"RELA_ACHTERNAAM" VARCHAR2(30),
	"RELA_SPECIALISME" VARCHAR2(37),
	"RELA_TELEFOON" VARCHAR2(28),
	"RELA_TELEFAX" VARCHAR2(15),
	"RELA_EMAIL" VARCHAR2(30),
	"RELA_INTERNE_POST" VARCHAR2(30),
	"RELA_AGB_CODE" VARCHAR2(8),
	"INST_ID" VARCHAR2(5),
	"INST_CODE" VARCHAR2(10),
	"INST_ADRES" VARCHAR2(2000),
	"INST_POSTCODE" VARCHAR2(7),
	"INST_WOONPLAATS" VARCHAR2(22),
	"INST_LAND" VARCHAR2(24),
	"INST_TELEFOON" VARCHAR2(16),
	"INST_TELEFAX" VARCHAR2(15),
	"INST_EMAIL" VARCHAR2(25),
	"INST_ICODE" VARCHAR2(7),
	"INST_AGB_CODE" VARCHAR2(8)
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
