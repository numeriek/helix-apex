CREATE TABLE "HELIX"."GEN_REPORT_QUERIES"
   (	"ID" NUMBER NOT NULL ENABLE,
	"RDN_ID" NUMBER,
	"QUERY_NAME" VARCHAR2(255 CHAR),
	"QUERY_TEXT" NCLOB,
	"CREATED_ON" DATE,
	"CREATED_BY" VARCHAR2(50 CHAR),
	"MODIFIED_ON" DATE,
	"MODIFIED_BY" VARCHAR2(30 CHAR),
	"QUERY_TYPE" VARCHAR2(1 CHAR),
	 CONSTRAINT "GEN_RQE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"  ENABLE,
	 CONSTRAINT "GEN_RQE_FK1" FOREIGN KEY ("RDN_ID")
	  REFERENCES "HELIX"."GEN_REPORT_DEFINITIONS" ("ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"
 LOB ("QUERY_TEXT") STORE AS BASICFILE (
  TABLESPACE "TS_HELIXPRD" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION
  NOCACHE LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;

/
QUIT
