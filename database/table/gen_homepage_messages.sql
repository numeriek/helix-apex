CREATE TABLE "HELIX"."GEN_HOMEPAGE_MESSAGES"
   (	"ID" NUMBER,
	"APPLICATION_ID" NUMBER,
	"MESSAGE_TEXT" VARCHAR2(2000 CHAR),
	"ACTIVE_IND" VARCHAR2(1 CHAR) DEFAULT 'Y',
	"START_DATE" DATE DEFAULT sysdate,
	"END_DATE" DATE,
	"CREATED_ON" DATE,
	"CREATED_BY" VARCHAR2(30 CHAR),
	"MODIFIED_ON" DATE,
	"MODIFIED_BY" VARCHAR2(30 CHAR),
	 CHECK ("START_DATE"   IS NOT NULL) ENABLE,
	 CHECK ("ACTIVE_IND"   IS NOT NULL) ENABLE,
	 CHECK ("MESSAGE_TEXT" IS NOT NULL) ENABLE,
	 CONSTRAINT "GEN_HPM_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD"  ENABLE
   ) SEGMENT CREATION IMMEDIATE
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TS_HELIXPRD" ;

/
QUIT
