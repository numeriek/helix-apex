CREATE OR REPLACE TYPE "HELIX"."KGC_REC_STOF_METING" AS OBJECT
 (STOF_ID NUMBER(10)
 ,STOF_OMS VARCHAR2(100)
 ,T_METING_SET KGC_TAB_METING
 ,OMF_SET_1 VARCHAR2(25)
 ,OMF_SET_2 VARCHAR2(25)
 ,OMF_SET_3 VARCHAR2(25)
 ,OMF_SET_4 VARCHAR2(25)
 ,OMF_SET_5 VARCHAR2(25)
 ,OMF_SET_6 VARCHAR2(25)
 ,OMF_SET_7 VARCHAR2(25)
 ,OMF_SET_8 VARCHAR2(25)
 ,OMF_SET_9 VARCHAR2(25)
 ,OMF_SET_10 VARCHAR2(25)
 ,OMF_SET_11 VARCHAR2(25)
 ,OMF_SET_12 VARCHAR2(25)
 ,OMF_SET_13 VARCHAR2(25)
 ,OMF_SET_14 VARCHAR2(25)
 ,OMF_SET_15 VARCHAR2(25)
 ,OMF_SET_16 VARCHAR2(25)
 ,OMF_SET_17 VARCHAR2(25)
 ,OMF_SET_18 VARCHAR2(25)
 ,OMF_SET_19 VARCHAR2(25)
 ,OMF_SET_20 VARCHAR2(25)
 ,OMF_SET_21 VARCHAR2(25)
 ,OMF_SET_22 VARCHAR2(25)
 ,OMF_SET_23 VARCHAR2(25)
 ,OMF_SET_24 VARCHAR2(25)
 ,OMF_SET_25 VARCHAR2(25)
 ,OMF_SET_26 VARCHAR2(25)
 ,OMF_SET_27 VARCHAR2(25)
 ,OMF_SET_28 VARCHAR2(25)
 ,OMF_SET_29 VARCHAR2(25)
 ,OMF_SET_30 VARCHAR2(25)
 ,OMF_SET_31 VARCHAR2(25)
 ,OMF_SET_32 VARCHAR2(25)
 ,OMF_SET_33 VARCHAR2(25)
 ,OMF_SET_34 VARCHAR2(25)
 ,OMF_SET_35 VARCHAR2(25)
 ,OMF_SET_36 VARCHAR2(25)
 ,OMF_SET_37 VARCHAR2(25)
 ,OMF_SET_38 VARCHAR2(25)
 ,OMF_SET_39 VARCHAR2(25)
 ,OMF_SET_40 VARCHAR2(25)
 ,OMF_SET_41 VARCHAR2(25)
 ,OMF_SET_42 VARCHAR2(25)
 ,OMF_SET_43 VARCHAR2(25)
 ,OMF_SET_44 VARCHAR2(25)
 ,OMF_SET_45 VARCHAR2(25)
 ,OMF_SET_46 VARCHAR2(25)
 ,OMF_SET_47 VARCHAR2(25)
 ,OMF_SET_48 VARCHAR2(25)
 ,OMF_SET_49 VARCHAR2(25)
 ,OMF_SET_50 VARCHAR2(25)
 ,OMF_SET_51 VARCHAR2(25)
 ,OMF_SET_52 VARCHAR2(25)
 ,OMF_SET_53 VARCHAR2(25)
 ,OMF_SET_54 VARCHAR2(25)
 ,OMF_SET_55 VARCHAR2(25)
 ,OMF_SET_56 VARCHAR2(25)
 ,OMF_SET_57 VARCHAR2(25)
 ,OMF_SET_58 VARCHAR2(25)
 ,OMF_SET_59 VARCHAR2(25)
 ,OMF_SET_60 VARCHAR2(25)
 ,OMF_SET_61 VARCHAR2(25)
 ,OMF_SET_62 VARCHAR2(25)
 ,OMF_SET_63 VARCHAR2(25)
 ,OMF_SET_64 VARCHAR2(25)
 ,OMF_SET_65 VARCHAR2(25)
 ,OMF_SET_66 VARCHAR2(25)
 ,OMF_SET_67 VARCHAR2(25)
 ,OMF_SET_68 VARCHAR2(25)
 ,OMF_SET_69 VARCHAR2(25)
 ,OMF_SET_70 VARCHAR2(25)
 ,OMF_SET_71 VARCHAR2(25)
 ,OMF_SET_72 VARCHAR2(25)
 ,OMF_SET_73 VARCHAR2(25)
 ,OMF_SET_74 VARCHAR2(25)
 ,OMF_SET_75 VARCHAR2(25)
 ,OMF_SET_76 VARCHAR2(25)
 ,OMF_SET_77 VARCHAR2(25)
 ,OMF_SET_78 VARCHAR2(25)
 ,OMF_SET_79 VARCHAR2(25)
 ,OMF_SET_80 VARCHAR2(25)
 ,OMF_SET_81 VARCHAR2(25)
 ,OMF_SET_82 VARCHAR2(25)
 ,OMF_SET_83 VARCHAR2(25)
 ,OMF_SET_84 VARCHAR2(25)
 ,OMF_SET_85 VARCHAR2(25)
 ,OMF_SET_86 VARCHAR2(25)
 ,OMF_SET_87 VARCHAR2(25)
 ,OMF_SET_88 VARCHAR2(25)
 ,OMF_SET_89 VARCHAR2(25)
 ,OMF_SET_90 VARCHAR2(25)
 ,OMF_SET_91 VARCHAR2(25)
 ,OMF_SET_92 VARCHAR2(25)
 ,OMF_SET_93 VARCHAR2(25)
 ,OMF_SET_94 VARCHAR2(25)
 ,OMF_SET_95 VARCHAR2(25)
 ,OMF_SET_96 VARCHAR2(25)
 ,OMF_SET_97 VARCHAR2(25)
 ,OMF_SET_98 VARCHAR2(25)
 ,OMF_SET_99 VARCHAR2(25)
 ,OMF_SET_100 VARCHAR2(25)
 )
/

/
QUIT
