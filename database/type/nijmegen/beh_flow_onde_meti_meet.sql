CREATE OR REPLACE TYPE "HELIX"."BEH_FLOW_ONDE_METI_MEET"
as object(

ONDE_ID number,
TOTAAL_METI number,
METI_AF number,
METI_NIET_AF number,
TOTAAL_MEET number,
MEET_AF number,
MEET_NIET_AF number,
NCONT number,
WCONT number,
NCONT_V number,
NCONT_S number,
NCONT_M number,
WCONT_V number,
WCONT_S number,
WCONT_M number
)
/

/
QUIT
