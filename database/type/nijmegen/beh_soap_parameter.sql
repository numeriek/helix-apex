CREATE OR REPLACE TYPE "HELIX"."BEH_SOAP_PARAMETER" as
       object
       (
        name varchar2(255),
        value varchar2(32767)
       );
/

/
QUIT
