CREATE OR REPLACE TYPE "HELIX"."T_DFT_AANVRAGER" as object
(
  -- Author  : RKON
  -- Created : 9-4-2014 11:45:33
  -- Purpose : Bevat Declaratie codes van de aanvrager.

  -- Attributes
     Order_provider  varchar2(20),
     Order_prov_Dep  varchar2(10),
     Order_prov_Type varchar2(2),
     Order_prov_nm   varchar2(200),
     Order_prov_check varchar2(20),
     Order_Prov_Error  varchar2(3),

  -- Member functions and procedures

   member procedure init,

  member procedure AddAanvragerCodes( p_Order_provider  varchar2,
                                      p_Order_prov_Dep  varchar2,
                                      p_Order_prov_Type varchar2,
                                      p_Order_prov_nm   varchar2,
                                      p_order_prov_check varchar2,
                                      p_Order_Prov_Error  varchar2
                                    )
)
/
CREATE OR REPLACE TYPE BODY "HELIX"."T_DFT_AANVRAGER" is

  -- Member procedures and functions

 member procedure init
 Is
 Begin
     Order_provider   := Null;
     Order_prov_Dep   := Null;
     Order_prov_Type  := Null;
     Order_prov_nm    := Null;
     Order_prov_Check := Null;
     Order_Prov_Error := Null;
 End;


 member procedure AddAanvragerCodes( p_Order_provider  varchar2,
                                     p_Order_prov_Dep  varchar2,
                                     p_Order_prov_Type varchar2,
                                     p_Order_prov_nm    varchar2,
                                     p_order_prov_check varchar2,
                                     p_Order_Prov_Error varchar2
                                    )
 Is
 Begin
     Order_provider  := p_Order_provider;
     Order_prov_Dep  := p_Order_prov_Dep;
     Order_prov_Type := p_Order_prov_Type;
     Order_prov_nm   := p_Order_prov_nm;
     Order_prov_Check :=  p_order_prov_check;
     Order_Prov_Error := p_Order_Prov_Error;
 End;

end;
/

/
QUIT
